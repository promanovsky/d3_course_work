# -*- coding: utf-8 -*-
""" Download news articles from the urls given in News Aggregator Dataset
    ref: https://archive.ics.uci.edu/ml/datasets/News+Aggregator

    https://towardsdatascience.com/keyword-extraction-with-bert-724efca412ea
    https://towardsdatascience.com/conditional-text-generation-by-fine-tuning-gpt-2-11c1a9fc639d
    https://medium.com/swlh/fine-tuning-gpt-2-for-magic-the-gathering-flavour-text-generation-3bafd0f9bb93
"""

import os
import pandas as pd
import numpy as np
import time
import csv
import re
from sklearn.feature_extraction.text import CountVectorizer
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity

# ------------------------------------------------------------------------------#
# Configuration
# ------------------------------------------------------------------------------#
URL = 'https://archive.ics.uci.edu/ml/machine-learning-databases/00359/NewsAggregatorDataset.zip'

NEWS_AGGREGATOR_DIR = 'news_aggregator'
ARTICLES_DIR = 'articles'

# DEBUG               = True
DEBUG = False
RE_D = re.compile('\d')
TOP_N = 20
TFIDF_THRESHOLD = 0.1


# ------------------------------------------------------------------------------#
# ------------------------------------------------------------------------------#
def load_articles(df):
    data = {'id': [],
            'category': [],
            'title': [],
            'text': []}

    for root, dirs, files in os.walk(ARTICLES_DIR, topdown=True):
        t0 = time.time()

        for i, f in enumerate(files):
            # id, category, title, keywords, text
            id = int(f[:-4])
            tmp = df[['CATEGORY', 'TITLE']][df.ID == id].values
            category, title = tmp[0][0], tmp[0][1]

            with open(f'{ARTICLES_DIR}/{f}', "r", encoding="utf-8") as infile:
                text = infile.read()

            data['id'].append(id)
            data['category'].append(category)
            data['title'].append(title)
            data['text'].append(text)

            if DEBUG and i >= 100:
                break

            if i % 1000 == 0 and i > 0:
                print(f"({os.getpid()}) Items processed: {i :,}/{len(files):,}; {(time.time() - t0) / 60 :.1f} minutes")

    print(f"Number of articles: {len(data['text']) :,}")

    return data


# --------------------------------------------------------------------#

def has_numbers(string, regex_pattern=RE_D):
    return bool(regex_pattern.search(string))


# --------------------------------------------------------------------#

n_gram_range = (3, 3)
stop_words = "english"


def mmr(doc_embedding, word_embeddings, words, top_n, diversity):
    word_doc_similarity = cosine_similarity(word_embeddings, doc_embedding)
    word_similarity = cosine_similarity(word_embeddings)

    keywords_idx = [np.argmax(word_doc_similarity)]
    candidates_idx = [i for i in range(len(words)) if i != keywords_idx[0]]

    for _ in range(top_n - 1):
        candidate_similarities = word_doc_similarity[candidates_idx, :]
        target_similarities = np.max(word_similarity[candidates_idx][:, keywords_idx], axis=1)

        mmr = (1 - diversity) * candidate_similarities - diversity * target_similarities.reshape(-1, 1)
        if mmr.size:
            mmr_idx = candidates_idx[np.argmax(mmr)]
            keywords_idx.append(mmr_idx)
            candidates_idx.remove(mmr_idx)

    return [words[idx] for idx in keywords_idx]


def get_keywords(corpus):
    t0 = time.time()

    model = SentenceTransformer('distilbert-base-nli-mean-tokens')
    keywords = []
    N = len(corpus)

    for i, text in enumerate(corpus):

        count = CountVectorizer(ngram_range=n_gram_range, stop_words=stop_words).fit([text])
        candidates = count.get_feature_names()

        doc_embedding = model.encode([text])
        candidate_embeddings = model.encode(candidates)

        kw = mmr(doc_embedding, candidate_embeddings, candidates, top_n=5, diversity=0.7)
        keywords.append(kw)

        if i % 1000 == 0 and i > 0:
            print(f"({os.getpid()}) Items processed: {i :,}/{N:,}; {(time.time() - t0) / 60 :.1f} minutes")

    return keywords


# --------------------------------------------------------------------#

def run():
    # Load News Aggregator Data with URLs --------------#
    columns = ['ID', 'TITLE', 'URL', 'PUBLISHER', 'CATEGORY', 'Alphanumeric ID',
               'HOSTNAME Url', 'TIMESTAMP']

    df = pd.read_csv(f"{NEWS_AGGREGATOR_DIR}/newsCorpora.csv",
                     sep='\t', header=None, names=columns)

    data = load_articles(df)

    keywords = get_keywords(data['text'])

    with open('keywords_advanced.csv', mode='w', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for id, kw in zip(data['id'], keywords):
            line = [id] + kw
            writer.writerow(line)


# ------------------------------------------------------------------------------#
# ------------------------------------------------------------------------------#

if __name__ == '__main__':
    T0 = time.time()
    run()
    print(f"Done in {(time.time() - T0) / 60 :.1f} minutes")

"""
(14840) Items processed: 1,000/25,366; 0.2 minutes
(14840) Items processed: 2,000/25,366; 0.5 minutes
(14840) Items processed: 3,000/25,366; 0.7 minutes
(14840) Items processed: 4,000/25,366; 0.9 minutes
(14840) Items processed: 5,000/25,366; 1.2 minutes
(14840) Items processed: 6,000/25,366; 1.4 minutes
(14840) Items processed: 7,000/25,366; 1.6 minutes
(14840) Items processed: 8,000/25,366; 1.9 minutes
(14840) Items processed: 9,000/25,366; 2.1 minutes
(14840) Items processed: 10,000/25,366; 2.3 minutes
(14840) Items processed: 11,000/25,366; 2.5 minutes
(14840) Items processed: 12,000/25,366; 2.8 minutes
(14840) Items processed: 13,000/25,366; 3.0 minutes
(14840) Items processed: 14,000/25,366; 3.2 minutes
(14840) Items processed: 15,000/25,366; 3.4 minutes
(14840) Items processed: 16,000/25,366; 3.7 minutes
(14840) Items processed: 17,000/25,366; 3.9 minutes
(14840) Items processed: 18,000/25,366; 4.1 minutes
(14840) Items processed: 19,000/25,366; 4.3 minutes
(14840) Items processed: 20,000/25,366; 4.6 minutes
(14840) Items processed: 21,000/25,366; 4.8 minutes
(14840) Items processed: 22,000/25,366; 5.0 minutes
(14840) Items processed: 23,000/25,366; 5.3 minutes
(14840) Items processed: 24,000/25,366; 5.5 minutes
(14840) Items processed: 25,000/25,366; 5.7 minutes
Number of articles: 25,366
(14840) Items processed: 1,000/25,366; 17.8 minutes
(14840) Items processed: 2,000/25,366; 33.0 minutes
(14840) Items processed: 3,000/25,366; 49.3 minutes
(14840) Items processed: 4,000/25,366; 66.4 minutes
(14840) Items processed: 5,000/25,366; 83.3 minutes
(14840) Items processed: 6,000/25,366; 99.1 minutes
(14840) Items processed: 7,000/25,366; 115.4 minutes
(14840) Items processed: 8,000/25,366; 130.9 minutes
(14840) Items processed: 9,000/25,366; 146.8 minutes
(14840) Items processed: 10,000/25,366; 163.7 minutes
(14840) Items processed: 11,000/25,366; 180.4 minutes
(14840) Items processed: 12,000/25,366; 196.8 minutes
(14840) Items processed: 13,000/25,366; 212.5 minutes
(14840) Items processed: 14,000/25,366; 228.6 minutes
(14840) Items processed: 15,000/25,366; 245.2 minutes
(14840) Items processed: 16,000/25,366; 261.7 minutes
(14840) Items processed: 17,000/25,366; 278.3 minutes
(14840) Items processed: 18,000/25,366; 293.8 minutes
(14840) Items processed: 19,000/25,366; 310.2 minutes
(14840) Items processed: 20,000/25,366; 326.5 minutes
(14840) Items processed: 21,000/25,366; 343.3 minutes
(14840) Items processed: 22,000/25,366; 359.8 minutes
(14840) Items processed: 23,000/25,366; 376.6 minutes
(14840) Items processed: 24,000/25,366; 392.4 minutes
(14840) Items processed: 25,000/25,366; 409.3 minutes
Done in 421.8 minutes

Process finished with exit code 0
"""
