Facebook is developing its own video-chat app, known internally as Slingshot, after its failed attempt to acquire mobile messaging startup Snapchat, the Financial Times reported on Sunday.

Facebook has been working for several months on the planned video-chat app, which would allow users to send short video messages using a touch-screen, the FT said, citing people familiar with its plans.

Slingshot could be launched this month, the paper cited one source as saying, while noting that Facebook might ultimately decide not to proceed with the launch of the app.

Facebook chief Mark Zuckerberg has been overseeing the “top-secret” project after the social network company was unable to win Snapchat’s creators, Evan Spiegel and Bobby Murphy, with its $3 billion takeover offer late last year, the FT said.

Snapchat, popular among teenagers, allows users to send smartphone photos that automatically disappear after a few seconds.