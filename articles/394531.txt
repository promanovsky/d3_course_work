WASHINGTON, July 28 (UPI) -- So-called nuisance flooding along the coast is on the rise, courtesy of rising sea levels and global warming, according to a new report from the NOAA.

"It no longer takes a strong storm or a hurricane to cause flooding," explained William Sweet, NOAA oceanographer and lead author of the new report. "Flooding now occurs with high tides in many locations due to climate-related sea-level rise, land subsidence and the loss of natural barriers."

Advertisement

According to the new report, nuisance flooding -- which is less violent but still disruptive -- has risen on all three U.S. coasts (East, West, and Gulf) between 300 and 925 percent since the 1960s.

The study was compiled by researchers at the NOAA's Center for Operational Oceanographic Products and Services who analyzed NOAA's water level records as well as local flooding data.

While nuisance flooding affects all three of the U.S. coasts, eight of the ten cities most affected by the increase in nuisance flooding are located along the Atlantic Coast. Annapolis and Baltimore, in Maryland, occupy the top two spots, followed by Atlantic City, New Jersey, and Philadelphia, Pennsylvania. Sandy Hook, New Jersey rounds out the top five. San Francisco and Port Isabel, Texas, are the only two non-Atlantic ports in the top ten.

Scientists say nuisance flooding events can only be expected to intensify and become more frequent as sea levels continue to rise.