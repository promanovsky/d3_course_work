Video of Paralyzed Men Regaining Use of Legs After Novel Implant

Four men, originally paralyzed below the waist, have regained movement in their legs after a novel procedure at the University of Louisville’s Kentucky Spinal Cord Injury Research Center.

According to a report by New Scientist, 16 electrodes are implanted at the lumbosacral region of the spine, which resides in the lower back. Researchers say that, despite injury, the nervous system in that area of the spinal cord retains the ability to send messages.

“We think it’s a very large milestone,” says Claudia Angeli of the University of Louisville’s Kentucky Spinal Cord Injury Research Center. “There’s not been anything like this, and no hope previously for the most severely injured patients, so this is a very important step forward for them.”

Researchers are looking to increase the number of electrodes in the device to 27, hoping to provide better coordination.

In addition, the implant has helped to change the livelihood of its patients, restoring “bladder, bowel and sexual function” to varying degrees.

Eight more patients are in line for testing