B. Saxton (NRAO/AUI/NSF)

Humans have attached a lot of mystique and value to diamonds, a crystalline form of pure carbon we're fond of digging out of the ground and romanticizing when it comes to engagements. If you think a "huge rock" would make a favorable impression on your future fiance or fiancee, then maybe you should figure out a way to go capture a newly discovered white dwarf star astronomers are calling "an Earth-size diamond in space."

Scientists at the National Radio Astronomy Observatory tracked down the very cold and dim star. It's so cold, its carbon is crystallized, hence the giant space-diamond talk. It was first noticed when astronomers located its pulsar companion, the charmingly named PSR J2222-0137.

The white dwarf may well be one of the coolest ones ever observed, both temperature-wise and in terms of the envy it no doubt fosters in people who covet diamonds. The astronomers calculate its temperature at only 3,000 degrees Kelvin, which is close to 5,000 degrees Fahrenheit. That sounds pretty hot to us earthlings, but it puts it closer to ice-cube status in terms of star temps.

"It's a really remarkable object. These things should be out there, but because they are so dim they are very hard to find," says David Kaplan, a professor at the University of Wisconsin-Milwaukee and one of the authors of a paper on the star published in The Astrophysical Journal on Friday.

A couple years of observations led researchers to conclude that the pulsar and its white-dwarf buddy are located about 900 light-years away from Earth, towards the Aquarius constellation, in case you need a direction to point your space-mining expedition.