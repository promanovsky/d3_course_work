As two cases of Middle East respiratory syndrome, or MERS, emerged in the United States, scientists said they remained concerned about the deadly virus — but that they were getting closer to understanding how it spreads, and how doctors might someday treat patients who are infected with it.

“I’m optimistic that we’ll get this under control,” said Columbia University epidemiologist W. Ian Lipkin. “I think we’ll be able to stop this outbreak.”

Characterized by fever, respiratory problems and kidney disease, MERS was first detected in a Saudi patient in 2012. In the two years since, 145 people have died from the ailment — nearly 30% of 538 confirmed to have had it, according to the U.S. Centers for Disease Control and Prevention.

In early May, the CDC reported the first case of MERS in the U.S. On Monday, a second case emerged. But just as MERS is arriving in the West, said Lipkin and Johns Hopkins University epidemiologist Trish Perl, scientists are beginning to understand more about its origins.

Advertisement

Some of what they are studying:

How MERS spreads. Scientists know that MERS can spread in respiratory secretions, said Perl, who was part of a team that defined the incubation period for the illness (around five days from the time a patient is exposed to when he or she develops symptoms). But now, she added, researchers are exploring what types of secretions spread MERS most readily.

Evidence is emerging that the virus can survive on surfaces, which means MERS might spread by touch. Researchers are also noticing that many people have been infected with the disease who haven’t fallen seriously ill.

“We’re recognizing there’s a wider spectrum of illness,” Perl said – though she added that once patients develop severe disease, mortality still remains “in the 60% range.”

Advertisement

The camel connection. Lipkin, who has worked to understand what animals harbor MERS, said that many cases of the illness might be traced back to camels -- a “major reservoir” of infection on the Arabian Peninsula. He estimates that a majority of camels in Saudi Arabia either are, or have been, infected with MERS.

The upside: if people can be convinced to change the way they interact with camels, fewer will get sick. But the widespread prevalence of MERS in camels is also worrisome, Lipkin said, because it could give the virus more opportunity to mutate and become more contagious or more deadly.

“We don’t have evidence that the virus is evolving, but we know there’s a risk, and the more people and animals are infected, the greater the risk becomes,” he said.

Hope for a cure? Currently, physicians treat MERS by controlling its symptoms, but researchers are beginning to hit on ways to combat the virus directly, Perl said.

Advertisement

Doctors may soon be able to dose people with antibodies to help their own immune systems fight MERS, she said; researchers are also working on a vaccine for camels that might be able to prevent the spread of MERS in people.

“It’s not a human treatment but it’s a prevention strategy,” she said. “It’s kind of like rabies, where we vaccinate the animals and not the humans.”

Scientists are also finding that common drugs already in use may have antiviral potential, Perl added.

For more information about MERS in the U.S., visit this CDC MERS website.