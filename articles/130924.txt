'Time is Illmatic’ - directed by multimedia artist and first-time director One9 – kicked off the Tribeca Film Festival yesterday, Wednesday, April 16. The film documents Nas’ time living in the projects to his ascendance as an internationally renown rapper with his critically acclaimed debut album.



Nas at the New York Premiere of 'Black Nativity'

The nature of the subject is about surviving and thriving,” said Tribeca Film Festival co-founder Jane Rosenthal. “That’s what New York did post-9/11,” when Rosenthal, Robert De Niro and Craig Hatkoff held the first Tribeca fest. “And that’s what Nas has done in his career. It’s about bridging cultures and bridging communities — that’s what his work is about.”

Nas only decided to join once the movie was in the final stages of completion, finding it difficult to open up to the cameras. “It was hard,” the 40-year-old rapper said “But I’m happy I did. I’m forever grateful,” he added.

More: Nas Pays Tribute to Robert De Niro at Tribeca Film Festival

“If you look at conditions then and now, not much has changed for the people Nas represented. But so much has changed in the world,” said the writer/producer of Time is Illimatic, Erik Parker. “What was important for us to tell is that Nas is a genius, but he’s one of just a few that made it out.”

The Tribeca Film Festival, founded in the aftermath of 9/11 by Robert Di Niro, has been criticised by many industry professionals and critics alike for premiering the dregs of festival season; what the other, more prestigious slates didn’t want, Tribeca would get. But this year – the festival’s 13th year - heralds a change. Tribeca is quietly gaining in stature and respect for the much improved quality of its programming, which is partly due to the appointment of Frédéric Boyer, former head of Cannes's Directors' Fortnight, as artistic director, in 2012.

Watch the video for "It Ain't Hard to Tell" here

