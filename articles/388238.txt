A decision on Apple's appeal, now in the Second Circuit in New York, might not be issued for another year, according to Wednesday's filing. Reuters

Apple will refund up to 400 million USD to consumers ensnared in a plot to raise the prices of digital books unless the company gets a court to overturn a decision affirming its pivotal role in the collusion.

The settlement bill emerged in a court filing today made a month after attorneys suing Apple notified US District Judge Denise Cote in New York that an agreement had been reached to avoid a trial over the issue.

Lawsuits filed on behalf of digital book buyers had originally been seeking damages of up to 840 million USD after Cote ruled in a separate trial last year that Apple Inc had violated US antitrust law by orchestrating a price-fixing scheme with five major publishers of electronic books.

Cote's decision sided with the US Justice Department's contention that Apple's late CEO, Steve Jobs, had schemed with major e-book publishers to charge higher prices in response to steep discounts offered by Amazon.com Inc. Jobs, who died in October 2011, negotiated the deals as Apple was preparing to release the first iPad in 2010.

Apple is appealing Cote's decision from last year. The Cupertino, California, company won't have to pay the 400 million USD settlement if it prevails. If the appeals court voids Cote's verdict and returns the case to her for further review, Apple would still have to refund 50 million USD to consumers. No money will be owed if the appeals court concludes that Apple didn't break any antitrust laws.

"Apple did not conspire to fix e-book pricing, and we will continue to fight those allegations on appeal. We did nothing wrong and we believe a fair assessment of the facts will show it," the company said in a statement.

A decision on Apple's appeal, now in the Second Circuit in New York, might not be issued for another year, according to Wednesday's filing. Consumer attorneys in the case are still hoping to get Cote's preliminary approval of the settlement.

If its appeal is rejected, it would be more of a blow to Apple's image than its finances. The company can easily afford to refund the money, given it has about 150 billion USD in cash.