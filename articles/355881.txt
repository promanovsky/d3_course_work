Recent research suggesting testosterone treatments increase the risk of heart attack and stroke have caused concern in the U.S., but a new study that looked at 25,000 older men shows otherwise.

Testosterone prescriptions for older men have increased three-fold over the past 10 years, University of Texas Medical Branch at Galveston reported. This new study suggests the treatments do not increase these patients' risk of suffering a heart attack.

The study, published in the July 2 issue of the journal Annals of Pharmacotherapy, looked at 25,420 Medicare beneficiaries 66 years or older who had been treated with testosterone for at least eight years. The results suggest testosterone treatments are not linked to heart attack risk.

"Our investigation was motivated by a growing concern, in the U.S. and internationally, that testosterone therapy increases men's risk for cardiovascular disease, specifically heart attack and stroke," Jacques Baillargeon, UTMB associate professor of epidemiology in the Department of Preventive Medicine and Community Health and lead author of the study. "This concern has increased in the last few years based on the results of a clinical trial and two observational studies,"

"It is important to note, however, that there is a large body of evidence that is consistent with our finding of no increased risk of heart attack associated with testosterone use," he said.

Testosterone treatments are usually used to increase muscle tone and sex drive in men. In recent years the market for these drugs has grown to $1.6 billion annually. "Doctors, researchers and government agencies" have all agreed that more research is necessary on the issue of whether or not tetasterone treatments increase heart attack risk in this growing population of patients.

"This is a rigorous analysis of a large number of patients," Baillargeon said. "Our findings did not show an increased risk of heart attack associated with testosterone use in older men, however, large-scale, randomized clinical trials will provide more definitive evidence regarding these risks in the coming years."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.