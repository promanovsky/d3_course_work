Apple

Apple's OS X betas aren't just for developers anymore.

Apple threw open the doors to its OS X Beta Seed Program on Tuesday, allowing anybody who registers at the Apple Seed site to download and install the latest OS X beta for 10.9.3 Mavericks. Users will be able to download the pre-release update today.

People who sign up for the beta must accept a confidentiality agreement then install a Beta Access Utility app on their Macs. It will then let you view and download pre-release versions of OS X in the Mac App Store Updates panel. The unusual move follows last fall's free update to OS X 10.9.3 Mavericks, the first time that Apple did not charge for a major OS X update.

While this is the first time since the final release of OS X 10.0 in 2001 that Apple has issued a public beta for OS X, the company did call the pre-release version of OS X from September 2000 the OS X Public Beta. However, it retailed for $29.95, a rare although not unheard-of practice for publicly-available betas of commercial software.

Apple declined to specify when the update will go live to all users.

Update at 3:43 p.m. PT with additional background.