Shares of Apple ( AAPL ) are roaring higher this morning following another solid earnings report from the Cupertino, California-based computer giant. AAPL saw earnings of $11.62 per share (before non-recurring items) on $45.6 billion in revenues, compared to the Zacks Consensus estimates of $10.22 per share on the earnings front, and $43.4 billion in revenues.

In terms of the key divisions, investors saw great numbers for iPhone sales, while iPads were a bit of a disappointment, as this division saw a 16% decline in unit sales.

Arguably the real focus of the earnings report though was Apple's triple announcement of higher dividends, an increase in share buybacks, and a massive stock split. AAPL revealed that it was boosting its quarterly dividend by 8%, adding another $30 billion to its buyback program, and then looking to undergo a huge 7:1 stock split on June 2 nd .

The dividend hike and the buyback increase come on the heels of some recent increases on these fronts, allowing AAPL to both become a legitimate consideration for income investors, and a company that is slowly reducing its share count as well.

However, the split, which will get AAPL back below $100/share, looks to be welcomed to the average retail investor, and it may rekindle discussion of getting AAPL back into the Dow Jones Industrial Average at these prices too.

The big split also marks the first one for Apple since 2005 and it is a bit of a reversal for the leadership at the giant company. That is because as recently as 2012 Tim Cook said that splits 'do nothing' for shareholders in most cases, so such a massive split is a bit puzzling to say the least (though it should be noted that splits don't really do anything for shareholders, but it is clear that it does make it a bit more tolerable for the average retail investor to buy and sell).

So while investors remain patiently waiting for the iPhone 6, or any hints about the long-awaited TV or iWatch, this will have to hold investors over until more is released about future product plans from Apple, and how they intend to get into new markets, something that has been rumored about for what seems like ages at this point.

But is this enough to keep investors happy in the near term?

Clearly, at least in the very short run, investors seem to be believers in AAPL as the company is trading higher by nearly 8% in morning trading. The company is seeing a bit of a mixed bag on the earnings estimate front though, as the stock currently has a Zacks Rank #3, while many estimates for the current year have been trending lower lately.

It will be interesting to see if new estimates move higher on hopes of more products coming down the pike, or if the new capital plans have any impact on analyst expectations. I for one think that the fervor will die down after the split takes place and that AAPL will have to show a few growth initiatives to get some stock price momentum once again.

Tim Cook, CEO of Apple, has been building up the hype regarding some new product launches, and he will probably have to deliver very soon once the glow from the triple play of the buyback, dividend increase, and stock split fades.

After all, in a recent Wall Street Journal interview , Cook said that Apple 'is closer than it's ever been' to releasing new products, so market expectations will be very high before too long once again.

But what do you think?

Are the retail-investor friendly moves as of late enough to make you a buyer of Apple stock ? Or are you still holding out for a fresh product category that can move the needle for AAPL once more?

Let us know in the comments section below!

Want more insights from Zacks? See our latest free report 5 Stocks to Double . Click here to receive this free report now >>>

APPLE INC (AAPL): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.