Facebook is fully operational after a 30-minute outage which affected the social network's web and mobile site as well as connected apps.

Users who tried to access the site early Thursday were greeted with a message saying "Sorry, something went wrong. We're working on getting this fixed as soon as we can."

See also: Facebook Briefly Goes Down for Some Users

"Earlier this morning, we experienced an issue that prevented people from posting to Facebook for a brief period of time. We resolved the issue quickly, and we are now back to 100 per cent. We're sorry for any inconvenience this may have caused," a Facebook spokesperson told CNET.

Facebook did not disclose a specific reason for the outage.

The outage caused a short but intense storm of tweets, either complaining about the site's unavailability or making fun of it.

with Facebook down I almost went back to sharpening a stick into a spear. #facebookdown — FacebookDownSurviror (@FbDownSurvivor) June 19, 2014

Either Facebook is down or Mark Zuckerberg just set everything to PRIVATE. — 9GAG (@9GAG) June 19, 2014

Help needed: Is there another website to find out which Game of Thrones character people from my primary school are? #facebookdown — James Martin (@Pundamentalism) June 19, 2014

Facebook's last notable outage was in February 2014, when the site briefly went down due to a technical issue.