Flickr, Yahoo's popular photo-sharing service has released an update (version 3.0) to its Android and iOS apps that brings a completely redesigned UI.

Flickr claims that the new UI is designed for quick sharing and browsing. Other features in Flickr 3.0 include Auto Sync feature, which allows users to upload original quality images from the smartphone directly to the Flickr account; batch-editing, which allows users to organise Flickr images and videos into albums or edit privacy settings, along with delete option; improved sharing features to other social platforms such as Facebook, Tumblr, and Twitter; new search feature which shows search results based on groups, people, time, date and location.

Other major changes in Flickr 3.0 include People tags - much like Facebook and Twitter, Flickr users can now tag friends in image titles and comments by using @ sign; new hashtag feature to trigger search results and new privacy indicator, which now shows an image marked as public or private by the user.

Flickr has also finally renamed Sets (a collection of images) to the more widely used Album.

In addition, Flickr to challenge the Instagram and other photo-editing apps has also introduced new editing tools such as levels, crop, colour balance, contrast, saturation and live filters (total of 14 live filters for iOS) for HD video editing to its iOS app.

The Flickr 3.0 for Android and iOS are now available on App Store and Google Play.

Notably, Yahoo announced 1TB of free cloud storage for all users in May last year.

Flickr's vice president, Bernardo Hernandez said in a blog post, "Whether you're new to Flickr or one of our biggest fans, we want you to know the new apps are the beginning of a great deal more to come! They represent our commitment to building new products for your changing needs and they offer a beautiful canvas for upcoming features."

Earlier, Instagram announced it crossed 200 million users and also revealed that it has started testing integration with Facebook Places for location tagging (alongside Foursquare data which it already uses).