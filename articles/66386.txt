Thursday's session closes with the NASDAQ Composite Index at 4,151.23. The total shares traded for the NASDAQ was over 2.23 billion. Declining stocks led advancers by 1.54 to 1 ratio. There were 1042 advancers and 1608 decliners for the day. On the NASDAQ Stock Exchange 10 stocks reached a 52 week high and 33 those reaching lows totaled. The most active, advancers, decliners, unusual volume and most active by dollar volume can be monitored intraday on the Most Active Stocks page.

The NASDAQ 100 index closed down -.55% for the day; a total of -19.76 points. The current value is 3,563.13.

The Dow Jones index closed down -.03% for the day; a total of -4.76 points. The current value is 16,264.23.

NASDAQ Market Wrap

As of 3/27/2014 4:44:00 PM

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.