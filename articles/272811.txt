Australian researchers assisting in the search for Malaysia Airlines Flight MH370, which mysteriously disappeared on March 8 with 239 people on board, released data on Wednesday about a noise recorded by two undersea receivers in the Indian Ocean, and stated that there is a very small chance it could be linked to the missing jet.

The low-frequency sound was reportedly picked up off the southern tip of India in the central Indian Ocean and about 3,000 miles northwest of a region in the southern Indian Ocean where searchers have been looking for the jetliner in the past few weeks. The sound, which is outside the normal range of hearing, was recorded on March 8 shortly after Flight MH370 went missing after take-off from Kuala Lumpur. The hunt for the Beijing-bound Boeing 777 has led to very little in the way of concrete findings so far.

"It's one of these situations where you find yourself willing it all to fit together but it really doesn't," Alec Duncan, a senior marine science research fellow at Curtin University near Perth in Western Australia, reportedly said. "I'd love to be able to sit here and say, 'Yeah, we've found this thing and it's from the plane' — but the reality is, there's a lot of things that make noise in the ocean."

Duncan, who is heading the research to find the source of the sound, reportedly said that the sound most likely came from a natural event such as a small earthquake, and added that there is a 90 percent chance the sound was not from the missing aircraft. Duncan also reportedly ruled out a connection between the underwater noise and signals from the black box of the missing Flight MH370.

“It’s way below the lowest frequency that we can hear. It would be associated either with the noise relating to the impact of the plane on the water surface or a part of the wreckage that was effectively sealed but not strong enough to withstand pressure as the aircraft sank. If something had imploded suddenly, that produces a loud underwater noise," he reportedly said, according to The Australian.

Meanwhile, the Australian government on Wednesday released tender documents outlining the first requirements for future search efforts, and stated that private contractors will have 300 days to complete the undersea search, while global experts have been given 26 days to finalize a plan to search the area.

Although details about costs were reportedly not mentioned in the tender, Australian authorities had previously said that about $55.5 million will be set aside for the next phase of the search.

The Malaysia Airlines plane's disappearance has triggered a massive, international search effort involving dozens of countries and agencies, and one that is expected to become the most expensive search of its kind in aviation history.