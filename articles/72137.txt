Microsoft's new head of Xbox Phil Spencer has declared that he is "honoured" to take up the post.

The executive said that he is "incredibly proud" of the team behind the company's gaming division, and is relishing the opportunity to oversee their future projects.

Microsoft



"This past year has been a growth experience both for me and for the entire Xbox team. We've taken feedback, made our products better and renewed our focus on what is most important, our customer," Spencer said in a statement.

"Our mission is to build a world-class team, work hard to meet the high expectations of a passionate fan base, create the best games and entertainment and drive technical innovation. As we continue forward, this renewed focus and mission will be a foundational part of how I lead the Xbox programme.

"You will hear much more as we head into E3, but we are at the beginning of an incredible new chapter for Xbox and I can't wait for the days and years ahead. This is going to be fun."

Microsoft



Reporting to operating systems chief Terry Myerson, Spencer will be responsible for the Xbox, Xbox Live, Xbox Music and Xbox Video brands, as well as Microsoft Studios.

Watch Digital Spy's Xbox One hardware review below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io