Sleeping Beauty has nothing on the gorgeous makeup in the new Maleficent collection from MAC Cosmetics.

The Disney film starring Angelina Jolie as Maleficent, the villain from Sleeping Beauty, premieres on May 30, but the limited edition collaboration is available online May 8 and in stores May 15.

The 14-piece collection includes all of the products needed for an evil sorceress-inspired look—even Jolie’s incredible contoured cheekbones.

For a Maleficent-worthy red lip, try the vibrant True Love’s Kiss lipstick ($17.50). Play up eyes with the Maleficent palette ($44), which includes gold, taupe, deep brown, and black shadows.

To achieve the look of those angled cheekbones, use the Sculpting Powder in Soft Taupe Matte ($23).

The line also includes a lip gloss, lip liner, eye liner, brow pencil, highlighter pen, nail polish, and lashes.