<> on April 16, 2014 in New York City. The new 2015 Mustang convertible is viewed on top of the observation deck at the Empire State Building in honor of 50 years of the Ford Mustang on April 16, 2014 in New York City. In 1965 a prototype Ford Mustang convertible made its way to the top of the observation deck. This time the iconic American car had to be brought up in parts and assembled on location. The Mustang will be on display for two days in celebration of the kick-off of the 2014 New York International Auto Show. (Credit: Spencer Platt/Getty Images)

DETROIT (WWJ/AP) – Help wish the Ford Mustang a very happy 50th birthday!

April 17 marks 50 years since the Mustang officially went on sale, and Ford is hoping to commemorate the accomplishment with a new world record. People across the globe are being urged to sign an electronic birthday card for the Mustang. To customize and add your birthday message to the virtual card, click here.

Ford says 50,000 signatures and messages are needed to set a new Guinness World Record title.

Ford is also celebrating the 50th anniversary of the Mustang with a limited-edition model and a display atop the Empire State Building.

At the New York International Auto Show on Wednesday, the company revealed the 50 Year Limited Edition. The company will only build 1,964 special cars, honoring the year when the Mustang first went on sale.

“If you don’t like this car, you don’t like cars,” said Executive Chairman Bill Ford, whose first car was an electric green 1975 Mustang.

Earlier in the day, Bill Ford appeared with a bright yellow 2015 Mustang convertible on the 86th floor observation deck of the Empire State Building. It’s the first time a car has been there since 1965, when Ford put a Mustang convertible there.

The car had to be broken into five pieces for the ride up the building’s elevators and reassembled late at night, when the deck is closed to visitors. It will be on display until Friday.

The 50 Year Limited Edition models will come in one of the two colors of Ford’s logo: white or blue. Buyers can choose a manual or automatic transmission.

There are special chrome highlights around the grille, windows and tail lights. The Limited Edition will also be the only 2015 Mustang GT with a faux gas cap badge on the rear, where the original cap sat.

Limited Edition cars will be among the first built when 2015 Mustang production begins later this year. Bill Ford said the company hasn’t yet decided how to allocate them, or what the price will be. In 1964, the Mustang’s starting price was $2,300.

Ford chose to mark the anniversary in New York because the Mustang was first shown here at the 1964 World’s Fair.

TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.