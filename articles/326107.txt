The World Health Organisation says drastic action is needed to fight the largest ever outbreak of the deadly Ebola virus in Guinea, Liberia and Sierra Leone.

The WHO said there had been a significant increase in the number of deaths and new reported cases in the past three weeks.

That is despite a number of experts from other countries being sent to the region to try to help.

Tulip Mazumdar reports.