Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.

Trade-Ideas LLC identified

Gilead

(

GILD

) as a pre-market leader candidate. In addition to specific proprietary factors, Trade-Ideas identified Gilead as such a stock due to the following factors:

GILD has an average dollar-volume (as measured by average daily share volume multiplied by share price) of $1.7 billion.

GILD traded 10,980 shares today in the pre-market hours as of 8:00 AM.

GILD is up 2.2% today from Thursday's close.

EXCLUSIVE OFFER: Get the inside scoop on opportunities in GILD with the Ticky from Trade-Ideas. See the FREE profile for GILD NOW at Trade-Ideas

More details on GILD:

Gilead Sciences, Inc., a biopharmaceutical company, discovers, develops, and commercializes medicines for the treatment of life threatening diseases in North America, South America, Europe, and the Asia-Pacific. GILD has a PE ratio of 43.0. Currently there are 18 analysts that rate Gilead a buy, no analysts rate it a sell, and 1 rates it a hold.

The average volume for Gilead has been 15.7 million shares per day over the past 30 days. Gilead has a market cap of $107.6 billion and is part of the health care sector and drugs industry. The stock has a beta of 0.93 and a short float of 6% with 3.93 days to cover. Shares are down 6.8% year-to-date as of the close of trading on Thursday.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreetRatings.com

Analysis:

TheStreet Quant Ratings

rates Gilead as a

buy

. The company's strengths can be seen in multiple areas, such as its robust revenue growth, solid stock price performance, reasonable valuation levels, expanding profit margins and increase in net income. We feel these strengths outweigh the fact that the company has had generally high debt management risk by most measures that we evaluated.

Highlights from the ratings report include:

GILD's revenue growth has slightly outpaced the industry average of 15.4%. Since the same quarter one year prior, revenues rose by 20.5%. This growth in revenue does not appear to have trickled down to the company's bottom line, displaying stagnant earnings per share.

Compared to its closing price of one year ago, GILD's share price has jumped by 33.20%, exceeding the performance of the broader market during that same time frame. Regarding the stock's future course, although almost any stock can fall in a broad market decline, GILD should continue to move higher despite the fact that it has already enjoyed a very nice gain in the past year.

The gross profit margin for GILEAD SCIENCES INC is currently very high, coming in at 75.91%. It has increased from the same quarter the previous year. Along with this, the net profit margin of 25.36% is above that of the industry average.

GILEAD SCIENCES INC reported flat earnings per share in the most recent quarter. This company has reported somewhat volatile earnings recently. But, we feel it is poised for EPS growth in the coming year. During the past fiscal year, GILEAD SCIENCES INC increased its bottom line by earning $1.83 versus $1.64 in the prior year. This year, the market expects an improvement in earnings ($3.84 versus $1.83).

You can view the full Gilead Ratings Report.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

null