Movie

Desperate for money, Murray's St. Vincent takes on a job to babysit a 12-year-old boy whose single mother has to work.

Jul 2, 2014

AceShowbiz - Bill Murray stars in a new comedy "St. Vincent" which first trailer has just been released. Murray is St. Vincent de Van Nuys who is strapped in cash and sees an opportunity to earn money when a new neighbor moves in.

The trailer begins with a bank teller telling Murray that he no longer has money in his bank account. Then Melissa McCarthy introduces herself as the new neighbor who has a 12-year-old boy, played by Jaeden Lieberher. Murray asks for 11 dollars an hour to babysit the boy and later introduces him to gambling, strippers and drinking.

Chris O'Dowd also appears in the film as a Catholic school teacher and priest while Naomi Watts stars as a Russian lady of the night. "He don't like people, people don't like him, except you, why you like him?" Watts asks Lieberher of what he sees in Murray.

"St. Vincent" is directed by Ted Melfi. It will be released October 24 across the States.