It has been nearly two years since Google Glass was first unveiled. Its definitely an intriguing device. So far Google has kept a tight hold on who can and can not buy a Glass unit. The Explorer program only gives access to selected individuals in the U.S. Previously anyone who signed up had to purchase the unit right away. Google now wants to give you the opportunity to see how Glass looks like through its home try-on program. This will help potential Explorers make up their minds about the wearable device.

Advertising

The company is sending out emails to a lot of people who signed up, telling them that they have been selected for the free home try-on pilot program. Google will send them a kit which will include all frame styles and four colors free. They will then be able to see which frame and color combination suits them best. Once they’ve made up their minds the kit can be sent back using prepaid shipping labels from Google.

The company sends actual Glass units, not mockups, though they are non-functioning and disabled. Even the USB port has been destroyed to keep units from being charged. So if someone is able to charge it, since the units are disabled, they won’t boot into the OS.

This is certainly a good move as potential Explorers will be able to make their minds if they want to purchase a unit or not. The luxury will be much appreciated given the fact that Explorer edition Google Glass units cost a whopping $1,500. Google is expected to release Glass publicly this year for much less.

Filed in . Read more about Google, Google Glass and Wearable Tech.