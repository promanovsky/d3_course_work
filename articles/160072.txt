Rumors and reports of a Microsoft Surface Mini have stirred for almost a year now, and the latest suggests that the wait may finally be over.

A tipster has pointed ZDNet's Mary Jo Foley toward an Amazon product listing by wholesaler Vostrostone (VTSN) for a Microsoft Surface Mini case dated for a May 18 release.

While Foley believes that the tablet in the image might be a placeholder - and we're inclined to agree - the retailer indicates that this case is designed solely for the Surface Mini. (It's even promised to protect the tablet's keyboard cover, too).

Judging from the images on Amazon, the case looks quite similar to the iPad Mini Smart Cover copycats, with several possible use modes through a bending design. It's also listed to come in four basic colors: black, white, red and blue.

Churning the miniature rumor mill

Rumors, and even half-confirmations, of a Microsoft Surface Mini are nothing new. The latest from Neowin suggests a 7 to 8-inch slate billed as the ultimate note-taking device. It's said that the tablet will come packed with a stylus - and hopefully somewhere to put it, for all our sakes - and a Wacom-like digitizer to best support it.

This is what VTSN thinks the Surface Mini might look like

Microsoft Surface lead Panos Panay has even plainly said that more sizes and aspect ratios are in store for the Surface line, even potentially larger devices. That would make use in portrait mode far easier and bring it in line with the Redmond, Wash. company's purported angle for the Surface Mini as the premiere note-taking tablet.

Another fine reason to release a smaller flagship Windows tablet would be to drive the asking price down. Many have lobbed the price tag at Microsoft as prohibitive, especially in the case of the Surface Pro 2.

Not to mention that tablets seem to be trending downward in size and price on the whole, oddly enough. Who knows, perhaps the Surface Mini is the panacea for peddling Microsoft pads.