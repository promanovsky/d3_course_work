'I waited six weeks!' Seventeen cover girl Bella Thorne reveals she made boyfriend Tristan Klier work to win first kiss

She's one of Hollywood's most in-demand young stars.

And Bella Thorne is keen to act as a role model for her young fans.

Appearing on the cover of Seventeen Magazine's June/July issue, the 16-year-old spoke about her relationship with boyfriend Tristan Klier and how she made him wait for six weeks before kissing her.

Scroll down for video



Cover girl: Bella Thorne, 16, sports beach chic on the cover of Seventeen Magazine's June/July issue

'Tristan was not a relationship guy before me,' she revealed. 'On our first date, he thought I was going to be just like the other girls he had met who would always give it up and kiss him right away…I waited six weeks – I had to make sure he was in it to win it.'

Posing on a sandy beach in a colourful bikini and palm-print sweater, Bella also addressed the body image issues faced by other young girls.

'Girls are shamed for their bodies and made to believe that they're not supposed to show them or love them,' the pretty redhead said.

'They're made to believe that they aren't perfect, that they need to ‘work on it'... and I think it's freaking ridiculous,' the Disney star vented. 'It gets on my nerves because boys can do whatever; it's like we're living in the 1800s again. It drives me crazy.'



Healthy: Bella posed on the beach in yellow shorts and a tropical-patterned crop top



Young love: In April, Bella shared a sweet snap of herself with her boyfriend of two-years Tristan Klier

They could be twins! Bella shared a snap of herself and her mother Tamara on Mother's Day

She also offered advice to young women who were considering losing their virginity.

'It needs to be the most special, beautiful thing that ever happened,' she explained. 'It should be a decision you feel right about. If you don't feel 100% about it, then it shouldn't happen.'

Bella is known for playing the role of CeCe Jones on Disney's Shake It Up, and she's soon to appear alongside Drew Barrymore and Adam Sandler in summer comedy Blended.

Gushing about her famous co-star, Bella said: 'Drew is so positive and beautiful on the inside and out. On the set of Blended, she always had a smile and looked at the positive side of things. I learned from that, and I think other girls should too.'

Fashionista in the making: Bella looked pretty in a purple and black dress at a KIIS FM Wango Tango event in Los Angeles on Saturday

Listening to the youngster speak of her family and work ethic, it's no wonder that her career is going from strength to strength.

Bella admits to getting her ambition from her father, who died when she was nine-years-old.

'There was no stopping him; it didn’t matter what anybody said,' she recalled. 'For me, I feel like it’s a cool way to bring him into my life because he did great things in his life, and I want to emulate that.'



And she certainly sounds like a force to be reckoned with.

'[If my agent says] No, you don't understand. They're offering [the part] to someone else,' I don't care. Get me back in. I want to see the director again. For me, it's not a matter of 'Well, it's gone.' No, there's always something you can do to change someone's mind.

Bella's fans will look forward to seeing her in Blended, which hits theaters in the US and UK on May 23.

Rising star: Bella is about to appear alongside Drew Barrymore in summer flick Blended, which hits theaters in the US May 23























