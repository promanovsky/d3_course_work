NEW YORK (TheStreet) -- IBM (IBM) - Get Report has cranked up its big data efforts, unveiling new servers built around its Power Systems technology, opening up the specifications for its latest POWER8 processor and teaming with Google (GOOG) - Get Report to build open servers.

Big Blue is part of the OpenPOWER Foundation, which was set up last year with the goal of developing open servers and components for Linux-based cloud data centers. In addition to IBM, other members include Google, Nvidia (NVDA) - Get Report and Mellanox (MLNX) - Get Report. On Wednesday, the foundation released a road map outlining contributions from a numbers of its members and presented its first "white box server" details. This includes a reference design from server component maker Tyan and firmware and operating system developed by IBM, Google and open source software specialist Canonical.

These are busy times for IBM. On Wednesday, the hardware and software behemoth unveiled five Power Systems S-Class servers that it is touting for "large, scale-out computing environments" and three Power Systems technologies for big data.

Big data refers to the management of vast quantities of unstructured data, or information that is outside the realm of traditional databases. Examples include email messages, PowerPoint presentations, audio, video and social media information.

Just over a year ago, the tech giant increased its 2015 revenue target for analytics and big data to $20 billion from $16 billion.

"Big Data is significantly changing the needs of data centers, networks and clouds, pushing our current infrastructure to its limits and creating the need to spur new innovations," said Brad McCredie, vice president of IBM and president of the OpenPOWER Foundation, in a statement emailed to TheStreet. "The new road map revealed by the OpenPOWER Foundation shows a major shift in how new business computing technology can be developed. Collaboration will be the core component of this open technology and the resulting innovation that comes over the next decade."

Analysts believe that IBM's decision to throw its weight behind open server technology is a shrewd move in the face of competition from chip specialists Intel (INTC) - Get Report and ARM (ARMH) . "It should make Power more relevant and provide a strong alternative to ARM processors that are attempting to grow to support servers," wrote Rob Enderle, principal analyst at Enderle Group, in an email to TheStreet.

"I believe it could shake up the markets for scale-out and cloud servers pretty significantly," added Charles King, principal analyst at Pund-IT, in an email. "Right now, Intel is obviously the undisputed leader there but there seems to be an appetite for new alternatives, such as the new chips and system designs based on the ARM architecture."

King, however, describes those solutions as "still in a nascent state," noting that 64-bit versions of the chips will arrive sometime in 2014 but it'll likely be a year or more before ARM-based servers become available in commercial volumes.

IBM also has a pressing desire to boost its Power Systems business. During the tech giant's recent fiscal first quarter, the company's Power Systems revenue fell 22% from the prior year's quarter. The Armonk, N.Y.-based firm saw its overall Systems and Technology revenue slide 23% year over year to $2.4 billion in the first quarter.

"If their sales hadn't dropped like this I doubt they would have even considered this approach," wrote Enderle. "Often high pressure forces bold moves."

In her recent annual letter to shareholders, IBM CEO Ginni Rometty vowed to maintain the company's significant presence in hardware, just six weeks after clinching a $2.3 billion deal to sell the firm's low-end server business to Lenovo.



Pund-IT's King believes that IBM's strategy around Power is sound, reflecting the realities of the marketplace. "Last quarter was dreadful partly due to a falloff in sales that's common prior to a next generation processor launch," he wrote. "But the larger story is that the market for traditional Unix/RISC systems has been faltering for the past couple of years (with many customers migrating to x86) and I don't know of anyone who expects it to return to full health."

IBM, he adds, is the leading vendor in that sector with a share near 50%, and with its new POWER8 solutions the company should continue picking up HP (HPQ) - Get Report and Oracle (ORCL) - Get Report customers that abandon those companies' platforms.

"But in order to create sustainable growth for Power, IBM needs to seek new markets and opportunities," wrote King. "Pursuing that effort alone would be frightfully difficult and costly but opening its microprocessor architecture and collaborating with OpenPOWER partners should provide IBM new ways to achieve its goals."

IBM shares dipped 0.75% to $190.71 during Thursday trading.

-- Written by James Rogers in New York.

Follow @jamesjrogers

>Contact by Email.