Armistice has been declared in a $1 billion lawsuit, with Google and Viacom announcing Tuesday that they have resolved a copyright infringement legal battle that stretched seven years.

The two sides were scheduled to appear in appellate court this week. Their dispute dates back to Google’s $1.65 billion acquisition of YouTube. Shortly after a deal closed, Viacom accused the online video platform in 2007 of violating its copyright by hosting clips from its movies and television programming such as “SpongeBob SquarePants” and “The Daily Show” without its permission. It sued the company for $1 billion.

Also read: Viacom Wins Reversal in YouTube Copyright Case

Terms of the settlement were not disclosed, but the resolution did not involve the exchange of any money, according to an individual with knowledge of the situation. The primary focus was on working together more collaboratively moving forward, the individual said.

It puts a period on a lengthy, presumably costly legal battle that was beginning to look increasingly anachronistic and out of step with a digital landscape where media companies are increasingly reliant on YouTube as a launching pad for trailers and show clips, both for advertising dollars and exposure.

YouTube has also increased its efforts to crack down on illegal content, introducing a filtering system in 2008 and more aggressively policing programming that is uploaded without the rights holder’s consent. Throughout the dispute, it always maintained that it pulled illegal content when notified.

The companies issued the following statement:

“Google and Viacom today jointly announced the resolution of the Viacom vs. YouTube copyright litigation. This settlement reflects the growing collaborative dialogue between our two companies on important opportunities, and we look forward to working more closely together.”

Lower court decisions have found in favor of both parties at different points during the lengthy legal battle. The case is considered a landmark in determining the extent that digital content is protected in an era in which it can be repackaged, repurposed and shared with the click of a cursor.