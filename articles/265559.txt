Celebrity

Pattinson discusses 'Twilight' in a new interview with The Hollywood Reporter and says that playing Edward Cullen in the movie 'was one of the hardest jobs I've ever done.'

May 29, 2014

AceShowbiz - Robert Pattinson is on the cover of The Hollywood Reporter's latest issue. The actor, who is popular for playing sexy vampire Edward Cullen in "Twilight" movies, wears a simple white shirt and spaces out on the magazine's front page. In an accompanying interview, the British star discusses the hype surrounding the movie and briefly mentions Kristen Stewart.

Two years ago, their relationship was on the rocks after she was seen kissing a director. They tried to rekindle their romance, but eventually decided to split. When asked if they are still in contact, he blithely replies, "Oh, yeah."

In the interview, the actor also mentions how promotional moves for the vampire saga overwhelmed him. "Everything changed when they did the marketing, and the general public started to view [the films] in a different way when they started to push the 'team' aspect of it. It was like, 'I'm on Team Edward or Team Jacob.' That saturated everything, and suddenly there was a backlash. Whereas with the first [film], there wasn't a backlash at all," he explains.

Pattinson also recalls moments of him auditioning for the movie, based on novels by Stephenie Meyer, saying that director Catherine Hardwicke called him at 2:30 in the morning and they had "this ridiculous conversation, and I hadn't read the books or the script or anything and I just bulls***ted on the phone." Later he had a screen test, but the producers apparently thought he was too old for playing a high school student. "Stephanie [his agent] was like, 'You've got to go and meet the producers and just shave 20 times before you go,' " he reveals.

"It was quite a constricting character, in a way," he says of playing Edward Cullen. "You want to make [him] as dramatic as possible, but you have someone who never loses his temper, and so it's like, 'How the f**k do you do this?' I think that was one of the hardest jobs I've ever done."