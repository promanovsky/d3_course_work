Microsoft finally launched its Office productivity suite for iPad a few weeks ago, but the free application will only let users view documents unless an Office 365 subscription is purchased. The company offers new subscribers a way to purchase Office 365 inside the iOS application for $99.99 per year, out of which Apple gets its own cut. However, Microsoft on Tuesday announced that its Office 365 Personal subscription, a cheaper alternative for buyers, is now available at a big discount.

Unveiled in mid-March, Office 365 personal costs $69.99 per year or $6.99 per month, and is available online at Office365.com, from Microsoft stores, and from other partners.

While Office 365 Personal is indeed a cheaper option for Office users looking for a subscription, users will be able to register only one PC or Mac and one tablet (iPad included). Comparatively, Microsoft’s $99.99 per year or $9.99 per month Office 365 Home plan will allow users to registers up to 5 PCs or Macs and 5 tablets in order to use the full Office applications.

Once purchased, an Office 365 subscription will also allow users to edit Microsoft documents on smartphones including iPhones and Android devices.

A “touch-first” Office application for Windows devices is also in the works, and has been demoed at Build 2014, although Microsoft is yet to launch it.