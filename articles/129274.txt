Twitter announced today that it will allow advertisers to serve up download links in the ads that appear on users' Twitter feeds.

The push, made in conjunction with the MoPub mobile advertising exchange that Twitter acquired in September, means that Twitter users might see a "download" button in ads going forward that will allow them to access an app directly from the micro-blogging service.

"This offering gives marketers unparalleled scale across the mobile ecosystem," Kelton Lynn, product manager of revenue, wrote in a blog post.

Twitter is currently testing this out in a private beta with companies like Spotify, HotelTonight, Kabam, and Deezer.

According to Lynn, MoPub reaches more than 1 billion unique devices, and handles more than 130 billion Android and iOS app ad requests each month. Now, by using just one ads.twitter.com interface, promoters can broadcast simultaneous campaigns to more than 241 million active Twitter usersand beyond.

"We have developed a full suite of targeting, creative and measurement tools to enable Twitter advertisers to effectively promote their mobile apps," Lynn said.

Facebook started allowing downloads from ads in 2012.

Further Reading

Mobile App Reviews