The chairman of Samsung Electronics Co., the world's biggest smartphone maker, is currently being treated at a hospital in Seoul after a heart attack but is in stable condition.

Late Saturday, Lee Kun-hee, 72, had trouble breathing and was sent to an emergency room at a hospital near his home. Mr. Lee showed symptoms of cardiac arrest and the hospital staff had to perform CPR, Samsung Medical Center where Mr. Lee is being treated said in a statement Sunday.

After his heart condition improved, Mr. Lee was treated and is currently in recovery, the hospital statement said. Hospital officials and the company declined to comment on how long he is expected to be hospitalized.

Mr. Lee built Samsung Electronics, the flagship of South Korea's largest conglomerate Samsung Group, into a global brand that sells everything from semiconductors to televisions and household appliances. Samsung is currently best known as the largest seller of mobile phones. While Mr. Lee is less involved in day-to-day operations at Samsung as it is currently run by three co-chief executives, he is believed to have a final say in key strategic decisions.

Mr. Lee's health issues raise concerns over whether his only son, Jay Y. Lee, is ready to take control over South Korea's largest company by market capitalization. The younger Lee, who has been groomed as a successor for many years, served as Samsung's chief operating officer until 2012 and is now vice chairman, although he doesn't hold an operational title.

The elder Lee's health complications come at a time when Samsung Electronics is facing slow growth in its core mobile unit, which saw a decline in its operating profit for the first time in more than three years in the first quarter. The company has long been searching for a fresh growth momentum beyond its key mobile and chip businesses.

This isn't the first time Mr. Lee has been hospitalized. In August last year he suffered from pneumonia. He was also treated for lung cancer in the late 1990s. Whenever Mr. Lee is ill, attention shifts to how ready Samsung Electronics is for a potential change in leadership.

The younger Mr. Lee, 45 years old, is the international face of Samsung, believed to have brokered deals with companies such as Apple Inc. Samsung has previously said Mr. Lee has built strong relationships with global business leaders and was "instrumental" in forging partnerships with customers.

The elder Mr. Lee took over Samsung in 1987 after the death of his father who founded the Samsung Group, which has businesses spanning from electronics to steel and financial services. The founding family controls Samsung through a complex web of cross-shareholdings in group companies. Lee Kun-hee and his three children have control over roughly 80 Samsung companies.

Industry experts say Samsung has been taking a number of steps recently that will eventually help the younger Mr. Lee gain control of leadership in the future. One of the latest moves includes the planned initial public offering of Samsung SDS Co., which provides IT services for Samsung Electronics. Samsung Electronics owns a 22.6% stake in the affiliate as the largest shareholder.

Analysts said the share sale could also help the younger Mr. Lee secure funds needed to take control of other Samsung affiliates as part of a transition of power from his father.

A spokeswoman for Samsung Group declined to comment on succession plans.

At the end of 2013, Jay Lee was the third-largest shareholder of Samsung SDS with an 11.3% stake. He also holds the majority stake of 25.1% in Samsung Everland Inc., the de facto holding company of the Samsung Group.

Write to Min-Jeong Lee at min-jeong.lee@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires