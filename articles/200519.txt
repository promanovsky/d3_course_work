Timothy Geithner says in a new memoir that he repeatedly offered to resign as Treasury secretary in the aftermath of the financial crisis and at one point suggested that Hillary Rodham Clinton be among those considered to replace him.

President Barack Obama rebuffed Geithner's suggestion, and he remained at Treasury until 2013.

Geithner's memoir will be published next week. On Thursday, The Associated Press bought an early copy.



Editor's Note: 18.79% Annual Returns ... for Life?

Geithner recalls his offers of resignation in "Stress Test," which explores his turbulent four years at Treasury. During his tenure, the Obama administration confronted the worst recession and most severe financial crisis since the Great Depression.

He portrays his offers to resign as reflecting his uncertainty about whether he was up to the immense challenges of the job. Ultimately, Geithner concludes that the economy stabilized under his watch.

In several sections of his memoir, Geithner writes of internal conversations about who might replace him in a post he had been initially reluctant to accept.

When his nomination ran into challenges over his tax filing problems, Geithner offered to pull his name from consideration. He writes that he later volunteered several times to step down from the Cabinet post if administration officials felt that doing so would boost Obama's standing at a perilous moment for the economy.

In proposing that the White House consider Clinton as a successor in 2010, Geithner cited her star power as secretary of state. Among the other names Geithner suggested was Jack Lew, who succeeded him last year.

By his own acknowledgement, Geithner seemed an awkward fit as Treasury secretary, neither an economist nor a banker nor a natural public speaker.

He found himself unable to quell what he calls the public's "Old Testament" outrage at bankers who received federal aid and cashed outsized bonus checks after the financial meltdown that erupted in 2008. At least one of those bankers, JPMorgan Chase CEO Jamie Dimon, offered to help the administration rewrite the regulations overseeing the big banks, Geithner writes. The administration declined Dimon's offer.

Clinton's husband, former President Bill Clinton, advised Geithner that publicly criticizing prominent bankers, such as Goldman Sachs' CEO Lloyd Blankfein, wouldn't appease ordinary Americans.

"You could take Lloyd Blankfein into a dark alley, and slit his throat, and it would satisfy them for about two days," Geithner recalled Clinton telling him. "Then the blood lust would rise again."

Geithner notes that easy money and risky behavior fueled the housing bubble and led to the financial crisis. Deceit and fraud by some financial institutions, he writes, were a component of the mania. But they weren't its primary cause.

No Treasury secretary since the Depression confronted so many financial threats at once. When Geithner became Treasury secretary in January 2009, the economy had sunk into a deep recession. Unemployment was surging, and the financial system was teetering.

Having previously led the Federal Reserve Bank of New York, Geithner had worked to craft the government's early response to the financial crisis. The crisis erupted in the fall of 2008 with the fall of investment bank Lehman Brothers and the government takeover of mortgage giants Fannie Mae and Freddie Mac

Geithner's supporters said he deserved credit for helping steady the banking system, restore investor confidence and avert a complete meltdown.

His critics countered that his policies consistently favored big banks and neglected ordinary Americans, including many struggling to save their homes after a wave of foreclosures followed the housing bust.

In his memoir, Geithner pushes back against such criticism. He writes that it would have been impossible to fix the economy without first shoring up the banking system and restoring confidence. This, he writes, was the most urgent task. A banking panic would have defeated the administration's efforts to help the economy recover.

He acknowledges that the government's efforts to help troubled homeowners keep their homes were inadequate. But he argues that no "game-changing housing plan" that would have offered Americans substantial relief existed for Geithner's team to embrace.

Geithner rejected the idea of fixing banks by nationalizing them or letting the weakest firms fail. Instead, he writes, while vacationing on a beach in Mexico, he fleshed out a plan for using "stress tests" to see how much additional capital banks would need to survive a financial catastrophe.

If banks struggled to pass these tests, they would have to raise capital privately or be forced to accept support from the government. Geithner writes that this strategic choice was key to salvaging the financial system.

In the book, Geithner addresses his biggest public relations disaster — his first speech as Treasury secretary on Feb. 10, 2009.

Obama had told reporters at his first news conference that Geithner would lay out how the administration would fix the financial system.

"He's going to be terrific," Obama said of Geithner.

The speech unnerved investors. The Dow Jones industrial average plunged nearly 400 points, an ominous start for an administration official whose main task was to restore confidence in financial markets.

"It's fair to say the speech did not go well," Geithner writes.

Yet Geithner said the plan, which involved determining how much capital the biggest banks needed, ended up working.



Editor's Note: 18.79% Annual Returns ... for Life?