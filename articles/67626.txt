Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The sister of Sir Mick Jagger’s tragic ­girlfriend, who was excluded from her funeral, has praised the Rolling Stone for sharing her ashes.

Speaking for the first time since L’Wren Scott’s cremation this week, Jan Shane said the star had made the right decision. It meant the ashes could be buried next to their parents near the family home in Ogden, Utah.

Mormon Jan said: “I want to thank, from the bottom of my heart, Mick’s kind gesture.

“He was a huge part of her life and had a big hand in ­organising the funeral so it was thoughtful to make sure a part of her returned to her roots.

"It’s pleasing to know her ashes will be buried next to Mum and Dad because L’Wren was so close to them.”

Jan, 53, was not invited to the Los Angeles funeral, planned by Sir Mick, 70, and Jan’s brother Randall Bamborough, but the feuding family have now made up.

(Image: Getty)

While some of L’Wren’s remains will be laid to rest next to her adoptive parents, Ivan and Lula Bamborough, it’s not known what Sir Mick intends to do with the rest.

Jan said: “It’s good to know I will be able to visit her grave. L’Wren and I never had any problems and it saddened me that I couldn’t pay my final respects. Now, I will be able to in Utah.”

Jan denies previously criticising Sir Mick for hijacking the funeral plans and complaining about L’Wren’s will, which left her fashion-design fortune, an estimated £5.5million, to Sir Mick.

L’Wren, 49, shocked the world by hanging herself on March 17 in the £5m New York flat Sir Mick had bought for her.