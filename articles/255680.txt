This satellite image released on March 20, 2014 by the Australian Maritime Safety Authority shows objects that may be possible debris of the missing Malaysia Airlines Flight MH370. (File/UPI/Australian Maritime Safety Authority) | License Photo

KUALA LUMPUR, Malaysia, May 27 (UPI) -- On Tuesday, the Malaysian government released a document detailing the communications between missing Malaysia Airlines Flight 370 and a satellite.

The 47-page document, which relatives of passengers on the plane requested more than two months ago, is "intended to provide a readable summary of the data communication logs" between MH370 and British company Inmarsat's satellite system.

Advertisement

It is believed that the missing plane went down somewhere in the southern Indian Ocean with 239 people on board.

Experts are already claiming the document is missing key pieces of information.

"There's not enough information to say whether they made an error," said CNN Safety Analyst David Soucie. "I think we're still going to be looking for more."

Inmarsat's vice president of satellite operations, Mark Dickinson, told CNN that he has "good confidence" in his company's calculations and interpretations of the data.

"This is all the data we have for what has happened for those six or so hours," he said. "It's important we all get it right and particularly that everyone looking at the data makes the best judgments on it and how it's used. And particularly for the families and friends of the relatives on board, try and make sure that we can help bring this sad incident to a close."