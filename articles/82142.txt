Every time I try and make sense of the many-headed hydra that is the Marvel superhero franchise I get dizzy and have to sit down for a bit, but here we go again. Captain America: The Winter Soldier is the ninth film in the series, and a sequel to the very enjoyable 2011 movie, Captain America: The First Avenger.

In that film we met Steve Rogers, a nine-stone weakling who volunteered for an experimental military drug trial during World War Two and was transformed into a Nazi-bashing hero. After a nasty plane crash in the Arctic, he was placed in a kind of cryogenic sleep and emerged, still young, in the present. As Captain America: The Winter Soldier opens, Steve (Chris Evans) is struggling to adapt to the complexities of 21st century life when S.H.I.E.LD. comes under attack. Rogers has had his doubts about his boss Nick Fury's ethics, but when Fury's beset by a remorseless armed assassin who seems to have superpowers, Captain America must come to his aid.

Steve joins forces with former Russian agent Natasha Romanoff (Scarlett Johansson) to find out who's behind the attacks, and the pair soon begin to suspect that S.H.I.E.L.D. has been compromised from within.

Iron Man 3, Thor: The Dark World and now this film have all had to cope with the added complication that their central characters also appeared in the 2012 blockbuster Avengers between sequels, and Captain America: The Winter Soldier wisely deals with this by making a couple of pertinent jokes and moving on. Two hours and 16 minutes is about par for these superhero yarns, and while Captain America 2 does ramble on a bit towards the end, it's entertaining and charming enough to just about get away with it. There are some nice gags early on about the gaps in Steve Rogers' cultural references, and the script intersperses the action with occasional insights into his character.

As you'd expect of a $170 million production, the special effects are impressive, particularly the sequence in which Nick Fury is ambushed in a supposedly bulletproof car.

Chris Evan is a perfect fit for the Captain America role – charming, square-jawed, and witty rather than giddy, he provides a solid anchor for the other characters to fly around.

Scarlett Johansson reprises her Avengers role with aplomb, and is very good as the Black Widow. But it's Robert Redford who gets most of the jokes, and he knows what to do with them too: his portrayal of suave politician Alexander Pierce gives this film a welcome touch of gravitas.

Captain America: The Winter Soldier (12A, general release, 136 minutes)

Director: Joe Russo, Anthony Russo.

Stars: Chris Evans, Scarlett Johansson, Robert Redford, Samuel L Jackson. 3 Stars

Muppets and Gervais are manic match in heaven

Muppets Most Wanted

(G, general release, 113 minutes)

Director: James Bobin.

Stars: Ricky Gervais, Ty Burrell, Tina Fey, Ray Liotta, Salma Hayek. 3 Stars

Jason Segel and Nicholas Stoller's franchise-reviving 2011 movie The Muppets was always going to be a hard act to follow. Full of catchy songs and daft routines, it caught the spirit of the original TV show brilliantly and charmed the pants off the lot of us.

Muppets Most Wanted may not be quite in that league, but it is a winning and worthy sequel that relies more on jokes than songs, and features some lovely non-muppet guest performances.

It starts precisely where the last film left off, with Kermit and co still basking in the unexpected success of their telethon and wondering what their next move should be.

An answer comes from Dominic Badguy (Ricky Gervais), a shifty agent whose name should set off alarm bells for the Muppets, but strangely doesn't. When Dominic suggests they embark on a whistlestop tour of Europe with him as their manager, only Kermit smells a rat, but is quickly overruled by his gullible colleagues. Off they set then, and during their first engagement in Berlin, Kermit is duped by Dominic and his boss, a notorious international criminal frog called Constantine who happens to be a dead ringer for Kermie. While the real Kermit's mistaken for Constantine and bundled off to a Siberian gulag, the imposter frog takes over the Muppets and unleashes a daring criminal masterplan.

Director James Bobin and his co-writer Nicholas Stoller opt for the frenzied, joke-heavy atmosphere of a Marx brothers film, an approach that pays dividends for the most part. The various Muppets do what Muppets do, and are helped along by two perfectly judged turns from Tina Fey and Modern Family star Ty Burrell.

Ms Fey plays a hatchet-faced but secretly sentimental Gulag commander called Nadya who has a soft spot for Kermit, and Burrell is hilarious as Jean-Pierre Napoleon, a Clouseau-like Interpol inspector who keeps going off on holiday at key moments in the case.

The film only flags in an overly frantic finale, but is huge fun for the most part, and the Muppets' tour even includes a brief date in Dublin.

No happy families

The Past (12A, limited release, 130 minutes)

Director: Asghar Farhadi.

Stars: Berenice Bejo, Tahar Rahim, Pauline Burlet, Ali Mosaffa. 4 Stars

Asghar Farhadi is one of the most talented directors working in cinema today. His stories tend to be little, and domestic, but their universal themes and convincing veracity lift them above barriers of culture and language. In his Oscar-winning 2011 film A Separation, Farhadi dissected the messy break-up of a marriage, but in The Past the marriage is already dead.

When a middle-aged Iranian called Ahmad (Ali Mossafa) arrives in France to finalise his divorce with his estranged wife, Marie (Bérénice Bejo), we soon discover that neither parties' wounds have healed. Ahmad soon finds himself mixed up in a dysfunctional family he thought he'd left behind. Farhadi uses grace and fluidity to tell a story in which no one is quite innocent.

Charting unsung heroes

20 feet from stardom (No Cert, IFI, 90 minutes)

Director: Morgan Neville.

Stars: Darlene Love, Merry Clayton, Mick Jagger, Bruce Springsteen, Stevie Wonder, Sting. 4 Stars

Morgan Neville's hugely enjoyable, Oscar-winning documentary celebrates the unsung heroes who helped make some of popular music's most celebrated songs and albums special. As Bruce Springsteen rather diplomatically puts it, the role of the backing singer is "a somewhat unheralded position", and 20 Feet from Stardom tries to right this wrong by highlighting the achievements of veteran performers like Darlene Love.

Love worked with everyone from Sam Cooke and the Beach Boys to Elvis Presley, but was badly treated by the industry and worked for a time as a house cleaner before her fortunes changed in the 1980s.

Neville's film is full of such anecdotes, and demonstrates the fine line between fame and obscurity.

Day & Night