Nikhil Subramaniam

Google I/O is just over a day away and a lot of things could soon be changing. As we have already seen in our previews, Google’s TV product could be in for a major overhaul, while Chrome and Chrome OS are expected to have new features too. And then there’s Android Wear, which is likely to be the headlining act at the Moscone Center West tomorrow evening.

But it’s Android that will be most closely watched, at least as far as India is concerned. Google’s new services usually take their time coming to India, but Android is here and dominating big time. So the changes to the OS will affect Indian users the most. So what can you expect at Google I/O? If indeed Google decides to show us the next-gen Android, we are in for some major changes.

The big Android redesign

Design will be a key focus at I/O this year, as Google looks to bring a consistency in US in Chrome, Android and its Web products. We have already seen the new design language called Quantum Paper in the form of the new Google+ app, which features a colour-blocked top bar and bigger buttons for quick actions. We have also seen this particular design leak for Gmail’s Web version, so Google could finally be bringing a measure of consistency across board.

As Ars Technica points out, Matias Duarte, the head of Android user experience and big driving force in Android design, has indicated that the company is rethinking its design strategy. "We need to stop thinking of 'mobile' as a distinct category," he is quoted as saying, while the report adds that Google is considering desktop, mobile, car, and wearable as one product from a design point of view.

Android app icons could also be getting a makeover which is said to be called Moonshine internally, while some other changes have also leaked, such as a native Help interface for Google’s core apps, instead of webpages.

64-bit support

Android will finally be able to support 64-bit architecture. In the past few months, post Apple’s big 64-bit announcement last year, there has been a lot of talk about Android also getting 64-bit support. It would allow devices to have over 4GB of RAM, while newer chips based on this architecture are expected to be blazing fast. Chipmakers will have to quickly align themselves with the 64-bit support; leading names such as MediaTek, Qualcomm have already started work on 64-bit multi-core chips.

ART—Android RunTime

Dalvik, the runtime that powers Android, is one of the most crucial cogs of the OS, since it allowed developers to easily port older apps for the platform, which in turn resulted in the proliferation of apps, which consequently drove hordes towards Android. But come I/O and we could be bidding adieu to Dalvik and saying hello to Android RunTime or ART. Some users would already be familiar with ART, since it’s been an experimental runtime in Android 4.4 KitKat.

Thanks to a dedicated session for ART, we know that Google is expanding its role and could be making it the default instead of Dalvik. Essentially, with ART Android goes from just-in-time (JIT) compiling to an ahead-of-time (AOT) compiler, meaning your apps won’t be compiled every time they are run, but only when they are first installed. This should increase install sizes, but most importantly boost app performance, and could also help in lower power consumption.

Better photography?

Frankly, we are tiring of hearing about Google’s upcoming Camera API. It’s been months since we first heard that it’s coming, but it’s nowhere to be seen. But I/O could finally change that if the developer session for the new Camera API is any indication. Commits to AOSP showed the API would bring about RAW image support, native burst mode and removable cameras.

Security and BYOD

Finally, we can see Google making a bigger play for security and enterprise device management. Android has been struggling in the race to become the BlackBerry replacement at the workplace, while Apple has been hitting the right notes with its OS updates.

As far as security is concerned, Google is supposedly working on a kill switch for Android, which would allow users to not just remotely wipe the device, but also lock it till it gets back in their possession. This would presumably be part of the current Android Device Manager app, but would also require changes on a low-level to enable such a lock.

Then there are SlickLogin and Bump acquisitions, and the emergence of Google Nearby, which could improve two-step verification, which requires user intervention, with a more stealthy authentication, by ‘listening’ for devices close by.

And lastly, Google’s purchase of Divide earlier this year, hints that the company is taking its lack of enterprise-friendly features seriously. Google is banking on Divide’s technology to make businesses feel more comfortable about Android and allowing their employees to use Android devices for business email and tasks involving other sensitive information. Divide’s device-management tools are available for iOS, but Google’s acquisition suggests native separation of information is coming to Android.

Android devices

There’s no clear indication that any Android phone or tablet hardware is coming to I/O, though last year too, we didn’t know Google would be announcing Play Store editions for some phones. That was finalised last year, and this year we could see Android Silver replacing the Nexus line altogether. But the fabled Nexus series could see one last hurrah in the form of the Nexus 9 or HTC ‘Volantis’, which leaked earlier this week.

So what changes to Android are you most excited about? Let us know and keep following our coverage of Google I/O, as we build up to the big keynote tomorrow evening.