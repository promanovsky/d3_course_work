Facebook's Internet.org project is taking another step toward its goal of bringing the internet to people who are not yet online, launching an app in Zambia.

The Internet.org app will give subscribers of Zambia's Airtel phone company access to a set of basic internet services for free. Users of the app won't incur the data charges that can be prohibitive for many people in developing nations. Facebook has already been working with mobile operators around the world to offer its own service free of charge to phone subscribers - think of it as a gateway to the rest of the Internet.

Internet.org project gives subscribers of Zambia phone company access to basic internet services for free. Credit:Reuters

Online services accessible through the app range from AccuWeather to Wikipedia, a job search site as well as a breadth of health information. Facebook's own app, along with its Messenger service, is also included, as is Google search, although charges apply if people click on search results.

The app works on Android phones as well as the simple "feature phones" that are used by the majority of people in Zambia, said Guy Rosen, product management director at Internet.org.