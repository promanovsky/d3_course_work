Google has rolled out a new update for developer services of its Google Play Store, which currently holds 1.5 million apps. The new update will provide better inclusion of a variety of these apps.

The legacy and popularity of the digital apps store is becoming solidified as more than half of the apps within it have garnered success. In fact, the free apps and games have boosted the popularity of the platform by leaps and bounds. The announcement was reportedly made on Google's Android Developers Blog.

"This release introduces Android wearable services APIs, Dynamic Security Provider and App Indexing, whilst also including updates to the Google Play game services, Cast, Drive, Wallet, Analytics and Mobile Ads," said the announcement.

According to reports, Google offered a sneak peak at the Google Play Services update to 5.0 at the Google I/O Developers Conference that took place in San Fransisco recently.

The update includes a new set of APIs that streamline development for wearables. The APIs reportedly provide "an automatically synchronized, persistent data store and a low-latency messaging interface that lets you sync data, exchange control messages and transfer assets."

New security options are available to provide patches for the API as well, sources explained.

One particularly noteworthy feature of the new Google Play Services is something called "Quest". Quest is an API set that enables developers of games to create a series of in-game challenges.

Financially, the Google Play Store couldn't be doing better. According to a report citing an App Annie study, the company's app store doubled its revenue YoY from June 2013 through this June. Gaming apps made up the bulk of that revenue, around 90 percent according to the findings. Of all apps revenue, "freemium" downloads provided a whopping 98 percent.

The number of apps grew by 60 percent over the same year. The Play Store had more than a million apps in June 2013, but this month the number sits at 1.5 million.

The Lyft app is reportedly going to be one of the first apps to make it to market in the newly released Android Wear compatible apps. Android Wear is an operating system for smart watches. The app allows users to hitch a ride with a registered driver that also has the app. You can tell who the drivers are by the pink mustaches attached to the grill of their vehicle.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.