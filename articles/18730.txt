With a roll of his eyes and a comment that he was good at using guns and knives, Chris Brown may have cost himself weeks of freedom and his chance to get back to making music anytime soon.

The reasons for Brown's dismissal from a Malibu rehab facility were detailed in court on Monday, with a judge ordering the Grammy winner to remain in jail until a formal probation violation hearing can be convened on April 23.

The jail stint will be Brown's longest and comes more than five years after he viciously attacked his then-girlfriend Rihanna in a rented sports car just hours before the Grammy Awards. Superior Court Judge James R. Brandlin said he was most troubled by a comment the singer made during a group therapy session last week.

"I am good at using guns and knives," the rehab reported Brown said in response to an exercise asking him to reflect on what he was good or excelled at.

Other transgressions cited by rehab workers included the singer ignoring a worker who was waiting to give him a drug test, rubbing elbows with a woman when he had signed an agreement to stay at least two feet away from all female clients and joking telling fellow patients, "I'm going to ask my higher power to take away my troubles."

When asked whether he was serious, Brown said yes while shaking his head no, a report on Brown's conduct stated.

'A bad day' in rehab

Outside court, Brown's attorney described Brown as having a bad day at the facility and said he didn't think his client should be forced to stay behind bars for another month.

"You know — do you have a bad day? I have bad days sometimes," Mark Geragos said outside the courthouse.

"Do you say things you'd like to take back? I certainly do. So I don't know that being in a therapeutic session and you're talking about your reflections and you say one sentence means you go to jail? Seems to me to be counterproductive to therapy."

Geragos said he planned to petition to have Brown released before the April hearing. The singer has legal woes on the East Coast as well and is due to go on trial in a misdemeanor assault case in Washington, D.C., on April 17.

Geragos said Brown's incarceration might make it impossible for the trial to start on time, and would be a waste of judicial and jail resources.

Deputy District Attorney Mary Murray however said Brown has had repeated chances to comply with his sentence for the Rihanna attack, which required him to obey all laws and complete six months' worth of community labour. Brown's completion of those hours was called into question last year, and Brandlin required the singer to do another 1,000 hours of roadside cleanup and graffiti removal as punishment for a misdemeanor hit-and-run case.

"He has put himself into custody," Murray said.

Brown appeared in court wearing an orange jail jumpsuit, a sharp contrast from the suits and designer jeans he has worn for other court hearings.

Geragos had requested that Brown be allowed to change into a suit, but Brandlin refused. He did allow the singer's handcuffs to be removed during the hearing, and ordered photographers not film deputies placing the restraints back on after the hearing.

Career affected

Brown had been in court-ordered rehab since November and until recently had received good reviews from probation officials and praise from Brandlin.

Since his arrest in February 2009 for assaulting Rihanna, Brown has worked to restore his public image and has released three albums, including 2011's F.A.M.E. that won the Grammy Award for best R&B album.

Brown's fifth album, X, has been delayed several times and a new release date has not been set. He has launched several singles from the album, but while some have reached the Top 40, they haven't resonated on the charts like his previous tracks.

The Nicki Minaj-assisted Love More was the strongest of the singles, peaking at No. 23 on the Billboard Hot 100 chart, while the current single, Loyal with Lil Wayne, broke into the Top 40 last week. Brown is still a popular guest collaborator in the R&B and hip-hop world: He's featured on rapper Kid Ink's rising Top 15 hit Show Me and singer-songwriter Sevyn Streeter's R&B hit It Won't Stop.