Is the son of Zeus having an identity crisis? The newly released second trailer for Brett Ratner’s 3-D action epic “Hercules” finds Dwayne “The Rock” Johnson playing the mythical Greek warrior as something of a reluctant hero.

“I only want to be a husband and a father,” he demurs early in the trailer, which you can watch above. Moments later, he protests, “I am no hero.”

Forgive us, Herc, but we know better. One look at those oversize muscles, that yak-hair wig and that lion pelt tells us all we need to know: This guy was meant to kick butt. As Ian McShane’s character says at one point, man cannot escape his fate.

Soon enough, Hercules is out for revenge against the gods who murdered his family. He leads armies and battles all manner of man and beast, including undead soldiers, the hellhound Cerberus, the invulnerable Nemean lion and the multi-headed serpent the Hydra.

Advertisement

Directed by Ratner from a script by Ryan J. Condal and Evan Spiliotopoulos, and based on a Radical Studios graphic novel, “Hercules” will put Johnson back in lead action-hero mode after roles in ensemble movies such as “Fast & Furious 6,” “Pain & Gain” and “G.I. Joe: Retaliation.”

The film also stars Rufus Sewell, Joseph Fiennes and John Hurt, but by and large it looks to be resting on Johnson’s broad shoulders.

Paramount Pictures will release “Hercules” July 25.