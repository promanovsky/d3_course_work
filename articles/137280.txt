With its brand-new Nearby Friends feature, Facebook is collecting a log of your travels so that it can one day pass that data along to advertisers, CNET can confirm.

Facebook trotted out Nearby Friends on Thursday. The optional feature, currently rolling out to US users of the native Facebook application for iPhone and Android, lets people turn on location-tracking to receive notifications when friends also using the feature are close by.

Nearby Friends is outwardly intended to facilitate offline get-togethers, as in this is meant to better the average person's experience with Facebook. The social network will, at some future date, enable advertisers to target based on location histories, bettering their experience as well and likely bolstering Facebook's business in the process.

"At this time it's not being used for advertising or marketing, but in the future it will be," a Facebook spokesperson told TechCrunch. CNET confirmed with Facebook that it may use the data for advertising or marketing at some point.

When asked ahead of the release, a Facebook spokesperson told CNET the company was not using data from Nearby Friends to target ads -- which is technically true, but for who knows how long. "The launch of this product doesn't impact the way advertisers can target people based on location," the spokesperson said at the time.

Though the news may be unnerving, it shouldn't come as a surprise that the social network would want to keep tabs on where you are. Facebook is in the business of selling advertising and the more data it can provide to ad buyers, a group that contributed $2.34 billion in revenue to the company in the fourth quarter, the better. If an advertiser had access to members' location history they could, for instance, target their ads to people they knew visited a competitor's store.

In Facebook's defense, the company has made the Nearby Friends feature optional and does allow those who have opted-in to delete their location history from their activity log.

Clarification: 4:17 PM: This article was modified to make clear that Facebook will in the future enable advertisers to target based on location histories.