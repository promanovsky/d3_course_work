Tokyo stocks close down

Share this article: Share Tweet Share Share Share Email Share

Tokyo - Tokyo stocks slipped 0.36 percent Monday, weighed by losses on Wall Street last week and the yen's rise as tensions in Ukraine escalate. The benchmark Nikkei 225 index fell 49.89 points to 13,910.16, while the Topix index of all first-section shares slipped 0.12 percent, or 1.33 points, to 1,132.76. Disappointment over the Bank of Japan's decision not to expand its stimulus drive - and a consequent rise in the yen - helped drag the Nikkei to a six-month low on Friday, with the headline index down 7.33 percent over the week. That was the Nikkei's worst weekly performance since a 10 percent decline in the period after Japan's 2011 quake-tsunami disaster. The headline index is down about 14 percent this year, and its slide may not have bottomed out, said Tomohiro Okawa, equity strategist at UBS Securities Japan.

“No optimism is warranted,” Okawa told Dow Jones Newswires.

“There remains downside risk.”

On Monday, the Nikkei was dragged lower by shares in market heavyweight Fast Retailing, operator of the Uniqlo clothing chain, falling 3.03 percent to 32,795 yen.

That extended the retailer's losses after its shares plunged 7.87 percent Friday as it cut its full-year earnings forecast, blaming weaker demand at home.

Last week, the tech-rich Nasdaq Composite Index led Wall Street sharply lower again, closing out a dreary week at its lowest level in more than two months.

The Nasdaq sank 1.34 percent to 3,999.73, its lowest close since February 3, while the Dow Jones Industrial Average tumbled 0.89

percent to 16,026.75.

Markets are jittery after Ukraine's acting president accused Russia of waging war in his country's eastern belt as fresh clashes left at least two dead.

Acting President Oleksandr Turchynov declared the launch of a “full-scale anti-terrorist operation” while the United Nations Security Council announced emergency talks late on Sunday.

In forex trading, the dollar slipped to 101.56 yen, down from 101.65 yen in New York Friday afternoon.

A stronger yen is bad for shares of Japanese exporters as it dents their profitability.

In other Tokyo stock trading, Sony lost 0.31 percent to finish at 1,870 yen, Canon declined 0.25 percent to 3,126 yen, while Toyota added 1.80 percent to 5,410 yen.

Sharp's volatile shares dropped 8.69 percent to 273 yen, after a report that the embattled electronics giant was mulling a $2.0 billion share offering to help shore up its dented finances.

The move was likely to be unpopular among current shareholders as it would dilute the value of their investment. - Sapa-AFP