Activists are all atwitter following an out-of-context tweet by @ColbertReport.

During one of Stephen Colbert’s comedy sketches years ago, he performed a stereotyped Chinese impression. You know the ones; eyes all squinty, using broken English, and talking like a doctor in an 80s Kung Fu flick. Flash forward to last Thursday when Stephen did a segment making fun of Dan Snyder who was attempting, perhaps poorly, to placate American Indians who might be offended by the name of his football team, the Washington Redskins. Colbert said he would create his own charity called “The Ching-Chong Ding-Dong Foundation for Sensitivity to Orientals or Whatever.” The Ching-Chong Ding-Dong name is attached to the aforementioned impression by Colbert.

It was a remark which initially didn’t seem to draw any ire from the easily offended. A few days later, activist Suey Park saw a tweet from @ColbertReport that read “I am willing to show #Asian community I care by introducing the Ching-Chong Ding-Dong Foundation for Sensitivity to Orientals or Whatever.” She soon responded and added the hashtag #CancelColbert, initiating a firestorm of back-and-forth comments that immediately began trending. The controversy eventually spilled over into the TV news media.

Everyone knows a successful show like The Colbert Report isn’t going to get cancelled over, of all things, satire. So what was the point of initiating a controversy? In an interview with HuffPost Live, Activist Suey Parks said she used the hashtag #CancelColbert to spread a critique on racial humor. But near the end of the interview, she drowned her own argument in hypocrisy by stating:

“I feel like it’s incredibly patronizing for you to paint these questions this way, especially as a white man. I don’t expect you to be able to understand what people of color are actually seeing with regard to #CancelColbert.” It’s hard to make an argument against stereotypes if you yourself are stereotyping others, especially during said argument.

There was initial confusion over who the actual person or group was that posted the comment to Twitter. Stephen shot back with a tweet explaining that his personal Twitter account name is @StephenAtHome, and not @ColbertReport. Twitter co-founder Biz Stone visited The Colbert Report and the offending Twitter account was removed.

Although the Colbert Report didn’t tape over the weekend, it returned yesterday ready to fight back with, of course, more light-hearted humor. Stephen joked “I’m still here. The dark forces’ attempt to silence my message of core conservative principles mixed with youth friendly product placement have been thwarted.” A sarcastic turnaround of a statement, considering The Colbert Report itself is a news show parody that often pokes fun at conservatives.

Colbert took a segment of the show to explain what had happened and why he wasn’t racist. He told the audience, as he has many times before, that he didn’t even see color. He only knows that he’s white because people tell him so.

“I just want to say that I’m not a racist. I don’t even see race, not even my own. People tell me I’m white and I believe them because I just spent six minutes devoted to explaining how I’m not a racist, and that is about the whitest thing you can do.”

The main response from The Colbert Report is that the tweet was part of a larger segment and had been taken out of context when it was reduced to 140 characters or less. Some protesters believe that even when taken in context, it was still a degrading remark.

It should be noted that everyone who has followed this Colbert Controversy, no matter what side you are on, has read this article with your inner voice saying ‘Coal – Bear’ and not ‘Coal – Burt’, which is how his name is actually pronounced. So, have you been playing along while he pokes fun at the French?

How stereotypical.