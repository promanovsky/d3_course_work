The Heartbleed security bug can be tested here and theere's a list of websites affected.(Screenshot Heartbleed.com)

Heart Bleed Bug Test: Are Passwords for Yahoo, Facebook, Gmail, Twitter, Chase, Wells Fargo, Citibank Affected by Error?

The three major tax return services–H&R Block, TaxAct, and TurboTax–say that the OpenSSL vulnerability HeartBleed doesn’t affect their websites. However, the

A number of people doing last-minute tax returns have expressed worry that HeartBleed may impact them. This comes after the Canada Revenue Agency totally shut down its e-filing operations to make sure that the HeartBleed error doesn’t cause it any problems.

CRA Commissioner Andrew Treusch told the CBC: “Our systems are back online. We apologize for the delay and the inconvenience it has caused to Canadians. That said, the delay was necessary. We could not allow these systems back online until we were fully confident they were safe and secure for Canadian taxpayers,” according to PC Mag.

But H&R Block, TaxAct, and TurboTax have stressed their services are fine.

“We have found no risk to client data from this issue. As a result of this situation, we are reviewing our systems and taking the appropriate steps to ensure our customers are protected,” H&R said in a blog post.

“The IRS has said that their systems continue to operate and are not affected by this bug, and they are not aware of any security vulnerabilities related to this situation. The IRS continues to accept tax returns as normal. The advice to taxpayers is to continue filing tax returns, including e-file,” it added.

TaxACT stated via the company Facebook page that their service “does not use any vulnerable version of OpenSSL for certificates and encryption and are thus not affected by the Heartbleed security issue.”

And TurboTax added: “TurboTax engineers have verified TurboTax is not affected by Heartbleed … Even though TurboTax was not vulnerable, we have taken additional security precautions to protect the security and privacy of customers’ personal and financial information.”

TurboTax said that it constantly monitors its “systems and have no indication that TurboTax.com was ever vulnerable. Earlier this week, we did patch a couple of support services to protect against the Heartbleed security vulnerability. We’re confident that TurboTax is safe to use. The fact is, the vast majority of our servers do not use the version of SSL that was vulnerable to Heartbleed.”

It remains unclear whether hackers have been able to exploit Heartbleed, which went undetected for more than two years, to steal personal information.

The bug is caused by a flaw in OpenSSL software, which is used on the Internet to provide security for both websites and networking devices such as routers, switchers and firewalls.

The Associated Press contributed to this report.