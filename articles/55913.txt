Children with autism may have irregular clusters of neurons in the brain, according to a paper published on Wednesday, March 27 in the New England Journal of Medicine. The findings suggest brain abnormalities in children with autism can be traced back to prenatal development.

The small study involved children ages 2 to 15, and was partially funded by the National Institute of Mental Health -- part of the National Institutes of Health -- and the Allen Institute for Brain Science in Seattle.

Clusters of disorganized brain cells were discovered in tissue samples from brain regions important for regulating social functioning, emotions and communication -- which can all be troublesome for children with autism.

The abnormalities were found in 10 of 11 children with autism, but in only one of 11 children without the disorder. The children's brains were donated to science after death; causes of death included drowning, accidents, asthma and heart problems.

"Because this points to the biological onset in prenatal life, it calls sharply into question other popular notions about autism," including the scientifically debunked theory that childhood vaccines might be involved, said lead author Eric Courchesne, an autism researcher at the University of California, San Diego.



The authors said the clusters, detected with sophisticated lab tests, are likely defects that occurred during the second or third trimesters of pregnancy. "It could be gene mutations and environmental factors together," said Courchesne.

Scientists have been working for decades to find the cause of autism, and they increasingly believe its origins begin before birth. In addition to genetics, previous research suggests other factors might include infections during pregnancy, preterm birth and fathers' older age at conception.

About 1 in 88 children in the U.S. have one of the autistic spectrum disorders, which include classic autism and a milder form, Asperger syndrome.

Dr. Melissa Nishawala, director of autism for the New York University Child Study Center, told "CBS This Morning" that these findings can help scientists locate biomarkers associated with autism and intervene earlier in a baby's development.

"We want intervene in that first year of life to know something so we can start to change what we do and we can help coax and nudge those brain pathways into connections," said Nishawala, who was not involved in the study.

Other experts not involved in the new study called the results preliminary and said larger studies are needed to determine if the unusual brain development found in the study causes problems, and if it is truly common in autism or even in people without the disorder.

Other scientists have suggested that autism may be linked with abnormalities in the brain's frontal region, and that for at least some children, problems begin before birth.