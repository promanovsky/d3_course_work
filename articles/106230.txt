SOUTH BEND, Ind., April 11 (UPI) -- NASA-funded researchers think they may have spotted an "exomoon," a moon orbiting a planet outside our solar system. Though they may never know for sure.

The astronomers say it's near impossible to confirm what they saw, but they say the sort-of-sighting may help them find other exomoons in the future.

Advertisement

"We won't have a chance to observe the exomoon candidate again," said David Bennett, a researcher at the University of Notre Dame. "But we can expect more unexpected finds like this."

The momentary sighting was part of a project led by the joint Japan-New Zealand-American Microlensing Observations in Astrophysics (MOA) and the Probing Lensing Anomalies NETwork (PLANET) programs. Their research on the subject is detailed in the latest edition of the Astrophysical Journal.

Astronomers with these research teams will continue trying to find other exomoons.

Astronomers are able to spot the moons orbiting Jupiter because of their relative closeness to Earth. However, picking out the faint glow of a tiny sphere in a solar system several light years away is exceedingly difficult.

They do so by looking for the proper ratio of light -- the differing glows of a star, alien planet, and accompanying moon. But with so many stars and orbiting objects spread out across our galaxy and beyond, defining the relationship between some of these luminous spheres remains tough. Complicating matters is the fact that astronomers need all three cosmological objects visible at once, the light from the foreign star hitting its obedient, orbiting planet, and that light reflecting off the planet and onto its moon.

The process of searching for this relationship between sun, planet and moon -- and the specific ratio of light, emitted and reflected -- is called the lensing system.

Because astronomers don't know exactly where and when to look, they must scan thousands of data points collected by NASA's Spitzer and Kepler space telescopes looking from the proper ratios -- for a lensing event. And even if they find a possible match, they then must undertake that also near-impossible task of determining how large and how far away these objects are.

So far, astronomers have catalogued 1,700 alien planets. But the search for the first alien moon continues.

Still, scientists like Bennet are hopefully the new lensing system technique will get them closer to finding a verifiable exomoon.

[NASA] [Space.com]

RELATED Satellite photos show Russian troops massed on Ukraine border