The old expression, "there's nothing like home cooking," may truly turn out to be words to live by as a study by the Center for Science in the Public Interest, a Washington non-profit group, claims you are twice as likely to get sick from food prepared in a restaurant versus food prepared in your home.

The group followed data for food poisoning including Botulism, Salmonella, E. coli O157:H7, Hepatitis A or Listeria and analyzed "solved" outbreaks over a 10-year period. The data reveals more than 1,610 outbreaks in restaurants were responsible for sickening over 28,000 people compared to 893 outbreaks sickening roughly 13,000 people in private homes.

The study adds 1 in 6 people in the U.S. or roughly 48 million, suffer from foodborne illness every year, Even more alarming is the fact that 128,000 are hospitalized and 3,000 die from diseases related to the sickness.

While meat-related food poisoning outbreaks typically grab the most media attention, the CSPI noted seafood, fresh produce and packaged foods were actually responsible for twice as many of the solved outbreaks followed in their research versus meat and poultry.

Another issue of concern cited by CSPI is a decrease in the reporting of foodborne illness outbreaks as the group claims 42 percent or fewer outbreaks were reported by states to the Centers for Disease Control and Prevention (CDC) last year.

"Underreporting of outbreaks has reached epidemic proportions," said Caroline Smith DeWaal, CSPI food safety director. "Yet the details gleaned from outbreak investigations provide essential information so public health officials can shape food safety policy and make science-based recommendations to consumers."

Regarding combating the increase in food poisoning cases the CDC has come up with a new way to study the sickness using gene mapping, the same concept used to predict a person's risk of cancer, as scientists are regularly scanning through the DNA of harmful bacteria and viruses to monitor different strains of food poisoning.

After reading this there's a good chance you'll be eating in tonight.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.