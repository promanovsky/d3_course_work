A recent Harvard Business School grad has come up with a brilliant way to disrupt the $55 million makeup industry. Rather than run to Sephora to pay premium prices on a unique—but let’s face it, really impractical—eyeshadow color, women can now quite literally print it from their home computer.

Welcome to the wonderful world of 3D printing, in which people can create anything from food to human skulls to your favorite, discontinued lipstick color.

“The makeup industry makes a whole lot of money on a whole lot of bulls—,” Grace Choi told an eager crowd at TechCrunch Disrupt. “They do this by charging a huge premium on one thing that technology provides for free, and that one thing is color.”

And so Choi created Mink, a mini 3D printer for your home that will allow users to print out any color of makeup using FDA-approved ink. (That should soothe moms who are a little worried about their kids standing too close to the microwave, let alone putting something that came out of a printer on their eyelids). Choi explained, “The inkjet handles the pigment, and the same raw material substrates can create any type of makeup, from powders to cream to lipstick.”

While Walmart and drug stores have limited their color selections to those that will lead to mass sales, Mink has turned computers and smartphones into endless beauty aisles with an unlimited color palate selection. Users can pull the hex code of any color found on a website — including Pinterest boards and YouTube makeup tutorials — or smartphone photos. “We’re going to live in a world where you can take a picture of your friend’s lipstick and print it out,” Choi said.

Once users have the color code, they only have to plug it into Photoshop or Paint and press the print button. Choi showed how simple the process was in a live demo. It took less than 40 seconds to print a pink eyeshadow.

Mink’s target demographic is 13 to 21 year old girls, who might find the price tag a little hard to manage. But even if printer ink might be comparable in price to Mac, the product is still pretty incredible.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.