U.S. stocks fell on Wednesday, ending a four-session rally that lifted the S&P 500 to a record, with investors skittish as benchmark Treasury yields fell to lows not seen since last summer.

"We've had a bunch of up days, and May has been a good month, but the market could be getting spooked by falling yields. Market bears will say the bond market is smarter than the stock market, and a lot of people thought 2.5 percent was the line in the sand for the bottom falling out of the economy," said Nick Raich, CEO at the Earnings Scout.

"It is a big disconnect; the stock market is signaling growth, but bonds are doing something different. I would be worried if I saw bond yields falling with a deterioration in earnings trends, but we are just not seeing that," Raich added.



"The divergence continues as we make new intraday highs on the S&P and new lows on the 10-year," said Art Hogan, chief market strategist at Wunderlich Securities.



The 10-year U.S. Treasury yield fell 8 basis points to 2.439 percent, its lowest since last summer.

A major factor driving bond yields lower is the unwinding of a trade that had investors long the U.S. dollar and shorting the Japanese yen and U.S. Treasuries coming into 2014, Hogan said.

Supply and demand is also playing a role, as the government issues less debt but pension funds still need to reallocate into bonds, given their common mandate of having 60 percent of their holdings in equities and 40 percent in bonds. "Equities were up 30 percent last year, so pension funds need to increase their bond allocation," Hogan said.

Finally, "one of the largest buyers of U.S. Treasuries is the U.S. government. To keep the balance sheet the same size, the government is an aggressive buyer of issuance," Hogan said.

Another reason is "what is going on in Europe. If people can get better yields in the U.S., global investors would be buying U.S. Treasuries," said Raich.

Regardless of the cause, the scenario could be a win-win for the U.S. Federal Reserve.

"Janet Yellen and the Fed get to exit QE without having to worry about choking off growth. The purpose of QE was to bring down bond rates to stimulate the economy," said Raich.

Read MoreThis what is keeping bond yields low

Toll Brothers rose after the homebuilder reported quarterly profit more than doubled; DSW plunged after the footwear retailer reported a decline in sales and Workday's shares rose, a day after the maker of human resources software tallied a surge in first-quarter revenue.