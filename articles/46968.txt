Rupert Murdoch-led 21st Century Fox today appointed his son Lachlan Murdoch as Non-Executive Co-Chairman of the company, while the other son James Murdoch has been elevated to the position of Co-Chief Operating Officer, signalling a future succession plan.

As Non-Executive Co-Chairman, Lachlan will work closely with senior management and the rest of the 21st Century Fox Board in developing global strategies and guiding the overall corporate agenda, the company said in a statement.

Lachlan will divide his time between Sydney and New York, it added.

On James' elevation, the company said as Co-Chief Operating Officer, he will partner with President and Chief Operating Officer Chase Carey "to set the strategic direction and drive momentum across the company's global portfolio of assets."



James will continue to report to Carey.

"In this capacity, James will have direct responsibility for Fox Networks Group, which will now report to him. James will also have direct responsibility for the strategic and operational development of the company's owned and controlled interests in the pay-television Sky and Star services in Europe and Asia, respectively," it added.

Commenting on the changes, 21st Century Fox Chairman and CEO Rupert Murdoch said: "I'm very pleased he (Lachlan) is returning to a leadership role at the Company, where he will work closely with me, Chase, James, and the rest of the Board of Directors to drive continued growth for years to come."



Carey said James' appointment formalises the role he's been playing for some time, and acknowledges the contribution he has made in driving the company's businesses.

"Over the course of his 17 years with the company, James has played a key leadership role in almost all of our major television businesses, from Mumbai to Munich to London and most recently the US," Carey said.

James played key role in the company's domestic and international channel expansion, from the creation of Sky Deutschland and Sky Italia, serving both as Chairman, to the renewed and continued leadership of Star India, India's number one entertainment broadcaster.

In addition to these appointments, the company also announced that Peter Rice, Chairman and CEO Fox Networks Group, has agreed to extend his employment agreement.

21st Century Fox is a global leader in cable, broadcast, film, pay TV and satellite assets spanning six continents with nearly 1.5 billion subscribers in more than 100 local languages.