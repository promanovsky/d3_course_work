The Los Angeles Department of Water and Power on Wednesday doubled the estimated amount of water lost in a massive main break that flooded UCLA to 20 million gallons.

A section of Sunset Boulevard pummeled by the geyser that erupted from the rupture will remain closed for an additional 48 hours, officials announced.

The pipeline, which ruptured on Sunset Boulevard shortly before 3:30 p.m. Tuesday, sent a cascade of water onto the UCLA campus for about four hours, inundating parking garages, sports facilities and campus buildings, including the Pauley Pavilion, where university officials said a recently installed wood floor will have to be completely replaced.

It also closed Sunset from Marymount Place to Westwood Plaza, putting a clamp on the busy morning commute.

Advertisement

Meanwhile, the estimated number of vehicles marooned in flooded underground parking garages increased to roughly 900, and officials warned that crews may not be able to start towing them out until Friday.

Previous estimates had placed the number of vehicles closer to 700, though officials said not all the trapped vehicles were damaged.

About half the vehicles probably were submerged at some point and suffered major damage, they added. Some of the cars, however, may be dry and fine, and just happened to be parked in an upper level that was declared off-limits during the water main break.

UCLA spokesman Phil Hampton said all UCLA students, faculty, staff and any visitors who could be reached were urged to self-report online if their autos remained in parking structures 4 and 7. The number of those reports rose from about 730 early Wednesday morning to about 900 later in the afternoon, said Hampton, whose own 2013 Honda Civic is among those on a lower level and presumed to be badly damaged.

Advertisement

He noted that the number could change again since crews have not been able to conduct a full inventory in the garages.

Complicating efforts to drain the garages was all the gasoline and other pollutants from the submerged vehicles now mixing into the water, officials said. Crews are testing the water to make sure that the liquid stew is not reaching any levels of becoming explosive or flammable. They were also monitoring the levels of pollution in the water before it enters the sewer system.

“They sometimes have to slow it down,” UCLA spokeswoman Carol Stogsdill said of the pumping, noting that the two garages now smell of gasoline.

In a briefing at the ramp entrance to the darkened parking lot 4 garage on Wednesday, Kelly Schmader, assistant vice chancellor of facilities management, said that although water remained in structures 4 and 7, it was being removed at a rate of 4,000 gallons per minute. Pumping any faster could create cabin monoxide risks, he said.

Advertisement

“At this rate, based on the amount of water we removed so far, we think that by the end of the day Friday we’ll probably have the water completely out of the garages and be able to start moving some of these vehicles out of there,” he said.

Another university official, Jack Powazek, said the vehicles will be towed out by UCLA to a location where people can pick them up. No date has been scheduled yet for those pickups.

Powazek said the university has asked motorists to contact their insurance companies about damaged vehicles. “This was caused by an event that was external to UCLA -- none of our infrastructure items -- we’ll have to talk with the city.”

On Wednesday, Stogsdill said the university believes Los Angeles Department of Water and Power should be held liable for the extensive damage caused by the deluge of water that descended on the campus Tuesday.

Advertisement

The pipeline, which ruptured shortly before 3:30 p.m. Tuesday, sent a cascade of water onto the UCLA campus for about four hours, inundating parking garages, sports facilities and campus buildings, including the Pauley Pavilion and Wooden Center.

“We’re keeping a running log of what this is costing, including anything we have to pay out,” Stogsdill said. “This all comes under the DWP. This will cost the people of Los Angeles, not UCLA.”

Officials said that 3 to 4 feet of water remained in both parking structures and that the bottom floor of the south end of each structure was completely submerged as of about noon.

Chancellor Gene Block said he was thankful no one was injured by the flooding, but acknowledged “there’s a lot of damage.”

Advertisement

“This is an enormous flood,” he said, “and I think we’ve made fantastic progress.”

He estimated the costs would be in the millions.

“These are big buildings,” he said.

The damage was also creating big problems for the campus community.

Advertisement

“I think about students that have final exams, not only do they have their cars here, but their notebooks in there, their lab books, their computers,” Block told The Times. “For a lot of our students, it’s very stressful.”

And for staff members who commute long distances, finding alternate transportation will be a challenge, he added.

Power had been restored at the Arthur Ashe Center as of Wednesday afternoon. Tables staffed by administrators and a doctor were in the plaza throughout the day to assist students who had appointments.

“We’re thankful we don’t know of any human injuries, but that doesn’t mean there isn’t a lot of human trauma,” Block said.