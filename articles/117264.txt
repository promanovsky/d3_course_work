NEW YORK (TheStreet) -- Shares of Coca-Cola are climbing on Tuesday after the company reported better-than-expected quarterly revenue, boosted by strong sales in China that offset a drop in Europe and flat volumes in its key North American market.

VIDEO TRANSCRIPT:

Shares of Coca-Cola (KO) - Get Report were climbing on Tuesday after the company reported better-than-expected quarterly revenue on strong sales in China.

Case volumes in the country rose 12% in the quarter, offsetting a drop in Europe and flat volumes in North America. Earnings came to 44 cents a share, excluding items, and were in line with analysts' estimates, according to Thomson Reuters. Soda sales remained a challenge, as global sales of carbonated beverages fell 1% by volume. Revenue fell 4% to nearly $10.6 billion, narrowly beating analysts' estimates.

WATCH:More videos from Brittany Umar on TheStreet TV

After returning $713 million to shareholders during the period, the company announced it is planning stock buybacks of between $2.5 billion and $3 billion by the end of this year. Coke also said it will spend $400 million in advertising efforts after the company said its advertising during the Sochi Olympics and Super Bowl improved first-quarter volume.

At last check, shares of Coke were climbing more than 3% to $39.95.

In New York, I'm Brittany Umar for TheStreet.

Written by Brittany Umar in New York.