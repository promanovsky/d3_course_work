Boeing, airline could face lawsuit over jet

Share this article: Share Tweet Share Share Share Email Share

New York - Malaysia Airlines and Boeing are facing a potential lawsuit over the Beijing-bound flight that disappeared more than two weeks ago with 239 people on board, according to a law firm representing passengers' families. A petition for discovery has been filed against Boeing, manufacturer of the aircraft, and Malaysia Airlines, operator of the plane, Chicago-based Ribbeck Law said in a statement on Tuesday. The Boeing 777 vanished while flying to Beijing from Kuala Lumpur. Malaysia said on Monday that the missing jetliner had crashed into remote seas off Australia, citing satellite data analysis. Airline officials on Monday said all on board were presumed dead. The petition for discovery, filed in a Cook County, Illinois Circuit Court, is meant to secure evidence of possible design and manufacturing defects that may have contributed to the disaster, the law firm said.

The court filing was not immediately available.

The filing initiates a multimillion-dollar lawsuit against the airline and Boeing by the passengers' families, the firm said.

“We believe that both defendants named are responsible for the disaster of Flight MH370,” Monica Kelly, the lead Ribbeck lawyer in the case, said in the statement.

The petition was filed on behalf of Januari Siregar, whose son was on the flight.

Additional pleadings will be filed in the next few days against other potential defendants that designed or manufactured component parts of the aircraft that may have failed, Kelly said.

Ribbeck is also asking that US scientists be included in the search for wreckage and bodies, the firm said.

A spokesman for Boeing declined to comment.

A spokesman for Malaysia Airlines could not immediately be reached for comment.

Ribbeck is also representing 115 passengers in the crash of Asiana Airlines Flight 214 in San Francisco in July.

The law firm's petition is asking the judge to order Boeing to provide the identity of manufacturers of various plane components, including electric components and wiring, batteries, emergency oxygen and fire alarm systems.

It is also seeking the identify of the company or person who last inspected the fuselage and who provided maintenance.

The petition also asks the judge to order Malaysia Airlines to produce information about crew training for catastrophic incidents, security practices, safety training and crew evaluations. - Reuters