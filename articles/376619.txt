JER2002090206 - TEL AVIV, Israel, Sept. 2 (UPI) - An Israeli Health Ministry nurse inoculates an unidentified health worker with the smallpox vaccination at Tel HaShomer Hospital in Tel Aviv, on Sept. 2, 2002. An estimated 15,000 Israeli emergency and health workers are receiving vaccinations against smallpox in the first phase of preparation against a potential biological attack from Iraqi leader Saddam Hussein. rlw/dh/Debbie Hill UPI. | License Photo

BETHESDA, Md., July 8 (UPI) -- "Don't worry guys, we found the missing smallpox vials."

This is not likely the phrase first uttered when National Institute of Health officials recently discovered several ancient vials of variola -- the Latin name for smallpox -- in an abandoned storage closet. Though such words exhibit the absurdity of the story.

Advertisement

According to the NIH, several scientists came upon three vials of smallpox left over from the 1950s. They were cleaning out a storage closet of a laboratory they're preparing to relocate from its current home in the FDA's campus in Bethesda, Md. to a new building.

Upon finding the vials, the scientists immediately called the Division of Select Agents and Toxins, the agency the handles toxic substances. The vials were taken to a secured lab.

RELATED Missing teenage girl left disturbing clues in diary and text messages

Though it's been sitting in a storage closet for more than 50 years, experts say the vials could still hold a potent virus.

"It's pretty hardy as viruses go, particularly in the freeze-dried state," said Dr. Steven Monroe, director of the CDC's division of high consequence pathogens and pathology. "That could certainly prolong viability."

The CDC says there's no evidence that the stash of variola vials was leaked, and that there is no risk to public health.

RELATED Netherlands ready for semifinal clash with Argentina

Believed to have emerged around 10,000 BCE, the viral disease has killed millions over the course of history. The disease killed roughly half a million Europeans annually during the latter stagers of the 18th century. Successful immunization programs eradicated the disease entirely by 1979.

Technically, the vials are in violation of an international bioterrorism law that says only a lab at the CDC's Atlanta headquarters and a lab at the VECTOR Institute in Russia are allowed to host strains of smallpox.