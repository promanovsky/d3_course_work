Music

German doctors report that they treated a 50-year-old Motorhead fan who ended up having a blood clot on the brain after getting carried away at one of the band's concerts

Jul 5, 2014

AceShowbiz - Being a heavy metal fan can sometimes be dangerous. According to a report by The Lancet, German doctors recently treated a Motorhead fan whose headbanging habit ultimately led to a brain injury.

The 50-year-old male fan from German ended up having a blood clot on the brain after getting carried away at one of the rock band's concerts. The unnamed fan came to Hanover Medical School earlier this year after he started suffering a constant, worsening headache that affected the whole head.

A CT scan showed that the man had a subdural haematoma on the right side of his brain. The fan had no history of head injury, but he said he had been headbanging for years, most recently when attending a Motorhead concert with his son. Surgeons removed the clot via a hole drilled into the skull, and the fan was eventually able to make a full recovery.

This is the fourth documented case of subdural haematoma linked to headbanging, one of which is fatal. However, fans don't have to give up on their headbanging habit as doctors insist that "the risk of injury is very, very low."

"We are not against headbanging," said Dr Ariyan Pirayesh Islamian, one of the doctors who treated the man. "The risk of injury is very, very low. But I think if [the patient] had gone to a classical concert, this would not have happened."

"This case serves as evidence in support of Motorhead's reputation as one of the most hardcore rock 'n' roll acts on Earth, if nothing else because of their music's contagious speed drive and the hazardous potential for headbanging fans to suffer brain injury," he added.