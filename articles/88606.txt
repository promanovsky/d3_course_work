Microsoft is discontinuing support for Windows XP, the study operating system first introduced in 2001.

As of tomorrow, users will no longer be able to receive technical assistance for XP, and all automatic updates will cease. The move, first announced last year, comes as Microsoft turns its eye toward new platforms, including Windows 8.1 and the Windows Phone 8.1 mobile operating system.

"If you continue to use Windows XP after support ends, your computer will still work but it might become more vulnerable to security risks and viruses," Microsoft reps explained in a message on the Windows site. "Also, as more software and hardware manufacturers continue to optimize for more recent versions of Windows, you can expect to encounter greater numbers of apps and devices that do not work with Windows XP."

Meanwhile, as the Associated Press points out, there's the question of compatibility – it's going to get harder and harder to find printers and scanners and other peripherals that play well with the old Windows OS. "Same goes for new software, particularly if it needs faster processors and more memory beyond what was standard in XP’s heyday," the AP notes.

Some holdouts, such as the UK and Dutch governments, have forked over some serious cash in order to prolong their use of the XP operating system, although we certainly wouldn't recommend a similar course of action.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

So how do you upgrade your XP machine? Well, the first step is to boot up the Windows Upgrade Assistant, free software that will help you determine if your machine is capable of running newer software. If it is, you can use Laplink to save and transfer all your files, gratis.

If it's not, it may be time for a new machine.