Manufacturing activities rose in May compared to the previous month on increased orders, according to the widely-tracked HSBC Purchasing Managers’ Index (PMI).

The measure of factory production was up at 51.4 points in May from 51.3 points in April. March, too, had recorded 51.3. In a year, May PMI was surpassed only in February 2014, when the reading stood at 52.5 points. A reading above 50 shows growth.

The increase in PMI reading could have been higher had power cuts not dulled factory orders. General elections also came in the way of manufacturing activities, said Markit Economics, the financial information firm that compiles PMI data.

There was a ray of hope in job creation. Respondents to the survey reported higher new orders, signing of new contracts and improved demand from domestic and foreign clients. “Growth of both total new orders and new export business accelerated over the month, leading to further job creation across the sector,” said Markit Economics.

HSBC asked the Reserve Bank of India (RBI), due to announce its monetary policy review on Tuesday, to not lose guard over inflation, though input price pressures have eased.

Markit Economics said new export business rose at a quicker pace than in April. Official data of exports for May is expected next week. That data would clarify if the respondents’ optimism of new orders has translated into more tangible results.

The May data highlighted further increases in new work, marking the seventh month of expansion. Moreover, the pace of increase accelerated to the quickest since February.

May data indicated that production volumes at manufacturers continued to rise. Output rose for the seventh consecutive month in May. However, the rate of expansion was unchanged from the modest pace in April.

“Output growth held steady as frequent power cuts forced firms to accumulate backlogs at a faster pace,” said Neumann.

The Associated Chambers of Commerce and Industry of India too said continued power disruptions forced industrial units to curtail production by about 30 per cent in April and May.

Markit Economics said the latest rise in production was broad-based by sector, with the sharpest expansion signalled by consumer goods producers. Staffing levels were raised in May, amid evidence of increased production requirements.