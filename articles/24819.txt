Marc Andreessen has fired back at Carl Icahn, this time excerpting from Elaine Grant’s TWA – Death of a Legend in a clear attempt to scare shareholders off the idea of giving Icahn more influence over eBay Inc (NASDAQ:EBAY) by giving him seats on the board. Icahn wants to shake up the board and senior management of eBay and then spin off PayPal into a separate company.

TWA excerpt mirrors some of Icahn’s own allegations

Nothing in the excerpt is really new to people familiar with Icahn’s career. He was one of the 1980’s consummate corporate raiders and an inspiration for the film Wall Street, and no one doubts that he can play tough. But the subtext of the excerpt that Andreessen chose is interesting because it mirrors the allegations that Icahn has been leveling against him for months.

2020 Letter: Maverick Is Set To Take Advantage Of The Great Hedge Fund Unwind [Exclusive] Short-sellers have been feeling the pain for months, but especially over the last two weeks. In his fourth-quarter letter to investors, Maverick Capital's Lee Ainslie pointed out the unprecedented levels stock prices have reached and why short-sellers have been hurting. Q4 2020 hedge fund letters, conferences and more High market caps Ainslie noted that Read More

Aside from accusations of general mismanagement, Icahn says that it was a conflict of interest when Andreessen bought Skype from eBay Inc (NASDAQ:EBAY) while sitting on the board, and then flipped it a few years later for a large profit. He also takes objection to Andreessen sitting on the board while he owns significant stakes in some of eBay’s direct competitors.

“The Skype debacle represents in my mind a complete and utter breakdown in the system of checks and balances that a properly functioning board should provide and proves to me that new blood is needed in the boardroom immediately,” Icahn wrote today in an SEC filing. “What exactly is “world class” about participating in a consortium that buys assets from the company and flips them for a quick and massive profit (current board member, Marc Andreessen)?”

Andreessen unintentionally casts himself as Lorenzo

By showcasing how Icahn made hundreds of millions in profits while TWA went into permanent decline, Andreessen is trying to give shareholders the impression that they should be careful what they wish for.

“As had happened 16 years earlier, when the fear of Frank Lorenzo drove TWA’s employees into the arms of an arguably deadlier foe, the specter of Icahn, who made a $1.1-billion offer and said he would keep the airline independent while demanding labor concessions and making job cuts, made the American offer seem aglow with promise,” writes Grant in the excerpt Andreessen chose.

It’s ironic that Andreessen seems to be casting himself as Lorenzo in this historical metaphor, implying that he is the lesser of two evils as shareholders decide who to vote for in the looming proxy fight.