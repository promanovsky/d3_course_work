The makers of 'Fast and Furious 7' will reportedly use body doubles to complete deceased actor Paul Walker's scenes. Reuters

The makers of 'Fast and Furious 7' will reportedly use body doubles to complete deceased actor Paul Walker's scenes.

The movie, which is gearing up to resume production, will reportedly use body doubles and CGI (Computer Generated Imagery) for scenes that feature Walker's character of Brian O'Conner, reported New York Daily News.

"They have hired four actors with bodies very similar to Paul's physique and they will be used for movement and as a base. Paul's face and voice will be used on top using CGI," said a source.

It was rumoured that Walker's brother, Cody, who is a stuntman, would be hired to stand in for him, but no official word has been released.

The shooting of the hit franchise's seventh installment had to be delayed after the untimely death of Walker, 40, who passed away in a car crash last November.

Also Read:

Live: BSE Sensex