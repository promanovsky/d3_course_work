The search engine giant is in a race with Amazon, Apple and others to lure users with devices that favor their content and services. In Google’s case, such devices also multiply the company’s opportunities to run targeted ads, its primary revenue stream. (Disclosure: Amazon founder Jeff Bezos owns The Washington Post.)

AD

According to the Journal, the device itself is made by another company but uses Google’s new Android TV software designed to play movies, games and other content on televisions. Users will be able to use Android phones, tablets and potentially other devices as remote controls.

AD

A key feature of Android TV will be videogames that work seamlessly between mobile devices and TV, a developer briefed on Google’s plans told the Journal. That means users could play a game on the bus on the way to work, then pick up where they left off on their Android-powered TV when they get home. Another source said a split-screen multiplayer game is in development.

According to CNET, at least one Google device unveiled on Wednesday will have a Nvidia Tegra 4 processor, with other boxes using Intel processors in the works. The Tegra 4 is the same chip used in Nvidia’s Shield, an Android-based handheld video game released last year.

AD

Google TV has been available through devices made by LG, Netgear, Vizio and other companies since 2010, but many Android partners use their own software to power the devices. The company’s previous forays into the TV market have flopped. Former partner Logitech called its 2010 Google TV project “a mistake of implementation of a gigantic nature.” Google also introduced a media streaming player, Nexus Q, at its 2012 I/O conference, but shelved the project after receiving poor feedback.

AD

However, Google has been successful with Chromecast. At $35, the dongle is cheaper than a set-top box and lets users stream Internet video through phones, tablets and computers running Google’s Chrome browser.

Market research suggests it’s worth Google’s while to keep trying for success in the TV market. A market researcher with Parks Associates told CNET that, while TV sales are down, sales of devices that plug into televisions and play video and music are expected to jump to 330 million units by 2017, double the total from last year.