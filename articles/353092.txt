NEW YORK - The Dow Jones industrial average closed above 17,000 for the first time Thursday, the index's first big 1,000-point milestone this year, following news that hiring in the U.S. accelerated last month.

The market rose from the start of trading after the government reported that U.S. employers hired more workers than investors and economists expected. Trading was extremely light, though, and the stock market closed early, at 1 p.m. ET, ahead of the Fourth of July holiday Friday.

Thursday's gains add to what has been a strong month-and-a-half for Wall Street. Along with the Dow hitting 17,000, the Standard & Poor's 500 index is approaching its own milestone of 2,000. The indexes have risen as a steady stream of good news on jobs and manufacturing bolsters investor confidence.

"Right now the story is onward and upward," said Neil Massa, senior trader at John Hancock Asset Management.

The Dow rose 92.02 points, or 0.5 percent, to close at 17,068.26. The S&P 500 rose 10.82 points, or 0.6 percent, to end at 1,985.44 and the Nasdaq composite rose 28.19 points, or 0.6 percent, finishing at 4,485.93.

Investors were encouraged by the latest jobs report from the Department of Labor, which showed U.S. employers added 288,000 workers to their payrolls in June, far more than forecast. The unemployment rate fell to 6.1 percent. The government also said employers hired more people in previous months than reported earlier: 217,000 in May and 304,000 in April. The U.S. economy is now creating around 31,000 jobs each month in 2014, compared to roughly 194,000 a month last year.

"It topped even some of the most optimistic of forecasts," Massa said.

The jobs report is the latest piece of data to show the U.S. economy continues to steadily improve. On Wednesday, the payroll processor ADP said private businesses added 281,000 jobs in June, up from 179,000 in May. Also this week the Institute for Supply Management said the U.S. manufacturing sector expanded for the 13th consecutive month.

While the Dow's passing of 17,000 is a notable milestone, most Wall Street professionals don't focus on it. The vast majority of mutual funds and investors use the broader S&P 500 index as their benchmark for how they are performing. In fact, the Dow has lagged behind the rest of the stock market this year. The index is up 3 percent in 2014 compared with the S&P 500's rise of 7.4 percent.

"That said, investors should be feeling good about Dow 17,000," Scott Wren, a senior equity strategist with Wells Fargo Advisors, wrote in a note to investors. "The stock market has more than recovered from levels seen during the financial crisis more than five years ago. Slow and steady can win the race; and it has."

The Dow's 17,000 milestone is another reminder of its bull market run. The index has climbed more than 10,500 points since its Great Recession low of 6,547.05 on March 9, 2009.

Among individual stocks, the pet supply chain PetSmart rose the most in the S&P 500 on Thursday. PetSmart (PETM) gained $7.48, or 13 percent, to $67.28 after the activist investor firm Jana Partners disclosed a 9.9 percent stake in the company.

Investors sold bonds after the strong jobs report. The yield on the 10-year Treasury note rose to 2.64 percent from 2.63 percent late Wednesday. Bond yields rise when prices fall.

Thursday was the slowest trading day of the year for stocks. Roughly 1.9 billion shares changed hands on the New York Stock Exchange.

U.S. markets will be closed Friday for the Fourth of July holiday. U.S. stock trading will reopen Monday.