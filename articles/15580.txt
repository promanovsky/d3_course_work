About 100 residents of Dorchester and other nearby neighborhoods burst into applause when he entered the room. As Menino made a mockingly bemused face at the waiting array of television cameras, a young boy ran up to hand him a paper heart.

Former Boston mayor Thomas M. Menino, who has been diagnosed with cancer, said he was a “fighter” and would beat his disease, at an appearance today at an indoor farmers’ market in Dorchester’s Codman Square.

Menino’s appearance at the market had been scheduled before news of his illness broke, and was intended to help promote a children’s book by Dorchester author Kathleen Chardavoyne about his mayoralty.

Advertisement

Menino said he looked forward to a healthy life after his cancer treatment. “I’m very confident on this. I’ll be back. ... I believe in myself, I believe in my doctors, and I believe in my faith, and I believe I have the ability to beat it back.”

Asked about the outpouring of support from Boston and beyond, Menino said, “I don’t deserve it. … Who am I? I’m just a guy from Hyde Park.”

Turning more serious, Menino added: “This is a big city, but it’s a small city. We’re a family city. We all know each other. We care about each other. ... When something happens to somebody else, they all rally. Look at what happened with the Marathon bombing and how we rallied then.”

Menino appeared in good spirits, even joking about the children’s book, “Goodbye, Mayor Menino.”

“What an appropriate title that is!” he cracked.

Supporters in attendance approached Menino to offer brief, quiet words of support. Some described family members who had overcome similar diagnoses. He then sat with a group of young children to listen to a reading of the book, giving them high-fives and leaning in to listen intently as a little girl whispered in his ear.

Advertisement

As mayor, Menino’s affinity for children was well-known. This afternoon, the mayor said the only aspect of his diagnosis that concerned him was how his six grandchildren would take the news.

“They misinterpret sometimes the news,” he said. “You have to talk to them about it.”

Asked if he felt sorry for himself because of a series of recent health problems, Menino said emphatically he did not.

“I’m the luckiest guy in America,” he said. “I was lucky to lead this city for 20 years. The health things are a little bump in the road, but we get over them.”

The Globe reported this morning that Menino, who left office in January, had been diagnosed with an advanced form of cancer that has spread to his liver and lymph nodes.