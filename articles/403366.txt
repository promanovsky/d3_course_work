Earlier this week, Dodgers ace Clayton Kershaw appeared on Jimmy Kimmel Live to promote an upcoming charity ping-pong tournament.

On the show, Kimmel challenged Kershaw to show off his outstanding control by knocking an apple off the host’s head with a baseball.

Kershaw matched William Tell, sort of: He knocked the fruit off Kimmel’s head by nailing Kimmel in the face. And now we can watch it over and over again, forever.