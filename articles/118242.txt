The Android 4.4 KitKat software will arrive for Google Glass users as soon as this week.

The XE14 update due in February, was not released. The confirmation came from Glass Guide, Teresa Zazenski, who mentioned that the team was working to move Glass from Ice Cream Sandwich to Android KitKat. The update was expected to make the Glass smoother and better. But Teresa did not provide details about the definite time frame for the update roll-out.

Now Google Glass official G+ account confirms it through a posting that the wearable computer will get Android 4.4 KitKat later this week. The update will be a big one with a number of changes. Besides, it will bring improved battery life.

The KitKat goodies include photo bundles. With the update, user's photos, videos and vignettes from each day will be organised in bundles. Besides, the photo replies in Hangouts, will enable users to send photos in Hangout messages.

The update also adds sending feedback, voice command sorting, video calls and several other features. Check G+ posting below to know more about Android 4.4 update for Google Glass.