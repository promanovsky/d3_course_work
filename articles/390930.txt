Johnson Controls Inc. said its fiscal third-quarter earnings slid 68% as restructuring and other charges offset a modest increase in revenue.

The results, however, were generally in line with expectations.

The manufacturer of car batteries and automotive seating said in May that it plans to spin off its automotive-interiors business with a Chinese partner, a major step for Chief Executive Alex Molinaroli's effort to focus the U.S.-based company on higher-margin non-auto businesses.

The company, which is based in Milwaukee, will create a new company co-owned with Chinese supplier Yanfeng Automotive Trim Systems Co. The U.S. company will retain a 30% interest in the new entity, which would become the world's largest producer of items such as instruments, door panels and other plastic and wood parts for car interiors.

For the quarter ended June 30, Johnson Controls reported a profit of $176 million, or 26 cents a share, down from $550 million, or 80 cents a share, a year earlier. The quarter's results included $162 million in restructuring charges related to the automotive interiors business and $140 million in losses from divested businesses and other transaction-related costs, Johnson Controls said.

Excluding those and other items, earnings from continuing operations were 84 cents a share, up from 72 cents a year earlier. Sales rose 3% to $10.81 billion.

The company had expected earnings from continuing operations of 81 cents to 84 cents, while analysts polled by Thomson Reuters forecast revenue of $10.8 billion.

Automotive sales increased 7% to $5.73 billion, on higher production across regions. Building efficiency sales slipped 4% to $3.58 billion, and power solutions sales rose 6% to $1.51 billion.

For the current quarter, the company expects earnings from continuing operations of $1 a share to $1.02 a share, in line with analysts' forecast for $1.01 a share.

Write to Erin McCarthy at erin.mccarthy@wsj.com

Access Investor Kit for Johnson Controls, Inc.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US4783661071

Subscribe to WSJ: http://online.wsj.com?mod=djnwires