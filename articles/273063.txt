Brisk demand for SUVs and pickups — and five sunny weekends — pushed U.S. auto sales to a nine-year high in May.

Chrysler, Nissan and Toyota all reported double-digit sales gains over last May. Even General Motors, battling bad publicity from a mishandled recall, surprised with a 13 percent increase.

Ford’s sales rose a better-than-expected 3 percent, while Hyundai’s were up 4 percent. Of major automakers, only Volkswagen’s sales fell.

May is traditionally a strong month for the auto industry, as buyers spend their tax returns and think ahead to summer road trips. This year’s calendar, with five weekends, gave it an extra boost. Sales were particularly strong the last weekend of the month, automakers said.