Samsung has expanded its Galaxy range with the introduction of four budget smartphones.

The Galaxy Core II, Galaxy Star 2, Galaxy Ace 4 and Galaxy Young 2 are all powered by the Android 4.4 operating system and will launch at an "affordable price".







Powered by a 1.2GHz quad-core processor, the Galaxy Core II features a 4.5-inch display, a 2,000mAh battery, 5-megapixel rear-facing camera, dual-SIM support and a faux-leather finish.

Both 3G and 4G variants of the Galaxy Ace 4 will be available, with the former packing a 1.0 GHz dual-core chip and 1,500mAh battery and the latter sporting a 1.2 GHz dual-core processor and 1,850mAh battery.

Each includes a 4-inch display and 5-megapixel rear camera.







Samsung's Galaxy Young 2 comes with a 3.5-inch HVGA screen, a 1.0 GHz single-core processor, a 1300mAh battery, and a 3-megapixel rear-facing camera.

Rounding off the budget range, the Galaxy Star 2 features a 3.5-inch HVGA display, a 1.0GHz single-core processor, a 1,300mAh battery, and a 2-megapixel rear-facing camera.

All of the phones will be available in black or white colour options, but specific pricing and release information is yet to be announced.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io