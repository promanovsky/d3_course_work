Whoever let the cat out of the big bag isn't definitely off the legal hook this time. Reports said an ex-Microsoft Corp. employee, who allegedly stole and leaked Windows-related trade secrets to an unidentified tech blogger in France, got slapped with federal criminal charges and held with no bail following his arrest Wednesday.

The Russian employee, identified as software architect Alex Kibkalo, admitted to investigators from Microsoft that he indeed passed on confidential documents and information of the company to the blogger, according to documents from a Seattle federal court.

On the other hand, the blogger was notorious in the Microsoft blogging community as someone who posts screenshots of pre-release version of the Windows Operating System. He concealed his true identity, falsely identifying himself as from Quebec. However, a Microsoft team called as Trustworthy Computing Investigations made attempts to track the blogger's whereabouts.

Court documents show that Kibkalo also bragged about the leak and an internal system that protects against software piracy. To prove wrongdoings, Microsoft investigators searched and found Kibkalo's email within the blogger's Hotmail account, which strongly established the case against him.

Investigators also claimed of a recovery of instant messages exchanges between Kibkalo and the blogger, proving further the case.

"I would leak enterprise today probably," Kibkalo said to the blogger in an Aug. 2, 2012 exchange.

"Hmm," said the blogger. "Are you sure you want to do that? Lol."

Upon being told the leak was "pretty illegal, Kibkalo allegedly responded with, "I know :)"

An individual who requested anonymity from Microsoft alerted the latter of the said case sometime in 2012. Said individual was also the same person contracted by the tech blogger to assist him in examining the code for the Microsoft Activation Server Software Development Kit. The kit is a confidential product developed solely for Microsoft use, according to the official complaint filed in the U.S. District Court for the Western District of Washington on Mar. 17.

An FBI agent taking part in the investigation disclosed that Kibkalo relocated to Russia. Before his arrest, he works for 5nine Software, another U.S.-based technology company that has offices in St. Petersburg and Moscow, as director of product management for security and management products, according to his LinkedIn account.

Asked for comment through a phone message, federal public defender Russell Leonard, who represents Kibkalo in court, could not be reached for any word on the case.

Kibkalo was with Microsoft for seven years. Prior to leaving the company, he received a poor performance review from the company. The complaint also notes that he threatened to resign if the company doesn't amend the review.

A study in February by the Center for Responsible Enterprise & Trade identifies "malicious insiders" as among the five categories of possible misappropriators of trade secrets. Other categories included competitors, states, transnational organized crime and so-called "hactavists."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.