Emma Stone at the Paris premiere of The Amazing Spider-Man 2 last night[WENN]

FREE now for the biggest moments from morning TV SUBSCRIBE Invalid email Sign up fornow for the biggest moments from morning TV When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The 25-year-old arrived at the Paris premiere for her latest film 'The Amazing Spider-Man 2' wearing a raunchy black leather dress by Lanvin. Not only did the strapless number show off Emma's pale complexion and slender frame to perfection, it showed a completely different side to the redhead and her style. The bondage-esque gown featured draped ruching and a thigh high split, as well as a wide belt which cinched in Emma's tiny waist.

The 25-year-old showed off her pale complexion in a black leather dress [SPLASH]

The 'Crazy, Stupid, Love' star teamed the raunchy number with a pair of black patent stilettos that featured bow detailing, and bright red lips - which were a stark contrast to her pale skin. She wore her red hair up in a chic side bun and left some down in a loose wave, which made her look every inch the Hollywood movis star. Of course, she was joined on the red carpet by her co-star and real-llfe boyfriend Andrew Garfield who looked very smart in a dark suit, a white shirt and a skinny tie.

The Lanvin gown featured a ruched top and a thigh-high split [SPLASH]

Emma teamed her strapless dress with black heels and red lips [SPLASH]

Emma was joined at the premiere by her co-star and boyfriend Andrew Garfield [AFP/GETTY]

The couple were joined by their co-stars Jamie Foxx and Dane DeHaan, as well as director Marc Webb, at the premiere which was held at Le Grand Rex. Emma and Andrew have been busy travelling all over the world as they promote the latest Spider-Man installment, and earlier this week found themselves in London. Emma, who is a self-confessed Spice Girls fan, was left shaking when she got the chance to speak to Mel C live on air during an interview on Capital FM.