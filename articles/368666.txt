We have some more rumors about the new iPhone 6, this time it is about the battery on Apple’s next iPhone, and we have details on what size batteries will come with both the 4.7 inch model and the 5.5 inch model of the iPhone 6.

The current iPhone 5S and iPhone 5C smartphones feature a 1,570 mAh battery, as the new iPhones will come with larger displays, both models are said to get larger batteries than the current handsets.

According to a recent report, the 4.7 inch version of the iPhone 6 will come with a battery which is between 1800 mAh and 1900 mAh, the 5.5 version of Apple’s new iPhone is said to feature a 2500 mAh battery.

As the new iPhones will come with larger displays over the existing models, they will need larger batteries to power the handsets, although it looks like Apple has only increase the battery size slightly in the 4.7 inch model.

As yet it is not clear on what these new batteries will translate to in terms of usage on the new iPhone models, both handsets are said to come with a new Apple A8 64-bit mobile processor and more RAM than the current models.

We still do not know for sure what resolution these two displays on the iPhone 6 will come with, we have heard rumors of a 1704 x 960 pixels resolution, possibly for the 4.7 inch model, and also rumors of a QHD resolution of 2560 x 1440 pixels, possibly for the 5.5 inch model.

The new iPhone is expected to launch in September, the handset will come with Apple iOS 8, and the device is rumored to launch on the 19th of September.

Source Gforgames

Image Credit Sonny Dickson

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more