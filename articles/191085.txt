RIYADH: Saudi Arabia’s acting health minister has announced the sacking of the head of a Jeddah hospital where a spike in Mers infections among medical staff sparked panic among the public.

The move, which Adel Faqeeh published on Twitter late Tuesday, came after he inspected King Fahd Hospital’s emergency department in the Red Sea city.

Hours after the news, health officials announced two more deaths from the Middle East Respiratory Syndrome coronavirus, bringing the toll to 117.

The victims were a 68-year-old woman in Jeddah, the kingdom’s commercial capital, and a 60-year-old man died in Madinah.

Four doctors resign

The Jeddah hospital was temporarily shut last month after several medics were infected by Mers.

And panic at the Red Sea city’s main hospital prompted at least four doctors to resign in mid-April after they refused to treat Mers patients for fear of infection.

Nearly a week later, Riyadh dismissed the health minister and appointed Faqeeh, who is labour minister, to take over the health portfolio on an acting basis.

Faqeeh, who has repeatedly promised “transparency” over Mers, said he has replaced the head of the hospital and his assistants. “The new team will immediately take up its duties,” he tweeted, adding that “the ministry will take all decisive measures to achieve its goals in preserving the health of members of society.”

Saudi Arabia has reported 431 infections since Mers first appeared in its eastern region in September 2012 before spreading across the kingdom.

Faqeeh said last week that measures to contain the spread of Mers “will be announced in the coming days,” as Western experts and representatives of the World Health Organisation met in Riyadh.

The WHO said on Wednesday that a team of its experts completed a five-day mission to the kingdom “to assist the national health authorities to assess the recent increase in the number of people infected” in Jeddah.

Experts visited two main hospitals there and have found that the increase in cases are “due to breaches in WHO’s recommended infection prevention and control measures,” the UN organisation said.

Transmission pattern

It said that the recent increase in numbers of infections does not suggest a “significant change in the transmissibility of the virus.”

So far, there is no evidence “of sustained human-to-human transmission in the community and the transmission pattern overall remained unchanged,” said WHO.

“The majority of human-to-human infections occurred in health care facilities,” it said, adding that “one quarter of all cases have been health care workers.” The team called for improving “knowledge and attitudes” of health care workers about Mers.