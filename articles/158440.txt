stunning report published by CNN's Scott Bronstein and Drew Griffin Wednesday night claims "at least 40" veterans died awaiting treatment from the Phoenix Veterans Affairs Health Care system and many of them were on a "secret waiting list." The list was allegedly designed so the Phoenix VA could hide the long wait times veterans experienced at its facilities.

CNN claimed "top management" at the Phoenix VA were aware of the list and even defended its use after complaints from staffers. Now, CNN reported the Phoenix VA is being investigated by both the VA Office of the Inspector General and the U.S. House Veterans Affairs Committee.

Advertisement

CNN based its report on internal emails, interviews with multiple unnamed staffers, and conversations with Dr. Sam Foote, who recently retired after over two decades with the Phoenix VA. Foote estimated there were about 1,400 to 1,600 veterans on the "secret" waiting list . CNN said other unnamed staffers it spoke with backed up his claim. Foote also said staff employed drastic measures to keep the wait times secret including shredding documents and avoiding the creation of standard electronic records.Phoenix VA Director Sharon Helman provided a statement to CNN expressing disappointment about the accusations.

"It is disheartening to hear allegations about Veterans care being compromised," Helman said, adding, "We are open to any collaborative discussion that assists in our goal to continually improve patient care."

Advertisement

The VA subsequently sent another statement to CNN indicating the accusation are being investigated.

"We have conducted robust internal reviews since these allegations surfaced and welcome the results from the Office of Inspector General's review. We take these allegations seriously," the statement said.