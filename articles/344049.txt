Corporations working in Africa will join doctors and policymakers in an effort to fight the largest-ever Ebola outbreak that’s already killed more than 750 people in Guinea, Liberia and Sierra Leone. In the countries affected, mining is a major source of economic growth, which has put multinational companies on the front lines.

“It is important to redouble efforts to have a rapid and coordinated response so as to contain the virus,” Amadou System, Africa economist at the Brookings Institute, said. “The private sector should be a part of it.”

At an emergency World Health Organization meeting set to take place in Accra, Ghana, on Wednesday and Thursday, representatives from mining companies and airlines will join health experts and lawmakers from 11 African countries and dozens of international organizations.

Representatives from Cote d’Ivoire, Democratic Republic of the Congo, Gambia, Ghana, Guinea, Guinea Bissau, Liberia, Mali, Senegal, Sierra Leone and Uganda will attend, as well as representatives from various U.N. agencies, the Centers for Disease Control and Prevention, Department for International Development, European Union, Médecins Sans Frontières and others.

The WHO had already deployed more than 150 expert teams to help control the outbreak, but according to data released on Tuesday, the Ebola virus has taken 467 lives and affected 759 people in Guinea, Liberia and Sierra Leone.

Microbiology professor Robert Garry of the Tulane University School of medicine told NBC news that the current numbers are just "the tip of the iceberg."

A few weeks ago, iron ore miner African Minerals Ltd. (LON:AMI), the biggest contributor to Sierra Leone’s economy, announced it would be randomly testing its workforce to fight the outbreak.

“We are working very, very closely, on a daily basis, with the government of Sierra Leone and the World Health Organization,” African Minerals Executive Chairman and Founder Frank Timis told Bloomberg, adding that they had yet to record a case as of June 17.

Company employees each had their temperature taken before shifts, and a team of medical staff stopped workers at random for checks.

Meanwhile, London Mining PLC (LON:LOND) said earlier in the month that a group of nonessential personnel had already left the country.

In May, metals and mining corporations Rio Tinto (NYSE:RIO) and Chinalco Mining Corp. International (HKG:3668) signed a $20 billion deal for the biggest iron ore and infrastructure ventures on the continent, employing more than 45,000 people in Simandou, Guinea, according to the BBC.

A company representative could not be reached for comment.

Another industry is also playing a major role in keeping Ebola from spreading.

Just a month after the outbreak began, Gambia banned flights from ebola-affected nations. For instance, Brussels Airlines, which often stopped over in Freetown, would only be allowed to drop passengers there, not pick them up, according to Agence France-Presse.

The CDC has issued a series of guidelines for airline officials who interact with arriving passengers, advising them on the best ways to isolate a possibly infected passenger, and protect others on the flight.

Before now, the deadliest Ebola outbreak killed 254 people in Congo just over a decade ago. The current strain of the disease has killed around 60 percent of those infected.