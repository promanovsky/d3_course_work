IBM CEO Ginni Rometty with Apple CEO Tim Cook. Courtesy of Apple/Paul Sakuma

Apple announced Tuesday a partnership with IBM that will help the company greatly increase the presence of its mobile devices in the enterprise.

In short, the partnership will play on the strengths of both companies. IBM will use its sales force to sell iPhones and iPads to its network of business customers. IBM will also develop cloud services optimized for iOS, the operating system for iPhones and iPads.

Apple will provide hardware support for devices through a special AppleCare program designed for big businesses buying Apple gadgets in bulk. It includes benefits like 24/7 assistance.

The iPhones and iPads sold through the program will ship with IBM's MobileFirst platform, which includes a lot of productivity tools big companies need.

While the IBM-Apple partnership may not seem shocking today, it's pretty crazy considering the two companies were mortal enemies some 30 years ago. Both competed directly against each other in the budding PC market.

For example, Steve Jobs famously flipped off IBM's logo in this iconic photo.

But times have changed, and in interview with CNBC Apple CEO Tim Cook said it was time to abandon the animosity that existed between the two companies for so long.

"We've come from 30 years ago being competitors to today being incredibly complementary and I think that the people that will really benefit from this are the enterprise customers who can be more productive running their businesses," said Cook.

It's clear Apple realized it needed to do something big to expand in the enterprise.

"The reality is, that the penetration of these businesses and in commercial in general for mobility is still low," Cook told CNBC. "So where we have very good market share the penetration suggests there's a huge opportunity here."

Not everyone shares Cook's optimism, though. Apple analyst Gene Munster noted that the company has already penetrated most large businesses, both in the U.S. and abroad.

"We do not expect the IBM partnership to have a meaningful impact on Apple's financials overall primarily based on our belief that large corporations are already utilizing iPhones," said Munster. "We expect IBM to eventually offer similar solutions on Android over time, thus we believe Apple's innovation on the OS and hardware remain the most critical factors in device sales."

Here's the full press release from Apple and IBM:

Apple and IBM today announced an exclusive partnership that teams the market-leading strengths of each company to transform enterprise mobility through a new class of business apps—bringing IBM’s big data and analytics capabilities to iPhone and iPad.

The landmark partnership aims to redefine the way work will get done, address key industry mobility challenges and spark true mobile-led business change—grounded in four core capabilities:

• a new class of more than 100 industry-specific enterprise solutions including native apps, developed exclusively from the ground up, for iPhone and iPad;

• unique IBM cloud services optimized for iOS, including device management, security, analytics and mobile integration;

• new AppleCare service and support offering tailored to the needs of the enterprise; and

• new packaged offerings from IBM for device activation, supply and management.

The new IBM MobileFirst for iOS solutions will be built in an exclusive collaboration that draws on the distinct strengths of each company: IBM’s big data and analytics capabilities, with the power of more than 100,000 IBM industry and domain consultants and software developers behind it, fused with Apple’s legendary consumer experience, hardware and software integration and developer platform. The combination will create apps that can transform specific aspects of how businesses and employees work using iPhone and iPad, allowing companies to achieve new levels of efficiency, effectiveness and customer satisfaction—faster and easier than ever before.

As part of the exclusive IBM MobileFirst for iOS agreement, IBM will also sell iPhones and iPads with the industry-specific solutions to business clients worldwide.

“iPhone and iPad are the best mobile devices in the world and have transformed the way people work with over 98 percent of the Fortune 500 and over 92 percent of the Global 500 using iOS devices in their business today,” said Tim Cook, Apple’s CEO. “For the first time ever we’re putting IBM’s renowned big data analytics at iOS users’ fingertips, which opens up a large market opportunity for Apple. This is a radical step for enterprise and something that only Apple and IBM can deliver.”

“Mobility—combined with the phenomena of data and cloud—is transforming business and our industry in historic ways, allowing people to re-imagine work, industries and professions,” said Ginni Rometty, IBM Chairman, President and CEO. “This alliance with Apple will build on our momentum in bringing these innovations to our clients globally, and leverages IBM’s leadership in analytics, cloud, software and services. We are delighted to be teaming with Apple, whose innovations have transformed our lives in ways we take for granted, but can’t imagine living without. Our alliance will bring the same kind of transformation to the way people work, industries operate and companies perform.”

Apple and IBM’s shared vision for this partnership is to put in the hands of business professionals everywhere the unique capabilities of iPads and iPhones with a company’s knowledge, data, analytics and workflows. Specifically, the two companies are working together to deliver the essential elements of enterprise mobile solutions:

- Mobile solutions that transform business: The companies will collaborate to build IBM MobileFirst for iOS Solutions—a new class of “made-for-business apps” targeting specific industry issues or opportunities in retail, healthcare, banking, travel and transportation, telecommunications and insurance, among others, that will become available starting this fall and into 2015.

- Mobile platform: The IBM MobileFirst Platform for iOS will deliver the services required for an end-to-end enterprise capability, from analytics, workflow and cloud storage, to fleet-scale device management, security and integration. Enhanced mobile management includes a private app catalog, data and transaction security services, and productivity suite for all IBM MobileFirst for iOS solutions. In addition to on-premise software solutions, all these services will be available on Bluemix—IBM’s development platform on the IBM Cloud Marketplace.

- Mobile service and support: AppleCare for Enterprise will provide IT departments and end users with 24/7 assistance from Apple’s award-winning customer support group, with on-site service delivered by IBM.

- Packaged service offerings: IBM is introducing IBM MobileFirst Supply and Management for device supply, activation and management services for iPhone and iPad, with leasing options.

Announced at Apple’s Worldwide Developer Conference in June and available later this year, Apple’s iOS 8 is the biggest release since the launch of the App Store℠, giving users incredible new features and developers the tools to create amazing new apps. For enterprise, iOS 8 builds on the new IT model for a mobilized workforce by improving the way users are informed of how their devices are configured, managed or restricted, with expanded security, management and productivity features.

Apple designs Macs, along with OS X, iLife, iWork and professional software. Apple leads the digital music revolution with its iPods and iTunes online store. Apple has reinvented the mobile phone with its revolutionary iPhone and App Store, and is defining the future of mobile media and computing devices with iPad.

IBM’s 5,000 mobile experts have been at the forefront of mobile enterprise innovation. IBM has secured more than 4,300 patents in mobile, social and security, that have been incorporated into IBM MobileFirst solutions that enable enterprise clients to radically streamline and accelerate mobile adoption, help organizations engage more people and capture new markets.

IBM has made a dozen acquisitions in security in the past decade, has more than 6,000 security researchers and developers in its 25 security labs worldwide that work on developing enterprise-class solutions.

IBM has also established the world’s deepest portfolio in Big Data and Analytics consulting and technology expertise based on experiences drawn from more than 40,000 data and analytics client engagements. This analytics portfolio spans research and development, solutions, software and hardware, and includes more than 15,000 analytics consultants, 4,000 analytics patents, 6,000 industry solution business partners, and 400 IBM mathematicians who are helping clients use big data to transform their organizations.