By Rachael Rettner, Senior Writer

Published: 07/30/2014 08:41 AM EDT on LiveScience

French toast served with butter syrup and bacon, a platter of fried seafood and hush puppies, and a cheesecake made with Reese’s peanut butter cups and fudge are among the meals singled out this year for their shockingly unhealthy nutrition content in a new report from a nonprofit watchdog group.

Nine meals from American chain restaurants were selected for the 2014 Xtreme Eating Awards, a list released yearly by the Center for Science in the Public Interest as a way to highlight meals that are very high in calories, saturated fat, sodium and sugar.

Nearly all the meals on this year's list have at least 2,000 calories, and a few top 3,000 calories. "You could take half home and still overeat," the CSPI says.

Three of the meals on the list are from the Cheesecake Factory, including the restaurant's Bruléed French Toast, which consists of custard-soaked bread, powdered sugar, maple-butter syrup and bacon, and contains more than 2,700 calories, CSPI says. The meal also has 93 grams of saturated fat, which is nearly five days' worth, CSPI says. It would take seven hours of swimming laps to burn this meal off, the group says. [9 Snack Foods: Healthy or Not?]

Other "winners" include:

A meal from Red Robin Gourmet Burgers that includes the A.1. Peppercorn burger (with bacon), a "Monster" Salted Caramel Milkshake and an order of steak fries, which contains 3,540 calories — the most of any meal on the list. That meal also contains 69 grams of saturated fat and 6,280 milligrams of sodium. You would need to take a brisk 12-hour walk to burn it off.

The Big "Hook" Up platter from Joe's Crab Shack, which comes with seafood and crab balls with cream cheese, jalapeños and panko breadcrumbs, beer-battered fish and chips, two kinds of shrimp, and hush puppies (fried cornmeal batter). The meal contains 3,280 calories, 50 grams of saturated fat, and 7,610 milligrams of sodium, and would take 11 hours of golf (without a caddie) to burn off, CSPI says.

The Signature Deep Dish Chicken Bacon Ranch Pizza from BJ’s Restaurant and Brewhouse — the size small (9 inches) has 2,160 calories and is about the same as eating three Pizza Hut Personal Pan Pepperoni Pizzas, CSPI says. You would need to bike for 5.5 hours to burn this meal off, the group says.

The Cheesecake Factory's Reese’s Peanut Butter Chocolate Cake Cheesecake, which contains the famous peanut butter cups along with fudge cake and caramel, and has 1,500 calories, CSPI says. It would take a 4.5-hour aerobic session to burn it off, the group says.

The Prime New York Steak, Contadina style from Maggiano’s Little Italy, which comes with a steak and two Italian sausage links, and contains 2,420 calories. It would take 7.5 hours on a rowing machine to burn it off, CSPI says.

"When French toast is ‘Bruléed,’ fries are 'bottomless,' and steaks are now garnished with not just one, but two Italian sausages, it’s clear that caloric extremism still rules the roost at many of America’s chain restaurants," said Paige Einstein, a dietitian at CSPI.

To help people avoid overeating while dining out, CSPI recommends the following, when possible: Order from the lower-calorie menu if one is offered; take home half of a regular meal; order alternative sides, such as steamed broccoli, in place of fries; order a burger with lettuce in place of the bun; order a thin-crust pizza instead of a deep-dish pizza; for Mexican dishes, order items à la carte instead of as a combo meal; and ask for no chips.

Follow Rachael Rettner @RachaelRettner. Follow Live Science @livescience, Facebook & Google+. Original article on Live Science.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>