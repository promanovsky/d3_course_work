General Motors Co. (GM) said Wednesday that it has notified Chevrolet dealers in the United States and Canada to stop selling new or used 2013-2014 Chevrolet Cruze vehicles until further notice because some of the vehicles may be equipped with a suspect driver's air-bag inflator module that may have been assembled with an incorrect part.

"We are working diligently with the supplier of the defective part to identify specific vehicles affected and expect to resume deliveries by the end of this week once those vehicles are identified," GM said in a statement.

The Wall Street Journal, citing people familiar with the situation, said earlier Wednesday that a fund set up by GM to compensate victims of crashes linked to defective ignition switches could offer payments for anyone injured or killed in a crash in which the car's air bags did not deploy.

Compensation expert Kenneth Feinberg, who will administer the fund, is expected to outline details of the victims' fund as early as next week, the Journal said.

GM shares closed Wednesday's regular trading session at $37.09, up 51 cents or 1.39%, but lost 21 cents or 0.57% in after hours trading.

For comments and feedback contact: editorial@rttnews.com

Business News