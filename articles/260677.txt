Google plans to build and launch onto city streets a small fleet of subcompact cars that could operate without a person at the wheel.

Actually, the cars wouldn't even have a wheel. Or gas and brake pedals. The company says the vehicles will use sensors and computing power, with no human needed.

Google Inc. hopes that by this time next year, 100 of the two-seaters will be on public roads, following extensive testing. The cars would not be for sale and instead would be provided to select operators for further tweaking and have limitations such as a 40-kilometre-per-hour top speed.

The announcement presents a challenge to automakers that have been more cautious about introducing fully automated driving and to government regulators who are scrambling to accommodate self-driving cars on public roads. Other companies are working on the technology but none as large as Google has said it intends to put such cars in the hands of the public so soon.

Chris Urmson, director of Google's Self-Driving Car Project, stands in front of a self-driving car at the Computer History Museum after a presentation in Mountain View, California earlier this month. (REUTERS)

To date, Google has driven hundreds of thousands of miles on public roads and freeways in Lexus SUVs and Toyota Priuses outfitted with special sensors and cameras. But with a "safety driver" in the front seat, those vehicles were not truly self-driving.

Instead of the standard controls, the prototypes would have buttons to begin and end the drive. Passengers would set a destination. The car would then make turns and react to other vehicles and pedestrians based on computer programs that predict what others might do, and data from sensors including radar and cameras that read in real time what other objects are actually doing.

The route might be set by typing a destination into a map or using spoken commands, Chris Urmson, the leader of Google's self-driving car team, told reporters Wednesday.

The car will be powered by electricity and could go about 160 kilometres before charging. Its shape suggests a rounded-out Volkswagen Beetle — something that might move people around a corporate campus or congested downtown — with headlights and sensors arrayed to resemble a friendly face.

Competitors aiming for 2020

Major automakers have steadily introduced technology that helps cars stay in their lanes and avoid accidents. However, all those vehicles come with a steering wheel and pedals — and the expectation that a driver will jump in should trouble arise. Several companies have said they expect by 2020 to market vehicles that can drive themselves under certain conditions.

Google's driverless car will have a top speed of 40 km/h. (Stephen Lam/Reuters)

"Nothing is going to change overnight, but (Google's announcement) is another sign of the drastic shifts in automotive technology, business practices and retailing we're going to witness," said Karl Brauer, a senior analyst at Kelley Blue Book's KBB.com.

A French company, Induct Technology, has produced a driverless shuttle, which in February drove people around a hospital campus in South Carolina. But in terms of a truly self-driving car from a major company, Google looks to be first.

The tech titan began developing the prototype more than a year ago after it loaned some employees its retrofitted Lexuses and saw that some "would basically trust the technology more than it was ready to be trusted," Urmson said. Making a car that drives itself seemed more practical than somehow ensuring that people zoning out behind the wheel could take over at a moment's notice.

The first 100 prototypes will be built in the Detroit area with the help of firms that specialize in autos, Google said. It would not identify those firms or discuss the cost of each prototype.

This summer, Google plans to send test prototypes on closed courses, then later this year on public streets. Those test cars will have a wheel and pedals because under California law a test driver must be able to take immediate control.

By summer 2015, however, California's Department of Motor Vehicles must publish regulations allowing the public to use truly driverless cars. Big questions the DMV is wrestling with include who is liable if a driverless car crashes and how the state can be confident that an automated car drives at least as safely as a person.

That change in the law would allow the 100 prototypes that would be intended for a public "pilot project"— details to be determined — to not have a steering wheel or pedals.

Though next year is the goal for the pilot project, Urmson said public access "won't happen until we're confident in the safety."