Alcohol kills 3.3 million people worldwide each year, more than AIDS, tuberculosis and violence combined, the World Health Organisation said on Monday, warning that booze consumption was on the rise.

Including drink driving, alcohol-induced violence and abuse, and a multitude of diseases and disorders, alcohol causes one in 20 deaths globally every year, the UN health agency said.

Alcohol causes one in 20 deaths globally every year, according to the World Health Organisation. Credit: Arsineh Houspian

"This actually translates into one death every 10 seconds," Shekhar Saxena, who heads the WHO's Mental Health and Substance Abuse department, told reporters in Geneva.

Alcohol caused some 3.3 million deaths in 2012, WHO said, equivalent to 5.9 per cent of global deaths (7.6 per cent for men and 4.0 per cent for women).