New figures show that malignant melanoma, the most dangerous form of skin cancer, is now five times more common than it was in the 1970s.

Forty years ago skin cancer affected just three people in every 100,000.

Cancer Research UK, which published the statistics, say the dramatic rise can, in part, be attributed to holidays in hot climates becoming more affordable and an increase in the use of sunbeds.

Amanda Crosland, who survived skin cancer, said she got sunburnt on her leg as a child.