Today is PMI day with both the euro area and the US releasing Flash PMI for May.



In the euro area we look for a further small increase as the recovery gradually strengthens. Yesterday euro consumer confidence hit a new seven-year high. Some indicators have softened a bit recently (ZEW, OECD leading indicator), which may indicate some downside risk, but overall PMI should still indicate quarterly GDP growth in the 0.4-0.5% area.



In the US we look for a broadly flat reading for Markit PMI at 55.5 (prev 55.4). US jobless claims will also be interesting after they hit a new cycle low last week, indicating a strengthening labour market.



Finally, US existing home sales should show an increase as pending home sales picked up last month, which may be a sign of a bottoming in the US housing slowdown following the sharp rise in mortgage rates last year. Mortgage rates have come down again and this should soon feed into a new recovery in US housing as may already be indicated by strong housing permits last month.

To Read the Entire Report Please Click on the pdf File Below