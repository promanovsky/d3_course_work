BBC

A thrill went down my spine when I watched the all-too-short teaser trailer for the August 23 return of I've been checking out fan photos taken from filming locations; assessing new Doctor Peter Capaldi's outfit (sweet!); and poring over speculation on possible villains. I can't freakin' wait for the premiere of the new season.

Everybody's favorite Time Lord isn't the only attention-grabber in TV land. News just broke about Season 4 of the BBC's hit "Sherlock." A special and three new episodes are scheduled to air, with shooting set to start in January 2015. That's a long way off, but it's better than just hanging around wondering when Benedict Cumberbatch and his sidekick Martin Freeman will ever get back to solving mysteries.

Another big story caught fans' attention this week. It was revealed that "Community" has been saved from the pit of network cancellation and will get a sixth season on Yahoo. That's a huge relief for the show's avid fanbase, which was starting to sweat over the show finding a suitable landing spot after leaving NBC.

Keeping with the trend of comic-book adaptations for television, many fans are getting all a-twitter about the October 24 premiere date for the fresh series "Constantine," based on the Hellblazer series from DC Comics. The official trailer dropped this spring and was full of themes like demon battles and damnation to hell. Here's hoping the NBC production truly embraces the dark feel of the comics.

It's a good time to be a television-viewing geek. We have plenty of binge-watching fodder ahead, but if you had to chose one series that makes the hairs on the back of your neck stand up, which would it be? Vote in our poll and talk it up in the comments.