ROLF HARRIS GUILTY: Judge warns paedophile star that jail is 'inevitable' after jury convict him of 12 charges of indecent assault

The 84-year-old Australian abused four women between 1968 and 1986

Victims included a seven-year-old autograph hunter and daughter's friend

Entertainer remained impassive as the unanimous verdicts were returned



Harris told by judge that jail is 'inevitable' and will pass sentence on Friday

Police and CPS say he used his celebrity status to abuse, often in plain sight

Six other women claim they were abused but not included in prosecution



NSPCC has had 28 calls from 13 people who claim they were also abused

Harris set to be stripped of his Bafta fellowship and his CBE from the Queen



Police mugshot: 84-year-old Rolf Harris is facing a lengthy jail term after he was found guilty of using his fame to abuse four victims, including two children, over 18 years

Rolf Harris has today been found guilty of 12 sex attacks on young girls, including his daughter's best friend who suffered a 16-year campaign of abuse from the age of 13.

A seven-year-old autograph hunter was also attacked by the 84-year-old entertainer, who was told a jail sentence is 'inevitable' after he was convicted of sex attacks on four victims between 1968 and 1986.

The paedophile remained impassive as the jury returned unanimous guilty verdicts and Harris then raised his eyebrows to his wife Alwen as he was led from the dock.



Harris was revealed as a 'Jekyll and Hyde' character who had a dark side to his personality and used his fame to 'mesmerise' underage fans before abusing them with impunity.

'He committed many offences in plain sight of people as he thought his celebrity status placed him above the law', Scotland Yard said today.



As the 12 guilty verdicts were read his daughter Bindi held hands with a fellow supporter, and later she sobbed in the court corridor as her mother Alwen and aunt Jenny waited for her father.

Rolf Harris then left court clutching the hands of his daughter and wife but refused to apologise to his victims.



The star was swept away in a limousine and will be sentenced on Friday.



Justice Sweeney said when giving him bail this afternoon: 'He will understand that in doing that I'm giving no indication whatsoever as to the nature of the sentence.



'In reality, given the conviction on all 12 counts it's inevitable that the type of sentence that is upper most in the court's mind is a custodial sentence and he must understand that.'

Scroll down for video

Facing jail: Despite his conviction for a string of child sex offences today Rolf Harris' wife Alwen, left, and daughter Bindi, right, held hands with him as he walked out of Southwark Crown Court

Impassive: Harris did not react as he was found guilty of abusing four victims aged between seven and 19

Verdicts: Harris looked towards his family as he was found guilty, raising his eyebrows at his wife, but showed no other expression even when he was told he faced jail

He was convicted by a jury of six men and six women who deliberated for 38 hours after a two month trial that laid bare his child abuse.

Harris had claimed he was simply a warm, tactile family man and the victim of lies from four women.

But he carefully concealed the 'demon lurking beneath his charming exterior' to maintain his reputation as a national treasure during his glittering 60-year career.

Warning: Rolf Harris, pictured leaving court, was told it is 'inevitable' that he will be jailed after being found guilty of abusing four women, some when they were children

In addition to the four women who were attacked, six more women came forward to claim that he had groped them but their evidence was not allowed to be used as part of the prosecution.



The entertainer and artist will now be stripped of the BAFTA Fellowship he was awarded two years ago, and could lose his CBE from the Queen after being found guilty.



Speaking outside court, DCI Mick Orchard from Scotland Yard said: 'Rolf Harris has habitually denied any wrong doing forcing his victims to recount their ordeal in public.



'He committed many offences in plain sight of people as he thought his celebrity status placed him above the law.



'I want to thank the women who came forward for their bravery, I hope today's guilty verdict will give them closure and help them to begin to move on with their lives'.

Jenny Hopkins, Deputy Chief Crown Prosecutor for the CPS in London, said: 'Rolf Harris used his status and position as a world famous children's entertainer to sexually assault young girls over a period spanning 18 years.



'The victims in this case have suffered in silence for many years and have only recently found the courage to come forward. I would like to pay tribute to the bravery they displayed in coming to court and giving evidence. That bravery and determination has seen Rolf Harris brought to justice and held to account.



'Each victim, unknown to the others, described a similar pattern of behaviour; that of a man acting without fear of the consequences'.

Once seen by a UK audience as a national treasure, Harris had enjoyed years of success, netting him a multi-million pound fortune and the chance to paint the Queen.



But Harris' reputation for groping was so widespread that he was dubbed 'The Octopus' in TV circles in Australia.



Disgraced: After the verdicts were read Harris raised his eyebrows to his wife Alwen, who was sat watching in the public gallery with his daughter Bindi

Guilty: Rolf Harris, 84, was found guilty of 12 counts of indecent assault today, pictured leaving court with his daughter Bindi (left), wife Alwen(right) and niece Jenny Harris (far right)

Lies: Harris had claimed that he was the victim of malicious lies, and was being punished for his infidelity, but the jury believed the women who said he abused them

Scrum: Harris scrambles into a waiting limousine and will return to court on Friday where he will be sentenced for his sex crimes

He arrogantly assumed he was 'untouchable' and his 'dark side' would never be exposed.



Harris faced 12 charges of indecent assault between 1968 and 1986, seven of which related to the best friend of his only child, Bindi, now 50.



He began grooming the girl when she was 13, treating her like 'a young puppy' after first molesting her on a family holiday to Hawaii and Australia in 1978.



Harris continued to target her when they returned to Britain, before 'psychologically dominating her into womanhood'.



She was 'dehumanised' during a 16-year campaign of abuse, which transformed her from a 'care-free child' to an 'emotionally scarred adult', and sparked her descent into depression and chronic alcoholism.

Disgraced: Harris, pictured with the Queen and Kylie Minogue backstage at The Diamond Jubilee Concert in 2012, will now probably lose many of the honours he received including his CBE

Manipulative: The millionaire TV personality - pictured in 2000 (left) and in October 1970 (right) as star of The Rolf Harris Show - 'exploited the very children who were drawn to him'

'THEY CALLED HIM THE OCTOPUS': SIX MORE WOMEN CAME FORWARD TO SAY THEY WERE ABUSED

During the trial, the court also heard from six other witnesses who claimed they had been groped by Harris, but were not part of the criminal charges.

The first claimed she was 11 or 12 when she was off sick from school at a family friend's home in 1969, when Harris told her 'I want to be the first person to introduce you to a tongue kiss'.

He then allegedly got her in 'a gentle hug' before sticking his tongue into her mouth.

A second, then aged 16 or 17, was waitressing at an event in New Zealand in 1970 when she claimed the entertainer put his hand on her bottom and between her legs.

She said: 'I saw the dark side of a man who I thought could be trusted.'

The third supporting witness was aged 18 when she was on holiday in Malta in 1970 when her boyfriend cut his foot while swimming in the sea and Harris helped them to find a doctor.

She claimed that after she went back to thank the artist, he pinned her up against the wall in a back room in a bar, kissing and groping her.

Jurors also heard from a make-up artist who claimed Harris had groped her more than 'two dozen' times in a single day.

The woman, then in her 20s, told the court that she later found out Harris's nickname was 'the octopus'. A mother and daughter claimed that Harris targeted them both on the same day when they met him at a promotional event at a shop in Australia in 1991.

It was alleged that he first groped the 15-year-old daughter after insisting on giving her a hug, and then rubbed himself against her mother's bottom as they had their photograph taken.

When the older woman challenged him, calling Harris 'a disgusting creature', he is claimed to have said: 'She liked it', referring to her daughter.

The 'Tie Me Kangaroo Down, Sport' singer wrote a letter to her father begging for forgiveness after the sordid details emerged in 1997.



But he doggedly protested his innocence, insisting she 'willingly' took part in a consensual relationship after 'seducing' him when she was an adult.



And Harris desperately tried to wriggle out of trouble by telling her brother: 'It takes two to tango'.

Prosecutor Sasha Wass QC described her as a 'damaged and emotionally dead creature', adding: 'He used her for his sexual gratification like she was a blow up doll.



'She had been psychologically destroyed and trained to perform like a pet.'



The cartoonist's other victims included a seven-year-old autograph hunter he targeted on stage after performing his hit 'Two Little Boys' in 1969.



He also fondled a teenage waitress after crouching on all fours and barking at a dog during filming of ITV show Star Games in Cambridge in 1978.



Harris was adamant he had never visited the university city until four years ago - but was outed as a 'deliberate liar' after bombshell footage of the programme, unearthed during his evidence, proved otherwise.



He also pounced on child actress Tonya Lee - who has waived her anonymity - twice in a London pub during a theatre tour of Britain in 1986, when she was 15.



But the octogenarian, who gave the world the 'wobble board', blasted his accusers as 'liars', insisting their claims 'never happened'.



He conceded being 'touchy feely' but slammed suggestions of sexual abuse.

Didgeridoo-playing Harris called a string of defence character witnesses including Bindi - who panned the accusations of her former childhood pal as 'laughable'.



But she admitted feeling suicidal after learning of her father's 'deviant' sexual behaviour.



Harris' downfall was part of millions of British childhoods came today, as Harris became the biggest scalp claimed by detectives from high profile sex crime investigation Operation Yewtree.

Dozens more alleged victims have come forward during the trial, including several in Australia, and Scotland Yard has been in touch with their counterparts in the Australian police, but it is not yet clear whether they are pursuing any investigation in Harris's home country.



The NSPCC said it has received 28 calls relating to Harris to date, involving 13 people who claim they fell prey to the performer.

The bombshell video evidence that proved Rolf Harris was lying about his abuse



Footage of Rolf Harris rowing topless in a rubber dinghy in Cambridge al m ost forty years ago turned his trial.

The star was accused of molesting a teenager while filming a celebrity It’s a Knockout-type show in Cambridge in 1975 – but claimed he did not visit the university city until 'a couple of years ago, for an art exhibition of my paintings'.

But his credibility was irreparably damaged after jurors were shown footage of Harris taking part in a Thames TV show called Star Games filmed at Jesus Green in Cambridge in 1978.

The Australian could be seen ‘jumping up and down like a kangaroo and mucking about’.

Bombshell: The jury were shown this photograph of Rolf Harris taking part in the ITV programme, Star Games filmed in 1978 in Cambridge. He denied ever having been there but admitted the footage meant her 'remembered it all' but his credibility was badly damaged

Prosecutor Sasha Wass QC put to Harris that the event was the one that the victim had described.

She said: ‘That video supports pretty much everything that (the victim) said apart from the year, she has got the year wrong?’

He replied: ‘By three years, yes, she is out by three years.’

Miss Wass said: ‘But when you told the jury with such confidence last week on Tuesday that you had never been to Cambridge until four years ago, that was a deliberate lie, wasn't it?’

He replied: ‘No, it wasn't. I didn't find out that it was in Cambridge until I saw the video played back and then at the very opening the voiceover introduced it over what I remember was a helicopter shot of the field.

‘That was the first time I had heard the word Cambridge.’

The veteran entertainer told jurors: ‘I had no idea. I don't think any of us knew.’

Miss Wass asked: ‘Nobody knew they were in Cambridge?’

Harris replied: ‘None of the performers, none of the stars knew.’

Footage shown to the jury in court showed Rolf Harris taking part in the celebrity show Star Games on ITV in 1978, where her was ‘jumping up and down like a kangaroo and mucking about amusingly’

During the opening credits of the Star Games final, a voiceover said the show was coming from ‘Cambridge, a tranquil seat of learning.’

Harris was announced by host Michael Aspel as captain of one of the celebrity teams, which also included actors Colin Baker, Rula Lenska, Julian Holloway and Robin Asquith.

Miss Wass told Harris: ‘Michael Aspel seemed to know where he was because he introduced it as being on Jesus Green, Cambridge.’



Harris told Miss Wass she ‘didn’t understand the showbiz scene’, saying he was often driven from place to place without knowing the location.



The alleged victim was working part-time to earn pocket money and was clearing up when she saw Harris entertaining crowds while pretending to be a dog.

But she ‘froze’ as he got to his feet and allegedly ran his hands up and down her as he hugged her.

Miss Wass told Harris: ‘The footage shows you monkeying around pretending to be an animal – all of which fits the description [the alleged victim] said she saw prior to being assaulted by you.

Miss Wass asked: ‘Are you saying that you entirely forgot that event?’

He said: ‘I did until I saw the video and then I remembered it all.’

Putting it to Harris that he had suggested that the victim lied, Miss Wass said: ‘The film footage ... demonstrated it’s not the victims who have lied, it’s you who lied.'

Rolf Harris got a 'thrill' from abusing his daughter's best friend while she slept nearby in 16 years of attacks that left his victim a suicidal alcoholic

Rolf Harris abused his daughter Bindi's best friend and even attacked her when she was feet away because it 'gave him a thrill'.



The entertainer first abused her at 13 on a dream holiday to Hawaii and Australia in 1978 when she walked out of the shower to find Harris waiting for her.



On the same holiday the girl said he touched her as she walked from the sea, close to where his daughter and wife were on the beach.

She told the court how after 16 years of attacks she 'felt dead' and had become a depressed and suicidal alcoholic.



Family: Bindi Harris helps her father into a car after he was found guilty of 12 sex attacks, including one while she slept in the same room

Harris continued to indecently assault her ‘whenever the opportunity arose’ and even told the court he had found her attractive when she wore a bikini as a child.

ROLF HARRIS TRIAL CHARGE SHEET

Count 1 : Indecent assault between 5/4/80 and 4/4/81 on girl aged 15

Count 2 : Indecent assault between 5/4/80 and 4/4/81 on same girl, 15

Count 3 : Indecent assault between 5/4/80 and 4/4/81 on same girl, 15 Count 4 : Indecent assault between 5/4/80 and 4/4/81 on same girl, 15

Count 5 : Indecent assault between 5/4/80 and 4/4/81 on same girl, 15

Count 6 : Indecent assault between 5/4/80 and 4/4/81 on same girl, 15

Count 7 : Indecent assault between 1/1/84 and 1/1/85 on same girl, then aged 19

Count 8 : Indecent assault on 31/5/86 on second girl, aged 14

Count 9 : Indecent assault on same girl, 14

Count 10 : Indecent assault on 31/5/86 on same girl, 14

Count 11 : Indecent assault between 1/1/68 and 1/1/70 on third girl, aged 7-8

Count 12 : Indecent assault on fourth girl, 14, between 1/1/75 and 1/1/76

VERDICTS: ALL GUiLTY



The abuse continued when they returned home and the entertainer even molested her ‘like a toy’ while his 14-year-old daughter Bindi slept in the same bedroom.



Trapped, the victim, who cannot be named started drinking ‘a shed load of gin’ at the age of 14 and by 20 she was an alcoholic.



The woman, now 49, told doctors Harris had treated her like a ‘young puppy’.

The assaults went on for 16 years, transforming a happy child from a loving family into a binge-drinking teenager who had blackouts and memory loss, jurors were told.



Harris claimed she was 18 when they had an affair.

On one occasion Harris got his penis out in front of her, she said. 'It was small, very, very small, and he started fondling it.'

The woman said after a few years of sexual abuse from Harris: 'I just felt dead.'

She said she never wanted him sexually, she said, adding: 'I did say no on a few occasions. But he'd just carry on.'



By the age of 19 she was totally controlled by Harris who used her for his own sexual gratification, Miss Wass said. ‘He never had a meaningful conversation with her. She was just his little toy.’

On one occasion when she was 22 she accepted a lift from a village in Berkshire to London from the artist, and claimed he pulled over in his red Mercedes so that she could perform a sex act on him.



He later put his hand down her trousers in front of a young boy who noticed what had happened, but Harris simply denied it and 'carried on as normal', she said.



The woman also said in one incident at Bindi's Devon home, Harris walked into her bedroom naked and got into bed with her, before asking her to perform a sex act.



When she left home, Harris sent a 'vulgar' postcard to her Norfolk flat saying 'all I've learnt in life I've learnt from my dog,' the court heard.



It included the phrase: 'a cold nose in the crotch is effective.'



It also said: 'If it's not wet and sloppy it's not a real kiss.'



The jury was shown this card Rolf Harris sent to the victim. The message includes the words: 'when it comes to having sex, if at first you don't succeed, beg... If it's not wet and sloppy, it's not a real kiss'

Inside: The note shown to Southwark Crown Court featured a 'Rolfaroo' drawing and was a number of pieces of evidence that helped convict him

She was then attacked when she was 28, and was attacked when she invited Harris to her flat to discuss his daughter, who was 'having some issues with her father'.



'They weren't getting on. I thought I could talk to Rolf about it... I just thought he would listen to what I had to say about Bindi and how she felt,' she told the court.



'We went into the sitting room and I tried talking to him about Bindi and how unhappy she was but he didn't want to know, he just said, "where's the bedroom?"'.



She said Harris had got undressed and undressed her, and forced her into a sex act.



Outlining another incident, the woman said she went to see Harris perform in a pantomime when she was about 29.



She was taken to his dressing room before the show, where he tried to put his hand down her trousers, jurors heard. She wriggled away and sat by the door, but he came over and forced her into a sex act.

After 16 years of abuse she invited him to her parents' home in Norfolk to confront him.



'I was angry, but confident that I could deal with him and felt that I was strong enough to approach him,' she said.



The victim told the court that during the meeting: 'He was sorry for what he had done.'

She told him 'that he caused me misery' and she had feared him as a child and was still scared of him.

'I said that he had ruined my life. He said, "I didn't realise",' she said. 'He thought that we were friends, and I said I'm not your friend.'



Generations of children in the 1960s, '70s, '80s and '90s knew Harris (pictured in 2004) as a talented artist but he will now be stripped of his honours

She then began hitting him and dragged him through the streets berating him. She then told her family about the abuse in 1996, prompting Harris to write to her father.



In the bombshell letter he had begged for forgiveness but despite his apology for the damage he caused he maintained he never abused her.

He admitted he was sickened and consumed with self-loathing about their sexual relationship. He apologised for harming the girl.

He wrote: ‘I have been in a state of abject self loathing. How we delude ourselves. I fondly imagined that everything that had taken place had progressed from a feeling of love and friendship – there was no rape, no physical forcing, brutality or beating that took place.’

He described how he wanted to atone after the victim, then aged 31, confronted him, saying she felt unable to stop him.

He admitted his wife of 56 years did not know of the affair, adding: ‘I know that what I did was wrong but we are, all of us, fallible and oh how I deluded myself. Please forgive me, love Rolf.’

Harris denied assault, claiming the affair took place in 1983 when he was 53 and she was 18.

He also lied and said she attempted to blackmail him, asking for a £25,000 donation to a bird sanctuary – threatening her brother would ‘go to the papers’ if he refused.

The Australian told jurors that he ‘laughed in disbelief’ when the alleged victim confronted him in 1997 at the age of 31.



He claimed the only reason the woman was ‘emotionally dead’ and ‘damaged’ was because she ‘felt scorned’ when their relationship fizzled out and he later began an affair with his 35-year-old housekeeper.

But the jury believed that she was the victim, not him.

'I HAD NO IDEA SHE WAS SCARED OF ME. PLEASE, PLEASE FORGIVE ME': HOW ROLF HARRIS APOLOGISED TO GIRL'S FATHER IN BOMBSHELL LETTER 'Please forgive me, love Rolf': The letter sent to the father of the girl who claims she was abused for 16 years Here is the full text of a letter read in court written by Rolf Harris to the father of one of his alleged victims in March 1997: 'Dear [the father], 'Please forgive me for not writing sooner. You said in your letter to me that you never wanted to see me or hear from me again, but now [the alleged victim] says it's all right to write to you.

'Since that trip up to Norfolk, I have been in a state of abject self loathing. How we delude ourselves.

'I fondly imagined that everything that had taken place had progressed from a feeling of love and friendship - there was no rape, no physical forcing, brutality or beating that took place.

'When I came to Norfolk, [the alleged victim] told me that she had always been terrified of me and went along with everything that I did out of fear of me.

'I said 'Why did you never just say no?'. And [the alleged victim] said how could she say no to the great television star Rolf Harris.

'Until she told me that, I had no idea that she was scared of me.

'She laughs in a bitter way and says I must have known that she has always been scared of me. I honestly didn't know.

'[The alleged victim] keeps saying that this has all been going on since she was 13. She's told you that and you were justly horrified, and she keeps reiterating that to me, no matter what I said to the contrary.

'She says admiring her and telling her she looked lovely in her bathing suit was just the same as physically molesting her. I didn't know. Nothing took place in a physical way until we had moved to Highlands. I think about 1983 or 84 was the first time. RELATED ARTICLES Previous

1

Next How Rolf Harris became a family favourite carving out a... Rolf Harris's strained relationship with only child saw them... Share this article Share 'I can pinpoint a date was 1986, because I remember I was in pantomime at Richmond.

'When I see the misery I have caused [the alleged victim] I am sickened by myself. You can't go back and change things that you have done in this life - I wish to god I could.

'When I came to Norfolk, spent that time with [the alleged victim] and realised the enormity of what I had done to [the alleged victim], and how I had affected her whole life, I begged her for forgiveness and she said 'I forgive you'. 'Whether she really meant it or not, I don't know. I hope she did, but I fear she can never forgive me.

'I find it hard to like myself in any way, shape or form. And as I do these Animal programmes, I see the unconditional love that dogs give to their owners and I wish I could start to love myself again.

'If there is any way that I could atone for what I have done I would willingly do it. If there is a way I can start to help [the alleged victim] to heal herself, I would willingly do it.

'With your permission I'll phone you in a week to talk to you. If you hang up, I will understand, but I would like to talk to you to apologise for betraying your trust and for unwittingly so harming your darling [the alleged victim].

'I know that what I did was wrong but we are, all of us, fallible and oh how I deluded myself. 'Please forgive me, love Rolf.

'Please forgive me for what must have been the most insensitive thing in your eyes - sending the book for Christmas. Alwen knows nothing about all this - at the time - and rather than tell her I signed the book and wrote the platitudes with sinking heart. 'Forgive me.'

The child star, an autograph hunter and a waitress: The three other women who claimed they were attacked by Harris

