Just one day before Jessica Simpson is set to tie the knot with Eric Johnson, the couple threw a lavish pre-wedding barbeque to celebrate their upcoming nuptials and the Fourth of July!

Jessica Simpson, 33, and Eric Johnson, 34, are so close to being wed! The couple started off their wedding weekend right with their rehearsal dinner on July 3 and now a pre-wedding barbeque on July 4. Jessica and Eric went for a real patriotic theme to celebrate America’s day! How festive! But let’s not forget the wedding on July 5!

Jessica Simpson & Eric Johnson Throw Lavish Pre-Wedding Barbeque

Jessica Simpson and Eric Johnson coupled their Independence Day and wedding weekend celebrations all in one. The couple threw a lovely pre-wedding barbeque for all their wedding guests at the San Ysidro Ranch in Santa Barbara, People reports.

The couple’s wedding planner, Mindy Weiss, dished on the festive party.

“We’re having a real Americana 4th of July party,” she told People. “It’s very down home. But it’s not a T-shirt and jeans Americana. Because Eric’s from Boston, it’s more khakis and white shirts. Very Kennedy!”

The guests experience a cocktail hour that included lobster dogs, corn fritters and a shellfish station. There was also a Southern buffet that included fried chicken, coleslaw, mac and cheese and baby back ribs. To put the cherry on top of an incredible meal, guests indulged the pie bar or went for something smaller like a Creamsicle or Bomb Pop.

Talk about a spread of good food! Jessica and Eric sure know how to throw a party!

There was also a photo booth to keep guests entertained and games for the kids — Maxwell and Ace included!

Jessica Simpson & Eric Johnson: Road To Wedding

The couple said “I do” at the San Ysidro Ranch on July 5. Jessica donned a custom-made champagne and gold Carolina Herrera gown for her big day.

Jessica and Eric’s wedding comes four years after their engagement. The couple became engaged in 2010 just months after they met. They have two children — Maxwell and Ace — together.

We’re so excited for the wedding! We can’t wait to see what Jessica and Eric have in store for us on their big day!

HollywoodLifers, are you excited for Jessica and Eric’s wedding? Let us know!

— Avery Thompson

More Jessica Simpson News: