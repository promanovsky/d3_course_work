Scott began his performance career with a rap group, the Almighty RSO. He later teamed up with David Mays, a Harvard graduate, who became his manager. The two started The Source, a hip-hop magazine that began publishing in 1988, and later put together Hip Hop Weekly, according to Globe reports.

Gai Scott, 36, was charged with armed assault with intent to murder, according to a statement from Plymouth County District Attorney Timothy J. Cruz, in the shooting of 48-year-old Raymond Scott, a hip-hop artist who goes by the stage name “Benzino.”

DUXBURY — A hip-hop artist on his way to his mother’s funeral was allegedly shot by his nephew while the two drove in the funeral procession in Duxbury, backing up traffic on Route 3 southbound for miles and forcing part of the highway to be shut down for most of Saturday afternoon.

Advertisement

Throughout Benzino’s hip-hop career, he has had highly publicized disputes with popular rap icon Eminem.

Benzino also stars in the VH1 show “Love & Hip Hop: Atlanta,” and hosts a weekly radio show “Zino After Dark” in Atlanta, according to his biography page on VH1’s website.

While he had an active career in hip-hop, relations within his family appeared tense.

“There has been growing family tension between Raymond Scott and Gai Scott,” said the statement from the district attorney’s office. According to the priest at the Plymouth church where the procession was heading, the funeral was for Mary Scott of Dorchester, who is listed as Raymond Scott’s mother in her obituary.

Just before noon, as Gai Scott and Raymond Scott were traveling side by side in the funeral procession on Route 3 near Exit 10, Gai Scott allegedly fired several shots from his Bentley into the red Dodge SUV Raymond Scott was driving, according to the statement.

Raymond Scott got out of his car and was driven by a passerby to the Duxbury police department. He was then taken to South Shore Hospital, where he was in stable condition, according to Duxbury Fire Captain John Guilderson.

Advertisement

Gai Scott was arrested by Plymouth police and taken to a State Police barracks in Middleborough. He is expected to be arraigned in Plymouth District Court on Monday.

“It was a 12 o’clock funeral. . . . They were a little late,” said the Rev. William Williams of St. Peter’s Church. “The funeral went on.”

When the funeral procession arrived at the church, said Williams, he looked out the door to see the hearse and a limousine both smudged with blood. “They were pretty upset about the death, and this made the sadness even sadder,” he said.

On Saturday afternoon, investigators at the scene of the shooting put down yellow evidence tags. Several miles away at Samoset Street in Plymouth, police stopped three cars, two with funeral tags hanging from the rear-view mirrors, and roped them off with crime scene tape. They appeared to be questioning people inside the cars.

At St. Peter’s Church, the hearse and limousine were covered in plastic and roped off with crime scene tape. Another car in the parking lot of the church had blood visible on the passenger-side door, and was also wrapped in plastic and crime scene tape.

A section of Route 3 south between Exits 10 and 11 in Duxbury was closed after the shooting, and State Police were re-routing southbound traffic. The highway reopened after 4 p.m.

Advertisement

Two people reached at numbers listed for family members declined to comment Saturday.