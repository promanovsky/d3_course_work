What would L’Wren Scott have made of being referred to as Mick Jagger’s girlfriend first, and a fashion designer second?

The 6ft 3in former model, who has dressed some of the world's best known people - including Nicole Kidman, Angelina Jolie and Michelle Obama - was found by her assistant in her Manhattan apartment at around 10.05am yesterday morning. She had reportedly committed suicide.

Reports of her tragic death spread quickly, gathering pace on Twitter as social media outlets rushed to break the story first.

And yet almost all of them did so by referencing her connection to the Rolling Stones frontman (Radio 2 omitted using her name in its report entirely), who was on tour with the band in Australia at the time, as her primary means of identification.

"I'm a fashion designer. I don't want to be defined as someone's girlfriend," she told The Times last year.

L'Wren Scott: Career in pictures Show all 20 1 /20 L'Wren Scott: Career in pictures L'Wren Scott: Career in pictures L'Wren Scott L'Wren Scott attends The Serpentine Gallery Summer Party at The Serpentine Gallery in London, 2013 L'Wren Scott: Career in pictures L'Wren Scott and Val Kilmer Val Kilmer and L'Wren Scott arrive at a dinner honoring the spirit of Special Olympics at the White House, 2000 L'Wren Scott: Career in pictures L'Wren Scott and Mick Jagger L'wren Scott and singer Mick Jagger arrive at the World Premiere of 'Alfie' at the Empire Leicester Square in London, 2004 L'Wren Scott: Career in pictures L'Wren Scott and Karl Lagerfeld L'wren Scott and Karl Lagerfeld chat backstage at the Chanel fashion show as part of Paris Fashion Week (Haute Couture) Spring/Summer 2005 in Paris L'Wren Scott: Career in pictures L'wren Scott, Victoria Beckham and Amira Casar L'wren Scott, Victoria Beckham and Amira Casar attend the Chanel fashion show during Paris Fashion Week (Haute Couture) Spring/Summer 2006 in Paris L'Wren Scott: Career in pictures L'Wren Scott Designer L'wren Scott attends the Sidaction Party raising funds in support of AIDS during Paris Fashion Week (Haute Couture) Spring/Summer 2006 at Pavillon D'Armenonville in Paris L'Wren Scott: Career in pictures L'Wren Scott L'Wren Scott arrives at the Nina Ricci show during Paris Spring/Summer 08 fashion week in Paris, 2007 L'Wren Scott: Career in pictures L'Wren Scott L'Wren Scott attend the British Fashion Awards 2008 held at The Lawrence Hall in London L'Wren Scott: Career in pictures L'Wren Scott L'Wren Scott arrives at the 'Shine A Light' New York premiere at the Ziegfeld Theater in New York, 2008 L'Wren Scott: Career in pictures L'Wren Scott and Mick Jager L'Wren Scott and Mick Jager arrive at the 2009 Vanity Fair Oscar Party at Sunset Tower in West Hollywood L'Wren Scott: Career in pictures Dan Williams, Jade Jagger, Mick Jagger and L'Wren Scott Dan Williams, Jade Jagger, Mick Jagger and L'Wren Scott attend the Jade Jagger shop opening party in London, 2009 L'Wren Scott: Career in pictures L'Wren Scott and Mick Jagger L'Wren Scott and Mick Jagger attend the 'Shutter Island' premiere at the Ziegfeld Theatre in New York, 2010 L'Wren Scott: Career in pictures L'wren Scott and Patti Hansen L'wren Scott and Patti Hansen attends the 'Stones in Exile' screening at The Museum of Modern Art in New York, 2010 L'Wren Scott: Career in pictures L'Wren Scott, Oona Ingle-Finch and Sydney Ingle-Finch L'Wren Scott, Oona Ingle-Finch and Sydney Ingle-Finch attend Finch's Quarterly Cannes Dinner at the Hotel du Cap as part of the 63rd Cannes Film Festival in Antibes, 2010 L'Wren Scott: Career in pictures Mick Jagger, L'Wren Scott and Tommy Hilfiger Mick Jagger, L'Wren Scott and Tommy Hilfiger arrive for the screening of 'Stones in Exil' during the 63rd Cannes Film Festival in Cannes, 2010 L'Wren Scott: Career in pictures Diego Della Valle and L'Wren Scott President and CEO of Tod's Diego Della Valle and L'Wren Scott attend the 3rd Annual Finch's Quarterly Review Filmmakers Dinner honoring Oscar-winning British film producer Jeremy Thomas and sponsored by Tod's and the IWC at Hotel du Cap-Eden-Roc in Cannes, 2011 L'Wren Scott: Career in pictures Raf Simons and L'Wren Scott Raf Simons and L'Wren Scott attend the Christian Dior Haute-Couture Show as part of Paris Fashion Week Fall / Winter 2013 in Paris, 2012 L'Wren Scott: Career in pictures Stephen Jones and L'Wren Scott Stephen Jones and L'Wren Scott attend the L'Wren Scott cocktail party during London Fashion Week Fall/Winter 2013/14 in London L'Wren Scott: Career in pictures L'Wren Scott L'Wren Scott attends the British Fashion Awards 2013 drinks reception at the London Coliseum in London L'Wren Scott: Career in pictures Jade Jagger, L'Wren Scott, Georgia May Jagger and Mick Jagger Jade Jagger, L'Wren Scott, Georgia May Jagger and Mick Jagger attend the annual Serpentine Gallery Summer Party co-hosted by L'Wren Scott at The Serpentine Gallery in London, 2013

"You always wonder if people will pay attention to the hard work that goes into what you do."

Many of her supporters took to the micro blogging site to criticise the lack of credit given to the late designer by news sources:



Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Similarly, prominent female journalists Clare Balding, Alice Arnold and Rachel Johnson have all tweeted their disapproval at the way Scott's death has been reported.

Scott, 49, had managed to craft a successful career after being adopted by Mormon parents in Utah.

After she graduated from high school, she was flown to Paris to work as a model for Chanel and appear in a campaign shot by renowned fashion photographer David Bailey. It was during this time that she changed her name from Luann Bambrough to L'Wren Scott.

During the early 90s, she built up a reputation working as a stylist on photoshoots, working on huge fashion and beauty campaigns and contributing costumes to films, including Eyes Wide Shut and Ocean's Thirteen.

She came out with her first own-label collection, Little Black Dress, in 2006 - five years after she started dating 70-year-old Jagger - and worked on a Martin Scorsese documentary about the Rolling Stones in 2008.

Scott went on to dress some of the best known stars in the world for events like the Oscars and the Golden Globes and was rumoured to be designing Angelina Jolie's wedding gown.

At the time of her death, she had just launched a more affordable line of women's fashion with Banana Republic and a beauty collaboration with Bobbi Brown.

She was also thought to be in millions of pounds of debt through her own label. It was thought that her financial struggles were partly to blame for the cancellation of her most recent show date at London Fashion Week. And yet, according to reports, she refused to take money from her partner.

Since her body was discovered, tributes have continued to pour in. One of the first to take to Twitter was Jagger's ex-wife, Bianca Jagger.

"Heartbroken to learn of the loss of the lovely and talented L'Wren Scott. My thoughts and prayers are with her family," she wrote. "May she rest in peace."

Others include her friends in Duran Duran, actresses Olivia Munn and Emmy Rossum, fashion designer Marc Jacobs and Michelle Obama.