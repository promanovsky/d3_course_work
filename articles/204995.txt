A U.S. court announced that Oracle Corp. should sue Google Inc. for copyrights violation, after the tech giant allegedly copied Oracle's Java programming language and integrated it into its Android OS.

The decision reverses a San Francisco judge's 2010 decision, Businessweek reported.

Google allegedly integrated Java code on its operating software without paying Oracle. The programming language is free for developers, but comes with a fee if it's to be used for commercial purposes. U.S. District Judge William Alsup ruled in 2010 that Java is not subject for copyright protection.

The multinational computer technology company is now seeking to receive more than $1 billion - an amount lower than the original $6.1 billion estimate - for the damages, after receiving the green light from the U.S. Court of Appeals in Washington on Friday.

"A set of commands to instruct a computer to carry out desired operations may contain expression that is eligible for copyright protection," Circuit Judge Kathleen O'Malley wrote for the three-judge panel. "An original work - even one that serves a function - is entitled to copyright protection as long as the author had multiple ways to express the underlying idea."

Oracle received support from other software companies including Microsoft, NetApp and EMC.

"This is a win for Oracle and the entire software industry that relies on copyright protection to fuel innovation and ensure that developers are rewarded for their breakthroughs," Oracle General Counsel Dorian Daley said in a statement to Businessweek.

Meanwhile, Google is preparing for its next move, spokesperson Matt Kallman said. According to a Reuters report, the Android maker still believes that the Java software should be patented instead of copyrighted.

The software companies considered it a victory for the software industry as a whole, but others saw it as a loss. Programmers and app developers worried that the price of the license fees would increase as a result of the ruling.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.