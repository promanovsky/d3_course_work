The new guideline is based on a review of medical studies that found no evidence that such exams help detect ovarian cancer early enough to cure it, or find other abnormalities in healthy women who have no pain, bleeding, or other warning signs of trouble.

The recommendation was criticized by the group representing obstetricians-gynecologists, the doctors most likely to perform the exams during annual visits.

Health care providers should stop routine yearly pelvic exams on women because they offer no benefits for those without symptoms, according to a new guideline issued Monday by the American College of Physicians, the group that represents the nation’s internists.

Advertisement

“For any exam or intervention that a doctor does, we need to have evidence of benefit, and we couldn’t find any,” said Dr. Amir Qaseem, director of clinical policy at the American College of Physicians and lead author of the new guideline, which was published in the Annals of Internal Medicine. “We did find some evidence that the exam caused fear, anxiety, embarrassment, and discomfort in women and may be preventing some from getting medical care.”

He and his coauthors concluded that doctors should abandon the routine practice except in cases where women are having symptoms such as unexplained bleeding, discomfort during sex, vaginal itching, pelvic pain, or bloating.

They also emphasized that the guideline does not change recommendations for cervical cancer screening, when doctors take a small tissue sample from the cervix every few years to look for cell changes.

“It’s important to stress that this guideline applies only to asymptomatic women,” said Dr. Hanna Bloomfield, associate chief of staff for research at the Minneapolis VA Healthcare System and leader of the research review on which the guideline was based. “Doctors need to continue to take thorough histories and might deem a pelvic exam appropriate based on symptoms a woman is having.”

Advertisement

Whether doctors and other medical providers who perform the exam will stop doing it remains to be seen.

The American College of Obstetricians and Gynecologists stated that it stands by its current guidelines “suggesting that the decision about whether to perform a pelvic examination be a shared decision between health care provider and patient, based on her own individual needs, requests, and preferences.”

“Pelvic exams are very much a part of the ob-gyn culture,” Bloomfield said. “It is something they’ve always done, and it’s very hard to change practice once it’s in place.”

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.