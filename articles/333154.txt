Sherri Shepherd has announced that she is leaving The View after seven seasons as a co-host.

“It’s been seven wonderful years on The View and after careful consideration it is time for me to move on,” the 47-year-old comedienne said in a statement (via Deadline). “I am extremely grateful to Barbara Walters and Bill Gedde for giving me the opportunity. I look forward to the business opportunities that lay ahead for me and I am incredibly grateful to my View family and my fans for supporting me on this journey.”

Sherri‘s exit leaves just two ladies left at the table – Whoopi Goldberg and Jenny McCarthy, who is rumored to be getting the boot this year.

ARE YOU SAD to see Sherri Shepherd leave The View?