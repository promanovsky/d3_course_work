Indonesian youth walk past an Intel sign during Digital Imaging expo in Jakarta March 5, 2014. Intel, IBM, Cisco and other tech giants are forming a consortium to promote the Internet of Things, the companies announced Thursday. (REUTERS/Beawiharta)

Several tech competitors including Intel and Cisco are setting aside rivalries to collaborate on the Internet of Things, the companies announced Thursday.

Intel and Cisco, as well as AT&T, General Electric, and IBM, are forming the Industrial Internet Consortium, a nonprofit dedicated to the Internet of Things — a term for a connected network of sensors and devices.

One of the group’s goals is to create inter-operability standards, so the devices, sensors and networks members create can communicate with each other, and that the data they exchange is secure.

Founding members will hold permanent seats on the steering committee, though membership is open to interested companies, researchers and public agencies, among others.The organization is managed by Boston-based nonprofit trade association, the Object Management Group. The coalition is still discussing which industries could serve as test-beds for new IoT standards, according to the group.

By Cisco’s estimate, about 50 billion devices will be connected to each other by the end of 2020, Tony Shakib, vice president of the company’s Internet of Things business group said during a conference call. Companies will need to work together to provide the sensors, devices, the network infrastructure, and ultimately manage the data created by these devices, he said.

Organizations like the newly formed IIC “bring all these [technologies] together to have them inter-operate almost out of the box, to be able to coexist with each other and to be able to secure them,” Shakib said.