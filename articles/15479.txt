A new study suggests that exposure to environmental toxins increases the risk of developing autism. However, researchers are yet to pinpoint the exact toxins that trigger this development disability.

According to Autism Speaks, autism is a complex development disability that affects an individual's intellectual disability, motor skills and attentiveness, and physical health issues such as sleep and stomach problems. This condition is not usually visible until a child turns 2 or 3 years old. The Centers for Disease Control and Prevention (CDC) also noted that 1 in 88 children has autism and this number increases by 10 percent every 40 years. Most of the cases recorded are in boys.

The cause of autism is still being debated by scientists but they have been looking at genetics and mutations which affect the brain development. There is also a nongenetic or environmental belief that connects the disorder with various factors such as parental age, time of conception, and oxygen deprivation during pregnancy. However, a new study suggests that environmental toxins are major players, as well.

Researchers from the University of Chicago, led by professor of genetic medicine Andrey Rzhetsky, analyzed data of nearly 100 million children gathered from different states in the United States to measure autism rates and intellectual disability. They also checked the genital defects of boys because they believe that these are indicators of parents' exposure to environmental toxins such as pesticide and lead.

On the data gathered, only two percent showed genital birth defects. Furthermore, they noted that the risk of developing autism increases by 300 percent for every one percent.

"Both genes and environment are important," Prof. Rzhetsky said to WebMD.

The researchers have not identified yet the exact toxins that cause autism, but they are eyeing the chemicals found in plastics, food cans, and other common products. They are also studying if air pollution can also trigger autism.

This study was published in the March 13 issue of PLOS Computational Biology.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.