The company is still not turning a profit, reporting a loss of $144.6 million, or 24 cents per share. But investors were cheered by the fact that the company is making more money off each individual user -- an estimated $1.60 per every thousand views of the Twitter timeline, or main newsfeed where it shows advertisements to users.

AD

The quarterly spike in revenue seemed to quell some concerns about the company's long-term business plan. In April, shares fell nearly 11 percent after the company reported that its monthly active users had increased just 6 percent -- to 255 million users -- after stagnating over the past several quarters. Twitter reported a similar level of growth for this quarter, just over 6 percent to 271 million users.

AD

Twitter definitely got a boost this quarter from world events such as the World Cup, for which it created a number of new features such as push notifications for scores as a ploy to keep users checking their feeds throughout the day.

In a statement, Twitter chief executive Dick Costolo said that the firm will continue improving its ad products and making the network appeal to a broad base of users.

AD

"[By] developing new product experiences, like the one we built around the World Cup, we believe we can extend Twitter’s appeal to an even broader audience,” Costolo said.