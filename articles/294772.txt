Following the market opening Monday, the Dow traded up 0.01 percent to 16,777.25 while the NASDAQ gained 0.23 percent to 4,320.68. The S&P also rose, gaining 0.18 percent to 1,939.61.

Leading and Lagging Sectors

Healthcare shares gained around 0.87 percent in today's trading. Meanwhile, top gainers in the sector included Covidien plc (NYSE: COV ), up 25 percent, and Achillion Pharmaceuticals, (NASDAQ: ACHN ), up 15.7 percent.

In trading on Monday, telecommunications services shares were relative laggards, down on the day by about 0.39 percent. Top losers in the sector included Telecom Argentina SA (NYSE: TEO ), down 5.6 percent, and Shenandoah Telecommunications Co (NASDAQ: SHEN ), off 5.1 percent.

Top Headline

Medtronic (NYSE: MDT ) announced its plans to buy Covidien plc (NYSE: COV ) for $42.9 billion in cash and stock.

Medtronic will offer $93.22 per share for Covidien, representing a 29% premium over Covidien's closing price on Friday.

Equities Trading UP

Bluebird Bio (NASDAQ: BLUE ) shares shot up 49.33 percent to $38.96 following the presentation of positive data on LentiGlobin BB305 at the European Hematology Association (EHA).

Shares of Covidien plc (NYSE: COV ) got a boost, shooting up 26.66 percent to $91.22 after Medtronic (NYSE: MDT ) announced its plans to buy Covidien for $42.9 billion in cash and stock.

Fusion-io (NYSE: FIO ) shares were also up, gaining 22.52 percent to $11.37 after SanDisk (NASDAQ: SNDK ) announced its plans to buy Fusion-io for $11.25 per share in cash.

TW Telecom (NASDAQ: TWTC ) jumped 9.52% to $39.80 after the company agreed to be acquired by Level 3 Communications (NYSE: LVLT ) in a stock-and-cash transaction valued at $40.86 per share.

Equities Trading DOWN

Shares of Karyopharm Therapeutics (NASDAQ: KPTI ) were 7.77 percent to $43.46 after the company announced that the FDA has found the effectiveness and safety for KPT-335.

Yahoo! (NASDAQ: YHOO ) shares tumbled 5.08 percent to $35.06. Alibaba reported revenue for the year ending March 31 of $8.47 billion and reported Yahoo stake in Alibaba of 22.5% versus 22.6% as of December 31, 2013.

DreamWorks Animation SKG (NASDAQ: DWA ) was down, falling 11.26 percent to $24.27 after the company announced the launch on YouTube.

Commodities

In commodity news, oil traded up 0.07 percent to $106.99, while gold traded up 0.51 percent to $1,280.60.

Silver traded up 0.15 percent Monday to $19.69, while copper rose 0.46 percent to $3.04.

Eurozone

European shares were lower today.

The eurozone's STOXX 600 tumbled 0.48 percent, the Spanish Ibex Index dropped 1.19 percent, while Italy's FTSE MIB Index fell 0.91 percent.

Meanwhile, the German DAX dropped 0.24 percent and the French CAC 40 fell 0.48 percent while UK shares dropped 0.36 percent.

Economics

The Empire State manufacturing index rose to 19.28 in June, versus a prior reading of 19.01. However, economists were expecting a reading of 15.00.

Industrial production rose 0.6% in May, after dropping 0.3% in April. However, economists were projecting a 0.5% growth in May.

The NAHB housing market index surged to 49.00 in June, versus a prior reading of 45.00. However, economists were expecting a reading of 47.00.

The Treasury is set to auction 3-and 6-month bills.

© 2014 Benzinga.com. Benzinga does not provide investment advice. All rights reserved.

Free Trading Education - Check out the free events taking place on Marketfy this week. Spaces are limited. Sign up today.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.