We know. XP has been your friend for 12 years. It was there to fall back on during the Vista debacle, and again when Microsoft made major changes to Windows with Windows 8. But after more than a decade, it's time to go -- and we're here to help you get through this process.

1. Denial: You may be wondering: Do I even have XP? Maybe, you're thinking, this announcement doesn't even relate to me and I don't have to worry about it.

AD

AD

In fact, the quickest and easiest way to tell if you still have XP — or an even older system — is to look at the start menu, in the lower left-hand corner of the screen.

Does it say “Start”?

Then we may have a problem here.

(Microsoft has also given you a super easy way to check, if you feel so inclined.)

2. Anger: Okay, so if your computer is running XP and you find you have to upgrade, you're probably a little upset right now.

But while it's tempting to think that this is purely a play to get users to spend money on new systems -- and, in some cases, new computers -- the truth is that this has been a long time coming. Microsoft traditionally keeps up support for its systems for about a decade, and it's now been 12 years for XP, which was first released in the summer of 2001.

AD

Twelve years is an astronomically long time in tech. Back then, we didn't have tablets. We didn't really have smartphones. A GE incandescent lightbulb made Time's list of the best inventions of the year.

AD

In other words, a lot has changed since then, and XP just wasn't built to handle the pressures of a modern computing world. Basically, this is Microsoft’s way of saying that we’re approaching the end of the line for Microsoft XP, and it’s time to jump aboard a new operating system — preferably, they’d say, one of theirs.

3. Bargaining: So maybe you're okay with relying on third-party support for XP. You're willing to get a better antivirus program. Or to limit your Internet time in order to minimize risk.

AD

Really, this isn't a horrible stage to stay in...for a little longer. If you still use your computer as a word-processor, or a place just to do your (offline) taxes, then you could chill for a while. Even if you do use your computer for limited online use, Microsoft is continuing to offer antivirus support through July, and your computer certainly isn't set to detonate or become nonfunctional if you don't upgrade it this week.

AD

But it is a tradeoff. Even the most popular antivirus software companies aren't going to keep XP support forever. As ZDNet reported, Bitdefender is keeping up support until 2017; Kaspersky's keeping it until 2018.

That gives you plenty of time to save up for a new computer, or a new operating system, through staying on XP will only get riskier as time goes on.

4. Depression: Still, for those who do need to upgrade, the options post-XP may not look that great to you -- after all, if they did look good you probably would have upgraded by now. And you're probably doubly bummed if it turns out that your current computer can't handle a newer system.

AD

But, really, this will probably be a good thing for you. Windows 7 will feel very familiar to XP users, and it has some useful features, such as "Snap," which lets you quickly resize windows to get more on your screen, and "Peek," which lets you preview the desktop or an open window without having to close anything. It's a little difficult to find Windows 7, but you can find copies online, and some new computers are still running Windows 7.

AD

Just remember: Microsoft is set to end mainstream support for Windows 7 in 2015, and extended support in 2020. So if you go that route, you'll back here sooner than you may think.

Those feeling a bit more bold can jump straight to Windows 8, which will feel very different to XP users, but is optimized for touchscreen devices. This is definitely what Microsoft wants you to do, and if you're looking to future-proof your computing, it's honestly the best option.

AD

The good news here is that Microsoft is working on making the most jarring parts of Windows 8 less jarring for legacy users by scaling back some of the major design changes they made (such as killing the Start Menu).

5. Acceptance: Okay. You're ready to do this thing. You've picked your operating upgrade -- Windows 7 or Windows 8, most likely -- and you're ready to start the process. What should you do now?

AD

You absolutely have to back up your files. You'll need to do a clean installation to upgrade to Windows 7 or Windows 8, which means that nothing from your old system will come over automatically. It's actually a good reason to take a look at your computer and eliminate the programs you don't use anymore -- and dump all those high school English essays that will never be useful again.