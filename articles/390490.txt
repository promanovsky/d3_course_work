A male in Florida has been infected with the viral disease chikungunya, which health officials say is the first locally acquired case of the mosquito-borne disease in the continental U.S.

He had not recently travelled outside of the U.S., the Centers for Disease Control and Prevention said Thursday.

Chikungunya virus is transmitted to people by two species of mosquitoes, Aedes aegypti and Aedes albopictus, the CDC says. (U.S. Centers for Disease Control and Prevention)

The newly reported case represents the first time that mosquitoes are thought to have spread the virus to a non-traveller, the agency said.

"The arrival of chikungunya virus, first in the tropical Americas and now in the United States, underscores the risks posed by this and other exotic pathogens," Roger Nasci, chief of CDC’s Arboviral Diseases Branch, said in a release.

Puerto Rico and the U.S. Virgin Islands have also reported locally acquired chikungunya this year. In local transmission, a mosquito bites someone who is infected with the virus and then bites another person.

Chikungunya is a disease caused by the chikungunya virus. It typically causes fever, along with an arthritis-like pain in the joints and a rash.

It is spread to humans through the bite of an infected mosquito. The symptoms of chikungunya can appear similar to those of dengue fever, the Public Health Agency of Canada says.

No one knows what course chikungunya will take now in the United States.

The Canadian agency’s travel notice said that since December 2013, confirmed cases of chikungunya have been reported on many islands in the Caribbean.

It advises:

See your health care provider or visit a travel health clinic at least six weeks before you travel.

Protect yourself from mosquito bites, particularly during peak mosquito biting times around sunrise and sunset.

If you develop symptoms similar to chikungunya when you are travelling or after you return, see a health care provider and tell them where you have been travelling or living.

Outbreaks of chikungunya have been previously reported from countries in Africa, Asia, Europe, India, and the Middle East.