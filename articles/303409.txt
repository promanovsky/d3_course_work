SEATTLE, WA - JUNE 18: Amazon.com founder and CEO Jeff Bezos presents the company's first smartphone, the Fire Phone, on June 3, 2014 in Seattle, Washington. (Photo by David Ryder/Getty Images)

SEATTLE (AP) — Amazon has introduced a new smartphone that will be closely tied to the products and services it sells, while adding such touches as the ability to render images in 3-D.

The Fire phone will share many characteristics found in other Amazon devices. The home screen will have a carousel of recently accessed apps, for instance. There’s also X-Ray for supplemental content and Mayday for live tech support.

Competing won’t be easy, though. Amazon is arriving late to a tightly contested marketplace. Samsung and Apple dominate worldwide smartphone sales with a combined 46 percent share, according to IDC. And in the U.S., Apple leads with more than 37 percent, with Samsung at nearly 29 percent.

Amazon has tried to chip away at Apple’s top position in the tablet market with its Kindle Fire HDX tablet, which beats the iPad Air’s screen resolution and is lighter and cheaper. Still, the iPad dominates the category while Amazon has seen its market share shrink from 7 percent in 2012 to 2 percent in the first quarter of this year.

As the phone was announced in Seattle, Amazon’s stock rose $8.82, or 3 percent, to $334.44 in afternoon trading.

Here’s a look at the new phone:

SPECS AND FEATURES:

— With a new Firefly feature, snap a photo of a book title, and it’ll show you where to buy it. Listen to a song playing in the background, and it’ll direct you to that tune on Amazon. It can even direct you to knowledge, such as pulling up a Wikipedia entry on a painting you snapped. The feature will also let you snap bar codes, phone numbers and more.

— The phone is smaller than leading Android phone, but larger than Apple’s iPhone. CEO Jeff Bezos calls the screen, measuring 4.7 inches diagonally, ideal for one-handed use.

— Bezos touts the camera on the new phone. He says it has image stabilization to counteract shaking as people take shots. Amazon is offering unlimited free storage on its Cloud Drive service.

— The phone will come with earbuds that have flat cords and magnets to clasp them together, so tangled cords will be history.

— Bezos says images are typically flat — and Amazon wants to change that. You can rotate the phone around and get a different view depending on your angle of vision. He says the phone is basically redrawing the image 60 times per second. Bezos calls this “dynamic perspective.”

— To make that happen, the phone has four front-facing infrared cameras to tell where your head is, even if your fingers happen to cover two of them.

— There’s an auto-scroll feature that lets you scroll down by tilting the phone. Samsung’s Galaxy phones have that, too.

— Amazon’s Kindle tablets run a highly modified version of Google’s Android system, and it’s likely an Amazon phone would do the same. That means apps for the phone would be limited to what’s available through Amazon’s own app store. The store has grown to include more than 240,000 apps, but there’s much more for Android and Apple devices.

AVAILABILITY:

— AT&T will be the exclusive carrier for the new phone. It’s a similar approach to what Apple took when it unveiled its first iPhone in 2007. AT&T had exclusive rights to the iPhone in the U.S. until 2011, when Verizon and eventually others got it, too.

— The phone will be available July 25. People can start ordering them Wednesday at $200 for a base model with 32 gigabytes and $300 for 64 gigabytes. Both require two-year service contracts.

— The phone comes with 12 months of Prime membership, which is normally $99 a year. Existing Prime members will get their term extended.

NOT A FIRST:

— Facebook once tried to release a phone tied to its services. The HTC First, released in April 2013, came with Facebook’s Home software, which takes over the phone’s front screen to present status updates, messages and other content. Both the phone and the software flopped.

— Google also has its own phones under the Nexus brand, mostly to showcase its Android operating system. Google makes Android available for free for any phone manufacturer to use and modify. That makes it difficult to know what’s really Android and what’s a modification.

PAST FORAYS INTO GADGETS:

— Amazon’s first gadget was a Kindle e-reader, released in 2007. Although there are plenty of devices that do more, many people still prefer stand-alone e-book readers because they typically have better screens for reading in direct sunlight and don’t have distractions such as Facebook and email.

— The company started making Kindle Fire tablets in 2011. The latest models, HDX, are notable for a Mayday help button that accesses live tech support 24 hours a day, seven days a week. You see the representatives in a video box, but they can only hear you and see what’s on your screen. They can also help guide you by placing orange markers on your screen or taking control of your device completely.

— In April, Amazon released its Fire TV streaming devices. What sets it apart from rival gadgets is a voice search feature that lets you speak the title, actor, director or genre into your remote to get matching content on the TV.

___

Anick Jesdanun reported from New York.

Copyright 2014 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.