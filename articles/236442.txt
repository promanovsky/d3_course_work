Jackman gets police escort on Vic train

He's made his name playing an invincible mutant, but even Hugh Jackman needs protecting sometimes.

The Wolverine star was snapped at Melbourne Central station making his way to South Yarra with the help of three protective services officers (PSOs).

Victoria Police then posted the picture to their Facebook page with the caption: "Our PSOs protect everyone at the station - even Wolverine!"

The Hollywood star won praise from social media users for opting for the low-key option of public transport.

But some fans - especially the females - seemed miffed to have missed Jackman on the city loop.

"I would be MORE than happy to assist in protecting Hugh Jackman," an enthusiastic female fan posted on the Victoria Police photo.

"Although perhaps I am who he needs protection from! That man is divine!"