A new government report says the Affordable Care Act's health insurance subsidies will cost a little less than previously thought.

The Congressional Budget Office predicts that health insurance subsidies under so-called "Obamacare" will total a little more than $1 trillion over the next 10 years, instead of almost $1.2 trillion.

CBO said the 8 per cent cut results largely from tighter cost controls by insurance companies offering plans on health care exchanges. Generally speaking, the plans offered on the exchanges pay health care providers less and have tighter management of patients' treatment options, and that means lower premiums and taxpayer subsidies.

Medicaid adds almost $800 billion in costs over the decade.

CBO is a nonpartisan congressional agency that does research and cost estimates for lawmakers.