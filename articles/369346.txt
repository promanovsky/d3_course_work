There's a new Hollywood couple and apparently it's left many fans utterly confused. The Daily Mail reports that "Fast & Furious" star, and openly bisexual actress, Michelle Rodriguez is dating Zac Efron. The two recently took a vacation together in Sardinia and were caught kissing and cuddling on a yacht.

The 35-year-old actress recently ended a whirlwind relationship with model Cara Delavigne, and Efron, 26, split from girlfriend Lily Collins in 2012. He also famously dated his "High School Musical" costar Vanessa Hudgens for four years.

Rodriguez opened up about her romantic life to Entertainment Weekly last October admitting that she has "gone both ways" in the past.

"I do as I please. I am too f**king curious to sit here and not try when I can. Men are intriguing. So are chicks," the actress said.

Besides her fling with Delavigne, the "Girl Fight" actress was also linked to Evan Rachel Wood - following her split from husband Jamie Bell - but Wood denied it. Rodriguez has also rumored to have dated Vin Diesel after meeting on the set of "Fast & Furious" and Oliver Martinez, who is now married to Halle Berry.

"I'm attracted to manly men," she previously told Cosmopolitan magazine. "I can't do metrosexuals who get their nails done more than me. Which, is why, living in LA for eight years, you can't blame me for not having a partner!"

Shortly after the photos of Rodriguez and Efron kissing surfaced, fans took to Twitter to express their shock over the rumored romance and their confusion.

"Michelle rodriguez and zac Efron are the weirdest couple ever... I don't get it..." @aurosan tweeted.

"Zac Efron and Michelle Rodriguez??????? Whaaaaaaaat why," @monicheken posted.

"Zac Efron and Michelle Rodriguez? Don't know why, but I find Cara and Michelle's relationship more interesting..." @WandererLass wrote.

"Wait... Zac Efron and Michelle Rodriguez...? What the hell..?!" @aurosan tweeted.

Are shocked, happy or indifferent that Michelle Rodriguez and Zac Efron are reportedly dating? Sound off in the comments below with your thoughts.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.