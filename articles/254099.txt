Rob Kardashian arriving on a flight (after leaving Kim's wedding) at LAX airport in Los Angeles, California on May 24, 2014 (FameFlyNet)

After skipping out on his sister Kim Kardashian’s wedding and returning home to Los Angeles — Rob Kardashian has finally broken his silence. Now back in the states, he’s deleted the majority of tweets on his Twitter account, only to replace them with inspirational ones.

Rob Kardashian, 27, has “seen the light” or so it would seem. After skipping out on Kim and Kanye’s wedding in Italy, Rob returned home to the states, deleted his existing tweets, and replaced them with inspirational messages. He’s finally broken his silence after the reported blow up he had with sister Kim over his weight issues, but what is Rob really trying to say in those tweets?

Rob Kardashian Breaks Silence On Twitter After Skipping Kimye Wedding — Read Inspirational Message

On May 26, Rob posted his first tweet, saying, “good morning Los Angeles.” He followed that tweet with a big change to his Twitter account. He deleted everything and even changed his Twitter “avatar.”

Rob had about 5 million followers at the time of his big social media change. His next series of tweets were “positive messages to members of the service for Memorial Day,” Us Weekly reports.

“Happy Memorial Day,” Rob posted. “Blessed is the nation whose God is the LORD. Psalm 33:12. Lord Jesus, protect those who protect me- police, firefighters, and those in the military. Teach me to pray for them and to honor them for their service. Amen.”

Rob Speaks Out On Skipping Kimye Wedding

We learned that Rob reportedly cut out on Kim’s wedding because he was embarrassed about his weight and didn’t want to be in any pics.

His family tried their hardest to provide him with support, but even with the family pulling for Rob and telling him “he looked great” — Rob still would not jump in the family photos.

However, the Jenner/Kardashian clan kept insisting, and it just made Rob more upset. Eventually, Rob had had enough and decided to leave the country altogether.

HollywoodLifers, what did you think about Rob’s heartfelt message? Do you think it was wrong for him to cut out on the wedding? Tell us what’s on your mind.

— Bryant Perkins

More Rob Kardashian News: