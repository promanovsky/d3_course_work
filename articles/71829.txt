The UN’s top court ruled on Monday to temporarily halt Japan’s whaling program in Antarctic waters, in a case brought against the country by Australia and environmental groups.

Presiding Judge Peter Tomka at the International Court of Justice said that the court’s 16-member panel decided that Japan has not justified the large number of minke whales it takes under its program, while failing to meet much smaller targets for fin and humpback whales.

The court ordered a halt to the issuing of whaling permits until the program is redesigned.

Japan hunted around a thousand whales annually in what it claims was a scientific program.

But Australia and environmental groups accused Japan of attempting to manoeuvre around a moratorium on commercial whaling imposed by the International Whaling Commission in 1986.

While whale meat is becoming less popular in Japan, it is considered a delicacy by some, and meat from the hunt is sold commercially.

Warning: Viewers may find footage shocking

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Japanese Foreign Affairs Ministry spokesman Noriyuki Shikata told reporters that the country "regrets and is deeply disappointed" by the decision, but added:"as a state that respects the rule of law and as a responsible member of the global community, Japan will abide by the ruling of the court."

Factors the court considered included the scale of Japan's program, whether it met the scientific targets it sets; and the degree to which the results of its hunt were coordinated with academic organisations.

The ruling noted among other factors that Japan had not considered a smaller program or non-lethal methods to study whale populations.

In its defense, Japan cited only two peer-reviewed scientific papers relating to its program from 2005 to the present — a period during which it has harpooned 3,600 minke whales, a handful of fin whales, and no humpback whales at all.

Australia launched the case in 2010, and in July 2012 argued there was no need to kill any whales for scientific purposes.

But Japan argued that Australia's case was an attempt to use the court to impose its cultural standards on Japan, rather than any violation of international law. It compared the case to Hindu people demanding an international ban on killing cows.

Despite the ruling, Japan will continue to conduct a smaller whaling program in the northern Pacific.

Meanwhile Norway and Iceland reject a 1986 moratorium on commercial whaling imposed by the International Whaling Commission and conduct for-profit whaling.

Patrick Ramage, director of the International Fund for Animal Welfare's, said the ruling "certainly has implications ultimately for whaling by Iceland and Norway as well."



"I think it will increase pressure on those two countries to re-examine their own whaling practices and the various reasons and pretexts given for that whaling activity."