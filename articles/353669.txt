HTC is back in black, at least for now.

The Taiwan-based company released its unaudited results on Thursday, posting a net profit of 2.26 billion New Taiwan dollars (or a little less than $76 million) for the June quarter. That beat analyst estimates for a profit of 2.17 billion New Taiwan dollars.

HTC had reported a net loss in each of three previous quarters despite positive reviews for the HTC One, its flagship phone. The company was criticized for its convoluted marketing efforts and struggled to compete against Apple and Samsung in the smartphone space.

The company has attempted a turnaround in recent months by updating its smartphone product line with a new flagship phone — the HTC One M8 — as well as the HTC One E8 and HTC One Mini 2.

It has also attempted to improve its marketing efforts by, most notably, bringing Samsung's former U.S. chief marketing officer on board.