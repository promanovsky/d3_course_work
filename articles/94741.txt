Sky Atlantic's Game of Thrones simulcast pulled in over half a million viewers, overnight figures suggest.

The UK broadcaster aired season four's opener 'Two Swords' at the same time as US channel HBO - with 537k tuning in at 2am BST.



675,000 later tuned in for the episode's first UK primetime airing at 9pm.

Though the 2am showing attracted a slightly lower audience, its audience share was substantially higher than the 9pm airing - 23.9% compared to 3%.

The third season premiere of Game of Thrones attracted an overnight audience of 742k in 2013. The episode was not simulcast, but aired less than 24 hours after US broadcast.

In the US, 'Two Swords' pulled in 6.6m viewers, HBO's biggest ratings since the series finale of The Sopranos in 2007.

Game of Thrones season 4 begins: Have your say and vote in our poll

Game of Thrones season 4 episode 1 Two Swords: What the critics said

Watch a clip from the Game of Thrones season 4 premiere below:

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io