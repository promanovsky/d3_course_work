Tesla and BMW held EV talks amid open patent move

Tesla and BMW have held talks this week about efforts to improve adoption of electric vehicles, coinciding with Elon Musk’s decision to open up Tesla’s EV patent portfolio for other manufacturers to use. Exact details on what was discussed between the two firms on Wednesday this week have not been revealed, though Musk did say that it was around promoting EVs in general, as well as taking greater advantage from the Tesla Supercharger network.

Tesla opened its 100th Supercharger station earlier this month, having already made it possible for drivers of the all-electric Model S sedan to cross the US without having to worry about sufficient charge.

Use of the Superchargers is free to all Model S drivers, barring those with the entry-level model. However, the connection is different from that of the standard SAE charger which other plug-in EVs and hybrids have adopted, such as BMW’s own i3 and i8 models.

While Tesla includes an adapter for using the Model S with an SAE charger, if a Supercharger isn’t convenient, there’s no reverse adapter at this stage.

It’s not clear whether that’s something BMW brought up with Tesla during the meeting, though the German marque has been experimenting with charging systems outside of the norm. Recently, it showed off a concept solar-powered carport design that could not only recharge the i8 without resorting to the mains grid, but also power a nearby home.

Nonetheless, BMW has proved reluctant to entirely cut the cord with gasoline engines. Both the BMW i3 and i8 carry a backup generator: in effect, a small gas engine that works as a range-extender to recharge the batteries of the cars should their own range be depleted. In the case of the i8 sportscar, the gas engine can also provide a significant boost in power for performance driving.

A possibility could be that BMW will use – and even help build out – the Supercharger network for its own models, agreeing some sort of split for the cost of electricity its own drivers use. There’s also the potential for BMW to use aspects of Tesla’s powertrain technology; much of the Model S’ running gear is also used in Toyota’s RAV4 EV, for instance.

SOURCE Newsday