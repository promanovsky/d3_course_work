Washington: Researchers have said when startled by predators, tiny fruit flies respond like fighter jets - employing screaming-fast banked turns to evade attacks.

Researchers at the University of Washington used an array of high-speed video cameras operating at 7,500 frames a second to capture the wing and body motion of flies after they encountered a looming image of an approaching predator.

Michael Dickinson, UW professor of biology and co-author of a paper, said although they have been described as swimming through the air, tiny flies actually roll their bodies just like aircraft in a banked turn to maneuver away from impending threats.

He said that the team discovered that fruit flies alter course in less than one one-hundredth of a second, 50 times faster than we blink our eyes, and which is faster than we ever imagined.

Time lapse images from a high speed video shows how a fruit fly startled by a looming shadow (off camera at the bottom right) performs a rapid roll to bank away from the threat.

In the midst of a banked turn, the flies can roll on their sides 90 degrees or more, almost flying upside down at times, said Florian Muijres, a UW postdoctoral researcher and lead author of the paper.

"These flies normally flap their wings 200 times a second and, in almost a single wing beat, the animal can reorient its body to generate a force away from the threatening stimulus and then continues to accelerate," he said.

"The brain of the fly performs a very sophisticated calculation, in a very short amount of time, to determine where the danger lies and exactly how to bank for the best escape, doing something different if the threat is to the side, straight ahead or behind," Dickinson said.

The study has been published in the journal Science.