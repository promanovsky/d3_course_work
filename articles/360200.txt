Getty Images

Use the phrase "Internet of Things" and you're just as likely to get a blank stare as an exasperated sigh. Pandora, the Web's biggest radio operator, once scoffed at it too, but not for long.

As the Internet integrates into the machines around us and connects them to one another -- a shift commonly referred to as the Internet of Things -- personalization becomes more valuable to music. You don't want to keep telling your gadgets what to play as you listen to music on your computer at work, then on the radio in your car, then on your phone at the gym, then on your TV at dinner, then on your clock as you fall asleep. With its Music Genome Project analyzing song DNA, and a database of millions of people's listening habits over nearly a decade, Pandora is in an enviable position, and the company prides itself on being among the earliest and most active settlers in the realm of the smart home, smart car, smart TV -- even smart fridge.

On Tuesday, Google purchased Songza, an online radio service similar to but smaller than Pandora. Songza's primary focus is anticipating what users may want to hear based on context clues like location, time of day, weather, and past behavior. That focus on context parlays well into Google's push to make its Android operating system the go-to platform for every kind of device, a campaign on display last month at the company's I/O developers conference.

As in past instances of a tech giant venturing onto its turf, Pandora -- the Web's biggest radio service by number of listeners -- isn't sweating more competition.

Ian Geller, Pandora's vice president of business development, has led the consumer-electronics partnerships there for eight years, stretching back to its first integrations in speakers. Geller spoke to CNET News about the cues he took from the Songza purchase and Google I/O, and how far the Internet's penetration into mundane machines has come. The following is an edited Q&A.

Q: What does Google's purchase of Songza say to you about the state of the Internet of Things and where Google is positioning itself within it?

Geller: No question, the signal that Google sent with that acquisition is reinforcing what we've been saying for years now: Personalized radio really is the primary way that people want to access and listen to their music. I look at it as a thumbs-up and an acknowledgment that they needed to be in the space, because this is the space to be in.

Pandora has seen tech giants with big war chests launching rivals before. Songza is small, but is there anything about Google's acquisition this week that changes your perspective about the competitive landscape?

Geller: We continue to focus 100 percent on our core product, which is delivering the best personalized radio in the world. We've been laser focused on that for 10 years, and the intellectual property we've amassed in that time puts us at a significant advantage over anyone who attempts to get into this space right now. Even for a company like Songza, who has been doing it for a few years.

Did Google I/O signal any changes for you about progress toward this vision of all devices and machines connecting and speaking with each other?

Geller: Google is taking the same approach as Pandora relative to defragmentation in the smart home. Chromecast was the first step, the next step is what they announced at I/O, which was Android TV. This is an operating system that they will make available to any number of hardware manufacturers to integrate into the television itself.

It's really exciting for us, because it's another indication from Google about how important they see the connected home. It used to be all these products would have very different operating systems. Or they might have all been based on Android but there wasn't a lot of commonality between them, so just because you built an app for one device didn't necessarily mean that the app was very easy to port to another device.

The statement from Google loud and clear is that, yes, the smartphone is their primary focus, but they recognize how important it is to provide a common framework to work across a wide variety of devices. Which is amazing, right? It really is that vision of connecting disparate devices. We continue to work very closely with Google on all of their other innovations, including Android TV and Wear. We're continuing to discuss opportunities with them in that space.

How have you seen the concept of the Internet of Things evolve from the vantage point of Pandora?

Geller: Because we've been involved in this space for so long, we've really seen it change. Originally, people were buying these devices and they just happened to have connectivity. And not only were you lucky if people even tried these services, you were lucky if people plugged these things into the Internet. The connect rates were ridiculously low, particularly since a lot of these early ones didn't even have Wi-Fi. It was like Ethernet -- try explaining that to my mom or dad. And if you got through all the hoops of plugging this thing in, using it was a bit of a nightmare.

But we knew historically something like 30 percent of all radio listening is in the home, so it was a category we needed to be in. The earliest indications, even when these devices were hard to use, was that the engagement with Pandora was through the roof. We were seeing 3 hours of listening a day. Today, people are spending roughly four and half hours a day watching TV, and back then it was lower. It meant people were listening to Pandora almost as much as people are watching TV on their TVs.

Now everything is Wi-Fi connected, and every year these devices become easier and easier. It's gotten to the point where my mom and dad have figured out to how to use them.

The mom test is everyone's bellwether.

Geller: Exactly. It used to be connectivity just happened to be there, now it's "I want connectivity." The usage -- we're up to something like 3.5 hours of usage per user per day on some devices -- it's significantly higher.

When Samsung came to us and were like "Look we're building a connected refrigerator and we want Pandora on it." I think the immediate reaction was, you could hear our eyes roll. But we stopped and thought about it, and thought how people spend a lot of time in their kitchens, and it makes sense.

If Samsung came to me and said "We want to put Pandora in one of our washer-dryers," I might think twice, though.

Correction, July 7 at 11:20 a.m. PT: Geller misspoke about Samsung's proposal to put Pandora on a connected device. Samsung proposed a connected fridge, not a connected TV.