PITTSBURGH (KDKA) — Nickel allergies affect 20 percent of the general population, which could cause problems if your electronic device has the metal in it.

Recently, an 11-year-old child developed a rash that doctors determined was caused by his iPad which tested positive for Nickel.

Exposure is rarely life threatening, but for those who are allergic it can cause problems.

Dr. Robin Gehris is the Chief of Pediatric Dermatological surgery for Children’s Hospital.

She’s seen first-hand what happens when a child allergic to Nickel is exposed.

Dr. Gehris says, “It can swell and if the inflammation has been going on for a long time it can sometimes ooze some clear fluid or even worse case become secondarily infected.”

It’s called contact dermatitis and according to a new report from the American Academy of Pediatrics educates dermatologists on the new risks associated with Nickel exposure from electronic devices.

The result is an extremely itchy rash which is typically treated with a topical cream.

The solutions are pretty simple.

You can either use a Dimethylglyoxime test which turns the metal temporarily pink to determine if your device has the metal.

You can also use a cover or even duct tape to conceal the metal.

RELATED LINKS

More Local News

Join The Conversation, Like KDKA On Facebook

Follow KDKA On Twitter