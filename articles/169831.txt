Beyoncé and Jay Z have officially announced the dates for their joint summer tour and will kick off the venture in Miami in June. The hip-hop couple will stop in at least 13 cities across America beginning June 25 and wrap up on Aug. 5 in San Francisco.

Along the way Beyoncé and Jay Z will perform in a number of big cities including Dallas, Philadelphia, Toronto and Chicago. The duo has not yet announced dates for the Barclays Center in Brooklyn, N.Y. but will perform at the Metlife Stadium in New Jersey on July 11.

According to the New York Daily News, the "Grown Woman" singer revealed more dates while on her "Mrs. Carter Show World Tour," so Bey and Jay could announce extra tour dates for their "On The Run" joint tour later on.

General ticket sales will be open to the public starting Friday, May 2 but members of Beyoncé's "Beehive" can purchase tickets as early as Tuesday, Apr. 29, the Daily News reports. News of the couple's tour was first reported a few weeks ago by the New York Post's Page Six.

At the time, a source told the newspaper that Beyonce and her husband would be going on a 20-stadium summer tour. So far only 16 dates were announced. A source told US Weekly, that the two are expected to begin rehearsals in mid-May.

One song fans can expect the couple to sing is "Drunk in Love" from Bey's latest self-titled album. The couple, who are parents to Blue Ivy, performed the hit song a few months ago at the Grammy Awards. It was the first time the couple had performed the song together and the sexy performance received mostly negative reviews for being too raunchy.

Check out the tour dates and cities below:

June 25- Sun Life Stadium in Miami, Fla.

June 28- Great American Ball Park in Cincinnati, Ohio.

July 1- Gillette Stadium in Foxborough, Mass.

July 5- Citizens Bank Park in Philadelphia, Pa.

July 7- M&T Bank Stadium in Baltimore, Md.

July 9- Rogers Centre in Toronto, Ontario.

July 11- Metlife Stadium in East Rutherford, N.J.

July 15- Georgia Dome in Atlanta, Ga.

July 18- Minute Maid Park in Houston, Texas.

July 20- Mercedes-Benz Superdome in New Orleans, La.

July 22- AT&T Stadium in Dallas, Texas.

July 24- Soldier Field in Chicago, Ill.

July 27- Investors Group Field in Winnipeg, Canada.

July 30- Safeco Field in Seattle, Wash.

August 2- Rose Bowl in Los Angeles, Calif.

August 5- AT&T Park in San Francisco, Calif.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.