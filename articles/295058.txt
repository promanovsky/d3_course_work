Amazon will unveil a mystery product, widely expected to be a smartphone on June 18, 2014. File picture shows Amazon.com CEO Jeff Bezos at a launch event for the Bezos Center for Innovation at the Museum of History and Industry in Seattle, Washington. — AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

WASHINGTON DC, June 17 — Amazon’s mystery unveiling tomorrow is widely expected to be a smartphone, which if managed well could shake up the market and boost the US online giant as a device maker.

The company has given few hints, inviting a small group of media to the event to be hosted by Amazon founder and chief executive Jeff Bezos.

The announcement contained an image of what appeared to be the edge of a smartphone — a device long rumoured to be in the works by the Seattle, Washington firm, which is a retailer of a multitude of goods and digital content.

“If it is a smartphone, Amazon would automatically be considered one of the top players before it even ships its first phone,” said Gerry Purdy, chief mobile analyst for Compass Intelligence.

Purdy said Amazon’s market clout, large customer base and willingness to sell devices at low profit margins gives the company an opportunity to get a foothold in this key segment.

“They have a lot of tools,” Purdy told AFP. “They can give free access to content, access to music, or they could credit your monthly phone bill if you spend enough with Amazon.”

Amazon has worked to keep customers close with its Amazon Prime subscription service, for US$99 (RM320) a year. This gives consumers free delivery for goods, as well as access to Amazon’s streaming video service. Last week, the company added streaming music for no additional charge.

But Amazon still needs to deliver a high-quality handset and offer enough apps to compete with the popular devices from Apple and makers such as Samsung which use the Google Android platform.

Hints from Bezos

Several reports have said Amazon’s new phone would have 3D technology, making for a richer, multidimensional display that could differentiate it from rivals.

In a cryptic message to journalists attending the event, Bezos sent copies of his favourite childhood book, “Mr. Pine’s Purple House,” which is about one house that stands out from the others.

Forrester Research analyst James McQuivey said Amazon is not interested in taking away a lot of Apple and Samsung market share, but in keeping its customers more engaged.

“Amazon wants to be in your pocket,” he told AFP. “It wants to be with you in your hand’s reach whenever you think about something you want to purchase.”

McQuivey sees Amazon following a course similar to what it has done with the Kindle tablets — selling at low prices to drive the purchase of more Amazon content, including books, electronics and streamed films or music.

“The Kindle Fire tablet has never gotten significant market share, but for the people who own them, it made them more connected to Amazon,” McQuivey said.

Mark Mahaney at RBC Capital Markets said Amazon’s strategy for Kindle devices appears to be working.

“Kindle device owners spend almost 30 per cent more with Amazon than non-Kindle owners — US$457 per year vs. US$361 per year — (which provides) a bit of an explanation as to why Amazon may be shortly unveiling a smartphone,” Mahaney said in a note to clients.

An ‘uphill climb’

Still, some analysts say Amazon is taking a risk in a new, fiercely competitive, segment of the market.

“It’s going to be an uphill climb,” said Ramon Llamas, a mobile analyst for IDC.

Llamas said a majority of US customers, and many in other parts of the world, already have a smartphone or have considered an iPhone or Android handset.

“Consumers are going to want to know how many applications there are,” he told AFP.

Amazon uses a modified version of Android, which keeps its system separate from the main marketplace offered by Google.

It claims to have 240,000 apps, compared with over a million for iOS and Android, although some Android apps may work on Amazon devices.

Llamas said Amazon may have a hard time getting consumers to switch out of iOS or Android and could face similar troubles as Windows and BlackBerry, whose phones lack a broad ecosystem.

He added that some consumers might want to use an Amazon phone for shopping, but that consumers have a much deeper relationship with their phones.

“It’s not shopping all the time,” he said. — AFP