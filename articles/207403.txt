“When [Chandler Bats] sent me [pink bats], I was surprised because Louisville was the only company that could,” McLouth said.

McLouth wasn’t in Sunday’s starting lineup against Oakland left-handed starter Scott Kazmir, but if he plays, he hopes to use his pink bat. Last season, McLouth wanted to use his pink bat to honor his mother and breast cancer survivors, but couldn’t because of MLB rules.

AD

Adam LaRoche was also prevented from using his pink stick last season. Nick Markakis of the Orioles and Trevor Plouffe of the Twins, both sons of breast cancer survivors, also voiced their displeasure with the rule.

AD

Last season, Louisville Slugger made a donation to Susan G. Komen for the Cure, the MLB’s charitable partner, to earn the exclusive rights. Pink bats made by companies other than Louisville Slugger could be used on Mother’s Day if a charitable donation was made to the Komen foundation. If a player used a banned bat in a game, the player wouldn’t be subject to fines or discipline. The bat manufacturer, however, would be in violation of rules. A player could use a pink bat but without the manufacturer’s logo.

According to a Fox Sports report earlier this week, MLB loosened the rules at Commissioner Bug Selig’s request. Under the new rules, companies other than Louisville Slugger are no longer required to donate to a breast-cancer charity if they make pink bats for use on Mother’s Day. Contributions are, however, encouraged.

AD

“I know some companies pay a lot of money to have exclusivity,” McLouth said. “But really? For one day, especially for a cause like this, I’m glad that they changed that. That might have been worse than the transfer rule.”