Accra - Health authorities in Ghana are testing blood samples from a 12-year-old girl who died of a viral fever with bleeding in the country's first suspected case of Ebola, officials said on Sunday.

More than 90 people have died of Ebola in Guinea and Liberia and there is a reported case in Mali. Medical charity Medicins Sans Frontieres has warned of an unprecedented epidemic in an impoverished region with weak health services.

Samples from the girl were taken from the Komfo Anokye Teaching Hospital in Kumasi, Ghana's second-largest city, to a medical research centre in the capital Accra, Dennis Laryea, head of public health at the teaching hospital, told Reuters.

“We are working around the clock to confirm whether it is that (Ebola) or not and once we are able to confirm, we'll put all the necessary information out,” Laryea told Reuters, adding that he hoped for results within 24 hours.

He said the girl died of a viral hemorrhagic fever, which could be traced to various diseases including Ebola, but declined to give details about her background.