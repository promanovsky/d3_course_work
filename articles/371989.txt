The Associated Press

NEW YORK (AP) — U.S. drugmaker AbbVie has boosted its offer for Shire by 11% to about $51.3 billion, hoping it will be enough to pique the interest of its British counterpart.

Shire, known for its rare-disease drugs, has rejected three AbbVie offers to date.

North Chicago, Illinois-based AbbVie is now making an unsolicited offer of 51.15 British pounds ($87.57) per Shire share in cash and stock. Its prior bid was worth about 46.26 pounds ($79.20) per share for a total value of 27 billion pounds ($46.2 billion).

AbbVie said that its revised proposal also increases the ownership held by Shire shareholders to about 24% of the new holding company. It previously offered approximately 23% ownership.

Shire said in a statement that it did not get a look at AbbVie's revised proposal before the company's announcement. Shire said its board will meet to consider the bid and that it will make a further announcement "in due course."

Last month AbbVie said that one of the reasons it was interested in Shire was that it saw a compelling tax break. AbbVie said at that time that it expected the combined company to pay a tax rate of about 13% by 2016 after AbbVie reincorporates on the British island of Jersey, where Shire is headquartered. That would be down from its current rate of roughly 22%.

Several other U.S. companies are using mergers to reincorporate in countries with lower tax rates. These moves are raising concerns among U.S. lawmakers since they can cost the federal government billions in tax revenue.

AbbVie executives have also said that the product portfolios of the two companies complement each other, and that the combination would immediately give Shire a broader geographic reach.

Shire has said that AbbVie's overtures undervalue the company and its prospects. The drugmaker has also said that its board had concerns about AbbVie's interest in making the move for tax purposes.

AbbVie, which was spun off from Abbott Laboratories last year, has until July 18 to either announce a firm offer or confirm that it won't make one under United Kingdom takeover laws.

AbbVie's stock was down 2.8% to $55.81 in afternoon trading.