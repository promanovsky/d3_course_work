Sorry, what - Hello Kitty is not actually a cat?

Hello Kitty is about to celebrate her 40th birthday, but it isn’t all streamers and candles and laughter.

Hold on to something sturdy and take a deep breath, because Hello Kitty isn’t the feline we once thought she was.

The very notion of Hello Kitty, which we’ve built our *entire lives around (*this is questionable), has turned out to be one big lie.

The truth as we knew it all started to unravel when Hello Kitty scholar Christine R. Yano was planning an upcoming exhibition for the Japanese American National Museum in Los Angeles.

Now that I know Hello Kitty is not a cat, I am worried that *my* cat is not a cat. Oh dear god, what am I living with? — bbogaev (@bbogaev) August 27, 2014

According to the LA Times, Yano was preparing written text for the exhibit when she described Hello Kitty as a cat.

But Sanrio, the company responsible for Hello Kitty, checked her text and came back with a surprise amendment.

"I was corrected - very firmly," Yano told the LA Times.

Related: Hello Kitty designer defends cute character as cat turns 40

"That's one correction Sanrio made for my script for the show. Hello Kitty is not a cat. She's a cartoon character. She is a little girl. She is a friend. But she is not a cat. She's never depicted on all fours. She walks and sits like a two-legged creature.”

Rewind – she is a little girl? With whiskers?

It gets weirder. After a quick background check, it turns out she lives in London with her parents Mary and George White and her twin HUMAN sister Mimmy.

It's time again to play CAT OR GIRL? #HelloKitty pic.twitter.com/ClBdsdpioS — Diane in 7a (@Diane_7A) August 28, 2014

According to Yano, it makes sense as Hello Kitty’s character was created for the tastes of the time.

Story continues

“Hello Kitty emerged in the 1970s, when the Japanese and Japanese women were into Britain. They loved the idea of Britain. It represented the quintessential idealised childhood, almost like a white picket fence,” LA Times reported Yano as saying.

If the thought of a world without a cuddly cat icon is too much to bear, fear not.

As it turns out Hello Kitty has a pet cat of her own.

Charmmy Kitty who was “a present from her papa” makes a far more convincing feline with a bushy tail, furry chest and a collar.