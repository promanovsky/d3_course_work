The second case of the deadly MERS virus in the U.S. has been confirmed in Florida, the Centers for Disease Control reported Monday.

A press conference from the CDC and the Florida department of Health is scheduled for 2 p.m. ET to discuss the newest case of MERS to hit the U.S.

The CDC is designating this case of MERS to be imported, meaning it was brought into the U.S. from another country. The first case of MERS in the U.S. was also imported. An Indiana man who formerly worked and lived in Saudi Arabia brought it back to the U.S.

The Indiana man was quarantined quickly. He was released from the hospital last week because he no longer was showing signs or symptoms of the virus according to Dr. Alan Kumar, chief medical information officer, Community Hospital in Munster, Ind.

The virus is newer to humans. The first case was reported in Saudi Arabia.

MERS is in the same family as the common cold and SARS. SARS, or severe acute respiratory syndrome, killed more than 800 people worldwide in 2003.

The CDC is reporting approximately 400 people have come down with MERS. However, there is questions as to whether all of those people actually have been confirmed to suffer from MERS. More than 100 have died from MERS.

There is no vaccine for MERS and it's thought to have come from an animal source. Treatment for MERS is similar to other respiratory illnesses. Officials are encouraging people to wash their hands, spray down dirty surfaces with anti-bacterial chemicals and to avoid being around others who are sick.

Since April 2012, countries with confirmed MERS cases include: France, Italy, Jordan, Kuwait, Malaysia, Oman, Qatar, Saudi Arabia, Tunisia, the United Kingdom and United Arab Emirates. In Qatar, MERS was found in camels, and in Saudi Arabia it was found in a bat.