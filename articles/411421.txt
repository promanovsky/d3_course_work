Early in his prolific career, James Gunn penned the screenplay for “The Specials,” a movie centering on the sixth or seventh most popular superheroes in the world. He has now come full circle as the writer-director of “Guardians of the Galaxy,” also about a group of oddballs–an orphan, two aliens bent on a revenge, a talking raccoon and a tree-like creature. “I love losers,” he says simply. “That’s just who my people are and that’s who I’m attracted to.” While they’re not as well-known as, say, “Captain America,” this ensemble of outcasts is earning rave reviews and already breaking box office records.

Gunn got an education in low-budget movie making via Troma Films, where he began his career, but has also worked on studio films, having written scripts for the “Scooby-Doo” movies and the acclaimed 2004 “Dawn of the Dead” remake. As he brings a specific vision to his movies, Gunn might have seemed an odd choice to assimilate into the Marvel universe. But as he told Variety, the experience proved to be fantastic–both for Gunn and the studio.

It seems like you’ve avoided directing big studio movies until now, is that fair to say?

What happened was, I was hired to do a movie called “Pets” with Ben Stiller and I couldn’t stand the situation at hand and I left; I really hated it. I felt like it was situation where I was stuck between some producers who wanted one thing and the studio who wanted another thing and the movie just kept seeming to get more and more vanilla. At a certain point, I was like, “There’s a million guys that could direct this movie and it would come out exactly the same as it would if I directed it,” and I felt useless. So I bailed. I think this is the exact opposite sort of situation. It’s a situation where I’ve been brought in by Marvel and given the chance to create a spectacle film my way, which has been an awesome opportunity.

I’m sure you’ve had other offers to do studio films before?

Listen, to make a studio movie it takes so long. I really just never really had an interest in just making a movie for the heck of it. For me, a movie has to be something that I feel passionate about the story. At this point, also it has to be something that I think has a chance to really go out there and make a difference.

Was it true when you went into the meeting with them you weren’t actually into the idea of doing “Guardians of the Galaxy”?

No I really wasn’t. To be honest, I was not really focusing on movies. I was focusing on television and video games at the point. So I was like, I don’t know, but it’s Marvel. “The Avengers” just came out and did huge, and I guess I’ll just go down and hear them out. And while I was in the meeting it became a little bit more appealing to me and then on my way home it just all sort of coalesced inside my head and I saw visually what I would do with the movie if it was in my hands. And then I wanted to do it pretty bad after that.

And then you had to pitch them on you, right?

Yeah, then I was competing with like five or six other directors who wanted the gig. That first meeting was with Jonathon Schwartz and Jeremy Latchem, and I went home and I just sort of vomited up this I don’t know what it was, 16-page document that was visually—because they weren’t hiring me to write at that point they were just hiring me to direct it—visually how I saw “Guardians of the Galaxy” and how It would be for me if I directed it. How I would make it look and feel, and I sent that off to them. I sent that document off and they were excited about it, they loved it, and thought it was perfect. So I knew right from the get-go that we saw something very similarly.

I’ve heard you say in the past you attribute a lot of your success to the fact that, in your words, you don’t give a shit. But it sounds like here you did.

Yeah, yeah, yeah. I broke my rule. I definitely broke my rule and definitely cared. I really pursued it, hardcore. I really don’t like working on things before I’m getting paid. And in this case I did. I storyboarded by hand the whole opening scene.

What was is like going from 24 shoot days and $3 million on “Super” to the budget of “Guardians”?

It’s wonderful. It’s orgasmic, to say the least. It really is. I remember one friend in particular was like, “It’s so hard, is the pressure getting to you, are you freaking out? And I’m like, No. It seems 1,000 times easier than “Super” was. You’re surrounded by the best people in the business, I can envision any shot in my head and I can make it a reality. It’s funny, I would still think about ways to save money—it’s in my breeding. I was raised making movies for $350,000 dollars. It’s part of who I am.

Was there anything you were surprised Marvel didn’t object to? I heard you were surprised they let Rocket grab his crotch?

No, I wasn’t too surprised they let me keep Rocket grabbing his crotch, it was that they let it in the trailer. We were going over what works and what didn’t work in the trailer, and I was like, “I promise if you put Rocket grabbing his crotch in the trailer, people will love it.” And they were doubtful, but they went with my suggestion and we had to really hurry work to finish the shot to get it done in time for the trailer. So I was surprised for that. I think probably my biggest surprise was that I wrote this huge role for Michael Rooker. I don’t know if they were initially that excited about Michael Rooker playing the role. Because, again, it’s a really big role, and it’s bigger than the roles of some of our really huge stars have in the movie. But he was perfect for it.

The Guardians aren’t the most well-known Marvel property. Do you see the film as a risk?

I think it’s a risk, but for reasons other than what people think. The truth is, Iron Man was not a household name. Comic book fans knew who Iron Man was, but most people didn’t. If every person that bought an Iron Man comic book went out and bought a movie ticket that movie wouldn’t have made very much money, so they had to sell it to a lot more people than that. So “Iron Man” was certainly a risky move. I think that the thing that’s risky about “Guardians” is that we’ve really made a unique film in terms of making a spectacle film. It’s got a lot of drama, a lot of comedy, and it just really does its own thing. The risky thing about it is doing something different.