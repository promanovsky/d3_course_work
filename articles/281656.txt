People with low vitamin D levels may be twice as likely to die prematurely as those who do not.

To make their findings researchers looked at 32 previous studies that looked at the connection between human vitamin D levels and mortality rates, a University of California, San Diego Health Sciences news release reported.

"Three years ago, the Institute of Medicine (IOM) concluded that having a too-low blood level of vitamin D was hazardous," Cedric Garland, DrPH, professor in the Department of Family and Preventive Medicine at UC San Diego and lead author of the study, said in the news release. "This study supports that conclusion, but goes one step further. The 20 nanograms per milliliter (ng/ml) blood level cutoff assumed from the IOM report was based solely on the association of low vitamin D with risk of bone disease. This new finding is based on the association of low vitamin D with risk of premature death from all causes, not just bone diseases."

The average age the study participants was 55 years when the blood was first drawn; they also participated in a nine-year follow-up.

The amount of vitamin D in the blood was linked to about half of the death rate that was 30 ng/ml; about two-thirds of the U.S. population has a blood vitamin D level below this threshold.

"This study should give the medical community and public substantial reassurance that vitamin D is safe when used in appropriate doses up to 4,000 International Units (IU) per day," Heather Hofflich, DO, professor in the UC San Diego School of Medicine's Department of Medicine, said in the news release.

Patients are urged to consult their doctors about vitamin D intake and to have their 25-hydroxyvitamin D tested every year. Some patients may need up to 14,000 IU per day under medical supervision and recommendation.

The average age of the study participants was 55 years at the time when the blood was first drawn; they also participated in a nine-year follow-up.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.