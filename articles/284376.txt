NEW YORK (TheStreet) -- Shares of Tesla Motors Inc. (TSLA) - Get Report are down -1.25% to $205.58 as Barron's pointed out today that CEO Elon Musk said he was considering giving away some of the automaker's patents.

Citing the BBC, the article in part said: "When asked by the BBC...if he was considering giving technology away, Mr. Musk said 'you're on the right track.' Mr. Musk said he hoped to break down technological barriers to help speed up electric car adoption."

Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates TESLA MOTORS INC as a Hold with a ratings score of C-. TheStreet Ratings Team has this to say about their recommendation:

"We rate TESLA MOTORS INC (TSLA) a HOLD. The primary factors that have impacted our rating are mixed some indicating strength, some showing weaknesses, with little evidence to justify the expectation of either a positive or negative performance for this stock relative to most other stocks. The company's strengths can be seen in multiple areas, such as its solid stock price performance, revenue growth and notable return on equity. However, as a counter to these strengths, we also find weaknesses including unimpressive growth in net income, weak operating cash flow and poor profit margins."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

This stock has managed to rise its share value by 116.94% over the past twelve months. Regarding the stock's future course, our hold rating indicates that we do not recommend additional investment in this stock despite its gains in the past year.

TSLA's revenue growth trails the industry average of 22.7%. Since the same quarter one year prior, revenues rose by 10.4%. This growth in revenue does not appear to have trickled down to the company's bottom line, displayed by a decline in earnings per share.

TESLA MOTORS INC's earnings have gone downhill when comparing its most recently reported quarter with the same quarter a year earlier. This company has reported somewhat volatile earnings recently. But, we feel it is poised for EPS growth in the coming year. During the past fiscal year, TESLA MOTORS INC continued to lose money by earning -$0.71 versus -$3.70 in the prior year. This year, the market expects an improvement in earnings ($1.21 versus -$0.71).

Net operating cash flow has declined marginally to $60.64 million or 5.36% when compared to the same quarter last year. In addition, when comparing the cash generation rate to the industry average, the firm's growth is significantly lower.

The company, on the basis of change in net income from the same quarter one year ago, has significantly underperformed when compared to that of the S&P 500 and the Automobiles industry. The net income has significantly decreased by 542.7% when compared to the same quarter one year ago, falling from $11.25 million to -$49.80 million.

You can view the full analysis from the report here: TSLA Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.