AP Top Stories June 30 a

Here's the latest news for Monday, June 30th: Oscar Pistorius trial resumes after one-month break; High winds damage buildings in Wisconsin; Two saved from rubble of building collapse in India; Thousands march in NYC gay pride parade.