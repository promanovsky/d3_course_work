Osmo turns iPad into AR arena for kids

Osmo is an ecosystem of learning tools – apps and accessories – that transform the Apple iPad into a kid-friendly center for augmented reality. Two key components are a stand – holding the iPad up straight – and a mirror, allowing the iPad’s front-facing camera to work with the area in front of the tablet. From there, it’s all about the apps.

When released later this year, Osmo will bring with it three unique apps for the iPad. Tangram, Words, and Newton will each also have their own physical package of materials, including paper and/or plastic shapes.

As you’ll see above, Osmo allows your child – or you, if you love Augmented Reality to such a degree – to interact with both physical elements and the iPad’s digital brain. Below you’ll see an image of the boxes that’ll be released when the Osmo base package is released later this year.

You’ll notice there are only three boxes, one of them for the Base Osmo set. Tangram’s box contains all of the plastic shapes you’ll need to construct the various puzzles presented by Osmo.

Words has a number of plastic chips with letters on them. Players will arrange letters in combinations that spell words associated with the images presented to them on the iPad.

The Newton app works with paper and a marker, if you like, but you’ll also be able to use any oddity you’ve got lying around. In the demonstration video above, one child drops a toy dinosaur into the image, instantly seeing the beast appear onscreen.

Osmo is currently in a funding phase and is available for pre-order for half of what the company plans on charging for it: $49 plus $8 shipping. The only component you’ll need (besides the Osmo Base) is the iPad 2, 3, 4, mini, mini Retina, or iPad Air.