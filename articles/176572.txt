Google is looking to expand its footprint in the same-day delivery business.

The search giant announced Monday that it has expanded Google Shopping Express, its same-day delivery service, into Manhattan and West Los Angeles. Google is working to bring the delivery service to Brooklyn and Queens in New York and Bel-Air, Beverly Hills, West Hollywood and other parts of Los Angeles "in the coming months."

The program launched in beta in parts of San Francisco just over a year ago and expanded to the entire Bay Area later that year.

"We opened the service to shoppers in the San Francisco Bay Area last year and now we’re ready to bring the service to both coasts, starting with West Los Angeles and Manhattan," Jenna Owens, GM of Operations for Google Shopping Express, wrote in a blog post.

Google offers the option to purchase goods from a number of national chains including Whole Foods, Staples, Walgreens and Costco (if you have a membership). You get $10 off with your first order and six months without delivery charges if you opt in to the trial program.

Here's what you see after placing a delivery order on Google Shopping Express.

When you visit the Google Shopping Express website, you'll need to select your city, provide billing information for your Google Wallet account if you haven't already and then select a time slot for delivery. The earliest time slot was already "sold out" for Manhattan when we tried it Monday morning.

As Mashable has previously reported, Google doesn't actually operate its own fleet of couriers. Instead, the company works with partners who wear Google Shopping Express uniforms and drive Google delivery trucks.

The same-day delivery space has been heating up in recent years. Amazon recently expanded its same-day delivery option to San Francisco and Dallas. eBay said last year that it plans to broaden its service, eBay Now, to 25 cities this year. Same-day delivery startups like Instacart are expanding into new cities as well.