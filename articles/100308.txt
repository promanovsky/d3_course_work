WINSTON-SALEM, N.C., April 11, 2014 /PRNewswire-USNewswire/ -- Scientists reported today the first human recipients of laboratory-grown vaginal organs. A research team led by Anthony Atala, M.D., director of Wake Forest Baptist Medical Center's Institute for Regenerative Medicine, describes in the Lancet long-term success in four teenage girls who received vaginal organs that were engineered with their own cells.

"This pilot study is the first to demonstrate that vaginal organs can be constructed in the lab and used successfully in humans," said Atala. "This may represent a new option for patients who require vaginal reconstructive surgeries. In addition, this study is one more example of how regenerative medicine strategies can be applied to a variety of tissues and organs."

The girls in the study were born with Mayer-Rokitansky-Küster-Hauser (MRKH) syndrome, a rare genetic condition in which the vagina and uterus are underdeveloped or absent. The treatment could also potentially be applied to patients with vaginal cancer or injuries, according to the researchers.

The girls were between 13 and 18 years old at the time of the surgeries, which were performed between June 2005 and October 2008. Data from annual follow-up visits show that even up to eight years after the surgeries, the organs had normal function.



"Tissue biopsies, MRI scans and internal exams using magnification all showed that the engineered vaginas were similar in makeup and function to native tissue, said Atlantida-Raya Rivera, lead author and director of the HIMFG Tissue Engineering Laboratory at the MRKH in Mexico City, where the surgeries were performed.

In addition, the patients' responses to a Female Sexual Function Index questionnaire showed they had normal sexual function after the treatment, including desire and pain-free intercourse.

The organ structures were engineered using muscle and epithelial cells (the cells that line the body's cavities) from a small biopsy of each patient's external genitals. In a Good Manufacturing Practices facility, the cells were extracted from the tissues, expanded and then placed on a biodegradable material that was hand-sewn into a vagina-like shape. These scaffolds were tailor-made to fit each patient.

About five to six weeks after the biopsy, surgeons created a canal in the patient's pelvis and sutured the scaffold to reproductive structures. Previous laboratory and clinical research in Atala's lab has shown that once cell-seeded scaffolds are implanted in the body, nerves and blood vessels form and the cells expand and form tissue. At the same time the scaffolding material is being absorbed by the body, the cells lay down materials to form a permanent support structure – gradually replacing the engineered scaffold with a new organ.

Followup testing on the lab-engineered vaginas showed the margin between native tissue and the engineered segments was indistinguishable and that the scaffold had developed into tri-layer vaginal tissue.

Current treatments for MRHK syndrome include dilation of existing tissue or reconstructive surgery to create new vaginal tissue. A variety of materials can be used to surgically construct a new vagina – from skin grafts to tissue that lines the abdominal cavity. However, these substitutes often lack a normal muscle layer and some patients can develop a narrowing or contracting of the vagina.

The researchers say that with conventional treatments, the overall complication rate is as high as 75 percent in pediatric patients, with the need for vaginal dilation due to narrowing being the most common complication.

Before beginning the pilot clinical study, Atala's team evaluated lab-built vaginas in mice and rabbits beginning in the early 1990s. In these studies, scientists discovered the importance of using cells on the scaffolds. Atala's team used a similar approach to engineer replacement bladders that were implanted in nine children beginning in 1998, becoming the first in the world to implant laboratory-grown organs in humans. The team has also successfully implanted lab-engineered urine tubes (urethras) into young boys.

The team said the current study is limited because of its size, and that it will be important to gain further clinical experience with the technique and to compare it with established surgical procedures.

Co-researchers were James J. Yoo, M.D., Ph.D., and Shay Soker, Ph.D., Wake Forest Baptist, and Diego R. Esquiliano M.D., Reyna Fierro-Pastrana P.hD., Esther Lopez-Bayghen Ph.D., Pedro Valencia M.D., and Ricardo Ordorica-Flores, M.D.,Children's Hospital Mexico Federico Gomez Metropolitan Autonomous University, Mexico.

Logo - http://photos.prnewswire.com/prnh/20111114/DC06171LOGO

SOURCE Wake Forest Baptist Medical Center