Planned Parenthood Tells Underage Girl to Experiment With Whips, Asphyxiation; Go to Sex Shop (Video) Planned Parenthood Tells Underage Girl to Experiment With Whips, Asphyxiation; Go to Sex Shop (Video)

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Pro-life group Live Action released its second undercover video Tuesday, titled, "Sexed: Planned Parenthood's Dangerous Advice for Kids," which shows a counselor advising an underage girl to go to a sex shop as she describes how to start experimenting with bondage and the various fetishes people have.

Even though it is against municipal law for a minor to enter a sex store, according to Live Action, the Planned Parenthood counselor tells the undercover 15-year-old that there are "tons of sex shops around here" that she and her boyfriend can visit in order to try different sex acts.

The counselor is also seen advising the young girl to come up with a safe word as she experiments with BDSM sex, which is described as: bondage and discipline/dominance and submission/sadism and masochism. A safe word is used to let the partner know that the sexual act is hurting them.

The Live Action site PlannedParenthoodExposed.com also lists resources Planned Parenthood provides to teens through its website, Go Ask Alice!, which they assert describes "how sexual partners can beat each other."

Live Action also cites Planned Parenthood's sponsorship of a seminar for educators called "50 Shades of Safe," which teaches "how a BDSM relationship is successfully navigated between sexual partners."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"The Planned Parenthood-approved advice of 'do whatever you want, but just use a safe word' glorifies kids acting out rape scenarios. It's extremely dangerous counseling," said Lila Rose, Live Action's president, in a statement shared with The Christian Post.

To illustrate how dangerous it is, the latest Live Action video features a real 911 call that was made by the mother of a 16-year-old Colorado girl who killed her 43-year-old lover because he didn't use their "safe word" while she asphyxiated him with an electrical cord. The mother told the 911 operator that the couple was also into "mutilation" during sex.

Planned Parenthood of the Rocky Mountains, the affiliate featured in Live Action's undercover investigative report, received $2,302,026 in government grants, according to its 2011-2012 990.

Lakewood Health Center, an affiliate of that clinic, is also featured in part two of Live Action's ongoing investigative expose.

"Planned Parenthood of the Rocky Mountains is already bragging about receiving Obamacare grants specifically for sex education," Rose asserted.

"Almost half of Planned Parenthood's budget comes from taxpayers and additional taxpayer funds are going to Planned Parenthood under Obamacare for teen sex education. Obamacare appropriates an additional $75 million a year for 'sex counseling,'" she continued.

Last month, Live Action released a video compilation that shows Planned Parenthood staff talking to an underage girl about bondage and sadism, circumventing statutory rape laws, and recommending a clinic that will perform illegal abortions on sex trafficked girls age 14 and younger.

"Here's an abortion corporation, which gets 45 percent of its budget from the taxpayers, telling 15 and 16 year olds not only to have sex, but also to choke each other in the process," said Rose, whose organization is calling on Colorado residents to contact their local officials, including school administrators, to end school districts' relationship with Planned Parenthood.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit