By Jim Donovan

PHILADELPHIA (CBS) — Deceptive advertisements that promise to help you lose weight are everywhere and some lawmakers in Washington D.C. are tired of it. As 3 On Your Side Consumer Reporter Jim Donovan tells us, they held a hearing today to address the issue and they called on a popular TV show host for help.

“You are being made an example of today, because of the power you have in this space,” said Missouri Senator Claire McCaskill as she criticized Dr. Mehmet Oz for using his television fame to promote weight loss aids. McCaskill says, “I don’t get why you feel the need to say this stuff, because you know it’s not true.” Dr. Oz replied by saying, “I actually do personally believe in the items I talk about in the show. I passionately study them.” He adds, “In attempt to engage viewers I use flowery language.”

Products such as Sensa and Pure Green Coffee have been fined by the Federal Trade Commission for false ad claims. Dr. Oz says companies like Pure Green Coffee often use clips from his show without his permission to promote a product. He says the biggest mistake he’s made is not telling viewers who to trust. According to Oz, “I should have been savvy enough to say to myself, and I kick myself still and maybe I’ll do it in the future, that I should say, here are the companies I trust. Just go buy their products, because they’re not going to scam you, they’re not going to make illegal claims.”

Dr. Oz told the Senate subcommittee he will be part of the solution. He maintains that he has never endorsed specific brands nor has he received money for the sale of any supplements.

For more information on the FTC’s charges against Pure Green Coffee, click here.

For more information on the FTC’s charges against Sensa, click here.