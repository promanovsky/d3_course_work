Japan’s whaling efforts in the Antarctic have long pitted wildlife conservationists against whalers who say their efforts are for scientific research. Those on the anti-whaling side of the “whale wars” saw a huge victory on Monday when an international court ruled that Japanese whaling is not for science and temporarily suspended whalers from hunting in the Antarctic.

The 16-judge panel of the International Court of Justice, the judicial arm of the U.N., ruled 12 to 4 that Japan’s killing of Antarctic whales -- whose meat ends up sold in shops and restaurants -- was unjustified.

“The court concludes that the special permits granted by Japan for the killing, taking, and treating of whales ... are not ‘for purposes of scientific research,’” the court’s Presiding Judge, Peter Tomka of Slovakia, said Monday, according to the Associated Press.

The decision marked a major victory for Australian environmentalists, who filed a lawsuit with the U.N.’s highest court against Japan in 2010 for the country’s whaling practices.

“I’m absolutely over the moon, for all those people who wanted to see the charade of scientific whaling cease once and for all,” former Australian environment minister Peter Garrett, who helped launch the suit four years ago, told Australian Broadcasting Corp. radio. “I think [this] means without any shadow of a doubt that we won’t see the taking of whales in the Southern Ocean in the name of science.”

Japanese Foreign Affairs Ministry spokesman Noriyuki Shikata told reporters that while Japan will “respect” the court’s decision, the country is “deeply disappointed” by the ruling. Japanese officials have maintained that the country’s actions are completely legal under the International Whaling Commission, or IWC.

Here’s a brief history of the events that led to Monday’s landmark ruling.

1946: International Whaling Commission Established Under the International Convention for the Regulations of Whaling, which was signed in Washington, D.C., on Dec. 2, 1946, the International Whaling Commission, or IWC was formed. Its stated purpose was to "provide for the proper conservation of whale stocks and thus make possible the orderly development of the whaling industry."

As of July 2013, the commission had 88 international state members, including the U.S., Australia and Japan.

1982: The IWC Bans Commercial Whaling The IWC put a moratorium on commercial whaling. However, the ban allowed room for exceptions, including for aboriginals in the Arctic who hunt whales for subsistence. It also permitted member nations to issue permits to kill whales for scientific purposes.

Several other nations opposed the moratorium, including Norway and Iceland who continue to whale under their own quotas.

Japan continued to whale under the guise of scientific research. Its whaling efforts are carried out by the Institute of Cetacean Research.

1994: Australia Becomes Vocal About Its Opposition To Japanese Whaling Australia was an early critic of Japan’s “scientific” whaling. In 1994, Australia established a 200-nautifical-mile area around the country’s Antarctic territory called an exclusive economic zone. It meant that only Australia had rights to any marine resources found in that zone. It included part of the Southern Ocean where whaling was known to occur.

2008: Australia Launches Surveillance Mission the Australian government made plans to monitor Japanese whalers around Australian waters in order to gather evidence it could use to possibly take Japan to international court. The reconnaissance mission brought back images of Japanese vessels harpooning and killing several different whales.

The investigation sparked public outrage and spawned an international movement to end Japanese whaling in Antarctica.

2010: Australia Sues Japan In International Court On May 31, 2010, Australia filed a lawsuit against Japan alleging that the country’s whaling efforts were not for scientific purposes like they claimed.

The lawsuit read: “Japan’s continued pursuit of a large-scale program of whaling under the Second Phase of its Japanese Whale Research Program under Special Permit in the Antarctic is in breach of obligations assumed by Japan under the International Convention for the Regulation of Whaling . . . , as well as its other international obligations for the preservation of marine mammals and the marine environment.”

2013: The Case Goes To Court The case was heard before the International Court of Justice in The Hague, Netherlands, between June 26, 2013, and July 6, 2013.