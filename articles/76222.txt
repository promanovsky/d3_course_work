A pink mohawk worn by Michigan high school student Mike Barker was intended to show support for his mother, who is suffering from breast cancer. But the track team coach did not mind the student wearing a mohawk, it’s when it was dyed pink that it suddenly became a distraction, and Barker was then banned from competing unless he shaved himself.

In a related report by The Inquisitr, a HOA forced a breast cancer survivor to remove her pink ribbon from inside her own ribbon because it supposedly violated homeowners rules and offended people.

The story began when Barker’s mother, Wendy Pawlicki, made this suggestion:

“You know what you should do for me? You should dye your hair pink and make a Mohawk for me.”

So in this case it was the parent, not the child, who came up with the idea for supporting her during her cancer fight. It also was not the hairstyle since Mike had worn a mohawk for weeks without incident. But when his hair became pink the school suddenly decided he did match up to their grooming and appearance policy:

“[My coach] told me I can’t have pink hair with Mohawk to support my mom with breast cancer. I want this to change. That’s my goal.”

Both the coach and the principal told Mike he would need to shave or he could not compete. In response, other students protested the pink mohawk ban by showing up to school sporting the same style. So all of these students were not allowed to compete in the track meet and students from other schools began supporting Mike’s mother.

Regardless, the school is standing firm behind their decision and they released this statement:

“The West Iron County Public Schools supports the needs of the students to express themselves, as long as their expression does not interfere with the educational process. We also strive to balance individualism of athletes with the concept of team that fit within our athletic code. The athletic department is going to work with the student and parents to resolve this issue. The student was not dismissed from the team and we welcome his participation in the future. The athletic department has long supported breast cancer awareness by participating in numerous events involving student athletes and staff held in October (Breast Cancer Awareness Month) and plan to continue to support this worthy cause.”

Do you think the school was wrong to ban a pink mohawk intended to support a mother suffering from breast cancer? Or do you think it’s fair for schools to demand dress codes that overrule all special circumstances?