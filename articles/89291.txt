The plot thickens in Game of Thrones Season 4, and with it comes a whole new set of characters. As well as catching up with the cast we know and love, Game of Thrones Season 4 Episode 1 introduced us to a few new cast members, and we can tell they’re gonna be good.

Ed Skrein has ditched GoT to appear in the new Transporter movie

The man mountain that is Ed Skrein has left GoT to pursue a career in the footsteps of Jason Statham. Replacing him as Daario Naharis, guard of Daenerys Targaryen and general badass, is Michiel Huisman. Huisman is a Dutch actor, who previously starred as Sonny on HBO’s Treme and as Liam McGuinnis on ABC’s Nashville. We were a little surprised to see the recast as we’d thought that Skrein and Emilia Clarke, who plays Daenerys had some great chemistry. When Huisman cheekily presented Daenerys with an array of flowers in last night’s episodes, our doubts were removed. We can’t wait to see some more interaction between these two.

The most ominous addition to the Game of Thrones cast is Pedro Pascal, who has been cast as Oberyn Martell, or the ‘Red Viper’ to those that have met him in battle. Intent on vengeance for the violent death of his sister Elia Martell, wife of Rhaegar Targaryen, who was raped and killed by the Lannisters, we have a sneaking suspicion that Oberyn is going to be a key player in GoT Season 4.

This is English actress Indira Varma’s second stint in an HBO series, her first being as Niobe in HBO’s Rome back in 2005. Varma has been cast as Oberyn Martell’s “paramour”, Ellaria Sand, and it seems as though the couple have quite the risque relationship. We first met the pair in the King’s Landing brothel, picking out a prostitute to join them.

Yuri Kolokolnikov has been cast as the Magnor of Thenn

Last night we met the Magnar of Thenn, Styr, for the time first time, played by 23 year old Russian actor Yuri Kolokolnikov. The Thenn certainly don’t look like a bunch to be reckoned with, particularly after we saw them roasting the arm of a crow on the spit for their dinner.

Recast as The Hound’s big brother, Ser Gregor Clegane, is Icelandic strongman Hafþór Júlíus “Thor” Björnsson. This is Björnsson’s first acting job, although we doubt anyone is going to be too critical of the 6 foot 8, 400 pound budding actor. Clegane was the knight who raped and murdered Elia Martell, we have a feeling that he and Oberyn Martell may come into contact at some point over the coming season.

What did you make of the first episode of Game of Thrones Season 4? Did it live up to your expectations? It certainly did ours!

More: Game of Thrones fans crash HBO website

More: Pedro Pascal hurt himself with his own Game of Thrones weapon

Watch Game of Thrones' Emilia Clarke talk about her role in 'Dom Hemingway' and Jude Law:

