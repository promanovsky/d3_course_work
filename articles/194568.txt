By now you may have seen or heard about those buzzy Willow Smith and Moises Arias pics posted on Instagram in which Willow, 13, is pictured lying on a bed with her shirtless pal, who's 20.

The photos sparked some debate, but one person who doesn't mind them at all is Willow's mom, actress Jada Pinkett Smith.

On Wednesday, TMZ caught up with Pinkett Smith as she was arriving to LAX in Los Angeles. Her response? No big deal.

"Here's the deal," she said, "There was nothing sexual about that picture or that situation. You guys are projecting your trash onto it. You're acting like covert pedophiles and that's not cool."

Tell us: What do you think about the shots?