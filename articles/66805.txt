Windows XP users have just over a week to get their microsoft upgrade, the company will no longer be supporting the system after April 8, 2014. Anyone needing to make the switch is urged to visit the Windows website for more information.

For many people the upgrade comes with a few perks, like the $100 offer Windows is giving to XP customers looking to buy a new device. It’s all in effort to make the transition as easy as possible. Tech support is available for data transfers if you choose to upgrade your Microsoft PC to Windows 7 or higher.

Due to the amount of notice put out by the Microsoft company about this upgrade, it’s clear that the Windows XP system will not be safe to use after April 8. It will still function, but Microsoft will no longer support it. This means the security software will no longer be kept updated and computers may become susceptible to viruses and such.

Microsoft upgrades are available to get computers running with Windows 7 up through to the newest software, the Windows 8.1. If you’ve been looking for a good time to give in and get a new computer all together, this may be a good time to do so.

According to the Microsoft Q&A, “An upgrade installation of Windows 7 will keep your files, programs, and settings in place. A custom (clean) installation doesn’t preserve your files, programs, or settings. If you plan to do a custom installation of Windows 7, be sure to back up your files and other info first. You’ll also need to manually reinstall your programs.”

Try transferring data to an external hard drive is recommended, then you can just download everything back after the installation. An external hard drive is actually a good thing to have around, too. It saves all your data in case of freak accidents like short circuiting, theft, or kids pressing buttons. Also, you’ll have it for the next emergency upgrade 10 years down the road.

This announcement came along with a lot of surprise, if you find yourself scrambling know that you’re not alone. Still, computer years pile up faster than dog years, so 13 years of technology really is night and day. You might hate it at the moment – but just look forward to when things will be bright, shiny and new. You know… before the next newest thing.