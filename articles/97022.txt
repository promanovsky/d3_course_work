Micro 3D printer blows past $2.2 million on Kickstarter in a few days

We have been following the Micro 3D printer closely since the project turned up on Kickstarter a few days ago. The project is for a very small and compact 3D printer that is also very inexpensive to purchase. The printer will sell for about $299 and will let the user print out all sorts of 3D creations. When the project first hit Kickstarter, it was looking for only $50,000 to come to market.

Only a day later, the little 3D printer had soared past that goal and broken the $1 million mark. The project hasn’t slowed much since it surpassed that million dollar mark earlier this week. Money raised by the project has now exceeded $2.2 million and it still has 26 days to go. One of the downsides to the project being so popular is that the early delivery dates for the 3D printer are already gone.

The spots to get the printer for $299 by November or December of this year are now gone. You can still get the printer as an early beta adopter for $599 with a delivery estimate of October 2014. There are 414 of those early units left. You can cough up $899 and get the Micro 3D printer by August.

Anyone wanting to get the printer for the $299 price that got so many people excited will need to wait until January 2015 for delivery. There are still 962 spots left in that tier as of writing. This little printer uses PLA or ABS filaments and can work with standard 1.75mm filament spools that are on the market.

SOURCE: Kickstarter