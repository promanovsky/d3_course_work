Screenshot by Lance Whitney/CNET

Facebook offers several tools and settings through which you can safeguard your privacy. But it's still up to you to control your own socially networked destiny.

On Thursday, Facebook tweaked the default option for new members so that updates are shared only with friends and not with the public. In line with that change, let's take a tour through Facebook's basic privacy settings. Tightening these settings can help ensure that you're as well-hidden as possible on the world's largest social network.

First off, you can change the default audience for all your updates and control each update individually. To do this, log into your Facebook account and click the Privacy Shortcuts icon in the upper right.

From the Privacy Shortcuts menu, click the link for "Who can see my Stuff?" Under the setting for "Who can see my future posts?" click the button and select the appropriate default audience for your updates. You can select Public (usually not the best choice), Friends (probably the best option), or Only Me (not much point putting it on Facebook then). You can also choose a custom option if you want to share your updates only with specific lists of Facebook friends.

You can then override the default setting for any individual update. In the update (or What's on your mind?) field on your Facebook page, simply click the default sharing setting and change it to something else. For example, you may want to set your default audience to friends but share a specific update only with a more limited group of people.

Further, you can change the audience for updates you've already posted. To do this, again click the Privacy Shortcuts icon in the upper right and click the link for "Who can see my Stuff?" Under the setting for "Where do I review who can see or find things I've posted or been tagged in?" click the link to Use Activity Log.

Facebook displays a timeline of all the updates you've posted, liked, or commented on. Hovering over the small audience icon to the right of the text shows you who can see the update.

You can't control who sees updates from other people, but you can control your own. Simply click the audience icon for one of your own updates and change the selection. For example, you may decide that an embarrassing update should no longer be shared with all your friends but only with a few people. Of course, all of your friends may have already seen this update, but at least you can limit its exposure for the future.

Now playing: Watch this: Master your Facebook privacy settings

Want to see how your timeline looks to other people? Again, click the Privacy Shortcuts icon in the upper right and click the link for "Who can see my Stuff?" Under the setting for "What do other people see on my timeline?" click the View as link.

By default, Facebook shows you what your page looks like to the public. You can change that by clicking the link to View as Specific Person at the top and then typing and selecting the name of one of your Facebook friends.

Next, you can determine which types of messages you want to appear in your Facebook inbox. Click the Privacy Shortcuts icon in the upper right and click the link for "Who can contact me?" Under the setting for "Whose messages do I want filtered into my Inbox?" choose either basic or strict filtering. The former allows messages from friends and people you may know, while the latter restricts messages from just your Facebook friends.

Under the setting for "Who can send me friend requests?" click the button to choose between Everyone or only Friends of Friends.

Finally, what can you do if someone keeps pestering you on Facebook? Click the link for "How do I stop someone from bothering me?" Type the name or email address of the person you wish to block, and that individual will be unfriended and won't be able to start conversations with you or see your updates.

Facebook offers still more tools and settings worth exploring. Click the See More Settings link at the bottom of the Privacy Shortcuts window.

A section called "Who can look me up?" controls who can look you up on Facebook using your email address or phone number. The Timeline and Tagging page lets you manage who can post on your timeline and what they can see. And the Blocking page allows you to block specific users and apps on Facebook.

Facebook has often been criticized for not caring about users' privacy. But the site does provide tools to help users control their own privacy. Reviewing and managing these settings is something all Facebook users should do.