People with mental health problems could die because of controversial NHS funding changes which breach ministerial pledges to treat patients with psychological or physical conditions equally, patient charities are warning.

Six leading mental health organisations say NHS England's decision to cut the amount of money the sector will receive in "tariffs" or fees for its services from April will badly hit an area of NHS care that is already "straining at the seams".

They are angry that NHS bosses and Monitor, the NHS's economic regulator, are imposing a 1.8% cut in the amount of money for non-acute care services, which include mental health. That is higher than the 1.5% reduction in the budget for acute hospital-based medical services.

In a letter published in Wednesday's Guardian they warn that the decision "contravenes the government's promise to put mental and physical healthcare on an equal footing and will put lives at risk".

The co-authors of the letter include the charities Mind and Rethink Mental Illness, the Royal College of Psychiatrists and the Mental Health Network.

The differential tariff is being brought in to raise £150m to help acute hospital trusts pay for the extra staffing needed to comply with the recommendations of the Francis report into the Mid Staffs care scandal. NHS England and Monitor argued that non-acute service providers did not need extra money to tackle understaffing because Francis's recommendations did not cover them. However, David Cameron and several ministers have made clear that providers of all types of NHS care should be improving their staffing levels if necessary.

Doing so will cost non-acute providers about £150m, the same amount that they are losing in effect to subsidise hospital services, according to an estimate drawn up by the Foundation Trust Network.

Norman Lamb, the care services minister, has called the decision to penalise mental health services "flawed and unacceptable".

Lord Victor Adebowale, chief executive of the addiction and mental health charity Turning Point, has called the decision "bordering on laughable". The NHS England board – of which he is a member – never discussed it before it was approved, he said, "and there are lessons that should be learned".

The co-authors of the letter want NHS England to think again and cut both types of services equally. But while NHS chief executive Sir David Nicholson last week acknowledged the criticism of the way the decision was reached, he made clear that it would not be reviewed because acute hospitals needed the money to hire more nurses.