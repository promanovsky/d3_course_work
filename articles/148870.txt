Saudi Arabia's King Abdullah sacked the country's health minister on Monday amid a spike in deaths and infections from the virus known as the Middle East respiratory syndrome, or MERS.

A royal order carried by the state news agency said Abdullah al-Rabiah was relieved of his post as health minister, and that Labor Minister Adel Faqih would temporarily lead the Health Ministry until a replacement is named. The statement said al-Rabiah will now serve as an adviser to the Royal Court.

No reasons were given for the move, but it comes as the kingdom is scrambling to contain the spread of the coronavirus related to SARS. The Health Ministry says 79 people have died and more than 240 have been infected by the virus since 2012.

Saudi officials are struggling to alleviate concerns that the virus is spreading. There is no vaccine or treatment for the virus, and it is still unclear how it is transmitted.

A day before al-Rabiah was fired, he held a press conference to stress that the kingdom was taking sufficient measures and doing its utmost to deal with the virus.

MERS belongs to a family of viruses known as coronaviruses that include both the common cold and SARS, or severe acute respiratory syndrome, which killed some 800 people in a global outbreak in 2003. MERS can cause symptoms such as fever, breathing problems, pneumonia and kidney failure.

The Saudi Health Ministry will host nearly two dozen experts this weekend from international bodies, such as the World Health Organization, who have experience dealing with epidemics.

An outbreak among health workers prompted authorities to shut down the emergency ward of one of the largest hospitals in the city of Jiddah for 48 hours earlier this month.

The ministry reported the most recent deaths late Sunday: a 49-year-old Jiddah resident; a 68-year-old from the southern Najran region; and another 68-year-old from the area around the western city of Medina.

The deaths were among 13 new cases reported in the kingdom.