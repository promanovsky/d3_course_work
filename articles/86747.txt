Hauling in more than $24,000 per site, "Captain America: The Winter Soldier" racked up $96 million domestically over the weekend and added another $207 million in foreign revenue, bringing it to more than $300 million worldwide and cementing it as the biggest blockbuster so far in 2014. Meanwhile, "Noah" struggled to hold on to its audience, dropping more than 60% from its opening, while "Divergent" climbed well past $100 million in its third weekend in theaters. Animated release "Mr. Peabody & Sherman" also moved past $100 million over the weekend for Fox, and Disney's "Frozen" cruised by "The Dark Knight Rises" to number nine on the all-time worldwide box office list.

While the first "Captain America" earned a steady $65 million in its opening weekend back in July of 2011, it seems that audiences were much more enthusiastic this time around for sequel "The Winter Soldier." "The Winter Soldier" easily toppled the $86 million April record previously held by "Fast Five," showing significantly more upside this time around for distributor Disney. Considering "Captain America: The First Avenger" earned $176 million in the U.S., $250-$300 million seems to be in the bag for "The Winter Soldier."

Thanks to the international reputation of Disney and the Marvel cinematic universe, "The Winter Soldier" also looks like it will be an enormous hit outside of the U.S. as well. Though "The First Avenger" put up a fairly subdued $193 million in foreign revenue, "The Winter Soldier" has already surpassed $200 million after just a few days in theaters, emphatically showing how internationally popular the Marvel brand is right now. With very positive critical reviews for "The Winter Soldier" as well, you can expect to see more impressive foreign numbers to trickle in over the next few weeks.

Also helping the outlook of "The Winter Soldier" in a big way is the extremely limited competition it will face until "The Amazing Spider-Man 2" opens near the beginning of May. While "The First Avenger" had to go up against big-budget releases like "Cowboys & Aliens" and "Rise of the Planet of the Apes," the next three weeks will bring only limited competition, and "The Winter Soldier" should have no problem holding onto a high percentage of its target audience. With the success of "Fast Five" and now "The Winter Soldier" in recent years, April is starting to look like an attractive release month for big-budget productions hoping to stay out of the way of the most anticipated releases of the summer.

But even though "The Winter Soldier" had no problem finding its target crowd, biblical epic "Noah" had a hard time holding on to its intended demographic, grossing just $17 million in second-weekend revenue. With a 10-day domestic total of $72 million, the numbers so far are fairly underwhelming for a $125 million production, though it at least has done well outside of the U.S. to bring it to $178 million worldwide. "Noah" looks like it will continue to sink domestically in the coming weeks, though it still looks to be on track to earn somewhere in the $250+ million range overall by the time it's out of theaters for Paramount.

Meanwhile, $2 million production "God's Not Dead" continues to do considerably better with religious audiences considering its limitations, as the independent release hauled in another $7.7 million to bring it to $32 million domestically so far. Adding nearly 600 new venues this weekend and dipping just 12% from the previous weekend, "God's Not Dead" is clearly resonating with audiences around the country and is enjoying one of the best low-budget box office runs of the last couple of years.

Also continuing to reach its target audience is "Mr. Peabody & Sherman," which landed in the domestic top 10 for the fifth straight weekend and is now up to $102 million in the U.S. Though crossing $100 million domestically isn't much of a feat for a $145 million production, "Mr. Peabody" has also overcome a sluggish start thanks to competition with "The Lego Movie" and appears to have made a dent with family audiences around the globe. With $238 million worldwide and counting, "Mr. Peabody & Sherman" should wrap up its box office run as an overall commercial success for distributor Fox.

Next weekend, Kevin Costner-starring NFL drama "Draft Day" is one of the most prominent new titles slated to hit theaters, with horror film "Oculus" and animated "Rio 2" rounding out next week's wide releases. Later in the month, Johnny Depp-starring sci-fi release "Transcendence" will also hit theaters with fairly big commercial expectations from distributor Warner Brothers.

Early Studio Box Office Estimates for 4/4/14 - 4/6/14 (in millions), [RTT Prediction]:

1) Captain America: The Winter Soldier (Disney): $96.20 [$75]

2) Noah (Paramount): $17 [$22]

3) Divergent (Lionsgate): $13 [$14]

4) God's Not Dead (Freestyle): $7.73 [$7]

5) The Grand Budapest Hotel (Fox Searchlight): $6.30 [$5]

6) Muppets Most Wanted (Disney): $6.29 [$6]

7) Mr. Peabody & Sherman (Fox): $5.30 [$5.8]

8) Sabotage (Open Road): $1.91 [$2.5]

9) Need for Speed (Disney): $1.84 [N/A]

10) Non-Stop (Universal): $1.83 [$2.1]

For comments and feedback contact: editorial@rttnews.com

Entertainment News