Sia can now call herself a chart topper!

The Australian singer/songwriter has earned her very first No.1 album on the Billboard 200 chart with '1000 Forms of Fear.'

As well as debuting at the top of the musical ladder, Sia also achieved her best sales week ever as her sixth studio album sold 52,000 copies in the week ending July 13th, according to Nielsen SoundScan.

However, it is worth noting that this is the lowest sales for a chart topping album in nearly two years.

'1000 Forms of Fear' was officially released on Tuesday, July 8th.

It seems the 38 year-old's bizarre marketing campaign surprisingly paid off, she purposely did not show her face during live television performances of the track 'Chandelier,' and refused to participate in any normal photo shoots.

Sia decided to perform with her back facing the camera while several dancers tried to interpret the song, this was recently seen in appearances on 'The Ellen DeGeneres Show,' 'Late Night With Seth Meyers' and 'Jimmy Kimmel Live!'

While the Aussi singer is an established solo artist, she is most well-known for her collaborations with the likes of David Guetta (Titanium) and Flo Rida (Wild Ones), as well as being a prolific songwriter, penning tracks for global artists such as Eminem, Beyoncé, Jennifer Lopez and Rihanna.

Once Sia was announced as this week's No.1, she took to Twitter to thank her fans for their ongoing support. "Woah, I can't believe this experiment worked," she tweeted. "Thank you so much for making my album number 1!"

More: Sia Releases 'Eye Of The Needle' As A Free Download [Listen]

Following '1000 Forms of Fear' is the hugely successful soundtrack to Disney's 'Frozen,' which climbs to the second spot by selling 46,000, and British singer Sam Smith hangs onto 3rd position with 'In the Lonely Hour' selling 42,000 copies.

Last week's chart topper, Trey Songz's 'Trigga' finishes at No.4, and rounding off the top five is Ed Sheeran's 'X,' selling 35,000.



Sia earned her first Billboard 200 No.1