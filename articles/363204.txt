The debate over the so-called “right to be forgotten” and the impact of May’s ruling in the European Court of Justice raises all sorts of interesting questions.

One of those is whether news publishers whose content has been “delisted” from Google’s search results should seek to have the material “re-listed”, for instance by making small changes to the content, then re-publishing it online. A more transparent way to highlight our concern about the ruling might be to group together all the articles which have been subject to successful removal requests in a more prominent part of our website.

It is debateable whether the ruling amounts to an attack on press freedom as such: The Independent is not being required to delete anything, after all. But it certainly has serious implications for equal access to information.

Watch more

And given Google’s immense role in our lives, that may amount effectively to the same thing. In either event, the impact of the court’s decision has caused us and other publishers real unease.

In this context, therefore, it is tempting to go ahead and highlight on our own site those articles which no longer appear in Google’s search results (at least for those using google.co.uk, as opposed to google.com).

But this raises its own ethical questions because, ultimately, we do not know why the material in question has caused such anxiety. It may be that if the people objecting had come directly to us with their concerns we would have agreed to amend the problematic content. We might not, of course. Yet because Google’s handling of the requests it receives is so opaque, we are left in the dark about the specifics of each case.

The result is that by promoting our de-listed material, we could unintentionally cause further distress to an individual for whom we would actually have sympathy. In the end, that seems an unnecessary risk to take, even if our protests against this ruling are justified.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

We must be careful curators

Recent research by The Times to mark the centenary of the First World War’s outbreak found some depressing holes in the historical knowledge of British adults.

Nearly a fifth of the people surveyed believed that Britain entered the war to counter the rise of the Nazis. A similar proportion went so far as to think that Germany was led by Adolf Hitler during the period.

A week after the results of the research appeared in the media, the latest instalment in The Independent’s “Great War in 100 moments” series recorded the memories of a teenage girl’s visit to see her brother, a soldier in the German army, at a recruitment camp in Posen.

While the piece was accompanied in our print edition by only a small image of the girl herself, for the online version a picture of the Posen army base was chosen.

Unfortunately, this second photograph was from the wrong era, the small swastika flags in the background not spotted until six night-time hours (and two reader complaints) later. The photo was immediately changed.

But this example also serves as a reminder that there is an ethical imperative to report on historical events with particular care. The media does, after all, have a role in helping to reduce the knowledge gap of those who might not know their Hess from their Hindenburg.