Men rendered infertile by two or more biological defects in their sperm or semen face risks of death twice that of men whose semen and sperm are healthy, researchers say.

Reporting their 8-year study in the journal Human Reproduction, the researchers said infertile men suffering problems like poor motility of lower sperm counts are more prone to dying early.

The finding suggests doctors treating infertile male patients should counsel them on healthier habits that could increase their chances of survival, lead study author Michael Eisenberg says.

"We've seen other data that there's a link between fertility and long-term health, but the guys I see in my clinic, they're pretty young," says Eisenberg, head of male reproductive surgery and medicine for Stanford University.

While previously studies have linked male infertility to long-term health issues including cancer, the new finding is one of the first to confirm an increased risk of death in men at a relatively young age, between 20 and 50.

In this study, Eisenberg with his fellow researchers reviewed medical records on around 12,000 men in that age group who had gone to be evaluated for possible infertility at Stanford or Houston's Baylor College of Medicine.

Men with two or more semen or sperm abnormalities such as low concentration of sperm, reduced semen volume, impaired sperm motility or low sperm counts were more than twice as likely to die during the course of the study than men without abnormalities, the researcher found.

"It's important to identify this group that's at risk and hopefully prevent these deaths moving forward," Eisenberg says.

However, he said, it should be noted the overall death risk for infertile men was still low; of the 11,935 in the study, only 69 died during the 8-year study period.

Also, he acknowledges, its unknown at the moment whether defective semen or sperm is the cause of health problems or just evidence of some underlying disease.

Although only men were evaluated in the study, Eisenberg said infertile women likely would also be at increased risk of early death.

"My belief is that infertility provides a window into a man's later health, and I think it would apply to both sexes," Eisenberg said. "We can expect to see the same effects in women as well."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.