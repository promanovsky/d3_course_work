Just like with most products and services, criticisms and negative attention can affect the marketability of Google's wearable computer device, the Google Glass, so it is apparently for this reason that the company has decided to address what it described as misconceptions and myths about the device.

In a post on Google+ Thursday, the web giant enumerated and cleared the "The Top 10 Google Glass Myths" which notably included issues regarding privacy and surveillance that hound the head-worn device.

"Myths can be fun, but they can also be confusing or unsettling. And if spoken enough, they can morph into something that resembles fact," the company wrote. "In its relatively short existence, Glass has seen some myths develop around it. While we're flattered by the attention, we thought it might make sense to tackle them, just to clear the air."

Addressing the issue that the device ends privacy, Google used the case of the camera as an example to show that what people currently see as threat to their privacy may actually end up beneficial. It noted that when the camera first appeared in the market, the device was banned in many places because of its perceived threat to privacy.

The company also said that the Glass was not designed to be a secret spy glass for if it was, it could have done something better than a device worn on the face and lights up whenever the user gives a command or presses a button. It also said that the Glass does not record everything because the battery will run out within 45 minutes when the device is used for recording.

While it admitted that the current $1,500 cost of the Glass prototype is prohibitive for many people, Google said that those who have the device are not necessarily rich. It also refuted claims that Google Explorers are "technology-worshipping geeks" as the device can be used and worn by people from all walks of life including parents, film students, reporters, and doctors.

Addressing skeptics who say that Glass is the ultimate distraction from real world, Google said that the device actually allows users to look up and engage with others instead of staring at their computing devices.

The company also stated that the Glass is still a prototype and not yet "ready for prime time"; that it decided to prohibit facial recognition apps and that the device does not cover the user's eyes. Google also addressed "would be banners" of the device. "Just bear in mind, would-be banners: Glass can be attached to prescription lenses, so requiring Glass to be turned off is probably a lot safer than insisting people stumble about blindly in a locker room," Google said.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.