Federal Reserve Chair Janet Yellen appeared before Congress for a second day on Wednesday, this time taking questions from the House Financial Services panel.

Republican Rep. Jeb Hensarling (R-Tx.), the chairman of Financial Services commitee, spoke out in favor of a GOP bill that would require require the Fed to follow a mathematical rule for when to raise or lower interest rates.

Asked about the bill, Yellen said it would be a "grave mistake" for Fed to commit to follow a rigid metric.

She said mathematical formulas should instead be used a tool to guide Fed policy, later telling Rep. Bill Huizenga, (R-Mich.) there is no evidence that adopting a monetary policy rule is the best way to run a central bank.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.