Actor Idris Elba and his wife Naiyana Garth have experienced the joy of welcoming their first child, a son, into the world. The Luther star and his girlfriend shared a beautiful Twitter photo of their baby, who they've named Winston, and proclaimed his birth to fans worldwide. "My Son Winston Elba was born yesterday..Truly Amazing :-)." Elba wrote.



Idris Elba & Girlfriend Naiyana Garth Have Given Birth To Their First Child.

The photo shows a teeny tiny baby's hand gripping on to Idris' index finger, which looks massive in comparison to Winston's. Winston will now be a half-brother to daughter Isan, 11, custody of whom Elba shares with ex-wife Dormowa Sherman. The 41 year-old British star and his make-up artist girlfriend announced they were expecting a baby together in November after having dated since the previous March.

My Son Winston Elba was born yesterday..Truly Amazing :-) pic.twitter.com/MrSEQPZo4z — Idris Elba (@idriselba) April 18, 2014

A friend told the Mirror at the time: "Idris and Naiyana are so in love and this is the icing on the cake for them. They had been keeping it quiet as it's early in the pregnancy, but they've told family and friends and Naiyana is beginning to show - she is absolutely lovely and she's looking radiant."



The Actor Seemed Beside Himself With Pride, Posting A Cute Photo On Twitter.

They continued: "Her and Idris are a great couple and feel lucky to have found each other. Obviously Idris is a bit of a TV heartthrob so Naiyana is the envy of women across the country. But the truth is Idris is the one who really considers himself the lucky one.

"He's a great dad and she'll be a brilliant mum. They can't wait for their new arrival," the friend added.

Winston's birth comes in the height of a busy and successful period for the Oscar-nominated Mandela: Long Walk To Freedom actor. Having appeared as Heimdall in Thor: The Dark World recently, he had a total of six new movies in the works, including an adaptation of The Jungle Book, for which he has reportedly provided the voice of Tiger Shere Khan.

More: Idris Elba to make Glastonbury Festival debut - find out more.

More: Idris Elba shows off rapping skills at Mr Hudson gig - see here.