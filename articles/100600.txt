Gilead Sciences Inc. GILD, -0.04% on Monday said the Food and Drug Administration has placed the company's new drug application for a hepatitis C treatment under priority review.

Gilead said it expects the FDA's decision on the ledipasvir-sofosbuvir combination by Oct. 10.