While we advise patients that no limit of alcohol is safe, we have for the longest time seen that diets that are moderate in alcohol consumption such as red wine, seem to confer some cardiac protection.



But now a new study has us all wondering if we should put that glass of red wine down!



In this study, Italians who consume a diet rich in resveratrol - the compound found in red wine, dark chocolate and berries - were found to be just as likely to develop cardiovascular disease or cancer as those who eat or drink smaller amounts of the antioxidant. The study also showed that consuming resveratrol did not increase life expectancy.



The study's authors say that the story of resveratrol turns out to be another case of a health benefit hype not standing the test of time. How disappointing!



Doctors and researchers have long thought that resveratrol can be good for your health. Studies have shown that consumption of red wine, dark chocolate and berries does reduce inflammation in some people, and still appears to protect the heart. So the question remains: If resveratrol is not providing those benefits, are they found in other polyphenols or substances found in those foods?



Many people take resveratrol supplements, but they were not included in this study. To date, despite the widespread supplement market, few studies have actually found any benefits associated with them.



In this study, 24 hour urine samples were examined from 783 people over the age of 65 for metabolites of resveratrol. After accounting for such factors as age and gender, the people with the highest concentration of resveratrol metabolites were no less likely to have died of any cause than those with no resveratrol found in their urine. The concentration of resveratrol was not associated with inflammatory markers, cardiovascular disease or cancer rates.



The participants live in Tuscany where supplement use is uncommon and consumption of red wine is the norm. The study participants were not on any prescribed special diet.



The so-called “French paradox,” in which a low incidence of coronary heart disease occurs in the presence of a high dietary intake of cholesterol and saturated fat in France, has been attributed to the regular consumption of resveratrol and other polyphenols found in red wine. It is unclear whether there are other polyphenols in red wine that are protective but for now, resveratrol is not the answer!