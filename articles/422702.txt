There is an increasing trend among parents to consider their overweight children are "healthy" and the right size, a study suggests.

Parents -- mostly mothers -- surveyed from 2005 to 2008 were more liable to say that they consider their overweight child to be healthy as compared to a similar survey using data from 1988 to 1994, the study authors say.

In the earlier survey, around 50 percent of parents correctly assessed their children of being overweight; in the latest study, that figure had fallen to 44 percent.

The biggest increases in parents seeming to be willing to overlook their children's weight were among low income parents and African Americans -- both groups already at an elevated risk of obesity, they reported in the journal Pediatrics.

"Crucial to parental involvement in weight reduction or maintenance efforts among children is parental recognition of their child's overweight status," the researchers wrote in their study.

Obesity is on the rise in children from ages 6 to 11, increasing from 7 percent in 1980 to almost 18 percent in 2012, they noted.

"The society as a whole is stuck with a vicious cycle," says lead study author Jian Zhang, an epidemiologist at Georgia Southern University. "Parents incorrectly believe their kids are healthy, they are less likely to take action, and so it increases the likelihood that their kids will become even less healthy."

Pediatricians are likely the best weapon to break that cycle, experts say, as parents for the most part look at doctors as authority figures and are willing to address obesity in their children if a pediatrician gets involved.

Part of the problem is the all-too-common incidence of obesity in our modern society, says Amanda Staiano, director of the Pediatric Obesity and Health Behavior in Baton Rouge, La.

"We compare ourselves to the people we see around us," she says. "If a child is in a class where most of the kids are overweight or obese, that becomes the new normal."

As Americans in general become increasingly obese it is easy for perceptions of what is healthy versus what is not to become distorted, the researchers say.

Also, parents may be unwilling to admit their children are overweight out of fears the child will suffer at school and with friends because of the stigma of obesity, Staiano says.

The researchers urge pediatricians to talk to their young patients -- and those patients' parents -- about what constitutes healthy weight.

TAG Nutrition, Diet, Obesity, Pediatricians, Overweight, parents, childhood

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.