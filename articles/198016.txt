Name: The goblin shark.

Age: You'd have to ask it.

Appearance: A kipper crossed with an ironing board, as imagined by HR Giger.

The artist who created the monsters in the Alien movies? That's the one. If you're still confused, you could look at the picture at the top of the page.

I'm trying not to. That snout! Those teeth! That purple-pink skin! The last time I saw anything that hideous, it was standing at a dispatch box, lying about its plans to destroy the welfare state. Are you thinking about any coalition minister in particular here?

I would be if I could tell them apart. Well, this odd-looking specimen was caught off the coast of Florida by fishermen trawling for shrimps. Who promptly threw it back in again.

And that's what passes for news nowadays, is it? Fishermen Accidentally Catch Something They Don't Fancy Eating! Of course not. It only counts as news if they take photos of it.

I stand corrected. Fishermen Accidentally Catch Something They Don't Fancy Eating – With Pictures! It also helps if the fish has a good backstory. The goblin shark lives at depths of several thousand feet and gets its name from Japanese legends of dangerous creatures with long noses and red faces.

Hmm. Sixty-three-year-old Carl Moore scooped up the fish on 19 April, but only reported his catch to the National Oceanic and Atmospheric Administration last week. It's more than a decade since the last goblin shark was seen in the Gulf of Mexico. "I didn't even know what it was," Moore told the Houston Chronicle. "The guys at NOAA said I'm probably one of only 10 people who've seen one of those alive."

Lucky he had a camera. Moore used his mobile phone – without getting too close – and reckons the shark was 18ft long. "I didn't get the tape measure out because that thing's got some wicked teeth. They could do some damage."

Especially if you're a smaller fish. Well, obviously. The shark's jaws can snap forward almost to the end of its snout – which, incidentally, is crammed with electrical sensors to help it find prey.

Isn't nature amazing? As the last surviving member of the Mitsukurinidae family, which dates back some 125m years, the goblin shark is sometimes described as a living fossil.

Are you sure it isn't something to do with the government? Er …

Do say: "I never forget a face … "

Don't say: "But in your case I'll make an exception."