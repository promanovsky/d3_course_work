In a report published Tuesday, Brean Capital analyst Todd Mitchell reiterated a Buy rating and $95.00 price target on DirecTV (NASDAQ: DTV).

In the report, Brean Capital noted, “Market over-discounting regulatory risk, missing upside potential for AT&T, maintain Buy rating and $95 target price. Shares of DTV traded off yesterday and remain well below the AT&T's proposed $95 offer. We believe this is a result of the market's concerns about the deal not getting approval and the rather onesided break up agreement between the two companies.

"We acknowledge the deal will be tougher to get approval for than Comcast/TWC because it will reduce competition in the MCVP market in about 30% of the country. However, we believe that AT&T's commitment to build out HSD services in rural markets will trump concerns about MCVP concentration from a policy perspective and that the deal will be approved. Moreover, unlike the Comcast/TWC deal, which is mostly about the benefits of scale, we think AT&T/DirecTV puts together highly complementary assets which will enhance AT&T's service profile in every segment of the market.

"Given our belief that the market has over discounted regulatory risk, and our belief that AT&T will emerge with a much stronger competitive set, which will ultimately bode well for its own share price, we remain buyers of DTV at these levels.”

DirecTV closed on Monday at $84.65.