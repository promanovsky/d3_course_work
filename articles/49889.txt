Say goodbye to that multi-colored can of crazy: Four Loko will soon be off the market entirely.

Chicago-based company Phusion Projects agreed this week to cease production of the alcoholic energy drinks, Attorney General Lisa Madigan announced on Tuesday.

According to DNAinfo Chicago, the company will stop all manufacturing and marketing of the product.

A statement from the Office of the Attorney General said the settlement puts to rest claims that Phusion breached consumer safety by "promoting the drink to underage youth, promoting dangerous and excessive consumption of Four Loko and failing to disclose to consumers the effects and consequences of drinking alcoholic beverages combined with caffeine."

Four Loko encountered much scrutiny since it first hit liquor store shelves in 2008, as health advocates pointed out the potential heart problems drinking the alcoholic energy beverages could cause. Additionally, critics of the drink said the company violated trade practice laws.

"Phusion used marketing and sales tactics that glorified alcohol use and promoted binge drinking," Madigan wrote in the release. "This agreement is a significant step forward in our ongoing efforts to reduce access to dangerous caffeinated alcoholic beverages, especially to underage drinkers."

Attorney generals in 19 different states signed the agreement, DNAinfo reported, thus closing the legal battle that waged on for years between the beverage company and the government.

But Phusion representatives steadily maintained that they were not guilty of the claims, according to DNAinfo. In his own statement, Phusion Projects President Jim Sloan said the cans were clearly marked with warnings of health and legal hazards.

"While our company did not violate any laws and we disagree with the allegations of the State Attorneys General, we consider this agreement a practical way to move forward and an opportunity to highlight our continued commitment to ensuring that our products are consumed safely and responsibly only by adults 21 and over," Sloan wrote. "Phusion continues to believe, however, as do many people throughout the world, that the combination of alcohol and caffeine can be consumed safely and responsibly."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.