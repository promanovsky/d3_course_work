Taylor Swift has cancelled a sell-out concert in Thailand, which is currently under military rule following the recent coup.

Concert promoter BEC-Tero announced on its website that the concert was canceled "due to recent events in Thailand." It didn't elaborate but called the move "a difficult decision for all parties."

The concert in Bangkok would have been the night of June 9. The country is under a nighttime curfew though it's loosely enforced.

"I'm sending my love to the fans in Thailand," Taylor tweeted." I'm so sad about the concert being canceled."

The American singer-songwriter has other performances throughout Asia during her current Red Tour.

PA Media