ARE things about to get ugly between Nick Cannon and Mariah Carey? They might be, thanks to Nick’s dad.

The couple, who have been married since 2008, confirmed last week that they’ve been living separately for a few months.

After the announcement, Nick Cannon’s father, James, posted some cryptic messages on his Facebook page hinting at some of the reasons behind the split.

“Here a thing to talk about,” wrote James Cannon according to Radar Online, “if the wife love sex and the man can’t stand making love to her. What should that women do?”

“Heres a thing, if the husband say no lies, and the wife lies all the time, what should that man do?

“Here a thing, if the wife put on 200 pounds and the husband like a nice slim wife what should that man do?

“If a husband tells his wife not to have an abortion, and she it [sic] anyways, what should that man do?

“Here a thing, if a husband wants to have 10 children and his wife only wants two children then what should that man do?”

James Cannon has since deleted the Facebook post and later backtracked saying the quotes aren’t about Nick and Mariah, but rather excerpts from a Christian book that he wrote in 2011.

“In America we have the right to ask question about anything we want,” James Cannon later wrote on Facebook.

“Heres the thing, small minds talk about people large minds talk about ideas ... I do not get into other people business not even my sons and I have 5 of them that i love dearly.

“So check my record I stay out of other people business, even my own sons. Please forgive me to those that may have misunderstood my question from my book.”