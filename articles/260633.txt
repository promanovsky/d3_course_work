Los Angeles: Google unveiled plans to build its own self-driving car that it hopes to begin testing in the coming months.

“They won’t have a steering wheel, accelerator pedal, or brake pedal ... because they don’t need them. Our software and sensors do all the work,” Google’s Chris Urmson said in a blog post.

Urmson said Google plans to build about 100 prototype vehicles, “and later this summer, our safety drivers will start testing early versions of these vehicles that have manual controls.”

He added, “If all goes well, we’d like to run a small pilot program here in California in the next couple of years.”

At least one US state has allowed the use of self-driving cars on the road when Google got its first self-driving car license in Nevada.

For Google, the car marks a shift away from adapting vehicles made by others in its quest to pioneer individual transport that needs only a stop-and-go function.

“It was inspiring to start with a blank sheet of paper and ask, ‘What should be different about this kind of vehicle?’” the post said.

The top speed of the battery-powered prototypes will be 40 km/h and are designed for utility, not luxury.

“We’re light on creature comforts, but we’ll have two seats (with seat belts), a space for passengers’ belongings, buttons to start and stop, and a screen that shows the route — and that’s about it,” Urmson said.

The blog post a photo of a prototype and an artist’s rendering — both rounded bug-looking vehicles.

“We took a look from the ground up of what a self-driving car would look like,” Google co-founder Sergey Brin told the Re/code conference in Rancho Palos Verdes, California.

“The reason I’m so excited about these prototypes and the self-driving car project in general is the ability to change the world and the community around you,” Brin added.

Until now Google has been refitting Lexus and Honda cars to work as self-driving ones.

In an interview with Re/code, Urmson said the new Google cars will have numerous safety features learnt from the company’s research.

“In our car there is no steering wheel so we have to design really fundamental capabilities,” he said.

“So we have effectively two motors and they work so if one of them fails the other can steer, so the car can always control where it’s going, and similar with brakes.”

In addition to crash protection for the occupant, the car has features to avoid pedestrians and other road users.

“So the front end is a whole new approach where it’s compressible foam and a flexible windshield that should do a much better job of protecting people if an accident should occur,” Urmson said.