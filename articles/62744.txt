Bewitching in white! Emma Roberts and Sarah Paulson brighten up American Horror Story presentation in angelic ensembles at PaleyFest



They both star as telekinetic witches in the hit dark television show, American Horror Story: Coven.



And on Friday, actresses Sarah Paulson and Emma Roberts were a far cry from character, brightening up the atmosphere in angelic pure-white ensembles and matching side-swept hairstyles.



The two leading ladies were in attendance for a presentation of the hit FX show at this year's PaleyFest in Hollywood.

Brightening up! Emma Roberts and Sarah Paulson were bewitching in white at an American Horror Story event at Paleyfest in LA on Friday



Roberts, 23, shimmered in a free-flowing white dress that demurely curved in at her waistline, all the while showing off her slender and bare shoulders.