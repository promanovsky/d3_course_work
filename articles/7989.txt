Scientists have developed a new blood test that they claim could detect whether or not a person will develop dementia within three years.

Changes in the blood may signify Alzheimer's disease in its earliest stages, researchers found.

A study, published in the journal Nature Medicine, identified 10 molecules in blood could be used to predict with at least 90% accuracy whether people will go on to develop mild cognitive impairment or Alzheimer's.

It is the first research that has been able to show differences in biomarkers in the blood between people with Alzheimer's before the symptoms occur and people who will not go on to develop the condition.

The finding has potential for developing treatment strategies for Alzheimer's at an earlier stage - when therapy would be more effective at slowing or preventing onset of symptoms, the authors said.

Researchers from Georgetown University Medical Centre in the US examined 525 healthy participants aged 70 and over and monitored them for five years.

During the research 28 participants went on to develop the conditions and 46 were diagnosed at the start of the study.

Midway through the research, the authors analysed 53 patients who already had one of the conditions and 53 "cognitively normal" people.

They discovered 10 molecules that appeared to "reveal the breakdown of neural cell membranes in participants who develop symptoms of cognitive impairment or Alzheimer's. They then tested other participants' blood to see whether these biomarkers could predict whether or not they would go on to develop the conditions.

By measuring the presence of 10 compounds the researchers could predict with 90% accuracy people that would go on to suffer from mild cognitive impairment (MCI) or Alzheimer's (AD).

"The lipid panel was able to distinguish with 90% accuracy these two distinct groups: cognitively normal participants who would progress to MCI or AD within two to three years, and those who would remain normal in the near future," said one of the study's authors, Professor Howard Federoff.

"Our novel blood test offers the potential to identify people at risk for progressive cognitive decline and can change how patients, their families and treating physicians plan for and manage the disorder.

"The preclinical state of the disease offers a window of opportunity for timely disease-modifying intervention. Biomarkers such as ours that define this asymptomatic period are critical for successful development and application of these therapeutics.

"We consider our results a major step toward the commercialisation of a preclinical disease biomarker test that could be useful for large-scale screening to identify at-risk individuals.

"We're designing a clinical trial where we'll use this panel to identify people at high risk for Alzheimer's to test a therapeutic agent that might delay or prevent the emergence of the disease."

Dr Simon Ridley, head of research at charity Alzheimer's Research UK, said: "Alzheimer's disease begins to develop long before symptoms such as memory loss appear, but detecting the disease at this pre-symptomatic stage has so far proved difficult.

"More work is needed to confirm these findings, but a blood test to identify people at risk of Alzheimer's would be a real step forward for research."

Dr Doug Brown, director of research and development at the Alzheimer's Society, added: "Having such a test would be an interesting development, but it also throws up ethical considerations.

"If this does develop in the future people must be given a choice about whether they would want to know, and fully understand the implications.

"This research could also give clues on how Alzheimer's disease occurs and warrants further study, but as such a small number of people showed dementia symptoms there need to be larger studies with different populations before it could be turned into a blood test for Alzheimer's disease."