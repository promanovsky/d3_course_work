Recent federal data showed that the average price of common hospital treatments increased at an alarming rate, mainly for chest-pain and vascular procedures.

Analysts from the Centers for Medicare and Medicaid Services (CMS) looked at the 2012 data collected from 3,376 hospitals performing the 100 most common procedures. The agency found that most hospitals charged higher prices for treatments because insurers typically pay a lower price. The results were consistent after comparing the data with those of private insurers who could negotiate a much lower amount to pay.

The problem with this scheme was that those who did not have health insurance were forced to pay a higher price for treatment that could be cheaper. On top of that, there were some hospitals that billed uninsured patients with balances that should have been covered by an insurer.

In addition, the analysts observed variations in the prices hospitals charged their patients for the same medical procedures. One example was Kaiser Permanente's Antioch hospital in Contra Costa, California, which charged its patients only $46,374 for joint-replacement procedures while those who had the same procedure at Northbay Medical Center in Fairfield paid up to $150,953.

"We pay wildly different costs for the same thing even when there's no difference in quality," executive director of the Catalyst for Payment Reform, Suzzane Dellbanco said to Wall Street Journal. "It opens your eyes to the fact that there's huge variation in charges for the same services."

The analysts continued their study on a national scale. They saw that costs of the procedured charged by hospitals varied state-to-state due to patient mix, labor costs, and other hospital priorities. For instance, the average discharge costs for a patient covered by Medicare increased by 24 percent in Muncie, Ind. from $33,168 in 2011 to $41,177 in 2012.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.