A law enforcement official runs toward the PG&E explosion in a residential neighborhood September 9, 2010 in San Bruno. (Justin Sullivan/Getty Images)

SAN FRANCISCO (KPIX 5) — A federal grand jury has indicted Pacific Gas and Electric Company for allegedly obstructing the federal investigation into the deadly San Bruno pipeline explosion.

Prosecutors say PG&E hampered the investigation by lying to regulators immediately after the September 9, 2010 explosion and inferno in the Crestmoor neighborhood of San Bruno that killed eight people and destroyed 38 homes.

The indictment also includes 27 counts of violating the Natural Gas Pipeline Safety Act. It alleges that at the time of the blast, the utility was operating under an unapproved, draft policy for addressing pipeline problems and as a result, it failed to prioritize or properly assess many older, high-risk pipelines.

The indictment also accuses PG&E of failing to address record-keeping problems, despite knowing they were inaccurate or incomplete.

Each charge carries a minimum $500,000 fine, but the penalites could end up being substantially higher.

In a statement, PG&E said it has not yet seen the indictment, but said “Based on all of the evidence we have seen to date, we do not believe that the charges are warranted and that, even where mistakes were made, employees were acting in good faith to provide customers with safe and reliable energy.”

The utility is scheduled to appear in court to answer to the charges on August 18.