US manufacturing growth accelerated for a second month in a row in March, an industry report showed today, as production recovered though employment growth slowed.

The Institute for Supply Management (ISM) said its index of national factory activity rose to 53.7 in March.

This was up slightly from February's read of 53.2 but below the median forecast of 54 in a Reuters poll of economists.

Readings above 50 indicate expansion in the sector.

The report remains below November's recent peak reading of 57, which was the highest reading since April 2011.

Gains in the month came alongside a rebound in the production sub-index, which jumped to 55.9 from 48.2, ending a three-month string of slowing growth. The forward-looking new orders index rose to 55.1 from 54.5.

There were some cautionary notes, as the employment index fell from 52.3 to 51.1, the weakest read for the index since June 2013. Analysts were expecting a reading of 52.8 in the employment index.

US construction spending barely rises in February

Meanwhile, US construction spending barely rose in February as outlays on private residential construction projects recorded their biggest decline in seven months, a sign that severe weather continues to hobble the economy.

Construction spending edged up 0.1% to an annual rate of $945.7 billion, the Commerce Department said.

Construction spending in January was revised to show a 0.2% drop instead of the previously reported 0.1% gain. Economists polled by Reuters had forecast construction outlays to be flat in February.

An unusually cold and snowy winter disrupted economic activity early in the year. Growth in the first three months of this year is expected to have slowed to an annualised pace below 2% after the economy expanded at a 2.6% rate in the fourth quarter of last year.

Activity, however, is showing signs of accelerating as temperatures warm up, with employment growth, industrial production and retail sales gaining momentum in February.

Construction spending in February was curbed by a 0.8% drop in private residential construction projects, which was the largest fall since last July.

However, a 1.2% surge in spending on non-residential construction projects, which include factories and gas pipelines, lifted overall private outlays to their highest level since December 2008.

The decline in private residential construction was led by a 1.1% drop in single-family home building.