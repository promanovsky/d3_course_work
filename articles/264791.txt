Apple recently announced the coming of its new home-based software platform for devices. Now Apple's rival, Google is claimed to be eyeing home security as well.

Google considers buying Dropcam, a home security camera company that sells $150 app-controlled cameras connected to the Internet. The search giant aims to expand its products on home automation after buying thermostat firm Nest just recently.

Dropcam cameras have smartphone and email alerts and claim to use the same security level as banks for its video encryptions. The device includes a two-way talk back, full 720p streaming, digital zoom, night vision and an optional recording cloud where users can access footages from the past seven to thirty days.

According to reports, Google might purchase Dropcam to add another feature to its rapidly expanding smart home department. The WiFi video monitor and optional recording cloud service will allow the users to remotely visit their homes, pets, babies, office from a tablet, smartphone or computer. Early in 2014, Google also bought Nest, a manufacturer of smoke alarms and smart thermostats for $3.2 billion.

When the Nest Protect launched in October 2013, it was described as artfully designed and connected to a smartphone, full of features that work more efficiently than the devices it wants to replace. However, Nest Protect had some problems and a recent recall of all the 440,000 devices that came out. The "wave to dismiss" feature was being triggered inadvertently and an alarm could be unintentionally switched off.

An SEC filing was recently revealed in which Google anticipates serving ads to devices in the future, including refrigerators, thermostats, car dashboards, watches and glasses.

"We are in contact with the SEC to clarify the language in this 2013 filing, which does not reflect Google's product roadmap. Nest, which we acquired after this filing was made, does not have an ads-based model and has never had any such plans," Google announces.

Meanwhile, Apple prepares the iPad to become a universal home remote that allows the users to control their household appliances such as security systems, TVs, lights and washing machines. The project is expected to be unveiled with the new iPad and iPhone software at the Worldwide Developer Conference in California. A published Apple patent from November 2013 hinted of the home system, showing how lights automatically turn on as soon as a user arrives home. Apple is said to be collaborating with home automation companies to ensure that its devices would work perfectly with the app.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.