Washington - Pfizer says an experimental drug has shown encouraging results in treating advanced breast cancer in an early clinical trial.

The world's second largest drugmaker says the drug prevented breast cancer from worsening for 20.2 months in a trial involving 165 patients.

Current medications do so for 10.2 months.

The drug is known as palbociclib.

It's among a new class of cancer drugs that target specific proteins to block tumors.