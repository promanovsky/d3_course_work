The California Department of Motor Vehicles said Saturday that it is investigating a potential security breach of its credit card processing services, but officials said the agency had no immediate evidence that its computer system had been hacked.

The DMV was alerted by law enforcement officials about the possible breach and has “heightened monitoring” of all of its Web traffic and credit card transactions, the agency said in a prepared statement. The agency is also in contact with credit card companies and the vendor who processes its online transactions.

“There is no evidence at this time of a direct breach of the DMV’s computer system,” the statement said. “However, out of an abundance of caution and in the interest of protecting the sensitive information of California drivers, the DMV has opened an investigation into any potential security breach in conjunction with state and federal law enforcement.”

“We will immediately notify any affected DMV customers as quickly as possible if we find any issue.”

Advertisement

Security blogger Brian Krebs — who broke the story of the blockbuster breach of retailer Target’s customer credit card data last year — cited several financial institutions that received private alerts this week from MasterCard about compromised cards used for charges marked “STATE OF CALIF DMV INT.”

MasterCard spokesman Seth Eisen told The Times on Saturday that the company was “aware of and investigating” reports of the potential breach. The company was in contact with banks in California and elsewhere, he said, but could not provide details about what information may have been compromised or how many cardholders may be affected.

It remained unclear how many people might be affected by a potential DMV breach. Krebs reported that one bank received a list from MasterCard of more than 1,000 cards that were potentially exposed.

He said that the information stolen included credit card numbers, expiration dates and three-digit security codes printed on the back, but that it remained unclear if other sensitive information — such as driver’s license or Social Security numbers — was also taken. The potentially problematic transactions were believed to have been made between Aug. 2, 2013, and Jan. 31 of this year.

Advertisement

More than 11.9 million transactions were completed on the DMV’s website in 2012, a 6% increase over the year before, according to the agency. The DMV touted its online services as a “convenient alternative” for customers to register vehicles, renew driver’s licenses or identification cards, and conduct other business.

Both MasterCard and the DMV encouraged customers to review their account statements and to immediately report any unusual activity to their credit card companies. “We fully understand the potential impact any breach of security can have,” the DMV said.

News of a potential breach comes on the heels of last year’s massive Target hack, believed to be the nation’s biggest cyber crime against a single retailer. Up to 40 million Target customers’ credit and debit card accounts were illegally accessed from Nov. 27 to Dec. 15, while as many as 70 million shoppers may have had their names and home and email addresses stolen over an indeterminate amount of time.

Then in January, upscale retailer Neiman Marcus said information from more than 1.1 million cards was potentially “scraped” by malicious software between July 16 and Oct. 30 of last year. The department store chain said at least 2,400 cards were used fraudulently after the breach, but said customers’ Social Security numbers, PIN numbers and birth dates were not among the compromised information.

Advertisement

MasterCard and Visa this month announced an initiative to increase payment security, including expanding chip technology in the U.S.

The integrated circuit cards generate a unique code for every transaction, which make it nearly impossible for the cards to be used for counterfeit activity, security officials said. The smart card technology is widely used in Europe, Asia and Canada. MasterCard and Visa had previously imposed an October 2015 deadline for U.S. banks and merchants to implement the new technology.

The new security initiative, which includes, banks, credit unions, merchants, manufacturers and industry trade groups, will also work on ways to better protect online and mobile transactions.

“The recent high-profile breaches have served as a catalyst for much-needed collaboration between the retail and financial services industry on the issue of payment security,” said Ryan McInerney, president of Visa Inc., in a statement. “As we have long said, no one industry or technology can solve the issue of payment system fraud on its own. These conversations will serve as a useful forum to share ideas, break down barriers and spur the adoption of next-generation security solutions for the benefit of all.”

Advertisement

California Atty. Gen. Kamala D. Harris also has made cyber crime a top priority, saying last month that the personal information of more than 21.3 million Californians had been compromised within the last two years. A 2012 law requires companies to report to the state any time the information of 500 or more customers has been taken.

“Unfortunately, cyber crime, data breaches, theft of proprietary information, hacking and malware incidents are now routine,” Harris said.

kate.mather@latimes.com

carla.rivera@latimes.com