The discovery of a chemical change in a single human gene could result in a simple blood test to identify people who are at risk of suicide, say researchers from Johns Hopkins University.

Researchers at the university in Baltimore say they studied a human gene involved in the brain’s response to stress hormones and discovered a chemical alteration that may be linked to an increased risk of suicide.

The findings, published online Wednesday in The American Journal of Psychiatry, suggest that changes in the gene known as SKA2 may play a role in turning an “otherwise unremarkable” reaction to stress into suicidal thoughts.

If the findings are confirmed in larger studies, they could result in a reliable blood test to predict someone’s risk of suicide, researchers say.

“Suicide is a major preventable public health problem, but we have been stymied in our prevention efforts because we have no consistent way to predict those who are at increased risk of killing themselves,” study leader Zachary Kaminsky said in a news release.

“With a test like ours, we may be able to stem suicide rates by identifying those people and intervening early enough to head off a catastrophe.”

Kaminsky, an assistant professor of psychiatry and behavioral sciences at the Johns Hopkins University School of Medicine, said the test could be used in the military and psychiatric emergency rooms, as well as in mental health assessments.

Kaminsky and his colleagues studied a genetic mutation in the SKA2 gene by looking at brain samples from both mentally ill and healthy people.

In people who died of suicide, SKA2 levels were “significantly reduced,” the study found.

Within the SKA2 mutation in some study subjects, researchers then found a modification that added chemicals called methyl groups to the gene. The process, known as methylation, alters the way the gene functions.

Higher levels of methylation were found in people who had died of suicide, researchers said.

In another part of the study, researchers tested three different sets of blood samples and found similar methylation levels in people with suicidal thoughts or those who nearly died of suicide.

The researchers say they were able to predict with 80 per cent certainty which study participants were experiencing suicidal thoughts or had nearly died. The predictions were 90 per cent accurate for participants with a more severe risk of suicide.

The largest set of those blood samples involved 325 participants. Kaminsky said more studies are needed with larger samples, “but we believe that we might be able to monitor the blood to identify those at risk of suicide.”

This is not the first time scientists have turned to blood samples and gene studies in search of suicide risk predictors. A Nature article published last year examined biological markers linked to suicide attempts and deaths by suicide.