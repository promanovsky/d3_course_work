Roger Yu

USA TODAY

The Federal Communications Commission said Tuesday it is extending the deadline for initial public comments on new net neutrality proposals as a heavy volume of comments swamped its servers.

The deadline was set for for midnight Tuesday, but the agency will continue to receive comments online or via e-mail -- to openinternet@fcc.gov -- until midnight Friday. After Friday, the public can continue to respond to already-filed comments until Sept. 10.

"Not surprisingly, we have seen an overwhelming surge in traffic on our website that is making it difficult for many people to file comments through our Electronic Comment Filing System," said FCC press secretary Kim Hart. "Please be assured that the commission is aware of these issues and is committed to making sure that everyone trying to submit comments will have their views entered into the record."

The net neutrality, or "open Internet," proposals that were pitched by FCC Chairman Tom Wheeler have been controversial from the start, and the agency had expected heavy interest and a massive inflow of comments. As of Tuesday afternoon, the agency received more than 780,000 comments.

In the proposals, Wheeler has said he wants new rules that would require Internet service providers to treat equally all legal content. The agency also is seeking public comments on "paid prioritization" of traffic, in which Internet service providers can seek payment from content providers for faster transmission. Net neutrality proponents oppose such deals for Internet "fast-lanes."

The agency is seeking to recast new net neutrality rules because the previous set of rules were tossed out by a federal court in January. The court gave the FCC an opportunity to redraft them, and the commission voted 3-2 to begin a rule-making process.

All the heavy hitters from Silicon Valley have had their say. The Internet Association, a lobbying group representing Amazon, Facebook, Google, Twitter and other Internet companies, filed its comment Monday and called for "strong" rules that would ensure net neutrality. "Segregation of the Internet into fast lanes and slow lanes will distort the market, discourage innovation and harm Internet users," said The Internet Association President Michael Beckerman in a statement. "The FCC must act to create strong, enforceable net neutrality rules and apply them equally to both wireless and wire-line providers."

Internet service providers say new rules would hamper innovation and the development of faster Internet. "Policymakers' light-touch approach has allowed flexibility and competition to spur incredible innovation and investment in broadband and wireless," wrote Verizon Senior Vice President for Federal Regulatory Affairs Kathleen Grillo in a letter filed among the public comments to the FCC.

Contributing: Mike Snider