

Harvard Medical School (Flickr user Tim Sackton)

Welcome to Health Reform Watch, Jason Millman's regular look at how the Affordable Care Act is changing the American health-care system — and being changed by it. You can reach Jason with questions, comments and suggestions here. Check back every Tuesday and Thursday afternoon for the latest edition, or sign up here to receive it straight from your inbox. Read previous columns here.

If you were spending $15 billion, you'd probably want to know what you were getting as a return on that investment. Especially if it was on something as important as the nation's health care.

Yet, a new comprehensive report finds that we don't have a good system of tracking the $15 billion the United States spends each year on training new doctors — a particularly pressing topic as 11,000 baby boomers become Medicare-eligible each day and about 25 million uninsured are projected to gain new coverage in the next few years under the Affordable Care Act. Further, our publicly financed program for training doctors doesn't ensure that the new crop of physicians will be positioned to meet changing demands for care, according to independent experts at the Institute of Medicine.

Recommendations from the IOM, an independent panel of experts, usually carry some weight. The panel's review focuses on Medicare, which distributed about two-thirds ($9.7 billion) of the $15 billion the federal government spent on the Graduate Medical Education program in 2012.

The IOM panel says that groups participating in the GME program basically only have to report limited data to the federal government, leaving major questions about the program performance unanswered. Questions such as: Who’s being trained by the program? How much of the GME funding is used for education? Do doctors go on to practice in areas where there’s a shortage of physicians? And – probably most important – does the program produce competent doctors? On that last point, the IOM says the federal government doesn't have data to measure whether the doctors are trained in patient safety or if they can provide coordinated care across different settings, a growing emphasis as America's health-care system is changing to focus on preventive care and better management of chronic conditions.

“America’s health care system is undergoing profound change as a result of new technologies, and the recent implementation of the Affordable Care Act will further increase the focus on primary and preventive care,” said Gail Wilensky, a former Medicare administrator who co-chaired the IOM panel. “It’s time to modernize how graduate medical education is financed so that physicians are trained to meet today’s needs for high-quality, patient-centered, affordable health care.”

The IOM says a nearly 20-year-old cap on GME slots has created geographic inequities in where the program's funding is focused. Further, hospitals can freely change primary care slots with specialty slots, "regardless of local workforce priorities," the panel said.

But the Association of American Medical Colleges offered a pretty stinging rebuke of the IOM recommendations, which it said could amount to a payment reduction of up to 35 percent for teaching hospitals.

"While the current system can and is being improved to train more doctors in non-hospital settings, these immediate cuts will destabilize a system that has produced high-quality doctors and other health professionals for more than 50 years and is widely regarded as the best in the world," AAMC president and chief executive Darrell Kirch said in a statement.

The IOM said the United States should maintain the $15 billion funding level for now, but policymakers in Washington should enact reforms to ensure that the federal government is funding a physician training program that meets the country's care needs. For example, while the IOM said it couldn't find credible evidence that the country faces a shortage of primary care physicians, there's more that the GME program should do to steer incoming doctors toward those careers. In the United States, primary care isn't nearly as lucrative as specialty fields.

Reforming GME will take resources and new authority, IOM says. The panel recommends setting up a new accountable governance structure within Medicare to oversee program spending. It points to the Medicare Payment Advisory Commission as a possible model — the independent panel, which advises Congress on Medicare issues, has 17 commissioners, 25 professional staff members and a $11.5 million budget. A new panel just for GME would likely be smaller, the IOM said.

Any change would take time, though. The IOM envisions a 10-year reform window before reassessing the program's funding needs.

Top health policy reads from around the Web:

Obamacare's employer mandate can't find any love. "When President Obama signed the Affordable Care Act, its requirement that large employers provide health coverage or pay a penalty seemed to many supporters a key pillar of the effort to guarantee health coverage to Americans. Four years later and after repeated delays, the so-called employer mandate has become something of an orphan, reviled by the law's opponents and increasingly seen as unnecessary by many of its backers. ... Backers of the Affordable Care Act list the employer mandate as among the provisions of the complex law that they hope could be revised when healthcare becomes less politically explosive." Noam Levey in the Los Angeles Times.

Narrow networks are probably here to stay. "By changing the rules, the Affordable Care Act pushed health insurers toward a new strategy: limiting the choice of doctors and hospitals they’ll pay for. ... The whole idea of the marketplaces was to give insurers an opportunity to compete for customers on price. And as long as price continues to drive shopping decisions and the old tactics are out, insurers have every motivation to keep these plans on the market." Margot Sanger-Katz in the New York Times.

What actually counts as a 'state' exchange? "One question that has arisen in the wake of the Halbig/King decisions is what exactly is a state exchange? The D.C. Circuit in Halbig and the Fourth Circuit in King seemed unclear as to the answer to this question. The D.C. Circuit counted 14 state exchanges, the Fourth Circuit 16. A great deal, however, may turn on the answer." Tim Jost in Health Affairs.