It's long been known that a daily smoking habit can take years, even as many as two decades, off a smoker's lifetime. But now the same is true for those suffering from a mental illness, according to new research.

It's even actually worse than smoking as mental issues can shear off 10 to 20 years where most lifetime heavy smokers lose about eight to 10 years.

The study from Oxford University researchers notes there has been little attention to the fact unlike the health impacts of heavy smoking.

The report focused on a wide range of mental illnesses, from childhood behavioral disorders to autism, and focused on 20 research reports that detailed insight on 1.7 million people and a quarter of a million death reports.

For example, those suffering with bipolar disorder will likely see a loss of nine to 20 years, states the study published in the journal World Psychiatry. Life expectancy is cut short by 10 to 20 years for those dealing with schizophrenia. Even those suffering from continual depression will see a loss of seven to 11 years off their life.

'We found that many mental health diagnoses are associated with a drop in life expectancy as great as that associated with smoking 20 or more cigarettes a day,' said Dr. Seena Fazel, of the Department of Psychiatry at Oxford University.

"There are likely to be many reasons for this. High-risk behaviors are common in psychiatric patients, especially drug and alcohol abuse, and they are more likely to die by suicide," he said, noting that those with mental health issues, and the mental health issues themselves, are often overlooked as health topics.

"Many causes of mental health problems also have physical consequences and mental illness worsen the prognosis of a range of physical illnesses, especially heart disease, diabetes and cancer," she said.

That scenario may change, she noted, with increased emphasis on public healthcare and helping those with mental issues get better and more research support.

"What we do need is for researchers, care providers and governments to make mental health a much higher priority for research and innovation."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.