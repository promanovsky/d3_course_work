Toyota Motor Corp. forecast that profit will fall from last year’s record as demand slumps in Japan, competition intensifies in the U.S. and the yen is no longer the boon it used to be.

Net income will probably slip to 1.78 trillion yen, or $17 billion, in the fiscal year that ends March 31, 2015, the Toyota City, Japan, automaker said Thursday. Toyota forecast profit that was 12% below the average analyst estimate compiled by Bloomberg.

President Akio Toyoda led the company founded by his grandfather to an unprecedented 1.82-trillion-yen, or $17.9-billion, profit in the 2014 fiscal year, as a weaker yen boosted the value of sales from Prius hybrids and Lexus vehicles exported from Japan. As the currency edge fades, Toyota and Honda Motor Co. are predicting smaller-than-estimated earnings and are among Japanese automakers bracing for a record decline in domestic demand because of the nation’s first sales-tax increase in 17 years.

“The results are rather disappointing,” said Satoru Takada, an auto analyst with Toward the Infinite World Inc. in Tokyo. “These results don’t show where the company is headed. They’re very conservative and safe.”

Advertisement

Toyota outsold all other carmakers in 2013 and maintained a lead over Volkswagen and General Motors Co. through the first three months of this year. Both Toyota and Volkswagen have forecast deliveries of at least 10 million vehicles in 2014.

For the fiscal fourth quarter, Toyota reported net income fell 5.4% to 297 billion yen, or $2.9 billion, missing the average analyst estimate for 359.6 billion yen, or $3.5 billion. Operating profit and sales also trailed estimates.

Still, full-year profit rose almost 90%.

As prospects dim for a further benefit from the yen, Toyota faces a number of challenges.

Advertisement

Safety defects have led to mounting global recalls, including one last month affecting more than 6 million vehicles to fix top sellers such as the Camry sedan and RAV4 sport utility vehicle, and another involving 1.9 million Prius hybrids in February.

In March, Toyota agreed to pay a $1.2-billion penalty to end a U.S. criminal investigation into sudden unintended acceleration problems with the company’s vehicles, which were tied to recalls of 10 million vehicles in 2009 and 2010.

Toyota’s sales have increased in the U.S. at a slower pace than the total market in the first four months of 2014 as deliveries slipped 0.2% for the Camry, the top-selling car for the last dozen years. Sales of the Prius, which last underwent a major overhaul in 2009, have slumped 18% this year, putting the company on course for a second-straight year of lost market share.

To compensate for its aging lineup, the automaker has boosted spending on incentives by 10% in the first four months of 2014, more than the industry average of 6%, according to Autodata Corp.

Advertisement

A restyled Camry also will roll into showrooms in the second half of this year. The sedan made its debut at last month’s New York International Auto Show with more contoured body panels, softer-touch interiors and added welds for sportier handling.