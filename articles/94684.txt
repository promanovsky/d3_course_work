The official song of the 2014 World Cup was released on YouTube this week, and it sounds like every other Pitbull song you’ve ever heard. Mr. Worldwide, Jennifer Lopez and Brazilian pop star Claudia Leitte teamed up for We Are One (Ole Ola), which unsurprisingly failed to best the greatest World Cup theme of all time, Shakira’s Waka Waka (This Time for Africa).

Waka Waka was not only legitimately one of the most enjoyable songs of 2010, it popularized this amazing dance.

A few of our other favorites: Ricky Martin’s La Copa de la Vida (The Cup of Life) in 1998.

The ultra-patriotic Gloryland from the 1994 World Cup.

1966’s World Cup Willie (Where in this World are We Going).