prices rose above $112 a barrel in Asia Tuesday, slashing last session’s losses as optimistic data from China – a major global economy and the world’s second largest oil consumer – supported an upside in the markets.

China's official Purchasing Managers' Index (PMI) stood at 51 in June, the National Bureau of Statistics said, growing at a faster pace compared with May's reading of 50.8 and in line with market expectations.

Fears of oil supply disruption in Iraq as ongoing frightening between military and militants closed its largest refinery and ongoing stand unrest in Ukraine have kept oil at elevated levels despite a weak demand outlook.

On the New York Mercantile Exchange, rose 0.32% to trade at $ 105.70 a barrel

On the ICE Futures Exchange in London, was at $ 112.67 a barrel, up by 0.19%

Oil markets have for weeks been rattled by supply concerns due to ongoing bloody Iraqi insurgency that would disrupt supply from the second-largest producer in the Organization of the Petroleum Exporting Countries.

In the week ahead, investors will be looking to the U.S. nonfarm payrolls report due on Thursday for further indications on the strength of the labor market. Key Purchasing Managers’ Index (PMI) manufacturing data out of Europe and China will also be in focus.

NYMEX Gasoline rose 0.22%, 0.67 points to trade at $ 305.00

NYMEX Heating Oil added 0.15%, 0.45 points to trade at $ 297.98