A little stress may be a good thing for teenagers learning to drive.

In a new study, teens whose levels of the stress hormone cortisol increased more during times of stress got into fewer car crashes or near crashes in their first months of driving than their less-stress-responsive peers did.

The study suggests that biological differences may affect how teens learn to respond to crises on the road, the researchers reported April 7 in the journal JAMA Pediatrics.

Efforts to reduce teen car accidents include graduated driver licensing programs, safety messages and increased parental management, but these efforts seem to work better for some teens than others, the researchers said.

Alternatives, such as in-vehicle technologies aimed at reducing accidents, may be especially useful for teens with a "neurological basis" for their increased risk of getting into an accident, they said.

The neurobiology of risk

Automobile accidents are the No. 1 cause of death of teenagers in the United States, according to the Centers for Disease Control and Prevention. Car crashes also kill more 15- to 29-year-olds globally than any other cause, according to the World Health Organization. [Top 10 Leading Causes of Death]

For the study, Marie Claude Ouimet, a professor of community health at the University of Sherbrooke in Quebec, and her colleagues recruited 42 teens in Virginia, and fitted their cars with video cameras, accelerometers, GPS and mileage recorders to record the teens' crashes and near misses during their first 18 months of driving.

They also took saliva samples from the 16-year-olds as they struggled with a series of stressful math problems, measuring how much the teens' cortisol levels rose during this stress test.

Stress and learning

The results revealed that the more responsive a teen's cortisol levels were to stress, the fewer crashes and near misses they experienced per mile driven, and the faster they saw their rates of crashing or nearly crashing drop.

The study was limited in that the participants were wealthier and healthier than all teenagers on average, and there were only a few, minor crashes during the experiment. Nevertheless, the findings suggest teens' stress responses are linked with their risk of getting into accidents, wrote Dr. Dennis R. Durbin, of the Center for Injury Research and Prevention at The Children's Hospital of Philadelphia, and colleagues in an editorial accompanying the study.

"[A]s teens gain independent driving experience, they are exposed to a variety of emotionally evocative stimuli such as near misses with unexpected hazards which enables them to better anticipate, avoid and react more effectively in the future," wrote Durbin and his colleagues, who were not involved in the research.

The hypothalamic-pituitary-adrenal (HPA) axis, which consists of brain regions and glands responsible for the stress response and for releasing cortisol, may influence how a teen handles these stimuli, the researchers suggested. Teens with a more reactive HPA may develop defensive-driving skills more quickly, they said.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.