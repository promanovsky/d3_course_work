As if a Type 1 diabetes diagnosis isn't enough of a challenge for young children and adolescents new research says a potential complication could prompt memory loss and impact attention skills in the first year of a diagnosis.

A new study by researchers at Royal Children's Hospital in Victoria, Australia says the brain issue complication, called diabetic ketoacidosis (DKA), has a potential to impact higher0learning tasks.

"Children and adolescents diagnosed with type 1 diabetes with diabetic ketoacidosis have evidence of brain gray matter shrinkage and white matter swelling," said the study's lead author, Dr. Fergus Cameron, head of diabetes services at Royal Children's Hospital in Victoria, Australia. "While these changes resolve within the first week, there are associated residual cognitive changes -- memory and attention -- that are present six months after diagnosis."

The DKA complication can also develop later due to poorly managed diabetes, says the report. The Juvenile Diabetes Research Foundation says 30,000 children and adults in the U.S. are diagnosed with Type 1 diabetes each year and the figure is growing annually. Diabetes is the seventh leading cause of U.S. deaths, states a report.

According to Cameron 20 and 30 percent of newly diagnosed patients could end up dealing with the DKA complication.

They study involved 36 children and teens diagnosed with diabetic ketoacidosis and 59 not impacted. The participants underwent MRI scans several times during the study to test memory and attention spans. The report claims there is decreased gray matter volume with those suffering DKA but the brain changes were not permanent.

"Changes in memory and attention are subtle, and may or may not be noticed by a parent or teacher on a daily basis," said Cameron. "However, any decrement in attention or memory in children is a concern as children are acquiring new knowledge and learning new skills all the time."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.