Natural methane leakage from vents in the seafloor are more widespread on the U.S. Atlantic margin than researchers previously thought.

Methane plumes are emanating from at least 570 seafloor cold seeps on the outer continental shelf and the continental slope, Mississippi State University reported. Cold seeps are areas where gas and liquids leak into the water.

"Widespread seepage had not been expected on the Atlantic margin. It is not near a plate tectonic boundary like the U.S. Pacific coast, nor associated with a petroleum basin like the northern Gulf of Mexico," said Adam Skarke, the study's lead author and a professor at Mississippi State University.

Researchers are yet to sample the gas being emitted from the seeps, but believe it is composed mostly of methane produced by underground microbes. Most of the seeps lie in water close to the shallowest conditions at which gas hydrate (an ice-like combination of methane and water) can exist.

"Warming of ocean temperatures on seasonal, decadal or much longer time scales can cause gas hydrate to release its methane, which may then be emitted at seep sites," said Carolyn Ruppel, study co-author and chief of the USGS Gas Hydrates Project. "Such continental slope seeps have previously been recognized in the Arctic, but not at mid-latitudes. So this is a first."

Most seeps observed in the study are too deep for the emitted methane to reach the atmosphere directly but it can oxidize into carbon dioxide, changing the acidity and oxygen levels in the ocean.

This study continues the tradition of advancing U.S. marine science research through partnerships between federal agencies and the involvement of academic researchers," said John Haines, coordinator of the USGS Coastal and Marine Geology Program "NOAA's Ocean Exploration program acquired state-of-the-art data at the scale of the entire margin, while academic and USGS scientists teamed to interpret these data in the context of a research problem of global significance."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.