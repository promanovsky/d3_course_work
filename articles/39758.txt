Lady Gaga isn’t retreating.

After her vomit-drenched performance at the recent South by Southwest music festival in Austin, Texas, inspired many Internet scolds to insist that she’d finally -- gasp! -- gone too far, the pop star might’ve been expected to restrain herself for her next move.

After all, with a run of shows at New York’s Roseland Ballroom set to kick off Friday night (and a North American arena tour after that), she needs committed fans right now more than she needs another round of media outrage.

Fortunately, though, Lady Gaga seems uninterested in such glum logic: Over the weekend, she unleashed a music video for her song “G.U.Y.,” and if you were fragile enough that her SXSW gig offended you -- as opposed to her truly icky corporate alliance -- then you’re in for another rude shock.

Advertisement

PHOTOS: Concerts by The L.A. Times

Filmed in large part at Hearst Castle in San Simeon, Calif., the “G.U.Y.” clip follows a loose narrative in which a winged Gaga is shot with an arrow, laid to rest in a swimming pool and then resurrected -- or something -- in time to jam with several ladies from “The Real Housewives of Beverly Hills.” (Another Bravo personality, Andy Cohen, appears in the video, which premiered Saturday night on Bravo’s sister network NBC.)

The singer also appears to wake Jesus, Gandhi and Michael Jackson from some kind of cryogenic deep sleep before taking over an office space in a bit that looks like “The Matrix” meets “The Wolf of Wall Street.”

Is it a lot? It’s a lot, which is probably why the clip is scored not just by “G.U.Y.” but also by parts of several other tunes from last year’s “Artpop,” including “Venus” and “Manicure.”

Advertisement

Watch it above and give thanks that Gaga is still going.

ALSO:

The VIP treatment at Britney Spears’ Vegas show

Eminem and Rihanna add second Rose Bowl date to Monster tour

Advertisement

Review: Kylie Minogue and Enrique Iglesias sex it up on new albums

Twitter: @mikaelwood

