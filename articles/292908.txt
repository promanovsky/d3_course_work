Not content with having actor, writer, film director, producer, lecturer and poet on his CV, the seemingly tireless James Franco can now add theatre director to the list.

Franco is to make his off-Broadway debut directing Robert Boswell's The Long Shrift at the Rattlestick Playwrights theatre in New York this summer.

The new play, about a teenage misfit accused of rape, launches the 20th anniversary season of the West Village venue, known for championing the work of emerging US writers.

Oscar-nominated for 127 Hours and currently appearing to mostly positive reviews opposite Chris O'Dowd in Of Mice and Men on Broadway, Franco has recently opened a drama school in Los Angeles that teaches techniques developed by the New York theatremaker Sanford Meisner.

To date, however, Franco's own directorial projects have focused on film. As I Lay Dying, his adaptation of William Faulkner's 1930 novel of the same name, debuted at the 2013 Cannes film festival, and Bukowski, a biopic about the beat poet's early years, is set for release later this year.

Taking in his projects on both sides of the camera, he currently has 14 films in production. A documentary about Franco's own life is also nearing completion, after the first-time director Lisa Vangellow tracked the pop-culture polymath for a year.

The Long Shrift will run at Rattlestick Playwrights theatre from 7 July to 27 August. The venue's anniversary season will also include premieres of Pitbulls by Keith Josef Adkins and Shesh Yak by Laith Nakli. Meanwhile Franco's Hollywood contemporary Julia Stiles will star in a Rattlestick co-production of Scott Organ's play Phoenix at New York's Cherry Lane theatre, also in July.