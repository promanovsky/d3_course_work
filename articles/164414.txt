A SpaceX rocket successfully completed a soft landing in the Atlantic Ocean this week, company CEO Elon Musk said Friday. The Falcon 9 rocket, however, may not be recoverable because of damage sustained in rough seas.

The landing was notable as the Falcon 9 used a powered descent to soften its landing after a resupply trip to the International Space Station. SpaceX has tested the soft landing procedure before but had not, until this week, used it as part of an actual space mission.

Musk, who is also the CEO of electric automaker Tesla, said Friday afternoon that the information relayed from the Falcon 9 during its descent into the Atlantic showed that it completed a stable upright landing.

“We have sensors on each individual leg [of the rocket],” Musk said, and “GPS units. They all agree [that it landed stably]. If we recover it from the ocean it’ll probably take a couple of months to refurbish for flight.”

Controlled rocket descents could greatly reduce spaceflight’s costs by removing the need to manufacture a new rocket for each launch. “You don’t have to keep rebuilding your rockets, there’s certainly a sustainability element there,” Musk said.

Here’s what it looks like when SpaceX successfully soft-lands a Falcon 9 rocket, as seen in earlier test footage:

Musk also announced Friday that SpaceX is filing suit against the U.S. Air Force to encourage competition for national security-related rocket launches. A joint venture between Boeing and Lockheed Martin provides all rocketry for USAF space launches. Musk said that arrangement is expensive, outdated and doesn’t allow new independent bidders to compete for procurement deals.

“This is not SpaceX protesting saying these launches should be awarded to us. These launches should be competed, and if we compete and lose that’s fine,” Musk said.

The suit was filed in the U.S. Court of Federal Claims.

Get our Space Newsletter. Sign up to receive the week's news in space. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.