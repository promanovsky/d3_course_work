LONDON — Barclays PLC BARC, -0.04% BCS, +0.62% announced plans Thursday to cut over a quarter of the jobs at its investment bank over the next three years, part of wider plan to boost returns at the lender.

The British bank presented a wider plan aimed at shrinking and simplifying its businesses, with a greater emphasis on its commercial and retail operations.

Barclays will cut 7,000 job in its investment bank by 2016. The slimmed down unit will represent no more than 30% of the group’s risk weighted assets, down from just over half now.

Goldman Sachs’s take on today’s ECB meeting

Earlier this year Barclays said it would cut its 139,600-strong workforce by as much as 9%, or nearly 12,000. It now expect cut 19,000 jobs across all divisions in the next three years, including those at the investment bank.

To speed the revamp Barclays said it would create a “bad-bank” division consisting of 115 billion pounds ($195 billion) of risk-weighted assets, which will include all its European retail-banking assets and some parts of the investment bank. Investment banking units facing the ax include nonstandard fixed-income derivatives, some commodities and emerging markets products, Barclays said.

The move is Chief Executive Antony Jenkins’s latest effort to retool the investment-banking division, where profit has fallen as a number of executives jump ship, regulators bear down on its riskiest activities and clients pull back from bond trading because of persistently low and stable interest rates.

“This is a bold simplification of Barclays. We will be a focused international bank, operating only in areas where we have capability, scale and competitive advantage,” Jenkins said in a statement.

The bank expects to incur £800 million of costs to on top of the original £2.7 billion restructuring costs it announced in February 2013. The outcome of the review is key for Mr. Jenkins. Since his promotion in 2012 to CEO, analysts questioned whether the former boss of Barclays’s retail division had the ability to reshape the lender’s investment bank, its main profit generator, while maintaining the group’s profitability. The investment bank currently employs around 24,000 people.

Revenue fell 41% in Barclays’s fixed income, currencies and commodities division in the first quarter, halving the investment bank’s operating profit. It was an even bigger fall than analysts had expected and compared with roughly 20% declines at rivals.