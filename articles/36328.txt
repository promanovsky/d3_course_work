'Fast & Furious 7': CGI May Be Used for Some Paul Walker Scenes (Report)

The director and crew are set for additional shooting in Abu Dhabi in April.

As Fast and Furious 7 gears up to resume production, more details about the send-off for Paul Walker's character are being revealed.

CGI and voice-effects, along with body doubles, may be used in scenes for Walker's detective character Brian O'Conner, according to The New York Daily News.

EARLIER: 'Fast & Furious 7' Writer Working on Fitting Send-off for Paul Walker

"They have hired four actors with bodies very similar to Paul’s physique and they will be used for movement and as a base," an unnamed production source told the paper. "Paul’s face and voice will be used on top using CGI."

Universal hasn't yet responded to THR's request for comment on the use of CGI for Walker's character.

The Hollywood Reporter earlier reported that Universal will use existing footage and screenwriter Chris Morgan worked on revisions that included retiring Walker's character instead of killing him off in the film.

Fast 7 director James Wan along with major cast members -- including Vin Diesel and Michelle Rodriguez -- will travel to the United Arab Emirates to shoot in Abu Dhabi in early April. Filming had been on hold after Walker died in a car crash on Nov. 30.