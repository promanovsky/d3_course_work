Economist Thomas Piketty warns that unless action is taken to redistribute wealth, inequality will widen until it becomes unsustainable. Development goal-setters ignore him at their peril

Focus on global inequality has gone mainstream. From papal proclamations to warnings of impending doom by the economist Thomas Piketty, social injustice is making headlines. Yet the issue is still not being considered seriously enough in the byzantine process of setting new development goals: the spotlight has remained on economic growth.

Tackling economic inequality is not easy, but as Piketty and others have stressed, ignoring it could create a timebomb, not least because it limits a country's ability to uphold human rights.

An ambitious, post-2015 fiscal revolution is needed to address key questions at the heart of sustainable development: how to raise enough resources; how to ensure those resources are used to tackle inequality and ensure no one is left behind; and how to provide transparency and accountability over public policy.

The question of who pays for all this is a political hot potato. The cost of delivering new sustainable development goals (SDGs), including climate change commitments, will be about $1tn per year.

The millennium development goals (MDGs) have been underfunded to the tune of $120bn, and with the SDGs we are talking about a much more ambitious programme.

Despite the financial crisis, the world has never been richer. The challenge is to use this wealth better, but a new approach is vital. That is why ideas such as a sustainable development solidarity capital tax, based on Piketty's proposal for Europe, should be considered.

The French economist suggests a 1% tax on all property and financial assets worth €1m-€5m, and 2% on capital above that. As with financial transaction taxes, there are practical questions about whether a sustainable development solidarity capital tax should be levied and spent domestically, or to what extent it should go into a global pot. These are questions the committee of experts on sustainable development financing should explore at its meeting in New York this week. All options, including those that may seem politically difficult, must be considered.

Fiscal measures such as this, and proposals to tackle illicit financial flows, offer the greatest hope of raising enough cash to pay for the SDGs. But raising money is not enough. The human rights principle of equality demands that we also attend to the impact of how resources are found and spent.

In many countries, regressive tax systems – which are heavily reliant on goods and services taxes, often at the expense of more progressive income and wealth taxes – have disproportionately burdened poor people. They have also fuelled rising income and wealth inequality.

Full transparency and meaningful public participation is required in domestic and global fiscal policymaking. Yet there is a long way to go before the conditions for accountable fiscal governance are in place worldwide. The new sustainable development framework must address accountability gaps at the international level.

It should include firm and monitorable pledges by powerful countries and international financial institutions to tackle tax evasion and other illicit financial flows. It should also ensure that tax policies in poor countries do not undermine their ability to meet human rights and sustainable development commitments.

The new SDGs could include targets to reduce economic inequality through enhanced use of progressive taxation on income and wealth. Indicators could track progress towards ending cross-border tax evasion, returning stolen assets, forgiving odious debt and progressively combating tax abuses.

Human rights and development advocates will also be watching to see whether member states involved in the financing conversation are brave enough to promote the fiscal revolution that will be vital for achieving post-2015 goals.

Nearly every review of Piketty's book has stressed how unrealistic his recommendations are. But a "mansion tax" in the UK is now a mainstream proposal, and a financial transactions tax – until recently dismissed as impractical – is poised to be implemented across Europe.

The SDGs are likely to take 2030 as their end date, and a lot can change in that time. This is no time for low ambitions. Given the magnitude of today's sustainable development challenges, and the mandate for transformative change that human rights provide, the international community should embrace proposals that think big and think ahead.

Nicholas Lusiani is senior researcher at the Centre for Economic and Social Rights. Helen Dennis is senior adviser on poverty and inequality at Christian Aid