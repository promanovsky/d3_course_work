More than two months after declaring the outbreak over, the U.S. Centers for Disease Control and Prevention said an additional 43 people have been sickened by strains of salmonella linked to Foster Farms poultry.

The new cases were reported between late February and March 18, bringing the total number of people sickened by the year-old national outbreak to 524, the CDC said Wednesday.

The outbreak first surfaced last October when the U.S. Department of Agriculture issued a health alert warning consumers of salmonella linked to three Foster Farms processing facilities in Central California.

Inspectors threatened to suspend operations at the plants after discovering poor sanitary conditions that could have contributed to the outbreak.

Advertisement

Foster Farms, which is headquartered 25 miles southeast of Modesto in the rural community of Livingston, never issued a recall. Costco and Kroger Co. pulled some Foster Farms products from their stores.

Federal inspectors and Foster Farms say salmonella-tainted chicken is safe to eat when handled properly and cooked to a minimum of 165 degrees Fahrenheit.

Salmonella is considered a common bacteria found regularly in poultry. Because it can be killed through cooking, government regulators allow some levels of the contaminant. But the bug is a growing concern for the industry, partly because strains are becoming increasingly resistant to antibiotics.

The CDC said the strains of Salmonella Heidelberg linked to the current outbreak have shown resistance to commonly prescribed antibiotics. However, those drugs aren’t typically used to treat salmonella infections, which can cause diarrhea, fever and abdominal cramps.

Advertisement

In statement released late Wednesday, Foster Farms said: “Since October 2013, Foster Farms has developed a multiple-hurdle approach to reduce or eliminate Salmonella at each stage of production -- from screening breeder flocks before entering the Foster Farms system, to farms where the birds are raised, to the plants where the chicken is processed as a whole bird and when it is cut into parts. As a result, the company has steadily reduced the prevalence of Salmonella at the parts level toward a goal of less than 10% -- well below the USDA-measured industry benchmark of 25%.