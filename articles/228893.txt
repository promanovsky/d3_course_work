"22 Jump Street" star Channing Tatum confesses in a new interview that he may have an issue with alcohol.

"I probably drink too much, you know," the actor says in the June issue of GQ, talking about how he enjoys sculpting in clay at night with a bottle of wine or a glass of bourbon. "I'm probably a pretty high-functioning, I guess, you know, I would say, alcoholic, I guess," he adds. "There's probably a tendency to escape. I equate it to creativity, and I definitely equate it to having a good time."

He notes the drinking is on and off, saying he cut back when he and his wife, actress-dancer Jenna Dewan-Tatum, welcomed their daughter Everly in May 2013. He says he would not drink during the four months of production on the upcoming sequel to his 2012 hit, "Magic Mike." Titled "Magic Mike XXL" and set for a July 2015 release, it's scheduled to begin shooting this fall.

"Then, at the end of that movie," Tatum, 34, tells the magazine, "it's go time."

Tatum also disputed Forbes magazine's claim that he made $60 million between June 2012 and June 2013, with "the vast majority" of it from "Magic Mike." The movie earned more than $167 million worldwide on a reported budget of $7 million, with many outlets claiming he netted $40 million.

"For movie people to think that anyone made $40 million by themselves on a movie is absolutely insane," he said.