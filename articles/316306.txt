We’re here at Moscone West for Google I/O 2014!

We are on the ground and live from Google I/O 2014! This year, Google has made some interesting changes to their format, and the entire feel of the event hints that it will be about the Developers and what they can do. From a new version of Android to other odds and ends we’re expecting to see (or not see), this year’s I/O should be a fun one.



The question on everyone’s mind is what we’ll see this year from the keynote. The absence of Google I/O creator Vic Gundotra is not to be overlooked, and signals a new direction for the event. This one is also a bit later in the year that usual, and is a mere two-day shindig.

What we’re about to see, though — that’s the exciting part. Android 5 (or at least the next version of Android) is coming, and we’re expecting an Android TV to arrive as well (or hoping, really). Android Wear is also going to make a big splash, and we’re hoping for some face time with the G Watch and Moto 360.

There are some omissions, but it’s time to focus on what will be happening. Google’s aim this year is better apps; ones that both look and react to users better. Above all, that tells us this event is about Developers, and Google pushing them in the back a bit to try just a bit harder to do more. We’re sure there will still be a lot of fun things for us to bring you, so be sure to follow our guide on how to keep tabs on Google I/O this year!

