Find Eric Fisher on Twitter and Facebook

It could be one for the ages for those lucky enough to enjoy clear skies. On Friday night into the pre-dawn hours of Saturday, Earth will take on a barrage of meteors, dubbed the ‘May Camelopardalids.’ They’re caused by dust from periodic comet 209P/LINEAR, and no one alive has seen this show before. The last time it came around was in the 1800s. There’s definitely an increasing giddiness amongst skywatchers, knowing that this is something no one else walking around the Earth has been able to take in before! Perhaps a particularly long-lived giant tortoise took in the show, but that’s about it.

So how many will be bombarding our atmosphere? Some forecasters are calling for up to 200 meteors per hour! To put that in perspective, here’s a list of all the larger meteor showers we see on Earth each year, from the American Meteor Society. For us, the Perseids are usually the show-stoppers. That’s due to both the rate and the conditions – it’s a lot nicer to stand outside in August than in January! But they’re also more dependable than the Quadrantids in terms of higher numbers.

Four different rates are given for each shower, under the following conditions:

city sky or rural sky with full moon, suburb sky or rural sky with quarter moon, rural sky and moonless, calculated Zenith hourly Rate, ZHR.

Date Shower 1 2 3 4

Jan 3-4 Quadrantids 5 10 25 120

Apr 21-22 April Lyrids 3 5 10 18

May 4-5 Eta Aquarids 3 5 10 60

Jul 28-29 Delta Aquarids 3 5 10 20

Aug 12-13 Perseids 10 20 50 100

Oct 21-22 Orionids 5 10 15 23

Nov 3-13 Taurids 1 2 3 5

Nov 16-17 Leonids 3 5 10 20

Dec 13-14 Geminids 20 50 75 120

Dec 21-22 Ursids 1 2 5 10

Viewing Time & Expectations:

Most estimates have the peak viewing time between 2am and 4am Eastern Daylight Time, Friday night into Saturday. And lucky us – North American viewers are favored for this shower. How did we just find out about the event? Well you can thank our friends at MIT, along with the Institute of Technology Lincoln Laboratory and the US Air Force. They are involved in a collaboration, the Lincoln Near-Earth Asteroid Research project, which is tasked with looking for rocks out there in the universe that may end life on Earth. Lo and behold, they came across Comet 209P/LINEAR in February of 2004. The astronomers calculated Comet 209P/LINEAR’s orbit and found that it returns about every five years in an orbit between the sun and Jupiter. They’ve managed to trace it back to 1703. There *IS* a catch though – no one really knows how much debris, for lack of a better term, is actually out there. In other words – how much did the comet leave behind for us to plow through?

“The general consensus is that this week’s Camelopardalids will be comparable to a very good Perseid meteor shower with an added possibility of a storm,” says Geoff Chester, astronomer at the U.S. Naval Observatory. “I’m planning to be out watching.”

All you really have to do is grab some coffee and look up to take in the scene, but more specifically, look north – close to Polaris, the North Star. The meteors will appear to emanate from the constellation Camelopardalis, the giraffe.

Could it be a dud? You bet. Remember the huge excitement over Comet ISON last year? While it was great to look at through a telescope, it never lived up to the high expectations regarding brightness to the naked eye. There’s some inherent uncertainty when it comes to forecasting a planet, moving 67,000 miles per hour around the sun, connecting with the path of a comet that we’ve never traveled through before (so far as we know). I’m sure you understand :-)

Who else takes a hit? The moon! In fact, if you really want to have yourself a night, the impact of comet debris vs. the moon could lead to visible explosions on its surface, seen through backyard telescopes. Yep. That’s about as geeky/awesome as anything I can imagine.

What if there are barely any meteors? Consolation prize! You could still take in a very beautiful sight. The crescent Moon and Venus are converging for a tight conjunction Sunday morning, May 25th. You can look up and find them rising together just ahead of the sun in the eastern sky at dawn.

The Forecast:

Okay now that I’ve got you all worked up, I have some bad news. You may have to hop on a plane or take a road trip out of Southern New England to see this one. We’ll have just come off a mainly cloudy and, at times, stormy Friday. Low level moisture will be tough to kick out Friday night without any dry winds moving in behind a departing ocean circulation. Some areas of fog and low clouds will most likely persist for the majority of the night, even until sunrise on Saturday. I don’t want to be a complete pessimist and say there’s no chance, but breaks in the cloud cover look to be limited. I suppose the only good news is that IF any breaks can occur, they may open up after midnight, when the show is going to be best anyway. If you want to believe in miracles, set your alarm and head outside! Get away from ambient light if you can, and look north. And if you snap a good pic, send it our way!