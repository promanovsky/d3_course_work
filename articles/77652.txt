Success has a thousand fathers, so it is hardly surprising that both sides of Australian politics welcomed Monday's International Court of Justice ruling that Japan's ''scientific'' whaling program in the Southern Ocean was not so scientific at all.

It was always a ridiculous justification – what other study involves the annual systematic killing of the subject species?

But ridiculous or not, the path of international litigation was a serious escalation by Australia with known risks. Success was anything but guaranteed, making the decision to accept the costs of dragging one of Australia's most important commercial and strategic partners to the ICJ a particularly brave one.

The now retired former environment minister, Peter Garrett, argued the case in cabinet to make the application. He deserves much of the credit. Other names such as Kevin Rudd, and Stephen Smith were also important as was the current environment minister, Greg Hunt on the Liberal side.