Seth Rogen and James Franco have released the preview for their new film "The Interview," which has raised the eyebrows of several in the North Korean government. The movie centers on Franco's character, a late night talk show host, who lands an interview with Kim Jong-un, the nation's leader.

After the CIA learns of the interview they convince Franco and Rogen, who plays a producer, to assassinate Jong-un.

"A film about the assassination of a foreign leader mirrors what the U.S. has done in Afghanistan, Iraq, Syria and Ukraine," said Kim Myong-chol, a rep for North Korea.

"And let us not forget who killed [President John F.] Kennedy . . . President [Barack] Obama should be careful in case the US military wants to kill him as well," he added.



"The Interview" is scheduled for release in October.

For comments and feedback contact: editorial@rttnews.com

Entertainment News