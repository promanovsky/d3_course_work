Videos of Justin Bieber's legal deposition have made their way onto the Internet, showing off the popstar's shocking behavior while speaking to lawyers.

In the videos posted by TMZ, Bieber refused to answer questions about Selena Gomez, made fun of a court reporter and dissed his former mentor R&B singer Usher.

"When asked if Usher discovered him, Bieber disses his mentor and says he was actually discovered on YouTube," TMZ reports. "The best part ... he clearly was trying to say he himself was instrumental in his success, but instead he says, 'I was detrimental to my own career.' Truer words have never been spoken."

Bieber's lawyer cleared up confusion about his client's statement, clarifying the singer meant "instrumental," not "detrimental."

The singer's deposition was relating to one of his bodyguards allegedly beating up a photographer. When asked if he had spoken to his on-again-off-again flame Selena Gomez about his "dislike" for paparazzi, Bieber immediately became defensive and furiously started wagging his finger at the court's lawyers.

Unfortunately your browser does not support IFrames.

The popstar told the court he would not be answering questions about her, and Bieber's lawyers' thought it best to take a break from the deposition. However, Bieber also refused to answer simple questions, like if he'd ever traveled to Australia.

"He just can't hide his contempt for the photog's lawyer, and it comes out in pretty awful ways. We put some snippets together and you'll get the idea. It represents his attitude throughout the depo," TMZ reports.

The popstar has every right to refuse to answer specific questions during the deposition, but Bieber struggled to keep his composure during throughout his legal hearing. You can watch all four videos of Bieber's deposition here.

What do you think about Justin Bieber's behavior during his legal deposition? Let us know your thoughts in the comments section below!

Unfortunately your browser does not support IFrames.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.