Is it un-American to say that some of us just don’t get the obsession with comic book conventions and movies featuring superheroes? To better understand this fascination, we did a little digging and here is what we discovered.

Comic-Con?

Post-San Diego’s 2014 Comic-Con, the headlines screech about the event — Hollywood A-listers who attended, crowds that got out of control and women who were sexually harassed. All of this had some Americans (OK, me) asking, “Will someone please explain to me exactly what the f*** Comic-Con is and why so many people are obsessed with it?”

Comic-Con (for those of us who live on the fringe and never cared enough to look into it) is — or at least was — a comic book convention that started in San Diego, California, in 1970, and had about 300 attendees. It has been dubbed everything from a “nerd prom” to “a celebration of popular arts” to “the comic book industry’s version of the Oscars.”

The event has since turned into an orgy of 125,000 people — including vendors, red carpet celebrities and ordinary American citizens decked out as their favorite comic book character. Buy why? Why would so many people participate in something like this? We already have Halloween, right?

Still with the superhero movies?

Perhaps as an entertainment editor, one gets jaded about the hype. I swear to all that is good and holy, every time I see a headline about another blockbuster based on a comic book hero, my knee-jerk reaction is the same — after a loud groan, I mutter, “My kingdom for an original premise.” I’m not proud of it — I wish I enjoyed this theme as much as the millions of Americans who are making these stories so popular. But I don’t. To me, it seems like Hollywood has just gotten extremely lazy and is bastardizing the same themes over and over again because of the theme’s monetary success.

The punchline is always the same — nerd’s alter ego engages in an epic battle of good vs. evil and after a lot of really loud surround sound noise, wins. It’s a good baseline — I’m not saying it isn’t. Since the beginning of time we’ve honored the fight between good vs. evil where good prevails. It’s just starting to feel a bit tired, and there are thousands of ways to tell a story of good vs. evil that don’t involve capes, tights and super hot women in need of rescuing.

So again, why?

A cynic might suggest it’s all about marketing and millions and beating the hell out of a successful formula for as long as the American public will allow. The non-cynic thinks it’s more about our need for heroes who can conquer all the world’s ills.

According to an ABC News article by David Wright, America’s fascination with superheroes waxes and wanes, depending on what is happening globally. In the early days, Superman fought Nazis. ABC News interviewed comic professor (who knew?) Jim Higgins, who said in the ’60s, no one took comics seriously. “For a long time, comics has this air of disrespectability.” Higgins goes on to explain that after Sept. 11, the popularity of superheroes returned — in a large way.

OK, maybe I’m starting to get this. That makes sense. The world would be a better place if we had a few superheroes with superpowers who were able to wipe out evil once and for all. And I’ll admit it is fun to imagine which superpower you’d pick. Being invisible would make eavesdropping a lot easier and the ability to fly would make the commute to work faster. Still, I’m not quite ready to dress up as Wonder Woman and head to San Diego next year.