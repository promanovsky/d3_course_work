The CBS legal drama delivers one of its most emotional hours in the aftermath of last Sunday's shocking death.

[Warning: Spoilers ahead from Sunday's episode.]

"I don't know what to do."

Alicia and Co. began to mourn the sudden loss of their friend/colleague/adversary Will Gardner on Sunday's The Good Wife, and in some ways, the aftermath of his death was far more difficult to experience. Up until that point, the characters were focused on their respective conflicts and issues -- for Governor Florrick, the impending voter fraud controversy; for Alicia and Cary, Florrick Agos' battle for clients against Lockhart Gardner; for Kalinda, her professional future; for Diane, Lockhart Gardner's desired plans for West Coast expansion. All those issues became trivial within seconds.

STORY: 'The Good Wife' Preview: 5 Things to Look for in the Aftermath

"The Last Call" brought to bear the events and conversations leading up to the deadly courtroom shooting, with a breakdown of the moments immediately afterward as relayed by various eye witnesses: the judge, the security guard, the shooter among them. But what fueled the episode, Josh Charles' final as a regular, was the mystery over the intent of Will's brief voicemail -- his "last call," if you will -- left on Alicia's cell just moments before the shooting.

The Hollywood Reporter breaks down the most important elements of the episode. (Weigh in on the episode in the poll below.)

The Mourning Begins

Alicia began going through the stages of grief almost immediately after learning of Will's death: denial (to Kalinda: "But I just saw him yesterday") and anger (to Grace: "You believe God is good … I don't find any good here"). Alicia sought comfort at Lockhart Gardner, the firm that kick-started her law career, grieving with an equally distraught Diane ("I loved him…" "I know…" "He loved you…" went their conversation). Others took the news in their own ways: anger and aggression (Cary), unfettered sadness (David Lee, Kalinda), worry and concern (Governor Florrick, Eli Gold).

PHOTOS: When Leads Vanish: TV Shows That Have Killed Off Major Characters

The Shooting

Details had been sketchy over what took place in the Cook County courtroom, but a timeline became clearer as the episode wore on, all put together through different accounts. The case was leaning toward Will, who was representing the accused, Jeffrey Grant, and he and Assistant District Attorney Finn Polmar (the newly promoted regular Matthew Goode) were negotiating a plea agreement. But in an act of frustration, the accused took the gun from the nearby guard and shot the witness dead, forcing Will to intervene, causing him to be the next one hit -- three total gunshot wounds. (The first bullet to the thorax was the fatal shot.) Finn covered Will with his body then dragged him over to a safer area before getting shot too, staying with him until paramedics arrived.

The Voicemail

Will's vague voicemail at 11:32 a.m. was made just before Will and Finn began their plea bargain, leaving more questions than answers. "Alicia…" Will began the message, before a distant voice (later confirmed to be the judge) interrupted the call: "Mr. Gardner, we're just about ready here." "Hold, hold on your honor … I'll call you back."

The different scenarios for why Will would call Alicia swirled. Option No. 1: "Alicia, this feud -- it's stupid. I care about you too much to let it come between us." Option No. 2: "Alicia, are you kidding me? Leave my clients alone, Alicia. Find your own!" Things between the two former Lockhart Gardner colleagues had thawed a bit, so the latter seemed unlikely, but things changed on a dime.

STORY: 'The Good Wife' Bosses Sound Off on Shocking Exit

It was through a conversation with Finn that Alicia's greatest fear was eased -- that Will's last thought of her was of anger and disappointment. Turns out the lawyer Will was angry about wasn't Alicia after all (Damien Boyle, for the record), though she did just recently steal a client. A small sigh of relief. So what prompted Will's call to Alicia? Finn shed some light, though it's all inference at this point: "I had some pictures on my phone -- my wife, my son. He asked me about them." "What did he ask?" "Their names…"

Was Will about to tell Alicia that he wanted to go all in, to go back to the way things once were? The tragedy of Will and Alicia was that they never had great timing. And this time, we'll never know.

"Alicia, I'm sorry. I want what we had. I want to be with you and only you forever. Call me back please."



Please enable javascript.



Other observations from the episode:

- Most heart-wrenching moment: Alicia crying alone in the car.

- David Lee may be Lockhart Gardner's resident cynic, but this was one of the first instances where he showed genuine emotion. Though moments later, he'd persuade Diane to let him notify Will's clients and ensure their firm loyalty as a preemptive strike. (Eli Gold would do the same, giving a call to Agent Dubeck regarding Governor Florrick's voter fraud case.)

- Amid all the sadness, Eli Gold's impromptu takeover at the Chicago Correspondents' Luncheon provided much-needed levity as he stumbled over the speech originally supposed to be given by Alicia.

- Jeffrey Grant, confirmed as Will's killer, wanted to end his turmoil by committing suicide, but Kalinda wouldn't let him, though at first it seemed she would: "You need to live with this."

- Diane made her first big decision in the post-Will era: firing one of Will's top clients, Bob Klepper, and black-balling him from joining competing firms. "If I were dead, it's exactly what Will would do."

- Grace chose to believe that Will was now in heaven, something Alicia had difficulty wrapping her mind around. "You think God is good. I don't find any good here," she said. "What does it mean if there is no God?" her daughter asked. "It's not better, it's just truer." It was a heartbreaking conversation between a grief-stricken mother and daughter: "Mom, I just want you to be happy." "I will eventually."

What did you think of the wrap-up to Will's story? Sound off in the comments below.

The Good Wife returns April 13 on CBS.

Email: Philiana.Ng@THR.com

Twitter: @insidethetube