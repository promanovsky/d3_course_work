WASHINGTON, June 13 (UPI) -- The FDA announced in a statement Wednesday that it will scale down its fight against wood-aged cheese after angering artisanal cheese businesses and enthusiasts.

They began cracking down on artisan cheese after passing the Food Safety and Modernization Act of 2011. The FDA sent a letter to the New York State Department of Agriculture and Markets claiming that cheeses aged on wooden boards are unsafe because they are too difficult to clean.

Advertisement

"The porous structure of wood enables it to absorb and retain bacteria, therefore bacteria generally colonize not only the surface but also the inside layers of wood. The shelves or boards used for aging make direct contact with finished products; hence they could be a potential source of pathogenic microorganisms in the finished products," FDA representative Monica Metz wrote.

The cheeses affected by this rule would include beaufort, comté, reblochon, abondance, Vacherin Mont d'Or, Salers and the very popular parmigiano reggiano. This means that regulations could severely hurt the artisan cheese business as it would block many imported cheeses. Artisanal cheese business owners also believe the aging process on boards gives certain cheeses their unique flavor.

"Wood is a living thing, and cheese is a living thing, and when you combine them it creates a microbiological environment for the cheese. It imparts a flavor, and creates an environment for a healthy rind to grow on the cheese. Basically, it's a catalyst for beneficial bacteria to grow on the outside of that cheese to essentially prevent the bad mold and allow the good mold to grow. Thirdly is the humidity factor -- the boards are acclimated to a certain humidity. The wood holds its own humidity and slows out the drying of the cheese," said Chris Roelli, a fourth generation Wisconsin cheese maker .

The FDA is mainly concerned about the prevalence of listeria, which can come from poorly maintained boards. If the boards are well maintained, they provide the good bacteria that gives cheeses their flavor.

After seeing the response from the artisanal cheese community, the FDA released a new statement saying that they are opening a dialogue so they can learn more about the health safety and advantages of aging cheese with wooden boards.

RELATED Congress may speed up FDA approval process for new sunscreen ingredients