Xbox One vs PS4 Sales: Microsoft Passes 5 Million Sold to Retailers; Sony Reaches 7 Million to Customers Xbox One vs PS4 Sales: Microsoft Passes 5 Million Sold to Retailers; Sony Reaches 7 Million to Customers

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Microsoft has just announced the Xbox One sold over five million consoles since its launch globally. These numbers were released shortly after Sony released their sales figures for the PlayStation 4.

According to NPD Data, approximately 311,000 consoles were sold in the month of March. These numbers were undoubtedly bumped with the release of the acclaimed Titanfall.

Yesterday Sony announced they had just passed the seven million sold milestone for their PS4.

Interesting to note is PS4's sales numbers indicate customer purchases, while Xbox One sales figures indicate sold to retailers. This means actual sold consoles to consumers numbers are quite large between the two.

However, Yusef Mehdi, the Xbox corporate vice president, said the new console's sales are "outpacing Xbox 360 by more than 60 percent at the same point in time," reported Polygon.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Meanwhile, PlayStation 4 also has over 20.5 million in software sales, and the share feature has been used over 135 million times, reported Polygon.

"The PS4 journey has just begun, and although we are still facing difficulties keeping up with the strong demand worldwide, we remain steadfast in our commitment to meet the needs of our customers, and surpassing the wildest expectations of gamers by delivering new user experiences that inspire and engage," Sony CEO Andrew House said told Polygon. "We look forward to unveiling many of these experiences to our fans in the coming months."

These next coming months will be the biggest for the two companies as summer releases will determine which console starts to run away in popularity. Early numbers dictate it is the PS4, however the biggest complaint for the next-gen systems is the lack of games.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit