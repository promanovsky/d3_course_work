GoPro, a leading producer of wearable HD action cameras, raised $427 million by offering 17.8 million shares (50% insider) at $24, the high end of the $21-$24 range. At the IPO price, the company will have a fully diluted market cap of $3.5 billion and an enterprise value of $3.3 billion. GoPro will list on the NASDAQ under the symbol GPRO. J.P. Morgan, Citi and Barclays acted as joint bookrunners on the deal.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.