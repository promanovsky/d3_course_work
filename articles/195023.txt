Jon Hamm shows off his rugged good looks in a suit on the cover of Vanity Fair‘s June 2014 issue.

Here’s what the 43-year-old actor had to share with the mag:

On a low point in his career in the 90s: “I was actually at that time working as a set dresser for Cinemax soft-core-porn movies. It was soul-crushing.”

On living cross-country from girlfriend Jennifer Westfeldt: “One or the other of us has to make the flight. It helps. Physical presence is a big part of it. Home is where the dog is.”

On Mad Men: “This is the best job I’ve ever had and maybe ever will have in my life�”it’s so fun to play all of this. It can be relentlessly dark. It can be terribly sexually inappropriate, is a way to say it. But who else gets a chance to do any of that stuff? There’s so much there.”

On his childhood: “Eleven-thirty would roll around�”10:30 in the Midwest�”and the big huge 22-inch TV would be tuned to Saturday Night Live. I would sit two feet in front of the TV and stare and watch the whole thing.”

For more from Jon, visit VanityFair.com.

Bigger cover inside…