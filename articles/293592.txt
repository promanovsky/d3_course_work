A child's reaction to a caffeinated beverage changes once puberty is passed, reports new research, and boys appear to experience a more dramatic impact.

A study of 96 children reveals all children show an increase in blood pressure and a decrease in heart rate, according to associate professor Jennifer L. Temple, who lead the University of Buffalo research team. But post-puberty, the impact between the genders is evident.

"After puberty, however, caffeine was found to affect boys and girls differently, with boys having a greater response to caffeine than girls," she said. The participants ranged in ages from 8 to 17 years old and provided insight on caffeine intake.

The research efforts comes on the heels of a February report which revealed teenagers and kids are consuming caffeine at an increasing rate, according to a new government study. Although young people are drinking less soda, the consumption of energy drinks, tea and coffee is rising. While the total percentage of young people consuming caffeine daily, 73 percent, has held nearly steady over the last decade, coffee has increased in popularity from 10 percent to 24 percent. Part of this may be accounted for by the increasing popularity of sweet coffee-blended drinks. Energy drinks, nearly unheard of in 1999, were the primary source of caffeine for 6 percent of youth in 2010.

The University of Buffalo study shows boys and girls, post puberty, undergo a different response to caffeine, though the study does not reveal the specific reason. As part of the study each participant was given a specific caffeine dose as well as a placebo dose. Heart rate and blood pressure were monitored after each intake.

The researchers say the impact on children, boys and girls, pre-puberty was the same. There was also a reported bigger heart rate drop among female teens who were on their menstrual cycle. The study, "Cardiovascular Responses to Caffeine by Gender and Pubertal Stage," was published Monday in the journal Pediatrics and funding was provided by the National Institute on Drug Abuse.

"One potential explanation for this finding is that changes in steroid hormones that occur with pubertal development alter the metabolism of caffeine, which results in differential cardiovascular responses to caffeine in boys compared with girls," the authors wrote.

TAG caffeine, girls, boys, Gender, Health

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.