Stocks have moved mostly higher in early trading on Tuesday, adding to the strong gains posted in the previous session. The major averages have shown notable moves to the upside, with the S&P 500 reaching a new record intraday high.

The major averages have moved roughly sideways in recent trading, hovering near their highs for the young session. The Dow is up 91.77 points or 0.6 percent at 16,549.43, the Nasdaq is up 50.33 points or 1.2 percent at 4,249.32 and the S&P 500 is up 10.71 points or 0.6 percent at 1,883.05.

After seeing initial strength, stocks have continued to perform well following the release of the Institute for Supply Management's report on activity in the manufacturing sector.

The ISM said its purchasing managers index crept up to 53.7 in March from 53.2 in February, with a reading above 50 indicating growth in the manufacturing sector.

The internals of the report were somewhat mixed. While the production and backlog of orders indices showed notable increases, the employment index saw a moderate decrease.

Peter Boockvar, managing director at the Lindsey Group, said, "Bottom line, some normality in the was reflected in the data but the headline figure is still 1 point below its 6-month average."

"I'm not sure if the further push higher in the S&P's is because they liked the internals of the data or are more focused on the mediocre headline figure and weak employment component that could keep the Fed ever more dovish," he added.

A separate report from the Commerce Department showed a modest uptick in construction spending in the month of February.

Continuing to recover from their recent pullback, biotechnology stocks have moved sharply higher in early trading. The NYSE Arca Biotechnology Index has surged up by 2.9 percent, bouncing further off the nearly two-month low set on Friday.

Networking, software, and airline stocks are also seeing early strength, moving higher along with a majority of the other major sectors.

In overseas trading, stock across the Asia-Pacific region turned in a mixed performance during trading on Tuesday. Japan's Nikkei 225 Index edged down by 0.2 percent, while Hong Kong's Hang Seng Index surged up by 1.3 percent.

Meanwhile, the major European markets have all moved to the upside on the day. While the French CAC 40 Index has advanced by 1 percent, the U.K.'s FTSE 100 Index and the German DAX Index are up by 0.9 percent and 0.7 percent, respectively.

In the bond market, treasuries are seeing moderate weakness, extending the pullback seen over the two previous sessions. Subsequently, the yield on the benchmark ten-year note, which moves opposite of its price, is up by 3.6 basis points at 2.759 percent.

For comments and feedback contact: editorial@rttnews.com

Market Analysis