A new, tiny species of elephant shrew, also called a round-eared sengi, has been discovered in the Namib Desert in Africa, scientists say.

The newbie, now called Macroscelides micus, is the smallest member of the scientific order Macroscelidea, which now includes 19 known sengis. Like other sengi, the creature sports a narrow, trunk-like snout.

One of the study researchers, Michael Griffin of the Namibia Ministry of Environment and Tourism, collected the first representative of this newfound species around the ancient Etendeka volcanic formation, which is an arid area inland from the coastal Namib Desert between the Ugab and Hoanib rivers. At first, the researchers thought the creature was a known species from Namibia, Macroscelides flavicaudatus. [See Photos of Evolution's Most Extreme Mammals]

"We knew that it looked a little odd, but it was the genetic analyses that suggested that it was really very different," researcher John Dumbacher, curator of ornithology and mammalogy at the California Academy of Sciences in San Francisco, told Live Science in an email. "Once we got back to the field and saw several live individuals, it was clear that they differed from M. flavicaudatus in many ways, and that this wasn't just an 'odd' individual."

For instance, not only is the newbie smaller than any other sengi at just 7.5 inches from nose-tip to tail-tip it also has redder fur and lighter skin, particularly noticeable on the ears and feet, Dumbacher said.

"They also have a very large scent gland on their tail, which is probably important in signaling other members of their species in order to find mates and mark territories," Dumbacher added.

Subsequent trips taken by the team revealed the newfound sengi lives throughout this ancient volcanic region, which is about 136 miles long and about 62 miles wide, Dumbacher said. The creature likely evolved its red fur as an adaptation to blend into the region's red soil.

"We hope to learn more about this in coming field seasons, where we plan to radio-collar some of these small sengis and study their activities and spatial movements," Dumbacher said.

The little sengi is described this week in the Journal of Mammalogy.