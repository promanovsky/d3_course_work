Movie

Warner Bros. CEO Kevin Tsujihara convinced the author to adapt the book into big screen for three installments.

Mar 31, 2014

AceShowbiz - One of Harry Potter's texbooks will come to life as a series. J.K. Rowling's "Fantastic Beasts and Where to Find Them" would be made into a trilogy, Warner Bros. CEO Kevin Tsujihara confirmed in his profile.

The movie was first announced in September along with the revelation of Rowling as the screenwriter. Tsujihara convinced her to adapt the book into big screen. "We had one dinner, a follow-up telephone call, and then I got out the rough draft that I'd thought was going to be an interesting bit of memorabilia for my kids and started rewriting," Rowling told the New York Times.

She added, "When Kevin got the top job, he brought a new energy, which rubbed off. He's a very engaging person, thoughtful and funny." "Harry Potter" regular David Heyman will produce the movies. No release dates have been announced.

The book was written by Rowling between the publication of the fourth and fifth "Harry Potter" series. An extension to the Harry Potter world, it is set in New York, 70 years before Potter's days. It follows "magizoologist" named Newt Scamander who is the author of a guide to magical creatures.