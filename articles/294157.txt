An agreement to restore the Chesapeake Bay by protecting its vast drainage area from pollution was signed on Monday by officials that included governors from six U.S. states.

The Chesapeake Bay Watershed Agreement is aimed at restoring and conserving the 64,000-square-mile watershed that spans six states and the District of Columbia and drains into the bay, the biggest U.S. estuary.

The accord updates an existing agreement and marks the first time that Delaware, New York and West Virginia have pledged to work toward restoration goals in the bay that go beyond water quality.

"All of these actions, if we continue to take them, are going to yield better and better results," Maryland Governor Martin O'Malley, a Democrat, told a news conference.

The accord was signed by the governors of Delaware, Maryland, New York, Pennsylvania, Virginia and West Virginia; District of Columbia Mayor Vincent Gray; Ronald Miller, chairman of the Chesapeake Bay Commission; and Gina McCarthy, the administrator of the U.S. Environmental Protection Agency.

For decades, overfishing, silting and pollution have taken a toll on the Chesapeake. Polluted runoff from urban areas and farms has been especially harmful to the bay, which produces only about 1 percent of the oysters it did in the late 19th century.

In a 2012 progress report, the non-profit Chesapeake Bay Foundation called the bay "dangerously out of balance" but said the estuary's overall health had improved since the previous review in 2010.

The 20-page agreement signed on Monday sets 10 goals and 29 projected outcomes and deadlines that aim to help create an environmentally and economically sustainable watershed.

They include lowering nutrient and sediment pollution, ensuring the water is free of toxic contaminants, sustaining blue crabs, oysters and forage fish environment, and restoring wetlands and underwater grass beds.

Virginia Governor Terry McAuliffe warned that the program could only succeed with more money from federal government.

McAuliffe said he had met Secretary of the Interior Sally Jewell and requested $50 million to help fund the regional initiative.