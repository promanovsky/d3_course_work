FXStreet (Łódź) - Speaking at the Atlanta Fed’s Financial Market Conference, Fed chair Janet Yellen says that major US banks might need more capital.



• "There might be room for stronger capital and liquidity standards for large banks," she suggests.



• New regulations do not encompass shadow banking.



• The FOMC is discussing further measures to address short-term wholesale funding market stability risks



• “New Basel liquidity rules do not fully address short term funding risk,” the Fed chair says.