Ford Motor Company (NYSE:F) released the results from its first quarter, posting earnings of 25 cents per share, excluding items, on revenue of $35.6 billion. Analysts had been expecting the automaker to report earnings per share of 31 cents on $34.06 billion in revenue.

Breaking down Ford’s results

According to this morning’s report, Ford Motor Company (NYSE:F) reported that pretax profits declined by $765 million year over year to $1.4 billion. The company’s net income fell $622 million to $989 million or 24 cents per share, including $122 million in special items.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Operating-related cash flow for Ford Motor Company (NYSE:F)’s automotive division was $1.2 billion. The automaker ended the quarter with gross cash of $25.2 billion in its automotive segment. That’s $9.5 billion more than the segment’s debt and a strong liquidity position at $36.6 billion. Ford Motor Company (NYSE:F) reported that revenue and wholesale volume both rose year over year, as it gained market share in the Asia Pacific region, saw record profits there, and set a new record for market share in China.

Ford Motor Company (NYSE:F) also reported that Ford Credit’s results were solid. The segment’s pre-tax profits were $499 million, while its net income was $312 million. The automaker cited higher volume as the main driver in pretax profits.

Factors affecting Ford’s earnings

Ford Motor Company (NYSE:F) reported that a number of factors had a negative impact on its earnings results. The company noted increases in warranty reserves for field service actions for previous models. That includes safety recalls and other project campaigns. Ford also noted costs related to the severe weather and, in South America, effects from currency exchange.

The automaker estimates that these impacts trimmed its pretax profits for the first quarter by approximately $900 million or 17 cents per share. Those impacts also accounted for the $700 million decline year over year in pretax profits. Ford Motor Company (NYSE:F) noted that while it’s possible for these factors to occur again, it’s “unusual for items like these to occur in this magnitude in the same quarter.”

Ford looks ahead

The automaker affirmed its previous guidance for pretax profits of between $7 billion and $8 billion for the full year. Ford Motor Company (NYSE:F) said it plans to launch 23 new vehicles globally, which is the most in one year in its history. The company expects automotive revenue to be flat with last year and operating margins for the segment to be lower. Ford projects automotive operating-related cash flow to still be in the positive although “substantially lower” than last year.

“We had a solid quarter, and we are on track with our most aggressive product launch schedule in our history,” said Alan Mulally, president and CEO. “Our One Ford plan continues to deliver as we serve customers in more markets around the world with a full family of vehicles committed to best-in-class quality, fuel efficiency, safety, smart design and value.”

During the first quarter, Ford Motor Company (NUSE:F) increased its quarterly dividend 25%, paying out approximately $500 million in dividends.