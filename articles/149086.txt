RESOLVE: The National Infertility Association has created a state-by-state "report card" for infertility.

The researchers graded the states by "fertility friendliness," a new release reported via PR newswire.

"For the second year in a row, we are working to highlight state-by-state disparities between access to support resources and fertility treatment, in an effort to motivate people to take action to improve their state's fertility friendliness," Barbara Collura, President/CEO of RESOLVE, said in the news release. "We developed the scorecard not to publicly call out specific states for their lack of access, but to bring attention to what still needs to be done in terms of improving access to care and support in every state."

The states that got the highest scores (an A grade) were: "Connecticut, Illinois, Maryland, Massachusetts and New Jersey," the news release reported. "Alaska, New Hampshire and Wyoming," received an F in fertility friendliness.

"Even for the states that received top grades, we need to be vigilant to ensure access remains strong, and we need to remember that insurance mandates don't cover everyone," Collura said. "This year, we have also highlighted states where the insurance mandate to cover fertility treatment may be in jeopardy as a result of the Affordable Care Act, or where the state legislatures may have a history of trying to pass laws that negatively impact the infertility community."

The researchers hope the research will help people start families in the future.

"We hope that by providing The Fertility Scorecard as a resource for people suffering with the disease of infertility we will help them take control of their fertility journey by becoming more educated about how to address both the financial and emotional barriers they encounter," Collura said. "We want to create conversations between patients, HCPs, insurance companies, pharmaceutical companies, policymakers and employers that will increase access to fertility treatment nationwide."

The association urges those with fertility problems to seek medical help and should not feel alone in their situation.

To see the full report click here.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.