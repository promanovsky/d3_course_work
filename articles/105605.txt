Chevrolet took the wraps off the all- new 2015 Corvette Stingray Z06 convertible ahead of its official debut at the New York auto show next week.

Chevrolet, one of the world's largest car brands, unveiled the convertible version of the Corvette Stingray Z06, one week ahead of its public debut at the New York auto show. The car-maker released series of photos and specifications of its new convertible supercar, with design and performance matching the hard-top Corvette Stingray Z06, which was debuted at the Detroit auto show earlier this year. The Corvette Z06 Convertible's under-the-hood features make it Chevy's most powerful open-top model till date.

The Corvette Z06 Convertible draws its power from 6.2-liter supercharged V8 engine projected to produce at least 625 horsepower and 635 pound-feet of torque, the company said in a press release. The Corvette Z06 Coupe is also equipped with the same V8 engine. Chevy said the Z06 Coupe's aluminum built was so stiff that it needed no additional structural reinforcements to convert the hard-top into a convertible, as they both share the same chassis tuning, powertrain output, driver technologies and equipment options. Both Corvette Z06 models weigh just about the same.

"The most impressive aspect of the Z06 Convertible may be its performance bandwidth," Corvette chief engineer Tadge Juechter said in a press release, Friday. "Very few cars on the market can match its combination of extreme, supercar levels of performance; the flexibility for daily driving and long-distance commuting; and the 360-degree, open-air driving experience only true convertibles can offer."

Juechter also noted that the final design came after 17,000 digital iterations and that the technologies used in the Corvette Z06's frame did not exist five years ago. The most notable tweak to the Z06 Convertible variant is its rear mounted transmission cooling intakes that are moved underneath the car, to make enough space of the tonneau cover to rest. The fabric top in the Corvette Z06 can be operated at speeds up to 30mph.

The Corvette Z06 will come in two types of engine and transmissions, a 6.2 liter V8 option and a seven-speed manual transmission or eight-speed automatic with paddle shifters. Both Chevrolet models are equipped with Z07 Performance Package that adds Brembo carbon-ceramic matrix brakes, Michelin Pilot Sport Cup tires and adjustable front- and rear-aerodynamic components, but it is unclear if the convertible will come with Performance Data Recorder, an integrated onboard video, audio and data recorder, which is an optional with the Coupe variant.

Standard safety features include front and side airbags, rearview camera, OnStar and daytime running lights and stability and traction control. The Performance Traction Management is available as an option for those who require safety and performance on the track.

Chevrolet did not reveal the pricing and the availability of the new Corvette Z06 Convertible, but it is likely to go on sale early next year, after the hard-top Z06 variant hits the market.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.