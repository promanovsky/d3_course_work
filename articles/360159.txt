AP Google have unveiled the first smartwatches to go om sale using Android Wear

FREE for the biggest new releases, reviews and tech hacks SUBSCRIBE Invalid email Sign up forfor the biggest new releases, reviews and tech hacks When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The California based firm said the LG G Watch and the Samsung Gear Live are available to pre-order now.



Both watches feature rectangular screens and will cost £145 and £119 respectively and are due to be released on July 7.



The gadgets use the Android Wear operating system which has been tailored for smartwatches and other wearable devices.



Motoroloa's Moto 360, which features a circular face and is also powered by the Android OS, will be released "later this summer".

AP The LG G Watch will cost £145 and is available to pre-order now

We're right at the beginning of a new phase in the miniaturisation of technology David Singleton - Google's director of engineering

Google unveiled the trio of smartwatches at their I/O developer conference presentation in San Francisco tonight.



The tech giant also gave a glimpse of the new features on the next version of their Android operating system which is widely used on tablets and smartphones.



The OS links up to other gadgets so users could check an e-mail on their smartwatch, answer the message on their phone and then delete it on a PC.



Speaking at the event David Singleton, Google's director of engineering, said: "We're right at the beginning of a new phase in the miniaturisation of technology."



Mr Singleton added that one of their core aims was to "quickly show you relevant information, and make sure you never miss an important message, while letting you stay engaged with the people that you are actually with".



When notifications are received by a user's phone they can set it so their smartwatch also vibrates.

AP The Samsung Gear Live will go on sale on July 7