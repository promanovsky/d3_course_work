The second generation of post-Carson latenight hosts plays better together than the first.

Carson’s two immediate heirs, Jay Leno and David Letterman, were never that close. And who can blame them? NBC’s decision to hand over its latenight mainstay, “The Tonight Show,” to Leno over Letterman made a close friendship almost impossible. Now their successors are exhibiting warmer feelings for each other.

Jimmy Fallon, Jon Stewart, and, perhaps surprisingly, Craig Ferguson, are among the latenight hosts wishing Stephen Colbert well on the heels of CBS’ announcement Thursday that Colbert would take the reins of its “Late Show” when David Letterman retires in 2015.

Each has a reason to be snarky. Colbert arguably got his start on Comedy Central’s “Daily Show,” which led to a berth on “Colbert Report.” Fallon, already grappling with Jimmy Kimmel at ABC, will be a direct competitor of Colbert’s new version of “The Late Show” when it launches next year. And Ferguson, who currently follows Letterman at 12:37 a.m. each morning on CBS, may have wanted to succeed him in the earlier spot.

Yet none of them offered a discouraging word in remarks delivered during tapings of their own programs Thursday evening.

Stewart seemed genuinely thrilled for his timeslot neighbor. “Truly one of great pleasures of doing this show has been trying to maintain professional composure while Mr. Colbert is making me laugh uncontrollably,” said Jon Stewart during tonight’s broadcast of “The Daily Show.”

“The exciting news today is I no longer need a cable subscription for the privilege of watching Stephen Colbert. Our good friend Stephen Colbert will be heading to CBS to take over ‘The Late Show’ from, for at least me, the comedian broadcaster who is the best there ever was, David Letterman. I think the best there ever was. And I think — here is the exciting thing for me. I think Stephen Colbert is up for the challenge,” Stewart said. “While we wish Dave the absolute best for a well-earned retirement, there’s no greater joy for seeing a genuinely good man who works as hard as he can every day and deserves all the success in the world actually get that success. For Stephen we’re just thrilled.”

Fallon addressed the new competition in the first few moments of this evening’s monologue. “Of course, I want to say congratulations to our pal Stephen Colbert, who will be taking over for David Letterman next year. We’re very happy for that guy. He’s very funny. He’s a good friend,” Fallon said, according to remarks provided by NBC. “Now, a lot of people in the media are already talking about there being a new late night war – and I just want to say there’s not going to be any war. It’ll be a dance off. A late night dance off! Get ready, Stephen.”

Interestingly, Colbert made a cameo on Fallon’s debut “Tonight” broadcast just a few weeks ago. During his time on screen, Colbert poured a bucket of coins all over Fallon, and then shrieked, “Welcome to 11:30, bitch!” before posing with the NBC host for a “selfie.”

Even Ferguson had nothing but kind words. “Congratulations Stephen! A fine addition to the CBS cavalcade of stars,” he said during his monologue, according to a transcript provided by CBS.