Rob Kardashian going through a tough spell. — Cover Media pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LOS ANGELES, June 27 — Rob Kardashian reportedly has “a very bad drug problem.”

The younger brother of Kim, Kourtney and Khloé Kardashian has been having a tough time lately. He has gained a lot of weight and skipped Kim's wedding to Kanye West in Italy last month. Now it's been alleged that drug abuse has also been added to the equation.

“[Rob's] family wants Rob to get help for what they say is a very bad drug problem,” a source directly linked to the family told TMZ.

The outlet adds that the family, headed up by momager Kris Jenner, are “well aware” of what is going on with the 27-year-old after photos were released of Rob at an allegedly drug-fuelled party a few days ago. The sock designer is seen smoking what is alleged to be weed, but TMZ believes the photos show more concerning issues.

“You can see Rob holding a double cup... the container of choice for Sizzurp. The cup on the table actually has the words, 'Codeine Boys' emblazoned on it,” the website writes.

The cough syrup apparently became a favourite of stars including Justin Bieber and Soulja Boy and is considered top of the line by people in the know. It can't be bought over the counter and the street value runs around US$800 (RM2,566) a pint. Following the glorification of the product, manufacturer Actavis made the decision to cease all production of promethazine codeine.

TMZ added that the Kardashians have been calling rehab facilities, but Rob reportedly refuses to go. — Cover Media