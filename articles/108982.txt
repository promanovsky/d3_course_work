It hasn't been long since the final season of Mad Men was announced, and now the show is set to bid its fans goodbye. AMC has confirmed that season 7 of Mad Men will air at 10 p.m. on Sunday, April 13. Until this date, followers of this television series will predict how the new season will continue the story. The series is said to have a powerful ending.

Earlier released spoilers hinted at four new cast members who play interesting new characters, namely Dennis Ford, Ferg Donnelly, Will Scully and Diana. It was said that two of these new characters will be Don's foes. Diana is reportedly a decent housewife and mother. Not much was divulged on the other new characters.

The season premiere reveals that Peggy, played by Elisabeth Moss, went on to get a high position at the advertising agency in exchange for giving up something in her personal life. It will also show how Joan was able to survive in the '60s in a business that is mostly dominated by males.

Matthew Weiner, the creator of Mad Men, said during an interview with AMC that he already knew how the final season would end but does not want to disclose the conclusion of the final episode. Weiner also admitted that he considered backing out from writing the episodes, but he was always persuaded to continue on and finish the season.

The season premiere episode will give us a hint as to what to expect in season 7. Weiner revealed that Season 7 of Mad Men will run for two years, which will have seven episodes this year and another seven episodes in 2015. Both series are scheduled for a spring showing.