Researchers conducting an ongoing study on the prevalence of tick-borne illnesses including Lyme disease in New Brunswick say there is a growing risk for both dogs and humans.

A team at Mount Allison University led by biologist Vett Lloyd says preliminary findings indicate the habitat for ticks carrying the bacteria that causes Lyme disease is expanding northward as the climate moderates.

The researchers began testing for Lyme diseases in dogs through blood tests in 2013 as a starting point for monitoring infection risks to New Brunswick communities.

Story continues below advertisement

They say previous studies have found that for every six infected dogs, there is one human that is also infected.

The lab collected about 300 blood samples from dogs in seven health districts across the province in the fall 2013 and spring 2014 tick seasons.

Results showed infection rates had risen to 7 per cent in the fall of 2013, when three years ago, fewer than 1 per cent of New Brunswick dogs were infected.

The highest rates of infection were in the Moncton area at 17 per cent, followed by the Saint John region with a rate of 10 per cent.

The study also found Lyme seropositive dogs as far north as the Edmundston area.

Lloyd's research team will continue to examine ticks this summer and says it plans to test up to 400 dogs at various veterinary clinics.