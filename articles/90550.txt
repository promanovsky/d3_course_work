The U.S. government on Monday said that final payments to private health insurers for 2015 private Medicare Advantage plans would rise 0.4 percent compared with a proposed cut it said would have been about 1.9 percent.

The final rate announcement comes after months of pressure from the industry, Congress and seniors who urged the government to keep in place benefits for the elderly and disabled through these plans offered by companies like UnitedHealth Group Inc. and Humana Inc.

The government had proposed payments on Feb. 21 that insurers complained would result in a cut of anywhere from 4 percent to about 7 percent.

On Monday, the Centers for Medicare and Medicaid Services division of the Department of Health and Human Services said that it had adjusted several of the factors that had made up the initial cut, resulting in the end result that payments on average would rise.

The proposed payment rates are a key factor in how insurance companies plan their business for the coming year, including in which markets they will offer health plans, what their medical and administrative costs will be, and at what level to set premiums and doctor visit co-payments.

The health agency said on Monday that costs for Medicare health services have continued to drop and that it now expected a decline in spending per member of 3.4 percent versus the 1.9 percent on which it initially based its 2015 payments.

In addition, the agency said that it would delay other changes to risk adjustment calculations and for requiring home health assessments, which will effectively lessen the cuts to insurers.