General Motors Co. said on Monday that its heads of human resources and communications are leaving "to pursue other interests."



John Quattrone, 61, will succeed Melissa Howell, 47, as senior vice president, global human resources.



GM did not immediately name a replacement for Selim Bingol, 53, senior vice president, global communications and public policy.



The automaker said the moves were not related to its ongoing safety crisis over faulty ignition switches.



"This is not related to the recall," GM spokesman Greg Martin said. "Companies go through changes like this during periods of transition."



Quattrone's appointment as human resources chief is effective immediately, as are the departures of Howell and Bingol, GM said.



Quattrone was most recently executive director of human resources for GM's global product development, purchasing and supply chain. He has held human resources and labor relations positions at GM since joining the company in 1975.



"John brings to the job a deep and rich breadth of experience across all levels of the enterprise," GM Chief Executive Mary Barra said in the statement announcing the moves.



"This background is invaluable as we create lasting change that puts the customer at the center of how we work and how we measure ourselves going forward."



Barra and GM have been under fire since the company admitted to failing to properly handle defective ignition switches in some of its small cars, an issue that has led to the deaths of at least 13 people.



When Barra testified before Congress earlier this month, she said that the culture of the "old GM" was different from the "new GM," which places safety and customer needs ahead of cost considerations.



Some lawmakers have challenged her contention that GM has changed since emerging from its U.S. government-directed bankruptcy in 2009.



Howell joined GM in 1990 and was named senior vice president, global human resources, in February 2013.



"Through Melissa's passion, the values that make up today's GM are now becoming a central part of how we develop and guide our employees around the world," Barra said in the statement.



Bingol joined GM in 2010 as head of communications when Ed Whitacre was the company's CEO.



In October 2012, Bingol added "head of global public policy" to his title.



During Bingol's tenure, GM returned to publicly traded markets with an initial public offering in November 2010. He remained in his position throughout the tenure of Whitacre's successor and Barra's predecessor, Dan Akerson.



© 2021 Thomson/Reuters. All rights reserved.