Update: 8GB iPhone 5c Goes Up For Sale In Europe & Asia, As Apple Also Tweaks Global iPad Line-Up

As per the rumours we reported on yesterday, Apple has now added an 8GB version of the iPhone 5c to European stores (via 9to5mac).

On the U.K. Apple store the 8GB device is priced at £429 unlocked, shaving £40 off the cost of entering into the iOS world. The 8GB iPhone 5c is also for sale on U.K. carrier O2’s website.

Update: The 8GB iPhone is also now available on the French Apple store, where it will set buyers back €559, which is €50 cheaper than the 16GB version.

Update 2: The 8GB iPhone 5c is also now available in China, where it costs RMB 4,088 from Apple’s store (RMB 400 cheaper than the 16GB version); and in Australia, where it costs A$679 (A$60 cheaper than the 16GB version).

The 5c, which on the Apple store is marketed under the tagline “for the colorful” — a reference to its candy-coloured plastic casing — has apparently not sold as well as Apple hoped. Hence the capacity and price reduction.

But at £429 the ‘budget iPhone’ still carries a substantial premium vs low end Android devices. Motorola’s well-reviewed 8GB Moto G can currently be picked up for £129 on Amazon, in just one example of available budget smartphones running the Android OS.

At the time of writing the 8GB version of the iPhone 5c is not available on the US Apple store, with only the 16GB and 32GB options currently being offered:

Update: Apple has confirmed the locations where the 8GB iPhone 5c is going on sale in this first wave of releases — and therefore, by implication, where it’s not (yet): namely the U.S.

“The 8GB iPhone 5c model will be available in the U.K., France, Germany, Australia and China on March 18,” the company said in a statement provided to TechCrunch.

Goodbye then, iPad 2…

Also today, Apple has tweaked its iPad line up, discontinuing the veteran iPad 2 tablet and replacing it with a reintroduced 4th-gen iPad with Retina display which is only available with 16GB capacity. The reintroduced 4th-gen iPad with Retina is priced at $399 (Wi-Fi only) or $529 (Wi-Fi and cellular).

The move creates a clearer weight distinction between the top-of-the-range iPad Air and the (now only) other full sized iPad in Apple’s tablet line up, with more than 0.40 lb in weight difference between the 4th-gen iPad and the Air, and 0.08 inch difference in thickness.

All iPads in the current line-up now pack Apple’s Lightning connector.