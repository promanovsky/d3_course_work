TORONTO - A new study suggests as few as five mutations can give an H5N1 bird flu virus the capacity to transmit among mammals instead of birds.

The just published study builds on some highly controversial research done to try to determine what changes would be needed to make these bird flu viruses able to infect people and spread among them like seasonal flu viruses.

The earlier work was blocked from publication for a number of months while a U.S. government advisory committee argued that putting papers outlining needed changes into the public domain was a biosecurity risk.

Those earlier studies, which were eventually published in the spring of 2012, found that more changes were needed.

But this work, by Dutch scientists, shows that with only five mutations an H5N1 virus from Indonesia can transmit from infected ferrets to nearby healthy animals.

Senior author Ron Fouchier says this minimum set of mutations is one way the virus could achieve airborne transmission but is not the only way that transformation could come about.