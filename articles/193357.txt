Nintendo has announced that it will be creating new consoles for emerging markets in a bid to turn around its lacklustre sales.

CEO Satoru Iwata has so far avoided expanding the gaming giant’s console business model to platforms such as smartphones and has said that devices aimed at countries like China will avoid the boom-and-bust problems of mobile gaming.

“We want to make new things, with new thinking rather than a cheaper version of what we currently have,” Mr Iwata said in Tokyo. “The product and price balance must be made from scratch.”

“The smartphone market is probably more competitive than the console business. We have had a console business for 30 years, and I don’t think we can just transfer that over onto a smartphone model.”

The company has previously experimented with lower-priced consoles such as the 2DS and the Wii Mini and have a whole stable of popular titles and characters to move onto any new platforms.

Nintendo's 2DS cut costs on the 3DS console by removing the hinge.

Read more:

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

In their most recent earning report Nintendo reported a net loss of 33.4 billion yen (£193 million), greater than the 27.9 billion loss that had been predicted by analysts.

The company has blamed poor sales of their latest Wii U console for its losses, with the home console and successor to the wildly-popular Wii selling just 2.72 million units in the fiscal year ending March.

Rival devices from Microsoft and Sony, the Xbox One and PS4, sold more than 7 million and 5 million units respectively despite only launching in November last year.

“We were unable to recover the momentum sufficiently even in the last year-end sales season,” said Mr Satoru regarding the Wii U. “We would like to spend one full year to leverage Nintendo’s own strength in order to regain momentum.”