Producers of The Bachelorette were faced with a unique problem when one of the season 10 contestants, Eric Hill, died in a paragliding accident shortly after leaving the show. Forced to make a call about how the new episodes should be edited or altered, they decided, ultimately, to let his scenes air as evidence of his "passion" and "courageous spirit" — and his sister, for one, is glad they did.

In a first-person account for Today.com, Hill's sibling Karen Tracy said she sees The Bachelorette as a "unique opportunity to watch him live on, and watch the world get to know him" every week. "In some ways, it's only fitting that he is defying death in this way," she explained. "My brother always thought bigger and dreamed bigger than anyone I have ever known."

Indeed, at the time of his death at age 31, Hill was working on a quest to visit every U.N.-recognized country in the world. Because of that project — the Global Odyssey, as he called it — Tracy was accustomed to admiring her brother from afar, via clips and pictures he posted online.

"People ask if it's difficult to watch him on the show, and I do get a little nervous until I see the handsome brother that I know and love," she said of Hill, who was sent home by Andi Dorfman after a contentious final conversation during the June 2 episode. "My family — my parents and my four other brothers — have all grown used to watching Eric onscreen. He always posted videos of his travels on his site and shared them with us on visits."

It's different, of course, now that he's gone, but Tracy is grateful to be able to watch one of her brother's final adventures on TV. "I get a little tearful," she admitted, "but I know Eric would want his audience to enjoy the journey with him, and seeing him makes me smile."

Hill lives on in other ways, too. "A day after his passing, my family went to the site where Eric fell from the sky. We watched the paragliders soaring overhead and imagined Eric enjoying his last flight," she told Today. "We remembered a man who had lived his life to the absolute fullest. The world was just not big enough for him. In the weeks since, we have set up a philanthropic foundation in his honor, LiveLikeEric.com. And I'll be watching him in his alternate reality Monday night."

Read Karen Tracy's full tribute to her brother at Today.com.