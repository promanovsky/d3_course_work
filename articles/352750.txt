PORT-AU-PRINCE, Haiti -- Within a dense cluster of flimsy shacks made mostly of plastic tarp and wooden planks, a young mother cradles her sick, whimpering toddler while trying to guard against a fierce tropical sun.

Delimene Saint Lise says she's doing her best to comfort her 2-year-old daughter and control her spiking fever during what has quickly become a familiar agony in their makeshift community of shanties by a trash-clogged canal in the Haitian capital.

"For the last three days, her body gets very hot and she's hurting all over," Saint Lise said as she sat on a mattress inside their sweltering home with flapping plastic walls in the capital's dusty Delmas section. "I know because I had this awful illness before her."

This latest scourge in Haiti is chikungunya. It's a rarely fatal but intensely painful mosquito-borne virus that has spread rapidly through the Caribbean and parts of Latin America after local transmission first started in tiny French St. Martin late last year, likely brought in by an infected air traveller.

Haiti is proving to be particularly vulnerable because so many people live like Saint Lise and her neighbours, packed together in rickety housing with dismal sanitation and surrounded by ideal breeding grounds for the mosquitoes that carry the illness.

"Chikungunya has been merciless in Haiti. Lack of basic infrastructure, poor mosquito control measures, and deep social and economic disparities hampered prevention and treatment efforts," says a new report on Haiti's epidemic by the Igarape Institute, a Brazil-based think-tank .

Since the virus was first documented in Haiti in May, there have been nearly 40,000 suspected cases seen by health workers, the Pan American Health Organization says. The only places with higher numbers are the neighbouring Dominican Republic and Guadaloupe.

But there are many signs that the actual number is far higher in Haiti, a country of 10 million people that struggles with many burdens, from crushing poverty, lack of access to clean water and the fact that some 146,000 people displaced by the January 2010 earthquake still live in makeshift homes.

The U.S. Centers for Disease Control is now assisting Haiti's health ministry to confirm new cases. But statistics are notoriously unreliable in Haiti, and public health experts say the number of people with the illness is unknown. Many poor Haitians don't bother seeking care at clinics so their cases go unrecorded, said Dr. Gregory Jerome of Zanmi Lasante, the Haitian program of the Boston-based non-profit organization Partners in Health.

It's clear the "attack rate of this infection is very high all over the country," Jerome said. And it's not just impoverished districts. People in wealthier areas such as the tree-lined Port-au-Prince suburb of Petionville and the scenic southern coastal town of Jacmel are complaining of cases.

Instances of local transmission have been reported in about 20 nations or territories in the region, from the Virgin Islands, Dominica, Martinique and Puerto Rico to El Salvador in Central America and French Guiana, Guyana and Suriname on the northern shoulder of South America.

In Haiti, it's gotten so bad so quickly that many people are resigned to catching the virus known in Creole as "kaze le zo," or "breaking your bones," for joint pain so intense some patients can barely walk or use their fingers for days. There is no vaccine and the only treatment is basic medication for the pain and fluid replacement for dehydration.

Painful symptoms of chikungunya generally dissipate within a week and people develop immunity after getting infected. But some patients can develop severe and even life-threatening complications including respiratory failure. It can also contribute to the deaths of people with underlying health issues. Just last week, former Haitian President Leslie Manigat died after a long period of illness and a deputy secretary of his political party said his condition might have been complicated by a recent bout with the virus.

Outbreaks of chikungunya have long made people miserable in Africa and Asia. In the Western Hemisphere, where the illness is new and advancing rapidly, health officials are working to educate the public and knock down the mosquito population. U.S. states are warily monitoring "imported" cases among residents who recently travelled to the Caribbean and were bitten by an infected mosquito.

Haiti's government has stepped up fumigation and education campaigns. Public service announcements about the illness appear regularly on radio and TV stations broadcasting World Cup games. Officials also recently distributed free pain-relief medicine at public health facilities, especially because there were signs of predatory price increases by pharmacies and freelance pill vendors.

But Dr. Gretta Lataillade Roy, head of a small public clinic in Delmas who was ill with the virus last month, said the free pain-relief medication ran out at her facility within 48 hours as a surge of patients showed up. Most people with the virus are now opting to suffer at home, she said, and high prices for acetaminophen are back.

Many health clinic workers have been falling ill with chikungunya after being bitten by infected mosquitoes, resulting in temporary staff shortages.

Although other mosquito-borne illnesses such as malaria and dengue exist in Haiti, simple precautions to deter bites are often not taken because many people can't afford bug repellant and window screens. The Igarape group's recent survey of 2,807 randomly sampled households suggested that treated mosquito nets, another recommended precaution, are seldom used.

Complicating matters, nearly one in five respondents told researchers they believe rumours that chikungunya was intentionally brought to Haiti by businessmen as a way to make money or possibly as a form of social and political control.

"There are some difficulties in many parts of the population to accept the reality that the virus is transmitted by a vector," said Dr. Jean-Luc Poncelete, the Pan American Health Organization's representative in Haiti.

In Delmas, Saint Lise and her neighbours say they've heard on the radio that mosquitoes spread the strange virus. But they say they are skeptical in part because the government has not been fumigating around their camp.

"They say it's the mosquitoes doing this," said Eliamese Derisier, whose 6-month-old son is struggling with the virus in a tent next door to Saint Lise's family. "But I'm not sure if I believe them."