Health care workers diverting patients' pain medications for their own use have been linked to six infectious disease outbreaks over the last decade, putting thousands of patients at risk, according to a new report.

The report published June 2 in Mayo Clinic Proceedings outlines infection outbreaks that have occurred in the past 10 years as a result of drug diversion -- the use of prescription drugs for unintended, illegal purposes -- committed by health care providers who stole or tampered with their patients' injectable medications.

The report identified six infection outbreaks that occurred in hospital settings in eight states beginning in 2004. Three technicians and three nurses, including a nurse anesthetist, who were tampering with injectable medications were implicated in spreading the infections.

Nearly 30,000 patients were potentially exposed to blood-borne or bacterial pathogens during the drug-diversion-related outbreaks mentioned in the report, which also identified 100 confirmed individual infections.

In four of the outbreaks, health care workers who diverted drugs were infected with the hepatitis C virus and transmitted the virus to patients. In two other outbreaks, drug-diverting health care workers passed bacterial infections to patients.

"The outbreaks we have identified illustrate some of the devastating and wide-reaching impacts of drug diversion in U.S. health care settings," study authors Dr. Melissa K. Schaefer and Joseph F. Perz, of the Centers for Disease Control and Prevention, said in a statement.

So, is there anything that hospitals can do to better deal with the issue of drug diversion?

"Health care facilities should ensure that patients safely receive medications as prescribed," Schaefer and Perz wrote in the report. "This effort includes having systems in place to prevent drug diversion as well as developing protocols for early detection and appropriate response if, despite safeguards, diversion does occur."

Hospitals should enforce appropriate drug security measures and maintain active monitoring systems, the study authors wrote, and they should also report drug diversion cases to enforcement agencies.

"This report serves notice that all health care facilities that house controlled substances or other drugs of abuse must have effective systems in place that deter drug diversion," Dr. Keith H. Berge and Dr. William L. Lanier of the Department of Anesthesiology at the Mayo Clinic wrote in an accompanying editorial. "Further, those facilities must be able to quickly and effectively investigate when a drug diversion is suspected in an effort to limit the number of patients potentially exposed to harm. The increasing incidence of drug diversion makes it mandatory that drug control be improved."