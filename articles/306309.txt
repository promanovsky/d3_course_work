Gold boosted by Federal Reserve rate outlook

Investing.com - Gold prices were higher on Thursday, after Federal Reserve Chair Janet Yellen signaled the central bank was in no hurry to raise interest rates.

On the Comex division of the New York Mercantile Exchange, for August delivery rose to a session high of $1,282.30 a troy ounce, the most since June 16, before trimming gains to last trade at $1,280.80 during European morning hours, up 0.64%, or $8.20.

Prices were likely to find support at $1,258.00, the low from June 17 and resistance at $1,285.10, the high from June 16.

Also on the Comex, for July delivery tacked on 0.83%, or 16.5 cents, to trade at $19.94 a troy ounce, the highest since May 14.

At the conclusion of its two-day meeting on Wednesday, the Fed cut its bond purchases by another $10 billion a month, to $35 billion, saying there was "sufficient underlying strength" in the U.S. economy to continue tapering.

The Fed said it expects the federal-funds rate, currently close to zero, to reach 1.2% by the end of next year and 2.5% by the end of 2016, a slightly faster rate of tightening than formerly expected.

But the forecast did not bring forward the timing for the first rate hike, disappointing many investors and weighing on the dollar.

The , which tracks the performance of the greenback against a basket of six other major currencies, declined 0.3% to 80.26, the lowest since May 27.

Dollar weakness usually benefits gold, as it boosts the metal's appeal as an alternative asset and makes dollar-priced commodities cheaper for holders of other currencies.

The U.S. is to publish the weekly report on initial jobless claims as well as a report on manufacturing activity in the Philadelphia region later in the day.

Elsewhere in metals trading, for July delivery tacked on 0.08%, or 0.3 cents, to trade at $3.063 a pound.