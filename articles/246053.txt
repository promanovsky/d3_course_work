The days when there was just one version of Windows are long gone. With a phone and RT version of

Windows 8.1

out already, Microsoft is adding yet another variant made specifically for hardware manufacturers.

First teased at Build 2014, Microsoft has announced Windows 8.1 with Bing designed for low-cost hardware platforms. In a blog post, the Richmond company detailed that these affordable devices could be equipped with as little as 1GB of memory and 16GB of storage.

Microsoft is also offering this new version of Windows for free to all hardware developers for tablets with screens smaller than 9-inches.

While regular consumers might not be able to take advantage of this free version of Windows, we could see many more Windows devices in the near future at lower prices. Microsoft even hinted in its announcement that we're in for more Windows devices in the weeks leading up to Computex in Taipei.

I love free stuff

Although the vanilla version of Windows 8.1 comes with Bing baked in, this new variant will come with Internet Explorer set as the default browser, therefore using Microsoft's search engine from the get-go. Users will likely be able to change IE's default search engine, but there's always the option to install another browser like Chrome or Firefox.

As an extra perk devices, especially tablets, also may come with Office pre-loaded or an included one-year subscription to Office 365.

Otherwise, Windows 8.1 with Bing seems to be largely the same operating system and Microsoft promises it will run just as fluidly on the lightly specced platforms.

It may be even more interesting to see how many new phones and tablets decide to take advantage of this free Windows offer. Stay tuned as we bring you more Windows-powered hardware news from Computex in the coming weeks.