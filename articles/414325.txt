Suppliers to Apple Inc are scrambling to get enough screens ready for the new iPhone 6 smartphone as the need to redesign a key component disrupted panel production ahead of next month's expected launch, supply chain sources said.

It's unclear whether the hiccup could delay the launch or limit the number of phones initially available to consumers, the sources said, as Apple readies larger-screen iPhones for the year-end shopping season amid market share loss to cheaper rivals.

But the issue highlights the risks and challenges that suppliers face to meet Apple's tough specifications, and comes on the heels of a separate screen technology problem, since resolved, in making thinner screens for the larger iPhone 6 model.

Cupertino, California-based Apple has scheduled a media event for Sept. 9, and many expect it to unveil the new iPhone 6 with both 4.7 inch (11.94 cm) and 5.5 inch (13.97 cm) screens - bigger than the 4-inch screen on the iPhone 5s and 5c.

Two supply chain sources said display panel production suffered a setback after the backlight that helps illuminate the screen had to be revised, putting screen assembly on hold for part of June and July. One said Apple, aiming for the thinnest phone possible, initially wanted to cut back to a single layer of backlight film, instead of the standard two layers, for the 4.7-inch screen, which went into mass production ahead of the 5.5-inch version.

But the new configuration was not bright enough and the backlight was sent back to the drawing board to fit in the extra layer, costing precious time and temporarily idling some screen assembly operations, the source said.

Output is now back on track and suppliers are working flat-out to make up for lost time, the supply chain sources added.

Japan Display Inc, Sharp Corp and South Korea's LG Display Co Ltd have been selected to make the iPhone 6 screens, the sources said.

Representatives for those three suppliers, and for Apple, declined to comment.

WIDER IMPACT

Apple is known to make tough demands on its parts suppliers for new iPhones and iPads as it competes to create designs, shapes, sizes and features to set it apart and command a premium price in a fiercely competitive gadget market.

This can cause glitches and delays, including screen problems that crimped supplies at last year's launch of a high-resolution version of Apple's iPad Mini.

It also highlights the danger for suppliers of depending too heavily on Apple for revenues, creating earnings volatility.

Earlier this month, Japan Display, said to be the lead supplier for the new iPhone panel, said orders for "a large customer" - which analysts said was Apple - arrived as expected, but shipments may be delayed in the July-September quarter.

Japan Display's reliance on Apple's cyclical business has spooked some investors. UBS Securities has forecast that Apple will contribute more than a third of the Japanese firm's total revenue in the year to March 2015. Japan Display's share price dropped to a 12-week low of 501 yen after first-quarter earnings on Aug. 7 lagged market expectations.

In Taiwan, home to several Apple suppliers and assemblers, export orders grew less than expected in July, even as factories rushed output ahead of new smartphone launches, reflecting the erratic nature of the business.

"Currently, there's a small shortage in supply of a specialised component for our communication devices," said a spokesman for Pegatron, which assembles iPhones. "This kind of problem regularly occurs and the impact on production is negligible."

Supply chain sources had previously said challenges with the new iPhone's screen in-cell technology, which eliminates one of the layers in the LCD screen to make it thinner, caused a delay in the production of the larger 5.5-inch version. One display industry source said the in-cell issues had now been resolved.

The pressure on Apple for stand-out products has increased as Samsung Electronics Co and, more recently, a clutch of aggressive, lower-cost Chinese producers such as Xiaomi Inc and Lenovo Group Ltd have eroded the U.S. company's market dominance.

The iPhone 6 unveiling has been widely anticipated to bolster momentum for Apple shares, which have risen by a third, to above $100 each, since the company posted strong first-quarter earnings in late-April.