Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Oscar Pistorius returned to court today as his trial for the murder of Reeva Steenkamp resumed following a month-long break.

Proceedings were paused in May when the judge agreed the athlete, 27, should be assessed by a panel psychiatrists.

FOR A RECAP OF LIVE UPDATES FROM MONDAY'S EVIDENCE - CLICK HERE.

A defence witness had told the court Pistorius suffers from Generalised Anxiety Disorder.

The judge decided she wanted a panel of experts to determine whether this would have affected his ability to determine right from wrong at the time of the shooting.

Here's what happened when the runner returned to court:

1. Pistorius was NOT mentally ill when he shot Reeva

After 30 days of assessment, the panel of experts concluded that Pistorius was not mentally ill at the time of the shooting.

While suffering from an anxiety disorder, the athlete WAS capable of knowing right from wrong.

Prosecutor Gerrie Nel read from the report: "At the time of the alleged offences, the accused did not suffer from a mental disorder or mental defect that affected his ability to distinguish between the rightful or wrongful nature of his deeds."

While quibbling over some of the details, the prosecution and defence accepted the overall conclusions of the report.

2. Pistorius is 'vulnerable to danger'

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

The first defence witness to be called was Dr Gerald Versfeld, the physician who has known Pistorius since he was a baby and amputated his legs.

Speaking in a slow and deliberate tone, he recalled the details of the athlete's 'congenital deformities' as a child and the difficult decision he had to make to remove his limbs.

He went into great detail about the discomfort Pistorius suffers while using his prosthetics, and the instability which he endures.

At one point he even asked the judge to come down from her seat to examine Pistorius and his stumps up close.

But his telling contribution came towards the end of his evidence when he concluded that Pistorius is 'vulnerable' and that he would have trouble turning and running from danger, unless he had a weapon.

3. Is Dr Versfeld 'objective'?

This comment was immediately seized upon by prosecutor Gerrie Nel.

Why, as an 'objective' witness, did Dr Versfeld feel the need to talk about Pistorius being able to flee from danger?

Nel established that Dr Versfeld was only called as a witnessed after having heard Pistorius' evidence.

He accused of making conclusions to fit the defence case.

Dr Versfeld denied this but admitted his report was compiled with the court case in mind.

4. Was it 'pitch black' when Oscar shot Reeva?

In a damaging exchange, the defence witness Dr Versfeld also appeared to state that Pistorius' version of events could not be true.

When asked about how well he could turn and flee from the bathroom into the bedroom in 'pitch black' conditions, the doctor conceded this was 'improbable'.

Nel asked if it is more likely if a light, for example the toilet or bathroom light, was on.

The doctor agreed it would be.

Pistorius insists that there was no light on in the toilet and therefore he did not realise Reeva was in the bathroom.

The defence attempted to repair the damage on cross-examination when Dr Versfeld said not "a lot of light" would be needed for Pistorius' stability to be greatly improved.

5. What happened to the electrical cord?

Judge Masipa was very unhappy about the apparent disappearance of an electric extension cord from Oscar's room.

Police crime scene photos show it was present on 14th February 2013 but not the next day.

It is not on a list of seized items and police do not know where it is.

Defence lawyer Barry Roux made much of this, asking for an order for the prosecution to produce it.

He says it is important in terms of it being a possible obstruction to Pistorius' during the incident.

The prosecution deny its significance but admit they don't know what happened to it.

Judge Masipa asked for an affidavit from whoever was in charge of the house - which has since been sold - to explain what happened.