Foreign doctors could take MERS global

Share this article: Share Tweet Share Share Share Email Share

New York - The biggest risk that Middle East Respiratory Syndrome will become a global epidemic, ironically, may lie with globe-trotting healthcare workers. From Houston to Manila, doctors and nurses are recruited for lucrative postings in Saudi Arabia, where MERS was first identified in 2012. Because the kingdom has stepped up hiring of foreign healthcare professionals in the last few years, disease experts said, there is a good chance the MERS virus will hitch a ride on workers as they return home. “This is how MERS might spread around the world,” said infectious disease expert Dr Amesh Adalja of the University of Pittsburgh Medical Center. It can take five to 14 days for someone infected with MERS to show symptoms, more than enough time for a contagious person to fly to the other side of the world without being detectable. Healthcare workers “are at extremely high risk of contracting MERS compared to the general public,” Adalja said.

The threat has attracted new attention with the confirmation of the first two MERS cases in the United States. Both are healthcare workers who fell ill shortly after leaving their work in Saudi hospitals and boarding planes bound west.

About one-third of the MERS cases treated in hospitals in the Saudi Red Sea city of Jeddah were healthcare workers, according to the World Health Organisation.

Despite the risk, few of the healthcare workers now in, or planning to go to, Saudi Arabia are having second thoughts about working there, according to nurses, doctors and recruiters interviewed by Reuters.

Michelle Tatro, 28, leaves next week for the kingdom, where she will work as an open-heart-surgery nurse. Tatro, who typically does 13-week stints at hospitals around the United States, said her family had sent her articles about MERS, but she wasn't worried.

“I was so glad to get this job,” she told Reuters. “Travel is my number one passion.”

So far, international health authorities have not publicly expressed concern about the flow of expatriate medical workers to and from Saudi Arabia.

“There is not much public health authorities or border agents can do,” said infectious disease expert Dr Michael Osterholm of the University of Minnesota. “Sure, they can ask people, 'did you work in a healthcare facility in Saudi Arabia,' but if the answer is yes, then what?”

Healthcare workers are best placed to understand the MERS risk, Osterholm said, and “there should be a heightened awareness among them of possible MERS symptoms.”

Neither the Centers for Disease Control and Prevention nor the Department of Homeland Security responded to questions about whether they were considering monitoring healthcare workers returning to the United States.

SOARING DEMAND

In the last few years, the number of expatriates working in Saudi Arabia has soared, said Suleiman Arabie, managing director of Houston, Texas-based recruiting firm SA International, with thousands now working in the kingdom.

About 15 percent of physicians working in the kingdom are American or European, and some 40 percent of nurses are Filipino or Malaysian, according to estimates by recruiters and people who have worked in hospitals there.

The majority of U.S.-trained medical staff are on one- or two-year contracts, which results in significant churn as workers rotate in and out of Saudi medical facilities.

The Saudi government is building hundreds of hospitals and offering private companies interest-free loans to help build new facilities. Its healthcare spending jumped to $27-billion last year from $8-billion in 2008. Building the hospitals is one challenge, staffing them with qualified personnel is another.

Arabie's firm is trying to fill positions at two dozen medical facilities in Saudi Arabia for pulmonologists, a director of nursing, a chief of physiotherapy and scores more.

Doctors in lucrative, in-demand specialties such as cardiology and oncology can make $1-million for a two-year contract, recruiters said.

Nurses' pay depends on their home country, with those from the United States and Canada earning around $60 000 a year while those from the Philippines get about $12 000, recruiters said. That typically comes with free transportation home, housing, and 10 weeks of paid vacation each year. For Americans, any income under about $100 000 earned abroad is tax-free, adding to the appeal of a Saudi posting.

One Filipina nurse, who spoke anonymously so as not to hurt her job prospects, told Reuters that she was “willing to go to Saudi Arabia because I don't get enough pay here.” In a private hospital in Manila, she made 800 pesos (about $18) a day.

“I know the risks abroad but I'd rather take it than stay here,” she said. “I am not worried about MERS virus. I know how to take care of myself and I have the proper training.”

None of Arabie's potential candidates “have expressed any concern” about MERS. Only one of the hundreds of professionals placed by Toronto-based medical staffing firm Helen Ziegler & Associates Inc. decided to return to the United States because of MERS, it said, and one decided not to accept a job in Jeddah she had been hired for.

Recruitment agencies in Manila have also continued to send nurses to the kingdom since the MERS outbreak, said Hans Leo Cacdac, the head of the Philippine Overseas Employment Administration. The government advises that returning workers be screened for MERS, Labour and Employment Secretary Rosalinda Baldoz said this week.

Expat healthcare workers now working in Saudi Arabia feel confident local authorities are taking the necessary steps to combat the spread of MERS in hospitals.

“Just today they came and put up giant posters in our hospital on MERS,” said Dr Taher Kagalwala, a pediatrician originally from Mumbai who works at Al Moweh General Hospital in a town about 120 miles from Tai'f city in western Saudi Arabia

“I have not heard of or seen any healthcare workers looking to leave their jobs or return to their countries because of the MERS panic. If it was happening, there would have been gossip very soon.” - Reuters