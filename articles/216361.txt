Infertile men who have semen defects could have a higher mortality risk than those with healthy semen.

A new study showed men with two or more abnormalities in their semen were twice as likely to die over an eight-year period, a Stanford University Medical Center news release reported.

In the study the researchers looked at records of men between the ages of 20 and 50 who had visited medical facilities to have their fertility tested.

The research team looked at factors such as "total semen volume and sperm counts, motility and shape," the news release reported.

They compared their findings with data from the National Death Index and the Social Security Death index to determine the rate of mortality over an eight year period.

"We were able to determine with better than 90 percent accuracy who died during that follow-up time," lead author, Michael Eisenberg, MD, PhD, assistant professor of urology and Stanford's director of male reproductive medicine and surgery, said in the news release. "There was an inverse relationship. In the years following their evaluation, men with poor semen quality had more than double the mortality rate of those who didn't."

The researchers were not able to link a single gene to a higher mortality risk, but they did determine that men with two or more mortalities had double the risk.

The study looked at 11,935, 69 of which died during the eight year follow-up period. The median age of death was 36.6 years old, but men who get evaluated for fertility tend to be of a higher socio-economic status and have better diets and healthcare.

"If you're trying to have a child, you're probably reasonably healthy at the moment and in mental shape to be planning for your future," Eisenberg said in the news release.

The link between semen abnormalities and mortality risk remained even after the researcher controlled for other health factors.

"It's plausible that, even though we didn't detect it, infertility may be caused by pre-existing general health problems," Eisenberg said. "But we controlled for this factor as best we could, and while that did attenuate the measured risk somewhat, there seems to be something else going on. Could it be genetic, developmental or hormonal factors? Or could it be that something about the experience of having and raising kids - even though you may sometimes feel like they're killing you - actually lowers mortality?"

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.