Orlando Bloom hopes to inspire his son to live his dream.

The 37-year-old actor, who split from his wife of three years Miranda Kerr in October, admits being a father to their three-year-old Flynn is his top priority.

The 'The Hobbit: The Desolation of Smaug' star told E! News: ''I can only hope that I always instil him with all of the ideas of living your dream, of actually understanding that life is a cyclical thing and when you give, you receive.''

Orlando participated in the We Day California conference on Wednesday (26.03.14), along with Selena Gomez, Seth Rogen and Magic Johnson, to inspire young people to take action and affect change.

The 'Lord of the Rings' actor has been romantically linked with actresses Margot Robbie, Nora Arnezeder and Condola Rashad since splitting from Australian supermodel Miranda.

Devoted parents, the former couple are regularly spotted together in New York City with their son and previously insisted they would always consider each other ''family'' and there were no hard feelings.

Miranda said: ''We love each other as family, forever.''

Orlando also revealed in the past that he tries not to spend too much time away from Flynn.

He said: ''I won't spend more than two or three weeks away from him.''