It sounds like the stuff of vampire movies, but scientists have shown that an infusion of young blood can reverse signs of ageing.

Although the ghoulish experiment was conducted on laboratory mice, the next step could involve a study of elderly humans.

The researchers believe young blood may contain natural chemicals that turn back the clock to rejuvenate the ageing brain.

In the study, blood from three-month-old mice was repeatedly injected into 18-month-old mice near the end of their natural life span. The "vampire therapy" improved the performance of the elderly mice in memory and learning tasks. Structural, molecular and functional changes were also seen in their brains.

Writing in the journal Nature Medicine, the US team led by Dr Tony Wyss-Coray, from Stanford University, said: "Our data indicate that exposure of aged mice to young blood late in life is capable of rejuvenating synaptic plasticity and improving cognitive function.

"Future studies are warranted in aged humans and potentially those suffering from age-related neurodegenerative disorders."

Evidence was seen of new connections forming in the hippocampus, a brain region vital to memory and sensitive to ageing.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Ageing mice given eight infusions of young blood over three weeks improved their performance in mental tests. Infusions of blood from other elderly mice had no effect.

What caused the changes is still unknown, but it appears to involve activation of a protein called Creb in the hippocampus that helps regulate certain genes.

The scientists wrote: "One possibility is that introducing 'pro-youthful' factors from aged blood can reverse age-related impairments in the brain, and a second possibility is that abrogating pro-ageing factors from aged blood can counteract such impairments.These two possibilities are not mutually exclusive, warrant further investigation, and may each provide a successful strategy to combat the effects of ageing."

Dr Eric Karran, from the dementia charity Alzheimer's Research UK, described the results as "interesting", but added that the study "does not investigate the type of cognitive impairment that is seen in Alzheimer's disease".