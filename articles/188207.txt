Eight years after then-Massachusetts Gov. Mitt Romney signed the landmark health-care legislation known as "Romneycare" into law, a new study says it has saved thousands of lives.

According to new research published this week in the Annals of Internal Medicine, the expansion of health insurance in Massachusetts has resulted in more than 300 lives saved per year since its enactment - a 4.5% reduction.

The law - which expanded Medicaid coverage, offered subsidies in private insurance, and established a mandate to purchase health insurance - is considered the model for the Affordable Care Act.

Advertisement

To conduct the study, the researchers - Benjamin Sommers, Sharon Long, and Katherine Baicker - compared Massachusetts counties with similar counties across the United States that did not have a health insurance expansion program.

Specifically, they looked at conditions "amenable to health care," screening for information on different types of cancer, cardiovascular disease, infections, and other situations in which a person would be more likely to survive with better medical care. Certain types of cancer, for example, can often be prevented with earlier screening or treated more successfully with early detection.

Overall, the researchers found that "h

ealth reform in Massachusetts was associated with

significant reductions in all-cause mortality and deaths from causes

amenable to health care." As Adrianna McIntyre points out at The Incidental Economist , the effects of health reform were more pronounced in counties with comparably lower incomes and with low pre-reform insured rates.

Advertisement

Here's a chart from the study showing the comparison. The gap in deaths between Massachusetts and counties in other states grows wider after 2006, when the law championed by Romney started to be implemented:

Advertisement

The study could lead to some debate over the Massachusetts law's costs - and the costs of the Affordable Care Act - going forward. The study found that for every 830 adults who gained insurance per year, one fewer person died.

According to the Massachusetts Taxpayers Foundation, the state spends about $91 million more per year on health costs compared to the years prior to enactment of the law.

According to projections released by the Congressional Budget Office, the net cost of Obamacare in its first year will be about $36 billion, a number that is expected to grow exponentially as more people gain coverage through federal and state exchanges and through the law's expansion of the federal Medicaid program. The Obama administration said last week that more than 8 million people had enrolled through the exchanges in the law's first open-enrollment period, and about 4.8 million more have gained coverage through the Medicaid expansion.

Advertisement

The authors warned that their study cannot be completely drawn to demonstrate causality. Compared with the nation as a whole, too, Massachusetts differs demographically and has more physicians per capita than any other state. But the authors wrote it will be important to monitor not only the coverage effects from the Affordable Care Act, but also the effects on mortality and other health provisions.

"Although this analysis cannot demonstrate causality, the results offer suggestive evidence that the Affordable Care Act - modeled after the Massachusetts law - may impact not only coverage and access but also mortality," the authors wrote.

"The extent to which our results generalize to the United States as a whole is therefore unclear, which underscores the need to monitor closely the Affordable Care Act's effect on coverage, access, and population health across all states."