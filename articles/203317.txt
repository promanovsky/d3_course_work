A new study suggests that eating more fruits and vegetables may reduce the risk of stroke. The finding is a result of a meta-analysis of 20 studies conducted in Asia, the United States, and Europe, which noted that risk reduction is possible worldwide.

Researchers analyzed the data of 760,629 male and female participants who had suffered from strokes. The total incidents documented totalled to 16,981.

“Improving diet and lifestyle is critical for heart and stroke risk reduction in the general population. In particular, a diet rich in fruits and vegetables is highly recommended because it meets micronutrient and macronutrient and fiber requirements without adding substantially to overall energy requirements,” said Dr. Yan Qu, senior author of the study, in a news release. Qu is also the director of the intensive care unit at Qingdao Municipal Hospital and professor at the Medical College of Qingdao University in Qingdao, China.

The researchers put together the results of six studies from each of the US and Asia (China and Japan) and eight studies from Europe. They found out that the risk of stroke is decreased by 32 percent for every 200 grams of fruit a day and 11 percent for 200 grams of vegetables.

In 2010, China had recorded an estimated number of 1.7 million people dying, making stroke as the country’s leading cause of death. Likewise, stroke is the number four cause of death in the US and also a leading cause of disability.

The studies demonstrate that increased consumption of fruits and vegetables has several advantages. These include lowering of blood pressure, improvement of microvascular function and gaining favourable effects on cholesterol, waist circumference, body mass index, inflammation and oxidative stress.

The World Health Organization (WHO) recommends an increase of fruit and vegetable consumption by up to 600 grams a day to reduce the risk of ischemic stroke by as much as 19 percent worldwide.

Results of this meta-analysis were published on the journal Stroke.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.