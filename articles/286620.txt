The new Xbox One commercial has some unexpected consequences.

The ad features "Breaking Bad" star Aaron Paul using the Xbox One's voice controls to play a game, powering on the Xbox with a "Xbox on" command.

The result? The commercial is booting up people's Xbox One without their permission.

Advertisement

The problem is, the Xbox One sitting in people's living rooms actively listens for the very voice commands in the commercial.People first began complaining about the far-reaching effects of Aaron Paul's "Xbox on" command on Reddit and Twitter. For those who already purchased Titanfall, people have even claimed the commercial causes their Xbox One to load up the game.

A solution does exist: People can always disconnect the hardware responsible for the always-on listening feature, Xbox One's Kinect sensor.

Advertisement

From an advertising standpoint, while the Aaron Paul commercial is clearly targeted at people without an Xbox One, it could be possible for Xbox to utilize this accidental fluke by crafting ads specifically for Xbox One users. Gamers probably wouldn't be happy about it, but it's interesting to realize Xbox has the technology in place to create a commercial that turns on people's Xbox and brings up an advertised game demo or purchase page.

You can watch the Aaron Paul Xbox One ad here.