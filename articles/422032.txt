Chris Brown's on-and-off girlfriend Karrueche Tran is now under fire for dissing Beyoncé and Jay Z's baby daughter Blue Ivy during her appearance on BET's "106 & Park" on Monday.

Tran took a part in a comedic segment titled "Top Six Things Blue Ivy Thought at the VMAs," in which she made fun of the 2-year-old's natural hair. "I really did wake up like this, because my parents never comb my hair," she said, making reference to the hook off the R&B diva's hit track "Flawless." After a brief laugh, she immediately said, "Sorry Blue! I love you!"

Ever since the 26-year-old model's comment, the hip-hop couple's fans threw shade at her via social media:

Karrueche or karate chop or whatever needs to exit before dissing blue Ivy's hair. Where's your career though? — Boo thang. (@Jedlives) August 26, 2014

Why karate always got something to say about people with actual careers? Blue ivy is more important than your whole being @karrueche — Salones Best (@ALICE_RIHANNA) August 25, 2014

Blue Ivy will buy all of Vietnam and have ur whole fam working for House of Dereon @karrueche — Rihanna's Wild Thing (@WTFUMeanNoRiRi) August 26, 2014

Tran then took to her Twitter account to clarify that she was not the one who wrote the script, writing:

Now y'all know I LOVE me some Beyoncé and Blue Ivy! I didn't write this script y'all lol — Karrueche Tran (@karrueche) August 25, 2014

I am definitely team #BeyHive ❤️ — Karrueche Tran (@karrueche) August 25, 2014

Meanwhile, RollingOut recently reported that there have been rumors that co-host Keisha Chante is leaving the music countdown show, replacing her with Karrueche Tran -- who previously appeared on BET's "Just Keke" to discuss the highly publicized love triangle involving Brown and Rihanna, and returned to the network as a host for the 2014 BET Awards.