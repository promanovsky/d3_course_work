Facebook manipulated the emotions of hundreds of thousands of its users, and found that they would pass on happy or sad emotions, it has said. The experiment, for which researchers did not gain specific consent, has provoked criticism from users with privacy and ethical concerns.

For one week in 2012, Facebook skewed nearly 700,000 users’ news feeds to either be happier or sadder than normal. The experiment found that after the experiment was over users tended to post positive or negative comments according to the skew that was given to their news feed.

The research has provoked distress because of the manipulation involved.

Studies of real world networks show that what the researchers call ‘emotional contagion’ can be transferred through networks. But researchers say that the study is the first evidence that the effect can happen without direct interaction or nonverbal clues.

Anyone who used the English version of Facebook automatically qualified for the experiment, the results of which were published earlier this month. Researchers analysed the words used in posts to automatically decide whether they were likely to be positive or negative, and shifted them up or down according to which group users fell into.

It found that emotions spread across the network, and that friends tended to respond more to negative posts. Users who were exposed to more emotional posts of either type tended to withdraw from posting themselves.

There are on average 1,500 possible stories that could show up on users’ news feeds. Source: Getty Images (GettyImages)

The research drew criticism from campaigners over the weekend, who said that the research could be used by Facebook to encourage users to post more and by other agencies such as governments to manipulate the feelings of users in certain countries.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Even the scientist that edited the study had ethical concerns about its methods, she said. "I think it's an open question," Susan Fiske, professor of psychology at Princeton University, told the Atlantic. "It's ethically okay from the regulations perspective, but ethics are kind of social decisions. There's not an absolute answer. And so the level of outrage that appears to be happening suggests that maybe it shouldn't have been done... I'm still thinking about it and I'm a little creeped out too."

Facebook’s ‘Data Use Policy’ — part of the Terms Of Service that every user signs up to when creating an account — reserves the right for Facebook to use information “for internal operations, including troubleshooting, data analysis, testing, research and service improvement.”

The researchers said that constituted the informed consent required to conduct the research and made it legal. The study does not say that users were told of their participation in the experiment, which researchers said was conducted by computers so that they saw no posts.