"Game of Thrones" showrunners David Benioff and Dan Weiss has signed on for two more seasons of the popular HBO show.

According to Entertainment Weekly, Beinoff and Weiss will write and produce the next to seasons of George R.R. Martin's novels turned TV series. Beinoff recently spoke to EW about "Game of Thrones" season four, revealing there is an end in sight for the franchise.

"It feels like this is the midpoint for us," Benioff said. "If we're going to go seven seasons, which is the plan, season 4 is right down the middle, the pivot point."

"I would say it's the goal we've had from the beginning," he added. "It was our unstated goal, because to start on a show and say your goal is seven seasons is the height of lunacy. Once we got to the point where we felt like we're going to be able to tell this tale to its conclusion, that became [an even clearer] goal. Seven gods, seven kingdoms, seven seasons. It feels right to us."

However, Martin recently teased to The Hollywood Reporter there could be "Game of Thrones" movies in the works once the series is finished.

"It all depends on how long the main series runs," Martin told THR. "Do we run for seven years? Do we run for eight? Do we run for 10? The books get bigger and bigger (in scope). It might need a feature to tie things up, something with a feature budget, like $100 million for two hours. Those dragons get real big, you know."

Martin added the "Game of Thrones" films could be based off of the prequels for the series.

"They could be the basis for [a film]," Martin said. "I have written these three stories, and I have about a dozen more."

HBO's "Game of Thrones" season four will premiere on April 6.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.