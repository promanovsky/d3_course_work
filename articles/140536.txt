On Easter Sunday, Christians around the world celebrate the resurrection of their Lord, Jesus Christ.

The account of Jesus' rising from death was recorded in the Gospels of the New Testament.

Below is the Gospel story in a nutshell.

Prior to his crucifixion, Jesus had told his disciples that he would rise again on the third day after his death.

He had been buried in a tomb, guarded by an enormous stone so that no one could steal the body.

When some women visited the grave, days after his death, they found that the stone had been moved and the tomb was empty.

For God so loved the world that He gave His one and only Son, that whoever believes in Him shall not perish but have eternal life - (John 3:16).

Jesus was seen that day and for several days later, when he revisited his friends and disciples.

One disciple did not believe Jesus was alive again until he touched his fatal wounds from the crucifixion.

In the following 40 days, hundreds of people witnessed Jesus' return from death, proving all he had prophesied was true – that Christ had indeed risen from the dead before ascending to heaven.

The fact the Jesus rose from death demonstrated He has power over death, and only through Him can people have hope for eternal life.

Jesus said, "For God so loved the world that He gave His one and only Son, that whoever believes in Him shall not perish but have eternal life" (John 3:16).

With his resurrection Jesus broke the power of death and gave a hope permanent to all mankind.

Following the sombre mood of Good Friday which marks the crucifixion of Christ, Easter Sunday is therefore a time of great celebration for Christians.

Easter eggs are given as a symbol of the resurrection. Just as a chick comes out of the egg shell, Jesus emerged from the tomb to everlasting life.

In Western Christianity, Easter also marks the end of Lent, a 40-day period of fasting, repentance, moderation and spiritual discipline in preparation for Easter.

Easter Sunday is one of the best attended Sunday service of the year for Christian churches.

The biblical account of Jesus' death on the cross, or crucifixion, his burial and his resurrection, or raising from the dead, can be found in the following passages of scripture: Matthew 27:27-28:8; Mark 15:16-16:19; Luke 23:26-24:35; and John 19:16-20:30.