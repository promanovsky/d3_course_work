The World Trade Center ship, discovered in the rubble of the once-mighty skyscrapers, is starting to reveal its secrets. Tree rings, seen in the lumber used in construction of the ship, tells researchers something about the age and origin of the maritime vessel.

The World Trade Center ship was discovered in July 2010, by workers at the Manhattan site. A significant portion of the hull of the wooden ship was still preserved in wreckage found under debris from the attacks of 11 September 2001. Samples of the ship, made from white pine, were obtained for testing.

These were then matched against samples of the wood with known ages and origins. The best matches for seven samples taken from the hull were from trees felled around 280 years ago, near Philadelphia. Researchers believe the wood used to construct the World Trade Center ship was likely harvested in 1773, or soon after. The trees likely grew near the City of Brotherly Love, before being chopped down around the time of the Boston Tea Party. The city was a hotspot of ship building in the late 18th century.

Tree rings are affected by seasonal variations of rainfall. Dry years are seen as tightly-packed rings, while years with more rainfall produce greater spaces between the lines. This provides a record of micro-climates throughout history, and also serves as proof of the origin of wood.

Details in the design of the vessel are unlike that used by any large ship builder, suggesting the vessel may have been constructed by a small shipyard.

"Few late-18th Century ships have been found and there is little historical documentation of how vessels of this period were constructed. Therefore, the ship's construction date of 1773 is important [and]... represents a rare and valuable piece of American shipbuilding history," researchers wrote in an article describing the study.

The layout of the entire ship was photographed and mapped for archeological study, before pieces were removed from its resting place.

Ed Cook, a research professor in the tree ring lab at the Lamon-Doherty Earth Observatory, examined wood samples obtained from Independence Hall in the early 1990's. His maps of rings found in those samples matched the wood from the World Trade Center ship. This suggests the ship was built from trees near those used in the construction of the famous historical site.

The ship is believed to be a Hudson River sloop, a type of vessel designed to ferry cargo and passengers in shallow, rocky water. It likely sailed for two to three decades before coming into the city to retire. Over years, it was covered in garbage and landfill before the Twin Towers were constructed over the artifact.

Investigation of the World Trade Center ship and how material used in its onstruction reveals the time and place of its construction was detailed in the journal Tree Ring Research.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.