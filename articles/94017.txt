“Is Richie Rich still alive?” — commenter KattMannDoo at cnn.com ponders the fate of another comic book character after reading an article about the impending death of Archie in the “Life With Archie” series. Not to be confused with the regular Archie comics, in which the red-headed teen is perpetually in high school, “Life With Archie” is a spinoff series that depicts Archie’s future. The real question, though, is whether he ends up with Betty or Veronica. P.S., Richie Rich is still alive in modern comics.

“Chicken and waffles funnel cakes just got a heck of a lot more portable, wheeeee.” — Dominique Zamora at foodbeast.com describes fast food chain KFC’s latest product — fried chicken paired with funnel cake fries instead of potatoes. Available only in select KFC locations in Baltimore until April 20, the special “Go Cups” cost $2.49. Sounds like a deal! Well, except for your arteries …

AD

AD

“Head down to the Tidal Basin after the Cherry Blossom festival. NPS workers will be removing seasonal plants that are planted exclusively for the festival. They’ll give you whatever you want. I’ve been getting tulips this way for years.” — redditor murgurdurth at reddit.com responds to a thread soliciting D.C. “life-hacks,” aka tips to make living in Washington better. The festival ends on Sunday, just as the clamoring for free plants begins.

“um, that’s a Facebook screen grab right?!” — @CapitolRomance tweets her reaction to Twitter’s latest addition, profiles, which look very similar to those on Facebook. The new profile design launched Tuesday. Do you like or unlike? Join the conversation by tweeting @WaPoExpress.