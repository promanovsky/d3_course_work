London: In a medical first, a survivor of a serious road accident in the UK has undergone pioneering surgery to reconstruct his face using 3D printed parts.

In an eight-hour surgery, doctors at Morriston Hospital in Wales repaired 29-year-old Stephen Power`s cheekbone and eye socket using the cutting-edge technology.

Doctors had to break his cheekbones again before rebuilding his face.

While printed implants have previously been used to help correct congenital conditions, this operation used custom printed models, guides, plates and implants to repair impact injuries months after they were sustained.

Despite wearing a crash helmet Power suffered multiple trauma injuries in an accident in 2012, which left him in hospital for four months.

"I broke both cheek bones, top jaw, my nose and fractured my skull," the BBC quoted Power as saying.

In order to try and restore the symmetry of his face, the surgical team used CT scans to create and print a symmetrical 3D model of Power`s skull, followed by cutting guides and plates printed to match.

Maxillofacial surgeon Adrian Sugar says the 3D printing took away the guesswork that can be problematic in reconstructive work.

"I think it`s incomparable - the results are in a different league from anything we`ve done before," he said.

A medical-grade titanium implant, printed in Belgium, was then used to hold the bones in their new shape.

Looking at the results of the surgery, Power says he feels transformed - with his face now much closer in shape to how it was before the accident.

"It is totally life changing," he said.

"I could see the difference straight away the day I woke up from the surgery."

"It tends to be used for individual really complicated cases as it stands - in quite a convoluted, long-winded design process," design engineer Sean Peel said.

"The next victory will be to get this process and technique used more widely as the costs fall and as the design tools improve."

Doctors said the surgery is thought to be one of the first trauma patients in the world to have 3D printing used at every stage of the procedure.

Power`s operation is currently being featured in an exhibition at the Science Museum in London.