It only took seven years, but Viacom and Google's YouTube have finally come to an agreement that settles their long-running copyright case.

Terms of the deal were not disclosed, but the arrangement apparently puts the $1 billion fight to rest.

"Google and Viacom today jointly announced the resolution of the Viacom vs. YouTube copyright litigation," the companies said in a joint statement. "This settlement reflects the growing collaborative dialogue between our two companies on important opportunities, and we look forward to working more closely together."

The case dates back to 2007, when Viacom sued YouTube for $1 billion. Viacom argued that YouTube facilitated the posting of copyrighted material. YouTube said it was not responsible if YouTube complied with Digital Millennium Copyright Act (DMCA) takedown notices, which it said it did. In 2010, a judge agreed and said YouTube has been compliant. Viacom swiftly appealed and the Circuit Court in 2012 ruled in the company's favor. That re-opened the case for another two years, until today's deal.

At the heart of the matter was whether YouTube was responsible for the copyrighted material its users posted on the site. In general, sites that host user-generated content are protected by the DMCA if they take swift action to remove offending content when it's reported. YouTube argued that it does remove this content, but Viacom's initial lawsuit said YouTube was hosting at least 160,000 unauthorized Viacom clips.

There was a lot of back and forth, of course. In 2010, Google said that Viacom had "secretly uploaded its content to YouTube, even while publicly complaining about its presence there" for years. Viacom denied the charges and said YouTube was hiding behind a "willful blindness policy."

It appears both sides have now put the fight behind them - for an undisclosed sum, no doubt.

Further Reading