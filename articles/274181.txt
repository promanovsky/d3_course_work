TV

Macklemore also helps poke fun at the lesser known Lewis as the latter is sent by Jimmy Fallon to interview New Yorkers to find out what they know about Lewis.

Jun 4, 2014

AceShowbiz - "Everyone knows who Macklemore is. But not many people know who Ryan Lewis is, or, even what he does," Jimmy Fallon said as he introduced a clip on "The Tonight Show Starring Jimmy Fallon" Monday night, June 2. In the video, Ryan Lewis of Macklemore & Ryan Lewis interviewed music fans to find out what they knew about him.

The result was hilarious as some self-proclaimed hip-hop fans were clueless about Lewis. One young guy, who said his favorite music acts are Eminem and Macklemore, said of Lewis, "Don't really know what he does; he's like a package deal. He comes with Macklemore a lot."

Another guy thought Lewis is working onstage with "computers and whatnot." One woman, who gave Lewis C+ for his looks, couldn't even recognize him after he showed his picture with Macklemore. Another woman said she didn't know what Lewis looked like, but said he's more good looking than Macklemore after seeing the photo of the rap duo.

Toward the end of the video, Macklemore himself showed up to poke fun at Lewis, saying he had "no idea" what Lewis did.

The next day, Fallon welcomed Jonah Hill as a guest. The actor used the chance to once again apologize for making a gay slur to a paparazoo. "I didn't mean this in the sense of the word, you know? I didn't mean it in a homophobic way," he said, looking emotional. "I think that, um, that doesn't matter. How you mean things doesn't matter. Words have weight and meaning, and the word I chose was grotesque. And, you know? No one deserves to say or hear words like that."

"I genuinely am deeply sorry to anyone who's ever been affected by that term in their life," the "22 Jump Street" star added. "I don't deserve or expect your forgiveness, but what I ask is that at home; if you're watching this, and you're a young person especially; if someone says something that hurts you or angers you, use me as an example of what not to do, and don't respond with hatred or anger. Because you're just adding more ugliness to the world."

Ryan Lewis Interviews Music Fans About Ryan Lewis:

Jonah Hill Addresses His Controversial Remarks: