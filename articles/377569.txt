US stocks ended higher on Monday, with the Dow Jones industrial average hitting an intraday record, lifted by Citigroup's better-than-expected earnings and more deals in the healthcare space.

Monday's flurry of mergers and acquisitions activity in the healthcare sector gave investors some confirmation that the US stock market is still attractive. Shire, which develops and sells drugs to treat rare diseases, succumbed to an increased takeover offer of £31 billion from AbbVie on Monday.

US-listed shares of Shire rose 2.1 per cent to $US254.27 while AbbVie shares slipped 0.2 per cent to $US54.85.

Generic drugmaker Mylan Inc said it would buy Abbott Laboratories' specialty and branded generics business outside the United States in an all-stock transaction valued at about $US5.3 billion. Mylan shares advanced 2.1 per cent to $US51.24 while Abbott shares rose 1.3 per cent to $US41.82.

Citigroup shares jumped 3 per cent to $US48.42 and gave the S&P 500 one of its biggest boosts. The stock rallied after Citigroup reported second-quarter adjusted earnings per share that exceeded the average analyst estimate and agreed to pay $US7 billion to settle a U.S. government investigation into mortgage-backed securities. The S&P financial index gained 0.6 per cent.