Nancy Trejos

USA TODAY

NEW YORK—Hilton Worldwide announced on Monday the launch of Curio, a collection of four- to five-star hotels.

So far, Hilton has signed letters of intent with five hotels to join the collection: the soon-to-open SLS Las Vegas Hotel and Casino, the Sam Houston Hotel in Houston, the Hotel Alex Johnson in Rapid City, S.D., the Franklin Hotel in Chapel Hill, N. C., and a soon-to-be-named hotel in downtown Portland, Ore.

"We had an opportunity to acquire a customer that maybe isn't in our system who has an interest in this type of hotel," Christopher Nassetta, president and CEO of Hilton Worldwide, said at the annual NYU International Hospitality Industry Investment Conference. "Once we get them in, and they experiment with us in any particular brand, they tend to start to travel with us much more frequently."

A number of major hotel chains have similar collections of independently owned hotels that maintain their own identities while taking advantage of the company's resources. Marriott has the Autograph Collection, Starwood has the Luxury Collection, and Choice has Ascend.

Guests staying at Curio hotels will be able to earn and redeem points from the Hilton HHonors loyalty program.

Nassetta said the company, which went public late last year, will also start a new "accessible lifestyle" brand with affordable rates as early as this fall.

"We'll open up to a much more broader base of demand that's going to allow us to serve more customers and grow at a much faster pace than we've seen with our competitors," he said.

Also today, Marriott International announced that it will add more than 200 luxury and lifestyle hotels over the next few years, reflecting a more than $15 billion investment.

The company now has 449 luxury and lifestyle hotels across eight brands.

Growth plans include 15 additional Ritz-Carlton hotels between now and by the end of 2016, 23 new J.W. Marriott hotels in the next two years, and nine new Renaissance hotels this year.

"The luxury and lifestyle category is stronger than ever and we see demand continuing to rise worldwide," said Arne Sorenson, president and CEO of Marriott.

On Wednesday, Starwood Hotels and Resorts will announce that its Element hotel brand will double in size in the next two years. The company will open the 15th hotel of the eco-friendly, lifestyle brand later this year. Another 15 will open by the end of 2017.

Element will also expand outside of North America for the first time with a new hotel at Frankfurt airport this September. Next year, Element Suzhou Science and Technology Town will open in China.

Starwood also plans to open dual-branded properties combining Element and Aloft hotels. The company recently signed a deal to open Aloft and Element Boston Waterfront in 2016 across the street from the Boston Convention and Exhibition Center.

"We're seeing tremendous growth momentum for our trailblazing Element brand and for the first time ever, we're expanding outside of North America, bringing stylish, sustainable living to the European and Chinese markets," said Brian McGuinness, Senior Vice President of Specialty Select Brands for Starwood.