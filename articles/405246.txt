Stop sunbathing and using indoor tanning beds, the acting U.S. surgeon general warned in a report released Tuesday that cites an alarming 200 percent jump in deadly melanoma cases since 1973.

The report blames a generation of sun worshipping for the $8 billion spent to treat all forms of skin cancer each year.

Rear Adm. Boris Lushniak said state and local officials need to do more to help people cover up, such as providing more shade at parks and sporting events. Schools should encourage kids to wear hats and sunscreen and schedule outdoor activities when the sun is low in the sky. And colleges and universities should eliminate indoor tanning beds on campus much as they would prohibit tobacco use, he added.

"We need more states and institutions on board with these policies that discourage or restrict indoor tanning by our youth," Lushniak said. "Tanned skin is damaged skin."

The surgeon general's "call to action" plan is part of a broader push this year by government officials and public health advocates to raise awareness on what they say has become a major public health problem. While other cancers such as lung cancer are decreasing, skin cancer is rising rapidly. According to the Department of Health and Human Services, 5 million people are treated for skin cancer each year. And the number of Americans with skin cancer in the past three decades eclipse the number of all other cancers combined.

Melanoma is the deadliest form of skin cancer with 9,000 people dying each year from the mostly preventable disease.

Stacey Escalante of Las Vegas, Nevada, blames years of sunbathing with baby oil and using indoor tanning beds for her melanoma diagnosis in 2005. The mother of two was a 34-year-old television reporter training for a marathon when she found a small red growth the size of a pencil eraser on her lower back. By the time she saw a doctor, the cancer had traveled to her lymph node, requiring two surgeries that left an 8-inch scar. She then spent two years on an experimental drug.

Escalante said she realizes now that she was lucky to survive, and was foolish to think she was immune to skin cancer because her father was Hispanic and she tanned well. Now an advocate for early detection, Escalante is pushing for state legislation prohibiting minors from using indoor tanning beds.

The Melanoma Research Foundation says exposure to tanning beds before age 30 increases a person's risk of developing melanoma by 75 percent.

"If only I had first gone to the doctor, when I first saw that spot, instead of ignoring it, I would have saved my family and myself ... the emotional, physical and financial burden of skin cancer," she said. "It was absolutely overwhelming."

Howard Koh, assistant secretary for health for the Department of Health and Human Services, said skin cancer prevention needs to become a bigger part of daily American life.

"We need to change the social norm with respect to tanning and shatter the myth that tanned skin is somehow a sign of health," Koh said.

Doctors recommend doing regular skin checks for new moles and seeing a doctor if any change in size, shape or color. Doctors also recommend applying at least 1 ounce of sunscreen with an SPF of 30 or more to exposed skin and reapplying every two hours, more if swimming or sweating. Children in particular should be protected because bad sunburns in childhood are thought to greatly increase risk later in life.