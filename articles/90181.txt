A new study has shown that, for women, a glass of milk a day can keep osteoarthritis at bay

A GLASS of milk a day can keep osteoarthritis (OA) at bay, at least for women with the disease affecting their knees, research has shown.

Increasing consumption of fat-free or low-fat milk was found to slow progression of the degenerative condition, which wears away the joints.

Women who drank more than seven eight-ounce glasses a week had significantly less space between their joints than those who drank none.

Those who drank no milk had an average width space of 0.38 millimetres, compared with 0.26mm for high consumers, after four years.

Even drinking up to three glasses a week led to a shrinking of the joint gap to 0.29mm.

However, no association was seen between milk consumption and reduced joint space width in men.

DISABILITY

"Milk consumption plays an important role in bone health," said lead scientist Dr Bing Lu, from Brigham and Women's Hospital in Boston, US. "Our study is the largest to investigate the impact of dairy intake in the progression of knee OA.

"Our findings indicate that women who frequently drink milk may reduce the progression of OA. Further study of milk intake and delay in OA progression are needed."

The findings are reported in the latest edition of the journal 'Arthritis Care & Research'.

In an editorial published in the journal, US experts Dr Shivani Sahni and Robert McLean, from the Hebrew SeniorLife Institute for Aging Research, affiliated with Harvard University, wrote: "The study by Lu et al provides the first evidence that increasing fat-free or low-fat milk consumption may slow the progression of OA among women who are particularly burdened by OA of the knee, which can lead to functional disability."

PA Media