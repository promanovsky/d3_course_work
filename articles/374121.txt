An Ohio man who jokingly sought $10 (U.S.) to pay for his first attempt at making potato salad has raised tens of thousands of dollars from a crowdfunding Internet site.

Zack "Danger" Brown, of Columbus, says he's now considering throwing a huge public potato-salad party with the money, which started pouring in after his request took on a life of its own.

Six days into the campaign, he's raised money from more than 3,400 backers worldwide.

Story continues below advertisement

The 31-year-old co-owner of a software company says he hasn't been getting much sleep.

Brown tells The Columbus Dispatch that he did it for the "pure enjoyment and silliness of life."

Below is a look at what people backing the crowdfunding campaign are getting for their money, though the figures have grown tremendously since the morning of July 8.