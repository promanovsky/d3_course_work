Apple Inc. (AAPL) said Tuesday that it updated MacBook Pro with Retina display with faster processors, double the memory in both entry-level configurations, and a new, lower starting price for the top-of-the-line 15-inch notebook. MacBook Pro with Retina display features a high-resolution display, 0.71-inches thin, the latest processors and powerful graphics, and up to nine hours of battery life.

The company also lowered the starting price of the non-Retina 13-inch MacBook Pro, a very popular system with Windows switchers, by $100 to $1,099.

According to the company, the 13-inch MacBook Pro with Retina display features dual-core Intel Core i5 processors up to 2.8 GHz with Turbo Boost speeds up to 3.3 GHz and 8GB of memory, up from 4GB in the entry-level notebook. The 13-inch model can also be configured with faster dual-core Intel Core i7 processors up to 3.0 GHz with Turbo Boost speeds up to 3.5 GHz.

The top-of-the-line 15-inch MacBook Pro with Retina display has a new, lower starting price of $2,499. The 15-inch model features faster quad-core Intel Core i7 processors up to 2.5 GHz with Turbo Boost speeds up to 3.7 GHz, 16GB of memory, up from 8GB in the entry-level notebook, and can be configured with quad-core Intel Core i7 processors up to 2.8 GHz with Turbo Boost speeds up to 4.0 GHz.

The company stated that the 13-inch MacBook Pro with Retina display is available with a 2.6 GHz dual-core Intel Core i5 processor with Turbo Boost speeds up to 3.1 GHz, 8GB of memory, 128GB of flash storage and Intel Iris graphics starting at $1,299 ; with a 2.6 GHz dual-core Intel Core i5 processor with Turbo Boost speeds up to 3.1 GHz, 8GB of memory, 256GB of flash storage and Intel Iris graphics starting at $1,499 ; and with a 2.8 GHz dual-core Intel Core i5 processor with Turbo Boost speeds up to 3.3 GHz, 8GB of memory, 512GB of flash storage and Intel Iris graphics starting at $1,799. Configure-to-order options include faster dual-core Intel Core i7 processors up to 3.0 GHz with Turbo Boost speeds up to 3.5 GHz, up to 16GB of memory and flash storage up to 1TB.

The 15-inch MacBook Pro with Retina display is available with a 2.2 GHz quad-core Intel Core i7 processor with Turbo Boost speeds up to 3.4 GHz, 16GB of memory, 256GB of flash storage and Intel Iris Pro graphics starting at $1,999; and with a 2.5 GHz quad-core Intel Core i7 processor with Turbo Boost speeds up to 3.7 GHz, 16GB of memory, 512GB of flash storage, and Intel Iris Pro and NVIDIA GeForce GT 750M graphics starting at $2,499. Configure-to-order options include faster quad-core Intel Core i7 processors up to 2.8 GHz with Turbo Boost speeds up to 4.0 GHz and flash storage up to 1TB.

The 13-inch MacBook Pro is available with a 2.5 GHz quad-core Intel Core i5 processor with Turbo Boost speeds up to 3.1 GHz, 4GB of memory, Intel HD Graphics 4000 and a 500GB hard drive starting at $1,099.

For comments and feedback contact: editorial@rttnews.com

Business News