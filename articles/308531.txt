What: Federal Reserve Chair Yellen may have taken the stage on last Wednesday to discuss current economic outlooks, but for Annaly Capital Management (NYSE:NLY) and ARMOUR Residential REIT (NYSE:ARR) only one question mattered: When will the Federal Reserve raise interest rates?

While Chair Yellen made it absolutely clear the Federal Reserve was not putting a timeline on interest rate "lift off," there was one comment on the topic that caught my attention. The statement stemmed from a question on the Fed's predictions on monetary policy, and how it will react if there's progress toward the 2% personal consumption expenditures (PCE) inflation target and full employment (a mixed bag of labor market numbers), faster than expected.

In response, Chair Yellen suggested, "The timing and pace of interest rate increase ought to, and I believe will, respond to unfolding economic development... if those were to improve faster than the committee expects it would be logical to expect more rapid increase in the fed funds rate."

So what: Due to the threat of rising interest rates during the past year, Annaly and ARMOUR have been loading their portfolios full of interest rate swaps. These derivative tools allow the companies to trade their floating interest rates -- roughly tied to the federal funds rate -- for a fixed rate. While the fixed-interest rates are more expensive in the short term, they lock-in the cost of funding and protect borrowing costs from exceeding asset yields – which would lead to both companies' losing money hand over fist.

Based on the Fed's comments in March, though, which suggested that interest rates won't increase for some time, both companies had the option to eliminate some of their shorter-duration contracts. This was something Annaly's CEO Wellington Denahan assured investors the company was doing in their first-quarter conference call.

However, considering Chair Yellen has left the door open to raise interest rates sooner than expected, it could leave both companies vulnerable. So, where does this leave Annaly and ARMOUR?

Now what: Ultimately, I believe both Annaly and ARMOUR are in a good position for the time being. Despite Chair Yellen's comment, the speech's overriding theme was that there is still much work to be done.

This holds especially true in the labor market. While the employment picture has improved substantially during the last six months, unemployment is still well above historical norms.

Moreover, Chair Yellen noted that the unemployment rate alone isn't reflective of the true health of the labor market. In fact, there are still a high number of discouraged workers -- those of working age not seeking work -- taking part-time employment for economic reasons, and wage increases are trending just slightly higher than inflation. All of these are signs that there's still a ways to go.

As far as inflation goes, consensus predictions point toward moderate increases during the next two years. However, even if inflation does rise faster than expected and it requires Fed action, the phrase "lift off" may be inappropriate because rates won't skyrocket immediately.

Rather, once the Fed decides to increase rates, it will likely be slow and controlled. Also, based on Chair Yellen's openness, investors should have fair warning, which should allow both Annaly and ARMOUR time to make adjustments to their respective portfolios.