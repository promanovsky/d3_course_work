Nine months ago, deep in a heavily guarded lab in Seoul, South Korea, LG's mobile design team set out to create a phone to rival Samsung's flagships. It was no light responsibility: According to IDC, Samsung dominated smartphone sales in 2013 with 31.3 percent of the market compared to LG's 4.8 percent. Yet, in the amount of time it takes a newborn baby to blossom from conception to her first appearance in this world, LG cranked out a compelling piece of circuitry. The G3, announced today, comes with the highest-res display on the market, a 5.5-inch body that feels smaller than it really is, a fresh metallic finish and a never-before-seen imaging component invented by a robotic vacuum company.

Gallery: LG G3 hands-on | 36 Photos





































































/36 Gallery: LG G3 hands-on | 36 Photos





































































/36

LG is struggling. It may not have the same image problem HTC does, but it's clearly coming into this project as an underdog. Sales of the G2, its prior flagship, sputtered against expectations despite a new design direction and market-topping components. The company wasn't able to climb out of the Galaxy S4's shadow, so it turned its focus to beating Samsung. The G3 uses a larger, higher-resolution screen than its rival without making the phone feel unwieldy. It features a larger battery and it boasts a faster autofocus (thanks to its new laser sensor), all while fixing sore spots users had with the G2.

LG is struggling. It may not have the same image problem HTC does, but it's clearly coming into this project as an underdog.

After walking through what seemed to be a typical, sterile, cubicle-laden office building, we entered a white-walled, nondescript meeting room in LG's design headquarters in Seoul. On a table, amongst an endless series of office chairs and whiteboards, sat a plethora of G3 mockups. Next to them sat several different variations on back button arrangements and at least two dozen back covers in nearly every color you can imagine. The full lineup was obviously transplanted here from some secret lab in another part of the building. Although none of the mockups functioned -- the front of each one was just a blank plastic face -- they were made to mimic the proper weight and feel of a real phone. Despite repeated requests, we weren't allowed to take pictures of the mockups because, understandably, any of them could be used for a future device.

These design candidates were just a few of the 300 models LG considered, and ultimately rejected, for the final G3 design. Each version had a different texture, color or material. Some were glossy, others ridged and still others built with materials we couldn't quite put a finger on. The back buttons were arranged in ways we'd never seen before; one version even replaced the buttons with a circular apparatus of undisclosed purpose. Not only were we staring directly at LG's past, but we were also likely gazing into its future.

The G3 isn't the first phone to feature a Quad HD display, but it's the first from a major manufacturer. Some argue that such high resolution on a small screen doesn't make enough of a difference; naturally, LG believes it does. The company touted blind tests that show people prefer screens with 500 pixels per inch (ppi) over 400 (the G3 sits at 538 ppi), and they found that small text and nighttime scenes were definitely clearer on the higher-res display. There will always be demand for more than what we already have, and LG is anxious to beat the competition in any way possible.

Not only were we staring directly at LG's past, but we were also likely gazing into its future.

LG is confident that if the display doesn't get you interested in the G3, the phone's new laser sensor will -- and the feature almost didn't even make it into the final product. A first in the digital camera industry, the new feature was actually developed by Roboking, the makers of LG's robotic vacuums. Originally, it was intended to measure the distance between the vacuum and obstacles that it may have to maneuver around. Roboking dropped it for a number of reasons, but it told the mobile team about it over a coffee break and the rest is now history: Imaging specialists went to work adding the vacuum's laser beams to the G3, which helped the camera snag an even faster focus lock than its closest competitors (both the One M8 and Galaxy S5 boast 300ms focus times, whereas the G3 claims 276ms). Fortunately for our eyes and sanity, this sensor doesn't double as a laser pointer.

To our surprise, nearly half of the prototypes didn't even feature a spot for the sensor because the team had to prepare for the possibility that it might not be ready in time. The feature was confirmed for the G3 two months ago, just before the final deadline. There was just enough time for Chul Bae Lee, VP of mobile design, and his team to tweak the design to make sure everything fit properly, which involved a few changes to the device's shape. "Any small treatment could affect the shape of the phone," Lee said. "Before, the shape was slightly different; the arc was a bit more flattened."

To our amazement, one of the mockups was aluminum. Wait: LG actually considered a metal phone? Like Samsung, LG has historically opted to use polycarbonate plastic for most smartphones. Alas, aluminum was vetoed at some point in the process, in favor of a metallic hairbrush finish that looks and feels like the real deal, but doesn't attract fingerprints or scratches. It's warmer, lighter and offers an anti-scratch coating. It's also much less slippery, a pain point that frustrated us with the HTC One M8. There's less risk of attenuation issues (as we saw with the iPhone 4 a few years ago), and wireless inductive charging is much more effective on a plastic surface, since it tends to heat up metal surfaces too easily. As it turns out, there are plenty of reasons why so many manufacturers avoid the material.

But why not use the same self-healing finish found on the curvaceous G Flex? While its ability to fend off dings and cuts isn't perfect, it was a groundbreaking feature that helped it stand out from the crowd. It's also a few steps ahead of anything LG's competitors have come out with. According to Lee, "We couldn't find a way to adapt self-healing without making it glossy."

Users' aversion to glossy plastic was just one of the lessons LG took to heart from the G2.

Users' aversion to glossy plastic was just one of the lessons LG took to heart from the G2. As good as the phone was, it had its share of flaws, and user feedback was critical. The G3 is designed to be more comfortable, simpler (tagline: "Technology, simplified") and easier to hold. To bolster the support of power users, the rear cover and battery are now removable and a microSD slot sits underneath for expandable storage. And as Dr. Ram-Chan Woo, head of mobile product planning, is quick to point out, "The [G2] back cover is more plasticky and a fingerprint magnet." Indeed, if you liked the G2, the G3 should look incredibly tempting right now.

Lee added that only half of G2 users liked the back button setup. It's an acquired taste that grew on us after a while, but it has to grab the attention of buyers from day one. His team changed the shape of the buttons to circles and added more separation between them and the camera in an attempt to make it more difficult to smudge the lens. The designers also added a pointy power key in the center, the highest peak on the phone's chassis, because too many users lose their grip on big phones. The new style, Lee said, stabilizes your thumb to offer a secure grip, despite the device's 5.5-inch frame. This, in addition to the thin sides and arched back cover, also makes the G3 feel smaller than it really is.

The G2's 3,000mAh battery was among the best for flagship smartphones of a similar size (that is, not a "phablet"), so LG chose not to make compromises on its successor by shrinking its size. This was a challenge, thanks to the device's floating arc design; the arched back is the reason why this 5.5-inch phone doesn't feel like a behemoth, but there was too much curvature for LG to fit a standard battery of that size. The solution: an "arc battery." Upon closer inspection, the battery seemed just as flat as any other battery on the market. Not so, according to Woo: "It looks flat, but it isn't. Since the back cover is slightly arched, the battery is also arched in order to fit the cover."

Trendy colors aside, LG can and has brought some innovative solutions to the table.

But if the battery stays exactly the same size, won't the higher-res Quad HD panel suck up a lot more power? Not if the company's marketing claims speak the truth: The display utilizes something called dynamic frame rate control, which tells the display to stop refreshing if you're on the same screen for long periods of time. If the content on the screen stays the same, the frame rate drops to 30 fps, which should be a huge aid to battery efficiency.

The G3 will come in five colors. White and "titanium" are the obvious go-tos for phone manufacturers. "About 80 percent of actual sales are white and black, so you have to offer [those colors]," Lee said. While we always prefer a choice of colors, it doesn't always equate to success, so the design team chose shades that reflect current trends: Gold, violet and red will also be offered. All of LG's biggest competitors have gold-colored flagships now, and red is starting to surface on more devices. Violet was an unusual choice, but Lee insisted: "Purple seems to be quite the fashion these days."

Trendy colors aside, LG can and has brought some innovative solutions to the table. The back buttons, laser sensor and metallic finish are primary examples of that, but it's had a difficult time selling the message that its flagships can hold their own against Samsung, HTC and other competitors. The G3 is a step in the right direction, but it's not going to matter if it can't beat Samsung at its own game: Marketing and mindshare.

Mat Smith contributed to this post.