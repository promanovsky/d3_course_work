TV

Radnor says the most important Robin-Ted scene, which would explain their last scene, was cut from the finale, gushing that it's 'really sweet and sad and funny.'

Apr 2, 2014

AceShowbiz - "How I Met Your Mother" ended its nine-season run on a high note. The series finale posted record ratings for the show with 12.9 million viewers and a 5.3 rating among adults 18-49, marking the most-watched and highest-rated episode for the series.

Jumping 51|percent| from last week's episode, the one-hour finale placed second among broadcast networks' programs that night at the 8 P.M. in term of total viewers, beating NBC's usual Monday winner "The Voice" which garnered 12.1 million viewers and a 3.4 rating last night. ABC's "Dancing with the Stars" was the No. 1 show on Monday, March 31 with 13.7 million viewers.

"HIMYM" series finale, however, was the winner in the adults 18-49 demo.

The show's ending displeased some fans as [SPOILER ALERT!] it killed off the titular mother and featured Ted pursuing Robin again. As Ted's daughter said, "That isn't the story of how you met our mother. This is a story about how you're totally in love with Aunt Robin," Josh Radnor who played Ted admits the show's title "was always a bit of a fake-out."

"It was more of a hook to hang the thing on," he further tells Vulture. "Really it was more about these are the crazy adventures and these are the lessons I had to learn before I met your mother."

Responding to fans' criticism to the last scene, Radnor says, "There are so many opinions floating around. There have always been people that thought that Barney and Robin were perfect together, there have always been people that thought it didn't make sense. There are people that wanted Ted and Robin to be together. There are people that thought they didn't work together."

He adds, "I think if you're going do something new and bold and daring, you're going to upset some people and you're gonna thrill others. I think it's better to do that than try to have some homogenized, safe ending that was never really what the show was."

Justifying the Ted and Robin possible reunion, Radnor says there's a scene that could explain it, but the scene was cut from the finale. "They cut a scene that Cobie [Smulders] and I shot between Ted and Robin," he reveals. "I thought it was a really important scene and I talked to Carter [Bays] and Craig [Thomas] about it. I understand why they cut it, but I thought it laid in that Robin had been thinking about Ted all these years more than Ted had been thinking about Robin. But who knows?"

Dishing on the said scene, he shares, "It was a scene after they ran into each other on the street. They had lunch the next day. I don't want to go too much into it because they obviously cut it for a reason, but I thought it was a really sweet and sad and funny scene. It also talked about Robin having a run-in with a bull in Spain."