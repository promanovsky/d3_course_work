Johannesburg - Nobel Laureate and writer Nadine Gordimer will be remembered for her principled stand against apartheid beyond her contribution to literature and the arts, former president Thabo Mbeki said on Tuesday.

“Her work sought to challenge all of us critically to reflect on the things we find comfort in believing without question,” Mbeki said in a statement.

“As a critic, she too willingly opened herself to criticism because she understood that her vocation as a writer was principally about social interrogation and inquiry. Our country is poorer without her.”

Mbeki has conveyed his condolences to Gordimer's family.

Gordimer died on Sunday, aged 90.