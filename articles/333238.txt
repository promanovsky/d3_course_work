According to a person familiar with the situation, Shepherd was negotiating a new contract and she and ABC failed to reach an agreement. The person wasn't authorized to discuss the matter publicly and spoke on condition of anonymity.

In a written statement Thursday, Shepherd said that after ''careful consideration'' she has decided it's time to move on.

LOS ANGELES — Sherri Shepherd is leaving ''The View'' after seven years and freshman co-host Jenny McCarthy said she's right behind her, a major upheaval for the daytime talk show a month after creator Barbara Walters' retirement from on-camera duties.

Shepherd's manager, Darris Hatch, replied ''yes'' in an email when asked if that was accurate.

Advertisement

Shepherd got a Twitter shout-out from fellow host Jenny McCarthy, whose one-year contract ended without a new deal, according to the person familiar with the situation.

''If Sherri goes ... I go too,'' McCarthy tweeted Thursday from her verified account, adding ''#sisters,'' followed by another tweet: ''My View will be changing too. As will with many hard working folks. Thanks to everyone at the show for your dedication and an amazing year.''

McCarthy's publicist, PMK-BNC, didn't immediately respond to a call and email seeking comment. Walters, who retired as a host but remains executive producer of ''The View,'' did not immediately respond to a request sent to the publicity firm, which also represents her.

ABC declined to discuss Shepherd or McCarthy's status but issued a statement saying change is ahead for the show, which is on hiatus but returns in July to finish out the current season. Whoopi Goldberg is the only host left in place.

'''The View' will be moving in an exciting new direction next year, and ABC has made decisions to evolve the show creatively,'' the network said, without any specifics.

The show has seen revolving-door change before, with past hosts including Elisabeth Hasselbeck and Rosie O'Donnell. But Walters' retirement looks to kick off a new era for ''The View,'' which launched a wave of multi-host daytime talk shows after its 1997 launch.

Advertisement

___

Online:

http://www.abc.go.com