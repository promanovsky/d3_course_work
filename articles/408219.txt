Dick Wagner: Sessionist for such acts as Alice Cooper, Lou Reed, Kiss and Aerosmith. — Reuters/Jon Gillies/Iron Street Studio handout pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

PHOENIX, Aug 1 — Rock guitarist Dick Wagner, whose prolific session work graced the albums of such acts as Alice Cooper, Lou Reed, Kiss and Aerosmith during the 1970s, has died at age 71 in Arizona, his manager said yesterday.

The Michigan-bred musician, featured on scores of albums and hailed by fans as “the Maestro of Rock”, suffered from a number of health problems in his later years, including two heart attacks and a stroke.

He died on Wednesday at a hospital in Scottsdale, Arizona, succumbing to respiratory failure about two weeks after undergoing a cardiac procedure, his manager and business partner, Susan Michelson, told Reuters.

Wagner began his rock career in the 1960s with the formation of an early Detroit-area band called the Bossmen. He gained wider notice after establishing the Frost, recording his first three Billboard-charted albums with that group.

After moving to New York he formed another band, Ursa Major, whose original but short-lived lineup included Billy Joel on keyboards, a juncture that led to Wagner’s longtime association with music producer Bob Ezrin.

Teamed up by Ezrin with fellow session guitarist Steve Hunter, Wagner went on to lend his talents to Lou Reed in the studio and on tour, including work on the seminal live album “Rock ’n’ Roll Animal”.

Ezrin also recruited Wagner to play lead guitar solos on Alice Cooper’s breakthrough “School’s Out” album, performances that were credited at the time to the Alice Cooper band, according to Wagner’s official website.

The Wagner-Cooper collaboration continued as Cooper went solo, starting with “Welcome to My Nightmare” and lasting for several more albums and tours in the 1970s. Wagner also was a co-songwriter for a number of Cooper tunes, such as “Only Women Bleed”, “Department of Youth”, “I Never Cry”, “Go To Hell” and “From the Inside”.

Wagner’s guitar licks were featured on releases such as “Get Your Wings” from Aerosmith, “Destroyer” from Kiss and Peter Gabriel’s self-titled solo debut. Wagner also penned songs for the likes of Meat Loaf and Air Supply. — Reuters