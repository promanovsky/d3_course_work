If your Whole Foods or Sprouts has a whole section devoted to products that have no gluten, it must be bad, right? If your friend says so, you’d better avoid gluten, right? In fact, talk show host Jimmy Kimmel says it seems to him that in Los Angeles, gluten is comparable to Satanism.

But just what is gluten? Kimmel figured plenty of people wouldn’t know -- even people who avoid it. And he was right.

“I don’t know if it’s just here in L.A., but people are very anti-gluten. Which bothers me, because I’m very pro pizza. And you can’t be pro-pizza and anti-gluten,” Kimmel said Monday on his late-night show, “Jimmy Kimmel Live.”

Kimmel said he suspected some people don’t eat gluten because “someone in their yoga class told them not to,” so he sent someone to interview people who avoid gluten just what it was they were steering clear of.

Advertisement

“This is pretty sad, ‘cause I don’t know,” the first man on the show replied.

The second man: “Gluten is in bread. It’s a flour derivative, wheat, things like that …. It’s like bread, pastries …. It’s in those things. Gluten is like a grain, right?”

“It’s the wheat in products in like bread or pastas or rice,” a woman said. “It makes you fat. I haven’t researched it to the fullest. I have a girlfriend from Russia and she got me into it.”

The Russians, Kimmel noted, know gluten and Putin.

Advertisement

The fourth man interviewed didn’t have a better answer.

By the way, gluten is protein found in wheat, barley and rye. People who have celiac disease, estimated to be one in 133 people in the U.S., can get sick from the tiniest bit of it. In addition, many people have less severe problems when they eat gluten. It is, as well, part of what some popular diets proscribe against.

Many people who don’t eat gluten for medical reasons feel conflicted about the wild explosion of gluten-free products on the market, with sales of several billion dollars estimated for 2013.

“When it hits pop culture, it does raise some kind of awareness,” said Deborah Ceizler, chief development officer for the Celiac Foundation. “Some years back, no one had heard of gluten, it wasn’t a buzzword.”

Advertisement

“Our mission is to drive education about celiac disease, which for right now the therapy is the gluten-free diet,” she said.