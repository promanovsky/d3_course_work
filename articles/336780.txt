LOS ANGELES, CA - JUNE 29: Singer Lionel Richie performs onstage during the BET AWARDS '14 at Nokia Theatre L.A. LIVE on June 29, 2014 in Los Angeles, California. (Photo by Kevin Winter/Getty Images for BET)

At the BET Awards on Sunday night, Lionel Richie was honored with the Lifetime Achievement Award. Artists like John Legend, Pharrell Williams and Yolanda Adams took part in the ceremony, performing his songs and presenting the trophy to the soul legend. However, despite the significant chunk of time dedicated to Richie, BET still managed to misspell his name on national television.

Following the caption of "Lionel Ritchie" that appeared onscreen, the misspelled name soon started trending on Twitter, the vast majority of people completely unaware of the error. Despite the gaffe, it was Richie's legacy and words that resonated that evening.

"Soul is a feeling. Not a color," Richie said. "Talent is a God-given gift, and not a category. Out of the box is that magical place where talent -– pure talent goes -– to live and thrive and breathe. And may you never give that up as long as you're in this business."