Justin Bieber followed me on Twitter yesterday. Yes, we're talking about the real Justin Bieber, complete with the shiny blue verification mark.

The reason I need to specify Bieber's authenticity, you see, is that there a lot of fake Bieber accounts out there on Twitter. And many of them are now following me, too.

Apparently the moment the Biebs follows you — and he follows a lot of people, 126,000 in fact — he inadvertently starts a chain reaction among his devoted Beliebers. These fans are so crazy for Justin, and anything that comes into his orbit, that apparently they want to follow anyone he follows on Twitter.

Here, say hello to some of my new friends:

When Bieber followed me, so did over 150 Beliebers.

More than 150 of these alleged Beliebers followed me in the 24 hours after I was followed by the pop singer. I'm now lucky enough to count people like @SwaggyBibs6, @BizzleBabess and @biebermyideal as members of my tribe. I even had a few Beliebers reach out to me to follow them back — and I've never even spoken to Justin. Why? Who knows!

I assume Bieber followed me because I wrote about the selfie app Shots a few weeks back, and Bieber is an investor. I didn't get to interview him, but I'm assuming he saw the story (or rather, his social media people saw the story).

It's hard to tell which of these Belieber accounts are real people and which are simply Bieber bots (and which are Bieber fans who might as well be Bieber bots). Most of them profess some kind of adoration for the tattooed Canadian man-child in their Twitter profile or include a picture of Justin as their avatar or header photo. A few even list the exact date when Bieber followed them as part of their profile description — surely a big day when your entire Twitter purpose is to follow the tweets sent by one person.

I knew Beliebers were crazy for Justin. I just didn't know how much. ♛