The world was shocked on March 17 when L’Wren Scott was found dead in her New York City apartment after what appeared to be a suicide. On March 19, medical examiner’s confirmed she took her own life.

Mick Jagger‘s longtime girlfriend L’Wren Scott sadly ended her life at the young age of 49, and now a medical examiner confirmed that it was a suicide, and that her remains have been claimed.

L’Wren Scott’s Autopsy Rules Death Official Suicide

“The manner of death was determined to be suicide,” Julie Bolcer, a spokeswoman for New York City’s chief medical examiner’s office told CNN. She did not exclose by whom, but L’Wren’s remains have been claimed.

L’Wren’s assistant found the famous designer after receiving a text that from her that said “come by.” She was hanging from a doorknob with a scarf around her neck when her assistant arrived.

Mick Jagger Remembers His ‘Best Friend’

On March 18, Mick broke his silence about the loss of L’Wren, who he had been dating for more than ten years. He shared a beautiful picture of the celebrity designer on his website and Facebook page, with an emotional message.

Mick, 70, wrote:

I am still struggling to understand how my lover and best friend could end her life in this tragic way. We spent many wonderful years together and had made a great life for ourselves. She had great presence and her talent was much admired, not least by me. I have been touched by the tributes that people have paid to her, and also the personal messages of support that I have received. I will never forget her,

Mick

— Emily Longeretta

More L’Wren Scott News: