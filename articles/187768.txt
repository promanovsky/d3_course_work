Nearly 615,000 teen pregnancies occur each year in the United States, but historically low rates reported this week may prove that those “free condom’’ baskets at health center waiting rooms are actually working.

Statistics released this week show historically low trends in teen pregnancy nationwide. The Guttmacher Institute, a reproductive health research and policy advocacy group, released a report this week analyzing public health and birth certificate data from 2010, (the most recent available) to examine teen pregnancy rates by state, age, race, and ethnicity.

In 2010, approximately 6 percent of teens became pregnant, the lowest rate in more than 30 years. That’s 57.4 pregnancies per 1,000 teenage women, a 51 percent drop from the peak rate in 1990. From 2008 to 2010 alone, there was a 15 percent drop.

Advertisement

So what’s the secret to the decline? Perhaps it’s the debut of MTV’s “16 and Pregnant’’ that some think might be pop culture’s answer to birth control. But the report this week attributes the decline in teen pregnancy to an increase in contraceptive use and overall access to health services.

“The decline in the teen pregnancy rate is great news,’’ lead author Kathryn Kost said in a statement with the report’s release. “Other reports had already demonstrated sustained declines in births among teens in the past few years; but now we know that this is due to the fact that fewer teens are becoming pregnant in the first place. It appears that efforts to ensure teens can access the information and contraceptive services they need to prevent unwanted pregnancies are paying off.’’

There’s good news for the Commonwealth and New England, too. Massachusetts had one of the lowest teen pregnancy rates at 37 per 1,000 teenage women. The lowest rates were our neighbors New Hampshire (28 per 1,000) and Vermont (32 per 1,000). On the other end of the spectrum, New Mexico had the highest teen pregnancy rate (80 per 1,000 women), followed by Mississippi (76 per 1,000), and Texas and Arkansas (73 per 1,000).

Teen Sexual Activity —Guttmacher Institute

According to the Massachusetts Youth Risk Behavior Survey, state high school students’ rates of sexual activity have not changed significantly since the 1990s, and pregnancy rates have gone down.

Advertisement

In April, Massachusetts Alliance on Teen Pregnancy announced a historic drop in state teen birth rates, the lowest in more than 20 years, dropping more than 50 percent from the peak in 1989. The advocacy group, like the Guttmacher report, attributes the decline in the teen birth rates to increased contraceptive use, along with more effective information and access.

The Guttmacher report found the use of contraceptives nationwide during first-time sexual encounters for women age 15 to 19 years old has increased from 48 percent in 1982 to 78 percent from 2006 to 2010.

The report attributes the variation in trends across states to the variety of availability and access to comprehensive sex education, knowledge of availability of contraceptive services, as well as cultural differences across states with regard to sexual behavior and childbearing.

National teen pregnancy rates in 2010 —Guttmacher Institute

Between 2005 and 2008, the teenage pregnancy rate decreased in 30 states and increased in 22 states. Then between 2008 and 2010, the teen pregnancy rate declined in all 50 states. Larger trends around fertility behaviors and economics, especially the recession in 2008, study authors wrote, may have played a larger role in the recent decline.

Arizona had the greatest drop (27 percent) in teen pregnancies in those two years, dropping from 82 to 60 pregnancies per 1,000 women. Nevada, Delaware, and Georgia also experienced substantial declines.

Teen pregnancy rates have also declined across racial and ethnic groups. Black and non-Hispanic white teen pregnancy rates declined by the same rate (56 percent from 1990 to 2010), but black teen pregnancy rates are still more than twice the non-Hispanic white rate at 99.5 pregnancies per 1,000 women. New York has the highest black teen pregnancy rate (114 per 1,000), while Georgia had the highest Hispanic teen pregnancy rate (113 per 1,000).

Advertisement

Births resulting from unintended pregnancies (regardless of age) in Massachusetts accounted for $194 million in public costs in 2008, $97 million at the federal level and $97 million at the state level, according to public health data. The Massachusetts Department of Public Health reports that in 2010 (similar to 2009), 35.8 percent of mothers had prenatal care paid for through public programs including Medicaid/MassHealth, Medicare, free care, and other public programs. Massachusetts spends $145 per woman in 2010 on contraceptive services and supplies to decrease rates of unintended pregnancy.

Public health groups in the state are largely in agreement: ignoring the fact that teens are having sex is not as effective as giving them access to contraception.