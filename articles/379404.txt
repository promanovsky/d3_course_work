WASHINGTON, July 15, 2014 /PRNewswire/ -- U.S. News & World Report today released the 25th edition of the annual Best Hospitals rankings, freely available at usnews.com. Best Hospitals includes key information on nearly 5,000 medical centers nationwide. For the first time, the Mayo Clinic in Rochester, Minnesota, claimed the No. 1 spot on the Honor Roll, followed by Massachusetts General Hospital and Johns Hopkins Hospital. Memorial Sloan Kettering Cancer Center is No. 1 in cancer and the Cleveland Clinic is 1st in cardiology & heart surgery.

The Best Hospitals rankings are intended to help patients with life-threatening or rare conditions identify hospitals that excel in treating the most difficult cases. Hospitals are ranked nationally in 16 specialties including cancer, orthopedics, and neurology & neurosurgery. For patients seeking care locally, the U.S. News Best Regional Hospitals highlights high-performing hospitals by state, region and metro area.

In the 2014-15 rankings, only 144 U.S. hospitals performed well enough to be nationally ranked in one or more specialties. Another 608​​ were regional high performers. Just 17 qualified for a spot on the Honor Roll, ranking at or near the top in six or more specialties.

The 2014-15 Honor Roll

1. Mayo Clinic, Rochester, Minnesota

2. Massachusetts General Hospital, Boston

3. Johns Hopkins Hospital, Baltimore

4. Cleveland Clinic

5. UCLA Medical Center, Los Angeles

6. New York-Presbyterian University Hospital of Columbia and Cornell, New York

7. Hospitals of the University of Pennsylvania-Penn Presbyterian, Philadelphia

8. UCSF Medical Center, San Francisco

9. Brigham and Women's Hospital, Boston

10. Northwestern Memorial Hospital, Chicago

11. University of Washington Medical Center, Seattle

12. (tie) Cedars-Sinai Medical Center, Los Angeles

12. (tie) UPMC-University of Pittsburgh Medical Center

14. Duke University Hospital, Durham, North Carolina ​

15. NYU Langone Medical Center, New York

16. Mount Sinai Hospital, New York

17. Barnes-Jewish Hospital/Washington University, St. Louis

U.S. News made several changes to the Best Hospitals rankings methodology this year, including adding new data and greater emphasis on patient safety. Patient safety metrics now account for 10 percent of each hospital's overall score, in most specialties – twice as much as in past years. The role of hospital reputation, as determined through a national survey of medical specialists, diminished by 5 percentage points.

"U.S. News strives to provide patients and their families with the most comprehensive data available on hospitals," said Avery Comarow, U.S. News health rankings editor. "With an estimated 400,000 deaths occurring in hospitals each year from medical errors, measuring safety performance is critical to understanding how well a hospital cares for its patients."

The research organization RTI International, in Research Triangle Park, North Carolina, conducted the physician survey and produced the Best Hospitals methodology and national rankings under contract with U.S. News.

U.S. News first began publishing hospital rankings in 1990 as part of an expanding group of consumer advice products. Over the past 25 years, the Best Hospitals rankings have guided millions of patients and their families to high-quality hospital care when they need it most.

Risk-adjusted survival rates, adequacy of nurse staffing and patient volume are among the array of data points on each ranked hospital that are freely available at www.usnews.com/best-hospitals. For more information, please see the Best Hospitals FAQ. The rankings will be published in the U.S. News "Best Hospitals 2015" guidebook (ISBN 978-1-931469-64-7), which will be available in August.

In an exclusive arrangement, the launch of this edition of Best Hospitals is being sponsored by Fidelity Investments.

U.S. News 2014-15 Best Hospitals Rankings

For the full list of national rankings by specialty, visit www.usnews.com/best-hospitals.

Top-Ranked Hospitals in Cancer

Memorial Sloan Kettering Cancer Center, New York University of Texas MD Anderson Cancer Center, Houston Mayo Clinic, Rochester, Minnesota Dana-Farber/Brigham and Women's Cancer Center, Boston Johns Hopkins Hospital, Baltimore University of Washington Medical Center, Seattle Massachusetts General Hospital, Boston UCSF Medical Center, San Francisco UCLA Medical Center, Los Angeles Stanford Hospital and Clinics, Stanford, California

Top-Ranked Hospitals in Cardiology & Heart Surgery

Cleveland Clinic Mayo Clinic, Rochester, Minnesota New York-Presbyterian University Hospital of Columbia and Cornell, New York Duke University Hospital, Durham, North Carolina Brigham and Women's Hospital, Boston Massachusetts General Hospital, Boston Hospitals of the University of Pennsylvania-Penn Presbyterian, Philadelphia Cedars-Sinai Medical Center, Los Angeles St. Francis Hospital, Roslyn, New York Mount Sinai Hospital, New York

Top-Ranked Hospitals in Orthopedics

1. Hospital for Special Surgery, New York

2. Mayo Clinic, Rochester, Minnesota

3. Cleveland Clinic

4. Hospital for Joint Diseases, NYU Langone Medical Center, New York

5. Massachusetts General Hospital, Boston

6. Rush University Medical Center, Chicago

7. Cedars-Sinai Medical Center, Los Angeles

8. Thomas Jefferson University Hospital, Philadelphia

9. (tie) Beaumont Hospital, Royal Oak, Michigan

9. (tie) Duke University Hospital, Durham, North Carolina

About U.S. News & World Report

U.S. News & World Report is a multi-platform publisher of news and analysis, which includes the digital-only U.S. News Weekly magazine, www.usnews.com and www.rankingsandreviews.com. Focusing on Health, Personal Finance, Education, Travel, Cars and Public Service/Opinion, U.S. News has earned a reputation as the leading provider of service news and information that improves the quality of life of its readers. U.S. News & World Report's signature franchise includes its News You Can Use® brand of journalism and its annual "Best" series of consumer Web guides and publications that include rankings of colleges, graduate schools, hospitals, mutual funds, health plans and more.

Logo - http://photos.prnewswire.com/prnh/20100603/PH13717LOGO

SOURCE U.S. News & World Report