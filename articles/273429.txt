Tree-hugging koalas sprawled across low-hanging branches are desperately trying to cool down by using the dramatically lower temperatures inside trees, researchers have discovered.



With Australian heatwaves increasing in length and intensity, koalas have developed a new behaviour to deal with the heat, acting in much the same way as a human fed up with a scorching day – sprawling across a cold surface.

A study of a koala population conducted on French Island, near Melbourne, found that significantly cooler temperatures in tree trunks and thick branches during a heatwave provide relief to the animal which, unlike a lot of tree-dwelling mammals, does not use hollows or dens for shelter.

“They’re just stuck out on the tree all the time so when hot weather comes they’re completely exposed to it,” Dr Michael Kearney from the University of Melbourne’s Zoology department told Guardian Australia.

“When a heatwave comes the most effective way for the koala to lose heat is through evaporation. They don’t sweat but they can pant and lick their fur,” he said. However, in times of high heat and low rainfall, koalas, which don’t have sweat glands, cannot sustain the evaporation.

The team witnessed French Island koalas seeking out lower, cooler branches and lying listless and flat along the surface. This is not its usual posture.

“I sort of see it as ‘Oh I’m so hot, I’m going to lie down, but there’s more reason to it than that,” said Kearney, a co-author of the report.

The overheated koala goes out on a limb to cool down. Photograph: Steve Griffiths Photograph: Steve Griffiths/supplied

“The fur on their tummy is quite a lot thinner than the fur on their backs, so they’re pushing that fur and that part of their body as much against the tree as possible.



“Any way that they can lose heat that doesn’t involve losing water is going to be a huge advantage to them. Dumping heat into the tree is one of those methods.”

The team of researchers from the university’s department of zoology, who were studying the effect of climate change on Australian animals, noticed the koalas’ unusual behaviour when the temperature increased. They used thermal imaging technology to further examine their observations.

“Tree temperature profiles showed strong congruence with observed koala behaviour patterns,” read the group’s research. “During hot weather average tree surface temperatures of the four dominant tree species at the site were significantly lower than local air temperatures.”

Surprisingly, a species of wattle was the coolest of all the studied trees. The act of leaving the edible gums and moving to a wattle tree had been observed before, but it was only now that the scientists had a better idea of why.

“It was [previously] thought that maybe they’re shadier or something like that, but in fact a big part of that behaviour in using non-food trees may be because they have cooler trunks,” Kearney said.

The wattle tree had base and mid-trunk temperatures that were up to 8.9C cooler than the air temperature.

“There are a couple of reasons why might be,” he said. “One is that a big tree has a lot of thermal inertia – they heat up and cool down more slowly that the ambient conditions. When it’s the peak heat of the day in terms of the air temperature the tree may not have caught up yet.

The koala's thick fur makes it difficult to stay cool in hot weather. Photo: Colin Briscoe Photograph: Steve Griffiths/supplied

“Perhaps what’s even more important is that trees are drawing up cold water from underground. At least where we studied the koalas on French Island, it’s on average cold throughout the year, so the temperature underground is quite cool.”

Kearney predicts koalas will use this behaviour more and more as climate change leads to longer and more extreme heatwaves, and further research is needed on how temperatures will affect the geographical distribution of populations of koalas and other animals. If it’s too hot then no behavioural method will work.

“There are times when it gets far too hot for a koala in certain parts of Australia and that’s a constraint on where they can live,” he said. “That behaviour, that resource and micro climate in those trees are likely to be really important for a whole lot of other tree-dwelling species. It’s not just the koala that would use this behaviour.”