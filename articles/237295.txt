Researchers of a new study found that most women who undergo double mastectomy don't really need it.

Breast cancer is the second most common cancer among women in the United States. After a breast cancer diagnosis, it is very common for women to undergo a mastectomy surgery. Many such women also opt for double mastectomies, called contralateral prophylactic mastectomy (CPM). It is a more aggressive form of the surgery but is often requested by patients.

According to the findings of a new study, most of these surgeries are unnecessary. Explaining why, researchers said that most women opt for double mastectomy due to fear of recurrence of the disease. However, what women don't know is that 70 percent of them are at a very low risk of developing cancer in the healthy breast.

"Women appear to be using worry over cancer recurrence to choose contralateral prophylactic mastectomy. This does not make sense, because having a non-affected breast removed will not reduce the risk of recurrence in the affected breast," said Sarah Hawley, associate professor of internal medicine at the U-M Medical School in a press statement.

The study was conducted on 1,447 women who were diagnosed and treated for breast cancer. None of them had any cases of recurrence. Despite this, researchers found 8 percent of the participants had opted for a double mastectomy while another 18 percent were considering getting one.

Researchers noted that though 75 percent of the patients admitting to worrying about recurrence, more than 70 percent of the women were at minimal risk of developing cancer in their healthy breast.

Hawley and her team clarified that those women with a family history of breast or ovarian cancer or with a positive genetic test for mutations in the BRCA1 or BRCA2 may consider contralateral prophylactic mastectomy because they are at a higher risk of a new cancer developing in the healthy breast. For other women, the chances or recurrence are highly unlikely.

Previous studies have also highlighted that the fear of recurrence has driven women to opt for double mastectomy. According to a study published in the journal Plastic Reconstructive Surgery, contralateral prophylactic mastectomy is on the rise. Estimates increased from 39 to 207 per 1000 women between 1998 and 2008.

The surgery became even more popular when Angelina Jolie revealed that she had undergone a double mastectomy after learning that she had a "faulty" gene that put her at an 87 percent higher risk of breast cancer.

According to a CBSN News report, Jolie admitted she had three mastectomy-related procedures over three months last year, the last one being on April 27, 2013. They included two operations - the second of which was breast reconstruction.

"I am writing about it now because I hope that other women can benefit from my experience," the actress said. "Cancer is still a word that strikes fear into people's hearts, producing a deep sense of powerlessness. But today it is possible to find out through a blood test whether you are highly susceptible to breast and ovarian cancer, and then take action."

Very recently, "Dancing with the Stars" co-host Samantha Harris also tweeted about having undergone a double mastectomy after find a lump in her right breast in a routine self-examination last fall, PEOPLE magazine reported.

The current study also found that women with higher education levels and those that had undergone MRI tests before surgery were more likely to choose the more aggressive form of the surgery.

Not only is double mastectomy more dangerous, it also has more complication and longer recovery duration. Sometimes, such surgeries are followed by chemotherapy or radiation therapy to further reduce the risk of cancer.

According to the American Cancer Society, 235,030 Americans will be diagnosed with breast cancer this year and 40,430 will die from the disease.

Surgeons are increasingly uncomfortable with the idea of performing extensive surgeries that they consider unnecessary, but are under pressure to acquiesce to patients' requests for these. In the U.S., almost all insurance providers cover CPM for breast cancer independent of an individual patient's risk factors due to the Women's Health and Cancer Rights Act of 1998. Hence, surgeons who refuse such treatment to women at average risk may lose valuable patients to others who are prepared to perform it.

Findings of the current study were published online in the journal JAMA Surgery. The study was funded by the National Cancer Institute.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.