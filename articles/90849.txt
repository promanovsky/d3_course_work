Washington: Buoyed by recovery in advanced economies, including the US, the International Monetary Fund today said global activity strengthened during the second half of 2013 and is expected to improve further in 2014-15.

A major impulse to global growth has come from the US, whose economy grew at 3.25 percent in the second half of 2013 stronger than expected, IMF said in its latest edition of the World Economic Outlook.

In the stressed euro area economies, however, growth is projected to remain weak and fragile as high debt and financial fragmentation hold back domestic demand, it said.

In Japan, where Prime Minister Shinzo Abe has introduced steps to revive the economy, fiscal consolidation in 2014?15 is projected to result in some growth moderation.

Growth in emerging market economies is projected to pick- up only modestly, it said.

These economies are adjusting to a more difficult external financial environment in which international investors are more sensitive to policy weakness and vulnerabilities given prospects for better growth and monetary policy normalisation in some advanced economies, the report said.

Olivier Blanchard, IMF's Economic Counsellor, said the recovery that started some six months ago to take hold in advanced economies is becoming broader.

"Fiscal consolidation is slowing and investors are less worried about debt sustainability," he said.

"Banks are gradually becoming stronger. Although we are far short of a full recovery, the normalisation of monetary policy -- both conventional and unconventional -- is now on the agenda," he said.

Observing that acute risks have decreased, he said they have, however, not disappeared.

"In the US, the recovery seems solidly grounded. In Japan, Abenomics still needs to translate into stronger domestic private demand for the recovery to be sustained. Adjustment in the south of Europe cannot be taken for granted, especially if euro-wide inflation is low," Blanchard said.

Although the evidence is not yet clear, potential growth in many emerging economies also appears to have decreased.

In countries such as China, this may be in part a desirable by-product of a more balanced growth. In others, there is clearly scope for some structural reforms to improve the outcome, he said.

The report said the global recovery is still fragile despite improved prospects, and significant downside risks -- both old and new -- remain.

"Recently, some new geopolitical risks have emerged. On old risks, those related to emerging market economies have increased with the changing external environment," it added.