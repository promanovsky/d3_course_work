Here's some adorable news. A photo shows a rare set of twins holding hands just minutes after entering the world together.

The newborn girls are monoamniotic or "mono-mono" identical twins, which means that the two fetuses share the same amniotic sack.

The identical twins were delivered at Akron General Medical Center in Akron, Ohio, on Friday.

According to the Today Show, the hospital says the sisters grasped each other as the doctors held them over a sheet following a cesarean section.

The girls were born healthy at 33 weeks but will remain in the hospital for monitoring for up to a month, according to the Associated Press.

"I didn't think they would come out and instantly holding hands," the twins' mother told ABC News. "It was overwhelming. I can't even put into words. There wasn't a dry eye in the whole OR."