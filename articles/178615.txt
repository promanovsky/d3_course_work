Brad Pitt and Angelina Jolie lit up the big screen and the tabloids in 2005's "Mr. and Mrs. Smith," and it looks like they're set to stage a repeat.

The Oscar-nominated pair is reportedly inking a deal to star in another movie together based on a script Jolie wrote, Deadline.com reports. As of now, there are no official details about the film, such as what the plot might be or which lucky studio may nab what will undoubtedly be one of the few non-superhero, non-sequel tentpole movies of the year.

Some insiders speculate the movie is a relationship drama, according to The Hollywood Reporter. If that's true, it could hail from a script Jolie wrote several years ago about a couple whose failing marriage prompts them to take a vacation as a last-ditch effort to fix their problems.

If the movie comes to fruition, it will be the first time Jolie and Pitt appear onscreen together since "Mr. and Mrs. Smith," which grossed $186 million in North America alone. It'll also mark another behind-the-scenes project for Jolie, who wrote and directed 2007's "In the Land of Blood and Honey" and helmed this December's "Unbroken."