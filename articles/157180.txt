Johnny Depp plays Will Caster, who becomes an out-of-control, digitized version of himself in “Transcendence.” (Warner Bros.)

‘Transcendence’ an AI Film That Does Not Transcend

The machines are taking over. Again. This endless futzing with artificial intelligence while rain forests burn and oceans die—isn’t it Nero fiddling while Rome burns?

It only leads to one outcome: The sole survivor of a human-ransacked Earth will be a little robot named Wall-E.

We know this. But, regardless, “Transcendence” is another movie about how technology is going to take over, and how we have to make sure that it takes over nicely. Notice how it almost never does?

In the near future, the information superhighway has gone kaput. As the narrator (Paul Bettany) confides, “Earth feels smaller without the Internet.”

We flash back from the near future to the present, to witness how Dr. Will Caster (Johnny Depp), had striven to make computers sentient.

Terrified terrorist group R.I.F.T. (Revolutionary Independence From Technology) shoots Caster with an irradiated bullet. Are they really the bad guys?

As Will’s on his deathbed, wife Evelyn (Rebecca Hall) and colleague Max (Paul Bettany) upload his consciousness into PINN (Physically Independent Neural Network) and flee the premises with a digitized Dr. Caster. Caster-in-a-can, if you will.

They set up shop in Southern California, in a super-expensive, solar-powered compound.

Once Caster-in-a-can gets into the Internet’s bloodstream, he starts sucking up info like a nine-headed cyber-Hydra. He wants to “expand, evolve, influence,” voraciously. The actual Dr. Caster was a mild man.

Caster-in-a-can develops, and also manifests himself in, a scary new form of nanotechnology.

After Max gets kidnapped by R.I.F.T., Caster’s former associate Joseph Tagger (Morgan Freeman) and an FBI agent (Cillian Murphy) start looking into the matter.

Evelyn is at first happy to have hubby as a genie-in-a-lamp. Make that genie-in-a-chip. Nah, let’s stick with Caster-in-a-can.

Eventually, though, she’s maximally creeped out as her “husband” manages, through the lightning-fast self-replicating nanotechnology, to figure out how to get out of the lamp-chip-can and into humans.

As he says of these nanoteched humans, “We’re not going to fight them, we’re going to transcend them.” Now you understand the title.

Caster-in-a-can does figure out ways to use technology for the good of mankind, but—surprise, surprise—can’t tell facts from emotions. Doesn’t it always come down to this?

Johnny Depp’s back from a very long sojourn in Jack Sparrow-land. So it’s understandable that the nanotechnology of Jack Sparrow appears to have taken over Depp’s entire system. He always speaks, nowadays, in ever-so-slightly British-infused Sparrow cadences.

Acting is about being yourself in various foreign situations. It appears that Depp can’t be himself anymore because he’s no longer Johnny. He’s Jack. Jack Depp. Maybe it’ll be his new stage name.

Hall has to play love-dovey with Caster-in-a-can’s onscreen image. That’s a significant acting challenge. She pulls it off rather well.

When it comes to the kind of stock-in-trade, wise, weary elder role of Joseph Tagger, which Morgan Freeman can do falling off a log, they should just do a good makeup job on a stand-in. Let the stand-in do the acting, pay him a little more, and let Morgan go home and put his feet up. Sort of how the old classical painters had minions who did the actual painting.

Wally Pfister’s directing. In a word—too slow. OK, that’s two words.

Probably the best-ever depictions of artificial intelligence in the movies were the cyborgs and androids of “Blade Runner.” Rent that. Stay home. Transcend “Transcendence” by watching “Blade Runner” on Blu-ray.

‘Transcendence’

Director: Wally Pfister

Starring: Johnny Depp, Rebecca Hall, Paul Bettany, Morgan Freeman, Cillian Murphy, Kate Mara, Cole Hauser

Run Time: 1 hour, 59 minutes

Rated PG-13

Release Date: April 18

2.5 stars out of 5