Anyone excited enough by the fact Amazon have acquired HBO shows to add to their streaming roster has probably already indulged on The Sopranos and The Wire. But they’ll probably convince a few of their friends to invest in the service just on the strength of those two shows.

That’s how powerful the HBO brand is in television, and nowadays, when we say television, we’re not just talking about Sunday nights at 9pm – online streaming is where the medium is going. Amazon’s deal with the cable network is a big one; it adds some of TV’s finest hours.

“HBO is gold-plated content that will make the Amazon Prime service much more attractive to potential members,” said Paul Sweeney, an analyst for Bloomberg Industries. “This is a seminal deal for the online-video business. Having a blue-chip programmer such as HBO sign a deal with Amazon Prime validates the online video space in many investors’ eyes.”

Amazon now has a vital weapon in the fight for an already crowded content streaming space, with Netflix – their main rivals – looking on in envy. Kevin Spacey starring as Frank Underwood in House of Cards got people talking, and rightly so, but attracting a name like HBO will serve Amazon when they start commissioning new, original content of their own.

Included in the deal are newer HBO shows, like Girls and The Newsroom, which sill start streaming on Amazon Instant Video three years after they debut on the cable channel. Deadwood, Rome, Eastbound & Down, Enlightened and Flight of the Conchords are also in there, alongside miniseries, films and documentaries, including Band of Brothers, Game Change and When the Levees Broke. You’ll also get comedy specials and select seasons of such current series as True Blood and Boardwalk Empire.

More: Well done internet, you've dealt with House of Cards spoilers well