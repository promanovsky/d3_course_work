The plane that crashed in San Francisco in July of 2013 could have been avoided.

According to the Los Angeles Times, a federal safety panel concluded on Tuesday that the pilots flying the Asiana Airline jet mishandled their landing approach and inadvertently shut off a speed control system that could have prevented the crash.

The National Transportation Saftey Board also blamed the crash on the pilot's failure to monitor their airspeed and altitude and their late decision to abort the landing. The crash resulted from the plane flying in too slow and too low to touch down safely.

The Aisana Flight 214 was en route from Seoul, South Korea. Its tail hit a sea wall, sending the plane crashing into the runway while attempting to land at San Francisco International Airport. Three people died and 187 were injured; 49 were seriously injured. The three who died are suspected of not wearing their seat belts and one of them survived, only to be run over by rescue vehicles in the chaos of the crash.

According to KPCC the pilots training was also faulted. The flight crew "over-relied on automated systems that they did not fully understand," said Chris Hart, the NTSB's acting chairman.

"In their efforts to compensate for the unreliability of human performance, the designers of automated control systems have unwittingly created opportunities for new error types that can be even more serious than those they were seeking to avoid," Hart said.

In documents made public by the safety board, Asiana Airlines said that the plane should have been designed so that the automatic throttle would maintain the proper speed after the pilot put it on "hold mode," although they did acknowledge pilot error as well.

Boeing, the plane's manufacturer, had been warned about the problem by U.S. and European aviation regulators prior to the incident.

"Asiana has a point," said John Cox, a former airline pilot and aviation safety consultant, "but this is not the first time it has happened. Any of these highly automated airplanes have these conditions that require special training and pilot awareness. ... This is something that has been known for many years and trained for many years."

There is video of the crash.

