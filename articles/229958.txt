A new survey has shown that couples who both have high level of cholesterol in their blood take longer to achieve a pregnancy.

The survey also found it proved harder for couples to become parents when the woman had high cholesterol but the man did not.

Why cholesterol should affect fertility remains unclear, but the fatty substance is closely involved in the manufacture of sex hormones such as testosterone and oestrogen.

Lead scientist from the National Institute of Child Health and Human Development Dr Enrique Schisterman said: "We've long known that high cholesterol levels increase the risk for heart disease.

“From our data, it would appear that high cholesterol levels not only increase the risk for cardiovascular disease, but also reduce couples' chances of pregnancy."

"In addition to safeguarding their health, our results suggest that couples wishing to achieve pregnancy could improve their chances by first ensuring that their cholesterol levels are in an acceptable range."

The study involved 501 US couples taking part in an investigation into links between fertility and environmental chemicals and lifestyle.

Volunteers provided blood samples which were tested for total "free" cholesterol and did not distinguish between different "good" and "bad" sub-types.

Of the two main forms of cholesterol, high density lipoprotein (HDL) is known to benefit the heart while low density lipoprotein (LDL) has a harmful effect.

Couples' chances of conceiving over up to one year were estimated using a statistical measure called the fecundability odds ratio (FOR).

Generally, higher cholesterol levels were associated with longer times to pregnancy and lower FORs.

Compared with partners who both had cholesterol in the normal range, couples in which the woman's cholesterol was raised but the man's was not took longer to achieve pregnancy.

Those in which both partners had high cholesterol levels took the longest of all.

The research took account of differences in race, age, body mass index (BMI) and education.

It was published in the Journal of Clinical Endocrinology and Metabolism.