It's no secret that Miley Cyrus was simply devastated after a severe allergic reaction landed her in the hospital, forcing her to postpone the remainder of her U.S. tour. The 21-year-old singer made it clear how upset she was to have to cancel on her fans, but in a new interview with Ryan Seacrest, she also revealed how frightened she was.

"It was so scary … I had basically been poisoning myself with something I didn’t know I was really scary allergic to," she told Seacrest on his radio show on Monday, April 28. "I was on this medicine for five days and everything was all good and on the sixth day, I just woke up … I couldn't breath and my skin -- everything -- was going insane."

Cyrus suffered a reaction to Cephalexin and was in the hospital for nine days before she was released on April 24.

The "We Can't Stop" singer kept fans abreast of her recovery and tried to abate her boredom with plenty of selfies.

“I was crying because I was so bored,” she told Seacrest. "I’m laying here hooked to all these machines. Every two hours, some woman comes in and pokes me with a needle, just miserable."

Cyrus revealed that she's still resting up at home, and she's feeling much better.

“I should be all good," she said. "I’ve been really good."