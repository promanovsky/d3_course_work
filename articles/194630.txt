With limited exceptions, nonessential air travelers will be required to reserve a three-night stay in a government-authorized hotel at their own expense before they depart for Canada.

Grace Community Church announced it would postpone its annual Shepherds’ Conference, which it planned to host indoors in Los Angeles.

The private Catholic university’s president said contact tracing efforts have linked much of the surge to parties and disregard for health and safety measures.

It’s hard to tell if someone is six feet away when you can’t see, and masks make it difficult to hear and understand people.

“What he did was very bad,” said “Buffy” actress Michelle Trachtenberg of showrunner Joss Whedon’s alleged misconduct. “But we win. By surviving!”

Dave Holmes, former host of MTV’s “Total Request Live,” reflects on the media’s mistreatment of Britney Spears and the hope he finds in a new generation’s voices.

“Welcome to the rebellion,” actress Gina Carano said Friday as she revealed she’ll develop and produce a film for conservative pundit Ben Shapiro’s Daily Wire.

The controversy that erupted around the series this week drew in contestant Rachael Kirkconnell, host Chris Harrison and former “Bachelorette” Rachel Lindsay.

Welcome back to our horse racing newsletter, as we preview the only Kentucky Derby prep at Golden Gate Fields.

In Washington, Trump’s trial began and Biden considered new stimulus. In California, vaccine distribution slowed amid shortages.

We’re a year into the pandemic, and San Gabriel Valley restaurants need our business

Newsletter Must-read stories from the L.A. Times

Get all the day's most vital news with our Today's Headlines newsletter, sent every weekday morning.

Enter email address Sign Me Up

You may occasionally receive promotional content from the Los Angeles Times.