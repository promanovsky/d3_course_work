Rocker Courtney Love is heading back to Tv to guest star on U.S. series Sons Of Anarchy.

The Hole frontwoman will make her dramatic Tv acting debut as a pre-school teacher of Charlie Hunnam character's son in the final season of the popular show, according to TvLine.com.

News of the singer's new gig comes just weeks after she unveiled plans to take a hiatus from music to focus on reviving her acting career. She previously scored a Golden Globe nomination for her performance as Hustler boss Larry Flynt's junkie wife Althea in 1996 movie The People vs. Larry Flynt and tried her hand at Tv in a 2009 episode of comedy Nice Brothers.

Love isn't the only musician to land a small role in Sons of Anarchy - Marilyn Manson has also filmed an episode as a prisoner for the series' upcoming seventh season, which will premiere in the U.S. this autumn (14).