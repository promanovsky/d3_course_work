J. Michael Pearson, the chief executive of Canadian drug giant Valeant Pharmaceuticals, is known for being cheap — and he did little Wednesday to dispel that reputation, upping the cash portion of Valeant’s bid for Botox maker Allergan by just 8 percent.

The hardball play seemed to turn off investors. Shares of Allergan fell 5.4 percent, to $156.12, while those of Valeant fell by 2.3 percent, to $126.95.

In the improved offer, explained in detail over a nearly-four hour presentation, Pearson said Valeant would not overpay for Allergan — the biggest prey the acquisitive Valeant has set its sights on.

But he indicated that the company might go higher.

“We are still willing to sit down with management and the board to negotiate a deal,” the CEO said, “but we’re not going to increase our offering until we have that chance to sit down.”

Valeant, whose big roll-up strategy has the backing of billionaire investor Bill Ackman, is offering $58.30 in cash per share, plus the same portion of Valeant stock as before. The deal is now valued at $49 billion — $5 billion higher than the first offer.

The total value is around $166 per share, up from the roughly $153 it first proposed last month. It is also roughly 50 percent higher than where Allergan traded when Ackman started amassing his stake — at $116.

Allergan rebuffed the initial bid and said Wednesday that it would “review” the new one.

One drawback was that the price was below the $180-to-$200 per share that investors surveyed by JPMorgan last week said they believed Valeant would need to pay for Allergan.

But Valeant estimated that the final price of its new offer will come in above $200 per share in the end, because the stock portion will rise significantly before the deal closes, which Valeant now believes might not occur until next year.

“Shareholders want this to occur, but they want it at a higher price,” Pearson said during the webcast.

Ackman, whose Pershing Square hedge fund owns 9.7 percent of Allergan, has asked the Securities and Exchange Commission for a clearance for a shareholder vote. Pearson said he expected that would occur before June 30.

“One alternative is to make a tender offer for the company,” Pearson said.

Now that the stock is trading at a discount to the offer, shareholders could be encouraged to snap it up, said one investor.

Valeant on Wednesday also said it would invest up to $400 million in Allergan’s Darpin eye treatment drug and continue developing it, adding an additional $25 per share to the total value of the deal.

“We’re committed to R&D, but we’re focused on outputs not inputs,” Pearson said in his presentation, responding to the criticisms that the company’s emphasis on costs will cut R&D at Allergan.

“We’re not slashing and burning R&D,” he said, “and we have an amazing pipeline.”

Valeant managers from around the world gave detailed presentations refuting Allergan’s assertions that the company’s business model can’t survive without acquisitions.

Valeant has a healthy late stage pipeline of new products and an overall organic growth rate of between 7 and 8 percent, the manager said.