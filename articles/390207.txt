'Wish I Was Here' N.Y. Premiere: Zach Braff, Cast on Kickstarter Benefits, Character Draws

Kate Hudson and Josh Gad talked to THR about how they connected with their roles while Joey King explained how she supported the project before she joined the cast.

Kickstarter supporters who donated money to Zach Braff's new movie Wish I Was Here got T-shirts, posters, advance screenings and even the chance to be extras.

But one backer ended up starring in the movie.

Joey King, who plays Braff's character's daughter, Grace, told The Hollywood Reporter at Monday night's New York premiere of Wish I Was Here that she donated money to the film on Kickstarter before she was even attached to the family-centric film as a castmember.

PHOTOS Hollywood on Broadway: Denzel Washington, Daniel Craig Among 2013-14 Season's Stars

"I was a backer of this film before I was even a thought of being in it," she said. "I went on Kickstarter and I donated. I was one of the 47,000 [supporters]."

King explained that she's known Braff for five years and previously worked with him on Oz the Great and Powerful.

Another one of Braff's longtime friends, his Scrubs co-star Donald Faison, who has a cameo in Wish I Was Here, said that his involvement with the project came about very casually and he was a cheerleader for Braff during the many years it took the actor-director to make his Garden State follow-up.

Faison added that while he supports the idea of people raising money for creative endeavors through Kickstarter, he wishes supporters got a bit more for their early support.

PHOTOS The Scene at Sundance Film Festival 2014

"I wish that the fans or the people that invested got more back … it would be cool, for me, personally speaking, if it was more like a stock option where you put your money down and there's a chance that you get your money back and then some," he explained to THR ahead of the screening on Manhattan's Upper West Side. "That would be awesome. That would make it even better."

Braff insists that raising money through Kickstarter was the only way he was able to make the movie he wanted to make, noting that even the independent financing entities he talked to wouldn't offer him the same freedom.

"None of the financier deals that we were presented were anywhere near giving me final cut, letting me shoot where I wanted. [They were] pitching, believe it or not considering my cast, pitching higher profile, bigger box-office stars. That was just ludicrous to me," Braff told THR. "I had been blessed by having a financier who made his fortune in the mortgage business the first time around [in making Garden State], so he didn't give me any of that. He supported me and said, 'I want you to go make the movie.' This time, when I couldn't get that, I went to crowdfunding, and my fans rallied around it and I made it with them, for them. But I was surprised and humbled that the success of Garden State didn't give me the same access to final cut or to casting who I want or to shooting in L.A."

Meanwhile, Kate Hudson, who stars opposite Braff as his character's wife, Sarah, said that the movie's emphasis on family and her role as a struggling working mother both appealed to her.

PHOTOS Sundance 2014: Exclusive Portraits of Aaron Paul, Kristen Stewart, Keira Knightley, Zoe Saldana and More in Park City

"Family's really important to me. I think that in my life, that's always been a rock-solid foundation as well as having a tumultuous relationship in my family as well. … it's like when Joey King['s character] says, 'You don't have to believe in God but can you believe in family?" … For me, that resonates with me in terms of family being where my faith lies," Hudson said. "I also feel like as a mom, working mom, who's pulling her weight and the challenges that come with that — it's not necessarily about relating because I feel like it would be irresponsible of me because I've been very blessed and lucky — but I'm a big advocate for women and getting women in the workplace and getting them more rights in the workplace as well. I loved playing that character. I think there's a lot of women who will really relate to Sarah's struggles and difficulties."

Josh Gad, who plays Braff's character's cynical, closed-off brother, said he was drawn to finding ways to connect to a character who's different from him.

"It's one of those things that attracts you as an actor is playing that which you don't know," he said. "Noah is one of those guys that I don't on the page relate to, and it's in the discovery of kind of who this guy is and picking him apart that you find where you do overlap and where that vulnerability is."

Wish I Was Here opens in select theaters on Friday. Watch the trailer for the film below.