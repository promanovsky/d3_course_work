Researchers have said that seeing people e-cigarettes increases the urge to smoke among regular combustible cigarettes users.

Study author Andrea King, PhD, professor of psychiatry and behavioral neuroscience at the University of Chicago, said e-cigarette use has increased dramatically over the past few years, so observations and passive exposure will no doubt increase as well.

To investigate, King and her team recruited 60 young adult smokers. Participants in the study were told they were being tested on their responses to a variety of social interactions.

They were paired with an actor, pretending to be a participant, who would smoke an e-cigarette or a regular cigarette during a conversation. The actual study subjects were measured for their urge to smoke at multiple points before and after this interaction.

The team found that seeing e-cigarette use significantly increased the observer's desire to smoke both regular and e-cigarettes. The increases in desire to smoke a regular cigarette after observing e-cigarette use were as strong as after observing regular cigarette use.

However, observing regular cigarette use did not increase participants' desire to smoke an e-cigarette. As a control, actors also drank from a bottle of water while engaging in conversation with the participant to mimick hand-to-mouth behavior. No increase in desire for either regular or e-cigarettes were seen in this scenario.

The study has been published in the journal Tobacco Control.