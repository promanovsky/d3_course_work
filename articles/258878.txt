Candy and cookie companies do not want their brand names to flavor nicotine in e-cigarettes.

According to the Associated Press, General Mills, Girl Scouts USA, and Tootsie Roll Industries Inc. have reportedly ordered liquid nicotine companies to not use their brands' signature flavors in e-cigarettes, for fear they might lure children into developing a smoking habit.

Companies have subsequently started to implement intellectual property rights on their inventory to ensure their products won't be used improperly.

A board member of the American E-liquid Manufacturing Standards Association told AP the issue has been around for quite some time.

"It's the age-old problem with an emerging market," Linc Williams said.

A rise in tobacco users since 2006 has given popular cigarette makers R.J. Reynolds Tobacco and Philip Morris USA legal battles with websites that sold e-cigarettes and reportedly took advantage of their Camel and Marlboro brand names and graphics.

Williams, also an executive at nicotine producer NicVape, said he doesn't see the issue going away anytime soon.

"As companies going through their maturity process of going from being a wild entrepreneur to starting to establish real corporate ethics and product stewardship, it's something that we're going to continue to see," Williams said. "Unfortunately it's not going to change unless companies come in and assert their intellectual property."

Tootsie Roll's president and chief operating officer said the company's main concern is protecting the brand.

"We're family oriented. A lot of kids eat our products, we have many adults also, but our big concern is we have to protect the trademark," Ellen Gordon said. "When you have well-known trademarks, one of your responsibilities is to protect [them] because it's been such a big investment over the years."

A spokeswoman for Girl Scouts said using the non-profit organization's Thin Mint flavor disgracefully misrepresents the audience it intends to serve.

"Using the Thin Mint name - which is synonymous with Girl Scouts and everything we do to enrich the lives of girls - to market e-cigarettes to youth is deceitful and shameless," Kelly Parisi said.

The brand flavors include Thin Mint, Tootsie Roll and Cinnamon Toast Crunch.

According to AP, R. J. Reynolds, and Philip Morris have released e-cigarettes without the flavors in question.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.