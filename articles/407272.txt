Five months after its launch in the United States, where it’s signed up nearly 700,000 paying subscribers, the WWE Network will start streaming overseas.

WWE will introduce the digital channel in 170 countries starting Aug. 12. Those include major territories like Australia, Canada, Latin America, Mexico, New Zealand, the Nordic countries, Russia, Singapore and Spain. WWE’s second-largest market, the United Kingdom, will follow in October.

The additional countries are key for WWE as it looks to grow the subscription numbers for the network, considered the cornerstone of WWE’s future as a major source of revenue and distribution platform for its shows and pay-per-views, including “WrestleMania.”

Through the end of the second quarter, which wrapped June 30, WWE counted nearly 700,000 paying subscribers to the channel, which is available as an app on mobile devices and platforms like Apple TV, Roku, Amazon’s Kindle Fire, and the Xbox and PlayStation videogame consoles. Subscribers pay $10 to access WWE’s vault of programming, original series, as well its monthly PPVs. The network generated $19.4 million in revenue during the quarter.

While WWE has been criticized for not being able to grow past 700,000 so far, just getting to that number is considered a feat by others, given the short amount of time the WWE Network has been available (it launched Feb. 24), and the churn rate subscription-based channels must typically contend with.

Just as pay cablers like HBO see subscriptions rise and fall depending on when content is available, WWE has seen a similar effect around its live PPVs, considered the biggest draw to the network so far among the mostly 18-34-year-old men who make up most of its demo, a sweet spot for advertisers. Users watch the network on more than two devices on average, with 91% of subscribers accessing the network at least once a week. Internal studies show that 51% of customers are extremely satisfied, while 39% are somewhat satisfied with the service.

A majority of its subscribers signed up to watch “WrestleMania,” on April 6, while another 161,000 signed up after that through the end of the quarter. But that figure drops to 33,000 when accounting for the 128,000 that dropped the service during that same period. It had 667,000 paid subscribers following the broadcast of “WrestleMania.”

PPVs typically cost from $45 to $60, and the network has impacted PPV buys on cable and satellite providers.

“WrestleMania” saw PPV buys decline from 1 million in 2013 to 690,000 this year because the channel, yet the 400,000 buys were still higher than WWE had expected.

Another significant event will take place Aug. 24, six months after the first subscribers started signing up. Subscribers must commit for six months in order to access the network.

That pricing structure will change, however, with WWE now offering a $20 a month package, without the six month requirement; and another that enables subs to pay the full $60 fee for six months up front.

“We never intended to offer just one pricepoint,” said George Barrios, chief strategy and financial officer, WWE, but it took time to create multiple codes for devices that offer the network.

It’s the rollout of the network in August in new markets that’s expected to help get WWE closer to crossing the milestone of 1 million subscribers. The timing of the launch is “SummerSlam,” WWE’s second most popular PPV that will take place again in Los Angeles on Aug. 17, as well as the addition of new shows like “The Monday Night War” and “WWE Rivalries.”

In Canada, WWE signed a new 10-year-pact with Rogers Communications, the country’s largest cable operator, through which it will continue to distribute its weekly “Monday Night Raw” and “Friday Night SmackDown” shows, but also offer the WWE Network as a subscription-based channel similar to HBO. Access to the network will be provided on digital devices when individuals prove they’re a Rogers customer, similar to how people now interact with HBO Go.

Other major markets like India, China, Germany, Thailand, Malaysia and areas of the Middle East will follow, but contractual rights for WWE’s programming in those territories must first be renegotiated.

Either way, WWE is seeing more of a growth than a decline of a digital platform other companies like the National Football League are also launching in order to take more control of their content and create a new source of revenue.

Yet in order to pay for the expensive operation, WWE also has had to take a hard look at its bottomline.

The company recently trimmed 7% of its staff, or 50 to 60 people, and let go members of its talent roster, who work as independent contractors, as part of a company wide cost-cutting effort. The move is expected to save WWE $10 million this year, and $30 million in 2015, it said. Headcount had grown from around 500 employees to more than 800 over the last two to three years.

That was starting to take a toll on profits, with WWE reporting a loss of $14.5 million during its second quarter, versus a profit of $5.2 million during the same year-ago period.

While the WWE Network generated $19.4 million during the second quarter, WWE saw PPV revenue decline 39% to $23.9 million from four events. On a comparable basis, PPV buys declined 41%, reflecting the availability of the events in the U.S. on WWE Network and weaker performance in several international markets.

In addition to “WrestleMania,” WWE also produced May’s “Extreme Rules,” which saw PPV buys decline from 231,000 to 108,000; June’s “WWE Payback” (67,000 from 186,000) and “Money in the Bank” (122,000), also in June. Overall, it sold 1.06 million PPV buys during the quarter, less than the 1.6 million a year ago.

The launch of the WWE Network helped WWE increase revenue in North America by 3% to $156.3 million. It was flat in other markets.

Its media arm saw sales increase 7% to $97.7 million, due in part to higher licensing fees from USA Network and Syfy. Ratings for “Raw” rose 5%, while “SmackDown” was up 3% during the quarter.

Revenue from WWE Network, PPV and video-on-demand offerings rose 13% to $43.3 million, while TV revenue increased 13% to $43.8 million primarily due to the second season of “Total Divas,” airing on E!.

WWE produced 77 live events in the second quarter, down from 87, with 54 shows taking place in North America, which generated $27.5 million in revenue, compared to $30.1 million in the prior year quarter. The decline was driven by “WrestleMania” taking place in New Orleans, versus New York in 2013. Higher ticket prices overseas helped revenue from international live events increase 8% to $10.8 million. Overall, live event revenue decreased 3% to $40.3 million.

WWE Studios, the company’s internal film division, also saw revenue decline to $1.7 million from $2.1 million to produce a loss of $200,000, due to the timing of its recent releases, which include “Scooby-Doo! Mystery” (March), “Oculus” (April) and “Road to Paloma” (July) in theaters and straight-to-homevideo.

Home entertainment sales also decreased to $5.4 million from $7.1 million as the company saw shipments of titles decline 40%. A majority of its business on homevideo is the sale of physical discs.

“We will continue to have a solid physical business, but the network becomes our outlet on the digital side,” Barrios said. “Where studios might go and put their (digital) content on Netflix,” WWE will turn to the WWE Network, he added.

In other areas, WWE’s consumer products arm remained relatively flat at $16 million, while WWE’s online store saw sales increase 33% to $4 million, due to improvements in its mobile site. However, licensing revenue was down to $5.5 million from $6.7 million, while venue merchandise sales were down 6% to $6.5 million mostly from domestic events.

Making PPVs available on the WWE Network also took a toll on WWE’s digital division, which had previously offered “WrestleMania” on WWE.com; sales declined to $5.2 million from $7.4 million.

“WWE’s core business metrics remain strong, and WWE Network continues to be the single greatest opportunity to transform WWE’s business model,” said Vince McMahon, chairman and CEO of WWE.

Variety discusses the health of online video channel WWE Network below: