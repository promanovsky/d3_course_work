The hip-hop group hopes to revive a lawsuit alleging its fans suffered harassment and discrimination after being painted as a criminal gang in a government report.

On Tuesday, the Insane Clown Posse and four of their "Juggalo" fans asked the 6th U.S. Circuit Court of Appeals to review a judge's ruling late last month rejecting the hip-hop group's lawsuit against the Justice Department.

LIST The Hollywood Reporter Reveals Hollywood's 100 Favorite Films

The dispute emanated from a 2011 gang threat assessment issued by the National Gang Intelligence Center. The report was intended to examine emerging trends and threats posed by criminal gangs throughout the United States. In a section on "non-traditional gangs," the authors flagged some Juggalos as "forming more organized subsets and engaging in more gang-like criminal activity, such as felony assaults, thefts, robberies, and drug sales."

The report eventually led to a lawsuit from Insane Clown Posse members Joseph Bruce (aka Violent J) and Joseph Utsler (aka Shaggy 2 Dope), along with four fans who reported various forms of harassment. One Juggalo, for instance, was stopped by a state trooper and asked whether he had any axes, hatchets or other chopping instruments in his truck. Another visited a U.S. Army recruiting station and was told that he would have to remove or permanently cover his tattoos in order for his application to be considered.

The ACLU represented the band and its fans and alleged in Michigan federal court that the DOJ and FBI had committed constitutional violations under the Administrative Procedure Act. Specifically, the plaintiffs identified the First Amendment's freedom of association and freedom of expression as well as the Fifth Amendment's right of due process.

PHOTOS Penny Lane's Coat, 'Terminator' Skull: Iconic Movie Props From the Top 100 Films

But in a ruling dismissing the lawsuit on June 30, U.S. District Judge Robert Cleland identified the failings of the claims. "Five of the six Plaintiffs complain of independent actions by third-parties who are not currently before the court," wrote the judge.

Judge Cleland added that there wasn't any indication that either the FBI or DOJ directed state police to detain and question the plaintiff-juggalos nor participated in the U.S. Army's treatment of the applicant-juggalo.

And as for Bruce and Utsler, who attempted to allege injury on the basis that a Royal Oak, Mich., concert was canceled at the local police department's request, they too failed for lack of any allegation that federal authorities had directed the cancelation. "The court’s review of the NGTA illustrates the voluntary nature of this conduct by independent actors," concluded the judge.

Here's the judge's full opinion, which will now have juggalos in attendance at a federal appeals court at a to-be-determined date.

Twitter: @eriqgardner