Mila Kunis is pregnant with her first child, E! News is reporting. The actress, 30, is engaged to the father of her future child, Ashton Kutcher, 36.

As for rumors that they are expecting twins, E! News and Gossip Cop insist they are expecting one child.

Just yesterday, the love birds were caught on the kiss cam at the Pistons-Clippers game.

Huffington Post has reached out to Kunis' reps for comment and will update this story once one comes through.

Kutcher and Kunis, who played self-absorbed couple Kelso and Jackie on "That '70s Show," started dating in 2012. Kutcher and wife Demi Moore split in 2011. They don't have any kids together.

If current reports are accurate, this will be a first child for both Kunis and Kutcher. And to that we say: Mazel tov!