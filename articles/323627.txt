New research out of Spain is challenging the long-held notion that Neanderthals, the evolutionary ancestors of modern humans, ate only meat -- a vital point in understanding the origins of modern diets.

Scientists from the Massachusetts Institute of Technology and Spain's University of La Laguna have dug through ancient remains of human feces and, aside from the expected remnants of metabolized cholesterol derived from consuming animals, uncovered traces of compounds associated with plant tissues.

The fecal samples are from El Salt, a Neanderthal settlement in southern Spain that dates back about 50,000 years, and the revolutionary find offers the first direct evidence that Neanderthals apparently had an omnivorous diet.

A study based on the findings has been published in the journal PLOS ONE.

"We have passed through different phases in our interpretation of Neanderthals," Ainara Sistiaga, a graduate student at the University of La Laguna who led the research as a visiting student at MIT, said in a school news release.

"It's important to understand all aspects of why humanity has come to dominate the planet the way it does," explained study co-author Roger Summons, who teaches geobiology in MIT's Department of Earth, Atmospheric and Planetary Sciences. "A lot of that has to do with improved nutrition over time."

For years, scientists have been trying to reconstruct the Neanderthal diet, but much of the previously acquired evidence has been inconclusive.

Prior analysis of bone fragments for carbon and nitrogen isotopes, indicating Neanderthals may have consumed certain types of prey, such as wild boars over wooly mammoths, underestimated plant intake and thus inaccurately depicted Neanderthals as exclusively carnivorous, the study said.

Likewise, other researchers recently identified tiny plant fossils trapped in Neanderthal teeth -- possibly proof of a lifestyle that included harvesting and cooking a variety of plants, along with hunting, but also possibly pieces of plant indirectly chewed with the animal's stomach.

As well, Sistiaga suggested, "sometimes in prehistoric societies, they used their teeth as tools, biting plants, among other things. We can't assume they were actually eating the plants based on finding microfossils in their teeth."

To find more definitive answers, Sistiaga ventured to El Salt in search of Neanderthal feces -- prehistoric poop at the excavation site in Alicante, Spain.

She and her colleagues took small samples of soil from different layers and then worked with Summons at MIT to investigate the soil composition.

After grinding the soil samples to powder in the lab, Sistiaga applied solvents to samples to draw out organic matter from the sediment.

While most of the samples indicated meat-heavy consumption, two samples contained biomarkers Sistiaga said demonstrated significant plant ingestion.

From those realizations, Sistiaga and her team deduced that Neanderthals had, yes, a mostly meat-based diet -- likely augmented with regular portions of plants, such as tubers, berries and nuts.

"We believe Neanderthals probably ate what was available in different situations, seasons and climates," Sistiaga said.

Going forward with their findings, Sistiaga, Summons and their colleagues plan to use similar geochemical biomarker techniques to analyze soil samples from Olduvai Gorge, Tanzania -- a 1.8 million-year-old site where some of the earliest signs of human life have been unearthed.

"Until now, people have carried out residue analysis on pots, tools and other objects," but 90 percent of archaeological discoveries come from earth samples, Sistiaga said. "We're opening a new window to the information that is enclosed in Paleolithic soil and sediment."