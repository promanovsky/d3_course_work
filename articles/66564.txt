Apple patent surfaces for oleophobic coating on sapphire

The iPhone 6 is expected to arrive with a sapphire display, which will replace Corning’s offering as a new way to keep the screen safe. This hasn’t been confirmed, but a newly discovered patent for an oleophobic coating on sapphire from the company suggests the rumors are correct.

Most telling among all the details is a sketched image of the iPhone in the patent, but the filing also details products like tablets and other similar gadgets. At its core, the patent details a process for coating a sapphire display for a mobile device.

Says the patent specifically, “A window for a portable electronic device, the window comprising: a substrate having a sapphire glass base layer; a transition layer on the sapphire glass base layer, the transition layer comprising alumina and silica; and an oleophobic surface coating on the substrate, wherein the oleophobic surface coating preferentially bonds to the silica as compared to the alumina.”

This isn’t the first time Apple would use oleophobic coating to deal with fingerprints, of course. Back in 2011, an Apple patent surfaced detailing the method for use with the iPhone 3GS. It makes sense the upcoming iPhone with a sapphire display would use the same method.

SOURCE: Apple Insider

