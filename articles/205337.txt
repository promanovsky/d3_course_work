James Martin/CNET

Microsoft is giving consumers another 30 days beyond its originally stated deadline to install Windows 8.1 Update.

Instead of requiring Windows 8.1 users to install the update by May 13, which is this month's Patch Tuesday, Microsoft is now allowing them a longer grace period -- until June 10 -- to move to the Update in order to continue to receive patches and fixes from the company. Microsoft announced the Windows 8.1 Update deadline extension on May 12.

Business users still have until August 12 to move to the Update for Windows 8.1 and Windows Server 2012 R2. Microsoft extended the deadline for those updating using Windows Server Update Services (WSUS), Windows Intune and/or System Center Configuration to update users' machines. Microsoft execs unveiled the new deadline in mid-April after outcry from some that Microsoft wasn't giving them enough leadtime to apply the update.

Consumers running Windows 8.1 who have Automatic Updates turned on don't need to do anything; they will receive the Update automatically via Windows Update.

This story originally appeared as "Microsoft pushes back Windows 8.1 Update deadline by another month" on ZDNet.