After a perilous start in life, the youngest gorilla at the San Diego Zoo Safari Park is healthy and “gaining some independence” from her mother, according to zookeepers.

The baby was born March 12 by caesarian section, the first such procedure done on a mother gorilla at the park. The newborn underwent emergency surgery for a collapsed lung and then treatment for pneumonia.

Now named Joanne, in honor of Joanne Warren, first chairwoman of the Foundation of San Diego Zoo Global, the baby is learning from her mother, Imani.

At 18, Imani was old for a first-time gorilla mother and had her own medical challenges. Zookeepers had also worried that she lacked the necessary maternal impulse, a factor that contributed to the decision to deliver by C-section.

Advertisement

But Imani and Joanne have bonded well, zookeepers say. Joanne rides on Imani’s back while Imani is foraging and moving around the exhibit.

Although she not yet on solid foods, Joanne is chewing on greens like lettuce and kale. She has six teeth.

“Anything big and attractive that her mom is holding Joanne will try to investigate,” like a normal gorilla baby, said keeper Jami Pawlowski.

The park has eight gorillas.