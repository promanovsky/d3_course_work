Apple's iPad devices are displayed at its store in Tokyo January 18, 2013. Apple and IBM will work together to develop applications tailored to work with the latter’s data analytics and cloud services. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

NEW YORK, July 16 — Apple Inc and International Business Machines Corp will work together to create business software for iPhone and iPad users, setting aside a three- decade-old rivalry to cater to an increasingly mobile workforce.

IBM’s staff will sell Apple devices to its business customers, and the two companies will work together to develop applications tailored to work with IBM’s data analytics and cloud services, the companies said in a statement today. Apple also will offer customer-service support for the apps.

The partnership helps Apple pursue a bigger slice of the market for Corporate users of smartphones and tablets. Working with its erstwhile foe may also help Armonk, New York-based IBM chase other technology giants — including Apple — that have done a better job seizing on the mobile-computing boom.

“This is a shot in the arm for IBM and a great validation of Apple in the enterprise space, where they already are a huge success,” Aaron Levie, chief executive officer of cloud-storage company Box Inc, said in an interview.

With the deal, Apple gains a large sales force that will push its mobile devices to companies, while IBM, whose sales have been stagnating, adds the cachet of being partners with one of the best known and most popular consumer-electronics brands.

“We really recognised almost simultaneously that we could be uniquely helpful to one another’s strategy and that there was literally no overlap,” Bridget Van Kralingen, IBM’s senior vice president of global business services, said in an interview. “It’s moved incredibly quickly and smoothly.”

Business customers

The success of the iPhone and iPad has helped Apple gain more traction with business customers that had long shunned the company in favor of machines running Microsoft Corp.’s Windows software. Deutsche Bank AG has almost 20,000 iPhones, while Siemens AG has 30,000 iPhones, Apple said in April.

Employees at more than 98 per cent of Fortune 500 companies are using the iPhone or iPad for their work, the company said. To gain more customers, Apple has been adding new security and software tools for makers of business software.

Apple and rival Samsung Electronics Co. are seeking to further displace BlackBerry Ltd., which once held a strong grip on the Corporate market. Shares of BlackBerry fell 3.4 per cent to US$10.92 in late trading after Apple’s deal with IBM was announced.

Back to 1984

IBM and Apple were once bitter rivals during the early days of the personal computer. Apple’s famous “1984” Super Bowl advertisement to introduce the Macintosh computer compared IBM to George Orwell’s totalitarian Big Brother. The competition eased as IBM abandoned the PC business and instead focused on software and services geared for Corporate clients.

While the companies’ agreement targets business customers, it puts IBM back in the position of selling consumer products — something the company left behind in 2005 with the sale of its PC unit to Lenovo Group Ltd.

A key element of the Apple-IBM deal is developing mobile- centric software tools for businesses. While smartphones have become ubiquitous, in business they are primarily used for checking e-mail or calendars. That’s expanding to include more sales functions and day-to-day work flow, like getting approvals needed from a manager, said Jeffrey Hammond, an analyst at Forrester Research Inc

Apple and IBM said they will build more than 100 industry- specific applications for the iPhone and iPad, as well as tools for managing functions on the devices such as activations, security and analytics.

Targeting CIOs

Cupertino, California-based Apple also gains help from IBM reaching the senior decision makers in charge of making businesses’ technology purchases. While many rank-and-file employees use iPhones and iPads, chief information officers who make a lot of information technology decisions have been more reluctant to adopt Apple devices, Hammond said. IBM could also help Apple sell more Mac desktops and Macbook laptops to companies, he said.

“They could sell a lot of Apple hardware and not just phones and tablets,” Hammond said. “I still see a lot of companies where all the company-issued PCs and laptops are getting increasingly old. IBM does some pretty big deals.”

IBM shares rose as much as 2.6 per cent to US$193.40 in late trading after the pact was announced. The company is scheduled to report earnings on July 17. Apple climbed as much as 1.9 per cent to US$97.16.

Mobile opportunity

IBM has identified mobile as one of the company’s biggest opportunities since before Ginni Rometty stepped into the role of CEO two years ago. She is seeking to reshape IBM with services such as data analytics to boost revenue growth after eight straight quarters of declines.

IBM is contending with technology customers that are increasingly seeking to store data and software on cloud- computing networks, rather than on-site, limiting clients’ need for IBM’s servers and mainframes.

Still, the potential financial impact for IBM will hinge on whether the applications are sold and if customers find value in them, said Daryl Plummer, an analyst at Gartner Inc

“Revenue-wise, it’s going to be hit or miss,” he said. “It depends on what they commercialise — I can’t say it’s a jump-start yet.” — Bloomberg