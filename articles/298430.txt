Tesla promises that the production version of its SUV Model X will be even better than the concept and that it will arrive with gullwing doors.

But as well as rear-passenger doors that will attract attention and jealous glances every time they open and close, the Model X will also be getting permanent all-wheel drive and customers will get the option of a third row of seats.

In an email sent to everyone that has already reserved the car, Tesla promises that the Model X “will be a production car that exceeds the promises made when we first showed the concept,” and that when deliveries start in early 2015 that the car will represent “the most stylish way to combine an SUV’s utility with a sports car’s performance.”

Tesla first took the wraps off the Model X prototype back in 2012 and production was earmarked for 2013, but the overwhelming success of the Model S – the company’s executive sedan – in the US and now beyond, got in the way.

Now that the company and its production facilities have grown, it has capacity to start making its first SUV. Although the final price for the car is yet to be confirmed, 13,000 customers have already paid a $5000 deposit to reserve a standard Model X or $40,000 for the special Signature edition.