It's all happening! Kim Kardashian and Kanye West are applying for their marriage license this week, sources confirm to Us Weekly. As previously reported, the couple is set to say "I do" in Paris, France on May 24.

"They need to do this before they leave for Paris," a source tells Us. The Keeping Up With the Kardashians reality star, 33, and West, 36, need to file for a license in the U.S. in order for the marriage to be recognized in the States when they return. (The couple were last photographed together in Paris earlier this month. While scouting for possible wedding venues, the duo also took some time out to do some shopping.)

The last six months have been a whirlwind for the parents of daughter North West, 9 months. After the rapper planned an elaborate proposal on Kardashian's 33rd birthday last October, his bride-to-be went topless for his racy and much-talked about "Bound 2" music video one month later. In March, West finally got his wish when the pair graced the cover of Vogue.

Kardashian opened up about the couple's much-anticipated nuptials to Ryan Seacrest that same month. "We're having a super, super small, intimate wedding," she said at the time. "As we go along, we're realizing we want it to be smaller and more intimate than people are imagining and thinking."

"There's certain things where I'm like, 'I know you're going to hate the seating chart, so I'll take care of this,'" she added of West's involvement. "And then there's some things that are really important to him. [But the] seating chart is like death."