HTC One M8 Ace Vogue edition leaks – specs, price, date

HTC‘s cheaper version of the One M8 might be upon us sooner than we think. A leak coming from China has the smartphone, here being called a Vogue Edition, scheduled for a June 3 launch, and could also very well be the rumored HTC One M8 “Ace”.

Early this month, HTC has teased its next “One”, once again playing with the flagship name. The teasers portrayed women in rather fashionable attire, which would match the “vogue” moniker of this edition. However, there was also a message about not equating fashion with luxury, hinting at a somewhat cheaper price that what is currently en vogue. This could also mean that this Vogue edition and the slightly more affordable “Ace” edition would be one and the same.

To be an “edition”, it would need to have similarities with the One M8 itself. According to this leak, we basically have the same hardware specs in both, including a Snapdragon 801, a 5-inch 1080p display, 2 GB of RAM, Android 4.4, and Sense 6.0. It is said to even have the same HTC BoomSound technology, which would again match the earlier teaser.

The difference between the two starts from the outside. Most noticeable is the design, which shifts away from HTC’s flagships and their more metallic sheen towards the plasticity of the mid-range Desire smartphones. However, it doesn’t go all the way, which leaves this One M8 Vogue, or “Ace”, somewhere in between the two tiers. Also quite evident, at least once you flip the smartphone over, is the lack of a second rear camera. This version of the HTC One M8 only sports one, and not an ultrapixel variant but a plain 13 megapixel one.

While some might see those differences as minor and perhaps superficial, this last one definitely isn’t. The HTC One M8 “Ace” or Vogue edition is said to carry a price tag of 3,000 RMB, roughly $481 and quite below the rumored sub-$500 price tag for this more affordable version of HTC’s flagship. As indicated, the smartphone is said to go live next week, at least in China, and will be available in white, black, blue, and red colors.

VIA: ifanr