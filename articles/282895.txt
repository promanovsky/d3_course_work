Conflict on the other side of the world is expected to drive up gasoline prices here in Canada.

Roger McKnight, an analyst with En-Pro International, said Friday that he sees pump prices rising by more than two cents in the Toronto area on Saturday to around $1.42 a litre. Wholesale prices are also rising in the western part of the country.

Canada-wide, the average pump price is currently at just under $1.36, according to price-tracking website Gasbuddy.com.

The coming increase has nothing to do with supply and demand in North America. Rather, McKnight said the culprits are traders on Wall Street who are nervously eyeing how sectarian tensions in Iraq could affect global oil supplies.

Al-Qaida-inspired militants captured two key cities in Iraq this week and are threatening to march on Baghdad.

"I get the impression that these traders are just watching these militants and seeing how far south they can go in Iraq because that's where the oilfields are and the export terminals," he said from Oshawa, Ont.

"So the further south they get, it looks like the further north our prices go."

North America buys relatively little crude from Iraq, so any potential disruption to supplies would have more of an impact in Europe.

In fact, McKnight said things are looking pretty good in the Canadian and U.S. markets.

"The inventories in the U.S. are fine. The refineries are running pretty well ... the demand is healthy, but not extreme. There's no pipeline problems. So there's no weather problems as yet," said McKnight.

"Everything is just fine in North America. It's just everybody's watching what's happening 6,000 miles away."

Events in Iraq have an impact on the price of Brent crude, a global benchmark for oil that can travel around the world by sea. The main benchmark for inland North American crude, West Texas Intermediate, trades lower than Brent, but has recently been moving in tandem with it, McKnight said.

Oil prices are at 10-month highs, with WTI for July delivery hitting a high Friday of around US$107.68 a barrel and Brent rising to as much as US$114.40 a barrel.

The good news for Canadian motorists is that high oil prices boost the Canadian dollar. That helps ease the pinch somewhat as energy products are bought and sold in U.S. dollars.