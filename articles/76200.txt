If we consider the current health of the children in the U.S. as hint on what would be the general health of the American population in the near future, then we have a grim outlook as a large percentage of today's youngsters are likely to face a life with high risks for cardiovascular diseases.

A group of researchers analyzed the medical records of more than 12,000 children between 9 and 11-years old who have gone through cholesterol screening and found that more than 4,000 of them had borderline cholesterol levels.

The findings show that 30 percent of the children screened are at risk of developing health conditions when they get older as elevated cholesterol levels are associated with increased risks for stroke and cardiovascular disease, which is currently one of the leading causes of death in the U.S.

"We know that higher levels of, and cumulative exposure to, high cholesterol is associated with the development and severity of atherosclerosis," said study lead investigator Thomas Seery, an assistant professor of Pediatrics at Baylor College of Medicine. Atherosclerosis is the hardening of the arteries and the common cause of strokes and heart attacks.

The researchers also observed that elevated total cholesterol, low-density lipoprotein (LDL), also known as the bad cholesterol and triglycerides, a type of fat found in the blood, were more prevalent in boys than in girls.

The researchers said that their study, which was presented at the American College of Cardiology 2014 Scientific Sessions in Washington, provides evidence of the need to screen the blood cholesterol of children. The National Heart Lung and Blood Institute (NHLBI) and the American Academy of Pediatrics (AAP) have been endorsing universal cholesterol screening of children 9 to 11-years old as well as younger adults 17 to 21-years old.

"Kids need to have their cholesterol panel checked at some point during this timeframe. In doing so, it presents the perfect opportunity for clinicians and parents to discuss the importance of healthy lifestyle choices on cardiovascular health," the researchers said.

The U.S. Centers for Disease Control and Prevention (CDC), recommends reducing cholesterol levels to reduce risks of heart attack and death because of heart disease.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.