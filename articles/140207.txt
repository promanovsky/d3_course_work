The validity of the handwritten list apparently outlining the A-list conquests of Lindsay Lohan has been in question since the note was published.

However, the 27-year-old actress has admitted that the leaked explosive list was penned by herself.

In an interview on Bravo’s ‘Watch What Happens Live’, the troubled starlet was grilled about the inventory of her sexual partners.

"I cannot confirm or deny it," she told host Andy Cohen initially during the "Plead the Fifth" segment on the show.

Yet, then she got “got serious for a second” and admitted it was her “fifth step in AA at Betty Ford”.

"Someone, when I was moving during the OWN show, must have taken a photo of it. That's a really personal thing and it's really unfortunate," she added.

Expand Expand Expand Previous Next Close Lindsay Lohan attends the "Just Sing It" app launch event at Pravda on December 16, 2013 in New York City. (Photo by John Lamparski/WireImage) Lindsay Lohan Lindsay Lohan in Mean Girls. / Facebook

Twitter

Email

Whatsapp Lindsay Lohan attends the "Just Sing It" app launch event at Pravda on December 16, 2013 in New York City. (Photo by John Lamparski/WireImage)

The extensive list, released by In Touch Weekly last month, included stars like Ashton Kutcher, Ryan Phillippe, Orlando Bloom, Justin Timberlake, Adam Levine, James Franco, Heath Ledger, Zac Efron, Benicio Del Toro, Joaquin Phoenix, Jamie Dornan and more.

Online Editors