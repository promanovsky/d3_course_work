Jenny McCarthy is hitting back at critics in a new column, saying she is not anti-vaccine and those implying as much as misrepresenting her stance.

In Saturday's Chicago Sun-Times, McCarthy wrote of the "gray area" that is the vaccine issue and sought to clarify what she believes and does not.

"I am not 'anti-vaccine,'" she says right at the onset, noting that "this is not a change in my stance nor is it a new position that I have recently adopted."

"For years, I have repeatedly stated that I am, in fact, “pro-vaccine” and for years," McCarthy adds, yet "I have been wrongly branded as 'anti-vaccine.'"

Why? Because "my beautiful son, Evan, inspired this mother to question the 'one size fits all' philosophy of the recommended vaccine schedule."

Her quest, she says, was inspired by a "desire for knowledge that could lead to options and alternate schedules, but never to eliminate the vaccines."

"Blatantly inaccurate blog posts about my position have been accepted as truth by the public at large as well as media outlets (legitimate and otherwise)."

“I believe in the importance of a vaccine program and I believe parents have the right to choose one poke per visit. I’ve never told anyone to not vaccinate."

"Should a child with the flu receive six vaccines in one visit? Should a child with a compromised immune system be treated the same way as a robust, healthy child?"

"Shouldn’t a child with a family history of vaccine reactions have a different plan?" Jenny McCarthy went on. "Or at least the right to ask questions?"

"I am passionate about important conversations on how we can improve health care for our children and generations to come. This is an extremely important discussion."

"I am dumbfounded that these conversations are discounted and negated because the answers are not black or white. Again I ask, what happened to critical thinking?"

Back in March, right around when Kristin Cavallari came out as anti-vaccine, the mother-of-one received some harsh criticism via Twitter over her opinions.

When Jenny asked her followers what he or she "look for in a mate," many took the opportunity to reply that they would want someone who isn't anti-vaccine.

A big part of the reason McCarthy has been criticized is likely that she's argued for years that some early childhood vaccines are linked to autism in children.

Her 11-year-old son has autism, and she's spoken out about about the issue via social media and even during an appearance on Larry King Live in 2008.

"We need to get rid of the toxins, the mercury ... which I am so tired of everyone saying it's been removed," she told King. "It has not been removed from shots."

What do you think about Jenny's column? Do you agree or disagree with her stance - or at least that there is a gray area in between black and white?

Vaccinating your kids: For or against?