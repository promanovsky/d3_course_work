Australian researchers are trying a novel way to boost the power of cochlear implants: They used the technology to beam gene therapy into the ears of deaf animals and found the combination improved hearing.

The approach reported Wednesday isn't ready for human testing, but it's part of growing research into ways to let users of cochlear implants experience richer, more normal sound.

Normally, microscopic hair cells in a part of the inner ear called the cochlea detect vibrations and convert them to electrical impulses that the brain recognizes as sound. Hearing loss typically occurs as those hair cells are lost, whether from aging, exposure to loud noises or other factors.

Cochlear implants substitute for the missing hair cells, sending electrical impulses to directly activate auditory nerves in the brain. They've been implanted in more than 300,000 people. While highly successful, they don't restore hearing to normal, missing out on musical tone, for instance.

The idea behind the project: Perhaps a closer connection between the implant and the auditory nerves would improve hearing. Those nerves' bush-like endings can regrow if exposed to nerve-nourishing proteins called neurotrophins. Usually, the hair cells would provide those.

Researchers at Australia's University of New South Wales figured out a new way to deliver one of those growth factors.

They injected a growth factor-producing gene into the ears of deafened guinea pigs, animals commonly used as a model for human hearing. Then they adapted an electrode from a cochlear implant to beam in a few stronger-than-normal electrical pulses.

That made the membranes of nearby cells temporarily permeable, so the gene could slip inside. Those cells began producing the growth factor, which in turn stimulated regrowth of the nerve fibers - closing some of the space between the nerves and the cochlear implant, the team reported in the journal Science Translational Medicine.

The animals still needed a cochlear implant to detect sound - but those given the gene therapy had twice the improvement, they concluded.

Senior author Gary Housley estimated small studies in people could begin in two or three years.

"That's a really clever way" of delivering the nerve booster, said Stanford University otolaryngology professor Stefan Heller, who wasn't involved with the Australian work. "This is a promising approach."

But Heller cautioned that it's an early first step, and it's not clear how long the extra improvement would last or if it really would spur richer sound. He said other groups are exploring such approaches as drug coatings for implants; Heller's own research is aimed at regrowing hair cells.

The work was funded by the Australian Research Council and manufacturer Cochlear Ltd.