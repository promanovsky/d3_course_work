Researchers suggest that a novel antiviral drug may protect people infected with the measles from getting sick and prevent them from spreading the virus to others.

Scientists from the Institute for Biomedical Sciences at Georgia State University, the Emory Institute for Drug Development and the Paul-Ehrlich Institute in Germany developed the drug and tested it in animals infected with a virus closely related to one that causes the measles.

As reported, virus levels were significantly reduced when infected animals received the drug by mouth. The drug also prevented the animals from dying of the disease.

This drug, one that can be produced cost-effectively, stockpiled and administered by mouth, could boost eradication efforts by rapidly suppressing the spread of the virus during local outbreaks.

Despite major progress in controlling the measles worldwide, annual measles deaths have remained constant at around 150,000 since 2007, and there has been a resurgence of the virus in European countries where it had been considered controlled.

The reasons for this are the highly infectious nature of the virus and insufficient vaccine coverage, which in the developed world is mostly due to parents opting not to vaccinate their children.

Dr. Richard Plemper, from the newly founded Institute for Biomedical Sciences at Georgia State University, and his colleagues at the Emory Institute for Drug Discovery (EIDD) have developed a drug, termed ERDRP-0519, which blocks the replication of the pathogen.

In collaboration with Dr. Veronika von Messling from the Paul-Ehrlich-Institute, the researchers tested the drug by turning to a virus very closely related to measles virus, the canine distemper virus, which causes a highly lethal infection in ferrets. All of the animals treated with ERDRP-0519 survived infection with the distemper virus, remained disease free and developed robust immunity against the virus.

The findings are published in the journal Translational Medicine.