Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Robin Thicke made a grovelling public apology to his estranged wife live on stage at the BET Awards last night.

The Blurred Lines singer dedicated his new song 'Forever Love' to Paula Patton, his former flame and mother of his child.

Paula, who was not in attendance at the awards at the Nokia Theatre in LA, unfortunately missed her ex-husband take to the stage with the very special dedication: "Good evening, my name is Robin Thicke. I’d like to dedicate this song to my wife, and say, ‘I miss you, and I’m sorry.’ This is called ‘Forever Love'".

During the heartfelt act, Thicke displayed an old picture of the couple behind him as he played the piano with the word 'Paula' written in huge letters above.

(Image: Getty)

Robin, 37, has even named his forthcoming album after his ex-wife, 'Paula', admitting that every track is inspired by the 38-year-old Mission Impossible actress.

After the sorrowful performance the evening's host Chris Rock, mocked the melancholic tone the 'Blurred Lines' hitmaker's music has taken since he split from his spouse.

He said: “He’s singing like she don’t know him. I miss the other song, the one that sounded like Marvin Gaye.”

Previously he made a desperate plea at the Billboard Awards for her to take him back.

The couple split following rumours Robin was propositioning other women.