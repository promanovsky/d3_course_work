Samsung's flagship smartphone, the Galaxy S5, is already available, but Samsung is not yet done with the Galaxy S4. In fact, Samsung has just released a new version of the S5 predecessor in Europe.

Belsimpel, a smartphone retailer in the Netherlands, has posted a listing for the Galaxy S4 Value Edition, which retails for EUR 398 or approximately $543. The only difference between the new smartphone and the original S4 is that this one will run on Android 4.4 KitKat, which is an upgrade from the S4's 4.2.2 Jelly Bean operating system. The S4 Value Edition's back cover also seems to be slightly different from the S4. Other than those two differences, both smartphones seem to share the same features and specifications.

"The Samsung i9515 Galaxy S4 Value Edition Silver is the newer version of the regular Samsung Galaxy S4," says the new listing on the Belsimpel website. "The device has a 5.0-inch screen with a high resolution of 1920 x 1080 pixels. The operating system is Android 4.4 KitKat, which you can download from the Google Play Store applications. The silver color gives the unit a nice and chic look. The Samsung i9515 Galaxy S4 Silver Value Edition is packed with techniques such as A-GPS and Bluetooth."

The Galaxy S4 Value edition is currently available in shades of gray and white. It is unclear whether Samsung will make its latest iteration to the S4 phone available in the U.S. or other countries.

Earlier this year, Samsung also released the Samsung Galaxy S3 Mini Value Edition, which was a minimal upgrade of the Galaxy S3. The Galaxy S3 Mini was launched with a newer Jelly Bean platform compared to the Android 4.0 Ice Cream Sandwich OS found in the Galaxy S3. In place of the Galaxy S3's 1.5GHz dual-core Qualcomm Snapdragon, the mini version sported a slightly slower 1.2GHz dual-core Cortex A9 processor.

Samsung did not specify its reasons for launching an updated Galaxy S4, although it could very well be the Android phone maker is intent on demonstrating its products' value for money. Earlier this month, Samsung also released another updated Galaxy S4 with features grabbed from the current flagship. The updates include Kids Mode, which allows children to play with games while locking other features you don't want them to tinker with. The other update is Knox 2.0, a suite of security apps for Samsung phones.

The Galaxy S4 Value Edition has the following specs:

Quad-core 1.9GHz Krait 300 CPU, Adreno 320 GPU, Qualcomm Snapdragon 600 chipset

Android 4.4 KitKat with TouchWiz UI

2GB of RAM

16GB/32GB.64GB of storage

5-inch 1080p Super AMOLED Gorilla Glass 3 touchscreen

13-megapixel rear camera with LED flash

2-megapixel front camera with 1080p video recording

2600 mAh battery

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.