Xbox One July Software Update Detailed by Microsoft's Major Nelson Xbox One July Software Update Detailed by Microsoft's Major Nelson

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Microsoft will roll out the next major Xbox One software update next month and company's Larry "Major Nelson" Hyrb spilled the beans on what it will include.

It will bring various features to the console including a new achievement snap which he explained in a recent post.

"With Achievement Snap, you can track your achievement progress in real time without ever leaving the game," he wrote.

These achievements will be sorted by a player's progress towards reaching them and they will update in real time. He also talked about Microsoft's plan to bring digital and disc compilation bundles to Xbox One.

"We're doing some work now so that publishers will have options for great digital bundle and disc compilation offerings in the future," he said. "As we test this feature, we'll be seeing more flexible combinations of game titles and game content."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

The July update will also feature a double-tap for snap and the ability to like Game DVR clips. Hryb did not reveal any release date for the software.

Microsoft released the Xbox One console at the end of last year.

The system features 8GB of RAM, an 8-Core CPU, 500GB hard drive, a Blu-ray drive, HDMI in and out ports, USB 3.0, 802.11n wireless, and a built-in Kinect sensor. Microsoft describes the Xbox One as an "all in one system" that is made for encompassing games, entertainment and television all into one device.

The Xbox One is designed to improve over time thanks to cloud computing. The console has have the ability to communicate with servers in the cloud to increase the computational potential of the system. Xbox One Director of development Boyd Multerer commented on the relationship around the time of the console's release.

"[As a developer] I can start doing things like shifting latency insensitive things to the cloud. You may have a limited number of transistors in your house, but you have an unlimited number of transistors in the cloud," he said.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit