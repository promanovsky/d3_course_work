Apple may be looking to boost its radio game. The Cupertino tech giant is in talks to purchase talk radio app Swell for $30 million, Re/code reported.

As part of the deal, Apple will shut down the Swell app this week, the site said, citing "multiple" unnamed sources.

For the uninitiated, Swell is kind of like Pandora for news radio — you listen to what it plays, and if you don't like it you can swipe to move on. The app learns your preferences over time, offering a personalized listening experience with unlimited streaming audio from iTunes, NPR, ABC, ESPN, BBC, CBC, TED, and more.

According to Re/code, Swell boasts high-engagement among users, but has failed to gain a huge following.

Most of the Swell team will reportedly move over to Apple following the acquisition. Swell, which is available on Apple devices running iOS 6 or later, had been beta testing an Android app, but never released it to the general public.

The deal follows a string of acquisitions Apple has made over the past couple of months, including its $3 billion purchase of Beats this May. Apple also last week confirmed it has acquired BookLamp, a company that greatly enhances the book recommendation process, for around $10 to $15 million.

During a recent earnings call, Apple CEO Tim Cook said the company had "completed 29 acquisitions since the beginning of fiscal year 2013, including five since the end of the March quarter," not including Beats, its biggest yet.

Apple can certainly afford it. During that same earnings call, the company said it had about $164.5 billion in cash.

In June 2012, Apple released a standalone podcast app that provides one-stop access to the podcasts currently available via iTunes. The app hit one billion subscriptions a year ago, but hasn't been a big hit with users, earning just 1.5 out of 5 stars in the App Store.

Further Reading

Streaming Music Service Reviews