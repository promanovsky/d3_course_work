A family in Englewood, New Jersey is raising money for their toddler, who had his right foot and left leg amputated in January.

Now, the 2-year-old boy is learning to walk with a prosthetic, and a video of his first steps has gone viral.

Kayden Elijah Kinckle was born with omphalocele, a type of abdominal wall birth defect that sometimes makes organs stick out of the body. His mother, Nikki, said that doctors told her that he would never survive outside of the body, and they suggested that she terminate her pregnancy.

"God said everything would be alright," Nikki Kinckle said.

While in the womb, Kayden had the umbilical cord wrapped around his feet, pinning them under his pelvic bone and causing deformity. Ultimately, Nikki delivered early so the omphalocele wouldn't be too severe; his spine and hip bones were fine and mentally there were no issues.

After two abdominal surgeries and his amputations, Kayden was fitted with prosthetics and is learning to walk. Nikki posted video of his first steps, which has garnered nearly 100,000 views on YouTube.

"We celebrate his life as well and every step he takes will be to glorify God. We want the whole world to know his testimony," Nikki Kinckle said. " Thank you for all of your awesome responses, and my family is so happy that God used Kayden Elijah Knickle to encourage and inspire so many people."

Click here to donate to the Kinckle family’s GoFundMe page.

Click here for more from My Fox New York.