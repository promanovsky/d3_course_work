Yesterday we saw our first photo of the new Samsung Galaxy Tab 4 7.0, now we have another one, although this ones shows Samsung’s new 7 inch Android tablet in white.

The photo that we saw yesterday for the Samsung Galaxy Tab 4 7.0 had a date on it of the 24th of April, and as you can see from the photo below this one has the same date.

This date could either be the date that the new Samsung Galaxy Tab 4 7.0 is made official, or the date that the device goes on sale, we suspect that it will be made official around the 24th of April.

As yet we do not have any specifications on the new Samsung Galaxy Tab 4 7.0, what we do know is that the device will feature a 7 inch display.

Samsung are expected to release three different models in their Samsung Galaxy Tab 4 range, which will include the Galaxy Tab 4 7.0, the Galaxy Tab 4 8.0, and the Samsung Galaxy Tab 4 10.1.

The Samsung Galaxy Tab 4 7.0 is expected to come with the latest version of Google’s mobile OS, Android 4.4 KitKat, as soon as we get some more details on this new Android tablet from Samsung, we will let you guys know.

Source @evleaks

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more