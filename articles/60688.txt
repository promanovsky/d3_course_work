Investing.com - The U.S. dollar was almost unchanged against the yen on Friday, after the release of mostly positive economic reports out of Japan, while Thursday's strong U.S. data still lent support to the greenback.

hit 102.04 during late Asian trade, the session low; the pair subsequently consolidated at 102.14, easing 0.03%.

The pair was likely to find support at 101.30, the low of March 19 and resistance at 102.86, the high of March 13.

Government data earlier showed that retail sales in Japan rose at an annaualized rate of 3.6% in February, beating expectations for a 3.2% increase, after a 4.4% gain the previous month.

A separate report showed that Tokyo's consumer price inflation rose 1.3% this month from a year earlier, exceeding expectations for a 1.2% gain, after a 1.1% increase in February.

Core consumer price inflation, which excludes fresh food, rose 1% this month from a year earlier, exceeding expectations for a 0.9% advance, after a 0.9% increase in February.

The reports came after data showed that household spending in Japan declined at an annualized rate of 2.5% last month, compared to expectations for a 0.1% uptick, after a 1.1% rise in January.

Meanwhile, the dollar remained supported after upbeat data added to hopes that the slowdown in economic activity seen at the start of the year would be temporary.

Data on Thursday showed that U.S. gross domestic product was revised up to 2.6% in the final three months of 2013, from a preliminary estimate of 2.4%. Market expectations had been for an upward revision to 2.7%.

Separately, the Labor Department said the number of people who filed for initial jobless benefits in the U.S. last week declined by 10,000 to a seasonally adjusted 311,000 from the previous week’s revised total of 321,000. Analysts had expected jobless claims to rise by 4,000.

The yen was steady against the euro, with inching down 0.01% to 140.39.

Later in the day, the U.S. was to release a report on personal spending and revised data on consumer sentiment.