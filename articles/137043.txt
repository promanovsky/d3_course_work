NASA's robotic moon explorer, LADEE, is no more.

Flight controllers confirmed Friday that the orbiting spacecraft crashed into the back side of the moon as planned, just three days after surviving a full lunar eclipse, something it was never designed to do.

Researchers believe LADEE likely vaporized when it hit because of its extreme orbiting speed of 3,600 mph, possibly smacking into a mountain or side of a crater. No debris would have been left behind.

"It's bound to make a dent," project scientist Rick Elphic predicted Thursday.

By Thursday evening, the spacecraft had been skimming the lunar surface at an incredibly low altitude of 300 feet. Its orbit had been lowered on purpose last week to ensure a crash by Monday following an extraordinarily successful science mission.

LADEE — short for Lunar Atmosphere and Dust Environment Explorer — was launched in September from Virginia. From the outset, NASA planned to crash the spacecraft into the back side of the moon, far from the Apollo artifacts left behind during the moonwalking days of 1969 to 1972.

It completed its primary 100-day science mission last month and was on overtime. The extension had LADEE flying during Tuesday morning's lunar eclipse; its instruments were not designed to endure such prolonged darkness and cold.

But the small spacecraft survived — it's about the size of a vending machine — with just a couple pressure sensors acting up.

The mood in the control center at NASA's Ames Research Center in Mountain View, Calif., was upbeat late Thursday afternoon, according to project manager Butler Hine.

"Having flown through the eclipse and survived, the team is actually feeling very good," Hine told The Associated Press in a phone interview.

But the uncertainty of the timing of LADEE's demise had the flight controllers "on edge," he said.

As it turns out, LADEE succumbed within several hours of Hine's comments. NASA announced its end early Friday morning.

It will be at least a day or two before NASA knows precisely where the spacecraft ended up; the data cutoff indicates it smashed into the far side of the moon, although just barely.

LADEE did not have enough fuel to remain in lunar orbit much beyond the end of its mission. It joined dozens if not scores of science satellites and Apollo program spacecraft parts that have slammed into the moon's surface, on purpose, over the decades, officials said. Until LADEE, the most recent man-made impacts were the LCROSS crater-observing satellite that went down in 2009 and the twin Grail spacecraft in 2012.

During its $280 million mission, LADEE identified various components of the thin lunar atmosphere — neon, magnesium and titanium, among others — and studied the dusty veil surrounding the moon, created by all the surface particles kicked up by impacting micrometeorites.

"LADEE's science cup really overfloweth," Elphic said earlier this month. "LADEE, by going to the moon, has actually allowed us to visit other worlds with similar tenuous atmospheres and dusty environments."