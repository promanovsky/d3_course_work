OK, let's just get one thing out of the way first before we start on this review proper. As is the case with pretty much every mobile device Samsung makes now, the build and materials used in the Galaxy Tab S just don't sit well.

It's plastic fantastic, faux leather and the whole nine yards. The icing on the cake is the iPhone 5S cloning bronze/gold wrap around the tablet. Basically, it looks like something even Kanye West wouldn't be happy carrying about.

Right, now we have that issue off our chest, onto the rest of the Galaxy Tab S, which is quite frankly the best Android tablet we have ever used.

The Good



The best screen ever?

Incredibly thin

Samsung's UI is improved

Materials used

Android still lacking on tablets

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Hardware and Design

Remember the Galaxy S2? This was Samsung's magic moment. It was the phone that catapulted Samsung into the lead when it came to Android.

Since then, Samsung has never quite managed to pull off the same with tablets. The Galaxy Tab S feels like the device it hopes will change this.

Samsung has literally thrown everything at the device, putting a jaw dropping spec sheet into the Galaxy Tab S that sits above just about everything else on the market.

That means an absolutely beautiful WQXGA 2560 x 1600 resolution 10.5-inch screen, mated to a 1.9 GHz quad-core processor which also has a 1.3-GHz unit on top of it, should you need even more grunt.



Then there is 3GB of RAM to manage multi-tasking, as well as a 7,900 mAh battery. In practice, this makes the Galaxy Tab S feel absolutely rapid.

It's certainly the fastest Android tablet we have ever used, with more than enough power for any games or apps you can throw at it.

The screen is the real talking point though. Like the iPad Air, its resolution is what stands out. Text is sharp, pictures vibrant and the overall user experience benefits from the clarity and quality it brings.

Like most Samsung screens, vibrancy is cranked up to 11, but this can be tweaked in settings if you so desire. Is it better than the display on the iPad Air? It might just be.

As for design, you already know how we feel about that. The tablet is amazingly thin though - at 6.6mm it's like carrying round a small notepad in your bag.

It's hugely frustrating that something so slim and so powerful is mated to such a naff design. Samsung could have been onto a complete winner here but yet again insist on the faux leather route.

Either way, it's hard to ignore the technology in use here. Samsung has put together a very compelling tablet and if you are into top-end Android, then this is going to be the one for you.

Camera

A year ago, the camera on a tablet didn't really matter. Such is the rate at which society adopts technology, however, it's now perfectly acceptable to capture photos in public with a big slab of gadget in front of your face.



Thankfully Samsung has pre-empted this by including a powerful 8-megapixel camera on the back of the tablet, with a 2.1-megapixel front facing unit.

Both are perfectly decent, but it's the screen that really benefits the photographic experience with the Tab S. It's so bright and so sharp that pictures look fantastic, as do video calls.

User Interface and Apps

We don't want to draw comparisons too much, as we understand iOS isn't for everyone, but this is where the Galaxy Tab S really starts to lose its lead against the iPad Air.

Android by itself is perfectly decent on tablets. It's just nowhere near as slick and fluid as iOS can be. Samsung has tried to combat this by beefing up the Galaxy Tab S with a whole host of apps.

The result is an experience that feels a touch clunky and overloaded. Take the included 'Side Sync' function for example, which lets you control your Galaxy S5 from the Tab S screen. It sounds great on paper, but in practice ends up being a laggy experience that you just don't really want to use much.

Then there is the ability to load two apps up side by side, Windows 8-style. It works, but the small screen size compared to a PC means you are probably better off just switching apps completely.

Samsung's home screen is also chock full of Flipboard-like social networking aggregation apps that no-one really wants to use.

Read: Samsung Galaxy S5 review

We can't help but feel that the user experience here would've been better suited to just leaving Android alone and letting the screen do the talking.

It's not all bad though. The speed of the tablet alone, coupled with the microSD support, means that you can get an awful lot of work done using it. This is a real power user's slate and having all that storage space on offer cheaply is only going to help things.

A lot of the issues we had with the Galaxy S5's user interface filter through here. It's just not simple enough and there really needs to be plentiful streamlining done in order to make the Galaxy Tab S feel as slick as the iPad.





Samsung has already released nine tablets this year



Perhaps a better idea would be to slow down and focus on getting the user experience right, before shipping out yet another device.

We should also give mention to the included fingerprint scanner. Like the Galaxy S5, it basically doesn't work. You need to be extremely careful with how you place your finger in order to get the unlock to happen. After a few attempts we turned it off.

Music and movies

The included speakers with the Galaxy Tab S are perfectly decent. They are exactly what you would need from a tablet to watch movies and TV. The shape of the bezels on the device, however, does mean that you easily cover them up.



Just a note on that as well. By having slimmer bezels to the left and right of the Galaxy Tab S display, you end up covering the screen a lot when using it in landscape mode. This can be a touch irritating if you intend to use it for a lot of movie watching.

MicroSD support comes back into play here, however. You can transport a vast amount of movies and music around with the Tab S at a much cheaper price than investing in a 128GB iPad Air.

Verdict

Samsung continues to churn out tablets at an alarming rate. This is by far its best attempt yet, with so much focus being given to the screen, which is what really matters with a tablet.

Sure, the user interface could do with being simpler and the materials used in construction are still awful, but if you have to go Android, then all that power and performance is hard to ignore.

Most will be able to switch off and avoid Samsung's tacked on features and instead take advantage of the Tab S screen, camera and power. For those that have invested heavily in Android it also provides a lightweight alternative to the iPad Air.

As such, it's difficult not to recommend the Galaxy Tab S despite its issues. Nothing is a deal-breaker here, instead just polish that Samsung should be putting on future devices.

For the meantime though, this is the tablet in its range to go for. If you do invest, however, don't expect it to be long before your tablet gets toppled by something new and better.

4

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io