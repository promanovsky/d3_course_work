Hawthorne rocket maker SpaceX said its planned launch to the International Space Station has been delayed because of a helium leak on its Falcon 9 rocket.

The company said it plans to fix the leak and prepare for the next launch opportunity on Friday, “though weather on that date isn’t ideal.”

It’s another setback for SpaceX and its third contracted cargo mission for NASA. The company initially planned to launch its Falcon 9 rocket and Dragon space capsule in March, but several nagging delays repeatedly pushed it back.

SpaceX had planned on a 1:58 p.m. Pacific time liftoff from Space Launch Complex 40 at Cape Canaveral Air Force Station in Florida on Monday. But NASA and the company called off the launch an hour before when the problem was identified on the first stage of the rocket.

Advertisement

The Falcon 9 rocket is set to carry the Dragon capsule, packed with 5,000 pounds of supplies for the two Americans, one Japanese and three Russians aboard the space station.

There is a wide array of cargo on board, including food, science experiments, and even a set of legs for Robonaut 2, NASA’s humanoid robot, designed to help astronauts with tasks in space.

The new launch time is set for 12:35 p.m Friday. There are expected to be scattered thunderstorms in Cape Canaveral, Fla., throughout the day, according to the Weather Channel.

When the rocket does take off, it will SpaceX’s third mission on its $1.6-billion contract to transport cargo in 12 flights to the space station for NASA.

Advertisement

ALSO:

Space entrepreneur seeks end to spy satellite launch monopoly

Boeing plans to increase workforce in Long Beach, Seal Beach

Rocket blasts off from Vandenberg, lifts military satellite into orbit