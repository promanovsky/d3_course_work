We reported earlier this week that AT&T is eyeing DirecTV. Apparently the carrier is offering as much as $50 billion for the country’s no.2 satellite provider. Both companies haven’t commented on the status of talks, haven’t even hinted that talks are ongoing, but previous reports suggested that a deal could be announced in the near future. For all we know AT&T might make the announcement today.

Advertising

The timing would certainly make sense. AT&T would be able to break the news before the markets open tomorrow, and much earlier than observers and analysts expected it to announce.

Buzzfeed hears from an unnamed source that “The deal is done,” with an announcement slated for later today. The source also claims that DirecTV CEO Mike White has already informed the company’s senior executives about the finalization of the deal.

DirecTV’s 20 million subscribers would certainly have played an important part in attracting AT&T. The carrier would be able to drastically increase its television footprint in the country, that too at a time when Comcast’s planned acquisition of Time Warner Cable is making waves. AT&T reportedly approached DirecTV after Comcast made its plans public.

That being said, AT&T still hasn’t officially commented on this report, or on the possibility of a DirecTV buyout. The satellite provider also did not comment on this report.

Filed in . Read more about AT&T and Directv.