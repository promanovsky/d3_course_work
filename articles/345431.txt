On Tuesday, the Federal Trade Commission filed a lawsuit alleging that T-Mobile made hundreds of millions of dollars from 2009 to December 2013 by charging customers for shady text message subscriptions.

The third-party merchants peddled subscriptions for celebrity gossip updates, horoscopes, and flirting tips, the FTC said, and many customers did not authorize the charges, which were allegedly obscured on monthly bills. About 40 percent of the customers affected tried to get their money back, which was an "obvious sign to T-Mobile that the charges were never authorized."

"T-Mobile knew about these fraudulent charges and failed to stop them or take any action," Jessica Rich, FTC consumer protection director, said in a conference call with media. The company claims that in 2013, it stopped billing for premium texting services and did set up a way for customers to get their refunds. CEO John Legere said in a statement that he wished the FTC would go after the third-party merchants instead of T-Mobile.

"T-Mobile is fighting harder than any of the carriers to change the way the wireless industry operates and we are disappointed that the FTC has chosen to file this action against the most pro-consumer company in the industry rather than the real bad actors," he added.

The exact number of T-Mobile customers affected is not yet known. The FTC said it would like to get refunds for all. Catherine Garcia