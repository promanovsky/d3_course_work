Music

The former Nickelodeon star says she and the Canadian singer worked on the duet and left it unfinished after they got busy with their respective project.

Aug 1, 2014

AceShowbiz - A few months ago, Ariana Grande and Justin Bieber revealed they were working on a duet and they posted an Instagram photo of them hanging out in a studio to prove it. Fans, who have been waiting so long for the collaboration, will likely be disappointed to know that the highly-anticipated project will not arrive anytime soon.

Speaking to Associated Press, Grande recently said that she and Bieber did work on the duet but they never managed to finish it. "We were working on it. We were in the studio together... We were going to do it, but then we both got busy," she explained. "I had to focus on my stuff and he was focusing on his, and it just sort of didn't really get done."

However, the duet hasn't completely been scrapped. "We feel like we owe it to our fans to do something one day," the former "Sam & Cat" actress continued. "We will finish it and put it out one day, but it just wasn't the right time."

During the same interview, Grande also revealed that she was actually "scared" of putting out her track "Problem". "Once we wrote it and recorded it, I was like, 'Hmm, I'm kind of scared of this.' It started to intimidate me a little because it felt different for me and it was such a risk with the whispers," she said. "It took me a lot of time. I had to separate myself from it for a while. For a couple months I didn't even listen to it."

She changed her mind after listening to the final version of the song, and everyone's glad she did because the Iggy Azalea-featured track eventually became a smashing hit and peaked at No. 2 on Billboard 100.