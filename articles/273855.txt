By Jefferson Graham and Edward C. Baig

USA TODAY

SAN FRANCISCO — Apple introduced many new features in its preview of the new iOS 8 mobile operating system this week at the Worldwide Developer's Conference.

But what ones really stick out?

Call me disappointed. It's the day after, and I find it hard to remember even one new feature that really sticks out, after a presentation that just wasn't a wow for me.

My colleague Edward C. Baig has a different take. He says the bigger picture is a bunch of features that when they're released in the fall, will make iPhone and iPad users happier folks.

Watch our debate in the accompanying video.

Follow Jefferson Graham and Ed Baig on Twitter