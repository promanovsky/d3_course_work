Image via Flickr

Have you ever come home and walked through your door only to realize that your door was unlocked? You immediately wonder if you’re in a house with an intruder, start trying to remember whether or not you forgot to lock it, and start looking for things that are out of place.

The Heartbleed bug has left that door to passwords and files open on the Internet. This is for the sites that use OpenSSL to encrypt files, which applies to about two-thirds of Internet sites. A number of things surrounding Heartbleed is concerning.

One is that the door you thought was protecting your information has been open for about two years. Not only that, but the word is that the NSA, currently under fire and heavy scrutiny for spying on everyone it can, found out about Heartbleed two years ago and didn’t tell anyone. Bloomberg News stated in a report that the NSA kept the vulnerability quiet and used that backdoor itself. To further the house analogy, the police, instead of patrolling and protecting your neighborhood, has possibly been breaking into your house whenever they please and looking in your underwear drawer.

Another bothersome thing about Heartbleed is the lack of a way to trace whoever peeks at files. So if there’s been someone in your house, they could scope out everything. You would never know if they had been there or not. This leaves companies like American Funds, who manages over one trillion in assets, to scramble for a fix for software that might not have even been compromised. American Funds spokesman Chuck Freadhoff stated:

“We have no evidence or belief that any information was used to gain access to any shareholders account. In fact it would be almost impossible to access a shareholder’s account and transact, given the multiple layers of security within the American Funds system.”

Still, they urged users to change passwords, user IDs, and create new security questions. Most companies are urging their users to do the same, implementing a better-safe-than-sorry approach while Heartbleed is being fixed.

On April 7, a Google employee and a Finnish company called Codenomicom discovered Heartbleed. The question then was how do you fix a worldwide flaw in the security of the Internet without alerting all the would-be hackers that it’s a free for all? The equivalent of telling everyone on Facebook that you’re going on vacation. Not a good idea. But since there was no way to know what sites had been exploited using Heartbleed, how can you not, in good faith, notify everyone on the planet?

But not everyone can use the Heartbleed bug and not leave a trace. Arthuro Solis-Reyes, a 19 year old computer science student of London, Ontario, was arrested at his home yesterday. Using the Heartbleed flaw, he gained access to around 900 social insurance numbers from the Canada Revenue Agency.

As that door to your virtual house is slowly closing, the threat posed by Heartbleed appears to be very real. Now may be the time to change your passwords. If you forget what they are after you change them, just call the NSA.