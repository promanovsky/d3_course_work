Wage growth has finally started to outpace inflation after years of falling real earnings, and unemployment has fallen to its lowest level in five years, figures released on Wednesday show.

In a boost to British households, pay including bonuses rose by 1.7% over the three months to February, just ahead of the March inflation rate of 1.6%, published by the ONS on Tuesday.

The improvement was driven by pay rises in the private sector, where wages were 2% higher than in the same quarter of 2012-13 – but that was more than double the 0.9% growth for those working in the public sector.



For those not getting a bonus, regular pay was up 1.4% over the three months compared with a year earlier, again dragged down by slower public sector earnings growth.



The trend marks a milestone for the economy, as wages have not consistently outpaced inflation since 2008. A brief period of rising real wages in 2010 was followed by climbing inflation.



Andrew Goodwin, senior economic adviser to the forecasters the EY Item Club, said it was “the beginning of the end of the cost-of-living crisis”.



He added: “As the recovery firms up, the labour market tightens and skills shortages start to appear we should see wage growth gradually accelerate and establish a clear gap over inflation.



“That said, the decline in real wages has been so severe that it will probably take another four years until we recover from the losses of the past six years.”



But Frances O’Grady, general secretary of the Trades Union Congress, said: “With wages increasing by just 1.7%, barely enough to keep up with a prices measure that doesn’t even include housing costs, the cost-of-living crisis is far from solved. This is not worth even half a cheer. The time to celebrate recovery will be when ordinary workers return to pre-crash living standards and the economy is generating more secure skilled jobs with good prospects and decent pay.”



The unemployment rate fell more sharply than expected, to 6.9%, over the three months to February, from 7.1% in the three months to November. It was the lowest rate of unemployment since January 2009, but still above pre-crisis levels.



Andrew Sentance, PricewaterhouseCoopers economist and a former interest rate setter on the Bank of England’s monetary policy committee (MPC), said the rapid fall in unemployment was a sign that a rise in UK rates was “long overdue”. Interest rates have been on hold at a record low of 0.5% since March 2009.



“Low UK inflation is not a reason for postponing rate rises,” Sentance said.



“Low inflation allows the MPC to raise rates gradually, which is the best exit policy.”

The MPC, led by the Bank’s governor, Mark Carney, has suggested that it will not raise rates until spring 2015.

The chancellor, George Osborne, welcomed the jobs data. “Coming alongside yesterday’s lower inflation, they are compelling evidence that our economic plan is working. There are now a record number of jobs in Britain – and today we have taken a further step in meeting the ambition I have set for full employment. Every job created means another family with greater economic security and the prospect of a brighter future.



“These remain difficult times for families facing pressures on their budgets, and much work needs still to be done to build a resilient economy. But today’s news supports the argument we have made all along that the only way to see rising living standards is to grow the economy.”

Rachel Reeves, shadow work and pensions secretary, hit back: “It’s deeply complacent and out of touch for the Tories to try and claim this deep-seated cost-of-living crisis has suddenly been solved. Only Labour has a clear plan to earn our way to higher living standards for all, not just a few at the top.”



There are now 2.24 million people out of work in the UK, a quarterly fall of 77,000 in the three months to February. Employment jumped by 239,000 to 30.39 million, helped by the continued rise of self-employment.



Joe Grice, chief economist at the ONS, said: “These figures - rising employment and falling unemployment and inactivity – continue the strong trend in the labour market that has been seen in recent months. Self-employment has again been a prominent growth area.”

The number of people claiming unemployment benefits also fell slightly more than expected, by 30,400 to 1.142 million in March, the lowest level since November 2008.



The unemployment rate among 16 to 24-year-olds fell to 19.1% from 20% in the previous quarter.



The better data on standards of living is starting to feed through to household optimism according to Markit’s household finance survey for April. The headline measure, which covers overall perceptions of financial wellbeing, reached the highest level in more than five years of data collection.



Jack Kennedy, senior economist at Markit, said: “A long-awaited return to rising real wages is likely to boost consumer spending as we move through 2014, adding another piece to the jigsaw of the UK’s economic recovery.”