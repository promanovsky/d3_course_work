A Bank of America sign is displayed in the Columbia Heights neighborhood of Washington D.C. UPI/Alexis C. Glenn | License Photo

CHARLOTTE, N.C., April 16 (UPI) -- Bank of America swung into the red, posting a first-quater loss thanks to $6 billion in legal charges and a weakening mortgage business.

The country's second largest bank by assets reported a loss of $276 million, or five cents a share, compared to a profit of $1.48 billion reported a year earlier. The company's revenues fell 2.7 percent to to $22.6 billion. This is the fourth time BofA has posted a loss since Chief Executive Officer Brian T. Moynihan took the top job at the start of 2010.

Advertisement

“The cost of resolving more of our mortgage issues hurt our earnings this quarter,” Moynihan said in a statement today detailing results. “But the earnings power of our business and customer strategy generated solid results and we continued to return excess capital to our shareholders.”

BofA reported $6billion in in legal costs -- $3.6 billion for a settlement disclosed last month that covered Fannie Mae and Freddie Mac and $2.4 billion for undisclosed mortgage-related matters. Chief Financial Officer Bruce Thompson refused to give details about which cases triggered the additional $2.4 billion.

“These are ongoing discussions that we obviously have with people we’re in litigation with, and I just don’t think it’s going to be productive to give a breakout,” Thompson told reporters.

The bank has been working to resolve mortgage-related lawsuits and investigations stemming from the financial crisis and its acquisition of Countrywide Financial Corp. While the company reported good progress in this respect, posting increasing revenues in all five of its major business units, Wednesday's results put the spotlight squarely back on its legal troubles.

The company has said it is at the center of investigations involving government agencies like the Justice Department and state attorneys general. The bank's legal expenses have totaled more than $50 billion since the financial crisis.

[Bloomberg] [The Wall Street Journal]

RELATED Number of uninsured in states embracing Obamacare drops significantly

RELATED Man arrested for placing backpacks at Boston Marathon finish line