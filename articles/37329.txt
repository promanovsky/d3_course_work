Endocyte, Inc (NASDAQ:ECYT) skyrocketed in early trading after the company announced that its lung cancer drug has been shown to slow down progression of the disease. Currently the biotechnology company doesn’t have any marketed products, and its lung cancer drug is still in the experimental stage.

Endocyte drug backed by EU panel

Bloomberg reports that regulators in the European Union have backed Endocyte, Inc (NASDAQ:ECYT)’s drug vintafolide in the treatment of ovarian cancer. The EU’s backing also covers the use of an imaging agent which is said to help doctors target the patents who will best respond to the treatment.

Barron’s Mailbag June 1962: Irving Kahn On False Comparisons The following letter from Irving Kahn appeared in the June 25, 1962, issue of Barron’s. Irving Kahn wrote to Barron's criticising the publication’s comparison of the 1962 market crash to that of 1929. Irving Kahn points out that based on volume and trading data, the 1962 decline was a drop in the ocean compared to Read More

RBC Capital Markets analyst Adnan Butt told Bloomberg that Endocyte, Inc (NASDAQ:ECYT)’s push into treating non-small cell lung cancer with the drug is important because it expands the number of patients the drug could treat. Currently the lung cancer study is in the middle stage, and it showed that vintafolide, when combined with chemotherapy, cut patients’ risk of dying or having their cancer get worse. The drug reportedly reduced these risks by 25% compared to use of chemotherapy alone.

Is Endocyte an acquisition target?

There have been a number of high-profile acquisitions within the biotechnology industry, and Butt believes this drug could make Endocyte, Inc (NASDAQ:ECYT) a potential target. However, the analyst didn’t name any specific possible suitors. Butt did say that this study demonstrates that Endocyte is a “platform technology company” and that it is similar to ImmunoGen, Inc (NASDAQ:IMGN) and Seattle Genetics, Inc. (NASDAQ:SGEN).

Currently researchers are still gathering data on how long the drug can extend patients’ lives. Endocyte, Inc (NASDAQ:ECYT) management said the data could be ready as early as later this year. However, they said the timing relies on Merck & Co., Inc. (NYSE:MRK), which is Endocyte’s partner in the development of the drug. Merck joined the research on it in 2012. Shares of Merck fell less than 1% in early afternoon trading today.