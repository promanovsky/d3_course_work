Prospects were encouraging for fashion designer L'Wren Scott's business, a PR firm says

Reports that fashion designer L'Wren Scott's company was on the verge of shutting down have been denied.

The 49-year-old long-time partner of Rolling Stones singer Sir Mick Jagger was found dead in her New York apartment on Monday. Authorities say she committed suicide.

New York-based PR Consulting said that Scott was considering a restructure of her global business, LS Fashion Ltd.

The firm says that while some areas of the business had not yet reached their potential, other parts were successful.

It says the long-term prospects for the business were encouraging.

Accounts filed by LS Fashion Ltd in London show the company had liabilities that exceeded assets by £3.5 million as of December 31, 2012.