For many of us, our mothers are awe inspiring. We see them as a person who can do it all, and we are often right. Moms have this impressive ability to care for the home, heart, and table while juggling their own jobs, friends, and partners. And since we are all convinced our mother is the best mother of all, it is impossible to assign a value to any mom. Whether she is a single parent, in a loving relationship, or a stepmother, her role as a caretaker and nurturer is one that is no more valuable than her fellow mothers.

But then there are moms who spread that motherly affection and natural talent beyond themselves. They look at the world's children and wonder what they can do to make their environment a better one to live in. While many moms choose to do this in their own way, we have a particular soft spot for the women who concern themselves with the nutritional value of our children's lives.

So in honor of Mother's Day, we set out to find the most influential mothers in the food industry. Since moms are each important in their own way, we felt that ranking their value was somewhat of a futile effort. Instead, we took a look at some of the food industry's most prominent activists, restaurateurs, celebrities, chefs, entrepreneurs, authors, and corporate figureheads who are mothers. We looked at dozens of mothers and considered the several ways they've influenced the food industry. Aside from their accomplishments, we took a look at their power ranking, their social reach, their "cool" factor, and how they serve as a "mother to the world," all while being focused on food and nutrition.

For instance as a mother of four and the world's first female Iron Chef, Cat Cora made it on to our list. We have mothers like Rosalind Brewer, who may not have a celebrity status but does serve as president and CEO of Sam's Club, which millions of families rely on for the best value for their food. It was a close, but editorial discretion ultimately helped to finalize the list, making leaving off moms like Jean Nidetch, the founder of Weight Watchers, and Lauren Deen, a longtime food producer, pretty tough.

In taking all of these factors into consideration, we comprised a list of some of the most influential mothers in the food industry in no particular order. And to all of the mothers out there, from the women who taught world renowned chefs how to cook, to the mom that was able to squeeze in a pan of brownies for their kid's classroom party, we salute you!

Michelle Obama

Photo Credit: Pete Souza

Click Here to see More of the Most Influential Moms in Food Does the First Lady really need any introduction? Michelle Obama is an influential mother of two beautiful girls, but extended her mom-reach even farther when she began the Let’s Move! campaign. This healthy, food-focused, active lifestyle program inspired parents, teachers, and kids to get off of the couch and into some fun. They’ve successfully taught families the importance of healthy food and portion control decisions. The First Lady has since partnered with several prominent public figures to reinforce healthy living and to help her wage the war against childhood obesity.Photo Credit: Pete Souza Ruth Reichl There is little that food writer and author Ruth Reichl hasn’t accomplished. Since she began writing about food in 1972, she’s served as the editor-in-chief of prominent food verticals like Gourmet Magazine, and has written more than a handful of best-selling memoirs. And awards? On top of her six James Beard Awards, Reichl was honored with multiple awards that included Adweek’s Editor of the Year in 2007. Her vast knowledge of the food industry is practically unparalleled and her thumbprint has permanently been impressed upon the food industry from her critiques to her novels.

Photo Credit: Ruth Reichl Martha Stewart

Photo Credit: Getty Images Entertainment

Click Here to see More of the Most Influential Moms in Food Mom and media mogul Martha Stewart has been helping mothers across the country elevate their entertaining status since her days as a gourmet caterer in the 1970s. A self-taught cook and the definition of a self-starter in general, Stewart’s empire has grown immensely since her days spent hunched over Julia Child's Mastering the Art of French Cooking. She’s published countless books, hosted her own show, and created the Martha Stewart Living Omnimedia brand that highlights everything from food to weddings with their various publications.Photo Credit: Getty Images Entertainment Shazi Vishram When Happy Baby grew up into Happy Family, CEO and founder Shazi Vishram truly made her mark on the modern family. As a mother herself, Vishram watched friends struggle with providing their children with healthy foods and vowed to help them get quality food without slaving away in the kitchen for hours. Today, Happy Family provides organic foods for babies, toddlers, kids, and adults that can help the family stay healthy together!

Photo Credit: Shazi Vishram Alice Waters

Click Here to see More of the 20 Most Influential Moms in Food

Photo Credit: Alice Waters Aside from being an accomplished chef, restaurateur and author, Alice Water’s became a mother to the world in addition to her own children when she began The Edible Schoolyard Project. Not only was she named one of Time’s 100 Most Influential People, her project has been educating the nation on sustainable healthy foods for more than 17 years.Photo Credit: Alice Waters

-- Lauren Gordon, The Daily Meal