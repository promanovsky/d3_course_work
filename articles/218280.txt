Sum 41 frontman Deryck Whibley has spoken about his health, following the collapse of his liver and kidney.

The singer has revealed that years of drinking have caused extensive damage to his organs, with doctors apparently warning him that one more drink could kill him.



The 34-year-old wrote on his website: "I was drinking hard every day. Until one night. I was sitting at home, poured myself another drink around midnight and was about to watch a movie, when all of a sudden I didn't feel so good."

Whibley was then rushed by his fiancÃ©e to a hospital, where he's had to remain for a month.

"Needless to say it scared me straight," he said. "I finally realized I can't drink anymore. If I have one drink, the doc's say I will die.

"The reason I got so sick is from all the hard boozing I've been doing over the years. It finally caught up to me."

Whibley, who was once married to Avril Lavigne, also gave a warning about alcohol to his fans.

He said: "I'm not preaching or anythin[g], but just always drink responsibly. I didn't, and look where that got me.

"I was stuck with needles and IVs all over. I was completely sedated the first week. When I finally woke up, I had no idea where I was. My mum and step dad were standing over me. I was so freaked out."

Whibley added that this experience has helped him to come up with new songs.

Catherine Earp Catherine Earp is Digital Spy’s News Editor covering all things entertainment.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io