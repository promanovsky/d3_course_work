The new photo and video sharing app that Facebook launched in the US for Android and iOS users last week has now been made available for audiences anywhere, Slingshot announced in a blog post Wednesday.

"Since we launched last week, we have heard from lots of people around the world who are excited to give Slingshot a try. Starting today, we are expanding our initial launch and making Slingshot available internationally!," the post read.

The Facebook app that has already created a buzz does not require one to be on Facebook and users can simply sign up for the service with their mobile phone number and connect with friends in their phone's contact list or Facebook friends, if they so want.

Seen as a move to increase the user involvement in the app, the shot-for-shot feature has, however, faced flak from some users and reviewers.

"The result is you and your friends constantly sharing uninteresting images purely to open each other's initial pictures. In just a weekend of using Slingshot, I received far more images of blank walls and ceilings than duck faces and lunches," Smartphone reviewer Brighthand wrote on its website.

"You can find Slingshot in app stores around the world for iPhone (iOS7) and Android (Jelly Bean and KitKat)," the blog post read.