

Credit: Ad Meskens

Last week, America celebrated the 238th anniversary of its independence. But is American freedom just an illusion? In this age of crony capitalism, does freedom even stand a chance? If the outcome of your life, constraints on your liberty, and ability to pursue happiness is pre-determined, why bother?

In his new book, Unstoppable: The Emerging Left-Right Alliance to Dismantle the Corporate State, Ralph Nader discusses the convergence of the political left and the right aligning in their outrage against crony capitalism. When government picks the winners and the losers in the marketplace, America loses.

And America is losing.

Take Uber, a company that has wrapped itself in this flag of economic liberty, beckoning new investors with an $18 billion valuation. Wall Street has beaten the patriotic drum in favor of the Uber business model, decrying the dreaded taxicab cartels that have, they argue, exploited the monopoly on taxi services across the country. They cheer on the rideshare behemoth, waiting for the capitalist wave to restore free market order to an unfair system.

But don't be fooled by the sparklers or wave the American flag just yet.

The reality is just the opposite: Uber's success is decidedly un-American. It is anti-free market. Its only success is rooted in manipulation and strong-arm tactics. Their powerful lobbyists pull political strings behind-the-scenes. The greedy dregs at Goldman Sachs, which swallowed a $12.9 billion taxpayer bailout in 2008 after fueling AIG's risky subprime housing market gambles, are among Uber's biggest backers.

This is crony capitalism at its worst. Uber is not interested in reforming the taxi system. They just want to substitute the beneficiaries: them.

Last month, Mayor Rahm Emanuel introduced a Chicago ordinance that magically created a new category of "transportation network provider," in order to legalize UberX's previously illegal taxi services. Emanuel, who has deep financial ties to Goldman Sachs and whose brother, Ari, was an early Uber investor, declared the measure the "most comprehensive in the country" to deal with this "new industry. "

But this isn't a "new industry."

An adult bookstore that offers fee-per-use video booths can't be charged with prostitution if the client just happens to receive the sex for free. Is this a "new industry?" No, it's the oldest one in the book.

The author, Patrick Rothfuss, once said, "Call a jack a jack. Call a spade a spade. But always call a whore a lady."

But Uber is no lady.

Uber is a taxi company masquerading as a "rideshare" app to circumvent the strict government-imposed regulations on the cab industry.

For example, since April 2013, UberX has provided illegal taxi services throughout Chicago with little to no consequence. Of the city's estimated 2,000 rideshare operators, only 16 UberX and Lyft drivers were ticketed for operating illegal cabs in the first three months of 2014. All of the tickets were dismissed.

But it was a different story in 2009 when the City of Chicago impounded 300 illegal cabs. At the time, scam operators painted bogus taxi company names on their vehicles to troll customers. Others were hustlers shaking down customers or uninsured drivers. Claiming credit, city officials crowed about protecting consumers from uninsured drivers. Offenders were charged $5,000 or more in fines.

Unlike Uber, taxicab companies must abide by the city's cab regulations. In Chicago, they are required to own or lease one of 7,000 official taxi medallions at a rate of $350,000 a piece (New York medallions have fetched $1 million) payable to the city's coffers. Cities control the number of medallions in the market.

Taxi medallions have been one of the best investments in America, averaging a 16 percent annual rate of return. Smaller operators and entrepreneurs have taken out mortgage-sized loans to secure the medallions.

But, large operator or small, these investments are now threatened by this technobabble bait-and-switch.

With UberX, anyone with a clunker can operate as a "rideshare operator." For Uber execs, there are no fleets of vehicles to maintain, no workers compensation, or medallions to purchase or lease. While Uber is free to set its own fare rates, taking advantage of the market, most cities control cab rates.

Using these anti-competitive advantages, Uber is recruiting cab drivers around the country. A secret recording of an Uber sales pitch reveals the strategy: no lease fees and the opportunity to cash-in on "surge pricing" by becoming a rideshare driver.

If the unfair playing field continues, the cab industry will die a death by a million cab driver cuts and the Taxi Bubble will go bust.

Recognizing the rideshare threat to their income, taxi drivers are fighting back. New York's 17,000-member strong taxicab union is now working to organize drivers in cities across the country both to gain traction against Uber and send a message to big-city mayors.

Yes, there have been big financial winners in the taxicab industry but government created this public-private system, imposed the stringent rules, and happily lined its pockets.

If the taxicab industry requires reform, reform it. But this Uber political sleight-of-hand is underhanded. It is dishonest.

And dishonesty is the most un-American thing of all.