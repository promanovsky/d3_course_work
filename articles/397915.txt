Running for just a few minutes each day can reduce the risk of dying from heart disease, research has shown.

Running for just a few minutes each day can reduce the risk of dying from heart disease, research has shown.

Lazy jogging at slow speed also has potentially life-saving benefits, say scientists.

The findings, from a study of 55,137 adults aged 18 to 100, suggest that healthy exercise does not necessarily have to be exhausting or time-consuming.

Researchers followed participants over a period of 15 years, during which more than 3,000 died. A total of 1,217 deaths were related to cardiovascular, or heart and artery, disease.

Runners, who made up just under a quarter of the study population, had a 30% lower risk of death from all causes and a 45% lower risk of death from heart disease or stroke than non-runners. They also lived an average three years longer.

Running for less than 51 minutes a week - or about seven minutes a day - fewer than six miles, or slower than six miles an hour all reduced the chances of dying.

People who ran for less than an hour a week benefited as much as those running more than three hours a week.

US lead scientist Dr Duck-Chul Lee, from Iowa State University, said: "Since time is one of the strongest barriers to participate in physical activity, the study may motivate more people to start running and continue to run as an attainable health goal for mortality benefits.

"Running may be a better exercise option than more moderate intensity exercises for healthy but sedentary people since it produces similar, if not greater, mortality benefits in five to 10 minutes compared to the 15 to 20 minutes per day of moderate intensity activity that many find too time consuming."

The findings are published in the Journal of the American College of Cardiology.

Study participants who ran regularly for an average of six years experienced the biggest benefit, the researchers found. Their risk of death from heart disease or stroke was reduced by 50%.

Christopher Allen, senior cardiac nurse at the British Heart Foundation, said: "Protecting ourselves against life-changing conditions like a heart attack or stroke should be everyone's top priority. But the reality is not everyone is managing to achieve their 150 minutes of physical activity a week.

"What this study proves is that when it comes to keeping physically active, every step counts towards helping you maintain a healthier heart.

"Breaking your exercise down into 10-minute chunks can make this goal much more achievable and can help prolong your life by reducing your risk of dying from cardiovascular disease."