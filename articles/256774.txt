Rich Trenholm/CNET

Uber is doing some defensive driving after London cabbies blasted the controversial e-hailing app.

"With so much chatter about us, we thought it was about time to jump in and clarify a few things," says Uber in a blog post responding to critics today. "London cabbies (sic) are iconic -- arguably the best taxis in the world. However, there is room for all and there is room for more and better. We are bringing competition to an industry that hasn't evolved in years."

In today's statement, Uber, an app that enables you to call a car from your smartphone in 36 countries, reiterated its licensed status in London and highlighted its safety practices.

"All drivers are thoroughly screened," says Uber. "(Drivers) carry full commercial insurance, and at the end of every trip, a rider anonymously rates his/her driver, to help maintain and improve the Uber experience. Every transaction is cashless, so drivers -- and riders -- never have to worry about the risk or hassle of carrying cash or making change.

"We fully share the TFL's vision that technology should be a key driver in changing the way people are moving around their city."

All Uber the road

Once again Uber paints itself as a disruptive new technology shaking up an entrenched taxi industry. It's not quite as black-and-white as that, however, as cabbies point out they have embraced technology -- for example, 60 percent of the capital's taxi drivers signed up to rival app Hailo. Cabbies argue that in fact they oppose what they see as Uber twisting the regulations by which taxis and private hire operators must abide.

In today's statement, Uber reiterates that it's a private hire operator officially licensed by Transport for London, referring specifically to rules surrounding meters. The app's opponents argue that a smartphone used by an Uber driver to sort out your fare is essentially a meter, but Uber reckons a phone doesn't count -- and TFL agrees.

The meter is important because it's one of the things that separates taxis -- London's black cabs -- from minicabs and other private hire vehicles. Taxis have meters that charge fares at a rate set by TFL, and taxi drivers have to be individually licensed at the expense of the individual driver. Meanwhile minicabs, limos and other private hire vehicles don't have meters and can set their own fares, while it's the dispatch company that's licensed rather than the individual drivers.

Concerned cabbies reckon TFL has caved in to Uber's practices, and will protest as such by blocking London streets on 11 June. Taxi drivers have protested against Uber in various cities, including Paris.

Tempers flared in London last week when black cab app Hailo applied for a license to offer minicabs and limos as well, prompting irate cabbies to descend on a dispatch office in South London.

Now playing: Watch this: Taxi apps Uber and Hailo cause road rage in CNET UK Podcast...

Speaking to CNET after the Hailo ruckus, Grant Davis of the 1,600-strong trade organisation the London Cab Drivers' Club said cabbies felt "stabbed in the back."

Referring to Uber, Davis said, "If they decimate the cab trade and minicab trade in London and just have one tier of 'taxi' -- which is actually just a car -- and that's the Uber tier, then what the public will get is price surges. When it's raining, the cab you used to get home from the club will have doubled in price, but you have to pay what you have to pay because no-one's left standing."