Apple is developing new set-top box hardware, according to a US Federal Communication Commission (FCC) filing from Comcast and Time Warner Cable.

The FCC document, which calls for a merger between the two companies, hints that a successor to Apple TV is in the pipeline.

Apple



"Today, Google competes as a network, video and technology provider, and eight out of nine of the next Google Fiber markets the company announced are in Comcast or TWC areas," reads the filing.

"Apple tablets are viewing platforms for cable services even while Apple offers an online video service, Apple TV, and explores development of an Apple set-top box."

The document suggests that the next-generation Apple TV could be backed up by content partnerships, including a deal with Time Warner to supply video content.



It could also indicate that the new hardware could be an over-the-top box that relies on an internet connection, rather than a coaxial input.

Apple is yet to officially confirm that it is working on a new Apple TV box.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io