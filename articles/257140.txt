CNET

Security vendor Avast is dealing with its own security problem.

The company revealed Monday that its community support forum was hacked over the weekend, compromising user nicknames, usernames, email addresses, and hashed (one-way encrypted) passwords. In response, Avast said it has taken down the forum for a brief time in an effort to rebuild and relaunch it on a different platform.

Though the passwords were encrypted, Avast acknowledged that a sophisticated hacker could determine many of them. As a result, those with accounts on the Avast support forum who used the same passwords elsewhere are urged to change them at the other Web sites. Once Avast's support forum is back up, users will be required to change their forum passwords.

The Avast hack is the latest in a long line of hacks that have seen user credentials wind up in the wrong hands. Last week, eBay urged its users to change their passwords following a hack to its corporate network that compromised a user database. Last December, retail giant Target revealed a hack that stole the names, credit or debit card numbers, expiration dates, and three-digit security codes of customers who had purchased items over the prior few weeks.

The irony here, of course, is that a security vendor in the business of protecting users from malware and other threats should find itself the victim of a hack.

The Avast hack hit just the user support forum, according to CEO Vince Steckler, affecting less than 0.2 percent of the company's 200 million users. No data from the company's payment, licensing, or financial systems was compromised, Steckler added.

Avast doesn't yet know how the hacker gained access to the forum, which had been hosted on a third-party software platform, Steckler said. But the company detected the hack "essentially" immediately, he added, and will move the forum to a faster and more secure platform once it's ready.

"We realize that it is serious to have these usernames stolen and regret the concern and inconvenience it causes you," Steckler said. "However, this is an isolated third-party system and your sensitive data remains secure."