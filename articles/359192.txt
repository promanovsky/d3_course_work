Washington: A new study has revealed that 25 minutes of focused meditation for three consecutive days affects people`s ability to be resilient under stress.

J. David Creswell, associate professor of psychology in the Dietrich College of Humanities and Social Sciences, said that many people reported that they practiced meditation for stress reduction but they knew very little about how much people need to do for stress reduction and health benefits.

He further explained that when people initially learn mindfulness mediation practices, they have to cognitively work at it, especially during a stressful task and these active cognitive efforts might result in the task feeling less stressful but they might also have physiological costs with higher cortisol production.

Creswell`s team is now testing the possibility that mindfulness could become more automatic and easy to use with long-term mindfulness meditation training, which may result in reduced cortisol reactivity.

The study is published in the journal Psychoneuroendocrinology.