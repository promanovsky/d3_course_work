Music

Wayne Coyne, who hit the studio with the former Disney star on March 14, revealed that they were covering the legendary band's 1967 track.

Mar 17, 2014

AceShowbiz - The Flaming Lips' frontman Wayne Coyne revealed that he had teamed up with Miley Cyrus to cover The Beatles' "Lucy in the Sky with Diamonds". The unlikely collaboration also included Andrew VanWyngarden from MGMT.

On Friday, March 14, Coyne posted a photo of Cyrus rolling a joint in a recording studio and wrote, "Yup...... Recordin with Miley... High as f**k...." He later shared a photo of Cyrus recording the track, writing, "Miley in the studio vomiting diamonds while singing Lucy In The Sky With Diamonds!! If you're a Beatles and John Lennon freak you gonna love love love this track!! Got Andrew from MGMT too!!!!"

The collaboration has sparked anger among Beatles fans, though. "Seriously Wayne, go f**k yourself. None of you guys are worthy (especially Miley) of singing even one word of that song. What the hell?!" an Instagram user wrote under the photo. Another wrote, "OK THIS RUINED MY LIFE."

The Flaming Lips has tied a link to Cyrus last month when Coyne and Steven Drozd performed with her during her show at Los Angeles' Staples Center. They were performing the band's 2002 song "Yoshimi Battles the Pink Robots Part 1".