Two new studies describe the latest achievements in growing body parts in a lab and transplanting them into people, this time with nostrils and vaginas.

Windpipes, bladders, blood vessels and other structures have previously been created in part from a patient's own cells and then implanted. Eventually, scientists hope to tackle more complicated things like lungs and kidneys with this strategy, which is aimed at avoiding rejection of transplanted organs.

The latest experiments were published online Friday in the journal Lancet.

It is not a trivial thing to engineer a functional tissue. - Ivan Martin, University Hospital Basel

"They both show that by using fairly simple tissue engineering techniques, you can get real tissue forming where it's supposed to," said Dr. Martin Birchall, of The Ear Institute at University College London, who co-authored an accompanying commentary. He said the simple methods could be useful for making other body parts, including joint cartilage, bowels and the esophagus.

One experiment involved four teenage girls in Mexico who were born without vaginas because of a rare disorder. Currently, surgeons use tissue grafts to create vaginas for such patients, but that method carries a risk of complications.

The experimental results were reported by Dr. Anthony Atala of the Wake Forest University School of Medicine in Winston-Salem, North Carolina, with researchers there and at the Metropolitan Autonomous University in Mexico City. Atala said the procedure might also prove useful for replacing vaginas removed because of cancer, and repairing or replacing the organ after an injury.

For the experiment, researchers took a tissue sample less than half the size of a postage stamp from the patients' genitals. They multiplied cells from this tissue in the lab, seeded them onto a biodegradable scaffold and moulded it into the right size and shape for each patient before implantation.

'I have a normal life'

The first surgery was done in 2005, and the Lancet report provides a follow-up of the patients for an average of nearly seven years. The women report normal levels of sexual functioning, without any long-term complications. It is not known whether the women could get pregnant; only two have wombs, Atala said.

One of the women, in a video provided by the Mexican university, said she felt fortunate "because I have a normal life." The university didn't identify the woman.

In the other experiment, Swiss scientists built new outer nostrils for five patients who had skin cancer on their noses. When surgeons removed the tumour, they also took a tiny bit of nose cartilage. They grew the cells for four weeks in the lab to make a small flap. That was then implanted onto their nose and covered with skin from their foreheads. Normally, cartilage is taken from the patient's ear or ribs to recreate the nostril.

Ivan Martin of University Hospital Basel, the study's senior author, said none of the patients reported any side effects by one year after surgery, and all were satisfied with their new nostrils.

"Now that we have demonstrated this is safe and feasible, we can use (this technique) for more complicated clinical needs," he said, adding that the same approach is being tested in people to supply knee cartilage. He said scientists were slowly gaining more expertise in making body parts, but predicted it could take another couple of decades before the process becomes mainstream.

"It's not a trivial thing to engineer a functional tissue," he said.