AstraZeneca management must explain to company shareholders why it failed to engage further with Pfizer's takeover bid, Royal London Asset Management's Robert Talbut has said.

Pfizer, the American pharmaceutical company, is expected to make a statement on Monday admitting defeat in its attempt to take over AstraZeneca.

Mr Talbut, an AstraZeneca shareholder, said he wanted assurances that Pfizer's bid was turned down "for the right reasons".

First broadcast on BBC Radio 4's Today programme on Monday 26 May.