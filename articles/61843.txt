Software ecosystems are obviously among the most important parts of the mobile experience these days, and Apple’s is second to none. The company has managed to create an environment that fosters the creation of apps that are beautiful, useful and functional. Of course, these great apps are buried under a mountain of crap.

Part of our job here at BGR is to try to separate the great apps from the… not so great apps. We recently put together a collection of five fantastic apps that have beautiful designs and smart features, and now we’re continuing with five more great apps.

We try to pick apps from a wide range of categories for each post, and we’ve got a few terrific iPhone apps to show you this time around. As always, feel free to let us know about your favorite beautifully designed apps in the comments section here, or let me know on Twitter.

Wake Alarm Clock

Sometimes a certain type of app gets so popular that it becomes practically impossible to find great options because it involves sifting through so many low-quality apps.

This is definitely the case with alarms — but we’ve done the heavy lifting for you.

Wake Alarm Clock by Tiny Hearts Limited casts aside all of the needless functions that clutter many alarm apps, instead offering a simple solution to help you with one of the most important things you do each day: Wake up.

Featuring a sleek, minimalistic design, Wake Alarm Clock allows users to choose from a small collection of alarm sounds or to pick from their iTunes libraries. The controls are unique and impressive, and it also lets users set new alarms simply by speaking.

Even more important, especially if you’re not a morning person, are the three different alarm styles the app supports:

SLAP & FLIP: Equipped with a virtual Snooze Button and perfect for anyone craving a few extra moments of sleep.

SHAKE: Ideal for deep sleepers who need more than a traditional alarm clock to wake them from their stupor.

SWIPE: A simple option for dreamers who like to keep things basic.

Wake Alarm Clock is a $1.99 download in Apple’s App Store, which is more than fair for a terrific app that you’ll use every day.

Here’s a quick video to show you how the app works:

Crossfader

Some might say that the advent of digital music has rendered DJing a far simpler skill than it once was. Those people will find that their opinions are definitely reinforced by this nifty new iPhone app.

Crossfader by DJZ has a very short description on the App Store:

Create instant mashups and DJ flawlessly.

Remix and share with a global remix community. + Remix classic tunes with hot new electronic music.

+ Make a DJ Face and connect with DJs around the world.

+ The easiest DJing tool ever — move your phone to mix!

What that brief text fails to convey, however, is how awesome the interface is on this fun app that can make anyone feel like a DJ.

Crossfader uses a visual interface coupled with controls based on motion. Two songs are played simultaneously, perhaps the beat from one and vocals and instrumentation from another, and they are represented by album covers shown side by side. If an iPhone is held level to the floor, both songs will play at the same volume. Tilt the phone to one side, however, and the volume of the track on that side will increase as the album cover gets bigger.

There are plenty of songs to choose from and the app has social elements as well. It’s much more fun than a brief description will ever be able to explain, so your best bet is to head to the App Store to download this great free app now.

Checkmark 2

As has been the case since the dawn of the App Store, Apple’s mobile software portal is packed full of to-do and list apps. Maybe it’s because they’re so easy to make.

But just as poker is a game that is simple to learn but can take a lifetime to master, to-do apps are easy to churn out but surprisingly difficult to make well.

Snowman, it turns out, is one developer that has managed to make them well.

Checkmark 2 is a to-do and list app that strikes a wonderful balance between being full-featured while maintaining a certain minimalism and simplicity that similar apps lack. It includes all of the features that one might expect from a premium list app, but the interface is designed in such a way that they’re not overbearing.

Included in Checkmark 2 are standard lists and to-dos, location-based reminders, time-based reminders, custom alerts, iCloud sync and plenty more.

The app is currently on sale for half off, which makes it a $2.99 download in the iOS App Store.

Trunx

Some might consider Trunx the app and service that Apple’s iPhone camera and Photo Stream should be.

From the app’s description:

Trunx is an all-in-one photo app that lets you snap, organize and store in the cloud forever. Relive your memories like never before while freeing up valuable storage on your devices. NEW FEATURE – SHAREDPIX! SharedPix lets you create shared albums and collaborate with friends and family. Start an album, add photos and then invite others to join in. Experience photos in a new way with Trunx. Trunx creates a whole new photography experience by merging audio recording with pictures, creating a unique and incredible way to tell your life story. The Trunx EchoPix feature allows you to take and capture life moments in pictures while simultaneously recording sounds from that moment in time.

Photos are stored securely on Trunx’s servers and can be deleted from your iPhone to free up valuable space. They’re also presented in a great timeline interface that lets you scroll through virtual stacks of photos.

The Trunx app is free in Apple’s App Store. There’s also a promo right now that offers free lifetime unlimited online storage for any and all photos uploaded before May 1st. Of note, there is a desktop uploader as well, so you can upload all of your old photos with ease.

FridgePoems

Last up, we have a very fun little app that takes a great idea and makes it beautiful.

Remember those magnetic poetry kits that were packed full of words you could arrange on a refrigerator to create poems?

FridgePoems is that, but for 2014.

Using a terrific, brightly colored interface and word bundles that can be expanded, FridgePoem encourages users to create a new poem every day. It then saves each poem so users can look back at their masterful prose forever.

From the app’s description:

For 100’s of years, people with bad backs have not been able to write beautiful poetry on-the-go because they couldn’t carry a fridge with them. But now they can! With FridgePoems you can write beautiful poetry whenever you feel the urge to write a poem with not-always-correct grammar. With “Poem of the day”, you get a new set of words, a beautiful fridge and wallpaper combination each day. Challenge your friends, who can write the best poem? You have the exact same words to play with. And on special holidays, you might get a surprise… There are even more Word Packs to unlock, so let the poetry flow. Take a snapshot, save the poems to your gallery, send them to your friends or post them directly on your Facebook timeline. You can even customise the fridge and wallpaper, there is a selection of various fridges, colours and wallpapers to choose from.

FridgePoems is completely free in the App Store.

Looking for more gorgeous iPhone apps? Be sure to check out the first post in this series, which features Gist, Radium, Matchbook, Harmony and Peek Calendar.