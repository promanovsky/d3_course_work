'Draft Day' NY Screening: How Bill Belichick and Matt Leinart Helped Football Movie

UPDATED: Jennifer Garner also told THR what attracted her to the film, Kevin Costner revealed how he really feels about the draft and the writers discussed the advantages of the NFL's extensive involvement.

Draft Day has gotten a lot of attention for the NFL's participation in the film, which extended to individual coaches and players.

At a Bud Light-sponsored screening of the movie in New York Thursday night Denis Leary, who plays the coach of the Cleveland Browns, told THR that New England Patriots coach Bill Belichick, whom he contacted through a friend of a friend, helped them get the football details right.

"The script was great, but a lot of the football details Bill helped us out with and improved some of the scenes at the same time that he knew there were certain scenes that had to be a certain way because this was a movie," Leary told THR. "But he definitely made a difference, from my end of it anyway."

PHOTOS: 'Draft Day' Premiere

Belichick wasn't the only NFL figure to help out behind the scenes. Castmember Tom Welling, who plays incumbent Browns quarterback Brian Drew, said he spoke to a former top draft pick about life as an NFL quarterback.

"Matt Leinart was very gracious and spent a lot of time with me, talking," Welling told THR, adding that he spoke to a few other players. "But we didn't really talk about the game of football. We talked more about the lifestyle and what it means to live as an NFL quarterback and what it's like to be traded and the idea that there's always someone there trying to either take your job or knock your head off."

In addition, the NFL's participation allowed filmmakers to have commissioner Roger Goodell introduce the movie's fake draft picks and even use clips from NFL Films.

Writers Rajiv Joseph and Scott Rothman added that the access they got through the NFL helped them authentically portray what goes on behind the scenes at the draft.

REVIEW: Draft Day

"Rajiv and I had never been to a draft before," Rothman explained. "We're sort of making up what went on behind the scenes. [The NFL was] able to put us in touch with the people who were actually responsible for behind-the-scenes. And then when it came to shooting, they just allowed us access to everything, which just adds this whole level of authenticity through the whole thing."

Filmmakers also had access to the Browns' facility, and Joseph used his in-depth knowledge of the team as a fan to flesh out the screenplay once a better tax credit led to Draft Day being shot in Ohio instead of New York, where the script's original team of the Buffalo Bills is based.

"It would've been hard to write about the Browns [originally] because they are so close to my heart, but once we had the script done, it was a real joy to kind of personalize it and make it about my hometown because I know so much about Cleveland and about the fans," Joseph said. "So the script really took a huge step forward then."

STORY: 'Draft Day': What the Critics Are Saying

It was the script, which was on the 2012 Black List, that first attracted Jennifer Garner to the project, the actress told THR prior to Thursday night's screening.

"This script was on the Black List for a reason," she said. "The writing is crackling smart, super good. It's got a super-strong female character, which you don't get all the time, and you don't expect to see in a sports movie."

Meanwhile, star Kevin Costner told a group of reporters that even though he's in a movie that's centered around the NFL draft, and he's a huge football fan, he's not that interested in what really happens at the annual event.

"I don't really care about what happens at the draft after the first pick," he said.

The screening was followed by an after-party at FC Gotham.