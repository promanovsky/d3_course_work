Mysterious US bear raider Gotham City Research has forced one of Spain’s leading technology companies - wifi provider Gowex - into declaring bankruptcy, using just a blog post on a wordpress website.

Gotham, which says it focuses on “due diligence-based, special situation investing”, last week published a damning 93-page report on Gowex, alleging that over 90 per cent of the company’s revenues do not exist and claiming shares are worthless.

Around €870m was wiped off Madrid-listed Gowex’s value in two days as shares tumbled and trading in the company was suspended on Thursday. Yesterday, the company filed for bankruptcy as chief executive Jenaro García Martín admitted that at least four years of accounts had been falsified.

Mr García Martín, the company’s founder, said on Twitter: “I made the deposition and confession. I want to collaborate with the justice. I face the (sic) consecuencies.”

The confession comes less than a week after Gowex, which manages wifi hotspots in cities, declared that Gotham’s allegations were “categorically false”.

The scalp of a major European technology company has thrown Gotham into the spotlight. The mysterious outfit, which appears to have little online footprint beside its basic website and Twitter account, readily admits it is not a qualified investment advisor.

Gotham appears to be a classic ‘bear raider’, an investor that bets a company’s share price will fall and then speaks out publically about weaknesses or concerns with the business. The company admits on its website that it “may have long or short equity positions in the companies covered”.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Little is known about who sits behind Gotham, but investor Daniel Yu, known as @LongShortTrader on Twitter, is said to have an association with it.

A business by the name of Gotham City Research LLC was registered in the state of Delaware last February, shortly before Gotham began publishing reports online.

Delaware state law does not require someone to live in the state to register a business there and Gotham’s ‘registered agent’ is listed as Harvard Business Services, a company that specialises in helping people set up businesses in the state and charges $50 a year to act as an agent.

State records show Gotham’s status was listed as “ceased good standing” at the start of last month due to unpaid taxes, with $463.50 owed to the state. Delaware law does not require LLCs to file annual reports but they must pay an annual tax of $250.

Gotham has in the past targeted NASDAQ listed insurance outsourcer Ebix, retailer Tile Shop and online search specialist Blucora. Gotham hit headlines in the UK in April after targeting AIM-listed insurance outsourcer Quindell.