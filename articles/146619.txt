Ford Motor Co (F) is soon expected to name Mark Fields, currently chief operating officer, as its next chief executive officer in place of current CEO Alan Mulally, media reports said Monday citing people familiar with the matter.

Alan Mulally's planned retirement as CEO could come before the end of the year, probably in May, reports said. Fields emerged as Mulally's likely successor when he was promoted as COO in December 2012.

The transition, when it occurs, will mark the end of an illustrious career for Mulally, who steered Ford through the 2008 financial crisis and averted the automaker from relying on federal funds.

Mulally joined Ford as CEO in 2006, shortly after serving in executive positions at Boeing. At that time, he led the effort for Ford to borrow $23.6 billion by mortgaging all of its assets - a move that eventually stabilized Ford's financial position, while rivals General Motors and Chrysler went bankrupt at the height of the economic crisis.

Mulally has cut costs at the automaker and enriched its product portfolio by introducing flamboyant and fuel-efficient vehicles.

That has placed Dearborn, Michigan-based Ford on a positive trajectory and also a legacy, which Mulally's successor can comfortably step into.

The automaker is hoping to benefit from a strong Chinese market, some recovery in Europe, and of course, a robust North American operation marked by its flagship F-150 pickup truck. The automaker also stands to gain on the quality front when seen against GM's huge vehicle recalls.

Ford itself had been hinting at the retirement of Mulally, who in the meantime was linked to the top position at Microsoft (MSFT). But the software giant eventually named India-born Satya Nadella as CEO.

Ford stock closed Monday at $15.98, down $0.02 or 0.13%, on a volume of 24.9 million shares on the NYSE. In after hours, the stock dropped $0.01 or 0.06%.

For comments and feedback contact: editorial@rttnews.com

Business News