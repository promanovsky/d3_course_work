Amazon has reportedly unveiled a new handheld device called the Amazon Dash, which lets users order groceries from Amazon Fresh.

According to PC World, Dash is a remote sized device with a microphone and barcode scanner, which allows users to speak or scan their order from Amazon fresh.

Though users have to confirm their order using a smartphone, pc or tablet.

Dash comes with a laser barcode scanner, which is quicker and far more reliable than the other camera based scanners.

Dash is not a high maintenance product so you can hand it to children and don't have to worry about getting it messy.