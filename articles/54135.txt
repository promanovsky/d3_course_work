Ninja Turtles trailer (TMNT 2014) video more than a peek

This week the folks behind the release of the 2014 Michael Bay film Teenage Mutant Ninja Turtles have released the first full-length trailer. This trailer not only sets up the spot for the Shredder and April O’Neil – now Shredder’s friend’s daughter, apparently – it shoes the turtles themselves. Here we get the first official look at our four crime-fighting heroes.

This trailer is just 1 minute and 30 seconds long, but it shows well and above more than we’ve seen in the past several months. Inside you’ll see April using a cell phone, you’ll see the shredder mask, and you’ll see all four (bulky) turtles in action. Try not to freak out too much if you’ve never seen them in leaks before.

Below you’ll also see the first official plot summary. This film also includes the acting skills of Alan Ritchson, Jeremy Howard, Pete Ploszek, Noel Fisher, Will Arnett, Danny Woodburn, and William Fichtner, for starters. Screenwriters Josh Appelbaum, Andrew Nemec, Art Marcum, Matt Holloway, and John Fusco also get top credits.

“The city needs heroes. Darkness has settled over New York City as Shredder and his evil Foot Clan have an iron grip on everything from the police to the politicians. The future is grim until four unlikely outcast brothers rise from the sewers and discover their destiny as Teenage Mutant Ninja Turtles. The Turtles must work with fearless reporter April O’Neil (Megan Fox) and her wise-cracking cameraman Vern Fenwick (Will Arnett) to save the city and unravel Shredder’s diabolical plan.”

This film will be released on August 8th, 2014, and will be directed by Jonathan Liebesman. If you weren’t aware, the film isn’t directed by Michael Bay, it’s directed by Jonathan Liebesman – he previously directed Clash of the Titans. TMNT is produced by Michael Bay and Ian Bryce – how about them apples?