Studio executives tested an alternate version of Darren Aronofsky's forthcoming biblical epic Noah that opened with a montage of religious images and ended with a Christian rock song, it has been revealed.

Aronofsky said recently that he had won a battle with executives to screen his own version of Noah in cinemas after around half a dozen alternate cuts failed to find traction with evangelical filmgoers. Now a new profile of the film-maker in The New Yorker details the desperate lengths to which Paramount went to court religious audiences in the US, who had earlier turned their noses up at a test screening of Aronofksy's edit.

"In December, Paramount tested its fifth, and 'least Aronofskian', version of Noah: an 86-minute beatitude that began with a montage of religious imagery and ended with a Christian rock song," reveals the profile.

Fortunately for cinemagoers, the new cut scored lower than Aronofsky's own version had with Christian audiences. The New Yorker piece also reveals why executives felt they had to move forward with (now abandoned) alternate cuts in the first place: the Black Swan director, who gave up final cut on his film in exchange for a reported $160m (£96m) budget, was seemingly in no mood to compromise.

"Noah is the least biblical biblical film ever made," Aronofsky is quoted as saying. "I don't give a fuck about the test scores! My films are outside the scores. Ten men in a room trying to come up with their favourite ice cream are going to agree on vanilla. I'm the rocky road guy."

The New Yorker piece suggests Noah is far from the successor to Mel Gibson's The Passion of the Christ, which took $611m worldwide in 2004 after evangelicals flocked to see it, that Paramount had apparently been hoping for. Aronofksy's movie is said to feature a segment showing how Darwinian evolution transformed amoebas into apes, as well as what the director describes as "a huge [environmental] statement in the film … about the coming flood from global warming".

Earlier reports suggested religious audiences at test screenings for Aronofsky's cut disliked "dark" scenes in which Russell Crowe's Noah gets drunk and ponders taking extreme measures to wipe mankind from the face of the Earth. Many complained that the film inaccurately represented the biblical story upon which it is based, despite the fact that a scene in which Noah has one too many after finding land with his ark does appear in the Bible.

Paramount now appears to have given up on its efforts to market Noah to Christians, with the studio issuing a statement last month making clear that the movie is not intended as a direct translation. It also looks likely to be banned across large swaths of the Middle East and parts of north Africa for contravening Islamic rules on the depiction of prophets.

Aronofsky's film, starring Crowe, Jennifer Connelly, Emma Watson and Ray Winstone, hits the US on 28 March, with UK cinemas to follow on 4 April.