The Season 4 finale of Game of Thrones cued a lot of changes, and we’re not sure how the show is going to handle them. It could be bad.



Photo credit: HBO

Game of Thrones has made it clear that it isn’t strictly following the books. In fact, it’s already started jumping around a bit. While the TV series has taken some creative liberties, the overall story structure has remained the same. Because of this, we’re hesitant to enter into Season 5, which we assume will pick up with the fourth book, A Feast for Crows.

It’s widely agreed that A Feast for Crows is more of a downer in the series for multiple reasons, which we outline below. Of course, the fifth book brings it back in full force, so fear not, Game of Thrones fans. But there is a very real possibility the next season in the show will be kind of rough compared to the awesomeness of Season 4.

WARNING: While this article doesn’t go into the gritty detail about the events in Book 4, it does contain some spoilers about what’s to come if the show follows the novels. Do not read unless you want to know what’s ahead on Game of Thrones.

1. Not chronological

The thing about the fourth and fifth books, like a lot of the Song of Ice and Fire series, is that they aren’t chronological. They’re more character- and location-focused. So, while you see events unfold for certain characters, other characters are left out completely. We’re not quite sure how the show is going to handle this decision by George R. R. Martin, since the Game of Thrones series is generally chronological. We’re assuming Season 5 will be the same.

2. Not as much action

Forget giant battles at The Wall and huge sieges at King’s Landing. The stuff with Brienne gets pretty intense, but otherwise, the battles kind of die down in the fourth book.

3. Lots of new faces

Doran Martell, Arys Oakheart and Arianne Martell all enter the scene with Cersei’s daughter Myrcella Baratheon in their care. It isn’t until the end of the book that we really feel invested in their presence in the series, so we’re not sure how the show is going to incorporate them when the other characters are already so beloved.

4. No more Joffrey hating

He was the king we all loved to hate. And, sure, watching him finally get what he deserved was well worth the wait. But admit it: You kind of miss him now. All those incredible memes floating around the internet just aren’t as satisfying as experiencing the actions that spur the online conversations each week during the show.

5. The disappearance of Tyrion

Tyrion (Peter Dinklage) is smuggled out of King’s Landing and not heard from in the fourth book. We don’t have any chapters from him for the first time in the series, and the heavy cloud of what has happened to him is a black spot over A Feast for Crows.

6. Arya gets mystic

Arya starts an independent quest of her own that takes her to an unexpected place. She is removed from the action of the outside world and is forced to focus inward in the fourth book. It’s good growth for her character, but not the most exciting sequence of events in the world of Westeros.

7. Lots of Cersei being Cersei

Now that Tywin is dead, Cersei (Lena Headey) is living it up, exercising her rights over King’s Landing and, frankly, not doing a very good job of it all. Even Jaime resents her.

8. No Dany

Like, literally. No Dany. Our favorite heroine is off building her empire, and we see none of it. Of course, the fifth book is called A Dance With Dragons, so let’s assume Martin makes it up to us. But life is rough without Daenerys.

9. Rise of the Greyjoys

There is just something so annoying about the Greyjoys. They’re those people who are so set in their ways, they’re blind to the reality of everything around them. It’s infuriating. And we have to read through their character chapters front and center in the fourth book. If only characters could actually hear you when you yell at the pages.

10. Relationships die

Thanks to all the deaths in the Season 4 finale, a lot of the relationships we loved watching week after week are now gone for good. Take Arya and the Hound, for example. Or Tyrion and Shae. Tywin is no longer around to stand in Cersei’s way, and Ygritte isn’t out there to tempt Jon Snow. Change is scary.

How do you think the show will handle the fourth book? Do you think the series can pull it off?