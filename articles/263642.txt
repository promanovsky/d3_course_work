In a new public statement , Hachette has denied Amazon's offer to fund 50% of a pool of money for authors affected by the ongoing disagreement between the book publisher and the e-commerce site.

For the last several weeks, Amazon has been removing pre-order buttons for Hachette books, tweaking its recommendation to feature non-Hachette titles, and ordering fewer copies of certain books, causing shipping delays. This is all because of a pricing negotiation disagreement between the two companies.

Advertisement

This has been bad news for Hachette authors like J.K. Rowling, James Patterson, Dave Cullen Nicole Mary Kelby , and many more, who don't get royalties when they're book sales decline because of Amazon's actions.Amazon offered to fund 50% of an author pool to mitigate the impact of the the two businesses' dispute on author royalties if Hachette would fun the other 50%, but Hachette said that it would not accept any money from Amazon until after the company has agreed to more favorable terms overall. Amazon, for its part, wrote in its statement earlier that it didn't see a resolution coming any time soon.

As long as both companies refuse to back down, authors will continue to be negatively affected.

Advertisement

Hachette also wrote that it is "extremely grateful for the spontaneous outpouring of support" that it has recieved from authors and agents.

Here's the full statement from Hachette:

It is good to see Amazon acknowledge that its business decisions significantly affect authors' lives. For reasons of their own, Amazon has limited its customers' ability to buy more than 5,000 Hachette titles. Authors, with whom we at Hachette have been partners for nearly two centuries, engage in a complex and difficult mission to communicate with readers. In addition to royalties, they are concerned with audience, career, culture, education, art, entertainment, and connection. By preventing its customers from connecting with these authors' books, Amazon indicates that it considers books to be like any other consumer good. They are not. We will spare no effort to resume normal business relations with Amazon-which has been a great partner for years-but under terms that value appropriately for the years ahead the author's unique role in creating books, and the publisher's role in editing, marketing, and distributing them, at the same time that it recognizes Amazon's importance as a retailer and innovator. Once we have reached such an agreement, we will be happy to discuss with Amazon its ideas about compensating authors for the damage its demand for improved terms may have done them, and to pass along any payments it considers appropriate. In the meantime, we are extremely grateful for the spontaneous outpouring of support we have received both privately and publicly from authors and agents. We will continue to communicate with them promptly as this situation develops.

Disclosure: Jeff Bezos is an investor in Business Insider through his personal investment company Bezos Expeditions.