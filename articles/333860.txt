According to a new study, the longer one takes low-dose aspirin, the lower the risk of pancreatic cancer even if aspirin is taken daily for three years lower the risk by 48 percent.

While there are potential risks from aspirin use, the benefits for each person must be assessed based on individual characteristics.

U.S. researchers suggest that people who regularly took low and dosages of aspirin for a couple of years to prevent heart diseases significantly lowered their risk of developing cancer. Doctors from Yale University also said that those who took aspirin 20 years before the research lowered their risk by 60 percent.

The team recruited the subjects from 30 general hospitals in Connecticut from 2005 to 2009. There was a total of 362 pancreatic cancer patients and 690 in the control group. The subjects were personally interviewed to determine the time they started to use aspirin, how long they used it, whether they used low or regular dose of aspirin, the time they stopped taking it, among others. The team also confounded factors such as body mass index, history of diabetes and smoking.

Low dose is between 75mg and 325mg of aspirin taken once daily to prevent heart disease. Regular doses are considered high doses of aspirin often taken four times daily to relieve pain. Among the subjects, 96 percent of those who took low doses and 92 percent of those who took regular doses reported using the drug daily.

Among the subjects, around 92 percent were non-Hispanic whites, 57 percent were males, around 49 percent were current or former smokers and 19 percents was diagnosed with diabetes in the last three years before the study.

People who develop pancreatic cancer undergo several different physiological changes like taste disorder for example. These changes start to occur around two to three years prior to pancreatic cancer diagnosis. These patients are more likely to stop using aspirin, which is why separating different patterns of pancreatic cancer risk and aspirin use may be complex.

Recently, more information is learned about inflammation in cancer but there are still ongoing investigations studying the role of anti-inflammatory drugs in lessening the recurrence of other tumor types.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.