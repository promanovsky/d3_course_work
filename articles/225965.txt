Shutterstock photo

Gold ( GLD , quote ) prices continue to move lower this morning as market participants wait for Federal Reserve's meeting minutes scheduled to be released tomorrow at 2pm EDT.

Adding pressure to this morning is the World Gold Council's Gold Demand Trend report indicating that the world's largest gold consumer, China ( , ), dropped by 18% in the first quarter. Most notably in the report was the drop of 39% (283 tons) in demand for gold bars and gold coins.

Market participants will be watching very closely as Charles Plosser (Federal Reserve Bank of Philadelphia) and William Dudley (Federal Reserve Bank of New York) give speeches at 12:30 and 1:30 EDT respectively.

Adding pressure to gold prices this morning is the World Gold Council's Gold Demand Trend report indicating that the world's largest gold consumer, China ( FXI , quote ), dropped by 18% in the first quarter. Most notably in the report was the drop of 39% (283 tons) in demand for gold bars and gold coins.

Although gold normally reacts to global conflicts, it doesn't seem to be reacting sharply to the heightened tensions between Russia ( RSX , quote ) and Ukraine.

However, the conflict should be watched closely by anyone short gold at the moment as Ukraine is scheduled to hold presidential elections this weekend on May 25.

Look for increase interference from Russia in the voting process along with an increase in tensions as elections approach.

European and U.S. leaders have already warned Russia any interference will result in additional sanctions.

Gold has been trading in a sideways channel since mid-April from $1,305 to $1,280 (open and close). Price is in a pretty tight range for channel traders using the gold ETF (GLD) as a vehicle to take advantage of the channel. I'd rather place entry orders 15 cents above and below the high and low of the channel and grab any potential breakout through the Monday after the elections.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.