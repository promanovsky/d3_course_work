Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

A downcast Zayn Malik is seen for the first time since a shocking video was published online showing him smoke a suspicious roll-up .

The sombre teen idol looks tired with bags under his eyes while sporting the early stages of unkempt facial hair.

These exclusive pictures show the scandal-hit One Direction singer outside The Lowry Hotel in Salford as he reflects on a turbulent 24 hours.

The 20-year-old was sat in the front passenger seat wearing a white polo T-shirt and a black beanie.

He tried his best to keep a low profile and looked visibly forlorn as a member of his security guarded the Jeep.

(Image: Jon Baxter / MEN)

Parents will be concerned after footage, believed to be from the band's recent stay in Peru, shows Zayn and Louis Tomlinson giggle in hysterics as they pass a 'joint' to each other.

It's reported the film was taken a few hours before they later performed at the Estadio Nacional stadium in Lima.

A speaker, believed to be Louis, says: “So here we are, leaving Peru. Joint lit. Happy days!”

He then asks: “What do you think about that kind of content?” Zayn, 21, replies: “Very controversial.”

Liam Payne, Harry Styles and Niall Horan, all 20, were not in the car during the 15-minute journey.

Later in the video, posted by Mail Online, Louis pans to a police motorcyclist just yards away.

Laughing Louis, 22, says: “One nil b***h! Look at this b***h! He’s having a look. He’s thinking, ‘I’m sure I can smell an illegal substance in there.’

“And he’s hit the nail on the head.”

Earlier, as they drove into the sunlight from an underground parking lot, Louis says, “I want to light up,” and he is handed a lighter.

At one point, Louis adopts a faux serious tone and says: “Zayn takes his job very seriously. He makes sure he goes through a two hour intense warm-up before every show.

(Image: WENN)

“One very very important factor of Zayn’s warm-up of course if Mary J herself. In fact I will present it to him now for some fantastic singing.”

Last night a spokesman for One Direction said: “The matter is in the hands of our lawyers.”

Professor Green said last night: “Poor 1d. . . . I just don’t get why they cause people such torment, two blokes having a giggle in the back of a car.”