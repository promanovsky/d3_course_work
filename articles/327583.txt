The Android 4.4.4 KitKat began rolling out a few days ago for the Google Nexus 5, Nexus 7, Nexus 4, and Nexus 10–just weeks after 4.4.3 KitKat came out.

According to reports on Friday, 4.4.4 KitKat arrived for the Sony Xperia Z1 Compact, while firmware was certified for the Xperia Z1 and Z Ultra.

The Xperia Blog writes, “Somewhat surprisingly, Sony Mobile has started to roll the Android 4.4.4 KitKat firmware update for the Xperia Z1 Compact. This comes to the handset in the form of build number 14.4.A.0.108.

“The update is currently rolling to users in Russia, Italy and Indonesia. We don’t have a changelog right now.”

Gotta be Mobile reported that it includes a number of bug fixes, mainly regarding security.

However, users have been saying there’s problems.

“Nexus users have taken to Google’s Nexus product forums to complain about a series of Android 4.4.4 KitKat problems impacting the Nexus 7, Nexus 10, Nexus 5, and Nexus 4. We saw complaints emerge at the end of last week but now that the smoke has settled a bit, the complaints have grown in number and include several problem areas,” says Gotta be Mobile.

Some of the 4.4.4 problems relate to applications like the BBC iPlayer.

AP Samsung update: Google shows off Android Auto, wearables

SAN FRANCISCO (AP) — Some 1 billion people are now using Android devices, Google said as the company kicked off its two-day developer conference Wednesday in San Francisco.

But the online search leader’s effort to broaden its focus beyond smartphones and tablets was on full display as the company unveiled far-reaching plans to push further into the living room, the family car and the TV set.

As part of a nearly three-hour opening presentation, Google gave more details about Android Wear, a version of the operating system customized for wearable gadgets such as smartwatches. The company also introduced Android Auto, which has been tailored to work with cars. Android TV, meanwhile, is optimized for TV-watching, aided by a recommendation system and voice searches for things like “Breaking Bad” or “Oscar-nominated movies from 2002.”

About 6,000 developers, bloggers and journalists flocked to the event. Following Google’s recent revelation that showed that just 30 percent of its employees are women, the company touted that the number of women attending its conference grew to 20 percent this year from 8 percent a year earlier.

“We were joking, that we’re at a tech conference and there is a line to the bathroom,” said developer Tricia Barton, referring to a common occurrence at tech events — long lines to the men’s bathrooms while the women’s facilities sit mostly empty. Barton was one of some 1,000 female I/O attendees that Google invited to networking dinners the night before the conference, at restaurants around San Francisco.

Wednesday’s three-hour keynote address was interrupted at several points by protesters who were quickly escorted out. Google has been the subject of disapproval for its use of shuttle buses to ferry its employees from San Francisco to its Mountain View headquarters. The buses have become a symbol of the divide between Silicon Valley’s tech millionaires and those left out of the latest boom.

Nonetheless, Google loyalty was apparent among conference attendees, many of whom wore Google Glass, sported Google shirts and collected Android-themed freebies in between workshops, which range from the likes of “Large scale data processing in the Cloud” to “Polymer and the Web Components revolution.”

Google’s I/O event —a rally of sorts designed to get developers excited about creating apps and devices for Google’s ecosystem— comes at a time of transition for the company, which makes most of its money from advertising thanks to its status as the world’s leader in online search. The company is trying to adjust to an ongoing shift to smartphones and tablet computers from desktop and laptop PCs. Though mobile advertising is growing rapidly, advertising aimed at PC users still generates more money.

At the same time, Google is angling to stay at the forefront of innovation by taking gambles on new, sometimes unproven technologies that take years to pay off —if at all. Driverless cars, Google Glass, smartwatches and thinking thermostats are just some of its more far-off bets.

On the home front, Google’s Nest Labs —which makes network-connected thermostats and smoke detectors— announced earlier this week that it has created a program that allows outside developers, from tiny startups to large companies such as Whirlpool and Mercedes-Benz, to fashion software and “new experiences” for its products.

Integration with Mercedes-Benz, for example, might mean that a car can notify a Nest thermostat when it’s getting close to home, so the device can have the home’s temperature adjusted to the driver’s liking before he or she arrives.

Nest’s founder, Tony Fadell, is an Apple veteran who helped design the iPod and the iPhone. Google bought the company earlier this year for $3.2 billion.

Opening the Nest platform to outside developers will allow Google to move into the emerging market for connected, smart home devices. Experts expect that this so-called “Internet of Things” phenomenon will change the way people use technology in much the same way that smartphones have changed life since the introduction of Apple’s iPhone seven years ago.

In March, Google released “Android Wear,” a version of its operating system tailored to computerized wristwatches and other wearable devices. Although there are already several smartwatches on the market, the devices are more popular with gadget geeks and fitness fanatics than regular consumers. But Google could help change that with Android Wear. Android, after all, is already the world’s most popular smartphone operating system.

At I/O, the company announced three new smartwatches running Android Wear. The LG G and the SamsungGear Live are available on Wednesday. A third one, Motorola’s Moto 360, is coming later this summer — an announcement that drew groans from the audience, which had hoped for an earlier date.

Intent on reaching the billions of people who don’t have a smartphone or even Internet access, Google also unveiled an initiative called Android One, designed to help manufacturers build low-cost smartphones for emerging markets such as India.

The company also showed off features of the next version of Android, which goes by the temporary code name “Android L.” It’s been designed to work across all devices as long as the gadgets are running the operating system, so a user can check email on a smartwatch, answer the message on a smartphone and then delete it on a computer, for example.

Android L will also look different, include more animation, colors and a feature called “material design,” which lets developers add shadows and seams to give visuals on a phone’s screen the appearance of depth.