Climate change rears its ugly head again and looks like it's got Alaska communities in its sights.

In a study titled "Ocean Acidification Risk Assessment for Alaska's Fishery Sector" published in the journal Progress in Oceanography, Jeremy Mathis and his team shows what ocean acidification will do to Alaskan communities exactly, what with the area particularly susceptible to changing pH levels in the water. Ocean acidification is not restricted to Alaskan waters but the area is considered to be ground zero for the phenomenon, a devastating effect of increased carbon dioxide emissions in the world.

The Alaskan coastline is 50 times longer than other U.S. Coastlines and is responsible for producing half of all the seafood commercially caught in the country. Red king crabs and salmon are particularly in danger, although for different reasons. Ocean acidification makes it harder for crustaceans to build their shells, which affects salmon because the fish feeds on a small kind of snail called pteropods. Salmon may not be directly affected by ocean acidification but it will still clearly feel its effects.

Dwindling stock will be a problem for Alaska because its fishing industry supports more than 100,000 jobs, generating over $5 billion in revenues every year. Beyond economic implications though, about 17 percent of the Alaskan population (around 120,000 individuals) turn to fishing for everyday sustenance. The study also showed that those communities that rely on the fishing industry the most will be the hardest hit by ocean acidification.

"A lot of those places are almost solely reliant on the fishing industry. It's like a stock portfolio. If you don't have any diversification, you have a lot of risk," explained Mathis.

To help alleviate the effects of ocean acidification on the state, the study proposed that different jobs be made available in Alaska, highlighting the importance of job training programs, new infrastructure investments and increased opportunities for schooling.

Oceans absorb about a third of the carbon dioxide in the atmosphere so when emission levels rise, so do too concentrations in the water. The higher carbon dioxide levels are in the water, the higher the level of acidity which results to ocean acidification. All oceans can undergo acidification but it happens more rapidly where the waters are cold such as in Alaska because more carbon dioxide is absorbed. It also doesn't help that natural circulation patterns push naturally acidic water from the deep up to the surface.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.