Gravitational waves, a prediction which grew out of Albert Einsten's Theory General Relativity in 1916, may have finally been detected. This could provide the best evidence yet for an even stranger theory.

For nearly 100 years, astronomers have been searching for these waves. This finding appears to be the first direct measurement ever made of these features. Astronomers still have not seen the waves themselves, but new observations show their effect on a feature left over from the Universe. Gravitational waves are produced by anybody as it moves through space, like waves off the bow of a speedboat. These waves bend space-time itself, and are most pronounced around pairs of orbiting quasars or black holes.

This latest discovery was made by a team led by Harvard astronomer, John Kovac.

Detection of this effect provides strong evidence in support of the theory of inflation. This idea suggests that during a tiny fraction of the first second after the Big Bang, the early Universe expanded faster than the speed of light. At the time this occurred, the Universe was only about one-billionth the size of a proton, but it quickly expanded during that period.

This theory was first put forward by Alan Guth, from the Massachusetts Institute of Technology (MIT), in order to explain how the Universe came to have such an even distribution of matter. Just 300,000 to 400,000 years after the Big Bang, small temperature differences around the early cosmos would lead to the clusters and super-clusters of galaxies that we see today.

One of the predictions of the inflation theory was a particular polarization pattern in the cosmic microwave background (CMB) radiation. This "echo" of the big bang was first detected by a pair of engineers at Bell Labs. When researchers looked at the modern data, the polarization patterns seen almost perfectly matched those predicted 30 years ago.

"The signal is compatible with models which I proposed a long time ago, so for me, this is fantastic news. For the general theory of relativity, for Einstein's theory, it's fantastic news because the gravitational waves is part of Einstein's theory, never seen - just like the discovery of the Higgs boson was necessary for proving the standard model of particles," Andrei Linde, a Stanford physicist who help developed Guth's ideas, told the press.

The BICEP2, a detector located near the South Pole, was used to measure the waves for the first time. Although detecting the waves themselves remains elusive, this research showed the effect these waves had on background radiation.

The Bell engineers who first detected the cosmic background radiation were awarded the Noble Prize for their work. Linde believes that if this discovery is confirmed, Kovac also deserves to win the highest prize in science.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.