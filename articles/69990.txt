Surgeons at a hospital in Pittsburgh, Pennsylvania are conducting the first human trials into the use of suspended animation, whereby a patient's body is cooled down and suspended between life and death in order to buy time for doctors to save their lives.

According to New Scientist, the first 10 potentially fatal gunshot or knife wound victims to be brought to the UPMC Presbyterian hospital's emergency room at the end of March will be subject to a ground-breaking technique, whereby on-call surgeons will replace all the patient's blood with a saline solution.

The replacement of blood with saline will render the patient clinically dead.

The operation, which has so far only been successfully performed on pigs, cools the body rapidly to temperatures as low as 10 degrees Celsius. Almost all brain activity stops, meaning that the patient is not breathing and has no pulse.

"If a patient comes to us two hours after dying you can't bring them back to life. But if they're dying and you suspend them, you have a chance to bring them back after their structural problems have been fixed," said surgeon Dr Peter Rhee at the University of Arizona in Tucson, who helped develop the technique.

Controversial trial

While the trial could potentially be a breakthrough in emergency healthcare, it is controversial as neither the patient nor their family can give consent to the suspended animation treatment.

The FDA has approved the trial only because life and death situations are considered to be exempt from informed consent.

The process of cooling a body, also known as inducing hypothermia, has been around for decades. At the normal body temperature of 37 degrees Celsius, when the heart stops beating, blood no longer carries oxygen to the brain, and the brain can only survive for at most five minutes without oxygen.

The drop in body temperature slows cell functions so they don't require as much oxygen, which gives surgeons up to 45 minutes in which to attempt to fix major structural damage to the body.

Two hours to save a life

In 2002, a team of researchers at the University of Michigan Hospital led by Hasan Alam successfully carried out the technique on pigs. The animals were sedated and a massive haemorrhage was induced to mimic the effect of multiple gunshot wounds.

Using suspended animation, doctors would now have two hours in which to save a life. Once the operation is complete, the saline will be replaced by new blood, which will slowly heat the body.

The surgeons at UPMC Presbyterian Hospital plan to compare the outcome of this trial against the results of operations on 10 other emergency room patients who fit the right criteria but did not undergo suspended animation since the on-call surgeons were not available.

"We've always assumed that you can't bring back the dead, but it's a matter of when you pickle the cells," said Rhee.