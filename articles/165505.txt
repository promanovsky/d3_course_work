In a speech this month, Yellen said the Fed ‘‘must respond to significant unexpected twists and turns the economy may take.’’

Gone are the benchmarks that her predecessor, Ben Bernanke, used to try to guide investors: That by a certain point in the future or when unemployment reached a specific rate, the Fed would consider slowing its stimulus for the economy.

WASHINGTON — In her first weeks as Federal Reserve chairwoman, Janet Yellen has made one thing clear: The Fed will keep all options open in deciding when to raise interest rates from record lows.

On Wednesday, when it ends a policy meeting, the Fed will likely repeat that theme and echo a point it made after Yellen’s first meeting as chairwoman last month: That even after the job market strengthens and the Fed starts raising rates, it will probably keep rates unusually low to support a still-subpar economy.

Advertisement

Yellen’s message of flexibility may help convey the Fed’s willingness to respond to abrupt shifts in the economy. Yet it can be tricky; it can leave investors uncertain and fearful of a sudden shift in the Fed’s approach to interest rates. Financial markets hate uncertainty.

The Fed will be meeting in a week when the government will issue a flurry of reports on the economy — from manufacturing growth and consumer spending to home prices, consumer confidence, economic expansion, and job gains.

And they are among the many indicators Yellen has stressed the Fed must monitor to fully assess the economy’s health and decide when to start raising rates.

That message marks a shift from the approach Bernanke took over the past five years as the Fed struggled to strengthen the economy after the Great Recession. Under his leadership, the Fed sought to be as publicly specific as possible about its intentions. And it did so by focusing primarily on the unemployment rate.

Advertisement

In December 2012, for example, the Fed said it intended to keep its benchmark short-term rate near zero at least as long as unemployment remained above 6.5 percent. The idea was to signal roughly how long the Fed was committed to keeping borrowing rates at record lows to spur spending and economic growth.

Yellen has said the unemployment rate, now 6.7 percent, overstates the health of the job market and economy and that the Fed must assess a range of barometers. She has expressed concern, for example, that a high percentage of the unemployed — 37 percent — have been out of work for six months or more and that pay is scarcely rising for people who do have jobs.

Some economists say Yellen’s decision to shift away from the Bernanke Fed’s approach of providing specific guideposts for a future rate increase — its ‘‘forward guidance’’ — carries risks.

‘‘The whole idea of forward guidance is to project this air of clarity and confidence, and that is not happening,’’ said David Jones of DMJ Advisors. ‘‘This is a time when the markets need confidence in the Fed.’’

Yellen also faces skepticism from some fellow Fed members on inflation. The Fed favors consumer inflation of about 2 percent annually. It becomes concerned if inflation goes too much above or below that target. The inflation index the Fed monitors most closely is measuring about 1 percent.

Some critics on the Fed say its efforts to keep rates super-low have elevated the risk of igniting inflation or inflating bubbles in assets like stocks or homes. Others counter that inflation remains too far below the Fed’s target and that rates should be kept historically low.

Advertisement

No major changes are expected in a statement the Fed will release when its meeting ends. But it will likely announce a fourth reduction in its monthly bond purchases. Those purchases have been intended to keep long-term loan rates low.

The Fed has cut its monthly bond buying from $85 billion in $10 billion increments to $55 billion.

If the economy keeps improving, it will likely keep paring its purchases until ending them late this year. The pullback is expected despite tough challenges the economy faces, from a slowing housing recovery to anemic economic growth last quarter resulting in part from a brutal winter.

For investors, the big question is when the Fed might start raising its key short-term rate from zero, where it’s stood since late 2008.

After last month’s meeting, the Fed said it expected to keep the rate near zero ‘‘for a considerable time’’ after it ends its bond purchases. Asked at a news conference to define a ‘‘considerable time,’’ Yellen said, ‘‘It probably means something on the order of around six months or that type of thing.’’

Stock prices sank on her mention of ‘‘six months’’ because it seemed to signal that the first rate increase would occur sooner than investors had assumed. In subsequent appearances, Yellen has deemphasized any plan to raise rates at a specific time. Rather, she has suggested that the job market remains far from fully healthy — a hint that the first rate increase might occur well into the future.