Kim Kardashian and Kanye West switch tuxedos and lace for matching 'Just Married' biker jackets at their lavish Italian wedding



They both wore exquisite custom-made Givenchy for their Italian nuptials on Saturday.



But after Kim Kardashian and Kanye West said their vows, they took a moment to replace the fancy tuxedos and lace with edgy black 'Just Married' leather jackets.



The newlyweds made use of their wedding photobooth to share snaps of the fun garments - which were said to have been ordered by the rapper - as they continued their loved-up festivities at Forte di Belvedere in Florence, Italy.

Scroll down for video

'Just married!' Kim Kardashian and Kanye West took a moment away from the lace and tuxedos at their wedding in Florence, Italy on Saturday to don these edgy, creative leather jackets custom-made for them

An endearing black and white photo shows the reality star, 33, and her musical genius husband, 36, standing with their backs to the camera to showcase the full greatness of their slick getup.



Kanye - whose jacket reads 'Just' - glances over his shoulder with an enigmatic gaze.



Kim - whose jacket reads 'Married' to complete the phrase relevant to their special day - shoots the lens her signature pout as she stands close to her husband's side.

Photobooth love: The newlyweds also shared this image of the two of them sharing a sweet kiss, which was also released by E! in the first look inside the wedding of the year

According to E!, who released the image, their cleverly matching jackets were customized by artist Wes Lang, who created a bird-like silhouette which contained a strike of lightning beneath the text.



Schott NYC was the designer responsible for the jacket Kanye wore, whilst the Keeping Up With The Kardashians star wore a similar one made by BLK DNM.



It was a fun and unique look for the pair, who had spent their wedding and the days leading up to it in a multitude of fancy outfits.

Special day: Kim Kardashian and Kanye West shared a kiss after exchanging vows at Forte di Belvedere

'Mr. and Mrs. West': The smiling newlyweds made their way down the aisle past their family and friends, including Kris and Bruce Jenner, their baby girl North West, Kourtney and Khloe Kardashian, Kendall and Kylie Jenner, and Scott Disick

A series of other jaw-dropping photos from the wedding of the year were also released by E!, giving the first official glimpse into the couple's extravagant occasion.



One heart-warming image shows Kim and Kanye kissing after exchanging their vows, on the backdrop of an enormous wonderwall of white flowers and in front of 600 of their loved ones.



Another shows the couple walking hand-in-hand down the aisle with everyone that witnessed their nuptials applauding and smiling for the happy pair.



The married couple also shared a snap of a romantic kiss they shared in the photobooth.

'Breathtaking': Kris Jenner, mother of the bride, shared this image of her daughter Kim's incredible wedding dress by Givenchy Haute Couture

Kim's stunning mermaid-silhouette gown that featuring delicate white lace was custom-made by Givenchy Haute Couture.



Small panels of a sheer lace decorated the arm and also accentuated her slimmed down waist.



The backless dress was long-sleeved with a train, while a dramatic flowing white cathedral-length silk veil completed the look.



Speaking moments after the ceremony to E! News, who film the family's reality show Keeping Up With The Kardashians, Kris said she was 'beyond bursting with happiness for Kim, Kanye and baby North'.

Happy in love: Kim and Kanye took another photo together in the photobooth looking loved-up

'Such a magical, romantic wedding!' she added. 'I feel blessed to have my new son and his family as part of ours.'



And Kanye seemed equally ebullient, with a source telling MailOnline he gave an eight-minute speech to the gathered guests.



The couple's special day was witnessed by the entire Kardashian-Jenner family except for Rob Kardashian and Brody Jenner, and their daughter North West was even dressed in a dress to match her mother's as she sat in the mother-of-the bride's arms.



After the wedding, Kim and Kanye headed to Ireland for a rumoured five-day long honeymoon, where they are said to have visited the luxurious Victorian estate Castle Oliver in County Limerick.