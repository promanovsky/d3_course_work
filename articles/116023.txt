Sound familiar? Your normally cheerful spouse has suddenly, and inexplicably, turned cranky and an otherwise pleasant day is fast becoming a scene from “Who’s Afraid of Virginia Woolf.”

When you see those storm clouds gathering in your significant other’s eyes, you might do well to give them some carbohydrates -- and fast.

At least that’s the advice of a team of researchers who examined the connection between low blood sugar levels and aggression in married couples.

The paper, which was published Monday in PNAS, found that when blood glucose levels dropped, spouses were far more likely to stick pins into voodoo dolls representing their mates. They were also more likely to blast loud noises into earphones strapped to their mate’s head.

Advertisement

The authors argue that loss of self-control is a contributing factor to aggression between intimate partners, and that self-control is linked to nutrition.

“Self-control requires energy, and that energy is provided in part by glucose,” wrote lead study author Brad Bushman, a professor of communication and psychology at Ohio State University.

“Glucose is made from nutritious intake that becomes converted into neurotransmitters that provide energy for brain processes. Low glucose levels can undermine self-control because people have insufficient energy to overcome challenges and unwanted impulses,” wrote Bushman and his colleagues.

The authors described the phenomenon of lashing out aggressively due to hunger with the slang term “hanger.”

Advertisement

To test their hypothesis, authors studied 107 married couples.

For three weeks, the spouses’ glucose levels were checked in the morning, before breakfast, and in the evening, before bed. They were also asked to perform a unique task at the end of the day, and to record the results.

“To obtain daily measures of aggressive inclinations toward their partner, each participant received a voodoo doll along with 51 pins and was told: ‘This doll represents your spouse. At the end of each day ... insert between 0 and 51 pins in the doll, depending how angry you are with your spouse,” the authors wrote.

Then, at the end of 21 days, they played a “game” in the lab that was designed to measure aggressive behavior.

Advertisement

“Participants were told that they would compete with their spouse to see who could press a button faster when a target square turned red on the computer, and that the winner on each trial could blast the loser with loud noise through headphones,” authors wrote.

The noises, they were told, included recordings of fingernails scratching a chalkboard, a dentist’s drill and an ambulance siren. A volume dial allowed the punishment-inflicting spouses to choose between silence and a raucous 105 decibels. They could also adjust the duration of the punishment -- from half a second to five seconds, they were told

In reality, the game, and the punishment, were a sham.

Instead of competing against each other, the spouses were playing the computer in a rigged outcome. Also, they weren’t blasting their partners’ ears. Because they were seated in separate rooms, they could not tell it was all a ruse.

Advertisement

Nonetheless, lower glucose levels translated to more pins stuck in the doll and longer, more intense noise settings, authors wrote.

“Results suggest that interventions designed to provide individuals with metabolic energy might foster more harmonious couple interactions,” the authors concluded.

Food, they wrote, might also be of use in aggressive situations that did not involve spouses.

“Food could be a potential tool for curbing aggression by bolstering resources for effective self-control. This intervention might be especially important in stressful settings in which aggression might erupt, such as in prisons, psychiatric hospitals and schools.”