Switching over to daylight saving time, and losing one hour of sleep, raised the risk of having a heart attack the following Monday by 25 per cent, compared to other Mondays during the year, according to a new US study released on Saturday.

By contrast, heart attack risk fell 21 per cent later in the year, on the Tuesday after the clock was returned to standard time, and people got an extra hour's sleep.

Losing one hour of sleep raises the risk of having a heart attack, according to a new study. Credit:Peter Rae

The not-so-subtle impact of moving the clock forward and backward was seen in a comparison of hospital admissions from a database of non-federal Michigan hospitals. It examined admissions before the start of daylight saving time and the Monday immediately after, for four consecutive years.

In general, heart attacks historically occur most often on Monday mornings, maybe due to the stress of starting a new work week and inherent changes in our sleep-wake cycle, said Dr. Amneet Sandhu, a cardiology fellow at the University of Colorado in Denver who led the study.