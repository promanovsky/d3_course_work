It’s no secret that the public loved Chris Martin and Gwyneth Paltrow together and would have liked to see them as a pair for much longer. But on Tuesday, the couple called it quits after more than a decade of seemingly sturdy marriage. One can’t help but wonder — what was the straw that finally broke the camel’s back? Here are eight wacky theories that could explain why one of Hollywood’s most loved-up couples had to call it quits:

1. Martin couldn’t sustain himself on a diet of Paltrow’s gluten-free quinoa granola recipe alone. He considered turning Coldplay gigs into acoustic sit-down shows because he didn’t have the energy to stay upright for more than 20 minutes.

2. Paltrow was tired of hearing the same song over and over again. There were only so many identical-sounding demos she could supportively sit through before the deceit of saying “you’ve really evolved” kept her up at night.

3. They realised that they named their daughter, Apple, after the most mainstream fruit. They constantly wondered if they had sold out by failing to champion more indie fruits such as fig or papaya.

4. Martin was jealous of Paltrow’s shiny mane. With the same rigid tuft of fur sitting on his head for years, Martin dreamt about having enough hair to rock a middle part like Paltrow, but he knew a Nick Carter-esque bob revival wouldn’t be in the cards.

5. Martin was secretly in love with Claire Danes. He wrote the Paltrow doppelgänger letters of longing and kept them stacked in a cardboard box underneath hundreds of Viva La Vida copies that he bought himself.

6. Paltrow was a closeted Kings of Leon fan. Though she claimed she never heard any of their songs, Martin came home early from tour to find his wife baking kale while listening to Use Somebody on repeat.

7. Martin was a self-proclaimed “Gleek” — i.e. a diehard Glee fan — and though he was elated to see Paltrow as part of his favourite show, he grew increasingly frustrated when she wouldn’t “put in a good word” for him to score a cameo.

8. Martin can’t sleep without his Grammy Awards strategically placed around his head like a halo. Paltrow thought it was just a sweet idiosyncrasy at first, but the distance it put between them became unbearable by the time he won his seventh award.