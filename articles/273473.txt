Among the big announcements at Apple's World Wide Developers Conference on Monday, the new Swift programming language got one of the biggest cheers from developer attendees with its promise of easier, faster coding for iOS and OSX apps, which could translate to quicker product releases to consumers.

Apple calls Swift the fastest language yet, as it can replace long lines of code with a simple character, which will potentially remove common coding errors that plague developers and prolong product releases.

The new language, to replace the existing Objective-C language used for iOS and OSX programming, will need to be learned, of course, but Apple has provided interested programmers with a free iBook tutorial.

Swift is a new programming language for creating iOS and OS X apps. Swift builds on the best of C and Objective-C, without the constraints of C compatibility. Swift adopts safe programming patterns and adds modern features to make programming easier, more flexible, and more fun. Swift’s clean slate, backed by the mature and much-loved Cocoa and Cocoa Touch frameworks, is an opportunity to reimagine how software development works.

The free tutorial provides:

- A tour of the language.

- A detailed guide delving into each language feature.

- A formal reference for the language.

The iBook can be read on iPhone, iPad or iTunes. Find it here.

As Objective-C has been used for several decades, it will take some time for Swift to become fully integrated into the iOS and OSX ecosystem. But the promised ease and fun of this new language might even get nonprogrammers to start thinking about a career in the hot field of coding.