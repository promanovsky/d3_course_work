Italian researchers have developed a special Cardio-Renal Pediatric Dialysis Emergency Machine for newborns and small infants who are battling renal health issues.

The CARPEDIEM device, announced late this week in The Lancet, is the first of its kind, according to a research abstract and was created as peritoneal dialysis, which had been the therapy for infant with acute kidney injury, was not proving feasible or effective.

"We aimed to design and create a CRRT (continuous renal replacement therapy) machine specifically for neonates and small infants," state researchers in the study abstract, nothing the development of the new machine was a five-year project. The device meets regulatory requirements and is now being used to treat critically ill infants.

"The CARPEDIEM CRRT machine can be used to provide various treatment modalities and support for multiple organ dysfunction in neonates and small infants," state the research abstract.

"A major problem is the potential for errors in ultrafiltration volumes. Adult dialysis equipment has a tendency to either withdraw too much fluid from a child, leading to dehydration and loss of blood pressure, or too little fluid, leading to high blood pressure and edema," said Prof. Claudio Ronco, of San Bortolo Hospital in Italy.

According to a new report acute kidney failure impacts 18 percent of low-weight infants and 20 percent of those children require intensive care.

The clinicians, states a report, chose the device name because of its potential for use for multiple indications.

"The investigators should be commended for their efforts in this important area and for their use of several collaborators," writes Benjamin L. Laskin, MD, a nephrologist at the Children's Hospital of Philadelphia, Pennsylvania, and Bethany J. Foster, MD, a nephrologist at Montreal Children's Hospital, Quebec, Canada, in a note accompanying the research.

TAG Babies, Dialysis, renal, therapy, Study, Research

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.