Europe steps in to prevent Bulgaria's banks going in to meltdown with £1.4 BILLION emergency credit following 'plot' to undermine system

European Commission approves £1.4 billion emergency credit line to stabilise Bulgaria's banks

Police have arrested five people for alleged 'actions undermining the banking system'

President Rosen Plevneliev said 'there is no banking crisis but a crisis in confidence and criminal attacks'

The European Commission has approved a £1.4 billion emergency credit line to stabilise Bulgaria's banks following what the government called a criminal plot to undermine the banking system.

Shares in the banks soared after the Commission approved the credit line being offered by the Bulgarian government.



Rumours of liquidity shortfalls caused panic and runs on deposits at First Investment Bank on Friday, despite the authorities' insistence there was no reason to worry.

The European Commisson has approved a £1.4 billion emergency credit line to stabilise Bulgaria's banks following what the government called a criminal plot to undermine them

Customers continued to withdraw money today from the country's third-largest bank, although the queues were visibly smaller than on Friday when the bank had to close early after people rushed to withdraw deposits, Associated Press has reported.

Bulgaria's fourth-largest lender, Corpbank, had already been put under special central bank supervision after suffering a run a week ago.

Police have arrested five people for alleged 'actions undermining the banking system'.

According to police the detainees used text messages, e-mails and phone calls to people across the country 'to spread false information that caused detriment to commercial banks and destabilized the banking system.'

President Rosen Plevneliev, who convened party leaders at an emergency meeting on Sunday, said 'there is no banking crisis but a crisis in confidence and criminal attacks.'

'We have sufficient reserves, means and tools to deal with any attempt at destabilization, and we stand behind each bank that becomes the target of an attack,' he said.

President Rosen Plevneliev, who convened party leaders at an emergency meeting on Sunday, said 'there is no banking crisis but a crisis in confidence and criminal attacks'

The EU Commission, the 28-nation bloc's antitrust authority, cleared the aid for Bulgaria's banks, saying it doesn't violate state aid rules. It said 'the Bulgarian banking system is well capitalized.'

Plevneliev said the country's currency will continue to remain pegged against the euro despite the volatility in the financial system.

In 1997, Bulgaria's currency, the lev, was pegged at a fixed rate to the euro under a currency board arrangement, which imposed strict financial rules aimed at restoring fiscal discipline, preventing deficit spending and controlling inflation.