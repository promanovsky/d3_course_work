"Luckey has held himself out to the public as the visionary developer of virtual reality technology, when in fact the key technology Luckey used to establish Oculus was developed by ZeniMax," the company said in a statement about the lawsuit.

AD

In a statement, Oculus denied the accusations and said it would vigorously defend against the claims. “The lawsuit filed by ZeniMax has no merit whatsoever," Oculus said. "As we have previously said, ZeniMax did not contribute to any Oculus technology."

AD

The debate centers on who owns development work done by both companies when its virtual reality headset was just a prototype. At that time, Oculus chief technology officer John Carmack was a ZeniMax employee and was working on virtual reality technology. Carmack, a well-respected game developer, announced he would join Oculus in August 2013. ZeniMax is accusing Carmack of taking key virtual reality technology with him to Oculus and using it in their headset.

According to a copy of the court filing shared with The Washington Post, the relationship started when Carmack contacted Luckey in 2012 after being impressed by material about the Rift that he'd read online. Luckey, in turn, sent a Rift prototype to ZeniMax's offices.

AD

From there, the complaint says, Carmack -- still a ZeniMax employee -- worked with others at the game company to help develop the Rift ahead of a showing at the Electronic Entertainment Expo, the video game industry's largest trade show. Following that show, the Rift project gained wide exposure and eventually raised $2 million on Kickstarter -- growing momentum that eventually led to its Facebook acquisition.

AD

The complaint says that Luckey and ZeniMax then signed a non-disclosure agreement in 2012 that gave the game publisher "exclusive ownership of the confidential and proprietary information" covered under the agreement -- namely what Carmack and other employees contributed to the Rift. But as the project gained more public attention, ZeniMax alleges, Luckey began violating that agreement and was not properly crediting ZeniMax for its contributions.

ZeniMax had asked for financial compensation and equity stake in Oculus in exchange for the contributions it claims to have made to the Rift, according to the complaint. But efforts to settle the matter out of court failed and, ZeniMax claims, forced its hand.