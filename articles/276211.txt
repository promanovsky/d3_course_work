Consumer prices in the euro zone are stable rather than too low, former European Central Bank (ECB) board member Jürgen Stark said, countering a widely held view that the current low level of inflation could derail the economic recovery in the euro zone.

"I miss the term price stability…Low inflation and price stability will boost real disposable income and will help to foster private consumption. This is good news," Stark told CNBC in an interview.

Consumer prices rose by 0.5 percent year-on-year in May, according to official statistics released by Eurostat earlier this month. This marked a fall from April's 0.7 percent and is well below the ECB's target of inflation below of close to, but below 2 percent.

Read MoreWave of confidence boosting euro zone: Trichet

He said the International Monetary Fund, the Organisation of Economic Cooperation and Development and the ECB were all participating in a discussion about "too low" inflation which was "irrational". The decline of consumer prices index was mainly driven by decline in oil and commodity prices and was also a reflection of the ongoing adjustment of prices in the periphery. That adjustment was "urgently needed" he said.

"One can really understand and explain why the inflation rate is so low and…it is in my view not a threat," Stark said. "The discussion, all in all, in my view, is really irrational."

In the medium-term, inflation will move closer to 2 percent again, according to the German former governing council member.

Stark, a well-known "hawk", resigned in September 2011 amid disagreement over the central bank's purchase of government bonds to prop up troubled euro zone member states. Germany has repeatedly voiced concern that such purchases go beyond the ECB's mandate and are not part of its main task of safeguarding price stability.