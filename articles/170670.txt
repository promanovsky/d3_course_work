A new study suggests that teens and young adults who have been prescribed high doses of antidepressants are two times more likely to display suicidal behaviors.

Researchers from the Harvard School of Public Health in Boston led by Dr. Matthew Miller, associate director of the Injury Control Research Center, analyzed data from more than 162,000 patients diagnosed with depression. Their ages ranged from 10 to 64 - all had been on some kind of medication between 1998 and 2010.

The study focused on commonly-prescribed antidepressants such as Celexa, Zoloft and Prozac. The recommended dosage for these medications per day is 20mg for Celexa, 50mg for Zoloft, and 20mg for Prozac. Eighteen percent of the participants were prescribed with more than the recommended dosage.

To check if the antidepressants were linked to increased suicidal behaviors in patients, the researchers looked at their medical records for self-harm reports during the first year of medication.

The findings revealed that subjects 24 years old and below taking higher doses of drugs were two times more likely to inflict self-harm. Three percent of the recorded self-harm incidents came from people who took more than the recommended dosage, while the rate was only 1.5 percent for those who took the proper amounts of antidepressants. Furthermore, the suicidal tendencies of those who took more than the recommended dosage were prevalent within the first 90 days.

"If I were a parent, I definitely wouldn't want my child to start on a higher dose of these drugs," said Dr. Miller to Healthday News.

Miller maintained that further study is needed, as researchers did not consider other factors that may have caused the teens to hurt themselves. They also failed to explain why the results were age-dependent, as well as the reason doctors prescribed higher than the recommended dosage.

The study was published on the April 28 issue of JAMA Internal Medicine.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.