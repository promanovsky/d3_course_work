From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

A new report — one that contradicts others — indicates that late Apple cofounder and CEO Steve Jobs had said his company would not be releasing a TV.

The report comes from information in a new book about Apple from a former Wall Street Journal reporter, Yukari Iwatani Kane. Called Haunted Empire: Apple After Steve Jobs, the book said Jobs told a 2010 top-secret meeting of the Top 100 executives, managers, and employees at Apple that TV was a bad business.

It was Jobs’ last attendance at the annual Top 100 meeting, when his weakness and difficulty walking reflected his poor health.

According to Kane, Jobs sat in front of everyone on the last day of the meeting. He told them, “You’ve got Steve Jobs sitting right here” and invited questions of any kind about the company. Someone asked if Apple planned to release a TV next, since there were rumors.

Jobs reportedly said no, adding that “TV is a terrible business. They don’t turn over, and the margins suck.” But according to the book, he did indicate he wanted to control the living room and intended to continue the Apple TV.

Some observers at the time reportedly felt that Jobs’ response was intended to keep his company focused on the products at hand, and didn’t necessarily close the door on television. Those comments had previously been reported in the Journal in 2011.

And then there’s Walter Isaacson’s take on Jobs’ feelings about TV.

The former Time editor wrote a best-selling biography of Jobs that featured extensive interviews with the Apple visionary. Isaacson’s book quotes Jobs as saying:

“I’d like to create an integrated television set that is completely easy to use. It would be seamlessly synced with all of your devices and with iCloud. It will have the simplest user interface you could imagine. I finally cracked it.”

Given that Apple reinvented the music industry, the mobile phone industry, and the publishing process and helped to reinvent the book industry, it would stand to reason that television, as one of the last major unconquered entertainment industries, is on the company’s hit list.

Although Jobs referred to the $99 set-top Apple TV device as a “hobby,” it’s a hobby than had more than a billion dollars in revenue last year in hardware and content.

A new and updated Apple TV could debut next month, and there continues to be persistent reports that Apple is working on a TV set and expanded content.

But the question remains: Did Jobs solve the problem of the TV interface, and possibly the problem of the TV industry’s business model as well?

As they say in the TV industry, stay tuned.