This week Don Draper returns for the first half of Mad Men's seventh and final season.

Since we left him the TV world has been gripped by Breaking Bad and stunned by True Detective, but Mad Men is an entirely different kind of drama, favouring slow-burning character development over bloodshed.

So ahead of season seven, here are seven predictions for Mad Men's seven biggest characters.

Don Draper (Jon Hamm)

We left Don at his lowest point, but not without a glimmer of redemption ahead of his final 14 episodes on our screens. He's hit rock bottom and has been suspended indefinitely by the company he helped build, but with work taken away from him he finally has time to put his life in some form of order.

Don ended Season 6 with his children, outside the whore house where the real Don – Dick Whitman – led a troubled childhood.

Throughout the season we saw foreshadowing of a death, but it wasn't a literal death, rather the beginning of the end of Don Draper and the beginning of him becoming his true self, Dick Whitman. Will he succeed in finding redemption? I don't think so, but he will come close and may well save his daughter in the process.

Sally Draper (Kiernan Shipka)

Speaking of Don's daughter... Sally is without a doubt the most intriguing character on the show, and last season she went through the ringer having walked in on her father cheating and tasting alcohol for the first time at boarding school.

After staying the night at school and getting drunk for the first time, she becomes closer to her mother Betty with the realisation that her father – not just her mother – is a pretty horrible human being.

Being the product of two majorly messed-up individuals, Sally could very well end up more than a little troubled herself – but we reckon she'll end up just fine and eventually learn from her parents' numerous mistakes.

Peggy Olsen (Elizabeth Moss)

Peggy has always been the heart and soul of the show and her meteoric rise through the world of advertising has been a constant of the entire series. Her single-minded pursuit of success has been clear but it has come at the detriment of her personal life.

She accidentally stabbed one boyfriend in the stomach, and an affair with her boss Ted – with whom things might have worked – failed when he decided to move to California with his family in an attempt to start afresh.

Season 6 ended with her literally in Don's chair - so where next? She still has to prove she's just as good as Don, but we reckon she'll quit advertising by the series' end to pursue personal happiness instead. She'll return one day, but she won't be lost to her work.

Pete Campbell (Vincent Kartheiser)

Nothing ever goes right for Pete Campbell. The ever-balding, slimy version of Don (well slimier, or a different kind of slime at least) has a broken marriage, a child he rarely sees and a probably-murdered mother. He's also been bested by Bob Benson, the office's closest homosexual who's escaping a former life and identity.

Having given up on it all, Pete has decided to join Ted in California. He'll be happy there, but not for long we suspect. Viewers can a tearful attempt at reconciliation with his wife and a short-lived attempt at being a better man.

Joan Harris (Christina Hendricks)

Joan has a child and despite Roger Sterling's attempts to rekindle their relationship it was revealed that she is seeing the aforementioned Bob Benson, who is getting closer to the son of Joan and Roger. It's soap opera territory but you know it'll all take a Mad Men twist.

Bob has been placed in Mad Men is a sort of ticking time bomb (or time bob) who will go off at some point. When the proverbial does hit the ceiling fan, it will be spectacular. Joan may well end up with Roger – the least screwed up of her recent men – but as for her career I don't see her becoming the respected partner she longs to be.

Roger Sterling (John Slattery)

More drinking, more wise-cracks. That's all we need. Having opened up to the drugs and love of the sixties, Roger is more spiritual than he's ever been, but he still desperately wants a family.

He's back in the life of his infant son Kevin, but really wants to be in Joan's life as well. His older daughter wants nothing to do with him. He will attempt to reconcile with both, but of all the character's on this list, Roger's trjectory is the hardest to predict.

Betty Draper (January Jones)

Side-lined in recent seasons, Betty has passed through a bizarre fat stage to come out more confident than ever.

There's not a whole lot that can done with her character. So I believe she'll be side-lined again, by choice or necessity, as Matthew Weiner focuses on other characters instead.

Mad Men returns to Sky Atlantic on Wednesday 16 April at 10pm.