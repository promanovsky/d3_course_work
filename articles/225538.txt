After 10 years of trading, GoPro look set to become the first multi-million dollar tech IPO which owes its success - at least in part - to the curiosity of pigs.

Well, we’re exaggerating slightly, but a video captured by one of the company’s ruggedized cameras as it was dropped by a skydiver and landed in a pig pen is just one of the many viral videos that have helped create the GoPro brand.

With a range of accessories that lets customers strap their GoPro to pretty much anything, the company’s high-definition cameras have captured a stunning array of footage and proved an excellent source for the sort of clips that YouTube loves.

We’ve gathered a list of some of our favourites, but if the company’s IPO filing is to be believed then these sorts of videos could soon be coming to a TV near you.

GoPro, who currently make all of their money from hardware, has ambitions to become a “media entity,” having already secured a channel on Virgin America’s in-flight entertainment system and with plans to introduce something similar for Microsoft’s Xbox One console.

It’s unlikely that these sorts of ventures will create significant revenue for the company (they don’t currently make any money at all) but they could prove a good way to put their products in front of new customers, showing them just what their cameras can do.

1 Skydivers drop a camera into a pig pen

2 Eagle's eye view in the mountains

3 A hipster and a cat go for a bike ride

4 'Full Circle' short film

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

5 Fireman saves a kitten

6 Slow-motion hand-held fireworks