People are living longer, which is good. But old age often brings a decline in mental faculties and many researchers are looking for ways to slow or halt such decline.

One group doing so is led by Dena Dubal of the University of California, San Francisco, and Lennart Mucke of the Gladstone Institutes, also in San Francisco. Dr Dubal and Dr Mucke have been studying the role in ageing of klotho, a protein encoded by a gene called KL.

A particular version of this gene, KL-VS, promotes longevity. One way it does so is by reducing age-related heart disease. Dr Dubal and Dr Mucke wondered if it might have similar powers over age-related cognitive decline.

Advertisement

What they found was startling. KL-VS did not curb decline, but it did boost cognitive faculties regardless of a person's age by the equivalent of about six IQ points. If this result, just published in Cell Reports, is confirmed, KL-VS will be the most important genetic agent of non-pathological variation in intelligence yet discovered.

Dr Dubal and Dr Mucke made their discovery when they looked at 220 volunteers aged 52 to 85, to study the effects of KL-VS on ageing. They assessed their volunteers' faculties of memory, attention, visuo-spatial awareness and language. From these, they constructed a composite measure of cognition.

Advertisement

That measure suggested people with a VS version of the KL gene in their chromosomes had better cognition than those without one.

When they analysed data collected by two other groups who work independently on KL-VS they discovered these researchers had found the same thing. That comparison brought the number of people examined to 718, a fifth of whom were possessors of KL-VS.

Advertisement

The six-point IQ gap is an extrapolation, since the cognitive tests did not measure general intelligence directly. But if it is correct, variation in the KL gene could account for as much as 3% of the variation of IQ in the general population (or, rather, in the population from which the researchers' samples were drawn, namely white Americans). In comparison, the previous record-holders, HMGA2 and NPTN, each account for only half a percent of that variation. This sort of result, it must be cautioned, has a tendency to come and go. The genome has so many genes in it that flukey correlations between one of them and some human trait are common.

But there are two reasons to believe this is not a fluke. One is that these three independent studies have found it. The second is that Dr Dubal and Dr Mucke did not rest on their laurels, but did some experiments on mice to investigate KL-VS's actions.

Up The Junction

To do this they added the murine equivalent of KL-VS to the genomes of some mice. Doing this increases klotho levels in mice (an effect also seen in KL-VS-positive people). The genetically engineered animals did much better than regular mice at learning how to navigate mazes and other memory tests which psychologists like to inflict on their subjects.

Advertisement

And analysis of their brain tissue revealed differences from regular mice in the structure of their synapses, the junctions between nerve cells (illustrated above) that act as neural switches.

Signals cross synapses in chemical form. The most common messenger chemical, known as glutamate, is picked up by the receiving cell using molecules called NMDA receptors. It is known from previous work that glutamate stimulation of NMDA, or the lack of it, can strengthen or weaken synaptic connections. This is believed to be the basis of memory.

The team's genetic engineering changed the nature of the NMDA receptors in the mice's hippocampuses and frontal cortices--two regions of the brain particularly involved in memory formation--by doubling in them the number of a particular sort of molecular subunit, GluN2B.

Advertisement

Previous research has found links between GluN2B levels and cognitive performance. Dr Dubal and Dr Mucke discovered that blocking GluN2B with a drug called ifenprodil abolished the genetically engineered mice's advantage.That suggests klotho works its magic, at least in part, by increasing the number of GluN2B subunits in the NMDA receptors of the brain's memory and learning circuits.

Dr Dubal and Dr Mucke hope, despite their failure to show any protective effect of KL-VS on age-related cognitive decline, that this knowledge may be put to use.

Advertisement

A drug that elevates klotho levels, or mimics that protein's function, might indeed enhance cognition, and there is no obvious reason why such a drug should be restricted to the elderly.

If it could be developed everyone--except, maybe, those already in possession of a copy of KL-VS in their genes--might be able to take pills to make themselves a little brighter.

Advertisement

Click here to subscribe to The Economist