Los Angeles: Talk show host Oprah Winfrey has unearthed her first ever audition tape and it seems that she wasn’t always the polished TV star, she is today.

The billionaire media mogul shares a tape from 1983 when she was just a young TV personality trying to her career to next level. After hearing that a Chicago-based morning show needed a new host, Winfrey stayed up all night with an editor to put together an audition tape, reports showbizspy.com.

The 60-year-old got the gig and within few months channel AM Chicago trumped 'The Phil Donahue Show' presented by TV host Phil Donahue to become the highest rating talk show in Chicago.

By 1986, the show was renamed 'The Oprah Winfrey Show', which was broadcast nationally, again shooting to the top of the ratings.

“In a field dominated by white males, she is a black female of ample bulk. As interviewers go, she is no match for, say, Phil Donahue... What she lacks in journalistic toughness, she makes up for in plain-spoken curiosity, robust humour and, above all empathy," said TIME magazine.