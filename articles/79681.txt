If you ever have trouble reading someone’s mood it could be because you haven’t learned how to interpret the full range of human emotion.

From happy to disgustedly surprised, scientists have identified 21 different facial expressions. The research, which was carried out using new computer software, more than triples the long-accepted figure of six emotional states.

Dr Aleix Martinez, from Ohio State University in the US, said: “We've gone beyond facial expressions for simple emotions like 'happy' or 'sad.' We found a strong consistency in how people move their facial muscles to express 21 categories of emotions.

”That is simply stunning. That tells us that these 21 emotions are expressed in the same way by nearly everyone, at least in our culture."

In future, the computer model could aid the diagnosis and treatment of mental conditions such as autism and post-traumatic stress disorder (PTSD), said the researchers.

In pictures: 21 facial expressions Show all 20 1 /20 In pictures: 21 facial expressions In pictures: 21 facial expressions Awed In pictures: 21 facial expressions Angry surprised In pictures: 21 facial expressions Angrily disgusted In pictures: 21 facial expressions Angry In pictures: 21 facial expressions Disgusted In pictures: 21 facial expressions Disgustedly surprised In pictures: 21 facial expressions Fearful In pictures: 21 facial expressions Fearfully angry In pictures: 21 facial expressions Fearfully disgusted In pictures: 21 facial expressions Fearfully surprised In pictures: 21 facial expressions Happily disgusted In pictures: 21 facial expressions Happily surprised In pictures: 21 facial expressions Happy In pictures: 21 facial expressions Hate In pictures: 21 facial expressions Sad In pictures: 21 facial expressions Sadly angry In pictures: 21 facial expressions Sadly disgusted In pictures: 21 facial expressions Sadly fearful In pictures: 21 facial expressions Sadly surprised In pictures: 21 facial expressions Surprised

Since ancient times, leading thinkers have tried to understand how and why our faces betray our feelings. Scientists such as Aristotle, and Darwin have long promoted the idea that people’s faces express only a handful of basic emotions - happy, sad, fearful, angry, surprised and disgusted.

Today, cognitive scientists try to link facial expressions to emotions in order to track the genetic and chemical pathways that govern emotion in the brain.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

But restricting emotions to just six categories is like painting only using primary colours, said Dr Martinez. The new study, published in the journal Proceedings of the National Academy of Sciences, has tripled the colour palette available to researchers.

"In cognitive science, we have this basic assumption that the brain is a computer," Dr Martinez added.

"So we want to find the algorithm implemented in our brain that allows us to recognise emotion in facial expressions.

“In the past, when we were trying to decode that algorithm using only those six basic emotion categories, we were having tremendous difficulty. Hopefully with the addition of more categories, we'll now have a better way of decoding and analysing the algorithm in the brain.”

The researchers photographed 230 mostly student volunteers – 100 male, 130 female – pulling faces in response to verbal cues designed to trigger different emotional states.

The words “you just got some great unexpected news”, for instance, produced an expression that was “happily surprised”, while “you smell a bad odour” prompted a “disgusted” face.

A search was then made for similarities or differences between the 5,000 resulting images and an expression database widely used in body language analysis called the Facial Action Coding System (Facs).

This yielded 21 emotions which included subtle combinations of the “basic six”.

From left to right: happy, disgusted and happily disgusted (PA)

Tagging prominent landmarks for facial muscles, such as the corners of the mouth or the outer edge of the eyebrow, enabled the scientists to match emotions to movement.

Putting on a “happy” face was done the same way by almost everyone, with 99 per cent of study participants drawing up the cheeks and smiling. “Surprise” involved widening the eyes and opening the mouth 92 per cent of the time.

“Happily surprised” was marked by wide-open eyes, raised cheeks, and a mouth that was both open and stretched into a smile.

Another hybrid emotion, “happily disgusted”, created an expression that combined the scrunched up eyes and nose of “disgusted” with the smile of “happy”.

It was the emotion felt when something “gross” happens that is also incredibly funny, Dr Martinez explained.

He described how the research might help people with PTSD who were likely to be unusually attuned to anger and fear.

“Can we speculate that they will be tuned to all the compound emotions that involve anger or fear, and perhaps be super-tuned to something like 'angrily fearful'?” said Dr Martinez.

“What are the pathways, the chemicals in the brain that activate those emotions?

"We can make more hypotheses now, and test them. Then eventually we can begin to understand these disorders much better, and develop therapies or medicine to alleviate them."

Here are the 21 facial expressions:

Happy

Sad

Fearful

Angry

Surprised

Disgusted

Happily Surprised

Happily Disgusted

Sadly Fearful

Sadly Angry

Sadly Surprised

Sadly Disgusted

Fearfully Angry

Fearfully Surprised

Fearfully Disgusted

Angrily Surprised

Angrily Disgusted

Disgustedly Surprised

Appalled

Hatred

Awed