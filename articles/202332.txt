NBC has cancelled Revolution, Believe, Crisis and Growing Up Fisher.

The network confirmed today (May 9) that the JJ Abrams-produced post-apocalyptic drama series will not be granted a third season.



Revolution starred Billy Burke, Tracy Spiridakos, Elizabeth Mitchell, Zak Orth and Giancarlo Esposito as survivors in a world where all electricity was disabled on a single night.

The series debuted on NBC in September 2012, and was created by Supernatural's Eric Kripke.

Abrams also produced the now-cancelled Believe along with Alfonso Cuarón, while political thriller Crisis has been dropped as well.



On the comedy front, Growing Up Fisher has also been axed by the Peacock Network.

The freshman comedy show starred Eli Baker as an 11-year-old boy trying to cope with the divorce of his blind father (JK Simmons) and his eccentric mother (Jenna Elfman).

NBC also cancelled cult hit Community today, seemingly bringing an end to the show's "six seasons and a movie" campaign.

Earlier this week, NBC renewed Hannibal and About a Boy, in addition to giving series orders to DC Comics adaptation Constantine and the Debra Messing drama Mysteries of Laura.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io