Ladies, Rick Fox is a single man again!

The Boston Globe reported that Fox and his longtime girlfriend Eliza Dushku have split up after dating for five years. Distance reportedly played a major role in the former couple's separation. The 33-year-old actress recently moved to Boston from Los Angeles to be closer to her family.

"Rick's an LA guy and I'm a Boston girl," Dushku told the newspaper in an interview. "Nobody in L.A. has a basement. They all have the obligatory storage spaces in the Valley. ... I'd rather be a little physically cold here than emotionally cold in L.A. I missed my town and I missed my family."

The actress, who is famous for her role in "Buffy the Vampire Slayer," said she bought a condo near the Watertown home where she grew up and is planning to go back to school. Dushku did not mention what school she will be attending or what subject she plans to study.

The couple began dating in 2009. In 2010, Fox made his dancing debut on ABC's "Dancing With the Stars" and the former NBA player was supported by Dushku.

The actress hinted that she was ready for a change during an interview with The Improper Bostonian Magazine.

After living in Los Angeles for 15 years, I definitely grew out of being an L.A. hater, but I think there's a level of truthfulness and individuality here that is refreshing in a business where everyone conforms to certain standards," Dushku told the publication. "I just sort of come alive when I'm home."

Fox guest co-host "the View" on Tuesday but did not mention his recently-changed relationship status.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.