Call of Duty publisher Activision is betting $500 million that you’re going to love Destiny, a role-playing-flavored first-person online shooter from Halo developer Bungie.

With the steep price comes a lot of expectation. Considered by many to be one of the flagship titles of the new console generation, Destiny is Bungie’s first new series since releasing 2001’s Halo: Combat Evolved. Although it’s still in alpha-testing, I played through a small sample of what Destiny will offer gamers when it launches on consoles Sept. 9, and I found a polished and competent experience that will feel familiar to anyone who’s spent time in Master Chief’s space boots.

Image Credit: GamesBeat

Saving the galaxy, one bullet at a time

In Destiny, you are a Guardian tasked with protecting the last safe city on Earth from various alien threats. Like many role-playing games, it gives you a decent amount of character creation options. You can choose one of three races — Human, Exo, or Awoken — and pick from various hair styles, eye colors, and markings. You can also pick one of three classes — Hunter, Titan, or Warlock. Each class has its own talent tree filled with active and passive abilities that level up as you use them. I chose the Warlock for my playthrough, an arcane warrior who blasts enemies with bolts of purple space magic. After choosing an “Explore” mission, I arrived with little fanfare in an old Russian launch port called Cosmodrome, a snowy landscape filled with abandoned, crumbling buildings and satellite arrays. There, I was free to roam around and pick up beacons, each one containing an objective.

Most of those objectives involved shooting aliens in the face or scouting the area. Destiny combat is quite satisfying. The Warlock’s rechargeable ability, Nova Bomb, is powerful and fun to use, and the guns all have a noticeable heft. Battles are frenetic games of peek-a-boo, where you fire off a full clip and then duck behind a wall to reload and recharge your shields. Although this alpha didn’t have a tutorial, I took to the controls immediately. Bungie isn’t straying too far from the Halo formula, creating a game that feels accessible, if not revolutionary.

Next, I tried a Strike mission called “The Devil’s Lair,” and it’s here that my time in Destiny really shined. Unlike the earlier Explore mission, I now had a singular purpose — find a big bad called Sepiks Prime and take it down. After the matchmaking system teamed me with two other players, we headed back into the dark corridors of the Cosmodrome in search of our prey.

The path to Sepiks Prime was fairly linear and punctuated by a number of scripted events. One such event forced the team to hunker down and defend itself from waves of Fallen while our A.I. companion Ghost (voiced by Game of Thrones‘ Peter Dinklage) hacked into an old piece of equipment. The second was a boss battle versus a big six-legged mech that could easily one-shot a Guardian caught directly in its blast.

Then we have the fight against Sepiks Prime itself, a floating mechanical orb that randomly teleported around the room firing powerful laser blasts. Each of these battles was a challenge. I respawned so many times while fighting the mech I began to feel like Tom Cruise in Edge of Tomorrow, dying over and over while chipping away at its health. Eventually, my team persevered and earned some shiny new loot for our troubles.

Image Credit: GamesBeat

Where’s the personality?

Once I’d grown tired of exploring the Cosmodrome, I hit up the Tower, which seems to be the game’s main social hub. Here you can check your mail, buy new equipment, toss old equipment in the Vault, or pick up bounties, player-vs.-environment and player-vs.-player quests that reward you with reputation and experience. Or you can just do what I did and dance in front of the post office. This is an online game, after all.

Destiny doesn’t hesitate to show its role-playing influences, liberally borrowing concepts like levels, stats, and talent trees. It also swipes a few ideas from MMOs like public quests, gear checks, and reputation grind. All of the foes (“mobs” in MMO speak) have health bars over their heads, and little flying numbers tell you how much damage you’re doing. But for all of its RPG trappings, two elements are missing from the Alpha that I sorely missed — an engaging storyline and game world.

Although Bungie promises “rich cinematic storytelling,” the alpha’s single Story mission shed no light on the plot. And although Bungie boasts of a persistent online world, that world is heavily instanced and seems to lack personality. Maybe it’s because I’ve recently been playing Carbine’s MMO Wildstar, a game that oozes Saturday morning-cartoon charm. But so far, Destiny feels a little soulless.

I hope I’m wrong. Destiny has the potential to be a lot of fun, a successful marriage of fast-paced action and addictive role-playing mechanics that could keep players happy for years to come.