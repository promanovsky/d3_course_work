Tesla Motors Inc (NASDAQ:TSLA) shares were hit hard on today’s market after the company’s earnings report disappointed investors. The offending number appears to have been the company’s earnings guidance for the current quarter, though there is good reason the figure came in lower than most analysts expected. Tesla Motors is clearly not a profit focused company, and that should be okay with its investors.

According to the report the company released on Wednesday, Tesla Motors Inc (NASDAQ:TSLA) will see but a slim profit when it shows off its numbers for the second quarter of the year. According to analysts at Stifel that should be expected. With Model X production firmly under-way, Tesla is becoming a real car company, and that means a change in several parts of its business.

Livermore Strategic Opportunities February 2021 Update Livermore Strategic Opportunities, LP performance update for the month of February 2021. Q4 2020 hedge fund letters, conferences and more Many of you are witnessing first hand that our country, economy, (and now stock market) are all very fractured and becoming extremely challenged. Therefore, our hedge fund's theme remains focused on specific sectors and companies. Read More

Model X costs more than expected

James J. Albertine and Lucy Webster, who authored the Tesla Motors Inc (NASDAQ:TSLA) report for Stifel, reckon that the company’s increase in R&D spending in the next quarter is a net positive for the firm. Tesla is trying to produce a great SUV with the Model X, and that means spending a huge amount of money to make sure there’s nothing to complain about. The company wants to avoid the safety fiasco the Model S suffered last year with its next vehicle.

According to the analysts “internal investment in Model X should bring some reality back to the momentum driven, high-flyer mentality that some have taken with their TSLA model outlooks.” Tesla is not able to print money from thin air. The company has to invest in the products it produces. If the bottom line suffers, it cannot pull away from investment. The firm’s future depends on the creation of great cars, and short term earnings are not worth sacrificing the long term.

Stifel is very positive on Tesla Motors Inc (NASDAQ:TSLA) and the Model X. The analysts believe that the company’s battery technology and other defining features will drive value for a long time, but they also acknowledge that the Model X is a car, and Tesla is a car company. There are high costs in the business, and high risks. The analysts are looking for the price to settle between $175 and $195 as a result.

Tesla will eventually look like a real auto maker

Right Tesla does, in fact, look more like a technology company than it does a car company, but that will have to change somewhere down the line. Making a car just isn’t the same as making an iPhone and the power that the product has over the lives of consumers is something that society takes seriously. Investors will want to see Tesla come across as a sturdy industrial with tech flair rather than a tech upstart with industrial ambitions.

The addition of the Model X to the company’s line of available products is the first is a long line of changes that will secure the company’s reputation as a real car company. The second is securing earnings while trying to sell two separate cars to the public. The 9% drop in value on Thursday’s market was a direct result of anxiety over the company’s future earnings. When the company faces the problems of a real car company those earnings are going to be much more susceptible to difficult-to-control issues.

Tesla Motors Inc (NASDAQ:TSLA) is learning to deal with the strain that an auto company is forced to face on a daily basis, and the company’s future is strengthened by what it’s learning through the production of the Model X.