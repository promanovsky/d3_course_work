I'd like to preface this issue by saying today's essay might prove uncomfortable for some.

At StreetAuthority, our job is to present you with the very best investing opportunities. To ignore an opportunity just because it might be morally objectionable would be doing you -- as well as the rest of our subscribers -- a grave injustice.

With those disclaimers in mind, today I want to tell you about the current investment opportunities in the United States' burgeoning marijuana industry.

As you may know, 20 states have already made cannabis legal for medicinal purposes, while several others have recently added a medical marijuana provision to their statewide ballots. Voters in both Colorado and Washington state have even gone so far as to legalize the drug for recreational use.

As a result, legal marijuana operations like Cannabis Science ( CBIS ) -- a purveyor of some of the country's finest grown hemp (or so I've been told) -- have been popping up in "green" states all over the country.

These dispensaries -- or "compassion clubs," as they're sometimes called -- have created a very interesting phenomenon...

That's because according to federal law, the possession and distribution of marijuana is still considered a crime. That means if the government wanted to, it could raid each and every one of these businesses, seize their assets, and haul the owners off to jail.

But they don't -- because, despite what many people think, U.S. officials aren't that stupid.

They've seen what happens when the government tries to force unwanted regulation down people's throats (think 1933 and the repeal of the Volstead Act, which ended Prohibition). That's why the feds have basically said they won't prosecute anyone who buys or sells marijuana in a state that has legally allowed it.

But make no mistake... this dichotomy won't last long. You can't have two legal codes -- which are supposed to operate in unison -- in direct conflict with each other. Something has to give.

As a result, right now Colorado (and soon Washington) is serving as one giant experiment for the full-scale legalization of marijuana in the U.S.

That is, Colorado and Washington will show us whether marijuana -- with all its surrounding controversy -- can be legally consumed without invoking public outcry, or if we need to put an end to the legalization talk once and for all.

Our guess is that the former scenario will likely play out. Here's why...

As recently as the 1980s, public opposition to marijuana was as high as 80%. Anti-pot movies like the 1936 film "Reefer Madness" led children and young adults to believe the use of marijuana would result in a life of social deviance (the film showed regular marijuana users committing suicide, conducting hit-and-run "accidents" as well as performing other clandestine activities).

In the past few years, we've seen that opinion begin to reverse. While there are a host of explanations for this (like new studies showing that consuming cannabis can cause pain relief in chronic illnesses like cancer, and the maturation of the baby boomers and younger generations, who generally tend to have more accepting views of the drug than their parents, to name a few), the important thing here is that America's perception of the plant is changing, and quickly...

According to a recent study by the Pew Research Center, 52% of Americans are now on board with legalizing marijuana. As today's younger generation matures, that number is likely to climb.

Again, at this point I'd like to reiterate that whether you're one of the 52% who agrees with legalization -- or part of the 48% who oppose it -- is irrelevant. We certainly understand if you don't want to participate in the marijuana trade, but to ignore the legalization story is to deny the fact that right now, the marijuana market could be setting up one of the biggest investment opportunities of the next few years.

If legalized, pharmaceutical companies... tobacco companies... even some alcohol companies are going to want a piece of this relatively unexplored market. When that happens, there will be a massive scramble to acquire companies with established marijuana operations -- especially those that have already proven to be profitable.

Andy Obermueller, Chief Investment Strategist for Game-Changing Stocks , talks about this opportunity in the February 11 issue of his premium newsletter:

The State of Legal Marijuana Markets report, released in early January by ArcView Market Research, estimates legal cannabis hit $1.43 billion in sales in 2013 and that it will grow 64% to $2.34 billion in 2014.To put that in context, that's a rate of growth faster than smartphones achieved. ArcView said more than 590,000 Americans will have bought pot legally from a retail storefront by the end of this year.That is just the beginning.The illegal drug business is worth an estimated $400 billion a year in the United States alone. Marijuana offenses account for 48.3% of all drug arrests, according to an analysis of FBI data published in U.S. News & World Report. So presuming all marijuana sales become legal in the next 20 years, you can see the incredible growth potential -- some 30% a year in compound annual growth.

To take advantage, Andy recommends buying stock in the companies selling the technology needed to make large-scale commercial marijuana production a reality. As he goes on to say...

The fact is, pot is easy to grow. It grows in the wild all over the United States. Proper cultivation is the game-changer: Add good light, plentiful water and good plant nutrition, and serious growers can produce a massive quantity in very little space. Ultimately, this is not an agricultural problem but a security question: How does a grower secure millions of dollars' worth of grass in a field somewhere? The likely answer is to grow the plant indoors, under secure and carefully controlled conditions.The companies that are able to establish themselves as the leading providers of premium-quality marijuana and derivative products will be wildly successful as more state legislatures give grass the nod.

Of course, we understand if you have reservations about directly investing in marijuana producers. After all, these are still relatively unproven businesses in a relatively unknown market. While there will undoubtedly be some big winners in this space, there's also going to be a lot of losers as the industry grows and consolidates.

But that said, we think it's foolish to overlook the investment opportunity in the marijuana industry. Whether we like it or not, "reefer madness" could be here to stay.

So there you have it. If, after reading today's essay, you still want to send us an angry email explaining why you disagree with our opinion, feel free. We always encourage feedback from our subscribers. For those who are interested, please send your comments to Editorial@streetauthority.com.

Note from the editor: Marijuana isn't the only industry with "game-changing" potential right now. In his most recent research report, " The 5 Unstoppable Game-Changers to Get In on Now ," Andy Obermueller tells readers about five investment trends -- and the companies behind them -- that could just as easily deliver 10-bagger gains over the next few years.

One of these companies has developed a machine that can perform surgeries with robotic precision. Another has built a car that allows blind people to drive. If Andy's right, then these -- plus the rest of his "unstoppable game-changers" -- could prove to be some of the market's best performers over the next few years. To learn about these trends, as well as the companies that are making them happen, follow this link .

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

© Copyright 2001-2016 StreetAuthority, LLC. All Rights Reserved.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.