Ireland is ranked fifth highest among 27 EU countries in incidence of childhood obesity

HIGH cholesterol levels may reduce a couple's chances of having children, a study has found.

The US research suggests a link between raised cholesterol and infertility as well as heart disease.

When both partners in a couple had high amounts of cholesterol in their blood it took the longest time to achieve a pregnancy, the research showed.

It also proved harder for couples to become parents when the woman had high cholesterol but the man did not.

Why cholesterol should affect fertility remains unclear, but the fatty substance is closely involved in the manufacture of sex hormones such as testosterone and oestrogen.

Lead scientist Dr Enrique Schisterman, from the National Institute of Child Health and Human Development, said: "We've long known that high cholesterol levels increase the risk for heart disease. From our data, it would appear that high cholesterol levels not only increase the risk for cardiovascular disease, but also reduce couples' chances of pregnancy.

"In addition to safeguarding their health, our results suggest that couples wishing to achieve pregnancy could improve their chances by first ensuring that their cholesterol levels are in an acceptable range."

The study looked at links between fertility, environmental chemicals and lifestyle.

PA Media