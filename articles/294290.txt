Shares of Fusion-IO (NYSE: FIO) closed up more than 22 percent on Monday following news SanDisk (NASDAQ: SNDK) is purchasing the company in an all cash deal valued at $1.1 Billion.

SanDisk stated the deal will give its company the broadest flash portfolio in the industry. If the deal is accepted by Fusion, then SanDisk will acquire Fusion's assets for cheap and its server-side flash segment will benefit from synergies.

Below are some highlights from Wall Street analyst's thoughts on the deal:

Credit Suisse - Samsung, Intel (NASDAQ: INTC), NetApp (NASDAQ: NTAP) may offer more for Fusion-IO than SanDisk's $1.1 billion. The firm downgraded Fusion-IO from Outperform to Neutral.

Summit Research - The firm sees SanDisk grabbing Fusion-IO "for a steal" If SanDisk pays its current offer of $1.1 billion. Summit sees further bids unlikely as most major companies within the sector already have server-side flash OEMS. Still, the firm's Srini Nandury would not rule out more offers because of FIO shareholders demanding a better deal.

Mizuho Securities - The price "indicates challenging environment as well as management's willingness to sell at more reasonable prices." Mizuho maintained a Neutral rating but raised the price target on Fusion-IO from $10 a share to $11.25 a share.

Piper Jaffray - target on SanDisk boosted from $95 a share to $110 a share, Overweight maintained.