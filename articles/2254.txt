Peter Dinklage holds his award for Best Supporting Actor in a Drama Series for "Game of Thrones" at the 63rd Primetime Emmy Awards at the Nokia Theatre in Los Angeles on September 18, 2011. UPI/Jayne Kamin Oncea | License Photo

Rose Leslie attends the opening of the "Game of Thrones" exhibit on March 26, 2013 in New York City. UPI/Monika Graff. | License Photo

Natalie Dormer attends the opening of the "Game of Thrones" exhibit on March 26, 2013 in New York City. UPI/Monika Graff. | License Photo

Maisie Williams attends the opening of the "Game of Thrones" exhibit on March 26, 2013 in New York City. UPI/Monika Graff. | License Photo

Sophie Turner attends the opening of the "Game of Thrones" exhibit on March 26, 2013 in New York City. UPI/Monika Graff. | License Photo

Emilia Clarke (Daenerys) and Kit Harington (Jon Snow) accept the Television Critics Association Award for Best Drama on behalf of the cast and crew of HBO's "Game of Thrones. (TCA/Cindy Ronzoni)

NEW YORK, March 10 (UPI) -- HBO has released a new trailer for the upcoming fourth season of Game of Thrones.

The trailer features a distraught Sansa Stark (Sophia Turner) in King's Landing with Tyrion Lannister (Peter Dinklage), a troubled Jon Snow (Kit Harington) still in the company of Ygritte (Rose Leslie) and the wildlings, and Daenerys Targaryen (Emilia Clarke) on the warpath accompanied by her dragons.

Advertisement

The fourth season is roughly based on the second half of the novel A Storm of Swords in George R.R. Martin fantasy series A Song of Ice and Fire, and will pick up where the third season left off. The trailer reveals that themes of revenge and justice will dominate this season's plot lines.

Game of Thrones season four will premiere on Sunday, April 6 at 9 p.m. EDT.