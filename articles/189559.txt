Three months and 13bn years: Birth of the Universe simulated

The way the universe evolved in the moments right after the Big Bang has been modeled in the most accurate simulation so far, with supercomputers working for a solid three months to crunch the calculations. The model, developed by a research team led by MIT, not only looks at how 41,416 different galaxies formed from dark matter, but goes on to make new predictions about how the raw ingredients are distributed through space.

“Some galaxies are more elliptical and some are more like the Milky Way, [spiral] disc-type galaxies,” assistant professor of physics Mark Vogelsberger explained of the theory tested out by the simulation. “There is a certain ratio in the universe. We get the ratio right. That was not achieved before.”

Dubbed the Illustris model, it’s a proposed explanation of what happened in the thirteen billion years after the Big Bang, taking into account aspects like how different galaxy shapes are formed, and how elements are distributed. Altogether, it encompasses a cube of space 350m light years to a side, to a scale of 1,000 light years.

Altogether, that involves modeling twelve billion different “resolution elements” which, once rendered by multiple supercomputer clusters spread over different locations, can be explored much in the way astronomers can now dig through real telescope data.

For instance, the model indicates that supernovas play a key part in distributing certain elements, and confirms the important role that a rich supply of neutral hydrogen plays in contributing to overall stellar mass. Perhaps most noticeably, scientists now have a model that creates galaxies like our own Milky Way, which had previously been a source of frustration.

Until now, no model had been able to demonstrate through simulation how a galaxy shaped like the Milky Way would actually form.

“We have long debated whether this failure was due to complex dark matter physics, unknown stellar feedbacks, or the difficulties in simulating the highly non-linear multi-scale process of galaxy formation … With their simulations, [the researchers] finally produce galaxies that look like our own” David Spergel, professor of astronomy, Princeton University

Next step is tinkering with the model to refine some of the lingering issues, with the research team conceding that it’s still not a perfect expression of how the universe formed.

Back in March, signs of the very first “cosmic contractions” that could help explain the sudden rush of expansion at the birth of the universe were spotted, remnants of gravitational energy left on the Cosmic Microwave Background picked out using high-resolution telescopes at the South Pole.

SOURCE Nature