“In other words, Facebook has failed to invent any new behaviors or even innovate on the behaviors that were commonplace on the service. – Om Malik.

Facebook, massive and successful as it is, is no longer a company that defines new social experiences. It either copies them outright or it just buys them. (See also: $19 billion for WhatsApp).

Facebook has been a copycat for a long time.

Back when users were still checking in on Foursquare, Facebook launched its own way to “check-in” with friends on Facebook.

Before that, when social networking early adopters started sharing and “liking” content over on FriendFeed, Facebook copied the “like” and made it such a ubiquitous part of Facebook, that a majority of people today would probably tell you Facebook originated the idea.

But in more recent years, Facebook’s attempts at cloning other services fell flat. Its Camera app was a dud. Its ephemeral messaging app Poke was even worse. But Facebook threw a billion dollars at the first problem, effectively solving it with the purchase of Instagram. It later tried to throw some $3 billion at Snapchat, and got shot down. But now it has WhatsApp, so it has the users it cares about more than money, it seems.

Other internal efforts have also failed. Facebook Home, a home-screen replacement and launcher for Android, not only flopped, but also showed that Facebook had a tin ear when it came to understanding the desires of an Android user base. These users revel in their ability to customize their phones and make them their own, not have one network’s content shoved down their throats.

Then there was that failed experiment with “Collections,” Facebook’s would-be Pinterest alternative – whatever happened to that? Did the company finally realize that the power of Pinterest wasn’t just the “pinning” concept, but the experience of using the service itself and the community it had built? From the looks of things, no. Facebook is just taking a new angle on Pinterest instead with a feature focused on “travel inspirations.”

More recently, Facebook launched Paper, a clear shot across the bow of social magazine Flipboard. Like Flipboard, Paper aims to aggregate news stories and social updates from friends in a more touch friendly format. Will Paper redefine Facebook proper on mobile? Probably not. (It’s now ranked No. 1,480 Overall on the App Store).

It’s not that Facebook should never copy great social concepts and make them its own. After all, that “Like” button worked out pretty well, didn’t it? And the company certainly popularized the concept of the “status update” to the point where if anyone mutters the term, you assume they mean a Facebook post.

But Facebook seems to have lost its touch when it comes to launching new concepts that later end up defining what social services look like and how they should behave.

Back in the day, it had the mojo: News Feed, for example, was such an outrageous idea that the backlash from Facebook’s user base at the time of its launch was deafening. Yet, it worked. In fact, it worked so well that now a stream of updates in reverse chronological order is often referred to as a “News Feed.”

More Facebook Apps

This year, Facebook said it would set Messenger free. But it meant it would force you to download it separately.

Still, it’s promising that the new Messenger app is very good.

It just was never so good as to squash the growing messaging app mini empires that sprung up around the world in the meantime. That’s why Facebook went for Snapchat. That’s why it went for WhatsApp. But elsewhere users are chatting it up on KakaoTalk, WeChat, LINE, Whisper, Kik, Tango, Hangouts and, god, even BBM … to name only a few.

It’s notable that Facebook is trying to buy its way onto mobile, in the wake of multiple standalone mobile flops. With the purchase of WhatsApp, Facebook bought growth and users. It needed a way to engage the young, mobile, and worldwide demographics turning to standalone messengers, and Facebook did so despite the fact that it even bought itself a group chat app called Beluga back in 2011.

To further serve its mobile efforts, Facebook this year may even roll back on forcing users to interact by way of their “real” identities. That’s also a radical, and somewhat concerning, shift for a company whose entire vision to date seemed to have been rooted in this idea of openness and transparency. It was once about forcing people to live in public for the greater good … or something like that. (At least, that’s how it once justified making everyone’s private content public by default, while offering privacy controls so complex users couldn’t figure them out.) Now Facebook will offer anonymity? Right.

Of course, Facebook is nothing if not adaptive. It changed the rules before, and can do so again.

Besides, in retrospect, the privacy violations of years ago seem now more like mere attempts at cloning Twitter: let’s make updates public! Searchable!

Maybe Facebook’s grand vision for social networking is really nothing but this: Don’t use anything else. Just us. Just Facebook.

Facebook is big enough that it has the luxury of failing. But thanks to its new R&D-ish group, Facebook Creative Labs, it can now do so faster. That’s smart.

It’s also smart that Facebook is thinking about spinning off other popular bits of its larger feature set – like Events and Groups – into apps of their own. These are parts of Facebook, like Messenger, that see heavy use. These apps don’t need to convince you to leave another app – Snapchat, Flipboard, etc. – for Facebook’s paler implementation.

And it’s smarter still that Facebook is just buying up mobile user bases in big chunks.

Presuming Facebook’s new apps perform as well as Messenger and its new acquisitions continue to hold their own, when mobile users jump back and forth between their social apps on mobile, they’ll be bouncing from Facebook to Facebook to Facebook to Facebook.

Maybe without even realizing it.