In 2011, the first of the baby boom generation reached what used to be considered retirement age. And for the next 15-plus years, they’ll be turning 65 at a rate of about 8,000 a day. As a result, health officials estimate the number of people living with Alzheimer’s disease could triple by 2050.

Knowing how to properly care for someone in cognitive decline can be overwhelming, so Harper College’s Continuing Education department is launching a new online class, “Dementia for Family Caregivers.” The class, which runs April 7 through May 2, is designed for family caregivers dealing with Alzheimer’s disease, vascular dementia, Lewy body dementia or other conditions that cause significant memory problems.

“Our instinct is to treat these adults like children, which should be avoided,” instructor Pamela Atwood said. “This class is really designed to teach family caregivers practical applied strategies and how to be advocates.”

Atwood, a 25-year veteran of the health care industry, said students will also learn about the basics about dementia, common health complications and receive tips from professional care providers. “There can be some bizarre and uncomfortable moments for caregivers, but there are ways to find moments of gratitude and grace during this very difficult disease process,” she said.

Tuition for the online class is $149. For more information or to register, call 847.925.6300 or visit harpercollege.edu/ce.