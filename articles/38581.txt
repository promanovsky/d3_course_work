In the technology industry, few concepts are as noble in theory and as coldly calculated in practice as net neutrality.

The stock market reacted strongly on Monday to a Wall Street Journal report claiming that Apple Inc. is in discussions with cable giant Comcast Corp. regarding a potential streaming-television service that would receive "special treatment" on Comcast's infrastructure. Even though there are no indications that the discussions are anything more than routine talks, or that any such service is imminent, investors quickly soured on shares of Netflix Inc., the company that would face direct competition from such a service. Shares of the popular online movie service dipped 6.67 per cent. But among many observers, a more serious concern of any potential Apple-Comcast deal relates to its possible impact on net neutrality.

"This [potential Apple-Comcast] workaround is worth considerable scrutiny, particularly in light of the proposed Comcast-Time Warner Cable merger, as it speaks to both Comcast's willingness to skirt around the rules now, and the potential harm if it's able to do so as a combined entity with considerable market share in a variety of related industries," said Sarah Morris, senior policy counsel for the Open Technology Institute.

Story continues below advertisement

In general, net neutrality describes the philosophy that the Internet is an equal space, where no single type of content is treated differently from any other.

Digital rights activists describe the concept as a central pivot of a free and open Internet. Many major cable and Internet providers, on the other hand, have frequently tried to weaken net neutrality rules in court.

(Comcast did not respond to a request for comment for this article. An Apple spokeswoman declined comment).

The topic is currently under a much brighter spotlight because of a U.S. federal court ruling in January that struck down part of the U.S. Federal Communications Commission's net neutrality rules. The court didn't speak out against net neutrality, but instead found that the FCC had made an error in the way it defined and articulated its guidelines.

But although the outcome of the net neutrality battle is of extreme interest to many digital activists and technical experts, at a business level, it is in many ways a purely financial battle between those companies that sell content, and those that sell network connections.

The central point of contention between content providers, such as Netflix, and Internet service providers, such as Comcast, is over the issue of "interconnection" fees. Broadly, the term describes the point at which content from Netflix arrives at Comcast's network. Such interconnection points have not traditionally been covered by the principles of network neutrality, which instead focus on ensuring all content is treated equally along the "last mile," or the part of the network the carries content to its end point, the consumer. Indeed, interconnection fees of one kind or another have been commonplace on the Internet for years.

The problem today is scale. Thanks to the rise of high-definition video and other large-size data, the content providers have become entangled in a bitter fight over who should foot the bill for all that extra traffic. And while both sides have sought to frame themselves as champions of an open Internet, the issue for all companies involved is, at its heart, about dollars and cents.

Story continues below advertisement

By trying to lobby government regulators to treat interconnection fees as a violation of net neutrality, content providers stand to save huge sums of money on data-transfer costs. Conversely, by objecting to increased net neutrality regulations, ISPs not only continue collecting fees, they increase the options available to make money off Internet traffic, rather than simply running their networks as "dumb pipes" that do nothing more than facilitate other companies' services.

"Most people don't want to say that this is about two businesses standing up for their business, which is what it is," said Dan Rayburn, a principal analyst at Frost & Sullivan.

Indeed, even as it complained about net neutrality, Netflix recently signed an interconnection fee deal with Comcast.

In reality, even if net neutrality rules were expanded to the level that companies such as Netflix are asking for, the most immediate benefit would be felt by a small group of large technology companies whose size allows them to build their own content distribution networks (which then have to connect to ISPs such as Comcast, paying fees in the process). The vast majority of content companies, including video-production heavyweights such as the major American sporting leagues, use third-parties to distribute their content.

But it is also unclear whether allowing ISPs to have a freer hand in signing "preferential treatment" deals with individual content providers would do any good to the consumer – or whether it would simply play a part in net neutrality's eventual death by a thousand cuts.

"We could remember this as the beginning of the cable-ization of the Internet," lawyer and Internet policy expert Marvin Ammori said, "where some 'channels' or companies get better treatment than others."