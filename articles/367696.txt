Brent crude oil slipped below $111 to begin the week as the conflict keeping Libyan oil from the market looked nearer to being resolved. The commodity traded at $110.45 at 8:30 GMT with geopolitical tension elsewhere keeping a floor under prices.

Libya’s National Oil Corp lifted a force majure from two of its eastern oil ports as rebel groups have agreed to give up control of the two complexes. The ports have been closed for nearly a year and will boost Libya’s export capacity by about 500,000 barrels per day once reopened.

However, tension in other parts of the world kept Brent from dipping too low. In Ukraine government forces have launched a renewed offensive to take back control of the nation’s eastern cities in what President Petro Poroshenko has called a turning point in the nation’s conflict.

Related Link: Brent Falls As Libyan Oil Returns

In Iraq, the deep divide between Shi’ites and minority groups like the Sunnis and the Kurds showed no sign of easing. Nonetheless the nation’s parliament is trying to form a new, more inclusive government. Meanwhile rebel groups have taken over much of the nation’s northern towns and threaten to push the entire country into a civil war.

Moving forward, investors will look to economic data from some of the world’s largest consumers this week for an indication of whether or not the global economy is picking up.

Over the weekend Bloomberg reported that International Monetary Fund Managing Director Christine Lagarde said that the global economy will likely grow at a slower pace than originally predicted. Lagarde cited reduced investment spending as one major reason the fund sees sluggish growth, and encouraged countries with low debt burdens and high growth to consider investing in infrastructure.