Apple updated its developers conference Web page Tuesday to say it will live stream its keynote event next week. Apple is expected to make several product announcements.

The Worldwide Developers Conference’s keynote will kick off Monday at 10 a.m. PDT.

Most notably, the company is expected to preview OS X 10.10, a redesigned version of its laptop and desktop software. Apple is also expected to unveil iOS 8, the latest version of its iPhone and iPad software.

Also probable is a new Healthbook app to allow users to centralize data collected by health apps on their mobile devices. Additionally, the Financial Times reported Monday that Apple would introduce a new platform to make it easier for consumers to use their iPhones to control smart appliances around their homes.

Advertisement

Apple may use the event to formally announce its rumored $3.2-billion acquisition of Beats Electronics, according to some reports.

The conference is being held June 2 through 6 at San Francisco’s Moscone West center.

To watch the keynote, viewers will need a Mac running OS X 10.6 Snow Leopard or later as well as Safari 4 or later. Mobile users can watch using the Safari app on an Apple device running iOS 4.2 or later. Windows users can tune in if they have QuickTime 7 installed on their machines. The broadcast will also be viewable on second- and third-generation Apple TVs.