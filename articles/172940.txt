Selena Gomez Vs Kendall & Kylie Jenner: The Truth Revealed?

Reports surrounding the apparent end of Selena Gomez’s friendship with Kylie and Kendall Jenner have been coming thick and fast over the last week, but it’s now been claimed that there aren’t even any problems at all.

It all started when Selena unfollowed everyone on Instagram, with rumours quick to point out that it was the termination of her friendship with the sisters that prompted the decision.

Add to that the fact that Selena has recently changed management, it had been speculated that she could have ditched the Keeping Up With The Kardashians stars in order to focus on her career. Conversely, separate sources alleged that Kylie and Kendall were only friends with Selena in a bid to improve their own brand.

Selena Gomez (WENN)

The Sun even ran a story last week saying that Selena suspected 16-year-old Kylie of hooking up with her on-off ex-boyfriend Justin Bieber, with whom she has been spotted on several occasions in recent months.

But now Gossip Cop claims to have got to the bottom of it once and for all – and it’s actually pretty drama-free.

“[Selena has] absolutely no issues with the girls,” a source close to the ‘Slow Down’ singer said.

Kylie Jenner at Coachella (Splash News)

They added that Selena’s decision to eradicate everyone from her Instagram profile was about making her social media accounts more business-based than oriented towards her friendships.

“She wanted to get back to using [Instagram] just for her fans and not about her personal life,” the insider added.

So is that the end of that?