iPhone 6 Release Date Nearing: September 9, 2014 Launch Event Rumors Continue; iPad Mini, iPad Air Following in October? iPhone 6 Release Date Nearing: September 9, 2014 Launch Event Rumors Continue; iPad Mini, iPad Air Following in October?

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Apple has been generating a lot of hype for their upcoming release of the iPhone 6. Expectations are high for the iPhone 6 with a probable Sept. 9 release.

With Samsung, HTC, and LG bringing out premium large-screen phones, Apple will need to make the iPhone 6 bigger as well if they ever want to stand a chance against the likes of the Galaxy S5, the One M8, and the G3.

In addition to the iPhone 6, Apple is also looking to announce its new iPad Air and iPad Mini as well.

The Cupertino company has always had the habit of releasing their new products during a special event on September. If anything, the iPhone 6 could be seeing a release alongside the new iOS 8 and then the iPad Air the following month. What's more, it looks like the company is also going to release both size variants – the 4.7-inch and 5.5-inch – at the same time.

Nothing is still final though as the release of the two size variants could still be split if the production of the 5.5-inch version falls short of the deadline.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

As with all Apple products, the iPhone 6 won't be cheap. The company has had a long history of releasing devices that come with a premium price, and the new iPhone won't be an exception.

With the iPhone 6's larger screen, a new glass covering, and improved hardware specifications, prices could reach as much as $850.

Truly, the pressure heaping up on the iPhone 6 is tremendous and the release could either make or break the new generation Apple smartphone.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit