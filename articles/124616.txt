ALBANY, N.Y. - An official familiar with the investigation said the New York Attorney General's Office has issued subpoenas to six firms and sent a letter to another for details about split-second stock trading and any unfair advantages.

The official told The Associated Press Wednesday that the subpoenas went last week to trading firms including Chicago-based Jump Trading LLC and Chopper Trading LLC and Tower Research Capital in New York. The official spoke on condition of anonymity because he wasn't authorized to publicly discuss the subpoenas. He said he did not know the names of the other companies.

Jump Trading, Chopper Trading and Tower Research did not immediately respond to requests for comment.

Attorney General Eric Schneiderman has said advantages in computer hardware and placement enable some traders to get millisecond timing advances to make "rapid and often risk-free trades before the rest of the market can catch up."

His office, with authority to investigate securities fraud under New York's Martin Act, is seeking details about trading strategies and special arrangements with trading venues including exchanges and so-called "dark pools" for buying and selling securities.

At a New York Law School symposium last month, Schneiderman discussed some concerns. "Rather than curbing the worst threats posed by high-frequency traders, our markets are becoming too focused on catering to them," he said.

According to the attorney general's office, advantages include extra computer network bandwidth, ultra-fast connection cables and special high-speed switches to computer servers. Some traders are allowed to place their computer servers within trading venues. The millisecond timing advantages let those traders make "rapid and often risk-free trades before the rest of the market can catch up."

The attorney general last year reached an agreement for Thomson Reuters to stop selling to high-frequency traders a two-second peek at some market-influencing consumer survey results, followed by agreements this year for several financial firms to stop cooperating with analyst surveys that favored some elite clients.