Industrial production in Japan contracted a seasonally adjusted 2.3 percent on month in February, the Ministry of Economy, Trade and Industry said on Monday - falling for the first time in four months.

The headline figure was well shy of forecasts for an increase of 0.3 percent following the 3.8 percent increase in January.

On a yearly basis, industrial production added 6.9 percent - also missing expectations for 9.9 percent following the 10.3 percent jump in the previous month.

Upon the release of the data, the METI maintained its assessment of out, saying: industrial production continues to show an upward movement.

Industries that mainly contributed to the decrease included transport equipment, oriented machinery and communications equipment.

Commodities that mainly contributed to the decrease included large passenger cars, drive transmissions and motor vehicle engines.

According to the survey of production forecast in manufacturing, production is expected to increase 0.9 percent in March and decrease 0.6 percent in April.

Industries that mainly contributed to the increase in March include communications equipment, electrical machinery and others.

Industries that mainly contributed to the decline in April include communication equipment, others and chemicals.

Shipments in February shed 1.0 percent on month, falling for the first time in six months. Shipments were also up 6.0 percent on year.

Industries that mainly contributed to the decrease include business oriented machinery, communications equipment and transport equipment.

Inventories in February dipped 0.8 percent on month, falling for the seventh consecutive month. Inventories also dropped 3.2 percent on year.

Industries that mainly contributed to the decrease included iron and steel, business oriented machinery and transport equipment.

The inventory ratio added 1.7 percent on month, rising for the first time in six months. The ratio also lost 8.8 percent on year.

Also on Monday, manufacturing activity in Japan expanded at a slower pace in March, the latest survey from Markit Economics revealed on Monday - coming in with a seasonally adjusted score of 53.9.

That was well shy of forecasts for a score of 57.0, and it was down from 55.5 in February.

A score above 50 means expansion in a sector, while a score below 50 signals contraction.

Among the individual components of the survey, new export orders climbed from 51.5 in February to 52.3 in March, while output sank from 58.4 to 54.2.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.