Celebrity

The rapper was reportedly upset that his wife Tameka 'Tiny' Cottle possibly got close to the boxer after she posted a photo of her with Floyd's daughter.

May 26, 2014

AceShowbiz - Jealousy might have caused T.I. and Floyd Mayweather, Jr.'s brawl in Las Vegas on early Sunday morning, May 25. TMZ has learnt that the rapper was upset about the professional boxer possibly getting close with his wife Tameka "Tiny" Cottle.

It might be sparked by a photo that Tiny posted on her Instagram hours before the fight. In the picture, she was seen posing with Floyd's daughter, whom she called "my new boo." "Had to come celebrate with my new boo @moneyyaya Happy Birthday bae," she captioned the image.

However, T.I.'s suspicion could be for no reason. Floyd reportedly told friends that he's shocked that the rapper confronted him since he believed there wasn't any beef between them. Floyd allegedly became upset that he's falsely accused of any shenanigans, claiming that he did not want to be romantically involved with Tiny and it was her choice to hang out with his daughter while in the Sin City.

In a video obtained by TMZ during the brawl at Fatburger, Floyd can be heard saying, "You control your b***h motherf**er." Fists and chairs were flying and an employee at the eatery was slashed and suffered a minor injury during the fight.

Police arrived at the scene just as Floyd was leaving while T.I. was already gone. The injured employee reportedly refused treatment and did not want to cooperate with investigating officers.