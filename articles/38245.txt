London: European shares rebounded on Tuesday as investors tried to take advantage of a drop in prices caused by geopolitical tensions and doubts about global growth.

Markets were encouraged by hopes of stimulus measures in China and by a growing consensus that a rift between the West and Russia was unlikely to get out of hand.

Bourses in London, Paris and Frankfurt opened around 0.9 per cent higher, bouncing after Monday’s 1 per cent drop put the region’s FTSEurofirst 300 index on track for its worst month since June.

While equity markets were hunting for bargains, the euro at 1.3827, the pound at 1.6483 and the region’s benchmark government bonds were little changed as they digested fresh data from the region’s biggest economies.

Top of the list was German business sentiment data from Germany’s Ifo institute. As expected, it dipped after this month’s tensions with Russia over Ukraine. A similar survey in France saw morale largely unchanged.

In Britain, inflation data showed price increases had slowed, reinforcing views that the Bank of England would hold off on raising interest rates.

“The reason why the equity markets are doing well is a bit of a rebound from the recent sharp sell-off,” said David Madden, a market analyst at IG index in London.

“The euro has struggled a bit because of dollar strength more than anything, though I do think any strong sanctions slapped on Russia by the U.S. and Europe or vice-versa would knock equities again.” Standoff

The Group of Seven major industrialised nations warned Russia on Monday it faces additional economic sanctions if President Vladimir Putin takes further action to destabilise Ukraine. Markets took the warning in stride.

“The fallout from Ukraine so far has been very limited,” said Ramin Nakisa, a global macro strategist at UBS. “The politicians won’t have a strong mandate for heavy sanctions.”

In Asian trading, Japan’s Nikkei had dropped 0.4 per cent. MSCI’s broadest index of Asia-Pacific shares outside Japan dipped 0.2 per cent after a lacklustre session on Wall Street.

Short-dated US Treasuries prices remained in focus after/sFederal Reserve chief Janet Yellen said last week that interest rates could rise early next year.

Two-year Treasuries, the most sensitive to interest rate changes, were steady at 0.4288 per cent in early European deals, after reaching a six-month high of 0.4650 on Monday. Money market futures were pricing in some possibility of a rate hike by spring 2015.

Rising US rates are also generally negative for emerging markets, but many of them have held up so far. They have been helped in part by expectations the Chinese government will introduce economic stimulus measures after weak Chinese manufacturing data was reported on Monday.

Mainland Chinese shares touched a one-month high in Asian trade as companies linked to Shanghai’s free-trade zone gained on the back of media reports that its restrictions on foreign investors may be relaxed.

“The [manufacturing] data was pretty bad,” said Naoki Tashiro, president of T.S. China Research. “It looks almost certain that the first-quarter growth is likely to fall short of the government’s growth target of 7.5 per cent. So the government is likely to take some measures.”

The Australian dollar, often seen as a liquid proxy for bets on the Chinese economy, briefly reached a three-month high of $0.9158. Copper, also highly sensitive to China’s fortunes, touched a week high.

Copper prices have now stabilised after sinking to a three-and-a-half year low on worries that China’s slowdown might trigger a wave of defaults on loan deals where copper was used as collateral.