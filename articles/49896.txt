Aurora Store Owner Says Protesters Outside His Store Have Started Following Him; Protesters Say His Mask Policy Of Pulling Mask Down When Buying Age Restricted Products Is RacistA battle keeps brewing between a store owner and some in his community, and now the store owner says some picketing his store are going further by actually following him.

Chicago Weather: Another Round Of Light Snow, Below Zero Wind ChillsMore cold temps and fluffy snow will be moving into the Chicago area, with higher lake effect accumulation in parts of Northwest Indiana likely.

Man Struck And Killed By City Salt Truck On South SideThe salt truck was backing up in a parking lot near 53rd and LaSalle streets around 8:30 p.m., when it ran over 57-year-old Yulelander Seals, according to police and the Cook County Medical Examiner's office.

River Forest Man Says Walgreens' COVID-19 Vaccine Sign Up System Makes It Impossible To Make Appointment For ParentsA River Forest man says the Walgreens COVID-19 vaccine sign up system makes it impossible to make an appointment for his parents.