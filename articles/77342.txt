All of your Google+ photos got a +1 today, but maybe not in the way you'd expect.

Google+ got in on the April Fool's Day prankage by giving all photos stored on Google+ a Hoffsome photobomb. That's like "awesome," but David Hasselhoff-style.

The social networking site has promised to make every photo #autoawesome by sneaking in the "Baywatch" star, and we have to admit that the results are pretty amazing:

You have three options: You can either go upload all of your photographs to Google+ immediately to take advantage of this once-in-a-lifetime opportunity to hang with the Hoff; you can check out the most #Hoffsome photos posted to Google+ today; or you can check out the epic wonder that is David Hasselhoff singing "Hooked on a Feeling."

You know what, just do all three.