OMSignal opens pre-orders for activity tracking clothing line

Wearable tech is hot right now, with no subcategory more interesting than clothing. Something we wear daily, smart clothing has a unique inroad to connect us to the other thing we have on us daily — our smartphones. None are doing the clothing/smartphone cohesion better than OMSignal, who have opened their products up for pre-order.



The product line is slightly limited right now, but it’s growing. OMSignal tells us their lineup, currently limited to Men’s shirts, will grow over the summer to accommodate ladies wear as well. The four shirts — under-shirt, sleeveless shirt, casual tee, and a long-sleeved shirt — are meant for fitness activity, as the tech is geared toward keeping track of your vitals during a workout. Don’t worry, though, OMSignal tracks you regardless of what you’re doing (or not doing).

Each of the shirts have sensors embedded in the fabric, which measure statistics like “heart rate, breathing rate, breathing volume, movement (including steps and cadence), movement intensity, heart rate variability and calories burned.” Getting the info to an app on your phone is done via a small black box that clips into the shirt and transmits the data via Bluetooth, but a constant connection is not needed — the box is compiling data all the time, and will tell the app what’s going on when you have a connection. OMSignal says the water-resistant black box has a 2-3 day battery life, and can last “up to 30 training sessions before recharging”.

The shirts are compression wear, designed to wick moisture, enhance blood circulation, and increase oxygen delivery to the muscles. The sensors are knitted into the fabric near the heart to give a more accurate picture of your vitals, as well as calories burned and physiological stress. OMSignal has worked out a proprietary algorithm they say offers “unparalleled insight about your body.”

The clothing is available now, with packages starting at $199 (one shirt of your choosing and a black box). The products will be shipping this summer, and OMSignal will ship anywhere in the world. The app is currently iOS only, and we don’t yet have word on when an Android or Windows Phone version will be made available.

