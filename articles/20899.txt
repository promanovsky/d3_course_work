Kuala Lumpur -

Malaysia's defence minister says files were recently deleted from the home flight simulator belonging to the pilot aboard the missing Malaysian jetliner.

Hishammuddin Hussein told a news conference on Wednesday that investigators are trying to retrieve the files.

He also said that the pilot, Captain Zaharie Ahmad Shah, is innocent until proven guilty of any wrongdoing.

Hishammuddin said background checks have been received from overseas agencies for all foreign passengers on the plane except for those from Ukraine and Russia - which accounted for three passengers.