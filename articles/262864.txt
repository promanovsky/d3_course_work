An outbreak of antibiotic-resistant salmonella linked to a California chicken company has struck another 50 people, bringing the total to 574 cases in the United States since March 2013, health officials said on Tuesday.

Since the April report confirming new infections caused by strains of drug-resistant Salmonella Heidelberg, an average of eight new salmonella illnesses have been reported, the Associated Press reported.

There has been no recall of Foster Farms chicken, The Centers for Disease Control and Prevention said.

Since the outbreak is resistant to many antibiotics, thirty-seven percent of those with the foodborne bacteria have been hospitalized, Reuters reported.

Thirteen percent of victims, from 27 states and Puerto Rico, developed blood infections, almost three times the normal rate, the CDC said. No deaths, however, have been reported.

A multiple step approach to reduce or wipe out salmonella at each stage of production has been developed by Foster Farms, which is based in the U.S. west coast, said in a statement.

"The Agriculture Department says it is monitoring Foster Farms facilities and that measured rates of salmonella in the company's products have been going down since the outbreak began," the AP reported. "The department threatened to shut down Foster Farms' facilities last year but let them stay open after it said the company had made immediate changes to reduce salmonella rates."

Three-fourths of victims who were able to provide the CDC with brand information said they had consumed chicken produced by Foster Farms before they became ill.

"The company continues to make steady progress that has effectively reduced Salmonella at the parts level to less than 10 percent - well below the 2011/2012 USDA-measured industry benchmark of 25 percent," the U.S. Department of Agriculture said.

Warm weather has increased the salmonella incidence, it added.

"In a statement, Foster Farms said it has put many new measures in place, including tighter screening of birds before they buy them, improved safety on the farms where the birds are raised and better sanitation in its plants," the AP reported.

After finding five cockroaches on five separate occasions over four months, USDA inspectors briefly closed the a Foster Farms plant in Livingston, California in January.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.