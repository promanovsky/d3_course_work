Canadian smart phone maker BlackBerry Ltd. (BBRY,BB.TO), which has been trying for a turnaround through drastic cost reduction and job cuts for the last few quarters, Thursday reported a small profit for the first quarter compared to a loss last year.

Despite deep fall in revenues, improved margins helped beat modest quarterly expectations. The shares rose about 13 percent in pre-market trading.

Looking ahead, the company said it is focusing on its growth plan to enable a return to profitability.

The firm noted that it reduced adjusted operating expenses by 57 percent from last year, and 13 percent from the sequential fourth quarter.

John Chen, executive chairman and chief executive officer of the company said, "Over the past six months, we have focused on improving efficiency in all aspects of our operations to drive cost reductions and margin improvement."

For the first-quarter ended May 31, 2014, the company posted net income of $23.00 million, compared to a net loss of $84 million in the previous year. On a per share basis, net loss was $0.37, wider than $0.16 per share a year ago.

The latest-quarter net income included non-cash income associated with change in fair value of Debentures of $287 million and pre-tax restructuring charges of $226 million related to the Cost Optimization and Resource Efficiency program.

Excluding items, adjusted loss for the recent quarter was $0.11 per share.

On average, 33 analysts polled by Thomson Reuters expected the company to report a loss of $0.26 per share for the quarter. Analysts' estimates typically exclude special items.

Revenues for the quarter fell to $966 million from $3.07 billion in the prior-year quarter. Twenty nine analysts had consensus revenue estimate of $963.17 million for the quarter. Revenues edged down 1 percent from the previous quarter.

Revenues consisted about 39 percent for hardware, 54 percent for services and 7 percent for software and other revenue.

Gross margin was 46.7 percent, up from 33.9 percent in the preceding year. Adjusted gross margin for the recent quarter was 48 percent.

During the quarter, the company sold about 2.6 million BlackBerry smart phones to end customers, which included shipments made and recognized prior to the first quarter and which reduced its inventory in channel.

The company anticipates maintaining its strong cash position, and looks for opportunities to prudently invest in growth. It is targeting break-even cash flow results by the end of fiscal 2015.

BBRY closed Wednesday's regular trading at $8.29 on the Nasdaq. In the pre-market activity on Thursday, the shares are up 12.79 percent.

For comments and feedback contact: editorial@rttnews.com

Business News