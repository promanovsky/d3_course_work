Good news for Apple Inc. (NASDAQ:AAPL) fans. The Cupertino-based tech giant plans to live-stream the Worldwide Developers’ Conference (WWDC) keynote speech next week.

Exciting news from Apple

The iPhone maker recently updated the official website with the news. The streaming event will be available on Mac computers that run on Safari 4 on up and mobile devices that run on iOS 4.2 on up. It also runs on Apple TV and Windows computers which have Quicktime 7. The keynote event starts on June 2 at 10 a.m. Pacific / 1 p.m. Eastern.

Apple Inc. (NASDAQ:AAPL) also claims to have an exciting announcement during the upcoming keynote. It is expected that Apple will introduce iOS 8 with Healthbook, new hardware, and an OS X upgrade.

Big possibilities in the upcoming iOS 8

One key highlight expected with the upcoming mobile operating system is the purported Healthbook. This fitness-related app was designed to aggregate fitness and other health-related features from other sources to one central location. There is a good chance this feature will work with the iPhone 6 and rumored iWatch.

There is also a possibility that iOS 8 will include a better mapping application. Apple Inc. (NASDAQ:AAPL) has relied on competitor Google Inc (NASDAQ:GOOG) (NASDAQ:GOOGL)’s Maps for the longest time. When the company initially tried to launch its version of maps, it was not as nearly as successful as hoped. There is a lot to look forward to from Apple this year.