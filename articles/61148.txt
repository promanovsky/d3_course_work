Tesla Motors Inc (NASDAQ:TSLA) has been fishing for the best deal in connection with its gigafactory plans. We’ve known for some time that Texas was one of the top contenders. However, if the state doesn’t lift the ban on Tesla selling its vehicles there, then it could be out of luck.

Tesla execs visit San Antonio

Thursday’s report from the San Antonio Express-News states that a pair of executives from Tesla Motors Inc (NASDAQ:TSLA) secretly met with San Antonio Mayor Julian Castro, as well as other city officials and even a representative from the utility company there. The newspaper said its sources claim that the executives who attended the secret meeting in Texas belong to a team which is looking at locations for the gigafactory. In addition to Texas, other states which are being considered include Arizona, New Mexico and Nevada.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

The sources even claimed that the group visited a possible site for the gigafactory in San Antonio. If this is true, however, it’s no different than what reportedly happened in Arizona, where officials also apparently met with Tesla Motors Inc (NASDAQ:TSLA) and showed them a place where the gigafactory could be built.

Texas governor compares Tesla to Apple

On his Facebook page, Texas Gov. Rick Perry posted some comments which suggest he may be having a change of heart regarding the ban on Tesla Motors Inc (NASDAQ:TSLA). He receives quite a bit of money from auto dealerships, so needless to say, their lobbyists have had a lot of pull in Texas.

However, his comments on Tuesday say that telling Tesla Motors Inc (NASDAQ:TSLA) that it can’t sell its own cars in Texas is like telling Apple Inc. (NASDAQ:AAPL) “it can’t sell its products at an Apple Store but has to sell them through Best Buy or Walmart instead.” He also said that it “makes no sense.”

What about the Tesla sales ban?

Of course there’s been no official word that keeping Tesla Motors Inc (NASDAQ:TSLA) from selling its cars directly to consumers will keep the automaker from building its gigafactory there. But making it possible would certainly be a nice incentive to offer, along with tax breaks and other incentives, of course.

According to Jalopnik, Arizona lawmakers are working on a bill which would allow Tesla Motors Inc (NASDAQ:TSLA) to sell its vehicles directly to consumers. However, Texas lawmakers are not planning to a special legislative session to talk about it, according to Gov. Perry.