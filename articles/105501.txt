PASADENA (CBSLA.com) — It’s not exactly a flying saucer, but a new lightweight spacecraft from NASA soon hopes to reach the skies of distant planets in our solar system.

Officials with NASA’s Jet Propulsion Laboratory (JPL) gave reporters a sneak peek Wednesday at its Low-Density Supersonic Decelerator (LDSD) project, which will launch a rocket-powered, saucer-shaped test vehicle into near-space on June 3 from the U.S. Navy’s Pacific Missile Range Facility on Kauai, Hawaii.

PHOTO GALLERY: NASA Debuts Saucer-Shaped Test Vehicle For Future Mars Missions

The LDSD mission will test what scientists are calling “breakthrough” technologies that will enable large payloads to be safely landed on the surface of Mars or other planetary bodies with atmospheres.

“The idea is to extend the human presence into the solar system,” NASA’s Jeffrey Sheehy said. “Mars is the first place to do that.”

Researchers say future robotic missions to Mars, and eventual human exploration of the “Red Planet”, will require that payloads more massive than the one-ton Curiosity Mars rover in 2012 be delivered to the surface.

The LDSD project aims to develop and test two sizes of inflatable drag devices and a large new, supersonic ringsail parachute that will not only enable landing of larger payloads on Mars, but also allow access to much more of the planet’s surface by enabling landings at higher altitude sites, according to officials.

“This project is aimed at providing the technologies for a technology area called ‘Entry, Descent and Landing,’ which is one of the major challenges for sustained exploration of Mars,” JPL’s Michael Meacham said.

Essentially, the vehicle will be launched by a balloon, at which point a rocket will accelerate it to 180,000 feet, which is four times the speed of sound, in order to see how it would handle the atmosphere of Mars.

“One of the big problems of exploring Mars is what we call ‘Entry, Descent and Landing’, getting stuff down to Mars,” Sheehy said. “The atmosphere is much different than Earth, it’s much thinner than Earth.”

Once tested, the devices could be used in Mars missions as early as 2018, officials said.