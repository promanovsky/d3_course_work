Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The dispute between singer Lana Del Ray and Frances Bean-Cobain looks like it may have finally been resolved.

Over the last 48-hours both have posted and deleted various comments on Twitter in reference to a press interview Del Rey gave.

The Born to Die singer had been asked by a reporter if she thought dying young was glamorous and responded by saying: "I don't know. Ummm, yeah. I wish I was dead already."

(Image: ?@alka_seltzer666)

Frances, 21, whose father Kurt Cobain took his own life in April 1994 at the age of 27, when she was aged 16-months-old, hit out at the singer.

She Tweeted: "the death of young musicians isn't something to romanticize".

And added: "I'll never know my father because he died young & it becomes a desirable feat because ppl like u think it's 'cool'.

"Embrace life, because u only get one life. The ppl u mentioned wasted that life. Don't be 1 of those ppl. ur too talented to waste it away."

(Image: Getty)

In response, Lana took to Twitter to defend herself claiming she had been asked "leading questions" in The Guardian interview.

"It's all good," she said to Bean on Twitter. "He [the interviewer] was asking me a lot about your dad. I said I liked him because he was talented not because he died young."

The 28-year-old continued: "The other half of what I said wasn't really related to the people he mentioned. I don't find that part of music glam either."

She then went on to accuse the journalist who interviewed her of twisting her words.

Lana tweeted: "His leading questions about death and persona were calculated."

However, it has now emerged that the singer has deleted all her tweets referencing the interview and the dispute with Frances .

That's not stopped the Nirvana front man's daughter getting the final word though.

She Tweeted: "I'm not attacking anyone. I have no animosity towards Lana, I was just trying to put things in perspective from personal experience."

Well said Frances - let's hope that's the end of the feud.