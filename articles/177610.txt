Can something be delayed when it hasn't even been released, yet alone announced? Apparently anything is possible for Apple's upcoming mobile operating system, which may now launch without one or more rumored new features.

9to5Mac has been aggressively leaking what Apple might be cooking up for this year's iOS 8 software, but now the website is backpedaling just a bit on what might be included with the initial release.

Apple is expected to introduce iOS 8 at its Worldwide Developer Conference in San Francisco next month, with a release to the public likely arriving later in the year alongside new hardware.

However, the latest scuttlebutt from inside Apple HQ is that some of the recently rumored new features might actually be delayed until the first major update, which will presumably be iOS 8.1.

Moving target

The report claims that Apple may be allocating additional resources from its iOS engineering team to the next big OS X release, which is expected to be a complete visual overhaul of the venerable Mac operating system.

Among the potential new additions to iOS 8 are Healthbook, TextEdit and Preview apps as well as a dedicated iTunes Radio app, the reintroduction of public transit directions to the built-in Maps app and Voice over LTE support.

Transit routing is cited as one potential feature that could be pushed back to iOS 8.1, instead paving the way for backend enhancements to iCloud and a rumored Shazam partnership, which would allow device owners to identify songs via Apple's Siri voice assistant.

Also said to be complicated matters is rumored new "multi-resolution support," which could pave the way for App Store titles to work across a variety of different screen sizes, including a larger iPhone 6 and a rumored Apple TV refresh with Siri voice control.