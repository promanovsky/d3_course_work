NEW YORK (TheStreet) -- It's been nearly six months and heads are still rolling in the aftermath of Target's (TGT) - Get Reportdata breach, which exposed the personal information of roughly 70 million customers. The latest is today's firing of Tony Fisher, the president of the company's Canadian operations.

This data breach didn't have to happen had the company followed some routine protocols. It will have a lot to do to regain the trust of shoppers and investors.

Target, whose shares trade around $57, down 10% for the year to date, was not the only retailer hacked. TJX Companies (TJX) - Get Report lost more than $250 million when 45 million accounts were breached over 18 months starting in 2006 through insecure firewalls. Thieves accessed TJX's WiFi and servers. Target's breach has since eclipsed that of TJX and has resulted in the resignation of CEO Gregg Steinhafel.

Fisher, the most recent head to roll, will be replaced with Mark Schindele, who served as vice president of merchandising operations. The company said the changes are effective immediately.

Although new to the role, Schindele has been instrumental in developing target's new store formats including Target Express and CityTarget in the U.S. His ideas has since been adopted by Walmart (WMT) - Get Report, which has begun to grow its so-called neighborhood stores.

Schindele has also led a global team and provided senior-level oversight to Target's merchandising operations, including systems, global sourcing and product development. So the company is in good hands. But the company is not done.

Target also announced plans to reduce executive compensation. According to documents filed Monday to the Securities and Exchange Commission, ousted CEO Steinhafel's pay package was cut by 35%, from $20 million to $13 million. This came as a result of shareholders complaining Steinhafel made too much money relative to the company's performance.

I've always considered Target to be an extraordinarily smart company. But I have no choice but to agree here with shareholders, especially since the Wall Street Journalrevealed the data breach could have been avoided had the company followed some routine protocols.

The good news is the company is working to shed this tainted image. Target recently invested $100 million to ensure a data breach doesn't happen again. Bu it's going to take much longer and investors can be impatient.

Target will report first-quarter results Wednesday. Given customers' fear of stolen identity, not to mention weather-related impacts, analysts have cut estimates since the company reported fourth-quarter results. The Street will be looking for 71 cents in earnings per share on revenue of $17.01 billion. Earnings are expected to decline more than 13%, while revenue is projected to grow by roughly 2%.

From its strategy of offering products at discounted prices while appealing to shoppers with an appetite for the high end, Target has been successful at finding its niche against the reach and scale of larger Walmart. But it has lost customers' trust. This continued house-cleaning is a good start at getting it back.

At the time of publication, the author held no position in any of the stocks mentioned.

Follow @Richard_WSPB



This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff.

>>Read more: It's Do or Die For Struggling Retailers

>>Read more: JCPenney Gets a Downgrade, Bullish Calls on Cigna, Sonus Networks