Rolf Harris was accused of brazenly attacking a prominent television celebrity during a television interview, it can be revealed today after new allegations emerged against the disgraced entertainer following his convictions for a string of sex attacks against four girls going back more than 40 years.

Harris faces a raft of new claims of sex assaults including against high-profile figures in both Britain and Australia that emerged during his trial that saw him convicted of indecent assaults on girls, one of them as young as seven or eight.

It emerged that the jury was not told about six other women who came forward to complain of groping and inappropriate sexual behaviour as their evidence was not used at the trial.

It included an alleged incident at a party in a pub for the talkshow host Michael Parkinson when Harris kissed the neck of a young woman in front of his wife, and an alleged assault on a 14-year-old at a Sydney motel when he told her: “Rolfie deserves a cuddle”. Another woman said he stuck his tongue into her mouth at an art class in Belfast.

Video: Harris thought he was 'above the law'

It has also emerged that other women in Australia have come forward to say they had been groped by the former children’s favourite, who was known as the ‘Octopus’ within sections of the entertainment industry.

The new claims and the history of his previous behaviour pointed to a series of high-risk sexual assaults often when he was surrounded by crowds of fans. He then acted as if nothing had happened.

The guilty verdict potentially opens the way for civil claims against the 84-year-old who has an £11 million fortune from a six-decade career in show business.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Rolf Harris: A life in pictures Show all 20 1 /20 Rolf Harris: A life in pictures Rolf Harris: A life in pictures Rolf Harris Rolf Harris in 'Stars on Sunday' TV Programme (1969 -1979) Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his daughter painting a wall together, 1967 Rolf Harris: A life in pictures Rolf Harris Rolf Harris on the 'Rolf Harris Show', 1973 Rolf Harris: A life in pictures Rolf Harris Rolf Harris in 1968 Rolf Harris: A life in pictures Rolf Harris Rolf Harris smiles during the 'The Rolf Harris Show' in 1973 Rolf Harris: A life in pictures Rolf Harris Rolf Harris with art book he wrote for children in London, 1978 Rolf Harris: A life in pictures Rolf Harris Australian entertainer Rolf Harris gets ready to blow his didgeridoo to promote a concert at Central Hall, Westminster, staged to raise money for research into cancer in children at the Royal Marsden Hospital. Keystone/Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris sketches a picture of Bambi, 1986 Rolf Harris: A life in pictures Rolf Harris bbc Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his wife at the David Frost's Society Party in London, 2001 Rolf Harris: A life in pictures Rolf Harris Rolf Harris performing at Glastonbury 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris performs in 'The Rolf Harris Show' in 1973 Rolf Harris: A life in pictures Rolf Harris "Rolf Harris" book signing in London, 2010 Rolf Harris: A life in pictures Rolf Harris Rolf Harris with his portrait of the Queen at a London art gallery in 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris at the Daily Mirror's Pride Of Britain Awards 2012 in London Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his wife Alwen attend the Press & VIP preview at The Chelsea Flower Show at Royal Hospital Chelsea in London, 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris surrounded by media leaves City of Westminster Magistrates Courts in London, 2013 Rolf Harris: A life in pictures Rolf Harris Entertainer Rolf Harris and his wife Alwen Hughes (L) arrive at Southwark Crown Court in central London, 6 May, 2014 Reuters Rolf Harris: A life in pictures Rolf Harris Rolf Harris is surrounded by members of the media as he leaves Westminster Magistrates Court, in central London, 2013 Rolf Harris: A life in pictures Rolf Harris Rolf Harris arrives at Southwark Crown Court in London, 27 June 2014

The final ignominy for Harris would come with the stripping of his honours, allowed if someone is jailed for more than three months. The Queen – whose portrait Harris painted in 2005 – will have the final say on whether the disgraced artist and performer loses his honours, which have included an MBE, OBE and CBE.

Witnesses at his trial gave evidence about attacks in Australia, Malta and New Zealand but Harris was only charged with crimes relating to four victims – aged from seven to 19 at the time of the attacks – that were committed in Britain.

The law in Britain was not changed until 1997 that meant Harris could not be charged with attacks that took place abroad before that time. Fresh allegations of abuse emerged during the trial of “inappropriate conduct” in his home state of Western Australia. The state’s force – which covers Bassendean where Harris was born – declined to comment on any questions “specific to an individual” about whether they were planning to investigate and charge Harris in those cases.

The guilty verdicts have completed the destruction of his reputation as a trusted children’s entertainer and defender of their interests. He appeared in a video intended for education in schools in 1985 to warn children of the dangers of being molested by adults.

The verdicts may not end Harris’s legal woes after new women came forward including one at the start of the trial who alleged that he assaulted her in front of the television cameras. Harris is accused of groping her during an interview which was then cut short. The presenter did not mention to anyone what had happened at the time.

November 2006: Rolf Harris with his portrait of the Queen. She will have the final say on whether the disgraced artist and performer loses his honours, which have included an MBE, OBE and CBE (Danny Lawson/PA Wire)

“It took place in a very public place,” said Sasha Wass, counsel for the prosecution during a pre-trial hearing. “It was during the course of that interview that she was indecently assaulted by the defendant.”

Harris allegedly put his hands “over her clothing and up her thigh, and higher still when she abruptly drew the interview to a halt. He cupped her buttocks which she describes as groping,” the barrister told Southwark Crown Court in the absence of the jury. “She then said the interview came to a halt.”

Two Australian radio presenters have also gone public during the trial on claims that Harris groped them after they both conducted interviews. Jane Marwick told radio station 6PR that the presenter grabbed her breast during a photo opportunity after an interview. She said that she put it down at the time to inappropriate behaviour by a “grubby old man”.

“Had I realised that there were allegedly people of very tender years involved I would have done something about that,” she told the station of the assault which she said was witnessed by a co-host. “I can’t comment on anyone else’s allegations but it’s time for me to say that I know that this man, in my case was capable of very inappropriate behaviour in a public place, in extraordinarily enough, in the company of others.”