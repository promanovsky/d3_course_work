Elon Musk has undercut Nasa's projection of landing humans on Mars by 2035, revealing plans to do the same by 2026.

"I'm hopeful that the first people could be taken to Mars in 10 to 12 years, I think it's certainly possible for that to occur," he told CNBC. "But the thing that matters long term is to have a self-sustaining city on Mars, to make life multi-planetary."

The rocket tycoon added that he plans to float SpaceX on the stock exchange, but only once its interplanetary mission is underway as right now its goals are too long term to attract many hedge fund managers.

"We need to get where things a steady and predictable," he said. "Maybe we're close to developing the Mars vehicle, or ideally we've flown it a few times, then I think going public would make more sense."

The bold claim follows Nasa's pledge to get man to Mars by 2035, but only if it can get help from the rest of the world and secure billions of dollars in public funding.

The magnate does not see himself as in competition with the space agency however, noting that "We would not be where we are today without the help of NASA."

Musk said that SpaceX's plan are important for the future survival of humanity, as he believes mankind will either become an interplanetary species or become extinct on Earth due to natural or man-made catastrophe.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The real challenge when it comes to Mars missions is not just getting man there however, but getting him there alive and then getting him back.