TV

Sookie laments the violence that has spread in the town as people began attacking each other, resulting in many deaths.

Apr 21, 2014

AceShowbiz - A new promo has been released to tease the final season of "True Blood". It features a voiceover by Sookie, who describes the chaotic situation in the town after vampires that are infected with the Hepatitis-V virus are on the loose.

Sookie says there's an attack on a church while the scenes show some fights and a pile of dead bodies kept in a chilling room. She laments how people in her town have become violent and turned on each other. At the end of the video, Sookie says, "There's no one left," with Sam, Jason, Alcide and Andy standing next to her.

The supernatural drama series will return with its seventh and final season on Sunday, June 15 at 10 P.M. on HBO. There will be some new villains, including "The Figure" which is described as being scary, ugly, mean and crafty. There's also Mr. Gus, a Japanese businessman who seeks for revenge after getting backstabbed in a business deal.

The upcoming installment will additionally reveal Bill's past and parents as the show flashes back to the year 1855. Fans will also see Bill's wife Caroline Compton giving birth to one of his kids in the 1850s.