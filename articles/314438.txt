Not only are many popular kids’ foods high in calories and sugar—here are 12 cereals of them that are more than 50% sugar by weight—but they may be packing too many vitamins and minerals, says the Environmental Working Group (EWG).

Manufacturers have been using nutrient fortification as a marketing tool to appeal to parents who want healthier foods for their families, but more isn’t necessarily better when it comes to certain nutrients. The advocacy organization studied 1,556 breakfast cereals and 1,025 snack bars and found that many contained substantially higher amounts of three nutrients—vitamin A, niacin and zinc—than is considered safe by the Institute of Medicine (IOM).

While nutrients are important for health, too much of certain nutrients can have harmful effects. Among the products the EWG tested, 114 cereals contained 30% or more of the Food and Drug Administration’s (FDA) recommended daily values for vitamin A, niacin or zinc, and 27 of the snack bars have more than 50% of the recommended values.

MORE: 12 Breakfast Cereals That Are More Than 50% Sugar

The fortification is especially concerning among children, since the values are set for adults, who are physically larger and need more of the nutrients. Children also typically eat more than one serving of cereal or snack bars in a day, and many also take vitamin supplements, further pushing them toward dangerously high levels of vitamin A, niacin and zinc.

Studies have shown that too much vitamin A can contribute to liver damage, brittle nails and hair loss. Overdoing it with zinc can interfere with normal immune functions, while niacin overages can cause rashes and vomiting.

MORE: Study Finds Folic Acid May Decrease Risk For Autism

The authors say that such excess isn’t as much a problem for children who eat more fresh and unprocessed foods. But in the average U.S. diet, earlier studies documented that 45% of children under 8 years old get too much zinc, 13% eat too much vitamin A and 8% get excessive amounts of niacin from their food alone.

To address the “over-nutrient-izing” of the American public, the IOM established upper limits of daily intake for common vitamins and nutrients, calculated for specific age groups and for pregnant women.

The EWG authors are calling for the FDA to adopt similar age specific guidelines for nutrient intake, and to revise its daily values, which were created before many manufacturers began fortifying their products with extra nutrients. And to avoid overdosing their kids on certain vitamins, parents should stay away from products that contain more than 20% to 25% of daily values for vitamin A, niacin and zinc.

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.