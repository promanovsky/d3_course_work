The dollar steadied on Friday after a significant rise in U.S. producer prices renewed speculation the Federal Reserve will hike interest rates ahead of schedule.

Earlier this week, the Fed signaled they are in no hurry to tighten monetary policy.

Today's inflation data, however, could be a game changer for policy makers on the fence about whether to hike rates soon after bond-buying has wound down completely.

The dollar index was last at 79.42 for a weekly loss of 1.2 percent, its worst decline this year.

Traders sifted through a pile of economic data today, including consumer price inflation figures from Germany and China.

The dollar was little changed at $1.3890 versus the euro, holding near a 2 1/2-year low set back in March.

Mid-morning gains helped to dollar to $1.6730 versus the sterling, up about 0.3 percent from yesterday.

The buck inched up slightly from a monthly low near Y101.30 versus the resurgent yen. The yen has gained 2.6 percent this year, with gains accelerating this week amid weakness in global equities.

In economic news, U.S. producer prices recorded their largest increase in nine months in March as the cost of food and services rose.

The Labor Department said its producer price index for final demand advanced by 0.5 percent in March after edging down by 0.1 percent in February. That was the largest increase since June last year. Economists had been expecting the index to tick up by only 0.1 percent.

Harmonized inflation in Germany slowed to its lowest level in nearly four years, final data from Destatis showed. The harmonized index of consumer prices rose 0.9 percent year-on-year in March, in line with flash estimate.

China's consumer price inflation accelerated in March on higher food prices, but remained within the government's full year 3.5 percent target, data from the National Bureau of Statistics revealed.

Consumer prices rose 2.4 percent from a year ago following a 2 percent rise in February, matching economists' expectations.

For comments and feedback contact: editorial@rttnews.com

Forex News