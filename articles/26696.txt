Duke porn star Belle Knox wiggled across the stage at a Hell’s Kitchen strip club Tuesday night, as the infamous Blue Devil made her debut as an erotic dancer in New York.

“This is my first time dancing,” Knox told The Post just before hitting the stage. “I’m nervous, but I think it’ll be really empowering.”

Knox rocked a plaid miniskirt as she strutted the stage at about 10:30 p.m. and danced to the glam rock anthem “Cherry Pie” before shedding her top and shaking her moneymaker for the packed crowd.

“I chose the song ‘Cherry Pie.’ It’s young, it’s cute, it’s fun to dance to,” the comely coed gushed. “I’m still shocked that I have fans. I few months ago, I was like a normal girl.”

The porn actress — who is planning to make thousands with books, films and her own sex-toy line — said she didn’t even have time to practice her routine. She said she watched some YouTube stripper videos to learn the basics.

“I’ve been so busy, I haven’t been able to practice anything,” said Knox.

https://twitter.com/belle_knox/status/446098511902560256/

One club patron came to see Knox dance after watching her CNN interview, and said he was impressed by her routine.

“She’s an attractive girl,” the fan said. “I think she’s admirable. She stands for something. She speaks with conviction.”

The coed has made headlines after being outed by a fellow student.

She has said her porn career is a pro-feminist statement.

She said she plans to continue her education even though her career has sparked controversy.