The FCC's top official wants answers about what's going on "under the hood" when content providers like Netflix make business deals with Internet service providers.

During the Federal Communications Commission's monthly open meeting Friday, Chairman Tom Wheeler said that the agency has already asked for more details on "peering" deals that Netflix has reached individually with Comcast and Verizon. The FCC, he added, has also requested information about deals between other ISPs and Internet content companies, all as part of its review to develop new Net neutrality rules.

The FCC's examination of these business deals comes as the agency gathers comments on a controversial proposal to reinstate Net neutrality, or Open Internet, rules that had been struck down by a federal appeals court in January. The debate over the new rules has hit a fever pitch in the last few weeks, since the proposal went public May 15, as thousands of people have submitted comments. In those comments, Wheeler said, the agency has seen questions about who is really responsible for network congestion: the ISPs or the companies delivering content over the Internet.

"The bottom line is that consumers need to understand what is occurring when the Internet service they've paid for does not adequately deliver the content they desire, especially content they've also paid for." Tom Wheeler, chairman, FCC

This issue has come to light as Netflix, provider of the highly popular streaming video service, has turned up the heat on its broadband partners, claiming that these ISPs have been strong-arming Netflix to pay fees to deliver video traffic requested by their joint customers. Netflix has been in a public battle with Comcast and Verizon over these so-called peering arrangements, which allow it to connect its service directly to ISP networks.

Wheeler previously has stated that these interconnection arrangements do not fall within the scope of any Net neutrality protections. But he said he has directed the FCC staff to gather more information on these peering deals as part of its overall effort to find out what's really happening on the Internet.

"The bottom line is that consumers need to understand what is occurring when the Internet service they've paid for does not adequately deliver the content they desire, especially content they've also paid for," he said. "In this instance, it is about what happens where the ISP connects to the Internet. It's important that we know -- and that consumers know."

Kevin Huang/Fight For the Future

Still, the chairman was careful to state that the FCC has not changed its position with regards to whether these relationships should be considered under its upcoming Net neutrality rules. Instead, he emphasized the importance of gathering information so that the agency can decide if any additional regulation may be needed.

"To be clear, what we are doing right now is collecting information, not regulating," he said. "We are looking under the hood. Consumers want transparency. They want answers. And so do I."

In a statement, Netflix, which has been pushing regulators to look more closely at these arrangements, said it welcomes "the FCC's efforts to bring more transparency in this area."

Verizon and Comcast also said they welcomed the FCC's inquiry into these business deals.Verizon said that the peering relationships, such as the one it has in place with Netflix, have always been handled in commercial agreements, which have worked well for consumers.

"We are hopeful that policy makers will recognize this fact and that the Internet will continue to be the engine of growth of the global economy," Verizon said in its statement.

Comcast said that it has always been open about its interconnection deals. And it hopes that the FCC's inquiry and the attention that Netflix has brought to this issue will be used to educate the public more about how the Internet works.

"We have long published our peering policies for example, and are open to discussions about further disclosures that would benefit consumers," the company said in its statement. "We also have voluntarily shared a vast array of information about our peering and interconnection practices with the FCC."

Netflix's public battle with ISPs

The public battle between Netflix and its ISP partners Comcast and Verizon has been going on for months. Even though Netflix has signed interconnection deals with both companies, it has recently been blaming them publicly for slowing down its streaming video traffic to Netflix broadband customers by refusing to make necessary network upgrades to support its traffic.

In an effort to make it clear who was at fault for poor Netflix performance on certain broadband networks, Netflix began publishing its ISP Speed Index from its own network testing data that shows performance on various ISP networks. Comcast and Verizon placed near the bottom of the list. Earlier this month, the company began posting messages to its users experiencing network congestion directly blaming ISPs for the buffering. In response, Verizon has threatened to sue.

Meanwhile, executives from Comcast and Verizon have called Netflix's claims bogus, stating that what's really been happening between the companies has been quibbling over business arrangements over who will bear the cost of network upgrades. They say that the fees Netflix has been asked to pay are standard in the industry.

"Internet traffic exchange on the backbone is part of ensuring that bits flow freely and efficiently and all actors across the system have a shared responsibility to preserve the smooth functioning and highly competitive backbone interconnection market," a Comcast spokeswoman said in a statement.

Verizon reiterated this point in its own statement: "Internet traffic exchange has always been handled through commercial agreements. This has worked well for the Internet ecosystem and consumers. "

Still, the FCC, ISPs and even Netflix agree that the peering and interconnection issue is separate from the current debate over new Net neutrality rules. But Netflix CEO Reed Hastings and now thousands of Netflix subscribers have been asking the FCC to take a look at these relationships anyway to determine whether big broadband providers are abusing their market power.

Wheeler said that this is an important question to answer.

"Consumers must get what they pay for," he said. "As the consumer's representative we need to know what is going on."