The strategy announced Monday will still cost an estimated $100 million, and it creates many uncertainties, especially for insurance companies and consumers. Some customers might eventually need to change insurance plans.

Simultaneously, the state is preparing to temporarily join the federal HealthCare.gov insurance marketplace in case the replacement system is not ready by the fall.

Massachusetts plans to scrap the state’s dysfunctional online health insurance website, after deciding it would be too expensive and time-consuming to fix, and replace it with a system used by several other states to enroll residents in plans.

As late as March, the state had considered rebuilding the balky Health Connector site, which has left thousands of consumers frustrated and many without coverage for months. But Sarah Iselin, the insurance executive whom Governor Deval Patrick tapped to oversee repairs to the site, said that approach turned out to be far too risky.

Advertisement

The state’s online insurance system must be ready by Nov. 15 for consumers to enroll in new health plans for 2015, and Massachusetts is one of several states under pressure from the Obama administration to make sure it meets the deadline.

“The responsible and prudent thing to do was to have another plan,’’ Iselin said in an interview. “Unlike other IT projects where the deadlines are self-imposed, we have a law. It’s not flexible.’’

Iselin said that adopting the new software, called hCentive, and preparing to connect to the federal website simultaneously will cost about $100 million through 2015. That is about 30 percent less than the estimated expense of rebuilding the existing site, she said. But it is unclear how much money the federal government will contribute and how much will fall to Massachusetts taxpayers.

“That’s what we’re talking to them about right now,’’ Iselin said. “We have been very clear that digging ourselves out of the hole we are in is going to cost us more money than we originally anticipated.’’

Advertisement

Another unknown is whether the transition will create disruption for consumers. Eric Linzer, a spokesman for the Massachusetts Association of Health Plans, said some insurers may not be able to afford to remain in the program, meaning consumers could end up having to switch coverage.

“I can’t overstate the complexity and technical issues that come with not having to develop just one but two separate systems,’’ he said. “Given the time frame in which all this has to be implemented, this is going to be a significant undertaking for plans.’’

Massachusetts also provides more generous subsidies than the federal health insurance program for residents with incomes below 300 percent of the federal poverty level. Iselin said whether the state can retain those unique aspects of its program if it connects to the federal site is still under discussion with the Obama administration. According to the state’s plan, use of the federal website, if necessary, would be for no more than a year, just until hCentive is ready.

“Federal officials haven’t made any commitment to what they can do yet,’’ Iselin said. “While there are many details to work through, we are committed to having the least disruptive experience as possible.’’

Officials from the US Centers for Medicare and Medicaid Services said the federal website is designed so it can be tailored to individual states’ needs. They said they will continue to work with Massachusetts on the details of its website.

Advertisement

“CMS is committed to working closely with states to support their efforts in implementing a marketplace that works best for their consumers,” Alicia Hartinger, an agency spokeswoman, said in a statement. “We are working with Massachusetts to ensure that all state residents have access to quality, affordable, health coverage in 2015.”

Representative Jennifer E. Benson, a Democrat and cochairwoman of the Legislature’s Joint Committee on Health Care Financing, said she had several concerns about the plan after being briefed on it by Iselin on Monday morning.

“I agree with the strategy the administration is employing but remain concerned with both the time frame and cost to the Commonwealth,” Benson said in a statement. “I believe at this point this is the best the administration can do within the current parameters and have to wonder why this avenue, using an ‘off the shelf’ solution, wasn’t more thoroughly considered at the outset of the project.”

State Senator James T. Welch, the Democratic Senate chairman of the committee, said he is hopeful the plan will succeed. “Maybe it’s not the time or place to recreate the wheel,” he said. “Let’s look at what other states have done.”

The technology company hCentive, based in Virginia, has helped build exchanges in Colorado, Kentucky, and New York.

Massachusetts is among four states that have been under pressure from the Obama administration to fix their faulty websites, said John E. McDonough, a professor of public health practice at the Harvard School of Public Health. The others — Maryland, Minnesota, and Oregon — “seem to be getting more attention,” he said, adding that, locally, “the decibel level on complaints would be higher were Governor Patrick running for reelection.”

Advertisement

Representative Vinny deMacedo, a Plymouth Republican, said he was surprised that a state that is home to world-class universities and high-tech firms has had so many problems with the website.

“It’s obviously incredibly disappointing,” he said. “I don’t know where the ball was dropped, but this is a very difficult situation and I can’t imagine that Massachusetts, one of the leading software states in the country, is having such a difficult time building a functioning website.”

Massachusetts had the first online health insurance marketplace in the country, created under its landmark 2006 law mandating coverage for most residents. The website worked well until it was revamped last year to meet the demands of the federal Affordable Care Act.

The new website was supposed to tell consumers whether they qualified for a subsidized plan, calculate the cost of coverage, and enable them to compare plans and enroll. It has not worked properly since it was launched in October, leading the state to encourage people to fill out paper applications instead. The flaws forced the state to enroll tens of thousands of residents in temporary insurance plans through the state Medicaid program.

In March, the state notified CGI, the contractor that created the website, that the deal was being terminated. The $68-million contract with the Montreal-based technology consulting company expires in September, and the state is still negotiating exactly how much the state pays for the work. It has paid just about $15 million so far.

Advertisement

“None of us wanted to be in this position,’’ Iselin said. “It’s created extraordinary challenges for everyone involved.’’

Despite the website’s problems, she pointed out, Massachusetts continues “to lead the nation in having the lowest number of uninsured’’ residents.

More coverage:

• Mass. dumps health insurance website contractor

• 50,000 filings for health coverage in limbo

• Health Connector chief cites vendor for site woes

• Troubled Mass. health site leads to feud on $45m grant

Liz Kowalczyk can be reached at kowalczyk@globe.com.