BlackBerry launches new budget handset

Share this article: Share Tweet Share Share Share Email Share

Jakarta - BlackBerry launched a low-cost touchscreen device in Jakarta, the Z3, as the embattled smartphone maker looks to revive sales in emerging markets like Indonesia where its once-fervent following has shrivelled. The handset, unveiled at a glitzy launch event in the Indonesian capital on Tuesday, is the first in a line of devices being made with FIH Mobile, a unit of the giant Taiwanese Foxconn Technology Group best known for assembling gadgets like iPhones and iPads for Apple. The success of the handset retailing for less than $200 (about R2 000) could well decide the outcome of both BlackBerry's tie-up with the contract manufacturing giant and its own future in smartphones. The Z3 Jakarta Edition will hit store shelves on May 15. “If this device allows them to grow again, even if it's just small, steady growth, that's a success in itself. That says there is still room for BlackBerry in Indonesia,” said Ryan Lai, market analyst at consultancy IDC. The Z3 is the first phone to be launched by BlackBerry since new Chief Executive John Chen took the helm late last year. It's initially being launched in Indonesia, but will be gradually introduced in other markets as well.

Waterloo, Ontario-based BlackBerry hopes that the device and others to follow will help it claw back some of the collapse in its market share, ceded to Apple's iPhone and Samsung Electronics's line of Galaxy devices powered by Google Inc's Android operating system.

“If the market doesn't receive this product well, then we definitely have some negative issues to deal with,” Chen said at the launch at Jakarta's Ritz-Carlton hotel.

Just two years ago, the Canadian firm had a 40 percent share of the Indonesian market, shipping more than 600 000 handsets per quarter in a country once known as “BlackBerry Nation”.

But the launch of the premium, high-priced BlackBerry 10 last year failed to attract buyers in a country where nearly 40 percent of the population live on about $2 a day. The company's market share has slumped to just four percent, with shipments of around 100 000 devices per quarter, according to IDC.

Indonesia is now dominated by Samsung, which sells about one of every three smartphones in Southeast Asia's largest economy.

Chen hopes that the Z3 and other devices to follow spark a change in the company's fortunes. The Z3 is being launched at a price point below $200 to address one of the big turnoffs for consumers in emerging markets - BlackBerry 10 devices being too pricey.

“From conception to delivery, the BlackBerry Z3 Jakarta Edition was designed specifically with out Indonesian customers in mind,” Chen said in a statement. The device will allow users to type in Bahasa and come with a special set of BlackBerry Messenger, or BBM Stickers featuring local characters.

Later this year, BlackBerry will launch a new, non-touchscreen device dubbed the BlackBerry Classic in partnership with Foxconn. The handset will see a return of the command keys that include 'Menu,' 'Back,' 'Send' and 'End' buttons, along with a trackpad. BlackBerry hopes the move will address the concerns of those users who found their new devices hard to navigate.

For Foxconn, the tie-up fits with plans to set up a manufacturing plant in Indonesia to build smartphones and other electronic devices. The Taiwanese company's ambitions have been on hold for years due to drawn-out talks over tax breaks, property and import restrictions. - Reuters