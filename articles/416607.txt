Chinese millionaire Chen Guangbiao, the self-proclaimed "most influential person of China," claims that he did a record-breaking ALS Ice Bucket Challenge.

The Shanghaiist reports that Guangbiao claimed that he sat in ice water for 30 minutes on Friday.

Advertisement

He also said that if anyone can beat his record in the next six days that he would donate one million yuan to the cause.Guangbiao posted an nearly 12-minute long video showing his team breaking up large blocks of ice out of the back of a truck. Guangbiao was then filmed sitting in a large blue trashcan while his team dumped water and small cubes of ice on him.

Because of this, he has been accused of cheating. Some people think that the smaller ice cubes were fake.

Advertisement

Guangbiao doesn't even appear to flinch as he's doused with water and ice.

Guangbiao denied those accusations on posting on website Weibo that he would show himself eating the ice to prove that it's real.

Advertisement

It gets even weirder. Toward the end of the video, he's wearing red underwear and is sandwiched between two slabs of ice as someone breaks it on him using a sledge hammer. Who's surprised? He's infamously eccentric.

Guangbiao, 46, is a recycling magnate with a reported wealth of $740 million . According to Reuters, he's "something of a celebrity" in China.

He has many self-proclaimed accolades. For example, his business card describes himself as the "Most Influential Person of China, Most Prominent Philanthropist of China, China Moral Leader, China Earthquake Rescue Hero, Most Well-known and Beloved Chinese Role Model, Most Charismatic Philanthropist of China." Perhaps he'll include the ALS Ice Bucket Challenge on there now.

Advertisement

Watch below: