European Central Bank President Mario Draghi signalled policy makers are ready to take action in June should they see low inflation becoming entrenched.

“What we need to be particularly watchful for at the moment is, in my view, the potential for a negative spiral to take hold between between low inflation, falling inflation expectations and credit, in particular in stressed countries,” Draghi said in a speech at the ECB Forum in Sintra, Portugal. “We are not resigned to allowing inflation to remain too low for too long.”

Watchful: Mario Draghi, president of the European Central Bank. Credit:Bloomberg

Mr Draghi said the key issue for the ECB was the timing of any action. Officials have said they’re working on a package of possible measures for the June 5 policy meeting, including interest-rate cuts and liquidity injections, while holding out the prospect of quantitative easing as a more powerful option.

Mr Draghi didn’t elaborate on any possible measures beyond his comments after the May 8 policy meeting, when he said officials were “comfortable” with acting next time and willing to use unconventional instruments if needed.