Tesla Motors CEO Elon Musk once thought the electric car company’s lineup would spell out SEX. There would be the Model S sedan, the upcoming Model X sport-utility vehicle and later, the Model E, a smaller, less expensive vehicle for the masses.

Stymied by Ford Motor Co. – it has the rights to E – Musk is now going for Model 3 for the third car, he told the British automotive news website Auto Express.

The Model 3 will go on sale in 2017 for about $40,000, said Tesla spokesman Simon Sproule, although the price could change as the vehicle gets closer to market.

The car will be about 20% smaller than Telsa’s Model S and much less expensive. The Model S starts at about $71,000 and can rise above $100,000, depending on options. The Model X is expected to have similar pricing when it comes out next year.

Advertisement

The Model 3 has great potential, said Jack Nerad, an analyst with Kelley Blue Book, the auto information company.

Consumers have balked at electric cars because with the exception of Tesla’s expensive Model S, their range between charges – just 70 to 80 miles depending on the vehicle – wasn’t large enough.

The range on Tesla’s Model S starts at about 200 miles, depending on the model, according to the Environmental Protection Agency.

“A versatile vehicle with significant range – say 200 miles on a charge – and a quick charge ability would go a long way toward moving EVs from a niche product to a mainstream choice,” Nerad said.

Advertisement

“Electric vehicles are clean, quiet and can be fun to drive as Tesla Motors has proven. But until a substantial portion of potential buyers determine that an EV can fulfill all their automotive needs as a substitute for a gasoline- or diesel-powered car their market will be limited,” Nerad said.

Previously known as Generation 3, the Model 3 also is dependent on Tesla getting its giant “gigafactory” battery facility up and running before the vehicle’s production.

“This is the car that will be enabled by the lower-cost batteries coming from the gigafactory,” Sproule said.

Five states -- California, Nevada, Arizona, New Mexico and Texas -- are competing for the factory. Tesla is expected to narrow the field within the next several months.

Advertisement

Sproule said Tesla settled on “Model 3" for the smaller vehicle because it will be built on the third vehicle platform and architecture used by the company. The first was its small Roadster two-seater, which is no longer for sale. The second is the platform used for the Model S and Model X.

Like the Model S and Model X, Model 3 will be built at the Palo Alto company’s factory in Fremont.

Tesla is selling about 20,000 cars in the U.S. annually. That is expected to increase with the introduction of the Model X next year and take a big jump with the Model 3, which Musk believes will quickly become the automaker’s best seller.

Follow me on Twitter (@LATimesJerry), Facebook and Google+.