Microsoft has this week been showcasing a demonstration of their new Skype Translator at the Code Conference which has been designed to enable people to communicate speaking different languages in real-time using the Skype service.

Unfortunately the new Skype Translator technology isn’t yet available to the public but Microsoft has explained that a beta version will be made available for Windows 8 users sometime before the end of 2014.

Watch the video below to learn more about the new Skype Translator and how it will be held to help Skype users communicate with other users in their native language.Microsoft explains :

“Peter Lee, Microsoft Research VP shares insights and a sneak peek into the Skype Translator, derived from decades of research in speech recognition, automatic translation, and machine learning technologies.

The Skype Translator is now being developed jointly by Skype and Microsoft Research teams, and combines voice and IM technologies with Microsoft Translator, and neural network-based speech recognition to deliver near real-time cross-lingual communication.

With the Skype Translator, we’re one step closer to universal communications across language barriers, allowing people to connect in ways never before possible. In Lee’s words “It’s truly magical.”

For more information on the new Skype Translator software jump over to the Microsoft website for details.

Source: Liliputing : Recode

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more