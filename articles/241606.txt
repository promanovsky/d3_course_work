hidden

Facebook has changed a privacy setting on its social network, limiting the audience that sees new users' first posts in a reversal of a feature that had critics said forced people to over-share personal information.

The company said on Thursday that the status updates that new users post will now be viewable only to friends by default, instead of being viewable to the general public as it had previously been. Facebook users will continue to be able to customize their settings so that status updates can be seen by as big or small an audience as they want.

The change to the default setting, which Facebook quietly implemented a few weeks ago, is the result of feedback from users, Mike Nowak, a Facebook product manager, said in an interview with Reuters.

"Sometimes people have felt that they've been unpleasantly surprised that their information was more public than they expected or intended," he said. "The feedback that we received is that oversharing is worse than undersharing."

The move reflects a changing approach by Facebook in how it handles the information that people share on its social network of 1.28 billion users.

In 2009, Facebook introduced a feature that allowed users to share posts beyond their circles of friends. When new users signed up for Facebook, their status updates were automatically broadcast to the general public unless users manually altered the setting.

Thursday's reversal comes as many Internet users appear interested in limiting who sees their online activities. Mobile messaging and social networking apps such as Snapchat and Whisper offer anonymity features that have become popular with many users. In February, Facebook announced plans to acquire WhatsApp, a private messaging app, for $19 billion.

Facebook said on Thursday that it will now display a pop-up message informing new users of the option to publish the message more broadly if they wish.

Facebook will also begin showing all of its users a new "privacy checkup" feature that walks through certain privacy settings, such as who can see personal profile information and which third-party apps users are connected to.

Reuters