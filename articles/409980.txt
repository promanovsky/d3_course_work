Shutterstock photo

Investing.com -

Investing.com - The dollar was hovering close to multi-month highs against the euro and the yen on Thursday, one day after data showed that the U.S. economy rebounded strongly in the second quarter, but gains were held in check after the Federal Reserve indicated that rates will remain on hold for longer.

EUR/USD was little changed at 1.3395, not far from the eight-month trough of 1.3366 reached on Wednesday.

The pair was likely to find support at around 1.3350 and resistance at 1.3425.

The dollar rallied against the other major currencies on Wednesday after official data showed that U.S. gross domestic product expanded at an annual rate of 4.0% in the three months to June, outstripping forecasts of 3.0%.

In addition, the contraction in the first quarter was revised to 2.1% from a previously reported 2.9% contraction.

The dollar pared back some gains after the Fed's latest rate statement said that considerable slack still remains in the labor market, despite the recent improvement in jobs growth.

The central bank also said inflation is rising and was moving closer to its long-term target.

Investors were also awaiting the U.S. nonfarm payrolls report for July on Friday for the latest indication of the strength of the recovery in the labor market.

The euro remained under pressure after data on Wednesday showing that the annual rate of inflation in Germany, the currency bloc's largest economy, slowed in July added to concerns over the threat of deflationary pressure in the region.

The euro zone was due to release preliminary data on consumer prices later on Thursday.

The euro was steady against the yen, with EUR/JPY at 137.67.

The dollar was steady against the yen, with USD/JPY at 102.80, holding below Wednesday's three month highs of 103.07.

The US Dollar Index, which tracks the performance of the greenback versus a basket of six other major currencies, was flat at 81.51, near Wednesday's 10-month peaks of 81.64.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.