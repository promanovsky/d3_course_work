Kim Kardashian has taken to her blog to pen an essay on racism and discrimination.

“To be honest, before I had North, I never really gave racism or discrimination a lot of thought. It is obviously a topic that Kanye is passionate about, but I guess it was easier for me to believe that it was someone else’s battle,” the 33-year-old reality star said in the blog entry.

“But recently, I’ve read and personally experienced some incidents that have sickened me and made me take notice. I realize that racism and discrimination are still alive, and just as hateful and deadly as they ever have been,” Kim added in this short excerpt.

WHAT ARE YOUR THOUGHTS on Kim Kardashian’s blog entry?

Click inside to read the full essay right now…

I never knew how much being a mom would change me. It’s amazing how one little person and the love I have for her has brought new meaning to every moment. What once seemed so important, now feels insignificant. It’s like I get to see the world for the first time again, but through someone else’s eyes. It’s a beautiful thing to feel and experience so much more, but with that beauty comes a flip side – seeing through my daughter’s eyes the side of life that isn’t always so pretty.

To be honest, before I had North, I never really gave racism or discrimination a lot of thought. It is obviously a topic that Kanye is passionate about, but I guess it was easier for me to believe that it was someone else’s battle. But recently, I’ve read and personally experienced some incidents that have sickened me and made me take notice. I realize that racism and discrimination are still alive, and just as hateful and deadly as they ever have been.

I feel a responsibility as a mother, a public figure, a human being, to do what I can to make sure that not only my child, but all children, don’t have to grow up in a world where they are judged by the color of their skin, or their gender, or their sexual orientation. I want my daughter growing up in a world where love for one another is the most important thing.

So the first step I’m taking is to stop pretending like this isn’t my issue or my problem, because it is, it’s everyone’s… because the California teenager who was harassed and killed by his classmates for being gay, the teenage blogger in Pakistan who was shot on her school bus for speaking out in favor of women’s rights, the boy in Florida who was wrongly accused of committing a crime and ultimately killed because of the color of his skin, they are all someone’s son and someone’s daughter and it is our responsibility to give them a voice and speak out for those who can’t and hopefully in the process, ensure that hate is something our children never have to see.