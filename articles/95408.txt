La Quinta's stock rose in the hotel chain's return to the stock market.

The Irving, Texas-based company was publicly traded for more than 30 years before being acquired by private equity firm Blackstone in 2006. Blackstone paid $2.3 billion in cash and also assumed La Quinta's debt, giving that deal a value of about $3.4 billion.

The IPO announced Tuesday night raised $651 million and valued the company at about $2.1 billion. Shares priced below the company's prediction, suggesting lukewarm demand from investors. The stock added 57 cents, or 3.4 percent, to $17.57 Wednesday afternoon after declining earlier.

La Quinta's debut comes amid growing demand for IPOs amid a rising stock market. The hotel industry has also started to recover from a deep slump after the recession.

Begun in 1968 in San Antonio, La Quinta runs select-service hotels, which typically have fewer amenities than full-service hotels. Under Blackstone's repositioning, La Quinta's franchise business has grown and the company says it has become more competitive with competitors such as Holiday Inn Express. It posted a profit last year from a loss in 2012, its sales improved and revenue per available room -- a key gauge of a hotel operator's performance -- grew nearly 7 percent. It had 834 hotels at the end of 2013.

Blackstone, which will still own a majority of La Quinta's stock after the IPO, is familiar with lodging companies.

It took Hilton Worldwide Holdings Inc. public in December in a $2.7 billion IPO.

Get the Biz Briefing newsletter! The latest LI business news in your inbox Monday through Friday. By clicking Sign up, you agree to our privacy policy.

La Quinta priced shares at $17, lower than the $18 to $21 it had projected. But it sold about 38.3 million shares, nearly 1 million more than initially expected. The banks managing the deal may buy another 5.7 million shares, which would add to the deal's proceeds. La Quinta plans to use the money to repay debt.

La Quinta Holdings Inc. trades on the New York Stock Exchange under the "LQ" ticker symbol.

The other company that debuted Wednesday, iKang Healthcare Group Inc., rose 10 percent on the Nasdaq to $15.40 under the "KANG" symbol.

IKang's IPO raised nearly $153 million. The Chinese health care provider priced 10.9 million American depositary shares at $14 each, the top of the expected range.