Netflix will launch its online movie and TV streaming service in six European countries including Germany and France later this year.

Netflix, the world's largest Internet subscription service, is expanding its presence in the international markets by launching the service in six new European markets later this year. The move is the company's biggest expansion in almost three years. The renowned online video service announced Tuesday that its service will be made available in Germany, Austria, Switzerland, France, Belgium and Luxembourg, but failed to specify the exact date and the prices.

The Los Gatos, California based company introduced its service in Europe in 2012 when it was first launched in the U.K. and Ireland. So far, the company has attracted over 48 million customers in more than 40 countries that stream more than 1 billion hours of TV shows and movies each month. The European expansion of Netflix follows its presence in the U.K. and Ireland, Denmark, Finland, Norway and Sweden. The company introduced its services in Netherlands just last year as a part of its European expansion.

"Upon launch, broadband users in these countries can subscribe to Netflix and instantly watch a curated selection of Hollywood, local and global TV series and movies, including critically-acclaimed Netflix original series, whenever and wherever they like on TVs, tablets, phones, game consoles and computers," Netflix said in a press release.

For Netflix, the US market has been the most favorable of all, with 35.7 million subscribers in the country. Germany ranks the highest in the European market, with 29.1 million subscribers streaming on-demand video content from Netflix as of last year, while France and Russia trail behind at third and forth position respectively, according to a report from Reuters.

Netflix's international presence is not as favorable but its U.S. operations are helping the company from sinking into losses. Last year, the company made a profit of $112 million despite losses from international markets of $274 million. But the company hopes to turn things around as it has set its long-term goal of 60 million to 90 million U.S. subscribers and more than 100 million internationally, a Washington Post report says.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.