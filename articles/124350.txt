“Transcendence” is a rarity among modern blockbusters. In place of flying men in tights and massive explosions, its biggest special effects are its ideas.

The film centers on a group of scientists led by Johnny Depp and Rebecca Hall, whose well-intentioned experiments with artificial intelligence lead to catastrophe — providing a cinematic illustration of Murphy’s law of unintended consequences. It’s also a film that asks tough questions about the ethics of technological innovation, one that should inspire intense debate given society’s increasing reliance on smartphones and web-enabled devices, as well as growing concerns about the use of big data and surveillance.

Beyond being an action film with a full deck of IQ points, “Transcendence” represents a risk in another way. It marks the directorial debut of Wally Pfister, an acclaimed cinematographer who helped bring Gotham to life in Christopher Nolan‘s “The Dark Knight” trilogy and won an Oscar for “Inception.”

Also read: Johnny Depp Makes First Promotional Visit to China, Hawking ‘Transcendence’

Judgment day for his inaugural behind-the-camera effort is Friday, when “Transcendence” opens nationwide. TheWrap spoke with Pfister about the perils and possibilities of technology, his transition to directing and what he learned from Nolan.

At the beginning of the film, Johnny Depp implies that every one wants to be God if given the chance. Do you think that our relentless pursuit of new and better technology is linked to a desire to play God?

I think so. The more power someone or something gets, the more temptation there is to play God. In this case, as the machine becomes sentient and more powerful, it acts in that way. It’s trying to use its power for benevolent reasons. On the one hand, we may think that manipulating the physical characteristics of people is scary and should not be tampered with — that it’s only God’s territory. In another, it’s hard to argue against healing infirm people, who in the film are afflicted with diseases that [the computer] can cure. [Depp] really believes he is using technology for good.

In the film, there aren’t straight heroes or villains. People seem to be motivated by good intentions even when their actions result in tragedy or violence. Was that ambiguity part of the attraction?

Absolutely. It was essential that the film be ambiguous and that it show these grey areas. Characters in the film’s opinions morph and change, and it’s not clear who the bad guys are and who the good guys are.

See video: ‘Transcendence’ Trailer: 5 Things We Learned From the Johnny Depp Thriller

Was it difficult to strike a balance between raising questions about the ethical ramifications of technology without seeming like a luddite?

Yes, I was definitely trying to strike a balance. My own view, which I channel through some of the characters, is that if the characters can use technology for the betterment of society, why not wipe out pollution? Why not heal people with diseases? But it’s important to look at both sides.

It’s a view that’s articulated by Cillian Murphy‘s character at one point, when he talks about people getting frustrated and sticking their cellphones in blenders. It’s that we should be careful about the possibility that we have an unhealthy reliance on technology. It’s really important that audiences connect with both sides of the debate before deciding where they land.

Do you think people are getting fed up with all the smartphones and the texting and the ubiquity of technology?

We are beginning to get frustrated with technology. I enjoy social media, for example, but I get tired of being asked all the time for my personal information. My Facebook profile says it’s only 66 [percent] complete because I refuse to tell them where I went to high school. It’s not a secret. If you asked me I’d tell you, but why do they need to know that?

The end of the film, with its image of a mainframe shutting down and a sunflower coming to life, is a tip of the hat to the notion that it might not be bad to shut stuff off once in awhile and appreciate what happens when you put a seed in the ground.

Also read: 21 Summer Movies We’re Dying to See – From ‘Transformers 4’ to ‘Godzilla’

Is the attraction of science-fiction that it is a good vehicle for raising questions about the way we live under the guise of entertainment?

I think any genre of film is good for that. First and foremost, I want to entertain the viewer, but I also want to give them some pause for thought. I enjoy large action films, but I find the greatest entertainment comes from something that’s thought-provoking.

Did being a cinematographer help you when it came to directing?

It got me about 25 percent of the way, but it was an important 25 percent. The value of the experience was in understanding how to run a set and of having to answer lots of questions while keeping things moving along. But directing opens up that narrow field of view and it becomes about working with the actors on their performances and the sound design and the visual effects. So that other 75 percent represents a huge expansion of talent. I’m visual by nature, but I prefer directing.

What did you learn about directing from your close collaboration with Chris Nolan?

The greatest lesson I learned from Chris Nolan is to keep my humility. He is an absolute gentleman on set and he is wonderful to everyone — from the actors to the entire crew, he treats everyone with respect.

Have you picked a follow-up to “Transcendence” that you’d like to direct?

I’m still reading scripts now. I’m trying to decide if I should stick with science-fiction or do something completely different.