A new study found that friends are genetically similar to each other.

"Friends are the family we choose." There couldn't be more truth in that, a new study finds. Researchers from the University of California, San Diego, found that friends that are not biologically related are still genetically similar to each other.

"Looking across the whole genome," James Fowler, professor of medical genetics and political science at UC San Diego, said in a press statement, . "We find that, on average, we are genetically similar to our friends. We have more DNA in common with the people we pick as friends than we do with strangers in the same population."

The study included 1,932 participants. The researchers compared pairs of unrelated friends against pairs of unrelated strangers. Researchers were surprised to find genome similarities between unrelated friends. No such observation was made among unrelated strangers.

Fowler and his team said that friends were "related" to each other as closely as a person is related to his fourth cousin. It's like they share the same great-great-great-grandparents.

"One percent may not sound like much to the layperson but to geneticists it is a significant number," the study authors said. "And how remarkable: Most people don't even know who their fourth cousins are! Yet we are somehow, among a myriad of possibilities, managing to select as friends the people who resemble our kin."

Of the genes most prominently expressed between pairs of unrelated friends, the researchers found that the olfactory system genes were overrepresented. This may have a prehistoric implication. In ancient times, people who liked the smell of blood hunted together whereas those who preferred the smell of wild flowers became gatherers.

Researchers said that our DNA could be a driving force behind the activities we are drawn to and the social activities we engage in. As such, we are more inclined to interact and foster friendships with people who are genetically similar, according to a CBS report.

Alternatively, researchers also found that the people we choose to associate with tend to be immunologically different, which may offer us extra immunological protection. This supports past research that found spouses tend to have different immune system genes.

This study also lends support to the view of humans being metagenomic - meaning we're not only a combination of our own genes but of the genes of the people with whom we closely associate.

A similar study conducted earlier this year found that individuals are more genetically similar to their spouses than they are to randomly selected individuals from the same population.

Further study needs to be conducted to determine whether the genetic similarities were restricted to only the Framingham study group, which were mostly Irish and Italian.

The research was supported by grants from the National Institute on Aging and the National Institute for General Medical Sciences. Findings were published online in the Proceedings of the National Academy of Sciences.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.