The deadly disease Ebola "haemorrhagic fever" is rapidly spreading across West Africa, primarily in Guinea and Liberia, says the charity Medecins Sans Frontieres (MSF).

Of 122 people affected in Guinea, 78 are known to have died since the first confirmed deaths six weeks ago.

Doctors had hoped the outbreak would be limited to the sparsely-populated south-eastern region of Nzerekore in Guinea, but confirmed cases have now emerged in the capital Conakry, which has a population of two million.

We are facing an epidemic of a magnitude never before seen - Mariano Lugli, MSF

Reports from the city say supermarket workers are wearing gloves as a precaution.

The internationally-renowned singer Youssou Ndour has cancelled a concert he was due to perform in the city, amidst fears that a large gathering of people would be dangerous.

Mariano Lugli, coordinator of MSF's project in Conakry said: "We are facing an epidemic of a magnitude never before seen in terms of the distribution of cases in the country: Gueckedou, Macenta Kissidougou, Nzerekore, and now Conakry."

First identified in 1976 in what was then Zaire, Ebola is highly contagious – spread by close contact - and kills between 25% and 90% of victims; there is no known vaccine.

It is thought this latest outbreak is the result of the human consumption of bats, which are a delicacy in the region. Guinea's government has now banned the sale or consumption of bats and sent a mobile laboratory to the Gueckedou region.

We believe it\'s safer to close our borders - Minister of Senegal Awa Marie Coll-Seck

The first reported cases in Liberia, to the south of Guinea, were those of two sisters, one of whom had visited Guinea recently. Of the seven people infected, four have died.

Walter Gwenigale, Liberia's health minister, issued a warning for people to abstain from having sex in a bid to prevent the disease spreading further.

To the south of Guinea, Sierra Leone has reported five suspected cases and to the north, Senegal has now closed its borders. Health Minister of Senegal Awa Marie Coll-Seck said:

"When it used to be only in the south of Guinea, we didn't do anything special. But now that it's reached Conakry, we believe it's safer to close our borders. We have also closed all weekly markets, known as luma, in the south. And we're having some discussions with religious leaders regarding big religious events."

At a regional conference in the Ivory Coast foreign ministers from the countries affected said the Ebola epidemic now poses a "threat to regional security".

The EU has pledged €500,000 to fight the rapid spread of the disease.