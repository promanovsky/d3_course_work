For some reason it sounds exactly like Three Men and a Baby. Actor Ashton Kutcher joining the likes of Facebook co-founder Mark Zuckerberg and Tesla chief Elon Musk to manage a new baby in the house - but we heard it's an artificial one.

The three guys pooled in funds worth $40 million in a secretive artificial intelligence company called Vicarious FPC.

The investment of Kutcher, in particular, comes through his fund, "A-Grade Investments." Other backers of Vicarious include Peter Thiel, a PayPal founder and Facebook investor, who invested $1.2 million in 2010; a new group of investors that include another Facebook co-founder Dustin Moskovitz also gave $15 million. Another investor came from a venture capital firm named Formation 8, founded by Joe Lonsdale who is also a co-founder of a data-mining software maker Palantir Technologies.

According to VPC's co-founder Scott Phoenix, the company would duplicate the neocortex of the human brain. Co-founding the company is Dileep George, a neuroscientist.

The neocortex is like the CPU of the human anatomy that handles motor and sensory functions. It also understands language and does math. Now, Vicarious' role is to attempt translating its function to a computer code, reports said on Friday.

"You have a computer that thinks like a person," Phoenix said. "Except it doesn't have to eat or sleep."

He added it also aims beyond recognizing images, further saying that its next milestone is to create a computer that can comprehend not just shapes and objects but as well as textures associated with these items.

He also hopes that it would also learn how to perform other things, such as cure diseases, produce cheap and renewable energy as well as perform jobs that engage most humans.

"We tell investors that right now, human beings are doing a lot of things that computers should be able to do," Phoenix also said.

What's in stake for these investors then?

Research says Facebook, for example, aims to turn enormous amounts of user-shared information into a database of wisdom. When you ask a question to Facebook, it would provide an answer that is based on user-shared facts.

A spokesman from Facebook, however, clarified that the Zuckerberg made a personal investment. The spokesman added that it doesn't necessarily reflect the social media network's interest in using the said artificial-intelligence software.

However, it's still a long road ahead toward their goals and to profiting from it, according to Phoenix.

Vicarious is not alone, though. Companies like Amazon and Google are among those companies looking into robotics as well. Google, specifically, has its interest in improving artificial intelligence. It acquired earlier this year an artificial-intelligence start-up called DeepMind for about $400 million.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.