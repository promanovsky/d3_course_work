LOS ANGELES, June 30 (UPI) -- Robert Downey Jr. issued a statement following the arrest of his son, Indio Downey, alleging that he "likely inherited" addiction from the actor.

Downey, 20, was arrested in West Hollywood on Saturday after police found a "a controlled substance [believed] to be cocaine" on him after was spotted smoking from a pipe while he was a passenger in a car.

Advertisement

The Iron Man actor, who dealt with drug abuse in past, told Us Weekly after the incident that, "Unfortunately there's a genetic component to addiction and Indio has likely inherited it."

"There is a lot of family support and understanding, and we're all determined to rally behind him and help him become the man he's capable of being," Downey continued.

"We're grateful to the Sheriff's department for their intervention, and believe Indio can be another recovery success story instead of a cautionary tale."

Downey Jr. was also involved in several drug related arrests during his battle with addition in the early 2000s. The actor's drugs of choice were believed to be cocaine, heroin and marijuana.

Downey was released from jail on $10,000 bail after midnight Sunday.