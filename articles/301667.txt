MINNEAPOLIS (WCCO) — Minnesotans involved in the fight against Native American mascots are cheering Wednesday’s federal action against the NFL’s Washington Redskins.

The Trademark Trial and Appeal Board ruled the term “Redskin” is disparaging, and canceled six of the trademarks the team has held for decades.

This means the team owner, Dan Snyder, could lose money on future sales of merchandise. Without trademark protections, others would be free to sell items using the Redskins name.

Minnesota is home not only to the American Indian Movement (AIM) but also the National Coalition on Racism in Sports and the Media — two groups that have been battling this issue for decades.

Clyde Bellecourt is a co-founder of AIM who can remember protesting mascots as far back as the 1970s. In the past 15 years, he said at least 1,200 teams have changed mascots, but many remain.

“I’m 78 years old, and I expect to be around for a while,” he said, “and they’re going to have to deal with this issue. We’re never going to give up. We can’t give up.”

A Minnesotan has also emerged in Congress as a leading opponent of the Redskins name – Rep. Betty McCollum (D-St. Paul).

“I mean it’s a hateful term,” she said. “It’s a term that refers to when bounties were paid for scalping, murdering men, women and children.”

Today’s action from the patent office takes away six trademarks, but the team’s owner has vowed to appeal. The chair of the National Coalition on Racism in Sports and the Media, David Glass, is optimistic.

“We know it’s going to be a journey to be successful,” he said, “but there’s little victories along the way, and today was one of them.”

Many WCCO viewers strongly disagree with Wednesday’s action, as evidenced by comments on the station’s Facebook page.

Many called it “political correctness” out of control. Others said it’s not the Patent Office’s job to decide what’s offensive.

What do you think?