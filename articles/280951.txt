Justin Bieber will not be charged in relation to an alleged tussle with a woman over a cellphone

Justin Bieber will not be charged in relation to an alleged tussle with a woman over a cellphone.

The Los Angeles County district attorney's office has declined to file a felony charge against the pop star over the alleged incident at a family entertainment centre.

The case was referred to the Los Angeles city attorney's office for consideration of a possible misdemeanour prosecution.

The woman said Bieber pulled her phone out of her purse to see if she was videotaping him when he was hitting balls at a batting cage. She showed him she was not.

The district attorney's report stated that the woman also said her daughter was crying, but when police interviewed the girl, she said she was crying out of excitement at seeing the star.