AT&T Inc is in active talks to buy satellite TV provider DirecTV and may complete a deal in the next few weeks that could be worth close to $50 billion, two people familiar with the matter said on Monday.

The second-largest wireless operator is discussing an offer in the low- to mid-$90s per share for DirecTV, one of the people said, compared with the company's closing price of $87.16 on Monday.

A bid near $95 per share would value DirecTV at more than $48 billion based on its shares outstanding, and would represent a premium of more than 20 percent to its stock price before news of AT&T's interest first emerged on May 1.

The deal price has yet to be finalized and terms could still change, the people said, adding that discussions are continuing. They asked not to be named because the matter is not public.

Other details also have yet to be worked out, such as a break-up fee as well as a potential role for DirecTV Chief Executive Officer Mike White, the second person said.

The talks are the latest sign of a rising tide of potential megadeals in the telecoms, cable and satellite TV space, which is being roiled by Comcast Corp's CMCSA.O proposed $45 billion takeover of Time Warner Cable Inc TWC.N as well as market forces such as the rise of Web-based TV and surging mobile Internet usage.

AT&T and DirecTV declined to comment. Bloomberg News earlier reported that AT&T was offering to pay around $100 per share for DirecTV, whose management team will continue to run the company as a unit of AT&T. (http://link.reuters.com/xyf39v). The Wall Street Journal said a deal could happen in two weeks.

DirecTV shares rose 6 percent to $92.50 in extended trading on Monday.

DirecTV is working with advisers including Goldman Sachs Group GS.N to evaluate a possible combination following a recent takeover approach from AT&T, Reuters reported last week.

A combination of AT&T and DirecTV would create a pay television giant close in size to where Comcast will be if it completes its pending acquisition of Time Warner Cable. For that reason, the proposed merger is likely to face a prolonged battle to convince regulators to allow further consolidation in pay-TV.

"This is not the first time that AT&T and DirecTV have danced around the fire and thought if they could give it a go," said ReconAnalytics analyst Roger Entner.

"They both looked at each other for at least 10 years. Both kind of came to the conclusion that it was in the right environment. It makes a lot of sense to get together, but there was never the right regulatory environment for it."

A Comcast-Time Warner Cable merger would call for a counterweight like a combined AT&T-DirecTV, Entner said. He added that the merger would make sense for DirecTV given the decline of satellite TV.

"They both see the Grim Reaper at the horizon. DirecTV hasn't gone out and bought spectrum. Dish has, so DirecTV needs to find a partner, and AT&T is that partner," he said.

Some investors have also speculated about a potential tie-up of DirectTV and smaller rival Dish Network Corp DISH.O.

But Dish Chairman Charlie Egan last week said his company, which attempted to buy DirecTV more than a decade ago, would not make a fresh approach at current prices even though he said such a tie-up would create many benefits.

© Thomson Reuters 2014

