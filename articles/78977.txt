AP REPORTER

All of the fantastically mixed-up characters in Heathers: The Musical seem plagued by the show's timeless catch phrase, a telling rhetorical question: 'What's your damage?'

There's plenty of damage to go around in Westerberg High, the wildly chaotic, often cruel, but always colorful wonderland at the center of off-Broadway's latest theatrical remake of a cinematic favorite.

Like the cult classic movie it's based on, this dark, demented comedy somehow keeps us in stitches though treading not-so-lightly on some of the most troubling subjects in the social curriculum of American youth - bullying, school violence and teen suicide.

Even bulimia is fair game, though according to one of the musical's three hopelessly shallow title characters, 'it's so `87.'

Broadway: Barrett Wilbert Weed plays Veronica and Ryan McCartan stars as J.D., the roles played by Winona Ryder and Christian Slater in the original movie

Heathers opened Monday at New World Stages, 25 years to the day that the 1989 film was released.



The lively, campy tribute combines all the twisted humor and teen spirit of its predecessor with a charming score of mostly tuneful melodies and fresh lyrics by Laurence O'Keefe and Kevin Murphy.



This reunion of Westerberg's social climbers, jocks, nerds and outsiders retains much of the original plot and revives many of the deliciously quotable lines the Daniel Waters screenplay is remembered for (including a certain now-famous expletive involving a chain saw and the adverb 'gently').

Most of these legendary quotes emanate from the crude but catchy lexicon of the Heathers, the three-headed power-clique who rule the school. Dressed in shoulder-padded blazers and skimpy miniskirts - and generously accessorized with Swatches and scrunchies - the girls terrorize less popular kids, while peppering their speech with the vague, disinterested appraisals 'so very,' and cutting insults like, 'Did you have a brain tumor for breakfast?'

Likable narrator and protagonist, Veronica Sawyer (Barrett Wilbert Weed), barters her marketable talent for forging handwriting to gain membership into the group, instantly elevating her social status a few notches.



But when she has a falling out with Heather No. 1, she gravitates to J.D. (Ryan McCartan), a mysterious transfer student with a dark streak.



That's when the violence begins, or as Veronica calls it, 'teenage angst with a body count.'



After getting its feet wet with a few concert performances at Joe's Pub in New York, Heathers made a successful premiere engagement in Los Angeles before coming to off-Broadway.



Great White Way: The director said all of the classic lines from the movie will appear in the musical whether as dialogue or lyrics to songs

The current production could do with some liberal trimming, running well over two hours and weighed down by a couple of songs that are either not strong enough to warrant inclusion or simply get in the way of the narrative.



But the extra polish it received before it arrived at New World Stages is reflected in many of its irresistibly fun and neatly executed ensemble numbers featuring more than a dozen cast members, under the direction of Andy Fickman with choreography by Marguerite Derricks.



The most memorable of them include Fight For Me, a hilarious cafeteria brawl, the Heathers' girl-power anthem Candy Store, and Shine a Light, a spiritually uplifting school assembly scene.

Thankfully, there is not a trace in McCartan and Weed's performances of the actors who originated their roles on-screen a quarter-century ago, Christian Slater and Winona Ryder.



Rather than leaning on familiar impressions of the movie stars, they make the characters all their own, especially with the lovely duet Seventeen, one example of the show's uniquely alluring ability to blend romance, humor and horror.



The up-and-coming Weed (Lysistrata Jones, Bare) has all the tools to be a star in musical theater, a daring vocal style, impressive acting chops and a natural flair for comedy.



Katie Ladner delights as Veronica's nerdy friend, Martha Dunnstock (cruelly nicknamed Martha 'Dump Truck'), though her solo ballad Kindergarten Boyfriend seems misplaced near the end of a second act interrupted as it builds to an explosive finale.



Another odd choice is the ensemble number that opens the second act, Dead Gay Son, an ambitious gospel shout that lacks the strong lead vocals you would expect from such a song.



Despite minor issues with pacing, particularly after the intermission, Heathers pays off in the end with all-cast finale that should send audiences away happy, at least by high school standards.