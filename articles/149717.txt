McDonald's said that its profit slipped in the first quarter as global sales remained weak for the world's biggest hamburger chain.



The Oak Brook, Ill.-based company said global sales edged up 0.5 percent at established restaurants. In the flagship U.S. market, the figure fell 1.7 percent as customer traffic declined. The company cited "challenging industry dynamics and severe winter weather."



It said global sales for April are expected to be modestly positive. April would reflect the first full month that Taco Bell has offered its national breakfast menu, which it has pitched a challenge to McDonald's dominance in the morning hours.



The decline in sales and customer traffic in the U.S. reflects the struggles McDonald's is facing as eating habits change and competition intensifies. After a decade of growth, annual sales at established U.S. locations fell for the first time last year. The continued decline in the U.S. in the first quarter of 2014 is in stark contrast to Chipotle Mexican Grill Inc., which last week said sales at established locations rose 13.4 percent.



McDonald's CEO Don Thompson has noted in the past there seemed to be a split in the fast-food industry, with people who have more spending money heading off the chains that charge more. He said McDonald's will focus on underscoring value for its more cash-strapped customers, but the chain is also offering more premium offerings such as its new Bacon Clubhouse Burger.



In a statement Tuesday, Thompson said McDonald's is focusing "creating the best overall experience for our customers." To adapt to shifting trends, for instance, the chain has been rolling out new prep tables in its U.S. kitchens that can hold more sauces and toppings.



The idea is to eventually offer greater customization on its menu while keeping orders easy to assemble for workers. Speed and accuracy have been an issue for McDonald's as it stepped up the pace of new menu items in the past year.



In Europe, McDonald's said sales rose 1.4 percent at established locations in the latest quarter. The figure rose 0.8 percent in the unit that encompasses Asia, the Middle East and Africa, despite a decline in traffic.



For the quarter ended March 31, net income fell to $1.2 billion, or $1.21 per share. Analysts expected $1.24 per share.



A year ago, the company earned $1.27 billion, or $1.26 per share. McDonald's Corp. noted that the year-ago results were boosted by income tax benefits.



Revenue edged up to $6.7 billion, but was shy of the $6.71 billion Wall Street expected.



Shares of McDonald's edged up 83 cents to $100.50 in pre-market trading.



© Copyright 2021 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.