DETROIT -- Another day, another recall from General Motors.

At least that's the way it seems as the automaker reviews safety issues across its line-up of cars and trucks in the wake of a mishandled recall of millions of older small cars.

The number of recalls issued this year by the top North American carmaker rose Tuesday to 29 as GM announced four separate actions affecting 2.4 million cars and trucks.

Three of the recalls affecting a total of 181,500 vehicles sold in Canada -- including 105,700 to fix a cable that could wear over time and misalign the gear shifter on Chevrolet Malibu and Pontiac G6 mid-sized cars with four-speed transmissions. The Malibus were from the 2004-2008 model years and the G6s were from the 2005-2008 model years.

While no fatalities were involved in the latest recalls, the problems were serious enough that GM has temporarily halted sales of the vehicles.

GM has recalled 13.6 million vehicles in the U.S. since early February. That's more than the total number of cars it sold here in the last five years, and already surpasses GM's previous U.S. recall record of 10.75 million vehicles, set in 2004. By comparison, rival Ford has recalled 1.2 million vehicles in the U.S. this year, while Toyota has recalled 2.9 million, according to federal data and the companies.

The parade of bad news is part of the fallout from GM's recall of 2.6 million Chevrolet Cobalts and other small cars for defective ignition switches -- and a consequence of government regulation. GM says it's redoubling efforts to resolve outstanding safety issues. It's hiring 35 new safety investigators and is issuing recalls one by one, as soon as a decision is made. GM can't wait and announce a group of recalls once a month; it's required by federal law to report defects to the government within five days of discovering them.

A failure to follow that law landed GM in hot water with the U.S. government. The company agreed last week to pay a $35 million federal fine for concealing a deadly defect in the ignition switches for more than a decade. GM says at least 13 people have died in crashes linked to the problem, although trial lawyers suing the company say the death toll is at least 53. Congress and the U.S. Justice Department have ongoing investigations.

More recalls are likely. GM spokesman Alan Adler said the company is making progress on reviewing older investigations, "but work is continuing."

But Akshay Anand, an industry analyst with Kelley Blue Book, said Tuesday's order to dealers to stop selling the 2015 Cadillac Escalade and 2014 Buick Enclave, Chevrolet Traverse and GMC Acadia until they're repaired could give more buyers pause. The initial recalls covered older models like the discontinued Cobalt; now they're affecting newer models.

The recalls announced Tuesday include Buick Enclave, Chevrolet Traverse, GMC Acadia full-size crossovers from the 2009-2014 model years and Saturn Outlooks from 2009-2010 because front safety lap belt cables can fatigue and separate over time. In a crash, a separated cable could increase the risk of injury to front seat passengers. A total of 1.3 million vehicles are affected, including 75,700 in Canada.

Detroit-based GM said it will take a $400 million charge for repairs on all vehicles recalled so far this quarter. That includes the $200 million charge the company announced last week when it issued five recalls covering 2.7 million vehicles. That's on top of a $1.3 billion charge the automaker took for recalls in the first quarter.

The recalls announced Tuesday were: