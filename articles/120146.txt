GENEVA, Switzerland, April 15 (UPI) -- The World Health Organization reported Monday that the death toll from an Ebola outbreak in West Africa had reached 121.

The Ministry of Health of Guinea reported 108 deaths from the deadly virus, and Liberia's Ministry of Health and Social Welfare reported 13 deaths.

Advertisement

WHO cautioned that the number of people infected and fatalities from Ebola may change as investigations into the outbreak continues.

"Numbers of cases and contacts remain subject to change due to consolidation of case, contact and laboratory data, enhanced surveillance and contact tracing activities and the continuing laboratory investigations."

On Monday, the Ministry of Health of Mali reported six suspected Ebola cases, all of whom are under medical observation.

The Ebola virus is spread by close contact and kills up to 90 percent of its victims. The large geographical range of the reports of incidents, and the relatively porous borders between African nations, has helped spread the virus. There is no known cure or antivirus.

[World Health Organization]