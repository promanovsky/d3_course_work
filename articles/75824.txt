Washington:A new Michael Jackson album ' XSCAPE' is set to release on May 13 and will soon be available to fans on iTunes.

Epic Records have announced that the record will be comprised of eight previously-unreleased tracks from Jackson's, which have all been "contemporized" by Timbaland and other producers for a more modern sound,reportedly.

The album, which is executive produced by Epic CEO LA Reid, will be the latest compilation of unreleased material since 2010's 'Michael'.

Reid said in his statement that the King of Pop left behind some musical performances that they take great pride in presenting through the vision of music producers that he either worked directly with or expressed strong desire to work with.