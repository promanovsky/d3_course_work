The world of fashion has been known to make culturally insensitive missteps, and the latest offense comes from Zara.

The striped pajama top had a yellow star on it, and it was sold in the children's section. The shirt was sold on Zara's online Albanian, French, Israeli and Swedish stores, according to The Huffington Post.

Zara apologized in multiple languages, stating that the inspiration came from Old West cowboy sheriffs and not from concentration camps.

"Zara has issued a heartfelt apology on its social network profiles," Inditex, which owns Zara, said. "The items will be reliably destroyed."

The link to the shirt now shows a striped shirt with the word "bien" or well on them.

On Facebook, where Zara didn't address the controversy, a user asked if the brand would be coming out with a perfume made out of a cyanide-based gas that was used to kill Jewish people.

"After your concentration camp tee shirt I was wondering when is you ZYKLON B perfume coming out? for those wanting to smell like the gas chambers?" the user said.

In 2007, Zara was also accused of anti-Semitism. An English woman found a handbag that had symbols that looked like swastikas. Swastikas are commonly used in Hinduism, and Inditex said Zara hadn't approved of the final design.

"After the return of one bag we decided to withdraw the whole range," a spokesman for Inditex said at the time.

The pajama top's star has the words sheriff embroidered on to it.

Lauren Frayer explains that the pajamas stripes "are horizontal, unlike the ones on prison garb worn by Holocaust victims, which were usually vertical."

Hana Levi Julian explained in the Jewish Press that the words 'Sheriff' disappear into the star and that those in Israel wouldn't understand the concept, as they don't have sheriffs.

Instead they would see one thing.

"To survivors of the Holocaust and their relatives and friends in Israel, that message is crystal clear," Julian said. "It is especially loud after recent fiery rhetoric from [Erdogan,] who has made his dislike of Jews and Israelis very plain over the years."