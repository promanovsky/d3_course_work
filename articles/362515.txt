Kim Willis

USA TODAY

Virus, schmirus!

Seven weeks after being sidelined by a virus that landed him in the hospital in Tokyo, Paul McCartney resumed his Out There tour Saturday night in Albany.

McCartney, 72, wore black jeans and a white dress shirt, topped with a bright blue military-style jacket, for his sold-out show at the Times Union Center. Opening the almost-three-hour concert with Eight Days a Week, he played a mix of Beatles songs (Eleanor Rigby), Wings hits (Listen to What the Man Said) and solo material (John Lennon tribute Here Today). He also brought one lucky couple on stage to get engaged.

The former Beatle's rescheduled tour continues Monday in Pittsburgh and Wednesday in Chicago.