Justin Bieber has unveiled two videos of himself rehearsing a dance routine with Selena Gomez.

The duo - who are believed to have recently reunited as a couple - are seen practicing a passionate routine to John Legend's ballad 'Ordinary People'.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Their routine is said to have taken place at Action Dance Studio in McAllen, Texas over the weekend, according to TMZ.

Bieber originally posted the two clips on Instagram, but deleted them shortly afterwards.

The videos emerged after Bieber dedicated a song to "my baby" while performing at the South By Southwest Festival on Sunday (March 9).

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

The owner of the dance studio claimed that the couple appeared "really close and into each other".

Bieber and Gomez, who have been dating since 2010, have been on-again-off-again in recent months.

The 20-year-old singer stormed out of a courtroom earlier this month over questions about his relationship with Gomez during a deposition taping session.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io