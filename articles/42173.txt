Edward C. Baig

USA TODAY

Premium phone has all-metal unibody design

Clever camera stunts include ability to focus some scenes after you shoot

HTC didn%27t match rivals feature for feature

NEW YORK — HTC wants you to read this review of its new HTC One flagship phone that launches today. The Taiwanese handset maker, a perennial underdog to Samsung and Apple, hopes you'll read other reviews too. In fact, the company's entire marketing campaign — ads feature actor Gary Oldman — is designed to draw potential buyers to read all about the device on the Internet. In one direct poke at Samsung, an HTC billboard ad says, "We don't call it the next big thing. The Internet will."

Short on marketing dollars compared with major rivals, HTC is willing to take the chance that this latest smartphone will generate knockout reviews and jazz up the fan base; indeed, HTC produces products critics typically like a lot but for whatever reason haven't sold real well.

Here we go again. I can't speak for other reviewers. But it's hard not to walk away impressed with the look and feel of this newest entry, which officially goes by the name HTC One (M8). This is a beautiful phone with a striking curved all-metal unibody design. The brushed metal covers 90% of the phone's housing and is a bit more prominent than its immediate predecessor. It represents a stark contrast to the plastic-y body of what will surely be HTC's chief Android competitor, Samsung's upcoming Galaxy S5. HTC One will be sold in three colors: gun-metal gray, amber gold and glacial silver. In the U.S. the phone will be available (starting today) from AT&T, Sprint and Verizon Wireless. The T-Mobile version won't arrive until April. Prices range from $199 to $249 under two-year plans.

Not every picture I took was perfect by a long shot, but the well-rounded device boasts a strong camera system. On the rear is what HTC refers to as a Duo Camera, so named because of an extra camera sensor that captures imaging and depth data and complements the 4-megapixel "ultra-pixel" camera. As with last year's model, HTC goes with larger but fewer pixels than its competitors.

Meanwhile, HTC's 5-megapixel, wide-angle front camera is about the best you can get for a selfie, and it captures 1080p videos too. HTC has simplified the camera software, and includes a bunch of cool if not entirely novel photo tricks and filters. For example, you can simultaneously shoot pictures, and videos, with the front and rear cameras — great to capture a parental reaction shot in the same image as the kids' activities mom and dad are going gaga over.

Another feature called UFocus adds a Lytro camera-like effect: You can "refocus" and sharpen a portion of the image that you've just shot while artistically blurring another part of the scene. Still another feature adds a 3-D effect to certain images, visible as you tilt the device. If you're shooting video, you can pause in the middle of a sequence and then resume the video, keeping scenes as part of one video.

HTC plans to add another feature in which you can cut out a portion of a picture and paste it into another application, but this stunt won't be ready at launch. Certain other features are holdovers from last years' model, including Zoe, which combines stills with a few seconds of videos. HTC says it will also let you share Zoe highlight videos in the cloud, but the upgraded Zoe experiences won't be available until summer.

HTC spent time refining the software with enhanced gestures. For instance, you can double-tap the screen to wake the device. Or lift it to your ear to answer a call. Small things. Seen them before. But nice.

The BlinkFeed magazine-style layout of social and news feeds that debuted a year ago is a swipe away, with smoother scrolling and improved customization. BlinkFeed may remind you of Flipboard, with feeds that appear as pictures in squares and rectangles of different sizes. Tap on a square or rectangle to read the underlying content. HTC opened up BlinkFeed to developers; Foursquare and Fitbit are among the first to take advantage.

As before, the HTC One boasts superb-sounding front stereo speakers, the finest I've heard on a smartphone.

The latest hardware is a little taller and heavier than last year's model (5.6 ounces versus around 5-ounces). It has a more robust quad-core Qualcomm processor and bigger battery. The Full HD screen is bigger too, 5-inches versus 4.7-inches.

Last year you could get an HTC One with up to 64GB of storage; this time around 32GB is the max. HTC's rationale is that you can bolster storage by up to 128GB via a microSD expansion slot that wasn't on the last version. Moreover, you get 50 GB of free cloud storage through Google Drive, with new users getting an extra 15GB.

HTC certainly didn't match rivals feature for feature. I wish, for example, HTC provided a fingerprint scanner similar to the one Apple included with the iPhone 5S and that Samsung will have on the Galaxy S5. Didn't happen.

The new phone isn't water resistant like the upcoming Galaxy S5, or some Sony Xperia phones I've seen. The battery still can't be removed (you can criticize the iPhone for the same thing). Speaking of the battery, HTC says it will have an extreme battery saving mode that's coming soon but that also wasn't available at launch.

I'm obliged to point out that the screen went dead on the first test unit that HTC supplied, a model from AT&T, possibly an isolated snag with a pre-launch device. I didn't encounter the issue with the Verizon Wireless test version HTC provided as a substitute.

Though most would-be buyers will gravitate to the lovely metal design, others will cover up the handset with a case. Along those lines, HTC is selling a cool $50 case called Dot View that lights up with a simple dotted display to reveal who is calling, the date, time and weather. You can swipe the case to answer.

By making the phone available immediately, HTC hopes to capitalize on any buzz generated from today's launch—and to jump the gun on Samsung, whose rival Galaxy S5 is still a few weeks away. I can't predict whether HTC will be any more successful selling the latest model—the space remains ultra-competitive. But HTC One is a mostly terrific phone that a lot of people are really going to like. I suspect my friends on the Internet will agree.

E-mail: ebaig@usatoday.com; Follow @edbaig on Twitter.

THE BOTTOM LINE

HTC One (M8)

www.htc.com

$199 to $249 under two year plans with major carriers

Pro. Beautiful all-metal design. Strong camera. Excellent speakers. Expandable storage. BlinkFeed. Bigger battery.

Con. No fingerprint scanner. Not waterproof. Some promised features are not ready at launch.