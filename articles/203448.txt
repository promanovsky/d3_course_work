— Even before Alibaba went online, its founder talked about making the fledgling e-commerce company a global player.

At Alibaba Group’s first staff meeting in 1999, a video shot by an employee shows Jack Ma rallying a workforce of 17 of his friends. They met in a cement-floored apartment in Hangzhou, a city southwest of Shanghai, at a time when few Chinese were online. Ma was an English teacher with no training in business or computers.

“Our competitors are not in China but in Silicon Valley,” says Ma in the video, which is included in a documentary about the company, “Crocodile in the Yangtze,” made by a former Alibaba vice president, Porter Erisman. “We can beat government agencies and big, famous companies because of our innovative spirit.”

Such Silicon Valley-style bluster was new to China. Over the next 15 years, he helped propel Alibaba through technical and financial challenges and a battle with eBay Inc. to become the world’s biggest online bazaar. The company is now planning to list in the U.S. and analysts say its initial public offering this year may raise up to $20 billion.