Apple's newly announced operating system for iPhones and iPads, iOS 8, remedies a lot of things people have been complaining their iPhone can't do.

The problem is, a lot of successful apps out there have gained notoriety because of just that: they saw a hole that needed to be filled and people responded.

Apple has not-so-subtly incorporated many of these features into iOS 8, taking aim at these innovative apps and cherry-picking their most innovative features.

Here are the apps rendered obsolete by iOS 8.

Messaging apps such as Snapchat and WhatsApp: Apple's updated iMessage now allows users to send bite-sized multimedia messages and the option to display their location, longtime features of WhatsApp and Snapchat. WhatsApp CEO Jan Koum tweeted that it was "very flattering to see Apple 'borrow' numerous WhatsApp features into iMessage."

WWDC

Dropbox and Google Drive: iCloud Drive means all your work is stored in the cloud and pushed instantly to any of your Apple or Mac devices, no app needed. Just like Dropbox, iCloud Drive users can monitor which documents or photos are being synced, and can even edit them online using Apple's iCloud productivity suite. iCloud Drive is free for 5GB of storage, with additional storage available starting at 20GB for $0.99 per month.

Apple





Skype: There are many applications which allow users to make phone calls over WiFi, but Skype is one of the most popular out there. The only downside is that you have to pay. Apple has revealed iOS 8 will allow you to make phone calls over WiFi to any device, which will be great if you have poor cell reception in your home or office.

Business Insider

Skitch: Ever need to point out a specific detail with an arrow or circle? Skitch has been the go-to app for annotating images, but Apple has added the same functionality to its Mail app, getting rid of the need to switch between an editing app and your inbox. Terrible at making arrows or incapable of drawing a non-sloppy circle? Apple has included a feature to recognize the basic shape attempted and seamlessly replace it with a more geometrically correct version.

WWDC

1Password: People have been itching for Apple to allow third-party apps to utilize their state-of-the-art fingerprint sensor, Touch ID, which acts as a unique and secure password to gain access to your iPhone. Until now, if you wanted to manage all your passwords, a manager like 1Password or LastPass were your best bets. Now, you won't even need to remember your passwords. All you'll need is your fingerprint.