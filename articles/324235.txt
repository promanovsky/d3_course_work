Google’s huge developer event kicked off on Wednesday with a painfully long keynote presentation that was decidedly lackluster. Among the highlights, however, was “Android L.” Google gave the world its first look at the upcoming new Android build on Wednesday, and it was awesome. The new look of the interface, defined by Google’s new “material design,” is stunning. More importantly, Android L will bring with it some significant performance enhancements that will dramatically improve the Android experience.

Of course, you don’t have to wait for Android L to enjoy those performance enhancements.

There are plenty of optimizations that will contribute to the performance improvements that come alongside Android L, but the biggest change is Google’s move from the Dalvik runtime to the newer ART runtime. Google had already included support for the ART runtime in earlier Android builds, however, but Dalvik was left enabled by default.

If you want to take advantage of the big performance improvements coming in Android L, you can do so right now — and it only takes a few seconds.

Most Android devices running Android 4.4 KitKat and later versions of Android allow the user to enable ART, including popular devices such as the Galaxy S5, HTC One (M8) and Nexus phones. To enable it, you’ll first have to gain access to the hidden “Developer options” menu.

Open the Settings app, scroll to the bottom and tap “About phone.” Then scroll all the way down to “Build number” and tap it seven times. Now tap the back button and you’ll see a new Developer options item in the Settings menu.

Simply open the Developer options menu, tap “Select runtime” and choose “Use ART” instead of “Use Dalvik.”

Voila!

Now you’ll notice big performance improvements in any apps that are compatible with the newer ART runtime. There are a few out there already but you can expect plenty more in the coming weeks and months as developers update their apps for Android L.

Want an even more impressive and immediate speed boost that works on any Android phone? Check out our guide on how to give your Android phone a huge speed boost, just like Apple sped up iOS 7.1!