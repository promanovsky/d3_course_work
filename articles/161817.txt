If you’re comfortable with the Supreme Court resolving disputes over technology, the transcript of Tuesday’s oral arguments in ABC vs. Aereo should change your mind.

Admittedly, the case is about copyrights, not circuitry. In particular, the issue focuses on whether Aereo’s service violates broadcasters’ exclusive rights to transmit works to the public. Yet the inner workings of Aereo’s system are crucial to that issue, at least from Aereo’s point of view. And the justices struggled to get past a simplistic view of the technology involved.

For example, at one point Justice Stephen G. Breyer said that unlike a rooftop TV antenna, the tiny antennas that Aereo sets up in a city could “pick up every television signal in the world and send it ... into a person’s computer.” That’s physically impossible, not just because antennas aren’t sensitive enough to detect signals from outside the local market but because the world isn’t, you know, flat. “And that sounds so much like what a [cable] TV system does or what a satellite system does,” Breyer continued, “that it looks as if somehow you are escaping a constraint that’s imposed upon them. That’s what disturbs everyone [on the court].”

Everyone outside the court should be disturbed by a question like that.

Advertisement

At stake here is the degree to which innovative companies are going to have to seek permission from copyright owners to enable people to do in the cloud what they can do for themselves at home. Aereo uses its tiny antennas and circuitry to let people tune in, record and stream local TV programs over the Internet. Because each antenna, recording and stream is initiated and controlled by individual users, Aereo says the online transmissions aren’t public performances, they’re private ones. The networks counter that if Aereo can do that without obtaining licenses, other pay-TV operators will follow suit, destroying an important revenue stream and pressuring them to abandon free over-the-air broadcasting.

Justice Sonia Sotomayor opened the session by asking the broadcasters’ attorney, veteran Supreme Court litigator Paul D. Clement, why Aereo isn’t simply a cable TV company. After all, she said, it has facilities that receive transmissions from broadcasters, then send the programs on to subscribers for a fee.

Clement’s response was that although Aereo is indistinguishable from cable operators in some respects, it isn’t one largely because the company doesn’t want to be considered one. It fell to Aereo’s attorney, David C. Frederick, to point out that Aereo doesn’t collect TV programming and retransmit it in bulk to its subscribers, as cable operators do. Instead, it rents equipment to its subscribers, who use it to watch or record one show at a time. Nothing happens unless it’s initiated by the user, he said.

To which Sotomayor responded: “I always thought ... that if I [make a copy] of a record and duplicate it a million times the way you’re doing it, and I then go out and sell each of those copies to the public, then I am violating the [Copyright] Act. So why is it that you are not?”

Advertisement

As the question indicates, Sotomayor didn’t quite catch Frederick’s point about who was actually making the recordings (Aereo’s subscribers) and where they were being transmitted (to themselves). Or maybe she just rejected it as sophistry. Regardless, Frederick said legality of the recordings wasn’t at issue because, as the broadcasters recognized earlier in the case, the Supreme Court held in the Sony Betamax case that people have a fair-use right to record TV shows for later viewing.

The right question was the one Justice Elena Kagan eventually put to Clement: Why should copyright law treat a company that rents people a TV antenna hooked to an Internet-connected DVR different than one that sells people the functional equivalent to install at home? The answer, Clement said, was that a private performance becomes a public one when it’s transmitted by a service provider from one place to another, rather than by a viewer to himself.

Clement’s formula ignores the U.S. 2nd Circuit Court of Appeals’ ruling in Cartoon Network vs. Cablevision, in which the appeals court ruled that a cable operator’s shared DVR didn’t violate the networks’ copyrights. Under the 2nd Circuit’s ruling, who owns the equipment and where it’s located are irrelevant to determining whether a performance is public or private. Instead, the court held, what matters is whether the equipment is controlled by users, and whether their recordings and transmissions are theirs alone. If the answer to those questions is yes, then it’s a private performance.

Aereo designed its service to comply with the 2nd Circuit’s ruling, giving users total control over the equipment and making sure none of the recordings or streams was shared. Several justices focused on that aspect, pressing Frederick to say whether the company had any technological reason to use 10,000 dime-sized antennas. Frederick said it was cheaper to set up tiny remote antennas than to install rooftop ones on Manhattan skyscrapers, and to take a modular approach that could minimize the start-up’s costs. That response, however, didn’t seem to mollify Chief Justice John G. Roberts Jr. and Justice Antonin Scalia, who suggested that Aereo was just trying to “get around copyright laws.”

Advertisement

One aspect of the case that seemed to help Aereo was the concern expressed by multiple justices about jeopardizing other cloud-based services, such as online storage lockers. Clement tried to reassure them, saying the difference between Aereo and a cloud-based storage service is like the difference between a car dealership and valet parking. One sells you a car, the other simply parks and returns the car you already own.

That’s a powerful metaphor, yet it’s not quite apt. Aereo isn’t providing the TV programs, the broadcasters are. Its subscribers then use the equipment they rent from Aereo to tune in the program, make a copy and transmit it to themselves. That latter transmission is a private performance, not a public one, and it’s separate legally from the public performance the broadcasters make when they put programs on air.

Frederick said that if Clement was right about Aereo being a content provider, it would mean that any company providing an antenna or a DVR was a content provider. “And if that’s true,” he argued, “then the implications for the equipment industry are obviously quite massive, and you can understand why that would frighten the cloud computing industry because that turns them into public performers whenever they are handling content.”

A second problem for some justices was Clement’s insistence that Aereo, unlike cable operators, wouldn’t qualify for a compulsory license to retransmit TV programs if the court ruled in the broadcasters’ favor. Under a compulsory license, Aereo would have to pay broadcasters for the content but wouldn’t face the daunting task of negotiating for a license from each copyright owner.

Advertisement

Clement offered cold comfort on that point. “If they actually provide something that is a net benefit technologically, there’s no reason people won’t license them content,” he told the justices. “But on the other hand, if all they have is a gimmick, then they probably will go out of business, and nobody should cry a tear over that.”

ALSO:

Obama’s Keystone pipeline trapWhat we don’t know about the killing of Anwar Awlaki

Free trade on steroids: The threat of the Trans-Pacific Partnership

Advertisement

Follow Jon Healey on Twitter @jcahealey and Google+