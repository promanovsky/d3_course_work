Mekhi Phifer has reportedly filed for bankruptcy.

The former ER star has amassed a tax debt of around Â£1.2 million (Â£707,380), according to TMZ.



He is said to have filed for bankruptcy at the end of last month, claiming approximately $67,000 (Â£39,500) in assets.

Legal documents state that Phifer's outgoings are approximately $11,600 (Â£6,838) per month, while he only earns $7,500 (Â£4,421).

Phifer is best known for playing Dr Greg Pratt in ER between 2002 and 2008. He has since starred in Torchwood, Psych, White Collar and House of Lies.

Meanwhile, he also starred as Max in Divergent earlier this year, and will reprise the role in next year's Insurgent.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io