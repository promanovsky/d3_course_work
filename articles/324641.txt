UPDATE: An NYPD spokesperson tells Us Weekly that Shia LeBeouf "has been charged with disorderly conduct. He was also charged with criminal trespass. While at Studio 54 he was upsetting patrons, being loud and yelling. He'll be jailed. There will be a court appearance for this matter."

"Shia seemed out of sorts," a source tells Us. "He was interacting with strangers during the show and slapping peoples' backsides. He was trying to avoid the police and fell. Then he was cursing at the police while being taken into custody."

———

Shia LaBeouf was handcuffed and escorted out of a production of Cabaret in New York City on June 26, a source confirms to Us Weekly.

The 28-year-old was reportedly being "disruptive" during the Broadway production which stars Michelle Williams and Alan Cumming. A show source tells Us: "He was disruptive during act one and escorted out of Studio 54 at intermission."

As to what exactly the actor was doing, another source tells Us LeBeouf was "harassing audience members, harassing actors on the stage, and chain smoking inside the theater."

A NYPD PIO officer confirms to Us: "He was removed from Studio 54 and taken to a local precinct. No charges have been filed as of yet."

According to Broadway World, bystanders on the street where LeBeouf exited say the Transformers actor was "in tears when taken away by police."

A rep for LeBeouf could not be reached.

Late Thursday night, Lena Dunham sounded off on the incident on Twitter, writing, "I feel you Shia: In 1993 I was carried out of Les Mis screaming because I couldn't handle the intimations of death." She added in a second tweet: "'You know you're a f–kin' G when you're arrested during Cabaret.'–Gillian Jacobs."