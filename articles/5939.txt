“Titanfall” is currently available only for the Xbox One and PC, but don’t worry, Xbox 360 fans, Bluepoint Games is working hard on the next version of the first-person shooter.

Last month, Microsoft announced “Titanfall” for the Xbox 360 wouldn’t launch on the same release date as the Xbox One and PC. While some critics believed it was a marketing tactic by Microsoft to move more Xbox Ones off the shelf, Microsoft assured eager players it was the developer's decision.

"I think we struck the right balance — it was Respawn, really, and the developer who struck the balance — which is, it will play great on Xbox 360," Microsoft Chief Marketing and Strategy Officer Yusuf Mehdi told Polygon in an interview at SXSW in Austin, Texas, Tuesday. "Believe me, we are thrilled about it. Xbox 360 is in, obviously, all the markets including the top 13 with Xbox One; we're in 48-plus markets for [Xbox 360]. For many markets, that will be the game. So, we've worked together to have that be a first-class experience.”

Mehdi added that “at the same time, on Xbox One you can get some of those extra features that set it apart, and there will be people who want to do that, and there will be enough encouragement to go — but it will be a great experience on Xbox 360.”

On Feb. 6, EA announced that the Xbox 360 version of “Titanfall” would be delayed until March 25. EA claimed the delay was needed so the developer, Austin-based Bluepoint Games, could have more time to “put the finishing touches on the game.”

At the same time, Microsoft (NASDAQ:MSFT) is hoping the much-anticipated game will be a system seller, allowing the company to catch up with competitor Sony’s PlayStation 4. "We're making a big bet that 'Titanfall' is going to be a blockbuster launch for Xbox," Mehdi told Reuters. "We don't have a sales forecast for the game to share, but we expect it to be big for us.”