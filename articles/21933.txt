In a rare public rebuke, Apple CEO Tim Cook has made a statement that slams the new book Haunted Empire by former Wall Street Journal reporter Yukari Iwatani Kane.

The statement was obtained by CNBC's Becky Quick, who asked Cook for a reaction to the new book, which characterizes him as tough, unapproachable and "relentlessly frugal."

See also: Apple CEO Tim Cook Addresses NSA Concerns in Rare Interview

"This nonsense belongs with some of the other books I've read about Apple," said Cook. "It fails to capture Apple, Steve, or anyone else in the company Apple has over 85,000 employees that come to work each day to do their best work, to create the world's best products to put their mark in the universe and leave it better than they found it. This has been the heart of Apple from day one and will remain at the heart for decades to come. I am very confident about out future."

Kane's book has been viewed in recent weeks as a hitherto unavailable glimpse into the workings of Apple under the leadership of Cook, laying out numerous anecdotes that attempt to depict the current tenor at the company.

One of the most interesting new passages highlighted from the book claims that in the year before his death Jobs dismissed the notion of an Apple Smart TV, saying, "TV is a terrible business. They don't turn over and the margins suck."

Following Cook's comments, the author hit back with her own comments in a report from Recode:

For Tim Cook to have such strong feelings about the book, it must have touched a nerve. Even I was surprised by my conclusions, so I understand the sentiment. I’m happy to speak with him or anyone at Apple in public or private. My hope in writing this book was to be thought-provoking and to start a conversation which I’m glad it has.

When asked by a fellow CNBC host whether Cook had specific gripes about the book, Quick said that she wasn't able to obtain additional details beyond his statement.

Nevertheless, whether Cook has actually read the book or not, it's clear from his statement that he believes it paints a negative picture of Apple. And, unlike his predecessor, it appears that Cook plans to be a lot more vocal when it comes to the coverage of Apple.

"We've always had many doubters in our history," said Cook in his statement to CNBC. "They only make us stronger."