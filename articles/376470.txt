A MIT-developed implant that allows women to control the level of birth control hormones emitted via remote control could be available as soon as 2018.

A microchip developed by MIT professor Robert S. Langer and recently leased to tech start-up MicroCHIPS has the capability to release controlled amounts of chemicals. This technology has been implemented in a device for women measuring just 20 x 20 x 7 mm.

Within the chip are tiny reservoirs of the hormone levonorgestrel, a chemical already used in some birth control products. The implant, meant to be placed in the buttocks, upper arm or abdomen, dispenses daily small doses of the hormone for up to 16 years. This lifespan is considerably longer than other contraceptive implants, which last a current maximum of five years.

If a woman wishes to stop receiving the birth control, she can choose to turn the device off or back on with a remote, instead of having to remove the implant altogether.

"The ability to turn the device on and off provides a certain convenience factor for those who are planning their family," Robert Farra, MicroCHIPS president, said.

The levonorgestrel is temporarily melted by passing an electric current from an internal battery through a platinum seal containing the hormone, allowing it to be absorbed into the body.

"The idea of using a thin membrane like an electric fuse was the most challenging and the most creative problem we had to solve," Farra said.

Skeptics of the microchip worry that there is potential for "hacking" of the microchip's release signals, but Farra assures that the remote communication "has to occur at skin contact level distance" so there is little chance that someone could reprogram the implant.

With additional backing from Bill Gates, MicroCHIPS plans on submitting the implant for preclinical testing in the U.S. next year, and if approved, believes the device could go on sale as soon as 2018.