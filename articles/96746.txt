Stocks resumed their April swoon Thursday, snapping two days of moderate gains with a broad-based sell-off that undercut the Nasdaq Composite Index by more than 3%. Blue-chip equities as measured by the Dow Jones Industrial Average fared slightly better, falling about 1.6% as a group, but the broader S&P 500 was down over 2% by the close.

Shares were under pressure throughout the session, turning lower soon after the opening bell following another big miss for a Chinese trade number reported overnight, casting new doubts on the pace of global growth.

China's export and import activity unexpectedly fell in March, with exports declining 6.6%, China's General Administration of Customs said, well under the 4% rise expected in the market consensus. The fall was even larger for imports, retreating 11.3% instead of the 2.4% gain experts had anticipated.

All 10 sectors in the S&P 500 finished in the red, led by declines by shares of healthcare, consumer discretionary and financial companies. Utility stock had the best day, declining less than 0.4%.

Crude oil for May delivery settled 20 cents lower at $103.40 per barrel while May natural gas was up 7 cents to finish at $4.66 per 1 million BTU. June gold rose $15.10 to $1320.80 per ounce while May silver rose $0.33 to $20.10 per ounce. May copper added a penny to settle at $3.05 per pound.

Here's where the U.S. markets stood at the end of the session:

Dow Jones Industrial Average down 181.18 (-1.72%) to 10,373.76

S&P 500 down 39.10 (-2.09%) to 1,833.08

Nasdaq Composite Index down 129.79 (-3.10%) to 4,054.11

GLOBAL SENTIMENT

Hang Seng Index up 1.51%

Shanghai China Composite Index up 1.38%

FTSE 100 Index up 0.10%

UPSIDE MOVERS

(+) RT, Non-GAAP Q3 net loss of $0.07 per share beats consensus by $0.01. Revenue falls 3.8% year over year to $295.6 mln, topping Street view by $10.8 mln. Expects same-store sales during Q4 to be up 1% to down 1% from year-ago levels.

(+) IGTE, Reports a 10% jump in revenue over year-ago levels to $302.2 mln, beats Street view by $1.2 mln. Adjusted profit of $0.45 tops analyst expectations by $0.04 per share.

(+) CLDN, Receives breakthrough therapy designation from FDA for its Mydicar drug candidate to treat potential heart failure.

DOWNSIDE MOVER

(-) AGIO, Discloses plans to raise up to $86.3 million in a stock offering, including an overallotment option for underwriters. Proceeds will be used to fund trials of the company's blood-cancer treatments.

(-) IMPV, Reduces Q1 revenue guidance by $5 mln to a new range of $31.0 mln to $31.5 mln, trailing estimates by at least $5.1 mln. Boosts projected non-GAAP net loss by $0.07 to $0.40 to $0.44 per share, or at least $0.06 wider than estimates.

(-) BBBY, Q1 EPS outlook misses by at least $0.06; sees EPS growth in the mid-single digits compared with consensus expecting 10% growth. Matches analyst expectations with Q4 earnings of $1.60 per share on $3.2 bln in revenue.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.