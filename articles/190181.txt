Willow Smith, the 13-year-old daughter of actors Will Smith and Jada Pinkett Smith, has been photographed lying on a bed with a 20-year-old actor.

The picture, taken with and posted by “Hannah Montana” alum Moises Arias, has been removed from the actor’s Instagram account and Tumblr but still has some people questioning what’s going on with the pair.

The black-and-white photo features the “Whip My Hair” songstress lying fully clothed on top of the bed covers and resting her head near Moises’ lap. He’s shirtless, by the way, and is sitting up on the bed, leaning against a wall behind it. Another similarly composed shot, obtained by the Daily Mail, features Arias laughing.

The context of the photo is still a mystery, though it seems to echo another image posted on his Tumblr and captioned “A Buddha Awakening.” Willow is not in that picture.

Advertisement

The image of the two of them has been obtained by several news outlets. Some critics have reposted it on social media, calling it perverted, TMZ said, because Willow is a minor.

But Willow’s parents don’t seem to have a problem with the photo.

Sources told TMZ that the actors believe their daughter is “very mature” and capable of making her own decisions, as long as they don’t clearly cross the line. They reportedly see these pictures as innocent. Arias has known the family for years, the site said.

Such a reaction isn’t that surprising, given that the couple practices a very open style of parenting.

“We don’t do punishment,” Will Smith said in a 2013 interview with Metro. “The way that we deal with our kids is, they are responsible for their lives. Our concept is, as young as possible, give them as much control over their lives as possible and the concept of punishment, our experience has been -- it has a little too much of a negative quality. So when they do things -- and you know, Jaden, he’s done things -- you can do anything you want as long as you can explain to me why that was the right thing to do for your life.”

Advertisement

Arias, who is good friends with Willow’s older brother Jaden and with Kendall and Kylie Jenner, is a photographer and has an Twitter feed filled with prophetic and cryptic messages. His Tumblr account features photos of his celebrity friends.

It’s not the first time Willow has raised eyebrows for acting older than her age. She has been criticized for age-inappropriate outfits and producing music that is believed to be too mature, the Mail reported.

She was dropped from the big-screen adaptation of “Annie” last year for aging out of the role. The musical, produced by Will and Jada Smith along with hip-hop mogul Jay Z, eventually cast “Beasts of the Southern Wild” star Quvenzhane Wallis.

Reps for Willow Smith and Arias did not immediately respond to Los Angeles Times requests for comment.