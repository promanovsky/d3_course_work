Google

A new feature on Google Street View lets you turn back time.

This digital time capsule feature, announced Wednesday, is only available on the desktop version of Google Maps at the moment and lets you browse through Google's huge Street View photo collection from as far back as 2007.

You can watch the Freedom Tower in New York or famous landmarks in other parts of the world, such as Singapore's Marina Bay Sands being constructed from ground up.

Besides landmarks, Google also says the feature can be used as a "digital timeline of recent history" to view reconstruction and cleanup efforts happening in Japan since the disastrous 2011 tsunami. On a more light-hearted note, you can also use it to experience the different seasons in other countries.