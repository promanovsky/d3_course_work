In recent weeks, out of the blue, it suddenly became hard for some American foodies to smile and say cheese.

The US government's Food and Drug Administration had questioned the hygiene standards of the centuries-old technique of aging artisanal cheese on wooden shelves.

The fear was that wood, which is porous, can harbor bacteria. At stake are some all-time favorites: Parmesan, cheddar and Gruyere.

The row has wafted into the corridors of power on Capitol Hill.

A petition surfaced Sunday on a section of the White House web page that is open to the public, calling on the administration of President Barack Obama to "lift the FDA restrictions on ripening cheese on wooden boards." So far, it has more than 5,700 signatures.

Then on Wednesday, a dozen lawmakers urged cheese-loving colleagues to keep the FDA from "going after the centuries-old aging process."

While the FDA seems to have since backed down, the row nevertheless sent a shockwave through the industry.

'Threat' to tradition

It all started a few months ago in New York state, according to Nora Weiser, director of the American Cheese Society.

The FDA, citing hygiene reasons, cited several cheese-makers for using wooden boards to age their products.

Clarifications were sought. After all, the cheese-makers argued, the method has been around for, well, ages.

Monica Metz, the branch chief for the FDA's Center for Food Safety and Applied Nutrition's Dairy and Egg Branch, cited an old rule that the surfaces on which cheeses are aged must be able to be properly cleaned.

Weiser said the clarification that came from the FDA made things even muddier. The old rules made no mention of wood, she said, adding that to ban this type of aging would be "a threat to traditional cheese-making methods".

She said a ban would affect not only US-made cheeses but also imported ones such as Parmesan and cheddar.

She said that every year, the Midwestern state of Wisconsin, America's cheese capital, alone produces 15,000 tons of cheese aged on wooden shelves.

'Save our cheese'

A "SaveOurCheese" campaign has been launched in recent days on Twitter and Tumblr. And the number of signatures on the We The People petition continued to grow.

"People have been aging cheese on wood since before the FDA even existed," said Kristin Hunt, a food writer on the web site Thrillist.

"This is devastating news for anyone who eats non-Kraft cheese, as many artisan varieties are currently aged on wooden boards," she said, referring to a common, inexpensive brand of American cheese.

"Wood has not only a very strong safety record as an aging material but also a very long history of adding flavor and quality," added Weiser.

The American Cheese Society asked the FDA to reconsider its interpretation of the circular.

Trade policy analyst William Watson of the Cato Institute said the FDA's move "seems to be part of a bizarre crusade to ban flavorful cheese."

Amid all the protests, the FDA eventually issued a statement that was seen by foodies as a climbdown.

"The FDA does not have a new policy banning the use of wooden shelves in cheese-making," it said.

Then late Wednesday, it issued another statement saying it had not banned nor planned to ban the use of wooden shelves to age cheese.

The FDA said it had historically expressed concern over whether wooden shelves where cheese is aged could be cleaned properly.

The earlier FDA comment "was provided as background information on the use of wooden shelving", the statement said, acknowledging that "the language used in this communication may have appeared more definitive than it should have."

On Thursday, Weiser rejoiced.

"We are delighted that cheese-makers may continue crafting the cheeses we love using this traditional cheese-making practice," she told AFP.

"We are encouraged that further discussions between the artisan cheese community and FDA will preserve wood aging and other traditional cheesemaking methods for all styles and types of cheeses."