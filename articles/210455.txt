After watching CCTV footage of Solange Knowles punching, kicking and lashing out at her brother-in-law Shawn 'Jay Z' Carter in a lift after the Met Gala, the question on everybody's lips is: What did Jay Z do to provoke such an attack?

Fans and conspiracy theorists have flooded online to offer their two cents-worth in an attempt to shed some much-needed light on the Knowles-Carter fight.

And while Beyoncé and Jay Z displayed a united front at a basketball game after the footage surfaced, memes and possible explanations were being churned out on social media.

Here are just a few of the explanations making the rounds online:

1) Solange was defending her sister after Beyonce had a row with Jay Z.

According to some fans, Solange may have felt compelled to intervene after Beyoncé and Jay Z had a maritial squabble. She is famously close to her older sibling so the rapper may have said or done something that rubbed her up the wrong way and the attack was her attempt to defend her sister.

2) Jay Z confronted Solange about her alleged erratic behaviour.

Solange has been accused recently of erratic behaviour and Jay Z challenged her about it. The last time she was accused of odd behaviour was in 2009 when she confessed to using marijuana. "I don't even smoke weed that often but I'm finding that I can't remember things," she tweeted.

3) Solange confronted Jay Z about rumours of infidelity.

Solange accused her brother-in-law of being unfaithful and an angry Jay Z hit back with some hurtful words.

4) Solange is trying to join the secret Illuminati and the lift attack was part of her initiation.

For years conspiracy theorists have claimed that Beyoncé and Jay Z are part of a secret Satanic cult and sold their souls for fame and fortune. The group allegedly runs the world through a network of rich and powerful members.

Solange is worth an estimated $5m. Not a lot if you compare it to the $900m combined wealth of Jay Z and Beyoncé.

Some fans claimed that the 27-year-old was eager to join the Illuminati and the lift attack was just one of the tasks she was required to perform before she becomes a member.

5) Alcohol and temper tantrums

The duo had had too much to drink during their night out and were getting on each other's nerves. Beyoncé did not get involved because she was sober and knew they would kiss and make up in the morning.