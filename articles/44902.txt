Living materials, created by MIT researchers are able to sense stimuli and react to environmental conditions, mimicking natural systems.

In the bodies of humans and other species, cells gather together in multi-cellular structures like bones and internal organs. These formations often include non-living structures. In order to benefit the organism as a whole, these collections of cells must be able to respond to the environment.

Timothy Lu, led a team of investigators looked at E. coli bacteria, which are present in every human digestive system. These bacteria can produce a thin sheet of cells called a biofilm. Proteins called curli fibers within this layer can hook onto surfaces, and take hold of non-living particles. These materials are then used by the biofilm to grow.

Researchers developed E. coli bacteria capable of capturing specific types of material. They found it is possible to attach nanoparticles, called quantum dots, onto the fibers, changing the nature of the biofilm. Gold particles created films capable of conducting electricity. Other materials and combinations of quantum dots can instill other properties in the film. By using microscopic crystals, Lu and his group designed biofilms capable of absorbing and emitting light.

"Our idea is to put the living and the nonliving worlds together to make hybrid materials that have living cells in them and are functional. It's an interesting way of thinking about materials synthesis, which is very different from what people do now, which is usually a top-down approach." Lu said.

Curli fibers are composed of long strings of a protein called CsgA. Researchers disabled the ability for E. Coli to produce this protein on their own. The bacteria were then reprogrammed to produce the chains when a chemical called AHL is introduced to the system.

Once that was complete, Lu and his team coaxed E. Coli cells to produce CsgA containing chains of an amino acid called histidine. By regulating the concentration of each version of the fiber, control over biofilm production can be controlled. When gold is introduced to the adapted fibers, histidine turns the tiny particles into rows of nanowires.

These new materials can respond to the environment and adapt their own structure to changing conditions. This could make them valuable in future electronics, including energy production and storage.

"This work lays a foundation for synthesizing, patterning, and controlling functional composite materials with engineered cells," researchers wrote in the accompanying article, published in the journal Nature Materials.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.