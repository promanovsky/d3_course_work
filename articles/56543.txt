TORONTO -- BlackBerry (TSX:BB) chief executive John Chen says he's fighting against future product leaks by taking "legal action" that he hopes will set an example.

The head of the Waterloo, Ont.,-based smartphone company alleges that a person he did not name stole confidential details about a future BlackBerry product and leaked them to the public.

He said the individual posed as an employee of a wireless carrier to gain access to BlackBerry's secured networks. From there, the information was taken.

The allegations, which have not been proven, were outlined briefly in a post by Chen on the company's website.

A BlackBerry spokeswoman did not provide any additional details on what the "legal action" would be, and later stopped short of calling it a lawsuit against the unnamed individual.

Chen did not say if the product was a new device or software.

Earlier this week, screenshots from an update to the BlackBerry operating system revealed some new features that are in the works, while numerous other BlackBerry devices have appeared on blogs in recent months.

Product leaks are common in the technology industry, particularly when it comes to smartphones, because of rabid consumer interest in the latest features.

But competitors often reap the rewards of leaked images and product specifics because it gives them a head start on creating their answer to the development.

"Leaks are, at their best, distracting, and at their worst downright misleading to our stakeholders," Chen wrote.

"The business implications of a leak are seldom advantageous."