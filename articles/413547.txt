Amazon Inc. has swooped in for video game site Twitch in a deal worth more than $1 billion, as first reported by the tech news site The Information. Google Inc. (NASDAQ:GOOGL) had been in late-stage talks to acquire the company, which allows users to record and broadcast videos of their game play, but those talks had cooled, giving Amazon an opening.

The deal could be announced as soon as Monday, the Wall Street Journal reported.

Amazon (NASDAQ:AMZN) has been investing heavily in content, including original series like "Alpha House" and "Annedroids." Now the Seattle company will have a network of user-generated content in the popular video game genre, as well as a set-top box, Fire TV. Amazon owns a host of popular websites including IMDB, Vine.com, DPReview and LoveFilm as well as shopping sites like Wag.com, Woot and Soap.com.

The deal puts Amazon in more direct competition with YouTube, home of one of the larger networks of user-generated video game content, Machinima. Twitch claims 55 million unique visitors a month; like YouTube, it sells video and display ads as well as "native" units like "featured game" and placement on its homepage carousel. Machinima's YouTube channel had 15 million unique visitors in the U.S. in June, according to comScore.

One big difference between the two is Machinima is largely dependent on YouTube for its audience and splits revenue with Google's video service. San Francisco-based Twitch owns its audience and has a partner program with 6,400 members that allows producers of video to accept ads and share revenue.

Sites like Twitch are of interest to advertisers because young men are difficult and expensive to reach on TV. But Twitch has them in abundance; 95 percent of its audience is male, the company says, and 60 percent are between 18 and 34.