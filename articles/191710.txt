The grocery retailer Whole Foods Market ( ) reported lackluster Q2 results after the market closed yesterday. The company missed our estimates on both the top and the bottom lines, and reduced its outlook for the full year, dampening investor sentiment and raising concerns about its future growth.

WFM Earnings in Focus

Earnings per share came in at 38 cents. Though earnings were flat year over year, it fell well short of the Zacks Consensus Estimate by 3 cents. Revenues climbed 9.7% year over year to a record $3.32 billion but missed the Zacks Consensus Estimate of $3.35 billion. Comparable store sales rose 4.5% in the quarter, down from 5.4% growth in the prior quarter and 6.9% in the prior-year quarter. This represents the smallest gain in at least 13 quarters.



Lower-than-expected performances were credited to rising competition in the specialty and organic grocery sector, in which WFM has been dominating over the past few years.



Sprouts Farmers Market ( ) went public in August 2013 and continued its expansion in the organic food market. Traditional grocery stores such as Kroger (KR) are also expanding its organic offerings. The largest box retailer Wal-Mart ( ) announced last month that it would carry Wild Oats organic foods in 2,000 stores and sell them at a 25% lower price than the other organic food brands.



Amid rapidly intensifying competition, Whole Foods cut its 2014 outlook for the third time in recent months. Earnings per share are now expected in the range of $1.52–$1.56, down from the previous expectation of $1.58–$1.65 and a far cry from the current estimate of $1.62. The company also reduced its revenue growth projection from 11–12% to 10.5–11% and comparable store sales growth outlook from 5.5–6.2% to 5.0–5.5%.



Weak profits, slowing same store sales growth and disappointing earnings outlook resulted in a massive sell-off in WFM shares. The stock tumbled as much as 15% in after hour trading on Tuesday on heavy volume.

Is This A Buying Opportunity?

The sharp decline in Whole Foods share price could be a compelling buying opportunity for investors over the long term. Though increasing competition and a saturating organic market are likely to weigh on the company’s sales and profit in the short term, WFM is confident of its ability to gain market share on improving demand for fresh and healthy food as the economy grows, attracting huge customers.



Whole Foods is one of the leading natural and organic foods retailers, with a strong brand image and marketing and merchandising expertise. The company expects its sales to reach $25 billion over the next five years and return to its historical gross margin of 34-35%.



Currently, the stock retains a decent Zacks Rank #3 (Hold) and has a solid industry rank (in the top 29%), as per the Zacks Industry Rank, suggesting optimism in the company’s growth story.

ETFs to Watch

The sluggish trading in the stock and some bullish fundamentals put ETFs having higher allocation to this supermarket chain grocer in focus for the coming days. Investors should closely monitor the movement of these funds and take opportunity from any dip in the WFM price.



Investors should note that these products carry much lower risk than owning an individual stock of Whole Foods and could be better choices to play given the heated competition in the organic food market.



PowerShares Retail Fund (PMR)



This fund provides exposure to the retail corner of the broader U.S. consumer space by tracking the Dynamic Retail Intellidex Index. It has accumulated $25.1 million in its asset base while trades in light volume of nearly 13,000 share a day. The ETF charges 74 bps in fees per year.



In total, the product holds 30 securities with Whole Foods occupying the eight position in its basket with 4.43% share. In terms of industrial exposure, food retail takes the top spot at 23.08%, followed by automotive retail (15.64%) and drug retail (13.40%). PMR is down nearly 4% in the year-to-date time frame and has a Zacks Rank of 3 or ‘Hold’ rating with a ‘Medium’ risk outlook.



PowerShares Dynamic Food & Beverage Fund ( )



This product offers targeted exposure to 30 U.S. food and beverage stocks by tracking the Dynamic Food & Beverage Intellidex Index. WFM takes the eight spot in the basket with 4.42% of assets. Packaged food product companies take up nearly 46% of the assets, followed by double-digit allocations to food retail, soft drinks and food distributors.



The fund appears quite rich with AUM of $432 million and average daily volume of more than 93,000 shares. It charges 63 bps in annual fees from investors. The product has added 1.7% so far this year and has a Zacks Rank of 3 with a ‘Medium’ risk outlook.



EcoLogical Strategy ETF ( )



This is an actively managed fund and does not track any index. Rather, it offers capital appreciation by investing in securities of ecologically focused companies that have positioned their business to respond to increased environmental legislation, cultural shifts to more environmentally conscious consumption and capital investments in environmental projects.



The fund has been able to manage $15.7 million in its assets, holding 54 securities. Volume is good as it exchanges more than 150,000 shares a day but has higher expense ratio of 0.95%. Here, Whole Foods occupies the fifth position in the basket with 3.90% allocation. HECO has added nearly 1% in the year-to-date time frame.

Original post