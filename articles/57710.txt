What's that flying overhead? It could soon be a Facebook-owned Internet drone. Facebook CEO Mark Zuckerberg said Thursday that the company will use "drones, satellites and lasers" to achieve its dream of bringing Internet connectivity to everyone in the world. (Read more: 'Mobile shift is happening': Facebook execs)

"We've been working on ways to beam internet to people from the sky," Zuckerberg wrote in a Facebook post Thursday.

Mark Zuckerberg, chief executive officer and co-founder of Facebook. Getty Images

It's part of Internet.org, the Facebook-led initiative launched last year that aims to bring Internet connectivity to the two-thirds of the world's population that currently lacks access. Zuckerberg said in his post that the project has already helped 3 million people access the Internet for the first time in the Philippines and Paraguay.

But, he added, "connecting the whole world will require inventing new technology too." (Read more: Here's who hates the Facebook-Oculus deal)

So Facebook is launching the new Connectivity Lab, which is working on Internet connectivity systems that involve technology like "high-altitude long-endurance planes, satellites and lasers." The idea is to harness technology that can beam to areas that lack Internet infrastructure. The Connectivity Lab team includes experts from NASA and from Ascenta, a U.K.-based company whose founders created early versions of what became the world's longest flying solar-powered unmanned aircraft.

More from NBC News: