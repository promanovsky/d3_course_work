A night full of laughs, tears, and jabs! The 2014 Emmy Awards, held at L.A.’s Nokia Theatre and hosted by Seth Meyers, were full of one-liners, funny acceptance speech interruptions, a somewhat random performance by Weird Al Yankovic, and an emotional tribute to the late Robin Williams. Not to mention, Hayden Panettiere revealed she’s having a girl!

Hollywood paid tribute to the late Robin Williams, as well as Elaine Stritch, Philip Seymour Hoffman, Paul Walker, Shirley Temple, and more who’ve died since the 2013 Emmys, with a poignant in memoriam segment toward the end of the show. After a short montage of photos, Billy Crystal emerged out of the dark to speak about his dear friend Williams, who passed on Aug. 11. “He made us laugh, big time…The brilliance was astounding. The relentless energy was thrilling,” Crystal said of the Mrs. Doubtfire actor.

Hayden Panettiere let it slip to Giuliana Rancic on the red carpet that she’s expecting a baby girl with fiancé Wladimir Klitschko.

Whoa! Julia Louis-Dreyfus was intercepted with a makeout sesh from Bryan Cranston while en route to accept her Emmy for Outstanding Lead Actress in a Comedy Series for her role on HBO’s Veep. The moment was a throwback to when Cranston appeared on Seinfeld in the mid-1990’s as Elaine’s love interest.

Emmys host Seth Meyers hosted a celebrity Q&A during the awards show, where he fielded questions from A-listers like Melissa McCarthy, Jon Hamm, Julianna Margulies and more. McCarthy’s question? Whether or not her car would get towed.

Weird Al Yankovic took the stage with Andy Samberg to perform a quirky montage of revamped TV show theme songs. While spoofing the Game of Thrones theme song, Samberg — dressed as King Joffrey — ambushed GoT author George R.R. Martin with a typewriter.