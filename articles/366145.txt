Jennifer Lopez claims Versace knows everything about her.

The 'I Luh Ya Papi' hitmaker jetted to Paris on the weekend for Donatella Versace's Atelier Versace couture show and spoke about her love for the brand and her chemistry with its designers.

Speaking to WWD at the event, she revealed: ''They did every single look and it was just out of this world. You know, I have a long history with Donatella, obviously, with Versace, and I don't know, it's always one of my go-tos because they know my body.

''Luigi [Massi], their designer, just knows everything about me. It's just one of those things when you have chemistry with somebody.''

The 44-year-old star is currently promoting her new album 'A.K.A' and often enlists the help of designers at Versace to create custom-made outfits for her red carpet and stage appearances.

Jennifer added: ''When you're onstage, there's so much that goes into it. You have to be able to move, it has to be beautiful but it has to be functional as well, and not everybody can do that.''

During the show at Paris' couture fashion week Versace unveiled dream ball gowns, including one which featured a bodice of tiny crystals custom-made by Swarovski, with metres of creamy-brown duchesse satin backed with crinoline.