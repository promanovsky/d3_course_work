Selena Gomez has admitted "losing sight" of herself due to the intense pressure she feels from others.

The 21-year-old singer completed a secret two-week stint in rehab in January, which came after she cancelled part of her tour. She hasn't confirmed why she sought help, with her representative only saying it had nothing to do with addiction.

On Wednesday Selena gave a speech at the We Day event in California, designed to inspire young people to change the world. She used it to touch on some of the problems she's experienced since she became a Disney star aged ten.

"I'm surrounded by people who are supposed to guide me and some of them have and others haven't. They pressure me: 'You gotta be sexy, you gotta be cute, you gotta be nice, you gotta be all these things,'" she said.

"They tell me what to wear, how to look, what I should say, how I should be.

"Until recently I had given in to that pressure; I lost sight of who I was. I listened to opinions of people and I tried to change who I am because I thought others would accept me for it. And I realised I don't know how to be anything but myself."

Selena explained she likes to talk directly to her fans, which is why she had agreed to speak at the event. She pointed out that she is no "activist" and knows she hasn't changed the world in any way, but she hopes her experience might be beneficial for others.

Expand Close OAKLAND, CA - MARCH 26: Selena Gomez in Oakland, CA speaks about youth empowerment to 16,000 students and educators at the first We Day California at ORACLE Arena on March 26, 2014 in Oakland, California. (Photo by Steve Jennings/Getty Images for Free The Children) / Facebook

Twitter

Email

Whatsapp OAKLAND, CA - MARCH 26: Selena Gomez in Oakland, CA speaks about youth empowerment to 16,000 students and educators at the first We Day California at ORACLE Arena on March 26, 2014 in Oakland, California. (Photo by Steve Jennings/Getty Images for Free The Children)

The star also hinted she's had her fair share of knocks over the years.

"I'm sure all of you have been told that you don't have what it takes and that you may not be good enough... when deep down, it's all you want to do, you want to be part of something great, you want to make something great," she said. "It crushes you when people try to tell you that you're not good enough."

The brunette star paid tribute to her mother Amanda, who has been her biggest supporter. She is grateful to her mom for encouraging her to follow her dreams and believe in herself, even when others didn't.