It is no secret that Valve is working on a virtual reality headset of their own, except that it has been a couple of years since Valve had shown it off. With players like Oculus VR (which will soon be owned by Facebook) and Sony entering the fray, we guess Valve thought it might be a good time to start showing off what they had, which is why they invited a few lucky people to attend an event in Boston to take their VR headset prototype for a spin.

Advertising

According to a report posted on Reddit by one of the attendees, it seems that Valve has some pretty cool ideas about what they plan to do, and what they can do with their VR headset. One of the examples was how a seemingly regular game like DotA 2 could be taken to the next level in terms of immersiveness and experience by adding a VR headset to the gaming process.

The poster on Reddit, jonomf, wrote, “Talking with the Valve guys about that Portal office experience, they mentioned offhandedly that they have a Dota 2 VR experience where you see the entire game arena sitting on a table in front of you and can bend down to inspect any piece of the action.” He also adds, “They also mentioned a life-size Dota 2 VR experience where you’re hanging out in a lane watching the heroes fight; they said it was very scary.”

Valve has stated in the past that they believe their VR technology will only be consumer-ready in another two years, or at least that’s what they estimate. They have also mentioned that they believe that Oculus VR will be a good candidate to ship Valve’s VR tech and have helped the company with the tracking on the Crystal Cove prototype that was unveiled earlier this year at CES 2014. In the meantime what do you guys think of a possible VR DotA 2 experience?

Filed in . Read more about Dota 2, Valve, Virtual Reality (VR) and Wearable Tech.