The number of people age 65 and older in the United States is expected to almost double by 2050, a shift that is expected to drastically alter the nation's racial makeup and pressure its economy, two government reports released on Tuesday said.

Those older U.S. residents are expected grow from 43 million in 2012 to nearly 84 million over the next four decades as the baby boomer generation ages, the Census Bureau said in its latest estimate.

One in 5 of the nation's population will be 65 or older by 2030, the year by which all baby boomers - named for the "boom" in U.S. births in the years following the Second World War - hit the unofficial retirement age, the Census Bureau found.

By 2056, its researchers expect another milestone: The number of U.S. seniors will be larger than the number of those age 18 and younger.

"The projected growth of the older population in the United States will present challenges to policy makers and programs, such as Social Security and Medicare. It will also affect families, businesses and health care providers," researchers wrote.

At the same time, as boomers age and die, their decline is expected to spur dramatic changes as the mostly white generation makes way for an increasingly diverse younger population.

Currently, there are about 76 million boomers. That number is expected to drop to 60 million by 2030 and just 2.4 million by 2060, researchers estimated.

"This pattern, coupled with increases in immigration and births to minority populations, is projected to produce an increasingly diverse population in the years to come," they wrote.

The number of births among racial minorities recently surpassed that of white births, and officials estimate that non-whites will make up the majority of the U.S. population by 2060.

Census, in its reports on Tuesday, said while boomers generally reflect the mostly white composition of the United States between 1946 and 1964, the nation's older population is also expected to diversify somewhat, in part due to differing life expectancies as well as immigration.

Researchers estimated that by 2050, 39 percent of those age 65 and older will be black, Hispanic, Asian or other racial minority, up from about 21 percent in 2012, the latest available year for census projections data.

Like the overall U.S. population, older Hispanics are expected to see the greatest increase, according to the report.

Even as the United States braces for its demographic evolution, "it is still younger than most other developed countries," researchers added.

Other countries such as Japan, Germany and Italy are also facing graying populations expected to make up more than one-quarter of their total residents by 2030, Census said. In comparison, just 20 percent of those in the United States will be age 65 and older.