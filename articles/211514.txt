The Saudi Arabia health ministry reported three more deaths from the Middle East Respiratory System (MERS) coronavirus, bringing the number of deaths in the kingdom to 142.

On May 12, just before an emergency meeting of the World Health Organization on the disease, a man died in Jordan due to MERS, bringing the total number of deaths in Jordan to five since 2012 when the virus first emerged. The 45-year-old man worked in a private medical facility.

The MERS coronavirus has so far infected 483 people in the Middle East since 2012, a majority of the 496 reported cases worldwide. Most of the MERS infections were in Saudi Arabia but there were reported cases in Jordan, United Arab Emirates, Lebanon, Egypt and the United States.

Though less-transmissible, MERS is more fatal than the SARS virus which spread in Asia in 2013, killing almost 800 people and infecting over 8,200 people. MERS is similar to SARS in that it causes lung infection, coughing and breathing difficulties, but MERS also causes quick kidney failure.

MERS has been dubbed as the "killer bug," with no known vaccines or antiviral drugs to prevent or treat it. Evidence shows that MERS is found in camels in the Middle East. Public Health England, a xxx agency, has already issued advice to people who will travel to the Middle East, urging them to avoid contact with camels, including consumption of raw camel meat or milk.

Health authorities encourage the public to regularly wash hands with soap and dry them completely. People should avoid touching their eyes, mouth or nose and use disposable tissues, especially when sneezing and coughing.

The Saudi Arabia health ministry encourages those who are 65 years old and above, with any chronic disease, pregnant women and children below 12 years old, to postpone pilgrimages while there is still a MERS virus outbreak. The Ministry of Agriculture also urges people to be cautious and follow preventive measures should they need to be in contact with camels.

WHO will hold its emergency meeting on May 13 to discuss the global concern of MERS. "The increase in the number of cases in different countries raises a number of questions," Tarik Jasarevic of WHO said on May 9.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.