Robonaut, the first out-of-this-world humanoid, is finally getting its space legs.

For three years, Robonaut has had to manage from the waist up. This new pair of legs means the experimental robot — now stuck on a pedestal — is going mobile at the International Space Station.

"Legs are going to really kind of open up the robot's horizons," said Robert Ambrose from NASA's Johnson Space Center in Houston.

It's the next big step in NASA's quest to develop robotic helpers for astronauts. With legs, the 2.4-metre Robonaut will be able to climb throughout outpost, which orbits 420 kilometres above the Earth, performing mundane cleaning chores and fetching things for the human crew.

The robot's gangly, contortionist-bending legs are packed aboard a SpaceX supply ship that launched Friday, more than a month late. It was the private company's fourth shipment to the space station for NASA and is due to arrive Easter Sunday morning.

Robonaut 2 — R2 for short — has been counting down the days.

"Legs are on the way!" read a message Friday on its Twitter account, (at)AstroRobonaut. (OK, so it's actually a Johnson Space Centre spokesman who's doing the tweeting.)

Space Exploration Technologies Corp.'s unmanned capsule, Dragon, holds about 2 tons of space station supplies and experiments, including Robonaut's legs.

Until a battery backpack arrives on another supply ship later this year, the multimillion-dollar robot will need a power extension cord to stretch its legs, limiting its testing area to the U.S. side of the space station. Testing should start in a few months.

Each leg — 142 centimetres long — has seven joints. Instead of feet, there are grippers, each with a light, camera and sensor for building 3-D maps.

"Imagine monkey feet with eyes in the palm of each foot," Ambrose said.

'A little creepy'

NASA engineers based the design on the tether attachments used by spacewalking astronauts. The legs cost $6 million to develop and another $8 million to build and certify for flight. The torso with head and arms delivered by space shuttle Discovery in 2011 on its final flight cost $2.5 million, not counting the untold millions of dollars spent on development and testing.

Ambrose acknowledges the legs are "a little creepy" when they move because of the number of joints and the range of motion.

"I hope my knee never bends that many degrees, but Robonaut has no problems at all," said Ambrose, chief of software, robotics and simulation division at Johnson.

The robot's not going to have as much fun as the astronauts. No jumping, no somersaults, no flying. - Robert Ambrose, NASA Johnson Space Center

The grippers will latch onto handrails inside the space station, keeping Robonaut's hands free for working and carrying things. Expect slow going: just inches a second. If Robonaut bumps into something, it will pause. A good shove will shut it down.

"The robot's not going to have as much fun as the astronauts," Ambrose said. "No jumping, no somersaults, no flying."

Robonaut already has demonstrated it can measure the flow on air filters, "a really crummy job for humans," Ambrose said. Once mobile, it can take over that job around the station.

How about cleaning the space station toilets? "I have a feeling that's in Robonaut's future," Ambrose said.

Staying indoors

This robot will stay indoors as it learns how to climb. The next-generation model, currently in development and targeted for a 2017 launch, will venture outside on spacewalks. That's where the real payoff lies.

A robot doesn't need oxygen tanks and fancy spacesuits. A robot never tires or gets bored. Why, a robot could stay out in the vacuum of space for days, weeks or even months, clinging to the station. Human spacewalkers are limited to eight or nine hours.

Now imagine base camps on the moon, Mars or beyond staffed by a team of robotic caretakers. Future Robonauts could be deployed in advance and get everything running before the humans arrive — and stay behind when they leave.

And if there's a chore too risky for humans "we could let the machine go out and sacrifice itself," Ambrose said, "and that's OK. It's not human. We can build another one. We'll build one even better."

NASA's space station program manager, Mike Suffredini, cautioned Friday that there's still "quite a ways to go" before future Robonauts make spacewalk repairs like the computer replacement job coming up Wednesday for the two U.S. space station astronauts. Software is the biggest challenge, he said, but "these are great first steps."

"They won't ever replace the crews, but they could do a lot of the jobs," Suffredini said.