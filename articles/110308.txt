HTC is committed to using an all-metal build for its high-end flagship smartphones. Both the original HTC One and the new HTC One M8 tout metal builds which have been much appreciated, in fact, the company itself considers the metal body a plus over the plastic Galaxy S5. So this rumor doesn’t exactly make a whole lot of sense. A rumor surfaced today claiming that a plastic HTC One M8 is going to be released in the near future. Apart from the difference in build material everything else, as far as the physical appearance goes, is expected to be the same.

Advertising

Since plastic will be cheaper, one can expect that this rumored variant would cost less than its metal counterpart. The rumor does claim this, adding that while the all metal HTC One M8 costs 5,299 Yuan or roughly $850, the plastic variant will cost 3,000 Yuan or $483. Its unclear if crucial hardware components like processor, RAM, camera etc will be replaced with cheaper options to keep the price down.

Since this seems highly unlikely, let us consider for a moment anyway if a plastic HTC One M8 really did come out. The only possible markets it could perform in would be those where carrier subsidies don’t exist. That way HTC would be able to undercut the Galaxy S5 on price, particularly if all the internals are kept the same as the all metal variant.

But as I previously mentioned, this seems highly unlikely. So take all of this with a grain of salt. HTC hasn’t commented on the rumor.

Filed in . Read more about Galaxy S5, HTC One (M8) and Htc One M8.