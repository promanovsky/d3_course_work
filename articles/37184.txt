Conakry (Guinea):Guinea has confirmed that a disease which has killed almost 60 people in the West African country, and may have spread to neighbouring Sierra Leone, is the haemorrhagic fever Ebola, a media report said.

The fever is introduced into the human population through close contact with infected animals, including chimpanzees, gorillas, fruit bats, monkeys, forest antelope and porcupines.

The disease, transmitted between humans through contact with organs, blood, secretions, or other bodily fluids, is most commonly found in the Democratic Republic of Congo, Uganda, South Sudan and Gabon.

Cases of the disease -- among the most virulent pathogens known to infect humans, with a fatality rate of up to 90 percent -- have been registered in three south-eastern towns and here in the capital since Feb 9, the New Zealand Herald quoted the government as saying, but adding that it has never been recorded in Guinea earlier.

Six of the 12 samples sent for analysis tested positive, said Sakoba Keita, who heads the epidemics prevention division at Guinea`s health ministry.

He added that health officials had registered 80 suspected cases of the disease, including 59 deaths.

World Health Organisation (WHO) officials said that cases showing similar symptoms, including fever, diarrhoea, vomiting and bleeding, had been reported in Sierra Leone near the Guinea border.