A viral Kickstarter campaign by a user named Zack "Danger" Brown of Columbus, Ohio, launched with a modest goal of $10 to make potato salad on July 3, 2014.

Within seven days, the project has raised more than $42,000 from more than 5,000 backers.

To entice Kickstarter users to fund his project, Brown currently offers nine different rewards, ranging from a "thank you" on his website for pledging $1 to a potato salad cookbook for people willing to donate $50.

Most of the reward offers include a bite of the potato salad Brown intends to make with the funds, which would make his campaign compliant with the Kickstarter rule that "projects must create something to share with others."

Some of the more amusing rewards include a signed jar of mayonnaise, a photo of Brown making the salad and having the donor's name carved into a potato to be used in the salad.

The viral attention on Brown's project has landed him an upcoming appearance Tuesday on Good Morning America, according to the project's Kickstarter page, and he conducted an "Ask Me Anything" interview session on Reddit on Sunday, July 6.

Get the Breaking News newsletter! Get the latest breaking news as it happens. By clicking Sign up, you agree to our privacy policy.

Brown updated his Kickstarter campaign on Wednesday, July 9, informing backers that he is working with his local independent radio station, CD 102.5, to set up a benefit concert. Brown dubbed the concert "Potato Stock," for the potato salad party he is looking to throw with raised funds.

Justin Kazmark, a member of the Kickstarter communications team, commented on whether the campaign is in compliance with the site's rules, saying, "Kickstarter's a global community of millions of people who fund projects of all shapes and sizes. There's no single recipe for inspiration."