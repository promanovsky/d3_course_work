Rapper Kanye West has reportedly angered guests at his wedding to Kim Kardashian by constantly changing their plans.

The 'Bound 2' rapper is said to be growing increasingly paranoid about information leaks so has told different groups friends and family members different locations as to where he and Kardashian will tie the knot, but then gives them different information hours later, reported Contactmusic.

"Kanye is furious about the leaks. He's acting paranoid and wants to know who he can trust. So he is telling some guests one plan, and other guests another plan. He is totally messing with everyone," a source said.

The couple, who are flying friends and family out to Paris, France for a pre-wedding party on Friday ahead of the big day which is expected to be in Florence, Italy, are insisting on confiscating guests' mobile phones for security reasons.

They will be issuing temporary handsets and this is said to be causing more unrest because their relatives won't even know which country they are in or be able to get in touch.