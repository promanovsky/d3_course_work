Low-cost airline EasyJet Plc. (ESYJY.PK,EZJ.L) said it now expects pre-tax loss for the six months ended 31 March 2014 to be between 55 million pounds and 65 million pounds compared with the previous guidance of a pre-tax loss of 70 million pounds to 90 million pounds.

easyJet said it expects to deliver a first half performance ahead of the guidance given in the 23 January 2014 Interim Management Statement.

Revenue per seat growth at constant currency for the six months to 31 March 2014 is expected to be about 1.5%, driven, in part, by allocated seating, increased average sector length and a number of digital and revenue management initiatives. Previously, Revenue per seat growth at constant currency for the six months to 31 March 2014 was expected to be "Very slightly up".

As previously indiacted, last year Easter fell on 31 March resulting in 25 million pounds of additional revenue in the first half of 2013. In this financial year Easter will fall in the second half on 20 April.

Capacity growth for the six months period is still expected to be 3.5%.

Cost per seat growth excluding fuel at constant currency for the period is expected to be about 0.5% which is better than the guidance, driven by a benign winter with reduced levels of de-icing and disruption in the three months to 31 March 2014. It also reflects the early delivery of a number of easyJet lean initiatives. Earlier, Cost per seat growth excluding fuel at constant currency is expected to be about 1.5%.

For comments and feedback contact: editorial@rttnews.com

Business News