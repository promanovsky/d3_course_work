Recently, local police departments have taken notice of the growing number of heroin overdoses they've seen in people who wouldn't have been your typical addicts 50 years ago.

Authorities have said that the drug is moving out of cities and into rural areas, where people who become addicted to prescription painkillers switch to heroin once their pill habit becomes too expensive.

Advertisement

A new study published in the Journal of the American Medical Association backs up that assertion. The study found thatThis is a stark difference from the 1960s, when heroin users tended to be young men living in cities whose first exposure to opioids came from heroin.For the study, researchers analyzed survey data from about 2,800 patients, some of whom entered treatment centers for heroin use and others who completed a detailed interview.

This chart shows how exposure to opioids has changed over the years:

Advertisement

Respondents who started using heroin in 1960 had an average age of 16.5, and 80% of them said their first opioid use was heroin. Those who started using heroin in recent years, however, had an average age of 23, and 75% of them said they were first exposed to opioids through prescription drugs.

Advertisement