Twitter has acquired social data provider Gnip, extending the companies' four-year partnership.

Financial terms of the deal were not disclosed.

According to Twitter, this move will help "to offer more sophisticated data sets and better enrichments" for developers and businesses.

Gnip (pronounced guh-nip) launched in 2008 and served as the first official data partner for Twitter, Tumblr, Foursquare, Wordpress, Google+, Facebook, and YouTube.

With access to the full "firehose" of data from various social networks, the firm collects content, boils it down to company-specific sets, and sends them to organizations around the world.

In its four years working with Twitter, Gnip has delivered more than 2.3 trillion tweets in 42 countries, providing insights into business intelligence, marketing, finance, professional services, and public relations.

Twitter itself delivers more than 500 million messages each day, counting on Gnip to help collect and digest that public data and deliver the most essential tweets to partners.

"This acquisition signals clear recognition that investments in social data are healthier than ever," Gnip CEO Chris Moody wrote in a blog announcement.

It is unclear whether Gnip will continue working with other sources, including some of Twitter's big-name social network rivals. Moody did say, however, that the company "will be introducing new offerings with Twitter."

Twitter's vice president of global business development and platform, Jana Messerschmidt, added that the micro-blogging site "will continue making our data available to Gnip's growing customer base."

"We want to make our data even more accessible, and the best way to do that is to work directly with our customers to get a better understanding of their needs," Messerschmidt said.

Twitter will also take advantage of Gnip's Colorado-based team to extend the Twitter data platformthrough Gnip and existing public APIs.

There may be more hidden perks, as well: This acquisition means Twitter can more easily sell data direct to brands in an effort to boost revenue, TechCrunch pointed out. Instead of trying to create conversation out of ad topics, the network could entice marketers to buy into already-trending topics.

Neither organization immediately responded to PCMag's request for comment.

Further Reading