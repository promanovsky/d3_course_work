LOS ANGELES, June 30 (UPI) -- Transformers: Age of Extinction dominated the box office over the weekend.

The fourth installment in the Transformers series earned $100 million domestic and $201.3 million overseas. It is the first movie to pass the $100 million mark this year in North America; the last film to do so was Hunger Games: Catching Fire in November. Comparatively, Captain America: The Winter Soldier earned $96.2 million on its opening weekend in April, and X-Men: Days of Future Past raked in $90.7 million in May.

Advertisement

Michael Bay returned to direct Age of Extinction, which features an all-new cast. The movie is set four years after Dark of the Moon and stars Mark Wahlberg as struggling inventor Cade Yeager, Nicola Peltz as his daughter Tessa, Stanley Tucci as Kinetic Sciences Institute head Joshua Joyce and Kelsey Grammer as paranoid CIA agent Harold Attinger. The film is also the first to include Dinobots, which are dinosaur robots.

"Mark is incredibly likable," Paramount vice chairman Rob Moore said in a statement. "Together, Michael and Mark have refreshed the franchise. You definitely see the great chemistry between them."

Bay's next project to debut is Teenage Mutant Ninja Turtles, which he produced. The movie is out August 8, and stars Transformers and Transformers: Revenge of the Fallen star Megan Fox as reporter April O'Neill.