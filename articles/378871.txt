An exceptionally large and glorious moon will grace our skies this weekend. The Moon will be full at the perigee point of its orbit — the point when it's closest to Earth.

According to NASA , the Moon will appear 14% bigger and 30% brighter as a result.

Greg Laughlin, an astronomer at UC Santa Cruz, recommends watching the moonrise for the full effect. “Your brain is able to compare the moon visually against objects of known size,” Laughlin told SFGate. “When the moon is high in the sky, you don’t have the reference.”

Traditionally, July's perigee full moon is called a Buck Moon, since young male deer are said to sprout their antlers around the time of its appearance. It's also known as a Hay Moon if the weather's fine, or a Thunder Moon if it turns stormy.

The Moon will beam its radiance unto humanity on both Friday and Saturday nights. If you can't see it locally due to clouds or light pollution, you can watch this live webcast from SPACE.com, courtesy of the skywatching website Slooh Space Camera.

The large, close Moon should cause unusually high perigean tides, too — so surfers and beachcombers could be in luck.