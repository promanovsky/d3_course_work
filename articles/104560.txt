Hundreds of millions of pounds spent stockpiling the anti-viral drug Tamiflu to combat a flu pandemic may have been a waste of money according to a review of the drug's effectiveness.

The Cochrane Collaboration, a global health-care research network, says its studies show Tamiflu shortened the symptoms of flu by just half a day and did nothing to reduce hospital admissions or complications.

Professor Carl Heneghan, one of the report authors and Professor of Evidence-Based Medicine at the University of Oxford, said there was no point "spending a lot of money on ineffective treatment".

The manufacturers Roche and other experts say the analysis is flawed; Prof Heneghan said the analysis examined Roche's own data and was robust.