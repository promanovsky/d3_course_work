We almost choked on our coffee when an image from the Star Wars Episode VII script read-through hit Star Wars.com on Tuesday evening (April 29, 2014). The black-and-white photograph showed Harrison Ford and Mark Hamill relaxing on the sofas with new cast-members Adam Driver, Domhnall Gleeson and Daisy Ridley. But there was on actor who flew under the radar. Perhaps the finest of the talented bunch. Max Von Sydow.

Max Von Sydow Is Set For Star Wars: Episode VII

The legendary actor, now 85, is probably best known for his performances in The Seventh Seal, The Exorcist and Three Days of the Condor.

More: Guess who? Star Wars Episode 7 cast revealed in team photo

In a statement, director JJ Abrams, who also rebooted the Star Trek movie franchise, said: "We are so excited to finally share the cast of Star Wars: Episode VII. It is both thrilling and surreal to watch the beloved original cast and these brilliant new performers come together to bring this world to life, once again.

"We start shooting in a couple of weeks, and everyone is doing their best to make the fans proud."

Some footage has already been shot in the deserts of Abu Dhabi, but the cast featured in the photograph will mainly be working at Pinewood Studios, outside London.

Watch Max Von Sydow at the premiere of Extremely Loud and Incredibly Close:



You may remember the leaked casting call descriptions leaked back in June, which led to the nationwide sessions in the UK.

One of the character descriptions read: "Seventy-something male, with strong opinions and tough demeanour. Also doesn't need to be particularly fit."

This is almost certainly the role Von Sydow will be playing. Probably an imperial villain.

Elsewhere, Disney Ridley may well be playing the role of Han Solo's daughter (remember the late-teen female from the casting call), while John Boyega is likely to be a young Jedi - perhaps a friend of the young Skywalker.

Gleeson could well be Skywalker's son, while it's no secret that Adam Driver will play an intellectual, scheming Sith.