Crew aboard the Australian Defence Vessel Ocean Shield move the U.S. Navys Bluefin-21 into position for deployment in the southern Indian Ocean April 14, 2014 to look for the missing Malaysian Airlines flight MH370 in this picture released by the U.S. Navy. Australia's deputy prime minister on May 29, 2014 said officials remained confident the missing Malaysia Airlines jet is somewhere in southern Indian Ocean despite searchers saying wreckage was not on the seabed in the area they had identified. The search was narrowed last month after a series of acoustic pings thought to be from the plane's black box recorders were heard near where analysis of satellite data put its last location, some 1,600 km (1,000 miles) off the northwest coast of Australia. Picture taken April 14, 2014. REUTERS/U.S. Navy photo by Mass Communication Specialist 1st Class Peter D. Blair/Handout (MID-SEA - Tags: MILITARY TRANSPORT MARITIME) ATTENTION EDITORS - THIS PICTURE WAS PROVIDED BY A THIRD PARTY. REUTERS IS UNABLE TO INDEPENDENTLY VERIFY THE AUTHENTICITY, CONTENT, LOCATION OR DATE OF THIS IMAGE. THIS PICTURE IS DISTRIBUTED EXACTLY AS RECEIVED BY REUTERS, AS A SERVICE TO CLIENTS. NO SALES. NO ARCHIVES. FOR EDITORIAL USE ONLY. NOT FOR SALE FOR MARKETING OR ADVERTISING CAMPAIGNS

An eye-witness account from yachting enthusiast Katherine Tee, 41, has now been filed with the Joint Agency Coordination Centre (JACC) leading the search based in Australia, which is yet to find any trace of the missing jet.

In her account, Ms Tee has described seeing “an elongated plane glowing bright orange, with a trail of black smoke behind it”, heading from north to south.

The object was flying at “about half the height” of two other flights in the same patch of sky at the time, she said, in a series of posts detailing the sighting on the yachting website Cruisers Forum, where Ms Tee is a moderator.

She later told the Phuket Gazette that she initially dismissed the incident as being “just a meteor” or even a figment of her imagination – until a recent check of GPS logs confirmed that the plane’s projected track may have taken it very close to where the yacht was sailing at the time.

Ms Tee’s 50-year-old husband Marc posted the data maps from their journey to the forum, where other members suggested it was very possible she could have been the last person to see the plane before it headed south into very remote seas.

“This is what convinced me to file a report with the full track data for our voyage to the relevant authorities,” she said.

As for whether or not what Ms Tee saw really was a burning plane, she has admitted on the forum that she was going through a particularly stressful part of the journey at the time, including difficulties in her marriage, and that her judgement may have been impaired.

Independent News Service