Treatment for psoriatic arthritis is moving away from the traditional injected medication, with a pill by the Celgene Corp newly approved for use by the Food and Drug Administration (FDA).

Known as Otezla (the brand name for apremilast), the pill treats arthritic symptoms associated with psoriasis, and may also be used for treating psoriasis itself. That decision, however, is yet to be made, with the FDA aiming for a September verdict.

Nevertheless, the news of approval has been welcomed by the medical community. "Relief of pain and inflammation and improving physical function are important treatment goals for patients with active psoriatic arthritis," said Curtis Rosebraugh of the FDA's Center for Drug Evaluation and Research. "Otezla provides a new treatment option for patients suffering from this disease."

Psoriasis is characterized by red, flaky patches on the skin, and the companion condition psoriatic arthritis compounds these symptoms with soreness stiffness, and inflammation in the joints. It affects around 7.5 million in the United States.

The FDA's decision was based on the results of three separate clinical trials, comprised of 1,500 test subjects in total. Though the FDA has granted approval, further scrutiny will be required for those using the drug during pregnancy, with a pregnancy exposure registry set to be established to observe any adverse side effects.

Celgene Corp anticipates that Otezla will rake in $1.5 billion to $2 billion by 2017, with analysts cautiously optimistic about the perceived revenue figures. "We're cautious about achievability of management's guidance for this product," said Sanford Bernstein analyst Geoffrey Porges. "It's a highly competitive category and there are a lot of drugs with greater efficacy."

Indeed, while Otezla is the first of its kind as a drug in pill form, several other drugs exist as injectables - and may provide greater relief for sufferers of psoriatic arthritis. Otezla's point of difference is particularly notable for those susceptible to skin allergies and reactions as a result of the injections.

Despite reservations from some independent analysts, Otezla similarly has its supporters. "We think that the psoriatic arthritis market will be a solid fit for Otezla given the dissatisfaction with current drugs," wrote Cantor Fitzgerald analyst Mara Goldstein in a research note. "We think the drug has a good chance of widespread use."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.