Beyoncé and Jay Z, the supreme-reigning music industry power couple whose combined $900 million net worth makes them the world’s richest celebrity couple, on Saturday released a “surprise” video trailer for a non-existent movie in which they are portrayed as desperate, gun-toting outlaws.

The trailer is directed by Melina Matsoukas, a prolific director of music videos who won a Grammy award for directing the video accompanying the Rihanna single, “We Found Love.” The clip also features several other celebrities acting out supporting roles in what appears to be a never-to-be-produced gangster epic starring Beyoncé and Jay Z.

Sean Penn appears sitting across a table from Jay Z in a dark room, looking grim as Jay Z delivers such lines as “One time you told me, if the s*** gets bigger than the cat, get rid of the cat.”

Penn in turn delivers such advice to Jay Z as “Don’t get cocky” and “It’s time to bump, you gotta take a powder.”

The video also features Blake Lively telling Beyoncé, “I don’t want to lose you,” and Jake Gyllenhall answering a telephone and saying, “They’re not here right now.”

Don Cheadle also appears in the trailer, as — as far as can be determined — a rival to Jay Z who is cryptically threatened by the rap star and entrepreneur.

The trailer appears to reveal no hint of irony or intentional humor, at least until the final seconds when the soundtrack plays a song, the final lyric of which is the word “cliché” repeated four times.

The celebrity couple appear coming out of an elevator in ski masks in the video, which presumably was shot sometime before last week’s leak of a surveillance video from New York’s Standard Hotel which showed 44-year-old Jay Z, whose real name is Shawn Carter, and his 32-year-old wife Beyoncé in an elevator with Beyoncé’s younger sister, Solange, 27. In that video, Solange Knowles physically attacks Jay Z for several minutes while Beyoncé stands idly by. The group then exits the elevator as if nothing had happened.

Beyoncé and Jay Z released a statement later in the week saying, “Jay and Solange each assume their share of responsibility for what has occurred. They both acknowledge their role in this private matter that has played out in the public. They both have apologized to each other and we have moved forward as a united family.”

While the new Jay Z and Beyoncé trailer, apparently designed to reflect the “On The Run” theme of their upcoming concert tour, may not raise any new questions about their relationship, it has already led to speculation about their creative judgment and level of self-awareness.

Watch the Beyoncé and Jay Z Run trailer below.