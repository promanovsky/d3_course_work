ATLANTA, March 12 (UPI) -- Tests in monkeys show a gel containing an HIV drug stops the virus from infecting cells -- even if applied after sex, U.S. researchers say.

Dr. Walid Heinene of the Centers for Disease Control and Prevention in Atlanta tested a gel containing the human immunodeficiency virus drug raltegravir. The gel protected monkeys when given before they were infected with HIV vaginally, but it also prevented infection when applied as long as 3 hours after sex, NBC News reported.

Advertisement

If the gel has a similar impact in humans, women would be able protect themselves from HIV infection after sexual intercourse, Heinene said.

The HIV virus that causes AIDS can be transmitted in blood, semen and breast milk, but most people are infected sexually and in Africa, where most cases occur, 60 percent of HIV patients are women. Almost all were infected by husbands or other sexual partners, Heinene said.