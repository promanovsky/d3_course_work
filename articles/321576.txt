Women who naturally conceive their last child after age 33 tend to live longer than those who have their final child by age 29, according to a new study by researchers at Boston University School of Medicine.

The study, published Wednesday in the journal Menopause, looked at data from 462 women who were part of The Long Life Family Study, which included families with members who had lived long lives. The researchers compared the ages at which these women had their last child and how long they lived.

Those who got pregnant naturally and successfully birthed their last child after age 33 were twice as likely to live to age 95 compared to those who had their last child by age 29.

Advertisement

The average age of women at first birth has risen over the past four decades, according to the U.S. Centers for Disease Control and Prevention. A growing number of U.S. women are now having children after age 35. The number of women undergoing fertility treatments such as in vitro fertilization to conceive later in life has also steadily increased. The current findings did not look into the life spans of women who used medical assistance to conceive.

The current findings suggest that prolonged fertility may be linked to a genetic marker for longevity, though a marker hasn’t yet been identified, according to Dr. Thomas Perls, a co-author of the study, a professor of medicine at Boston University School of Medicine and the Director of the New England Centenarian Study

“The natural ability to have a child at an older age likely indicates that a woman’s reproductive system is aging slowly, and therefore so is the rest of her body,’’ Perls said in a public statement.

The study findings held true even when researchers took into account lifestyle factors like smoking and obesity, which decrease the chance of having children naturally and are also considered age accelerators.

Giving birth at older ages also means parenting younger children at an older age, a factor that seems disadvantageous to some women. But for these women, it might not be all that bad, Perls said.

Advertisement

“If they are aging slowly, I would wager they’re in good shape to take care of these children at older ages too,’’ Perls said.

Previous research conducted by Perls found that women who gave birth after age 40 were four times more likely to live to 100 than younger women. However, Perls said, both the previous and current findings are not a justification for women to wait until they’re older to have children. The women included in the study had a family history of exceptional longevity, and, in general, the chances of a woman being able to conceive naturally diminishes the older she gets, he said.