The Hunger Games: Catching Fire took the prize for best film at the MTV Movie Awards, as well as best male and female performance for actors Josh Hutcherson and Jennifer Lawrence.

Johnny Depp gave the popcorn-shaped award to two of the film's stars, Hutcherson and Sam Claflin.

Jonah Hill won best comedic performance for The Wolf of Wall Street.

Jared Leto presented Mila Kunis with the best villain prize, while Channing Tatum won the trailblazer award.

Watch highlights from the ceremony.