The Canadian Cancer Society is striking out at the Alberta Government for failing to protect minors from tanning beds, in their annual Canadian Cancer Statistics report, released on Wednesday.

According to the report, Melanoma, the deadliest form of skin cancer, has been on the rise since 1986. The Society says approximately 6,500 Canadians and 570 Albertans will be diagnosed with the disease in 2014.

“The increasing rate of melanoma is especially concerning because the disease is highly preventable by using proper skin protection and not using tanning beds,” Evie Espheter with the Canadian Cancer Society wrote in the media release. “The Government of Alberta must do its part in helping to reverse this trend by introducing legislation that will ban minors from using tanning beds.”

The study goes on to say that people who start using tanning beds before the age of 35 are 59% more likely to get the disease, and that Melanoma is the most common cancer to be diagnosed in people between the ages of 15 and 29 years of age.

“Tanning beds are known to cause cancer,” Espheter says. “Nearly one in three 17-year-old girls in Alberta have used tanning beds. Based on current risk behavior, it’s likely that melanoma rates will continue to increase year after year if young people continue to use indoor tanning equipment.”

Alberta and Saskatchewan are currently the only provinces that do not regulate youth access to tanning equipment. The Northwest Territories introduced legislation in 2013 banning anyone under the age of 19 from using tanning beds.