NEW YORK (TheStreet) -- Shares of Radio One Inc. (ROIAK) are down -6.50% to $4.03 on Thursday after reporting a net loss of -$25.2 million, or 53 cents per share for the 2014 first quarter, compared to a net loss of -$18.1 million, or 36 cents per share for the same quarter the previous year.

The urban-oriented multi-media company reported a 12.1% increase in revenue for the most recent quarter versus the $99.1 million reported in the 2013 first quarter.

Must Read: Warren Buffett's 10 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates RADIO ONE INC as a Hold with a ratings score of C-. TheStreet Ratings Team has this to say about their recommendation:

"We rate RADIO ONE INC (ROIAK) a HOLD. The primary factors that have impacted our rating are mixed some indicating strength, some showing weaknesses, with little evidence to justify the expectation of either a positive or negative performance for this stock relative to most other stocks. The company's strengths can be seen in multiple areas, such as its revenue growth, solid stock price performance and expanding profit margins. However, as a counter to these strengths, we also find weaknesses including disappointing return on equity, weak operating cash flow and generally higher debt management risk."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

ROIAK's revenue growth has slightly outpaced the industry average of 5.1%. Since the same quarter one year prior, revenues slightly increased by 5.4%. This growth in revenue does not appear to have trickled down to the company's bottom line, displayed by a decline in earnings per share.

Compared to its closing price of one year ago, ROIAK's share price has jumped by 188.81%, exceeding the performance of the broader market during that same time frame. Regarding the stock's future course, our hold rating indicates that we do not recommend additional investment in this stock despite its gains in the past year.

RADIO ONE INC' earnings per share from the most recent quarter came in slightly below the year earlier quarter. This company has not demonstrated a clear trend in earnings over the past 2 years, making it difficult to accurately predict earnings for the coming year. During the past fiscal year, RADIO ONE INC continued to lose money by earning -$1.30 versus -$1.33 in the prior year.

Return on equity has greatly decreased when compared to its ROE from the same quarter one year prior. This is a signal of major weakness within the corporation. Compared to other companies in the Media industry and the overall market, RADIO ONE INC's return on equity significantly trails that of both the industry average and the S&P 500.

Net operating cash flow has decreased to $14.55 million or 10.49% when compared to the same quarter last year. In addition, when comparing the cash generation rate to the industry average, the firm's growth is significantly lower.

You can view the full analysis from the report here: ROIAK Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.