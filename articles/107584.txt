File photo: The focus of the FBI search at the Vitreo-Retinal Consultants Eye Center was reportedly Dr. Salomon Melgen’s finances. (Source: CBS4)

WEST PALM BEACH (AP) — A wealthy Florida doctor whose offices were raided by the FBI and who allowed a U.S. senator to use his private jet is again under scrutiny for receiving more money from the taxpayer-funded Medicare program than any other doctor.

A massive data dump from federal health officials this week showed South Florida ophthalmologist Dr. Salomon Melgen received nearly $21 million in 2012 from the tax-payer funded Medicare program — more than any other doctor in the country. Melgen has not been criminally charged or accused of wrongdoing.

Melgen’s attorney, Kirk Ogrosky, said the doctor’s billing conformed with Medicare rules and is a reflection of high drug costs, noting he has four offices and treats hundreds of patients every week.

“Dr. Melgen stands by his record of improving the vision and quality of life of patients from around the world,” Ogrosky said in a statement.

In photos with political luminaries, Melgen seemed to relish the attention cast on him as a revered and reliable donor. But he appears to be shying away from the spotlight since the scandal broke with Sen. Robert Menendez of New Jersey, who repaid the doctor more than $70,000 for the flights to the Dominican Republic.

Below is a look at Melgen’s public life in recent years:

LOVING THE LIMELIGHT

Melgen is a Dominican Republic native who came to the U.S. and built a reputation as a top ophthalmologist. After operating on former Florida Gov. Lawton Chiles, a Democrat, in 1997, he became a reliable donor to Democratic power brokers and a frequent host of fundraisers, both at his massive North Palm Beach home and his house in an exclusive resort community in the Dominican Republic.

“He loved the limelight, he loved it,” Patricia Goodman, his office manager and personal assistant throughout the 1990s, told The Associated Press last year. “He loved being with the politicians.”

He can be seen smiling in photos, sandwiched between Menendez and former House Speaker Nancy Pelosi, and his name is a frequent entry in campaign finance logs.

In 2012 alone, Melgen’s company, Vitreo-Retinal Consultants, gave $700,000 to Majority PAC, a super political action committee set up by former staffers for Democratic Senate Majority Leader Harry Reid of Nevada to aid the party’s Senate candidates nationwide, according to a search of records with the Center for Responsive Politics.

Separately, Melgen, his wife Flor and daughter Melissa gave more than $350,000 in individual contributions between 1998 and 2012 to a variety of candidates and committees.

All of that came to a halt last year.

LINKS TO MENENDEZ

Melgen first made headlines in early 2013 after revelations that Menendez, D-N.J., had used the doctor’s personal jet. Menendez was forced to repeatedly deny reports that he flew on Melgen’s plane for trysts with prostitutes. None of the allegations was substantiated. But Menendez’s relationship with Melgen prompted Senate Ethics Committee and Justice Department investigations.

Menendez has acknowledged at least three areas where his official work appeared to serve the interests of Melgen, though he has steadfastly denied wrongdoing.

A staffer in Menendez’s office once sent emails to the Homeland Security Department questioning potential U.S. donations of cargo-screening equipment to the Dominican government. At the time, donated screening equipment could have jeopardized a lucrative port security contract for a company owned by Melgen.

Menendez sponsored legislation with incentives for natural gas vehicle conversions that could have benefited a company that Melgen was invested in called Gaseous Fuel Systems Corp. of Weston, Florida.

And Menendez acknowledged his office twice contacted Centers for Medicare & Medicaid Services, each time urging them to change what he called an unfair payment policy that had cost his friend Melgen $8.9 million.

KEEPING A LOW PROFILE

Melgen hasn’t made a splash at any recent, large-scale political events in South Florida and has seemingly ended his public appearances in the Dominican Republic. Reporters who routinely cover social events in the Caribbean country say they have not seen him at any activities in the past year. Before that, Melgen was known for regularly granting interviews to Dominican media in Florida, especially to talk about some of his successful operations.

Melgen has close ties to a prominent political family in the Dominican Republic. His cousin, Pelegrin Castillo Seman, has been a legislator since the 1990s, and Castillo’s father, Vinicio Castillo, is the director of the government’s Ethics Commission.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)