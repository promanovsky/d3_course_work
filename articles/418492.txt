



Disney/US Patent and Trademark Office

What could be more magical than Disney fireworks, light shows and multifarious water fountains? These things plus high-tech entertainment with drones.

Walt Disney Co. has filed three patent applications that could let it provide complex aerial entertainment shows with the support of drones. The applications involve floating projection screens, marionettes supported by drones, and a synchronized aerial light display with "floating pixels."

"In the entertainment industry, there are many applications where it is desirable to provide an aerial display," Disney wrote in the patent applications. "For example, an amusement park may have a lagoon or other open space over which it is desired to present a display to entertain visitors. In another example, massively large aerial displays may be presented at sport stadiums or other venues to celebrate holidays such as New Year's Day."

Disney wrote that without the use of drones, aerial shows are challenging. Additionally, there's a limit to how fantastic the company can get with simple fireworks, light shows, and water fountains.

If Disney could create a massive marionette and use drones to pull its strings as it ambles through the sky -- that would certainly wow onlookers. Or, what about a sky-born display screen where people could watch Mickey Mouse, Winnie the Pooh and Cinderella dance around?

When drones enter the picture, it appears the creative options are endless.

(Via Stitch Kingdom)