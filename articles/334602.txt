PITTSBURGH (AP) -- Paint and coatings maker PPG Industries is buying Consorcio Comex SA de CV for $2.3 billion to help bolster its architectural coatings presence in Mexico and Central America.

Comex makes coatings and related products in Mexico and sells them in Mexico and Central America. Pittsburgh-based PPG makes coatings, specialty materials and glass products.

Privately held Comex, based in Mexico City, had 2013 sales of about $1 billion. Its brands include Effex, Texturi and its namesake. Comex has eight manufacturing plants and six distribution centers.

PPG plans to fund the transaction mostly with available cash and short-term investments, but may fund part of the acquisition with the addition of debt.

The deal is expected to immediately add to PPG's earnings, excluding one-time acquisition-related costs.

The transaction may take four to six months to complete.

Shares of Pittsburgh-based PPG Industries rose $5.95, or 2.9%, to $210 in premarket trading Monday.