LOS ANGELES (CBSLA.com) — A Southland teenager has won a years-long fight for back-up cameras in vehicles after a car accident at just 14 months old changed his life.

USC student Patrick Ivison, now 19, nearly died when he was walking with his mom behind a car that suddenly backed up, pinning him underneath.

He says he survived to advocate for others.

“I’m definitely here for a reason,” he said.

He is now a quadriplegic and has testified in front of the National Highway Traffic Safety Administration, pushing for rear-view visibility systems in all vehicles.

The NHTSA announced Monday all vehicles under 10,000 pounds will be required to have back-up cameras if manufactured after May 1, 2018.

According to the ruling, the field of view must include a 10-foot by 20-foot zone directly behind the vehicle. The system must also meet other requirements including image size, linger time, response time, durability and deactivation.

“Safety is our highest priority, and we are committed to protecting the most vulnerable victims of back-over accidents — our children and seniors,” U.S. Transportation Secretary Anthony Foxx said of the ruling.

“As a father, I can only imagine how heart wrenching these types of accidents can be for families, but we hope that today’s rule will serve as a significant step toward reducing these tragic accidents.”

After years of fighting, Patrick says he knows this new law will save countless lives.

“Every car that gets one of these cameras is a car that’s not going to run someone over.

“I’m a rarity. In most of these cases kids die, which is unacceptable,” he said.

According to the NHTSA, there are 210 fatalities and 15,000 injuries per year caused by back-over crashes on average.

The agency has found that children under 5 years old account for 31 percent of back-over fatalities each year, and adults 70 years of age and older account for 26 percent.

“Rear visibility requirements will save lives, and will save many families from the heartache suffered after these tragic incidents occur,” NHTSA Acting Administrator David Friedman said.

“We’re already recommending this kind of life-saving technology through our NCAP program and encouraging consumers to consider it when buying cars today.”

Including vehicles that already have systems installed, 58 to 69 lives are expected to be saved each year once the entire on-road vehicle fleet is equipped with rear visibility systems meeting the requirements of Monday’s final ruling.

Additional safety tips are available via the NHTSA’s recommended website, SaferCar.gov.