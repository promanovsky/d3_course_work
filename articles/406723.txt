In a case closely watched in the United States and overseas, a federal judge in New York held Thursday that Microsoft must comply with a U.S. search warrant to turn over a customer’s e-mails held in a server overseas.

Judge Loretta Preska — in a surprise ruling from the bench — upheld a magistrate judge’s opinion in December ordering the Redmond, Wash., company to allow federal authorities to obtain copies of the data, which is stored in Ireland. The government is seeking the e-mails in connection with a drug-trafficking probe.

Preska agreed to stay her ruling to allow the company time to appeal. Microsoft immediately announced its intention to do so.

“The only issue that was certain this morning was that the District Court’s decision would not represent the final step in this process,” said Brad Smith, Microsoft’s general counsel, in a statement. “We will appeal promptly and continue to advocate that people’s e-mail deserves strong privacy protection in the U.S. and around the world.”

The case — the first of its kind in the United States — is a test of whether the government can assert a right to digital content wherever in the world it is stored. If that view is upheld, Microsoft and privacy advocates argue, it would set a dangerous precedent that other governments might be encouraged to emulate and would have grave implications for customers’ privacy.

“The ruling could lead to chaos, where other governments demanding reciprocal treatment insist that their warrants compel U.S. providers to turn over content that they store in the United States,” said Gregory Nojeim, senior counsel for the Center for Democracy and Technology, which supports Microsoft’s position.

The court’s ruling is also likely to exacerbate tensions between U.S. tech companies and the government created in the wake of disclosures by former National Security Agency contractor Edward Snowden over U.S. surveillance, analysts said.

“We’re extremely disappointed with today’s U.S. District court decision in favor of the U.S. government’s extraterritorial search warrant,” said Wayne Watts, AT&T general counsel. “We will strongly support Microsoft’s pursuit of a stay and subsequently a successful appeal of this decision.”

A Justice Department spokesman had no comment.

The case raises significant policy issues on several fronts. The government has an interest in gaining access to communications stored abroad during criminal investigations without having to rely on the cooperation of foreign law enforcement.

But allowing the government to reach across borders for evidence probably will harm U.S. tech companies that store data overseas, as foreign customers may flee to non-U.S. competitors. Allowing prosecutors to do so without the knowledge of the foreign government could harm diplomatic relations.

Foreign cloud providers will now have even more ammunition to say that foreign citizens’ and businesses’ data are not safe when stored with a U.S. company because not only can the NSA get it, but U.S. law enforcement — federal, state, and local — can, too, analysts said.

“How to balance these conflicting considerations is a quintessential policy decision that should be made by Congress, not by a prosecutor or a court in Manhattan,” said Michael Vatis, a partner at Steptoe & Johnson who co-

authored a friend-of-the-court brief for Verizon, which operates data centers overseas.

The judge’s ruling probably will prompt more expressions of outrage from foreign officials, especially in the European Union, about the potential for intrusion into their sovereignty. On June 24, the European Commissioner for Justice, Viviane Reding, voiced concern that the magistrate judge’s opinion in the case “may be in breach of international law” and “may impede” European citizens’ privacy.

The government argues that the e-mails are business records that can be obtained wherever in the world they are. Microsoft attorneys argued that e-mails are the customer’s property, “saturated with the highest constitutional privacy rights.”

During two hours of lively argument, the judge “said she saw no difference between the companies’ own records and a customer’s e-mail,” Vatis said. The judge also said she endorsed the lower court’s ruling which held that Microsoft could simply copy the e-mails from the United States.

The law underlying the case is the 1986 Electronic Communications Privacy Act, which is silent on whether a search warrant for digital content applies abroad.

But Preska said that the law implicitly authorizes extraterritorial collection, said the attorneys. She cited a 1984 case that held that a court may require a company to disclose its business records no matter where in the world they are, and that the disclosure did not require the consent of the country in which they are stored. She said that Congress was aware of the 1984 case when it passed ECPA, and so the law implicitly authorized the overseas reach.

Others disagree.

“If Congress wants to permit the government to obtain electronic communications stored abroad using a search warrant, it should amend the law to say so,” Vatis said.

The issue is gaining global currency. Two weeks ago, Britain passed a law that permits the authorities there to require companies outside the country to provide communications in response to a wiretap order.