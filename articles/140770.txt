When both a Christian and a Pagan festival coincide, it can make for a very confusing story.

This is certainly the case with Easter, which I've struggled to understand since I was a kid. Knowing the difference between historical fact, ancient mythology and religious symbolism isn't easy when you're a child with a very active and curious mind, especially when you believe everything the grown-ups tell you, like I did.

Let's look at the facts: at some point between Saturday evening and Sunday morning, the Easter bunnies appear in your garden and hide chocolate eggs in every imaginable nook and cranny.

No-one has ever actually seen them do it, so how many of them there are and how they actually carry them or place them there isn't known or understood. Why they do it is an even greater mystery.

One theory (of mine) was that they worked in pairs. One bunny wore some kind of basket arrangement attached to its back containing the said chocolate eggs while its partner bunny nudged them out of the basket and then somehow managed to toss them on to the branches of trees or hide them inside upturned plant pots, possibly using its tail and/or hind legs to propel them.

Of course, in order for them to cover such a large area in a short time and cater for so many children at once meant there must have been an Easter Bunny HQ somewhere where they were trained (possibly by Santa) in covert undercover tactics ... errr ... including the ability to scale walls, open gates and climb trees.

Of course, this theory raised as many questions as it answered. For example, where did they get the eggs from and who paid? Why did the stickers on them say Woolworths? And why would rabbits (mammals that give birth to live offspring) be dealing with eggs in the first place? And why go to all this trouble when Easter eggs had been in the shops since January and anyone could go and lift one very simply down from the shelf?

Meanwhile, days before the actual 'drop', we had been preparing for the celebrations by going to church almost every day for a week -- called Holy Week -- to hear a series of stories about Jesus.

Forgive me if this sounds irreverent, but I'm simply recalling the confusion I felt as a child, and how it all got a bit jumbled up in my mind.

On one of the days we were given palm leaves and had to wave them in the air while singing hymns to remember Jesus riding into town on a donkey. On another we had our feet washed by the priest while the Queen gives out money to the poor, but only on telly. On another we did something called the Stations of the Cross, where we stopped at each 'station' (even though they didn't have trains in those days) to repeat prayers over and over again while the priests shook smoke into the air from a golden ball.

The terrible story about Jesus being crucified by a pilot (even though there weren't any planes in those days) was called the "passion" -- even though I thought that meant something you really enjoyed -- and the day he was actually killed was called Good Friday -- even though I thought that meant something nice. Also, on that day we weren't allowed to eat any meat and just had sandwiches for tea instead of a proper meal.

On the upside, though, was that while all this was going on, the Easter bunnies had been.

Before you could have the eggs, though, you had to go to church again. Only then could we search the garden.

Upon finding the eggs you weren't allowed to eat them until you had rolled them across the garden, because this was to remind ourselves of Jesus rolling away the stone from his tomb when he rose from the dead ... errr ... before he went up to heaven to become one third of the Holy Trinity, which is one person made up of three people including a holy ghost.

Once we had done that and virtually gorged ourselves on chocolate we then had a big dinner of roast lamb (I think it was called the Lamb of God or something like that) and then watched the Sound of Music all afternoon followed by Songs of Praise and then Watership Down.

You see, it's quite self-explanatory when you break it down like that, isn't it?