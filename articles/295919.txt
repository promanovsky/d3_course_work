HBO

Yesterday saw the airing of the final episode for season four of HBO's "Game of Thrones". According to data from the official US ratings, "The Children", as the episode was titled, was the most watched GoT episode to date, hitting 7.1 million viewers.

Continuing the ongoing trend, it was also popular with pirates, breaking the series' own torrenting records.

According to TorrentFreak -- almost the unofficial industry body for this sort of information -- the episode also saw "roughly" 1.5 million downloads in the first 12 hours after airing. That equates to around 2 petabytes of data. (For some scale on just how big a petabyte is, Deloitte Analytics offers this: it would take 233,000 DVDs to store a single PB.)

The episode also broke the record for the largest "swarm" -- the number of people sharing the same file. Back in May, 207,054 people shared the same torrent file for the episode "First of his Name". This time round, "The Children" managed a total of 254,114 sharers.

Australia has always been one of, if not the, top torrenter of GoT and this season saw many Australians putting the blame on pay TV service Foxtel's exclusive deal for the show. A similar deal exists in the UK, with Sky's Now TV bagging exclusive streaming rights -- and making a shoddy job of showing it. It's only now that the full season is over that other video services and broadcasters are able to offer the season for download or streaming.

Foxtel unsurprisingly reacted angrily to the claims, telling CNET unequivocally that " piracy is theft" and noting that it had special pricing deals in place for GoT to make it more accessible. HBO, which makes the show, is apparently more sanguine, with programming boss Michael Lombardo quoted last year as saying that illegal downloads are " a compliment of sorts".

Now that the final episode has aired, the full season can be bought in HD from Google Play for AU$35 -- £18.49 in the UK -- and from Australian streaming service Quickflix at AU$34 to name just two. It has yet to be made available on iTunes for the Australian audience -- a fact that some commentators suggest shows a dissatisfaction with the Foxtel exclusivity.