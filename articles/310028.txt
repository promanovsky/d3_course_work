SAN FRANCISCO (MarketWatch) — Oil futures settled lower for a second straight session, but kept their grip on the $106-a-barrel on Tuesday, finding some support as upbeat U.S. economic data raised the prospects for energy demand.

Traders continued to keep a close watch on developments in Iraq for any potential disruptions to global supplies and awaited weekly data on U.S. petroleum supplies.

West Texas Intermediate crude oil for August delivery CLQ24, -2.05% fell 14 cents, or 0.1%, to settle at $106.03 a barrel on the New York Mercantile Exchange, while August Brent UK:LCOQ4, the European benchmark, climbed 34 cents, or 0.3%, to end at $114.46 a barrel on the ICE Futures exchange.

Part of the pull back in Nymex oil prices late in the trading session was likely due to positioning ahead of a U.S. government report on supplies, “as last week’s inventory data was rather bearish,” said Tyler Richey, an analyst for the 7:00’s Report, which offers daily markets commentary.

The American Petroleum Institute was set to release a report on petroleum supplies late Tuesday, while the Energy Information Administration’s data were due out Wednesday.

Analysts surveyed by Platts expect the reports to show a drawdown of 2 million barrels in crude supplies for the week ended June 20. They are also looking for a climb of 2 million barrels in gasoline stockpiles and a rise of 1 million barrels for distillate inventories, which include heating oil.

July gasoline US:RBN4, the front-month contract, rose nearly 2 cents, or 0.6%, to $3.13 a gallon, while July heating oil US:HON4 ended at $3.04 a gallon, up a cent, or 0.3%.

Iraq and demand prospects

For now, WTI crude is “continuing to trade comfortably above former resistance (which is now support) at $105,” Richey said. “There is no question that Iraq was the reason behind the break-out and the conflict continues to help support prices.”

“At the end of the day, the U.S. economy is growing and that is the primary driver of the energy markets over the longer term, as more and more people are entering the work force ... and are driving to work,” he said.

U.S. data released Tuesday showed that consumers are the most confident they’ve been since early in the recession, with a gauge of consumer confidence rising to 85.2 in June, the highest since January 2008. Sales of new homes also surged nearly 19% in May to the highest annual level in six years.

Still, analysts said signs that violence has swept across northern Iraq as government forces battle Sunni insurgents hasn’t yet affected the country’s oil exports. That was helping to keep a lid on prices after both WTI and Brent crude benchmarks last week hit their highest levels in nine months. Oil futures retreated on Monday.

Iraqi Prime Minister Nouri al-Maliki Shutterstock/360b

“If there is confirmation that ISIS extremists will not be able to disrupt the oil supply, prices are likely to fall further. The ongoing uncertainty means that no sharp price slide is likely, however. In the next few weeks, we expect to see Brent trading at above $110 per barrel,” said analysts at Commerzbank in Frankfurt, in a note.

Abdulah Al-Badry, the secretary-general of the Organization of the Petroleum Exporting Countries, or OPEC, on Tuesday said Iraq is still “producing as normal,” with 95% of its capacity in the south unaffected by the violence, the Associated Press reported.

ISIS takes control of Iraq's largest oil refinery

Developments in Ukraine might also put downward pressure on energy prices, analysts said. Russian President Vladimir Putin on Tuesday asked parliament to rescind his authorization to use military force in Ukraine, a day after pro-Russian separatists in eastern Ukraine agreed to join a government-declared cease-fire until the end of the week. Putin’s move was welcomed by Ukraine President Petro Poroshenko.

Rounding out the energy action on Nymex, prices for natural gas rebounded after suffering from declines over the past four trading sessions in a row.

July natural gas US:NGN14 climbed 9 cents, or 2%, to settle at $4.535 per million British thermal units.

Recent weakness for the natural gas came as a “result of the last 6 consecutive EIA reports showing builds of 100 [billion cubic feet] or more, easing some concerns over whether or not inventories would be restored to levels needed to comfortably enter the draw season this fall,” said the 7:00’s Report’s Richey. “Also, revisions to extended weather forecasts that suggests temperatures will be milder than initially expected are lowering energy demand expectations for the near term.”

More must-reads from MarketWatch:

This is what needs to happen for gold to rally

Are we setting up for a 2011-type crash?

Goldman Sachs: 15 cheap stocks for an expensive market