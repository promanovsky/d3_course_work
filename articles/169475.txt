By: CBS News

There were a lot of big names receiving Tony Award nominations on Tuesday morning -- ranging from Bryan Cranston to Neil Patrick Harris. They're just two of the nominees in the running for honors on Broadway's biggest night coming up on June 8.

A Gentleman's Guide to Love and Murder leads the pack with 10 nominations, followed by Hedwig and the Angry Inch with eight nods. After Midnight, Beautiful - The Carole King Musical, The Glass Menagerie and Twelfth Night all received seven nominations each.

Continue reading...