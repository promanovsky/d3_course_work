Scientists have found the mechanism used by fruit flies to regulate their flight speed from their antennae by using both vision and wind-sensing information.

The researchers traced the flies' flight trajectories in a wind tunnel and found that the wind-sensing antennae stabilize the fly's visual flight controller.

The results showed that in gusts, air drag causes part of the deceleration, but in addition, antennae sense airspeed changes and induce a response that causes the fly to decelerate even further.

The researchers found that the flies accelerated when the wind was pushing them from behind and decelerated when flying into a headwind and eventually recovered to maintain their original groundspeed.

Array

The study published online in Proceedings of the National Academy of Sciences.