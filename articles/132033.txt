NEW YORK: India-born former Goldman Sachs director Rajat Gupta , who had lost his appeal to overturn his conviction on insider trading charges , has been ordered to surrender and begin his two-year prison sentence in June.

“Upon consent of the parties, the court hereby orders the defendant to surrender for service of his sentence at the institution designated by the Bureau of Prisons not later than 2 pm on June 17, 2014,” US district judge Jed Rakoff said in his order.

On March 25, a US court upheld the 2012 conviction of 65-year-old Gupta on insider trading charges for supplying confidential information to jailed hedge fund boss Raj Rajaratnam. The Harvard-educated Gupta had suffered a major setback a district denied his bid for a new trial, ruling that there was no merit in Gupta’s appeal.

Gupta was sentenced to two years’ imprisonment, to be followed by a one-year term of supervised release, and was ordered to pay a fine of $5,000,000. He had been free on bail pending decision on his appeal.

The prosecution had presented as evidence telephone records that showed that calls were made to Rajaratnam’s direct line from the conference room telephones where Gupta had participated in Goldman Sachs board meeting. The connection between Rajaratnam’s line and the telephone Gupta used lasted approximately 30 to 35 seconds. Gupta had challenged his conviction, contending that he is entitled to a new trial on the grounds that the trial court erred by admitting statements of a co-conspirator, recorded in wiretapped telephone conversations to which Gupta was not a party.