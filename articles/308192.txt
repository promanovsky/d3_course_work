Oracle Corp. agreed to buy Micros Systems Inc. in the tech giant’s biggest acquisition since 2010.

The transaction is valued at about $5.3 billion including items such as options, and $4.6 billion net of Micros’s cash. Oracle is offering $68 a share for the Columbia, Md., company, which sells software used by retailers and hospitality providers.

That is a 3.4% premium to the $65.77 that Micros shares closed at Friday after surging earlier in the week when Bloomberg reported the two sides were in exclusive talks to combine.

Micros sells Internet-connected cash registers, and the software and technical services to power them, to restaurants, retail shops, casinos and other companies.

Analysts have said a purchase of Micros US:MCRS , which long has been a rumored acquisition target for Oracle ORCL, +0.61% , would be a sign the database giant is interested in grabbing a bigger foothold in the retail and hospitality industries. FBR Capital Markets said in a recent research note that Oracle, which has $39 billion in cash and marketable securities and a $190 billion market capitalization, needs to “go on the deal warpath” to expand into emerging areas of corporate technology where the company doesn’t currently generate much revenue.

Oracle’s purchase of Micros is one of the largest tech takeovers so far this year, and the biggest for Oracle, a serial acquirer, since its $7.4 billion acquisition of Sun Microsystems Inc. in 2010.

An expanded version of this report appears at WSJ.com.

More must-reads from MarketWatch:

Are investors misreading Fed chief Janet Yellen?

5 industries that millenials are destroying

How to cut your summer energy bills