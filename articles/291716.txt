Amazon may not have a new smartphone yet, but it already seems to be making a not-so-subtle pitch for developers to build apps for one.

The ecommerce giant announced Monday that its Appstore for Android devices and the Kindle Fire now has more than 240,000 apps, nearly triple the number one year earlier. That still makes the Amazon app ecosystem significantly smaller than Apple or Google, both of which now have more than one million apps across their respective devices, but the growing number of apps is only part of the pitch.

See also: Why an Amazon Phone Makes Sense

“Developers tell us that they experience improved reach, greater monetization, and, oftentimes, higher revenue when they have their apps and games in the Amazon Appstore,” Mike George, vice president of Amazon Appstore and Games, said in a statement.

To back that up, Amazon commissioned an IDC survey of tablet and smartphone developers, which provided — surprise, surprise — a glowing picture of what it's like to develop apps for Amazon. The majority of developers surveyed (65%) said total revenue for Kindle Fire is "the same or better" than on other platforms and nearly three-quarters of those surveyed said average revenue per app/user was "the same or better" on Kindle Fire.

The message Amazon hopes to send to developers is clear: You can do quite well on our platform.

Amazon is widely expected to unveil a smartphone at an event on Wednesday. The device is said to feature 3D capabilities, face-tracking technology and, perhaps, a better selection of apps than some might expect.