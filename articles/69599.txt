Neil Patrick Harris and Jason Segel are very multi-talented.

On Thursday (March 27th) the stars of CBS's 'How I Met Your Mother' appeared on 'Inside The Actors Studio' to talk about the highly anticipated season finale, and the two actor's decided to treat the crowd to something extra.



The cast of 'HIMYM' appeared on 'Inside The Actors Studio' on Thursday

In 2006, while appearing on the 'Megan Mullally Show', the two co-stars revealed that they sometimes like to perform a duet of the 'Les Miserbales' song 'Confrontation' on set.

A member of the crowd must have seen this as he requested that Neil and Jason perform it for them, as a nod to the famed musical returning to Broadway.

"Since Les Mis is coming back to Broadway," the fan said. "I thought maybe Jason and Neil might give us a little bit of the Confrontation."

This was met with a wild roar from the crowd who began to clap as the co-stars glanced over at each other.

They both willingly threw themselves into the duet, with Neil taking the role of the inspecter and Jason as Jean Valjean, while other members of the cast began to laugh around them.

The audience gave them a standing ovation.

Segel has also performed the same duet with his 'Man, I Love You' co-star Paul Rudd back in 2009 when being interviewed by ABC.

Watch the 'HIMYM' co-stars perform the duet below



MORE: Has 'How I Met Your Mother' Lulled Us Into a False Sense of Security?



The very last episode of 'How I Met Your Mother' airs this Monday on CBS. Fans of the sitcom, which follows Ted Mosby's journey to how he met his children's mother, were finally introduced to her as actress Cristin Milioti at the end of the previous season and as the finale is only days away, the show will finally reveal what happened between them.

MORE: Is 'How I Met Your Mother' Planning a 'Ross and Rachel' Ending?