Earlier today we saw a new trailer for Game of Thrones Season 4, and now Sky TV in the UK has announced that the first three seasons of Game of Thrones are available to watch on their Now TV and also Sky On Demand.

The fourth season of Game of Thrones will air in the UK on the 7th of April on Sky Atlantic, this is 24 hours after it launches in the US.

Julia Barry, Director, Sky Atlantic HD, said: “For anyone who hasn’t yet experienced the GAME OF THRONES phenomenon, now there is an easy way from Sky to catch up on every episode of one of the most talked about TV shows in the UK. The new season is easily one of the most eagerly anticipated TV events of the year, which we’re delighted to offer exclusively on Sky Atlantic HD, less than 24 hours after the US broadcast.”

The first three seasons are now available to existing Sky on demand and Sky Now TV customers, Now TV users will need a subscription to the Entertainment Pass which costs £4.99 a month.

Source Sky

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more