Today's release of the Intergovernmental Panel on Climate Change (IPCC)’s final working report is a landmark moment. It highlights the case that there remains limited time to reduce global warming of some 2 degrees Celsius (3.6 degrees Fahrenheit) above pre-industrial levels — the level scientists say we must not breach if we are to avoid the worst risks of climate change.

However, to seize this limited window of opportunity, urgent action is now required by all major countries — in both the developed and developing world.

At a time when the debate around global warming is becoming increasingly polarised, this solutions-orientated message is exactly the right one to rally action in the lead-up to the November 2015 Paris Summit when a new global climate treaty will be agreed. It is clear that, for a comprehensive, ambitious deal to be reached at Paris, the underpinnings must be based upon more countries taking action at a national level.

Here there is room for encouragement and optimism, although we cannot be complacent. For in contrast to the slow pace of international negotiations to combat climate change, national legislation is advancing at a startling rate, a surprise to those who ascribe to the conventional wisdom that progress has waned.

Remarkably, some 450 climate-related laws since 1997 have been passed in 66 countries covering around 88% of global greenhouse gases released by human activities. This legislative momentum is happening across all continents. Encouragingly, this progress is being led by the big emerging and developing countries, such as China and Mexico, that together will represent 8 billion of the projected 9 billion people on Earth in 2050.

These are the key findings of the 4th edition of the GLOBE Climate Legislation Study, released in February, the only compendium of climate legislative action created by legislators from around the world, and the most comprehensive audit yet of the extent and breadth of the emerging legislative response to climate change.

While optimistic, we must also be honest. These laws are not yet enough to limit global average temperature rise to 2 degrees Celsius above pre-industrial levels. Yet these actions are putting into place the legal frameworks necessary to measure, report, verify and manage greenhouse gas emissions — the cause of man-made climate change.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Part of the reason for this spectacular wave of progress is changing attitudes. Previously, the debate on climate change was framed by a narrative about sharing a global burden — with governments naturally trying to minimise their share. Now, however, countries are seeing mitigating climate change — through clean energy and energy efficiency solutions — and strengthening resilience to its impacts, as being firmly in the national interest.

For example, 61 of the 66 countries in the GLOBE study have passed laws to promote domestic, clean sources of energy and 54 have legislated to increase energy efficiency. The former reduces reliance on imported fossil fuels, thereby mitigating exposure to volatile fossil fuel prices, increasing energy security and reducing energy poverty. The latter reduces costs and increases competitiveness.

And it's no surprise, too, that 52 out of the 66 countries covered by the study have developed legislation to improve their resilience to the impacts of climate change, some of which we are already experiencing.

As the formal U.N. negotiations move towards Paris, the scheduled conclusion of negotiations on a post-2020 framework, this legislation is creating a strong foundation on which a post-2020 global agreement can be built.

Moreover, it is increasingly clear that not only is the agreement in Paris dependent on national legislation in place in advance; implementation of the Paris agreement will only be effective through national laws, overseen by well-informed legislators from all sides of the political spectrum.

A national “commitment” or “contribution” put forward at the U.N. will only be credible — and durable beyond the next election — if it is backed up by national legislation, supported by cross-party legislators that put in place a credible set of policies and measures to ensure effective implementation.

That is the reason legislators must be at the centre of international negotiations and policy processes, not just on climate change, but also on the full range of sustainable development issues. And it is why, on climate change, governments must immediately prioritise supporting the implementation of national legislation.

A comprehensive climate change law is expected to pass in Costa Rica this year, and members in China, Colombia, Democratic Republic of Congo, Nigeria and Peru, amongst others, are developing legislation now.

However, we need to do much more. And that is the reason, in collaboration with the World Bank and the United Nations, GLOBE has launched the Partnership for Climate Legislation to promote the advance of climate-related laws.

Of course, the role of legislators does not end when legislation is passed - legislators must also implement it, and hold their governments to account. This is crucial if the agreement made in Paris is to deliver.

Success in Paris to create a climate agreement, the follow-through to implement the accord, and the fate of our planet depend on our actions.