University life is a tough game. Amid the deadlines, endless requirements, and social responsibilities, every student is forced to do whatever it takes to ace examinations and extra-curricular activities in school. Even if that means using a stimulant drug known to treat attention-deficit/hyperactivity disorder (ADHD).

In a paper presented Saturday at the annual Pediatric Academic Societies (PAS) convention in Vancouver, Canada, researchers reported that one in five of the surveyed students in Ivy League College admitted using ADHD drugs as a "study drug" during college to perform better academically despite some of them considering it as cheating.

In the 616 sophomores, juniors and seniors without ADHD who were asked to complete an anonymous survey, it was the varsity athletes and members of Greek house that were revealed to more likely abuse these stimulants and most of them are juniors (24 percent).

Data also showed that 18 percent confirmed that they used a "prescription stimulant" solely for academic reasons at least once during college, while 24 percent admitted using the ADHD drug more than eight times for the same purpose.

Those who misused the ADHD stimulants to write an essay recorded 69 percent. Students who merely used the drug for an exam or when they were to take a test registered 66 percent and 27 percent, respectively.

"While many colleges address alcohol and illicit drug abuse in their health and wellness campaigns, most have not addressed prescription stimulant misuse for academic purposes," said Andrew Adesman, M.D., FAAP, senior investigator and chief of developmental and behavioral pediatrics at Steven & Alexandra Cohen Children's Medical Center of New York. "Because many students are misusing prescription stimulants for academic, not recreational purposes, colleges must develop specific programs to address this issue."

This rings true to the findings that showed students who abused stimulants viewed it as a typical going-on in the campus. In fact, 37 percent of the students thought that there are more students who have done the same, compared to the modest 14 percent of students had not used an ADHD drug.

ADHD drugs such as Ritalin are methylphenidates, medicines used to increase dopamine levels produced by the brain. Dopamine is the chemical responsible for the feeling of euphoria and extreme happiness, hence the "being on high" state.

However, experts have warned that use of these medicines may trigger side effects such as nervousness, insomnia, loss of appetite, palpitations, headache, among others.

How do students obtain ADHD medications even though they do not have such illness? High school students, not only those in college years, feign symptoms of ADHD just to get a prescription, Adesman revealed.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.