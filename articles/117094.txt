US consumer prices rose slightly more than expected in March, suggesting a disinflationary trend had run its course.

While the increase last month should allay concerns among some US Federal Reserve officials that inflation was too tame, price pressures remain subdued enough for the US central bank to keep interest rates low for a while.

The country’s Labor Department said its Consumer Price Index increased 0.2% in March, as a rise in food and shelter costs offset a decline in gasoline prices.

The CPI index had gained 0.1% in February.

Economists polled by Reuters had expected a 0.1% rise last month. In the 12 months through March, consumer prices increased 1.5% after rising 1.1% over the 12 months through February.

The so-called core CPI, which strips out the volatile energy and food components, also rose 0.2% in March after edging up 0.1% the prior month.

In the 12 months through March, the core CPI advanced 1.7% after rising 1.6% in February.

US stock index futures pared gains after the report, while the dollar hit session highs against the euro and the yen.

The Fed targets 2% inflation and it tracks an index that is running even lower than the CPI.

The central bank is expected later this year to end the monthly bond purchases it has been making as part of its massive stimulus programme.

While US demand is picking up and the labour market is tightening a bit, most economists do not expect the first interest rate hike before the second half of 2015.

The Fed has kept benchmark overnight interest rates near zero since December 2008.

Last month, food prices increased 0.4% after rising by the same margin in February.

A drought in the western United States has pushed up prices for meat, dairy, fruit and vegetables.

More price increases could be on the way after food prices at the factory gate posted their biggest gain in ten months in March.

Gasoline prices fell 1.7%, declining for a third straight month.

Within the core CPI, shelter costs increased 0.3%, which accounted for almost two-thirds of the rise in the index. Rents increased 0.3%.

There were also increases in medical care, apparel, used cars and trucks, airline fares and tobacco. The cost of recreation, and household furnishings fell.