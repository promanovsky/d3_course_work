On July 16, 1969, Apollo 11 launched atop the Saturn V rocket from the Kennedy Space Center to put man on the moon. Forth-five years ago, three legendary astronauts, Neil Armstrong, Buzz Aldrin and Mike Collins, were 114,000 miles from Earth at 9:00 A.M. ET. The mission was successful, but the ride was not all that comfortable.

Apollo 11 astronauts landed on the moon on July 20, 1969

During an interview with CBS News in 2005, Neil Armstrong said it felt like a train running on a bad railroad track. It was really loud. But the astronauts enjoyed the journey. as they moved deeper into the space, they relaxed and listened to music. As Apollo 11 reached the lunar orbit, Neil Armstrong and Buzz Aldrin moved into a lunar module to land in the Sea of Tranquility.

Armstrong became the first person to land on the moon on July 20, 1969. As people on Earth watched him live on TV, he spoke the now-famous line, “One small step for man; one giant leap for mankind.” Aldrin soon followed him on the surface of the moon. They spent about 21 hours on the lunar surface collecting various specimens. Mike Collins piloted the spacecraft alone until the other two rejoined him.

Apollo 11 fulfilled the dream of JFK

The mission was a success, and the astronauts landed safely on the Earth. It fulfilled the dream of former President John F. Kennedy who vowed in 1961 to put a man on the moon and bring him safely back by 1970. It also put an end to the space race with the USSR (now Russia). Neil Armstrong died on August 25, 2012.

Buzz Aldrin, 84, and Mike Collins, 83, are pushing the U.S. to launch a mission to send humans to Mars. Earlier this week, Aldrin told The Washington Post that the U.S. will have to forge international partnerships to get to the red planet. Meanwhile, billionaire Elon Musk’s SpaceX is also working on a Mars colonial transporter. But Aldrin thinks it will be too expensive even for a billionaire.