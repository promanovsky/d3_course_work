After the coffee. Before discussing last night’s “The Good Wife” shocker.



The Skinny: I’m so glad I stayed off Twitter and other social media last night until after 10 p.m. I would have been totally bummed if the end of “The Good Wife” was ruined for me. I don’t think anyone saw that coming. Kudos to the show for keeping it under wraps. No small feat these days. Monday’s headlines include the weekend box office recap, Comcast and Apple talk streaming services and a look at the calls for greater safety on movie sets after the tragic death of Sarah Jones.

Daily Dose: Not everyone is giving SportsNet LA, the new Dodgers-owned channel distributed by Time Warner Cable, the cold shoulder. Though none of the area’s major distributors, including DirecTV, Dish and FiOS, are carrying SportsNet LA, Champion Broadband, which serves Monrovia and Arcadia and has 5,000 subscribers, has struck a deal to distribute the service. So if you there is a sudden increase in the number of people moving there now you know why.

No diversions for “Divergent.” While I was seeing the latest “Muppet” movie Saturday, everyone else was at “Divergent,” which took in $56 million in its opening weekend. You know what that means? It means another sequel. “Muppets Most Wanted,” however, came in under expectations with a take of just $16.5 million. Probably didn’t help Kermit and the gang that “Mr. Peabody and Sherman” and “The Lego Movie” are still doing strong. Box office recaps from the Los Angeles Times and Hollywood Reporter.

Advertisement

PHOTOS: Upcoming Disney spring films and beyond

Biting the apple. Comcast is in early talks with Apple about working with the software giant on a streaming service that the cable giant would distribute over its broadband platform, reports the Wall Street Journal. There are many hurdles before any agreement could come to fruition including, of course, cost and control. Another rub may be Apple’s desire to get preferential treatment on Comcast’s broadband pipes. Interesting timing on this story given the concerns about Comcast’s recently announced deal to acquire Time Warner Cable.

Rallying cry. The tragic death of camera assistant Sarah Jones last month while filming a scene in the Gregg Allman biopic “Midnight Rambler” has become a rallying cry for improving safety in movie shoots. Jones was killed and six others injured when an unexpected freight train roared through the shoot. The Los Angeles Times and New York Times look at what happened and whether it will lead movie and television producers to toughen up their safety efforts.

Are Knicks tickets part of the deal. Madison Square Garden Co., parent of the New York Knicks, Radio City Music Hall and some cable networks, has struck a deal for 50% of Tribecca Enterprises, parent of the Tribecca Film Festival. Details from Variety.

Advertisement

Pulling the plug. ABC Family abandoned its plans to make a TV show called “Alice in Arabia” about an American teen who, according to log lines, is kidnapped by relatives and forced to live in captivity with her grandfather in Saudi Arabia. The project was blasted by Muslim advocacy groups for perpetuating stereotypes and was heavily criticized on social media sites. More from Buzzfeed and Entertainment Weekly.

Inside the Los Angeles Times: James Rebhorn, a character actor whose credits included “Scent of a Woman” and “Seinfeld,” died at age 65. Kate Winslet has a rare turn of playing the heavy in “Divergent.”

Don’t be selfish, follow me on Twitter. @JBFlint.