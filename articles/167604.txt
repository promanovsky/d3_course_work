We had recently reported that Apple is rumored to launch their updated MacBook Airs this week and according to a previous report, it seems that the launch could take place as soon as tomorrow. If the rumors are to be believed, the updates are said to be relatively minor and will only see slight processor upgrades, so if you were hoping for Retina displays and whatnot, you’d be out of luck.

Advertising

This has since been “confirmed” by the folks at 9to5Mac who managed to get their hands on a photo which apparently shows off the new processors that the updated MacBook Airs are supposed to get. Starting with the entry-level 13-inch model, this device is expected to see a 1.4GHz Intel Core i5 processor which is a slight bump from the clock speed of 1.3GHz in the previous version.

Unfortunately the other configurations were not shown off in the photo but presumably they will be given the same treatment as well and we can expect minor upgrades in terms of their clock speeds and processors. Other than that it looks like Apple did not touch the MacBook Air’s RAM, WiFi, Thunderbolt, and so on, so if you were hoping for a more dramatic upgrade, you might be disappointed.

In any case there’s no way of telling if any of these specs are indeed the real deal, but given that 9to5Mac has had some pretty reliable leaks in the past, we guess there might be some truth to their claims. Either way check back with us tomorrow if you’d like to find out more.

Filed in . Read more about Macbook Air.