Geneva: A total of 122 suspected and confirmed cases of Ebola, including 80 deaths, have been reported in Guinea, the World Health Organisation (WHO) said Tuesday.

“Although there was no infection in Liberia itself, the infected were Liberians who had travelled to Guinea and had been consequently infected.”

"There were seven suspected and confirmed cases of whom four had died," Xinhua quoted Gregory Hartl, spokesperson of WHO, as saying during the conference. "The Sierra Leone cases had been tested negative."

Asked about the scope of the outbreak, he stressed the current Ebola outbreak "was smaller than others in the past such as in Uganda and the Democratic Republic of the Congo in the 1990s."

The largest outbreaks had seen over 400 recorded cases.

Hartl said the deadly virus, with the fatality rate up to 90 percent, spread from the rural villages of the south east to the capital city of Conakry.

WHO had been alerting neighbouring countries of Guinea to heighten surveillance for illness consistent with a viral haemorrhagic fever, especially along land borders.