Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

We're testing a new site: This content is coming soon

Nasa have marked Earth Day by creating an amazing mosaic of the Earth from space - featuring thousands of selfies from across the globe.

The space program collected more than 36,000 pictures from volunteers across the web, who posted them on social media using the hashtag #globalselfie - and now they've released the finished product.

They created this 3.2 gigapixel image on Earth Day back on April 22nd in answer to the campaign's question - 'Where are you on Earth right now?'

A Nasa spokesperson said: "We asked people to answer the question on social media, with a selfie.

"The goal was to use each picture as a pixel in the creation of a 'Global Selfie' - a mosaic image that would look like Earth appeared from space on Earth Day."

Together, around 36,422 pictures make up the image, which allows users to zoom in on their face and scroll through galleries of other people who joined in.

Even a few celebrities got involved in the project, such as Family Guy voice actor Seth Green who joined in with a group selfie when he visited a Nasa laboratory.

It features people from every continent, with 113 countries represented including Antarctica, Yemen, Greenland and Micronesia.

The picture is hosted online by GigaPan, and was built from snapshots from Twitter, Instagram, Facebook, Google+ and Flickr.