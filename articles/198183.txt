WASHINGTON, May 8 (UPI) -- A group of large tech companies, including Google, Facebook and Amazon, have written a letter to the Federal Communications Commission expressing concern over proposed net neutrality rules.

The companies expressed their desire for a free and fair internet saying their success could be attributed to "a world without discrimination." They want users on fixed-line and mobile networks to be protected from Internet providers slowing or blocking content.

Advertisement

"According to recent news reports, the Commission intends to propose rules that would enable phone and cable Internet service providers to discriminate both technically and financially against Internet companies and to impose new tolls on them. If these reports are correct, this represents a grave threat to the Internet," the letter read.

The letter was signed by close to 150 technology-related companies, including Microsoft, Dropbox, Mozilla and dozens of other smaller companies.

RELATED Amazon adds 15 cities eligible for Sunday delivery services

The letter came as FCC Commissioners Mignon Clyburn and Jessica Rosenworcel said they had serious concerns with Chairman Tom Wheeler's proposed rules.

Wheeler needs the support of his fellow Democratic members on the commission, as the the two Republican members are opposed to enacting net neutrality rules.

Critics of Wheeler's proposal said he has gone back on promises to maintain equal treatment of all content by proposing a pay-for-priority arrangement between Internet providers and content companies, like Netflix.

Both Rosenworcel and Clyburn have asked Wheeler to postpone the May 15 vote by a month and allow the public more time to comment on the proposal.

"His proposal has unleashed a torrent of public response. Tens of thousands of emails, hundreds of calls, commentary all across the Internet," Rosenworcel said in a speech to a meeting of the Chief Officers of State Library Agencies in Washington. "We need to respect that input and we need time for that input," she said.

"Over 100,000 Americans and counting," Clyburn wrote in a blog post Wednesday. "I am listening to your voices as I approach this critical vote to preserve an ever-free and open Internet."

Wheeler has been attempting to fast-track the net neutrality issue after Verizon won a challenge against net neutrality regulations, on the grounds the FCC didn't have the authority to regulate internet service providers, with a federal appeals court in January sending the contested rules back to the FCC for updating. The FCC intends to vote on and adopt new rules by the end of this year.

RELATED Sony makes a tape that can store 185 TB