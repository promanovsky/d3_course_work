FedEx reported $11.8 billion in total revenue for the quarter ended in May.

The 15-count indictment, handed down by a federal grand jury in San Francisco, includes charges for conspiracy to distribute controlled substances. FedEx allegedly gained at least $820 million from the conspiracy, the filing said, and could be fined up to twice that amount.

FedEx was indicted on Thursday for shipping packages from illegal online pharmacies despite repeated warnings from U.S. drug enforcement officials, according to a court filing.

In a statement, FedEx Senior Vice President Patrick Fitzgerald said the company is innocent and will plead not guilty. U.S. prosecutors are asking that the company assume responsibility for the legality of millions of packages a day, he said.

"We are a transportation company. We are not law enforcement," he said.

Beginning in 2004, the indictment said, FedEx was warned "on no less than six different occasions" that illegal Internet pharmacies were using its services to distribute prescription drugs. Senior managers were among those who received warnings, the filing said.

In one instance, FedEx knew the Drug Enforcement Administration had shut down one pharmacy, but continued to ship packages from its affiliates. FedEx also developed policies and procedures that allowed it to continue shipments, the indictment said.

For example, some drivers in Kentucky, Tennessee and Virginia said they had been stopped on the road by customers demanding packages of pills. Delivery addresses included parking lots and vacant homes where people would wait for pharmaceutical deliveries, the indictment said.

In response, a senior FedEx vice president of security approved a procedure under which Internet pharmacy packages from problematic shippers were held for pickup at specific stations, the filing said.

"This policy was eventually expanded to include all Internet pharmacy packages delivered to the stations that were experiencing concerns," the indictment said.

Melinda Haag, the U.S. Attorney in Northern California, said the indictment "highlights the importance of holding corporations that knowingly enable illegal activity responsible for their role in aiding criminal behavior."

But FedEx's Fitzgerald said the DEA never supplied a list of pharmacies engaging in illegal activity. The privacy of the company's customers is "at risk" because of the charges, he said.

"We continue to stand ready and willing to support and assist law enforcement," Fitzgerald said. "We cannot, however, do the job of law enforcement ourselves."

The case in U.S. District Court, Northern District of California, is United States of America vs. FedEx Corporation, FedEx Express Inc and FedEx Corporate Services Inc, 14-cr-380.

—By Reuters