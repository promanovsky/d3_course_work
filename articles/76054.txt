Liberia has confirmed two cases of the deadly Ebola virus that is suspected to have killed at least 78 people in neighbouring Guinea, according to the World Health Organisation.

The WHO said in a statement the cases were among seven samples tested from the northern Foya district, which shares a border with southern Guinea.

"Two of those samples have tested positive for the Ebola virus. There have been two deaths among the suspected cases, a 35-year-old woman who died on March 21 tested positive for Ebola virus while a male patient who died on March 27 tested negative," it said.

Ebola has killed almost 1,600 people since it was first observed in 1976 in what is now Democratic Republic of Congo but this is the first fatal outbreak in west Africa.

The tropical virus leads to haemorrhagic fever, causing muscle pain, weakness, vomiting, diarrhoea and, in severe cases, organ failure and unstoppable bleeding.

Guinea's health ministry said on Sunday that 122 "suspicious cases" of viral haemorrhagic fever, including 78 deaths, had been registered.

Samples taken from a number of the suspect cases include 24 that tested positive for Ebola, according to the latest official figures -- 11 in Conakry and the rest in the south.

The WHO said Sierra Leone has also identified two suspected cases, both of whom died, but neither has been confirmed to be Ebola.

No treatment or vaccine is available for Ebola, and the Zaire strain detected in Guinea has a historic death rate of up to 90%.

It can be transmitted to humans from wild animals, and between humans through direct contact with another's blood, faeces or sweat, as well as sexual contact or the unprotected handling of contaminated corpses.

The WHO said in the statement, seen by AFP reporters in Dakar, it was not recommending travel or trade restrictions to Liberia, Guinea or Sierra Leone based on the current information available about the outbreak.

But Senegal has closed border crossings to Guinea "until further notice".