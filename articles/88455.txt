So, this is it. The biggie, the main event, Samsung's 'new thing'. The Galaxy S5 is finally here.

The phone has been sat safe and warm in my pocket for a good few days now, while I have obsessively explored every single nook and cranny of what it has to offer.

There is a heck of a lot to get through as Samsung has really run wild with this one, cramming vast numbers of features into the SGS 5. The good thing is, unlike previous offerings from Samsung, the majority of them are useful.

The Good



Stunning screen

Genuine useful features, less gimmicks

Class leading specs

Fingerprint sensor

Design doesn't quite cut it

Some confusing UI decisions

Design

Samsung has made it quite clear over the last few months that it knows its design isn't up to scratch. Going down the faux-leather route in the likes of the Note 3 was a start, but it was still way off what the competition was doing with materials and build.

With the Galaxy S5, it has taken a much bigger step in the right direction. The back of the handset uses a sort of dimpled plastic that is easy to grip and doesn't load up quickly with fingerprints.

Then you have a 'metal' wraparound, just like the one in the Note 3, with the front of the phone dominated by the 5.1-inch display.

It weighs virtually nothing at just 145 grams. Compared to the likes of the HTC One M8, it's also more compact, with a 142 x 72.5 x 8.1mm chassis. This is largely due to the fact that you don't have the larger speakers found on the M8.



Being IP67 certified, with proper water resistance, means the phone has flaps covering the ports on the bottom. Interestingly, the back is removable, with the phone offering up a warning explaining that you need to check you have put it back on properly before you go dunking it in the bathtub.

Sony Xperia Z2 hands-on review

It's definitely a step in the right direction for Samsung, but from a pure aesthetics and 'feel' point of view, the Galaxy S5 is way behind what Sony and HTC are doing right now. It just isn't quite as premium.

Hardware

This is where Samsung wins us back over. It has thrown just about everything that smartphone technology has to offer at the Galaxy S5, absolutely cramming it full of hardware.

A Qualcomm Snapdragon 801 processor clocked at 2.5 GHz gets things off to a good start. Pretty much any Android handset with this sat inside is absolutely rapid and the SGS 5 is no exception.



You then get 2GB of RAM, which is more than enough for multi-tasking but not class-leading, as some handsets offer 3. It all depends if you care about spec sheets or user experience, I think the latter is more important.

Next up is a Full HD 1080p 5.1-inch Super AMOLED display. This is where Samsung really works its magic, delivering what has to be one of - if not the best - smartphone screens we have ever seen.

More important though, is the inclusion of a new 'adapt display' mode. It changes colour saturation to dynamically suit a situation. Basically, it fixes the overly saturated and colour-bleed heavy displays of old.

Samsung Galaxy S5: Where can I get it?

The result is Super AMOLED levels of brightness, but the depth of colour you would get from something like a plasma TV.

Wi-Fi takes an interesting twist in the Galaxy S5, in that it's possible to use a 'download booster' function to combine the power of both LTE and wireless networks when downloading. In practice, full bars of 4G and a decent bit broadband makes downloads lightning quick.

Finally, a 2800 mAh battery powers the entire setup. It's not as big as what you find in some phablets, but is more than enough. Samsung's clever 'ultra power saver mode' - which turns the screen grayscale and turns down the smartphone's processor - can get you up to ten days on standby.

In real world use, the usual 'charge-a-day' rule applies. But with the Galaxy S5, we did find ourselves being able to use it a bit more than previous offerings from Samsung.

Every box is ticked and there is more than enough performance for this phone to sit right at the top of what smartphone's can do right now.

Camera

On the back of the Galaxy S5 is a 16-megapixel camera, while the front is daubed with a 2-megapixel unit.

Both are 1080p capable, but the big deal here is that the rear facing unit can also shoot 4K. It's a nice feature to have, especially if you have splashed out on an Ultra HD TV, but few will ever use this feature.



What they will take advantage of however is the rest of the camera's functions. It has, for example, HTC M8-style selective focus allowing you to shift focus between parts of an image after a shot has been taken.

Another bonus is how rapid it is to focus and take pictures. This is a phone you can whip out of your pocket and snap away. It's all about real world use here and for the most part, Samsung has got it right with the SGS 5 camera.

Real time HDR, slow motion video and a decent shot quality when you leave the camera just to do its thing, completes the Galaxy S5 photo package. It's a shame the user interface didn't get the same TLC the rest of the phone got, but you can't have everything.

Fingerprint scanner

First up, it's annoying. Basically, the decision to incorporate it into the home key, but require you to swipe down, results in you consistently leaving a greasy fingerprint at the bottom of the phone's screen.

Then there is the fact that it doesn't really work that well. Most of the time we found ourselves being asked to 'swipe again' which was hugely annoying.

It very much feels like 'Apple did it, so we should too'. The problem is, Apple did it a lot better.

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

That said, Samsung has been far more forward thinking with what it intends to do with the fingerprint scanner. Pairing it up with Paypal strikes us as the first big step toward getting rid of wallets and debit cards altogether.

It might not work right just yet, but Samsung might end up doing something more exciting with the fingerprint scanner in the future.

User Interface and Fitness Tech

Samsung wants to make the Galaxy S5 a much more friendly place to be and it shows from the outset with the Galaxy S5.





Right from the outset, handy pop-ups will boot whenever the phone can point you in the direction of a new feature or function.



The user interface is also much cleaner and simpler, without any of the nasty icon sets seen in previous versions of Touch Wiz.

As a result, operations are much better suited to the mobile beginner. This is great for Samsung, who has long been pushing to grab those who may have lauded iOS's simplicity and design.

That's not to say there isn't the odd gap in the phone's user interface. The settings menu for example is cluttered and doesn't make it easy to customise the phone to your needs.

Handy features like quick links to 'S Finder' - which will search the phone and web for apps and answers - are useful. The inclusion of Quick Connect, which is basically AirDrop and AirPlay in one, is also handy.

These new features actually help with how you use a smartphone. Old gimmicks like the 'Smart Stay', which detects you are looking at the screen and does things like pause and play video, remain but it's Samsung's new additions that are genuinely useful.

The biggest feature for Samsung's entire new family of Galaxy products is fitness. Wearables like the Gear 2 and Gear Fit work in tandem with the Galaxy S5 to show just how much exercise you are doing.



A heart rate sensor sits underneath the camera's flash. It works well provided you keep your finger still and will help give you an idea of just how hard you are working while out on a run.

Unlike the Gear Fit, which keeps on tracking your every move, you need to opt-in for the S Health setup on the Galaxy S5. Provided you stay faithful though, it can paint a pretty good picture of what you are doing to keep fit.

The user interface in the Galaxy S5 is not unlike the phone's design. It is a total step in the right direction but still needs polishing to be over the super high standards that the Galaxy range deserves.

That said, new features and a vast number of functions mean there is just so much that you can do with the Galaxy S5. Paired up with the likes of a Gear Fit and loaded with apps, we can't really imagine needing any other gadget in our lives.

Verdict

So should you buy a Samsung Galaxy S5? The chances are, if you are reading this, you've likely already made your mind up. Samsung will shift a ton of these and we feel they deserve to.

That said, if you are split between the HTC One, Galaxy S5 and Xperia Z2, then consider this. The HTC is a much nicer thing to hold, but it can't match the Samsung's screen or camera. Are you about form, or functionality? If it's the latter then the Samsung wins every time.

As for the Xperia Z2, we are still to deliver our final verdict, but initial impressions suggest that Sony's newest smartphone is the go-between of the two.

To conclude then, the Galaxy S5 is everything you expected it to be, an outstanding Android phone held back just a tad by its below average design. Perhaps next time Samsung will finally go all metal. If they do, then that's the end of the Android competition.



4.5 5

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io