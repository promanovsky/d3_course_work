ASPEN — Microsoft's energetic new hope Satya Nadella is still in his honeymoon period, judging by the warm reception he got at the Fortune Brainstorm tech conference Monday. But his animated conversation with Walter Isaacson at the Aspen Institute was long on big ideas and short on specifics for his much-touted new direction for Microsoft.

A couple of days ago, Nadella released a 3,200-word memo to rally his troops which used the word "productivity" more than 20 times. Asked to define productivity by Isaacson, who wondered if it meant that Microsoft was going to focus on the business world, Nadella told a story about learning to cook via online recipes. "That's productivity," he said.

Caution was the new CEO's watchword. As much as you get the sense when he speaks that he's underlining every word — this guy really believes what he's saying, you think — he's also carefully censoring each word.

For instance, asked about whether Microsoft would join the booming tech category of wearables, Nadella gave a full-throated response, but all he really said to answer the question was three words:

What about Microsoft wearables, asks @WalterIsaacson. "We have ambitions", insists @satyanadella — but offers no specifics. #fortunetech — Chris Taylor (@FutureBoy) July 14, 2014

This means Nadella is given to gnomic statements. One of the more interesting came when the CEO had this to say about whether his company could learn something from Google. Was it a genuine plaudit, or a subtle dis?

Nadella on learning from google moonshots: "always a lot to learn from people who market themselves well." #FortuneTech — danprimack (@danprimack) July 14, 2014

Apart from these brief bursts of quotability, and his rousing style notwithstanding, the rest of Nadella's Q&A came across like a Microsoft services catalog. The words of veteran VC Jean-Louis Gassée in Quartz are starting to ring true: Nadella is a "repeat befuddler," Gassee said.

.@satyanadella pretty adept at pivoting each question to #msft software and cloud products without sounding like he's ducking #fortunetech — Chris Taylor (@FutureBoy) July 14, 2014

Then again, a Microsoft CEO has a pretty low bar when it comes to delighting a tech audience, given the tenure of Nadella's predecessor, Steve Ballmer:

Never thought I'd hear a Msft CEO be passionate about open source. It must be a cold day ... #FortuneTech — Mike Moeller (@_mike_moeller) July 14, 2014

Probably the most coherent message I've heard in years from MSFT via @satyanadella #FortuneTech — Erik Lammerding (@Lammerding) July 14, 2014

Got a caption for the photo above? Let us know in the comments.