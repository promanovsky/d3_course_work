Celebrity

In a piece published in New York Magazine's June 16 edition, the controversial photographer explains that there are other people working with him in photoshoots.

Jun 16, 2014

AceShowbiz - Fashion photographer Terry Richardson denies sexually harassing models. In an article pusblished on New York Magazine's latest issue, hitting the newsstands on June 16, the photographer slams reports of him sexually exploiting models during his photoshoots.

"It was never just me and a girl ever. It was always assistants, or other people around, or girls brought friends over to hang out," he is quoted as saying. "It was very daytime, no drugs, no alcohol. It was a happening, there was energy, it was fun, it was exciting, making these strong images, and that's what it was. People collaborating and exploring sexuality and taking pictures."

Some models previously claimed that Richardson asked them to do some sexual acts during his photoshoots. Most recently, a New York-based writer and stylist named Anna wrote to Jezebel, saying that she was approached by Richardson's assistant for an impromptu photoshoot when she attended a party in 2008. The report posted on Wednesday, June 11 claimed that the photographer sexually assaulted her at his studio.

Anna added that she only revealed her story now because she thought no one would pay attention to a "non-model" story. "The guy shouldn't just be locked out from the fashion photography world, he should be in jail," read an e-mail from Anna.