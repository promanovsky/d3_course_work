AOL says a security breach may have exposed the private information of a "significant number" of its email users' accounts.

The company said Monday that the email addresses, postal addresses, address books, encrypted passwords and encrypted answers to security questions of users may have been exposed, along with some employee information.

AOL believes spammers used this contact information to send "spoofed" emails. Spoofing is a tactic used by spammers to make an email look like it is from someone the recipient knows to trick him or her into opening it. These emails do not originate from the sender's email -- the addresses are just edited to make them appear that way.

The company estimates these emails came from about 2 percent of its accounts. The investigation is still underway.

