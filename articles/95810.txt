Stocks had a positive session on Tuesday after selling off in the preceding two sessions. Pre-open sentiment is indicating a modestly positive open, but the overall mood remains tentative as the market nervously awaits the Q1 earnings season.

Alcoa (AA) came out with a fairly decent earnings release after the close on Tuesday, but that tells us little about to what the rest of the earnings season will bring. Including the Alcoa release, we now have 2014 Q1 results from 23 S&P 500 members. Total earnings for these 23 companies are up +14.4%, with 60.9% beating EPS expectations. Total revenues for these companies are up +6% and 43.5% are coming ahead of top-line expectations. Hard to draw any conclusions from this sample of reports, but the growth rates thus far from these companies are lower than what we saw from the same group of companies in recent quarters.

The reporting cycle gets into high gear next week with almost 60 S&P 500 members reporting results, but the bank sector reporting season will get underway with results from J.P. Morgan (JPM) and Wells Fargo (WFC) this Friday. The Zacks Consensus EPS estimate for JPM has weakened a bit in recent days while WFC's estimate has improved a bit.

The overall level of Finance sector's earnings in Q1 will remain very high, but still remain below the year-earlier level. Most of earnings gains thus far have come from expense controls and reserve releases, with net interest margins still under pressure and the mortgage refinancing business steadily going down. Loan demand continues to remain to tepid, with growth barely in the low-single-digits vicinity.

There is some improvement on the commercial loan side, but we have yet to see any material improvement on the consumer loan side. The consensus expectation is for the resumption of stronger loan growth later this year and beyond, with loan growth accelerating to the mid-single-digit pace in the second half of 2014 and even higher next year. This optimistic outlook, a function of above-trend growth expectations for the U.S. economy, is also expected to drive overall earnings growth in the back half of the year and beyond.

Corporate guidance on the Q1 earnings calls will determine how expectations for the coming quarters will evolve. But if past is any guide, we will most likely see those estimates come down as the Q1 earnings season unfolds. All the more reason for investors to start nervous about the earnings picture.

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.