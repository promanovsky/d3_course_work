It seems like Zac Efron‘s shirtless body has been everywhere recently, but now we are going to be able to see it up close on the big screen once again!

Neighbors hits theaters Friday (May 9) and the movie is getting rave reviews for Zac and his co-stars Seth Rogen and Rose Byrne.

We’re already guaranteed to see some skin from the MTV Movie Award winner for Best Shirtless Performance, but we can’t wait to see if it’s enough to warrant another win for Zac at next year’s show!

The film follows a couple (Rogen and Byrne) with a newborn baby who face unexpected difficulties after they are forced to live next to a fraternity house. The members of the frat are played by Zac, Dave Franco, and Christopher Mintz-Plasse, to name a few.

Check out some stills in the gallery and watch the trailer below!



“Neighbors” Theatrical Trailer

30+ pictures inside of Zac Efron and others in Neighbors…