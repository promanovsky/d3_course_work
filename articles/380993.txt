A non-biographical musical based on the poetry and music of Tupac is getting the chop just over a month after it opened.

Holler If Ya Hear Me began previews on 2 June but officially opened on 19 June at the Palace Theatre on Broadway, New York, to lacklustre reviews and a low turnout.

Despite the show being expected to close this coming Sunday, top price tickets in October are still available to buy for about $187 (£109), with the cheapest about $48 (£28).

A blurb for the musical, which is set in a gritty inner-city neighbourhood, states: “Broadway has always given voice to popular culture and social change. South Pacific, West Side Story, Hair, For Colored Girls, Sarafina and Rent are among the hits that hit home.

“Now Broadway gives voice to the lyrics of Tupac Shakur, whose music has sold over 77 million albums!”

The biggest West End flops Show all 10 1 /10 The biggest West End flops The biggest West End flops Viva Forever Mel B, Mel C, Geri Halliwell, Emma Bunton and Victoria Beckham at the press night of the Spice Girls musical Viva Forever, which turned out to be Viva Fo-Never when it closed on the West End in June 2013 after just six months PA The biggest West End flops Spider-Man: Turn Off the Dark It cost a massive £48m to make and U2 penned the music, but after main cast members were injured in rehearsal, its opening night was postponed 6 times and its director resigned, Spider-Man: Turn Off the Dark has lost so much money it will take 12 years of sell-out shows to make an ounce of profit Getty Images The biggest West End flops Behind the Iron Mask Behind the Iron Mask previewed on the West End in summer 2005 but was destroyed by critics and closed just 18 days after its official opening night The biggest West End flops Lord of the Rings Lord of the Rings opened in Covent Garden in summer 2007 but was criticised for being too long, confusing and dull, leading to a final curtain in 2008 after 492 performances Gareth Cattermole/Getty Images The biggest West End flops Thoroughly Modern Millie Britain's Got Talent judge Amanda Holden made her West End debut as Millie in this 1920s flop - critics described her performance as 'thoroughly charmless' and the final curtain fell after 8 months in June 2004 The biggest West End flops Desperately Seeking Susan Blondie's Deborah Harry poses with actresses Emma Williams and Kelly Price as they promote the launch of Desperately Seeking Susan - an odd combo of Madonna's 1985 film and Blondie's music that opened in 2008 and ran for just four weeks with just 200 people watching each night The biggest West End flops Gone With the Wind Darius Danesh, who sung 'Baby One More Time' during his Pop Idol audition, was cast in the iconic role of Rhett Butler, the show received poor reviews, was too long at over 3.5hrs and closed in 2008 after just 79 shows Gone With the Wind The biggest West End flops Twang! Lionel Bart's 1965 Robin Hood musical (you know he has an arrow...twang...) opened with a nightmare first performance involving power failures, last-minute script rewrites and the musical director collapsing - unsurprisingly it closed after 43 shows and a flood of scathing reviews The biggest West End flops Carrie the Musical The musical adaptation of Stephen King's horror novel received a critical panning when it opened in 1988 - financial backers withdrew and it closed on Broadway after just 16 previews and 5 performances The biggest West End flops Oscar Wilde the Musical Written and directed by DJ Mike Read to coincide with Wilde's 150th birthday, the show opened at the Shaw Theatre in London on a Tuesday and closed on the Wednesday after the kindest review called it 'the worst musical in the world ever'

As Billboard reports, other hits on Broadway such as Wicked regularly post a weekly income nearing the $2million mark, yet Holler didn’t quite muster ticket sales of $200,000 in any of its six weeks.

Producer Eric L. Gold said in a statement that he hopes “a production of this calibre, powerful in its story telling, filled with great performances and exciting contemporary dance and music, will eventually receive the recognition it deserves.

Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

“It saddens me that due to the financial burdens of Broadway, I was unable to sustain this production longer in order to give it time to bloom on Broadway.

“Tupac's urgent socially important insights and the audiences' nightly rousing standing ovations deserve to be experienced by the world.”

The initial cost of putting on the stage show was $8million, however last week only 45 per cent of its seats were occupied, the New York Times reports, as the production prepares to close at a loss.

The musical was directed by Tony Award winner Kenny Leon and stars singer and poet Saul Williams and Tony nominee actress Tonya Pinkins.

It was reported last week that production staff were trying to raise $5million to keep the show afloat while box office sales improved, with Gold telling Variety: “I made a rookie mistake by underestimating how much capital was necessary, but I’m tenacious.”

The veteran TV bigwig, who has managed the Ellen DeGeneres Show and also produced My Wife and Kids, admitted that the show’s precariousness meant they were going “week to week.” Unfortunately it has now transpired that this will be its last.