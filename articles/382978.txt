Organic produce and grains contain more protective antioxidants, less pesticide residue and lower levels of the toxic metal cadmium than food raised in traditional ways, a new review finds.

It's not clear what this means for your health. And several agriculture experts claim the analysis missed some important points.

But a study co-author said the research offers a clear message.

"Organic plant-based foods offer some significant nutritional advantages, and also reduced risks associated with exposures to cadmium and pesticides in food," said study co-author Charles Benbrook, a research professor with the Center for Sustaining Agriculture and Natural Resources at Washington State University.

Benbrook said the new research provides more accurate numbers about specific nutrients and offers more reliable evidence about cadmium levels in non-organic food than past reports.

The study is unlikely to resolve the longstanding debate about whether organic food is worth the extra cost -- in some cases twice the cost of conventionally grown food.

In 2012, the American Academy of Pediatrics declared there's little evidence to support the idea that organic food is healthier over time. Stanford University also released a study that year that came to similar conclusions.

For the new study, published July 14 in the British Journal of Nutrition, researchers examined 343 peer-reviewed studies. On average, antioxidant levels were 17 percent higher in organic crops than in crops grown in traditional ways, the researchers found. And some levels of antioxidants, such as flavanones, flavonols and anthocyanins, were significantly higher.

Scientists believe antioxidants protect the body's cells from damage due to smoking, stress, processed foods and many other harmful elements, but there's debate over whether they're beneficial when they come from food.

Why would organic foods have more antioxidants than non-organic foods?

"They are fertilized differently, and in ways that support higher levels of certain nutrients," Benbrook said. Also, organically grown foods produce antioxidants in response to stress like pests, he said.

Levels of pesticide residue, meanwhile, were four times higher in non-organic crops, and levels of cadmium were twice as high, the studies found.

"No other review has had enough comparison studies analyzing cadmium to reach that conclusion," said Benbrook. However, he acknowledged he doesn't know how many Americans are thought to consume too much cadmium, a toxic metal often found in industrial workplaces.

Samir Samman, head of the department of human nutrition at the University of Otago in New Zealand, praised the research as "an important addition to our knowledge in this field." Samman said the study especially adds to details about antioxidants in organic foods.

But some food researchers faulted the research and its conclusions.

"The important toxicological question is how much cadmium are we exposed to, not whether there are any differences between organic and conventional forms," said Carl Winter, vice chair of the department of food science and technology at the University of California, Davis.

That organic foods contain lower levels of pesticide residue isn't surprising since it's produced without the use of most conventional pesticides, experts said.

However, Winter said pesticides in non-organic foods tend to be at much lower doses than those that cause problems in laboratory animals.

"There is still an enormous health cushion even if one ate just conventional foods," he said.

Richard Mithen, leader of the food and health program at the Institute of Food Research in England, noted that the analysis doesn't show that the observed differences between organic and non-organic foods have any effects on health.

"The additional cost of organic vegetables to the consumer, and the likely reduced consumption, would easily offset any marginal increase in nutritional properties, even if they did occur, which I doubt," he said in a statement.

"To improve public health we need to encourage people to eat more fruit and vegetables, regardless of how they are produced," he said in his statement.

Sheepdrove Trust, a charity that supports organic farming, provided partial funding for the study.