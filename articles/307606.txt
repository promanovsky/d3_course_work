The Board of Directors of Alstom on June 21, 2014 has given its nod to the offer from General Electric (GE) to acquire the power and grid businesses of Alstom, ignoring the joint revised proposal from Siemens and Mitsubishi Heavy Industries. “The ad hoc committee of independent directors appointed by the Board on April 29, 2014 and led by Jean-Martin Folz, thoroughly reviewed, on multiple occasions, the proposed transactions. Based on the works of the committee and financial and legal advisors, the Board of Directors has unanimously decided to issue a positive recommendation of the offer from GE,” said Alstom in a press release.



The Directors of Alstom expressed their satisfaction that the productive exchanges established with the French State had resulted in a business proposal that not only addresses the interests of Alstom and of its stakeholders, but also provides assurances in connection with concerns expressed by the French State.



As per GE’s offer, the American company would acquire, as previously announced, the Thermal Power, Renewable Power and Grid Sectors, as well as corporate and shared services (the Energy Transaction) for a fixed and unchanged price representing an Equity Value of Euro 12.35 billion and an Enterprise Value of Euro 11.4 billion.



Under the terms of the updated offer, following completion of the Energy Transaction, Alstom and GE would establish joint ventures in Grid and Renewable Power. In Grid, each company would hold a 50% stake in a global business combining Alstom Grid and GE Digital Energy. In Renewables, each company would hold a 50% stake in Alstom’s Off-shore Wind and Hydro businesses.



In addition, Alstom and GE would create a 50:50 Global Nuclear and French Steam alliance, which would include the production and servicing of the Arabelle steam turbine equipment for nuclear power plants, as well as Alstom’s steam turbine equipment and servicing for applications in France. In addition, the French State would hold a preferred share giving it veto and other governance rights over issues relating to security and nuclear plant technology in France.



The investment by Alstom in these Energy related alliances represents Euro 2.5 billion, assuming these companies are debt-free, cash-free. The terms of these alliances include usual shareholders agreements with standard governance and liquidity rights.



Finally, GE proposes the creation of a global alliance in which GE would sell Alstom 100% of its signalling business, with sales of $ 500 million in 2013 and 1,200 employees, and the companies would sign multiple collaboration agreements including a service agreement for GE locomotives outside of the US, R&D, sourcing and manufacturing and commercial support in the US.



The ad hoc committee of independent directors reviewed the transaction proposed by GE with the assistance of its financial and legal advisors. The financial expert appointed to advise the Board has concluded that the financial consideration offered by GE to Alstom is fair from a financial point of view.



The Board, acknowledging unanimously the strategic and industrial merits of GE offer, has decided to issue a positive recommendation of this offer, authorising Patrick Kron, in his capacity of Chief Executive Officer of Alstom, to engage in the next step of the process, with the information and consultation of the competent works councils within the Alstom group.



Patrick Kron, Chairman and CEO of Alstom, commented, “The combination of the very complementary Energy businesses of Alstom and GE would create a stronger entity, best placed to serve customers globally and invest in people and technology over the long run. Alstom would be associated to this ambitious combination through the Energy alliances. Alstom Transport, a solid leader with a large portfolio of technologies and a worldwide presence in a dynamic market, would be further strengthened through the acquisition of GE's signalling business as well as a far-reaching rail alliance with GE.”



The Board of Directors, has unanimously determined that the improved proposal received from Siemens and Mitsubishi Heavy Industries on June 20, 2014 does not adequately address the interests of Alstom and of its stakeholders.Under the terms of the improved proposal, Siemens would acquire Alstom’s gas business for an equity consideration of Euro 4.3 billion, a Euro 400 million improvement versus the initial proposal. MHI would buy a 40% equity stake in the combined steam, grid and hydro business of Alstom through one single holding company, for a consideration of Euro 3.9 billion. In addition, Siemens would offer to enter into an up to 50:50 Joint Venture with Alstom in Signalling and Mobility Infrastructure.Completion of the GE transaction will be subject to works council consultation and merger control and other regulatory clearances, including French Foreign Investment authorisation. In accordance with the AFEP-Medef code, the final approval of the transaction will be submitted to the shareholders.Should this offer be approved and completed, Alstom would refocus on its fully owned Transport activities and on its Energy alliances with GE. Alstom would use the proceeds of this transaction to strengthen its Transport business, to invest in its Energy alliances, to pay down its debt and return cash to its shareholders.