A recent study has found that chronic pain is a significant problem for soldiers who are returning from tours of duty. What’s worse is that more than 15 percent also report needing to rely on prescribed opioids to get pain relief.

According to an article published by Medical News Today, a quarter of members of the general population who require primary care are also suffering from chronic pain, and similarly are commonly prescribed opioids to help with the pain. However, because there is a growing amount of misuse related to those potent painkillers, that reality has resulted in an increase of related hospitalizations and fatalities.

This chronic pain study is the first that targeted opioid dependency in returning soldiers, and it was lead by researchers at the Walter Reed Army Institute of Research, located in Maryland.

For the purposes of this research, “chronic pain” was defined as lasting for three or more months. Scientists collected data related to one infantry brigade that served in Afghanistan. The administered surveys were given on a confidential basis, and began getting passed out three months after the soldiers came back from the war zone.

Unfortunately, the reports about chronic pain and its severity were bleak: Over 55 percent of the respondents had pain almost every day, and more than 48 percent said their chronic pain persisted for a year or longer. Over 50 percent also admitted that the pain they experienced was moderate or severe in its intensity.

In terms of opioid reliance to control the chronic pain, over 57 percent of the group said they used painkillers every few days, and slightly more than 23 percent said they had used them within the past month. In contrast, 26 percent of the civilian population reports having chronic pain, and four percent use opioids to manage it.

There were 2,597 soldiers whose chronic pain experience was examined to get data for the study. Most of them were males, and just over 41 percent ranged in age from 18 to 24. Additionally, almost half said they were injured in combat. Alcohol abuse was reported by slightly more than 16 percent, and 9.1 percent were also suffering from post-traumatic stress disorder.

Judging by the results, and the fact that chronic pain and opioid use as it relates to the military population has not been studied at length prior to this, it suggests a clear need for medical personnel to be mindful of proper chronic pain management procedures, especially if patients are returning from combat.

[Image Credit: army.mil]