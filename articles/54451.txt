Mick blamed for ‘showbiz, fake funeral’

Share this article: Share Tweet Share Share Share Email Share

London - L'Wren Scott's sister says she “deserved more” than her funeral in Los Angeles. Jan Shane has hit out at the late fashion designer's grieving partner Sir Mick Jagger and accused him of “hijacking” her memorial, after he planned it to take place in Los Angeles, insisting her sibling - whose real name was Luann Bambrough - should have been laid to rest alongside their parents in their hometown of Ogden, Utah. Jan said: “It's become a media circus, it's so not how Luann would have wanted it. “She has been away from home for so long, this is her place, not in some strange cemetery in Los Angeles. It has no connection, but it's Mick and his kids that are making all the decisions. “He hijacked this funeral. Yes, he was with her for 13 years, but family is family, that's what it boils down to.

“She's going to be alone there at this cemetery. I wish it had been handled differently. I hate to believe that it's become a showbiz, fake funeral - she deserves more. She's a human being, she made a life that was hers, not his. She deserves to be remembered for herself.

“She'll be buried and passed away as Mick's girlfriend, not Luann Bambrough or L'Wren Scott. It's so far blown out of proportion, she's a human being, she deserves more.”

It has emerged that the Rolling Stones rocker - who had been in a relationship with L'Wren for 13 years until her suicide earlier this month - is the sole beneficiary of the designer's will but Jan has vowed to fight for the return of some personal items.

She told MailOnline website: “I would assume she had a Will, I'm not really sure. But I know after mum passed away, L'Wren took pictures away from the family home. They were my childhood, I want them back, I hate to think they're gone forever, it's our memories of growing up. The Jagger family doesn't need them, they're our memories from our childhood.

“She had things of mum's that have no use for anyone else, but there's some nieces and nephews that would want them. I would like it kept in the family, they don't need to be destroyed or given away.”

L'Wren's brother Randall Bambrough was the only family member to attend the private funeral service at the Hollywood Forever Cemetery on Tuesday. - Bang Showbiz