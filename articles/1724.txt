Apple on Monday released an updated version of its mobile operating system, iOS 7.1, and it comes with several noticeable changes over the previous release, as well as a few differences that won't be apparent unless you know to enable them.

Many of the physical alterations smooth out the design leap that iOS users experienced when jumping from iOS 6 to iOS 7, which introduced the so-called flat design and parallax effects. A few of the changes in iOS 7.1, such as how some Web pages display in Safari, are more behind-the-scenes for programmers and won't be apparent to users until developers leverage them.

But these six changes are worth investigating. Here's a few words about them, as well as where you can find them.

Phone and FaceTime Answering Options

Incoming phone calls and FaceTime requests look a little different in iOS 7.1. Buttons shaped like rounded rectangles move to the wayside to make room for circles. The buttons for "remind me" and "reply with message" look completely different now, almost played down compared with their blocky past versions.

Where: On screen when a phone call or FaceTime call comes in.

Shift and Caps Lock Indicators

The shift key on iOS's virtual keyboard now uses new icons to indicate when it is not engaged (white key, black arrow), has been pressed once for a single keystroke (gray key, white arrow), and tapped twice to turn on caps lock (white key, black arrow with underline). In the image above, from left to right: shift not engaged; single cap shift; all caps.

Where: On the keyboard.

Siri Voices

With iOS 7.1, Siri should sound a little sweeter, or more masculine, depending on your taste. Siri now has both male and female voices for Australian English, British English, Japanese, and Mandarin, and they should sound better, too. The developer release notes state that your iPhone or iPad initially uses a "compact voice for Siri," but that it will download and install a higher-quality version automatically once you connect to a Wi-Fi network and plug your device into a power source. I could hear the improvements most clearly in the Australian male voice.

Where: Settings > General > Siri

Button Shapes

When enabled, Button Shapes highlights buttons that otherwise look like text to better differentiate them. It makes iOS 7.1 look slightly less sophisticated and austere, but it makes the buttons pop for sure. In the image above, you can see the traditional view on the left, and the enhanced button shape on the right. Sometimes it takes the form of a simple underline instead of a gray box. I rather like a lot of the accessibility features in iOS because they often improve the experience without really getting in the way, and Button Shapes is one you can enable and forget you ever did. (For other useful accessibility features, try the reduce motion setting if iOS 7 makes you dizzy, and Guided Access to lock your screen while kids use your device.)

Where: Settings > General > Accessibility > Button Shapes

Perspective Zoom (Ability to Disable Parallax Effect for Wallpaper Only)

With iOS 7.1, you now have finer controls for disabling some of the visual effects within iOS 7. When you choose a wallpaper for your lock screen or home screen in iOS 7, you'll see an option to turn "perspective zoom" on or off. Turning it off disables the parallax effect (if you're still not totally sure what parallax effect is, scroll slowly through this BBC page, and you'll get it), without disabling the other animations, such as the zooming in and out of app preview pages when you double-tap the home button.

Where: Settings > Wallpaper & Brightness (and then choose a wallpaper)

Less Bouncy Control Center

It's not a huge change, but the quick-access control bar, which lets you adjust the screen brightness, adjust Wi-Fi and Bluetooth, etc., bounces less when you first engage it. The Control Center, which was entirely new as of iOS 7, formerly pulled up higher onto the screen and sprang back into place when you drew it beyond its capacity. Now, it locks into place quicker and more tightly. You still can't use it for location services (i.e., GPS), which has been on my wish list since iOS 7 debuted in 2013.

Where: Control Center; use one finger near the bottom of the screen and swipe up.

Further Reading

Operating System Reviews