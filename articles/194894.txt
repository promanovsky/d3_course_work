We're only a few days away from the premiere of "Rosemary's Baby" and these new clips from the NBC mini-series already have us creeped out.

In the first, we see Rosemary (Zoe Saldana) exploring the catacombs of Paris while being entertained with ghost stories from her chatty tour guide. Talk of Satanists, demon spirits and the overabundance of human skulls start to get the newly pregnant Rosemary a bit flustered, but it's nothing compared to what happens when her demon baby gets too close a certain religious symbol.

The second clip is even more worrisome as a detective comes by to question Rosemary on the apparent suicide of a priest whom she had visited the day before. After spewing nonsense about cannibals living in her building and people eating human hearts, Rosemary left the priest who later hanged himself. The clip ends with the detective warning Rosemary to be wary of trusting others.

Take a look at both clips below.