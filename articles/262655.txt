Guinea's capital Conakry has recorded its first new Ebola cases in more than a month, while other previously unaffected areas have also reported infections in the past week, according to the World Health Organization.

The spread of the two-month-old outbreak, which Guinean authorities had said had been contained, risks further complicating the fight against the virus in a region already struggling with weak healthcare systems and porous borders.

"The situation is serious, you can't say it is under control as cases are continuing and it is spreading geographically," Dr Pierre Formenty, a WHO expert who recently returned from Guinea, told a news briefing in Geneva on Wednesday.

"There was no decline. In fact it is because we are not able to capture all the outbreak that we were under the impression there was a decline," he said.

The WHO reported two new cases, including one death, between May 25 and 27 in Conakry. They were the first to be detected since April 26. An outbreak in the capital could pose the biggest threat of an epidemic because the city is Guinea's international travel hub.

Telimele and Boffa - two districts north of Conakry that were previously untouched by the disease - both confirmed outbreaks through laboratory testing, the WHO said. Twelve cases, including four deaths, were reported there between May 23 and 26, while suspected Ebola infections were documented in the adjacent districts of Boke and Dubreka.

Aboubacar Sidiki Diakité, who heads the Guinean government's efforts to halt the virus's spread, said the origins of all the new outbreaks had been traced back to cases in Conakry.

"The problem is that there are families that refuse to give information to health workers. They hid their sick to try to treat them through traditional methods," he told Reuters.

The outbreak - the first deadly appearance of the haemorrhagic fever in West Africa - spread from a remote corner of Guinea to the capital and into Liberia.

Sierra Leone reported its first confirmed outbreak of the disease earlier this week.

The WHO has documented a total of 281 clinical cases of Ebola, including 185 deaths in Guinea since the virus was first identified as Ebola in March.

The disease is thought to have killed 11 people in Liberia, thought there have been no new cases there since April 9.

Sixteen cases - seven of them confirmed through laboratory testing and another nine suspected - have been reported in Sierra Leone's Kailahun district, where four people were believed to have died of the disease.

Ebola, which has a fatality rate of up to 90 per cent, is endemic to Democratic Republic of Congo, Gabon, Uganda and South Sudan. Researchers believe the West African outbreak was caused by a new strain of the virus.