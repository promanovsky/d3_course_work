WHEN it came to romantic comedies, Katherine Heigl was once Hollywood’s ‘It’ girl… until she wasn’t.

Now, in a new interview with Marie Claire, Heigl has revealed that she felt “betrayed” when her career came to a screeching halt.

“The thing that was my best friend for a long time suddenly turned on me. And I didn’t expect it. I was taken by surprise and angry at it for betraying me,” she told the magazine.

Heigl, who became a star thanks to her role on TV’s Grey’s Anatomy, then went on to star in movies including Knocked Up, 27 Dresses and The Ugly Truth. The actress said that her propensity to take every rom-com she was offered probably contributed to her professional decline.

“I had an amazing time. I love romantic comedies. But maybe I hit it a little too hard. I couldn’t say no,” she said.

“I stopped challenging myself. It became a bit by rote and, as a creative person, that can wear you down. That was part of why I took that time off, to ask myself, ‘What do I want? What am I looking for?’ and shut down all the noise.”

Heigl even revealed that she came close to quitting the business altogether.

“Oh yeah, I had a moment where, I don’t know, I was thinking I’d maybe open a knitting store, get my money out of retirement accounts and live off that, live off the land. I had my moment where it all seemed so complicated and all I wanted to do was simplify.”

Still, it seems the knitting will have to wait.

“There’s a part of me that’s a Hollywood animal as well,” she said. “I can’t wait to get into the writer’s room and see how we do this. I feel like I’m finally rolling into the next phase of my adulthood.”