The Amazin' Don Draper, like the Amazin' Mets, is about to embark on one helluva comeback. Meanwhile, everyone else is as grumpy as a cult.

Mad Men creator Matthew Weiner has shown that he has no problems tackling death as a metaphor -- which should come as no surprise in a series based on one man's existential crisis -- and he has allowed his writers to tap into that device with equal consistency. In "The Monolith," an episode replete with references to death, the emphasis was actually to look beyond death, to triumph against the odds.

And no one seems more poised to do that than Don Draper who, after an episode of hitting the bottle after rough times, a la Old Don, was given the straight dope by none other than Freddie Rumsen.

"Are you just going to kill yourself and give them what they want?" he asked Don, telling him to soldier up, strap on the bayonet and go get it done. "Do the work, Don."

PHOTOS: 'Mad Men' Season 7 Premiere: Cast Gets Sentimental

It's what Don has always done. It's the only thing that matters to him, really. The work.

And the work is drudgery, at least for now. He's tasked by Peggy -- oh, let's not start on Peggy right now because that will lead to so much disappointment -- to write 25 tags for a new ad campaign that has no concept because Lou (oh, dear Lord, that Lou) believes in sneaking up on a concept rather than creating one. Don is on the account with Mathis, the lowest-ranking copywriter who is ecstatic to be tapped for the job -- the absolute opposite of Don's emotions. We know this because Don bore holes through Peggy's skull with his eyes. It's a humiliating situation. But Don did say "OK" last week to the deal.

Let's not kid ourselves -- it's a shit job. And while Don endures it, he also sees that Lloyd, from Lease-Tech, is a guy who runs an emerging business that SC&P should be going after. But the many constraints on Don, from the partners, forbid this kind of thing and that's pretty much when Don realizes the partners would rather make him miserable enough to quit or drink himself senseless than make him happy. They took him back to ruin him.

After Bert Cooper scolds Don on this new accounts idea, Don asks why he's even there. Why hire him back if you want to make him the janitor? "Why are you here?," retorts Cooper, who is suddenly, like almost every main person outside of Roger at SC&P, irrationally hostile toward Don.

"Because I started this agency!" Don yells back. To which Cooper says, rather coolly: "Along with a dead man whose office you now inhabit."

STORY: 'Mad Men' Deconstruction: Ep. 3, 'Field Trip'

Ah, yes, the death thing. It was everywhere in "The Monolith." Technology was killing creative, according to Ginsberg (who is also irrationally irritable this season). Pete's former father-in-law has had a heart attack. Roger's grand kid is killing secretary Caroline. President Nixon was considering a nuclear strike against North Korea. Lou, in his never-ending cliches, told Peggy to "knock 'em dead" with the new account. And, of course, the not-so-subtle mention by Cooper that Don was using a dead man's office (Lane's).

Beyond death, a major theme in "The Monolith" was not going gentle into that good night -- well, at least it was for Don. Where everyone else was giving up, conceding or pouting, Don was taking inspiration from the New York Mets. Yep, the Mets. At first, he found the team's souvenir pennant under the furniture in Lane's old office and threw it away. But we saw briefly that Don had pinned it back up (almost in the exact same spot Lane did) and thereafter it was in countless shots. (And yes, it was orange, if you want to extend the death metaphor beyond its elasticity.) No, what's more important about the Mets pennant in Don/Lane's office is that the loveable losers of New York, an expansion team that floundered in its first seven years of existence, would go on to win the World Series -- in 1969. Yep, our current Mad Men year. It was one of the most shocking feats in baseball history. A comeback like, well, like Don Draper's.

Anybody want to give Don Draper the early lead for comeback player of the year?

With only three more episodes left in this seven-and-seven split season of 14 episodes, "The Monolith" boldly stepped up and solidified some of the earlier themes -- mostly that Don is not done. Like the Mets, nobody believes he's got much of a chance (and it bears repeating that, of all people, Freddie Rumsen does). But we are left, with this episode, looking at SC&P as a real mess. People are coming in late. There's a lack of attention to detail. Nobody can see how far the firm has slipped -- either because they're in denial or because they're too close. Don, as much of a representation of old-school values as we have now on Mad Men, was disgusted last week by everyone's nonchalant attitude. He sees the slippage. He's befuddled. And concerned.

STORY: 'Girls on Men' Podcast, Season 7, Episode 3 with 'Mad Men's' Joel Murray

The arrival of the IBM 360 might be the firm's first step toward acknowledging the future -- five years after it was introduced -- but there's no indication that anyone involved thinks this is a good idea, except Harry. And yet, the most intriguing scenes in "The Monolith" came not long after Lloyd, from Lease-Tech, asks Don a familiar question: "Advertising -- does it work?" The two have a very tech vs. humanity discussion. "It's more of a cosmic disturbance," Lloyd tells Don. "A machine is intimidating because it contains infinite quantities of information, and that's threatening."

It's the notion that we are all going to be replaced that permeates this episode -- everyone at SC&P is included in that bleak scenario. But that's life, right? And Lloyd seemed to know people are scared of technology, of the future, because that means change of the worst kind. "Because human existence is finite," Lloyd says, continuing the death theme. But for Lloyd, who is now selling the future to companies like SC&P without much thought to the consequence, there's something beautiful about its power. "But isn't it godlike that we've mastered the infinite," he says. A character doesn't just say those words without the intent, from the writers room, of being noticed.

While continuing Mad Men's overarching theme about existentialism is always nice, another scene with Lloyd tapping into the show's other strong theme -- identity -- was even more riveting (and weird). Don, drunk on Roger's smuggled booze, oddly confronts Lloyd before going home (or to a mystical Mets game, if you will). "You talk like a friend, but you're not," Don says, which is also something that he himself can be accused of. "You go by many names," he says to Lloyd. "I know who you are."

It's perfectly conceivable that Don finally saw into a charlatan like himself -- maybe Lloyd is actually living a double life -- and played the it-takes-one-to-know-one card. "You don't need a campaign. You've got the best campaign."

It was one of those scenes that may or may not be explained, or we could find out next week that Lloyd is a newer Dick Whitman. But in either case, this drunk Don -- a firing offense under the new rules of his employment -- seemed to see a ghost of a man and confronted Lloyd in a scene that was both riveting and out of place.

PHOTOS: 'Mad Men' Cast: Before They Were Famous

Mostly thanks to Freddie -- who saves him from being fired and rescues him with black coffee the next morning -- Don lives to fight again. And that's how he leaves us in this episode, typing up taglines for Peggy. It's pretty clear that Weiner and his writers have positioned Don for at least a semitriumphant return. And it's intriguing that SC&P is a mess without Don leading everyone to greater heights.

With Don soaring, it's a little disappointing to see some of the others mailing it in. Peggy and Bert Cooper in particular don't seem like themselves. She's hell bent on ruining Don for crimes mostly unknown, and Cooper seems to share her indifference to Don. I don't think Don's meltdown in season six would generate such feverish responses, but without Don as the orbital center of SC&P, the whole firm seems to be searching for something that it lacks. I don't like shrill, defensive, overly emotional Peggy. Nor do I like waking up the dormant Cooper only to find him dismissive and overly judgmental and harsh to Don. I'm having a hard time believing either character.

I wish Mad Men had painted a picture of Don's departure with a longer expanded take on exactly why everyone is so pissed at him -- Don doesn't seem to have incurred as much wrath as he's been getting. Sure, he botched the Hershey's account. But that was more a plea for help than something that has sent SC&P into a tailspin. Why torture and kick out your best employee? There's no turning back now. Besides, after Sunday's episode, you have to believe that SC&P's fortunes lie with Don. His comeback is one for the ages. He's not dead yet. In fact, he's rarely been this alive.

Email: Tim.Goodman@THR.com

Twitter: @BastardMachine