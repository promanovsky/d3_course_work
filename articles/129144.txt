Nokia Corporation (NYSE:NOK) (BIT:NOK1V) (HEL:NOK1V) recently stopped selling the Lumia 2520 tablet because the charger could produce an electric shock. The Finnish tech giant also advised current Lumia users to stop using the charger until further notice.

Barron’s Mailbag June 1962: Irving Kahn On False Comparisons The following letter from Irving Kahn appeared in the June 25, 1962, issue of Barron’s. Irving Kahn wrote to Barron's criticising the publication’s comparison of the 1962 market crash to that of 1929. Irving Kahn points out that based on volume and trading data, the 1962 decline was a drop in the ocean compared to Read More

Nokia tablet chargers are a safety hazard

Nokia Corporation (NYSE:NOK) (BIT:NOK1V) (HEL:NOK1V) ‘s Lumia 2520 tablet was sold in select markets including Austria, Germany, Russia, the United Kingdom, Denmark, Finland, and Switzerland. It is still being sold in the United States however the travel charger is no longer for sale. The tech company is advising everyone to not use the travel charger.

The Lumia maker admitted there is a possible product quality issue with specific AC-300 chargers that could lead to safety problems. These chargers were manufactured by third-party suppliers, which means Nokia Corporation (NYSE:NOK) (BIT:NOK1V) (HEL:NOK1V) has no control over the design or manufacturing.

Nokia added, “Under certain conditions, the plastic cover of the charger’s exchangeable plug could come loose and separate. If loose and separated, certain internal components pose a hazard of an electric shock if touched while the plug remains in a live socket.”

Nokia hopes to correct the problem

Unfortunately without the chargers, the tablets are essentially useless. However, the website’s Frequently Asked Questions page, the company apologizes for any inconvenience and is working with urgency for a solution.

In other news, the Nokia Lumia 930 is set to arrive next month. This phone features decent specs and a decent screen. Mashable elaborated in a recent blogpost, “Once you turn on the screen, however, you can tell this phone has something a little more special than your average Lumia.The 5-inch display has full HD (1,920 x 1,080) resolution, and it really pops.This is about as small as a 1080p Windows Phone can comfortably be. The live tiles would just be too tiny at smaller sizes. Apps, photos and media-rich websites look excellent on the screen. Those sites load fast, too. Calling up the New York Times website — the desktop version, complete with photos, ads and the stock ticker — took just a second or two, thanks in large part to the phone’s Qualcomm Snapdragon 800 chip.”