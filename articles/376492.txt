From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Wouldn’t it be nice if family planning were as simple as turning on the TV? Well, it might be in the near future, thanks to a contraceptive computer chip slated for potential public sale sometime in 2018.

Created by MicroCHIPS, a Massachssetts-based drug delivery implant developer, the chip is to be inserted under a woman’s skin and can be activated and deactivated by remote control. It works by administering a low dose of the hormone levonorgestrel once a day.

The really impressive part? The chip is good for 16 years; only then would you need another visit to the doctor’s office for replacement.

The chip measures at just 20mm x 20mm x 7mm and contains a 1.5cm microchip within it, where the hormone is stored. Each hormone dose is just 30 micrograms and is administered thanks to an electric charge, powered by an internal battery, which causes a seal around the hormone to melt and allows it to be released into the body.

Remote control operation is wireless but is designed to be tamper-proof. That is, someone wouldn’t be able to turn off your birth control from across a room. Rather, the remote must be practically touching the skin to work. All communications between the remote and the chip are encrypted, too.

The project is backed by the Bill & Melinda Gates Foundation, which got its start in 2000 and often funds innovative health projects. It also funded a campaign back in 2012 to improve contraception access in developing countries.

via: BBC News