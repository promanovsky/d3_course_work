Elon Musk's SpaceX Dragon successfully docks with International Space Station and delivers more than two tons of much-needed supplies to orbiting astronauts

The private spacecraft delivered food, gear, experiments and other supplies to the astronauts

SpaceX has been contracted by NASA to run deliveries to and from the ISS

The supply capsule docked with the ISS about 260 miles over Egypt

SpaceX's billionaire CEO, Elon Musk, called the docking a 'happy day' for he and his client: NASA

The crew of the International Space Station on Sunday took delivery of a special Easter treat: a cargo ship full of supplies.

The SpaceX company's cargo ship, Dragon, spent two days chasing the ISS following its launch from Cape Canaveral. Astronauts eventually used a robot arm to capture the capsule, 260 miles above Egypt.

More than two tons of food, spacewalking gear and experiments fill the Dragon, including mating fruit flies and legs for the station's resident robot. Nasa also packed family care packages for the six spacemen.

Success: SpaceX's Dragon delivery capsule successfully docked with the ISS to deliver supplies to the astronauts

Supplies: More than two tons of food, spacewalking gear and experiments were delivered to the ISS by the Dragon

Special delivery: NASA astronaut Richard Mastracchio looks on as the Dragon docks with the ISS

On Wednesday, the stakes will be even higher when the two Americans on board conduct a spacewalk to replace a dead computer. Nasa wants a reliable backup in place as soon as possible, even though the primary computer is working fine. The backup failed on 11 April.

The SpaceX delivery was delayed more than a month. A minor communication problem cropped up during Sunday's rendezvous, but the capture took place on time.

SpaceX flight controllers, at company headquarters in Hawthorne, California, exchanged high-fives, shook hands, applauded and embraced once Japanese astronaut Koichi Wakata snared the Dragon with the station's robot arm.

'Great work catching the Dragon,' radioed Nasa's Mission Control in Houston. 'Thanks for getting her on board.'



SpaceX's billionaire CEO, Elon Musk, said it was a 'happy day' for he and his customer: NASA

Got ya: This image shows the Dragon capsule just before it is captured by the Candarm2 to bring it in to the ISS

The Dragon will remain attached to the space station until mid-May. It will be filled with science samples – including the flies – for return to Earth.

Nasa is paying SpaceX as well as Virginia's Orbital Sciences to regularly stock the orbiting lab. These commercial shipments stemmed from the 2011 retirement of the space shuttles. This was the fourth station delivery carried out by SpaceX. Russia, Japan and Europe also make occasional deliveries.

This was the second launch attempt this week for SpaceX after a month's delay.

On Monday, NASA's commercial supplier was foiled by a leaky rocket valve. The valve was replaced, and the company aimed for a Friday liftoff despite a dismal forecast. Storms cleared out of Cape Canaveral just in time.

SpaceX's billionaire chief executive officer, Elon Musk, was delighted with the successful launch for NASA, the customer. 'This was a happy day,' he told reporters from company headquarters in Hawthorne, Calif.

Hello down there: The Dragon docked with the ISS about 260 miles above Egypt on Sunday morning

Launch: The SpaceX Falcon 9 rocket is one of the first reusable rockets in existence and was used to bring the supply capsule to the ISS

Last Friday, a critical backup computer failed outside the space station, and NASA considered postponing the SpaceX flight. The primary computer is working fine, but numerous systems would be seriously compromised if it broke, too. A double failure also would hinder visits by the Dragon and other vessels.



'It's imperative that we maintain' backups for these external command-routing computer boxes, also called multiplexer-demultiplexers, or MDMs, said flight director Brian Smith said Friday. 'Right now, we don't have that.'



NASA decided late this week to use the gasket-like material already on board the space station for the repair, instead of waiting for the Dragon and the new, precision-cut material that NASA rushed on board for the computer swap. Astronauts trimmed their own thermal material Friday to fit the bottom of the replacement computer, and inserted a fresh circuit card.



The space station's crew watched the launch via a live TV hookup; the outpost was soaring 260 miles above Turkey at the time of ignition. Video beamed down from Dragon showed the solar wings unfurling.



The SpaceX Dragon resupply capsule connecting with the ISS about 260 miles above the Pacific Ocean

Berthed: This grab was taken as the SpaceX Dragon begins the process of being berthed onto the ISS

The shipment is close to five weeks late. Initially set for mid-March, the launch was delayed by extra prepping, then damage to an Air Force radar and, finally on Monday, the rocket leak.



Earlier, as the countdown entered its final few hours, NASA's space station program manager Mike Suffredini said an investigation continues into the reason for last summer's spacesuit failure. The helmet worn by an Italian astronaut filled with water from the suit's cooling system, and he nearly drowned during a spacewalk.



Routine U.S. spacewalks are on hold until engineers are certain what caused the water leak. The upcoming spacewalk by the two Americans on board is considered an exception because of its urgent nature; it will include no unnecessary tasks, just the 2½-hour computer swap.



NASA is paying SpaceX — Space Exploration Technologies Corp. — and Virginia's Orbital Sciences Corp. to keep the orbiting lab well stocked. It was SpaceX's fourth trip to the space station. Russia, Japan and Europe also make periodic deliveries.

