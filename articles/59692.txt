A comprehensive new U.S. study has found one in every 68 children now falls in the autism spectrum, a shocking 30-per-cent increase since 2012 that Canadian experts say mirrors rising prevalence of autism here.

The U.S. Centers for Disease Control and Prevention released the data on Thursday, calling for an immediate action plan to avert "a pressing health crisis that must be prioritized at the national level."

Autism Speaks Canada executive director Jill Farber likened the 30 per cent jump to a "public health crisis."

Story continues below advertisement

In 2012, autism affected one in 88 children (one in 54 boys and one in 252 girls) compared with Thursday's findings of one in 68 (one in 42 boys and 189 girls). The figures reflect the fact that boys are four times more likely than girls to have autism.

Ms. Farber said the prevalence of autism in Canada is in sync with that of the United States, although she added that this country lacks the hard empirical data to prove that.

"We've used their prevalence rates up to now because we don't track it in the same way, whether that's because we have a less dense population or because we haven't put the money into the database structure. Public Health Canada has said they plan on doing a prevalence study, but nothing has happened yet," she said.

"We have no firm numbers on how many adults, children and adolescents have autism spectrum disorder, but we get thousands of calls, e-mails and requests for services, for help and assistance. The one in 68 is an astounding number, but there are other things we should also be alarmed about," she added.

Research has shown that early intervention can make a huge difference in a child's development. But children with autism are still being diagnosed at four- to four-and-a-half years old.

"We need to move that needle down to intervene and diagnose earlier," Ms. Farber said.

Experts say autism can be identified and diagnosed as early as age two.

Story continues below advertisement

It is not clear why the autism prevalence metre has jumped, but Lyndon Parakin, executive director for Autism Calgary, has a theory.

"There is a growing knowledge and awareness of autism on all levels, which means people are being identified earlier than in the past," he said. "There are also studies that have argued that a drop in diagnoses of other developmental disabilities or mental health conditions is contributing to some of the increase in the autism spectrum. But because no one has amalgamated and consolidated data, it's hard to answer why the incidence of autism is growing."

Like Ms. Farber, he said studies and organizations are starting to collect statistics, but no central body is keeping track of the prevalence of autism in Canada.

"It's a hard undertaking because every province is different in how they identify people with special needs," he said.

April is Autism Awareness Month, with April 2 designated as Autism Awareness Day.

"We're asking people to spread the word because we desperately need to bring greater awareness," said Carrie Habert, marketing director for Autism Speaks Canada.

Story continues below advertisement

"Autism doesn't distinguish between racial, economical or cultural boundaries," Ms. Farber added. "It affects everybody, so it's an area we have to focus on."

Editor's note

An earlier version of this article referred to autism as an illness instead of a disorder. This has been corrected.