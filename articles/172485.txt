Comcast Corp. ( CMCSA ) has reached an agreement with Charter Communications Inc. ( CHTR ) to divest 3.9 million subscribers as a strategic decision to ease the regulatory concern over its proposed merger with Time Warner Cable Inc. ( TWC ).

In Feb 2014, Comcast had reached an agreement with Time Warner Cable to acquire the latter in an all-stock deal valued at around $45.3 billion. Liberty Media Corp. ( LMCA ), which controls a 27.3% stake in Charter Communications, was also aggressively pursuing the idea of Charter Communications taking over Time Warner Cable. However, Charter Communications lost to Comcast in the bid.

The merged entity of Comcast and Time Warner Cable will have around 33 million pay-TV (video), 32 million high-speed broadband (Internet) and 16 million telephony (voice) subscribers. Thus, the deal is expected to face tough scrutiny and close monitoring by the Federal Communications Commission (FCC) and is expected to close within a year.

In order to avoid antitrust restriction, Comcast has decided to divest around 3.9 million Time Warner Cable video subscribers to maintain its total market share at 30% of the U.S. pay-TV industry. Comcast is expected to derive a significant $1.5 billion of operating synergies from this merger of which 50% may be realized within the first year of merger.

The Comcast-Charter deal has been divided into three parts. First, Charter Communications will acquire 1.4 million Time Warner Cable subscribers for $7.3 million in cash after the merger of Comcast and Time Warner Cable closes.

Second, approximately 1.6 million existing Charter Communications and Time Warner Cable subscribers will be swapped in a tax-efficient exchange to expand the geographic reach of both entities. Third, Comcast will spin-off another 2.5 million subscribers as a new entity in which Comcast will hold a 67% stake and Charter Communications will acquire the remaining 33%.

The total deal size will stand at approximately $20 billion. Following the subscriber takeover, Charter Communication will become the second largest pay-TV operator in the U.S. with around 5.7 million subscribers.

We believe that this deal will benefit both the companies. Comcast will be able to avoid antitrust measurers. The company has already assured the FCC that its merger with Time Warner Cable will not result in higher prices for cable TV and high-speed Internet packages.

On the other hand, Charter Communications will achieve necessary scale to remain competitive in the intensely competitive U.S. pay-TV market.

Currently, Comcast, Time Warner Cable, Charter Communications and Liberty Media, all carry a Zacks Rank #3 (Hold).

CHARTER COMM-A (CHTR): Free Stock Analysis Report

COMCAST CORP A (CMCSA): Free Stock Analysis Report

LIBERTY MEDIA-A (LMCA): Free Stock Analysis Report

TIME WARNER CAB (TWC): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.