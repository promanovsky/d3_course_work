Rapper Benzino is in a critical condition after he was shot while attending his mother's funeral.

The hip-hop star and reality Tv personality, real name Raymond Scott, was travelling in the funeral procession in Duxbury, Massachusetts on Saturday morning (29Mar14) when he was hit by a bullet to the shoulder.

Police believe Benzino's nephew, Gai Scott, carried out the shooting, pulling up in another vehicle alongside his uncle's car and taking aim at him through the window following a dispute.

The rapper is now receiving treatment in the South Shore Hospital.

A statement from his representative reads, "He was shot in the shoulder while attending his mom's funeral... At this time we ask that you keep Benzino in your prayers as we are hoping for a speedy recovery."

Scott has been charged with armed assault with intent to murder in connection to the shooting. He is due to appear in Plymouth District Court on Monday (31Mar14).