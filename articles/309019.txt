Samsung is known for launching many variants of their flagship devices, which often end up as exclusive devices on different carriers. Take the case of the Samsung Galaxy S5 Active, which is a rugged version of the Samsung Galaxy S5 with physical buttons.

AT&T managed to score the Samsung Galaxy S5 Active, but this year, Sprint is going to carry another version of the flagship device, dubbed as the Samsung Galaxy S5 Sport.

Sprint will exclusively carry the device, and will hit retail shelves from July 25 in two color options: Electric Blue and Cherry Red. Sprint is also offering $50 discount on the Samsung Gear Fit to customers who will purchase the Samsung Galaxy S5 Sport from July 25th onwards.

“We continually innovate to provide our customers with new products that can improve their lives,” said Dan Hesse, Sprint Chief Executive Officer, today during an event in Chicago. “Previously, Sprint brought together Harman Kardon, HTC and Spotify to redefine how music should sound on the modern smartphone. Today, Sprint joins leading innovators in Samsung, Under Armour and MapMyFitness to drive technology advances for fitness, health and wellness.”

As far as the specifications are concerned, the device offers the same hardware as the Samsung Galaxy S5, but with a different look. There’s a slightly different design, and the device also features physical buttons. As for the internals, it also comes with a 5.1-inch 1080p display, a quad-core Snapdragon 801 SoC with 2GB of RAM and 16GB internal storage with microSD card support.

There’s a 16MP camera for photos and videos, as well as a 2MP front-facing camera, with a 2,800 mAh battery. On the software side, it comes with Android 4.4.2 KitKat as its operating system. As far as the durability is concerned, the device comes with IP67 certification making it water and dust-resistant, but it’s not as rugged as the Samsung Galaxy S5 Active.

Customers interested in the device will be able to grab it for $0 down (plus tax) and 24 monthly payments of $27.09, starting July 25th. Are you planning on getting yourself a Galaxy S5 Sport.

Source: Sprint, GSMArena

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more