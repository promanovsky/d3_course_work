The Dow Jones Industrial Average (DJINDICES:^DJI) was trading 80 points lower, or 0.47%, by midafternoon. One of the most discussed developments today focused on details of the minutes from the June meeting of the Federal Open Market Committee. The minutes, released Wednesday afternoon, showed that Fed officials agreed that if the economy continues improving the central bank would in October announce a final $15 billion reduction in monthly bond buying. If that final reduction takes place, Fed officials would then plan how to begin selling off the holdings, which would send interest rates higher. Short-term interest rates have hovered near zero since December 2008.

Wall Street has paid little attention to the potential for rising rates. If there's a small market pullback when interest rates eventually rise, investors would be wise to keep long-term investing strategies in mind with the understanding that the economy is gaining strength.

With that in mind, here are some industrial giants making major headlines today.

Inside the Dow, Boeing (NYSE:BA) is making some noise today with its latest 20-year Current Market Outlook. The aerospace giant projects global demand for 36,770 new airplanes over the next two decades, which is a 4.2% increase from last year's forecast. Those forecast aircraft are estimated to be worth a staggering $5.2 trillion, of which Boeing seems positioned to earn more than its fair share.

Boeing's 4.2% forecast spike was more bullish than recent years' reports; the 2013 report projected a 3.8% spike over two decades, while the 2012 outlook estimated a more modest 1.5% rise in global demand for aircraft. Behind Boeing's increased forecast was growing demand in Asia, more specifically China: The nation is expected to overtake the U.S. as the world's largest aviation market over the next two decades.

Boeing anticipates that the Asia-Pacific region will take 13,460 new airplane deliveries over the next 20 years, which is far ahead of second-place North America with 7,550 expected deliveries.

Boeing also expects single-aisle jets to account for more than 25,000 of the 36,770 forecast airplanes over the next 20 years.

Boeing's forecast comes ahead of next week's Farnborough International Airshow in the U.K., which is expected to generate massive orders for both Boeing and rival Airbus.

In other industrial news, Ford (NYSE:F) announced a small 100,610 recall earlier this week. Roughly 92,000 vehicles have front axles that might not have been installed properly during assembly. According to Ford, no injuries, crashes, or deaths are linked to the issue.

The automotive industry has already set a new record for the most recalls in a single year, with nearly two-thirds coming from America's largest automaker, General Motors. Surprisingly, vehicle sales, especially GM's, haven't been negatively affected, and the industry is continuing its resurgence since sales collapsed during the recent recession.