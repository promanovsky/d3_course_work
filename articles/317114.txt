The US economy shrank much more than previously estimated in the first quarter, in the sharpest contraction in five years, the Commerce Department says.

Gross domestic product fell at a 2.9 per cent annual pace in the first three months of the year, much worse than the previous estimate of 1.0 per cent.

It was the steepest contraction since the 2009 first quarter, when GDP plunged 5.4 per cent during the deep recession spawned by the 2008 financial crisis.

The Commerce Department said showed weaker growth in consumer spending, a larger increase in exports and higher imports than previous estimates.

The economy was hit by unusually severe winter weather in much of the country at the beginning of the year, after growth of 2.6 per cent in the 2013 fourth quarter.