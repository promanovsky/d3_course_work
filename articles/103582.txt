"The Amazing Spider-Man 2" doesn't come out until next month but we're already seeing a ton of early reviews for the sequel.

Although the reviews seem positive, with a very healthy 86% on Rotten Tomatoes, many reviewers have noted its overriding focus on setting up sequels and spin-offs - in other words an attempt to replicate Disney's superhero success at theaters.

That may leave a bit of a bad taste in moviegoer's mouths.

Advertisement

For example:

IGN:

"There's great action and compelling performances - Garfield is Spider-Man - but there's also an obvious pressure to rapidly expand the Spider-Man universe that threatens to derail the film at points."

Advertisement

Total Film:

What all this means is that t he film often seems more focused on franchise-building than storytelling, nowhere more so than in its final few scenes."

Advertisement

"The intention is clear - Sony wants their own expanded universe to rival Marvel, since Spidey joining the Avengers will remain the stuff of Garfield and other fans' dreams. Paul Giamatti's Rhino and Felicity Jones's Felicia Hardy are given glorified cameos clearly designed to pay off down the line, in ASM3 or ASM4 or the already-promised Sinister Six spinoff.

The Playlist:

"It's wildly overstuffed. Sony seems to have taken the lesson from the mammoth success of "The Avengers" that people want an abundance of characters in their superhero movies, but the script from J.J. Abrams acolytes Jeff Pinkner, Alex Kurtzman and Roberto Orci flits around from plotline to plotline shapelessly, and the result is something bloated, that at 141 minutes, is way too long."

Advertisement

"Plans are already afoot for installments 3, 4 and Venom and Sinister Six spinoff movies, and that feels like a big a part of the problem here -Amazing Spider-Man 2 seems more like an exercise in calculated franchise architecture than its does a cohesive stand-alone blockbuster."

Since "Captain America: The Winter Soldier" it feels like every impending superhero movie will ultimately have some sort of set up toward a larger franchise.

Advertisement

Many reviews praise director Marc Webb's action sequences - a NYC Times Square scene that's been

stands out - and the romantic storyline between Andrew Garfield and Emma Stone on screen.

Still, the movie, out in theaters May 2, should be an enjoyable one.

It should also be a successful film for Sony.

Estimates are already calling for

opening weekend.

Advertisement

Check out a trailer below: