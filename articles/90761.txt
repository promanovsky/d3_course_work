The percentage of mothers who stay home with their children is rising due to shifting demographics and a troubled job market.

The portion of stay-at-home mothers with children under age 18 rose to 29% in 2012 from 23% in 1999, according to a new Pew Research Center analysis of government data. Prior to that, the share of stay-at-home moms had declined for three decades as women in general flooded into the workforce.

The increase in home-based mothers is driven partly by the desire to focus on children. But it’s also partially a result of economic hardship for those lower on the financial rungs.

PHOTOS: World’s most expensive cities

Advertisement

One-third of stay-at-home mothers live in poverty, compared with 12% for their counterparts who work outside the home. Stay-at-home mothers also are more likely than working moms to be immigrants and less likely to be white, according to the report.

“For a variety of reasons — from a tough job market to changing demographics — we’re seeing an uptick in the share of mothers who are staying at home,” said D’Vera Cohn, one of the report’s authors.

“Stay-at-home mothers are a diverse group,” Cohn said. “They are younger and less educated than their working counterparts, and more likely to be living in poverty. Many are staying at home to care for family, but some are home because they can’t find jobs, are enrolled in school or are ill or disabled.”

The financial status of stay-at-home mothers depends partly on marital status.

Advertisement

Married stay-at-home moms are generally better educated and less likely to live in poverty, according to Pew. The vast majority of married stay-at-home mothers — 85% — say they stay home to care for their families, though a rising portion — 6% — complain that they can’t find a job.

Single or co-habitating mothers, by contrast, are likelier to be at home because they are unemployed, disabled or enrolled in school.

Currently, 28% of American children are being raised by a stay-at-home mother. That’s up from 24% in 2000 but down from 48% in 1970, according to Pew.

Follow Walter Hamilton on Twitter @LATwalter