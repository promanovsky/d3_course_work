Big sodas can stay on the menu in the Big Apple after New York state's highest court refused Thursday to reinstate the city's first-of-its-kind size limit on sugary drinks. But city officials suggested they might be willing to revisit the supersize-soda ban.

The Court of Appeals found that the city Board of Health overstepped its bounds by imposing a 16-ounce cap on sugary beverages sold in restaurants, delis, movie theaters, stadiums and street carts. The appointed board trod on the policy-making turf of the elected City Council, the court said.

"By choosing among competing policy goals, without any legislative delegation or guidance, the board engaged in lawmaking," the court wrote in a majority opinion. "... Its choices raise difficult, intricate and controversial issues of social policy."

Indeed, debate over the soda size cap pitted health officials who called it an innovative anti-obesity tool against critics who considered it unfair to businesses and paternalistic toward consumers. Even a Court of Appeals judge, during arguments earlier this month, wondered aloud whether regulators would target triple-decker burgers next.

The American Beverage Association, which led the legal fight against the measure, welcomed Thursday's ruling against a measure it said would have "limited New Yorkers' freedom of choice." Curbing obesity should start "with education — not laws and regulation," spokesman Christopher Gindlesperger said.

But city leaders signaled they might not give up the fight. Mayor Bill de Blasio said the city was "actively reviewing all of its options to protect the health and well-being of our communities"; officials wouldn't immediately specify what those might be. The city hasn't said whether it plans to try to appeal, but the case doesn't raise federal issues that would make it a likely choice for the Supreme Court.

City Council Speaker Melissa Mark-Viverito said in a statement that the big-soda ban would get a hearing if it were brought up in the council. It's unclear how any such measure might fare in a vote, as she and numerous other council members oppose it.

New Yorkers, meanwhile, greeted Thursday's ruling with mixed feelings.

"I think it's up to the individual" what size of soft drink to buy, said Constance Jong, a cashier in her 20s.

But Hazel Plunkett, a 51-year-old fundraiser for a public health group, was disappointed that the regulation remains blocked.

"I don't mind the controversy over it. It's got to get people's attention," she said.

Soda has been under fire for years from health advocates, who say the beverages are uniquely harmful because people don't realize how much sugar they're guzzling. A 21-ounce Coke, McDonald's medium size, has 200 calories and 55 grams of sugar, for instance.

Amid the publicity, U.S. soda sales have dropped for nearly a decade. But consumption of other sugary beverages, such as sports drinks and energy drinks, has grown.

Former Mayor Michael Bloomberg made the soda ban a signal piece of his assertive public-health agenda.

"Due to today's unfortunate ruling, more people in New York City will die from obesity-related impacts," he said in a statement Thursday.

A trial court struck down the measure days before it was to take effect last year. Some eateries had already swapped out cups and adjusted menus to comply, and some proprietors decided to stick with the changes. But others were glad Thursday that they hadn't.

"I thought it was ridiculous" — and unfair, said midtown Manhattan cafe manager Young Shin, 30. The measure would have applied to his workplace but not to bars and groceries under state regulation, including 7-Eleven, the home of the Big Gulp.

Lawmakers and health advocates around the country have proposed soda taxes in recent years, but none has succeeded. A California measure that would have slapped warning labels on sodas was recently defeated.

Meanwhile, Coke and Pepsi have rolled out smaller cans and bottles, some as small as 7.5 ounces.