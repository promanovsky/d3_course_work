A South Carolina teen was shocked when he was told by the DMV he would have to remove his makeup to get his driver’s license picture taken. According to Fox Carolina, Chase Culpepper was told he needed to look more like a boy before the picture could be taken because the DMV had rules against “disguises” being used in photos. The rules state, “at no time will an applicant be photographed when it appears that he or she is purposefully altering his or her appearance so that the photo would misrepresent his or her identity.”

WYFF4 notes that this rule was used to justify asking the teen boy to remove his makeup for his photo. South Carolina Department of Motor Vehicles spokesperson Beth Parks said it’s important for a license picture to portray who the person is legally.

When this young man has to show his ID, his ID shows that he’s male. The card says he’s male [and therefore], he needs to look like a male.

Culpepper isn’t buying that excuse and said he thinks people should be able to walk into the DMV and take a picture as they truly are, without regard to gender stereotypes. Chase said, “This is how I am every day. And if a police officer wanted to recognize how I am, then, he would want to see who I am in my picture as well.”

The rule clearly states that the applicant must not alter their appearance to misrepresent his identity, so isn’t that what the DMV forced Culpepper to do? His mother, Teresa Culpepper, certainly thinks so:

They said he was wearing a disguise. It was very hurtful. He was absolutely devastated. That’s who he is 24/7.

Teresa is upset that the state feels it is their place to tell her son what a male should look like. She feels her son’s identity was removed when they made him remove the makeup. If the photo is supposed to represent the person, Chase is best represented with his makeup on. He wears the makeup to school and work and feels that is the way his license photo should be portrayed as well.

The Transgender Legal Defense and Education Fund agrees, and is calling upon the South Caroline DMV to allow Culpepper to retake his picture. TLDEF executive director Michael Silverman in the letter to the DMV stated:

Chase’s freedom to express his gender should not be restricted by DMV staff. He is entitled to be who he is and to express that without interference from government actors. Forcing Chase to remove his makeup prior to taking his driver’s license photo restricts his free speech rights in violation of state and federal constitutional protections.

However, transgender sex change surgery is now covered by Medicare.

Do you think the DMV overstepped their boundaries in this case and should have allowed Culpepper to wear makeup in his driver’s license photo?