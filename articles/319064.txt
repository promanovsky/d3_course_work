Google kicked off I/O, its annual developers' conference, on Wednesday with the traditional keynote address where the company showed off its latest products. However, the presentation was interrupted - twice - by protestors inside the hall, while a stream of activists registered their displeasure with the company's activities outside the venue, Moscone West in San Francisco.

About 45 minutes into the keynote, a woman walked up near the stage holding a banner that seemed to say, "Develop a Conscience: Stop Jack Halprin From Evicting SF Teachers." Jack Halprin is a Google lawyer who was was accused of trying to evict people under the Ellis Act in San Francisco. The woman was apparently later identified as Claudia Tirado, one of the eviction victims.

The woman was removed from the venue, and David Burke, the Google engineer on stage at the time, even managed to throw-in a joke at her expense, saying, "Battery Saver [the feature he was talking about at the time] is really great if you're about to embark on a long hike or, say, a long protest, and you want the battery to last even longer."

That wasn't all. Two hours into the keynote, another protestor, a male, walked near the stage and started shouting. "You all [Google employees] work for a totalitarian company that is building machines that will kill people," he said, in an apparent reference to Google's purchase of Boston Dynamics.

The man was also removed from the hall, but not before he asked the audience to "wake the f* up."

Even before the keynote began, activists began gathering outside, continuing their protest well into the day.