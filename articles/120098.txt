Researchers have said that young fathers are at an increased risk of depression.

Depressive symptoms increased on average by 68 percent over the first five years of fatherhood for these young men, who were around 25 years old when they became fathers and whom lived in the same home as their children.

This study is the first to identify when young fathers are at increased risk of developing depressive symptoms.

Lead author Craig Garfield, M.D., an associate professor in pediatrics and medical social sciences at Northwestern University Feinberg School of Medicine, said it's not just new moms who need to be screened for depression, dads are at risk, too, asserting parental depression has a detrimental effect on kids, especially during those first key years of parent-infant attachment.

This paper used data collected from 10,623 young men enrolled in the National Longitudinal Study of Adolescent (Add Health). It includes a nationally representative sample of adolescents in the U.S. and follows them in several waves over nearly 20 years into young adulthood.

The results have been published in the journal Pediatrics.