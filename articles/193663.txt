By Alina Selyukh

WASHINGTON (Reuters) - More than 100 technology companies, including Google Inc, Facebook Inc, Twitter Inc and Amazon.com Inc, have written to U.S. telecom regulators to oppose a new "net neutrality" plan that would regulate how Internet providers manage Web traffic.

The letter to Federal Communications Commission Chairman Tom Wheeler and the agency's four commissioners, warning of a "grave threat to the Internet," came as one FCC commissioner called for a delay of a vote on the plan scheduled for May 15.

"Rushing headlong into a rulemaking next week fails to respect the public response to his (Wheeler's) proposal," Commissioner Jessica Rosenworcel said on Wednesday in remarks prepared for delivery at an industry meeting. She called for a delay of the vote to formally propose Wheeler's plan by "at least a month."

Wheeler has been under fire for proposing new so-called "open Internet" or "net neutrality" rules that would allow content companies to pay broadband providers for faster Internet speeds delivering their traffic as long as the deals are deemed "commercially reasonable."

Consumer advocates are worried the rules would ultimately allow Internet companies such as Comcast Corp or Verizon Communications Inc to create "fast lanes" on the Web for traffic of content companies that pay up, potentially shutting out poorer newcomers.

The latest to weigh in is the consortium of technology and Internet companies, which ranged from household names to small startups. They called on the FCC to "take the necessary steps to ensure that the Internet remains an open platform for speech and commerce."

Commission rules should not permit "individualized bargaining and discrimination," the companies said.

Engine Advocacy and New America's Open Technology Institute, long-time supporters of Open Internet policies, helped organize the effort.

FCC spokesman Neil Grace said Wheeler does not plan to delay the May 15 vote. Though Wheeler has written and spokes about his proposal, the proposed rules have not been made public in full.

"Moving forward will allow the American people to review and comment on the proposed plan without delay, and bring us one step closer to putting rules on the books to protect consumers and entrepreneurs online," Grace said in a statement.

Tens of thousands of public comments have been received by the FCC on Wheeler's plan over the past two weeks, and commission staff has met with nearly 100 stakeholders, including public interest groups and Internet content providers.

With two Republican commissioners broadly opposed to regulation of Internet traffic, the support of two Democrats on the panel - Rosenworcel and Mignon Clyburn - is critical for Wheeler's proposal to pass.

"I am listening to your voices as I approach this critical vote to preserve an ever-free and open Internet. ... My mind remains open as I continue to evaluate how best to promote these fundamental, core values," Clyburn said, reiterating her opposition to pay-for-priority arrangements.

To read Rosenworcel's comments, go to http://bit.ly/1uC4JHu

To read Clyburn's comments, go to http://fcc.us/1s4ukEN

A handful of protesters gathered in the rain in front of the FCC headquarters in Washington on Wednesday afternoon, vowing to show up daily until the May 15 vote, when a high-profile demonstration is expected.

Consumer advocates have long urged the FCC to reclassify Internet service providers as more highly regulated utilities, like the telecom companies, a view that has faced staunch opposition from Republican lawmakers and broadband companies.

(Reporting by Alina Selyukh; Editing by Ros Krasny, Sandra Maler, Jan Paschal, Richard Chang and Mohammad Zargham)