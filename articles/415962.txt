Jay Z and Beyoncé may just have 99 problems after all.

Rumors have been swirling for weeks that the power couple’s relationship has been on the rocks, but at Sunday’s MTV Video Music Awards, they tried to at least present a united front.

As Beyoncé finished her 15-minute set, hubby Jay Z — carrying daughter Blue Ivy — presented her with the Video Vanguard Award onstage. Queen Bey said a few words to the crowd, she embraced her family, and then they headed home. (Hopefully without having to travel in any elevators.)

To many viewers, the family’s appearance felt like something cooked up in a p.r. boardroom in an attempt to counter the reports of their imminent split.

Beyoncé seemed to shed tears, but were they tears of joy for her award or of sadness over the unraveling of her six-year marriage?

“I see a lot of hurt and pain here,” says Florida-based body language expert Susan Constantine. “They’re just trying to save face. They feel no emotions toward each other,” adds Constantine, who examined a video of the appearance as well as this still photo (at left).

The body language expert, who also works as a jury consultant, says that pictures can sometimes be more telling than videos. Discovering how someone truly feels is about analyzing their “microexpressions” — brief, sometimes involuntary movements of the face that betray emotions.

“Jay Z and Beyoncé are completely distant, and their minds are on something else,” she says. “Beyoncé feels really uncomfortable.”

Constantine says the superstar’s body language indicates she might be going through emotional trauma and hasn’t reached the acceptance stage yet. Hence the discomfort.

Even 2-year-old Blue Ivy might be trying to tell us something.

“I see this sense of sadness. It’s almost like Beyoncé is looking at her going, ‘It’s OK,’ ” Constantine says. “And the baby is saying, ‘This doesn’t seem right.’ Children are very perceptive.”

Of course, that uneasiness on the baby’s part might just be about appearing on a stage in front of thousands of screaming fans. Or maybe she was just thinking about Miley Cyrus’ performance from last year.

Beyoncé’s face

“She’s giving a false smile,” Constantine says. If she were really happy, her eyebrows would be lifted and her eyes would be more open.

Jay Z’s face

“This is what we call a flat face,” Constantine says. “There’s no expression whatsoever.” Happiness would be indicated by upturned corners of the mouth. But he’s less strained than Beyoncé.

Eyes

“Neither one of them is looking at each other,” says body language expert Susan Constantine. “Their interest is more in their daughter.”

Beyoncé’s hand over midsection

She’s blocking her stomach area — the home of emotions in the body — indicating a feeling of vulnerability. Were she feeling comfortable, she’d expose her midsection.

Beyoncé’s hip

Her hips are angled away from her husband, indicating disassociation. “In other [older] photos, you see Beyoncé leaning into her husband,” Constantine says.

Blue Ivy

Her hands are clasped in a prayer-like way. Happiness would be indicated by raised arms and an extended body. Instead, she’s making herself small. “Maybe she’s praying her parents get back together,” Constantine says.