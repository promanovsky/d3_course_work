A sudden outburst of violence in Iraq has seen oil traders send Brent crude prices to near-nine-month highs amid fears over the country's oil supplies, but BP's chief economist says increased production in the United States will help keep oil prices relatively stable.

Both Brent and WTI were slightly higher in early Monday trade, though they also wobbled briefly into negative territory as well.

Iraq produces about 3.3 million barrels a day, and so far the only reported disruption is the flow of oil through the 600,000 barrel Kirkuk-Ceyhan pipeline, which runs from Kirkuk to Turkey. The Kirkuk fields produce about 400,000 to 500,000 barrels a day, while the major fields in the Basra area produce about 2.6 to 2.7 million barrels a day, according to IHS.

Christoph Ruehl told CNBC ahead of the release of BP's 2014 statistical review that between 2011 and 2014 we've seen the most stable oil price in a three-year period since the 1970s. U.S. shale growth matched the many supply disruptions including those caused by the Arab Spring, he said.