Calling your cable service provider is usually a headache, but tech journalist Ryan Block's experience was far worse than your typical support call.

When Block and his wife Veronica Belmont attempted to cancel their Comcast service over the phone, the representative incessantly demanded to know why.

Advertisement

The representative was more than persistent - he sounded aggressive and inappropriate based on the 8-minute snippet from the conversation Block posted online via SoundCloud. The portion of the call posted on SoundCloud starts from 10 minutes in, which means the entire phone call lasted for about 20 minutes.

Even after Block said he didn't want to state why he chose to leave Comcast, the customer service rep kept pushing.

Advertisement

"Why don't you want faster speed?" the rep asked.

Block patiently and calmly asked if it was possible for the representative to disconnect his service by phone, asking for a simple yes or no answer. Rather than answering, the rep demanded to know why, clearly sounding agitated and a little angry.

Advertisement

Block eventually got his service disconnected over the phone, but still went to the store anyway to double check, according to his Twitter . You can listen to the full phone call below.