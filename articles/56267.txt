After a week of travel in the Mideast, U.S. Secretary of State John Kerry changed course and arrived in Paris Saturday for talks with his Russian counterpart on the Ukraine crisis.

Halfway home from a trip to Riyadh, Saudi Arabia, Kerry landed in Shannon, Ireland, for a refueling stop, when he decided to turn his plane around and headed to Paris. Kerry is to meet with Russian Foreign Minister Sergei Lavrov Sunday evening at the Russian ambassador's residence.

Kerry spoke to Lavrov on the flight to Shannon after President Barack Obama and Russian President Vladimir Putin agreed in a call on Friday to have their foreign ministers meet to discuss a possible diplomatic resolution to the Ukraine situation.

Confirmation of the meeting comes hours after Lavrov said on Russian television that Russia had "no intention" of invading eastern Ukraine, responding to Western warnings over a military buildup on the border following Moscow's annexation of the Crimean peninsula.

Russian Foreign Minister Sergei Lavrov insisted on Saturday that Russia has 'no intention' of pushing farther into eastern Ukraine, despite an estimated 50,000 troops currently amassed on the Ukrainian border. (Baz Ratner/Reuters) Lavrov also reinforced a message from Putin that Russia would settle — at least for now — for control over Crimea despite massing an estimated 40,000 troops near Ukraine's eastern border.

"We have absolutely no intention of — or interest in — crossing Ukraine's borders," Lavrov said.

The call between Putin and Obama was believed to have been the first direct conversation between Obama and Putin since the U.S. and its European allies began imposing sanctions on Putin's inner circle and threatened to penalize key sectors of Russia's economy.

The White House and the Kremlin offered starkly different summaries of the call, which occurred while Obama was travelling in Saudi Arabia. The contrasting interpretations underscored the chasm between how Moscow and Washington perceive the escalating international standoff.

U.S. President Barack Obama says Russia's military moves might be no more than an effort to intimidate Ukraine, but also could be a precursor to other actions. (Francois Lenior/Reuters) White House officials described the call as "frank and direct" and said Obama had urged Putin to offer a written response to a diplomatic resolution to the Ukraine crisis that the U.S. has presented. He urged Moscow to scale back its troop build-up on the border with Ukraine, which has prompted concerns in Kyiv and Washington about a possible Russian invasion in eastern Ukraine.

The Kremlin, on the other hand, said Putin had drawn Obama's attention to a "rampage of extremists" in Ukraine and suggested "possible steps by the international community to help stabilize the situation.”

Klitschko drops out of presidential race

Ukraine remains deeply divided over protests that led to Yanukovich's ousting and many eastern Russian-speaking regions are skeptical about the policies of the new pro-Western government in Kyiv.

Yanukovich called on Friday for each of the country's regions to hold a referendum on their status within Ukraine, instead of the presidential election planned for May 25.

That election is shaping up as a context between former prime minister Yulia Tymoshenko and billionaire confectionary oligarch Petro Poroshenko, after boxer-turned-politician Vitaly Klitschko withdrew on Saturday.

Speaking on Saturday, Poroshenko said the political forces that brought down Yanukovich must stick together to tackle the huge economic and security challenges facing Ukraine.

"I'm convinced it would be a betrayal of Maidan [anti-government protest movement] if we were not united," he told a meeting of Klitschko's party, Interfax news agency reported.

"I'm convinced that today the volume and scale of the challenges facing the state ... demand this kind of unity."

'The Chocolate King' Poroshenko

Poroshenko, whose net worth was estimated by Forbes at $1.3 billion, is the owner of Roshen, one of the world's top twenty confectionery firms but which has borne the brunt of trade sanctions from Russia since last year.

He is an experienced politician, having held several ministerial posts including a brief stint as economy minister under Yanukovich.

Confectionery oligarch Petro Poroshenko, seen here on the right, is now a top contender in the upcoming Ukrainian presidential election, after protest leader and former professional boxer Vitaly Klitschko, middle, dropped out of the race. (Paul Hackett/Reuters) A prominent backer of the 2004-05 Orange Revolution against the election fraud and alleged corruption of Ukraine's post-Soviet establishment, Poroshenko campaigned for the role of prime minister in its wake, but lost out to Tymoshenko, who co-led the revolution.

Klitschko urged his supporters to back Poroshenko, and announced he would run instead for mayor of the capital. So far, no other candidate is seen mounting a serious challenge to the two front runners.

The parliamentary faction Party of Regions, Ukraine's former ruling party, also announced its candidate on Saturday.

Deputies voted for Mykhailo Dobkin, a businessman and former governor of the eastern city of Kharkiv, who fiercely opposed the Maidan uprising.

'Deep constitutional reform'

Lavrov called for "deep constitutional reform" in Ukraine on Saturday, a sprawling country of 46 million people divided between those who see their future in closer ties with Europe and mainly Russian speakers in the east who look to former Soviet master Russia.

"Frankly speaking, we don't see any other way for the steady development of the Ukrainian state apart from as a federation," Lavrov said.

Each region, he said, would have jurisdiction over its economy, finances, culture, language, education and "external economic and cultural connections with neighbouring countries or regions".

There was also a bid for regional devolution within Crimea. Its Tatar community, an indigenous minority who were persecuted under Soviet rule and largely boycotted last month's referendum on joining Russia, want autonomy on the Black Sea peninsula, the Tatar leader said on Saturday.