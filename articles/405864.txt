One of the biggest momentum names, LinkedIn (NASDAQ: LNKD), reports second quarter earnings after Thursday's close.

Earnings per share are expected to be $0.40 with $510 million in revenue. While LinkedIn, trading recently at $181.53, has rallied nearly 28 percent off the May lows of $141, shares are still down 16 percent year to date, and well below the all-time highs of $257.56 set on September 11, 2013.

Although revenue growth looks to remain strong at nearly 40 percent year over year, the question for investors will be how that revenue growth will translate into earnings growth, especially given that LinkedIn has a market cap of $22 billion. With the Fed now tapering, much of the easy money tailwinds that fueled gains in momentum names is not quite as evident.

Forward guidance, therefore, should be especially crucial, with consensus for third quarter revenue at $541 million. Any disappointment in the guidance could lead to further weakness in LinkedIn shares.