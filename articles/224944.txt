Very sick people in Colorado have new options as of this past weekend. A state law passed on Saturday gives terminally ill patients the ability to seek experimental treatments that have yet to be approved by the U.S. Food and Drug Administration. And with FDA approval averaging 11 to 14 years for a new drug, skipping the wait could mean a lot for someone who has only months to live.

The “Right to Try Act”—the very first to pass in the U.S.—was signed into law by Colorado’s governor after passing the state legislature unanimously. Similar bills are coming up for approval in Arizona, Louisiana, and Missouri. Each bill would allow patients, in partnership with their physicians, to request experimental medications that are still being tested.

“This is such a paradigm shift for how we approach terminally ill patients’ access to medication in this country,” says Lucy Caldwell, communications director for the conservative public policy and advocacy organization Goldwater Institute, which is behind the current bills.

More from Prevention: Alternatives For The Top 10 Prescribed Medications

Nationwide, people with a terminal illness for which available treatments aren’t working can seek a Compassionate Use Exemption to be included in a clinical trial of a drug or procedure that might help them. Of the 550 requests the FDA received for such exceptions during the 2013 fiscal year, all were approved. “The FDA is strongly supportive of the appropriate use of expanded access,” says Sandy Walsh, a spokesperson for the agency. But many complain that the process is long and mired in paperwork. The new law would allow terminally ill Colorado patients to request treatments without any special exemptions or enrollment in a trial.

Critics of the legislation note that the FDA approval process exists to protect patients from harm, and that true “miracle” drugs are few and far between. Colorado patients will have access to drugs that have passed “Phase 1″—known as the safety phase—of clinical trials. The treatments need not have passed subsequent tests to show whether they are effective or safe in a larger population.

More from Prevention: 20 Ways To Lower Your Cancer Risk

Some 30% of drugs that begin the approval process pass the first phase of clinical trials (which usually include a very small numbers of patients—often between 20 and 80). But only 8% ultimately win approval, and far fewer are truly lifesaving. Some critics worry that opening up unapproved drugs for use will undermine the standards and safeguards the government has put in place to keep patients from needless suffering.

“The FDA has not taken a position on any particular ‘Right to Try’ bill,” says Walsh. “However, the agency is concerned with any efforts that would be inconsistent with its Congressionally mandated authority to protect the public from therapies that are not safe and effective.”

In Colorado, drug companies are under no obligation to provide the drugs, and patients may have to pay for them out of pocket.

“We don’t know what the level of efficacy is going to be—that’s where they’re rolling the dice,” Caldwell says of people who might request experimental drugs. But she notes that for terminally ill patients, side effects might be of little concern: “There’s a side effect that’s certain if I don’t take this drug,” she says. “And that’s death.”

More from Prevention: Measles Vaccine Cures Woman Of Cancer

The article was written by Katherine Harmon Courage and originally appeared on Prevention.com

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.