All things being equal, the more useful a product, the more sales it would generate. Products deemed lifesaving fall into a category all by themselves.

Google (NASDAQ: GOOG) Glass might be such a product – at least when it comes to medical and military applications.

The Google Glass device Dr. Steven Horng, at Boston’s Beth Israel Deaconess Medical Center wore last year while working on a man with a bleeding brain helped Dr. Horng save the man’s life according to Dr. Horng and reported by The Boston Globe.

Dr. Horng knew the man was allergic to certain drugs, but didn’t know which ones. He called up the man’s medical records while tending to him, discovered and avoided those drugs, and stabilized the man’s condition. All without having to leave the room.

Related: Google Glass Is Going To Work

Because of Dr. Horng’s experiences with Glass, Beth Israel Deaconess Medical Center decided to expand its use of the device. Emergency room doctors now wear Glass during their shifts. QR codes posted on patient room doors provide access to their records.

The Glass device doctors at Beth Israel have been using isn’t standard issue. The software was removed and replaced with an Android version created by a company called Wearable Intelligence. The device cannot be used off the medical center’s Wi-Fi network and patient information is not shared with Google.

There are other examples of medical application of Glass. Surgeons at the Indiana University Health Methodist Hospital used the device in February to call up MRI scans while removing an abdominal tumor.

To provide additional oversight when residents perform procedures, experienced doctors at the University of California-Irvine Medical Center have been able to monitor those procedures when residents wore Glass devices.

Meanwhile, the medical field isn’t the only area where lives could potentially be saved.

Military Applications

At Wright Patterson Air Force Base in Dayton, Ohio, a research team has been beta-testing Google Glass for possible use on the battlefield.

According to Second Lt. Anthony Eastin, Glass has demonstrated several positive features including “its low power, its low footprint, it sits totally above the eyes, and doesn’t block images or hinder vision.”

The BATMAN evaluation group, part of the U.S. Air Force 711th Human Performance Wing at Wright-Patterson, is one of the military’s top research units.

So far, the team has concluded that Glass could be useful for forward air controllers working on the ground helping vector fighter and bomber aircraft to their targets. Other potential life-saving uses include search and rescue missions and combat troops communicating with aircraft flying overhead.

With both medical and military applications, Google has said it had no involvement and had none planned. This position may help alleviate concerns about privacy as well as national security.

At the time of this writing, Jim Probasco had no position in any mentioned securities.