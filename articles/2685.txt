The Bachelor finale is set to air with Juan Pablo Galavis picking between Clare Crawley and Nikki Ferrell, but spoilers indicate a twist ending to what has been an all-around controversial season.

Though Juan Pablo started out as a fan-favorite pick to serve as the latest Bachelor, some of his actions this season have worn thin with viewers. First there were remarks he made in January that having a gay Bachelor would be “too perverted,” a comment that prompted ABC to respond that “Juan Pablo’s comments were careless, thoughtless and insensitive, and in no way reflect the views of the network, the show’s producers or studio.”

Then there have been instances on the show the paint him as a womanizer, including a fantasy suite date with Andi where Juan Pablo seemed not too interested in learning about her. That ended with Andi calling out Juan Pablo for his behavior before taking herself off the show.

The Bachelor finale spoilers indicate an even bigger twist to end the show.

[WARNING: SPOILERS AHEAD]

For one, the blogger Reality Steve seems to think that Juan Pablo is more interested in himself than either of the two remaining women. While Steve does say that Juan Pablo ends up picking Nikki, but not actually proposing to her.

Steve, who has successfully predicted the ending of all but one season so far, wrote:

“Make no mistake about this season, and you can choose to believe me or not – this was a complete cash grab for Juan Pablo. He never had any interest in finding a wife on this show, or a step mother for Camila, or whatever else line of BS ABC will run at you all season. This guy wants to act, he wants to be in commercials, he wants to model, and he wants to be on Dancing with the Stars. Come the spring season, you can pretty much bet your a** that Juan Pablo will be on DWTS, even though he can’t dance a lick.”

Reports from after The Bachelor finale seem to bolster the idea. In February, Juan Pablo was seen at a Miami Heat game with singer Mayra Veronica, with witnesses saying the two were acting very much like a couple.

Another source told Life & Style magazine that Juan Pablo has been seen cozying up with at least three different women since the show wrapped, and none of them were the final two women on the show. The source also said that Juan Pablo doesn’t seem to have much direction in his life.

“The cash is running out – fast,” the show insider told Life & Style magazine. “Nobody seems to know how Juan Pablo makes a living. During the week he spends his time going to the gym or the beach. His life in Miami seems rather mundane.”

Fans who want to see if The Bachelor finale spoilers come true can tune in to the episode, which airs Monday at 8 pm ET on ABC.