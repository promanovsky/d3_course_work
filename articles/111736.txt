Jenny McCarthy has finally responded to the huge backlash she received when revealing her controversial anti-vaccination stance.

The television personality was thought to be against vaccinated children ever since her son was diagnosed with the condition in 2005 at age 2.

McCarthy allegedly believed getting the vaccination could have led her child to suffer from the condition.

The 41 year-old has finally cleared up what her actual thoughts on vaccinating children are, in a column she wrote for the Chicago Sun-Times, published Saturday (April 12th).

"I am not 'anti-vaccine,'" McCarthy wrote as the column's opening. "This is not a change in my stance nor is it a new position that I have recently adopted. For years, I have repeatedly stated that I am, in fact, 'pro-vaccine' and for years I have been wrongly branded as 'anti-vaccine.'"

"My beautiful son, Evan, inspired this mother to question the 'one size fits all' philosophy of the recommended vaccine schedule," she continued, referring to the many years she has attempted to argue certain vaccinations cause the condition in young children.

Last month McCarthy was blasted on Twitter by social media users who claimed she was 'anti-vaccination', when she was tweeting about a completely unrelated topic.

"What happened to critical thinking? What happened to asking questions because every child is different?" McCarthy asked. "I embarked on this quest not only for myself and my family, but for countless parents who shared my desire for knowledge that could lead to options and alternate schedules, but never to eliminate the vaccines."

"For my child, I asked for a schedule that would allow one shot per visit instead of the multiple shots they were and still are giving infants." She wrote.

MORE: Jenny McCarthy slams claims that she lied about her son's autism

"I believe in the importance of a vaccine program and I believe parents have the right to choose one poke per visit. I've never told anyone to not vaccinate," McCarthy made her stance clear. "Should a child with the flu receive six vaccines in one doctor visit? Should a child with a compromised immune system be treated the same way as a robust, healthy child? Shouldn't a child with a family history of vaccine reactions have a different plan? Or at least the right to ask questions?"

'The View' co-host concluded with: One size does not fit all," adding, "God help us all if gray is no longer an option."



McCarthy and her son, Evan