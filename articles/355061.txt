THE world was watching – and now events in Dublin may place Garth Brooks' comeback in jeopardy.

Millions of the country icon's fans throughout the world – from the home of country music, Nashville, to cities throughout Europe – had been hoping the capital was to be the launchpad for a comeback world tour.

His legions of dedicated Irish fans might be greatly disappointed after news broke that all five of his Irish concerts were in danger of being cancelled.

But there are fears that it might impact on the much anticipated world tour. Fans eagerly clicked into the website of the country superstar yesterday but the only message uploaded was suitably cryptic – "The wait is over ... 7/7."

Previously Brooks told Nashville publication 'Country Aircheck': "There is something that could be happening between now and the tour that might be the biggest thing I've ever tried to take on."

The power of Brooks and the excitement with which a world tour would be greeted is not to be underestimated.

Back in December 2010, Garth Brooks sold out nine benefit shows for the recovery of the Nashville Flood.

Cancelling any shows in Dublin could hit the expected world tour.

The singer is well known for making sure his fans are the most important people in his career. There is one thing that fans can be sure of – Garth Brooks will not give up until his promise is kept to give the best show he can to his fans.

Irish Independent