General Mills lawsuits have been given a new legal precursor. If you like the company on Facebook, you’re not allowed to sue them. So what’s stopping us from simply “unliking” General Mills before heading out to see a lawyer?

It is apparently now a legal issue to show any favor toward a product. Even using downloaded and printed General Mills coupons is the equivalent of telling them they aren’t responsible for any problems you have with them. After connecting your name favorably to the company behind General Mills cereal products like Cheerios, Lucky Charms, or Cocoa Puffs, you have legally rescinded your right to sue them.

According to the website, General Mills lawsuits are not allowed and all disputes must be handled through informal means such as emails if you show any kind of favor which could be used as a paper trail, such as entering contests. Even sending an email could disqualify you from using legal means to show your lack of favor. This includes even brands that fall under the General Mills name, such as Yoplait, Betty Crocker, and Nature Valley.

You’re Not Allowed to Sue General Mills If You Like Them on Facebook http://t.co/F6Tm4qMSk3 — The Technology Tweet (@tumblingeek) April 17, 2014

The official wording is as follows:

“We’ve updated our Privacy Policy. Please note we also have new Legal Terms which require all disputes related to the purchase or use of any General Mills product or service to be resolved through binding arbitration.”

The only way you can legally file any General Mills lawsuits is apparently if you don’t buy their products or show them any favor of any kind. How long will it be before Kellogg’s or Malt-O-Meal does the same thing?

This new policy was put in place after two mothers sued the company, claiming that Nature Valley granola bars, though marketed as “natural,” still contained high-fructose corn syrup and genetically modified ingredients.

Julia Duncan, an arbitration expert at the American Association for Justice, says that General Mills is “essentially trying to protect the company from all accountability, even when it lies, or say, an employee deliberately adds broken glass to a product.”

Jaw-dropping. @GeneralMills TOS now claims liking its products on Facebook waives right to sue over *anything.* http://t.co/iNicTOuv1S — William McGeveran (@BillMcGev) April 17, 2014

This means that technically, the food manufacturer can do whatever it wants as long as you don’t interact with them in any way that can legally connect you to favoring them. If you happen to find something deadly in their food, you can only take legal action if you can’t prove you bought it, and if you haven’t “liked” them on Facebook, followed them on Twitter, et cetera.

Could this new clause on General Mills lawsuits be a disturbing new trend in food manufacturers across the US? It’s enough to make you want to spend more and buy organic if you don’t already.