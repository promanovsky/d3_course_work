Kim Kardashian had her heart set on being married at Versailles, but those running the palace made it clear they didn’t want a Kimye wedding at the historic location.

Kim Kardashian and Kanye West may have just been given a huge dose of reality about how important they actually are after Versailles turned down their request to hold their wedding at the palace.

A spokesperson for the palace told the Daily Mail that Kimye was not “considered distinguished enough” to hold their wedding there. But they will be enjoying some time there the night before their wedding, and can “enjoy a guided tour… after all the tourists have gone home.”

Catherine Pegard, president of the Palace of Versailles, said a Kimye wedding at the historic site just “wasn’t possible.”

“Kim Kardashian and Kanye West have decided to visit the Chateau of Versailles with their guests in a private surprise tour organised for Friday, May 23, on the eve of their marriage,” said Pegard. “In making this choice, they will contribute once more to a better understanding of Versailles, and will help to maintain the exceptional heritage of Versailles, which is classed as a world heritage site by Unesco.”

After rumors of a Versailles wedding, Kardashian was later seen taking a tour of another French chateau, but it looks like they finally settled on Florence as the site of their wedding.

“I adore Florence. I love Italy and the Italian lifestyle,” West recently told Florence’s La Nazione newspaper. “To tell you the truth, I already came to the banks of the (Arno) river with Kim last year. Just the two of us, incognito. I think that our daughter North was conceived here among the Renaissance masterpieces.”

A spokesperson for the Mayor of Florence confirmed the couple has rented a location in the Italian city for nearly $350,000.