William Shatner performs at the The 2013 – 82nd Hollywood Christmas Parade Benefiting Marine Toys for Tots Foundation held in the Hollywood section of Los Angeles on December 1, 2013. UPI/Phil McCarten | License Photo

LOS ANGELES, April 29 (UPI) -- William Shatner -- the actor famous for playing Captain James Tiberius Kirk on NBC's "Star Trek" and for his spoken-word cover of Elton John's song "Rocket Man" -- was bestowed with a Distinguished Public Service award by NASA over the weekend, the highest honor given to a non government employee.

“William Shatner has been so generous with his time and energy in encouraging students to study science and math, and for inspiring generations of explorers, including many of the astronauts and engineers who are a part of NASA today," David Weaver, NASA’s associate administrator for the Office of Communications, said in a statement.

Advertisement

Shatner has been an unofficial spokesperson for the space agency for many years. In addition to playing one of television's most beloved space pioneers in Captain Kirk, Shatner more recently narrated a NASA documentary celebrating the space shuttle's 30th anniversary, and also voiced a video preview of the Mars Curiosity mission.

Over the years, Shatner has even recorded personalized messages for astronauts aboard various NASA missions.

As NASA's Weaver added: "He's most deserving of this prestigious award."