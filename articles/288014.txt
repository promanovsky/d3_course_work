“Little” Jimmy Scott, a jazz singer whose a unique and unforgettable alto voice made him a legend to true aficionados but an acquired taste for others, is dead at age 88, family friends said Friday.

Sometimes termed the “father of falsetto,” Jimmy Scott’s unusually high pitch was the result of a rare genetic condition known as Kallman Syndrome, which prevents those born with the disease from going through puberty.

As result of Kallman Syndrome, “Little” Jimmy Scott grew to only 4’11” tall and remained at that stunted height until age 37, when he suddenly experienced a growth spurt that increased his height by eight inches. But the condition left him with his signature high-pitched singing voice.

In addition to the genetic disease, Jimmy Scott experienced a number of often cruel setbacks. His mother passed away when Scott was just 13. In 1949 he sang on Lionel Hampton’s hit record “Everybody’s Somebody’s Fool.” But Hampton would not put Scott’s name on the recording, robbing the singer of recognition that could have propelled his career forward.

Nonetheless, Jimmy Scott continued to record for the next decade but in the 1960s, he became too fed up and frustrated with the music industry and returned to his native Cleveland — where he was born on July 17, 1925 — and went to work as a clerk in a hotel.

The last straw for Jimmy Scott was the recording of a 1963 album, Falling In Love Is Wonderful, produced by Ray Charles, who was one of Scott’s most ardent supporters in the music business. But Charles wanted to issue the LP on his own label, Tangerine. At the time, Scott was under contract to Savoy Records and that label’s boss, Herman Lubinsky, brought legal action that killed distribution of the album.

Now considered a classic, Falling In Love Is Wonderful was reissued in 2003, as Scott was enjoying a comeback of sorts that began in 1991 when he sang at the funeral of songwriting legend Doc Pomus, writer of such hits as “This Magic Moment,” “Viva Las Vegas” and many others.

Among Jimmy Scott’s champions were Lou Reed — who brought “Little” Jimmy on tour and considered him “the most extraordinary voice I’ve ever heard in my life” — as well as David Lynch, who featured Scott in the highly-rated final episode of his TV series Twin Peaks in 1992. The appearance helped relaunch Jimmy Scott’s career, which continued almost until his death.

But Jimmy Scott never achieved the success that most hardcore music fans believe he deserved. The New York Times called him “the most unjustly ignored American singer of the 20th century.”

Jimmy Scott died at his home in Las Vegas on June 12. The cause of death was not announced. View his appearance on Twin Peaks, below.