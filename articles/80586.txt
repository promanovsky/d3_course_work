Zee Media Bureau

New Delhi: The Reserve Bank of India on Tuesday kept the key interest rates unchanged in its monetary policy review as retail inflation, especially in food items, is yet to show definite signs of moderation.

Overlooking demands of the industry, the RBI left the short-term lending (repo) rate and the Cash Reserve Ratio (CRR) steady at 8 percent and 4 percent, respectively.

In a bid to curb inflation, RBI in January raised the key repo rate by 0.25 percent to 8 percent.

Strengthening of the rupee against dollar in the past few days following inflow of foreign currency has put pressure on exports. In addition, unseasonal rains in March may end up stoking food inflation in the near term.

The annual rate of inflation, based on the monthly Wholesale Price Index, was 4.68 percent in February. Retail inflation, based on the Consumer Price Index, was at 25-month low of 8.1 percent.

Raghuram Rajan, who took charge as Governor of the central bank last September, raised the rates during his first policy announcement, rightly foreseeing a pressure on the inflation front.

He increased it again in January - for the third time since he took charge - when the market was expecting a pause.

Even though the RBI has not formally adopted inflation targeting, it has gone public on targeting CPI-based inflation down to 8 percent by January 2015 and further down to 6 percent by January 2016, as per the recommendations of the Patel committee.

With agency inputs