When the Coachella Valley Music and Arts Festival launched 15 years ago, creature comforts included any small slice of shade cast by the odd light pole, the occasional working toilet and maybe somewhere to sit other than the dusty desert floor.

Today, there are $799 VIP passes that give you cocktail bar privileges, even though you’d have to pay extra for privileged parking. Well-heeled foodies can splurge on a $1,024 gourmet dining package. Shell out more, and you can skip the traffic — round-trip transportation is available via private jet.

These pricey add-ons are emblematic of larger shifts for the event, which runs the next two weekends on the manicured grounds of Indio’s Empire Polo Club.

Acclaimed in the early 2000s for its free-spirited vibe, Coachella is now more akin to a spring break weekend at a walled-off resort than to an edgy music festival. High-end car companies and fashion brands stage elaborate pool parties throughout the area, including nearby Palm Springs. Some say the music has become the soundtrack to a vacation — rather than the draw.

Advertisement

INTERACTIVE: Plan your Coachella schedule

The blowout, expected to attract approximately 90,000 fans per three-day weekend, typically sells most of its tickets before the lineup of bands is even announced.

“It doesn’t matter who the artist is onstage,” said Gary Bongiovanni, editor of the concert-industry trade magazine Pollstar. In the increasingly crowded festival scene, he added, Coachella is distinguished by its “special accouterments” and “Hollywood presence.”

A steerage-level ticket for one weekend costs $375 — about $100 more than similar passes for Chicago’s Lollapalooza, San Francisco’s Outside Lands or Tennessee’s Bonnaroo. The prices for various deluxe packages go up thousands from there. Factor in travel, lodging and food, and the expense is on par with that of a stay at an upscale spa.

Advertisement

The festival is about “attainable aspiration,” according to Ellie Meyer of Marketing Werks, a Chicago firm that consults with music events. “It’s a lifestyle that people want to have. It’s saying, ‘Hey, you can pay for that lifestyle for a weekend.’”

PHOTOS: All of Coachella’s past headliners

Festival-goers can enjoy concierge service in an air-conditioned “safari tent” for two (for a price: $6,500). And if the combination of sunscreen and 90-degree-plus heat wreaks havoc on attendees’ appearance, they can pop into the Sephora Collection Beauty Studio for a cosmetic touch-up.

Such posh festival accessories are surprising even some artists slated to perform this year.

Advertisement

Dee Dee Penny of Dum Dum Girls had this reaction when asked what she thought of Coachella’s $225 four-course meals prepared by celebrity chefs.

“No,” she said. “Really? Now I’m intrigued.”

The all-inclusive Coachella experience — with a designer boutique, craft beer gardens and art installations — enables pleasure seekers to buy into resort vacationing without admitting they’ve become their cruise-taking parents. (Coachella, by the way, launched a cruise in 2012.)

Marlo Nielsen, 42, and two friends stayed in a safari tent at Coachella last year.

Advertisement

TIMELINE: Coachella and Stagecoach music festivals

“They create a really nice atmosphere for you,” said the human resources worker from Calgary, Canada. She especially appreciated the swimming pool and private restrooms and showers; there’s also dedicated parking and on-site security.

Tie-ins with upscale brands add another elite tier to the proceedings.

Lexus, Sonos and Marc Jacobs have sent out VIP invitations to parties. And although tickets to the festival sold out months ago, members of Neiman Marcus’ preferred shopper program could still buy them in early April — for $1,500.

Advertisement

Once there, they’ll find sushi and small-plate tapas from swanky restaurants such as Sugarfish, Night + Market and Crossroads. Night-life maven Cedd Moses is setting up smaller versions of several of his popular Los Angeles establishments, including Honeycut and Caña Rum Bar.

Some things have not changed. The festival is still presented by its original promoter, Goldenvoice. However, the L.A. company is now a unit of Anschutz Entertainment Group, the sports and concert giant whose other holdings include Staples Center and the Los Angeles Kings NHL hockey team.

In the decade-plus since Goldenvoice was bought by AEG, its footprint has expanded to booking such venues as the Roxy and the Shrine Auditorium and building Stagecoach, the Coachella festival’s country music cousin.

The growth of the company’s flagship festival almost mirrors that of Goldenvoice itself: from a small promoter of punk rock shows to one of the most powerful forces in live music on the West Coast.

Advertisement

Some say the glamour and ambience come at a cost for a festival once known for its spirit of artistic adventure.

RELATED: Coachella 2014 by numbers

“There certainly is a loss of edge or whatever you want to call it,” said Jon Wurster of the long-running indie rock band Superchunk, which played Coachella in 2009 and is set to appear again Sunday. “It’s just a totally different thing.”

Another of this year’s performers, Blair Greene of Washed Out, worries that the “next level” amenities at Coachella could reset priorities among concert-goers — and lock out many others. If she weren’t on the bill, Greene said, the festival wouldn’t be in her budget.

Advertisement

“The experience is important, but I’d rather have more and better music,” she said.

Yet the resort-like transformation of Coachella probably will only escalate, according to some experts.

“There’s no stopping it,” said Marketing Werks’ Jay Lenstrom, who compared the festival to Formula 1 racing in Europe. “There’s a race, but there’s all the other things. It’s very expensive. The more expensive it is, the better it is.”

What’s in store for next year? No word yet, although Goldenvoice President Paul Tollett said he’s “always trying to enhance Coachella on all fronts.”

Advertisement

With an agreement in place that allows Goldenvoice to put on Coachella in Indio through at least 2030, it’s a safe bet that the lush life at this desert festival will only get lusher.

todd.martens@latimes.com

mikael.wood@latimes.com



