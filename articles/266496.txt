Being too cynical may increase the risk of dementia in later life, a study suggests.

People who display high levels of "cynical distrust" - the belief that others are mainly motivated by selfish concerns - are three times more likely to develop Alzheimer's and other forms of dementia than those with low levels, scientists found.

Researchers used a questionnaire to measure cynicism levels in 1,449 people with an average age of 71 who were also given two tests for dementia.

A total of 622 participants completed the tests over an average period of eight years. During that time, 46 individuals were diagnosed with dementia.

The scientists adjusted their findings to take account of factors known to influence dementia risk, including raised blood pressure and cholesterol, and smoking.

Of the 164 people with high cynicism scores, 14 developed dementia compared with nine of the 212 with low levels of cynicism.

Study leader Dr Anna-Maija Tolppanen, from the University of Eastern Finland, said: "These results add to the evidence that people's view on life and personality may have an impact on their health.

"Understanding how a personality trait like cynicism affects risk for dementia might provide us with important insights on how to reduce risks for dementia."

The findings are reported in the latest on-line edition of the journal Neurology.

Belfast Telegraph