In hopes of spurring innovation and "sustainable transport," Tesla CEO Elon Musk announced on Thursday he will not penalize any company that wishes to use its patented electric motor technology.

In a blog post, Musk wrote that he removed a wall of Tesla patents in the lobby of the company's lobby of its headquarters. The patents, he says, do more harm than good.

"Our true competition is not the small trickle of non-Tesla electric cars being produced, but rather the enormous flood of gasoline cars pouring out of the world's factories every day," he wrote.

In 2013, the International Organization of Motor Vehicle Manufacturers (OICA) reported that vehicle production reached 84.1 million vehicles worldwide. With that number on path to approach 100 million per year -- as well as 2 billion cars on the road by 2050 -- the market is too large for one company to handle, Musk says.

Tesla originally patented the technology out of fear that larger manufacturers would copy its innovations and use their automotive might to drive the company out of business. However, Musk soon realized that was not the case.

"The unfortunate reality is the opposite: electric car programs (or programs for any vehicle that doesn't burn hydrocarbons) at the major manufacturers are small to non-existent, constituting an average of far less than 1% of their total vehicle sales," he wrote.

Finding ways to curb carbon dioxide emitted by cars -- thought to be one of the main causes of global warming -- has become a hot topic. In 2013, governors in eight states, including California and New York, vowed to work together to create a network of charging stations and other infrastructure. The goal was to help get 3.3 million electric vehicles on the roads by 2025 to curb pollution. When China became the first country to sell more than 20 million cars in a single year, in 2013, some Chinese officials were criticized for allegedly downplaying the significance of auto emissions in China's air pollution crisis.

However, developing clean-powered cars has not been. According to the OICA, five to seven years are needed to design, test, build and introduce vehicles with new power units. These power units require extensive development and testing, government certification and redesigned automated production systems. Luckily, Tesla has already done much of this development and testing, so by allowing others to use its technology, Musk hopes to eliminate one of the hurdles stopping companies from manufacturing zero-emission cars.

"We believe that Tesla, other companies making electric cars, and the world would all benefit from a common, rapidly-evolving technology platform," he said.