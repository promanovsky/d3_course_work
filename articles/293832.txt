German ZEW economic sentiment falls to 18-month low in June

Investing.com - German economic sentiment deteriorated unexpectedly in June, hitting the lowest level since December 2012, industry data showed on Tuesday.

In a report, the ZEW Centre for Economic Research said that its index of German economic sentiment fell by 3.3 points to 29.8 this month from May’s reading of 33.1. Analysts had expected the index to increase by 1.9 points to 35.0 in June.

The Current Conditions Index improved to 67.7 this month from 62.1 in May, beating expectations for an increase to 62.6.

Meanwhile, the index of euro zone economic sentiment increased to 58.4 in June from 55.2 in May, below expectations for a rise to 59.6.

On the index, a level above 0.0 indicates optimism, a level below 0.0 indicates pessimism.

Following the release of that data, the euro was little changed against the U.S. dollar, with easing up 0.01% to trade at 1.3574.

Meanwhile, European stock markets remained higher. Germany's rose 0.8%, the advanced 0.4%, France’s added 0.45%, while London’s tacked on 0.25%.