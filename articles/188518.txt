Dollar gains as Ukraine crisis ebbs

Investing.com - The dollar edged higher against most major currencies on Wednesday after Russian President Vladimir Putin said he'd do everythign possible to diffuse the crisis in Ukraine, which bolstered the U.S. currency.

In U.S. trading on Wednesday, was down 0.09% at 1.3915.

Concerns that Ukraine will descend into civil war eased after President Putin called on separatists in the eastern reaches of the country to postpone their referendum on independence, and added that Russia had withdrawn its forces from the border.

Putin stressed that Russia will do "all it can" to resolve the crisis and will take a "most positive" approach to international peace efforts.

The crisis has taken its toll on the dollar by stoking fears the U.S. will become more involved, which could hamper recovery, though Putin's words supported the greenback.

The dollar did, however, see headwinds back home.

Federal Reserve Chair Janet Yellen said earlier that a high degree of monetary accommodation remains warranted given the slack still persistent in the economy, and added that while conditions in the U.S. labor market have improved, they remain far from satisfactory.

Yellen added that monetary authorities expect economic growth to accelerate this year despite the slowdown in the first quarter but warned that the recent housing market slowdown "could prove more protracted than currently expected."

Still, losses were limited, as expectations for interest rates to remain low even after the Fed wraps up its bond-buying stimulus program have been priced into trading.

The dollar was up against the yen, with up 0.11% at 101.79 and up against the Swiss franc, with up 0.21% at 0.8741.

The greenback was up against the pound, with down 0.13% at 1.6955.

The dollar was up against its cousins in Canada, Australia and New Zealand, with up 0.01% at 1.0893, down 0.14% at 0.9335 and down 0.79% at 0.8671.

The kiwi dropped after central bank Governor Graeme Wheeler warned against the currency's strength on Wednesday, stressing that "it would become more opportune for the Reserve Bank to intervene in the currency market to sell New Zealand dollars," in the face of worsening fundamentals.

The comments overshadowed data on Wednesday showing that the number of people employed in New Zealand rose more than markets were expecting in the first three months of the year, while the unemployment rate remained unchanged at 6.0%.

The , which tracks the performance of the greenback versus a basket of six other major currencies, was up 0.12% at 79.26.

On Thursday, the U.S., is to publish the weekly report on initial jobless claims.