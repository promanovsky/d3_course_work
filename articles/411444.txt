Does digital data offer indicators that can be used to monitor marketing effectiveness and predict box office success even before awareness turns into intent? We analyzed this weekend’s new movies across Facebook, YouTube, Twitter and Google (the methodology behind the numbers is laid out in the appendix below) over the seven days leading up to their release, when marketing campaigns should be at their peak.

“Guardians of the Galaxy”



Marvel Studios’ “Guardians of the Galaxy” enter the weekend off the back of San Diego Comic-Con, a huge marketing push and a chart of very impressive social numbers. However, as this is a new property with characters unknown to moviegoers (that may not matter since a sequel already was announced before its bow in theaters) it would be hard to expect Star-Lord-Peter Quill and his group of misfits to mix it up at the box office with the almighty team of Avengers, which was essentially a sequel to four different Marvel movies.

The characters are making their debut in the Marvel Cinematic Universe but the actors themselves are relatively familiar — the biggest names are Vin Diesel and Bradley Cooper, but they lend their voices to the movie rather than their faces, and Chris Pratt, Dave Bautista and Zoe Saldana are all well known, even if they aren’t on the same level as Robert Downey, Jr.

The new characters and their attributes have featured heavily on Facebook and Twitter in videos and graphics to establish familiarity with the audience, who also had the opportunity to explore the new universe through the Galaxy Getaways site. The cast and director James Gunn have also been pushed heavily, featuring in livestream chats such as BuzzfeedBrews and many extensive Q&As with Yahoo! and Tumblr, as well as Twitter using the hashtag #AsktheGuardians.

Despite all of this activity, “Guardians” is still behind the larger Marvel movies and blockbusters this year on Twitter, with 258,000 where “Thor: The Dark World” and “The Amazing Spider-Man 2” both clocked in at around 1 million tweets, but ahead of “Captain America: The Winter Soldier” which had 300,000. “Guardians” also has more trailer views than the “Captain America” sequel with over 100 million to his 77 million and “The Dark World’s” 60.5 million, a total boosted by cosplay featurettes and Lego trailers. Using search as an indicator of wider interest and awareness sees “Guardians” slightly behind the “Thor” and “Captain America” sequels with 365,000 to their 383,000 and 527,000, suggesting the “Guardians” will fall just short of “Winter Soldier,” but they should still fight their way to a total close to “The Dark World,” at around $80 million.

Final expectations: “Guardians of the Galaxy” looks set to be the beginning of another successful Marvel franchise and should secure between $75 and $85 million.

“Get On Up”

Universal’s James Brown biopic is counter programming to Marvel’s “Guardians,” looking to interest older moviegoers and tap into the ethnic audiences that made “The Butler” such a hit last year, which opened with $24.6 million and held strong.

Star Chadwick Boseman has already found success as Jackie Robinson in “42,” which opened to $27.4 million, and the sex machine musician is equally as well-known as the Dodgers’ infielder. While “Get On Up” doesn’t have “The Butler’s” Oprah seal of approval, Brown’s popularity is born out by the 5 million trailer views, much more than “Butler’s” 1 million or the 2 million posted by “Jersey Boys,” the Frankie Valli biopic.

Although search isn’t particularly telling for movies interesting ethnic audiences, “The Butler”’s wider appeal is demonstrated by its search volume over 150,000. Here the large trailer count should see “Get On Up” beat “Jersey Boys”’ $13.3 million on its way to a total at least in the high teens, if not over $20 million. However the mid and high 20s totals posted by “Butler” and “42” look to be out of reach.

Final expectations: The James Brown biopic should get on up to a total of around $20 million.

Tobias Bauckhage (@tbauckhage) is co-founder and CEO of Moviepilot, a fan-focused platform for movie geeks, cinephiles and everything in between, reaching over 10 million monthly unique users and over 15 million Facebook fans. Based on community data, Moviepilot helps studios to optimize their social media campaigns, identifying and activating the right audiences. The company works with studios like Sony, 20th Century Fox, Fox Searchlight, A24, CBS Films and Focus Features.

————

Appendix

Facebook fan (or like) numbers are a good indicator for fan awareness for a movie, even months before the release. For mainstream movies with younger target audiences, fan counts are particularly important. However, big fan numbers can be bought and movies with older target audiences typically have lower fan counts. Fan engagement measured by PTAT (People Talking About This) is a more precise but also a fickle indicator, heavily driven by content strategy and media spending. Both numbers are global and public facing numbers from the official Facebook fanpage.

YouTube trailer counts are important for measuring early awareness about a movie. We track all English language original video content about the movie on YouTube, down to videos with 100 views, whether they are officially published by a studio or published unofficially by fans. The Buzz ratio looks at the percentage of unique viewers on YouTube that have “liked” a video and given it a “thumbs up”. Movies with over 40 million views are usually mainstream and set to dominate the box office, while titles drawing 10 million to 20 million views indicate a more specific audience. If a movie does not have a solid number of trailer views on YouTube four weeks before its release, it is not promising news. But again, it is important to understand whether trailer views have been bought or grew organically. These numbers are global and public facing.

Twitter is a good real-time indicator of excitement and word of mouth, coming closer to release or following bigger PR stunts. Mainstream, comedy and horror titles all perform particularly strongly on Twitter around release. We count all tweets over the period of the last seven days before release (Friday through Thursday), that include the movie’s title plus a number of search words, e.g. “movie” OR a list of movie-specific hashtags. The numbers are global, conducted using a Twitter API partner service.

Search is a solid indicator for intent moving towards release as people actively seek out titles that they are aware of and are thinking about seeing. Search is particularly significant for fan-driven franchises and family titles as parents look for information about films they may take their children to see. We look at the last seven days (Friday through Thursday) of global Wikipedia traffic as a conclusive proxy for Google Search volume. We have to consider that big simultaneous global releases tend to have higher search results compared to domestic releases.