Malaysia Airlines Flight 370 vanished on March 8 with 239 people on board

A British firm that helped reveal the last position of missing flight MH370 has offered to provide a free global airline tracking service.

Analysis of data from Inmarsat and the UK Air Accidents Investigation Branch in March showed the Malaysian Airlines plane crashed in the southern Indian Ocean.

Inmarsat has now offered a tracking service to "virtually 100% of the world's long haul commercial fleet".

The London-based company made the announcement ahead of an International Civil Aviation Organization (ICAO) conference in Montreal, Canada today.

Rupert Pearce, chief executive of Inmarsat, said: "We welcome and strongly support ICAO's decision to place the delivery of next-generation aviation safety services at the heart of the industry's agenda at its meeting on 12 May.

"Inmarsat has been providing global aviation safety services for over 20 years and we are confident that the proposals we have presented to ICAO and IATA (International Air Transport Association) represent a major contribution to enhancing aviation safety services on a global basis.

"In the wake of the loss of MH370, we believe this is simply the right thing to do.

"Because of the universal nature of existing Inmarsat aviation services, our proposals can be implemented right away on all ocean-going commercial aircraft using equipment that is already installed."

Flight MH370 vanished on March 8 with 239 people on board, during a flight from Kuala Lumpur to Beijing.