Katy Perry has been accused of plagiarising another act on her recent hit Dark Horse.

Christian rap group Flame have gone legal over the alleged similarities between Perry’s track and their Joyful Noise.

The religious outfit have also complained that their song has been “irreparably tarnished by its association with the witchcraft, paganism, black magic and Illuminati imagery evoked by the same music in Dark Horse”.

According to The Guardian, the lawsuit was filed in St Louis, Missouri after DJ Cho'zyn Boy noticed tweets to Flame about the similarities and decided to compile an audio comparison to prove their resemblance.

“What listeners are hearing is Katy Perry's Dark Horse at 66 beats per minute and they're hearing Flame's Joyful Noise at 76 beats per minute,” the producer told Rapzilla.

“When they're separated, they seem a bit different, but when you bring them to the same tempo and you just change her pitch down one octave, they're identical . . . When things are that similar, it's hard to dispute.”

Perry has yet to respond to the filed lawsuit.

What do you think? Here are the two tracks: