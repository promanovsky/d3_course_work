There may be no more strikingly different debuts than the ones James Franco and Chris O’Dowd are making in “Of Mice and Men.”

As George, the wandering ranch hand in John Steinbeck’s hard-luck 1930s California, Franco is all surface, never giving us any insight into what drives him — he’s a very handsome blank.

O’Dowd, on the other hand, disappears into Lennie, George’s simple-minded friend. His newly shaved head and bushy beard help distance him from the amiable, goofy figure we knew from “Bridesmaids.”

But it’s more than that: O’Dowd helps us know and empathize with a gentle giant desperate for contact, one who can pet a puppy so hard that it dies.

“Dumb bastard like he is, he wants to touch everything he likes,” George tells one of their fellow workers. “Jest wants to feel of it.”

The scenes between the two men are key to the show, but they’re off-balance here. You can see why Lennie sticks with George, the protector who spins fairy tales about them buying a farm together. But Franco’s unable to make us understand what’s in it for George.

Lennie’s combination of childlike innocence and prodigious strength leads to a tragedy that — decades after Steinbeck wrote it in 1937 — hasn’t lost an ounce of intensity.

Director Anna D. Shapiro and her “August: Osage County” scenic designer, Todd Rosenthal, make the setting feel very real — this is a visually rewarding show — especially when the action moves to the men’s bunkhouse.

It’s a man’s man’s man’s world. They can be supportive, but in the end, everybody’s trying to mark their turf.

The worst is the boss’ son, Curley (Alex Morf), a bully who compensates for being short by picking fights. He’s also painfully aware that his pretty young wife (Leighton Meester, late of “Gossip Girl”) is a flirt who teases the men’s boundaries — down to visiting the quarters of the lone black employee (Ron Cephas Jones).

One of the most intense scenes involves elderly handyman Candy (a fine Jim Norton) and the sad-eyed mutt he’s had for years. Carlson (Joel Marsh Garland), a ranch hand who shares their cramped quarters, offers to take Candy’s dog outside and shoot it because, well, it’s old and useless, and it stinks.

The minute we spend waiting to hear that shot is almost unbearable.

It’s clear why Candy fears being cast off as well. As Steinbeck puts it so powerfully, this world isn’t for the weak.