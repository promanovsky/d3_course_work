Before NASA can send humans to Mars, as it hopes to do by the 2030s, it must look beneath the surface and study the planet's interior.

The agency and its international partners got the go-ahead for construction on a new lander after completing a successful Mission Critical Design Review on Friday. The Interior Exploration Using Seismic Investigations, Geodesy and Heat Transport (InSight) mission will investigate how Earth-like planets formed and developed, collecting information about the Red Planet's inner structures of core, mantle and crust.

Scheduled to be launched in March 2016 from Vandenberg Air Force Base in central California, InSight will be an international effort. Those involved include the French Space Agency, the Swiss Federal Institute of Technology, and the German Space Agency with members from Austria, Belgium, Canada, Spain and the United Kingdom.

InSight team leaders presented their mission design results to NASA last week, which then gave approval for advancing to the next stage of preparation.

Tom Hoffman, the InSight project manager of the Jet Propulsion Laboratory in Pasadena, Calif., said in a press release that they plan to begin building and testing in November.

The stationary lander will carry a robotic arm that will deploy surface and burrowing instruments, as well as built-in wind and temperature sensors that will be provided by Spain. A first for planetary exploration, InSight will include two new instruments, NASA said.

The two main instruments, the Seismic Experiment for Interior Structure -- from the space agencies of Switzerland, Germany and the U.K -- and Heat Flow and Physical Properties Package -- from the German national space agency -- will be included in the InSight lander. The SEIS will measure waves of ground motion carried through the planet's interior, from "marsquakes" and meteor impacts. The other instrument will measure heat that comes to the surface from Mars' interior.

"Mars actually offers an advantage over Earth itself for understanding how habitable planetary surfaces can form," Bruce Banerdt, the InSight principal investigator from JPL, said in the same statement. "Both planets underwent the same early processes. But Mars, being smaller, cooled faster and became less active while Earth kept churning. So Mars better preserves the evidence about the early stages of rocky planets' development."

Landing near the Mars equator, the three-legged InSight lander will be similarly designed to the 2008 Phoenix Mars spacecraft, which examined ice and soil on far-northern Mars. Originally scheduled to last for three months, the Phoenix mission lasted for almost six. Soil tests had revealed similarities with Earth's soil and the lander discovered evidence of water on the surface of the planet, sparking hope that the planet could support life.