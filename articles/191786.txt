On Tuesday afternoon, Whole Foods Market (NASDAQ: WFM) announced record quarterly sales but still failed to please investors.

Despite the record, the $3.3 billion in revenue announced was 1.2 percent lower than analysts predicted.

Whole Foods EPS of $0.38 was the same as the second quarter 2013 and missed analysts expectations of $0.41.

The most troubling aspect of the release was weak guidance. Whole Foods said full year EPS is expected to range from $1.58 to $1.65, about three percent lower than the previous forecast. The Street is currently expecting FY14 EPS of $1.61. This is the second quarter in a row that Whole Foods Market has had to reduce its guidance.

In the earnings call, the company stated competition is more intense than ever. Whole Foods execs indicated the company is expecting to lower prices.

Shares are being obliterated in after-hours trading, now down 14.33 percent to $41.03.