General Motors Co recalled three million more cars for ignition switch issues on Monday, roughly doubling the number of GM vehicles with known switch problems in a crisis that has defined the automaker and new Chief Executive Mary Barra this year.

GM on Monday recalled 3.36 million midsize and fullsize cars globally with ignition switches that can be jarred out of the "run" position, potentially affecting power steering, power brakes and air bags.

The switch issue is similar to the defect linked to at least 13 deaths in an earlier, 2.6-million vehicle recall of Chevrolet Cobalts and other small cars.

GM engineers first noted the Cobalt problem more than a decade ago, and GM's slow response to the switch issue triggered investigations within the company and by Congress and federal agencies.

"The recall is just sort of the tip of the iceberg in terms of what has to be done" at GM, Senator Richard Blumenthal, a Democrat from Connecticut and one of GM's more vocal critics in Congress, said after Monday's recall.

GM said the engineer who designed the defective Cobalt switches, Ray DeGiorgio, also designed the switches on the latest batch of recalled cars. DeGiorgio was fired after the earlier recall. He could not be reached for comment.

GM has issued 44 recalls this year totaling about 20 million vehicles worldwide, which is more than total annual U.S. vehicle sales. Of the recalls this year, nearly 6.5 million of the vehicles were recalled for ignition switch-related issues, including more than half a million Chevrolet Camaros on Friday.

The automaker raised a recall-related charge for the second quarter to $700 million from $400 million. That takes GM's total recall-related charges this year to $2 billion.

Despite the rash of recalls this year, GM US sales rose in May to the highest level since August 2008.

GM's high profile problem this year has catalyzed recalls at other automakers, said David Cole, chairman emeritus of the Center of Automotive Research in Ann Arbor. He described the recent flurry of activity as "recall spring."

"If it were unique to GM, I would say it is a much more serious problem," said Cole.

Clarence Ditlow, Executive Director Center for Auto SafetyOf Monday's recall, said GM could not afford to take a chance on not recalling a car. "Their calculus has totally changed," he said.

GM said it was aware of eight crashes and six injuries related to the latest recall, but could not say if there were any fatalities.

The automaker on Monday said it would replace or rework ignition keys to eliminate a slot in the end of the key. The slot allows a dangling key ring to slip to one side and pull the ignition key out of run position.

"The use of a key with a hole, rather than a slotted key, addresses the concern of unintended key rotation due to a jarring road event, such as striking a pothole or crossing railroad tracks," it said. A spokesman said the ignition switches did not need to be replaced, even though they were "slightly" below the company specification for torque -- the force needed to move the switch out of the run position.

The latest recall includes Buick LaCrosse, Chevrolet Impala, Cadillac DeVille and several other models, though only the Impala is currently in production. The cars cover model years 2000 through 2014.

Monday's recall comes two days before CEO Barra is due to return to Congress to testify about the earlier Cobalt recall.

Barra will be joined by Anton Valukas, chairman of GM's outside law firm Jenner & Block, who conducted a months-long investigation that detailed deep flaws in GM's internal decision-making process. The so-called Valukas report triggered the departures of 15 GM employees, including several high-ranking executives in the legal, engineering and public policy groups, as well as DeGiorgio.

GM said Barra wants to update Congress on the actions the company has taken in response to the switch recall crisis, including fixing the failures outlined in the company's internal report, announcing plans to establish a victims' compensation fund and setting up a structure at the company to ensure vehicle safety.

The US National Highway Traffic Safety Administration, which administers vehicle recalls, said Monday that it would "monitor the pace and effectiveness" of the latest GM recall and "take necessary action as warranted."

Copyright: Thomson Reuters 2014