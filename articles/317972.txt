Eric Franklin/CNET

Barnes & Noble unveiled plans Wednesday to break apart into two companies, separating its retail unit from its struggling Nook Media business.

The bookstore operator said it plans to complete the separation by the end of the first quarter of 2015. The company has engaged Guggenheim Securities as financial advisers and Cravath, Swaine & Moore as legal counsel.

The announcement comes after the company last year said it had abandoned plans to split up, having weighed the idea of separating its retail and Nook operations for 18 months. However, in February, Chief Executive Michael P. Huseby said the company was again studying breaking up parts of the business.

"We have determined that these businesses will have the best chance of optimizing shareholder value if they are capitalized and operated separately," Huseby said in a statement. "We fully expect that our Retail and Nook Media businesses will continue to have long-term, successful business relationships with each other after separation."

Earlier this month, the company teamed up with hardware giant Samsung to launch a co-branded tablet called the Samsung Galaxy Tab 4 Nook. Featuring Samsung hardware and Barnes & Noble software, it's due to reach US stores in August.

Barnes & Noble had tried for years to find success in the tablet market with its Nook line but faced considerable competition from much larger electronics players such as Apple and Samsung. Its 661 bookstores also have faced pressure from Amazon as more book sales move online.

More recently, Barnes & Noble has worked to cut back on its spending in the Nook operations -- which includes e-books, devices, and accessories -- as it attempted to turn around the money-losing digital business. The company's previous chief executive, William Lynch, set an ambitious path for Banes & Noble to become a major player in the tablet world, but the company changed direction after he resigned last year amid weak Nook sales.

In Barnes & Noble's latest quarterly results, also released Wednesday, the Nook business posted a 22 percent decline in revenue, to $87.1 million, though it trimmed its adjusted loss for the period to $56 million. The retail business, which includes its bookstores and BN.com, eked out a small rise in revenue, to $955.6 million, but adjusted-profit edged down.

Overall, Barnes & Noble reported a 3.5 percent rise in fiscal fourth-quarter revenue, to $1.32 billion, thanks to growth in its college business, and the company managed to trim its loss to $36.7 million. For the full year, it posted a loss of $47.3 million.

Shares jumped about 5 percent Wednesday afternoon, adding to a recent rally for the company's stock.