SECURITY BREACH

P.F. Chang’s diners have card data stolen

P.F. Chang’s confirmed Friday that data from credit and debit cards used at its restaurants was stolen.

The company learned about the security breach Tuesday from the U.S. Secret Service and began investigating the breach with the agency and forensics experts.

While it knows that customers were exposed, it doesn’t know how many, when it happened or which restaurants were affected.

The company is working with credit card companies to determine which cards may have been affected. All P.F. Chang’s restaurants are using manual credit card imprinting devices to process card payments while the investigation continues. There are 209 locations across the country.

— Associated Press

RESTAURANTS

Priceline to purchase reservation service

Priceline has negotiated a $2.6 billion entree into the restaurant business.

The global travel booking king announced Friday it is acquiring OpenTable in a deal that would put Priceline into a new business doing for restaurant reservations what it does for hotel bookings.

The deal should give Priceline a new way to cater to its increasingly mobile-savvy customers, while parlaying Priceline’s global reach to expand OpenTable to other countries. Priceline generated sales of $6.8 billion in 2013.

“Travelers are diners,” Darren Huston, Priceline Group chief executive and president, said on a conference call. “It’s the same customers. There’s opportunity to cross-promote brands.”

In a statement, OpenTable chief executive Matt Roberts cited Priceline’s expertise in online marketing globally on all types of devices. “They have an exceptional track record of customer service in dozens of languages around the world,” he said.

Priceline, based in Norwalk, Conn., will pay $103 per share, which is a 46 percent premium to OpenTable’s closing price Thursday of $70.43.

Shares of OpenTable soared 33.05, or nearly 47 percent, to $103.46 in midday trading Friday. Priceline shares fell $22.16, or 1.8 percent, to $1,203.84.

— Associated Press

Also in Business

— From news services

Coming Monday