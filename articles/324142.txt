An Android update, "the ultimate smart TV," wearable gadgets and so-called smart home devices are just some of the innovations Google showed off today as its two-day developers conference, known as Google I/O (for Input/Output), got underway in San Francisco.

In recent years, the conference has focused on smartphones and tablets, but this year Google's Android operating system is expected to stretch into cars, homes and smartwatches.

The head of Google's Android division, Sundar Pichai, took the stage first to talk about the growing reach of Android phones and tablets. He said Android now has one billion users, who take 93 million selfies, walk 1.5 trillion steps, and check their phones 100 billion times each day. Android app installations rose 236 percent over last year, he said.

Dave Burke, director of engineering for Android, described highlights of Google's latest Android operating system update, referred to as "L."

Sundar Pichai, Senior Vice President, Android, Chrome & Apps speaks on stage during the Google I/O Developers Conference on June 25, 2014 in San Francisco, California. Stephen Lam, Getty Images

One new security feature called personal unlocking will allow a device to automatically unlock when its owner picks it up, by recognizing a familiar Bluetooth signal and other clues. If a stranger picks it up, it will stay locked.

It will have a battery saving feature that Burke says can extend battery life by 90 minutes, and 3D graphics capability for an immersive gaming experience.

Then came the much-anticipated announcement of Android's expansion into wearables. Google engineering director David Singleton introduced Android Wear, the software that will run smartwatches like the LG G and the Samsung Gear, which go on sale starting today on the Google Play store. Another model, the Moto 360, will be coming from Motorola later this summer.

Android Wear will enable developers to offer health tracking, navigation apps, voice-activated ride sharing requests, step-by-step cooking instructions and other functions in an even more portable format.

The company also announced Android Auto, which will bring many popular features of smartphones to new car models from more than 40 automakers beginning later this year. It will feature contextually aware navigation, streaming music and communications functions in an easy-to-use interface. CNET has more on how Android Auto will work.

Not satisfied with infiltrating cars, watches, phones and tablets, Google also made a play for the TV market, introducing Android TV. It will play streaming movies, television shows and games from any device and "cast" them onto your big screen. CNET's Tim Stevens' first reaction, as he live-blogged the announcement: "The power of Google search, plus Google voice recognition, on your TV, driven by your phone. The ultimate smart TV?"

Android TV will be available in the fall.

Google's I/O event comes at a time of transition for the company, which makes most of its money from advertising thanks to its status as the world's leader in online search. The company is trying to adjust to an ongoing shift to smartphones and tablet computers from desktop and laptop PCs. Though mobile advertising is growing rapidly, advertising aimed at PC users still generates more money.

At the same time, Google is angling to stay at the forefront of innovation by taking gambles on new, sometimes unproven technologies that take years to pay off -- if at all. Driverless cars, Google Glass, smartwatches and thinking thermostats are just some of its more far-off bets.

On the home front, Google's Nest Labs -- which makes network-connected thermostats and smoke detectors --announced earlier this week that it has created a program that allows outside developers, from tiny startups to large companies such as Whirlpool and Mercedes-Benz, to fashion software and "new experiences" for its products.

Integration with Mercedes-Benz, for example, might mean that a car can notify a Nest thermostat when it's getting close to home, so the device can have the home's temperature adjusted to the driver's liking before he or she arrives.

Nest's founder, Tony Fadell, is an Apple veteran who helped design the iPod and the iPhone. Google bought the company earlier this year for $3.2 billion.

Opening the Nest platform to outside developers will allow Google to move into the emerging market for connected, smart home devices. Experts expect that this so-called "Internet of Things" phenomenon will change the way people use technology in much the same way that smartphones have changed life since the introduction of Apple's iPhone seven years ago.

Google may also have news about Glass, including when the company might launch a new and perhaps less expensive version of the $1,500 Internet-connected eyewear. Google will likely have to lower the price if it wants Glass to reach a broader audience. But that's just one hurdle. Convincing people that the gadget useful, rather than creepy, is another one.