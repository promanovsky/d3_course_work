Summer box office is receiving a much-needed boost, thanks to a boffo opening by Marvel-Disney’s “Guardians of the Galaxy.”

Early Friday afternoon estimates placed the new superhero property’s opening weekend at $90 million — far above recent forecasts in the $70 million range.

Starring Chris Pratt, “Guardians” debuted to the highest Thursday opening of the year with $11.2 million and should easily score the best August debut of all-time, besting the $69.3 million haul of “The Bourne Ultimatum.”

It’s also the widest August opening in history with 4,080 theaters, nearly 3,200 of which are in 3D, carrying with them surcharges. Overseas, the film will debut in 42 international territories including the United Kingdom, Russia, Brazil, Mexico and South Korea. The picture carries an $170 million production budget.

Box office trackers quickly revised projections in the wake of the stellar performance at the Thursday-night preshows, which topped the $10.2 million take for Marvel-Disney’s “Captain America: The Winter Soldier.”

“Guardians,” directed by indie stalwart James Gunn, was on track to total $36 million on Friday — including the Thursday night take — followed by about $30 million Saturday and $23 million to $24 million Sunday.

“Guardians” stars Pratt as the Star Lord along with Zoe Saldana, Dave Bautista, Lee Pace and Bradley Cooper, who voices the wisecracking Rocket Racoon. The film has been a big hit with critics with a 91% “fresh” rating on Rotten Tomatoes.

Universal Pictures’ James Brown biopic “Get on Up” appeared heading for a $5 million Friday and a $14.6 million opening weekend. It grossed an estimated $371,000 late Thursday in 1,769 locations.

Universal’s second weekend of “Lucy” looked likely to finish second with about $19 million, declining 57% from its surprisingly strong opening. Friday estimates placed the Scarlett Johannson actioner at $5.6 million.

“Lucy” should finish the weekend with a domestic cume above $80 million.

Paramount-MGM’s “Hercules” showed little strength in its second frame with $3.2 million on Friday. The $100 million Dwayne Johnson epic was headed for a weekend in the $10 million to $11 million range, representing a sharp decline in the 64% range.