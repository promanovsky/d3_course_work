You may already know that eating fried foods can make you fat but you're likely to gain more weight consuming such foods if you're genetically predisposed to becoming fatter.

Results of a new study suggest that people who have high genetic risk for obesity are likely to get fatter when they eat fried foods which might explain why some individuals do not get as fat as the others regardless if they eat the same quantity of fattening food.

In the study published in the journal BMJ Tuesday, researchers involved over 37,000 individuals to examine the association of the body mass index (BMI) to genetic predisposition to obesity and consumption of fried foods. They determined how often the subjects ate fried foods using questionnaires and calculated their obesity risk using 32 genetic variants known to be associated with BMI and obesity.

The researchers found that the subjects with high obesity risks who consumed fried food more than four times a week were two pounds heavier than those who only ate fried food once a week. The difference, however, is only nearly one pound in low risk subjects showing that the effect of eating fried food more than four times a week is twice as high in those with the highest genetic obesity scores as in those with the lowest risks.

"We examined the interaction between frequency of fried food consumption (both at home and away from home) and a genetic risk score based on 32 well established genetic variants associated with BMI in relation to BMI and obesity in women," the abstract of the study read.

The researchers, however, noted that their results may have been affected by unknown factors albeit the study showed proof of the importance of modifying food choices particularly among those who are predisposed to getting fat.

"Our findings emphasize the importance of reducing fried food consumption in the prevention of obesity, particularly in individuals genetically predisposed to adiposity," said study author Lu Qi, from the Harvard School of Public Health.

Still, Alexandra Blakemore and Jessica Buxton of Imperial College London pointed out that despite the findings, the study may not actually affect public health guidelines. "This work provides formal proof of interaction between a combined genetic risk score and environment in obesity," they wrote in an editorial but added that the results "are unlikely to influence public health advice, since most of us should be eating fried food more sparingly anyway."

TAG Obesity, BMI

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.