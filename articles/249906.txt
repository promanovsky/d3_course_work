Lang received support from House Republican leader Jim Durkin of Western Springs. Durkin said he was skeptical at first, but he was convinced children would not become addicted.



Opponents worried youngsters under 18 could be exposed to marijuana.



The bill already had passed the Senate, but Lang changed it to include provisions designed to bar kids from smoking medical marijuana and also from buying it.



The House approved the measure 98-18, but the bill has to go back to the Senate for another vote.