Will Smith is ready to tackle the NFL’s concussion problem as he is in talks to star in the untitled drama based on the GQ article “Game Brain” for Scott Free and Sony.

Peter Landesman (“Parkland”) is on board to write and direct.

Giannina Facio, Ridley Scott and Michael Schaefer will produce for Scott Free while David Wolthoff and Larry Shuman will produce for the Shuman Company. Scott and Facio have become very passionate about the issue and were very aggressive in landing a star for the pic and have done so with Smith’s attachment.

The article was written by Jeanne Marie Laskas and follows Dr. Bennet Omalu, played by Smith, the forensic neuropathologist who single-handedly made the first discovery of CTE in a professional football player and brought awareness to the public. Story is described as a whistle-blower tale in the vein of “The Insider” humanizing the price paid by professional athletes in impact sports — and the political, cultural and corporate interests that fuel the business of professional sports.

The untitled feature is one of a handful of Hollywood projects revolving around the concussion problem in the NFL taking shape in the industry. Parkes/MacDonald Productions are developing a project based on the book “League Of Denial: The NFL, Concussions And The Battle For Truth” and Isiah Washington is set to star in the indie drama “Game Time Decision,” both of which focus on the concussion issue.

The news follows Smith’s exit from the Legendary pic “Brilliance” and sources say a possible reason Smith exited “Brilliance” may have had to do with this film and scheduling concerns.

Smith can be seen next in the Warner Bros. pic “Focus” opposite Margot Robbie. He is repped by CAA and Overbrook Entertainment.