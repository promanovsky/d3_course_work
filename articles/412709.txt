The merger of Burger King and Tim Hortons into the third largest quick service restaurant chain in the world means that the Tim Hortons brand could soon be going global.

CTVNews.ca has compiled a list of must-know facts and jargon every Canadian already knows, and that could take the rest of the world a bit of getting used to…..

Our favourite doughnut shop is named after a hockey player. Yes, Tim Horton really was a person, and yep, he was about as Canadian as you can get. He grew up in the mining town of Cochrane, Ont., and played so much darn hockey, he made it to the NHL. Played 24 seasons, in fact. The humble coffee shop he opened in Hamilton went on to grow into a multi-million dollar conglomerate -- although Horton himself never got to see it. He died in a car crash in '74.

Double-double – You can't be a true Canadian without knowing that a double-double means two shots of sugar, two shots of cream. It's such a common way to order coffee here, recent immigrants to Canada have been known to order double-doubles for years, not realizing there's any other way to get your coffee in Canada.

Timbits – Oh sure, you could call these tiny treats "doughnut holes." But no one in Canada would know what you were talking about. They're called Timbits and everyone in Canada has known since 1976 that they're the perfect way to make everyone in a crowd happy: not too filling, bite-sized, with so many varieties that no one can ever complains they couldn't find one they liked.

Roll Up The Rim to Win – Back when Roll Up the Rim began in 1986, the biggest prize was a snack box of Timbits. These days, it's free cars and TVs that keep Canadians rolling. Though other doughnut chains have tried to launch similar contests, none have ever been able to make grumbling about "Please Play Again" losses into such a national sport.

This is ‘our’ coffee - Eight out of every 10 cups of coffee sold in Canada is from Tim Hortons, the company claims. We love our coffee that much. Whether it’s on our way to a job site or after hockey practice, on the way home from school or before a late night shift, some argue that it’s integral to our cultural fabric. So while sipping this in America might not bestow upon you the benefits of universal health care and hockey supremacy, just know this is what keeps us going. Don’t screw it up.