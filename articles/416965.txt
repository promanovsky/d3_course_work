The only good thing about waking up at 5am to watch the live telecast of a high-fashion event happening in Los Angeles is that you can feel some satisfaction at sitting with bed hair, in your jammies and being entirely within your rights to pass judgements upon the carefully-cultivated, haute couture looks of the stars.

8.39 am: Modern Family wins Best Comedy series, Emmys come to an end this year

"It's hard to believe we're almost a tenth of the way through the evening," quips Jay Leno, thus winning the award for Worst Attempt at Wit.

The award for Best Comedy Series goes to Modern Family. The cast is up there, including the kids, which is cute but doesn't quite make up for Louie not getting enough love from the Academy. Still, Modern Family's given us enough to giggle about over the years and look! there's Manny! Awwww.

They're drowning out Vince Gilligan! Really Emmys? Is this the on which you want to end?

On the plus side, at least it is over now. Pulling the camera back as the last shot was not a good idea. The audience seems to be as happy to get up and go as we are to switch off the TV.

That's it from the Emmys. Fingers crossed that next year, Orange is the New Black gives Modern Family a run for its money.

8.31 am: Breaking Bad wins Outstanding Drama Series

The Emmy for Outstanding Drama Series goes to Breaking Bad. The show, which concluded in 2013, received 16 nominations this year. Not a bad way to go at all. Bravo!

8.20 am: Will Matthew McConaughey in his blinding blue suit make history by becoming the first actor to win both the Oscar and the Emmy in the same year?

But first, Julia Roberts is here to announce the nominations, wearing the dress covered in glistening boils.

Nope, there's no history being made tonight for McConaughey. It's Breaking Bad chiming in again, with Bryan Cranston winning the award for Best Lead Actor in a Drama Series.

"Even I thought about voting for Matthew," says Cranston, coming up to the mic. Cranston deserves some serious respect for beating the movie stars to win this award — that's testimony to how incredibly popular Breaking Bad became in its last season.

8.14 am: Anna Gunn wins best supporting actor for a drama series for Breaking Bad

Cary Joji Fukunaga wins the award for best direction of a drama series, for True Detective. Yes, he did indeed tie his hair up in pigtails. No, he didn't explain the whole "time is a flat circle" theory that Matthew McConaughey's Rust expounds in the show.

Best Supporting Actress in a Drama Series: Anna Gunn, for her performance in Breaking Bad. No surprises here. This also means the 35th cut away to Bryan Cranston's moustachioed face. The 'tache might be growing on us.

8.12 am:Julianna Margulies wins the best actress in a Drama Series for The Good Wife

Moira Walley wins the award for Best Writing in a Drama Series, for Breaking Bad. She's only the second woman to win an award (leaving aside the awards for actresses).

Best Lead Actress in a Drama Series: Julianna Margulies for The Good Wife. "What a wonderful time for women on television!" says Margolies, before dedicating the award to two men (the writers of The Good Wife).

That said, it really is a superb show and Margulies is absolutely right about the nominees in this category — like Claire Danes, Robin Wright and Lizzy Kaplan — showing how great parts are being written for women.

Meanwhile, Sophia Vergara has been made to stand on a revolving platform while Television Academy CEO Bruce Rosenblum spoke.

What better way to make the best of having a talented comic actress on stage than by reducing her somewhat literally to a sexy object and prop? Vergara basically had to stand on a platform — she registered her protest, by the way — which revolved so that we could admire Vergara's figure while the CEO blathered on. Charming.

8.09 am: The Emmys should come with a snooze warning!

They really better be serving coffee instead of wine at this year's Emmys because no one's going to be awake till the end otherwise.

Not to be difficult, but why is Maya Angelou in the Emmy In Memoriam tribute? Admittedly she's got a few TV credits, but was she really one of the LA television fraternity/ sorority?

Robin Williams, however, gets a special segment, with Billy Crystal remembering the actor and comedian. "He was the greatest friend you can ever imagine," says Crystal.

"It's very hard to talk of him in the past because he was so present in all of our lives." Listening to Crystal talk about Williams does leave you with a little lump in your throat. Sadly, the producers of the Emmys were clearly not listening, because could put together a well-edited collection of Williams's TV moments. There wasn't a single shot from Mork and Mindy.

8.04 am: The golden age of American television appears to actually be the golden age of BBC!

Jessica Lange wins best Lead Actress In A Miniseries or a Movie for American Horror Story: Coven. Second win for the show, both in the acting categories.

News flash: even Weird Al Yankovich can have dull moments. His musical take (or whatever that was) on shows like Mad Men, Scandal and Homeland is just sad and unimaginative. Not even a brief shot of Jon Hamm smiling makes up for this. Andy Samberg is just not helping the cause.

Lena Headey, on the other hand, looks pretty darn fantastic.

Fargo wins the award for Outstanding Miniseries.

The award for best TV Movie, unsurprisingly, goes to HBO's The Normal Heart.

Someone give Ricky Gervais a cupcake for trying to liven things up even though he hasn't won. It isn't working though.

Sarah Silverman wins the award for best writing for a variety special, for her We Are Miracles. Her attempt to add excitement to the Emmys is to say, "We're all just made of molecules and we're hurling through space right now." Ok then.

The award for Directing a Variety Special goes to Glenn Weiss for the 67th Tony Awards.

The Colbert Report wins the award for Outstanding Variety Series. Stephen Colbert generously suggests to all those who wanted to work on the show that you should add your name as a creative consultant "or some such bullsh*t" in the show's "very comprehensive" IMDb page.

Aaron Paul wins best Supporting Actor in a drama series, for Breaking Bad. "It has changed my life," says Paul of Breaking Bad and adds that he misses the show. He's not the only one.

7.51 am: Jim Parsons wins Emmy for best lead actor in comedy for The Big Bang Theory

The montage introducing the nominations for Outstanding Direction for A Comedy was a smart little package in which cast members talked about the best advice that the director has given them. One of the child actors from Louie said Louis CK taught her how to hold a joint.

Winner for Outstanding Direction for a Comedy: Gail Mancuso. Win number two for Modern Family.

The award for Lead Actor in a Comedy Series goes to Jim Parsons for The Big Bang Theory. According to this alternative LA worldview, Parsons is a greater comic talent than Louis CK.

Excuse us while we weep into our morning cuppa. (Hey, at least Louis won the writing award.)

Considering how many Satyamev Jayate ads we've seen during this live telecast, it feels more like the new season of Aamir Khan's show, peppered by bits and bobs from the Emmys.

Julia Louis-Dreyfuss wins the award for Lead Actress in a Comedy Series for her performance in Veep. Yay!

The Amazing Race wins best Reality-Competition program. There's something either deeply depressing or deeply sarcastic about getting comedy writers like Mindy Kaling to introduce this category.

Seth Meyers is talking again. About to nod off... wait! Jon Hamm alert! Nothing can make this man look un-delicious, not even a Chewbacca-esque beard.

Steven Moffat won Best Writing for a Miniseries, Movie or Dramatic Special, for Sherlock. Moffat is probably the only person from the show's cast who's attending the Emmys. (Sorry Cumberbitches.)

Kathy Bates wins best Supporting Actress in a Miniseries or a Movie, for American Horror Story: Coven.

She's worn a kimono to the Emmys. Bless. This is probably the first genuine moment of surprise in the Emmys so far.

Martin Freeman wins best Supporting Actor in a Miniseries or Movie for Sherlock: His Last Vow. Freeman's not here.

The award for Directing for a Miniseries, Movie Or A Dramatic Special goes to Colin Bucksey, for Fargo.

Good grief. For some reason, we didn't get to see Julia Louis-Dreyfuss and Bryan Cranston snog each other passionately, only to be broken up by Jimmy Fallon. (It's part of a joke that they started when they came to present an award and Cranston was miffed that Louis-Dreyfuss had forgotten he'd shared an onscreen kiss with her in Seinfeld.) So much for live telecast and hooray for the internet!

Woody Harrelson and Matthew McConaughey come to present the award for best lead actor and prove that actors really do need scriptwriters to make them funny and/ or cool.

Benedict Cumberbatch wins best Actor in a Miniseries or Movie for Sherlock: His Last Vow. He's not here either.

7.34 am: And Seth Meyers conspires to put us back to sleep!

It's the 66th Annual Prime Time Emmy Awards and you could argue that these are better paisa vasool than the Oscars, given it's the golden age of television. At least that's what got us out of bed at 5am anyway.

Host Seth Meyers isn't helping 'Project Stay Awake Through The Emmys'.

Meyers is so gentle and sweet that it seems a little mean to point out he's also, well, boring. When the major source of excitement and comedy is that the Emmys are taking place on a Monday night, it's not a good sign.

Only kids who have been allowed to stay up late and miss school on Tuesday should be this excited about a party on Monday night.

Amy Poehler, we love you. She's the first presenter of the evening and everyone who had started dozing after Meyers's intro must have jolted to wakefulness when Poehler announced she was here to present the award for "best onscreen orgasm in a civil war re-enactment."

Actually, it's the award for the best supporting actor in a comedy series.

7.20 am: Ty Burrell wins best supporting actor in Comedy category for Modern Family

Ty Burrell wins in the Supporting Actor in a Comedy category. Win number one for Modern Family. A moment's silence for all those who were rooting for Jesse Tyler Ferguson. At least Burrell gave a lovely, funny little speech.

Oh look! Manny's on camera.

The award for Outstanding Writing in a Comedy Series goes to Louis CK for Louie: So did the Fat Lady. He will probably also get the award for the most bland acceptance speech of the evening. (We still love Louis CK, even though he looks entirely too orange.)

Praise the television gods, Jimmy Kimmel is here and he's actually funny. "Is that Matthew McConaughey?" Kimmel said, peering at the actor who was unmissable in his electric blue ensemble. "You got so fat since the Oscars, i almost didn't recognise you." We're quite certain the loudness of the applause comes from the ladies.

But Kimmel has more for McConaughey: "How many of those speeches of yours are we supposed to sit through? I mean, alright alright alright already."

Kimmel goes on to observe McConaughey doesn't have a television face.

"Ricky Gervais, now that is a television face. Actually, it's not even a television face. It's a Netflix face." Gervais, well known for being horribly nasty when he's a host, does have it in him to laugh out loud.

Allison Janney wins Supporting Actress in a Comedy for her performance in Mom.

7.00 am: And here are the red carpet heroes

The only good thing about waking up at 5am to watch the live telecast of a high-fashion event happening in Los Angeles is that you can feel some satisfaction at sitting with bed hair, in your jammies and being entirely within your rights to pass judgements upon the carefully-cultivated, haute couture looks of the stars. Girls' Lena Dunham, however, has robbed us of that solitary joy. She came out wearing the top of her night suit and a massive, frilly skirt that made her look like a doll cake.

Sarah Paulson, on the other hand, took the whole business of being part of American Horror Story a little too seriously in her off screen life. Her dress is clearly attacking her — what are those red spots? — and we're *this* close to calling Ghostbusters. Or David Attenborough, because we may have last spotted what's on Paulson's dress in Planet Earth's exploration of the deep seas.

Speaking of horror, the real Minnie Driver has been replaced by a life-sized bobble-head doll. It's very scary, especially if you're watching her skeletal frame and grinning face at twilight.

Two moments of Indian pride: Mad Men's January Jones wore a lovely red dress designed by Prabal Gurung. Mindy Kaling wore Kenzo dress and looked precisely as stylishly cute as we expect the creator and star of The Mindy Project to be. It's a terrible shame that her show wasn't nominated, but look at that, she came to watch the show anyway. (It's the sort of thing that takes those of us who get Bollywood awards shows by surprise.)

There was a lot of red on the red carpet and in a few cases, we're certain that the stylists were so lazy that they just sliced up the actual red carpet and velvet ropes, and wrapped the star in it. Like Allison Janney, for instance.

The disco ball was quite popular on the red carpet this year, with a number of ladies discovering their inner Beyoncé, notably Amy Poehler and Hayden Pannettiere and her baby bump.

Sarah Silverman deserves an award not for her dress, but for being able to bring pot (i.e. marijuana) into a red carpet conversation. Brava! Although it's a little worrying that Silverman thinks she's going to, ahem, medicinal aid to get through the evening.

It's hard being a man at an awards' show. You get to choose between a black dinner jacket and a black dinner jacket. Of course, if you look as good as Matt Bomer, you manage to make that boring outfit seem ridiculously dishy. Or you could be Matthew McConaughey, who decided to wear electric blue jacket, pants and — wait for it — blue shoes.

Bryan Cranston is dealing with the end of Breaking Bad by...growing a moustache. A thin moustache, last spotted on a mid-level bureaucrat in the BMC. On the subject of facial hair, Jon Hamm is now sporting a beard. Give us a moment while we do the ice bucket challenge, to promote hormonal stability.

Julia Roberts wore a shiny little dress that looked great until you saw it in close up. The material looks like it's breaking out in boils. Somewhere in the middle of the Emmys, Roberts' dress will hatch and a million little creatures will swarm the stage.

While the Oscars' red carpet is all about thin people, the good thing about the Emmys is that television stars come in all shapes and sizes. So it wasn't all protruding clavicles and geometric jawlines, we also saw curves being flaunted stylishly, thanks to people like Melissa McCarthy, Sofia Vergara and Christina Hendricks.

The actor who totally won the red carpet round at this year's Emmys was Lizzy Kaplan, who stars in the fantastic Masters of Sex. She wore a Donna Karan Atelier dress in black and a white. It's got a back that's swoon-worthy and everything about Kaplan's look was just perfect.