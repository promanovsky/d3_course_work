Justice Antonin Scalia ruled EPA got nearly everything it wanted in recent Supreme Court decision UPI/Roger L. Wollenberg | License Photo

WASHINGTON, June 24 (UPI) -- A Supreme Court decision on the Environmental Protection Agency's authority over emissions earned rare claims of victory from both sides of the debate.

The Supreme Court ruled in a 5-4 decision the EPA overstepped its authority in certain provisions of the Clean Air Act that cover emissions from all new or modified power plants and by changing the emissions thresholds for greenhouse gases, an action the court said only Congress could take.

Advertisement

The American Petroleum Institute, which represents the interests of the energy sector, said the high court's decision eases the EPA's grip over refineries and other production sources.

"It is a stark reminder that the EPA's power is not unlimited," API vice president and general counsel Harry Ng said in a statement Monday.

The court's decision means the EPA can still oversee more than 80 percent of all greenhouse gases emitted nationwide from stationary sources.

David Doniger, director of the Climate and Clean Air Program at the Natural Resources Defense Council, said the court upheld the "most important" parts of the EPA's authority under the Clean Air Act, those related to carbon pollution emitted by automobiles and power plants.

Justice Antonin Scalia, who wrote the lead opinion in the case, said the "EPA is getting almost everything it wanted in this case."