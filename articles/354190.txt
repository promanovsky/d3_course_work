Earlier this week a published report revealed that back in 2012 Facebook ran a test on nearly 700,000 of its users in which their emotions were manipulated. There has been widespread criticism for these actions once word broke and it looks like this might not be the only test Facebook conducted. A new report claims, citing a former member of Facebook’s Data Science team, that “hundreds” of tests were done ever since the team was formed back in 2007. The member says that anyone on the team could run on the test and that they were “always trying to alter people’s behavior.”

Advertising

Yesterday Facebook COO Sheryl Sandberg said that the study had been a part of ongoing research that was aimed to understand the social network’s users better. Technically what it did and continues to do isn’t illegal since users agree in the T&Cs to have their data be used for improvement of Facebook’s products, that means they can run as many tests as they want.

Andrew Ledvina, a data scientist who was at Facebook from February 2012 to July 2013, said that there was no review process per se for tests, he even recalled a minor experiment that he and a colleague ran without telling anyone else.

Since the feedback about the study has been quite harsh Facebook is now considering additional changes to the guidelines for its Data Science team. Most of the tests the company runs aren’t that controversial since they’re merely to figure out user behavior on the website itself, such as interaction with ad units.

What irks most people is the fact that Facebook has used their data to manipulate their emotions. Even if it was technically legal many would say that it wasn’t quite the moral thing to do.

Filed in . Read more about Facebook.