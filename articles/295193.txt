Nuance Communications is apparently in talks with a number of companies about a possible sale, one of those companies is Samsung, according to a recent report by the WSJ.

Nuance is used in a number of voice powered applications, in smartphones, TVs, and GPS, the technology is also used by Apple’s voice activated assistant, Siri.

Samsung also uses the Nuance technology in its smartphones, tablets and TVs, and car makers like Daimler and games console maker Nintendo, it is also used in Microsoft’s Cortana and Google Now

The company had revenue of $1.86 billion last year, and Nuance’s largest business is it health car unit, which develops software that it used to digitize patient records. The company has market capitalization of around $5.5 billion and its largest shareholder is Carl Icahn.

As yet there are no details on how much Samsung are looking to pay or Nuance Communications, and also no details on what they would do with the company’s voice technology if the deal goes ahead.

As soon as we get some more information on the proposed deal between Samsung and Nuance, and also more details on what Samsung’s plans for Nuance are, we will let you guys know.

Source WSJ

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more