Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The family of British Ebola victim William Pooley has thanked doctors fighting to save his life for the 'excellent care' he is being given.

In their first statement since the 29-year-old volunteer nurse was flown back to the UK last night for emergency treatment, his family paid tribute to those who orchestrated his speedy return.

And they also urged Britons to consider the thousands in West Africa afflicted by the virus, but dying in their droves because they lack adequate medical care.

They said: "We would like to express our thanks to all involved in bringing our son back to the UK.

"We have been astounded by the speed and way which the various international and UK government agencies have worked together to get Will home.

"Will is receiving excellent care at the Royal Free Hospital and we could not ask for him to be in a better place.

"We would like to thank all our family and friends for their best wishes and ask everyone to remember those in other parts of the world suffering with Ebola who do not have access to the same healthcare facilities as Will."

Mr Pooley, who comes from the small village of Eyke in Suffolk, contracted the potentially deadly virus while in Sierra Leone, where he had been volunteering at the Ebola centre in Kenema.

He was airlifted by a specially equipped C17 RAF jet back to Britain last night, and is being treated in a specialist isolation ward at the Royal Free Hospital in Hampstead, north London.

Doctors clad in protective plastic clothing and wearing gloves and masks are caring for the nurse in the strictly monitored ward.

(Image: PA)

There is no known cure for Ebola, which is transmitted through sweat, blood and saliva. The World Health Organisation says that more than 2,500 people have been killed by the latest outbreak in West Africa, where the fatality rate stands at 90% if it goes untreated.

The round-the-clock care Mr Pooley is receiving will radically improve his chances, but experts said his treatment could take weeks, if not longer, and there is no guarantee he will survive.

And it is not yet known if he will be given the experimental drug ZMapp - dubbed by some as the "cure" after two aid US workers were successfully treated for Ebola after taking it. But manufacturers of the drug have said their stocks have been exhausted because of the high demand.

Mr Pooley, who has been praised by colleagues as "particularly brave", had originally travelled to the country to volunteer at a hospice in the capital Freetown treating HIV and cancer patients.

But he risked his life to move to the Kenema Government Hospital when he heard about the pressing need for medics after other healthcare workers died from Ebola.

(Image: PA)

Oliver Johnson, a friend of Mr Pooley's who worked with him in Sierra Leone, said he was an 'extraordinary guy' who knew the risks involved but was prepared to take them for the sake of the patients.

Dr Johnson, programme director of King's College London's Sierra Leone partnership, said: "He and I spoke about the risk together and I think he absolutely understood that there were risks involved.

"He also knew, though, that he was well trained and there were good precautions in place, so it was a measured risk. But where he was in Kenema a number of staff had become sick.

"He was a hugely professional nurse and a hugely dedicated one, so he understood those (risks) but was prepared to take them for the sake of the patients and colleagues he had there."