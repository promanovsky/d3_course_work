Because of a decision by the Internal Revenue Service, bitcoin has officially been classified as an asset, but does it really have value? Citigroup Chief Foreign Exchange Strategist Steven Englander says we’re at a “fork in the road” right now, as competing perceptions of the digital currency begin to diverge. According to DealBook, he said it’s also difficult to see how bitcoin can be viewed as having value since many companies try to avoid holding them.

How bitcoin transactions are processed

To understand this, it’s important to see exactly how digital wallets process payments made using crypto-currencies. Companies which say they accept bitcoin actually work with digital wallet companies. The digital wallet actually accepts the coin and then converts it into cash based on the exchange rate at the time of the transaction. As a result, the companies themselves don’t even actually hold the digital currency.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

According to Englander, the IRS decision to classify bitcoin as an asset rather than a currency has “shattered” any belief that it’s possible to do transactions of any large size without using the financial system. The reason many got interested in the digital coin in the beginning was because they could conduct financial transactions anonymously. Consumers and companies have also been interested in using them because they could save on the fees associated with using credit cards or money transfer companies while also avoiding the burden of reporting them to the IRS.

Clearing up bitcoin myths

The analyst also said the decision will likely push virtual currencies in the direction of becoming payment systems used in the mainstream. In addition, he said it won’t really matter which crypto-currency people use because they all are a sort of payment system.

Englander said exposure to bitcoin and other digital currencies “will be infinitesimally short” and can be made “infinitesimally small.” In addition, he said exposure within foreign exchange can be very small in transactions made across borders.