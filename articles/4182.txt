Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Beyonce has joined a host of stars to front a campaign urging girls to take charge and become leaders.

The Girl Scouts of the USA and LeanIn.org have teamed up with Lifetime television to create a campaign featuring a whole bunch of famous faces, who include Queen Bey, Jane Lynch, Diane von Furstenburg, and Jennifer Garner.

Speaking about the campaign, Facebook COO and founder of LeanIn.org Sheryl Sandberg said: "We need to recognise the many ways we systematically discourage leadership in girls from a young age - and instead, we need to encourage them.

"So the next time you have the urge to call your little girl bossy? Take a deep breath and praise her leadership skills instead."

The campaign aims to empower young girls by encouraging everyone to sign a pledge to ban the b-word.

In a short clip, Beyonce stares down the camera and says: "I'm not bossy - I'm the boss."

For more information, head to banbossy.com.