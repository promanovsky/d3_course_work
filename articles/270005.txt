Actress Anna B Davis, best known for her role as Alice Nelson on hit sitcom 'The Brady Bunch', has died. She was 88.

The actress passed away Sunday morning in a Texas hospital. An unconscious Davis was admitted to the hospital after suffering a fall on Saturday, reported E! Online.

Florence Henderson, who played Carol Brady in the 'The Brady Bunch', took to Facebook to mourn the demise of her co-star.

"I am so shocked and sad to learn that my dear friend and colleague, Ann B Davis, died today. I spoke with her a couple of months ago and she was doing great," she wrote.

Davis was born in Schenectady, New York and originally went to school to study medicine. She changed her mind, however, after seeing her brother in a production of 'Oklahoma!'.

She went on to get a degree in drama and speech from the University of Michigan in 1948.

Davis's sitcom 'The Brady Bunch', a story about a blended family, aired from 1969 to 1974 and had numerous reunion specials like 1981's the Brady Brides and 1988's A Very Brady Christmas.

She also earned a star on the Hollywood Walk of Fame in 1960.