Box-Office Preview: 'Transformers 4' Looks to Revive Franchise With $100 Million-Plus Debut

Mark Wahlberg replaces Shia LaBeouf as star of Michael Bay's blockbuster action series; John Carney's "Begin Again," starring Keira Knightley, opens at the specialty box office.

Paramount's Transformers: Age of Extinction is hoping to set off early fireworks this weekend and become the first movie of 2014 to cross the $100 million mark in its North American debut, as well as reboot director Michael Bay's action franchise with a new leading man, Mark Wahlberg.

Prerelease tracking services show the movie opening in the $100 million range, although Paramount is trying to temper expectations by noting it's been three years since the last film in the series, Transformers: Dark of the Moon, played in theaters, taking in $97.9 million on its first weekend in 2011. Also, Age of Extinction has a new look and feel sans Shia LaBeouf, previous star of the Paramount and Hasbro toy-to-film series.

Studio insiders say anything on par with the debut of Dark of the Moon would be a major victory for Age of Extinction, which cost north of $200 million to make.

LIST Hollywood's 100 Favorite Films Revealed

The first three Transformers movies took in a combined $2.7 billion at the worldwide box office, and all opened midweek, making comparisons difficult (Dark of the Moon is the highest grossing, with $1.12 billion in ticket sales).

Age of Extinction should do massive business overseas, where it is rolling out this weekend in Russia, Australia and most of Asia, including China (it opens later in Japan). Normally, it would have debuted day-and-date around the globe, but Paramount is waiting in many markets because of the World Cup (Russia's team has already been eliminated from the soccer championships).

Domestically, the 3D tentpole is widely expected to score the best opening of the year to date, besting the $95 million debut of Captain America: The Winter Soldier in April.

Young stars Nicola Peltz and Jack Reynor also make their first appearance in the franchise. Age of Extinction takes place four years after a devastating face-off between the Autobots and the Decepticons in Chicago, and introduces the Dinobots (i.e., dinosaur robot) for the first time.

PHOTOS 'Transformers' Hong Kong Premiere

During production of Dark of the Moon, both Bay and LaBeouf confirmed it would be their final Transformers film. A number of other directors were considered, but Bay ultimately decided to return and make the movie for Paramount and Hasbro.

No other movie dared go up against Age of Extinction and open nationwide this weekend, although a slew of specialty films begin their limited runs, including three from The Weinstein Co.: John Carney's Begin Again, starring Keira Knightley, Mark Ruffalo and Hailee Steinfeld, Korean film Snowpiercer (a Radius-TWC release) and French biopic Yves Saint Laurent.