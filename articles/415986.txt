Beyonce, Jay-Z &Blue Ivy at the 2014 MTV Video Music Awards at The Forum on August 24, 2014 in Inglewood, California. (Photo by Getty Images)

Blue Ivy is a true fashionista! Beyonce and Jay Z’s daughter grabbed attention for more than one reason at the VMAs on Aug. 24. Not only is she a great dancer, but her shoes were so fierce! Find out where YOU can get them.

We think it’s safe to say that all eyes were on Blue Ivy Carter during the 2014 MTV Video Music Awards on Aug. 24. She danced during her mom Beyonce’s performance, congratulated her upon receiving the Video Vanguard Award, and the 2-year-old tot also wore some pretty incredible shoes! Do YOU agree?

Blue Ivy’s VMAs Dress & Shoes — Get Beyonce & Jay Z’s Daughter’s Awards Show Look

In a perfect world, we’d be as cool and cute as Blue Ivy. But alas, we’re not, so we’ll settle for dressing our toddlers to look like the future queen of pop.

Blue Ivy wore an adorable pair of tap ballet flats to the awards show in Inglewood, California. While we don’t have exact details on her incredible shoes, we did some digging and found a pair, which have an uncanny resemblance to Blue Ivy’s, online.

For just $37.99, you can purchase Kenneth Cole Reaction Copy Tap Ballet Flats in four different colors, including black patent, light gold snake, pewter metallic and black leather.

If you asked us, we think they look just like Blue Ivy’s shoes!

And just in case you’re wondering about Blue Ivy’s beautiful gold dress, it’s a Bonpoint gown.

Blue Ivy Dances To Beyonce’s VMA Performance

Blue Ivy attracted massive attention after she was spotted dancing as her Beyonce performed every song from her most recent album, Beyonce.

During “Drunk In Love,” Blue Ivy, who was seated in Jay Z‘s lap, was seen grooving to the music. (Aside from Blue Ivy’s shoes, it was the cutest thing we’ve ever seen.)

What do YOU think, HollywoodLifers? Do you love Blue Ivy’s shoes and dress from the VMAs?

— Chris Rogers

Follow @ChrisRogers86

More VMA News: