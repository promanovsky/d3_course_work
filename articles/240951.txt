Geraldshields11 via Wikimedia

WASHINGTON (AP) — The Internal Revenue Service says over 318,000 federal workers and retirees owe just over $3.3 billion in back taxes.

Numbers the IRS released Thursday show that nearly 3.3 percent of all 9.8 million federal workers and retirees are behind in their taxes.

That compares with at least 8.7 percent of all taxpayers the IRS estimates are delinquent.

Almost 4.1 percent of civilian government workers owe back taxes. Among Cabinet-level departments, the highest rate of delinquency is at the Department of Housing and Urban Development, where 5.3 percent are overdue.

Congress is not immune — the IRS says 714 House and Senate employees owe taxes.

The IRS says that 1.7 percent of active duty military personnel are also overdue.

The figures are as of last Sept. 30.