The Chinese government has made surprise visits to four Microsoft offices located in China, in an apparent move to investigate the U.S. tech firm's alleged antitrust violations in the country, the South China Morning Post reported Monday.

According to the report, officials from China's State Administration for Industry and Commerce raided Microsoft offices in Beijing, Shanghai, Guangzhou and Chengdu yesterday. There was no clear indication of why the officials made the visits, but Microsoft said they would fully cooperate with the government.

"We aim to build products that deliver the features, security and reliability customers expect and we're happy to answer the government's questions," Microsoft said in a statement.

Advertisement

The South China Morning Post said the Microsoft investigation "might have to do with antitrust matters," quoting a story by news portal Sina. But the Financial Times said it might be part of the Chinese government's efforts to protect local tech companies from American firms that own big marketshare in China.

Today's news comes on the heels of the Chinese government banning the use of Windows 8 on new computers. The ban was imposed in May following former NSA contractor Edward Snowden's recent revelations of U.S. tech firms' spying programs, which have created a growing anti-U.S. technology sentiment in China, according to the Financial Times.

In May, the Chinese government urged banks in China to

And just last week, Chinese officials

although it didn't elaborate on what penalties the U.S. chipmaker might face.

Advertisement

Chinese investigations are often come with a mix of political motives. But they can have serious consequences. A private detective hired by GlaxoSmithKline to investigate whether GSK execs had bribed Chinese officials is now in prison in that country, according to the Financial Times, and a British GSK executive has been detained there and not allowed to leave.

