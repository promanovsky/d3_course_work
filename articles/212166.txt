Times publisher Arthur Sulzberger Jr.'s decision to remove Abramson and replace her with Managing Editor Dean Baquet shocked the newsroom earlier this week. The New Yorker's Ken Auletta quickly reported that the dismissal was at least in part the result of Abramson's complaints that her salary, both as executive editor and in previous positions, was significantly less than that of her male counterparts.

First, the context. The Times is now embroiled in a full-blown scandal over the ouster of its executive editor, Jill Abramson.

Corporate crisis PR pros should be following the latest New York Times debacle very closely. Because it is a case study in how to do just about everything wrong.

In this June 2, 2011 file photo released by The New York Times, Managing Editor Dean Baquet, Executive Editor Jill Abramson, center, and outgoing executive Bill Keller, pose for a photo at the newspaper’s New York office.

Times spokeswoman Eileen Murphy then told Politico's Dylan Byers that Abramson's compensation was "not less" than her predecessor Bill Keller's. And Sulzberger put out a memo to the newsroom saying that it was not true that Abramson's pay was "significantly less" than her predecessors.

Read More



Then came this bombshell paragraph from Auletta: "Let's look at some numbers I've been given: As executive editor, Abramson's starting salary in 2011 was $475,000, compared to Keller's salary that year, $559,000. Her salary was raised to $503,000, and—only after she protested—was raised again to $525,000. She learned that her salary as managing editor, $398,000, was less than that of a male managing editor for news operations, John Geddes. She also learned that her salary as Washington bureau chief, from 2000 to 2003, was a hundred thousand dollars less than that of her predecessor in that position, Phil Taubman."

Unless the Times releases further data showing how other elements of compensation made Abramson's pay "broadly comparable" to her male counterparts—as the paper's spokeswoman has insisted—it seems pretty clear it wasn't. In fact it wasn't even close.

But instead of addressing this apparently massive discrepancy, the Times spokeswoman told Politico she would seek a correction for another part of Auletta's story where he quotes her saying that Abramson's decision to hire a lawyer to inquire after the salary differences "was a contributing factor" in her firing because "it was part of a pattern."

Auletta stood behind the quote as accurate and in context. Murphy then responded with a truly bizarre email: "I said to you that the issue of bringing a lawyer in was part of a pattern that caused frustration. I NEVER said that it was part of a pattern that led to her firing because that is just not true."

This could easily come from an excruciatingly parsed Jay Carney statement at a White House press briefing. So we are to understand that this "frustration" was in no way related to Abramson being relieved of her job? It was just part of a totally unrelated "pattern"? OK.

And never mind that all this dancing on the head of a pin entirely misses the point.

Read More Hubbard to Geithner: You're lying



The problem for the Times is less Abramson's firing (her management style did in fact have significant problems) than the fact that the paper seems to have a problem with equal pay for woman. Murphy should be rebutting Auletta's numbers, if she can, and not parsing her own confusing statements.

Because the pay numbers are a pretty huge embarrassment to a left-leaning newspaper that crusades for equal pay and other economic justice issues in its news and editorial pages.

It's also demoralizing to staff and clearly not good for business—witness the decline in the company's stock price. As a business matter, the pay issues could hurt the Times as a place for highly talented women to want to work as reporters, editors and executives.