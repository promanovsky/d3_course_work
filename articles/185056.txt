Target Corp.’s board, seeking a new chief executive officer after pushing out Gregg Steinhafel yesterday, is under pressure to do something it’s never done before: hire a leader from outside the company.

The retail chain is working with recruitment firm Korn Ferry, signaling that it’s not focusing on internal candidates, said Carol Spieckerman, a retail consultant whose clients include Target suppliers. The company also has increasingly looked outside for other executive roles, including its latest chief information officer and chief marketing officer. Potential candidates for the top job include HSN Inc. CEO Mindy Grossman, Bon-Ton Stores Inc. chief Brendan Hoffman, Gap Inc. head Glenn Murphy and Victoria’s Secret CEO Sharen Turney.

Hiring an external CEO would break with decades of tradition at Minneapolis-based Target, which began in 1962 as part of Dayton Co. Such a move would help the company distance itself from the regime of Steinhafel, who stepped down after a devastating hacker attack and a money-losing Canadian expansion.

“They really need to restore some confidence, and I don’t think anyone is going to have any confidence in someone from inside, even though that’s their culture to promote from within,” said Kathy Gersch, executive vice president at Kotter International, a leadership and strategy firm. “They need someone known for their integrity to really help boost shareholder confidence in the brand.”

Representatives from HSN, Bon-Ton and Victoria’s Secret parent L Brands Inc. didn’t immediately respond to a request for comment. Gap declined to comment.

Uncommon Practice

Hiring an external CEO is uncommon in U.S. companies. In the last five years, among 27 consumer-related companies that replaced a CEO, only seven chose external candidates, according to data provided by Equilar, an executive-compensation and corporate-governance consulting firm. None of those candidates came directly from a previous CEO job, according to the data.

While internal hiring is still the norm at many retailers, including Wal-Mart Stores Inc., more are looking outside. Best Buy Co. upended tradition in 2012 when it named Hubert Joly, a Frenchman who had run hotel chains, as its fourth CEO. All its previous chief executives, including founder Richard Schulze, were company lifers.

At Target, there’s added urgency to find someone unsullied by its recent struggles. After last year’s hacker attack compromised the personal data of millions of shoppers, the board said yesterday that the time was right for new leadership. For now, Chief Financial Officer John Mulligan will serve as interim CEO. Board member Roxanne Austin, a former DirecTV executive, will be interim chairwoman. Steinhafel, a 35-year veteran of the company, had held both roles.

Holiday Hack

Under his watch, hackers overcame Target’s defenses during the peak of the holiday season and stole shoppers’ personal data. Bloomberg Businessweek reported in March that Target had ignored warnings from its detection tools, missing an opportunity to stop the attack sooner. The breach compromised 40 million credit card numbers, along with 70 million addresses, phone numbers and other pieces of information.

After the attack became public in December, Target’s reputation and foot traffic took a hit. U.S. comparable-store sales decreased 2.5 percent in the fourth quarter. Target then replaced its technology chief, Beth Jacob, with an outsider: Bob DeRodes, a former adviser for the Department of Homeland Security, the Justice Department and the Secretary of Defense.

Now the company is doing something similar with the CEO job, said Joe Feldman, a New York-based analyst at Telsey Advisory Group.

‘Fresh Blood’

“They need some fresh blood at the top that can facilitate some change,” Feldman, who sees Grossman, Turney and Hoffman as candidates, said in an interview on Bloomberg Television. “They wanted to clear the slate.”

The board said in a statement yesterday that Steinhafel held himself personally accountable for the breach. The 59-year-old executive, who had been CEO since 2008, will remain an adviser to the retailer during the transition.

The breach was one of several headaches for the once-thriving retail chain. Target is facing more competition from e-commerce rivals such as Amazon.com Inc., and it’s been slow to move into small-format stores, an area where Wal-Mart is accelerating its efforts.

Possible Candidates

An executive from a retailer in an “adjacent” sector such as Amazon or Starbucks Corp. would be a good fit, said Jason Hanold, managing partner of Hanold Associates, a Chicago executive-search firm. Someone from a direct competitor would have trouble fitting in, he said. The company also could pick a candidate outside the industry who has strong analytic skills. He cited John Lewis, president of the Americas region for Nielsen Co., the ratings and data-analysis company. Nielsen didn’t immediately respond to a request for comment.

The challenge for any new CEO will be emphasizing the best parts of Target while identifying the problem areas, Gersch said. Before his recent troubles, Steinhafel had successfully differentiated Target from Wal-Mart by building a reputation for stylish products.

“They need to articulate first where are they running to, not running away from,” Gersch said.

Target shares fell 2.6 percent to $58.33 at 9:45 a.m. in New York. They declined 5.4 percent this year through the close of trading yesterday.