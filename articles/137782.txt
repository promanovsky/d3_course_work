Actor Idris Elba attends the BAFTA LA Britannia Awards at the Beverly Hilton Hotel in Beverly Hills, California. (File/UPI/Jim Ruymen) | License Photo

LONDON, April 18 (UPI) -- British actor Idris Elba has announced via Twitter his girlfriend, makeup artist Naiyana Garth, gave birth to their first child, a son.

"My son, Winston Elba, was born yesterday. Truly amazing," the 41-year-old TV and film star tweeted Friday.

Advertisement

The Luther and The Wire leading man also has an 11-year-old daughter with his ex-wife, Dormowa Sherma. He and Garth reportedly began dating last year.