Update: According to Tom Warren of The Verge, the event described below will “mark the introduction of Office for iPad.” As Office for iPad will be a mobile product that leans on Microsoft’s cloud products, this squares with what the company initially indicated. Microsoft declined to comment.

Newly minted Microsoft CEO Satya Nadella will hold a press event in San Francisco on the 27th to discuss the cloud and mobile industries.

Nadella has been CEO for less than two months, meaning that his remarks will receive extra scrutiny. Will he outline a new vision for the company? A new product? The cloud name-check makes it feel like the little confab isn’t to tie a bow on the Nokia deal, but more on that later.

Build, Microsoft’s developer event, is set to take place mere days after this gathering, making the timing almost odd. But why have one press cycle when you could have two? And, the smaller, earlier event could have a different bent than Build, making the two more antagonistic than symbiotic if paired.

TechCrunch will be there.