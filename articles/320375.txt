Kanye West is wearing his love for daughter North on his neck.

The rapper was photographed this week wearing a simple gold necklace with the word “Nori” on it, which is the nickname he and wife Kim Kardashian use for the now 1-year-old. The 37-year-old flashed a smile — complete with a grill on his bottom row of teeth — and gave a peace sign to a photographer in Beverly Hills this week.

The public display of love for Nori came just a few days after the baby girl’s first birthday. The parents planned a Coachella-style party called Kidchella to celebrate her big day.

Partygoers were given tie-dyed wrist bands upon entering and were treated to a festival atmosphere in the couple’s spacious backyard. In the grounds there were booths for hair-braiding, face-painting, and tie-dying T-shirts and bandannas. Guests were also given custom Yeezus T-shirts as presents, TMZ reported.

Kardashian family members cut loose at the party, with sisters Kylie and Kendall Jenner trying out karaoke with their friends Jaden Smith and Moises Arias.

Kanye West has been open about his love for North West, gushing about his daughter to the press. He even showed up to mother-in-law Kris Jenner’s short-lived talk show to discuss his little one and how she changed his life.

“I’m supposed to be a musical genius, but I can’t work the car seat that well,” he told Kris.

West added that Kim came around at the right time, when he was at a low following the sudden death of his mother.

“After I lost my mother, there was times I felt like, I would put my life at risk. I felt like sometimes I didn’t have something to live for. Now I have two really special people to live for,” he said.

Kanye now has no problem playing the role of doting dad, and even planned his May 24 wedding with Kim in Florence, Italy, where North was conceived.

“I adore Florence. I love Italy and the Italian lifestyle,” Kanye West told a newspaper in Florence.”To tell you the truth, I already came to the banks of the Arno [river] with Kim last year, just the two of us, incognito.” He added, “I think that our daughter North was conceived here among the Renaissance masterpieces.”