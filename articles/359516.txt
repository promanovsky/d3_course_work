The USDA issued a public health alert involving raw chicken from Foster Farms on October 7, 2013, after 278 people became sick with strains of Salmonella Heidelberg in California and 17 other states. (CBS) The USDA issued a public health alert involving raw chicken from Foster Farms on October 7, 2013, after 278 people became sick with strains of Salmonella Heidelberg in California and 17 other states. (CBS)

FRESNO (AP) — The first product recall has been issued for California chicken producer Foster Farms since it was linked to an outbreak of an antibiotic-resistant strain of salmonella that has been making people sick for more than a year.

The U.S. Department of Food and Agriculture said Thursday night it has found evidence directly linking Foster Farms boneless-skinless chicken breast to a case of Salmonella Heidelberg.

As a result, Foster Farms issued a recall for 170 different chicken products that came from its Fresno facilities in March.

Foster Farms says the products have “use or freeze by” dates from March 21 to March 29 and have been distributed to California, Hawaii, Washington, Arizona, Nevada, Idaho, Utah, Oregon and Alaska.

The total amount of chicken involved was not immediately announced.

The salmonella Heidelberg outbreak has led to more than 500 illnesses in 27 states, but no deaths.

© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.