Nokia was once the driving force of the mobile scene, but are we about to see the once-mighty company disappear forever?

Microsoft has been gradually completing the $7.17 billion takeover of the former world’s largest mobile vendor, and as the final handshakes are made this month, reports suggest that the iconic Nokia name will be dropped on the ash heap of history.

Enthusiast Web site Nokia Power User has revealed a leaked letter from Nokia to its existing suppliers that read:

“Please note that upon the close of the transaction between Microsoft and Nokia, the name of Nokia Corporation/Nokia Oyj will change to Microsoft Mobile Oy.”

So does this signal the death of the famous Nokia mobile and the birth of Microsoft mobile? While nothing has yet to be officially confirmed, reading between the lines one can assume that Nokia-branded hardware will be no more.

It signals the end of an era for many.

There wasn’t a pocket in the early 2000s that didn’t bulge with the familiar shape of a Nokia phone. The Finnish mobile maker dominated around the world, and of the most-sold handsets of all time, it still holds positions 1 to 7 (the Nokia 110 takes the top spot with 250 million units sold).

But this glory faded fast as rivals were quicker to evolve, with flashier features and newer interfaces that were a joy to use compared to the relatively rudimentary Symbian operating system which Nokia clung to for far too long.

But not all is dead and buried. The letter also hinted that it would like to keep its suppliers as new products are in the pipeline.

“In addition to becoming a supplier for Microsoft Mobile Oy, we appreciate that you might also remain a supplier to the continuing businesses of Nokia (i.e., NSN, HERE or Advanced Technologies) after the close of the transaction between Microsoft and Nokia Devices and Services.”

The deal, to be rubber-stamped on April 25, will have nostalgic gadgeteers surely mourning the loss of the Nokia mobile. But they can take comfort in the hope that we will see it rise again in some form — even if it doesn’t play Snake.

This article originally appeared on News.com.au.