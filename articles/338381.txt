Oscar Pistorious Was Not Mentally Ill At Time of Shooting

The Oscar Pistorious murder trial is back on.

It had been on hold till the Olympic sprinter’s mental health could be evaluated.

An assessment found Pistorious was not mentally ill at the time of the shooting of his girlfriend Reeva Steenkamp. And the fact that the prosecution requested the evaluation comes as a little strange to a CNN analyst.

The thing is, Pistorious so far isn’t going for an insanity defense. But what the prosecution could get out of this, is undermining a witness for the defense.

You might remember the testimony with a forensic psychiatrist back in May.

In other words, the testimony was that Pistorious had some kind of anxiety disorder.

So, what all of this means is the trial will proceed and the 27-year-old Pistorious does not have the option to use mental health as a defense for a mitigating factor.

The Telegraph reports both legal teams — the defense and the prosecution, have accepted mental health report. It also notes the “condition which could lessen criminal responsibility [was] ruled out.”

Prosecution believes Pistorious murdered his 29-year-old girlfriend in a jealous rage. Pistorious has maintained he shot her by mistake in February of 2013, thinking there was an intruder in his home.

The New York Times calls the mental assessment results “critical since defendants deemed unable to distinguish between right and wrong can be committed indefinitely to state mental institutions.”

If convicted without mitigating factors such as mental health, Pistorious faces a mandatory minimum sentence of 25 years in prison.