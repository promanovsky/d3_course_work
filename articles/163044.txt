What's been truly alarming has been the recent surge in numbers of those infected, with more confirmed reports so far this year than in all of 2013. Ian Mackay, an epidemiologist at the Australian Infectious Diseases Research Centre at the University of Queensland, has been monitoring the spread of the disease. In the chart he tweeted below, note the dramatic spike in cases in the past month (KSA is the acronym for the Kingdom).

How does the view look from up there #MERS? More at VDU blog... http://t.co/d041Bqykk6 pic.twitter.com/3AKpWY7eHQ — Ian M Mackay, PhD (@MackayIM) April 24, 2014

The Saudi health ministry has been criticized for inadequately dealing with the situation. A lack of data shared by the Saudis has made it difficult for experts like Mackay as well as monitors at the WHO to study whether the virus has mutated and become more easily transmissible between humans--a development that could pave the way for a pandemic.

AD

AD

Public health experts and epidemiologists are still struggling to understand where the virus originated and how it spreads--with most suggesting it emerged through contact with camels, and others pointing to bats as prime carriers of the virus.