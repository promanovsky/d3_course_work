China has accused Vietnam of ramming its ships in a disputed part of the South China Sea amid concerns that a tense maritime standoff between the two countries could boil over into armed conflict.

Chinese ships have been ramming into and firing water cannons at Vietnamese vessels trying to stop Beijing from putting an oil rig in the South China Sea 130 nautical miles off Vietnam's coast, according to officials and video footage on Wednesday. Several boats have been damaged and at least six Vietnamese people on board have been injured, officials said.

China is "deeply surprised and shocked" by Vietnam's actions, Yi Xianliang, deputy director general of the foreign ministry's department of boundary and ocean affairs, said at a press briefing on Thursday afternoon. "From 3 May to 7 May, in a short period of five days, Vietnam has dispatched 35 vessels of various kinds which rammed Chinese ships as many as 171 times," he said.

Yi added that China's operations in the area were "legitimate, justified and lawful", and had been ongoing for a decade. "I don't think we have any reason to stop such operations," he said.

Elsewhere in the area, the Philippines arrested 11 Chinese fishermen for catching endangered turtles, angering Beijing and further exposing regional strains.

China has recently been harassing Vietnam and Philippine vessels and fishermen in the potentially oil and gas rich waters it claims almost entirely – a shaky stance to many international law experts.

But China's deployment of the oil rig on 1 May and the flotilla of escort ships, some armed, is seen as one of its most provocative steps in a gradual campaign of asserting its sovereignty in the South China Sea. With neither country showing any sign of stepping down, the standoff raises the possibility of more serious clashes.

"Because China is an authoritarian [country], you have different interests there operating unchecked," said Bruce Jacobs, an expert on east Asian security at Monash University in Melbourne. "The military wants a bigger and bigger budget so it can have more toys to play with and people to control, and the leadership wants the military's support – there's a mutual understanding there … they won't promote peace on the Chinese side, and to me, that's a concern."

Hanoi, which has no hope of competing with China militarily, said it wants a peaceful solution and – unlike China – has not sent any navy ships to areas close to the deep sea rig near the Paracel islands. But a top official warned that "all restraint had a limit".

At the press conference, Yi said China had exercised "utmost restraint" in its use of water cannons. He refused to answer questions about the number of Chinese ships present at the rig, and whether any Chinese sailors had been injured.

"We stand ready to discuss with Vietnam on relevant issues on the basis of consultation," he said. "But the precondition is that Vietnam must put an end to the disruption of Chinese operations, and must remove its vessels and personnel at the scene. We believe if these preconditions are met, then we may be able to find a solution."

"Our maritime police and fishing protection forces have practised extreme restraint. We will continue to hold on there," Ngo Ngoc Thu, vice-commander of Vietnam's coastguard, told a news conference in Hanoi. "If [the Chinese ships]continue to ram into us, we will respond with similar self-defence."

Video was shown at the news conference of Chinese ships ramming into Vietnamese vessels and firing high-powered water cannons at them. Thu said the Chinese vessels had done so "dozens" of times over the past three days. He said Vietnam had not carried out any offensive actions of its own close to the rig, around 140 miles off the Vietnamese coast.

A Vietnamese official said earlier that its ships were outnumbered by the Chinese flotilla. He said the Vietnamese ships were trying to stop the rig from "establishing a fixed position" at the spot where it wanted to drill.

China's assertiveness, along with its growing military and economic might, alarms many smaller neighbours even though they are aware they need to keep relations open with a vital trading partner. The US shares these concerns.

Vietnam has limited leverage. While it is no longer as isolated as it once was, the country cannot expect much diplomatic or other help from powerful friends. It appears likely to try to rally regional support.

"China seems intent on putting down its footprint squarely in contested waters and force Hanoi's hand. It appears a critical juncture has occurred and one would expect Hanoi to be weighing its options," said Jonathan London, a Vietnam expert at the City University of Hong Kong. "Hanoi's back is against the wall, though China's policies – which according to virtually everyone except China are baseless legally – have brought about this situation."

The Philippines has filed a legal challenge to China's territorial claims at a UN tribunal, against Beijing's wishes. Vietnam and other claimant states have not done that yet but Tran Duy Hai, vice-chairman of Vietnam's national borders committee, did not rule it out. "Vietnam will have to use all measures stipulated in the UN charter to defend its interests," he said.

China occupied the Paracel islands 40 years ago, leaving 74 US-backed South Vietnamese forces dead in a military clash. The Vietnamese and Chinese navies clashed again in 1988 in the disputed Spratly islands, leaving 64 Vietnamese sailors dead.

In 1992, China awarded a contract to US energy company Crestone to explore for oil and gas in the Spratly islands. Vietnam protested against the move. Two years later, Vietnam's navy forced the company's oil rigs to leave.