Nadella, who replaced longtime CEO Steve Ballmer earlier this year, will address the media and industry executives in San Francisco on March 27. (AP)

Microsoft Microsoft Corporation Chief Executive Satya Nadella may unveil an iPad version of the company's Office software suite on March 27, a source familiar with the event told Reuters, and use his first big press appearance to launch the company's most profitable product in a version compatible with Apple Inc's popular tablet.

Nadella, who replaced longtime CEO Steve Ballmer earlier this year, will address the media and industry executives in San Francisco on March 27.

Investors for years have urged Microsoft to adapt Office for mobile devices from Apple and Google Inc, rather than shackling it to Windows as PC sales decline. But the Redmond, Washington-based software giant has been reluctant to undermine its other lucrative franchise, its PC operating software.

Microsoft gives up some $2.5 billion a year in revenue by keeping Office off the iPad, which has now sold almost 200 million units, analysts estimate.

Tech blog Re/code first reported news of Nadella's event. Microsoft said in an invitation to reporters that Nadella will discuss "news related to the intersection of cloud and mobile" but declined to comment on the specifics of the CEO's appearance.

Microsoft has had iPad and iPhone versions of Office primed for several months now, sources told Reuters, but the company has dallied on their release due to internal divisions, among other things.

Although Nadella is expected to discuss his thinking in depth next week, the company has already signaled that it will adopt a more liberal attitude toward putting its software on different platforms.

Microsoft said earlier on Monday that it would make OneNote, its note-taking software, available on Mac, a move interpreted by observers as a shot against Evernote, the popular note-taking application that has both Mac and Android compatibility.

Aside from Evernote, Microsoft also faces budding challenges from startups that have released mobile-friendly alternatives to Word, Excel and Powerpoint.