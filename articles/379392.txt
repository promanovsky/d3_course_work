BALTIMORE (WJZ)–Johns Hopkins Hospital takes third place in the national ranking by US News & World Report.

Pat Warren has more on how the administration is reacting.

It’s a fall from first place but third out of 4,800 hospitals nationwide is nothing to sneeze at.

For the past 12 months, Johns Hopkins has been riding high, holding the number one ranking after a year of being number two and staff couldn’t be happier.

Now a lesser honor: ranking number three.

“Maybe a little disappointed not to be number one because we—for 22 years of the 25 years of the US News survey, we were number one so maybe a little bit of disappointment but yet to be number three out of 4,800 is not too shabby,” said Johns Hopkins President Ronald Peterson.

Johns Hopkins’ #1 ranking last year included first in ear, nose and throat; geriatrics; neurology and neurosurgery and rheumatology.

The administration is in the process of determining what changed.

“We’re just getting the information in today so we want to understand what has occurred but our commitment is to continuous improvement and we’ll strive to be number one yet again,” Peterson said.

Meanwhile, a congratulatory basket of Maryland goodies is headed to the new number one, the Mayo Clinic in Rochester, Minnesota.

“We hope they’ll reciprocate in the future when we reassume the number one position,” Peterson said.

Hopkins ranked in the top 10 of 11 medical categories.

Massachusetts General came in second.

The third place ranking isn’t expected to have a negative impact on the hospital attracting patients, doctors or research dollars.

Other Local News:

[display-posts category=”local” wrapper=”ul” posts_per_page=”5″]