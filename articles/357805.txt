NASA on Tuesday approved the production of a rocket believed to be the most powerful to ever launch in history, which will explore areas beyond our moon including near-Earth asteroids and Mars by the end of the decade.

On Wednesday, Boeing Co., the prime contractor for the rocket known as Space Launch System, announced the finalization of its $2.8-billion contract with the aeronautics administration as well as the completion of a design review for the aircraft.

According to The Los Angeles Times, NASA's use of the Saturn V, which took astronauts to the moon, was the last time it had boasted such an assessment of a deep-space rocket.

"We're ready to move forward," said Frank McCall, Boeing's Space Launch System deputy program manager. "This program has the potential to be inspiring for generations."

If all goes as planned, Space Launch System will have its initial test launch from Cape Canaveral, Florida in 2017. However, groups such as the Space Frontier Foundation, an advocacy group, have doubted the space agency and Boeing's ability to actually launch such an aircraft in the timeline they have projected.

Congress is also expected to pose more funding questions in the years to come prior to the proposed launch date, but McCall insisted that the program's budget isn't necessarily inadequate, The Times reported.

"We're not operating on the budgets of Apollo missions anymore," McCall said. "But we're not operating on a shoestring budget either.

Much of the criticism Space Launch System has drawn comes from its design, which the Space Frontier Foundation called a "Frankenstein rocket," as most of its parts come from already developed technology. The craft's two rocket boosters are reportedly advanced versions of the Space Shuttle boosters, while the motor from a rocket that is typically used by the Air force is the basis for a cryogenic propulsion stage.

The Space Launch System was "built from rotting remnants of left over congressional pork," the Space Frontier Foundation said. "And its budgetary footprints will stamp out all the missions it is supposed to carry, kill our astronaut program and destroy science and technology projects throughout NASA."

Since 2010 when President Barack Obama cancelled NASA's Constellation mission to the moon, the space agency has been developing the technology for the president's proposed vision of deep space missions, according to The Times.

This has posed a problem for NASA, which would have to pay $71 million to Russia if it wanted to get astronauts to the International Space Station. It would prefer the astronauts to hitch a ride with private companies but the technology is not there yet.

As a result, NASA has been focusing its attention on the Space Launch System, which includes a deep-space mission to land on an asteroid by the mid-2020s.