Jedward delighted their Canadian fans as they took to the red carpet at the MuchMusic Video Awards last night. And it appears Ed Sheeran loves hanging out with the Dublin twins.

Speaking on the backstage TV show at the MuchMusic Video Awards the 23-year-old Sing hitmaker said: "We watched the football me and Jedward. I was driving through the streets and I noticed Jedward walking and I was like why are Jedward outside so we called them and we kidnapped them in the van, took them back to the hotel and watched football."

When Jedward then joined Ed on the couch he added: "It's John, it's Edward and together we are JedSheeran.

"I really like your suits…I was inspired by your looks."

Hedley were the big winners at the ceremony, scooping the awards for Video of the Year, Pop Video of the Year and Favourite Video of the Year.

Drake scored two awards - International Video of the Year by a Canadian for Hold On, We’re Going Home and Hip-Hop Video of the Year for Worst Behavior.

Lorde was also a big winner, taking home International Video of the Year for Royals. The New Zealand-born pop star also performed at the event, taking to the stage with live renditions of her hits Tennis Court and Team.

Canada native Justin Bieber picked up the Favourite Artist or Group of the Year gong.

Reality TV shows star Kendall and Kylie Jenner hosted the ceremony, which featured performances by Ariana Grande, Ed Sheeran, Magic!, Sam Roberts Band and Virginia to Vegas (feat. Alyssa Reid).

Check out the the full list of winners below: International Video of the Year By a Canadian: Drake, Hold On, We’re Going Home

Hip-Hop Video of the Year: Drake, Worst Behavior

MuchFact Video of the Year: SonReal, Everywhere We Go

International Video of the Year – Artist: Lorde, Royals

International Video of the Year – Group: Imagine Dragons, Demons

Rock/Alternative Video of the Year: Sam Roberts Band, Shapeshifters

Pop Video of the Year: Hedley, Anything

Dance Video of the Year: Autoerotique, Asphyxiation

Favourite International Artist/Group of the Year: Selena Gomez, Come & Get It

Favourite Video of the Year: Hedley, Anything

Favourite Artist or Group of the Year: Justin Bieber

Video of the Year: Hedley, Anything

Check out 'JedSheeran' at the MuchMusic Video Awards

Jedward recognised fan Alex from Twitter (@thunderauhl) at the MuchMusic Awards © Twitter/@thunderauhl

RTÉ is not responsible for content on external websites

Keeping Up WIth The Kardashian stars Kendall and Kylie Jenner

The hosts made several outfit changes during the awards

Ariana Grande performed at the MuchMusic Awards 2014