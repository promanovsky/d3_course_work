The U.S. Food and Drug Administration proposed on Tuesday a more collaborative approach aimed at speeding up marketing approval for high-risk medical devices intended to treat patients with serious conditions that have no other treatment options.

The proposed Expedited Access Premarket Approval Application program would feature earlier and more frequent interactions between companies and FDA staff.

The proposal is a response to criticisms by policymakers, patient groups and the industry that the FDA's process for approving medical devices is inefficient and slow, thereby denying patients access to new, helpful products.

The program is not a new pathway to market, the FDA said, but rather a change in approach aimed at reducing the time it takes to develop a product and get it to market.

A device can be eligible for the program if it features breakthrough technology with significant benefits over existing products.

The FDA issued a rule in September that requires device manufacturers to put unique codes on their products that will allow regulators to track and monitor them in the event of a safety problem.

Also in September, the agency issued final rules on mobile medical apps, saying it would only regulate apps that transform smartphones into devices that the agency currently regulates, such as electrocardiography machines that can determine whether a patient is having a heart attack.

The FDA also published on Tuesday draft guidance on when data can be collected after a product's approval and what actions the agency can take if approval conditions such as postmarket data collection are not met. The regulator is now seeking public comment on the proposals.