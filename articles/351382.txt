Kendall Jones, the Texas Tech cheerleader who became infamous for her propensity for hunting down rare South African game, is firing back at her critics, claiming conservation.

Kendall Jones, in case you’re not up to speed, is a Texas Tech cheerleader who rose to online fame for her Facebook page, on which she posts pictures of herself posing with the South African game she’s hunted down. There are photos of her posing with either dead or tranquilized lions, leopards, and white rhinos, and her huge, toothy grin while doing so is kind of disturbing. She has received tons of online hate for her actions, and she has fired back at her many detractors, claiming that she is helping to conserve South Africa wildlife, and also sort of compared herself to Teddy Roosevelt. OK. See her claims below!

Kendall Jones’ ‘Conservation’ Effort Is The Reason For Taking Down South African Game

“Our 26th President of the United States, Theodore Roosevelt, has been labeled by many as the Father of Conservation. He helped create and establish the United States Forestry Service, which would later become the National Forest Service. […] But he was a hunter too, right? He killed the same species that hunters now chase today under a mound of anti-hunting pressure.

“Yet, how can it be possible that someone can love the earth, and take from the Earth in the name of conservation? For some folks, they’ll never understand. For the rest of us…we were born that way. God Bless Teddy.”

Kendall Jones: ‘The Rhino Was A Green Hunt’

Kendall has received a particularly large amount of heat for a photo of her posing next to a felled white rhino, the population of which has dwindled to just 20,000 and is in danger of extinction; however, conservation efforts can account for the number climbing even that high.

On her Facebook page, Kendall has given the following explanation for tranquilizing a rhino, and killing both a lion and a leopard:

“The rhino was a green hunt, meaning it was darted and immobilized in order to draw blood for testing, DNA profiling, microchip ping the horn and treating a massive leg injury most likely caused by lions. […] “Controlling the male lion population is important within large fenced areas like these in order to make sure the cubs have a high survival rate. Funds from a hunt like this goes partially to the government for permits but also to the farm owner as an incentive to keep and raise lions on their property. […] Now to the leopard, this was a free ranging leopard in Zimbabwe on communal land. The money for the permit goes to the communal council and to their village people. […] “Leopard populations have to be controlled in certain areas. So yes, my efforts do go to conservation efforts and are all fair chase, not canned hunts. In fact these are very mentally and physically challenging hunts, on foot tracking and walking miles and miles a day.”

Kendall has also been posting a number of photos on her Facebook page with quotes from National Geographic and the hashtag “#SUPPORTKENDALL,” and she may not be doing it on purpose but it implies that the National Geographic is calling for support of Kendall, which is a bit problematic. See below:

But what do you think, HollywoodLifers? Is Kendall truly doing her part to preserve animal wildlife? If that is indeed the case, should she really be holding dead leopards like stuffed toys while showing off her pearly whites? Let us know what you think.

— Amanda Michelle Steiner

Follow @AmandaMichl

More Animal News: