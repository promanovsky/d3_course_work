Lizard Squad and Finest Squad have apparently been doing battle over PlayStation Network and Xbox Live. This March 8, 2012 file photo shows attendees walking past the Sony PlayStation PS Vita console on display in the Sony PlayStation booth at the Game Developers Conference in San Francisco. Despite a refreshed focus on real-world issues at the 2014 GDC convention that kicks-off Monday, March 17, 2014, the ever-changing virtual world and how to view and interact with it will take center stage at GDC. PlayStation 4 creator Sony Corp. is expected to tease its rendition of virtual reality technology during a Tuesday presentation called "Driving the Future of Innovation at Sony Computer Entertainment." (AP Photo/Paul Sakuma, file)

Lizard Squad Twitter: John Smedley, Sony Exec, Gets his Flight Diverted Because of Bomb Threat Amid PSN Hacking

John Smedley, the head of Sony Online Entertainment, reportedly had his flight diverted by a hacking group, Lizard Squad, around the same time the PlayStation Network went down.

A group known as Lizard Squad reportedly called in a bomb threat to divert the flight–an American Airlines plane.

Smedley confirmed that his flight was diverted for security reasons, reported Polygon. “Something about security and our cargo. Sitting on Tarmac,” Smedley tweeted.

An American Airlines spokesperson told The Arizona Republic that Flight 362 was sent to Phoenix.

The FBI is apparently investigating.

The group, Lizard Squad, confirmed responsibility for the hacking and diverting Smedley’s plane.

Lizard Squad tweeted: “… LOLOLOLOLO LIZARDSQUAD ON YA FOREHEAD @AmericanAir.”

The group also wrote: “.@j_smedley Glad to see you following us. How’s your day?” It added: “@LizardSquad @AmericanAir LIZARDSQUAAAAAAAAAAAAAAAAAAAAAAAAAD ON YA FOREHEAAAAAAAAAAAAAAAAAAAAAAAAAD.”

“Look’s like @j_smedley’s flight is being redirected, we call upon all Lizards to #PrayForFlight362,” it added.

It said earlier, “.@AmericanAir We have been receiving reports that @j_smedley‘s plane #362 from DFW to SAN has explosives on-board, please look into this.”

However, a Twitter user called FamedGod has been saying the Lizard Squad isn’t responsible for the attack.

“Why must someone take credit of ones work? LizardSquad couldnt hurt a fly. Decrypting a memory dump and finding the server was all my work,” he wrote after the attack. “Everyone Retweet this so everyone understands that i provided the proof. pic.twitter.com/1e3kGUIc2C,” he added.

He said, “People think a skid group called “Lizard Squad” actually did something… lol. It was hard to find your main group IP either. 188.32.172.91.”

Sony issued a statement about the hacking, which was apparently a DDOS.

“Like other major networks around the world, the PlayStation Network and Sony Entertainment Network have been impacted by an attempt to overwhelm our network with artificially high traffic. Although this has impacted your ability to access our network and enjoy our services, no personal information has been accessed,” the company wrote on its blog. “We will continue to work towards fixing this issue and hope to have our services up and running as soon as possible. We regret any inconvenience this may have caused.”

Here’s the AP update on the matter:

TOKYO— Sony’s PlayStation Network service for video games was unusable from Sunday until Monday afternoon after being flooded by an online attack.

Separately, an American Airlines flight carrying Sony Online Entertainment President John Smedley was diverted to Phoenix while the online attack was happening, Sony Computer Entertainment spokesman Satoshi Nakajima said.

An individual or group called Lizard Squad claimed through a Twitter account there might be explosives on the plane, which was en route from Dallas to San Diego. The account also claimed responsibility for the attack on PlayStation Network.

It was still unclear if the account’s claims were true, Nakajima said.

Sony’s network was compromised for about a month in 2011, including the personal data of 77 million user accounts. The network’s security was upgraded to protect against such attacks.

Sony says there was no breach of personal information in the latest incident, which was resolved by Monday afternoon.

Smedley said on Twitter: “Yes, my plane was diverted. Not going to discuss more than that. Justice will find these guys.”

American Airlines officials in Tokyo were not immediately available for comment.