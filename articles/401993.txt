BlackBerry taps into eavesdropping technology

Share this article: Share Tweet Share Share Share Email Share

Toronto - BlackBerry, pushing further into security services, agreed to buy Secusmart, a provider of anti-eavesdropping technology whose clients include German officials such as Chancellor Angela Merkel. Secusmart, a Dusseldorf, Germany-based company that already had a partnership with BlackBerry, makes voice and data encryption for mobile phones. Financial terms weren't disclosed. The acquisition is BlackBerry's first since the hiring last November of Chief Executive Officer John Chen, who vowed to cut losses by focusing on services to corporations and governments. Now, having stabilised the Waterloo, Ontario-based company, Chen said in an interview yesterday that he's laying the groundwork for hiring and sales growth. With Secusmart, BlackBerry aims to capitalise on demand for spy-proofing technology in the wake of revelations about US government surveillance tactics, including allegations that Merkel's mobile phone was tapped. The deal is still subject to regulatory approval. Chen said he's confident Germany will approve the sale, especially since the government already uses BlackBerry phones and software.

At a time of backlash against US companies, including the German government's decision not to renew a Verizon Communications Inc. contract, Chen said it doesn't hurt being a Canadian company.

“Canadians are very neutral, as you know,” said Chen, who grew up in Hong Kong and is a naturalized US citizen. “Canadians always do business with everybody else and it's fine.”

Germany's government has already distributed about 3 000 encrypted smartphones made by BlackBerry to federal officials, and has plans to order more such devices, Tobias Plate, a spokesman for the Interior Ministry, said at a news conference in Berlin.

Last month, Germany's top prosecutor said it would start a formal investigation into whether US intelligence agents tapped Merkel's phone. The White House has said agents aren't spying on Merkel and has pledged not to do so in the future.

Chen's goal is to return BlackBerry to profitability by the company's next fiscal year, which ends in March 2016. The company is seeking to counter declining demand for its phones by focusing on supplying software and hardware to customers in regulated industries such as finance, government, health care and law, which have higher standards for security and risk management.

BlackBerry announced plans to fire 4 500 employees, or a third of its workforce, less than two months before Chen took over as it tried to streamline the business for a buyout deal that eventually fell through. As the turnaround plan unfolds, Chen said he's ready to start hiring again, with a focus on sales, software development and customer care.

“I have a plan to start slowly adding headcount into the company, and now we're in that phase,” Chen said.

While BlackBerry shares are up 34 percent this year with Chen at the helm, other technology companies are eyeing the same corporate niche he's going after.

This month, Apple and IBM said they would work together to develop mobile applications for businesses. BlackBerry dropped 12 percent the day of the announcement.

“I'm paying a lot of attention” to the partnership, Chen said. “I'm giving them due respect that they could get something done, although they're going to have to come from way behind,” he said.

Part of his plan to stay ahead might include acquisitions, he said. Areas BlackBerry is interested in include server technology and identity management, Chen said.

Still, he said, he's being careful not to risk too much in potential deals.

“It's not, 'Because the future is so bright, I've got to bet the farm,'“ he said. “I'm a very safe, conservative guy.” - Washington Post/Bloomberg

* With assistance from Rainer Buergin in Berlin.