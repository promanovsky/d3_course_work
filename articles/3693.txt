Looks like Stacy Keibler chose to not let the dust settle after her split from George Clooney last July, having reportedly married in a secret Mexican wedding on Saturday (8th March). The former wrestler has found happiness with her longtime friend Jared, who is the chief executive of a private interactive firm called Future Ads.



Stacy Keibler Marries Longtime Friend Jared Pobre In Mexico.

The pair began dating in autumn and evidently realised they had something special pretty quickly. Stacy, 34, married Jared, 39, at an intimate beach ceremony in Mexico in front of a handful of their closest friends and family who hadn't originally known that they were attending a wedding, according to People.

The couple had invited their nearest and dearest on vacation with them and surprised everybody by turning the trip into a wedding event. "We both felt strongly that our 'love day' should be intimately special, and that's exactly what it was," the couple explained. "It was a blend of romance, tranquility, natural beauty, bonding and overwhelming love."

After dating actor George Clooney for two years, Stacy apparently wanted things to move a little faster in terms of commitment and thinking about having children together - something George reportedly wasn't too keen on. For both Stacy and Jared, this is their first marriage and even though their big day came pretty quickly, they seem to be head-over-heels.



Stacy & George Split Amicably Last July.

"My happiness is indescribable! Marriage is the ultimate bond of love and friendship. It means putting all your faith and trust into a person that you can't help but believe is your soul mate. Someone who has all of your best interests at heart; someone handpicked for you, to help you grow and be the best person that you can be. Jared is all of this for me," Stacy revealed.

Meanwhile, Clooney, who was last married from 1989 to 1993, has a colourful history of dating but claims to have never found "the one." When asked by W magazine in December, Clooney replied "I haven't met her yet."