It seems there's no stopping this Internet giant to grow into what could be the biggest organization ever, and the next thing we know, Google is buying this and that company. The latest from its range of new acquisitions is London-based Rangespan, a startup back office service provider for online suppliers and retailers.

Rangespan assists suppliers and retailers in deciding what new products to sell by using data science grounded on real-time sales dynamics. It keeps a database of around 200 million products that are ranked according to forecasted future sales and provides these data to suppliers and retailers in the UK. Aside from providing data on what to sell, it also advises when to sell it.

"We are very happy to announce that Rangespan is joining Google. We will continue to work on services for shoppers and retailers at Google, and we're super excited about the opportunities to come," Rangespan website says.

Backed by Octopus Investments, Rangespan was founded in 2011 by two former employees of Amazon: Ryan Regan and Matt Henderson. Regan worked at e-retailer, as managing director among others, for eight years. Henderson worked as a merchant services director.

Regan, in fact, says he-along with unnamed others-is joining Google. Other people who were listed working for Rangespan are data science director Jurgen Van Gael and technical director James Summerfield-but it wasn't clear if these two directors are also joining them.

"There are a lot of parallels between what we are doing and what Google is doing and we are excited to work together," Regan shares to TechCrunch.

The new acquisition is said to boost and complement the Internet shopping business called Google Shopping by helping the latter work with more retailers and suppliers in managing products and inventory.

"As part of the change, we will wind down Rangespan's services. We've already begun working individually with each of our retailers and suppliers on this process," Rangespan also says.

Research says Amazon previously obtained more product searches than Google, thus clutching away with them some profitable advertising prospects from the latter. That said, the recent deal is deemed perfect fit for Google in its ongoing competition against Amazon, in terms of e-commerce or shopping and for merchants to advertise their products to reach potential consumers all over the globe.

Rangespan is the third business in the line-up of Google acquisitions in the UK, next to anti-malware startup Spider.io and artificial intelligence specialist DeepMind.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.