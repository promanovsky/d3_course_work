Thursday, a week after David Letterman announced his plans for a 2015 retirement, CBS formally announced that the "Late Night" host's successor will be Comedy Central funnyman Stephen Colbert.

As expected, the news has been met with mixed reactions. One especially intriguing response came from fellow late-night host Arsenio Hall, who tweeted:

Even though Dave Letterman wants ME to replace him, @CBS wants Stephen Colbert! Oh well. You go SC! #congratz — Arsenio Hall (@ArsenioHall) April 10, 2014

Hall's tweet sparked a viral debate, with some onlookers supporting the comedian's comment and others wondering if it was a joke. Hall clarified the matter during Thursday's episode of "The Arsenio Hall Show."

"The tweet blew up. I got a lot of retweets and people kept asking me if it's really true ... and it is. Dave asked me personally," Hall said before playing an altered, spoof clip of his September 2013 appearance on "Late Night."

"Stephen Colbert is a wonderful, talented guy who will do a great job. And don't you feel weird, Stephen, being Dave's second choice?" Hall said following the clip. "Y'all are blinking that sarcasm sign, right?"

Check out Arsenio Hall's hilarious joke in the clip above.

