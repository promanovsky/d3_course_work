People who jogged or ran for as little as five minutes a day reduced their risk of premature death by nearly one-third and extended their lives by about three years, according to a new study.

Researchers examined the exercise habits of more than 55,000 adults in the Dallas area who were monitored for six to 22 years. About 24% of the adults described themselves as runners.

Compared to those who didn’t run, those who did were 30% less likely to die of any cause during the course of the study. They were also 45% less likely to die as a result of cardiovascular disease, researchers reported Monday in the Journal of the American College of Cardiology. (These figures were adjusted to take into account people’s smoking and drinking habits, how old they were when they enrolled in the study, their family’s health history and their other exercise habits.)

Put another way, non-runners were 24% more likely than runners to die during the study period. In fact, the mortality risk associated with not running was greater than the mortality risk associated with being overweight or obese (16%), having a family history of cardiovascular disease (20%), or having high cholesterol (6%).

Advertisement

The researchers divided up the roughly 13,000 runners into five groups based on how many minutes they ran per week. Those in the lowest group ran up to 50 minutes over a seven-day period, and those in the highest group ran for more than 175 minutes over the course of a week.

But the benefits of running were pretty much the same for all runners, according to the study.

“Running even at lower doses or slower speeds was associated with significant mortality benefits,” the researchers found.

They also measured running in other ways – by total weekly distance, frequency, speed and the “total amount of running” (which was calculated by multiplying duration and speed). In all categories, even the runners in the lowest groups were less likely to die during the study than the non-runners.

Advertisement

In order to reduce the risk of premature death, all it took was 30 to 59 minutes of running per week, the researchers calculated.

“This finding has clinical and public health importance,” according to the team from Iowa State University, the University of South Carolina, Louisiana State University and the University of Queensland School of Medicine.

“Because time is one of the strongest barriers to participate in physical activity, this study may motivate more people to start running and continue to run as an attainable health goal for mortality benefits,” they wrote. People who can’t devote 15 or 20 minutes to moderate physical activity each day may appreciate the efficiency of a five-minute run, they added.

If all of the non-runners had taken up running, 16% of the 3,413 deaths that occurred during the study could have been averted, the researchers wrote. That would have saved 546 lives.

Advertisement

It’s not clear that the findings of this study would apply to the nation as a whole. Most of the adults who were tracked were college-educated, middle-class or upper-middle-class whites. However, the researchers noted that the “physiological characteristics” of the study participants were “similar” to those of samples that are more diverse.

In an editorial that was published alongside the study, researchers from Taiwan urged doctors to use this information to motivate their patients to exercise, even if it’s only for a few minutes a day.

“It is important to promote exercise by stressing the potential harm of inactivity,” they wrote. “Warn patients that inactivity can lead to a 25% increase in heart disease and a 45% increase in cardiovascular disease mortality, not to mention a 10% increase in the incidence of cancer, diabetes, and untold depression.”

Three of the editorial’s four authors worked on a 2011 Lancet study that found that even 15 minutes of brisk walking per day could extend a person’s life expectancy. Both that study and the new study are “good news to the sedentary.”

Advertisement

“Exercise is a miracle drug in many ways,” they wrote in the editorial. “The list of diseases that exercise can prevent, delay, modify progression of, or improve outcomes for is longer than we currently realize.”

The study was funded in part by Coca-Cola Co., which has emphasized the role of physical inactivity in the nation’s obesity crisis. The National Institutes of Health helped pay for the study as well.

For the latest news about science and medicine, follow me on Twitter @LATkarenkaplan and “like” Los Angeles Times Science & Health on Facebook.