Toni Garrn is a light blue beauty as she steps out at the 2014 Met Gala held at the Metropolitan Museum of Art on Monday (May 5) in New York City.

The 21-year-old German model opted for a gorgeous Topshop gown that night.

PHOTOS: Check out the latest pics of Toni Garrn

“Thank for taking such good care of me tonight!! @wendyrowe @braydon_nelson @topshop #metball #traffic,” she tweeted earlier in the day.

Over the weekend, Toni looked picture perfect as she attended super hairstylist Harry Josh‘s second annual #HarrysParty at The Jane Hotel in the West Village.

FYI: Toni completed her look with Stephen Russel jewelry, Lee Savage bag, and Gianvito Rossi shoes.