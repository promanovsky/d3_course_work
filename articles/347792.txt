Numerous vaccine opponents link some immunizations to long-term, detrimental side effects. It was believed vaccinations such as the Measles, Mumps, and Rubella (MMR) were associated with autism, but a new analysis of 20,000 reports suggests that this is not the case.

Vaccines are responsible for the elimination of countless diseases, most notably polio and smallpox, among others. However, recently many parents have opted to not vaccinate their children due to speculation of links to autism, and some of the previously eliminated ailments began to resurface, including measles, mumps, and pertussis.

In recent months, Ohio State University experienced a mumps outbreak that ended up spreading to nearby counties in Central Ohio. The total number of cases in a mere two-month span exceeded the total for all of the United States in 2013. Health officials announced last week that the case total reached 444, more than half of which were linked to the Ohio State outbreak.

Additionally, from January 1 to June 27, the Centers for Disease Control and Prevention tallied a total of 539 measles cases throughout the US with 17 outbreaks occurring in 20 states. The 17 outbreaks contributed to 88% of the cases. The total measles count thus far has exceeded numbers from 2013 already and are the most documented in the United States since 1996.

The latest study examining the side effects of vaccinations, "Immunization Exemptions Leave Kindergarten Entrants at Higher Risk for Vaccine-Preventable Diseases," was published on Tuesday in the journal Pediatrics. The researchers analyzed over 20,000 reports published between 2010 and 2013 and specifically referenced 67 of those reports that effectively demonstrated the low-risk factors of vaccinations through control and comparison groups.

The researchers found that there is no link between vaccines and leukemia or food allergies, and the MMR vaccine is not associated with autism, although it can occasionally result in severe side effects such as fever and/or seizures. They concluded that vaccinations are "one of the greatest public-health achievements of the 20th century for their role in eradicating smallpox and controlling polio, measles, rubella and other infectious diseases in the United States," in this Time Magazine article.

Medical experts hope that this comprehensive study will coax parents into getting their children vaccinated in order to avoid the prevalence of previously eliminated/uncommon diseases. You can read more about the study in this CNN News article.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.