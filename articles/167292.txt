AOL responds to widespread email spamming hack

If you’ve been receiving odd emails from an AOL email address, you’re not alone. A massive hack led to many accounts being compromised, used to send out spammy emails that included catchy headlines and suspect links. AOL has issued a response, saying their investigation ingot he matter is ongoing, and users should take steps to protect themselves.



Though AOL says there is “no indication that the encryption on the passwords or answers to security questions was broken”, they nonetheless advise users to reset passwords and security info. The company is notifying potential victims of the hack as they can, but if you’re still using AOL email, it’s likely a good time to change your information.

AOL is also committed to seeking justice, saying they are “working closely with federal authorities to pursue this investigation to its resolution”. Again, the investigation is ongoing, and the emails have not stopped flowing. If you receive suspect email, AOL asks that you not open it or provide any personal information.

AOL says they’re addressing the situation “as quickly and forcefully” as they can. If you think your AOL email has been spoofed, the company suggests you notify possible recipients of the issue as well as change your information. That can be difficult, though, considerign the scope of this breach.

Source: AOL

