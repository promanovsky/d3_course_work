The fiscal third quarter was Microsoft Corporation's ( MSFT ) first since Satya Nadella took over. The new CEO has taken a candid approach to Microsoft's current position and opportunities, which along with better-than-expected quarterly results impressed both investors and analysts. As a result, shares appreciated 2.5% in extended trading.

Nadella's vision includes the repositioning of Microsoft in a cloud-first mobile-first world where Microsoft's services and technology will reach a larger number of devices across the existing PC and mobile platforms, as well as new and even undiscovered platforms like wearables and the Internet of Things (IoT).

While Nadella said that the last quarter was spent "nailing the basics," he also stressed that he intended to increase transparency and accountability to stakeholders. Execution in this transition phase will be key to Microsoft's future prospects, so let's dig into the quarter's details-

Revenue

Revenue of $20.40 billion was down 16.8% sequentially and 0.4% from last year, roughly in line with our estimates.

Devices & Consumer Segment

Management changed the operating structure at the beginning of fiscal year 2014. Accordingly, the company now has two main segments: Devices & Consumer (D&C) and Commercial.

D&C Licensing, hardware and other revenue made up 21%, 10% and 9% of quarterly revenue, taking the contribution of the Devices & Consumer segment to 40%.

The licensing side of the business did moderately well, with both Windows and Consumer Office growing from the year-ago quarter. Within Windows, OEM Pro revenues were particularly strong, as businesses of all sizes increased investments (at least partially helped by the withdrawal of XP support). Microsoft also benefited from higher sales in developed regions, which resulted in higher attach rates. Non Pro revenue remained extremely weak, particularly in China.

Hardware (Xbox and Surface platforms) was impacted by seasonality, but increased over 40% from the year-ago quarter. Revenue from the Xbox platform grew 45% with volumes at 2 million (1.2 million were Xbox Ones). Surface revenue increased more than 50% with a mix shift toward second generation devices.

Other revenue increased 8.8% sequentially and 17.8% year over year. All of the main drivers, i.e. Office 365 Home, Bing and Xbox Live contributed to the increase. Particularly, Home subscriptions increased by around a million over the past year, Bing picked up 170 bps of market share (search advertising revenue up 38%) and transaction revenue from LIVE members grew 17%.

Commercial Segment

Commercial licensing revenues dropped 5.2% sequentially, remaining 3.4% above year-ago levels. Server and Office commercial revenues were up 10% and 6%, respectively. The two bright spots were SQL Server (up 15%) and Windows volume licensing (up 11%).

Commercial Other revenues increased 6.9% sequentially and 31.1% year over year. Cloud services are becoming more popular, with commercial cloud, Office 365 and Microsoft Azure growing triple digits.

Operating Results

Microsoft's gross margin of 70.9% was up 467 basis points (bps) sequentially and down 575 bps from the year-ago quarter. The sequential increase was due to the much higher mix of hardware products and Xbox One launch costs in the Dec 2013 quarter. The decline from last year was partly mix-related, as Microsoft sold more game consoles in the last quarter.

At the same time, a higher mix of second generation Surface tablets was positive for the margin. Total D&C gross margin was up 777 bps sequentially and down 748 bps year over year. Commercial was down 180 bps sequentially and 79 bps year over year.

Operating expenses of $7.49 billion were down 9.4% sequentially and 7.4% from last year. The company is clearly focused on innovation as reflected in its R&D spending, which increased as a percentage of sales by 244 bps and 56 bps, respectively from the previous and year-ago quarters.

G&A also increased sequentially while other expenses declined. Cost of sales was the most significant contributor to the decline from last year. As a result, the operating margin expanded 168 bps sequentially and shrank 297 bps year over year to 34.2%.

The company generated a net income of $5.66 billion, or 27.7% net income margin compared to $6.56 billion, or 26.7% in the previous quarter and $6.06 billion, or 29.6% in the year-ago quarter. Since there were no one-time items in any of the quarters, the pro forma and GAAP earnings were same at 68 cents a share compared to 78 cents in the previous quarter and 72 cents in the year-ago quarter.

Balance Sheet

Inventories jumped 20.5%, with inventory turns sliding from 20.8X to 12.4X. Management said that inventories would be worked down in the ongoing quarter. Days sales outstanding (DSOs) went from 59 to 60.

Microsoft ended with a cash and short-term investments balance of $88.43 billion, up $4.48 billion during the quarter. The net cash position was around $65.75 billion ($7.96 a share), up from $60.97 billion at the beginning of the quarter.

In the last quarter, the company generated $ 10.10 billion in cash flow from operations, spent $300 million to repurchase its debt, $1.85 billion to repurchase shares, $2.32 billion to pay dividends, and $1.19 billion to purchase capital assets.

Guidance

For the fourth quarter, the company expects D&C licensing revenue of $4.1-4.3 billion, D&C hardware revenue of $1.3-1.5 billion, D&C other revenue of $1.9 billion, Commercial licensing revenue of $11.0-11.2 billion and Commercial other revenue of $2.1 billion. Deferred revenue is expected to be $100 million. Microsoft expects COGS of $5.7-5.8 billion and opex of $8.4-8.6 billion.

Recommendation

Microsoft reported another good quarter that exceeded our expectations. The consumer side of the business was notably weaker (as expected), but it's heartening to see the growth in tablets. This is strategically a very important segment for Microsoft, since tablets continue to eat into its core computing business. Also, consumer purchases, particularly in emerging markets continue to get more mobile.

The company is still not making money on its surface devices however and management did not say when the business was expected to break even. But in the meantime, it is sparing no expense to succeed. Of course, a higher percentage of hardware in the mix is going to tell on margins and this should be expected. The addition of Nokia's business will add to the pressure on margins.

However, business PC spending is clearly on the rise, helped by the withdrawal of support for XP (as expected). But Microsoft said that overall spending on Windows is also increasing with 90% of desktops now using Windows 7 or Windows 8. Microsoft is also seeing huge momentum in the cloud, which is likely to be a major growth diver.

The devices and services strategy looks far more sensible now that it's being called the mobile-first cloud-first strategy. Innovation and execution will be the main drivers of future growth, so R&D is likely to remain at elevated levels.

Microsoft shares carry a Zacks Rank #3 (Hold). Better-ranked technology stocks to consider at this time are Micron Technology ( MU ), SanDisk Corp ( SNDK ) and Western Digital ( WDC ), all of which carry a Zacks Rank #2 (Buy).

MICROSOFT CORP (MSFT): Free Stock Analysis Report

MICRON TECH (MU): Free Stock Analysis Report

SANDISK CORP (SNDK): Free Stock Analysis Report

WESTERN DIGITAL (WDC): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.