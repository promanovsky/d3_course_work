The nation's schools to shut down and most civil servants to stay home as fears deepened over the virus that already has killed more than 670 people in West Africa

Freetown: Sierra Leone has declared a public health emergency to tackle the worst ever outbreak of Ebola and will call in security forces to quarantine epicentres of the deadly virus, President Ernest Bai Koroma said in a statement.

The measures resembled a tough anti-Ebola package announced by neighbouring Liberia on Wednesday evening. Koroma also announced he was cancelling a visit to Washington for a US-Africa summit next week because of the crisis.

Koroma also ordered the nation's schools to shut down and most civil servants to stay home as fears deepened over the virus that already has killed more than 670 people in West Africa making it the largest recorded Ebola outbreak in history.

Highly infectious Ebola has been blamed for 672 deaths in the West Africa nations of Liberia, Guinea and Sierra Leone, according to the World Health Organization.

Ebola has no vaccine and no specific treatment, with a fatality rate of at least 60 percent.

Experts say the risk of travelers contracting Ebola is considered low because it requires direct contact with bodily fluids or secretions such as urine, blood, sweat or saliva. The virus can't be spread like flu through casual contact or breathing in the same air.

Patients are contagious only once the disease has progressed to the point they show symptoms, according to the World Health Organization. The most vulnerable are health care workers and relatives who come in much closer contact with the sick.

Reuters and AP