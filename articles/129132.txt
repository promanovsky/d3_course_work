Nokia’s much awaited Windows tablet was released late last year. The Lumia 2520 received good reviews but so far it is not entirely known just how well the tablet has performed in the market. Today Nokia announced that it is suspending the sales of Lumia 2520 in Austria, Denmark, Finland, Germany, Russia, Switzerland and the UK. The company discovered that the AC-300 charger it bundles with the tablet may cause an electric shock if touched when it is still plugged in a live socket. The company says its manufactured by a third party supplier.

Advertising

Nokia estimates that approximately 30,000 chargers are impacted including 600 travel charger accessories sold in the U.S. Jo Harlow, EVP of Smart Devices at Nokia, said that there have been no “confirmed consumer incidents” related to this issue. The tablet is still being sold in the U.S. though Nokia has suspended sales of the travel charger accessory.

Nokia “strongly” advises customers not to use the charger until further notice. The company says that its working with suppliers to minimize the inconvenience as far as replacement chargers are concerned. Since the AC-300 charger is exclusive to the tablet the issue doesn’t affect other Nokia chargers, which can be used without any worries.

So if you happen to have the AC-300 charger with your Nokia Lumia 2520 its best to listen to the company. There have been countless tales of people getting injured and even meeting their maker due to faulty chargers.

Filed in . Read more about Lumia 2520 and Nokia.