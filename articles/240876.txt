Hewlett-Packard plans to cut as many as 16,000 more jobs in a major ramp-up of chief executive Meg Whitman's effort to turn around the personal computer maker and relieve pressure on its profit margins.



Whitman said the turnaround remained on track and her raised target reflected how HP continued to find areas to streamline across its broad portfolio.

This encompasses computing, networking, storage and software.

But some analysts wondered whether it signalled a worsening outlook for the coming year, or if more jobs may be cut.

HP, whose sprawling global operations employ more than 250,000, estimated about three years ago when it first announced its sweeping overhaul that it would need to shed 27,000 jobs.

That number rose to 34,000 last year.



Last night it estimated another 11,000 to 16,000 more jobs needed to go, scattered across different countries and business areas. That took the grand total under Whitman's restructuring to 50,000.

HP is one of Ireland's biggest multinational employers with plants in Leixlip, Co Kildare and Ballybrit in Galway. It employs about 4,000 here.

A spokesperson for HP said it was too early to say whether or not the further reduction in its global workforce would have an impact on its Irish operations.

The spokesperson said a decision will be made about where any cuts will take place after a period of consultation.

The Silicon Valley company is trying to reduce its reliance on PCs and move toward computing equipment and networking gear for enterprises, part of Whitman's effort to curtail revenue declines and return the world's biggest PC maker to growth.



But that goal remains elusive. The company posted a disappointing 1% drop in quarterly revenue, as it struggled to maintain its grip on the shrinking personal computer market and weak corporate tech spending. That marked its 11th consecutive quarterly sales decline.



Shares in HP closed down 2.3 percent at $31.78, after the company inadvertently posted the results on its website more than half an hour before the closing bell.



Research jobs, which are vital for innovation and long-term growth, will continue to grow, Whitman stressed. HP is looking to cut back more in "areas not central to customer-facing and innovation agendas," she said in an interview, rather than areas like research.

HP recorded sales of $27.3 billion in its fiscal second quarter ended April 30, just shy of the $27.41 billion Wall Street had expected.



Whitman said China remained a challenging region, though revenue from that country rose in the quarter. US companies like IBM and Cisco Systems have blamed recent lacklustre performances on a backlash against American companies in China, in the wake of US spying allegations.



HP last night forecast full-year earnings of $3.63 to $3.75 a share, compared with Wall Street's estimate for $3.71.