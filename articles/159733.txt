Dave Toycen, World Vision Canada's president and CEO, recently visited Bangladesh as part of the agency's No Child For Sale Campaign.

One year ago today (April 24), the Rana Plaza garment factory collapsed on top of thousands of Bangladeshi workers. The world watched in disbelief as body after body was pulled from the rubble. More than 1,100 people died under the factory's hulking weight.

In the weeks to follow, the investigation into what caused the collapse gave Canadians a better understanding of the global garment industry. Before the collapse, complex global supply chains made it easy for many of us to think no further than the brand names on our jeans. We didn't have a way of making the link between the things we buy and the desperately poor people who suffer long hours in stifling conditions to make them.

Story continues below advertisement

But as details were uncovered about people who worked at Rana Plaza, it was nearly impossible not to feel connected, even complicit. We hadn't meant to contribute to circumstances that left pregnant women widowed and young children orphaned. Yet we all demand the vast selection and low prices that helped create this horrific outcome.

While working in Bangladesh last week, I took some time to visit the place where Rana Plaza stood. There's a real heaviness about the place, a deep sadness that won't go away. Looking at the sea of rags, threads and garment labels all tangled up in the bricks and rubble, I felt convicted once again that things needed to change.

Like many Canadians, I've been personally changed by this tragedy. Yet for most Bangladeshis, life goes on as usual. The poor may know all about the danger, cruelty and unfairness of their work situations – but that doesn't mean they can escape them. Everywhere I looked, there were workplace tragedies just waiting to happen. Far too many involved children.

In open-air markets, boys between the ages of 10 and 15 haul bags half their weight. Their young backs are already permanently bowed from the continuous strain. Just around the corner in machine shops, other boys work far too quickly, using huge power tools that could slice off a hand.

I met with a group of 22 girls barely into their teens, working full-time shifts in shrimp processing plants. Shrimp is Bangladesh's second-largest industry, after garments. When the girls' sharp fish knives slice into their own flesh, they are told to continue working. The blood is simply washed off further down the line. All local fish processing plants have signs posted at the front gate reading: "No employment under 18". It's not being rigorously enforced.

All of these children had every reason to be hopeless, yet I was struck by the tremendous light in their eyes. Wherever I travelled, girls and boys were quick to break into the great anthem: We shall overcome. I was humbled by the commitment that drives many children to show up at night school, on empty stomachs at the end of a long work day. They're determined to continue their education and pursue their dreams.

But I was also personally challenged by the song the young girls sing together as they work in the shrimp processing plant: We are the child labour. We want to do something exceptional for our country. Don't misjudge us…don't neglect us.

Story continues below advertisement

Rana Plaza is a powerful memorial to the workers who suffered and died there. But the tragedy can also become a rallying cry for change. For the sake of children whose parents were lost in the factory, and for those who forfeit their childhoods to toil in conditions that are dirty, dangerous and degrading, there's more we need to do.

Canadian retailers that sell products made in Bangladesh must not delay in signing the Bangladesh Fire and Safety Accord. Signing will obligate companies to take immediate action that would prevent further tragedies and protect the income of workers displaced due to unsafe conditions. It's a first step along a critical path toward addressing exploitative and unsafe labour – including child labour – in countries around the world. To date, only one Canadian retailer, Loblaw, has signed this accord.

Canadians have an important role to play in pressing companies to ensure workers are treated safely and fairly. Canadians must ask companies where and how their products are made, via company websites, Facebook and Twitter.

The Canadian government has supported the International Labour Organization's initiative to improve working conditions in Bangladesh and has also provided major support for primary education, and we encourage them to help parents keep their children in school. There is a pressing need to strengthen skills training, so children have safe, age-appropriate ways to earn money when there's no other choice.

For the sake of children in Bangladesh and around the world, we must all act together. We must not neglect them. We must help them overcome.