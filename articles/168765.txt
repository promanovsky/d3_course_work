Craig Ferguson announced to his audience that he will be leaving The Late Late Show when his contract is up at the end of 2014.

Photo credit: Daniel Tanner/WENN.com

Craig Ferguson has spent the last 10 years building a late-night show that dares to be different from the rest, but he announced to his live audience today that it will all be coming to an end when his contract with CBS expires in December of this year, according to Entertainment Weekly.

“CBS and I are not getting divorced, we are ‘consciously uncoupling,'” the talk show host said in a statement. “But we will spend holidays together and share custody of the fake horse and robot skeleton, both of whom we love very much.”

Ferguson’s “conscious uncoupling” reference was, of course, a nod to the recent breakup of Gwyneth Paltrow and Chris Martin’s marriage. Paltrow raised some eyebrows when she announced her separation from her husband of 10 years on her website, Goop.com, using what some interpreted as a pretentious term.

The Scottish funny man’s departure is just the latest move in what appears to be a giant game of musical chairs in the world of late-night talk shows. In February, Jimmy Fallon took over The Tonight Show after Jay Leno’s contract was not renewed following his nearly-22-year-long run as host. Then, in early April, David Letterman announced he would be retiring from the Late Show in 2015. Stephen Colbert will be stepping in as Letterman’s successor.

Now, let the replacement speculation begin! One professional talker we know for sure who won’t be stepping in: Chelsea Handler. At least, for now. After Handler posted a cryptic tweet involving a meeting with The Late Late Show‘s network, CBS released a statement that the comedienne was not in the job running. “There are no discussions with Chelsea Handler regarding the network’s 12:30 late night broadcast; her meeting with CBS yesterday was a general meeting with our syndication group,” a CBS spokesperson said.

CBS did release a statement regarding Ferguson’s departure that confirmed the separation is amicable. “During his ten years as host, Craig has elevated CBS to creative and competitive heights at 12:30,” said CBS chair, Nina Tassler. “He infused the broadcast with tremendous energy, unique comedy, insightful interviews, and some of the most heartfelt monologues seen on television… while we’ll miss Craig and and can’t thank him enough for his contributions to both the show and the Network, we respect his decision to move on, and we look forward to celebrating his final broadcasts during the next eight months.”