Phil Collins has the world's largest private collection of Texas Revolution artefacts

Phil Collins is giving away his vast collection of Texas Revolution artefacts.

The 63-year-old Genesis singer and drummer is looking for a home for his collection relating to the 1836 Battle of the Alamo and the revolution, which up until now has been kept at his home in Switzerland.

Texas Land Commissioner Jerry Patterson said some artefacts could go on display at the Alamo within a year.

Phil first became an Alamo aficionado watching a 1950s Disney miniseries on Davy Crockett as a lad in England.

Patterson said the musician has the world's largest private collection of Texas Revolution artefacts.

His most prized item is a receipt signed by Alamo commander William Barret Travis.