A new study from researchers at the Massachusetts General Hospital for Children in Boston has found further links between poor sleep habits and childhood obesity. For the study the researchers reviewed sleep habits of 1,000 children between the ages of six months to seven years.

They found that those children who slept less than the average amount of hours per night in their age group were two and half times more likely to develop obesity at some point in their childhood. They were also two and half times more likely to have excess body fat, especially in the mid-section.

"Insufficient sleep is an independent and strong risk factor for childhood obesity and the accumulation of total fat and abdominal fat," the researchers explained. "The main strength of this study is we looked at sleep at multiple periods."

For comments and feedback contact: editorial@rttnews.com

Health News