Chinese scientists say there could one day be a high-tech submarine that crosses the Pacific Ocean in less time than it takes to watch a movie, the South China Morning Post reports.

Researchers at the Harbin Institute of Technology, in northeast China, have made dramatic improvements to a Soviet-era military technology called supercavitation that allows submersibles to travel at high speeds, the Post says.

Supercavitation envelops a submerged vessel inside an air bubble to minimize friction. It enabled the Russian Shakval torpedo to reach speeds of 230 m.p.h. — but theoretically, a supercavitated vessel, given sufficient power at launch, could reach the speed of sound (some 3,603 m.p.h.). That would mean crossing the 6,000-odd miles from San Francisco to Shanghai in just two hours.

One of the problems of supercavitation has been how to steer a vessel at such speeds. The Harbin scientists say they could have the answer.



According to the Post, they’ve developed a way of allowing a supercavitated vessel to shower itself with liquid while traveling inside its own air bubble. The liquid creates a membrane on the surface of the vessel, and by manipulating this membrane, the degree of friction applied to different areas of the vessel could be controlled, which would enable steering.

“We are very excited by its potential,” said Li Fengchen, professor of fluid machinery and engineering at the Harbin Institute’s complex flow and heat transfer lab. “By combining liquid-membrane technology with supercavitation, we can significantly reduce the launch challenges and make cruising control easier,” he told the Post.

Li stressed, however, that many technical problems needed to be solved before supersonic submarine travel could take place.

[SCMP]

Get our Space Newsletter. Sign up to receive the week's news in space. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Charlie Campbell at charlie.campbell@time.com.