Facebook’s Sheryl Sandberg issued an apology Wednesday, but British data protection authorities are continuing an investigation into revelations that Facebook conducted a psychological experiment on its users.

The Information Commissioner's Office — a U.K. authority that aims to uphold information rights in the public interest — said it wants to learn more about the circumstances of the experiment carried out by two U.S. universities and the social network.

The commissioner's office is working with authorities in Ireland, where Facebook has headquarters for its European operations. French authorities are also reviewing the matter.

The researchers manipulated the news feeds of about 700,000 randomly selected users to study the impact of "emotional contagion," or how emotional states are transferred to others. The researchers said the evidence showed that emotional contagion occurs without direct interaction between people and "in the complete absence of nonverbal cues.”

Facebook acknowledged that the research was done for a single week in 2012.

The survey provoked an outcry on social media sites and sparked essays in media outlets including The New York Times and The Atlantic about the ethics of manipulating users' feeds without their consent.

Facebook's chief operating officer, Sheryl Sandberg, told television network NDTV in India that "we clearly communicated really badly about this and that we really regret." Later she added: "Facebook has apologized and certainly we never want to do anything that upsets users."

Facebook's data use policy says the company can use user information for "internal operations, including troubleshooting, data analysis, testing, research and service improvement."

The concern over the experiment comes amid interest in Europe about beefing up data protection rules. The European Court of Justice last month ruled that Google must respond to users' requests seeking to remove links to personal information.

Suzy Moat, a Warwick Business School assistant professor of behavioural science, said businesses regularly do studies on how to influence behaviour. She cited the example of Facebook and Amazon experimenting with showing different groups of people slightly different versions of their websites to see if one is better than another at getting customers to buy products.

"On the other hand, it's extremely understandable that many people are upset that their behaviour may have been manipulated for purely scientific purposes without their consent," Moat said. "In particular, Facebook's user base is so wide that everyone wonders if they were in the experiment."