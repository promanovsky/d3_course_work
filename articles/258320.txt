Kim was truly a vision at her wedding to Kanye West on May 24. Her makeup looked flawless, and she surprised us by wearing her hair down during the ceremony and reception!

Kim Kardashian, 33, looked seriously stunning as she wed Kanye West in Florence, Italy on May 24. She was beaming during her vows, rocking a bronzed glow, dramatic lashes and her hair down and tucked behind her ears.

Kim Kardashian’s Wedding Makeup — Stunning Beauty Look

Kim’s makeup was done by longtime friend and makeup artist Mario Dedivanovic. Kim used to work with Mario years ago and he has recently reconnected with the star — now we know why!

Her hair was done by Jennifer Aniston‘s BFF and business partner Chris McMillan. Take Our Poll

Mario posted the stunning pic of Kim’s beauty from the wedding photo booth on May 25, writing: “Sneak peek Wedding glam

on @kimkardashian #makeupbymario

#muse. Hair by @mrchrismcmillan.”

E! Online released the first official wedding pictures on May 27, showing off Kim’s gorgeous look at the alter and walking down the aisle with her new husband!

Kim’s Sleek & Shiny Hair

Kim let her long and shiny hair cover the back of her stunning Givenchy gown. The back was a sheer panel, so the hair made the dress slighly less revealing and more tasteful.

Her hair was down and simply tucked behind her ears for the ceremony. During the reception, Kim pulled her hair out and rocked soft curls that framed her face.

Later, she appeared to have a few pieces pulled back, showing off her perfect cheekbones and long, luxe lashes.

Kim rocked a nude lip, her signature look that must make her feel the most comfortable.

Click to see more pics of her beauty in the gallery.

Do you think it’s cool Kim wore her hair down for her wedding, HollywoodLifers?

— Dory Larrabee

More Kim Kardashian Beauty News: