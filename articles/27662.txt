Theo James had to lie to Shailene Woodley about her cooking

Theo James has joked about having to use his acting skills when Shailene Woodley cooked for him.

The British actor, who stars with Shailene in Divergent, admitted he had to lie when she made him a meal.

"She cooked for me several times and I would say to her, 'Oh no, it's tasty,' but I'm not a fan," he told Access Hollywood.

"Not because she a bad cook, but because [the food was] very pure and my taste buds are not as refined."

Theo, who plays Shailene's on-screen love interest Tobias 'Four' Eaton in the big-screen adaptation of Veronica Roth's novel, also revealed he had to put on his poker face after she indulged in raw garlic.

"She's big into garlic. She would literally eat raw garlic and she would try and tempt me that way. I tried it once and nearly threw up," the 29-year-old said.

Meanwhile, Shailene revealed why she is a fan of eating clay.

"Clay is amazing," she said of her unusual diet choice. "If you look it up, indigenous cultures around the world, they all eat it and have for many, I mean thousands of years."

She added: "What it does is your body doesn't absorb clay. It binds to radioactive isotopes as well as heavy metals and it helps your body eliminate it. So it's one of the only ways to get rid of radiation out of our bodies."