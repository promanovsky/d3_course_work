Sue Townsend, the best-selling author of the Adrian Mole books, has died aged 68.

The cherished British writer was surrounded by loved ones when she passed away at her home in Leicester.

"Sue passed away on Thursday night," a friend of the family confirmed in a statement.

"She was with her family. She'd been very ill recently - she'd suffered a stroke - and succumbed to that illness."

Townsend, a passionate socialist who was left blind due to the complications of her diabetes, achieved worldwide success for her book series about the teenager Adrian Mole, who battles haphazardly through adolescence under Margaret Thatcher’s government and, in later books, struggles to come to terms with maturity when Tony Blair is the Prime Minister.

Adrian Mole's best quotes Show all 10 1 /10 Adrian Mole's best quotes Adrian Mole's best quotes Adrian Mole's best quotes “I have a problem. I am an intellectual, but at the same time I am not very clever." Rex Features Adrian Mole's best quotes Adrian Mole's best quotes “Guilt is a destructive emotion and doesn’t fit in with my Life Plan.” Rex Features Adrian Mole's best quotes Adrian Mole's best quotes “I asked Pandora how long she would love me. She said, ‘As long as Britain has Gibraltar.’” Rex Features Adrian Mole's best quotes Adrian Mole's best quotes “I have realised I have never seen a dead body or a real female nipple. This is what comes of living in a cul-de-sac.” Rex Features Adrian Mole's best quotes Adrian Mole's best quotes “My skin is dead good. I think it must be a combination of being in love and Lucozade.” Rex Features Adrian Mole's best quotes Adrian Mole's best quotes “I was racked with sexuality but it wore off when I helped my father put manure on our rose bed.” YouTube/Thames TV Adrian Mole's best quotes Adrian Mole's best quotes “Mrs Thatcher has got eyes like a psychotic killer, but a voice like a gentle person. It is a bit confusing.” YouTube/Thames TV Adrian Mole's best quotes Adrian Mole's best quotes “Perhaps when I am famous and my diary is discovered people will understand the torment of being a 13 3/4 year old undiscovered intellectual.” YouTube/Thames TV Adrian Mole's best quotes Adrian Mole's best quotes “My mother is in the hospital grounds smoking a cigarette. She is looking old and haggard. All the debauchery is catching up with her.” YouTube/Thames TV Adrian Mole's best quotes Adrian Mole's best quotes “The tap drips and keeps me awake/ In the morning there will be a lake” – one of the first poems written by Adrian Albert Mole, aged 13 ¾. YouTube/Thames TV

The first novel, Secret Diary of Adrian Mole, Aged 13 ¾, was published in 1982. She followed the success of her debut diary with seven sequel novels, which went on to sell more than 8million copies.

Adrian Mole has been translated into nearly 30 different languages, and has been adapted for TV and for the stage.

Stephen Mangan, who played Adrian Mole in a 2001 BBC series, paid tribute to the late author.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

"Greatly upset to hear that Sue Townsend has died," he said. "One of the warmest, funniest and wisest people I ever met."

Townsend, who also wrote non-fiction, plays and other novels, including 1992 best-seller The Queen And I, started her career after winning a Thames Television playwrighting bursary.