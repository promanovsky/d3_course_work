Researchers have reached a step closer to Alzheimer's treatment by coming up with a new blood test which can predict the commencement of the disease in people.

Scientists from King's College London and UK proteomics company, Proteome Sciences plc, analysed over 1,000 individuals, and have identified a set of 10 proteins in the blood which can foresee the start of disease, marking a significant step towards developing a blood test for the disease.

The researchers used data from three international studies. Blood samples from a total of 1,148 individuals (476 with Alzheimer's disease; 220 with 'Mild Cognitive Impairment' (MCI) and 452 elderly controls without dementia) were analysed for 26 proteins previously shown to be associated with Alzheimer's disease. A sub-group of 476 individuals across all three groups also had an MRI brain scan.

Researchers identified 16 of these 26 proteins to be strongly associated with brain shrinkage in either MCI or Alzheimer's, and identified a combination of 10 proteins capable of predicting whether individuals with MCI would develop Alzheimer's disease within a year, with an accuracy of 87 percent.

Lead author of the study Dr Abdul Hye, said that the proteins could predict whether someone with early symptoms of memory loss, or mild cognitive impairment, would develop Alzheimer's disease within a year, with a high level of accuracy.

Professor Simon Lovestone, senior author of the study added that Alzheimer's begins to affect the brain many years before patients are diagnosed with it and most of the drug trials fail because by the time patients were given the drugs, the brain had already been too severely affected. A simple blood test could help in identifying patients at a much earlier stage to take part in new trials and hopefully develop treatments which could prevent the progression of the disease.

Alzheimer's disease is the most common form of dementia, and it is estimated that globally, 135 million people will have dementia by 2050. Approximately 10% of people diagnosed with MCI develop dementia within a year, but apart from regular assessments to measure memory decline, there is currently no accurate way of predicting who would, or wouldn't, develop dementia.

The study is published in Alzheimer's and Dementia: The Journal of the Alzheimer's Association.