US stocks fell at the open on Tuesday as investors continued to hold off from making big bets going into the start of corporate earnings season.



The Dow Jones industrial average fell 59.21 points, or 0.35 per cent, to 16,965.



The S&P 500 lost 5.43 points, or 0.27 per cent, to 1,972.22.





The Nasdaq Composite dropped 9.12 points, or 0.2 per cent, to 4,442.42.Copyright @ Thomson Reuters 2014