SAN FRANCISCO (CBS SF) — The fog and cold stayed away, but Silicon Valley’s tech community was out in full force at Sunday’s 2014 Gay Pride Parade in San Francisco.

What was once a gay protest has evolved into a huge commercial parade, with dozens of local Bay Area tech companies participating en masse.

Thousands of employees from Silicon-Valley based companies like Apple, Facebook, and Google donned matching t-shirts and walked with signs, banners or rode on corporate floats.

MORE:

Pride 2014 Photo Gallery

Pride 2014 Full Coverage

An estimated 2000 employees turned out from Apple, and while they may or may not be gay, lesbian or transgender, they all share a connection to each other, their company and the LGBT community.

One Apple worker (who wished to remain anonymous) walked the parade route with his wife and 3 small kids. He told CBS5.com his father is gay. Then, he offered an observation typical of the technologically-oriented mind.

“This morning I calculated the probability – I have 3 kids, and if they each have 2 kids, there’s a very strong possibility that one of them will be gay,” he said. “So I’m here for them.”

Scott McNutt’s office sits high atop one of the hi-rise buildings along the parade route. His office has a perfect birdseye view of the parade.

“I’ve seen the parade evolve over the past 12 years,” said McNutt. “It used to be a protest march, now it’s really a commercial parade.”

He feels the participation of so many tech companies reflects the shift in the culture of San Francisco. “This very building was just purchased by Google,” said McNutt. “It would have been impossible to sell it, a few years ago.”

McNutt says he will have to move his office in a few months. Next year, Google employees will be on his balcony enjoying the parade. He will have to see it from the street, like everyone else.