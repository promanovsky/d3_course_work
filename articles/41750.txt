Walgreen Co., the nation’s largest drugstore chain, said it will close 76 unprofitable stores around the country in the second half of the fiscal year.

The Deerfield, Ill., company has 8,210 stores nationwide, or 138 more than a year ago. Of those, 638 stores are in California.

In the chain’s western region, which includes a dozen states, 14 stores are set to be shuttered. But Walgreens declined to specify which stores would close, saying that it would notify affected stores in the coming months.

“We won’t be confirming those locations until we’ve notified employees at the stores first,” said spokesman Michael Polzin.

Advertisement

In a conference call with analysts, Chief Executive Greg Wasson said that most of the targeted locations exist near other Walgreens. Their employees will largely be reassigned, he said.

The shift is expected to boost operating income by up to $50 million annually, though Walgreens will need to take charges of up to $280 million through the end of the 2014 fiscal year.

“While we seize the opportunity for store growth as the population ages and consumers look to community pharmacy for their healthcare needs, we also continue to focus on optimizing our assets and organization to position Walgreens for our future as a global company,” Wasson said.

Shares were up 4.3%, or $2.78, to $67.09 a share in morning trading.

Advertisement

Also Tuesday, the company said its profit slipped to $754 million, or 78 cents a share, during the second quarter ended Feb. 28. In the year-earlier period, Walgreens earned $756 million, or 79 cents a share.

The chain said it faced “expected headwinds from slower generic drug introductions” as well as a less severe flu season and severe weather.

But Walgreens’ revenue hit a record high, soaring 5.1% to $19.6 billion. Sales at stores open at least a year rose 4.3% as customers’ average basket size increased 3.4% even as foot traffic slipped 1.4%.

The company said it filled 214 million prescriptions in the quarter – 2.2% more than a year earlier. Prescription sales made up 62.2% of Walgreens revenue.

Advertisement

Wasson noted in the conference call that Walgreens doesn’t plan to halt sales of tobacco products, even after its primary competitor CVS Caremark said last month that it would do so by Oct. 1.

ALSO:

Healthcare reform heats up drugstore battle

Latest Walgreens comes with sushi chef, barista and juice bar



Advertisement

CVS Caremark, No. 2 drugstore chain, will end all tobacco sales