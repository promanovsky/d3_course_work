NEW YORK (TheStreet) -- Shares of Toll Brothers Inc. (TOL) - Get Report are up 3.25% to $36.80 in pre-market trade after the home builder reported fiscal second-quarter profit that more than doubled, as the company continued to benefit from higher home prices and more deliveries, the Wall Street Journal reports.

The company reported a profit of $65.2 million, or 35 cents per share, up from $24.7 million, or 14 cents per share, last year.

Toll Brothers pretax income was $93.5 million, an increase from $41 million a year ago.

Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Revenue jumped 67% to $860.4 million.

Analysts polled by Thomson Reuters expected per-share earnings of 26 cents and revenue of $828 million.

TheStreet Ratings team rates TOLL BROTHERS INC as a Buy with a ratings score of B. TheStreet Ratings Team has this to say about their recommendation:

"We rate TOLL BROTHERS INC (TOL) a BUY. This is driven by several positive factors, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its robust revenue growth, increase in net income, growth in earnings per share, good cash flow from operations and largely solid financial position with reasonable debt levels by most measures. We feel these strengths outweigh the fact that the company has had lackluster performance in the stock itself."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

TOL's very impressive revenue growth greatly exceeded the industry average of 18.8%. Since the same quarter one year prior, revenues leaped by 52.8%. Growth in the company's revenue appears to have helped boost the earnings per share.

The net income growth from the same quarter one year ago has significantly exceeded that of the S&P 500 and the Household Durables industry. The net income increased by 928.4% when compared to the same quarter one year prior, rising from $4.43 million to $45.58 million.

Net operating cash flow has increased to -$250.39 million or 18.12% when compared to the same quarter last year. Despite an increase in cash flow, TOLL BROTHERS INC's cash flow growth rate is still lower than the industry average growth rate of 55.27%.

TOLL BROTHERS INC reported significant earnings per share improvement in the most recent quarter compared to the same quarter a year ago. This company has reported somewhat volatile earnings recently. But, we feel it is poised for EPS growth in the coming year. During the past fiscal year, TOLL BROTHERS INC reported lower earnings of $0.97 versus $2.79 in the prior year. This year, the market expects an improvement in earnings ($1.63 versus $0.97).

TOL's debt-to-equity ratio of 0.86 is somewhat low overall, but it is high when compared to the industry average, implying that the management of the debt levels should be evaluated further.

You can view the full analysis from the report here: TOL Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.



WATCH: More videos from Jim Cramer on TheStreet TV