TV personality Jenny McCarthy arrives at the 2013 Billboard Music Awards held at the MGM Hotel in Las Vegas, Nevada on May 19, 2013. UPI/Jim Ruymen | License Photo

Jenny McCarthy may have thought she was asking an innocent enough question when she took to Twitter on Thursday to chat with her fans, but was slammed in return by those who don't agree with her thoughts on vaccinations.

The View host has become well-known for several years for her public views on vaccines, specifically her claims that vaccines cause autism. McCarthy has blamed vaccines for causing her son Evan to develop autism.

Advertisement

But she wasn't specifically asking her Twitter followers about their views on the subject Thursday when she received the backlash.

"What is the most important personality trait you look for in a mate? Reply using #JennyAsks," she wrote on Twitter.

What is the most important personality trait you look for in a mate? Reply using #JennyAsks — Jenny McCarthy (@JennyMcCarthy) March 13, 2014

Her seemingly innocuous question inspired a barrage of responses criticizing the talk-show host for her thoughts on vaccines. One even blamed her way of thinking for the recent resurgence of the measles in New York City.

Loving the replies to #JennyAsks, measles are back in the nyc thanks to @JennyMcCarthy and every anti-vax internet expert/idiot like her. — Sheila Larkin (@SheilaSays) March 16, 2014