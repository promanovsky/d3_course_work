Separatist rebels in the eastern Ukrainian city of Donetsk have denied that they are responsible for shooting down a Malaysia Airlines passenger plane.

Separatist rebels in the eastern Ukrainian city of Donetsk have denied that they are responsible for shooting down a Malaysia Airlines passenger plane.

Andrei Purgin, deputy prime minister for the rebels, told The Associated Press the plane must have been shot down by Ukrainian government troops. He gave no proof for his statement, and Ukraine earlier denied shooting at any airborne targets.

A Ukrainian official said a passenger plane carrying 295 people was shot down Thursday over eastern Ukraine.

AP