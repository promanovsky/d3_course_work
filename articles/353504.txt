It’s a scandal! It’s a conspiracy! It’s economic warfare!

No, not the crimes the U.S. government is punishing the French bank BNP Paribas for perpetrating, but the punishment itself.

So say French politicians, at least.

On Monday, BNP Paribas pleaded guilty to violating U.S. economic sanctions against Sudan, Iran and Cuba. As part of the settlement, it agreed to pay nearly $9 billion and temporarily suspend some of its valuable dollar-clearing services. The magnitude of the penalties — the largest imposed on a financial institution for U.S. sanctions violations — reflect not only the astounding volume of forbidden transactions routed through the U.S. financial system but also the lengths the bank went to to hoodwink authorities. Internal documents revealed that BNP officials falsified records and obscured the countries involved, even as they privately recognized that some of the entities they served were party to a “humanitarian catastrophe” and directly supported terrorists.

In other words, BNP officials knew all along that they were behaving badly, from both a moral and a legal standpoint.

A logo for BNP Paribas SA sits on a window of a bank branch,. (Kosuke Okahara/Bloomberg)

But no matter. The real problem, French politicians have asserted for the past few months, is that the United States shouldn’t be sticking its nose into BNP’s business.

Last month, French President Francois Hollande confronted President Obama over “l’affaire BNP” at a D-Day event, insisting that Obama intervene to ease the proposed fine. Hollande also took up the cause with German Chancellor Angela Merkel and other European governments, according to the Financial Times. U.S. regulators, after all, are still investigating other foreign banks — including, reportedly, Germany’s Deutsche Bank and Commerzbank, Italy’s UniCredit and France’s Credit Agricole and Societe Generale – that may have also violated our trade embargoes. The penalties in those cases could be similarly onerous.

The United States is engaging in a cash grab from helpless foreigners, French politicians have intimated, and is attempting to hobble U.S. banks’ competitors. Large fines “sometimes offer some advantages in economic warfare,” French economic minister Arnaud Montebourg said recently, adding ominously, “Maybe we should imitate them.”

Never mind that U.S. regulators recently slapped JPMorgan Chase with an even higher penalty of $13 billion over its mortgage business or that Bank of America is anticipating similarly scaled punishment. Extraterritorial bullies that we Americans are, we’ve resorted to stealing France’s lunch money just because we can.

Of course, such bristling at the United States’ attempts to curb international companies’ transactions with terrorists, drug cartels and corrupt politicians is nothing new.

“You [expletive] Americans,” an executive at London-based Standard Chartered Bank wrote in a leaked e-mail from 2006. “Who are you to tell us, the rest of the world, that we’re not going to deal with Iranians.”

Indeed, who are we to exert such control over foreigners’ behavior?

We are no longer just the world’s policeman. Thanks to our status as the largest market in the world and the guarantor of the world’s reserve currency, the United States has become the world’s economic conscience, too. And in my view, that is a very good thing as long as we brandish our economic superpower status responsibly.

Since the days of our earliest English-speaking settlers, America has tried to be a “city upon a hill.” Believing that the eyes of all people were upon us, we tried to lead by example. Of course, our success at persuading others to embrace our self-proclaimed values of equality and freedom, like our own commitment to them, has been somewhat mixed. And when they would not see the light, we at times sought to impose our ideals (and interests) upon others by force, bludgeoning them into adopting the one true path toward democracy, freedom and respect for human life.

Today, however, 238 years to the day after the launch of the American experiment, we have a far superior, and more peaceful, weapon: economic clout. Right now, if not necessarily forever, we live in a world in which we can use our economic superpowers to pressure others to behave more honorably. We can tell the world: If you want to do business with us, or even just transact with our currency, you can’t also be the de facto central bank for the government of Sudan. That’s true even if your home country is uninterested in banning such profiteering.

Trade sanctions (and laws such as the Foreign Corrupt Practices Act) are exactly the kinds of policies we should be pursuing to make the world a better place through our economic influence — instead of, to the maximum extent possible, our military influence. Lord knows that if other rising superpowers ever develop the same economic clout we have, they might be somewhat less conscientious about wielding it for the betterment of mankind.