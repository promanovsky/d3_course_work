From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Updated: 9:58 a.m., 10:28 a.m. Pacific

Google has just restored service.

Google Docs and Gchat were both working intermittently, if not completely down, for at least two hours today.

Google’s own App Status Dashboard has only just begun to show a status issue for Google Talk, but I’ve been experiencing downtime on Google Docs for over two hours.

As of almost 10 a.m. Pacific, Google is now showing an issue on its Apps Status page. Apparently the outage is primarily affecting Sheets, not all of Google Docs. Google also is showing a partial outage, not a full and complete failure:

Meanwhile — predictably — Twitter is blowing up about the outage, with one person showcasing the result on his productivity by linking to an animated GIF prominently featuring the word “nothing.”

Guess what I’m doing at work now that #googledocs is down? http://t.co/0BzXZMmy3y — bryan Steel is Dead (@bryansteel) March 17, 2014

Relying on the cloud for productivity solutions does have its dangers. A Google Docs outage may not be the end of the world, but some are claiming fairly serious impacts — at least with their tongues firmly embedded in their cheeks.

US GDP down 2% and millions of desks flipped over due to Google docs outage — Alexander Variano (@VarianoA) March 17, 2014

VentureBeat has asked Google for an update on the status of those two apps. We’ll update this post when we hear from Google. The last tweet on the Google Drive account on Twitter is an ominous one, in retrospect:

Today we’re lowering the price of all our monthly #GoogleDrive storage plans: http://t.co/v0UDtvn8iU pic.twitter.com/JJjk9Io7Uc — Google Drive (@googledrive) March 13, 2014

Perhaps it’s all that new capacity that’s causing the problem?