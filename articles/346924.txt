Paul Mazursky has died, aged 84.

The director and screenwriter passed away of pulmonary cardiac arrest on Monday, June 30, according to family spokeswoman Nancy Willen.

JC Olivera/WireImage



Mazursky was well known for his sometimes controversial movie topics in the '60s and '70s, and penned and directed films such as Bob & Carol & Ted & Alice, Harry and Tonto and An Unmarried Woman.

Over his long career, Mazursky directed six actors in Oscar-nominated performances, including Anjelica Huston in Enemies: A Love Story and Art Carney in Harry and Tonto.

He once told the Chicago Tribune: "I seem to have a natural bent toward humour and I seem to make people laugh, but I think there is in me a duality.

"I like to make people cry alsoâ€¦ I like to deal with relationships. The perfect picture for me does all that."

The filmmaker also received a star on the Hollywood Walk of Fame in 2013.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io