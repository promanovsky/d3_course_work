Last week word broke that Apple might acquire Beats for $3.2 billion. For those who don’t know its a company that makes high-end audio products including headphones and only recently launched an on-demand music streaming service as well. The acquisition was seemingly confirmed by Dr. Dre as well, a popular icon in the music industry who owns a sizeable stake in the company, though Apple hasn’t confirmed it as yet. A new report suggests that Apple might keep Beats alive as an independent brand after the acquisition.

Advertising

Apparently one of the major reasons why Apple became interested in the company is Beats Music. Bloomberg reports that Apple executives were impressed with the on-demand music streaming service that is “rapidly converting users into paying subscribers.” For $10 per month Beats Music subscribers can access more than 20 million songs and take advantage of a powerful recommendation algorithm.

Only recently it was rumored that Apple is interested in developing its own on-demand music streaming service. This acquisition will deliver Apple a ready made service that already has all the contracts and licensing agreements with record labels in place. Beats Music is also multi-platform, being available on Android and Windows Phone apart from iOS. While Apple probably wouldn’t have branched out an iTunes music streaming service this way, keeping Beats as an independent brand would mean not losing subscribers on rival platforms.

The report also claims that since Beats high-end audio products business is profitable Apple might keep it around as well. A source claims that the company will work with beats to improve quality of design in future products.

Filed in . Read more about Beats and Beats Music.