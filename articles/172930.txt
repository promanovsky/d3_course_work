Kendall Jenner flashes her toned midriff in crop top amid rumours she is shooting Calvin Klein ad with Justin Bieber



She is a rising star in the modeling world.

And Kendall Jenner certainly has the figure to make it.



The 18-year-old flashed her toned midriff in a crop top while out in New York on Monday, amid rumours she and Justin Bieber are shooting a Calvin Klein advert together.

Model good looks: Kendall Jenner displayed her slender physique in skintight jeans in New York on Monday



The reality star paired the top with skintight black jeans, a scarf, leather biker jacket and black ankle boots.

She accessorised with a large leather handbag and shielded her eyes with aviator shades.

The beauty wore her brown hair straight and loose and kept her make-up natural, opting for a slick of pink lipgloss.

She's got style: The reality star paired the trousers with a white crop top, scarf, leather biker jacket and black ankle boots

Breathtaking: The beauty wore her brown hair straight and loose and kept her make-up natural, opting for a slick of pink lipgloss

Perfectly put-together: She accessorised with a large leather handbag and shielded her eyes with aviator shades

Kendall stepped out amid rumours she and Justin are collaborating on a Calvin Klein ad together.

The pair are both in New York at the moment, and both posted Instagram shots recently showing them wearing Calvin Klein underwear.

But a source told E! News that the rumours are false, while a spokesperson for the designer told the website, 'as a policy we don't comment on rumours.'

Poster child: In March, Kendall posted a snap of herself wearing a white Calvin Klein bra and matching underwear peeking out from baggy jeans

In March, Kendall posted a snap of herself wearing a white Calvin Klein bra and matching underwear peeking out from baggy jeans.

She wore her brown hair loose and straight and laughed happily for the camera.

'morning! just going to hang out in #MyCalvins alllll day! ... thank you @calvinklein for my little gifts,' she captioned the picture.



Around ten days later, Justin also posed in a pair of Calvin Klein underwear in two Instagram shots.



Model behaviour: Justin Bieber showed off his underwear modelling skills on Instagram on Tuesday

Showing off his new tattoos - which have now spread to his right arm and chest - the star stopped short of stripping down to his black briefs, instead wearing a black and white shirt left open and a pair of low slung tracksuit pants.

Clearly the Canadian has been putting in the hours at the gym, as Justin sported a very impressive four-pack.

When the star first posted the image he captioned one of the images, 'What if I do a Calvin Klein campaign?? Comment below yes or no'.

Underwear democracy: The 20-year-old asked fans what they thought about him becoming a Calvin Klein model but later deleted the caption

However, just an hour later, the Beauty And The Beat singer had a change of heart and kept the picture up but deleted the caption.

Proving he has the pose down, in the second image Justin threw his hands behind his head and crunched forward as if he was doing a magazine campaign.

Besides his abs, one of his newer tattoos, a giant cross, was also hard to miss in the image.

The rumours are sure to further fuel the fallout between Kendall and Justin's on-off girlfriend Selena Gomez, who allegedly 'unfollowed' Kendall and her sister Kylie after they 'spent time' with Justin.



Trouble in paradise: Justin's on-off girlfriend Selena Gomez has allegedly unfollowed Kendall and her sister Kylie on Instagram after the spent time with Justin, pictured last week in LA



The 21-year-old former Disney star also reportedly believes the half-sisters of Kim Kardashian party too hard.