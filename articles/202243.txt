Kim Kardashian, a reality TV star, has vowed to fight racism so that her baby daughter North can grow up in a world free of prejudice.

The 'Keeping Up With The Kardashians' star, who has 11-month-old daughter North with her fiance Kanye West, turned her hand to political activism after personally experiencing acts of discrimination, reported Contactmusic.

"To be honest, before I had North, I never really gave racism or discrimination a lot of thought. It is obviously a topic that Kanye is passionate about, but I guess it was easier for me to believe that it was someone else's battle.

"But recently, I've read and personally experienced some incidents that have sickened me and made me take notice. I realise that racism and discrimination are still alive, and just as hateful and deadly as they ever have been," she posted on her blog.

The 33-year-old beauty is determined to fight all kinds of discrimination and make a difference so that her daughter can grow up in a world free of prejudice.

"I feel a responsibility as a mother, a public figure, a human being, to do what I can to make sure that not only my child, but all children, don't have to grow up in a world where they are judged by the colour of their skin, or their gender, or their sexual orientation. I want my daughter growing up in a world where love for one another is the most important thing."