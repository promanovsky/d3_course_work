New Delhi: Prime Minister Narendra Modi today pitched for raising awareness about the importance of blood donation and asked his "young friends " to take lead in this.

In his tweets on the World Blood Donor Day, he said blood donation was a great service to society and congratulated those who donate blood.

"On World Blood Donor Day I congratulate every one who keeps donating blood. It is a great service to society.

"Today we reaffirm our pledge to raise greater awareness about the importance of blood donation. My young friends should take a lead in this," Modi said.