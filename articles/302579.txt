During periods of low volatility, the best way to find some good forex action is to find a currency pair where the opposing economies are going in different directions. For the majors, this type of gambit puts focus on pound sterling and then on whatever rival is sulking away to lick its wounds. Europeans may be tired of licking or taking a licking, but the fact remains that they are dragging near the end of developed economies in the recovery race. Any doubt that was remaining was quelled last week after central bankers for both arenas deliberated the future.



Mario Draghi and his buddies at the ECB were quick to exit stage left, having caved to market pressure to implement a plan to combat deflation and give new life to bank lending performance, especially to small and medium-sized business establishments. The BoE, however, had the luxury of stalling a market that longed to see the UK as the first major power in the northern hemisphere to raise interest rates. Like a poker player with a winning hand, they sat pat, electing to bluff for another round.



Board governors, however, are not a tight-lipped crew. Many schedule speaking engagements, if only to make their opinions known, or to beat their chests with pride, as Mark Carney, the Governor of the Bank of England, did at a recent bankers banquet. Looking a bit like the Cheshire cat that ate the canary, Carney waxed eloquently about the UK's stable recovery, but gave nary a hint as to when interest rates might be raised. But central bankers get their kicks by jostling markets out of boredom, just the opposite of their stated missions no less, as can be seen in the following chart:

EUR/GBP Daily

It almost seemed as if Carney had one eye on his watch and the other on his speech, because he waited until U.S. markets closed to deliver his “piece de resistance” to the attentive reporters and bankers in attendance: “There's already great speculation about the exact timing of the first rate hike and this decision is becoming more balanced. It could happen sooner than markets currently expect.”



You could almost hear the cannon’s roar from over the British Channel in the eurozone. The general expectation had been that the UK and the U.S. would both begin raising rates in 2015, but the supposition has always been that the UK was ahead on all fronts. As the U.S. recovery has gradually grabbed the headlines, most savvy traders expected Carney to move in 2014, but the man would just not come out and say it.



But perception is sometimes reality. The pound has beaten down the and now pulverized the . The GBP/USD pair is barking at the gates of resistance that have not been penetrated since August of 2009. The EUR/GBP, however, still has room to fall, another 200 to 300 pips, if historical resistance levels are the benchmark. If you want more bang for your forex buck, you may want to lean heavy on the EUR/GBP.



But, whoa! Not just yet. The downward channel in the above chart suggests that the market may have overreacted to Carney’s remarks. After the wine and after-dinner cognacs have worn off, there may be a “dead-cat bounce” in the offing. England’s economy has established an admirable recovery, but not all of the holes are buttoned up. Economists will tell you that consumers have primarily driven the recovery, but exports are declining due to a strong pound, lending remains tepid, and disposable income has not budged.



Those in the know believe that sustainability is not yet certain. $1.7044 may be a tough nut to crack.