The industry is stepping up after the group of developers who volunteer to maintain OpenSSL revealed that they received donations averaging about $US2000 a year to support the project, whose code is used to secure two-thirds of the world's websites and is incorporated into products from many of the world's most profitable technology companies.

"I think we get complacent as an industry when we see something as working well or working 'well enough'. We sort of see it as a 'maintenance job,'" said Chris DiBona, director of open source and engineering with Google. "We have to be a bit more vigilant."

The Heartbleed bug has likely cost businesses tens of millions of dollars in lost productivity as they have had to update systems with safe versions of OpenSSL, according to security experts. It has already resulted in at least one major cyber attack: the theft of data from Canada's tax authority.

The non-profit Linux Foundation, which promotes development of the open source Linux operating system, organised the group, whose formation it announced on Thursday in the US.

It will support development of OpenSSL as well as other pieces of open source software that make up critical parts of the world's technology infrastructure, but whose programmers do not necessarily have adequate funding to support their work, said Jim Zemlin, executive director of the Linux Foundation.