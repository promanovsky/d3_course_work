BRUSSELS--The European Union is levying sanctions against Valery Gerasimov, chief of staff of Russia's Armed Forces, as part of an escalating response to the country's threats and incursions against Ukraine.

Mr. Gerasimov is one of 15 individuals the EU announced Tuesday it was adding to a list of Russian leaders and pro-Russian Ukrainian figures facing punitive measures for helping violate Ukraine's territorial integrity. This brings to 48 the number of individuals facing EU travel bans and asset freezes.

Mr. Gerasimov, also Russia's first deputy minister of defense, is the most prominent of the new names, and a figure who has significant dealings with the West. Military leaders of the North Atlantic Treaty Organization have spoken with him regularly.

Other top Kremlin figures included in the new sanctions are Russia's Deputy Prime Minister Dmitry Kozak; Ludmila Shetsova, deputy chairman of the Russian Duma, or lower house of parliament; and Igor Sergun, who heads Moscow's Main Intelligence Directorate.

The list also includes figures from Eastern Ukraine who are promoting the pro-Russian cause there, such as Andriy Purgin, head of the Donetsk Republic, a self-proclaimed entity that is not internationally recognized.

European leaders selected the new names Monday, but they became public Tuesday morning when officially published in the EU's legal journal. The U.S. imposed its own new sanctions Monday, focusing in significant part on private Russian companies, an approach the EU has so far been reluctant to take.

The new EU and U.S. sanctions reflect Western leaders' conclusion than a Russia-Ukraine agreement reached in Geneva on April 17 has failed. American and European leaders say Russia has completely ignored the agreement, which calls for measures such as disarming militias, vacating occupied buildings, and awarding amnesty to protesters.

The path ahead remains unclear. Russian troops are massed on Ukraine's eastern border and leaders in Kiev say Russian special forces are operating in the country's eastern region. Ukraine's presidential elections are scheduled for May 25, an event that seems certain to spur action of some kind.

Meanwhile, conditions in eastern Ukraine risk descending into lawlessness. Kharkiv Mayor Henadiy Kernes was shot and critically wounded Monday and military observers from the Organization for Security and Cooperation in Europe have been kidnapped.

"The downward spiral of violence and intimidation undermines the normal functioning of the legitimate state institutions," Catherine Ashton, the EU's foreign policy chief, said Tuesday. "A number of people have been killed, wounded, tortured or kidnapped in the last few days."

U.S. and European leaders are contemplating whether to impose a broader sanctions against Russia that would target sectors of the country's economy, such as banking, energy or defense.

EU foreign ministers are scheduled to meet May 12, though that meeting could be moved up to next week, with Ukraine likely to dominate the session.

U.S. Secretary of State John Kerry told The Wall Street Journal Monday that broader sanctions could be in the works shortly. "We're inches away from that now," Mr. Kerry said. "And if they continue on this path, that's where it's heading."

Write to Naftali Bendavid at naftali.bendavid@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires