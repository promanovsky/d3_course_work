For those who believe in the power of chocolate comes the start of a new study. Researchers will look into whether certain ingredients in dark chocolate can help prevent heart attacks and strokes.

But before you start digging into your stash of chocolate, know that researchers won’t be handing out thousands of candy bars to participants.

The study, which will include 18,000 men and women, will focus on bio-active nutrients found in the cocoa bean, without all the extra ingredients such as sugar, found in chocolate candies.

Testers will be given dark chocolate pills that contain 750 milligrams of cocoa flavanols, naturally occurring plant-based nutrients found in chocolate. And unfortunately for them, the pills won’t actually taste like candy.

Advertisement

According to Dr. JoAnn Manson, one of the study’s lead researchers, in previous studies, cocoa flavanols have been shown to reduce blood pressure and improve cholesterol levels.

Participants at the Brigham and Women’s Hospital in Boston and the Fred Hutchinson Cancer Research in Seattle will be given two capsules a day of the cocoa flavanols or dummy pills for four years.

The study is sponsored by Mars Inc., the company behind M&M’s and Milkyway bars and the National Heart, Lung and Blood Institute. Mars Inc. has been researching cocoa flavanols for the past 20 years and already sells CocoaVia cocoa extract capsules containing 250 mg of flavanols.

The company claims that to get the same amount of CocoaVia flavanols, you’d have to eat one and a half bars of dark chocolate containing 300 calories, 22 grams of fat and 24 grams of sugar.

Advertisement

Love chocolate? Follow me on Twitter: @Jenn_Harris_

ALSO:

Quizno’s files for bankruptcy, but you can still get a toasted sub

Classic chili cheese dogs and corn dogs at Motordogs in Pasadena

Advertisement

Chef of the Moment: Paris’ Alain Passard comes to L.A. for All-Star Chef Classic

