Last year T-Mobile made its first “uncarrier” announcement. It shunned traditional two year service contracts in favour of an equipment installment plan which debuted alongside its new Simple Choice plans. The carrier then made more uncarrier moves in the coming months by introducing an early upgrade program and even offering free 2G data roaming. Its expected to make the uncarrier 5.0 announcement soon and according to a rumor it might bring simplified rate plans that include all taxes.

Advertising

If you’re a postpaid subscriber in the U.S. you’ll surely know about the addition of taxes and fees that swell the bottom line. For example in the state of California subscribers have to pay 38 cents in City Utility Users Tax, 2 cents in the Relay Service Device Fund and 8 cents in the State 911 Tax apart from quite a few other fees and charges. This adds up if you have multiple lines since these are calculated on a per-line basis.

Tax inclusive rate structures are typically offered to prepaid customers. MetroPCS, which is now under T-Mobile’s wing, structures its branded rates in the same way. Folks at PhoneArena hear that T-Mobile is going to replicate that to some extent by offering postpaid customers rate plans that include all regulatory fees and taxes. It is claimed that the prices of the rates won’t be increased.

If this is true, and its what T-Mobile announces at its uncarrier event this coming Wednesday, not only will the carrier get a leg up on its rivals but it will also significantly disrupt the conventional telecom billing practice particularly for postpaid subscribers.

Filed in . Read more about T-Mobile.