The Iranian dancers who starred in the viral "Happy in Tehran" video that led to their arrest have been freed, a human rights group said Wednesday. But according to the International Campaign for Human Rights in Iran, Sassan Soleimani, the director of the video — a remake of Pharrell Williams' hit song "Happy" — remains in custody.

"Hi I'm back," one of the six dancers, Reihane Taravati, announced on her Instagram account. "Thank you @pharrell and everyone who cared about us."

The release of the dancers comes after a day a social media campaign was launched urging Iranian police to free them.

Williams said he was saddened by the news that Iranian police arrested six people, after their "Happy in Tehran" video remake of his ubiquitous hit "Happy" went viral.

"It's beyond sad these kids were arrested for trying to spread happiness," he wrote on Twitter and Facebook on Tuesday.

Their video — showing men and women without veils dancing in the streets of Tehran — had amassed more than 165,000 views on YouTube, attracting the attention of police.







The video was later pulled, and its stars were forced to repent on Iranian state television.

Following their public humiliation, Tehran Police Chief Hossein Sajedinia called the video a "vulgar" clip that "hurt public chastity,” and warned Iranian youth not to follow their lead.

"The detained [dancers] were not abused during their detention," ICHRI said in a press release, "although their personal belongings, including personal computers and cell phones, were confiscated."

The arrests came just days after Iranian President Hassan Rouhani denounced censorship, saying the country should embrace the Internet.





#Cyberspace should be seen as opportunity: facilitating two-way communication, increasing efficiency & creating jobs. pic.twitter.com/iYkzgHpXkQ — Hassan Rouhani (@HassanRouhani) May 17, 2014



"#Cyberspace should be seen as opportunity," Rouhani tweeted on May 17, "facilitating two-way communication, increasing efficiency & creating jobs."

Story continues

"We must recognize our citizens’ right to connect to the World Wide Web,” Rouhani said, according to the state-run IRNA news agency. “Why are we so shaky? Why have we cowered in a corner, grabbing onto a shield and a wooden sword, lest we take a bullet in this culture war?”

Not surprisingly, the arrests have angered Twitter users, who are expressing support for the filmmakers using the hashtag #freehappyiranians.





The Iranian people cannot be forced to live in a world where enrichment is a right, but happiness is not. #freehappyiranians @rezaaslan @ase — Trita Parsi (@tparsi) May 21, 2014



The "Happy" episode is not the only recent cultural contradiction coming out of Tehran. Iranian actress Leila Hatami angered officials this week after she was photographed kissing the 83-year-old president of the Cannes Film Festival, Gilles Jacob, on the cheek.

"Those who attend intentional events should take heed of the credibility and chastity of Iranians, so that a bad image of Iranian women will not be demonstrated to the world," Deputy Culture Minister Hossein Noushabadi said of the photograph, according to the Daily Mail. "Iranian woman is the symbol of chastity and innocence."

But perhaps the tide in Tehran is turning. On Wednesday, Rouhani published a tweet that appears to agree both with Pharrell and the song's message.



