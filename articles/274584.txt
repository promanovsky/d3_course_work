Channing Tatum and Mila Kunis' sci-fi flick "Jupiter Ascending" has been pushed back to 2015.

The delay comes a month before the film was set to hit theaters on July 18. Warner Bros. Domestic Distribution Chief Dan Fellman told Variety the film needed more time to work on their special effects.

"With the July release date, they were just not going to make it on time," Fellman said. "A lot of the issue for us was getting it ready for the international release, since the foreign territories need additional time."

Kunis and Tatum recently spoke about making the film in the June issue of GQ magazine. Kunis explained the physicality of the film was difficult for both of them.

"It was one of the hardest things I've ever done and I know some of the hardest things that I think he's probably ever done," Kunis told GQ. "Just the pure exhaustion on the physical level. I think you know knowing that we were both in the same boat together makes it easy for us, but you never really know how it's gonna be five months in, and it was such a great relief to have somebody who had the same outlook."

Tatum admitted he was unsure what he was getting into when he signed on for the role, claiming the physicality almost "killed him."

"I had no idea what I was going into with Jupiter, zero idea," Tatum said. "I had no clue what it was going to mean to make a movie like that. And it almost killed me. I think were behind the eight-ball from the very beginning. Those movies, I like to call them like pseudo-controlled avalanches."

"Jupiter Ascending" will be released to theaters on Feb. 6, 2015. You can check out the trailer for the film in the video below.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.