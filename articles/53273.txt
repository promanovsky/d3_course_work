Like any world unto itself, Hollywood has its own lexicon. But Gwyneth Paltrow's use of the term "conscious uncoupling" to describe her breakup with Chris Martin this week had even Hollywood veterans scratching their heads and reaching — metaphorically at least — for a dictionary.

Of course, there was snark, too. And, on the other hand, some real appreciation for the message and the way it was delivered — in a joint post from the actress and her rock-star husband on Paltrow's lifestyle website, goop. (Which — surprise! — crashed from the traffic.)

But before we get to that, let's start with the basics: What the heck does "conscious uncoupling" mean?

"I've never heard it, but it sounds like a phrase used by marriage therapists in Malibu," quipped Janice Min, editor of The Hollywood Reporter.

Pretty close, actually. The term was coined by a Los Angeles therapist and author, Katherine Woodward Thomas, who has created a five-step "Conscious Uncoupling" online process — to "release the trauma of a breakup, reclaim your power and reinvent your life."

Speaking by telephone Wednesday from Costa Rica, where she traveled to write her second book — called, not surprisingly, "Conscious Uncoupling" — Thomas explained that her goal was "to create a map for a couple to consciously complete a relationship — to have an honorable ending."

Thomas said that the assumption that people will have only one lifetime partner — and that anything else is a failure — comes from a time long ago when the lifespan was much shorter.

"I'm a fan of marriage, but I recognize that most people in their lives will have two to three longtime relationships — which means one to two breakups. And so we need to learn how to do this better," she said.

Thomas said she doesn't know Paltrow, but applauded what she called her and Martin's courage in the way they announced their breakup. "They're modeling this for the world," she said.

Not surprisingly, though, the reference evoked some snark, in Hollywood and across the pond in Britain, where the couple is also based.

"What deluded tosh," headlined a column in The Guardian, using slang for rubbish, or nonsense. (Tosh perhaps, but the phrase actually made it to the House of Lords, Britain's upper chamber of Parliament, where a Labour Party lawmaker referred to a political disagreement over university fees on Wednesday as "yet another example of the coalition's conscious uncoupling.")

Others, though, were touched by the message — while noting how expertly it was managed from a public relations standpoint, with the news released late on a Tuesday, after the celebrity weeklies had all closed their issues.

"It was very smart," said Min, who is also former editor of US Weekly. "By next week, there will be other news, and they probably won't be on the cover at all." And the fact that the couple made the statement on Paltrow's website gave them, of course, message control.

It is with hearts full of sadness that we have decided to separate. We have been working hard for well over a year, some of it together, some of it separated, to see what might have been possible between us, and we have come to the conclusion that while we love each other very much we will remain separate. We are, however, and always will be a family, and in many ways we are closer than we have ever been. We are parents first and foremost, to two incredibly wonderful children and we ask for their and our space and privacy to be respected at this difficult time. We have always conducted our relationship privately, and we hope that as we consciously uncouple and coparent, we will be able to continue in the same manner.

Love,

Gwyneth & Chris

On the other hand, Min said, "I was touched — it really felt sincere. And it gave us more information than you normally get in these situations — revealing they'd been separated for a while. There was a sincerity here that you rarely see."

Also, Paltrow and Martin, the Coldplay frontman, have two children — Apple, 9, and Moses, 7 — so they have a strong reason to control the message. "No child wants to see news of their parents' breakup on the supermarket shelf," Min noted. "It's clear they love their children."

Longtime Hollywood public relations expert Howard Bragman agreed, applauding the couple for their honesty and civility.

"Listen, I've been involved in a LOT of Hollywood divorces, and I have to say, this is refreshing," said Bragman, who is vice chairman of reputation.com. "You can roll your eyes at the purportedly New Age language, but the broader message is, 'We're gonna do this together.' I give them a lot of credit."

So does Jen Singer, New Jersey mother and mom blogger. Singer found the message especially meaningful because she, herself, went through a divorce last year, and sought her own version of "conscious uncoupling."

"I called it my 'unengagement' from marriage — the process of preparing for life on my own," Singer said.

"Sure, Gwyneth has a reputation of being a little bit woo-woo, so we kind of giggle at the phraseology," she said. "But the concept is solid."

Singer added that she and her husband looked for a marriage counselor that would help them, well, uncouple. "But nobody would do it if we weren't willing to commit to trying to fix the marriage," she said. "But we were beyond that. We wanted help in carefully unraveling the husband-wife part of the family while supporting the parents-and-children part. We had to wing it instead."

What will this do for Paltrow's image? Min says she doesn't think that image will change one way or the other — for either avid fans or detractors.

"Maybe if you don't like her, you'll say, 'Even her breakup has to be precious,'" she said. "But if you do like her, you'll say, "Bravo. Well thought out."

___

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Associated Press writer Jill Lawless in London contributed to this report.

Copyright 2014 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.