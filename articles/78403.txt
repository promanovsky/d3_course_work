Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Cameron Diaz has just shown the entire world of Hollywood how to really advertise a film.

The blonde bombshell stars alongside Bad Teacher co-star Jason Segel in Sex Tape, and they play a married couple who take a raunchy tape to spice up their sex life and then accidentally send it to all their friends.

Of course this can only mean a whole lot of nakedness.

Wearing tiny pink knickers, a white crop top and rollerskates, the star shows off her unbelievably toned figure as she tries to seduce her hubby .

(It didn't take a lot of trying).

The pair discuss deleting the evidence the next day, but by the time they wake up, Jason has backed it up to iCloud and all his friends can see it.

Awkward.

We also get a glimpse of the pair in the shower in the red-band trailer - and even Jason looks like he's got into shape for the film.

In fact, we see more of their bare bodies than we do of them in clothes.

The 41-year-old has certainly proved age is just a number in the comedy, which airs in cinemas this summer.

Who knew rollerstakes could look so good?