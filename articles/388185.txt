A man displays an Apple's iPhone 5 smartphone on February 25, 2014 in the French northern city of Lille. Apple said on February 24, 2014 it was developing a security fix for its Mac OS X computer operating system after a patch released for its iPhones and iPads to thwart hacker attacks.AFP PHOTO PHILIPPE HUGUEN (Photo credit should read PHILIPPE HUGUEN/AFP/Getty Images)

SAN FRANCISCO (CBSMiami/AP) — Refunds are coming to Apple users ensnared in a plot to raise the prices of digital books.

The company will refund up to $400 million to consumers unless the company gets a court to overturn a decision affirming its pivotal role in the collusion.

The settlement bill emerged in a Wednesday court filing made a month after attorneys suing Apple notified U.S. District Judge Denise Cote in New York that an agreement had been reached to avoid a trial over the issue.

Lawsuits filed on behalf of digital book buyers had originally been seeking damages of up to $840 million after Cote ruled in a separate trial last year that Apple Inc. had violated U.S. antitrust law by orchestrating a price-fixing scheme with five major publishers of electronic books.

Cote’s decision sided with the U.S. Justice Department’s contention that Apple’s late CEO, Steve Jobs, had schemed with major e-book publishers to charge higher prices in response to steep discounts offered by Amazon.com Inc. Jobs, who died in October 2011, negotiated the deals as Apple was preparing to release the first iPad in 2010.

Apple is appealing Cote’s decision from last year. The Cupertino, California, company won’t have to pay the $400 million settlement if it prevails. If the appeals court voids Cote’s verdict and returns the case to her for further review, Apple would still have to refund $50 million to consumers. No money will be owed if the appeals court concludes that Apple didn’t break any antitrust laws.

“Apple did not conspire to fix e-book pricing, and we will continue to fight those allegations on appeal,” the company said in a statement. “We did nothing wrong and we believe a fair assessment of the facts will show it.”

A decision on Apple’s appeal, now in the Second Circuit in New York, might not be issued for another year, according to Wednesday’s filing. Consumer attorneys in the case are still hoping to get Cote’s preliminary approval of the settlement.

If its appeal is rejected, it would be more of a blow to Apple’s image than its finances. The company can easily afford to refund the money, given it has about $150 billion in cash.

Millions of electronic book buyers will be eligible for refunds if Cote’s decision is upheld, though the precise number wasn’t spelled out Wednesday. More than 23 million consumers received notices of a $166 million settlement previously reached with the five book publishers found to be conspiring with Apple. Those publishers are Hachette, HarperCollins, Macmillian, Penguin and Simon & Schuster.

Consumers who bought e-books from those five publishers from April 1, 2010 through May 21, 2012 will be eligible for a slice of the settlement fund. To qualify, the consumers also must have lived in in one of the 19 states covered in a class action lawsuit or Guam, the U.S. Virgin Islands, American Samoa or the Northern Marina Islands at the time of the purchase.

Some of the settlement proceeds also will be distributed by the attorneys general in 33 states and territories involved in the case.

New York Attorney General Eric Schneiderman estimated e-book buyers in his state could receive up to $28 million in refunds from Apple. “This settlement proves that even the biggest, most powerful companies in the world must play by the same rules as everyone else,” he said.

Besides the consumer compensation, Apple also will pay $50 million in lawyer fees unless it wins its appeal. Of that amount, $20 million would be paid to the attorneys general to cover their costs and the remainder would go to class-action lawyers led by Steve W. Berman of the Seattle law firm Hagens Berman.

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)

RELATED CONTENT:

[display-posts category=”consumer” wrapper=”ul” posts_per_page=”5″]