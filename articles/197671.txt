Snapchat has agreed to settle federal charges that it deceived users when it promised that messages, photos and other data transmitted via the popular smartphone messaging app would disappear permanently, the Federal Trade Commission announced today.

“Touting the ephemeral nature of snaps, the term used to describe photo and video messages sent via the app, Snapchat marketed the app’s central feature as the user’s ability to send snaps that would “disappear forever’’ after the sender-designated time period expired,’’ the commission said in a statement.

However, officials said, recipients of snaps can save the messages in “several simple ways.’’

Advertisement

“Consumers can, for example, use third-party apps to log into the Snapchat service … to view and save snaps indefinitely,’’ the FTC statement said.

“Indeed, such third-party apps have been downloaded millions of times,’’ the statement said. “Despite a security researcher warning the company about this possibility … Snapchat continued to misrepresent that the sender controls how long a recipient can view a snap.’’

The federal complaint also alleged that Snapchat failed to secure its ‘Find Friends’ feature which led to a recent security breach in which attackers complied a database of 4.6 million Snapchat usernames and phone numbers.

“If a company markets privacy and security as key selling points in pitching its service to consumers, it is critical that it keep those promises,’’ FTC Chairwoman Edith Ramirez said in statement.

“Any company that makes misrepresentations to consumers about its privacy and security practices risks FTC action,’’ she added.

The settlement requires Snapchat to start a “comprehensive privacy program,’’ which will be monitored by an independent privacy professional for the next 20 years, federal officials said.

The deal also prohibits Snapchat from misrepresenting the privacy, security and confidentiality of users’ information, officials said.

The commission’s complaint also alleged that the app stored video snaps in a way that left them vulnerable to being accessed by recipients if they connected their device to a computer to search through its file directory.

Advertisement

Snapchat was also accused of deceiving users when it told them they would be notified if a recipient took a screenshot of their snap.

“In fact, any recipient with an Apple device that has an operating system pre-dating iOS 7 can use a simple method to evade the app’s screenshot detection, and the app will not notify the sender,’’ the FTC said.

The commission also charged that Snapchat transmitted geolocation data from users on Android devices, a contradiction to its privacy policy which said the company neither tracked nor accessed such information.

And, the commission said that Snapchat collected iOS users’ entire contact lists and address books without notice or consent and that it did not do enough to verify that users who signed up were in fact who they claimed to be — a shortfall that led some users to unknowingly send snaps to complete strangers.

The commission said the case is part of an international crackdown on mobile app privacy by members of the Global Privacy Enforcement Network, which it described as “a cross-border coalition of privacy enforcement authorities.’’

“The settlement with Snapchat is part of the FTC’s ongoing effort to ensure that companies market their apps truthfully and keep their privacy promises to consumers,’’ the commission’s statement said.

Meanwhile, Snapchat rolled out new features for its app last week that have drawn some criticism because some of the additions do not automatically disappear.