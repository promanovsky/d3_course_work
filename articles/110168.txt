The SpaceX International Space Station resupply mission will not be delayed due to an unresponsive computer. The Dragon spacecraft will carry 3,347 pounds of cargo to the ISS as part of the Commercial Resupply (CRS-3) mission.

SpaceX's third ISS resupply mission has been delayed twice, March 14 and March 30, and on Sunday, NASA officials determined the launch will happen despite an unresponsive backup computer that controls some of the robotic systems aboard the space station. The Multiplexer-Demultiplexer (MDM) is used to control some of the robotics used in the docking procedure that attaches the Dragon spacecraft to the ISS and the main computer is functioning normally. NASA is planning a future spacewalk to replace the backup MDM although no date has been set. The Expedition 39 crew members are not affected by the computer outage.

Weather for Monday #Dragon launch is 70% “GO” – liftoff @ 4:58pm ET. Watch the launch live @ http://t.co/NHREgPdLxw pic.twitter.com/97C9h37eXp — SpaceX (@SpaceX) April 12, 2014

NASA said in a statement on Saturday, "International Space Station Program officials and representatives of SpaceX decided Saturday to continue preparations for the launch of the Falcon 9 rocket and the Dragon cargo craft to the space station Monday from Launch Complex 40 at the Cape Canaveral Air Force Station, Fla., despite the failure Friday of a backup computer component that provides redundancy for commanding the Mobile Transporter rail car on the truss of the station."

The space station's robotic arm, Canadarm2, is attached to the Mobile Transporter system and will be used to grab the Dragon spacecraft and attach it to the Harmony node of the ISS. According to NASA, there are other backup systems that are not affected by the faulty MDM backup computer. The date for the spacewalk, and who will replace the MDM, will be announced on Sunday.

SpaceX had to delay the commercial resupply mission from March 16 to March 30 to "close out remaining open items before launch." A fire at the Eastern Range, which supports rocket launches, damaged radar components, notes NASASpaceFlight, was the cause for the most-recent delay in the Dragon spacecraft launch.

The Dragon spacecraft is carrying 3,347 pounds of supplies, hardware and scientific insturments. The science payload as part of the ISS cargo resupply mission includes the prototype laser communications system, Optical Payload for Lasercomm Science (OPALS); the the High Definition Earth Viewing (HDEV) project which includes four cameras that will be attached to the exterior of the ISS and provide live streams from space; and the the T-Cell Activation in Aging investigation to gain new insights into immune-system depression in microgravity.

One of the more interesting experiments being sent up to the ISS aboard the Dragon spacecraft is the Vegetable Production System (Veggie). To grow plants in space, the plants are packed in little "pillows," which provides several health benefits and could be a help to astronauts during deep-space exploration.

Photo: NASA/Gioia Massa

Gioia Massa, NASA payload scientist for Veggie, said in a statement, "Veggie will provide a new resource for U.S. astronauts and researchers as we begin to develop the capabilities of growing fresh produce and other large plants on the space station." The plant pillows are placed in a growth chamber which has contains red, blue and green LEDs. The astronauts will observe plant growth and water consumption as part of the Veggie experiment. In addition to the science benefits, taking care of plants provides a mental boost for the astronauts. "You could also think of plants as pets. The crew just likes to nurture them," said Massa.

The SpaceX dragon spacecraft launch is scheduled for 4:58 p.m. EDT on Monday.