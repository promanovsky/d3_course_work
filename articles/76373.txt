Many experts were skeptical that the benefits seen after a year would last. Now, three-year results show an even greater advantage for surgery.

Doctors on Monday gave longer results from a landmark study showing that stomach-reducing operations are better than medications for treating “diabesity,” the deadly duo of obesity and type 2 diabetes. Millions of Americans have it and can’t make enough insulin or use what they do make to process food.

WASHINGTON — New research is boosting hopes that weight-loss surgery can put some patients’ diabetes into remission for years and perhaps in some cases, for good.

Blood-sugar levels were normal in 38 percent and 25 percent of two groups given surgery, but in only 5 percent of those treated with medications.

Advertisement

The results are “quite remarkable” and could revolutionize care, said one independent expert, Dr. Robert Siegel, a cardiologist at Cedars-Sinai Medical Center in Los Angeles.

“No one dreamed, at least I didn’t,” that obesity surgery could have such broad effects long before it caused patients to lose weight, he said. Some patients were able to stop using insulin a few days after surgery.

At three years, “more than 90 percent of the surgical patients required no insulin,” and nearly half had needed it at the start of the study, said its leader, Dr. Philip Schauer of the Cleveland Clinic. In contrast, insulin use rose in the medication group, from 52 percent at the start to 55 percent at three years. The results were reported Monday at an American College of Cardiology conference in Washington. They also were published online by the New England Journal of Medicine.

Doctors are reluctant to call surgery a possible cure because they can’t guarantee diabetes won’t come back.

But some patients, like Heather Britton, have passed the five-year mark when some experts consider cure or prolonged remission a possibility. Before the study, she was taking drugs for diabetes, high blood pressure, and high cholesterol; she takes none now.

Advertisement

“It’s a miracle,” said Britton, 55, a computer programmer from suburban Cleveland.

“It saved my life. I have no doubt that I would have had serious complications from my diabetes” because the disease killed her mother and grandmothers at a young age, she said.