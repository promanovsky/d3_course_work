Caffeine is a drug millions of people turn to everyday to get that little push to get the day going but according to a recent study the impact it may be having on adolescent boys and girls is worth a closer look.

Researchers are discovering the drug is having different effects on kids depending on gender.

Researchers at the Department of Exercise and Nutrition Sciences, a part of University at Buffalo School of Public Health and Health Professions, are claiming boys and girls, post puberty, experience different blood pressure changes and heart rates after consuming various different caffeinated sodas and energy drinks.

"We found an interaction between gender and caffeine dose, with boys having a greater response to caffeine than girls," explained Jennifer Temple, an associate professor at University at Buffalo's School of Public Health and Health Professions. "In this study, we were looking exclusively into the physical results of caffeine ingestion."

"There was an interaction between gender and caffeine dose, with boys having a greater response to caffeine than girls. In addition, we found interactions between pubertal phase, gender, and caffeine dose, with gender differences present in postpubertal, but not in prepubertal, participants. Finally, we found differences in responses to caffeine across the menstrual cycle in postpubertal girls, with decreases in heart rate that were greater in the midfollicular phase and blood pressure increases that were greater in the midluteal phase of the menstrual cycle," states the report.

"There are lots of things we can't do because we're not old enough or mature enough. Caffeine should probably be added to that list," said Dr. Kevin Shannon, a professor of pediatric cardiology and director of pediatric arrhythmia at the Mattel Children's Hospital of the University of California, Los Angeles. He was commenting on the study, "Cardiovascular Responses to Caffeine by Gender and Pubertal Stage," which was published in the journal Pediatrics.

The University at Buffalo study examined the effects of low doses of caffeine in 52 children ages eight to nine and then 49 more children between the ages of 15 and 17. The results among the children on the younger group showed no difference between the sexes. However, caffeine's impact was felt more strongly by the boys in the older group. For all children, caffeine slowed heart rates and increased blood pressure.

Additional responses to the results of the study are pointing out concerns regarding the continued popularity of energy drinks among the younger demographic in the United States.

Jessica Lieb, a registered dietician at the Children's Hospital of Pittsburgh of the University of Pittsburgh Medical Center, aired concerns that children are turning to these drinks and bypassing other fluids that are more essential to their development.

"These caffeinated beverages are lacking in nutrients and a lot of them have added sugar," Lieb said. "Some of the coffee drinks blow soda out of the water with the amount of sugar added to them."

TAG caffeine, Children, Gender

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.