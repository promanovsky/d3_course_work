Scandal Season 3 Episode 15 Spoilers: The Grant Children Finally Appear (+Photos, Trailer)

Scandal season 3 episode 15 features the Grant children for the first time.

Jerry (Dylan Minnette, who played Jack’s son in “Lost”) and Karen(Madeline Caroll, who played Juli in “Flipped”) are preparing for a live sit down television interview with Fitz and Mellie at the White House on the ABC show.

The family dynamic is interesting, First Lady Bellamy Young told TVLine. “We had many happy years as a family, but they have been through a rocky road these last few years. You read ugly things about your parents in the paper and on the Internet, so yeah, we’ll meet them in a fraught moment. You can imagine teenagers in this situation and how they might react!”

“It so humanizes Fitz and Mellie to see them try and parent! Nothing can undo you like your own teenagers,” she added. Pictures from a family dinner show intensity.

(ABC)

Meanwhile, Adnan looks to Harrison for help, and Rowan warns Olvia to stop investigating B613.

The episode descriptions for the next two episodes–which lead up to the season finale–have been released.

Stop reading if you don’t want to see the hints into what happens in them.

April 3

Abby steps in as Olivia’s proxy and takes on duties at the White House. Meanwhile, the team continues to investigate B613, and someone throws a wrench in Reston’s presidential campaign.

April 10

A security breach calls for an all hands on deck situation at OPA. Fitz’s re-election campaign becomes stalled and he considers going against Olivia’s advice. Meanwhile, Adnan and Maya continue to plot their next move.