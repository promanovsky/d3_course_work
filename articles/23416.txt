Earlier this month, Miami police released a video of Justin Bieber trying to walk a straight line for his sobriety test the night he was arrested for DUI and drag racing.

As you can tell, the singer stumbles a bit while doing so:

But sources tell TMZ that Bieber has a reasonable excuse for this misstep... and it's not because he threw back a few too many.

The superstar allegedly told authorities that he fractured his foot skateboarding a few months earlier and that is the basis for his difficulty walking.

Seems like a bit of a stretch, but here's the thing:

Bieber only blew a .014 on his breathalyzer. That’s a minuscule number.

Moreover, his GPS make it look like he was NOT speeding when pulled over, meaning all charges against the artist are flimsy at best.

We doubt he does any jail time.

Still, like in his Miami deposition, the official record states Bieber was a total jerk when approached by the cops.

When the arresting officer said he wasn't driving around in a Lamborghini at the age of 19, Justin shot back: "Yeah, well, I bet you didn't have millions of dollars in your bank account either."

Wow, Justin Bieber mouthing off and acting like a self-involved, entitled jackass? Where oh where have we seen that before...