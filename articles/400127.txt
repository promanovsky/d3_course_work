NEW YORK (TheStreet) -- Apple (AAPL) - Get Report rose slightly Monday after re/code reported the tech giant bought radio app Swell for $30 million.

Swell, described as "Pandora for talk radio," takes various podcasts and shows and delivers personalized recommendations to users. Swell raised $7.2 million from investors. The app will be shut down as part of the deal, but much of Swell's team will join Apple. The site reports Swell's UI lends itself to in-car listening and could engage fans, but the app could not reach many users.

This marks the latest Apple acquisition after Beats for $3 billion and the book recommendation service BookLamp for between $10 million and $15 million.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

The stock was up 0.74% to $98.39 at 1:03 p.m.

Separately, TheStreet Ratings team rates APPLE INC as a "buy" with a ratings score of B+. TheStreet Ratings Team has this to say about their recommendation:

"We rate APPLE INC (AAPL) a BUY. This is driven by some important positives, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its revenue growth, largely solid financial position with reasonable debt levels by most measures, notable return on equity, expanding profit margins and solid stock price performance. Although the company may harbor some minor weaknesses, we feel they are unlikely to have a significant impact on results."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

Despite its growing revenue, the company underperformed as compared with the industry average of 8.6%. Since the same quarter one year prior, revenues slightly increased by 6.0%. Growth in the company's revenue appears to have helped boost the earnings per share.

Although AAPL's debt-to-equity ratio of 0.26 is very low, it is currently higher than that of the industry average. Along with the favorable debt-to-equity ratio, the company maintains an adequate quick ratio of 1.18, which illustrates the ability to avoid short-term cash problems.

The return on equity has improved slightly when compared to the same quarter one year prior. This can be construed as a modest strength in the organization. When compared to other companies in the Computers & Peripherals industry and the overall market, APPLE INC's return on equity exceeds that of the industry average and significantly exceeds that of the S&P 500.

44.56% is the gross profit margin for APPLE INC which we consider to be strong. It has increased from the same quarter the previous year. Along with this, the net profit margin of 20.69% is above that of the industry average.

Investors have apparently begun to recognize positive factors similar to those we have mentioned in this report, including earnings growth. This has helped drive up the company's shares by a sharp 54.18% over the past year, a rise that has exceeded that of the S&P 500 Index. Regarding the stock's future course, although almost any stock can fall in a broad market decline, AAPL should continue to move higher despite the fact that it has already enjoyed a very nice gain in the past year.

You can view the full analysis from the report here: AAPL Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.