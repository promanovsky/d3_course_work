Just weeks after Disney Star Zendaya Coleman was announced as the star of Lifetime’s Aaliyah biopic, the network has announced her withdrawal and put the project on hold. Lifetime announced the news via their official Twitter account on Sunday.



After fans deemed Zendaya inappropriate, the 17-year-old singer has officially dropped out.

The company is yet to release a detailed statement. At this point, the reasons behind Zendaya’s departure are unknown.

However, her casting prompted a lot of backlash online, when it was announced on June 15, with fans of Aalyiah claiming that Zendaya’s skin was too light and that at 17, the former Dancing with the Stars Contestant didn’t have the necessary experience to play her idol, who died in a plane crash at 22.

More: Lifetime Need To Cast Lead Role In Aaliyah Biopic As Zendaya Drops Out

More: Who Is Zendaya? The 17-Year-Old Who Will Play Aaliyah

Zendaya (or more likely a rep) shared the disappointing news with followers on her own Twitter feed, writing: “Aaliyah has always been a source of inspiration to Zendaya ... She was honored to portray her and pay tribute to her. If she is going to do it, she wants to do it right.”

According to Us Weekly, Aaliyahh's family also opposed the project. They don't feel like a small screen adaptation will do justice to the singer's life, and apparently are refusing to release the rights for her music to be used. This means that not only is Zendaya’s involvement terminated, but the entire project might be in jeopardy.



What will happen to the project now that Zendaya is no longer involved?