Eminem celebrated Mothers' Day in the US by releasing a new video – directed by Spike Lee – for Headlights, from last year's No 1 album The Marshall Mathers LP 2.

Headlights sees Eminem apologising to his mother, Debbie Mathers, for the way he has referred to her in his music in the past. The video, shot from her perspective, explores the troubled relationship between the rapper and his mother and sees the two reconciled.

Eminem had revealed on Twitter and Instagram in April that he was working with Lee on the video.

Have a look at the clip and let us know what you think.

Reading on mobile? Click to view

