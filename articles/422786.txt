Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The Ebola outbreak in West Africa could eventually exceed 20,000 cases - more than six times the current level, the World Health Organisation says.

A new study by the UN health agency also assumes that in many hard-hit areas the actual number of cases may be two to four times higher than is currently reported.

The agency has published new figures saying that 1,552 people have now died from the Ebola virus from 3,069 cases reported so far.

At least 40% of the cases have been in the last three weeks, the WHO said, adding that "the outbreak continues to accelerate".

In Geneva, the agency also released a new plan for handling the Ebola crisis that aims to stop transmission in affected countries within six to nine months and prevent it from spreading internationally.

(Image: Reuters)

Dr Bruce Aylward, WHO's assistant director-general, said the plan would cost 489 million US dollars (£294 million) over the next nine months and require the assistance of 750 international workers and 12,000 national workers.

The 20,000 figure, he added, "is a scale that I think has not ever been anticipated in terms of an Ebola outbreak".

"That's not saying we expect 20,000," he added. "But we have got to have a system in place that we can deal with robust numbers."

Mr Aylward said the far-higher caseload is believed to come from cities. "It's really just some urban areas that have outstripped the reporting capacity," he said.

He also said the agency is urging airlines to lift most of their restrictions about flying to Ebola-hit nations because a predictable "air link" is needed to help deal with the crisis.

Air France has cancelled its flights to Sierra Leone. Mr Aylward said the agency hopes airlines will lift most restrictions within two weeks.