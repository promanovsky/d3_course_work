Unexpected loose gas from fracking

A survey of hydraulic fracturing sites in Pennsylvania revealed drilling operations releasing plumes of methane 100 to 1,000 times the rate the EPA expects from that stage of drilling, according to a study published Monday in the Proceedings of the National Academy of Sciences.