Disney and Marvel could be one of the finest teams around.

Continuing the success of their superhero franchise, Captain America: The Winter Soldier has set a record as the biggest April release ever in the United States. The Disney sequel debuted with $96.2 million (Dh353.3 million), topping the previous record-holder, 2011’s Fast Five, which opened with $86.2 million.

The Winter Soldier, which stars Chris Evans as the shield-wielding superhero and Scarlett Johansson (whose science-fiction film Under the Skin also debuted this weekend in the US with $140,000) as Black Widow, commanded 32 international markets last weekend.

Expanding to Russia, Australia and China in its second week, the Marvel comic adaptation, boasting a budget of over $170 million, earned $107.1 million internationally this weekend, bringing its overall worldwide haul to more than $303 million.

Also a touchstone for Imax, The Winter Soldier received $9.6 million of its US haul on 346 screens showing the film in the enhanced resolution format. Internationally, it showed on 278 Imax screens, bringing in $6.5 million, $4 million of which was delivered from China.

Captain America: The First Avenger, which had a budget of around $140 million, debuted in July 2011 with $65 million. Overall, it earned $371 million worldwide.

Why the shift to an April release rather than remaining a summer launch?

“We looked at the possibility of creating separation from the other summer tentpoles,” said Dave Hollis, head of worldwide theatrical distribution for Disney. “There was an opportunity. We have the second Marvel film coming at the end of the summer in Guardians of the Galaxy. We wanted to start and end the summer and take advantage of this holiday. In the next month or so, we’ll have the benefit of spring break.”

“The Avengers effect,” as Hollis calls it, set the stage for the continuation of Marvel’s massive box office presence, which includes the Iron Man and Thor franchises. With the release of the films’ sequels, both have seen earning jumps of over 35 per cent.

“There are very few movie brands that are this consistent,” Paul Dergarabedian, senior media analyst for box-office tracker Rentrak, said of the Disney-Marvel team. “For The Winter Soldier to push on $100 million in April shows that you can release a big movie any time of the year. Every studio is going to be looking at this date to plant their flag in the future.”

Meanwhile, Paramount’s biblical saga Noah, starring Russell Crowe, Jennifer Connelly and Emma Watson, took a drastic dip in its second weekend, earning $17 million after debuting with $44 million. Still, it sailed into second place, crossing the $70 million mark in the US, while pushing Lionsgate’s young adult science-fiction thriller Divergent, led by Shailene Woodley, to third with $13 million in its third week. Its stateside total is now $114 million.

Freestyle Releasing’s surprise hit God’s Not Dead took the No. 4 slot with $7.7 million in its third weekend.

Despite the decrease in the Noah box office performance, the outcome of the film’s debut, its overall haul, and the success of both Son of God and God’s Not Dead bodes well for other religious-themed films coming this year, including Heaven is for Real, starring Greg Kinnear and Exodus: Gods and Kings, starring Christian Bale.

Coming in at No. 5, Wes Anderson’s The Grand Budapest Hotel collected $6.3 million, bringing its US total to $33 million. Fox Searchlight expanded its stylish comedy to 1,263 locations, and the studio plans to add even more in the coming weeks. This is Anderson’s second widest expansion following 2009’s Fantastic Mr. Fox.

“As this movie is expanding, it is just building an audience,” Dergarabedian said. A total of “$33 million compared to something like Captain America doesn’t sound like a lot, but it’s a huge number for a film like Grand Budapest that is so indie-minded and original.”

The weekend’s other releases included Codeblack Films’ Frankie and Alice, starring Halle Berry as a dancer with multiple personality disorder. Playing in 171 locations, it earned $350,000.

And Fox Searchlight’s dark comedy Dom Hemingway, starring Jude Law and Game of Thrones star Emilia Clarke, had a gross of $32,000 after showing in only four theaters in New York and Los Angeles. Next week, the film will expand to nearly 40 locations.

Disney’s Muppets Most Wanted, which landed at No. 6 with $6.3 million in its third weekend, and Fox’s Mr. Peabody and Sherman, coming in at No. 7 with $5.3 million in its fifth weekend, were the top children’s films. But Fox’s animated Rio 2 stands to knock them down a few notches when it opens in the US next weekend.