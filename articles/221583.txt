Pamela Anderson flashes her famous cleavage in a VERY low-cut top as she enjoys another night out in Cannes



She might be in Cannes promoting her new charity, but Pamela Anderson indulged in a spot of self-promotion as she stepped out on Sunday night.

The former Baywatch babe ensured her famous cleavage was on full display as she rocked a rather daring low-cut black top while making her way inside the VIP club at the JW Mariott Hotel in the French Riviera resort town.



Tucking her revealing top into a flared white skirt, Pamela paired the monochrome number with sheer sockets, red pumps and a black baker boy cap covering her platinum pixie haircut.

On show: Former 'Baywatch' actress Pamela Anderson is seen enjoying a night out at the VIP club at the JW Mariott Hotel in Cannes, France

Anderson chose the 67th Annual Cannes Film Festival to launch her The Pamela Anderson Foundation, which is dedicated to boosting human rights, helping animals and the environment.

She ensured all eyes were on her as she celebrated the charity with another plunging ensemble - this time, a white frock by Vivienne Westwood who was co-hosting the event on Friday night.

The pair marked the occasion with a cocktail party and backgammon tournament thrown aboard a luxury yacht in the French Riviera in support of Cool Earth.



Dressed to impress! The busty starlet was dressed in a low-cut black blouse and a flared white skirt

Anderson chose the 67th Annual Cannes Film Festival to launch her The Pamela Anderson Foundation, which is dedicated to boosting human rights, helping animals and the environment

The actress smiled coyly as she arrived at the JW Mariott Hotel

All eyes were on the 46-year-old mother-of-two as she enjoyed a night out

Anderson has previously talked about being a survivor of rape, but in an emotional speech for the new charity, she credited animals with helping her survive years of abuse at the hands of others.



She said of the traumatic period: 'I did not have an easy childhood. Despite loving parents, I was molested from age 6 to 10 by my female babysitter.



'I went to a friend's boyfriend's house and when she was busy the boyfriend's older brother decided he would teach me backgammon which led in to a back massage, which led in to rape.



'My first heterosexual experience. He was 25 years old, I was 12.'



Anderson wore a cute black flat cap to cover her pixie hair cut The former 'Baywatch' star was in the French resort to launch her new charity The brave woman talked of the sexual abuse she suffered as a young girl and into her first relationship and said animals helped her survive

The assaults didn't end there, she said: 'My first boyfriend in grade nine decided it would be funny to gang rape me with six of his friends. Needless to say I had a hard time trusting humans and I just wanted off this earth.'



'My parents tried to keep me safe. But to me the world was not a safe place. My dad an alcoholic, my mom worked two jobs waitressing.



'My mom was always crying, dad didn't always come home, leaving us in tremendous pain and worry. I couldn't bear to give her more disruptive information so I couldn't break her heart any more than it was breaking,' she added.

She credits animals and nature for giving her hope to live.