Biotech and Internet stocks tumbled again Thursday, and the broader market followed.

After a two-day respite, investors again started dumping shares of cutting-edge drug companies and other industries that have soared over the past year. Biotechnology stocks have turned volatile in recent weeks as regulators scrutinize the cost of their drugs and investors worry their earnings won't justify lofty stock prices. Investors are also worried that high-growth companies like Twitter and Facebook have become too expensive.

On Thursday, the Nasdaq composite, which is weighted heavily toward tech and biotech companies, had its worst day since November 2011.

The rout started slowly and picked up speed throughout the day. By the close, the tech-heavy Nasdaq composite index had its worst day since November 2011. Few companies escaped the sell-off. Of the Nasdaq's 100 largest stocks, only one, C.H. Robinson Worldwide, a freight company, ended higher.

The Nasdaq ended the day down 129.79 points, or 3.1 percent, to 4,054.11. It is now down 7 percent from its recent high reached March 5.

Other major index also fell, but not as much.

The Standard & Poor's 500 index dropped 39.10 points, or 2.1 percent, to close at 1,838.08. The Dow Jones industrial average lost 266.96 points, or 1.6 percent, to 16,170.22.

Get the Biz Briefing newsletter! The latest LI business news in your inbox Monday through Friday. By clicking Sign up, you agree to our privacy policy.

Biogen Idec, Gilead Sciences and other biotech companies plunged. Facebook and Twitter, other recent investor favorites, also dropped.

The sudden downfall of these former high-flyers comes as investors shift from riskier investments to safer areas like utilities, health care and consumer staples. The sell-off in these former darlings, whose stock prices appealed to investors because their rise seemed unstoppable, has weighed on the overall market, especially the Nasdaq.

Gilead Sciences slid $5.17, or 7 percent, to $65.48 on Thursday. Biogen Idec dropped $13.33, or 4 percent, to $287.35. Both roughly doubled in value last year.

Facebook, another stock that doubled last year, sank $3.25, or 5 percent, to $59.16.

The market's drop wiped out gains made earlier in the week. On Wednesday, minutes from the Federal Reserve's latest meeting reassured investors that the central bank wasn't in a hurry to raise interest rates. The S&P 500 had its best day in a month.

Brad McMillan, chief investment officer for Commonwealth Financial, said it seems like investors had been searching for a reason to push the market up. "But there's no compelling story," he said. "Without a catalyst to move the market higher, people are going to start questioning their assumptions."

Weaker sales at Bed Bath & Beyond drove the company's stock down $4.19, or 6 percent, to $63.72. The company reported a drop in quarterly revenue and profit late Wednesday. Like many other retailers, Bed Bath & Beyond laid some of the blame on cold winter weather for keeping customers at home.

Among the handful of winners Thursday was Rite Aid, which surged after the retailer turned in quarterly results that topped analysts' expectations. Rite Aid also announced the acquisition of RediClinic and said it plans to expand the Texas chain of health clinics. The company's stock gained 54 cents, or 8 percent, to $6.94.

In government bond trading, the yield on the 10-year Treasury note dipped to 2.63 percent from 2.69 percent late Wednesday. The price of crude oil fell 20 cents to close at $103.40 a barrel. Gold climbed $14.60 to settle at $1,320.50 an ounce.