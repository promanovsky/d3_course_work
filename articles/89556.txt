Don't miss one heart-pounding moment as "Marvel's Agents of S.H.I.E.L.D." airs back-to-back episodes, which follow in the footsteps and tie into Marvel's new feature film, "Captain America: The Winter Soldier," TUESDAY, APRIL 15 on the ABC Television Network.

From 8:00-9:00 p.m., ET, an encore presentation of the previous week's episode, "Turn, Turn, Turn," airs, in which Coulson and his team find themselves without anyone they can trust, only to discover that they are trapped with a traitor in their midst.

From 9:00-10:00 p.m., ET, the story continues with a new episode, "Providence," which finds Colonel Glenn Talbot on the trail of Coulson and his team, who seek refuge in the last place anyone would look, where they begin to uncover S.H.I.E.L.D.'S most dangerous secrets -- secrets that could destroy them all. Bill Paxton guest stars as Agent Garrett, Patton Oswalt as Koenig and Adrian Pasdar ("Heroes") as Col. Talbot.

"Marvel's Agents of S.H.I.E.L.D." stars Clark Gregg as Agent Phil Coulson, Ming-Na Wen as Agent Melinda May, Brett Dalton as Agent Grant Ward, Chloe Bennet as Skye, Iain De Caestecker as Agent Leo Fitz and Elizabeth Henstridge as Agent Jemma Simmons.

"Marvel's Agents of S.H.I.E.L.D." is broadcast in 720 Progressive (720P), ABC's selected HDTV format with a 5.1 channel surround sound. A TV parental guideline will be assigned closer to the airdate.

For more information on "Marvel's Agents of S.H.I.E.L.D.," visit ABC.com