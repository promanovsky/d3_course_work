In thinking about how we can best serve our veterans, we must spread hope that there are meaningful treatment options. According the Department of Veterans Affairs, roughly 22 veterans die each day by suicide, and that number is on the rise, particularly among younger people who serve. One of the greatest predictors of suicide is hopelessness and isolation; treatment may both instill faith in recovery and a sense of connection.

There are several scientifically supported therapies for post-traumatic stress, suggesting it may be worthwhile to call the condition an injury rather than a disorder.

“The stigma of ‘disorder’ as contrasted with the clarity of ‘injury’ will serve not only the military but impact our whole society,” Dr. Bertram S. Brown, former director of the National Institute of Mental Health, said.

Monty Roberts, an award-winning American horse trainer best known for his humane approach to training horses and decades of work with individuals with post-traumatic stress, echoes Brown’s sentiment

“I don’t believe our psychology fraternity is doing anything to give our returnees a better life with the term disorder,” Roberts, who has developed a novel approach to working with veterans using horses, said.

A few weeks ago, I participated in a psychology workshop at Flag Is Up Farms, in Solvang, California. With my heart palpitating, I watched Roberts, the “Man Who Listens to Horses,” enter a round pen on his 79th birthday with an unruly, stray horse, to demonstrate his “Join-Up” technique.

Amazingly, within 15 minutes, using inviting body language and a calm tone, a roughly 1,200-pound animal cooperatively heeded Roberts’ cues, following him, rather than fleeing, as you’d imagine a wild horse would instinctively respond.

“In giving horses freedom of choice and then putting them through a series of conversational modes, the horse considers trusting you,” Roberts said. "...[With time], the horse comes toward you, instead of away from you, breaking the rules of all flight animals in approaching the predator.”

Roberts confides he was the victim of physical abuse as a child, and sought solace through a mentor, and through his relationship with horses. His own experiences fueled his passion for working with trauma survivors.

To date, Roberts estimates roughly 100 veterans and over 1,000 victims of other traumas have benefited from the program he developed to treat post-traumatic injury (PTSI), in which he combines compassionate conversations with therapeutic interactions with horses. Roberts and his volunteers offer PSTI participants the opportunity to either observe or engage in his “Join-Up” method.

“A military returnee is brought to molten mass-- if that big horse can trust, they too can trust," Roberts said.

Roberts’ non-profit organization offers individuals with post-traumatic stress the opportunity to visit his farm at no cost for three days. At the present time, Roberts does not have data on the efficacy of his work, as he reports he hasn’t had funding for research, though he hopes to conduct research in the future.

Through anecdotal evidence, Roberts shares, “People who suffer for decades report they have relief in days.”

In thinking about why his approach helps, Roberts speculates, “When a horse likes them, chooses them, and trusts them, it has a profound effect on how they think.”

In a letter Roberts showed with me, a veteran who participated in his training thanked him for feeling “renewed” and saving him from a “dark hole.”

Several other therapies that have been researched and found useful to those suffering from post-traumatic stress include cognitive processing therapy (CPT) and prolonged exposure (PE). Both approaches typically consist of roughly 12 sessions.

Recently, people who live far away from therapists have benefited from therapy delivered via the Internet, which has been found to significantly reduce PTSD symptoms. Free apps are also available to support recovery.

To find a CBT therapist, visit http://www.abct.org/home/.

