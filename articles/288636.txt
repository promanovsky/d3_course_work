A spike in crude oil prices and discouraging economic data dragged the benchmarks down on Thursday. Investors were worried about the extent to which the Iraq crisis could affect the nation's oil supply. Moreover, less-than-forecasted retail sales growth and an unexpected rise in unemployment benefits also dented investor sentiment. The S&P 500 dropped for three-consecutive days for the first time since early April. The blue-chip index declined for two-consecutive sessions, a losing streak last seen almost a month ago.

For a look at the issues currently facing the markets, make sure to read today's Ahead of Wall Street article

The Dow Jones Industrial Average (DJI) declined almost 0.7% to close Thursday's trading session at 16,734.19. The Standard & Poor 500 (S&P 500) dropped 0.7% to finish at 1,930.11. The tech-laden Nasdaq Composite Index dropped 0.8% to 4,297.63. The fear-gauge CBOE Volatility Index (VIX) surged 8.3% to settle at 12.56. Total volume for the day was roughly 5.50 billion shares, lower than last month's average of 5.76 billion. Decliners outpaced advancing stocks on the NYSE. For 56% stocks that declined, 41% advanced.

Benchmarks ended in the red on Thursday following concerns about oil supply disruption after unrest intensified across northern and central Iraq. Kurdish forces, the ethnic militant group in Iraq, have seized major cities. They also took control over the oil hub in Kirkuk after government's troops failed to secure the area and abandoned their posts. Prime Minister of Iraq Nouri al-Maliki stated that this is the greatest threat to his government since taking over power.

President Barack Obama responded to these developments by saying the U.S. is contemplating military action in Iraq against these militants. He said: "There will be some short-term, immediate things that need to be done militarily."

Following the news, U.S. oil prices surged to a nine-month high on Thursday. Crude futures gained $2.13 to settle at $106.53 a barrel, its highest closing price since Sep 18, 2013. Brent crude oil futures also went up $3.07 to settle at $113.02 a barrel, its highest level since Sep 9, 2013.

The U.S airlines industry was hit hard due to this surge in oil prices. Airline stocks tumbled for the second-straight day on Thursday. On Wednesday, the stocks were hit hard after Deutsche Lufthansa AG lowered its earnings forecast for 2014 and 2015.

Coming back to Thursday's events, shares of U.S. airline companies such as Delta Air Lines Inc. (NYSE: DAL ), United Continental Holdings, Inc. (NYSE: UAL ), American Airlines Group Inc. (NASDAQ: AAL ), The Boeing Company (NYSE: BA ) and Southwest Airlines Co. (NYSE: LUV ) plunged 5.4%, 5.9%, 4.9%, 1.4% and 4.5%, respectively.

However, the energy sector gained following the sharp rise in crude oil prices. The Energy Select Sector SPDR (XLE) advanced 0.3%. Key stocks from the sector such as Occidental Petroleum Corporation (NYSE: OXY ), EOG Resources, Inc. (NYSE: EOG ), ConocoPhillips (NYSE: COP ), Chevron Corporation (NYSE: CVX ), Denbury Resources Inc. (NYSE: DNR ) and Chesapeake Energy Corporation (NYSE: CHK ) increased 1.4%, 0.8%, 1.1%, 0.7%, 1.9% and 1.5%, respectively.

Markets also received disappointing economic data. The U.S. Department of Commerce reported that seasonally adjusted sales of retail and food services rose 0.3% in March, less than previous month's gains of 0.5%. This rise in retail sales in May was also less than the consensus estimate of an increase of 0.6%. Excluding motor vehicles sales, retail sales increased 0.1%, less than the consensus estimate of a 0.4% increase.

Most retail stocks ended in the red due to the less-than-expected rise in retail sales. Stocks such as apparel & footwear company Bebe Stores, Inc. (NASDAQ: BEBE ), department stores company Bon-Ton Stores Inc. (NASDAQ: BONT ), drugstore chain Vitamin Shoppe, Inc. (NYSE: VSI ), home improvement chain Lowe's Companies Inc. (NYSE: LOW ) and specialty stores company Bed Bath & Beyond Inc. (NASDAQ: BBBY ) decreased 1.8%, 1.4%, 0.3%, 2.9% and 0.5%, respectively.

Separately, the U.S. Department of Commerce reported that business inventories increased 0.6% in April. This was more than the consensus estimate of a rise by 0.4%.

In another report, the U.S Department of Labor reported that seasonally adjusted initial claims increased 4,000 to 317,000 in the week ending June 7. However, this rise in application for unemployment benefits was in contrast to consensus estimate projecting the figure would remain flat at 313,000. The previous week's level was revised up by 1,000 to 313,000.

Eight out of 10 sectors of the S&P 500 ended in the red. The Industrial Select Sector SPDR (XLI) declined 1.3%, the highest among the S&P 500 sectors. Key stocks from the sector such as General Electric Company (NYSE: GE ), United Technologies Corp. (NYSE: UTX ), Union Pacific Corporation (NYSE: UNP ), 3M Company (NYSE: MMM ) and Honeywell International Inc. (NYSE: HON ) decreased 0.7%, 1.2%, 1.0%, 0.9% and 1.3%, respectively.

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

DELTA AIR LINES (DAL): Free Stock Analysis Report

UNITED CONT HLD (UAL): Free Stock Analysis Report

AMER AIRLINES (AAL): Free Stock Analysis Report

BOEING CO (BA): Free Stock Analysis Report

SOUTHWEST AIR (LUV): Free Stock Analysis Report

OCCIDENTAL PET (OXY): Free Stock Analysis Report

EOG RES INC (EOG): Free Stock Analysis Report

CONOCOPHILLIPS (COP): Free Stock Analysis Report

CHEVRON CORP (CVX): Free Stock Analysis Report

DENBURY RES INC (DNR): Free Stock Analysis Report

CHESAPEAKE ENGY (CHK): Free Stock Analysis Report

BEBE STORES INC (BEBE): Free Stock Analysis Report

BON-TON STORES (BONT): Get Free Report

VITAMIN SHOPPE (VSI): Free Stock Analysis Report

LOWES COS (LOW): Free Stock Analysis Report

To read this article on Zacks.com click here.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.