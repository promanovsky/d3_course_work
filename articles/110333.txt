It looks like speculations about an Amazon phone are true after all. The rumor of an Amazon smartphone started back in 2011 and has continued up to today due to the amazing success of its Kindle Fire tablet.

The Amazon phone is expected to be released in the coming three to six months. This was announced by Ming-Chi Kuo of KGI Securities, a respected analyst who had correctly divulged plans of gadget retailers prior to their announcement. He has come out with precise information concerning many key Apple products for the last two years. He is also known to provide correct details on some occasions regarding Samsung products.

His forecast for the Amazon smartphone is that the company will use the same hardware it had used in its tablet and e-reader. Kuo added that Amazon will start to collect materials that will amount to 700,000 to 1.2 million units and three to six hundred thousand units of assembly.

Ming-Chi Kuo said the Amazon smartphone will have six cameras: one basic rear camera, a normal front-facing camera for video chats plus an exceptional system of four extra cameras used for gesture controls, meaning users can use the phone without touching the screen. This feature will make the Amazon smartphone unique when compared with other smartphones. This will also allow the company to incorporate the phone with services like e-commerce.

Here are the Amazon phone specifications according to Ming-Chi Kuo:

Qualcomm Snapdragon 801 processor

4.7-inch display 300-320 ppi

13 mp main camera (Sony); secondary camera (Primax)

Battery size between 2,000-2,400 mAh

Additionally, Ming-Chi Kuo predicted that the hardware features of the Amazon smartphone model would potentially get an excellent reaction from the market and will trigger strong sales.

With this forecast, it seems that the increasingly competitive smartphone market will continue to be more exciting and provide users with more and more features.