'Selina Meyer is running for President!' Julia Louis-Dreyfus's show Veep renewed for fourth season by HBO

HBO announced on Monday that it has elected to renew the Julia Louis-Dreyfus political comedy Veep for another season.

The show features the 53-year-old Seinfeld star as discontented US vice-president Selina Meyer.

The cable network picked up the show for a fourth season after the third episode of its current season aired on Sunday.

Coming back: HBO announced on Monday that it has renewed Veep starring Julia Louis-Dreyfus for a fourth season

The comedy has been critically acclaimed with Julia winning two Emmy awards for her performance of the second-in-command.

Former Arrested Development star Tony Hale, 43, also won an Emmy for Best Supporting Actor in a Comedy Series last year for his portrayal of Selina's personal aide Gary Walsh.

Veep had eight episodes its first season and 10-episodes were ordered for seasons two and three.

Critically acclaimed: Julia is shown at an HBO party last September after winning an Emmy for her work on Veep

Special message: HBO announced the renewal on Twitter and shared a phone number for a special message from Selina Meyer

Selina and her crew have been gearing up this season for the announcement of her campaign for the White House and HBO on Monday shared a message from her on Twitter with her questionable slogan 'Let's get HIGHER with MEYER'.



'Selina Meyer is running for President! #Veep renewed for 4th season! Call 202-688-3894 for a special msg from Selina,' the show tweeted to its more than 22,000 followers on Twitter.

Supporters who called the number heard a message direct from Selina, but she quickly gets tangled up over a quote by President Calvin Coolidge.

Stellar cast: Tony Hale shown holding his Emmy last year after winning Best Supporting Actor in a Comedy Series for Veep

She and her director of communications Mike McLintock stumble through the rest of the recording.

'Please continue your crucial support of my campaign,' Selina said toward the end of the message.

'God, I hate doing these things. Now you can push stop,' she told Mike as the message concluded.

HBO also announced on Monday that it has renewed its new comedy Silicon Valley for a second season following its debut earlier this month.

Silicon Valley and Veep air on HBO on Sunday nights immediately after the hit medieval fantasy show Game Of Thrones.