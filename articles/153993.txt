Quentin Tarantino has seen his lawsuit against Gawker for putting a copy of his Hateful Eight script online thrown out by a federal judge, but the U.S. District Court Judge John F. Walter has also allowed Tarantino to refile the case by May 1, suggesting his legal proceedings are far from over.

Quentin Tarantino was initially dismayed by the leak, but his stance has softened somewhat

In his copyright suit for contributory infringement, Tarantino claimed Gawker "crossed the journalistic line" when it linked to a leaked copy of the screenplay. But Walter said the director had so far failed to make his case, according to Deadline.

"Nowhere in these paragraphs, or anywhere else in the complaint, does plaintiff allege a single act of direct infringement committed by any member of the general public that would support plaintiff's claim for contributory infringement," he said. "Instead, plaintiff merely speculates that some direct infringement must have taken place."

Quentin Tarantino Enlists Stars To Stage Live Reading Of Leaked Script

So, in inviting Tarantino to resubmit the case, Walter has provided the legendary director with another chance to litigate against the popular website, but Q.T will have to come back with something more substantial given this high profile dismissal.

Meanwhile, Tarantino has thawed from his cold reaction to the leak and despite being initially “depressed” he has been holding read-throughs with some of his perennial collaberators with a view to creating second and third drafts of the western screenplay.

"I'm working on a second draft and I will do a third draft but we're reading from the first draft," he told the audience at Los Angeles' Theatre at the Ace Hotel. The 51-year-old also suggested the script would be changed substantially in future drafts - in particular the final act, which he described as the "fifth chapter. The chapter five here will not be the chapter five later so this will be the only time it is seen, ever," he said.

See Tarantino and his 'Django Unchained' stars in this video

