Several variants of the Samsung Galaxy S5 are now available since the April release of the original, including the rugged Galaxy S5 Active and Galaxy S5 Sport, the truncated Galaxy S5 Mini and even the super-powered Galaxy S5 LTE.

But Samsung may also be developing new variant of the Galaxy S5, running its own Tizen operating system.

Details about a Tizen-operated Samsung Galaxy S5 were recently discovered within the “Zauba” database that keeps track of imports and exports to and from India. The database specifically indicates that three units of "Samsung Galaxy S5 Tizen Test Phone" were received by Chennai Air Cargo in India for testing purposes. Zauba has often been a reliable source for uncovering details about unreleased Samsung devices, the model numbers of devices in particular. A Tizen-powered Galaxy S5 appearing within the database does not confirm that such a device would release to market; however, just that such a device exists.

Samsung Electronics Co. (KRX:005930) made headlines in June after it announced that its operating system, Tizen, which it has been developing for over two years, will soon power the Samsung Z, a device set to release in Russia during the third quarter of 2014. The Samsung Z will be the first smartphone to run Samsung’s Linux-based system commercially; Tizen has already debuted on a number of Samsung’s Gear-brand smartwatches.

While Samsung currently powers most of its devices with the Android operating system by Google Inc. (NASDAQ: GOOG), many believe Samsung would ultimately like to switch its high-end devices from running Android to running Tizen.

“Samsung realized hardware innovation is slowing and [becoming] increasingly difficult, so they need to take control of software innovation and differentiation options, to retain a strong place in the value chain,” Nick Spencer, senior practice director at ABI Research, told International Business Times.

Tech website SamMobile noted that the manufacturer’s recent rebranding of its Samsung App store as the “Samsung Galaxy App Store” could be an indication of the manufacturer attempting to differentiate its Android-powered Galaxy brand from its Tizen brand. Notably, Tizen-powered devices will run Tizen apps, which will be available at the Tizen app store. But with the Samsung Z still unreleased, Samsung's plans likely won't unfold until much later.

Follow me on Twitter.