Despite what you may have heard, there are actually many black and Hispanic workers at tech companies. They're just not paid much, and they mostly don't get the same benefits as white and Asian employees.

Black and Hispanic workers make up 41 percent of private security guards, 72 percent of janitors and 76 percent of maintenance workers in Silicon Valley, according to a report released Tuesday by Working Partnerships USA, a labor-affiliated nonprofit that works on behalf of temporary employees.

While software developers in the Valley make more than $60 an hour, the median wage for workers guarding buildings, cleaning toilets and trimming hedges on tech campuses ranges from $11.39 to $14.17 an hour, according to the report.

At that rate, a janitor working full-time wouldn't even be able to afford rent on an average apartment in Santa Clara County, where Silicon Valley is located, the report said.

The findings come at a time when tech companies are under scrutiny for their evident dearth of white-collar diversity. Facing pressure from activists who say the industry should recruit more black and Latino employees, several tech companies released data this summer showing the lack of those minorities in their workforces and the prevalence of white and Asian men, and pledged to do better. At Facebook, Twitter, LinkedIn, Yahoo, Google and eBay, the portion of U.S.-based tech workers who were either black or Hispanic ranged between 3 and 4 percent, Working Partnerships' report said.

"These twin dynamics -- lack of access to high-end tech jobs, and lack of adequate wages in contracted service jobs -- have a profound impact on the communities that are left behind by Silicon Valley's flagship industry," the report reads.

Such low-wage workers are not counted on tech company payrolls, because they work for contractors. Thus, they aren't eligible for the benefits offered on the tech campuses where they work. In Santa Clara County, for example, 88 percent of computer and math jobs offer paid sick days, while only 41 percent of building and ground cleaning jobs and 45 percent of security jobs allow employees to take at least one paid sick day, the report said.

Charles Wilson is one of the workers whom Silicon Valley's perks and benefits don't quite reach. Wilson, who is 32 and black, makes $12 an hour as a security guard patrolling the campuses of IBM and Seagate, a data storage company in Cupertino.

Wilson says he doesn't get health benefits or paid sick leave. So when he sprained his ankle recently, his only option was to ice it down, go to work and "hope I don't have to get into a fight or chase anybody," he said.

When his roommate had to move out because of a medical emergency, Wilson said, he could no longer afford an apartment in San Jose and was forced to move back in with his mother.

"These companies make billions of dollars a year and want to pretend they can't pay the people who clean up after them or safeguard their lives, because it would be some sort of economic hardship," he told The Huffington Post. "But that is untrue."

The Working Partnerships report found that 36 percent of black households and 59 percent of Hispanic households in the county surrounding Silicon Valley fall below the "self-sufficiency line," a term used to describe the amount of income that a family requires to meet its basic needs.

Giving a raise of $5 per hour to 10,000 contracted workers would be enough to lift a security guard from the median wage to self-sufficiency. Such a move would cost less than 0.1 percent of the $103.7 billion the tech industry made in profits last year, the report said.

The report is based on government data for Santa Clara County and workforce data released by tech firms. It doesn't give details on how many low-wage workers work for tech companies, because those companies haven't released information on their contracted workforces.

Working Partnerships' findings are another sign that the wealth being created by the tech industry has not trickled down to the low-wage workers who support those companies and live in the surrounding communities. A recent article in USA Today highlighted the struggles of shuttle drivers who ferry tech workers between San Francisco and Silicon Valley. Earlier this year, demonstrators blocked industry shuttle buses and congregated outside a Google engineer's home to protest what they said were rising rents created by the tech industry's success.

