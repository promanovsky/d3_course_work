Warning: Full "Game of Thrones" Season 4 spoilers ahead. If you have not watched "The Lion and the Rose" or don't want to have the Purple Wedding spoiled, stop reading.

Whether you want a quick refresher or to relive the Purple Wedding, HBO has released a new "Game of Thrones" Season 4 recap video for "The Lion and the Rose." The video does a great job setting up Joffrey's death and the events that led up to the young king's murder at his own wedding.

The "Game of Thrones Season 4: Episode #2 recap" video includes scenes from previous "Game of Thrones" seasons, such as the beginning of Arya and the Hound's journey, the deaths of Craster and Lord Commander Mormont, and the road to Meeren. It also shows the Season 3 scene in which Melisandre leeches Gendry and Stannis Baratheon names three people who he wants killed, including Joffrey Baratheon.

Of course, the final moments of the video focus on the Purple Wedding and Joffrey's death, with one final look at the young king's face. The recap helps explain some recent events and gives some clues about what to expect in the next "Game of Thrones" episode, "The Breaker of Chains."

Based on the synopsis, the Night's Watch scenes from the recap, which highlights the role of the brothers who killed Craster and Mormont, are a reminder that not all the men are as honorable or upstanding as Sam, whereas Jon Snow may try to convince Allister of attacking the Wildlings or sending a group of Brothers beyond the Wall on a mission.

In HBO's "The Breaker of Chains" preview, the aftermath of the Purple Wedding will be an important story line, as will Jaime and Cersei's troubled relationship. For those looking to relive Joffrey's death, HBO has released the final 20 seconds of the Purple Wedding, with Cersei clutching Joffrey and accusing Tyrion of murder.

"Game of Thrones" Season 4, episode 3, "The Breaker of Chains," airs on Sunday at 9 p.m. EDT on HBO.