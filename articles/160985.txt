In January, a federal appeals court rejected regulations designed to assure some measure of fairness in the way America's internet service providers (ISPs) handle information traveling through their networks. The problem, according to the court, was not so much that the Federal Communications Commission (FCC) couldn't insist on what is called "network neutrality" – the idea that customers, rather than ISPs, should decide priorities for information they get online. No, the issue was that the FCC had tried to impose broadband rules under the wrong regulatory framework. And the court all but invited the FCC to fix its own mistake and rewrite its own updated rules.

The FCC's new chairman, the former cable and wireless industry lobbyist Tom Wheeler, said he would comply, rather than appeal. "Preserving the Internet as an open platform for innovation and expression while providing certainty and predictability in the marketplace is an important responsibility of this agency," he said in a February statement.

Now, based on a slew of frightening news reports last night and a "clarification" from the FCC late this morning, we know how the agency – or at least the former cable and wireless industry lobbyist Tom Wheeler – proposes to respond: it won't exercise its supreme regulatory authority in the manner the court suggested.

No, not at all.

Rather, the FCC will say – loud and proud – that it is fixing the open-web problem while actually letting it get worse, by providing a so-called "fast lane" for carriers to hike fees on sites trying to reach customers like you and me. Which, inevitably, would mean you and I start paying more to use those sites – if we aren't already.

This is a potentially tragic turning point in American politics and policy. We are on the verge of turning over the internet – the most important communications system ever invented– to telecoms that grew huge through the government granting them monopoly status. Barring a genuine shift in policy or a court stepping in to ensure fair treatment of captive customers – or better yet, genuine competition – companies like Verizon and Comcast will have staggering power to decide what bits of information reach your devices and mine, in what order and at what speed. That is, assuming we're permitted to get that information at all.

Do we want an open internet? Do we want digital innovation and free speech to thrive? If we continue down the regulatory road pursued by the former cable and wireless industry lobbyist Tom Wheeler, all of those good things will be in serious jeopardy.

Now, in the interest of fairness, it's important to quote the former cable and wireless industry lobbyist Tom Wheeler's reaction to the news leaks, which obviously came from his office. After a brief statement last night, today comes a blog post called "Setting the Record Straight on the FCC’s Open Internet Rules", in which the former cable and wireless industry lobbyist Tom Wheeler says his new proposal "would establish that behavior harmful to consumers or competition by limiting the openness of the Internet will not be permitted."

The proposed rules, according to the blog post, would require:

1. That all ISPs must transparently disclose to their subscribers and users all relevant information as to the policies that govern their network;

2. That no legal content may be blocked; and

3. That ISPs may not act in a commercially unreasonable manner to harm the internet, including favoring the traffic from an affiliated entity.



It's unclear why the FCC has the right even to enforce this under the current non-rules, but never mind that. More to the point is what the rules would allow: the oligopoly ISPs, by all reports, would have the right to cut special deals with web companies to give them that fast lane.

This is an outright perversion of network neutrality. We pay our ISPs for access to the internet, to get a certain speed – we're promised "up to" that speed, which is almost never delivered, because the industry's sleazy but common sleight-of-hand marketing is permitted by the FCC and other regulators.

Consider: an ISP like, say, Time Warner Cable, tells, say, Google that its YouTube videos won't reach us at the, say, "gold package" speed we've already paid for ... unless Google, too, pays a gold-standard fee. That is nothing short of a protection racket, run by a company that has little or no competition. In an actually competitive market, an ISP couldn't conceivably get away with such a thing.

Not everything about the proposed rules, as leaked, is awful. Forcing ISPs to be much more transparent about the level of service they actually provide, for example, is highly useful. Getting them to comply honestly, given their record, is another story.

If you live in America and believe in an open internet, don't waste your time sinking into despair over politicians' betrayals. A little anger wouldn't hurt, but aiming it at the former cable and wireless industry lobbyist Tom Wheeler is pointless. Focus your attention on the people who he works for, and who allegedly work for you. Start with President Obama, whose unequivocal vow as a candidate to support an open internet was as empty as so many of his other promises, if not an outright lie.

Don't stop with Obama, of course. He's made it absolutely clear which side he's on (hint: not ours). We all need to ask our legislators – in your communities, in your states, in Washington – whether they want to discourage innovation and free speech by giving control of this essential public utility to a tiny oligopoly. Then:

At the local level, push for community broadband networks, owned and operated by the public. (Waiting for Google Fiber? You might as well wait to win the lottery. Google is not your daddy, or your savior.) The community broadband success stories are already dramatic ...

... so dramatic that the telecom cartel has frantically worked to get state legislatures to prevent them from existing in the first place. Tell your state legislators that this is an unacceptable intrusion on your community's right to govern itself.

Finally, tell your member of the US House of Representatives and your US senators that they have a job to do – to ensure the future of innovation and free speech in a digital world. In particular, tell them that internet access is a public utility and should be treated as such.

There are two ways to prevent abuses by the owners of public utilities. One is to regulate them. Remember that advice from the appeals court? The FCC could re-classify Internet access as a "telecommunications service" (as opposed to the current, largely unregulated "information service") and require neutral treatment by ISPs. For the moment, this may well be the best choice, but we need to be clear that doing this would not be simple nor without unintended consequences.

Another, better fix is to create genuine competition, or at least the conditions for it. The simplest path forward here would be to require the monopoly/oligopoly ISPs – remember, they got this big in the first palce because they were once granted exclusive rights to "serve" their geographic communities – to share their lines, at a fair price, with competing ISPs. This is what many other countries do, and we should give it a try here.

The best solution? Taxpayers should pay for a fiber-everywhere system, then let competing ISPs use it to compete in a genuine free market. But do not hold your breath on that one.

The sky hasn't fallen with today's FCC announcements. Let's not panic. But if we don't start getting serious about this, as a public, we will lose the most important medium in human history. That would be worse than tragic.