Although Lena Dunham may seem in her element as the spirited female protagonist on Girls, a long-term acting career doesn't appear to be in her future.

In a revealing new interview in the April issue of Glamour magazine, the 27-year-old star admits that she's already considering spending a lot less time in front of the camera.

"I don't know if I'm going to want to act any more," she says bluntly. "I'm always relieved on the days I don't have to. I'd rather give parts to other women than be the woman having the parts."

Story continues below advertisement

Fortuitously, Dunham has other talents to fall back on: She also writes and directs most episodes of Girls, which wraps its third season this month.

And of course there will always be those occasions when Dunham may stumble upon a project to her liking.

Dunham vaulted into the business by writing, directing and starring in the low-budget indie feature Tiny Furniture in 2010.

She memorably essayed a small support character in her mentor Judd Apatow's 2012 comedy This Is 40 and tackles a more substantive role in the upcoming feature Happy Christmas.

But Dunham's primary job these days is making Girls, which has already been renewed by HBO for a fourth season, set to premiere in 2015.

In the same Glamour interview, Dunham provides some rare insight into her personal life, including her relationship with her boyfriend, rock guitarist Jack Antonoff of the band fun.

"I remember talking with him on our first date and him being like, 'God all the articles about your nudity on the show are such bulls–t,' " says Dunham. "It's funny, 'cause in some ways that the conversation we still have when I'm upset [about articles about the show]."