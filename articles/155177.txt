Michigan resident Roger Pontz has regained some of his eyesight with the help of the surgical implantation of a "bionic eye".

Pontz was diagnosed with retinitis pigmentosa as a teenager, and has been almost completely blind since, according to The Washington Post.

The procedure has granted the 55-year-old former competitive weightlifter and factory worker enough eyesight to see tiny glimpses of his wife, grandson and cat.

"It's awesome. It's exciting - seeing something new every day," Pontz said during an appointment at the University of Michigan Kellogg Eye Center.

Pontz is one of the four people in the U.S. that has received an artificial retina since its use was signed off by the Food and Drug Administration (FDA) last year. Since then, all four surgeries have taken place at facility in Ann Arbor. The fifth surgery is set to take place in May, AOL reported.

Retinitis pigmentosa, which is an inherited disease, causes loss of side vision and night vision, as well as central vision, which can make some go nearly completely blind.

The transplant is included in a system created by Second Sight that provides a transmitter and tiny video camera in a pair of glasses, The Washington Post reported. Images taken from the camera are turned into electrical pulses that are transferred wirelessly to electrodes on the retina's surface. The retina's remaining healthy cells are stimulated by the pulses and send the signal to the optic nerve. The brain then receives the visual information and translates it into patterns of light that the patient can recognize and understand, giving some visual function back to the patient.

Pontz also received assistance from occupational therapist Ashley Brown, who taught Pontz techniques to improve his vision. These techniques included moving white and black plates back and forth in front of light and dark backgrounds and figuring out their color, AOL reported.

Pontz's wife, Terri, who drove her husband 200 miles to the facility and helped him practice the techniques at home, said all of the work was a blessing.

"What's it worth to see again? It's worth everything," Terri Pontz said.

Dr. Thiran Jayasundera, one of the two physicians who performed the surgery on Pontz, said the procedure is a "game-changer", to which Pontz agreed, AOL reported.

"I can walk through the house with ease," Pontz said. "If that's all I get out of this, it'd be great."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.