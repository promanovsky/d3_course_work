Eliott Rodger in his chilling YouTube video in which he describes his killing spree plan[YOUTUBE]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up fornow and never miss the top politics stories again

Film director’s son Elliot Rodger, 22, stabbed three male students to death in his flat before embarking on a shooting spree, which cost the lives of two female students and a man in a local delicatessen.

It ended only when Rodger was cornered by police and killed himself with a single shot to the head.

Police found a print-out of the manifesto in the student’s apartment in the Santa Barbara suburb of Isla Vista, 95 miles north of Los Angeles.

Across 141 pages bearing the headline My Twisted World, it sets out the obsession with women of virgin Rodger and his crazed belief that they must die for rejecting him throughout his life and his intention of killing “50 or more” people.

Detectives believe Rodger may have murdered his first three victims, one of them his roommate, in his apartment in a series of frenzied knife attacks as long as 24 hours before he began his drive-by shootings.

He had warned in the manifesto: “On the day before the Day of Retribution, I will start the First Phase of my vengeance, silently killing as many people as I can.”

In a later video, he then threatened: “I will take to the streets of Isla Vista and slay every single person I see there.”