The prevalence of HIV/AIDS in South Africa is rising due to the world's fastest growth in new infections and a higher patient survival rate, according to a new health study.

An estimated 12.2pc of South Africa's population was infected with the HIV virus in 2012, compared with 10.6 percent in 2008, according to a survey of 38,000 people carried out by the country's Human Sciences Research Council.

The percentage rise was partly due to 400,000 new HIV cases in the year studied, the highest in the world, taking the total number of people infected in South Africa to 6.4 million.

Young black African women were the worst affected, with 23.2 percent of females aged 15-49 infected, compared with 18.8 percent of men, the study showed.

Treatment of the virus is increasing, with around 2 million people on an expanded antiretroviral treatment plan.

However, the study found the overall knowledge about how HIV is transmitted and can be prevented fell to 26.8 percent in 2012, from 30.3 pct in 2008.

Three-quarters of those surveyed believed they were at low risk of contracting HIV, even though one-in-ten of those tested were found to be already infected.

South Africans under fifty were having increasing numbers of sexual partners and using condoms less.

"The increases in some risky sexual behaviours are disappointing, as this partly accounts for why there are so many new infections still occurring," said Professor Leickness Simbayi, an investigator on the study.

Despite a government push to spread the treatment of HIV, medical charities warned last year that many clinics were running short of life-saving HIV/AIDs drugs.

South Africa awarded a $667 million two-year contract in 2012 to pharmaceutical firms, including Aspen Pharmacare , Abbott Laboratories and Adcock Ingram , to supply HIV/AIDS medication.

Online Editors