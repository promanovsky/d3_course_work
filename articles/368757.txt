“Modern Family” star Sofia Vergara apparently moved on from ex-fiancé Nick Loeb to People’s Hottest Bachelor Joe Manganiello; looks like the magazine is going to have to rescind that title if the rumors are true.

Turns out the “True Blood” hunk, 37, was able to work his magic on the Columbian actress, 41, various sources told Us Weekly Monday evening. "It's very, very new," an insider said. "They're having fun and getting to know each other."

Manganiello, who once starred as a stripper in “Magic Mike,” visited the brunette beauty over the Fourth of July weekend in New Orleans, where she is filming “Don’t Mess With Texas” with Reese Witherspoon.

A different source said the two were introduced at the White House Correspondents Dinner in May. Things didn’t click back then because Vergara was there with her ex. "That's the first time Sofia and Joe met," the source said.

"Sofia is Joe's dream girl!" a different insider side. "He's been smitten with her for years. He's always had a huge crush on her. Things are going well for them so far."

Another source said: "Sofia had no idea that he had been crushing on her for a while."

Vergara and Loeb broke up because their relationship was just “not fun anymore,” she said on Twitter. "We are still very close but we believe it's the best thing for us right now."

The last time they were reportedly seen together was also the first time she and Manganiello met: at the White House Correspondents Dinner.

Before being linked to Manganiello, it was rumored Vergara was the reason there was turmoil in Ellen DeGeneres and Portia de Rossi’s marriage. Apparently di Rossi is jealous of the comedian’s relationship with the “Modern Family” actress.

Follow me on Twitter @mariamzzarella