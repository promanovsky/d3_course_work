Chicago - A new treatment option is more effective than tamoxifen at preventing a return of breast cancer in young women, according to the results of two international trials.

The findings show that exemestane, given along with treatment to suppress the function of the ovaries, reduced the risk of breast cancer's return by 34 percent, and cut the risk of a subsequent invasive cancer by 28 percent.

Exemestane is an aromatase inhibitor that until now has largely been used in older women who have reached menopause, since it requires women to have a low level of estrogen in order to work.

Tamoxifen, meanwhile, is commonly prescribed to women who have had breast cancer as a way to prevent its return.

“For years, tamoxifen has been the standard hormone therapy for preventing breast cancer recurrence in young women with hormone sensitive disease,” said lead study author Olivia Pagani, clinical director of the breast unit at the Oncology Institute of Southern Switzerland.