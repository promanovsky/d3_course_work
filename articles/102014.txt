The Justice Department and FBI have opened an investigation of Los Angeles nutritional products company Herbalife Ltd., which has been fighting critics who say it’s operating an illegal pyramid scheme.

Law enforcement sources confirmed the investigation. The FBI started looking into the company “more than several months ago,” said a person who has been briefed on the investigation.

“We are doing our job of getting to the bottom of this issue,” said the official, who asked not to be identified because the investigation is ongoing. “And I would stress that no one is going to be under arrest imminently. Maybe no one will be arrested.”

Shares of Herbalife Ltd. suffered their biggest loss in more than a year Friday after reports of the investigation surfaced. The stock plummeted $8.36, or 14%, to close Friday at $51.48; trading volume was heavy.

Advertisement

Herbalife said in a statement that it was unaware of the FBI probe, first reported by the Financial Times.

“We have no knowledge of any ongoing investigation by the DOJ or the FBI, and we have not received any formal nor informal request for information from either agency,” the statement said. “We take our public disclosure obligations very seriously. Herbalife does not intend to make any additional comments regarding this matter unless and until there are material developments.”

At the FBI, Christos Sinos, a supervisory special agent, did not deny that an inquiry was underway. Rather, he said simply, “We are not commenting.”

If Herbalife is unaware of the investigation, it’s possibly a sign that the probe has not yet reached a critical stage, said Jeff Ifrah, a partner at Ifrah Law in Washington, D.C.

Advertisement

Should the case lead to criminal charges, “the impact on stock price and the company’s ability to sign up new distributors would be completely, completely crippled,” Ifrah said.

Robert A. Mintz, a former federal prosecutor who is now a partner with McCarter & English in Newark, N.J., said the criminal investigation provides “an added degree of uncertainty as to the future of the company.”

“The potential consequences from a criminal investigation are significantly more severe than in a civil investigation,” he said. “A criminal investigation will typically look at the conduct of individuals as well as the company, and there’s obviously the threat of jail time for an individual who may be charged and ultimately convicted.”

Herbalife, founded in 1980 by a charismatic pitch man named Mark Hughes, sells nutritional and weight-loss products in more than 80 countries.

Advertisement

Its products — the most popular of which is a meal-replacement shake mix — are not available in stores. Rather, the company relies on a team of more than 3 million independent sales people, also called distributors, to sell the products and coach clients about nutrition. The distributors are paid by commissions from sales and bonuses for recruiting new distributors.

The company reported $4.8 billion of revenue and $527 million of net income in 2013, making it one of the largest and most profitable companies in the world.

In December 2012, hedge fund manager Bill Ackman launched a high-profile attack on Herbalife, saying he had concluded after extensive research that the company operates a pyramid scheme that will ultimately be shut down by regulators. He said he had shorted more than $1 billion of the company’s stock, a move that would allow him to profit if Herbalife’s stock price fell.

Ackman said the vast majority of the company’s distributors lose money, while a spare few “at the top of the pyramid” make millions of dollars from recruiting efforts.

Advertisement

Herbalife has said the allegations are misguided, noting that most of its sales people sign up with the company in order to receive discounts on products they personally consume.

Investors sold shares of Herbalife after Ackman’s allegations, driving its stock to a midday low of $24.24 on Dec. 24, 2012, but the company’s shares surged 139% in 2013 as regulators failed to act. Ackman reduced some of his short position, as his bet was down hundreds of millions of dollars.

But this year, Ackman’s high-profile allegations have gained traction.

The Federal Trade Commission confirmed last month that it was conducting a civil inquiry of Herbalife. The California attorney general’s office also confirmed it was meeting with Herbalife critics, including some former distributors. Herbalife has previously disclosed that the Securities and Exchange Commission was investigating the company.

Advertisement

richard.serrano@latimes.com

Twitter: @rickserranoLAT

stuart.pfeifer@latimes.com

Twitter: @spfeifer22

Advertisement

Pfeifer reported from Los Angeles and Serrano from Washington.

Times staff writers Andrea Chang and Walter Hamilton contributed to this report.