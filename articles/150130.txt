From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Google has updated and streamlined its popular AdWords platform — and the new features are downright nifty.

It’s clear that Google is betting big in the mobile advertising space, where it dominates as the number one online ad platform in an industry that will reach over $31 billion by the end of the year, according to eMarketer. Facebook and Twitter have also made inroads in the arena, but Google still captures more than half the global market.

The announcement about the AdWords update was posted to Google’s website this morning. VentureBeat obtained a sneak peek of the enhancements late Monday.

Jerry Dischler, Google’s vice president for product management at AdWords, was clearly delighted by the enhancements, which include automated billing, more bulk actions, advanced reporting protocols that let user analyze ad data without having to re-format it, among numerous other features.

According to Dischler’s post:

“People are constantly connected, moving seamlessly between screens, sites and apps while on-the-go. This makes the best ads more than just messages sent to various devices. Instead, ads are now most effective when they connect people with the information, content and places that matter most to them, at the moments they are looking.”

Dischler unveiled the upgrades at the AdWords Performance Forum. Google made it clear that smaller companies looking to expand their digital marketing footprint were just as crucial as larger ones with massive advertising budgets.

In the post, which was headlined “Shiny, appy AdWords!,” Google laid out the changes. You can read the post here.

“Better discovery: For businesses looking to promote app installs on the AdMob network, we’ll enable you to reach people who are your most likely customers, based on the apps they use, the frequency of use and the types of in-app purchases they make. For example, if you exercise regularly and use an app to measure how far you run, you might see an ad for an app that helps you measure the foods you eat and calories consumed. On YouTube, we’re enabling app installs as an enhancement to the current TrueView offering.”

The upgrades are already scaling for Google, as Dischler pointed out in his post.

“The feedback on Estimated Total Conversions has been great, so we’re continuing to invest in this product. As people search more online for local businesses and then go into the store to make purchases, we’re testing ways to measure the effectiveness of search ads at driving in-store sales, using anonymized purchase data from retail partners.”

In the competitive mobile ad space, Google wins again.