Zero hour is finally approaching for the thousands of eager tech-savy shoppers who have been waiting months for HTC to reveal its new smartphone device which will follow up its HTC One, designated "Smartphone of the Year" by Mobile World Congress last month.

However, in the last few months, a plethora of online leakers have posted spoiler photos and information about the device, codenamed M8. While it's safe to consider a handful of the rumors as nothing more but conjecture, quite a bit of the information flying at high speed across the web will more than likely come to fruition at Tuesday's unveiling.

Last month the Taiwan-based cell phone manufacturer announced its plan to unveil its new smartphone on March 25 instead of revealing it in Spain at the annual Mobile World Congress exhibit.

So many rumors have been leaked about the HTC One 2 or One Up that it's like getting to see your presents unwrapped days before Christmas but without the joy of getting to unwrap it and appreciating the gifts yourself.

Although so much hype has been built up around the new device the audience that will be attending the London and New York events Tuesday will still be in for a treat.

According to The Economic Times, earlier this month Roshan Jamkatel, a teenager from Schaumburg, Ill., somehow got his hands on the phone before and posted a YouTube video giving viewers a tour of the high-profile device.

The video was taken down but numerous websites got a chance to post it before also being ordered to remove it.

The publication reported that HTC Senior Global Online Communications Manager Jeff Gordon posted a message to Jamkatel's Twitter account.

"It's not going to be a good week for you, my friend," Gordon wrote.

Jamkatel replied via Twitter that the phone was a fake, but Gordon wrote, "We have the IMEI and all the other info. We'll be in touch."

Numerous leaks have included photos of the smartphone and its new pin-hole case as well as a myriad of information regarding the device's design, production, components and features.

The Economic Times reported that based on the research firm Canalys, HTC has sold more smartphones in the U.S. than any other company since 2011. But the company's shares have plummeted almost 90 percent since.

Its market capitalization dropped from $33 billion to $4 billion. HTC's primary competitor, Samsung, spent $14 billion in 2013 on advertising.

Although the phone has received critical acclaim tomorrow's new phone will instantly become yesterday's old one if HTC doesn't step up its marketing campaign.