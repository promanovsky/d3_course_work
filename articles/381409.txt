Following news that she will be exiting her post on ABC's THE VIEW, Jenny McCarthy announced yesterday that she will move on to host a one-hour weekly series on SiriusXM beginning this Wednesday.

Titled 'Dirty, Sexy, Funny With Jenny McCarthy, the show will feature hot topics such as parenting, sex, dating and marriage. Among the guest stars already booked to appear on the show are Chelsea Handler, Perez Hilton and McCarthy's fiance, Donnie Wahlberg. (

In an interview with Howard Stern yesterday, the 41-year-old spoke about her year-long stint on the ABC daytime talk show. "The most rewarding [part of being on 'The View'] has definitely been working with Barbara Walters for a year, I mean work[ing] under her and just ... asking her questions backstage has been the most intriguing thing," she said.

However, McCarthy shared that she quickly realized the experience was not how she had envisioned it would be. "I'll tell you something Howard, it's like halfway into it, it was probably around February, I went, 'God I'm not allowed to be the best of me here,' I feel like to have a voice, to be able to speak without having to interrupt people ... It's very difficult, and I don't like doing it, it's very uncomfortable, so I felt like, 'You know what? Maybe this is just a stepping stone for me to go somewhere where I can actually be my full self.'"

Speaking on her upcoming radio gig, McCarthy said, "It's no secret that I love to talk. I'm excited to do it on SiriusXM without having to interrupt anyone or keep things clean."

Source: US Weekly