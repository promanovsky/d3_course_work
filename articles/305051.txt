Justina Pelletier is being released to her family after 16 months in state custody. At a press conference this morning her mother and sister stood by their position that Justina should have remained in her family’s care.

“I’m just so grateful that today has finally come,’’ Linda Pelletier said. “But it’s going to be a long haul for her to get bet better.’’

Justina’s mother and sister, Jennifer, spoke alongside family spokesperson Rev. Patrick Mahoney, thanking those who have helped them regain custody and asserting that they should have had custody of Justina all along.

In February 2013 doctors at Boston Children’s Hospital alleged Pelletier‘s declining health was the result of child abuse rather than the rare mitochondrial disease her parents suspected, and custody was granted to the Department of Children and Families.

Advertisement

“We are not here to bash DCF of Massachusetts,’’ Mahoney said. “We are here to say this: It is parents and families who know what is best for their children, not government entities. Justina needs her family’s care, love, and support.’’

Mahoney also said at today’s press conference that DCF never visited the Pelletier home despite being invited to do so, and Justina’s mother and sister say they met only one DCF worker after Justina was taken.

“This should have never happened, and this should never happen again to any child in any situation,’’ Jennifer Pelletier said.

The court ruling for Justina’s release is effective June 18, and the Pelletiers say they plan to share video of her homecoming.