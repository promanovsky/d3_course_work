Family Dollar Stores Inc. said it would close 370 under-performing stores later this year and slash prices on 1,000 products after its profit for the second quarter plunged more than 30%.

The North Carolina discount retail chain, which has more than a dozen stores in the Los Angeles area, also said it would curb store openings in the next fiscal years to 350 to 400. The company planned to launch 525 new stores this year.

Family Dollar has 8,100 stores across the country.

The so-called strategic actions also include “reducing corporate overhead,” a move that will result in job cuts. The company said it hopes the effort will save $40 million to $45 million annually in operating costs.

Advertisement

Howard R. Levine, Family Dollar’s chief executive, said in a statement that “a more promotional competitive environment and a more financially constrained consumer” were partly to blame for the chain’s disappointing performance.

“Severe winter weather” also didn’t help, forcing stores to close and disrupting merchandise deliveries, he said.

During the second quarter, which ended March 1, Family Dollar revenue slid 6.1% to $2.7 billion. The chain, which competes with Dollar General Corp., 99 Cents Only and Dollar Tree Inc., said customers bought less often but spent more per transaction amid higher markups.

Net income tanked 34% to 80 cents a share, or $90.9 million, from $1.21 per share, or $140.1 million. Same-store sales at locations open at least a year dipped 3.8% -- a trend the company said it expects to continue in the third quarter.

Advertisement

Family Dollar stock was down 1.5%, or 89 cents, to $58.18 a share in morning trading in New York.

ALSO:

Family Dollar Stores to expand into California

Walgreens to close 76 unprofitable stores, 14 in the West

Advertisement

Staples to close up to 225 stores; February retail sales mixed