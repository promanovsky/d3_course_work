Macy's Fourth of July Fireworks 2014: Live Stream, NBC TV Start Time, NYC Best Viewing Spots, How to Get There, Location and Watch Online [MAPS] Macy's Fourth of July Fireworks 2014: Live Stream, NBC TV Start Time, NYC Best Viewing Spots, How to Get There, Location and Watch Online [MAPS]

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Macy's 4th of July Fireworks Spectacular 2014 will take place in New York City on Friday night with millions across the city set to celebrate Independence Day together.

All the schedule, start time, details of best viewing places to see the fireworks in NYC and on TV on NBC, are shown below. In addition, the fireworks will be available to watch online through free live stream (details below).

This year's Macy's Fourth of July Fireworks will take place over the East River, where hundreds of thousands are set to gather.

Amy Kule, Executive Producer of Macy's 4th of July Fireworks, has said ahead of the event: "We are thrilled to be working with our partners in the City of New York to have the magnificent Brooklyn Bridge as our canvas and stunning backdrop for this year's bigger-than-life display."

The fireworks will start at 9 p.m. ET and last for about 25 minutes, according to organizers, although build-up on TV will start at 8 p.m. ET. This year, those managing the display have boasted that there will be 50,000 pounds of fireworks used over the 25 minute show.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

The fireworks will be set off from the Brooklyn Bridge as well as numerous barges set up all along the East River.

The firework display will be shown as various musical numbers are performed – most of which will boast some kind of patriotic theme. The event will build up to a climax where dozens of fireworks will be set off at once to create the flag of the United States of America in the sky as the national anthem plays.

Pop princess Ariana Grande will be at the event in New York to add some celebrity element to the proceedings, as will Enrique Iglesias and Lionel Richie.

This year's event will be shown on TV on the NBC network, but viewers will also be able to tune in to see the spectacular display online, where Nick Cannon will be presenting a long show featuring various family friendly entertainment from 5 p.m. ET.

There are numerous viewing areas across the City, but organizers have advised that those hoping to catch the fireworks go to one of the elevated viewing platforms on portions of FDR Drive. The access points to get to those platforms can be seen by clicking here.

Meanwhile, those who want to watch the show on TV can watch on NBC, or those wanting to watch online through live stream can watch by clicking here.

The following map will show those wanting to attend the event live, where the show takes place and how to get there:

HOW TO GET THERE

Call MTA at 511 for automated travel information 24/7. Or agents are available in stations from 6am to 10pm daily. Visit www.mta.info to plan your trip with Trip Planner.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit