MADRID (MarketWatch) — European stocks advanced on Monday after European Central Bank President Mario Draghi’s unexpected suggestion at Jackson Hole, Wyo. that further stimulus could be on the way for the region.

A jump in U.S. stocks that carried the S&P 500 above 2,000 for the first time provided an extra boost.

“He feels action may be needed to boost demand in Europe, and this will see markets start to price in probability for further stimulus in the near term,” said Stan Shamu, market strategist at IG, in a note. “The result was a drop in the euro today and is also likely to support European equities.”

Also read: 5 things to watch in Europe this week

Market reaction: The Stoxx Europe 600 index SXXP, +0.64% rose 1.1% to 340.46, adding to the gain of 2.1% last week.

Gains extended across key national markets. The CAC 40 index PX1, +0.60% jumped 2.1% to 4,305.76, while the German DAX 30 index DAX, +0.06% climbed 1.8% to 9,510.14. The Spain IBEX 30 index IBEX, +0.22% gained 1.8% to 10,690.10 and the FTSE MIB index XX:FTSEMIB rallied 2.3% to 20,375.39.

London markets were closed for a holiday, further weighing on the typical end-of-summer trading-volume lull.

Also read: Notable stock moves in Europe

More downbeat German data and what Draghi said: “Everyone in society is affected by high unemployment. For central banks it is at the heart of the macro dynamics that determine inflation, and even when there are no risks to price stability it increases pressure on us to act,” Draghi said, according to a summary of the speech made available by the ECB.

Draghi was speaking at the Kansas City Federal Reserve’s annual symposium in Jackson Hole, Wyo. on Friday. He argued that more unconventional action may be needed, but that increased flexibility on fiscal policy across the region and efforts to overhaul the labor market are crucial. The remarks were a hint that bigger countries in healthy shape need to do more to drive demand. Also see: Yellen says debate over when to hike is now center stage

On the heels of Draghi’s comments, a measure of German business confidence declined more than expected, according to data released Monday by the Munich-based Ifo Institute. The survey fell to 106.3 in August from 108.0 in the previous month, versus expectations for a fall to just 107.