Fans of ‘The Walking Dead,’ we hear your frustration over the March 30 Season 4 finale. We know that you think ‘nothing happened’ and that the flashbacks were pointless. However, we disagree! It was the perfect way to cap off Season 4 and to hit the reset button for Season 5.

A lot of fans are upset over the March 30 Walking Dead season finale, claiming that “nothing happened” and that the flashbacks to the prison to show how much everyone has changed were redundant and unnecessary. However, I thought that the finale was a great way to cap off the season; after all, if all of your questions were answered, what would keep you tuning in next season? Read on for more about why we thought the Season 4 finale was as perfect as it could get, and tell me if you agree with me or if I’m talking nonsense.

‘The Walking Dead’ Finale Was The Perfect Way To End Season 4 And Gear Up For Season 5

Firstly, let’s be honest — for a show about zombies, the fast-paced, high-tension action scenes are few and far between in comparison to the various scenes of our heroes walking through the woods, endlessly. As fans, we all know this. A whole lot of “nothing” always has, and always will happen on The Walking Dead — but during these “nothing” scenes is the focused, meaningful, and compelling character development that keeps us coming back for more.

A lot of fans were frustrated by the flashbacks to the prison, insisting that they didn’t need to be told that Rick (Andrew Lincoln) and Carl (Chandler Riggs) have changed — that it’s fairly obvious. However, Rick was already pretty hardened by the time our heroes ended up at the prison; regardless, he still had hope that a home could exist for them. There was still hope that he could just plant vegetables, farm animals, and that he could still draw Carl away from the darkness inside of him.

Now, Rick is more violent, more protective, and more suspicious. Rick, Carl, Darl (Norman Reedus) and Michonne (Danai Gurira) entered through the back door of Terminus, and were wary of the Terminus welcome wagon from the get-go. They weren’t going to just accept that Terminus could be a home for them — not after how their experience with the prison turned out.

The Grimes family used to have a milder side to them, but being attacked by Joe (Jeff Kober) and his group of “Claimers” was a turning point for the both of them; Carl has almost definitely crossed over to the dark, unfeeling side after escaping sexual assault by a hairsbreadth, and Rick ripped out Joe’s throat with his teeth. His teeth! Immediately afterward, he gutted Carl’s attacker like a fish. These people are not messing around anymore.

‘The Walking Dead’ Finale — It’s Not True That ‘Nothing Happened’

When Rick, Carl, Daryl and Michonne were corralled into the train car to reunite with the rest of Team Prison (and a few newcomers), Rick’s closing line said it all: “They’re screwing with the wrong people.” It’s kind of a bold statement to make when you’re trapped in a train car, but it’s a show of how far Rick has come as a character. He is willing to do anything and everything to protect himself, Carl, and the rest of Team Prison, and he won’t let being trapped like a rat by a bunch of cannibals stop him. It’s the perfect way to end Season 4 — Rick and Carl have grown up, and are putting away childish things, and are going to open Season 5 kicking ass and taking names.

Remember when Hershel (Scott Wilson) asked Rick in one of the prison flashbacks: ”He needs his father to show him the way. What way are you going to show him? What’s his life going to be? What’s yours?” That was important for us to hear. Murder and suspicion are their lives now; that’s what Rick showed his son, and Carl is clearly taking it in. It will be interesting to see how Carl develops in Season 5. Is he going to go the way of Lizzie (Brighton Sharbino)?

The character development this season has been incredible. Glenn (Steven Yeun) forgave Tara (Alanna Masterson) for her inadvertent part in his separation from Maggie (Lauren Cohan) after the calamity in the prison. Carol (Melissa McBride) confessed to Tyreese (Chad Coleman) her murder of Karen (Melissa Ponzio); he forgave her. Daryl, who once butted heads with Rick daily, stood up to Joe and his gang to defend him. Rick called Daryl his “brother.”

The Walking Dead is not all about zombies and murder; it’s about characters, and it’s about how real-life people would deal in a real-life apocalyptic scenario — that’s what the show always has been, and will continue to be. And we’ll keep watching to the bitter, bloody end.

What do you think, HollywoodLifers? Do you agree with me, that the Season 4 finale was not so bad as everyone has been saying? Or do I have it all wrong? Vote and comment below with your thoughts on “A!”

— Amanda Michelle Steiner

More ‘The Walking Dead’: