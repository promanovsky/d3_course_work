This past week has been tough for Internet users because of the discovery of the serious Heartbleed bug that caused panic all over the web. The entire world came to know about the critical security flaw in the OpenSSL library which allowed attackers to exploit any secure system and collect up to 64k of data at a time from the active memory of affected systems. This so-called Heartbleed bug was then patched thanks to an immediate fix.

App to check for ‘Heartbleed’ Bug

Lookout Mobile Security, a company focused on security, has released a free app over at Google Play Store that basically checks your Android smartphone or tablet to determine whether or not your device is vulnerable to the Heartbleed bug in OpenSSL. The app, called Heartbleed Detector, will check what version of OpenSSL your Android device is running on, and based on that information, it informs you whether your device is affected by this bug or not.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

However, note that is not a cleaner app and that’s why it won’t fix this problem. Just like the name suggests, it will only try to detect if the bug will affect you or not. To fix this problem, Google has sent Android updates to OEMs, which means the OTA updates should fix this problem. Don’t forget to check for updates manually each day, so as to ensure that you get those updates as soon as you can.

This Heartbleed vulnerability can only be fixed by Google or by your device manufacturer, but the app does a good job at keeping you informed about the status of your device. So if you’re eager and want to check if your Android device is vulnerable to this bug or not, then this is the app that you should give a try.

Install Heartbleed Detector from Google Play