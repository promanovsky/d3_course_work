Prior to the 50th anny of the Beatles' A Hard Day's Night movie, Ringo Starr sat down to recollect the making of it. Speaking with Billboard, Starr said that the Fab Four couldn't believe what they were doing at the time:

"I mean, we were in a movie, man. We were making a movie! Four guys from Liverpool making a movie - it was so great. I loved it."

He added, "It was a really exciting thing to do. We were making records and, wow, the records were taking off and then we're playing to bigger and bigger audiences and that's taking off, and now we're doing a movie. It was mad . . . but it was incredible."

A fully restored version of the film hit theaters for one day on July 4. A DVD/Blu-ray release is slated for July 21.

For comments and feedback contact: editorial@rttnews.com

Entertainment News