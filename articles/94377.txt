Here's what the critics had to say through the years about Mickey Rooney, who died Sunday in Los Angeles at age 93:

---

In each picture he handled a different characterization - a gangster's son, a swell-headed jockey, and the adolescent son of Judge Hardy - and brought them off with the forthright expertness of a sophomoric Spencer Tracy. - Ed Sullivan, show biz column, 1938.

---

He is convinced there is no better actor in the world than Mickey Rooney. And no matter what scenario writers put in the scripts, Mickey knows he can play it. Strange part of it is, he never has been stumped. - Associated Press, 1938.

---

Another factor in his success has been his consistently "all-out" performance in whatever role he has played. Hollywood jargon describes his talent as showmanship, made up about equally of huge vitality ("he tears himself apart in anything he does") and timing ("everything clicks in perfect.") Both equalities show in his "straight" acting, his singing, dancing, drumming and even in his notable mimicry. - New York Times, 1941.

---

Judy Garland and Mickey Rooney were an unbeatable combination: they could do just about anything, and do it well - sing, dance, play instruments, act, mime. - "Gotta Sing Gotta Dance: A Pictorial History of Film Musicals," by John Kobal, 1970.

---

His puck in "A Midsummer Night's Dream" ... is a masterpiece amid the weirdly isolated evocation of the fairy spirit. Rooney seems inhuman, he moves like mist or water, his body is burnished by the extraordinary light, and his gurgling laugh is ghostly and enchanting. - "A Biographical Dictionary of Film," by David Thompson, 1975.

---

The presence of Mickey Rooney, who plays the trainer, is welcome but perhaps too familiar. Rooney has played this sort of role so often before (most unforgettably in 'National Velvet') that he almost seems to be visiting from another movie. His Academy Award nomination for the performance is probably a recognition of that. - Roger Ebert, review of "The Black Stallion," 1980.