Health ministers from 11 west African nations and experts in disease control are meeting in Ghana's capital to plan "drastic action" to fight the deadliest Ebola outbreak in history.

The number of deaths attributed to an epidemic of Ebola virus in Guinea, Liberia and Sierra Leone stood at 467 by Monday, out of 759 known cases in total, the World Health Organization said yesterday.

The outbreak of the deadly disease is already the largest and deadliest ever, according to the WHO, which previously put the death toll at 399 as of 23 June, out of 635 cases.

The 17% rise in deaths and 20% jump in cases in the space of a week will add urgency to the emergency meeting, which aims to co-ordinate a regional response.

In response to the outbreak, Liberian authorities yesterday warned that anyone caught hiding suspected Ebola patients will be prosecuted.

Some families, faith healers and traditional doctors were reported to be removing patients from hospital for special prayers and traditional medicine.

Liberian President Ellen Johnson Sirleaf said in a statement that the crisis has become a national public health emergency,urging people to heed health guidelines.

The outbreak in West Africa has left some of the world's poorest states, with porous borders and weak health systems undermined by war and misrule, grappling with one of the most lethal and contagious diseases on the planet.

The WHO said three key factors were contributing to the spread of the disease.

One was the burial of victims in accordance with cultural practices and traditional beliefs in rural communities.

Another was the dense population around the capital cities of Guinea and Liberia. The third was commercial and social activity along the borders of the three countries.

"Containment of this outbreak requires a strong response in the countries and especially along their shared border areas," the statement said.

The WHO figures include confirmed, probable and suspected cases.