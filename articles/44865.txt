Google Glass Explorers have a reputation for being, well, glassholes. The stereotype has proliferated enough that Google last week posted The Top 10 Google Glass Myths, which defended its early adopters and argued that new technology has always had its detractors.

For example, "did you know that people used to think that traveling too quickly on a train would damage the human body?" Now you do. Thanks, Google.

The search giant seems to have realized that it needs a different approach to get real people, not just the tech-savvy, to wear Glass. So yesterday, it announced a partnership with eyewear manufacturer Luxottica Group, which brings a new level of style to Google's high-tech specs. Through this collaboration, the eyewear maker will provide a team of experts to work on future Glass products, specifically developed under its Ray-Ban and Oakley brands.

As the movies tell us, Ray-Bans are the arbiter of cool. They've graced the faces of everyone from Audrey Hepburn to Jeff Bridges. But what if their movie counterparts had wanted to upgrade their shades to Google Glass? Hit the slideshow to check out how Jordan Belfort, Jeff Lebowski, and more would have lobbied Google to become an Explorer via its #ifihadglass campaign.

Advertisement

1. Jordan Belfort @wolfofwallstreet

#ifihadglass I would never wear the f**kin' thing. My house was bugged, I had to wear a wire. Without me this thing is a worthless hunk of plastic. Like a loaded M-16 without a trained Marine to pull the trigger.

2. James Darrell Edwards III @AgentJ

#ifihadglass I could flashy-thing people without worrying that I'll erase my own memory since I'd have backup.

3. Jeff Lebowski @HisDudeness

#ifihadglass I could keep better track of things, man. Money, Walter's undies, rugs. And it might help with these occasional acid flashbacks I have.

4. Brian Johnson @TheBrain

#ifihadglass I'd tell Mr. Vernon that we accept the fact that we had to sacrifice a whole Saturday in detention for whatever we did wrong. But we think he's crazy to make us write an essay telling him who we think we are when we could just show him with these things.

5. Susan Thomas @therealsusan

#ifihadglass I'd give them to my friend Roberta. But first I'd use them to find Jim in Seattle, Mexico City, New York... Maybe I wouldn't wear them in New Jersey, though.

6. Jake Blues @JolietJake

#ifihadglass my ex-fiancée could see it all: me running out of gas, getting a flat tire, having no money for cab fare, my tux not coming back from the cleaners, an old friend coming in from out of town, my car getting stolen, the earthquake, the flood, the locusts.

7. Holly Golightly @meanreds

#ifihadglass I'd have to get Jose to pay for them, I guess. You could always tell what kind of a person a man thinks you are by the glasses he gives you. I must say, the mind reels.

Further Reading

Wearable Reviews

Wearable Best Picks