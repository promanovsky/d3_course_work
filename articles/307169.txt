In addition to Test Drive and Music Freedom - Un-carrier numbers 5.0 and 6.0, T-Mobile and Rhapsody have created a brand, spankin' new internet radio service as part of the 6.0: Rhapsody unRadio.

If you're already a customer with 4G LTE through T-Mobile's Simple Choice Unlimited, you'll get the unRadio for free. If not but you're still on T-Mo, it'll cost you $4 per month. Prepaid customers will not be able to take advantage of T-Mobile's special pricing

Non-T-Mobile customers can still subscribe to the music service for $5 a month.

Once paid for, you'll get unlimited data to stream the music. Other features include unlimited skips, replaying favorite songs and no commercials - plus the library boasts 20 million songs.

Rhapsody unRadio will be available for both Android and iOS, though not Windows Phone or BlackBerry users. Whether you're on T-Mo or not, the music service will be available starting June 22.

Sounds alright so far

With so many restrictions on current internet radios, T-Mo and Rhapsody may have stumbled on a goldmine.

Even if you're not on magenta carrier, five bucks a month is certainly much cheaper than other services out there. The promises attached to the new unRadio also sound pretty good.

The appeal of free streaming is also quite a draw since that's a huge concern for people who listen to music on their LTE mobile devices.

So far it pretty much resembles Pandora, Spotify and other popular services in terms of features - where in addition to the ones mentioned above, you can also thumbs up or thumbs down, so the algorithm learns your preferences; there's also TrackMatch which is like SoundHound or Shazam, where you can identify random songs when you're out and about plus create radio stations from the identifications.

With T-Mo seemingly on the rise in terms of popularity and all around generally positive feedback, it will certainly be interesting to see how well the unRadio does when it goes live next week.