CLARION, Mexico, May 20 (UPI) -- The 80-year game of hide-and-seek between the Clarion nightsnake and herpetologists is over, thanks to researcher Daniel Mulcahy, who rediscovered the nocturnal reptile.

The Clarion nightsnake (Hypsiglena ochrorhyncha unaocularus), brownish black and some 18-inches-long at adulthood, was first discovered by naturalist William Beebe in 1936. But he collected only a single specimen, and when scientists were unable to find additional evidence of the species, Beebe's discovery was negated. The nightsnake wasn't declared extinct, because technically, it didn't exist.

Advertisement

But Beebe is now back in the record books and the Clarion nightsnake is an official species -- all as a result of the collaboration between Mulcahy, of the National Museum of Natural History, and Juan Martínez-Gómez, of the Instituto de Ecología in Xalapa, Mexico.

The two scientists and a team of researchers retraced Beebe's steps, using his journals, back to Clarion, the volcanic island off the coast of Western Mexico.

RELATED Bombers add former NFL running back Kevin Smith

"The rediscovery of the Clarion nightsnake is an incredible story of how scientists rely on historical data and museum collections to solve modern-day mysteries about biodiversity in the world we live in," Mulcahy said in a statement released this week.

Mulcahy and Martínez-Gómez collected 11 specimens of the nightsnake, and through DNA analysis confirmed the species' distinction from similar snakes on Mexico's mainland.

The Clarion nightsnake hunts -- as one might expect -- at night on the black lava rock habitat near the waters of Clarion's Sulphur Bay. Lizards are its main source of food. And while the population appears to be stable, growing populations of feral cats could threaten the nightsnake's and the island's lizard supply.

RELATED Marine falls from Osprey aircraft during training flight

The expedition of Mulcahy and Martínez-Gómez is detailed in the latest issue of the journal PLOS ONE.

RELATED DUI suspect accused of crashing into police car and not even realizing it