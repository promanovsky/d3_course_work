We've previously reported on New Matter, a company making an ultra-affordable 3-D printer called the MOD-t that's currently raising money on Indiegogo. At the time of this writing, supporters have pledged some $544,000, meaning New Matter has raised 145% of its $375,000 target sum with 15 days of fundraising still left.

Advertisement

We call the MOD-t "ultra-affordable" because its $249 price tag stands in stark contrast to pretty much all other 3-D printers out there (early Indiegogo backers scooped up a limited number of MOD-t devices at $149!). Consider the devices by 3-D-printing hardware company MakerBot, which could easily be considered "the Apple of 3-D printing." They start at $1,375 for the Replicator Mini. The Replicator Z18, MakerBot's top of the line model, is $6,500.So how can New Matter make its printer available for so cheap?

"In order to create the low-cost hardware, we invented a new mechanism that drives two of the three axes of the machine," Steve Schell told Business Insider. He's cofounder, president, and CTO of New Matter.

Advertisement

"Pinion rods drive the build plate, and this unique mechanism has cost advantage because it makes for fewer overall parts. We've designed this for high-volume manufacturing and prepared to invest in tooling and injection molding to drive cost down."

In simpler terms, the New Matter team has boiled down a 3-D printer so that it operates just as effectively with many fewer parts. One of the biggest savers here is in the pinion system that drives the build platform. It moves forward, back, left, and right, while the print head only has to move up and down.

Advertisement

The video below will give you a closer look at this reduced part-count design. New Matter engineer Derek Schulte explains the system that moves the build platform: