Taylor Swift bundles up while arriving at the Baryshnikov Arts Center on Monday morning (March 24) in New York City.

Over the weekend, the 24-year-old country singer paid a special visit to the children’s wing at the Memorial Sloan-Kettering Cancer Center in the Big Apple.

Taylor was reportedly only scheduled to stay at the hospital for one hour, but she stayed for more than four and a half hours instead. So awesome of her!

Watch a video below of Taylor meeting with Shelby Huff, who is currently receiving a life saving clinical trial at the cancer center.

FYI: Taylor is carrying a Dolce&Gabbana bag.



Tayor Swift Visits Shelby Huff at Memorial Sloan Kettering Cancer Center

10+ pictures inside of Taylor Swift out and about…