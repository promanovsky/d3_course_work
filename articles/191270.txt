Brooke Birmingham was so proud of losing 172 pounds that she wanted to share her story, and her amazing “after” picture, with the world.

The editorial staff at Shape magazine got in contact with Brooke about sharing her “Success Story,” but when she sent a picture showing off the amazing transformation the magazine decided to pass.

The reason? Brooke Birmingham says it’s because she sent a picture of herself in a bikini, which also showed off the loose skin around her stomach that was the inevitable result of losing the equivalent of an entire person.

Brooke said she talked to a reporter from the magazine for a half hour about her journey, her weight loss strategy, and the effects of her life transformation. But after about a week, she got an email from an editor asking that she send a new picture, this one with a shirt on.

The editors claimed it was policy that with some stories the subject be shown wearing a shirt. After a few email exchanges, Birmingham decided that if she couldn’t show off her body as the other women on the site did, she would pass entirely.

“The whole thing still really frustrates me because I don’t feel like my body was given the same respect as others on their site,” Brooke wrote in a column for The Huffington Post. “Why all of the sudden is it their policy to have fully clothed people? The reporter stated that she wasn’t sure if someone had complained about the previous photos to Shape or not. But in my eyes if someone is complaining about them featuring women in bikinis, then again they shouldn’t have them anywhere on the site.”

Brooke added that her’s was the kind of picture that should be featured on the site.

“My body is real, not photoshopped or hidden, because I feel like I should be ashamed,” she wrote. “This is a body after losing 172 pounds, a body that has done amazing things, and looks AMAZING in a freaking bikini.”

Though she may have been rejected by Shape magazine, Brooke Birmingham may have found an even bigger outlet to show off her 172-pound weight loss. Her story has been picked up by a number of news sites and blogs, and was a featured story on Yahoo News.