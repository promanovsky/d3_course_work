Apple has hired another luxury brand exec away from an established brand. Patrick Pruniaux, VP of sales for watch company Tag Heuer, will join Apple on Monday after seven years in the world of luxury timepieces. Pruniaux, second from the right in the above photograph, joins the company just as talk of an iWatch has risen to a dull roar.

Tag Heur is best known for its Formula One-themed watches and spokesperson Tiger Woods.

Reuters quotes watch analysts as saying the iWatch won’t make a dent in the luxury industry but I wouldn’t be so sure. Watch companies work in an odd market where demand for mid-level pieces is very fickle. A unique, desirable smart watch that works with iOS and is considered by some as a status symbol or a must-have accessory could tip the balance away from a quartz or mechanical piece. Most Swiss watchmakers have stumbled while trying to grab a piece of the smart watch market, most notably Swatch’s Paparazzi watch that used radio signals to push news to your wrist.

Pruniaux isn’t the first luxury hire for Tim Cook’s Apple. The company nabbed Burberry ex-Chief Executive Angela Ahrendts in May and head of Yves Saint Laurent’s head Paul Deneve.

Perhaps the bells are tolling for the arrival of an iWatch? Only Pruniaux may know for sure. All we do know is that Apple is tripling down on luxury.

Image via ishida-watch.com