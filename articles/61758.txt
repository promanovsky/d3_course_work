Mark Zuckerberg has made two shocking acquisitions this year.

He paid $19 billion for WhatsApp, and $2 billion for Oculus VR. Prior to buying these companies, the biggest Facebook acquisition was Instagram for $1 billion.

Advertisement

With these moves, Facebook is evolving from a single-purpose social network to a corporate conglomerate.

The logic for Facebook's expansion is fairly simple. Smartphones have disrupted Facebook's control of social networking . Therefore it's buying potential rivals like WhatsApp, Instagram, and Snapchat are smart defensive moves.

Advertisement

Oculus is a giant bet on the future. Since the majority of Oculus acquisition is funded through stock, it's not the end of the world financially for Facebook if it's a bad bet.

Advertisement

These acquisitions demonstrate a major evolution Zuckerberg's thinking about Facebook. They also reflect a shift in technology trends. In 2011, Zuckerberg and Facebook COO Sheryl Sandberg were asked by Charlie Rose if he would ever make video games. They responded by saying no.

Rose said, ok, you say that now, but what about in the future? After all, there was a time when people thought Steve Jobs would never do retail, but then he did.

Zuckerberg's answer: "No, I'm pretty sure we're not going to build any games. I'll tell you why. Because building games is really hard."

This is particularly ironic now that Facebook owns Oculus VR, a company that makes video games.

Advertisement

But there's more from Zuckerberg:

"And we're doing - what we're doing is really hard. And we think that we're better off focusing on this piece. I think that building a great game service is really hard. Building a great music service is really hard. Building a great movie service is really hard. And we just believe that an independent entrepreneur will always beat a division of a big company which is why we think that the strategy of these other companies trying to doeverything themselves will inevitably be less successful than an ecosystem where youhave someone like Facebook trying to build the core product to help people connect andthen independent great companies that are only focused on one or two things doing thosethings really well."

Sheryl Sandberg chimed in by saying: "

And those companies can't - don't have the di scipline to do it, right. They get big, and

everyone wants to do everything, and they just say yes. And then they don't do

everything well."

Advertisement

Zuckerberg was correct about small companies beating a big company. Snapchat beat Facebook's "Poke" app. WhatsApp beat Facebook's Messenger app. Instagram beat Facebook's Camera app.

But the success of those applications forced Zuckerberg to diversify his company, risking that he will lose the focus he spoke about with Rose.

In the narrowest sense, Zuckerberg hasn't abandoned his ideology. He's letting Instagram, WhatsApp, and Oculus operate as independent companies, as opposed to integrated divisions of Facebook.

Advertisement

At the same time, though, he's trying to connect the world, which is not a core strength for Facebook. Zuckerberg is also leading a political action group, FWD.us.

So, in a broader sense, Zuckerberg is taking on a bunch of new companies, and new ideas, which will spread him thinner as he manages a portfolio of properties.

It's a fascinating evolution of not just Zuckerberg, but technology broadly. In 2011, the iPhone was still the hottest product in the world. The iPad had just launched. Steve Jobs and Apple were the Kings of technology. Jobs' whole mantra was "focus."

Advertisement

Other tech companies started to follow suit. Google shut down a bunch of products, reorganized the company, and generally became more disciplined in its approach.

But today, Apple is the only major company that remains committed to focus.

Google bought Motorola, then shut it down. Then it bought Nest. It's doing self-driving cars, Google Glass, Chromebooks, Internet balloons, and all sorts of projects.

Advertisement

Amazon, which is primarily an e-commerce website, is exploring drones, making TV shows, selling tablets, and may release its own phone.

For the most part, investors have rewarded this behavior, so few people are questioning this strategy.

But, there is a risk. These companies could turn into Microsoft, which went from being a dominant monopoly to a company trying everything but not really excelling at any thing. (Look at its early tablets, the Zune, or Bing.)

Advertisement

Here's the Zuckerberg video. He starts talking about all of this at 28:23. A transcript of his comments is here

Advertisement

(Via Fimoculous