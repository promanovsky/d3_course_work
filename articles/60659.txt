Dollar turns mostly higher vs. rivals after personal spending data

Investing.com - The dollar was mostly higher against the other major currencies on Friday, after the release of positive U.S. personal spending data and as markets eyed a report on U.S. consumer sentiment, amid fresh optimism over the outlook for the nation's economy.

The dollar was steady against the euro, with down 0.06% to 1.3734.

Official data showed that U.S. personal spending rose 0.3% in February, in line with expectations, Personal spending in January was revised down to a 0.2% gain from a previously estimated 0.4% increase.

A separate report showed that the core U.S. personal consumption expenditures price index remained unchanged at 0.1% last month, in line with expectations.



The data came after economic reports on Thursday showed that U.S. jobless claims fell to the lowest level since late November last week and that U.S. economic fourth quarter growth was revised higher.

The upbeat reports added to hopes that the slowdown in U.S. economic activity seen at the start of the year would be temporary.

In the euro zone, official data showed that French consumer spending rose 0.1% in February, less than the expected 0.8% increase, after a 2.1% decline the previous month.

The pound was higher against the dollar, with adding 0.13% to 1.6632.



Data showed that the U.K. gross domestic product rose by 0.7% in the fourth quarter, in line with expectations.

A separate report showed that the U.K. current account deficit narrowed to £22.4 billion in the fourth quarter, from £22.8 billion in third quarter, whose figure was revised down from a previously estimated deficit of £20.7 billion.

Analysts had expected the current account deficit to narrow to £14 billion in the fourth quarter.

The dollar was higher against the yen, with rising 0.20% to 102.37 and little changed against the Swiss franc, with up 0.06% to 0.8871.

Government data earlier showed that retail sales in Japan rose at an annaualized rate of 3.6% in February, beating expectations for a 3.2% increase, after a 4.4% gain the previous month.

A separate report showed that Tokyo's consumer price inflation rose 1.3% this month from a year earlier, exceeding expectations for a 1.2% gain, after a 1.1% increase in February.

Core consumer price inflation, which excludes fresh food, rose 1% this month from a year earlier, exceeding expectations for a 0.9% advance, after a 0.9% increase in February.

The reports came after data showed that household spending in Japan declined at an annualized rate of 2.5% last month, compared to expectations for a 0.1% uptick, after a 1.1% rise in January.

The greenback was steady to higher against the Australian, New Zealand and Canadian dollars, with edging down 0.13% to 0.9246, inching up 0.02% to 0.8674 and dipping 0.02% to 1.1030.

The , which tracks the performance of the greenback versus a basket of six other major currencies, was up 0.07% to 80.34.

Later in the day, the U.S. was to release revised data on consumer sentiment.