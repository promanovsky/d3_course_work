The Heartbleed cybersecurity problem began on New Year's Eve 2011, about an hour before the stroke of midnight on a Saturday, when a British computer consultant in Staffordshire posted a new version of a popular free cryptographic system, OpenSSL.

The British consultant, Stephen Henson, had reviewed and approved the update, which had been written by Robin Seggelmann, a German graduate student at the University of Muenster.

Neither realized that the OpenSSL version 1.0.1 they released that night, two years ago, had a flaw that would only be detected this week, causing much commotion around the world because it could allow hackers to access a server's security-related data.

Story continues below advertisement

The problem has snowballed, affecting major portals like Yahoo, Netflix and Flickr, forcing the federal government in Canada to shut down public websites and now raising concerns that it also affects network routers and firewalls.

It has also underlined how much the Internet relies on open-source software endeavours, such the OpenSSL project, a small band of volunteer computer scientists who run a shoestring operation to maintain one of the Internet's key infrastructure.

The global scare began with the unpaid contribution to the OpenSSL project from Mr. Seggelmann, who is now a 31-year-old programmer for Deutsche Telekom AG.

For his efforts, he is now facing online speculations that he purposely wrote faulty code to help an intelligence eavesdropping agency like the U.S. National Security Agency.

Contacted by e-mail, Mr. Seggelmann declined a request for an interview but referred to his current employer.

In a statement, Deutsche Telekom, said one of its employees is now the victim of "absurd conspiracy theories" because he wrote the software that caused the Heartbleed problem.

The flaw turned "a simple mistake into one with massive consequences," Mr. Seggelmann said in the communiqué.

Story continues below advertisement

Since he specialized in communications and encryption, he had contribued to the OpenSSL project several times while he was a research assistant in electrical engineering and computer science at the University of Muenster.

"I was working on a research project at the University of Münster using the OpenSSL encryption library and releasing bug fixes and new features that were developed as part of my work on the OpenSSL project," he said. "The various changes were checked by a member of the OpenSSL development team and then incorporated into the official code."

The person who reviewed Mr. Seggelmann's input was Dr. Henson, the only member of the OpenSSL core team who works full-time on the project.

Bob Tennent, a professor at the School of Computing of Queen's University, said it is unlikely that the problem was maliciously inserted because it was in plain sight and is a common flaw in software written with this programming language.

"This particular bug is the sort of thing that unfortunately occurs often in C code," Dr. Tennent said in an interview.

Nevertheless, Bloomberg News reported Friday that the NSA, which devotes millions of dollars to detect security flaws, had been aware of the Heartbleed vulnerability for at least two years and used it to gather intelligence without notifying ordinary users.

Story continues below advertisement

The update Mr. Seggelmann wrote dealt with the heartbeat extension, a protocol that maintains a secure connection between a remote user and the web server. This is done by bouncing back and forth little chunks of data, "heartbeats," to ensure that the connection stays alive.

Mr. Seggelmann failed to set a limit on the amount of data being sent back to the user, meaning that additional information could trickle out of the computer's processing memory.

A persistent hacker could keep sending heartbeat requests and collect the resulting data leak until the computer spat out a password or an encryption key.

"I failed to check that one particular variable, a unit of length, contained a realistic value. This is what caused the bug," Mr. Seggelmann said.

"Unfortunately, the OpenSSL developer who reviewed the code also did not notice that a mistake had been made when carrying out the check. As a result, the faulty code was incorporated into the development version, which was later officially released."

Open-source software is widely used not just because it is free but also because the original programming text, before it has been compiled into a series of inscrutable computer commands, are available for public scrutiny so that security flaws can be detected, Dr. Tennent said.

Story continues below advertisement

The Heartbleed problem was discovered jointly by a programmer at Google Security and a Finnish firm Codenomicon. The fix was also written by volunteers.

The OpenSSL project began about 15 years ago.

The project is currently managed by a core group of four people, joined by another seven programmers who are active contributors. The volunteers are mostly from Britain and Germany. One is from Canada.

With members scattered over several countries and time zones, OpenSSL is a virtual operation, with many contributors never having met face-to-face.

The project usually gets about $2,000 a year in donations, said Steve Marquess, who manages the OpenSSL Software Foundation.

The foundation helps finance the OpenSSL project by contracting out the work of the volunteer programmers. Last year, that labour earned the project more than $1-million in gross revenue.

Story continues below advertisement

Other widely-used programs, such as the Linux operating system, the Apache web server application or the Mozilla Firefox web browser, are also open source software but are better funded, Mr. Marquess said.

"OpenSSL is an under-the-hood component. Most people are never aware that it's there … that led to it being taken for granted," he said.

Because of the lack of resources, there is a backlog of hundreds of code submissions from programmers to the OpenSSL project that still haven't been properly analyzed, Mr. Marquess said.

The sudden spotlight has helped, however. Since the bug became public this week, OpenSSL has received $3,000 in donations, more than it used to receive yearly.