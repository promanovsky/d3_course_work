Miley Cyrus obtains temporary restraining order against man who 'believes the singer is communicating with him through her songs'



She has skyrocketed to fame through her raunchy stage shows, parading around in barely-there ensembles and racy lyrics.

But Miley Cyrus has been on the receiving end of some rather unwanted attention recently.

The 21-year-old Wreckingball singer obtained a temporary restraining order Friday against an Arizona man who was recently detained by police while trying to meet the singer-actress.

A Los Angeles judge granted Cyrus the stay-away order against Devon Meek after Cyrus' lawyers filed court documents stating that Meek, 24, believes the singer is communicating with him through her songs.

Scroll down for video



Documents filed: Miley Cyrus, 21, obtained a temporary restraining order Friday against an Arizona man who was recently detained by police while trying to meet the singer-actress



According to documents obtained by MailOnline: 'Meek, who we are informed and believe suffers from delusions about Ms. Cyrus, believes that the singer/actress/celebrity Ms. Cyrus communicates with him through her songs and is telling him to come to see her so that they may be together.'

Los Angeles police arrested the man from the southeastern Arizona city of Sierra Vista on May 16, and he was placed in a psychiatric hospital, where he remains. Meek has said he will continue to try to meet Cyrus once he is released, according to filings by Cyrus' attorneys and a police detective.

Meek was arrested outside a property he believed is owned by Cyrus, but the documents don't specify the location.

Restraining order: MailOnline obtained documents of the stay-away order granted by a judge against Devon Meek

Subliminal: According to the document Meek 'believes that the singer/actress/celebrity Ms. Cyrus communicates with him through her songs and is telling him to come to see her so that they may be together'



Stay-away: As a result, Meek must stay at least 100 yards away from the home, job or workplace, and vehicle of the Party In The USA hitmaker



Luckily the former Disney star has yet to be approached by Meek but 'she is in fear for her safety because of Meek's mental state and apparent obsession with Cyrus.'

According to the court documents that Meek must stay at least 100 yards away from the home, job or workplace, and vehicle of the Party In The USA hitmaker.

Detective Rosibel Smith wrote in a sworn statement that Meek told officers they should shoot him in the head if he couldn't meet Cyrus.

Smith wrote that a police officer who interviewed Meek 'informed me that Mr. Meek stated that when he is released from the hospital, he will not stop seeking Ms. Cyrus and that he will continue to go to Ms. Cyrus' residence until Ms. Cyrus accepts him or he dies.'

Protected: Luckily the singer/actress has yet to be approached by Meek, who was arrested outside a property he believed is owned by Cyrus



A hearing to extend the restraining order for three years is scheduled for June 16.

There is no declaration from Cyrus in the filing. The pop star rose to fame as the star of the TV show Hannah Montana and has recently postponed several concerts while she recovers from what her representatives have said is an allergic reaction to antibiotics.

The We Can't Stop singer is currently in France as part of her Bangerz tour and will head out to Cologne, Germany for a concert at the Lanxess Arena on Monday.