The Notebook is one of the most beloved romantic films of all time and when it was released, stars Ryan Gosling and Rachel McAdams were dating in real life.

The adored nature of the movie and the seeming perfection of Gosling and McAdams' relationship makes the recent revelations made by The Notebook director Nick Cassavetes all the more explosive:

"Maybe I'm not supposed to tell this story but they were really not getting along on set," said Cassavetes in a recent interview.

"Really not. Ryan came to me...and he says, 'Would you take [McAdams] out of here and bring in another actress to read with me?"

Naturally, Cassavetes refused out of respect for McAdams. Still, things only got worse from there:

"I went into a room with a producer and they started screaming and yelling at each other. I walked out."

"They had it out...I think Ryan respected her for standing up for her character. The rest of the film wasn't smooth sailing, but it was smoother sailing."

It may not seem like a big deal that a now-broken-up couple fought while working together ten years ago, but The Notebook is a rare beast of a movie.

The film's fans are so cultishly devoted that video of Rachel McAdams auditioning for The Notebook became an Internet sensation just last month.

Will knowledge that the cute couple that met while making the movie were actually at each other's throats from the start tarnish the film's legacy?

Ah, who are we kidding? If anything, the fact that Gosling and McAdams fought before falling in love (Just like the Noah and Allie!) will only make Notebook obsessives swoon even harder.