It almost feels like people are playing a game of oneupmanship when it comes to 3D printing: You're 3D printing a building? Well, how about 3D printing a building on the moon. Yeah, well, this guy's 3D printing Coney Island's Lunar Park in its entirety. But experimental tech demands experimental and ambitious ideas, right?

The latest addition to this ever-expanding list of insane concepts is called Sugarbabe and involves 3D printing, a little bio art, and, oh yeah, Vincent van Gogh's severed ear. Using cells from van Gogh's distant relative Lieuwe van Gogh, the ear has been regrown by Dutch artist Diemut Strebe who uses "science basically like a type of brush."

After growing enough cells from the material obtained from the great-great-grandson of Vincent's brother Theo, they were shaped to replicate van Gogh's detached organ using a 3D printer and the resulting "ear" is being kept alive using a nutrient solution. Lieuwe shares around a sixteenth of the same genes as the Impressionist master, which includes the Y-chromosome passed down through the male lineage.

And in a surreal twist that brings meta new meanings to the phrase 'fall on deaf (death?) ears', visitors at the The Center for Art and Media in Karlsruhe, Germany—where the ear is on display—can speak into the reformed appendage using a microphone.

"You can talk to the ear. The input sound is processed by a computer using software that converts it to simulate nerve impulses in real time. The speaker remains in soliloquy. The crackling sound that is produced is used to outline absence instead of presence." explains the museum.

The piece is on display until July 6th and will be coming to New York in spring 2015.