A pregnant Fresno, Calif., woman who has spent months in a coma, gave birth to a baby boy Thursday morning, the ABC affiliate KFSN reports.

Melissa Carleton, 39, suffered a seizure in March which left her in a partial coma. She has been in the hospital and unable to speak since she was 26 weeks pregnant, ABC News reports.

Carleton’s frequent headaches landed her in the emergency room a few times early on in her pregnancy before she was diagnosed with a brain tumor, according to the couple’s GoFundMe page, which has been created to help pay the family’s medical and personal expenses. Carleton began experiencing seizures as the tumor grew larger.

Advertisement

Carelton’s baby, West Nathaniel Lande, was born Thursday morning by C-section at the University of San Francisco Medical Center, weighing 5 pounds and 9 ounces. Carleton’s husband, Brian Lande, told KFSN that the couple had wanted a baby for a long time, so he tried to make the birth “memorable’’ even though Carleton was unable to consciously experience it.

“He went right to Melissa’s chest and was able to lay on her chest. She was obviously still under anesthesia. But he got to smell her first and feel her first, and then he came to me,’’ Lande told KFSN.

Lande told the local news station that before the delivery, Carleton moved her hand to her stomach, indicating the baby was kicking. She also reached her hand up to touch Lande’s face.

Since delivering, Lande said that Carleton has been able to breastfeed her son, which was something she expressed prior to the stroke she had wanted to do.