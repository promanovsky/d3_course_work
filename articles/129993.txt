The Christian Holy Week begins every year on Palm Sunday and culminates with Easter the following Sunday. It is a time of worship and reflection on Jesus' journey into Jerusalem, through the Last Supper, his crucifixion and his ultimate resurrection as portrayed in the Christian gospels.

Observances for Palm Sunday, Maundy Thursday, Good Friday and Easter follow certain customs but can vary by region and culture.

Here is a look at some of the diverse 2014 Holy Week observances around the world:



Pope Francis asperges holy water as he celebrates a Palm Sunday mass in St. Peter's Square, at the Vatican, Sunday, April 13, 2014. (AP Photo/Gregorio Borgia)



An Indian Christian girl holds a cross made of blessed palm during a Palm Sunday mass at a church in Bhubaneswar, India, Sunday, April 13, 2014. (AP Photo/Biswaranjan Rout)



An Ethiopian Orthodox Christian priest holds a holy book for women to kiss at Deir El Sultan in the Church of the Holy Sepulcher, traditionally believed by many to be the site of the crucifixion and burial of Jesus Christ, during Palm Sunday services in Jerusalem's Old city, Sunday, April 13, 2014. (AP Photo/Ariel Schalit)



Franciscan Friars take part in the traditional Palm Sunday procession from Mt. Olives to Jerusalem's Old City on April 13, 2014. (Gali Tibbon/AFP/Getty Images)



Egyptians celebrate Palm Sunday during a service in the Samaan el-Kharaz Church in the Mokattam district of Cairo, Egypt, Sunday, April 13, 2014. (AP Photo/Roger Anis, El Shorouk)



Penitents take part in the "Procesion del Silencio" by the "Cristo de las Injurias" brotherhood, during the Holy Week in Zamora, Spain, Wednesday, April 16, 2014. (AP Photo/Andres Kudacki)



A Christian pilgrim kisses a sacred column at the entrance to the Church of the Holy Sepulchre in Jerusalem's Old City on April 17, 2014. (Gali Tibbon/AFP/Getty Images)



Hooded Filipino penitents flagellate during Maundy Thursday rituals to atone for sins on April 17, 2014, in suburban Mandaluyong, east of Manila, Philippines. (AP Photo/Aaron Favila)



Penitents from 'Cristo de la Buena Muerte' or 'Good Dead Christ' brotherhood take part in a procession in Zamora, Spain, on the early hours of Tuesday, April 15, 2014. (AP Photo/Andres Kudacki)



A "Costalero" from "La Sangre" brotherhood takes part during a Holy Week procession in Cordoba, southern Spain, Tuesday, April 15, 2014. (AP Photo/Manu Fernandez)



Pope Francis greets the faithful as he arrives at the Don Gnocchi Centre for the elderly and disabled for the Maundy Thursday Mass on April 17, 2014 in Rome, Italy. (Photo by Franco Origlia/Getty Images)



Pope Francis washes the foot of a man at the Don Gnocchi Foundation Center in Rome, Thursday, April 17, 2014. (AP Photo/L'Osservatore Romano)



Pope Francis washes the foot of a woman at the Don Gnocchi Foundation Center in Rome, Thursday, April 17, 2014. (AP Photo/L'Osservatore Romano)



Pope Francis kisses the foot of a person as he performs the traditional washing of the feet during a visit at a center for disabled people as part of Maundy Thursday as part of the Holy Week on April 17, 2014 in Rome. (Alberto Pizzoli/AFP/Getty Images)



A Catholic devotee nailed to a cross is hoisted by participants during a reenacment of the crucifixion of Christ on Good Friday on April 18, 2014 in San Pedro Cutud village in Pampanga province, Philippines. (Photo by Dondi Tawatao/Getty Images)



Christian Orthodox pilgrims carry wooden crosses along the Via Dolorosa (Way of Suffering) during a procession marking Good Friday on April 18, 2014 in Jerusalem's old city. (GALI TIBBON/AFP/Getty Images)



Faithful wait for the start of the Via Crucis (Way of the Cross) torchlight procession to be celebrated by Pope Francis in front of the Colosseum on Good Friday in Rome, Friday, April 18, 2014. (AP Photo/Alessandra Tarantino)



Pope Francis lays prostrate on the floor in prayer before presiding over a Good Friday Passion service, in St. Peter's Basilica, at the Vatican, Friday, April 18, 2014. (AP Photo/Stefano Rellandini)



Christian pilgrims hold candles at the church of the Holy Sepulcher, traditionally believed to be the burial site of Jesus Christ, during the ceremony of the Holy Fire in Jerusalem's Old City, Saturday, April 19, 2014. (AP Photo/Dan Balilty)



Cardinals attend the Holy Saturday Easter vigil mass celebrated by Pope Francis at St.Peter's Basilica on April 19, 2014 in Vatican City, Vatican. (Photo by Franco Origlia/Getty Images)



Pope Francis holding a tall, lit, white candle, enters a darkened St. Peter's Basilica to begin the Easter vigil service, at the Vatican, Saturday, April 19, 2014. (AP Photo/Alessandra Tarantino)



Bosnian Croat women poses in traditional dress during celebration of the Easter in the village of Breske, 150 kms north of the Bosnian capital of Sarajevo, on Sunday, April 20, 2014. (AP Photo/Amel Emric)



Pope Francis drives through the crowd after celebrating an Easter Mass in St. Peter's Square, at the the Vatican, Sunday, April 20, 2014. (AP Photo/Alessandra Tarantino)