Woman's in Coma Gives Birth to Healthy Baby Boy: 'Immense Joy' and 'Immense Sorrow,' Says Husband (VIDEO) Woman's in Coma Gives Birth to Healthy Baby Boy: 'Immense Joy' and 'Immense Sorrow,' Says Husband (VIDEO)

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

A woman's coma birth has brought joy to her husband and family in California. Melissa Carleton, a 39-year-old family therapist, has been in a semi-comatose state for months following brain surgery, but doctors still delivered her son by caesarean section Thursday.

West Nathaniel Lande was born weighing 5 pounds, 9 ounces at the University of California, San Francisco Medical Center, making Carleton's husband, Brian Lande, a very happy father. The Santa Cruz County sheriff's deputy said he can't wait one day for his wife to meet their new baby boy.

"I was just so happy to have a healthy baby, healthy son," Lande told NBC news. "It's a feeling of immense relief, joy, and immense sorrow for Melissa not able to be awake for it."

Carleton's medical troubles began earlier this year with painful headaches that started not long after she became pregnant. Months later, doctors discovered that she had brain tumor, and a seizure sent her into a comatose state. Since the surgery, she hasn't been fully awake since March 11.

Still, the new mother has opened her eyes and even held her husband's hand since the birth of the baby, which has given her family hope.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"She was more awake than she has been," Carleton's mother, Lawanna Farrell, told the Fresno Bee. "She reached for Brian and puckered her lips so she could kiss him."

She has also been able to breastfeed with some assistance. Carleton's husband Lande was prepared for the challenges though— he predicted before his son's birth that he would be doing a lot of the parenting and has stuck by his wife's side.

"I know that being able to love this little guy is going to be a wonderful experience, but I also know it's going to be very hard for me to know that I am going to be the one to hold this baby first," Lande previously told ABC 30 of Fresno.

Carleton's recovery is still uncertain. Lande and his family have started a Facebook page and GoFundMe account to ask for support.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit