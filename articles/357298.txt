Dan Kitwood/Getty

The long and winding road for Uber just took another twist and turn. London's transit authority has dropped plans to take the controversial e-hailing app to court -- because the courts are busy with individual Uber drivers.

Uber is an app that allows you to call a taxi or private hire vehicle to your location from your smartphone. When you arrive at your destination, you pay through the app too with no cash changing hands. Operating in more than 100 cities in 37 countries, Uber has proved enormously controversial with cabbies around the world, with many blockading streets in a worldwide protest last month and taxi bodies warning of further disruption.

In London, home of the iconic black cab, taxi drivers are angry that local authority Transport for London (TfL) has allowed Uber to operate. TfL responded by announcing plans to refer its decision to the UK's High Court, which it hoped would end the argument by giving Uber the green light once and for all.

However, TfL today has gone back on that plan, with a new statement revealing that the High Courts cannot proceed because of smaller cases in the magistrates' court against individual drivers. Six such actions have been taken by cabbie trade organisation the London Taxi Drivers' Association (LTDA).

"The LTDA have been forced into taking out these prosecutions on behalf of Londoners," says Steve McNamara, the LTDA's general secretary, "because TfL, the regulator, which has a statutory duty to enforce the Private Hire laws, are refusing to act against Uber."

TfL reckons those cases will probably end up in the High Court anyway.

In the meantime, the transport body maintains it was right to allow Uber to operate. "TfL's position, supported by legal advice," maintains the authority, "is that there are no grounds to take action against Uber London Ltd, Uber BV (the parent company based in Holland), or Uber drivers under s.2 of the 1998 Private Hire Vehicle (Licensing) Act."

"Today is a victory for common sense, technology, innovation -- and above all, London," said Jo Bertram, Uber's General Manager UK & Ireland. "Following another round of scrutiny, Uber has yet again been rubber stamped by TfL as a fully compliant operator. London is a great city, full of creative, innovative people and companies changing the way that people think, live and interact with the world. London is fast becoming the tech capital of Europe. We're incredibly proud to be playing a part in that journey. Uber on, London!"

The meter's running

London cabbies argue that Uber is not licensed properly based on its global corporate structure, and that the smartphone app used by Uber drivers is technically a meter. Only licensed taxicabs are allowed a meter, which charges a fare based on time and distance travelled. The rate for a taximeter is set by TFL, which refutes both of these arguments.

TFL defines a taximeter as "a device for calculating the fare by means of time or distance (or both). However, it is not unlawful for a private hire operator to charge its customers on the basis of time taken and distance travelled in respect of journeys."

See the difference? Me neither. TFL does at least acknowledge that this judgement is "finely balanced," hence the plan to pass the buck to the High Court.

"This fight is on two fronts," says Steve McNamara, "one being fought in the courts and the other against the ineptitude and mismanagement at TfL. We will probably have to take to the streets in a repeat of 11 June."

The full text of TFL's statement is below.