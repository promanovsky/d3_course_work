Scientists from the Massachusetts Institute of Technology and the University of La Laguna in Spain found evidence that Neanderthal diets included both plants and meat.

The researchers gathered data from human excrement discovered at El Salt - an area of Spain where Neanderthals were known to have settled some 50,000 years ago. The researchers then analyzed each sample and found out that most of them contained the digested version of cholesterol from animals. But two of the samples showed a cholesterol-compound typically associated with plants. This analysis was the first direct evidence supporting the theory that Neanderthals might have been omnivores.

"It's important to understand all aspects of why humanity has come to dominate the planet the way it does," said Roger Summons, co-author of the study and a professor of geobiology in MIT's Department of Earth, Atmospheric and Planetary Sciences. "A lot of that has to do with improved nutrition over time."

Ainara Sistiaga, lead researcher of the study and graduate student at the University of La Laguna, worked with her colleagues to excavate the human poop from different soil layers. The analysts then sought the help of Summons for analysis.

In the laboratory, Sistiaga transformed the soil into a powder-like form and integrated mixtures that would extract the organic matter present in the feces. Then, the team looked for markers that led them to identify the organic residue that would confirm whether the excrement belonged to humans.

The team specifically looked for a lipid called coprostanol. This lipid was formed in the intestines as the body absorbed cholesterol.

The feces collected from El Salt contained coprostanol, but two samples showed contents of phytosterol, a biomarker indicating that the plant was digested. But, on a gram-for-gram comparison, the researchers concluded that Neanderthals' diet was mainly meat-based, but also included berries, tubers, and nuts if they were available.

"We believe Neanderthals probably ate what was available in different situations, seasons, and climates," Sistiaga added.

Further findings of this study were published in the June 26 issue of Plos One.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.