London: Lindsay Lohan has confirmed that she will make her West End debut as a sexy temp character Karen in David Mamet`s Speed-the-Plow.

The theatre also tweeted the news today, saying that tickets were on sale now for Speed-The-Plow starring Lohan, suggest reports.

The 27-year-old actress told the New York Times recently that it was the first time she had done a stage play or anything like that and she was nervous but excited.

Lohan also tweeted a snap of her outside a stage door with the caption, "Ready for my close-up!"