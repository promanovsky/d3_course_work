Have you ever tried to lose weight, taking every precaution to cut down on red meat and cheese and bread and really get healthy, and yet still don’t lose a pound? Oftentimes, liquids are the culprit. They’re the most ignored source of calories, mostly because it’s hard to measure out the exact caloric value of every beverage you consume throughout the day.

That’s where the Vessyl comes into play. After seven years of research and development, CEO and co-founder Justin Lee has developed a smart drinking cup, that not only measures the amount of liquid you’ve consumed, but how many calories are in that liquid.

Because the cup is packed full of sensors, which can measure the contents of the cup on a molecular level, the Vessyl can tell the difference between a McFlurry, water, coffee, wine, etc. I even tested out syrup, and it could tell what was going on. Not too shabby at all.

[gallery columns="4" ids="1015522,1015524,1015525,1015528"]

Beyond simple calorie tracking, Vessyl also has a unique feature called Pryme, which measures your hydration and energy levels throughout the day. With an LED screen right on the side of the cup, users can see if they need more water, or are good to chill for a second, from right on the cup.

Alongside Justin Lee, Vessyl was also designed with the help of Yves Behar. You may have heard of him. He helped design the Jawbone Up and the JamBox, and is one of the most iconic designers of our time.

Eventually, Mark One (the company behind the Vessyl) has the opportunity to create an entire line of dishes, bowls, wine glasses, etc. to ensure that the whole family is tracked. For now, however, the team is focused on getting the Vessyl into the hands of consumers.

You can pre-order the device now for $99, or wait and get it after launch for $199.