Scientists said Monday they had used cloning technology to make embryonic stem cells that carry a diabetic woman's genes, and turned them into insulin-producing beta cells that may one day cure her disease.

The team reported clearing an important hurdle in the quest to make "personalised stem cells" for use in disease therapy, but a bioethicist said the breakthrough also highlighted the need for better regulation of lab-grown embryos.

"We are now one step closer to being able to treat diabetic patients with their own insulin-producing cells," said Dieter Egli of the New York Stem Cell Foundation (NYSCF), who led the study published in the journal Nature.

Egli and a team had transplanted the nuclei of cells taken from the woman's skin into human eggs to create stem cells, which they could then coax into becoming beta cells a shortage of which causes insulin deficiency and high blood-sugar in diabetics.

In doing so, the team confirmed a potentially important source for future cell-replacement therapy.

It was not the first study to create stem cells in this way, but it was the first to use cells sourced from a diseased adult person with the aim of producing therapy-specific cells.

Insoo Hyun, a bioethicist from the Case Western Reserve University's school of medicine in Cleveland, Ohio, said the research, the latest to produce embryonic stem cells that carry the genomes of living people, raised red flags.

"This repeated cloning of embryos and generation of stem cells, now using cells collected from adults, increases the likelihood that human embryos will be produced to generate therapy for a specific individual," he wrote in a comment carried by Nature.

"Regulatory structures must be in place to oversee it."

Embryonic stem cells neutral, primitive cells that can develop into most of the specialised tissue cells of the body are viewed as a potential source for rebuilding organs damaged by disease or accident.

But they are controversial, as until fairly recently stem cells could only be obtained from human embryos.

They can be grown in the lab by transferring the nucleus of a cell from tissue like the skin, which contains a person's DNA, into a human egg from which the nucleus has been removed.

The egg is then given an electrical pulse to start dividing until it forms a blastocyst, a hollow, early-stage embryo made up of about 150 cells with the DNA of the donor of the tissue cell.

Cloning technology

Technically known as somatic-cell nuclear transfer (SCNT), the technique is used for therapeutic research but is also the first step in cloning, and was used to create Dolly the sheep.

SCNT is banned in many countries.

For the new study, scientists in the United States and Israel said they made "technical improvements" altering the chemicals used in the culture in which the cells are grown.

The stem cells could in turn be coaxed into becoming various different types of adult cells including beta cells, the team said.

"Seeing today's results gives me hope that we will one day have a cure for this debilitating disease," said NYSCF chief executive Susan Solomon.

The same team had previously made beta cells with a similar method, but using eggs with their nuclei still intact resulting in stem cells with three sets of chromosomes that could not be used in therapy.

But using the new, improved method, the stem cells emerged with the normal two sets of chromosomes, the team wrote.

Hyun said such research could raise fears of a future in which human babies are cloned or embryos callously created and destroyed for research, and called for the strengthening of oversight structures.

But Solomon said the research was "strictly for therapeutic purposes" and adhered to strict ethical oversight.

"Under no circumstances do we or any other responsible scientific group have any intention to use this technique for the generation of humans, nor would it be possible," she told AFP.

The beta cells produced in the study cannot yet be used in replacement therapy, the team said.

Diabetics' immune systems attack beta cells, and ways have yet to be found to shield them.