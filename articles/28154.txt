Is there really a link between vaccine and autism, cellphones and cancer, the HIV virus and the CIA? Almost half of Americans believe the answer is yes for at least one of the many medical conspiracy theories that have circulated in recent years. And the attitudes and behavior of those conspiracists toward standard medical advice reflect that mistrust, says a study out this week.

A pair of University of Chicago social scientists set out to determine the extent of “medical conspiracism” among the U.S. public and conducted a nationally representative online survey. They gauged knowledge of and beliefs about six widely discussed medical conspiracy theories and explored how belief in those theories influenced individuals’ behavior when it came to matters of health.

Their results appeared as a letter published online this week in the journal JAMA Internal Medicine.

Fully 37% of those surveyed endorsed the belief that the Food and Drug Administration, under pressure from pharmaceutical companies, is suppressing natural cures for cancer and other diseases, and 31% said they “neither agree nor disagree” with that idea, the researchers found.

Advertisement

One in five of those surveyed said they agreed that physicians and the government “still want to vaccinate children even though they know these vaccines cause autism and other psychological disorders.” And 36% were on the fence, saying they neither agreed nor disagreed that there may be truth in the much-studied and widely discredited contention that vaccines cause autism.

Similarly, 20% said they believed that cellphones had been found to cause cancer but that the government had bowed to large corporations and would do nothing to address the health hazard. Though 40% disagreed, the remaining 40% withheld judgment on the idea that the government has been silenced about a known link between cellphones and cancer.

Less widely recognized medical conspiracy theories concerned genetically modified foods, HIV and water fluoridation, and they were not without adherents.

Just 12% of respondents said they agreed with a widely discussed theory that genetically modified foods have been widely disseminated by Monsanto Inc. as part of a secret program called Agenda 21, launched by the Ford Foundation and the Rockefeller Foundation to shrink the world’s population. While 42% disagreed, 46% stayed on the fence.

Advertisement

Just over half of Americans rejected outright a widely circulated theory alleging that the CIA deliberately infected African Americans with the HIV virus under the guise of a hepatitis inoculation program. But 12% agreed, and 37% said they neither agreed nor disagreed.

The authors of the letter, J. Eric Oliver and Thomas Wood, said the conspiracy believers spanned the political spectrum and tended to espouse conspiracy theories outside of medicine as well. But they found that the more conspiracy theories a person endorsed, the more likely he or she was to take vitamins and herbal supplements and buy mostly organic food, and the less likely he or she was to get an annual physical, wear sunscreen, visit a dentist or get a flu shot.

Given that medical conspiracy theories are so widely known and embraced, said Oliver and Wood, it would be unwise to dismiss all those who believe them as a “delusional fringe of paranoid cranks.” Instead, they suggested, “we can recognize that most individuals who endorse these narratives are otherwise normal” but use a sort of cognitive shortcut to explain complex and confusing events. Physicians can also glean a bit more about their patients -- and their readiness to accept medical counsel -- when they know that a conspiracy adherent has come for the occasional doctor visit.

[For the Record, 4:05 p.m. March 20: This post has been altered to correct the name of study coauthor Thomas Wood. An earlier version identified him as Thomas Weed.]