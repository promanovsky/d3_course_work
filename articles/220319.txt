Microsoft is widely expected to unveil its first-generation Surface mini tablet during a press conference on Tuesday. The device is thought to feature a display that measures approximately 8 inches diagonally, and it is also said to include a Qualcomm processor instead of an Nvidia chipset like the ones that powered Microsoft’s earlier Windows RT tablets. According to a new report, however, a refresh of one of Microsoft’s current tablets could steal the show.

A pair of reports that popped up this past weekend reaffirm earlier rumors suggesting a new Surface Pro 3 tablet could also be introduced during Tuesday’s press conference.

First, Paul Thurrott of Supersite for Windows reported that the new Surface Pro tablet model will feature a larger display than current models, which include a 10-inch screen. He also claims the new third-generation Surface Pro will be thinner than the first and second versions.

A separate report from WPCentral claims the new Surface Pro 3 will be made available in four colors — black, purple, cyan and red — and will be sold in the following five configurations:

i3-4GB RAM-64GB – $799

i5-4GB RAM-128GB – $999

i5-8GB RAM-256GB – $1299

i7-8GB RAM-256GB – $1549

i7-8GB RAM-512GB – $1949

The blog also noted that the new Surface Pro 3 may feature a tweaked design with a narrower bezel around the display, a larger screen than the current Pro model, and a Windows button that has been relocated to one of the vertical bezels — an odd choice, if true, for a tablet with a larger display.