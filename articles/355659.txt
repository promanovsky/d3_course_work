Chris Colfer is not leaving “Glee” in its final season, contrary to a message posted on his Twitter feed. A rep for Colfer confirms that he is returning for the sixth season of the Fox comedy and that his Twitter account was hacked.

“Glee” producer 20th Century Fox Television also released the following statement: “We’ve been alerted that Chris Colfer’s twitter account has been hacked. Rumors of his dismissal from Glee could not be further from the truth. We love Chris and look forward to working with him again this season.”

The actor is currently in transit without access to his Twitter account, which is why the offending tweet (below) has yet to be removed.