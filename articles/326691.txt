The struggle between Argentina and holdout bondholders led by Paul Singer’s Elliott Management has taken a strange turn this week, when Argentina first announced that it had deposited $539 million with Bank of New York Mellon to pay its main bondholders, and Judge Thomas P. Griesa then ordered the money to be sent back to Argentina because the payment was illegal in the first place, the AP reports.

Argentina says it will negotiate, but has yet to do so

Argentina has said that it’s willing to negotiate with Elliott Management following the Supreme Court’s decision to uphold the Second US Court of Appeals ruling that holdouts have to be paid along with main bondholders. Even though Argentine Economy Minister Axel Kicillof was recently in New York, he didn’t make time to initiate negotiations with Elliott Management, or even make contact as a show of goodwill. When that was followed up by a payment intended for bondholders who accepted the haircut back in 2001, it was seen as a bit of brinkmanship at best and possibly even the country acting in bad faith.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

“This is a brazen step in violation of the court’s orders, and it warrants a swift and decisive response,” wrote Robert A. Cohen of the law firm, Dechart LLP, before today’s ruling.

Argentina has about a month to reach a settlement with Elliott

After years of bitter disagreement and a recent memo from Argentina’s legal counsel that seems to recommend defaulting on the country’s debt, there’s no chance of either side giving benefit of the doubt. Argentina also said that it’s impossible to meet its $500 million interest payment to main bondholders by June 30 because it won’t have a settlement with Elliott Management and the other holdouts by then, so the payment to Bank of New York Mellon is even stranger. Besides, there’s no chance that Bank of New York Mellon would defy a Supreme Court decision, so if the payment was meant as a ploy it was really doomed from the beginning.

While Argentina’s interest payment is due in just a couple of days, it has a 30 day grace period before it is in default. Whether it intends to make good on its obligations or default and restructure its bonds outside US jurisdiction, and whatever legal fallout would come with it, we’ll know soon enough.