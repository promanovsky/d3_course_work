Editors Pick

NDAL MFG Inc. called back one lot of Manukaguard Allercleanse nasal spray citing yeast contamination, the U.S. Food and Drug Administration said in a statement. The spray is used to clean nasal passages and sinuses of irritants and other environmental contaminants. The recalled Allercleanse lot is lot # 2010045, with expiration date of BB 10/2023.

Netflix is set to make a new home for itself in Canada by opening a new corporate office in the North American country soon. The office is expected to be setup mostly in Toronto or Vancouver. The move by the online-video streaming giant comes a decade after Canadians invited Netflix into their homes for the first time. It had begun filming their first original production in Ontario in 2012.

Drugstore chain Walgreens, and healthcare company Aegis Sciences Corp. are extending their relationship to perform fast and accurate COVID-19 diagnostic testing as Walgreens further expands COVID-19 drive-thru testing nationwide. The drive-thru locations are part of Walgreens' collaboration with the administration, federal health agencies, as well as state and local authorities.