Phusion Projects, the Chicago-based company that sells Four Loko, announced in 2010 that it would remove caffeine from all of its products (while contending at the time “that the combination of alcohol and caffeine is safe”). It also agreed the following year to relabel its cans to accurately show how much alcohol was in them (while contending at the time that the company didn’t “believe there were any violations”).

AD

AD

The settlement was announced Tuesday by New York Attorney General Eric T. Schneiderman, who said the attorneys representing 19 states and the city of San Francisco had alleged that Phusion failed to alert consumers to the dangers of combining caffeine and alcohol and promoted flavored malt drinks to underage consumers.

“While our company did not violate any laws and we disagree with the allegations of the State Attorneys General, we consider this agreement a practical way to move forward and an opportunity to highlight our continued commitment to ensuring that our products are consumed safely and responsibly only by adults 21 and over,” Phusion president Jim Sloan said in a statement e-mailed to The Washington Post.

Sloan pointed out in the statement that Phusion sells Four Loko without any caffeine in 48 states and will continue to do so, before adding that the company still believes “that the combination of alcohol and caffeine can be consumed safely and responsibly.” The company later posted a note on Facebook saying that “Four Loko isn’t going anywhere.”

AD

AD

The agreement includes things that sound reasonable and some things that are a little more strange. For instance, Phusion agreed not to sell or promote alcoholic beverages to underage people, something it says it has already been doing. But Phusion also agreed that any model or actor hired to appear in its marketing materials “shall be a minimum of 25 years old … and shall reasonably appear to be over 21 years of age” (so any baby-faced actors are out of luck). The company also agreed not to “use depictions or descriptions of Santa Claus” in any advertisements.

As part of the settlement, Phusion agreed to pay the 20 states and San Francisco $400,000 by the end of the month, money which is meant to go toward the cost of the investigation, attorneys fees and programs aimed at preventing underage drinking.