PICTURE EXCLUSIVE: Zac Efron and Michelle Rodriguez confirm their romance by sharing a smooch on holiday in Sardinia



Zac Efron and Michelle Rodriguez have finally confirmed rumours they are dating after they were pictured kissing on holiday in Sardinia.



The 26-year-old hunk and the openly-bisexual actress enjoyed a PDA as they cosied up to one another on a yacht on Saturday.



Zac leaned in to kiss 35-year-old Michelle, who was seen puckering up her lips in preparation for the passionate kiss.



Scroll down for video



Sneaky smooch: Zac Efron and Michelle Rodriguez have finally confirmed they are dating after they were pictured kissing on holiday in Italy She's ready for you: Zac leaned in to kiss 35-year-old Michelle, who was seen puckering up her lips in preparation for the sneaky smooch

Michelle first spoke about her sexuality in October last year, when she told Entertainment Weekly, 'I've gone both ways.'

Speaking about assumptions that she has had relations with women, she added: 'I do as I please. I am too f***ing curious to sit here and not try when I can. Men are intriguing. So are chicks.'

However, it seems her amorous display with Zac can confirm that is no longer dating model Cara Delevingne.

Touchy feely: At one stage the Fast and the Furious actress was seen lying on Zac as he stroked her raven-coloured locks Enjoying a PDA: Zac and Michelle looked very hands-on as they had fun in the boat Cheeky: But Zac soon moved his hands elsewhere and couldn't resist a grab of Michelle's hip Sealed with a kiss: Michelle had a smile on her face as Zac gave her a little peck on the head Very close: The actress and the hunk stood hand-in-hand on the yacht at one point

Cara, 21, was first spotted passionately kissing Michelle at a basketball game in February, and the pair continued to remain close.

The last time they were reported to have been together was at the end of May, after a night of partying at London's Chiltern Firehouse.

Around this time there were also rumours that Michelle was romancing bisexual star Evan Rachel Wood - following her split from husband Jamie Bell - which Evan later denied.

He's a Lucky One! Zac put a loving arm around Michelle at one stage Off she goes: Not content with her and Zac's PDA, Michelle went on a mini adventure How did you get up there? The star showed her adventurous side by climbing up the yacht's mast What are you doing? Zac looked to be concerned about Michelle as he was seen throwing his arms up in the air Before Zac and Cara, Michelle has been rumoured to have dated Vin Disel after meeting on the set of Fast And The Furious. She was also linked to Halle Berry's husband Olivier Martinez, when he was single and working with Michelle on the action movie S.W.A.T. She previously told Cosmopolitan: 'I'm attracted to manly men. I can't do metrosexuals who get their nails done more than me. Which, is why, living in LA for eight years, you can't blame me for not having a partner!'. Former lovers: Michelle and Cara were last seen together at the end of March, stepping out in Miami, Floria Passionate kiss: In March, the pair were seeing smooching while on holiday together in Mexico But it seems that the fiery actress has now fallen for Zac, who was certainly showing off his manly physqiue while the pair enjoyed their holiday together. Zac has been linked to numerous Hollywood beauties over the years, and openly dated Vanessa Hudgens for around four years until 2010. He was rumoured to be to his Bad Neighbours co-star Halston Sage earlier this year.

In good company: Despite their romantic moment, Michelle and Zac were not alone as they enjoyed time on the yacht with their pals Buff body: The actor showed off his impressively toned torso on the boat Chatting away: The pair looked to be involved in a deep and meaningful conversation Zodriguez: Michelle looked particularly relaxed as she and Zac chatted to a blonde pal Buff: When Zac didn't have his hands all over Michelle, he was clenching his fist to showcase his muscles Leaning in: The pair also appeared to have some fun and games on the boat Assisting: The star gave Michelle a hand to dry off by providing her with a big white towel

Zac's shift from High School Musical teen heartthrob to Hollywood leading man hasn't been without drama - he spent time in rehab for a rumoured drug addiction last year.



Michelle has also made headlines in recent years for her wild antics and party lifestyle - including a short stint in jail for hit and run and driving under the influence (DUI) charges.



The pair have been enjoying a relaxing Italian getaway with their beefcake businessman pal Gianluca Vacchi.



PDA: Zac later covered himself up with a black T-shirt and couldn't resist a cheeky stroke of Michelle's chin

Mellow yellow: Zac and Michelle have been enjoying a relaxing Italian getaway with their businessman pal Gianluca Vacchi Shall we dance? Gianluca later did away with his yellow outfit and showed off his body in a pair of white swimming trunks Slim pins: Michelle showed off her impressive bikini body on the boat