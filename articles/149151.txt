Gilead Sciences Inc. (GILD) reported first quarter non-GAAP EPS of $1.48 after the bell Tuesday, up from $0.48 a year ago. The consensus estimate was for EPS of $0.89. The stock is now up 3.05 on 2.7 million shares.

Gilead Sciences gapped open higher Tuesday and posted gains in early trade, before settling into a range. Shares finished up by 1.26 at $72.86 on above average volume. The stock rose to a 2-week high.

For comments and feedback contact: editorial@rttnews.com

Business News