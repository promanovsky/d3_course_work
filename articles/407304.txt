NEW YORK (AP) – WWE is set to make its video vault of classic matches on its video-streamed network available around the globe.

WWE has hit 700,000 subscribers since the company home to Hulk Hogan, Brock Lesnar and John Cena launched the WWE Network in February, an increase of just 33,000 since the company revealed its initial total in April.

The sports entertainment giant had hoped to reach 1 million subscribers by the end of the year. WWE could still hit that target by next year, with international network expansion coming Aug. 12 in more than 170 countries and territories, including Hong Kong, Mexico, Spain, and Russia.

But WWE lost $14.5 million in the second quarter and will lay off seven percent of the company’s staff.

WWE announced Thursday a new 10-year partnership with Rogers Communications in Canada that will launch the WWE Network as a traditional pay-TV channel in Rogers’ cable systems, also starting Aug. 12.

“WWE’s core business metrics remain strong, and WWE Network continues to be the single greatest opportunity to transform WWE’s business model,” said Vince McMahon, Chairman and Chief Executive Officer. “Additionally, we identified efficiencies that will improve our 2015 OIBDA outlook by $30 million. Based on these initiatives, we are optimistic about our potential to drive long-term growth.”

So much hinges on the health of the network.

George Barrios, WWE’s chief strategy and financial officer, said before the network launch about 1 million subscribers would allow the network to break even. Barrios had said WWE could have between 2 million and 4 million global subscribers.

Barrios told The Associated Press this week the network should still hit those numbers, though he had not laid out a timetable for reaching that mark.

“We’ve done a lot of research that makes us excited about the ability to get to that place,” he said. “Then it’s going to be a ground game every day.”

Barrios said if the WWE could average 1.5 million subscribers in 2015, the company, with headquarters in Stamford, Connecticut, would expect a profit of $100 to $120 million.

The expansion should help and more tag-team partners are on the way. The network is expected to air live in Britain by October 2014, in Italy and United Arab Emirates in 2015, and in Germany and Japan by early 2016.

WWE is still a hit every Monday night with its flagship show, “Raw,” and posts solid ratings for Friday night’s “Smackdown.”

The July 28 “Raw” averaged 4.317 million viewers and last week’s averaged 4.43 million — so the fan base is there for the WWE to think big when it comes to network numbers.

But, much like the bad guy who needs time to turn good, some viewers are still hooked on the traditional PPV outlet, even at $50-$60 a month with no additional content.

“It takes time for people to hear about it, so part of it is awareness,” Barrios said. “Part of it is people getting comfortable with it being something they want to shift to. The pay-per-views were a behavior we trained for 30 years and people don’t change overnight. It’s time and awareness.”

WWE said 91 percent of subscribers access the network at least once per week and use 2.5 devices to consume network content. WWE said 90 percent of subscribers are satisfied with WWE Network, with 51 percent extremely satisfied and 39 percent somewhat satisfied.

Those fans will soon face a change in how they pay their bill to have 24-hour access to The Rock and Daniel Bryan. The network launched set at $9.99 per month with a six-month commitment. The company will introduce new payment plans in August, including a $19.99 monthly plan (with no commitment) and an upfront one-time payment option for its existing $9.99 per month offering (with six month commitment).

WWE will add a resume play feature for video content in the next few months.

WWE is making a strong network push centered around its loaded Aug. 17 SummerSlam card in Los Angeles. The show boasts the best on-paper main event of the year with company stalwart Cena defending the WWE World Heavyweight Championship against former UFC star Lesnar. Also, Stephanie McMahon will face Brie Bella in one of the other marquee matches.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: