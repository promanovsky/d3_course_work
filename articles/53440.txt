Actor Zac Efron got into an altercation with at least one homeless person early Sunday in downtown Los Angeles after the vehicle he and a friend were driving became “stranded,” police officials confirmed.

Officers responded to the scene around 2:25 a.m. Sunday to Temple Street and the 110 Freeway, where Efron, 26, and his friend were waiting, LAPD spokesman Richard French said. Police officials said the officers were flagged down and pointed to the scene by a third party.

Police Cmdr. Andy Smith said there were indications of an altercation, but no details on what transpired were immediately available.

No arrests were made, French said.

Advertisement

Over the past year, the 26-year-old actor has had to deal with a broken jaw he reportedly suffered after slipping in a puddle of water at his home, and address rumors that he’s secretly been to rehab, twice.

While promoting his latest moving “That Awkward Moment” on “The Today Show” in February, Efron declined to directly address host Savannah Guthrie’s inquiry about the rehab rumors, instead saying: “I’m in the best place I’ve ever been. I’ve never been this happy before.”

[For the Record, 2:08 p.m. PDT, March 27: A previous version of this post gave an incorrect first name for Savannah Guthrie, calling her Samantha.]

richard.winton@latimes.com

Advertisement

kate.mather@latimes.com