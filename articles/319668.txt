Once again, Gary Oldman is very sorry.

The actor made headlines this week for defending Mel Gibson and Alec Baldwin, telling Playboy that he hates hypocrites and double standards and, come on, everyone has insulted Jews and gays at some point... right?

The Anti-Defamation League promptly blasted Oldman’s his anti-Semitic comments, which included a rant about how Jews "run Hollywood."

Oldman quickly apologized after the interview went public - and then did so again during an appearance on Jimmy Kimmel Live yesterday.

"Once I saw it in print, I saw that it was insensitive, pernicious and ill-informed," Oldman told the host. "I am a public figure, I should be an example and inspiration and I am an a--hole.

"I am 56. I should know better. I extend my apology and my love and best wishes to my fan base."

Prior Oldman's appearance on Kimmel, Anti-Defamation director Abraham Foxman told the UK's Guardian newspaper that he remained unhappy with the Harry Potter star.

"At this point, we are not satisfied with what we received. His apology is insufficient and not satisfactory," Foxman said.

So Oldman tried again last night.

"Words have meaning and they carry weight. And they carry on long after you've said them," he told Kimmel. "I don't condone or excuse the words that I used in any context. I just basically shouldn't have used them, but I did and I have injured and wounded a great many people...

"I stepped out of my area of expertise and I landed both feet in a hornets' nest. It came over in a certain way and for that I am deeply sorry."

What do you think, THGers? Is this show of remorse enough?

Compare Oldman's apology to other notable mea culpas in celebrity history below: