'The Big Bang Theory' star Kaley Cuoco debuted a new hairdo in which her long blonde locks were chopped off.

The 28-year-old actress posted the picture of a shoulder-length hairdo after updating a series of pictures that captured various moments of her haircut, reported People magazine.

She posted the snap of her stylist, Christine Symonds in which Symonds is about to chop her long locks.

"This is happening," Cuoco captioned the snap.

The actress followed it up with a shot of her updated hairdo along with the caption "Spring refresh."



"I can't stop!" she captioned the new pic, which showed a much-shorter shoulder-length bob.