By Connor Adams Sheets - Pet product retail chain PetSmart Inc (NASDAQ: ) announced Wednesday that it will stop selling dog and cat treats manufactured in China amid concerns that the treats are sickening animals.

The move comes a day after competitor Petco Animal Supplies pledged to stop selling the same Chinese treats.

The company’s announcements come in the wake of a growing outcry over potential dangers posed by the treats. The U.S. Food and Drug Administration has received more than 4,800 complaints about sick pets and 1,000 reports of dog deaths since 2007, according to the Associated Press.

The FDA, which is investigating the complaints, has not proven a direct link between the potentially tainted treats and the complaints, but a rising tide of vocal activists and animal lovers has been calling on pet stores to stop carrying treats imported from China.

PetSmart said it plans to clear its stocks of the treats by March 2015, the AP reported. It added that a company spokeswoman said the “vast majority” of the treats it sells are not from China but that it will not stop selling them immediately because “we don’t want to leave pet parents high and dry.”

Meanwhile, Petco CEO Jim Myers told the AP that his company will stop selling Chinese-made treats by the end of 2014.

"We know the FDA hasn't yet identified a direct cause for the reported illnesses, but we decided the uncertainty of the situation outweighs the lack of actual proof," Petco CEO Jim Myers said.

Petco and PetSmart each operate about 1,300 stores.