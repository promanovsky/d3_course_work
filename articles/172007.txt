Drivers sometimes clip badly parked trucks, hit unpredictable cyclists, and get caught on train tracks; these are all hazards of modern driving. But Google thinks there's a better way to navigate these tricky scenarios.

Google announced a new update to its driverless car project on Monday, saying it has logged more than 700,000 miles of accident-free driverless driving. Its most recent experimentation has focused on city driving, which poses many more challenges and random factors than highway driving. Though there is still a ways to go before drivers glance at the car next to them and see an empty driver’s seat, Google’s most recent milestone brings the technology another mile closer to mainstream.

“We’ve improved our software so it can detect hundreds of distinct objects simultaneously—pedestrians, buses, a stop sign held up by a crossing guard, or a cyclist making gestures that indicate a possible turn,” says Chris Urmson, director of the self-driving car project at Google in a blog post. “A self-driving vehicle can pay attention to all of these things in a way that a human physically can’t—and it never gets tired or distracted.”

In a video on the post, the Google driverless car can sense a biker who veers in and out of its lane, navigate around road construction, and pause for errant pedestrians. The new technology can also read stop signs and waits for all cars to cross railroad tracks before crossing itself.

Certainly impressive, but the driverless car has a ways to go. The next steps include improving the car’s ability to recognize hand signals, merge, turn right on red, and drive in hazardous weather conditions. Not to mention, the test drives have mostly occurred in Google’s hometown of Mountain View, Calif. Google will need to expand and test it in new areas to see how the car reacts to new settings.

And these are just the figurative and literal roadblocks waiting on the road. There are also major legal issues facing driverless cars. So far, only four states (California, Nevada, Florida, Michigan) and the District of Columbia have approved the testing of driverless cars, though many other states don’t have a law expressly addressing the new technology. Who is to blame in an accident without a driver?

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

That being said, Google is pushing full steam ahead with this innovation. At a 2012 ceremony allowing the testing of driverless cars in California, Google co-founder Sergey Brin said, “you can count on one hand” the number of years before he expects this tech to hit the mainstream. The company hasn’t released any indication that it isn’t on track to hit this goal.

Other carmakers, such as Nissan-Renault and Ford, have been developing their own self-driving cars, and analysts think the options are only going to increase. Analyst firm IHS predicts that by 2035 there will be 11.8 million self-driving cars on the road, and by 2050 all cars will be driverless.