"Saturday Night Live" host Andrew Garfield opened his monologue by telling the audience that he was "petrified" to host the live show.

Cue Garfield's "The Amazing Spider-Man 2" co-star and real-life girlfriend, Emma Stone, who has previously hosted "SNL."

"I'm not even here, I just wanted to say you're doing so great!" Stone said, before offering her boyfriend some words of wisdom and refusing to leave the stage.

Advertisement

Here are some of Stone's tips that don't work out very well for Garfield:

"If you get into trouble, just remember that the band is really great and they laugh at anything." (They don't laugh when Garfield then makes a joke.)

"Use the cue cards." (He has his lines memorized instead.)

"Be self-deprecating, make a little joke about yourself." (He starts telling a story about his new movie.)

Watch the complete opening monologue below: