Google announced Thursday that government requests for user information has increased by 120% since 2009. Though the number of Google users has also increased over the same time period, the company says it is “seeing more and more governments start to exercise their authority to make requests.”

Google’s announcement comes by way of its bi-annual transparency report, which details government requests for user data where the company is legally able to do so. The report comes just one week after news that Google is working on enhancements to its Gmail service that would make it more difficult to conduct mass surveillance of users’ messages.

[Google: Official Blog]

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.