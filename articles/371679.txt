NEW YORK, NY - JUNE 06: A selection of cupcakes is seen at Crumbs Bake Shop & David Burke NYC event to launch lunch menu at David Burke Treehouse Bar on June 6, 2013 in New York City. (Photo by Brian Ach/Getty Images for Crumbs Bake Shop)

NEW YORK (AP/WJZ) — Crumbs says it is shuttering all its stores, a week after the struggling cupcake shop operator was delisted from the Nasdaq. It’s local shops include Towson Town Centre, Columbia Mall and White Marsh Mall.

The New York City-based company said all employees were notified of the closures Monday. A representative for Crumbs could not immediately say how many workers were affected or how many stores it had remaining on its last day.

“Regrettably Crumbs has been forced to cease operations and is immediately attending to the dislocation of its employees while it evaluates its limited remaining options,” the company said in an emailed statement. That will include filing for Chapter 7 bankruptcy liquidation.

A press release from its website in March listed 65 locations in 12 states and Washington, D.C. The website had not been updated with notification of the closures late Monday.

Crumbs was founded in 2003 and went public in 2011, selling giant cupcakes in flavors including Cookie Dough and Girl Scouts Thin Mints. More recently, however, it had been suffering from a steep decline in sales. For the three months ending March 31, Crumbs Bake Shop Inc. reported a loss of $3.8 million, steeper than the loss of $2 million from the same period a year ago.

The company had warned in a filing with the Securities and Exchange Commission this past May that it “may be forced to curtail or cease its activities” if its operations didn’t generate enough cash flow.

As of the end of last year, Crumbs listed about 165 full-time employees and about 655 part-time hourly employees working in its stores.

(Copyright 2013 by The Associated Press. All Rights Reserved.)