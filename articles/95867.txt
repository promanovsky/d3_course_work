Amazon

Amazon has been hard at work perfecting an efficient, small, unmanned aerial vehicle strong enough to carry packages straight to customer's doors within 30 minutes of an order being placed.

In his 2013 Letter to Shareholders released on Thursday, Amazon CEO Jeff Bezos wrote that the company has already flight-tested several generations of aerial vehicles, aka drones, and is working on designs for future generations.

"The Prime Air team is already flight testing our 5th and 6th generation aerial vehicles," Bezos wrote, "and we are in the design phase on generations 7 and 8."

Amazon made a splash in December when it announced it was testing a drone delivery service. Dubbed Amazon Prime Air, the idea is to deliver shoebox-size packages to customers with unmanned aerial vehicles -- about the size of a remote-controlled airplane -- on a faster time-scale than other delivery services.

"One day, Prime Air vehicles will be as normal as seeing mail trucks on the road today," the company says in its Prime Air FAQ. "Safety will be our top priority, and our vehicles will be built with multiple redundancies and designed to commercial aviation standards."

However, despite the company building several prototypes -- such as the 8-propeller a few months ago -- it's likely the delivery service won't be dropping off packages anytime soon. Besides continued testing, the company also faces strict privacy regulations from US lawmakers and the Federal Aviation Administration.

While aerial delivery drones sound futuristic, a couple of other countries have already begun allowing their use. The United Arab Emirates announced its plans in February to begin delivering official documents with unmanned aerial vehicles and a parcel service in China began testing package deliveries via drone in the city of Dongguang in September.