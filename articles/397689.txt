A doctor from Sierra Leone who was leading the fight to battle the deadly Ebola virus outbreak there has died from the disease, officials in that country said.

Dr. Sheik Umar Khan died at a Doctors Without Borders facility in Kailahun, Sierra Leone, said Dr. Brima Kargbo, chief medical officer of the country’s Ministry of Health and Sanitation. Speaking to the Los Angeles Times by phone, Kargbo said Khan had died Tuesday afternoon.

He was the latest casualty in the Ebola outbreak in West Africa, which is the largest recorded, killing more than 670 people and sickening hundreds more since March, according to the World Health Organization.

Doctors Without Borders released a statement, saying the organization is “deeply saddened” by the death of Khan, who was hailed a national hero by the government and had treated more than 100 patients.

Advertisement

This month, three nurses working alongside Khan died of Ebola, but he continued to work.

“Dr. Khan was an extremely determined and courageous doctor who cared deeply for his patients. His work and dedication have been greatly appreciated,” the statement read.

Government officials in Sierra Leone reported last week that Khan had contracted the disease and had been admitted for treatment in Kailahun.

Sierra Leone President Ernest Bai Koroma visited the center where the doctor was being treated on Monday.

Advertisement

Medical personnel are among some of the most vulnerable people to contract the incurable disease because of the close contact they have with infected blood or bodily fluids.

“We are the first port of call for somebody who is sickened by disease,” Khan told Reuters last month. Khan told the news service that he had installed a mirror in his office to check carefully for any holes in his protecting gear before treating infected people. “Even with the full protective clothing you put on, you are at risk.”

Dr. Samuel Brisbane, a top Liberian health official, died Sunday of the disease, and a Ugandan doctor died earlier this month, according to the Associated Press.

Two American volunteers, Dr. Kent Brantly and Nancy Writebol have also contracted the disease, according to Christian aid organization Samaritan’s Purse. Both are reportedly in stable condition and receiving round-the-clock care in Liberia.

Advertisement

As a precaution, the charity has evacuated all nonessential personnel in Liberia.

There is no vaccine or specific treatment for Ebola, which causes fever, vomiting, diarrhea and massive internal bleeding, and has a fatality rate of 60% to 90%.

For more breaking news, follow me @cmaiduc