SARAH VINE: ADHD and why we working mums need to look in the mirror



Fascinating stuff in yesterday’s Mail from paediatric neurologist Dr Richard Saul, who argues in a new book that Attention Deficit Hyperactivity Disorder is wildly over-diagnosed and that thousands of children are being identified as victims and treated with drugs they do not need.

Instead, their behaviour is often a sign of simple, resolvable problems such as poor diet, lack of sleep, hearing loss, learning difficulties — even just old-fashioned boredom.



He quotes the case of one boy whose inability to concentrate turned out to be caused by anaemia and a girl whose disruptive behaviour in class was down to extreme short-sightedness.



Attention: Sarah Vine says the problems lie not with the children but with the paucity of parenting. File picture

Their symptoms disappeared once the underlying problems had been solved.



In other cases, the diagnosis of ADHD invariably masks something far more serious: depression, bipolar disorder, even schizophrenia.



And because some of the symptoms of ADHD — irritability, listlessness, memory problems — are similar to those present in youngsters taking drugs or abusing alcohol, such harmful social habits are often allowed to take hold instead of being addressed.



As a parent, I know how tempting it is to become frustrated with a child’s behaviour and to look to the medical profession for answers.



We’re all busy people: so much quicker and easier to give a problem a name and treat it with a handy little pill.



But the first place any parent should head when a child starts to play up is not their doctor’s surgery, but the bathroom mirror.



'The first place any parent should head when a child starts to play up is not their doctor's surgery, but the bathroom mirror'. File picture

Saul’s analysis presents us with an inconvenient truth. He is telling the mums and dads of children diagnosed with ADHD (of which there are approximately 400,000 in Britain) what many of them perhaps suspect, but don’t want to hear: the problems lie not with the children but with the paucity of their own parenting.



I do not exonerate myself from this accusation. As someone who never once contemplated giving up work after babies, I made many wrong decisions in the first few years of my children’s life. The upshot was that around the time my eldest started school, the most terrible realisation dawned on me: I hardly knew my children.



That, really, is the unspoken tragedy of the working mother. For all the financial and intellectual advantages we have by carrying on working, ours is also a story of benign neglect, of guilt, of children fighting for windows in our busy diaries — and of little ones left to their own (increasingly electrical) devices while we rush around frenetically, juggling priorities.



This is all very different from my own upbringing. When I was a child, my father was out of the door first thing and often not home until past bedtime.

But it didn’t matter because my mother was my universe, the one constant in my life, a source of wisdom, discipline, advice, affection, fun. To this day she knows me better than I know myself.



I am not sure I can say the same of myself in relation to my own children? They’re definitely always top of my to-do list; but there’s an awful lot else on there as well. Can I really blame them, then, when they exhibit attention-seeking behaviour?



When my son jumps all over the sofa in the evenings, knocking over the lampshade, does he have ADHD or is he just trying to get me to look up from my laptop? Is he ill or is he simply reacting to my parental incompetence?



It’s a tough question that we all have to ask ourselves. Many parents, I imagine, find themselves in this kind of situation — and it’s not one that’s going to change in the near future.



Not just because, increasingly, people need two incomes to keep their heads above the economic waterline, but also because, thanks to technology, many people no longer enjoy any such thing as a day off.



Last week, for example, I tried to take a few hours out to accompany my daughter on a school trip, only to be constantly distracted by a stream of work-related texts, emails and calls.



Dr Saul may be right when he says ADHD is not a genuine illness. But the very fact of its existence is an inescapable symptom of one indisputable fact: the fragmentation of family life, by whatever means, is neither good for us nor our children.

I’d hit Russians with Yoko Ono

Besieged by loudspeakers spouting Russian propaganda, Ukrainian marines in the Crimea have hit back with Cher’s greatest tunes.

Russian soldiers are used to hardship, but not even Vladimir Putin could withstand repeated exposure to a woman who’s had so many facelifts she sings from her pelvic floor.

While Cher is certainly a terrifying weapon, she wouldn’t be my first choice. For me, it has to be John and Yoko’s Imagine.

With its wet, weedy lyrics and smug, self-satisfied air of moral superiority, just a few seconds of it drains me of the will to live.

Why I’m addicted to Poundland

I can’t say I’m surprised at the successful stock market flotation of Poundland. I hate paying over the odds for a label and couldn’t care less what brand of washing-up liquid I use, which is why I regularly shop in my local Poundland.

Not everything is an unqualified success. I once bought some Christmas wrapping paper there only to discover that not only was it so thin it ripped at the slightest provocation, it also smelt funny and covered the carpet in bits of glitter that took months to extract.

But when it comes to everyday domestic necessities, from loo paper to cheap halloween decorations, it can’t really be beaten.

Keep your bumpfies to yourself ladies



Tamara Ecclestone (left) and Chloe Delevingne proudly display their baby bumps

It’s the ultimate in exhibitionist chic: the naked pregnant selfie, aka the ‘bumpfie’.

While most pregnant women do their best to stay away from the lens, those lucky enough to have been born gorgeous or rich (or both) see it as yet another way of reminding mere mortal women of their physical and financial superiority.

And so Chloe Delevingne, elder sister of model Cara, gives us her best Earth Mother on a beautiful beach in the Caribbean. Meanwhile, Tamara Ecclestone assumes a Madonna-like expression for her own carefully posed studio shot.

Two very different approaches, both equally self-obsessed and daft.

Of all the words used to insult assertive women, ‘bossy’ seems to me to be the least offensive. I’ve always been called bossy, and it’s never once struck me as an insult — more of a perfectly fair assessment of my personality.

So quite why the former U.S. Secretary of State Condoleezza Rice and Facebook CEO Sheryl Sandberg are putting their faces to a video campaign called ‘Ban Bossy’ is a bit of a mystery — the worst kind of pointless Seventies feminism.

Besides, also taking part in the video is Beyonce, last seen on stage at the Grammys dressed as a lapdancer, twerking her way around a chair.

Not exactly a role model for young girls. Unless, that is, they’re thinking of marrying a former drug dealer and pursuing a career in the porn industry.

Cheese BEFORE pud takes the biscuit, Mary!



Readers, I saw it with my own eyes.

On Monday night’s cookery show, Mary Berry dropped the following bombshell: ‘I like to serve the cheese before the pudding’, and then went back to presenting her programme as though the world of aspirational middle-class dining had not just turned on its axis.

Whatever next? The Dowager Countess telling Carson to scrap the fish course?

Cake controversy: Mary Berry has the cheese course before the pudding

Why all the fuss about Kate and William’s break in the Maldives?

Now that the heir is eight months old, it’s time to get cracking on that all-important ‘spare’.

See, it’s practically a working holiday.

Much talk this week about how the BBC can find ways of saving money.

Here’s a suggestion: try not to employ sexist pondlife such as Frankie Boyle who, having insulted everyone from the Queen to Rebecca Adlington with his especially vicious brand of playground bully ‘humour’, has been working on two new shows, one for Radio 4 and one for iPlayer.

Research for the journal of Evolutionary Psychology has turned up a fascinating nugget about the different ways in which men and women react to infidelity.

Men are more concerned about the physical betrayal, whereas women care more about the emotional implications of a partner’s affair. That certainly rings true to me. If my husband accidentally found himself in a clinch with another woman, I’d be pretty furious, but it wouldn’t be a sacking offence.



Marriage is a marathon; the odd slip-up is permissible.