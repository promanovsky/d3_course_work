NEW YORK (MarketWatch) — The dollar rises against the pound, the first gain in five sessions, after weaker-than-expected U.K. inflation raised questions about the Bank of England’s need to increase interest rates, which tends to weigh on price pressures.

The U.K.’s annual inflation rate fell to 1.5% in May from 1.8% in April, missing expectations. That’s the lowest rate in nearly five years.

Of course, the central bank would likely need to see evidence of a more sustained decline in prices before reacting. “To the extent that this is only temporary...we doubt that it would have lasting impact on the pound sentiment, which received a huge boost in the wake of the Mansion House speech,” said Valentin Marinov of Citi in a note.

The pound GBPUSD, +0.29% fell to $1.6962 from $1.6983 late Monday.

The BOE last week surprised markets when Gov. Mark Carney said a raise in interest rates could come sooner than the markets expect, a reversal from his previously dovish stance on monetary policy. His comments added to confidence among investors that the BOE would be the first major central bank to hike interest rates in response to a strengthening U.K. economy. Higher interest rates tend to make a country’s assets more attractive, boosting the currency. The pound has gained 2.4% against the dollar in 2014 to date.

The Bank of England is expected to release the minutes from its latest rate-setting meeting on Wednesday, with market participants on the watch for discussions about the timing of a rate hike. Read: Time to read the BOE’s tea leaves on rate hikes

In other action, the dollar USDJPY, +0.16% rose to ¥102.15 from ¥101.84 late Monday.

U.S. consumer prices jumped 0.4% May, sending the annual consumer inflation rate to 2.1%, the highest level since fall 2012. Economists polled by MarketWatch had expected a 0.2% increase in consumer prices. U.S. new-home construction fell by 6.5% last month to an annual rate of 1 million units, versus expectations for an annual rate of 1.02 million.

Investors are also paying attention to the Federal Reserve for its monetary-policy decision and latest economic projections due Wednesday. Investors will be watching for a change in inflation expectations or clues on when the Fed could hike interest rates. Read: What brokerages are saying about the FOMC meeting on Wednesday

The ICE dollar index DXY, +0.03% , which pits the greenback against six other currencies, rose to 80.607 from 80.450 late in the prior session. The WSJ Dollar Index BUXX, +0.02% , a broader gauge of the dollar’s strength, rose to 73.35 from 73.14.

Optimism for the U.S. dollar is gaining momentum among fund managers, with a net 79% of respondents to a Bank of America Merrill Lynch survey calling for the dollar to rise over the next year. That’s one of the most bullish readings in the last 15 years.

The euro EURUSD, -0.12% fell to $1.3546 from $1.3572 late Monday. The Australian dollar AUDUSD, +0.06% declined to 93.36 U.S. cents from 93.99 U.S. cents.

More must-reads on MarketWatch:

Time to read the BOE’s tea leaves on rate hikes

Burrito bonds, meteor bonds: 8 crazy bond issues

How China is keeping U.S. bond yields low