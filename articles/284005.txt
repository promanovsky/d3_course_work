GM said Friday that it knows of three crashes and four minor injuries from the problem. A spokesman said the air bags did not go off in the crashes, but GM hasn’t determined if the nondeployment was caused by the switches.

That disables the power steering and brakes and could cause drivers to lose control.

This time the company is recalling nearly 512,000 Chevrolet Camaro muscle cars from the 2010 to 2014 model years because a driver’s knee can bump the key and knock the switch out of the “run” position, causing an engine stall.

GM said the Camaro switches met its specifications — unlike those at the center of a recall of 2.6 million small cars. That problem has caused more than 50 crashes and at least 13 deaths.

Company spokesman Alan Adler said the problem occurs rarely and affects mainly drivers who are tall and sit close to the steering column so their knees can come in contact with the key.

The Camaro switches are completely different from those in the small cars with ignition switch problems. The Camaro switches, he said, were designed by a different person, and meet GM standards for the amount of force needed to turn the cars on and off.

Currently, the Camaro key is integrated like a switchblade into the Fob, which contains the buttons that let people electronically lock doors and open the trunk. GM will replace the switchblade key with a standard one, and a separate Fob attached by a ring so it will dangle from the key. Adler said with the change, if the driver’s knee hits the Fob, it doesn’t come in contact with the key.