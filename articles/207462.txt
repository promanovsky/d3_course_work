In the three decades since the first U.S. dietary guidelines were issued, Americans have become heavier and more saddled with diabetes and other diet-related diseases. The documentary “Fed Up” takes a look at what happened and offers a most poignant profile of what life is like for overweight children.

“Fed Up,” which opened this week, lays a large share of the blame at the door of the food industry. It looks at the idea that we don’t seem to get healthier despite a proliferation of products, surgeries, exercise programs and diets. The film is narrated by journalist Katie Couric, who also is an executive producer.

“The conventional wisdom [about obesity and health] turns out not to be true,” executive producer Laurie David, who was producer of the 2006 climate change film “An Inconvenient Truth,” said by phone on Monday.

The most affecting part of the film might be the families that appear — caring families struggling mightily with obesity, diabetes and other health issues. The director, Stephanie Soechtig, said in a phone interview that she has been following some of them for more than two years.

Advertisement

The filmmakers also talk to a number of doctors, scientists, writers and others who argue against many of the fat tropes, including that weight is a personal responsibility and that a calorie is a calorie. The history of diet advice — low-fat, low-carb, high-protein dieting; the U.S. government’s food pyramid for “healthful” eating — all come under scrutiny.

The film blames an industry that it charges is more interested in profit than in the nation’s health. Margo Wootan, director of nutrition policy at the advocacy organization Center for Science in the Public Interest, noted that candy and other snacks are no longer available just in food stores; they’re in office supply stores, linen stores, “everywhere.”

“We’ve got to change the way we produce and consume food,” says former President Clinton. And the message of “Fed Up” is to start by cooking real food and avoiding processed items.

The film is likely to be met with plenty of criticism.

Advertisement

“It’s a very myopic view of how obesity develops, and it offers no real solutions,” was the assessment of James O. Hill, a pediatrics and medicine professor at the University of Colorado School of Medicine in Denver.

Hill said that he objected to the lack of attention to physical activity in the film and that the assessment of caloric sweeteners as the major problem in Americans’ diets was mistaken.

“I’m not arguing that the food environment is unimportant. I’m not arguing sugar is not important,” Hill said. “I think the food industry has some responsibility,” but he believes the food industry and scientists need to join forces to find solutions.

From 1977 to 2000, Americans doubled their average daily intake of sweeteners, including high fructose corn syrup, in part because it was used as a replacement when foods were reformulated to remove fat. And at a certain level, sugar is toxic, Dr. Robert Lustig says in the film. Lustig is a pediatric endocrinologist at UC San Francisco.

Advertisement

And that point has the Sugar Assn. objecting. There is plenty of disagreement over whether it matters to distinguish among sweeteners. Adam Fox, a lawyer who represents the trade group, said Tuesday that consumption of table sugar has been on the decline. “I have to believe that Katie Couric and some of the doctors are well-meaning, but when you lump” all sweeteners together, it’s a disservice to the public.

The Grocery Manufacturers Assn. said in a statement that, “rather than identifying successful policies or ongoing efforts to find real and practical solutions to obesity, [the film] adopts a short-sighted, confrontational and misleading approach by cherry-picking facts to fit a narrative, getting the facts wrong, and simply ignoring the progress that has been made over the last decade in providing families with healthier options at home and at school.”

The filmmakers interviewed many well-known researchers and writers about the American diet, including author Gary Taubes, New York University professor Marion Nestle and Kelly Brownell, dean of the public policy school at Duke University. They noted, however, that food industry representatives declined to take part in the movie.

“I was floored” by that, Soechtig said. “I am apolitical when it comes to the issue. I think we tried to show both sides where we could. It was a mandate from Katie [Couric]. She’s a journalist. She’s not an activist.”

Advertisement

The first change the makers of the movie suggest is to give up added sweeteners — with and without calories — for 10 days. David, who also has a new cookbook out called “The Family Cooks,” said she’d like to see school lunch improvements, nutrition education, package labeling reforms to make the information clear and the disappearance of snack foods near store cash registers.