NEW YORK - MAY 22: A package of the new weight-loss drug 'alli' is displayed May 22, 2007 in New York. Pharmaceutical giant GlaxoSmithKline created 'alli', the nation's first over-the-counter version of a prescription weight-loss drug. (Photo by Chris Hondros/Getty Images)

LONDON, March 27 (Reuters) - GlaxoSmithKline is recalling all supplies of its non-prescription weight-loss drug Alli in the United States and Puerto Rico, the company said on Thursday, after reports that some bottles had been tampered with.

The British group, which believes that some supplies may not contain authentic Alli, is working with the U.S. Food and Drug Administration on the retailer-level recall.

News of the tampering first emerged on Wednesday.