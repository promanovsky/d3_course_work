Samsung has launched their all new Galaxy Tab S tablet, and boy it looks gorgeous. The tablet is poised to take on Apple’s iPad Air. Samsung has particular focused on making this device slim and powerful.

The Galaxy Tab S comes in 8.4-inch and 10.5-inch sizes but apart from that, both offer the same technical specs. In fact, the Tab S beats the iPad at least on paper. It offers higher resolution, storage support of up to 128GB and faster wireless connectivity. The Tab S is super slim at 6.6mm compared to iPad Air which is 7.5mm thin. With the Tab S, Samsung has finally managed to cram in as many features as possible, while still making the tablet slim and lightweight.

Blue Eagle Capital Partners: Long Thesis For This Lending Stock Blue Eagle Capital Partners was up 17.7% net for the third quarter of 2020, bringing its return to 49.1% for the first nine months of the year. During the third quarter, longs contributed 28.15% to the fund's performance, while shorts subtracted 7.36%. The S&P 500 was up 8.93% for the third quarter. Q4 2020 hedge Read More

So let’s compare the newly launched Galaxy Tab S with the iPad Air and see which tablet offers better specs.

[table]

Features, Samsung Galaxy Tab S, Apple iPad Air

OS, Android 4.4 KitKat, iOS 7

CPU, 1.9GHz Exynos S Octa CPU, Apple A7 chip

Display, “2,560×1,600 pixels”, “2,048×1,536 pixels”

RAM, 3GB, 1GB

WiFi, 802.11ac, 802.11ac

LTE, Yes, Yes

Camera, 8MP rear with Flash and 2.1MP front-facing, 5MP rear and 1.2MP front-facing

Storage, 16/32GB options, Up to 128GB

Expandable storage, Up to 128GB using MicroSD card, Not possible

Thickness, 0.23 inches / 6.6mm, 0.29 inches / 7.5mm

Weight, 465 g for 10.5″; 294 g for 8.4″, 469 g

Pricing, “$499 for 10.5-inch, $399 for 8.4-inch”, $499 for 16GB

Colors, Brown and White, Space Gray and Silver

[/table]