Bachelorette no more! After accepting Josh Murray's proposal on the season finale of The Bachelorette on Monday, July 28, reality star Andi Dorfman stopped by Good Morning America on Tuesday morning, July 29, with a ring on her finger.

The newly engaged Dorfman, 27, hit the morning show with her fiancé Murray, 29, looking happy and holding hands as they stepped onto the ABC set in New York City. The future Mr. and Mrs. Murray chatted with Lara Spencer about the previous night's happenings and what's ahead for them as a couple.

"I definitely knew there was that chemistry and attraction with him," Dorfman said of first meeting Murray. "I'm definitely an overthinker, though, so it took me till the very end. But there was something special with him."

The Atlanta native chose fellow Southerner Murray over runner-up Nick Viall. Dorfman dismissed Viall, 33, before the final rose ceremony in order to spare his feelings, once she had decided Murray was the man she wanted to marry.

"That's always the tough part," Dorfman said of her interaction with Viall. "That's kind of the nature of the beast with the show. I had to break up with 24 guys in order to get the one I really wanted and needed. It's always a little uncomfortable when you have to watch the breakups and talk about them."

The tough parts were seemingly worth it for the brunette beauty, however, as she has plans to say "I do" to her former pro baseball player love. As to when wedding bells will be heard, Dorfman said they have given "a little" thought to what's ahead.

"We would like to do something in the spring," she explained. "I think for now, it's like, I finally got the engagement ring, I'm just going to enjoy this for a little while… Just enjoy engagement life!"

Murray showed how much he will be soaking up said life, praising his future wife in his chat with Spencer. "I knew she was very intelligent, but she is so funny," he gushed of Dorfman. "She's beautiful. I was intimidated when I first got out of that limo, for sure."

Though the attractive pair's relationship began in front of the cameras, they did not say whether they will take the next step with the tapes rolling. "We'll see!" Dorfman said.

After the finale, Dorfman and Murray both expressed their love for one another on social media, as well. "Finally able to shout it from the rooftops!!" Dorfman captioned an Instagram shot of herself with her man. "Amazing season, amazing guy! One lucky girl. I love you more than you will ever know @joshmurray11 #thebachelorette."

"Meet my beautiful fiancé and the love of my life @andi_dorfman," Murray chimed in with a romantic photo of the couple. "I've waited 29 years to find her and it's been well worth the wait! God is soooooo great! #VeryBlessed #ForeverKindOfLove."