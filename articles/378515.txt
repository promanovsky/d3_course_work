The councilmembers' pro-neutrality starting point isn't the city's considerable new tech scene but the fact that about one-third of the city's population was born somewhere other than in the United States. Of course, New York City has long been a magnet for immigrants. But the immigrant existence takes on a different texture in an era when long-distance calling fees and hard-to-find native newspapers have been replaced by cheap and easy digital communications.

AD

AD

"Prior generations of immigrants left for America," reads the filing, "never seeing their families or homes ever again. Today, many of these immigrants subscribe to broadband for the applications that allow them to speak to families over Voice-over-IP (VoIP), see loved ones over video chat on Skype, and keep up with current events by watching news and independent coverage from their nations of origin." Such exchanges could be derailed, the argument goes, if those services are put out of business, degraded, or made more expensive on an Internet where content providers might be charged more for offering up their services.

New York City isn't the only city getting involved in the net neutrality debate. The more punctual city of Cambridge, Massachusetts, submitted a filing in May that leaned on the FCC to reclassify broadband Internet under Title II of the Telecommunications Act, the one that applies to essential telecommunications services. "Cambridge," wrote the city council three days before the FCC's comment period even formally began, "asserts that this classification would help restore free market competition to the broadband industry and provide for a democratic dissemination of information."

Perhaps New York City's filing took so long because it gets deeper into the weeds than Cambridge's filing, pointing to the protests in Ukraine that broke out last winter, and the degree to which New Yorkers were able to stay engaged because of the ease with which they could connect with family and friends, watch the news, and otherwise track the chaotic, bloody uprising.

AD

AD

"In our increasingly globalized city," argues the councilmembers, "the ability to connect with loved ones is as vital as a utility." It's a compelling hint at a feedback loop that has, perhaps, implications for home countries, too: if more North Koreans were able to leave, set up in New York City, and Skype back, perhaps fewer of their people would believe that Korea DPR had reached the World Cup final.

But while "vital as a utility" sounds like the New York City officials might, too, be edging toward support for Title II reclassification, the councilmembers pronounce themselves agnostic, saying that whether it's reclassification or a reliance upon the commission's so-called Section 706 power to deploy broadband, the important thing is protecting citizens' ability to use "the digital tools necessary to thrive in the modern world."