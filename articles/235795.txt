hidden

Videogame publisher ZeniMax Media on Wednesday sued Oculus, alleging theft of trade secrets during development of a gaming headset by the virtual-reality startup that Facebook has agreed to buy for $2 billion.

ZeniMax and its subsidiary id Software are suing Oculus and co-founder Palmer Luckey for, among other things, hiring away employees like well-known game programmer John Carmack to "surreptitiously gain further unauthorized access" to intellectual property, according to the lawsuit filed in a federal court in Texas.

The case has gained wide attention partly because of the involvement of Carmack, credited with helping conceive of such groundbreaking titles as "Quake" and "Doom." According to the lawsuit, Carmack, who now works with Oculus, may have provided valuable technology to Oculus VR as they collaborated on various projects.

Oculus said in a statement that the ZeniMax lawsuit "has no merit whatsoever. As we have previously said, ZeniMax did not contribute to any Oculus technology." Oculus said it will "defend these claims vigorously."

Oculus VR marks one of Facebook's largest deals to date, seen as a bet on the future of mobile and computer interfaces.

ZeniMax, which houses well-known game developers such as Bethesda Softworks and id under its umbrella, asserts in its lawsuit that the well-reviewed Oculus Rift headset - not yet available for sale - was developed using its intellectual property.

"We cannot ignore the unlawful exploitation of intellectual property that we develop and own, nor will we allow misappropriation and infringement to go unaddressed,” ZeniMax CEO Robert Altman said in a separate statement.

The lawsuit was filed in the U.S. District Court for the Northern District of Texas.

Reuters