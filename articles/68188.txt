Gold prices are set to drop next week with several analysts expecting the precious metal to continue exhibiting weakness after two weeks of muted values.

As many as 12 of 22 analysts polled in a Kitco Gold Survey said they expected gold prices to drop next week, while six predicted that prices would rise and four forecast prices to remain unchanged.

Lower prices could now boost physical demand.

Phillip Streible, senior market strategist at RJO Futures, said: "Gold futures should remain under pressure as long as the issues with Russia and the Ukraine fade and investors remain focused on the thoughts of tapering (stimulus) and rising interest rates,".

Kevin Grady of Phoenix Gold Futures and Options, said: "I still believe that the physical market is directing our underlying price.... In the gold forward rates we noticed that the physical tightness dropped off as we approached the $1,400 level. This situation put the shorts in control.

"We have noted that the rates have gotten tighter as we broke the $1,300 level. It appears that the price-sensitive buyers are back. We need to hold our major support level of $1,270 for gold to rebound."

Commerzbank Corporates & Markets said in a 28 March note to clients: "Gold not only dipped under the $1,300 per troy ounce mark [on 27 March] but also fell below the technically important 200-day moving average.

"Although the price closed beneath the 200-day moving average, there has been no technical follow-up selling so far. Gold ETFs recorded slight inflows again [on 27 March], holdings having been reduced in the two previous days.

"In other words, speculative financial investors are likely to have been mainly to blame for the price slide."

Gold Ends Lower

US Gold Futures for delivery in June finished 50 cents lower at $1,294.30 an ounce on 28 March.

Prices lost 3.1% for the week as a whole.

Earlier, spot gold inched up 0.2% to $1,293 an ounce.

Gold prices are down $100 from their price peak two weeks ago. Prices ended near six-week lows on 28 March, logging a second consecutive weekly loss.

Improving US economic outlook has supported the US dollar and has boosted the global appetite for risk, denting gold's safe-haven investment status.