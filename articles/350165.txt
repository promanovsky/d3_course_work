If you thought Facebook becoming popular in India would mean a localised version of the worlds most popular social network, you are mistaken. Facebook is already personalised for the individual, so the content Indian users see here is already Indian. So we dont need to do a lot of localisation of content, Facebook chief operating officer Sheryl Sandberg on Wednesday told indianexpress.com.

Facebook now considers India its second-largest market outside the US as far as users are considered . In the US, we are by far the number one mobile app, bigger than the next seven combined, she said, adding that while Facebook has been able to cover a large portion of the US population, it does not represent a large section of the population here despite 100 million users.

This means there is such huge opportunity for growth here, she told a select gathering of journalists in New Delhi. Sandberg said Facebook already reaches four times more people than the most popular TV show in India.

She said Facebook had no intention of launching a smartphone as speculated. We are the number one thing people do on their phones. I don't think we need to do hardware. We are doing very well with the software, she said. Facebook is the leading mobile application in almost every single market it was in, she added.

Our mission is to connect the world. Not only do we want to connect every person to every person, we also want to enable commerce, she said, adding that during her trip to India she has been talking to different stakeholders on how they can do personalisation at scale for marketing.

Asked how much of a business opportunity does Facebook see in everyone using their data for personalisation, she said the companys goal was to serve its mission of connecting the world, and having a thriving business is what enabled it to meet its goals.

Personalisation at scale is essentially what we do and we do it everyday. Any marketer can show a post in such a way that it appears personalised for each user and that is exciting, she said, adding how companies like Hindustan Unilever were using this ability to target ads at specific user groups.

She said Facebook was looking at India to grow its business, and was willing to invest here as the returns would be great. As people are spending more time on digital, the marketers will go where the people are, she said, explaining how the line had already been crossed in the US with people spending more time online than watching TV.

Incidentally, Sandberg, who has worked for Google as well as the US secretary of the treasury, started her career in India in 1991 as part of a World Bank project on leprosy in Madhya Pradesh. There were 3-4 million leprosy cases when I was last here, now there are less than 100,000, she said, adding this underlined the resilience of the Indian people.