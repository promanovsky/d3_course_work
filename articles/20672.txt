Today, cancer is one of the leading causes of human death, but the disease is practically absent from the archaeological record. Until now. Scientists have discovered what they believe to be the oldest known evidence of metastatic cancer in a 3,000-year-old skeleton found in a tomb in Sudan.

The finding suggests that cancer was already present in the Nile Valley around the same time David became king of the ancient Israelites. Researchers hope the discovery could shed light on the early days of the now common and deadly disease.

“This find is of critical importance,” Michaela Binder, a doctoral student at Durham University in England who led the research, told The Independent. “It allows us to explore possible underlying causes of cancer in ancient populations, before the onset of modernity, and it could provide important new insights into the evolution of cancer in the past.”

The skeleton, discovered at an archaeological site in northern Sudan known as Amara West, was that of a man between the ages of 25 and 35. According to researchers, he was buried on his back inside a painted wooden coffin. Dozens of other skeletons were also unearthed at Amara West.

“From footprints left on wet mud floors, to the healed fractures of many ancient inhabitants, Amara West offers a unique insight into what it was like to live there – and die – in Egyptian-ruled Upper Nubia 3,200 years ago,” Neal Spencer, from the Department of Ancient Egypt and Sudan at the British Museum and co-author of the study, said in a statement.

Spencer and a team of researchers from Durham University and the British Museum analyzed the skeleton using radiography and a scanning electron microscope. They uncovered lesions on the bones that were consistent with those caused by a soft tissue cancer.

The ancient man suffered cancerous damage to his pelvis, spine, shoulder blades, breast bone, collar bones and ribs. Researchers can’t pinpoint the exact cause of the cancer, but say several factors – environmental carcinogens, genetic factors or infection – could have triggered the disease.

The team hopes the discovery will help them explore the underlying causes of cancer in ancient populations and provide insights into cancer’s progression through history.

“Through taking an evolutionary approach to cancer, information from ancient human remains may prove a vital element in finding ways to address one of the world’s major health problems,” Binder said.

While the skeleton uncovered in Sudan is certainly one of the earliest examples of cancer, it’s not the only case of the disease in ancient times. In 2000, paleopathologists excavating an ancient burial mound in Russia found a 2,700-year-old skeleton whose bones were riddled with tumors. According to the New York Times, scientists determined this to be the oldest known case of metastasizing prostate cancer.

“The fact remains that there are only a minute number of truly ancient mummies and skeletons that show evidence of cancer,” Michael R. Zimmerman of Villanova University in Pennsylvania told the New York Times in 2010. “We just don’t find anything like the modern incidence of cancer.”