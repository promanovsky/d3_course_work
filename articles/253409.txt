SAN FRANCISCO, May 26 — These days, a laptop or tablet weighs only a pound or two, some even lighter than that. A smartphone weighs even less.

The screens are flat and the hard drives process information at lightning pace. But it actually wasn’t all that long ago when computers were huge, slow and expensive. The Internet was in its infancy or didn’t exist at all. Everything was slow, tedious and complex.

All this inspired The Fine Brothers to show a group of young children old computers from the past such as a classic Apple II. The reactions from the kids are simply adorable and hilarious.

It wasn't all that long ago when computers were huge, slow and expensive.