RICHMOND, Va. — Gov. Terry McAuliffe on Monday directed Virginia’s Board of Health to review new abortion clinic regulations and appointed five members to the board that the governor said share his commitment to women’s health.

The executive directive signed by McAuliffe asks the Board of Health to immediately review the regulations adopted last year that require existing facilities to meet the same building standards as newly constructed hospitals. Supporters say the regulations are intended to protect women’s health, but opponents say the aim is to put clinics out of business. The board must complete its review by Oct. 1.

As part of the state’s recognition of National Woman’s Health Week, McAuliffe also announced plans aimed at reducing the cost of drugs at women’s health clinics and expanding access to testing and treatment of sexually transmitted diseases.

“It’s not just a health issue as it relates to women’s health, it’s also an economic issue,” McAuliffe said at a news conference. “We do need to grow and diversify our economy, and in order to do that, Virginia needs to be open and welcoming to all and we need to make sure that all Virginia women have access to the health care resources that they need and deserve.”

McAuliffe said he’s very concerned that the new clinic regulations jeopardize the ability of most women’s health centers to keep their doors open and place the health and reproductive rights of Virginia women in jeopardy. He said the regulations were approved under “dubious circumstances marked by political interference and arbitrary, convoluted procedural maneuvers.”

The board initially voted to exempt existing clinics from the strict building standards. But then-Attorney General Ken Cuccinelli, a Republican and staunch abortion opponent, advised the board that it had overstepped its authority. The board reversed course and voted to apply the standards to all clinics.

The governor also announced that he’s directing the Virginia health commissioner to register and enroll eligible women’s health clinics in a federal program that allows the clinics to get drugs from manufacturers at a discount. He also said the state Department of Health to sign a memorandum of understating with Virginia’s four Planned Parenthood affiliates to provide free HIV testing to more than 1,800 women and men by the end of 2014.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)