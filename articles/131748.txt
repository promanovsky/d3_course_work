Each year in the U.S., approximately 12 million adults who seek outpatient medical care are misdiagnosed, according to a new study published in the journal BMJ Quality & Safety. This figure amounts to 1 out of 20 adult patients, and researchers say in half of those cases, the misdiagnosis has the potential to result in severe harm.

Previous studies examining the rates of medical misdiagnosis have focused primarily on patients in hospital settings. But this paper suggests a vast number of patients are being misdiagnosed in outpatient clinics and doctors' offices.

"It's very serious," says CBS News chief medical correspondent Dr. Jon LaPook. "When you have numbers like 12 million Americans, it sounds like a lot -- and it is a lot. It represents about 5 percent of the outpatient encounters."

Getting 95 percent right be good on a school history test, he notes, "but it's not good enough for medicine, especially when lives are at stake."

For the paper, the researchers analyzed data from three prior studies related to diagnosis and follow-up visits. One of the studies examined the rates of misdiagnosis in primary care settings, while two of the studies looked at the rates of colorectal and lung cancer screenings and subsequent diagnoses.

To estimate the annual frequency of misdiagnosis, the authors used a mathematical formula and applied the proportion of diagnostic errors detected in the data to the number of all outpatients in the U.S. adult population. They calculated the overall annual rate of misdiagnoses to be 5.08 percent.

"Although it is unknown how many patients will be harmed from diagnostic errors, our previous work suggests that about one-half of diagnostic errors have the potential to lead to severe harm," write the authors in their study. "While this is only an estimate and does not imply all those affected will actually have harm, this risk potentially translates to about 6 million outpatients per year."

Lead author Dr. Hardeep Singh, of the Baylor College of Medicine and the Michael E. DeBakey Veterans Affairs Medical Center in Houston, has previously conducted other studies that examine the reasons for misdiagnosis.

One study he coauthored in 2012, published in JAMA Internal Medicine, found that some of the most common reasons for misdiagnosis include problems with ordering diagnostic tests; failure by the patient to provide an accurate medical history; and errors made by a doctor in interpreting test results.





LaPook says it's important for doctors to take the time to do a thorough exam, listen to their patients and talk about their health concerns. "Doctors' visits these days tend to be rushed," he says. "That's a big problem."





There are also things patients can do to improve their chances of getting the right diagnosis.





LaPook advises patients to explain their medical history to the doctor in a clear, chronological way. "As a physician, I'm listening for clues," LaPook explains. Don't forget to share details of your family's medical history, such as a parent with cancer, which may be useful information for the doctor to take into consideration.



