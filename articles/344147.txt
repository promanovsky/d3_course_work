Asia Pacific share market advanced on Wednesday, 02 July 2014, as appetite for risk assets underpinned after reports showing manufacturing expansion in the world's two biggest economies. The MSCI Asia Pacific Index advanced 1 percent to 147.65.

Regional shares commenced trading with firm footing today, thanks to record high finish of Wall Street overnight on improved U. S. and Chinese economic activity. The S&P 500 and the Dow Jones Industrial Average added 0.7% and 0.8% respectively to new all-time highs on Tuesday as of strong US auto sales and better than expected ISM manufacturing activity data for June.

The auto industry sold 1.42 million vehicles in June in the US, well above the Edmunds.com projection of 1.36 million. US manufacturing grew in June for the 13th straight month, but the pace of the expansion slowed from May. The Institute for Supply Management's manufacturing purchasing managers index dipped 0.1 point to 55.3 in June, but well above the 50 reading that separates growth from contraction.

Traders were also encouraged by surveys showing China's manufacturing activity improved in June for the first time in six months after the government steps up policy support. The official purchasing managers' index (PMI), published by the National Bureau of Statistics, hit a six-month high of 51 in June, in line with market expectations and up from May's 50.8. The final HSBC/Markit purchasing managers' index for June rose to 50.7 from May's 49.4, surging past the 50-point level that separates growth in activity from contraction for the first time since December.

The Chinese government has unveiled a series of modest stimulus measures in recent months to give a lift to economic growth, which dipped to a 18-month low in the first quarter. Such measures have included targeted reserve requirement cuts for some banks to encourage more lending, quicker fiscal disbursements and hastening construction of railways and public housing projects. On Monday, China's banking regulator announced changes in the way it calculate banks' loan-to-deposit ratio to help channel more credit into the real economy.

The Chinese economy has started showing signs of stabilization since May as such supportive steps kicked in. Growth in factory output, retail sales and fixed asset investment all either met or beat market expectations last month, with retail sales notching its best showing in five months. As one of the earliest pieces of Chinese data released each month, the improved PMI will bolster market expectations that the economy is regaining some traction, after growth hit an 18-month low of 7.4% between January and March.

Among Asian bourses

Australia stocks rise on US, China growth

Australian share market closed sharp higher on tracking another record high finish of Wall Street overnight and improved Chinese manufacturing data, with shares of miners and lenders leading rally. The benchmark S&P/ASX200 advanced 1.48% to 5455.40 while the broader All Ordinaries grew 1.4% to 5441.70.

Shares of material and resources companies extended rally for second day in row, helped by better manufacturing data from China and a firmer iron ore price. The National Bureau of Statistics and China Federation of Logistics and Purchasing stated on Tuesday that the manufacturing Purchasing Managers' Index was at 51.0 in June increasing from May's 50.8. A similar index from HSBC Holdings Plc and Markit Economics rose to 50.7 from the previous month's 49.4. Numbers above 50 signal expansion.

Resources giant BHP Billiton climbed up 2.4% to A$36.85, while Rio Tinto rose 2.6% to A$62. Iron ore miner Fortescue Metals Group was up 0.9% to A$4.40.

Financial stocks closed higher, with major four lenders leading rally. Commonwealth Bank of Australia rose 1.2% to A$80.92, Westpac Banking Corp 1.5% to A$33.87, ANZ Banking Group 1.7% to A$33.39 and National Australia Bank 2.7% to A$33.18.

Shares in Treasury Wine Estates (TWE) fell 1% to A$4.88 after lawyers filed a class action against the company on behalf of 600 shareholders. The shareholders are seeking to reclaim some of the losses they incurred when TWE shares fell in July 2013, after the company announced A$160 million in write-downs relating to excess stock being held by its US distributors.

The Australian Bureau of Statistics said on Wednesday that seasonally-adjusted trade deficit rose to A$1.91 billion in May, an increase of A$1.13 billion (145%) on the deficit in April 2014. goods and services credits fell A$1.3 billion (5%) to A$26.68 billion, while goods and services debits fell A$169 million (1%) to A$28.59 billion.

Nikkei closes up 0.3%

Japan share market advanced today, as appetite for riskier assets underpinned on tracking record high finish of Wall Street overnight and yen depreciation against the greenback. Meanwhile, buying spree also encouraged by hopes that Japan's massive public pension fund will boost its stock holdings at home and abroad. The benchmark Nikkei 225 index added 0.29% to finish at 15369.97, while the Topix index of all first-section shares gained 0.37% to 1280.78.

The fresh records in the US equity market lifted the dollar against the yen, a positive factor for shares of Japanese exporters as it boosts their profitability. In currency markets, the dollar bought 101.60 yen, up from 101.53 yen in New York on Tuesday.

Shares of exporters closed higher, with car makers leading rally following the release of the overnight data showing strong U. S. new vehicle sales. Industry-wide auto sales rose 1.2% to 1.4 million in June from a year earlier, pushing annualized sales to 16.98 million, its highest level since July 2006. Toyota said sales rose 3.0% from the previous year in June, while Nissan Motor booked a 5.0% gain. Honda Motor's sales declined 6.0%. But shares of all three gained on the day, with Toyota up 0.6%, Nissan up 1.2% and Honda edging higher by 0.2%. Bridgestone surged 3.6%, benefited from the strong vehicle sales, though its acquisition of a U. S. hose maker was seen as the main catalyst behind a jump in its price. Bridgestone wholly owned U. S. subsidiary, Bridgestone Hose America, had reached an agreement to acquire U.

S.-based Masthead Industries, operator of one of the largest networks specialized in hose sales and service in the U. S.

Shares of shippers climbed up following a Nikkei report that Nippon Yusen and trading house Mitsubishi Corp. will join hands with French energy giant GDF Suez to sell liquefied natural gas as a fuel for ships. Nippon Yusen added 2.0%, while rivals OSK Lines and Kawasaki Kisen gained 1.8% and 0.9%, respectively.

China stocks extend gain on growth optimism

Mainland China share market advanced for third consecutive day, as investors were encouraged by surveys showing China's manufacturing activity improved in June for the first time in six months after the government steps up policy support. The benchmark Shanghai Composite closed 0.44% higher from prior day to 2059.42. Trading turnover increased to 89.30 billion yuan from yesterday's 83.5 billion yuan.

Shares of shipping companies advanced the most in Shanghai market after Pentagon report stating China's military budget will rise 12.2% this year as spending focuses on high-technology weaponry and longer-reach naval and air capacity. The military is improving its training, doctrine, weapons and surveillance to be able to conduct more sophisticated attacks against the U. S. and others. Shipbuilder China CSSC Holdings surged 7.7% to 22.32 yuan. China Shipbuilding Industry Co., the biggest equipment supplier for the navy, advanced 3.2% to 4.90 yuan. China Avic Electronics Co jumped 2.8% to 23.10 yuan.

Property developer stocks declined after poll by real estate services firm E-House China Holdings showed prices of new homes in 288 cities fell 0.06% in June from May, the third month-on-month drop in a row. A separate survey by China Real Estate Index System (CREIS) showed average prices in the 100 biggest cities fell 0.5% in June from May, the second consecutive monthly drop. China Enterprise Co fell 1% to 4.78 yuan. Gemdale Corp, which went ex-dividend, slid 2.7% to 8.76 yuan.

Hang Seng jumps 1.55% on China, US data

Hong Kong share market closed sharp higher, as risk sentiments lifted up on improved U. S. and Chinese economic activity, led by casino and Mainland developers stocks. The benchmark Hang Seng Index closed 358.90 points higher to 23549.62. Trading turnover increased to HK$73.32 billion from Monday's HK$53.37 billion. Hong Kong stock market was shut on Tuesday, 01 July 2014, for the anniversary of the city's return to Chinese rule in 1997.

Shares of casino players advanced as Bank of America Corp.'s Merrill Lynch unit said Macau casino revenue will rebound this month and after Deutsche Bank issued a research report noting that the worst is almost over for Macau gaming sector as World Cup's impact subsides. Macau's casino revenue fell for the first time in five years last month as the World Cup competed for bettors' attention. Total gross gaming revenue dropped 3.7% to 27 billion patacas ($3.4 billion) last month, according to Macau's Gaming Inspection and Coordination Bureau.

Galaxy rose 4.8% to HK$65. SJM Holdings, Asia's biggest casino operator, gained 3.5% to HK$20.10. Sands China, a unit of Las Vegas Sands Corp., climbed 3% to HK$60.30.

Shares of Mainland developers listed in HK advanced after UBS stated in latest report that property sales volume for top 41 Chinese cities rose 13% last week from the previous week. China Overseas Land rose 4.2% to HK$19.58. Shimao Property Holdings Ltd. climbed 3.7% to HK$14.76. Guangzhou R&F Properties Co, a builder in the southern Chinese city, advanced 2.7% to HK$9.83.

Sensex, Nifty hit fresh lifetime high

Indian stock market closed at all-time highs today as sustained foreign fund inflows and buying by retail investors on hopes of strong economic measures in the upcoming Union budget 2014. Optimism that the Narendra Modi-led National Democratic Alliance (NDA) government Government would present its first budget on 10 July that would lead to a reduction in the fiscal and current account deficit. The 30-share BSE index Sensex surged 324.86 points to end at 25,841.21 and the 50-share NSE index Nifty rose 90.45 points to end at 7,725.15.

The trading sentiment was buoyed after Finance Minister Arun Jaitley had yesterday said that the Government would take bold decisions in the forthcoming Budget and pursue the path of fiscal prudence to revive the economy.

All BSE sectoral indices ended significantly in the green. Among them, metal, power, healthcare and capital goods indices were the star-performers and were up 2.01 per cent, 1.93 per cent, 1.88 per cent and 1.67 per cent, respectively.

Among 30-share Sensex constituents, SSLT, NTPC, BHEL, HDFC and Maruti were the major gainers, while GAIL and Infosys were the only two losers.

A2Z Maintenance and Engineering Services rose 5% to Rs.19.95, after it got an order worth Rs.2,400 crore for engineering, procurement and construction (EPC) work against advance purchase order (APO) received by Sterlite Technologies Ltd and ITI Ltd.

Gujarat Gas Co. jumped 8.81% to Rs.539, after its board approved a proposed merger with other state-run companies involved in city gas distribution business.

Sasken Communication Technologies rose 13.75% to Rs.302.05 after it won $31.7 million arbitration award against US-based Spreadtrum Communications Inc. for failure to pay royalties for using Sasken's IP in a joint development project.

Elsewhere in the Asia Pacific region- Taiwan's Taiex index was up 0.46% to 9484.96. South Korea's KOSPI index rose 0.81% to 2015.28. Indonesia's Jakarta Composite Index climbed up 0.48% to 4908.27. Malaysia's KLSE Composite grew 0.41% to 1886.84. Singapore's Straits Times index added 0.55% to 3260.52. New Zealand's NZX50 added 0.06% to 5149.38.

Powered by Capital Market - Live News