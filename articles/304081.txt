O.J. killed Jay Leno’s stand-up career.

Not Simpson himself, of course, but rather the circus spectacle that surrounded the “crime of the century” case following the twin murders in Brentwood, Calif., 20 years ago this month.

The case, to be clear, did give life to Leno’s career as a late-night host. And the comedian, who will receive the 2014 Mark Twain Prize for American Humor from the Kennedy Center, seemingly never stopped doing hundreds of stand-up gigs a year, like the passionate pro and workhorse he is. What Simpson ended up icing –as a demarcation point — was Leno’s widespread and well-earned reputation as one of the sharpest working comics around.

The network suits giveth, the need for Nielsens taketh away.

Leno, of course, took over the prized NBC institution that is “The Tonight Show” — what now, for comedians, can be considered the “show of the half-century” — in the early ’90s. He inherited the chair from the unquestioned king of late night, Johnny Carson, much to the discontent of Leno’s onetime friend, David Letterman.

Leno and Letterman would soon become rivals in the late-night ratings, of course. NBC vs. CBS. And once the Simpson circus changed the 24/7 TV news-cycle forever, Leno smelled ratings blood. The L.A.-based NBC juggernaut had just the ticket to top New York’s “Late Show With David Letterman” for audience share. In a nod to the SoCal case’s camera-loving, autograph-seeking judge, Leno would soon unfurl…

The Dancing Itos.

Leno, as surging ratings champ, never looked back.

To be fair, satirizing Judge Lance Ito’s love of the spotlight — as yet another bizarre sideshow to two brutal murders — by bringing out his wacky Rockettes-in-judicial-robes dancers was kind of an inspired parodic move.

Yet once Jay was top of the heap, his opening monologues seemed blander and his style began to pander. You could just feel the imploring blink of the “Applause” sign, even though you couldn’t see it. Leno’s new job was to win a huge swath of American viewers and keep the affiliates happy and make sure the network suits were still smiling with each “sweeps” period and profit report. To keep the NBC champagne flowing, Leno had to drink from the mainstream.

Too bad it was laced with arsenic. Because what we lost onscreen, at least, was one of the most brilliant comedic minds of his generation. His wry and spiky, sometimes askew style began to recede, as if he were a Louis C.K. character grabbing the big brass ring. The image was buffed, but as his 5-o’clock shadow was shorn, so was his road comic’s “1 a.m.” edge.

As the Kennedy Center announces this morning that Jay Leno is the newest recipient of the Mark Twain Prize for American Humor, some will grumble that there are better recipients to be had — including Letterman, a recent Kennedy Center Honoree, himself. (The retiring Letterman would seem a near-lock for next year if interested.) I think that sells Leno short as a comedy giant who once commanded a massive audience. And for some, there will be the lingering, carping bitterness over how he regrettably handled the Conan O’Brien passing-and-taking-back of the “Tonight Show” torch.

So here’s what I would hope from the Twain Prize producers: Program a show that reminds viewers why Leno was once so great and admired by his comedy peers. And of course, the best man to remind us would be Letterman himself. He could reflect and recall this:

Back in the ’70s, Leno and Letterman were both young and hungry comics on the L.A. scene — the former a Massachusetts transplant, the latter from Indiana. But much to Letterman’s then-stated admiration, it was the confident Leno who was superior at commanding a room, including venues like the Improv and the Comedy Store.

So when Letterman got his own show from NBC just a few years later, whom did he insist upon often booking and featuring like an admirer and friend?

The great and masterful Jay Leno, of course.

When the cameras roll at the Kennedy Center later this year, O Twain producers, please remind the good folks watching why Leno was indeed, undeniably, great.

Cavna, the “Comic Riffs” columnist, was The Post’s TV editor from 2004 to 2010.