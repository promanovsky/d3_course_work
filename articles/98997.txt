Paul Walker is to receive a tribute at the MTV Movie Awards.

The Fast And Furious actor - who was killed in a car accident in November at the age of 40 - will be remembered at the ceremony in a tribute apparently featuring clips of his movie work and a speech from his co-star Jordana Brewster, reported The Los Angeles Times.

MTV president Stephen Friedman said the late star was a "role model" for the MTV audience.

He continued: "Our audience grew up with him, from Varsity Blues to the Furious franchise. He was a movie star, but one they could relate to. Paul's humanity and kindness clearly showed through every role.

"We're honoured to be able to spend a moment in this year's Movie Awards to celebrate him."

Walker is also posthumously nominated for an award at the event - Best Onscreen Duo alongside co-star and close friend Vin Diesel.

The MTV Movie Awards will be held on April 13 at the Nokia Theatre in Los Angeles, and will be hosted by US comedian and talk show host Conan O'Brien.