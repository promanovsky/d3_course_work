A funeral service was held for fashion designer L'Wren Scott in Los Angeles on Tuesday.

"A small private gathering of family and close friends" attended the service at Hollywood Forever Funeral Home, according to a spokesman for Mick Jagger, who was Scott's longtime partner.

It included blessings and prayers led by Reverend Ed Bacon of the All Saints Church in Pasadena, Calif., and words of tribute from Jagger, Scott's brother Randy Bambrough, and others.

Poems were read by Jagger's daughter, Karis, as well as the actress Ellen Barkin, a good friend of Scott's. Jagger's daughter Jade and his grandchildren, Mazie and Zak, read psalms.

"Will the Circle be Unbroken?" was sung by Bernard Fowler with Dave Stewart on guitar.

Scott was found dead on March 17 at her apartment in New York. Her death was ruled a suicide

The Rolling Stones canceled scheduled tour dates in Australia and New Zealand following Scott's death, and Jagger's bandmates have offered him their support. The Stones frontman paid tribute to Scott last week, calling her his "lover and best friend" and adding, "I will never forget her."