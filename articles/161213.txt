Apple just released a new promotional video.

On the surface, it's about how the company is going to take care of the environment. But, digging just a little deeper reveals that this is a mission statement by CEO Tim Cook.

Since Steve Jobs died in October 2011, people have wondered what kind of company Apple would become under Cook. This video, which leads Apple.com right now, is the answer.

It is narrated by Tim Cook:

Here's what he says:

"'Better.' It's a powerful word, and a powerful ideal. It makes us look at the world and want more than anything to change it for the better. To innovate. Improve. To reinvent. To make it ... better. It's in our DNA. And better can't be better if it doesn't consider everything. Our products, our values, and an even stronger commitment to the environment for the future. To use greener materials, less packaging. To do everything we can to keep our products out of landfills, Changes that will benefit people as well as the planet. To us, better is a force of nature. It drives us to build things we never imagined. New data centers powered by the sun and wind. A new manufacturing facility that runs on 100% clean energy. And new product designs that make use of recycled materials. All ways to reduce our impact on the environment. We have a long way to go and a lot to learn. But now more than ever we will work to leave the world better than we found and make the tools that inspire others to do the same."

While Cook mentions the environment, the big picture here is that he wants Apple to produce world-changing products that leave the planet in better condition. This can be in a literal sense like pollution, but also in a more figurative sense, like the iPhone has made millions of lives better.

This video feels like it's Cook's version of "The Crazy Ones" ad from Apple. When Steve Jobs took over Apple, he helped develop an advertisement that showed creative geniuses throughout history. (Full story on it here.)

Interestingly, Jobs did a voice-over for the video, but chose actor Richard Dreyfuss for the final cut. We're not sure why he didn't use his voice. You can see the video with Jobs' voice here, and it feels like a statement about Apple, which at the time was in trouble.

Apple isn't in the same trouble, but it's clearly in a transition. Cook's video, with his voice, feels like a big statement.

Here's "The Crazy Ones":