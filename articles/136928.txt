New figures reveal that more than 50 million Android smartphones and tablets are at risk of attack by the reverse heartbleed bug.

According to data supplied to IBTimes UK by mobile security experts Lookout, more than 95% of the people who used their Heartbleed Detector app were told the version of Android they were using was safe from attacks exploiting the Heartbleed Bug.

While that is good news for most Android smartphone and tablet users, when you consider there have been more then one billion official Google devices activated, there are potentially millions of devices out there vulnerable to attack.

The Heartbleed Bug was uncovered last week as a flaw in the code of OpenSSL, a software library which helps encrypt online communication. Google revealed that a specific version of its mobile operating system - Android 4.1.1 (Jelly Bean) - used the vulnerable version of OpenSSL.

While the primary attack vector for those exploiting the heartbleed bug was to snoop on traffic coming to and from vulnerable websites, the reverse heartbleed bug used the same flawed code let criminals target specific devices - such as Android smartphones and tablets.

UK users more vulnerable

Lookout revealed that Android users in the UK were more than twice as likely (7%) to be vulnerable to a reverse heartbleed attack than those in the US (3%) while German users were the most vulnerable with 8.56% of all devices checked open to a heartbleed attack.

Putting a definitive number on the amount of smartphones and tablets at risk is difficult, as Google doesn't break down statistics for what versions of Android are in use beyond 4.1.x - which is currently the most popular version of Android, being used on over 34% of active Android devices.

However US online advertising network Chitika has given us some indication of the amount of devices currently using the vulnerable Android 4.1.1.

The latest figures show that just 19% of devices visiting web sites with a version of Android 4.1 installed were using the vulnerable 4.1.1 software. Overall this represents just 4% of total US Android web traffic.

But how many devices does that represent? Well according to ComScore, the current install base of Android devices in the US is 85 million, meaning that around 4 million smartphones and tablets are vulnerable.

Going global

Extrapolating that globally is difficult as the Lookout figures show vulnerability rates are much higher in other parts of the world.

It is also difficult to get a exact figure for active Android devices globally, but research published last November by BI Intelligence suggested that 72% of the 1.5 billion smartphones in use globally are using Google's operating system, putting the the install base for Android smartphones at just over one billion.

If we then take a median of the vulnerability rates reported by Lookout, say 5%, we are looking at 50 million smartphones which are vulnerable to a reverse heartbleed attack.

Android tablets

Looking at tablets, the situation is similar. It is very difficult to get an accurate idea of what the install base of Android tablets is.

In July of last year, mobile analyst Benedict Evans estimated that the number of active Android tablets in use was around 70 million. Clearly that figure will have risen since then and in December, ABI Research claimed the install base for branded tablets would be 285 million by the end of 2013.

With Android tablet sales now matching and possibly surpassing those of Apple, it is safe to suggest that at least 100 million Android tablets are in use today globally.

Taking the same median figure from Lookout's app reports, it would suggest somewhere around 5 million Android tablets are likely to be open to this reverse heartbleed exploit.

Unofficial figures

These calculations of course don't take into account devices like the Kindle Fire or white label smartphones and tablets from Chinese manufacturers which don't connect to Google's services and are therefore not officially recorded.

Many of these devices are however likely to be running the vulnerable version of Android and as they are not official Google devices, they are unlikely to ever be patched.

Google released Android 4.1 (Jelly Bean) back in July 2012 alongside the launch of the original Nexus 7 tablet, and since then has issued two updates (Android 4.1.1 and Android 4.1.2)

The Android 4.1.1 update was issued just weeks after Android 4.1 was launched, to fix a specific problem with the Nexus 7 tablet, relating to the inability to change screen orientation in any application.

Google has yet to give a definitive date for when it expects to update its Android software to patch the affected version.