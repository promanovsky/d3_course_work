Cuba appears at the top of this high oblique image, photographed by one of the Expedition 38 crew members aboard the International Space Station. (Source: NASA.gov)

CAPE CANAVERAL (CBSMiami/AP) — While they weren’t delivered by the Easter bunny, the crew of the International Space Station did get some goodies Sunday morning.

A cargo ship full of supplies.

The shipment arrived via a Dragon, versus a bunny.

The SpaceX company’s cargo ship, Dragon, spent two days chasing the International Space Station following its launch from Cape Canaveral. Astronauts used a robot arm to capture the capsule 260 miles above Egypt.

More than 2 tons of food, spacewalking gear and experiments fill the Dragon, including mating fruit flies, a little veggie hothouse and legs for the resident robot. NASA also packed family care packages for the six spacemen.

On Wednesday, the stakes will be even higher when the two Americans on board conduct a spacewalk to replace a dead computer. NASA wants a reliable backup in place as soon as possible, even though the primary computer is working fine. The backup failed April 11.

The SpaceX delivery wasn’t exactly express. The launch was delayed more than a month. A minor communication problem cropped up during Sunday’s rendezvous, but the capture still took place on time and with success.

SpaceX flight controllers, at company headquarters in Hawthorne, Calif., exchanged high-fives, shook hands, applauded and embraced once Japanese astronaut Koichi Wakata snared the Dragon with the station’s hefty robot arm.

“Great work catching the Dragon,” radioed NASA’s Mission Control in Houston. “Thanks for getting her on board.”

The capsule was solid and stable for grabbing, Wakata reported, making the job easy. He congratulated the SpaceX team and added, “we’re excited.”

The Dragon will remain attached to the space station until mid-May. It will be filled with science samples — including the flies — for return to Earth.

NASA is paying SpaceX as well as Virginia’s Orbital Sciences Corp. to regularly stock the orbiting lab. These commercial shipments stemmed from the 2011 retirement of the space shuttles. This was the fourth station delivery for SpaceX.

Russia, Japan and Europe also make occasional deliveries.

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)