Google will begin selling a basic version of its Project Ara modular smartphone in January, the company said this week.

Project Ara is an effort by Google to make a smartphone that consumers can customize and upgrade by easily switching modules in and out. Each module will contain different parts of a smartphone, such as a battery, a camera, a GPS chip and others kinds of device components.

To read and comment on this story, go here.