Freddie Prinze Jr. says that he contemplated quitting acting after appearing on Fox's hit show "24." During a recent interview with ABC News, Prinze not only slammed the show but also claimed the star Kiefer Sutherland was nearly impossible to work with.

"I did '24,' it was terrible. I hated every moment of it," Prinze said in the interview. "Kiefer was the most unprofessional dude in the world. That's not me talking trash, I'd say it to his face. I think everyone that's worked with him has said that."

He went on to explain just one of his issues with Sutherland saying:

"I just wanted to quit the after that. So, I just sort of stopped. I went and worked for Vince McMahon at the WWE for Christ's sake . . . At least he was cool and tall. I didn't have to take my shoes off to do scenes with him, which they made me do (with Sutherland). Just put the guy on an apple box or don't hire me next time. You know I'm 6 feet and he's 5'4."

For comments and feedback contact: editorial@rttnews.com

Entertainment News