A NINE-year-old girl, an avid water skier and swimmer, has died after a rare amoeba found only in freshwater entered through her nose and ate her brain.

Hally Yust, from the city of Spring Hill, Kansas, contracted the extremely rare brain-eating amoeba, Naegleria fowleri, which is found in lakes, rivers and hot springs, Fox News 4 reported.

“Our precious daughter, Hally, loved life and part of her great joy was spending time playing in the water,” said her mother Jenny in a statement.

“Her life was taken by a rare amoeba organism that grows in many different fresh water settings. We want you to know this tragic event is very, very rare, and this is not something to become fearful about.

“We hope you will not live in fear of this rare infection that took our daughter’s life. Our family is very active in water sports, and we will continue to be.”

Hally’s family is uncertain where she was infected as she had swum in several lakes located near where they live over a two-week time period before she fell ill.

There have been fewer than 200 cases of Naegleria fowleri — also known as primary amoebic meningoencephalitis (PAM) — in the US in more than 50 years. Hally’s case is only the second recorded in Kansas.

“The amoeba goes up through the nose and into the brain and once it’s there, there’s really nothing anybody can do. There’s only been one case that actually lived through this. All the other cases have passed away,” said Tiffany Geiger, the investigator with Johnson County Health Department.

Geiger said while the chances of contracting the brain-eating amoeba are very low. Wearing noseplugs when swimming, skiing or doing other freshwater activities lessens the chance.

The risk increases in the summer with warmer water temperatures.

Symptoms include headache, fever, nausea, stiff neck and confusion and usually happen about five days after contracting the amoeba.

The health department says infection cannot be spread from person to person, and you cannot get it from a swimming pool which is properly maintained.

A scholarship fund has been set up in honour of Hally — whose funeral service is being held today, to provide educational opportunities for young women who love basketball as their daughter did.

The Hally “Bug” Yust K-State Women’s Basketball Scholarship, Ahearn Fund, 1800 College Ave., Suite 138, Manhattan, Kansas 66502