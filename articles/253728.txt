Apple Inc. (NASDAQ:AAPL) is all set to release the new mobile operating system iOS 8 at the Worldwide Developers Conference on June 2. The tech giant confirmed in its revised schedule that the two-hour keynote, which begins at 10 AM PST (1 PM EDT), will include details on the updates for the iOS and OS X. With the release date approaching, many existing iDevice users want to know whether the iOS 8 will support their device or they will have to upgrade their new hardware to make a software upgrade.

The current iOS 7 supports the iPhone 4 and later, iPod touch 5G and the iPad mini 1 and later. With each new OS update, Apple Inc. (NASDAQ:AAPL) drops support for one or two older models. So, let’s see which deviecs are likely to get the iOS 8 support.

Which iPhone models will support the iOS 8?

History of the iOS suggests that one model has been dropped every one or two versions of the mobile software. The iPhone 4 supports a toned down version of the iOS 7. And this device has been withdrawn from most of the markets, including Brazil and India. So, it’s highly likely that the iOS 8 won’t support the iPhone 4. Cupertino may provide support to he 4S with a toned down version. MacWorld says that the new operating system will support the iPhone 4S, 5, 5S, 5C, and of course, the iPhone 6 when it is announced.

iOS 8 support for iPads

Apple Inc. (NASDAQ:AAPL) has been slow in dropping the iPad support. The first iPad was launched when the iPhone OS 3 was the current OS, but it was able to run the iOS 5. Apple has discontinued the iPad 2, so it may be removed from the iOS 8 support. The new OS is likely to be compatible with the iPad 3 and later versions. There are chances that the first iPad Mini model may not get the iOS 8 support. But then there are just two mini versions available to consumers and the device is still on sale, so it maybe able to run the new OS.

Which iPod touch versions will support the iOS 8?

The iOS 7 supports the iPod touch 5G. The sixth generation iPod touch isn’t available in the market yet. Apple Inc. (NASDAQ:AAPL) may unveil the sixth generation iPod touch alongside the iOS 8 at WWDC. It’s likely that the new operating system will support the 5th-generation iPod touch and any new model launched later this year.