Businesses and fisheries in Alaska, including the delicacy of the king crab, could be at risk because of man-made carbon dioxide emissions, a recently released report said.



According to the study by the National Oceanic and Atmospheric Administration titled "Ocean Acidification Risk Assessment for Alaska's Fishery Sector," many marine organisms important to Alaska's economy are being harmed by increased levels of ocean acidification. Part of this is due to the release of carbon dioxide from various technologies such as power plant smokestacks, cars' tailpipes and burning fossil fuels, The Associated Press reported.

"Many of the marine organisms that are most intensely affected by ocean acidification (OA) contribute substantially to the state's commercial fisheries and traditional subsistence way of life. ... Other economic and social impacts, such as changes in food security or livelihoods, are also likely to result from climate change," the report said.

As a result, shellfish like mollusks are at risk because the acidification causes the creatures to have difficulty building and maintaining their shells. Meanwhile, king crabs and tanner crabs are known to grow slower in acidic water, and king crabs have even died in very acidic water, according to earlier studies cited by AP.

Ocean acidification is especially threatening to those in the southeast and southwest regions of Alaska because of their heavier dependence on fishing than other Alaskan areas, the report said.

"The analysis showed that regions in southeast and southwest Alaska that are highly reliant on fishery harvests and have relatively lower incomes and employment alternatives likely face the highest risk from OA," the report said. "Although this study is an intermediate step toward our full understanding, the results presented here show that OA merits consideration in policy planning, as it may represent another challenge to Alaskan communities, some of which are already under acute socio-economic strains."

Jeremy Mathis, co-lead author of the study and NOAA oceanographer, told AP that oceans are around 30 percent more acidic today than when the Industrial Revolution started.

"We could have a 300 percent greater change between now and the end of the century than we have in the past 250 years combined," he said. "So the rate of change is what's accelerating."

---

Follow Scharon Harding on Twitter: @ScharHar.