In the nearly year and a half since I have been investigating America's broken mental health system – even with my 30-year background in clinical psychology – I have been shocked to learn just how much our country has failed those with severe mental illness.

Take Elliot Rodger, a 22-year-old whose instability was known, but went overlooked before he killed six college students and himself in California over the weekend. Or take Gus Deeds, another young man who was in mental health crisis but was denied extended inpatient care before he killed himself and stabbed his father, a Virginia state senator. There was Adam Lanza in Connecticut, Jared Loughner in Tucson, James Holmes in Aurora, Aaron Alexis at the Washington Navy Yard, and on and on.

All had untreated or undertreated serious mental illness. All spiraled out of control within a system that lacked the basic mechanisms to help.

While these are extreme cases, they highlight how our broken system does not respond until after a crisis when we could be doing something to stop it from happening. But even in the face of tragedy, we are too uncomfortable to acknowledge the facts because the last bastion of stigma in mental health concerns those with serious mental illness.

The facts are that mental illness is a brain disease, and of the 9.6m people in this country with a serious mental illness – like schizophrenia, bipolar disorder or major clinical depression – approximately 40% won't even receive treatment this year.

We've found it easier to focus both the discussion and public resources on gauzy programs for "behavioral wellness" and "emotional well-being" than to confront the painful reality that those with schizophrenia or severe psychosis are more likely to up without care (4.4m), homeless (250,000), in prison or on parole (1.3m), or dead by or attempting suicide (1.38m) than in appropriate psychiatric treatment (approximately 4m).

The time has come that we approach serious mental illness as a medical emergency demanding an approach best described as "crisis psychiatry". That's the basis for a bill I first introduced six months ago, the Helping Families in Mental Health Crisis Act. My legislation fixes the shortage of psychiatric hospital beds, clarifies HIPAA privacy laws so families are part of frontline care delivery team, and helps patients get treatment well before their illness spirals into crisis.

Unlike newer, more limited legislation that lacks bipartisan support, my bill represents a comprehensive and wide-ranging overhaul, delivering acute psychiatric care to the most vulnerable. None of the tough issues surrounding serious mental illness are taken up in a competing measure offered by Rep Ron Barber of Arizona.

Putting more money into the current failed system will do nothing to prevent the next Elliot Rodger, nor will it help the millions of families caring for a loved one with an acute untreated psychiatric illness who refuses treatment and lacks insight into their condition.



And most assuredly, maintaining the status quo will only serve the financial interests of legal advocates and anti-psychiatry activists who get federal taxpayer dollars to actually stop patients from getting treatment and direct them into the criminal justice system or homelessness.

The status quo is not just uncompassionate; it is inhumane.

Now is the time for courage before another tragedy unfolds. For the millions of families in mental health crisis, we must have the courage to reject the failed status quo and offer real solutions to help SMI patients get life-saving treatment and recover. When it comes to saving lives, there can be no compromise.

Rep Tim Murphy is a Republican US Representative from Pennsylvania and a practicing psychologist.

• Interactive report: America's mental health care crisis, part one – families left to fill the void of a broken system