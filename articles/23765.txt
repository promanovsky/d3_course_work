Ohio State University is one of the largest colleges in America with over 50,000 students. When people think of the Buckeyes, they are reminded of our strong school spirit, The Shoe, Mirror Lake and so much more. Being part of amazing traditions year after year is something to be grateful for. OSU is an extremely diverse campus having half of their students being from different cities in Ohio and the other half being from all over the world. You have students from the west coast, the east cost, down south and over 100 different countries. Since I am fortunate to be a part of such a diverse campus, it makes my job as a style guru that much easier.



Although Columbus isn't known for its fashion, many people will be pleasantly surprised to hear it is one of the chicest cities in America today. Victoria's Secret, The Limited and Abercrombie & Fitch and Express have headquarters located in Columbus. If you ever visit Columbus, it's definitely worthwhile to do some shopping in the city. Columbus's Easton Town Center has many high-end lavish stores and High Street in the Short North District has many little boutiques and trendy clothing shops.



One of my favorite parts of being located in the Midwest is that we get to experience all four seasons. Fashion trends and styles are always changing. My favorite season in Columbus is winter. Not only is the campus absolutely beautiful in the snow, but the cold weather allows you to truly express yourself with no limits to the amount of accessories you can wear.



As the cold weather starts to drift away, students all over campus are starting to get excited and show off their spring apparel. It's clear that this fashionista is ready for the warm, spring weather to finally appear. Her black, loose fitting pants were the perfect pick to either dress up or down. She tucked in a white silk tank top to her look. This is the ideal outfit to wear for a spring shopping day in downtown Columbus. Her Steven Madden black flats added some edginess to her look but still ensured she will be comfortable. Keeping her outfit simple, she added a Hermes bracelet and a gold necklace. Although every student at OSU has a completely different style from one another, this fashionista definitely shows one side of what student's wear around Columbus.