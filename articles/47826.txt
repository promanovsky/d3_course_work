General Motors Co. Chief Executive Mary Barra took to video again to address a handful of customer questions--such as, "Is my car safe to drive?"--that have surfaced in the wake of the auto maker's faulty ignition switch recall.

"The simple answer is yes," Ms. Barra said in response to the question on whether the recalled vehicles are still safe to drive. "GM engineers have done extensive analysis to make sure if only you have the key or only the key on a ring, the vehicle is safe to drive. In fact, when they presented this to me, the very first question I asked is would you let your family, your spouse, your children drive these vehicles in this condition and they said yes."

The auto maker has been using blogs and video to keep Ms. Barra in controlled public appearances while legal and legislative pressures continue to mount. Ms. Barra is slated to testify in congressional hearings next week and explain why the auto maker took nearly a decade to recall 1.6 million vehicles--1.3 million in the U.S.--for faulty ignition switches that can slip from the "on" to the "accessory" position thereby cutting power to the steering, brakes and air bags. Twelve deaths have been linked to the switch issue.

The safe to drive question has also grown in importance after two legal firms in Texas have asked federal courts to direct GM to issue a "park it" warning to all drivers of older Chevrolet Cobalts and other vehicles covered in the recall. Currently, the company is offering rental cars to consumers who are concerned with their safety and don't want to drive their vehicles until the problem is fixed. Repairs are expected to start until after April 7.

As to why the delay in recalling calls for a problem the company first detected in 2004, Ms. Barra didn't provide a straight answer. She instead focused on the company's need to make improvements.

"Clearly that it took over 10 years indicates we have work to do to improve our process," Ms. Barra said. "We are dedicated to doing that and that is why we hired [Chicago attorney] Anton Valukas who is a former U.S. district attorney to help us investigate this so we can get ever lesson learned. I can commit to you that we will put all these processes and lessons into place to make sure this never happens again."

The video postings come the same day Nissan Motor Co. said it is recalling almost 990,000 cars, sport-utility vehicles and minivans in the U.S. because the front passenger air bags may not inflate in a crash. The recall affects the Altima, Leaf, Pathfinder and Sentra models from the 2013 and 2014 model years, as well as the NV200 Taxi van and Infiniti JX35 SUV from 2013. Also covered is the Infiniti QX50 SUV from 2014.

The company told the National Highway Traffic Safety Administration that the vehicles' computer software may not detect an adult in the passenger seat. If that happens, the air bags won't inflate. Nissan will notify owners and dealers will update the software for free. The recall is expected to start in mid-April.

Write to Jeff Bennett at jeff.bennett@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires