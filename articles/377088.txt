Dublin-headquartered SMBC Aviation Capital is to buy 115 aircraft from Airbus, in a deal with a list value of $11.8bn.

The aircraft leasing company, which was established in 2012, will buy 110 A320neo and five A320ceo aircraft as part of the deal.

As part of the deal, the company has the option to convert most of the aircraft to the larger A321 model if it wishes.

SMBC Aviation Capital was formed in 2012 after Royal Bank of Scotland Group sold its leasing subsidiary to Japan’s Sumitomo Mitsui Banking Corporation.

The company is the third largest aircraft leasing firm in the world and already operates a fleet of over 350 craft.

It is headquartered in Dublin’s IFSC and has offices in Asia, the US and across Europe.

Meanwhile another Dublin-based aircraft leasing company, Avolon, has committed to buy 15 Airbus A330neo aircraft.

The deal has a list value of $4.1bn and will bring to 222 the number of aircraft owned and operated by the company.