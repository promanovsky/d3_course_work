It looks like the Nokia Lumia 2520 Windows 8.1 RT tablet is headed to mobile carrier T-Mobile in the US, as a photo have been leaked which shows the device in some T-Mobile press material, the tablet has been available in the U.S. for a while now.

The Lumia 2520 comes with Microsoft’s Windows 8.1 RT, and the device features a 10.1 inch full HD display that has a resolution of 1920 x 1080 pixels.

Other specifications on the Nokia Lumia 2520 include a quad core Qualcomm Snapdragon 800 processor which is clocked at 2.2GHz. The device also features 2GB of RAM and 32GB of built in storage, plus a microSD card slot that supports up to 32GB cards.

On top of that the Nokia Lumia 2520 also comes with a 6.7 megapixel rear facing camera, and the device is also equipped with an 8,000 mAh battery which will give you up to 10 hours of usage., plus 4G LTE, WiFi and Bluetooth.

As yet we do not have any details on when the Lumia 2520 Windows 8.1 RT tablet will be available on T-Mobile, as soon as we get some more information, including pricing on the device, we will let you guys know.

Source @evleaks

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more