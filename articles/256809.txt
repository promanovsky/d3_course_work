Promising to tear down the wall that separates notebook computers and leading tablets is Microsoft Surface Pro 3 tablet unveiled by the company on May 20.

Microsoft announced the newest addition to the Surface family in a blog post.

“It’s the tablet that can replace your laptop. It is the most powerful, thinnest, and lightest Surface Pro yet. It is a full PC and a brilliant tablet,” writes Panos Panay, corporate vice president at Microsoft Surface.

Panay explains that many people, apparently, are not ready yet to give up their computers or laptops for handy tablets, while other people have both devices. Which is why Microsoft decided on a design that “can marry the power of the full PC without compromising the sleek finish, elegant look and feel, light weight, thinness, and great battery life that we expect from a tablet.”

It appears though that Microsoft Surface Pro 3 is being put in competition against Apple products such as the iPad and MacBook Air. Based on a few reviews gathered, Microsoft’s latest baby is a killer. Collated reviews enumerated key points for comparison between Surface Pro 3 and iPad or MacBook Air.

Application-performance wise, Surface Pro 3 was found to be equal with Yoga Pro 2 of Lenovo and 13-inch MacBook Air of Apple.

It says Surface Pro 3 can be used with keyboard, stylus pen and touch inputs, packing all together how the iPad and MacBook Air can function. Surface’s keyboard attachment was said to be way ahead that any of the third-party keyboard attachments for iPad. The stylus pen was meanwhile described as an awesome support for those creative individuals indulging in painting and drawing activities on the screen, as compared to iPad and Macbook Air that have no stylus pen for use.

In terms of weight of the device, the Surface Pro 3 is said to be far slimmer and lighter than MacBook Air and Ultrabooks. MacBook Air’s 2.96 pounds is somewhat popular among students, but Pro 3 can compete with its 2.4 pounds weight including the cover to think that it can function like a complete PC.

Microsoft Surface Pro 3 carries the Office productivity suite for its fanatics. Though iPad has its own Office version, the Pro 3 assures no compatibility issues since it is basically a personal computer slightly tweaked in design.

Photographers are sure to love the Surface Pro 3 as well. Besides the 12-inch display of Pro 3 with 3:2 aspect ratio, the large screen is perfectly suited for photo editing. The resolution of 2160 x 1440 is a great catch as well over MacBook Air’s own screen display. Since Pro 3 is half pound lighter than MacBook Air, stuffing the former in the photo bag is easy breezy too.

Here’s the upside for Apple and downside for Microsoft. Apple’s MacBook Air and iPad tablet have better battery life that lasts up to 12 hours all day, while Microsoft Surface Pro 3 can only last for nine hours of web browsing. Now, if the consumer finds battery a huge necessity in its productivity, then that could end up as a deciding factor when purchasing.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.