MICROS Systems (NASDAQ: MCRS) shares moved up 3.33% to $67.96. The volume of MICROS shares traded was 6048% higher than normal. Oracle (NYSE: ORCL) announced its plans to buy Micros in a $5.3 billion deal.

Integrys Energy Group (NYSE: TEG) shares rose 14.37% to $69.71. The volume of Integrys Energy shares traded was 1228% higher than normal. Wisconsin Energy (NYSE: WEC) announced its plans to acquire Integrys Energy Group in a deal valued at $9.1 billion.

Tonix Pharmaceuticals Holding (NASDAQ: TNXP) surged 9.10% to $13.55. The volume of Tonix Pharmaceuticals shares traded 396% higher than normal. Tonix shares have jumped 84.00% over the past 52 weeks, while the S&P 500 index has gained 24.78% in the same period.

Rockwell Medical (NASDAQ: RMTI) shares climbed 5.93% to $11.79. The volume of Rockwell Medical shares traded was 317% higher than normal. Rockwell Medical announced the FDA approval for Calcitriol.