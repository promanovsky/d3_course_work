Under pressure from Congress, celebrity Dr. Mehmet Oz on Tuesday offered to help "drain the swamp" of unscrupulous marketers using his name to peddle so-called miracle pills and cure-alls to millions of Americans desperate to lose weight.

Oz appeared before the Senate's consumer protection panel and was scolded by Chairman Claire McCaskill for claims he made about weight-loss aids on his TV show, "The Dr. Oz Show."

Oz, a cardiothoracic surgeon, acknowledged that his language about green coffee and other supplements has been "flowery" and promised to publish a list of specific products he thinks can help America shed pounds and get healthy — beyond eating less and moving more. On his show, he never endorsed specific companies or brands but more generally praised some supplements as fat busters.

McCaskill took Oz to task for a 2012 show in which he proclaimed that green coffee extract was a "magic weight loss cure for every body type."

"I get that you do a lot of good on your show," McCaskill told Oz, "but I don't get why you need to say this stuff because you know it's not true."

Oz insisted he believes in the supplements he talks about on his show as short-term crutches, and even has his family try them. But there's no long-term miracle pill out there without diet and exercise, he said.

Within weeks of Oz's comments about green coffee — which refers to the unroasted seeds or beans of coffee — a Florida-based operation began marketing a dietary supplement called Pure Green Coffee, with claims that the chlorogenic acid found in the coffee beans could help people lose 17 pounds and cut body fat by 16 percent in 22 weeks.

The company, according to federal regulators, featured footage from "The Dr. Oz Show," to sell its supplement. Oz has no association with the company and received no money from sales.

Last month, the Federal Trade Commission sued the sellers behind Pure Green Coffee and accused them of making bogus claims and deceiving consumers.

The weight-loss industry is an area where consumers are particularly vulnerable to fraud, Mary Koelbel Engle, an associate director at the FTC, testified at the Senate hearing. She said the agency conducted a consumer survey in 2011 and found that more consumers were victims of fraudulent weight-loss products than of any of the other specific frauds covered in the survey.

Oz stressed during the hearing that he has never endorsed specific supplements or received money from the sale of supplements. Nor has he allowed his image to be used in ads for supplements, he said.

"If you see my name, face or show in any type of ad, email, or other circumstance," Oz testified, "it's illegal" — and not anything he has endorsed.