The net neutrality fight will enter a new phase on Tuesday when the embattled head of the Federal Communications Commission faces questioning from Congress over his controversial proposal to revamp how the Internet works.

FCC Chairman Tom Wheeler's solo appearance before the House subcommittee on communications and technology comes just days after the commission voted 3 to 2 to move forward with proposed rules that would allow Internet providers to charge Web companies like Netflix for faster access to customers. The proposed rules sparked a firestorm of criticism from advocates and tech companies who believe a so-called "two-lane Internet" would violate the popular notion of net neutrality, or the idea that all Web traffic should be treated equally.

Internet providers aren't pleased with Wheeler's proposal either, since it leaves open the possibility of tighter regulations of their services.

The proposal is now subject to a four-month period of public comment.

Here are some things to watch for at Tuesday's hearing:

1. The ‘R’ word

Lawmakers will likely ask Wheeler about one especially controversial question in his proposal: Should Internet providers be regulated? Wheeler's proposal asks whether the FCC should increase regulation of broadband providers by reclassifying high-speed Internet as a utility like phone, electric and gas service. Democrats argue regulations are the only way to ensure such companies keep Internet traffic neutral. But cable companies and some Republicans oppose any type of new oversight, arguing that it would hurt investment in their networks.

Rep. Greg Walden, a Republican who chairs the subcommittee holding Tuesday's hearing, has called the question of whether to reclassify broadband in order to regulate Internet providers “misguided.”

Many will be watching Tuesday to see how Wheeler responds to questions about the issue. While his proposal suggests reclassification is possible, “it's immensely unlikely that he's willing to go the extra mile such a push would require,” Karl Bode, a telecom analyst, wrote last week on his blog DSL Reports.

2. Some clarity on what “commercially reasonable” means

Wheeler’s plan would allow Internet providers to charge companies for access to a fast lane. But Wheeler has insisted that the proposed rules would not put smaller companies at a disadvantage.

"Personally, I don't like the idea that the Internet could become divided into 'haves' and 'have-nots,'" Wheeler said last week, according to the Wall Street Journal. "I will work to see that doesn't happen."

Wheeler has said the FCC will review such deals to ensure they are “commercially reasonable.” Many have said this standard is too vague. Last week, Wheeler offered an example: If an Internet provider slowed a customer’s broadband service, "it would be commercially unreasonable, and therefore prohibited," he said.

People will be watching Tuesday for more clues from Wheeler about what this standard would look like.

3. Money talking

While Internet providers are in favor of charging extra for faster access to customers, tech companies oppose this idea. Both sides have made contributions to lawmakers who will be asking questions to Wheeler Tuesday.

Walden received $10,000 from Google for his 2012 re-election campaign, according to The New York Times.

But Internet providers have also contributed to Walden. He has received $109,250 over the last two years from the cable industry -- more than any other member of the House of Representatives, according to Maplight, a nonpartisan research organization that compiles data on campaign contributions.

Walden and others who received campaign contributions from the cable industry have expressed their concerns with Wheeler's proposal. The 28 lawmakers who signed letters last week to the FCC opposing the idea of regulating Internet providers have received, on average, $26,832 from the cable industry -- more than twice the average for all members of the House of Representatives, according to Maplight.

4. Details on wireless

Wheeler’s net neutrality proposal focuses mostly on wired Internet service and very little on an increasingly popular way that people are getting online -- mobile wireless networks.

In many rural areas, for example, residents can only access the Internet via wireless because cable companies are unwilling to serve them. Several lawmakers at Tuesday's hearing represent rural parts of the country.

Yet Wheeler’s proposal “won't seriously cover" wireless Internet, Bode notes.

For now, the FCC has said wireless should be subject to different rules than wired Internet. Christina Warren, a senior tech analyst for the tech website Mashable, calls that "a mistake."

For many people, advancements in 4G technology will mean their wireless speeds will soon eclipse the speeds of their wired connections, Warren wrote in a column on the site last week.

She called mobile broadband a “hugely important aspect of the entire net neutrality discussion."