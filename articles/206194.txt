Hugh Jackman said this weekend that he’s in good health, after revealing a recent skin cancer operation via an Instagram selfie.

The actor had a basal-cell carcinoma removed two days before attending the New York premiere of his new movie, X-Men: Days of Future Past, on Saturday.

“Get checkups, wear sunscreen,” Jackman said by way of offering health tips, the Associated Press reports.

After the Australian actor was diagnosed with the same form of cancer last November, he told the Today show that basal-cell carcinoma is less severe than melanoma but noted that it “does grow, and there are complications the bigger it is.”

Australia has the highest rate of skin cancer in the world, with approximately two out of three Australians diagnosed before the age of 70, according to the country’s national health department.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Nolan Feeney at nolan.feeney@time.com.