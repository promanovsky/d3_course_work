Miranda Lambert has a new album to promote. Platinum comes out on Tuesday, so the country singer is currently doing the obligatory press rounds and digging deep into her personal life in the process. In one interview, Lambert tells USA Today all about her marriage to Blake Shelton and how the couple deal with life in the spotlight.

"Even if it's people you know, sometimes you need to cement it," Lambert says. "It's just like, 'No one come over; we're having us-time for a second.' We haven't seen each other, and we need to be like, 'Hey, how was the last five days?' It just gets too crowded sometimes."



Lambert collaborated with Carrie Underwood on one song off Platinum.

Lambert likes to keep her personal life away from the media, but she does enjoy riffing on the pregnancy rumors – which, for the record, are completely false, says the singer. And as for the anorexia rumors, those aren’t true either.

"I'm not anorexic. I'm not sure if I'm not pregnant, because I'm supposed to be having an alien child. That's the next headline,” she jokes in her USA Today interview.

Speaking to Us Weekly, she delves deeper into the story of her weight loss, although apparently there isn’t much to tell.



She might be carrying an alien as far as the tabloids are concerned says Lambert.

“I've always been happy with myself, Lambert says. “But when I hit 30, I thought, It's not going to get easier. So I chose to get ahead of it. There's still nothing I love more than a bag of Cheetos, though!”

There’s a reason the country belle is so candid in interviews. For Lambert, her personal life isn’t just tabloid fodder – it’s the subject of her music as well. The relationship, the weight issues – all of it makes an appearance on Platinum.

“Every song talks about who I am as a person right now in my life,” Lambert tells Radio.com. “It’s kind of a lifestyle album.”

More: Miranda Lambert Dreams Of Beyonce Duet