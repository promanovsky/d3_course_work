BUENOS AIRES, Argentina -- Argentina's president is refusing to go along with a U.S. judge's ruling requiring a $1.5 billion repayment of defaulted bonds, even though the U.S. Supreme Court rejected her government's appeals and left the order in place.

In a national address Monday night, Cristina Fernandez repeatedly vowed not to submit to "extortion," and said she had been working on ways to keep Argentina's commitments to other creditors despite the threat of losing use of the U.S. financial system.

Her hard line came hours after the justices in Washington refused to hear Argentina's appeal, and it could be a last effort to gain leverage ahead of a negotiated solution that both sides say they want. But with only days before a huge debt payment ordered by the court is due, many economists, analysts and politicians said the country's already fragile economy could be deeply harmed if she didn't immediately resolve the dispute.

Argentina's Economy Minister Axel Kicillof will hold a press conference at 6 p.m. local time (5 p.m. EDT) to explain the Supreme Court's decision and how Argentina's government will proceed.

Refusing to comply with rulings that have been allowed to stand by the U.S. Supreme Court "would be very damaging to the Argentine economy in the near future," said Miguel Kiguel, a former deputy finance minister and World Bank economist in the 1990s who runs the Econviews consulting firm in Buenos Aires.

Argentine markets were already reflecting fear. The Merval stock index dropped 10 per cent after Monday's court decision, its largest one-day loss in more than six months, and the value of Argentina's currency plunged 3 per cent on the black market. But on Tuesday, Argentina's official peso currency opened stable at 8.14 against the U.S. dollar. The black market peso also remained nearly unchanged trading between 11.85 and 12 against the dollar. The Merval index was down 2.28 per cent in opening trade. The Edenor Argentine electricity distribution company, was trading 6.28 lower after plunging by 20 per cent a day earlier on the Supreme Court's decision.

Fernandez urged her countrymen to "remain tranquil" in the days ahead.

Bowing to the U.S. courts would force her to betray a core value that she and her late husband and predecessor, Nestor Kirchner, promoted since they took over the government in 2003: Argentina must maintain its sovereignty and economic independence at any cost.

But a chorus of analysts said that if she complied with the ruling, it would become much easier for Argentina to borrow again, rebuilding its reserves and preventing the recession from getting even deeper.

U.S. District Court Judge Thomas Griesa order requires that $1.5 billion be paid "all together, without quotas, right away, now, in cash, ahead of all the rest" of bondholders, Fernandez said.

"This represents a profit of 1,608 per cent, in dollars!" she complained. "I believe that in all of organized crime there has never been a case of a profit of 1,608 per cent in such a short time."

But Fernandez also said repeatedly that her government is ready to negotiate with the "speculators" who scooped up Argentine junk bonds after the country's 2001 default. Owners of more than 92 per cent of the nearly worthless debt agreed to accept new bonds worth much less than their original face value, but investors led by New York billionaire Paul Singer held out and litigated instead, seeking to force Argentina to pay cash in full plus interest.

Singer's NML Capital Ltd. has now won in the U.S. courts -- and if Argentina doesn't hand over $907 million to the plaintiffs in the next two weeks, the judge will order U.S. banks not to process Argentina's June 30 payment totalling an equal amount to all the other bondholders.

Fernandez said her government "will not default on those who believed in Argentina." But analysts have questioned whether holders of restructured debt would accept payments outside the U.S. financial system.

"Some people say, 'Why don't you pay them and end all this right now?"' the president said. "It's because there's another problem, even more serious. There's another 7 per cent who would be able to demand payment from Argentina, right away and now, of $15 billion. That's more than half the reserves in the Central Bank. As you can see, it's not only absurd but impossible that the country pays more than 50 per cent of its reserves in a single payment to its creditors."

"It's our obligation to take responsibility for paying our creditors, but not to become the victims of extortion by speculators," she said.

The plaintiffs said her government needs to settle now.

"The time has come for Argentina to enter into good-faith negotiations with holdout bondholders," said Richard Samp, an attorney for the Washington Legal Foundation who has acted as a spokesman for NML's position throughout the case. "Argentina has expressed a desire to be permitted to re-enter financial markets around the world. The only way that it can do so is by coming to terms with its existing creditors."

Refusing to comply was "the best option" among a series of grim alternatives that Cleary, Gottlieb, the U.S. law firm representing Argentina in Washington, presented to Fernandez ahead of the Supreme Court decision. That guidance suggested Argentina should default on all its debts before negotiating in order to gain more leverage.

Associated Press writers Mark Sherman in Washington, Luis Andres Henao in Santiago, Chile, and Almudena Calatrava in Buenos Aires contributed to this report