Movie

The 'Transformers: Age of Extinction' director shrugs off the critics as saying, 'Let them hate. They're still going to see the movie!'

Jun 28, 2014

AceShowbiz - Michael Bay is not too worried about harsh criticisms surrounding his "Transformers" franchise. During a new interview with MTV News to support the latest installment "Transformers: Age of Extinction", the director had a few words for the haters.

"They love to hate, and I don't care; let them hate. They're still going to see the movie! I think it's good to get a little tension. Very good," he told MTV's Josh Horowitz. He also said, "I used to get bothered by it, but I think it's good to get the dialogue going. It makes me think, and it keeps me on my toes, so it's good."

"Age of Extinction" is coming to theaters across the country this weekend. "Transformers 5" is rumored to follow in 2016, but producer Lorenzo di Bonaventura denied it. "No, that's not accurate. Somehow that gets reported, it's made up by somebody and then everybody thinks it's true," he told Screen Rant.

"No, I think we've been doing these on different cycles. I think the same thing is going to happen this time. It's an exhausting process to make these movies. The scale of them is so enormous. It really does tax everybody and our tendency is to finish one at a time, recover, and then begin talking. And we'll do the same thing this time."

Mark Wahlberg leads the franchise, joining new faces like Nicola Peltz and Jack Reynor after the original main star Shia LaBeouf left.