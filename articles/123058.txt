Lindsay Lohan is seen breaking down and crying in the clip from her Oprah Winfrey documentary series titled 'Lindsay'.

The 27-year-old American actress, whose post-rehab attempts to get her career back on track are being chronicled in the documentary series on the media mogul's OWN network, is seen sitting cross-legged with her phone and cigarettes in the short video, the Mirror reported.

The video is an emotional reminder of the battle she has been facing to overcome her issues.