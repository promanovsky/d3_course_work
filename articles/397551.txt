Google Inc. (NASDAQ:GOOG) has unveiled the details of an ambitious project that aims to collect genetic and molecular information on thousands of people in an attempt to redefine how much scientists know about the human body and, perhaps more important, how to improve it. This project is just the latest of Google's "moonshot" health initiatives that, rather than aiming for profit, seek to figure out a better way for humans to live, the company says.

Dubbed simply Baseline Study, the project will first collect information from an anonymous 175 people before expanding the sample size into the thousands. Hoping to build on existing mass medical studies, the Google researchers believe they’ll eventually be able to detect the early signs of heart disease and cancer, aiming to stop those diseases earlier and move the ball forward for the entire medical field.

“With any complex system, the notion has always been there to proactively address problems,” Dr. Andrew Conrad, a molecular biologist leading the team, told the Wall Street Journal. “That’s not revolutionary. We are just asking the question: If we really wanted to be proactive, what would we need to know? You need to know what the fixed, well-running thing should look like.”

Conrad, 50, built his resume by developing inexpensive, high-volume HIV tests for blood plasma donations before moving to Google X in March 2013. Since then he’s assembled a team of nearly 100 experts in physiology, biochemistry, optics, imaging and molecular biology (the study of cells).

The Baseline Study will be conducted by plugging in the samples collected from subjects, entering them into the computer, then comparing them in search of any patterns that might reveal themselves. The examinations, for example, could reveal that one person’s body is better disposed to breaking down fat and avoiding heart disease than another individual. The Baseline Study could then assess other differences between the two patients that make one person more susceptible than the other.

Google's Baseline Study, which will be published with help from Duke University and Stanford University, will be governed by an Independent Review Board, a requirement that's certainly welcome news to users still nervous after the public relations fiasco that followed Facebook’s (NASDAQ:FB) covert psychology experiment.

“Google will not be allowed free rein to do whatever it wants with this data,” Dr. Sam Gambhir, who chairs the department of radiology at Stanford and has worked with Google’s Conrad for more than a year, told the Journal.

Google also explained that the information would not be used to develop a new commercial product.

“This research is intended as a contribution to science; it’s not intended to generate a new product at Google,” said a company statement sent to International Business Times. “That said, a study like this could unlock lots of ideas for future projects, not just at Google but across the health and technology industries. That’s why we plan to make the study and its underlying results available for qualified researchers in health to use for their own medical efforts.”

While it’s far too early to determine if the Baseline Study will be successful, the project joins a number of other lofty goals that Google is at work on, to varied success. The company’s so-called moonshot projects aim to “live in the gray area between audacious projects and pure science fiction; instead of mere 10 percent gains, they aim for 10x improvements,” the company has said. “The combination of a huge problem, a radical solution, and the breakthrough technology that might just make that possible solution possible is the essence of a moonshot.”

The highest-profile "moonshot" project to date is certainly Google’s driverless car initiative. As with the Baseline Study, it’s too soon to tell if the vehicles really will be more efficient and make road travel safer, but Google hopes that customers will be able to summon a car with their smartphone and be taken to their destination without worry at some point over the next decade.

No less ambitious is Project Loon, Google’s plan to launch WiFi-equipped balloons into the skies to bring Internet access to remote areas of the world that are still in the dark. The plan, while promising, has drawn a mountain of criticism even before liftoff, with skeptics saying the idea is emblematic of the technocentrism that prevails in Silicon Valley.

“When you’re dying of malaria, I suppose you’ll look up and see that balloon, and I’m not sure how it’ll help you,” said Bill Gates, the philanthropist and Microsoft (NASDAQ:MSFT) co-founder. “When a kid gets diarrhea, no, there’s no website that relives that.”

Yet as Google has expanded its focus, it has greatly pushed its weight into the health care field. The past year alone has seen Google unveil a wearable eye lens capable of monitoring a diabetes patients’ blood sugar level and Calico, a startup that aims to improve human reactions to “aging and associated diseases.”

In a sign of things to come last year, Google CEO and co-founder Larry Page explained that the motivation for Calico was to take advantage of the “tremendous potential” technology has to “generally improve people’s lives. So don’t be surprised if we invest in projects that seem strange or speculative compared with our existing Internet businesses. … These issues affect us all – from the decreased mobility and mental agility that comes with age, to life-threatening diseases that exact a terrible physical and emotional toll on individuals and families.”