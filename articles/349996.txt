VANDENBERG AIR FORCE BASE, Calif. (AP) -- A rocket carrying a NASA satellite lit up the pre-dawn skies Wednesday on a mission to track atmospheric carbon dioxide, the chief culprit behind global warming.

The Delta 2 rocket blasted off from California at 2:56 a.m. and released the Orbiting Carbon Observatory-2 satellite in low-Earth orbit 56 minutes later, bringing relief to mission officials who lost a similar spacecraft five years ago.

The flight was "a perfect ride into space," said Ralph Basilio, the OCO-2 project manager, at a post-launch press conference.

Power-supplying solar arrays deployed, initial checks showed the spacecraft was healthy and two-way communications were established, he said.

The launch was delayed a day because of a failure in ground equipment 46 seconds before liftoff Tuesday morning.

NASA tried in 2009 to launch a satellite dedicated to studying carbon dioxide, the main greenhouse gas caused by the burning of fossil fuels. A satellite plunged into the ocean off Antarctica after a hardware failure with the Taurus XL rocket.

After the loss, NASA spent several years and millions of dollars building a near-identical twin.

Mike Miller, a senior vice president with satellite builder Orbital Sciences Corp. who has been with the program since its earliest days, said he was among those devastated by the first failure.

"It was very much like losing a close family friend or member ... so we're very happy to see this new day," Miller said.

Like the original, OCO-2 was designed to measure atmospheric carbon dioxide from 438 miles above the Earth's surface. Its polar orbit will allow it to cover about 80% of the globe.

About 40 billion tons of carbon dioxide are released yearly from factories and cars. About half of the greenhouse gas is trapped in the atmosphere, while the rest is sucked up by trees and oceans.

The goal of the $468 million mission, designed to last at least two years, is to study the processes behind how the environment absorbs carbon dioxide.

NASA spent more money on the new mission, mainly because it is using a more expensive rocket. Engineers also had to replace obsolete satellite parts, which drove up the price tag.

"Seldom do we get a second chance to be able to do a mission like this but because of the importance of the mission to the nation we've been given this second chance to do the OCO-2 mission," said Geoff Yoder, a deputy associate administrator from NASA headquarters.

The satellite is approximately the size of a telephone booth, with solar arrays spanning about 30 feet. The launch placed it into an initial orbit 429 miles high.

Completing spacecraft system checks will take about two weeks and then OCO-2 will be moved higher during a span of several weeks into its operational orbit, joining a loose formation of other Earth-observing satellites informally called "the A-train," said Basilio, the project manager from NASA's Jet Propulsion Laboratory in Pasadena, Calif.

There will then be a period of instrument checks to ensure the validity of observations.

Production of science data is expected early next year, he said.