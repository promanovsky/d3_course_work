Todd Burpo’s bestselling book “Heaven Is for Real,” which tells the story of how his son said he traveled to heaven while seriously ill, has been adapted as a movie.

The film adaptation of “Heaven” stars Greg Kinnear as Burpo, “Flight” actress Kelly Reilly as his wife Sonja, and actor Connor Corum, who is making his film debut, as the Burpos’ son Colton.

Actors Margo Martindale and Thomas Haden Church also co-star.

Burpo’s book, which was written with Lynn Vincent and first released in 2010, details how Colton was diagnosed with a burst appendix when he was four years old. Afterwards, says Burpo, who is a pastor, Colton told his parents how he had traveled to Heaven and seen Jesus, John the Baptist, and an older sister whom his mother had lost in a miscarriage and about whom his parents say he had no prior knowledge. Colton also described his great-grandfather and pointed him out in a photograph, says Burpo.

In 2011, Burpo and his family filmed interviews that addressed critics of the book. The interviews were screened at Lifetree Café locations, spots where religious and other issues are discussed around the country.

“As a pastor and as a dad, I want my son to know I tell the truth,” Burpo said during the interview. “He can read the book. He knows if I exaggerated or if I didn't. My son is forever going to believe that I'm an honest person or I'm a liar by what I wrote in that book, because he can read."

“Secretariat” director Randall Wallace is helming the film adaptation of Burpo's book and told Entertainment Weekly that he felt “Heaven” is different from other religiously themed films.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

“I’ve been around churches all my life and I’ve been exposed to a lot of material that would be categorized as inspirational,” he said. “Most of the stuff is anything but inspirational for me. But I found this story to have an incredible intrigue and emotional power. It speaks to the cynic in most of us."

The film version of “Heaven” arrived in theaters on April 16.