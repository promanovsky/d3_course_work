While last week's episode featured the shocking death of a significant character, this week's episode features perhaps the most controversial scene that Game of Thrones has ever filmed. And that's saying something.



The scene in question is the one between Jaime Lannister and Cersei. Not only are we witnessing Jaime and Cersei perform incest, but it's incest performed right next to the dead body of their oldest son (and in a church, no less - Happy Easter!) And then there's the really controversial aspect: have we just watched Jaime Lannister rape his own sister?

Throwing the R-word around is a risky business, and there will doubtless be many a think-piece written about Jaime's actions. But perhaps the most controversial aspect of it all is that, under Benioff and Weiss' script, the scene is changed so dramatically from the book. On the page, the pair still have icky incestual sex next to their dead son, but there's no question of consent: they simply have sex.

Quite why the show has decided to ramp up that scene with the rape aspect is curious. Is it merely shock-tactics? Game of Thrones has largely avoided descending to such methods - the story is good enough that it doesn't need to.

As difficult to watch, and arguably gratuitous as it is, from a character point-of-view, it's actually not all that unlikely. For all the good work done in redeeming Jaime in our eyes, he's still the man who nonchalantly threw Bran out of a tower window in the pilot. Cersei has always been his weak-spot, and it's once again dealing with his feelings for her that seems to bring out the worst in him. Jaime is a man who isn't used to feeling powerless or being denied, and as a result, he reacts in the worst possible way.

"The things I do for love," he once exclaimed. Love or not, what he does in this episode is going to change his standing in many people's eyes.



Game of Thrones is full of complex characters, and tends to stay neutral on whether we're supposed to be rooting for them at any given point or not. If Jaime Lannister is the prime example of this, then Ygritte is certainly another. We like Ygritte, but when we see her casually firing an arrow through a defenseless villager's eye, what stance are we to take?

She's an enemy of every character south of the wall, and yet we want her to succeed and survive? It's admirable that the show gets us rooting for people from all corners, and doesn't flinch on depicting the good and bad of these characters. Westeros is a tough place, and Benioff and Weiss continue to paint it with determined shades of grey.

Samwell Tarly is perhaps one of the more purely good characters on the show, and that goodness does not serve him well in a world not used to it. Gilly can't understand why Sam would want to send her away to protect her (and only in Game of Thrones could sending someone to a whore-house be deemed a form of protection). Growing up with Craster, Gilly is not used to kindness. Sam might have killed the first Walker in thousands of years, but dealing with girls is a fight far beyond his skill set.



If Sam is pure but soft, then Daenerys is as hard as rock. Her motives are true, but unlike Sam, she has the will and capability to get things done. The episode closes on the wonderful notion that Dany is bombarding the slaver-city of Meereen, not to cause damage or destruction, but to sow the seeds of rebellion. Lobbing the broken chains of the episode's title into the city is a wonderful way of showing the slaves of Meereen what the Mother of Dragons can offer, and it's a scene complimented brilliantly as usual by Ramin Djawadi's stirring score. Dany's "being a bad-ass" theme really is a superb audio cue.

If Dany started the show as a meek child, you definitely understand why an army would follow her now. Stannis Baratheon, meanwhile, is alone, and you can similarly understand why. Stannis thinks himself pure and true - and he's certainly hard enough - but his association with the Red Woman, and his manner with Davos, the one man actually trying to help him, provides a stark contrast with Dany's benevolence.

If Dany has earned her support through her actions, then Stannis has lost his as a result of his own. She's surrounded by loyal supporters - just look at them all lining up to take on the Meereenese champion for her - while Stannis languishes alone on his dank little island. He might be the one true king by rights, but what sort of king would a man like that make?



Oh yes, and speaking of Kings - Joffrey is still very much dead. Tywin wastes no time at all in grooming young, affable Tommen to replace him, ensuring he gets in early to mold a pliable puppet-king that Joffrey could never be. It's a strong episode for Charles Dance, as he also recruits Prince Oberyn to his cause, offering the charismatic Dornishman a chance for vengeance.

And as to the who-dunnit cliff-hanger from last week, as the prime-suspect, innocent Tyrion is locked up in the dungeon, and – like Sam with Gilly – has to send away his closest ally for protection. The farewell between Podrick and Tyrion packs a surprisingly emotional punch, given how little of Podrick we actually know. If it's the last we see of Pod, Daniel Portman had a strong scene to go out on.

And, as Sansa is spirited away to a mist-shrouded boat, hauntingly shot by director Alex Graves, who should step out of the shadows but everyone's favourite bafflingly-voiced trickster, Littlefinger! Aiden Gillen's strange manner of speaking might be somewhat jarring, but there's no doubt it adds a certain sliminess to Petyr Baelish. And who'd bet against him having had a hand in Joff's demise at this point?

Sansa - another of our purest characters - has had nothing but misery piled on her since the series began. Here, it's a definite frying-pan-and-fire scenario, as she finally escapes King's Landing, but only by virtue of a polite kidnapping by Baelish. "They're all liars here," Sansa recalls him warning her, and when he says ominously with his next breath "You're safe with me", you can't help but think Sansa might want to keep that warning in mind...

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io