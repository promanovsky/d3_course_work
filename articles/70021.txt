NEW DELHI: India being declared 'polio-free' by the World Health Organization is a "landmark achievement" and it should now assist other countries such as Pakistan, Nigeria and Afghanistan in their fight against this disease, President Pranab Mukherjee said on Saturday.

"India must assist other countries such as Pakistan, Nigeria and Afghanistan in their fight against this disease in improving their health systems and infrastructure," said Mukherjee while inaugurating the Polio Free Conclave 2014 organized by the Rotary International here.

"The unique challenge in India was the sheer number of people combined with the difficult terrain in many states. It was possible only because of the unparalleled dedication of all concerned and the relentless efforts put in by the workers, doctors, activists and various stakeholders including parents of children in achieving this day", he said. He, however, stressed the need to be vigilant to ensure that there was no recurrence of this disease.

"We must be vigilant. There is need for watchfulness and preparedness to see that there is no recurrence of this disease," he said.

The unprecedented programme to eradicate the polio virus from the country was taken up by the central and state governments supported by several national and international agencies such as Rotary International, UNICEF, WHO, CDC and Melinda and Gates Foundation , he added. The President said that the agencies helped with material resources and best practices to wipe out this crippling disease. Initiative was taken up at several levels to spread awareness, provide prompt medical attention and administer the polio vaccine.

Mukherjee also presented mementos of appreciation and recognition to the former health minister Dr A Ramadoss and Union health minister Ghulam Nabi Azad.

Azad credited this achievement to the decision to introduce the new bivalent polio vaccine which was indigenously developed for the first time, aggressive immunization and micro block level planning covering the most endemic areas. He said strong political will at the highest levels that ensured pumping in of required financial resources and deployment of huge manpower in this gigantic task, constant monitoring and maintaining supply chains were some of the other factors.

Present at the occasion were Health Secretary, Lov Verma, Dr Harsh Vardhan, Gary Huang, Rotary International President Elect, D K Lee, Chairman, Rotary Foundation, Rajendra Saboo, Conclave chairman, P T Prabhakar, Rotary International director and Kalyan Banerjee, Conclave co-chairman.