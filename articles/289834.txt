SAN DIEGO, June 15 (UPI) -- Of the 621 people in San Diego County diagnosed with whooping cough this year, 85 percent were up to date with their pertussis immunizations, calling into question the effectiveness of the vaccine.

California is currently on track for having the most cases of the disease since 2010, and state officials declared the recent increase of cases an epidemic.

Advertisement

San Diego County has seen 621 cases of the bacterial disease since the beginning of 2014, way up from the 430 total cases in 2013. There have been 3,458 cases this year in the entire state.

Of the 621 people who've had the disease in San Diego County this year, 527 had the six shots necessary to be up to date with their pertussis vaccine and 67 were not. Immunization status for 27 of the ill wasn't known.

"Pertussis vaccines offer high levels of protection within the first year of completing vaccinations," said Dr. Wilma Wooten, a San Diego county public health officer.

"But then the protection decreases over time," she added.

Even with the decreased effectiveness of the vaccine, it still helped to lessen the severity of the disease.

The Centers for Disease Control and Prevention recommend children get the pertussis vaccine at 2 months, 4 months, 6 months 15 to 18 months, and 4 to 6 years.

Whooping cough usually starts with cold-like symptoms, including a mild cough and fever, the CDC says.

Severe coughing and coughing fits can continue for weeks after that, resulting in a loud whooping sound as the infected gasp for air.

In babies under 1 year of age, there may be a dangerous change in breathing pattern.