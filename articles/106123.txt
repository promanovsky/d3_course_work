Brandon Carte

USATODAY

Some could say Twitter is turning into a mockingbird as the social media company continues to roll out updates that make the site more and more like Facebook.

Twitter recently announced it would bring users real-time notifications on twitter.com when someone is engaging with their Tweets.

The announcement follows shortly after the social media site rolled out a redesign with a large cover photo and a stronger emphasis on images and videos and removed the sense of chronological order that has been present in past Twitter timelines.

Those who sign up for the update will receive notifications if someone has replied, favorited or retweeted one of their Tweets. Users can tweak settings to also receive notifications for direct messages and new followers.

"They're fully interactive, so that you can reply, favorite, retweet, and follow right from the notification," a blog post from Twitter states.

If you want to sign up for the new notification feature, just head to your settings in Twitter and choose what types of notifications you want to get on twitter.com, email and mobile.

If you use the app, users can also refine the types of push notifications they receive.