Chris Brown has just been hit with some pretty bad news: not only has his Washington D.C. assault trial been pushed back to June but he'll also have to stay in jail after his bid for freedom was rejected. The R&B singer is charged with punching a man in the face in Washington, D.C. on Oct. 27, 2013.



Chris Brown Will Remain In Jail As His Assault Trial Is Delayed Until The 25th June.

Brown, who famously dated singer Rihanna, was arrested early on Sunday (27th Oct.) morning after a man, named as Parker Adams, accused him and his bodyguard of breaking his nose outside the W Hotel. Adams told police he'd tried to push his way into a photograph Brown was taking with a woman and her friend.

The bodyguard, Christopher Hollosy, admitted to hitting the man first and was convicted in a separate trial on the 21st April of a single count of misdemeanour assault with sentencing set for the 25th June.

At the time of the incident, Brown was on five year probation in California after he attacked his ex-girlfriend Rihanna in 2009. The singer's current girlfriend, Karrueche Tran, has been reportedly comforting the star in jail as his experience behind bars has already left him "broken," according to Hollywood Life.



Brown Is Reportedly "Broken" Over His Jail Ordeal.

"Kae couldn't believe it because he's rarely ever emotional with her," a source connected to the couple revealed. "He told her he's tired and that he couldn't sleep and how he doesn't know if he'll be able to fall asleep because he's nervous.

Brown spent one night in jail before entering rehab to combat his anger issues late last year, but he was jailed in mid-March after violating the treatment facility's rules and he has remained in prison.

Brown's lawyer Mark Geragos said outside court that he wanted to move forward. "I don't think that there's any doubt Chris Brown is not guilty," he said, via BBC News. Geragos added that Hollosy's testimony was crucial to Brown's case and that it was unfair that prosecutors refused to grant immunity to the bodyguard.



Brown's Bodyguard Would Argue That He, Not Brown, Punched The Man.

He argued that Hollosy should be allowed to testify at Brown's trial, saying prosecutors had a "duty to fairly treat a defendant and to extend immunity in certain situations." The lawyer explained "It's going to be our argument that this is one of those situations."

Chris Brown's trial has been delayed until the 25th June.

More: Chris Brown wants to duet with Ariana Grande.

More: Chris Brown's modelling agency threatened with legal action - read full story.

See Chris Brown Out Shopping With Friends:

