A floor trader prints out a trading record at the end of a trading day at the Hong Kong Stock Exchange November 23, 2010. REUTERS/Tyrone Siu

Stocks were up, then down, then up again. At one point, the Nasdaq was down by as much as 1.8%. The Dow was down by 51 points.

First, the scoreboard:

Dow: 16,448.7 (+87.2, +0.5%)

16,448.7 (+87.2, +0.5%) S&P 500: 1,869.4 (+6.0, +0.3%)

1,869.4 (+6.0, +0.3%) Nasdaq: 4,074.4, (-1.1, -0.0%)

And now the top stories: