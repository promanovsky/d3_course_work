'Does she twerk in that song?' Chris Martin shows quick wit while serving as guest mentor in pre-taped segments on The Voice

English singer Chris Martin showed his sense of humour and easy-going manner on Monday night's broadcast of The Voice.

The 37-year-old Coldplay frontman and his wife Gwyneth Paltrow announced last week they were 'consciously uncoupling' after 10 years of marriage and have been secluded in the Bahamas with their children.

The pre-taped segments of The Voice showed Chris affably giving advice to singers.

Scroll Down for Video



He's funny! Chris Martin flashed his sense of humour and easy-going manner on Monday night's broadcast of The Voice as he served as a guest mentor to all the remaining singers

Chris, in gray t-shirt and jeans, was the special guest mentor helping all the coaches prepare their singers for round two of the face-to-face battle rounds.

'I'm really exited about getting to work with Chris Martin, the guy's brilliant, he's so creative,' country superstar Blake Shelton said. 'The only thing I don't like is the other three coaches get to work with him also.'

Blake was far from the only fan of the star singer.

Miley joke: Chris cracked a twerking joke while chatting with contestants on The Voice

'I just want to say Violet Hill is the greatest song ever,' contestant Christina Grimmie of Adam Levine's team gushed on seeing him.

'That song changed how I listened to a lot of music. I love it.'

'Thank you so much,' Chris replied, with Christina later admitting: 'Oh my gosh, I lost it! This guy is a songwriting genius.'

Grabbing a seat: Blake Shelton took a chair while Chris sat on the floor during The Voice rehearsals

Team Shakira's Patrick Thomson also could not believe his eyes.

'Walking in to see Chris Martin there - it's crazy, man,' he said. 'He's a superstar. To be able to meet these people and get there input - that's amazing.'

Chris - who called Mick Jagger 'the best frontman in the world' - took the gig seriously, but also got to show off his dry wit.

Told that two contestants planned to sing The Climb by Miley Cyrus, he quipped: 'Does she twerk in that song?'

Sage advice: Chris was friendly yet took his duty seriously at the same time

Two of the performers, Cierra Mickens and T.J. Wilkins, had the coaches stretching for higher and higher praise for their duet of Get Here by Oleta Adams.

'It was perfect, and I just have this to say,' Shakira told them as she took an exaggerated bow, while Blake told Cierra it was 'greatness, perfect'.

'I just shed a tear for all these shows that have singers, these other singing competitions, because the is the best one,' Adam said after their duet.

Performance tips: Chris gave tips to Cierra Mickens and Los Angeles singer T.J. Wilkins

Chris during rehearsal told T.J. that he liked his approach to the song.



'I was very impressed by the fact that you could deliver the line ‘You could windsurf to me''' Chris said.

T.J. said he thought it was cool that a guy from South-Central Los Angeles could connect with a guy like Chris 'over a windsurfing lyric'.



Despite such high praise, all the other judges surprisingly let Cierra go when Usher - wearing an eye-catching Davy Crockett-style hat - chose T.J. to stay in the show.

Awaiting their fate: The Voice host Carson Daly stood by Cierra and T.J. before Usher deemed T.J. the winner

The coaches got more competitive, however, when Blake rejected pop singer Tess Boyer for young country singer Jake Worthington - and all three remaining coaches battled to steal her after giving both singers standing ovations.

It was more the amazing for Usher who had rejected the NFL cheerleader in the previous battle round - and now wanted her back.

The R&B star tried to win her over, saying his 'vote has always been for her' - before his rivals reminded him he let her go, forcing him to start singing Peaches & Herb's Reunited.

Tough call: Blake picked Jake Worthington after stealing Tess Boyer last week from Usher's team

'Wow, you blew me away girl,' Shakira told her of her version of John Hiatt's Have a Little Faith In Me.

Shakira ultimately won out, having told her: 'Tess, after what I saw tonight you are hands down the best female voice right now on The Voice.'

'For me, it blows,' Blake said of losing a stand-out in the show, saying he picked Jake mostly through 'loyalty' as he was his original team member - as well as country.

'I think Jake could be a star. His voice is an authentic country sound, and to be able to introduce him to Nashville and the world - that's exciting to me.'

Getting serious: Adam Levine, Shakira, Usher and Blake had their game faces on

In his other battle, Blake chose Audra McLaughlin after she sang against Megan Ruger to Miley's The Climb.

'That was a heartbreaker for me,' Blake said. 'But Audra is unique. I think I can win this thing with Audra.'

We got a winner: Audra McLaughlin won her battle against Megan Ruger to remain on Blake's team

Adam put his two big soul voices up against one another, with Delvin Choice and Josh Kaufman singing the Stevie Wonder hit Signed, Sealed, Delivered I'm Yours, getting standing ovations from all the coaches.

And it brought one of the biggest surprises so far, with Josh - who up until now has had much of the highest praise of the season - getting turned down for Delvin.

'I told you that you're only job was to make it impossible for me - well congratulations guys, you made it impossible for me,' Adam told them, saying it 'was flawless'.

'Either way I lose a potential winner of this thing,' he added, before picking Delvin, saying: 'His voice has more power than anyone else in this competition.'

Usher was quick to steal Josh, saying: 'That was one of the most incredible vocal performances of the competition. I feel like we could go right to the end, man.'