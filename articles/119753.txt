Someone on US Airways’ social media team is liable to get fired after a colossal mistake Monday. Their official Twitter account sent out a graphic photo to a customer, which left many Twitter users in a mix of horror and amusement.

It might have been an honest error, but many people were forced to see a toy plane inserted into the intimate region of a woman’s body. The image, unfortunately for US Airways (now part of American Airlines Group (NASDAQ:AAL)), can still be easily found on Twitter through a simple search and lives on on sites like Jezebel and Gawker.

After the airline noticed the egregious error, which had been out for an hour, according to Jezebel, it removed the tweet and issued a public apology for the pornographic image.

The full response has been posted below, courtesy of The Washington Post:

We apologize for the inappropriate image we recently shared in a Twitter response. Our investigation has determined that the image was initially posted to our Twitter feed by another user. We captured the tweet to flag it as inappropriate. Unfortunately the image was inadvertently included in a response to a customer. We immediately realized the error and removed our tweet. We deeply regret the mistake and we are currently reviewing our processes to prevent such errors in the future.

USA Today reported the original complaint said: "You ruined my spring break, I want some free stuff." The airline then responded with, “We welcome your feedback, Elle. If your travel is complete, you can detail it here for review and follow-up.”

American Airlines is currently in the process of merging with US Airways, and the recent social media gaffe is the second to involve the aviation conglomerate. Over the weekend, a 14-year-old girl from the Netherlands was taken into custody after she sent a terrorist-themed message to American Airway’s Twitter account.

Twitterati have naturally had a strong response to the social media blunder. Some of their responses have been posted below:

The PR department over at US Airways are working overtime tonight. pic.twitter.com/nLlmA4qjFQ — Jamie DMJ (@JamieDMJ) April 14, 2014

Thanks to @USAirways, we learned that there's at least one place where CNN won't look for a missing plane. — LOLGOP (@LOLGOP) April 14, 2014

Think u r shocked by @USAirways #usairways twit pic - imagine people who realize they are related to the woman in the photo! Fame vs Shame. — Chris Cuomo (@ChrisCuomo) April 14, 2014

US Airways announces all flights will arrive in terminal V. #Zing — Bob Kevoian (@bobkevoian) April 14, 2014

I do not want to see where @USAirways puts the luggage. Eeesh. — Robert Flores (@RoFloESPN) April 14, 2014

You can see why that @usairways plane crashed- there was no landing strip. — Chris Santos (@SantosCooks) April 14, 2014

.@USAirways Looks like someone set their device to airplane mode lol! honestly tho thanks guys this is better than Christmas — Ken Jennings (@KenJennings) April 14, 2014

I'm amazed the @VirginAmerica team haven't capitalised on the @USAirways screw up, it's the perfect set up... — Neal Mann (@fieldproducer) April 14, 2014

Dear @CNN can you finally call off the search for #MH370? @USAirways was hiding it all along. — Anonymous (@YourAnonNews) April 14, 2014

Sorry @USAirways I thought that was a DM #mybad — Amy Schumer (@amyschumer) April 14, 2014

Surprised CNN hasn’t started wall to wall coverage on this US Airways plane yet. — Awful Announcing (@awfulannouncing) April 14, 2014

Some people are saying @USAirways was hacked but it looks like an inside job. — Veronica de Souza (@HeyVeronica) April 14, 2014

Follow me on Twitter @mariamzzarella