Drchrono's new medical record storage app caters to doctors' latest needs, The Daily Digest reported Sunday.

According to Reuters, the tech company created the new healthcare app to give medical professionals somewhere convenient to keep and view their patients health information. Staff can also record a appointment or procedure, if the patient allows it. Doctors can also retain notes, videos, and photos from patient's visits. These are virtually kept in a cloud program. Clients can ask doctors to see the content.

Drchono states it is the first "wearable health record," Reuters reported.

The company's co-founder also told Reuters that engineers are figuring out other ways consumers want to use the app.

"Google is still in the early-stages of determining the most viable use-cases for Google Glass," Daniel Kivatinos said. "But some doctors are demanding Glass, so Google is providing resources and support to developers."

According to The Wall Street Journal, Google began to research how it could bring the device to optometry offices through VSP or Vision Service Professionals in November.. This gives Google employees health care benefits for their eyes. Thirty-thousand eye doctors, and 60 million individuals currently use the VSP's offerings for job and personal health care coverage.

Google Glass has many other attributes. Users can record video, search the Internet and even play tennis the company's blog said. There is also a balance game that evaluates one's ability to keep books on their head, and a clay shooter similar to the famous duck hunt game.

The match game evaluates how well one pairs patches with similar patterns, while a shape splitter game breaks up shapes.

The connected eyewear also plays music. Consumers can look for songs, review playlists, and hear tunes in good quality sound while the device is on their face.

60,000 registered physicians utilize the apps features. Over 300 have agreed to institute the free app while on the job. Drchrono could tack on a fee soon.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.