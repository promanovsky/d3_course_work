LONDON, May 19 (UPI) -- AstraZeneca rejected another offer from Pfizer, saying the price was too low and ending all chances of creating the world's biggest drug maker.

The $117 billion takeover offer was Pfizer's final offer and was rejected by the U.K. drug maker, as it failed to take into account experimental drugs in its pipeline and also the risks it presented to shareholders. The deal valued AstraZeneca at $92 per share, but the company said something closer to $98 per share would represent the true value of the company.

Advertisement

"From our first meeting in January to our latest discussion yesterday, and in the numerous phone calls in between, Pfizer has failed to make a compelling strategic, business or value case. The Board is firm in its conviction as to the appropriate terms to recommend to shareholders," said AstraZeneca Chairman Leif Johansson.

He added that the offer was a minor improvement upon Pfizer's third offer, but still continued to "fall short of the Board's view of value and has been rejected."

The deal, which looks like it is dead, had split AstraZeneca shareholders, with many suggesting that the board was making a "gross miscalculation" in rejecting the offer. They also felt that the company should have engaged in constructive talks with Pfizer to reach an acceptable figure.