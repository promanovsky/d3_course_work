Cops are hereee! Mike "The Situation" Sorrentino was arrested following a brawl at his family's tanning salon in Middletown, N.J., Us Weekly confirms. It is unknown when the incident exactly occurred.

According to TMZ, police were called to Boca Tanning Salon following a "fist fight" between Sorrentino and his brother Frank. A source tells Us the Jersey Shore alum, 31, was the only one arrested and taken into custody.

The family-owned business has been coming under fire as of late. Earlier this month, TMZ reported that employees had to call the Middletown Police Department after their paychecks bounced. Each employee was due between $100-$200.

Sorrentino is said to still be in custody and is currently being booked.

More to follow.