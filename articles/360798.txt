Sydney: The judge leading an Australian inquiry into institutional child abuse said on Saturday that the conviction of disgraced entertainer Rolf Harris for sexual assault would encourage more victims to come forward.

Australian-born Harris, 84, once one of the country’s best-loved personalities, was jailed Friday in London for five years and nine months for a string of attacks on girls.

Justice Peter McClellan, the chairman of the ongoing Royal Commission into Institutional Responses to Child Sexual Abuse, said publicity of the six-week trial and testimony from victims would help survivors to lift the “burden of guilt and shame”.

“Newspaper reports suggest that as a consequence of the publicity and the jury’s verdict more victims have come forward,” McClellan said in a speech to a survivors’ support group.

“This is not surprising... The burden of guilt and shame which many have felt is lifted by knowing that others who have suffered have overcome their reticence.

“It becomes legitimate to talk openly of their childhood experience.”

McClellan’s comments came as Sarah Monahan, a former child star and ambassador for child protection advocacy group Bravehearts, said on Facebook that Harris’s sentence “seems very light”.

“It’s a pity courts take the age of the offender into consideration, when that person didn’t care about the age of the victim,” Monahan added on Twitter.

The British government’s chief legal adviser, Attorney General Dominic Grieve, was considering whether Harris’s sentence was too lenient after a complaint was made to his office by an unnamed person.

McClellan said his commission’s interviews with victims showed they felt abusers should be punished even if they were elderly.

“Our experience in speaking with survivors tells us that those who have suffered from Harris’ criminal acts will believe that, notwithstanding his age, Harris must be punished for his actions,” McClellan said.

“They have suffered and, in their minds, so must he.”