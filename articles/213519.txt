



The EU's recent decision to allow citizens to request the removal of a link from Google's database is already having an effect. A politician, a pedophile and a would-be killer are among the people who have already asked Google to remove links to information about their pasts.

The idea behind the ruling was that European citizens should be allowed to hide a link on Google if they don't want it to be publicly accessible, under a so-called "right to be forgotten." The question, however, is whether an individual's personal desires overrules the public's right to know about events that are a matter of public record.

Advertisement

A former politician requested that Google remove links to a news article about his behavior when previously in office, so that he can have a clean slate when running for a new position.

A man who was convicted of possessing child sexual abuse imagery requested that Google remove links to pages about his conviction.

A convicted cyberstalker requested that Google remove a link to an article about cyberstalking laws that mentions his name.

A physician requested that Google remove a link to a review site.

A man who tried to kill his family requested that Google remove a link to a news article that discusses the event.

This question becomes more urgent when you know a few of the examples of links that have already been submitted for removal by Google. A source familiar with the situation gave Business Insider these five cases that have been brought up since the EU ruling on Tuesday:These are the kinds of people who are taking advantage of the EU's recent ruling.

According to Viviane Reding, European commissioner for justice of fundamental rights and citizenship, the EU ruling was a success for personal data protection. "The data belongs to the individual, not to the company," she posted to Facebook. "And unless there is a good reason to retain this data, an individual should be empowered - by law - to request erasure of this data."