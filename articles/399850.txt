Nintendo’s struggles continued in the first quarter of fiscal 2015, as relatively poor Wii U sales and high costs resulted in yet another loss for the struggling video game giant. Nintendo on Wednesday reported a June-quarter loss of 9.92 billion yen, or $97.3 million, compared to an 8.6 billion yen profit in the same quarter last year. Revenue totalled 74.7 billion yen, or roughly $732.3 million, in the quarter, which represents an 8% decline from the year-ago quarter.

Perhaps the most disconcerting news is that this markedly worse performance comes on the back of dramatically improved Wii U unit sales; Nintendo said it sold 510,000 Wii U consoles in the fiscal first quarter, which is a huge improvement over the 160,000 units it sold in the same quarter last year.

Sales of the company’s latest home console were bolstered by the release of its highly anticipated Mario Kart 8 game, which sold 2.82 million copies worldwide.

Finally, the company noted a huge decline in sales of its portable Nintendo 3DS console lineup. Global unit sales totalled just 820,000 consoles in the June quarter, down from 1.4 million units in the same quarter last year.