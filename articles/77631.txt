Google's Gmail is turning a decade old today and given it was announced on April Fool's Day, many techies initially believed it was just a fun prank.

At the time Google released a press statement indicating its free search-based webmail would boast storage capacity of up to eight billion bits of information (about 1GB), the equivalent of 500,000 pages of email for each user account. At the time, most email services offered 2 to 4 MB of storage capacity.

The idea for email service came from a Google user who was upset about the current webmail services, based on the initial news announcement. As Google Co-founder and President Larry Page states in the release the user reported she was spending way too much time trying to organize or even find needed email.

"She kvetched about spending all her time filing messages or trying to find them," Page said in the release in 2004. "And when she's not doing that, she has to delete email like crazy to stay under the obligatory four megabyte limit. So she asked, 'Can't you people fix this?'"

So Google did just that, building the free webmail service on the "idea" users shouldn't have to file, delete and spend time searching for a needed email. One published report claims the project took three years and that it was initially called "Caribou" while under development. A big key feature developed was the ability to search through emails using a keyword.

A Google engineer set to work on the project as part of the search giant's philosophy that Google employees should spend 20 percent of their work time on new projects unrelated to specific day-to-day tasks.

Published reports indicates there are now somewhere between 289 million and 425 million Google mail users, with one report suggesting there are now as many as 500 million adopters and 5 million users reportedly tapping Google's Gmail related Google Apps.

While the service has remained free for 10 years, Google's scanning of emails to push advertising at users has been hotly debated as user privacy and data privacy have become more prevalent concerns by both consumers and lawmakers.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.