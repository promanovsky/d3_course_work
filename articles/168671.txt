Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

If you didn’t manage to catch a glimpse of Kim Kardashian’s North inspired studs yesterday, fear not - for here's the earring selfie.

Kanye West's fiancee proclaimed her motherly love with the gold studs which spelt out her daughter's name 'North' whilst out running errands in LA - but North herself, was nowhere to be seen!

And last night, Kim posted a close-up of her new jewels, adding "<3 my North earring!" - aww.

(Image: Splash)

New mum Kim was papped out in Holllywood yesterday rocking a rather unflattering long leather skirt, which certainly didn't cling onto all the right places.

Kim was snapped as she was joined by momager Kris to run errands in town - no doubt for her upcoming Parisian wedding to Kanye West.

The ambitious power pair are said to be spending £200,000 on a temporary balcony at their wedding venue which is believed to be France's Chateau Louis XIV next month.

A source said: "Kanye is also in talks to hold a spectacular fireworks display to rival the lightshow at the London Olympics."