Come May 13th, Motorola is set to announce their latest smartphone said to be affordable and accessible for everyone. In the run up to this event, we've already seen quite a bit leak out thanks to Motorola's own website. We've seen listings of the Moto X+1 and now, a mysterious device complete with pricing has appeared in Motorola's website. At this point, it looks very much like Motorola is getting ready to launch their new devices and put them up for sale at the same time, or at least that's what their website is suggesting to us.

Advertisement

German website Mobiflip has discovered three pages on Motorola's website listing an unknown 'Moto' device. The all-but blank pages tell us that there's a "Moto 3g Global 4gb" and two listings for a "Moto 4G Global". The 3G device is priced at $179 (the price of the Moto G right now) and one of the 4G devices is also $179 and the other is $189. With no names to go on, it's hard to understand just what Motorola is planning for these devices, but they sound like a 4G variant of the Moto G to us.

A price difference of just $10 doesn't seem enough to deliver a 4G smartphone, but considering Motorola is looking to sell the best phone they can for as little as possible, it doesn't seem ridiculous. Add that to the fact that the Moto G has been out for some time now, meaning production costs could have fallen, and it looks like the Moto G is about to go 4G in some regions. It's possible that these devices are placeholders for the Moto E, but that's pegged as being an even cheaper device than the Moto G, so we're unsure about that.

Launching a 4G smartphone on the cheap would be a good move for Motorola, as 4G is becoming more and more available to lower price points, not to mention the recent launch of 4G in China. Still, we only have until the 13th to find out just what Motorola is planning, so sit tight.

Sponsored Video