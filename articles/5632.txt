Vietnamese creator of Flappy Bird, Nguyen Ha Dong, is thinking of resurrecting the smash-hit free game that he abruptly took offline a month ago. ― File pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

WASHINGTON, March 12 ― The Vietnamese creator of Flappy Bird says he's thinking of resurrecting the smash-hit free game that he abruptly took offline a month ago ― albeit with a warning about its addictive qualities.

In his first interview since he pulled the app from the Apple and Android app stores, citing the pressure its success put on his “simple life,” Nguyen Ha Dong told Rolling Stone magazine he now feels a sense of “relief.”

But asked if Flappy Bird will ever fly again on mobile devices, Nguyen responded: “I'm considering it.”

While he is not working on a new version, he said any sequel would come with a warning to users to “please take a break.”

With its 2D retro-style graphics, Flappy Bird ― in which gamers try to direct a flying bird between oncoming sets of pipes without touching them ― was wildly popular.

When Nguyen, 28, announced on Twitter that he was about to take it down, 10 million people downloaded it in just 22 hours ― and one month on, clone versions still pop up.

Nguyen told Rolling Stone he was upset not only by the fame that surrounded his Flappy Bird success, but also by messages from people telling him how the game caused them to flunk exams and lose jobs.

Now enjoying a quieter life, he told Rolling Stone he is busy creating other games, including a cowboy-themed shooter, a vertical flying game and an “action chess game” ― one of which he will release this month.

Vietnam has a small but thriving software and games development sector and the global publicity surrounding Flappy Bird is likely to help it grow, technology experts have said.

Rolling Stone posted its interview with Nguyen on its website yesterday ahead of publication in its March 27 issue. ― AFP