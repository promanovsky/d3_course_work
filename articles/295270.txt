Nest on Monday announced on its blog that it’s resuming Nest Protect sales, after having recalled more than 400,000 smoke alarm units in mid-May due to safety risks. Nest said that its smart smoke detector now sells for $99, or $30 cheaper than before. However, the Wave feature, which allows users to silence alerts from the Nest Protect by simply waving their arm – and which was found to be a security threat, as it could misinterpret certain gestures – will not be available to buyers.

In addition to announcing the new price for the device, Nest also revealed various findings based on the data collected by its Nest Protect smoke alarms, including statistics and a personal account on how Nest Protect works.

“We believe Nest Protect is the best smoke and carbon monoxide (CO) alarm out there. It’s designed to give you the information you need in an emergency. And that makes you safer,” the company wrote.

“Our goal is that our groundbreaking research will help shed light on how actual CO events unfold. Because behind all the numbers are real people experiencing very real emergencies,” Nest added.