Spoiler alert: This post contains spoilers for this week's Game of Thrones. If you haven't watched the episode, stop reading.

Parts of Game of Thrones season Four episode 4 are disturbing to the degree that it’s impossible to accept anyone’s imagination ever going ... there ... in the first place.

The episode, titled Oathkeeper, is that horrifying. What's more, we find out who killed King Joffrey.

SEE ALSO: 'Game of Thrones' Episode 3 Recap: 6 Big Clues in the Westeros Whodunit

Shake it off. We have Ser Pounce!

That’s right, all the confusion, dread and outrage we’re feeling about death-by-rape and other unpublishable creative atrocities can only be mitigated by Game of Thrones’ first cat to purr on the Iron Throne.

SP — in the books he's a kitten, but a full-grown cat on TV — pounced at a great time Sunday night, just as Margaery Tyrell, whose boy-king bridegroom Joffrey has just been murdered, is sidling up to his little brother Tommen Baratheon for some stuff that Nancy Grace would be pretty steamed about.

Margeary Tyrell tries to seduce Tommen Baratheon in HBO's "Game of Thrones." Image: HBOGo.com HBOGo.com

Tommen is nonplussed when Margarery asks him to "tell me a secret." GULP:

Tommen Baratheon is worried on HBO's "Game of Thrones." Image: HBOGo.com HBOGo.com

When "Ser Pounce" jumps on the bed and breaks the tension! Oh, brave Ser Pounce!

“Joffrey didn’t like him,” Tommen says, in a moment that breaks the ice and plays right into Margaery's hands. “He threatened to skin him alive and mix his innards up in my food so I wouldn’t know I was eating him.”

WHAT? Not Pounce! That Joffrey was awful.

'Sir Pounces,' first Cat of the King, arrives on 'Game of Thrones.' Image: HBOGo.com HBOGo.com

Also, if cats have taught us anything, it’s that the Internet loves them. So without further justification, here’s one more screengrab of Ser Pounce, the royal cat, who we only saw for a fleeting moment Sunday but whose extra eight lives are going to come in handy:

'Sir Pounces' shows up at a good time in Season 4 Ep. 4 of HBO's 'Game of Thrones." Image: HBOGo.com HBOGo.com

BONUS: 15 Terrible 'Game of Thrones' Jokes