Facebook on Tuesday made a surprising announcement, revealing that it purchased the Oculus Rift virtual reality startup in a $2 billion deal, although it’s not yet clear what the company plans to do with Oculus Rift in the future. According to a report from The New York Times though, a person familiar with the deal said the company “eventually plans to redesign the Oculus hardware and rebrand it with a Facebook interface and logo.”

On the other hand, Facebook promised Oculus that the company will be able to operate “largely autonomously” within Facebook, just like WhatsApp or Instagram, Oculus board member and general partner at Matrix Partners Antonio Rodriguez said. And TechCrunch says that Facebook is denying that it plans to alter the Oculus’s hardware or slap its own logo on the device.

Even though Oculus Rift was seen as an exciting gaming gadget, Facebook may do other things with it, including creating a new social experience. Mark Zuckerberg suggested that Facebook aims to fuse gaming and social communication together – according to the CEO, 40% of the time that people spend online on computers is for gaming purposes, while 40% is for social networking.

“People will build a model of a place far away and you’ll go see it,” Zuckerberg said during a conference call. “It’s like teleporting.”

Unsurprisingly though, Facebook may use Oculus Rift for monetization purposes as well. “We view this as a software and services thing,” Zuckerberg said during conference call, “a network where people can communicate and buy things.”

Even so, the Facebook deal is seen as “the best shot virtual reality has ever had and probably will ever said,” by Oculus Rift co-founder Palmer Luckey. “Mark does believe in our vision of virtual reality, and we’re going to continue operating independently, delivering what we’ve always wanted to deliver,” he further told The Verge.

Not all virtual reality fans are equally impressed or excited about the purchase though. Of those, popular Minecraft game developer Markus Persson voiced his concerns on Twitter saying that Minecraft for Oculus Rift is canceled for the time being. “We were in talks about maybe bringing a version of Minecraft to Oculus. I just cancelled that deal. Facebook creeps me out,” Persson said.