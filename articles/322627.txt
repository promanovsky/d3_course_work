From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

GoPro, the maker of high-definition cameras for extreme sports and action moments, saw its stock price rise 30.6 percent after its initial public offering on Thursday. The stock opened at $28.65 a share today and closed at $31.34 a share.

The San Mateo, California-based company is the latest Silicon Valley gadget maker to take advantage of the “IPO window” and investors’ love for cool new tech stocks. GoPro priced its offering at $24 a share, at the upper end of its targeted IPO price range, because of high investor demand. And given the first-day stock price increase, that aggressive pricing proved to be conservative for a company that is all about capturing life’s passions. The stock hit a high of $33 a share during the day.

GoPro’s video cameras are mounted on helmets, handlebars, or even long sticks, allowing them to capture and stream live video of your action sports exploits — or Disneyland adventures. A whole community has arisen around the GoPro video channels, which feature some amazing photos and videos. GoPro shipped more than 3.8 million sports cameras in 2013.

In a YouTube video, founder and chief executive Nick Woodman said, “The magic of GoPro is that we are enabling the world to communicate in this new way, to express themselves in a new way, and it’s snowballing.”

GoPro raised about $427 million at a fully diluted market cap of $3.5 billion and a valuation of $3.3 billion, according to Renaissance Capital. GoPro is trading on the NASDAQ under the ticker symbol GPRO.

Proceeds from the IPO will go to pay off a loan through its credit facility, as well as to fund new plans such as developing speciality hardware, bringing in revenue through a content management system, and making the company into a media company that sells user-generated content.

In other camera news, home-security company Dropcam was acquired last week by Google’s Nest for $555 million in cash. GoPro could very well find itself competing with the likes of Apple, Samsung, and Google. It already competes with consumer electronic companies such as Sony.

GoPro was founded in 2002 by Woodman, who used a $264,000 check from his parents to get started. Woodman rang the opening bell on the trading floor today.

In 2013, GoPro generated $985.7 million in revenue, compared to $526 million in 2012 and $234.2 million in 2011. The company was profitable in each of those years. Revenue for the three months ended March 31, 2014 was $235.7 million, down 8 percent from $255.1 million a year earlier. Units shipped in the first quarter were 900,000, down 11 percent from 1 million in 2013. The number of employees has grown from 49 at the end of 2010 to 718 at the end of March 2014.

You can see the whole S-1 here.