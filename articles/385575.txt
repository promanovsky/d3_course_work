Cameron Diaz left everything to the imagination at last night's Sex Tape premiere [SPLASH]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The stunning actress looked every inch the Hollywood starlet last night as she graced the red carpet for the premiere of Sex Tape in LA, opting for a stylish printed jumpsuit over a flesh-flashing ensemble.

The star - who is usually keen on showing off her stunning frame and slender pins - decided to cover up and cut a more casual figure for her appearance, which saw her posing up a storm on the red carpet.

With her hands in her pockets and her hair tied up in a gorgeous up do, Cameron looked stunning in the navy number, which featured white polka dots, a plunging neckline and a cinched in waist.