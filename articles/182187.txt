Michelle Obama, one of the more fashion-conscious first ladies in decades, joined a host of fashion luminaries on Monday as she cut the ribbon at the Metropolitan Museum’s new costume centre.

With a veritable who’s who list of top designers on hand — Calvin Klein, Oscar de la Renta, Michael Kors, Donatella Versace, Carolina Herrera, Marc Jacobs, Donna Karan, Ralph Lauren, Diane von Furstenberg and others — the first lady helped launch the museum’s new $40 million (Dh146 million) Anna Wintour Costume Centre, named for the editor of Vogue magazine.

After her remarks, the designers and other guests toured the centre’s inaugural exhibit, which features the work of Charles James, an influential mid-20th century American couturier.

“I’m here today because of Anna,” Obama said. “I’m here because I’m so impressed by Anna’s contributions not just to the fashion industry, but to the many causes she shares and cares about, particularly this great American museum.

“This centre,” she added, “is for anyone who is curious about fashion and how it impacts our culture and our history. And we know that that curiosity is out there.”

The first lady, who wore a forest-green silk organza dress with three-quarter sleeves by Naeem Khan, one of her favourite designers, said the centre would teach young people “that fashion isn’t just about what we wear, but that fashion is also a business, it is an art, it is a career that involves science, engineering, accounting and so much more.”

She also said she and Wintour, who has long been a key fundraiser for President Barack Obama, are working to bring students to the White House for a fashion workshop.

Others at the event included Tory Burch, Tommy Hilfiger, Zac Posen, Victoria Beckham, Alexander Wang, Mary Kate and Ashley Olsen of The Row, Reed Krakoff, Prabal Gurung and the Mulleavy sisters of Rodarte.

Obama did not attend the Met’s annual Costume Institute gala on Monday night, which typically attracts many celebrities and Hollywood A-listers.

Many of those guests were expected to try to channel the spirit of James, who died in 1978, into their outfits. Although his name is not well-known to the general public, he’s revered by fashion insiders and current designers.

“He really was a designer’s designer,” Kors said as he awaited the beginning of the ceremony. “Not only all the architecture and the structure that he put into the clothes, but he was a modernist, too. He thought about how a woman would get into and out of her dress. Or stay warm. I think you look at his clothes and they really stand the test of time.”

Posen said he had long been influenced by James, whose gowns, he said, exude “a deep love and passion for the sculpture of the human body.”

Through complex, innovative work with fabric from the 1930s to the 1950s, James designed spectacular gowns that often resembled sculptures more than mere garments. His clients included Gypsy Rose Lee, Marlene Dietrich and Mrs William Randolph Hearst Jr. None other than Christian Dior called him “the greatest talent of my generation.”

The new Met show, “Charles James: Beyond Fashion,” emphasises technology. In a large ground-floor gallery, animated videos illustrate how each gown was constructed, from the original piece of fabric to the intricate completed garment. The gowns stand individually on pedestals to display a 360-degree view.

A 1938 black gown in silk faille, one of the first strapless gowns to be made in the 1930s, is called the “Umbrella” evening dress because the folds of its skirt, structured with silk-encased “ribs,” resemble a folded umbrella.

A 1932 knee-length black dress is called the “Taxi Dress” because, James used to say, it was so easy to put on you could do it in a taxi — it was basically an early wrap dress. A 1933 black satin cocktail dress features an early use of a zipper seam. A “Ribbon Dressing Gown” is made entirely of ribbons of different widths, in peach, gold, yellow and ivory silk satin. The shape of the gown is formed not with seams and darts, but merely by varying the width of the ribbons.

James even designed the first elegant down-filled puffer jacket. Only one of them was made, said curator Harold Koda, touring the exhibit with a reporter recently, and it was passed around among his fans and clients.

But James was most proud of his striking 1953 “Clover Dress” in white satin and black velvet, with a full, sculptured skirt formed with four distinct “lobes” — like a clover. The gown’s wide skirt never touches the ground — it is meant to lift up on the dance floor and create a gliding effect. Met curators commissioned a full recreation of the dress so that they could better understand how it moved and what it was like to wear.

“Look at that,” remarked Posen as he examined the gown on Monday. “You can’t even see the seam on that velvet! It’s amazing.”

One room of the exhibit is devoted not to gowns but to biographical items — such as hats, which were James’ earliest designs (he started as a milliner in the 1920s), as well as prototypes for jewellery and typewritten notes that display his rather mercurial and demanding work style. One note lists celebrities James hadn’t dressed, but wanted to, including rockers Mick Jagger as well as David Bowie and Lou Reed.

James was born in England, but came to the United States at age 18, first to Chicago. He later centred his business in New York, catering to well-known socialites of the day.