YES, Zac Efron took off his shirt on stage, but surely the true star of this year’s MTV Movie Awards was Grumpy Cat.

Strutting a red carpet accompanied by a pesky human handler, clad in a stunning outfit made entirely from her own fur, the feisty feline donned a mini version of the oversized hat Pharrell Williams has been rocking for most of the year.

MTV MOVIE AWARDS: All the glitz and glamour

We’d tell Grumpy Cat she’s our pick for best dressed at the Awards ... but we don’t think he’d be very happy to hear it.

Grumpy cat took to Twitter to express her utter disdain for the event throughout the day — yes, you can tweet without opposable thumbs: