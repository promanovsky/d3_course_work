He was nine metres long, weighed several tonnes and had 53 curved, serrated teeth nearly as long as your hand – and Ebenezer the Allosaurus lived alongside humans a few thousand years ago.

Or at least he and all other dinosaurs did if the Creation Museum in Petersburg, Kentucky, is to be believed.

The museum, run by the Answers in Genesis ministry, plans to unveil its new star attraction on Saturday and issued a statement trumpeting the “world-class” status of the fossilized skeleton, which forms the centrepiece of their “Facing the Allosaurus” exhibit.

Mainstream science says the allosaurus, a creature similar to the Tyrannosaurus rex, lived in North America about 140 million years ago in the late Jurassic Period.

But the Creation Museum said in a statement that Ebenezer “met his end during Noah’s Flood about 4,300 years ago”.

“He likely tried to flee the violently rising waters surging over the land where he lived, and he was finally swept away and rapidly buried beneath tons of water-borne sediment, explaining the practically undisturbed arrangement of so many of his smaller bones,” the museum’s researchers said.

“Ultimately he and every air-breathing animal not on the Ark with Noah died (Genesis 7:22). Biblical history gives us a context for this dinosaur’s death and preservation in what’s called the ‘Jurassic’ portion of the fossil record.”

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The dinosaur was found in 2001 in northwester Colorado with many of its bones in their correct positions. Its 34-inch long skull was 97 per cent complete.

The museum said it was “thrilled about the great biblical creationist teaching opportunities” that the dinosaur would provide for “thousands and thousands of children and adults who visit”.

Museum founder Ken Ham said the dinosaur “will help us defend the book of Genesis and expose the scientific problems with evolution”.

“Evolutionists use dinosaurs to reach children more than anything to promote their worldview,” he added. “Our museum uses dinosaurs to help tell their true history according to the Bible.”

However Daniel Phelps, president of the Kentucky Paleontological Society, said that the Creation Museum “has decided, without doing research, that the dinosaur fossil is evidence of Noah's flood”.

Well-known US science educator Bill Nye discussed the biblical account of the flood with Mr Ham at the Creation Museum in February. The debate's live Web stream drew millions of viewers and intense national media attention.

Mr Nye said if “there was a big flood on the earth, you would expect drowning animals to swim up to a higher level,” which would mean their bones would be mingled with fossils known to be from a later time period. “Not any one of them did, not a single one.”