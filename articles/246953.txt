The Shield actor Michael Jace has been charged with the murder of his wife.

The 51-year-old, who played LAPD Officer Julien Lowe in the FX drama, was arrested after his wife April Jace was found dead at their LA home.

Richard Shotwell/Invision/AP



The AP reports that April was shot multiple times and police have since recovered a handgun which is thought to be owned by Michael.

Police Detective Dean Vinluan said that the actor called emergency services himself, telling police that he had shot his wife.

The officer added that there had been no reports of domestic violence at the property, located in the Hyde Park neighbourhood of Los Angeles.

Michael Jace is currently being held in custody on a $2 million (Â£1.186m) bail, and could face 50 years in prison if convicted. He is scheduled to appear in court today.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io