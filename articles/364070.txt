SCARSDALE, N.Y. - Carolyn Ngbokoli doesn't remember the sound of her mother's voice. She was just 19 when her mom died, and no recordings were left.

Now Ngbokoli, 37, faces the possibility of her own early death, from breast cancer. But she has made sure that her sons, 4 and 6 years old, can see how she loved them, hear how she spoke to them and be reminded of her advice to them long after she's gone.

With the no-cost help of an organization called Thru My Eyes, Ngbokoli, of White Plains, recorded a video of memories and guidance.

"I want to be able to tell my boys as much as I can and leave them something to look back on," she said.

Leaving a farewell video isn't new - Michael Keaton did it in a 1993 movie called "My Life" - but it is evolving beyond the version in which a dying person talks to an unmanned camera on a tripod or spends hundreds of dollars for a videographer who also records weddings and bar mitzvahs.

Thru My Eyes, based in Scarsdale, and Memories Live, of Milburn, New Jersey, are among the nonprofits filling a niche in which people with terminal diagnoses - usually cancer-stricken parents with young children - get emotional as well as technical support, for free.

E. Angela Heller, a social worker for cancer patients at New York's Presbyterian Hospital, has sent half a dozen patients to Thru My Eyes, which was founded by a cancer survivor.

"Every single one has said it's a wonderful experience," she said. "What makes this different is the deep support from the videographers. These people know illness, they know cancer. They know how to schedule around chemotherapy weeks."

Ngbokoli found the production to be an emotional process.

"There were times when I was laughing about funny things that happened to us," Ngbokoli said. "But then there were times when it was torturous, where I had to look in the camera and say, 'If you're watching this and I'm not here.'"

Carri Rubenstein, 61, is the co-founder and president of Thru My Eyes, which has completed more than 40 videos. A cancer survivor herself, she was inspired when she heard a friend with a bad diagnosis wish aloud a few years ago that she could find someone to help her make a video for her family.

Rubenstein wanted to make it a free service, so with the help of her lawyer husband, she formed the not-for-profit. She accepts donations and holds fundraisers.

At first, Rubenstein went to hospitals "looking for business," she said. Now she's getting calls from across the country.

Kathy Yeatman-Stock, a social worker in the cancer center at the Pomona Valley Hospital Medical Center in Pomona, California, contacted Thru My Eyes in hopes of getting patients at Pomona to make videos via Skype.

"People in the past have left letters and birthday cards for their children, but there is so much more impact with seeing the parent on film," she said.

Heller said patients are "facing their mortality in probably the most profound way" and want to give advice for their children at various life stages. "They say, 'I won't be at the wedding' but they want to give advice."

She said one mother read "Goodnight Moon" on the video so her children could hear it forever.

Such videos convey "a very personal touch, going beyond the stiff words you might have in your will, let's say," said Sally Hurme, a project adviser at AARP and author of "Checklist for Family Survivors."

Patients who want to make a video are given an interviewer, usually a volunteer health care professional who tries to take subjects through their lives. One prompt that always brings joy, Rubenstein said, is to talk about the day patients found out they'd be parents.

"Then we get into the first step, the first words, all the fun moments."

The videos run between an hour and 90 minutes and include photos, documents, music and interaction with the family.

Kerry Glass, 41, a former nursing home art therapist who runs Memories Live, says she prompts patients to talk about the overview of their lives as well as details: "the house you grew up in, your favorite game, your first job, your first car."

In one video, a man talks about growing up in a family of 10 in which the boys could never get into the bathroom and "would have to go outside to take care of whatever we had to take care of." Another talks about spaghetti and meatballs and says, "Marrying into an Italian family was probably the best move I ever made." A woman says she still gets "fluttery in my heart" when her husband enters a room.

Ngbokoli is enthusiastic about her video and says she's recording more family moments in hopes of being around to update it in a few years.

"It's a nasty cancer that I have, but I'm responding well," she said. "Every day's a gift, so as long as I'm here, why not document it?"