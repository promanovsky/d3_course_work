Chris Brown's trial on an assault charge has been delayed

A judge in Los Angeles has denied a request to release R&B singer Chris Brown from custody while he awaits trial on an assault charge in Washington, DC.

Superior Court Judge Victor Greenberg denied a request today by Brown's lawyer to release the Grammy-winner from custody either on bail or his own recognisance.

The singer has been jailed since mid-March on a no-bail warrant after he was expelled from a court-ordered rehab programme.

Brown's bodyguard has been convicted of assaulting a man in Washington, and Brown's trial has been delayed until at least June.

He remains under court supervision in Los Angeles for his 2009 attack on then-girlfriend Rihanna.

The singer's attorney, Bob Kalunian, requested Brown's release, but Judge Greenberg said there was no change in circumstances that warrants his release.

Online Editors