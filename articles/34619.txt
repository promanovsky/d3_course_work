U.S. health regulators have approved a new arthritis drug.

The Celgene Corp drug is designed to fight psoriatic arthritis, which can affect the skin (psoriasis) and cause symptoms such as joint pain, swelling, and stiffness, Reuters reported.

The drug is chemically known as apremilast, but will be sold commercially under the brand Otezla.

"Relief of pain and inflammation and improving physical function are important treatment goals for patients with active psoriatic arthritis," Curtis Rosebraugh, director of the Food and Drug Administration Office of Drug Evaluation II, said in a statement, Reuters reported.

"Otezla provides a new treatment option for patients suffering from this disease," he said.

In the future the drug could also help treat psoriasis and ankylosing spondylitis, Reuters reported.

Pregnant women who are using the drug will be required to join a registry to determine the risk it could impose.

Those taking Otezla will also be monitored for weight loss; if significant weight loss is observed it is recommended that the patients be switched to a different treatment.

Psoriasis is the "most common autoimmune disease in the U.S.," Bloomberg Businessweek reported. The condition causes symptoms such as red scaly patches on the skin. Psoriatic arthritis causes "pain, stiffness and swelling in the joints."

About 30 percent of the 125 million people in the world are affected by the condition; It is believed to affect 7.5 million American, Bloomberg reported.

According to research from 14 analysts, Otzela will bring in about $1.1 billion in sales by the year 2017.

"Psoriatic arthritis can affect any joint in the body, and it may affect just one joint, several joints or multiple joints. For example, it may affect one or both knees. Affected fingers and toes can resemble swollen sausages, a condition often referred to as dactylitis. Finger and toe nails also may be affected," the American College of Rheumatology stated.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.