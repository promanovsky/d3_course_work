Seattle-based wireless carrier T-Mobile has called on the three other top U.S. wireless companies to end its use of overage fees, the Los Angeles Times reported.

T-Mobile launched its "Abolish Overages" petition through Change.org on Monday morning to get AT&T, Verizon and Sprint to end overage fees. The company said that 20 million cellphone users got hit with the overage fees last year, and the three carriers earn more than $1 billion in overages per year.

T-Mobile Chief Executive Officer John Legere criticized the other carriers in a scathing statement that challenged them to end the overage charges.

"I'm laying down a challenge to AT&T, Verizon and Sprint to join T-Mobile in ending these outrageous overage penalties for all consumers -- because it's the right thing to do," Legere said. "Overage fees are flat out wrong."

Overage charges occur when customers go over their paid monthly data allotment. Beginning in May, T-Mobile said it would not charge overage fees to its customers with older plans anymore, according to USA Today.

The company already doesn't charge overage fees for customers signed on with the company's Simple Choice and newly released Simple Start plan. The Simple Start plan, which was unveiled last week, gives customers the option to pay for "additional data sessions" if they go beyond their 500 megabyte monthly limit. Customers only pay the overage fee with their approval rather than have the charge tacked onto their monthly bill automatically.

At the time of this publication, T-Mobile had reached 3,896 supporters and needs 1,104 more to reach its goal.

"Charing overage fees is a greedy, predatory practice that needs to go," Legere said. "It's going to be a ball to not only make this statement to our customers, but to watch millions of Americans just flip the bird on the insanity."