VENICE (CBSLA.com) — A Venice man was robbed of his Google Glass and a laptop computer after being threatened with a Taser, police said Tuesday.

The incident was reported around 8 p.m. Monday at On The Waterfront Cafe, which is located at 205 Ocean Front Walk.

Detailed descriptions of the two suspects were unavailable, police said.

The alleged robbery comes after an increase in the number of Google Glass-related crimes nationwide, including a Sacramento reporter who claims he had his Google Glass snatched from his face while walking down the sidewalk Friday.

KNX 1070’s Megan Goldsby reports Google Glass — which is priced around $1,500 and features a built-in computer on a customizable lightweight frame — allows a user wearing the device to capture what they see before saving it or sending it.