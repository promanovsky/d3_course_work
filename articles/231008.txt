Market Brief

The BoJ refrained from adding additional monetary stimulus given the improvement in economic data recently. The monetary base target remains unchanged at 270 trillion yen in 2014. In addition, Japanese trade deficit narrowed from Yen -1,446.3bn to Yen -808.9bn in April (vs. Yen -844.6bn exp.), exports surged from 1.8% to 5.1%, while imports tumbled from 18.1% to 3.4%. The Nikkei stocks and JPY crosses were downbeat in Tokyo. The pair aggressively sold-off to 100.82 as Europe walked in. The buying interest in weakens below 200-dma (101.26), negative bias confirms. The key support stands at 100.76 (2014 low), stops are touted below. Option barriers trail below 101.70 for today expiry. More resistance is seen at daily Ichimoku cloud base (102.44). On a similar patter, EUR/JPY consolidates weakness. The key support remains at 200-dma (138.09).

G10 Advancers - Global Indexes

In Australia, the Westpack consumer confidence fell 6.8%, to 92.9 (lowest in more than 2 years). spiked down to 0.9216. The bearish trend confirms, the next key support is seen at 0.9203/09 (May 2nd low & Fib 50.0% on Oct’13 – Jan’14). Option barriers at 0.9300 are likely to keep the upside limited today. breaks the 21-dma on the downside, now testing 1.0753/65 (50 & 100 dma).

rebounded from 1.3678 yesterday and traded in the tight range of 1.3698/1.3710 in Asia. The bias is clearly negative; however the bearish moves remain interestingly fragile. Deflation fears, rush into the peripheral bonds keep the downside anchored. The short-term resistance remains at 100-dma (1.3739), support stands at 200-dma (1.3635). The sentiment is skewed to negative yet ECB officials’ dovish comments gather no significant reaction, looser ECB is already priced in. Large option bids wait to be activated above 1.3800.

extended losses to the fresh year low of 0.81196 in London yesterday. The cross approaches our 0.80850 target given the positive UK divergence based on fundamental data. The UK will publish April retail sales (at 08:30 GMT) and is likely to meet the optimistic market expectations given the CPI, RPI and PPI dynamics over the same month. We remind that the UK’s April CPI already hit BoE’s 1.8% forecast for 2014/15. The BoE will release May meeting minutes today in London (08:30 GMT), and we see more probability in hawkish surprise. Remember, last week’s QIR report has mentioned “MPC central view”. If there is divergence in MPC expectations, it can only be hawkish given the economic situation. How many of 9 MPC members would break unanimity in the heart of the BoE, if any?

The BoE and the Fed will release meeting minutes today. Economic calendar consists of Swiss April M3 Money Supply, ECB March Current Account, UK April Retail Sales Ex & Incl. Autos m/m & y/y, US May 16th MBA Mortgage Applications, and Euro-Zone May (Prelim) Consumer Confidence.

Todays Calender

Currency Tech



EUR/USD

R 2: 1.3775

R 1: 1.3740

CURRENT: 1.3720

S 1: 1.3678

S 2: 1.3635



GBP/USD

R 2: 1.6996

R 1: 1.6903

CURRENT: 1.6844

S 1: 1.6785

S 2: 1.6732



USD/JPY

R 2: 102.44

R 1: 101.70

CURRENT: 100.95

S 1: 100.76

S 2: 99.57





R 2: 0.9003

R 1: 0.8960

CURRENT: 0.8905

S 1: 0.8883

S 2: 0.8840