Two years after she brought Google Glass to the New York Fashion week runway, fashion designer Diane von Furstenberg has teamed with Google Inc. to launch a line of designer frames for the smart glasses to hang on.

Von Furstenberg-- best known for her easy-to-put-on wrap dresses -- helped reshape the $1,500 computerized eyewear into something that looks more like a normal pair of glasses.

Women’s fashion retailer Net-A-Porter.com is selling the limited collection in an $1,800 package that include Glass (in black, green, plum, brown or white), a frame and shade, a mono ear bud and a case. The site’s men-focused companion, MrPorter.com, is selling a $1,650-package with frame varieties of either bold, thin or half-and-half.

Elements of the packages can be bought separately through Google. The frames support prescription lenses.

Advertisement

Though they don’t completely shed the sense that Glass wearers are robots, the upscale frames have been applauded by some consumers as giving more style to Glass.

Potential Google Glass buyers have complained that Google’s original version isn’t appealing, and they have no intention of buying the smart glasses until Oakley, Ray-Ban, Armani or the like give it a makeover.

Google has said the DVF | Made for Glass line is expected to be the first of many such collaborations.

In addition to a growing number of apps, Google Glass allows users to do online searches, use maps, take photos and record videos through a combination of voice commands and taps on the frame. On Monday, Google opened up sales of Glass outside the U.S. People in the United Kingdom can now buy Glass for £1,000, or about $1,700.

Advertisement

Chat with me on Twitter @peard33