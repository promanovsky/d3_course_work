Yahoo Inc. ( YHOO ) shares tumbled 6.6% to settle at $34.07 after the Chinese e-Commerce giant, Alibaba, filed for an initial public offering (IPO) with the U.S. Securities and Exchange Commission.

Founded in 1999, Alibaba is primarily owned by Yahoo! Inc., Japan's Softbank Corp. as well as other founders and senior managers. Reportedly, Alibaba is expected to raise $15 to $20 billion from the IPO. However, the exact amount remains unclear. After Google Inc. ( GOOGL ) and Twitter Inc. ( TWTR ) IPOs that raised $1.9 billion in 2004 and $2.09 billion in 2013, respectively, Alibaba is expected to be the next major Internet stock in the U.S. market, close to Facebook Inc. ( FB ), which raised about $16 billion in 2012.

Reportedly, Alibaba's IPO filing showed that Yahoo holds a 22.6% stake in Alibaba as against earlier expectations of 24%. The unexpected details revealed in the Alibaba IPO filing became a reason of concern for many Yahoo shareholders as can be seen from yesterday's steep price fall. Investors now expect Yahoo!'s Alibaba stake to be worth less than anticipated, creating a panic in the market.

However, Alibaba is the most valuable asset Yahoo owns, and shareholders will continue to benefit from Alibaba's growth, as Yahoo is required to sell only 40% of its current holdings in Alibaba, while retaining the rest.

Alibaba's impending IPO will likely have a major impact on Yahoo. The IPO deal will generate enough cash for CEO Marissa Mayer to reinvest in any new initiatives to get the core business into decent shape. Mayer could bring a new product focus to the company and spend some of the proceeds on acquisitions. She could also return some cash to investors, either as dividends or share repurchases.

We believe that prudent usage of the cash received could revive core revenue growth and narrow the ever-widening gap between Yahoo and its main competitors, Facebook and Google.

Since 2006, Yahoo had been striving to improve its financials and build shareholders' confidence. However, since Mayer took over Yahoo's reins, there has been a tremendous drive to optimize Yahoo products for mobile devices, as well as intense focus on building more engaging content. The efforts have not had a material impact on Yahoo's results yet, with shares mostly responding to the strength in Alibaba and the stake in Yahoo Japan.

Post the IPO euphoria, shareholders and investors will be focused on the core business. Therefore, Mayer now needs to prove that the company's turnaround is for real.

Yahoo shares carry a Zacks Rank #3 (Hold). Another stock that has been performing well and is worth considering include Rovi Corp. ( ROVI ), carrying a Zacks Rank #1 (Strong Buy).

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

FACEBOOK INC-A (FB): Free Stock Analysis Report

ROVI CORP (ROVI): Free Stock Analysis Report

TWITTER INC (TWTR): Free Stock Analysis Report

YAHOO! INC (YHOO): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.