It's as good a reason as any to put a ring on it: married folks are five times less likely than their single, divorced or windowed counterparts to develop heart disease.

While making it up to the altar and back is unlikely to be the sole driving force behind the trend, a new study of 3.5 million Americans found that vascular disease had a lower incidence rate in married people than in any other marital status bracket. The same pattern persisted among younger couples, with those below 50 witnessing a 12 percent drop in the likelihood of developing heart disease. For ages 51 to 70, that number became seven percent, and couples aged 61 and over experienced just a four percent drop.

"The association between marriage and a lower likelihood of vascular disease is stronger among younger subjects, which we didn't anticipate," said the study's lead author Dr. Carlos L. Alviar in a statement.

Divorced or widowed participants were more likely to experience vascular issues, the study found, in direct contrast to their married or single friends. Of the demographics surveyed, widowers were subject to a three percent increase in vascular issues, with divorcees experiencing a similarly elevated risk.

Other factors were also taken into consideration, including high blood pressure and whether or not the test subjects were smokers. The results were then adjusted to account for discrepancies as a result of sex, race, age, and additional cardiovascular risk factors. The study is not the first of its kind to link sound health with marital status - previously findings have suggested that married people may experience higher cancer survival rates and better bone density.

Despite the established patterns, Dr. Alviar noted that marriage wasn't the only factor that saw a reduction in heart disease rates. "These findings certainly should not drive people to get married, but it is important to know decisions regarding who one is with, why, and why not may have important implications for vascular health." He continued: "Of course, it is true not all marriages are created equal, but we would expect the size of this study population to account for variations in good and bad marriages."

The findings of the study will be presented at the American College of Cardiology's 63rd Annual Scientific Session on March 30.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.