WASHINGTON (AP) — Mortgage giant Freddie Mac posted net income of $4 billion for the January-through-March period, helped by a strengthening housing market. The government-controlled company has turned a profit in each of the past 10 quarters.

Freddie's first-quarter profit reported Thursday compared with earnings of $4.6 billion in the first three months of 2013.

Net income was bolstered in the latest period by a decline in mortgage delinquencies, the company said.

McLean, Virginia-based Freddie said it will pay a dividend of $4.5 billion to the U.S. Treasury next month. Freddie already had repaid its full government bailout of $71.3 billion after paying its third-quarter 2013 dividend.