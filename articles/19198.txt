Mick Jagger has paid tribute to his girlfriend L'Wren Scott who was found dead in her New York apartment.

The Rolling Stones frontman described the 49-year-old fashion designer, who was found hanged in her Manhattan home in a suspected suicide, as a women with "great presence".

"Her talent was much admired. I will never forget her," he said.

The 70-year-old rocker, who has been partnered with Scott since 2001, was on tour in Perth, Australia, when news broke of her death. The group has cancelled forthcoming concert dates.

In a message to fans on his Facebook page, Jagger expressed his grief over his loss. She was said to have been struggling with financial problems.

"I am still struggling to understand how my lover and best friend could end her life in this tragic way," he wrote.

"We spent many wonderful years together and had made a great life for ourselves.

"She had great presence and her talent was much admired, not least by me."

He signed off the post by thanking fans for their support during a difficult time.

"I have been touched by the tributes that people have paid to her, and also the personal messages of support that I have received. I will never forget her."

Scott, who had been a model before venturing into design, had created outfits for the likes of Michelle Obama, Nicole Kidman and Naomi Campbell.