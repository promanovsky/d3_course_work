Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Kim Kardashian gave Kanye West a board game for his birthday.

The 33-year-old reality star bought her husband My Monopoly, which allows buyers to name properties, make their own cards and create game pieces, as a gift for his 37th birthday on June 8.

Kim reportedly put her own sentimental spin on the Hasbro product, which won't be available in stores until August, by placing the Palace of Versailles - where the couple held their wedding rehearsal dinner - and Italy's Fort Belvedere - where their nuptials were held last month - on Kanye's customised board, according to Us Magazine.

Kim, who changed her surname to Kardashian West following her wedding on May 25, also treated her husband to a heartfelt message on Instagram on his birthday, which she accompanied with a stunning black and white photograph of them sharing a kiss.

She wrote at the time: "Happy Birthday to my husband and best friend in the entire world! You have changed my life in more ways than you know! The way you look at life inspires me! I love you so much!!! (sic)."

The brunette beauty, who has 12-month-old daughter North with Kanye, also had a special cake made for the occasion, which spelled out the word 'Yeezus', a reference to the rapper's current tour, in thick grey icing.