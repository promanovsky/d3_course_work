When Google announced Project Ara last October, its plan to make modular smartphones, it shared some photos and very little else. This week, at the Computer History Museum in Silicon Valley, the company is digging into the nitty gritty, by hosting the first Project Ara developer conference. It’s showing prototypes in public for the first time and explaining the technology to the hardware engineers it hopes will build stuff for the platform.

Back in February, I wrote the first in-depth look at Project Ara. It includes most of the key facts Google is discussing at the developer conference. (At least so far: It’s still in progress.) Here’s a recap of what makes Project Ara so ambitious, fascinating and — in some respects — odd.

1. It’s an infinitely customizable phone. Every feature — the screen, the cameras, the battery, stuff nobody has invented yet — comes in the form of a tile-shaped module. You slip these modules into a framework called an “Endo” to build a phone with the features of your choice. And modules are interchangeable, so you could decide to skip the rear camera and slide in a second battery, for instance.

2. It’s not going to be for you, at least at first. The concept sounds like it’s aimed at lovers of bleeding-edge gadgetry. But Google wants to offer Project Ara phones to folks who’d otherwise be unable to afford any smartphone. It plans to roll out the platform in developing nations first, and isn’t saying when it might reach the U.S.

3. The cheapest, most basic phone will be very cheap and very basic. With the target market in mind, Google aims to offer a $50 “grayphone” starter model — no wireless contract required. That version wouldn’t have frills such as one or more cameras. It wouldn’t even be capable of working on cellular networks — just Wi-Fi. But owners could upgrade their grayphones on the fly as their needs changed and budgets permitted.

4. Google is trying to do this fast and efficiently. Work began on Ara in earnest only a little over a year ago, and only a handful of Google employees are involved, along with outside collaborators as required. The company plans to have its first phone on the market in January 2015.

5. It’s inspired by the U.S. Department of Defense’s approach to innovation. Project Ara is part of Google’s Advanced Technology and Projects group, which models its small-team, tight-deadline approach on the Defense Department’s fabled Defense Advanced Research Projects Agency, which brought us the Internet and satellite navigation, among other things. Regina Dugan, who heads ATAP, is a former DARPA director; Paul Eremenko, who’s spearheading Ara, is also an alumnus.

6. Google thinks of it as Android for hardware. The company’s mobile operating system has done well because it’s essentially a joint effort between Google and the multitudes of software developers who have embraced it. The idea of Project Ara is to allow even tiny companies with inventive ideas to make modules and market them to phone owners — a big shift from the current situation, in which a few large manufacturers crank out one-size-fits-all phones designed to please the masses.

7. The phone isn’t as bulky as you’d expect. You can’t build a phone made out of multiple blocks and make it as skinny as the skinniest entirely-self-contained handsets. But Google’s prototype is 9.7mm thick, which is only half a skosh chunkier than the new HTC One M8. (The final shipping version may be slightly thicker.)

8. It won’t fall apart if you drop it. At least that’s the idea. The modules will use capacitive technology for electrical connections, and will lock in place using super-strong magnets (for modules on the back) and latches (for ones on the front). Google says an Ara phone should be as sturdy as a typical smartphone.

9. The project involves some 3D printing breakthroughs. Project Ara modules will be encased in covers that will be produced on demand using a new generation of 3D printers designed by 3D Systems. Consumers will be able to pick custom designs and snap new covers onto their old modules if they choose.

10. Google’s vision for how Project Ara phones will be marketed is pretty wacky. The company is designing portable stores, which it will be able to ship by sea to the first countries where Ara phones will be available. It’s also developing technology that will do things such as measure your pupil dilation and scan your social networks to help you choose an Ara phone that matches your personality.

11. The platform is going to require lots of enthusiasm from third parties. The only Google-branded part of the hardware will be the Endo. Everything else, like batteries, wireless subsystems, cameras and sensors will be produced by other companies, who will presumably only choose to get involved if they think they can make money. If only a handful of such companies buy the vision, it won’t work.

12. Being both excited and skeptical is a reasonable response. I’m glad Google is trying this: It involves both a big dream and multiple technological innovations, and it’s going to be awfully neat if it takes off.

But that doesn’t mean that I think the folks who are instinctively dubious — such as Daring Fireball’s John Gruber — are being unreasonable. Many things have to fall into place for Ara to evolve from a wild concept to a functioning product to something large numbers of people want. And if Google does indeed have a phone ready to sell in January of next year, it’s not the end of the journey, but the beginning.

I’m not placing any bets on its chances of success, but I can’t wait to see how the world — and especially the smartphone newbies who Google envisions would want this — will react.

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.