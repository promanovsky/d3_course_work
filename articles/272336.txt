Bishop Wilton D. Gregory listens to Anne Burke, the Acting Chair of the National Review Board discuss her perspective on the progress being made in terms of sexual abuse within the Catholic church, on November 11, 2003 at the U.S. Conference of Catholic Bishops in Washington..(UPI/Michael Kleinfeld) | License Photo

NEW HAVEN, Conn., June 2 (UPI) -- One in 8 U.S. children will experience maltreatment in the form of neglect, physical, sexual or emotional abuse by the time they reach age 18, researchers said.

The study, published in JAMA Pediatrics, also found 1 in 5 black children and 1 in 7 Native American children experienced maltreatment by age 18.

Advertisement

Author Christopher Wildeman, an associate professor of sociology and faculty fellow at the Institution for Social and Policy Studies at Yale University, and colleagues used data from the National Child Abuse and Neglect Data System Child File.

The database included information on all U.S. children with a confirmed report of maltreatment. From 2004 to 2011, the database indicated more than 5.6 million children experienced maltreatment.

"Confirmed child maltreatment is dramatically underestimated in this country. Our findings show that it is far more prevalent than the 1 in 100 that is currently reported," Wildeman said in a statement.

"Maltreatment is on the scale of other major public health concerns that affect child health and well-being. Because child maltreatment is also a risk factor for poor mental and physical health outcomes throughout life, the results of this study provide valuable epidemiologic information."