Lena Dunham definitely seems comfortable in front of the camera, but the creator and star of "Girls" hints that a long-term career in acting may not be in her future.

Lena Dunham appears on the April 2014 cover of Glamour. Glamour

In a new interview with Glamour magazine, Dunham said, "I don't know if I'm going to want to act anymore. I'm always relieved on the days I don't have to. I'd rather give parts to other women than be the woman having the parts."

Considering she also writes and directs, it sounds like Dunham doesn't have to rely on her acting skills to make a living. Prior to "Girls," Dunham, 27, wrote, directed and starred in the 2010 critically-acclaimed movie, "Tiny Furniture." She made an appearance in 2012's "This is 40" and will star in the upcoming "Happy Christmas" with Anna Kendrick.

Dunham also opens up about her boyfriend, fun. guitarist Jack Antonoff, in the magazine, saying, "I remember talking with him on our first date and him being like, 'God, all the articles about your nudity on the show are such bull****.' It's funny, 'cause in some ways that's the conversation we still have when I'm upset [about stuff I read]."

The April issue of Glamour hits newsstands March 18. Go here for more of her interview.



