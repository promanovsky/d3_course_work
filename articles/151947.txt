NORTH TEXAS (CBSDFW.COM) – Each year the Asthma and Allergy Foundation of America (AAFA) puts out a list of the “100 Most Challenging Places to Live With Allergies.” Usually the Dallas area is ranked about a quarter of the way down – this year Dallas is number 7! Dallas ranked 23 in 2013.

Sniffling, sneezing, runny noses… all signs that it’s allergy season. You aren’t alone if this year seems particularly bad.

So what cause the change? Well, there’s more pollen in the air and allergy experts also blame this winter’s unusual cold.

Now all the plants are blooming at the same time. In most years, trees, plants and weeds will bloom at different times.

Unfortunately, short of going to live on a cruise ship, doctors say do your best is to avoid the pollen. And that can be tough because here in North Texas pollen is everywhere, not just at parks and playgrounds.

It seems springtime in North Texas is briefly sandwiched between bitter cold and blazing hot – not good news for allergy sufferers.

For the Wallace family, the seasonal sniffling and sneezing is even worse this year.

Alysia Wallace says the allergic reactions are far-reaching. “It seems to be affecting everybody — even people who usually don’t have allergies.”

Allergy patient John Sadlowski is staring to feel it. “For the past couple of days my allergies have been acting up and my throat has been really sore. [My] nose is running a ton and it’s affecting my asthma as well.”

According to the AAFA, some 45 million Americans are living with nasal allergies and 25 million have asthma.

Experts say, this year it is more than just misery talking. Doctor Richard Wasserman is an allergist-immunologist in Dallas. He admitted that, “Most of the time I kind of nod and grin, but this year I really think it is the worst year.”

He blames the long, cold winter. “This year, everything started late. Trees didn’t really start until very late February, early March. And oak, which is usually finished by the end of March, is still out there.”

Doctor Wasserman suggests keeping windows and doors closed. If you choose not to 80-percent of the outdoor pollen count will be found inside. He says over-the-counter nasal saline sprays will help, in addition to over-the-counter nasal steroids. Of course he recommends seeing an allergist if none of those things seem to work.

Remember, even if you keep the windows and doors closed you need to be aware of how much pollen you’re carrying inside after spending time outdoors. “You’re coated with pollen,” Dr. Wasserman said plainly. “If pollen glittered you’d look like you were wearing an evening gown. So, you should go straight to the shower. The clothes should go straight to the hamper.”

Don’t buy into the myth that rain will make things better by ‘cleaning’ the air. Doctor Wasserman said rain doesn’t help as much as we may think. He said while the water washes the pollen from the air, even more pollen is left behind when it evaporates and it lingers right about nose level.

(©2014 CBS Local Media, a division of CBS Radio Inc. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: