Kendall Jones is a 19-year-old from Texas, a university student and a former cheerleader - but those characteristics of her life are not what she posts about on Facebook.

Instead Kendall's pictures mostly feature her posing next to large animals she has hunted and sometimes killed, legally, in Africa.

The page has attracted huge amounts of criticism in the past few days - with thousands of comments and online petitions calling for her to be stopped from hunting in Africa. But she is arguing back, comparing what she does to former US President Theodore Roosevelt, who started the country's national parks.

Video Journalist: Anna Bressanin

You can follow BBC Trending on Twitter @BBCtrending

All our stories are at bbc.com/trending