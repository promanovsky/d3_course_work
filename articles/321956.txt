Adults who watch TV for three hours or more every day could be doubling their risk of premature death.

"Television viewing is a major sedentary behavior and there is an increasing trend toward all types of sedentary behaviors," Miguel Martinez-Gonzalez, M.D., Ph.D., M.P.H., the study's lead author and professor and chair of the Department of Public Health at the University of Navarra in Pamplona, Spain., said in an American Heart Association news release. "Our findings are consistent with a range of previous studies where time spent watching television was linked to mortality."

To make their findings researchers looked at 13,284 healthy participants with an average age of 37 to see if certain sedentary behaviors increased the likelihood of early death. The team looked at time spent watching television, computer time, and driving time. The participants were followed for median time of 8.2 years. During that time there were 97 deaths: "19 deaths from cardiovascular causes, 46 from cancer and 32 from other causes," the news release reported.

The team found the risk of death doubled in participants who watched three or more hours of TV every day compared with those who watched less than an hour. The researchers did not find a significant link between computer time or driving time and a higher risk of premature death from any cause. More research will be needed to see what biological mechanisms are behind the link between watching television every day and the risk of premature death.

"As the population ages, sedentary behaviors will become more prevalent, especially watching television, and this poses an additional burden on the increased health problems related to aging," Martinez-Gonzalez said. "Our findings suggest adults may consider increasing their physical activity, avoid long sedentary periods, and reduce television watching to no longer than one to two hours each day."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.