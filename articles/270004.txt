Ann B. Davis' former The Brady Bunch co-stars have paid tribute to the late actress, who died on Sunday (01Jun14).

The 88 year old, who played beloved housekeeper Alice Nelson on the classic sitcom, passed away from a subdural haematoma after slipping and hitting her head in the bathroom at her home in Los Angeles.

After receiving news of her death, Florence Henderson, who played family matriarch Carol Brady, took to Twitter.com and shared her grief, writing, "I'm so shocked & saddened to learn my dear friend & colleague Ann B Davis died today. I spoke with her a few months ago & she was doing great."

Maureen MCCormick, the show's Marcia Brady, tells The Hollywood Reporter, "I admired Ann B so much as an actor... She was one of the greats. Most of all, I admired her heart. She was a dear friend... deep, honest and true. She was one of my earliest role models, and that continues to this day. She made me a better person. How blessed I am to have had her in my life. She will be forever missed."

Eve Plumb, Jan Brady on the sitcom, told TheWrap.com that Davis was "an amazing lady", adding, "She was great to work with, and I have wonderful memories of our scenes together on The Brady Bunch."

Plumb continued, "She was kind and generous to all of us on set. Although we hadn't seen each other as often as we may have wanted to in the last few years, I am sure she knew she held a very important place in my heart. My thoughts are with her family and friends."

Meanwhile, a slew of celebrities have added their own condolences, including Christina Applegate, who tweeted, "Ann B Davis. How many mornings I have spent with you. Rip", and while actress Marlee Matlin added, "I grew up wanting to be Marcia Brady but with 4 kids, I turned into Alice the Maid. Rip Ann B. Davis."