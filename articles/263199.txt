Consumer shares were generally higher in pre-market trade Wednesday.

In consumer sector news, DSW ( DSW ) plunged about 20% after it reported Q1 2014 EPS of $0.42, up from $0.38 in Q1 2013 but missing analyst estimates of $0.48. Total sales of $598.94 million were down from $601.36 million in Q1 2013 and missed analyst projections of $622.33 million.

And, coverage of home improvement retailer Lowe's Companies ( LOW ) shares were down 0.9% after the stock was been downgraded by analysts at Canaccord Genuity to a sell rating from a hold.

The firm also lowered its price target on the stock to $37 from $47 a share.

Finally, Brown Shoe Company ( BWS ) reported fiscal Q1 earnings that exceeded analysts' expectations and raised its FY EPS guidance.

The footwear retailer and wholesaler said adjusted earnings in the quarter ended May 3 rose to $0.35 per share from $0.32 the year earlier, exceeding the $0.31 average estimate from analysts polled by Capital IQ. Sales rose just 0.4% to $591.2 million, shy of the $594 million consensus estimate.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.