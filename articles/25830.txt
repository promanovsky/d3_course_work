The Android 4.4 KitKat update for Samsung Galaxy devices in Canada began last week with the Galaxy S4 on the mobile network Rogers receiving the new OS. Now several Canadian carriers are rolling out the Android 4.4 update for the Galaxy Note 3.

In addition to Rogers, mobile networks Bell, Telus, Videotron, Eastlink and WIND Mobile are now sending out Android 4.4 over-the-air updates for the Galaxy Note 3, tech website Mobile Syrup reports. Rogers has updated its release schedule, indicating that the Galaxy Note 3 is now receiving Android 4.4. Users on the Android Central forums have also indicated that the Galaxy Note 3 on Telus and Bell have begun to update.

Build numbers are not currently available for the respective Galaxy Note 3 model updates, but the firmware is weighing in at a hefty 437.05MB. Many adopters report a long download time, so people waiting to update will want to have a strong Wi-Fi connection or mobile data on. Users will also want to have their devices charged at least 50 percent before proceeding with Android 4.4 installation.

The Android 4.4 update should be available for manual download by accessing Settings > General > About device > Software update on Canadian Galaxy Note 3 handsets.

The Android 4.4 KitKat update for Samsung Galaxy devices commenced in February, and now several Galaxy S4 and Galaxy Note 3 models have received the new operating system in various locations and for various carriers. Most recently, the T-Mobile Galaxy S4 and Galaxy Note 3 updated to Android 4.4 in the U.S., as did the Rogers’ Galaxy S4 in Canada. The French carrier SFR has also updated its Galaxy Note 3.

Follow me on Twitter.