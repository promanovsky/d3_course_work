Prime Minister David Cameron has called for global action to tackle the growing threat of resistance to antibiotics.

Speaking to BBC health correspondent Fergus Walsh, Mr Cameron warned the world was "in danger of going back to the dark ages of medicine" if governments and drug firms failed to act.

Growing numbers of infections are resistant to antibiotics but no new classes of anti-microbial drugs have come onto the market for more than 25 years.

The prime minister also announced a review of the market and explained why it would be led by economist Jim O'Neill.