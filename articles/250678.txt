Google is apparently distributing its Project Tango 3D-scanning tablets to developers next month.

The 7-inch slates are capable of making 250,000 3D measurements per second and able to create detailed 3D maps based on the user's surroundings.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Potential applications of the Android devices include mapping out interiors and helping visually-impaired users find their way around inside unfamiliar buildings.

Google will hand 4,000 of the Project Tango tablets to developers after its I/O developer-focused conference in June, according to The Wall Street Journal.

The web giant's Advanced Technology and Projects group previously incorporated the technology into smartphones, revealing prototype devices in February.

It is yet to be confirmed when the technology will be adapted for commercial use.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io