Whatever else you want to say about Facebook’s acquisition of virtual reality startup Oculus VR, give Mark Zuckerberg credit for having the nerve to pursue and pull the trigger on a $2 billion deal with a still-unproven (and historically lackluster) technology.

But then that’s the purview of companies with unfathomable cash hoards, and so on the other hand, perhaps we shouldn’t give Zuckerberg credit. This is what companies like Facebook — whose start-out names belie their evolving all-consuming functions — wind up of necessity doing. Google is just a search engine company, after all — except when it’s mapping the planet, inviting us to become cyborgs and working to conquer death itself.

There’s a sense, sometimes, that whatever gets this much attention is the next zeitgeist, but it’s easy to confuse the latter with spectacle, and there’s a lot about virtual reality technology in 2014 still deeply mired in hype.

Analyst Michael Pachter may or may not be right about Sony’s VR contender, Project Morpheus, being “a really bad idea for Sony,” but I think he’s spot-on when he said this in a recent interview with DualShockers:

…I don’t think it’s [virtual reality] gonna be a big market. It sounds interesting, but I don’t think there will be enough content to justify making the capital investment … I think it’s chicken and egg. If there’s no content you’re not gonna buy a virtual reality headset, and if you don’t buy a virtual reality headset, there won’t be any content, because no one will make a dedicated game for a very small audience.

That’s the challenge — growing that “very small audience.” If Facebook can develop some sort of killer app for the platform, be it virtualizing how we interact with one another (or the inexorable rise of VR erotica), then maybe. But now you’re talking about overcoming cultural conventions and assumptions, and that takes time and generational turnover. Make no mistake: We’re talking about a paradigm shift much bigger and broader and upending than moving from pen and paper to typewriter, or from typewriter to mouse and keyboard. Taking that shift mainstream is Facebook’s challenge, and every company fooling with virtual reality technology today is still well off from meeting it.

Let’s walk through a few choice quotes circulating in the wake of the announcement last night, starting with Mark Zuckerberg’s acquisition manifesto.

“It’s incredible.” (Mark Zuckerberg, referring to Oculus’s virtual reality technology)

The notion of wrapping large and cumbersome objects around your head, possibly tethered via restrictive cables to computers, to simulate a relatively low-res and crude version of someone’s notion of an alter-verse is arguably not incredible. It’s a compromise, and I’d say still, in 2014, a pretty big one. Even with the advances Oculus VR’s made in recent years, it’s still radically unlike the wraparound promise of virtual reality in films like The Lawnmower Man and The Matrix.

Incredible would be a direct neural interface. Incredible would be understanding the brain well enough to make that sort of connection. Incredible would be effortless, fully immersive virtual reality (Zuckerberg is spinning big and bad when he describes Oculus Rift as “a completely immersive computer-generated environment”). Oculus VR is none of these things. And I say that as an investor in the technology: I just ordered the devkit 2. The difference between me and a guy like Zuckerberg is that I see Oculus VR for what it is: another stepping stone in a long line of stepping stones we’ve been slowly traversing for decades.

“Oculus’s mission is to enable you to experience the impossible.” (Zuckerberg on the nature of virtual reality)

Nope. Oculus’s mission is, in fact, to enable you to experience the next stepping stone on the yellow brick road to bona fide immersive simulations well down the road. The “impossible” would be time travel, or maybe faster than light travel, or doing Harry Potter-style magic, or comprehending infinity. Oculus Rift is very much about grappling with the possible and long-expected.

“Our mission is to make the world more open and connected.” (Zuckerberg, describing Facebook)

Facebook’s mission is, and this is generally a legal matter, to maximize profits. That’s first and foremost. Along the way, the company might manage to make the world more open and connected because those interests dovetail, but the paradox is that doing so involves concentrating access to and control of all that openness and connectedness in the hands of a single corporate entity.

That’s been well and good for the sort of presence-detached interface that’s been Facebook’s stock in trade for years, but what would that sort of concentrated control of future virtual geographies (and our presence in them) entail? It’s the sort of question writers like Tad Williams ask in books like the Otherland sequence, and it’s not too soon to start asking it of Facebook and Oculus VR today.

“You selling out to Facebook is a disgrace. It damages not only your reputation, but the whole of crowdfunding. I cannot put into words how betrayed I feel by this.” (Oculus VR Kickstarter page commenter)

I sympathize with those who backed Oculus VR on Kickstarter, and who feel this Facebook deal violates some sort of unspoken bond between Oculus VR and its supporters, but I think it says more about Kickstarter supporters than Facebook or Oculus VR. There’s a deep misunderstanding of what both Kickstarter (as a vehicle to pool money) and crowd-funding (as a vehicle to get clever ideas off the ground) mean.

Kickstarter is a way to allow anyone so inclined to finance a project directly, a middlemen-eliminator coupled to a public stage on which startups audition for contributions. Each Kickstarter is a tacit agreement between supporters and idea-makers to eventually deliver some product or service, but unless otherwise specified, it’s not a promise to not take money from other sources of support, or to not be acquired by a company like Facebook along the way.

Oculus VR founder Palmer Luckey says Facebook’s purchase changes nothing about Oculus VR’s Kickstarter deliverables, and there’s no reason for us not to take him at his word at this point. If Oculus VR starts rejiggering its commitments, then there’s reason to get worried or upset, but until then, it sounds like it’s still business as usual from the Kickstarter standpoint.

“…if Facebook can own the pipe, the platform or the operating system of the future, it will have much greater control over its destiny.” (Sterne Agee analysts, writing about the acquisition)

Analysts say the darndest things, most of it self-evident and/or devoid of insight. If Facebook can put its imprimatur on the Next Big Thing, of course it’ll have a firmer grip on its rudder. Who wouldn’t? The question is whether Oculus VR’s particular take on virtual reality is the Next Big Thing. No one’s satisfactorily answering that question right now, least of all “analysts” saying stuff like this.

“Facebook is making a long term bet on VR, not a short term run on profit.” (Oculus VR founder Palmer Luckey, answering questions on Reddit about the acquisition)

Oculus Rift is unproven technology that could still fall flat. There’s nothing etched in stone here, and the history of virtual reality is littered with brilliant-sounding-at-the-time shipwrecks. If Facebook wanted short term profits, it’d invest in something far less volatile. Short term profits are clearly not — and forget Facebook, because this applies to any company today — what investing in virtual reality interfaces circa 2014 means. Luckey is certifiably correct here.

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Matt Peckham at matt.peckham@time.com.