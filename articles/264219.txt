KIMIHIRO HOSHINO/AFP/Getty Images

Google on Wednesday publicly released data on the diversity of its workforce for the first time and admitted it was disappointed with its record for hiring women, African-Americans, and Hispanics.

Women comprise 30 percent of the Web giant's overall global workforce but make up only 17 percent of those employed in technology capacity, according to data published by the company Wednesday. When it came to leadership positions, men outnumbered women by nearly 4 to 1, according to the data.

The data release comes as tech companies are increasingly being pressured to add more females and people of color to their oftentimes all white male staffs. Apple has been under fire for having only one woman and no minorities on its board, and Twitter also faced criticism for having no female board members right before it went public late last year.

While more women have been hired at tech firms in the past few years, their roles are not usually at the executive level. A study by CNN Money last year showed that women in tech dominated the "Administrative" category (which combined clerical workers, as well as skilled and unskilled laborers) but were significantly less represented as officers or managers.

Finding comparison hiring data from other companies in the tech sector is difficult as hiring records are usually a closely guarded secret. Of the 20 companies probed by CNNMoney, 17 successfully petitioned for their data to be excluded from the report. The US government requires all major employers to file diversity statistics with the Equal Employment Opportunity Commission, but it's left up to employers whether to release the information to the public.

Asians made up 34 percent of Google's US tech workforce and held 23 percent of the company's leadership positions, but African-Americans and Hispanics did not fare so well, making up just 1 percent and 2 percent of the company's US tech workforce, respectively. Caucasians made up 60 percent of the company's US tech workforce and held 72 percent of its leadership positions.

About 7 percent of US employees in science, technology, engineering, and mathematics positions in 2011 were black or Latino, while 13.1 percent of the US population is black and 16.9 percent is Latino, according to the most recent US Census Bureau data available (PDF).

"We're not where we want to be when it comes to diversity," Google said in a statement that accompanied the data. "It is hard to address these kinds of challenges if you're not prepared to discuss them openly, and with the facts. All of our efforts, including going public with these numbers, are designed to help us recruit and develop the world's most talented and diverse people."

In response to a call for greater transparency by the Rev. Jesse Jackson, Google announced earlier this month that it would release the data, noting that it -- along with many companies in Silicon Valley -- had historically been reluctant to divulge the data.

The Anita Borg Institute, a nonprofit that promotes a more welcoming culture for women in tech, said it was pleased with Google's transparency and called it a step in the right direction.

"We recognize that the numbers are not particularly good," Elizabeth Ames, the institute's vice president of marketing and alliances, told CNET. "They are substantially lower than the industry benchmark. These companies should have between 20 percent and 24 percent of their technical workforce be women."

Ames said she expected the data release would help improve workforce diversity at Google and other companies, which she also hoped would follow Google's lead.

"We firmly believe that this is an important step to solving this problem in measurement and accountability," Ames said.