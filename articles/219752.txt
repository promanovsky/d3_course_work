Frankfurt: Deutsche Bank launched plans to raise €8 billion ($11 billion) in new capital on Sunday, with the Qatari royal family lined up as a major new investor, in a bid by Germany’s largest bank to end questions about its capital position.

The bank had already raised 10.2 billion euros in equity in 2010 and a further €3 billion in 2013, but that had not been enough to assuage investor concerns about its capital position as it faces increased regulatory demands.

The cash injection gives Deutsche the firepower to expand investment banking, especially in the US, after a retreat by competitors Barclays, UBS and others left a gap that Deutsche aims to fill.

“It was either an up or out moment. Either we had to affirm our strategy and reinforce it, or we had to consider the alternatives,” said a source close to the transaction. “To us the alternatives have never been attractive.” The new money also helps the bank build up its regulatory ratios as the European Central Bank runs the region’s top banks through rigorous checks before it becomes the Eurozone’s leading banking regulator in November.

But at the same time, it underscores how the bank fell short of its ambitious turnaround targets, and how burdensome fines and settlements and lagging profitability hampered management’s efforts to fortify capital by retaining earnings.

A stake worth 1.75 billion euros has already been placed with an investment vehicle owned and controlled by Shaikh Hamad Bin Jasem Bin Jaber Al Thani of Qatar, Deutsche Bank said in a statement. It plans to raise another €6.3 billion in a rights issue to existing shareholders.

The Qatari investor has not requested a seat on the board, nor were any special fees offered to the investor, a source close to the transaction said. “They’re an investor like anyone else,” he said.

Deutsche Bank said it would focus on an “accelerated growth programme” by hiring top bankers in the US, investing some 200 million euros over three years on technology improvements to its retail operations in Germany and Europe, and will hire up to 100 advisers to support its biggest corporate clients.

The bank also aims to expand its wealth management team in key emerging markets by 15 percent over three years.

The capital measures will increase Deutsche Bank’s Common Equity Tier 1 ratio, a measure of a bank’s ability to withstand stress, by approximately 230 basis points, from 9.5 per cent at the end of the first quarter of 2014 to 11.8 per cent. That is closer to the level already posted by several rivals.

The decision to raise capital came as a “pro-active decision by the management board,” said the source, not due to regulatory pressure.

“Deutsche Bank’s cap hike will dilute shareholders and make dividend payments per share shrink. But of course, investors have anticipated a cap hike at Deutsche Bank for a long time,” SEB fund manager Juergen Meyer.

“Comparing an investment in Deutsche Bank to investments in other large German companies — the likes of BASF — being an owner of Deutsche Bank shares is not a very pleasant experience,” he said.

The bank also weakened some of the reform targets it had set out in 2012 as part of a turnaround plan. A post-tax return on equity of 12 per cent will come in 2016, the bank said, one year later than previously promised.

Likewise, it now says a cost-income ratio of 65 per cent originally envisaged for 2015 will only come in 2016. The ratio was last measured at 77 percent at end-March.

Until now, Deutsche Bank had targeted a core tier 1 equity ratio of 10 per cent under the Basel III bank rules in their most stringent form as of March 2015. It had aimed at achieving that mainly by retaining earnings.

Tapping shareholders for cash represents a clear change in Deutsche’s plans after co-CEO Anshu Jain said in April that the bank “would not rule out any option” to strengthen its capital base.

As early as January, the bank said it had not discussed raising equity since raising €3 billion ($4.15 billion) from shareholders last year.

Deutsche Bank itself is the global coordinator and joint bookrunner on the deal. Other bookrunners include Barclays, Commerzbank, Banco Santander, Goldman Sachs, JPMorgan and UBS, sources close to the transaction said.