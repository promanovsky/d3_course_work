Major League Soccer Commissioner Don Garber has been diagnosed with prostate cancer, the league announced Saturday.

Garber, 56, who has led MLS for 15 years, has undergone a series of comprehensive tests and doctors say the cancer has not spread. Garber has begun treatment at Sloan Kettering Memorial Hospital in New York, which will be followed by surgery at Mt. Sinai Hospital.

Based on the stage of his cancer, his doctors expect he will have a full recovery. Garber will continue managing the league during treatment.

“Obviously no one wants to hear that they have cancer,” Garber said in statement released by MLS. “However, I am being treated by exceptional doctors at two of the top hospitals in the world and am confident, as are they, that the prostate cancer will be successfully treated, with a full recovery. “

Advertisement

Prostate cancer is the most common non-skin cancer in the U.S., affecting one in six men, according to the Prostate Cancer Foundation. Nearly a quarter-million American men will be diagnosed with the disease in 2014 and in more than 10% of those cases the cancer will prove fatal.

Garber, a former NFL executive who was senior vice president and managing director of NFL International, has become one of the most successful commissioners in American sports during his time with MLS. The league has grown from 12 to 19 teams since Garber took over, with franchises in Miami; Orlando, Fla.; and New York set to join the league soon. MLS has expanded into Canada and its average per-game attendance is larger than both the NHL and NBA.

Garber also has overseen the construction of 12 soccer-specific stadiums and the adoption of the designated-play rule, which has allowed teams to go beyond the league-mandated salary cap to lure big-name players such as David Beckham, Thierry Henry, Clint Dempsey and Michael Bradley to the U.S.