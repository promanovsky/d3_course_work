Kevin McCoy

USA TODAY

A former BP crisis manager was hit with insider-trading charges Thursday for using confidential information about the magnitude of the 2010 Deepwater Horizon oil spill disaster to unload his family's $1 million portfolio of company stock.

The improper trades enabled Keith Seilhan, who coordinated BP's oil collection and cleanup operations after the Gulf of Mexico catastrophe, to "avoid losses and reap unjust profits as the price of BP securities dropped by approximately 48%" after the improper sales, the Securities and Exchange Commission charged.

The explosion and fire disaster left 11 dead amid an uncontrolled spill that lasted 84 days and spewed an estimated 200 million gallons of oil into the Gulf and on surrounding coastlines.

"Corporate insiders must not misuse the material non-public information they receive while responding to unique or disastrous corporate events, even where they stand to suffer losses as a consequence of those events," said Daniel Hawke, chief of the SEC enforcement division's market abuse unit.

Seilhan, a former 20-year employee of BP, agreed to settle the case by returning $105,409 of alleged improper gains and paying a civil penalty for the same amount. The crisis manager, who will also pay $13,300 in prejudgment interest, neither admitted nor denied the allegations.

The settlement is subject to approval of a federal judge in Louisiana's eastern district.

"Four years have passed, and Mr. Seilhan wants to avoid further distraction and protracted litigation," said defense attorney Mary McNamara on Thursday. She added that Seilhan "is widely respected for his work helping to lead the cleanup and containment efforts in the Gulf of Mexico in 2010."

The case is the latest of dozens of similar civil or criminal proceedings brought by the SEC and federal prosecutors in a multiyear crackdown on suspected insider trading.

The Deepwater Horizon was an offshore drilling platform operated by a BP subsidiary in the Macondo Prospect area of the Gulf. The rig exploded on April 20, 2010, killing 11 of the 126 workers on the platform before sinking about 5,000 feet to the sea floor two days later.

The disaster uncoupled the oil riser pipe that connected the wellhead below the Gulf to the rig. That unleashed a massive oil spill that caused billions of dollars in damages, losses and environmental harm in the Gulf, as well as coastal areas of Alabama, Florida, Louisiana, Mississippi and Texas.

Immediately after the explosion, BP deployed Seilhan, now a 47-year-old resident of Tomball, Texas, to serve as an on-scene coordinator of containment and cleanup efforts.

BP reported publicly and told the SEC in late April 2010 that estimated oil-flow rates from the disaster totaled approximately 5,000 barrels of oil per day.

But Seilhan learned from his role as response coordinator that the damage was far worse, the SEC charged. Among other sources of information, he received an April 22 e-mail from another BP manager with non-public estimates that the flow rate ranged from 64,000 barrels per day to 110,000 barrels per day, the agency's court complaint alleged.

Seilhan also "had additional non-public information that the flow rate of oil was well in excess of the then-public estimates," the complaint charged.

Acting on the confidential data, Seilhan on April 29 allegedly sold 87,512 units of the BP Stock fund in his and relatives' retirement accounts, generating proceeds of $984,697. He sold three different BP employee stock-option grants in a brokerage account the following day, netting $47,561 from those transactions, the court complaint charged.

Five days after the brokerage transactions, a BP attorney sent Seilhan and other disaster workers an e-mail that cautioned the firm's code of conduct "prohibits you from trading on the basis of any price sensitive information relating to either BP securities or those of any other publicly traded company."

The message advised recipients to contact the attorney if they had any questions or uncertainties about prohibited transactions.

"Thanks for this. I would like to discuss with you soon," Seilhan allegedly e-mailed back.

He did not respond to subsequent email and voice-mail messages left by the attorney, and never disclosed his transactions to BP, the court complaint charged.