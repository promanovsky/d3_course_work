The risk of heart disease may be higher in men with HIV, a new study suggests.

The study, published in the journal Annals of Internal Medicine, showed that men with HIV have more soft plaque buildup in the arteries that bring blood to the heart, a condition called coronary atherosclerosis, than men without the virus -- a risk factor for heart attack.

Soft plaque buildup in the arteries can lead to heart attack in a different way than hard plaque buildup. With hard plaque, the plaque eventually builds up and blocks blood from flowing to the heart, thereby stifling the supply of oxygen to the heart, and potentially leading to a heart attack. Soft plaque, on the other hand, can cause part of the artery to rupture, leading to a blood clot. This blood clot can cause a heart attack by blocking blood flow.

For the study, researchers from Northwestern University, Johns Hopkins University, the University of California, Los Angeles, and the University of Pittsburgh examined the heart health of 618 HIV-infected men and 383 men without HIV who were part of the Multicenter AIDS Cohort Study. The men examined in the study were between ages 40 and 70 and had sex with other men.

Sixty-three percent of the HIV-infected men in the study had coronary atherosclerosis from soft plaque, compared with 53 percent of the men without HIV. The increased coronary atherosclerosis association remained true for the HIV-positive men even after researchers took into account other heart disease risk factors, such as smoking and high blood pressure.

Researchers also found that men with HIV who had taken anti-HIV drugs for the longest amount of time and whose immune health had worsened the most over the time they'd been infected with HIV were more likely to experience coronary artery blockages from soft plaque.