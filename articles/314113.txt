FOX has announced that Jennifer Lopez, Keith Urban and Broadway's own Harry Connick, Jr. will return as judges and Ryan Seacrest will host American Idolfor its 14th season.

"Jennifer, Keith, Harry and Ryan are the very best in the business at what they do, and I'm thrilled that they are returning to American Idol for another season. Each brings unique qualities and expertise to the team, but they all share the same passion for helping undiscovered singers achieve the American dream," said David Hill , Senior Executive VicePresident, 21st Century Fox.

Commented Trish Kinane, President, Entertainment, FremantleMedia North America and Executive Producer, AMERICAN IDOL, "I am delighted that our dream team of Jennifer, Keith and Harry are back for American IdolXIV. Their experience, humor and passion for new talent has made them one of IDOL's strongest-ever judging panels, and coupled with the expertise and charisma of Ryan Seacrest , we are ready to get out there to find the best talent in America."

"I am thrilled our consummate host, Mr. Ryan Seacrest , is returning for Season 14, together with Jennifer, Harry and Keith," said American Idol creator and executive producer Simon Fuller . "Their interaction, warmth and experience made them an absolute joy to watch. I can't wait to see their evolution together continue next season."

Click here to watch Harry Connick Jr.

perform medley of hits on IDOL

In addition to his music career, Harry Connick Jr. has also appeared on the screen and stage as an actor. Among his feature film credits are "Little Man Tate," "Copycat," "Hope Floats," "Memphis Belle," "Independence Day," "Life without Dick," "Mickey," "Basic," "P.S. I Love You," "New in Town" and "Dolphin Tale." His voiceover skills were featured in "My Dog Skip" and "The Iron Giant." Next fall, he will be seen in "Dolphin Tale 2," the sequel to 2011's No. 1 hit family film.

On television, Connick is perhaps best known for his recurring role on "Will & Grace." He also appeared in an arc of "Law & Order: Special Victims Unit," and has graced audiences with his musical talents in several Primetimespecials, including "Harry for the Holidays," the animated tale "The Happy Elf" and the Emmy Award-winning PBS Specials "Only You in Concert" and "Harry Connick, Jr. in Concert on Broadway."

His Broadway career boasts equal recognition, having received Tony Award nominations as both composer/lyricist for the musical "Thou Shalt Not" and as the lead in the Tony Award-winning revival of "The Pajama Game." He also has adapted "The Happy Elf" for children's theater, starred in the Broadway revival of "On a Clear Day You Can See Forever" and - on the 20th anniversary of his first Broadway concerts at the Lunt-Fontaine Theatre - brought his live show for an extended residency at the Neil Simon Theatre.