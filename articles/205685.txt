Lea Michele has paid tribute to her late boyfriend and Glee co-star Cory Monteith on what would have been the star's 32nd birthday.

Michele shared a black and white photo of the couple laughing onTwitter with the caption, "The biggest heart and most beautiful smile... In all of our hearts... We love you so. Happy birthday."

Monteith passed away last July from an accidental drug overdose.