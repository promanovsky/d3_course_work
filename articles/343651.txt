By Bahar Gholipour, Staff Writer

Published: 07/01/2014 10:21 AM EDT on LiveScience

Almost half of the U.S. soldiers who have recently returned from deployment have chronic pain, and 15 percent use opioid painkillers, a new study finds.

These new estimates of chronic pain and opioid use among soldiers are higher than those seen among the civilian population, the researchers said. About 26 percent of people in the general population report having chronic pain and 4 percent use opioids.

The researchers surveyed nearly 2,600 soldiers three months after they had returned from Afghanistan or Iraq who were not seeking medical treatment. About 45 percent had combat injuries and chronic pain that lasted at least three months, and 15 percent said they had used opioid painkillers in the past month, according to the study published today (June 30) in the journal JAMA Internal Medicine. [5 Surprising Facts About Pain]

Opioid medications are strong painkillers prescribed for chronic pain, but those who take them can become addicted, accidentally overdose or develop other health problems. Reports indicate increasing use of opioid medications among U.S. adults in general.

"Recently, rates of opioid use and misuse have ballooned, leading to significant numbers of overdose-related hospitalizations and deaths," the study researchers, lead by psychologist Robin Toblin of the Walter Reed Army Institute of Research, in Silver Spring, Maryland, wrote in the study.

Notably, 44 percent of soldiers who reported using opioids said they had no pain or only mild pain during the past month, the study found.

"This might imply that opioids are working to mitigate pain, but it is also possible that soldiers are receiving or using these medications unnecessarily," the researchers wrote. "This is cause for concern because opioids should be prescribed generally for moderate to severe pain and have high abuse and overdose potential."

Among the soldiers who reported having chronic pain, 48 percent said they had had pain for a year or longer, 56 percent reported having pain almost every day and 51 percent reported moderate to severe pain. Also, 23 percent of this group reported using opioids in the past month.

In a commentary on the new findings, Dr. Wayne Jonas of the Samueli Institute in Alexandria, Virginia, and Dr. Eric Schoomaker, of the Uniformed Services University of the Health Sciences in Bethesda, Maryland, said the study's findings show the worrisome impact of recent wars on the rates of pain and narcotic use among soldiers.

"The nation's defense rests on the comprehensive fitness of its service members — mind, body and spirit. Chronic pain and use of opioids carry the risk of functional impairment of America's fighting force," they wrote.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>