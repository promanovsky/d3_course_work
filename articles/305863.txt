Along with the announcement of its deal with Amazon for some 240,000 Android applications, BlackBerry has also said that it would be shutting down its music and video stores on BlackBerry World.

The company announced that it would shut down its music and video stores starting July 21. The previously downloaded content would stay accessible by users, but they won't be able to buy a new content.

The news came from BlackBerry's own blog post on Wednesday where John Chen, Chief Executive , BlackBerry mentioned that "While BlackBerry World will continue to offer music and video services via third party applications - again giving you more choices for your valued content - we will be closing the music and video sections of our store on July 21st. Don't worry! Previously downloaded content will be available after that date through MyWorld."

The blog post additionally mentioned that users can continue browsing and downloading the music and video content via Amazon Appstore, and through BlackBerry World via third-party offerings.

This decision by BlackBerry is expected to not make much impact to users, as they will have plenty of alternatives coming this fall.

BlackBerry would now be offering some 240,000 Android applications from Amazon's app store on its line-up of BlackBerry 10 devices. Users will be able to access popular apps like Groupon, Netflix, Pinterest, Candy Crush Saga and Minecraft. The apps will be available on BlackBerry 10 devices from this fall, when the company rolls out the BlackBerry 10.3 operating system.

"We're excited to be working with Amazon to deliver the apps and content that you've been looking for. It's an exciting new day for BlackBerry 10 device owners. We look forward to hearing from you about what apps you are looking forward to downloading," Chen added.