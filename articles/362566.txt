Warning: This article contains graphic language.

Anthony Cumia, of Opie & Anthony radio fame, launched a misogynistic, racially charged rant after he was allegedly roughed up by a woman in Times Square.

The 53-year-old shock jock said he was repeatedly punched by the woman early Tuesday while innocently snapping pictures and she accidentally got into a frame.

“So I’m taking pix in NYC & a black girl who was in frame punched me in the face. I called her a f–king “&$;;-:” cause that’s what she WAS!” the always articulate radio personality tweeted on Wednesday.

Then she punched me 5 more times. She’s lucky I was a white legal gun owner or she’d be dead. Then 5 blacks started giving me shit! — Anthony Cumia (@AnthonyCumia) July 2, 2014

Anthony has been on a non-stop Twitter rant ever since, claiming white people are easy targets of African-Americans.

Among his more incendiary tweets:

It’s really open season on white people in this day and age. No recourse. Fight back and you’re a racist. The predators know this. Good luck — Anthony Cumia (@AnthonyCumia) July 2, 2014

The switch to violence is immediate. No discussion, just violence. When will THAT be addressed? Oh, right, never. Slavery did it? Oh, ok. — Anthony Cumia (@AnthonyCumia) July 2, 2014

The NYPD had no assault reports from anyone named Anthony or Tony Cumia this week, law enforcement sources said.

Reps for SiriusXM, which carries the “Opie & Anthony” show, could not be immediately reached for comment on Thursday.

Cumia posted several pictures of the woman he claims roughed him up.

“Here’s a blurry shot of her taking another swing. See her claw in the lower left? It’s bound for my head,” Cumia wrote.

“Here she is all up in my face. Mouth flippin’ and yappin’. She had already sucker punched me so I snapped a pic.”

Cumia also said the woman was the first to hurl a racial slur.

“The 1st words out of her dumb a-s mouth after punching me in the head were `white motherf–ker’ but I’m the racist here? Bizarro land!” he ranted.

“I was assaulted and used `mean’ words on twitter and I’M the bad guy in all this? Lol! This fucking world is completely ass backwards!”

Cumia demanded he be viewed as the sympathetic figure here and insisted he’s no bigot.

“My posts on Twitter after I was assaulted were profanity laced indeed but racist? Nope. Why should I afford my attacker any courtesy. Insane,” he tweeted.