The Federal Open Market Committee (FOMC) has announced that it will taper its asset purchases by another $10 billion as most people expected, reducing agency mortgage-backed securities and long-term Treasury bond purchases by $5 billion each.

Fed thinks economic activity can support higher employment

“Economic activity has rebounded in recent months. Labor market indicators generally showed further improvement. The unemployment rate, though lower, remains elevated. Household spending appears to be rising moderately and business fixed investment resumed its advance, while the recovery in the housing sector remained slow,” said the FOMC statement.

While unemployment is still too high, the FOMC said that it was convinced there is enough economic activity to support an improving job market. While inflation is still below the Fed’s target (the other half of its dual mandate), that’s clearly less of a concern in today’s announcement.

“The Fed’s assessment of the economy was surprising realistic, noting the uneven growth across sectors and the lingering weakness in several key areas,” writes Sterne Agee chief economist Lindsey M. Piegza. “Despite the lowered expectations for growth, 12 of 16 Fed officials see the first rate hike in 2015, raising the median forecast for the fed funds rate from 1% to 1.13% for 2015. Given the muted outlook for the economy, this would potentially pose the worst combination for the market – a slower rate of growth and a more aggressive Fed.”

The Fed also pointed out that the end of QE doesn’t mean the end of monetary accommodation, hinting that rates might stay low for longer than the most recent tally suggests if the economy doesn’t start to pick up.

Long-term policy goals in line with Taylor Rule, not the New Neutral

The FOMC also released detailed information on the pace of policy tightening that falls in line with expectations for next year, but seems to contradict the New Neutral thesis of a long-term 2% fed rates target. Of the sixteen people voting, fifteen said they expect to maintain the current low policy rate through the end of this year versus one who thinks we should jump to 1%. Asked to look forward to year-end 2015, the consensus spreads out with people voting for everything from 0.25% all the way up to 3%.

In the long run, there is a consensus clustered around 3.75%, more or less where the Taylor Rule says it should be. But according to PIMCO chief economist Paul McCulley the target should probably be closer to 2%. It’s not surprising that the FOMC hasn’t radically altered its stance after a few suggestions (even from someone as highly regarded as McCulley), but if the Fed starts to reconsider its role in the post-crisis economy, the consensus around a long-term rate target is one of the first places you’d expect to see it show up.