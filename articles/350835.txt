It is the news that will have parents up in arms and music fans burying their heads in their hands. Lily Allen is set to join Miley Cyrus on the next leg of her Bangerz tour.

The twerking, naked-swinging, masturbation-simulating, drug-promoting Cyrus invited the “Not Fair” singer to play seven dates in August. These will be Allen’s first US gigs in five years, bar a New York show in May.

The 29-year-old has been a dedicated supporter of “fearless b**ch” Cyrus despite the headlines she relentlessly generates with her controversial antics.

“I don’t think someone sat in a room with Miley Cyrus and said, ‘Right, you’re going to be sexy, stick your tongue out and wear bikinis’,” Allen said in February.

“She wants to do that, she’s got a banging body. She’s worked out in the gym all the time, she wants to show it off, that’s great.”

Allen’s recent single “Hard Out Here” saw her attack the treatment of women in the music industry and mock Robin Thicke’s “Blurred Lines”.

Miley Cyrus Bangerz tour in pictures Show all 12 1 /12 Miley Cyrus Bangerz tour in pictures Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley wore white boots with dollar signs on as she cavorted around on a gold car Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour In her usual fashion, Miley spanked two dancers who joined her in red spandex Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour The 21-year-old was joined on-stage by colourful animals Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley gives her fans the finger which is, you know, nice of her Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley gives an energetic performance backed up by cowboy-clad dancers Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley wore a leotard with a cannabis leaf print in a nod to her love of smoking weed Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Cyrus twerked, and twerked, and twerked some more throughout the first Bangerz show Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Cyrus singing on top of a car in a spangly cannbis print leotard Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley whipped her hair back and forth during her first Bangerz tour show Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley performed on-stage with a dwarf wearing a Britney Spears mask Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley Cyrus kicks off her 'Bangerz' tour in anticipated controversial form in Vancouver, US Miley Cyrus Bangerz tour in pictures Miley Cyrus on tour Miley wore this sparkly red costume complete with Chanel bag and feathers

“If I want to be sexy and take my clothes off, I’ll do it of my own accord,” she said.

Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

“It’s when somebody tells you to do it that I find it offensive. I just don’t want someone saying to me, ‘You’ve got to lose 50 pounds or take your clothes off to sell records’, because it’s not true.”

Allen’s set at Glastonbury Festival last weekend saw her dance around giant milk bottles in a revealing pink jumpsuit. “Is my camel toe, like, really prevalent?” she asked the crowd at one point, before dedicating “F*** You” to “annoyingly corrupt” Fifa President Sepp Blatter.

Following her time with Cyrus, Allen’s own tour in support of her third album Sheezus will commence in Miami in September.