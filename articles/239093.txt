Are you impressed with what Microsoft has done with their new 12” Surface Pro 3 tablet that intends to replace the laptop, with some even calling this the laplet instead? Well, for those who have not yet managed to place a pre-order, here is some good news that might just entice you to make the jump. Apparently, Best Buy has opened up pre-orders for the Microsoft Surface Pro 3, and to sweeten the deal, they will offer the blue version of the Type Cover on an exclusive basis, now how about that?

Advertising

You will be able to choose from all five models of the Surface Pro 3 from the Best Buy website, including the models that run on the Intel Core i5 processor. That model will ship this coming June 20th, while those who have decided for any of the others will have to wait until August 31st for shipping to commence, with a starting price point of $799 a pop. As for the Type Cover, it will be priced at $129.99, and remember, apart from the blue version, there are also purple, cyan and black Type Covers to choose from. If you have caught sight of the red version, that is an exclusive on Microsoft’s Store website.

Filed in . Read more about Best Buy, Microsoft and Surface Pro 3.