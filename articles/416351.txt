Seth Meyers walks off the stage and immediately does the ALS Ice Bucket Challenge after hosting the 2014 Emmy Awards held at the Nokia Theatre L.A. Live on Monday (August 25) in Los Angeles.

The 40-year-old actor’s staff posted the video on his Late Night show’s Instagram account with the caption, “You didn’t think we’d end the #Emmys without Seth doing the #alsicebucketchallenge, did you?”

In the video, Seth thanks Jessica Biel for nominating him and then forwards a nomination to Vladimir Putin!

Check out the video below:



Seth Meyers – Ice Bucket Challenge