$30bn overseas cash is for buyouts, Google tells SEC

Share this article: Share Tweet Share Share Share Email Share

Brian Womack San Francisco Google was keeping a large portion of its cash outside the US so it could use as much as $30 billion (R312bn) for possible acquisitions, the company said in a letter to regulators. The owner of the top internet search engine generates about half of its revenue overseas, and it avoids paying US taxes by keeping foreign earnings abroad. Google needs the cash for deals as competition rises overseas, according to a letter to the US Securities and Exchange Commission (SEC), dated December 20 last year and filed on Tuesday as part of correspondence with the agency regarding disclosures. “We continue to expect substantial use of our offshore earnings for acquisitions as our global business has expanded into other product offerings like mobile devices,” Google said in the letter. “It is reasonable to forecast that Google needs $20bn to $30bn of foreign earnings to fund potential acquisitions of foreign targets and foreign technology rights from US targets in 2013 and beyond.”

Google, while facing criticism for keeping the cash parked overseas, has been stepping up its deal activity, including the $3.2bn purchase this year of digital-thermostat maker Nest Labs.

It has also made several smaller purchases, benefiting its advertising, cloud services and mobile businesses.

“In the past few years we have completed significant acquisitions with the individual deal size increasing in more recent years, and this trend is likely to continue in future years,” the company said in the letter, which was a response to a question from the SEC about Google’s plans for reinvesting foreign earnings.

The largest US-based firms added $206bn to their stockpiles of offshore profits last year, according to securities filings from 307 corporations.

At the end of last year, $33.6bn of Google’s $58.7bn of cash, cash equivalents and marketable securities was held by its foreign subsidiaries.

“If these funds are needed for our operations in the US, we would be required to accrue and pay US taxes to repatriate these funds,” the company said in a filing in February. “However, our intent is to permanently reinvest these funds outside of the US and our current plans do not demonstrate a need to repatriate them to fund our US operations.”

Richard Windsor, an analyst at Radio Free Mobile, said: “Google’s overriding strategy is to categorise and gather internet data, which it sells to advertisers. Any asset that has an incremental effect on that strategy is a viable target.”

Game developers such as Ubisoft Entertainment and content companies similar to Twitch Interactive could be on the shopping list, he said.

Google said in the letter that it saw other uses for overseas earnings, such as $2bn to $4bn for capital expenditure on data centres and other operations, and $12bn to $14bn for a research and development cost-sharing agreement.

The filing was part of an exchange that began last year related to SEC questions about disclosures in the company’s 2012 annual report. The SEC pressed for more information on topics including advertising, mobile and revenue.

It eventually said it would reveal more details on advertising. – Bloomberg