Aaron Paul's Xbox One advert is turning on consoles, it has been revealed.

The Breaking Bad star's "Xbox On" voice command is being picked up by consoles on standby.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

The issue with Kinect's voice activation system was reported by a number of users on Twitter.

Microsoft is yet to comment on the problem, but insisted that previous ads featuring the phrase had never caused an issue.

The firm is now selling Xbox One consoles without Kinect.

Xbox One developer kits have received an increase in GPU bandwidth as a result of Kinect's removal.

Developers react to Kinect no longer being mandatory for Xbox One

Watch our Xbox One video review below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io