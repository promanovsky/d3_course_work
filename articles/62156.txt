Facebook working on drones for Internet.org mission

Earlier this month, word surfaced that Facebook would be acquiring Titan Aerospace, maker of drones, to help bring Internet access to under-connected areas around the world. Today, Mark Zuckerberg announced that Facebook’s Connectivity Labs is working on building drones, among other things, to accomplish that goal.

The ultimate goal, says Zuckerberg, is to bring a basic form of Internet access to every person, regardless of where they live. This work has been ongoing, and under Internet.org, the social network has helped bring access to many people in the Philippines and Paraguay, among other areas.

Not many details on what exactly Facebook is up to were revealed, but said Zuckerberg, the company is working with experts from NASA’s Jet Propulsion Lab and Ames Research Center, among other areas. As of today, he says, Facebook is also pulling in members from Ascenta, a small company founded by those behind Zephyr, an award-winning solar aircraft.

The technology used will vary based on the area being serviced. According to a post on Internet.org, suburban areas in limited geographical regions will get solar-powered high-altitude aircraft that can run for months, while lower density regions will get low-Earth orbit and geosynchronous satellites.

SOURCE: Mark Zuckerberg and Internet.org