How to defend yourself against the 'two-week' attack By Dave Lee

Technology reporter, BBC News Published 3 June 2014

image copyright Thinkstock image caption Various steps can be taken to ensure you are safe online

Alarming news from the UK's National Crime Agency (NCA): you have "two weeks" to protect yourself from a major cyber-threat.

The warning came as the FBI, in partnership with authorities in several countries around the world, shut down a network of criminally operated computers that were stealing important information from victims' machines.

But since that announcement, which directed concerned users to a website which promptly crashed for more than 15 hours, many BBC readers have been in touch wondering what they need to do to stay safe on the internet. Here's an at-a-glance guide.

Am I affected?

media caption Rory Cellan-Jones reports on a "powerful computer attack", which people have two weeks to protect themselves from

If your computer does not run Windows, stop right here. This does not affect you - but other problems might, so always keep your antivirus up to date.

If you are using Windows, read on.

Gameover Zeus is a particularly nasty piece of malware - malicious software - that will fish around your computer for files that look like they may contain financial or other sensitive information. Once it finds them, it steals them.

The FBI has said that the criminals in this case used "phishing" emails to install Gameover Zeus on victims' computers. A phishing email is one that looks like it came from somewhere official, like your bank, but didn't - instead directing you to mistakenly download the malware.

The NCA has estimated that around 15,000 computers may currently be infected in the UK. Worldwide, it runs into the millions.

Those in the UK will be receiving correspondence from their internet service provider (ISP) soon, warning them that they are at risk. If you get one of these notices, you must act immediately.

But while the 15,000 figure is relatively low, this warning should not be ignored. Everyone should run a scan on their system.

GetSafeOnline.org - a government-backed initiative - published a list of downloads it recommends to run a sweep of your system and get into shape. Unfortunately, overwhelming traffic is causing the site to falter, and so people are also being directed to the UK Cyber Emergency Readiness Team (Cert) instead.

What's going to happen to me in two weeks?

image copyright FBI image caption Evgeniy Bogachev is being sought by the FBI in connection with the criminal botnet

Nothing.

The operation carried out by the FBI was able to knock out many of the servers used by criminals to control this particular threat.

But nobody involved has been arrested, and therefore it is extremely likely that very soon the operation will start up again.

It is estimated it will take around two weeks for the botnet - that's the network of criminally-controlled, hijacked computers - to be fully operational again.

That's why the security experts are advising people to use this relatively quiet two-week period to make sure they're up to date.

In truth, the advice given should be applied at all times. The message is always the same - make sure your antivirus software, and firewall, and everything else designed to protect you is up to date.

I had to change my passwords last week. And the week before that. When is this going to end?

image copyright Reuters image caption "This is the new normal," warned the FBI

To quote an FBI spokesman: "This is the new normal."

One of the big talking points from this latest security threat is the idea of "notification fatigue". Barely a week seems to go by without us being told about a cyber-attack putting our personal data at risk.

This is not going to go away - but there is a risk that the security industry may sound like it is beginning to "cry wolf" about cyber-threats.

But we may begin to see the way we deal with major cyber-risks changing.

In the case of Gameover Zeus, this is the first time security firms have worked directly with ISPs to target particular users it knows are infected.

In the future, it may mean that rather than millions of people being told to change passwords as a precaution, a much smaller number will be notified that they are immediately at risk.

Advice in the meantime is to use different, complex passwords for all the important sites you use.

If this seems like a bit of a faff, one easy way, experts suggest, is to use a password manager. We can't endorse products here, but a quick look on your favourite search engine will point you in the right direction.

Follow Dave Lee on Twitter @DaveLeeBBC