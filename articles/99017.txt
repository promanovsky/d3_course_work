Paul Walker fans can expect to see a moving tribute to the late actor when they tune into the MTV Movie Awards 2014 on Sunday (13 April).

The star of the Fast & Furious franchise, who was killed in November 2013 aged just 40 after the Porsche Carrera GT he was travelling in crashed into a lamppost, will be remembered at the annual ceremony.

“Paul Walker was a role model for the MTV audience since the very beginning of his too-short career,” MTV president Stephen Friedman said.

“Our audience grew up with him, from Varsity Blues to the Furious franchise. He was a movie star, but one they could relate to. Paul's humanity and kindness clearly showed through every role.”

“We're honoured to be able to spend a moment in this year's Movie Awards to celebrate him,” he added.

Paul Walker: a career in pictures Show all 20 1 /20 Paul Walker: a career in pictures Paul Walker: a career in pictures Paul Walker as Jeremy in the American television sitcom 'Throb' Rex Features REX FEATURES Paul Walker: a career in pictures Paul Walker in 'Meet The Deedles' in 1998 Rex Features REX FEATURES Paul Walker: a career in pictures Walker in Pleasantville with Reese Witherspoon Rex Features REX FEATURES Paul Walker: a career in pictures Walker in 'She's All That', with Freddie Prinze Jr Rex Features Rex Features Paul Walker: a career in pictures Walker, second left - In a publicity shot for Varsity Blues, 1999 Rex Features REX FEATURES Paul Walker: a career in pictures With James Van Der Beek in Varsity Blues Rex Features REX FEATURES Paul Walker: a career in pictures Paul Walker in 'The Skulls', 2000 Rex Features REX FEATURES Paul Walker: a career in pictures Walker in 'Joy Ride' (Roadkill) with Leelee Sobieski and Steve Zahn Rex Features REX FEATURES Paul Walker: a career in pictures In the film that made his name, The Fast And The Furious with Vin Diesel Rex Features REX FEATURES Paul Walker: a career in pictures In 'Timeline' with Frances O'Connor, 2003 Rex Features REX FEATURES Paul Walker: a career in pictures A publicity shot for '2 Fast 2 Furious', in 2003 Rex Features REX FEATURES Paul Walker: a career in pictures With Jessica Alba in 'Into The Blue', 2005. Rex Features REX FEATURES Paul Walker: a career in pictures Paul Walker in Eight Below, 2006 Rex Features REX FEATURES Paul Walker: a career in pictures Walker in 'Running Scared', 2006 Rex Features REX FEATURES Paul Walker: a career in pictures The Death And Life Of Bobby Z', Paul Walker 2007 Rex Features REX FEATURES Paul Walker: a career in pictures In 'The Lazarus Project' with Brooklynn Proulx Rex Features REX FEATURES Paul Walker: a career in pictures In 'Takers' from 2010 Rex Features REX FEATURES Paul Walker: a career in pictures Walker in Fast & Furious 5 with Vin Diesel Rex Features REX FEATURES Paul Walker: a career in pictures Walker in Fast & Furious 6 released this year Rex Features REX FEATURES Paul Walker: a career in pictures Poster art for 'Vehicle 19' starring Paul Walker, released this year Rex Features REX FEATURES

Speaking to the AP, the MTV Movie Awards executive producer Garrett English added: ”Well, I think we are just really happy and honoured that they were willing to allow us to celebrate him in some way on the show. And his family and friends were willing to have that happen.

“He's been such a big part of the channel, what the channel's audience has been about. He's been such a big part of our show in the past that it felt really right to us, and we're just really happy and honoured that they were willing to allow us to do something for him on the show.”

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Walker took to the stage at the MTV Movie Awards 2013, where he presented an award alongside his co-stars Jordana Brewster, Vin Diesel and Michelle Rodriguez.

The host of the 2014 event, held at the Nokia Theater in Los Angeles on Sunday evening, will be Conan O'Brien.

The tribute comes just weeks after experts at the Los Angeles County Sheriff's Department ruled out speculation that the accident had been caused by a mechanical failure, saying instead it had been caused by speed and speed alone.