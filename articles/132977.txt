Why you might not want a $50 Google Project Ara

Over the past several days it’s become apparent that Google’s next big release – Project Ara – is one aimed for a $50 price point. The idea that you’d be able to buy a $50 smartphone is pretty exciting – but it’s not that simple. In fact, not only is the $50 price point not set for consumers, but the device that’ll come with a point anywhere near that amount of cash will be so extremely basic, you’ll consider it a Feature Phone.

The release of Project Ara – or Google Ara, as it’ll likely eventually be called – will be marked by a single “Gray” unit. This unit will be the most basic of builds, made to be the skeleton onto which Ara components will be able to be added.

Based on what we know now, buying this Gray unit will be akin to purchasing a customizable computer case with a touchscreen display and just as much hardware as is required to turn it on and connect to wi-fi.

The object of this first release will be to introduce the world to the idea that they’ll be able to buy a phone whose hardware can be upgraded. Not unlike a PC tower, not unlike a hotrod.

The $50 price point quoted throughout the blogosphere this week comes from a mention of an (approximate) cost of parts and labor for the device. Google may decide to release the device for $50, or they may decide to release it for more – or maybe even less. It all depends on how Google wants to market the machine.