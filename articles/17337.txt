On Monday, a team of scientists led by a Harvard astronomer announced they had discovered a pattern that further suggests our universe came into existence through hyper-expansion — the rapid growth of the early universe, a phenomenon otherwise known as cosmic inflation. Their findings give further credibility to decades old Big Bang theory, which suggests our universe was created through a massive explosion 13 billion years ago.

But what do their so-called Big Bang breakthrough findings really mean? Let’s break it down:

Cosmic inflation comprises a large part of the Big Bang theory

Cosmic inflation is the rapid expansion of the universe, which comprises a large part of the Big Bang theory. According to scientists, inflation occurred fast, but the universe continued to grow slowly afterwards.

Advertisement

Astronomers looked for gravitational waves

For the first time, astronomers announced they directly observed gravitational wave patterns, which are ripples through space that are generated when any mass passes through. Think of it like dropping a rock into a pond. This is a big deal because for decades scientists theorized that gravitational waves were generated through black holes that collided after the cosmic inflation.

“We’re very excited to present our results because they seem to match the prediction of the theory so closely,’’ team leader John Kovac of the Harvard-Smithsonian Center for Astrophysics told the Washington Post.

They found the waves through a high powered telescope

The astronomers used a telescope called BICEP2, which is based at the South Pole, to detect the waves. The telescope is positioned to have a very clear view of the cosmos and has the power to detect polarizing radiation signals that can occur from gravitational waves, according to Discovery News. The team was specifically looking for a type of polarizing signal known as B-mode polarization, and they announced on Monday that they found it.

“The swirly B-mode pattern of polarization is a unique signature of gravitational waves,’’ Chao-Lin Kuo of Stanford University and the SLAC National Accelerator Laboratory, who is also co-leader of the project, told Discovery News. “This is the first direct image of gravitational waves across the primordial sky.’’

Advertisement

Cosmic inflation theory has a Massachusetts link

MIT professor of physics Alan Guth was one of the first to propose the idea of cosmic inflation 1980, according to Standford University news service, but his theory has not been proven since scientists could not find a direct pattern suggesting hyper-expansion of the early universe. These latest findings haven’t proven his theory, but may have brought it one step closer to fact.

The astronomers are fairly certain of their findings

For the last three years, the astronomers have tried and tossed previous theories on how the universe came into existence. In their paper published online today, they reported a confidence level greater than 5 sigma — that translates to the odds of 1 in 3.5 million that their findings are due to chance, the New Scientist reported.