It is obvious after last week's budget that Australia is no longer part of the world's weather patterns. Prime Minister Tony Abbott and Treasurer Joe Hockey have, in effect, excised the continent from the global climate, and lowered a dome of denial to prevent any penetration from outside forces of nature.

This is no mean feat, but, given the Coalition's attitude towards climate change, not surprising. The budget starkly illustrates the meeting of philosophy with politics, delivered from a position of scepticism. It is an ignoble legacy on which to be judged.

Per capita, Australia is one of the worst polluters of carbon dioxide in the world. So what does the government do? It cuts funding for renewable energy and research. This is a retrograde step and a degradation of vision.

Budget documents reveal that funds for climate-change-related programs will be savagely cut from $5.75 billion in the present fiscal year to $1.25 billion by next fiscal year and halved again by 2017-18.

The Coalition went to the election pledging the removal of the carbon price and replacing it with the establishment of the Emissions Reduction Fund, from which polluters would be paid to cut emissions. However, over the next four years only $1.14 billion has been committed, half the amount Environment Minister Greg Hunt said a month ago would be allocated.