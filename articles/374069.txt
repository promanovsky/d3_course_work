It's been a disappointing day for Garth Brooks' fans. However, here's some of the more humorous reactions to the news that all five of his Croke Park shows have been cancelled.

I wanted to do five Garth Brooks-related tweets - or none at all. #GarthBrooks— Liam Maloney (@omaoldhomhnaigh) July 8, 2014

Kerry 1982... Kilkenny 2010...Garth Brooks 2014 all denied 5 in a row in Croke Park!— Dáithí Ó Sé (@daithi_ose) July 8, 2014

I hope there are five replays in the @officialgaa hurling championship this year #garthbrookscrokepark— SOH (@OHalloran1984) July 8, 2014

Well, they always say 'subject to licence' on the ads... http://t.co/THGBHS6BkP Feel sorry for the fans though #garthbrookscrokepark— Miriam Burke (@The_MiriamBurke) July 3, 2014