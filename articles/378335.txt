When it comes to software like Windows or Office, it is safe to say that Microsoft probably holds a majority market share. In fact last we checked, Microsoft’s share of the PC market was still around the 90% mark, but in terms of actual software released across all sorts of devices, Microsoft admits that they could do better.

Advertising

Speaking at the Worldwide Partner Conference in Washington, D.C., Microsoft’s COO, Kevin Turner, revealed that Microsoft’s market share in terms of devices currently sits at a lowly 14%, nowhere near their dominance when it comes to the PC market. “The reality is the world’s shifted, the world’s evolved. We now measure ourselves by total device space. We have a much bigger opportunity than we’ve ever had in the past to grow our business, but we have to rethink how we look at our business.”

He then stated that this was a positive thing as it allows Microsoft to challenge themselves and be more open about releasing software onto non-Windows based devices, like for the iPad and even Android. Earlier this year we saw Microsoft release a touch version of Office for the iPad, and word has it that Microsoft is working on a version of Office for Android tablets that could see a release ahead of the Windows version.

Naturally Microsoft’s focus would still be on Windows but at the same time it is good to know that Microsoft will not be playing exclusive with their software, so here’s hoping that Microsoft’s device market share will see an increase in the future.

Filed in . Read more about Microsoft.