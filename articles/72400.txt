JK. Rowling will make her screenwriting debut with Fantastic Beasts And Where To Find Them[WENN]

FREE now for the biggest moments from morning TV SUBSCRIBE Invalid email Sign up fornow for the biggest moments from morning TV When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The 48-year-old is said to have agreed to a big-screen adaptation of her 2001 fictitious textbook Fantastic Beasts And Where To Find Them used by students at the exclusive Hogwarts School Of Witchcraft and Wizardry. The writer announced last year that she was set to make her screenwriting debut with the book, which was described as "neither a prequel nor a sequel to the Harry Potter series, but an extension of the wizarding world".

The author penned the Harry Potter franchise, which went on to win numerous awards [WENN]

Now Warner Bros. boss Kevin Tsujihara reveals fans can look forward to three instalments of the project, which follows the adventures of Newt Scamander, a magical creatures expert known as a "magizoologist". The film will be set in New York - seventy years before the Harry Potter series began. "We had one dinner, a follow-up telephone call, and then I got out the rough draft that I’d thought was going to be an interesting bit of memorabilia for my kids and started rewriting!,' revealed the author.

Harry Potter raked in £4.8 billion worldwide, making it the highest-grossing film series of all time [UMI]