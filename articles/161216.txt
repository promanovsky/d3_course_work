George Clooney stormed out of a dinner with Las Vegas casino mogul Steve Wynn, claiming the billionaire had insulted US President Barack Obama.

As differing versions emerged of what happened in a restaurant at Mr Wynn's Encore Hotel, the tycoon responded that the actor had been "drunk" on tequila and had sworn at him.

In a statement, Clooney said: "There were nine people at that table, so you can ask them. Steve likes to go on rants. He called the president an a**hole. That is a fact.

"I said the president was my longtime friend, and then he said, 'Your friend is an a**hole.' At that point I told Steve that HE was an a**hole and I wasn't going to sit at his table while he was being such a jackass. And I walked out.

"There were obviously quite a few more adjectives and adverbs used by both of us. It had nothing to do with politics and everything to do with character."

Mr Wynn remembered the argument differently, telling the 'Las Vegas Review-Journal' that it began when a Hollywood executive at the table told a joke about Mikhail Gorbachev, which led to Clooney "throwing a hissy fit".

He said Clooney began talking about Mr Obama's health reforms, and didn't like it when he voiced opposition. "When he's drinking, he considers himself a close personal buddy of the president," he said. "The only person who got excited at the table was George." (© Daily Telegraph, London)

Irish Independent