Game of Thrones author George R.R. Martin has suggested that feature films may be required to conclude the HBO series, and that prequel series Tales of Dunk and Egg is also a big screen possibility.

The show returns for season 4 next month, but has barely even begun telling Martin's expansive saga.

"It all depends on how long the main series runs," he The Hollywood Reporter at the new season's New York premiere.

"Do we run for seven years? Do we run for eight? Do we run for 10? The books get bigger and bigger (in scope). It might need a feature to tie things up, something with a feature budget, like $100 million for two hours. Those dragons get real big, you know."

Movie spin-offs are a popular, if divisive way of concluding a series, with fellow HBO show Sex and the City previously heading to cinemas after finishing its TV run.

Martin said his prequel novellas Tales of Dunk and Egg are also being considered for a film adaptation, which take place in Westeros 90 years before the events of A Song of Ice and Fire and revolve around a group of characters including Ser Duncan the Tall.

Independent News Service