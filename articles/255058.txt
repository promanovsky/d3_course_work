The May Advance Report on April Durable Goods was released this morning by the Census Bureau. Here is the Bureau's summary on new orders:

New orders for manufactured durable goods in April increased $1.9 billion or 0.8 percent to $239.9 billion, the U.S. Census Bureau announced today. This increase, up three consecutive months, followed a 3.6 percent March increase. Excluding transportation, new orders increased 0.1 percent. Excluding defense, new orders decreased 0.8 percent.



Transportation equipment, also up three consecutive months, led the increase, $1.7 billion or 2.3 percent to $76.9 billion. Download full PDF

The latest new orders number came in at 0.8 percent month-over-month, which substantially beat the Investing.com forecast of -0.5 percent. Year-over-year new orders are up 7.1 percent.

If we exclude transportation, "core" durable goods came in at 0.1 percent MoM, below the Investing.com forecast of 0.3 percent. The YoY core number was up 4.8 percent.

If we exclude both transportation and defense, durable goods were down a disappointing 2.4 percent MoM and up only 1.6 percent YoY.

The Core Capital Goods New Orders number (nondefense captial goods used in the production of goods or services, excluding aircraft) was also negative -- down 1.2 percent MoM. The YoY number was up 3.9 percent.

The first chart is an overlay of durable goods new orders and the S&P 500. We see an obvious correlation between the two, especially over the past decade, with the market, not surprisingly, as the more volatile of the two. Over the past year, the market has certainly pulled away from the durable goods reality, something we also saw in the late 1990s.

Durable Goods/S&P 500 Monthly Averages of Daily Closes





An overlay with unemployment (inverted) also shows some correlation. We saw unemployment begin to deteriorate prior to the peak in durable goods orders that closely coincided with the onset of the Great Recession, but the unemployment recovery tended to lag the advance durable goods orders.

Consumer Durable Goods New Orders and Unemployment





Here is an overlay with GDP — another comparison I like to watch.

Consumer Durable Goods New Orders and Real GDP





The next chart shows the percent change in Core Durable Goods (which excludes transportation) overlaid on the headline number since the turn of the century. This overlay helps us see substantial volatility of the transportation component.

Core Durable Goods New Orders Percentage Change Since 2000





Here is a similar overlay, this time excluding Defense as well as Transportation (an even more "core" number).

Durable Goods New Orders Percent Change Since 2000

This last chart is an overlay of Core Capital Goods on the larger series. This takes a step back in the durable goods process to show Manufacturers' New Orders for Nondefense Capital Goods Excluding Aircraft.

Core Capital Goods New Orders Percent Change Since 2000





In theory the durable goods orders series should be one of the more important indicators of the economy's health. However, its volatility and susceptibility to major revisions suggest caution in taking the data for any particular month too seriously.