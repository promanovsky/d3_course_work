LOS ANGELES — The Electronic Entertainment Expo tradeshow is all about games, but that doesn’t mean we can’t drool over hardware.

While we didn’t get a new console or handheld announcement this year, we did get a white PlayStation 4 — and it’s looking hot. The new device will go on sale on Sept. 9 bundled along with the upcoming Bungie shooter Destiny, and Sony is showing off the shimmering device in a glass case on the E3 show floor. The system grabbed my eye as I walked by — and the controller is even better.

Check it out:

Image Credit: Jeff Grubb/GamesBeat

Look, I love black consumer electronics as much as the next guy with eight things sitting next to his television, but it’s time we got some color in the entertainment center. The white PS4 will do that. But the thing that we handle the most is the controller, and the new DS4 will bring a tiny bit of sleek-looking brightness to my living space.

Here’s what I like about it. It’s white, but it doesn’t use the Fisher-Price paint that Nintendo used on the 8GB white Wii U. Instead, we get a matte look that looks much more designer-crafted and premium. Also, it’s mostly white, with just a bit of black accents for the touchpad, sticks, and buttons — the ratio of white-to-black fells good when my eyes look at it.

Really, the whole package looks like a team of sports-car designers built it, which is more grown up than monolithic, monotone, and monotonous black that we typically get.