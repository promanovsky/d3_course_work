A new report from the World Health Organization (WHO) paints a grimmer picture of the negative effects of polluted air. In a new research released Tuesday, WHO reported that about 7 million people worldwide died in 2012 because of air pollution.

The estimate accounted for one in eight deaths worldwide and doubles earlier estimates. The agency's last estimate for air pollution-related deaths was released in 2008 and it only accounted outdoor pollution with 1.3 million deaths and indoor pollution with 1.9 million deaths.

The latest data associate indoor air pollution with 4.3 million deaths and outdoor air pollution with 3.7 million deaths establishing air pollution as the world's biggest environmental health risk.

"Air pollution, and we're talking about both indoors and outdoors, is now the biggest environmental health problem, and it's affecting everyone, both developed and developing countries," said WHO public and environmental health chief Maria Neira. "Few risks have a greater impact on global health today than air pollution. The evidence signals the need for concerted action to clean up the air we all breathe."

The UN agency said that of those who died from indoor pollution, most have lived in homes that use wood, biomass and coal stoves. The agency also said that the hardest hit are countries in South-East Asia and Western Pacific Regions which accounted for 5.9 million of the death toll.

Flavia Bustreo, WHO Assistant Director-General for Family, Women's and Children's Health, said that the poor pays the heavy price of indoor pollution because they are more exposed to smoke and soot coming from the coal and wood cook stoves that they use at home. Bustreo has likewise noted the importance of clean air.

"Cleaning up the air we breathe prevents non-communicable diseases as well as reduces disease risks among women and vulnerable groups, including children and the elderly," Bustreo said.

Of the outdoor air pollution-related deaths, 40 percent happened via ischaemic heart disease, 40 percent through stroke, 11 percent via chronic obstructive pulmonary disease (COPD), 6 percent through lung cancer and 3 percent via acute lower respiratory infections in children.

Thirty-four percent of indoor air pollution related deaths, on the other hand, occurred via stroke, 26 percent through ischaemic heart disease, 22 percent via COPD, 12 percent via acute lower respiratory infections in children and 6 percent through lung cancer.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.