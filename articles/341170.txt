Cuba sold just 50 cars in six months

Share this article: Share Tweet Share Share Share Email Share

Havana - Cuban dealers sold just 50 cars and four motorcycles across the country in the first six months of the year under a new law that removed limits on vehicle purchases for the first time in half a century. Problem is, the new cars came with prices so high that few people could afford them. Long-frustrated Cubans welcomed the law that took effect in January until they saw sticker prices were marked up 400 percent or more, pricing family sedans like European sports cars. Cuba has said it would invest 75 percent of the proceeds from new car sales in its woeful public transportation system. But total sales at the country's 11 national dealerships reached just $1.28 million in the first six months of the year, the official website Cubadebate.com reported on Monday, citing Iset Vazquez, vice president of the state enterprise Corporacion CIMEX. Before the start of this year Cubans had to request authorisation from the government to buy from state retailers, which sell new and second-hand vehicles, usually former rental cars.

Most of the sales this year appeared to be of the second-hand variety considering the average sale price of $23 759 (R250 000) per vehicle, including the motorcycles.

PAYING A HUGE PREMIUM

A Havana Peugeot dealership was pricing its 2013 model 206 at $91 000 (R965 000) when the new rules came into effect, and it wanted $262 000 (R2.77 million) for the sportier 508.

Such prices drew howls of protest from the few Cubans who could even consider buying a car. Most state workers make around $20 (R212) a month.

The high prices have also been a complaint of foreign businesses and potential investors, who need government permission to import a new or used car without the huge markup.

Cuba is only gradually loosening the vehicle market. In 2011, it started allowing its people to buy and sell used cars from each other. Before then, only cars that were in Cuba before the 1959 revolution could be freely bought and sold, which is why there are so many US-made, vintage 1950s cars on the streets.

Giant Chevys and Buicks rumble alongside little Soviet-made Ladas, another popular brand dating from the era before 1991 when Moscow was the communist island's main benefactor.

Reuters