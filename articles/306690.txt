Yesterday Apple revealed a new iMac that costs $1,099. That is $200 cheaper than the base model previously was, but they had to make some compromises to make it cheaper. For instance, the processor and GPU were downgraded, and now it looks like the 8GB of RAM that comes under the hood is not upgradeable. Sorry DIYers.

The folks at Other World Computing got their hands on the new iMac and did a teardown, which is how we know this. If you select the $1,099 iMac in Apple’s online store, you’ll soon see that you can not upgrade the memory to 16GB on the customization page. It would be almost impossible to upgrade it by yourself, which is a real shame.



Their teardown revealed that the RAM for the new iMac has been soldered to the motherboard, so you will be stuck with 8GB of storage. Period. If you’re a power user looking to boost the memory here, this is not for you. For other users it likely won’t matter. The previous base model allowed users to add more RAM, although it involved the removal of the screen which is tricky. The 27-inch model even has a door on the back for easy access to RAM. Hopefully this news doesn’t sour you on the iMac.

Source Ubergizmo

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more