Xbox Live down as Titanfall launches [Update: Fixed]

Eager Titanfall gamers have seemingly overwhelmed Xbox Live, with Microsoft’s online gaming service down for many players. Reports of issues signing in to Xbox Live began earlier today, with Microsoft recently confirming that it was investigating the problem. Access to the online service is required for multiplayer-only Titanfall, among other games.

“Are you having difficulties signing in to Xbox Live?” Microsoft’s service status page for Xbox asks. “We are currently engaged and our team is hard at work to get you back online. Thanks for your patience while we get this fixed.”

Microsoft has not specifically named Titanfall as the cause of the downtime, and in fact spokesperson Major Nelson has said that it is “not a Titanfall issue” on Twitter. Nonetheless, it seems unlikely to be a coincidence that Xbox Live would struggle to stay up when one of the most-anticipated titles of recent months is released.

Meanwhile, some Xbox One players are reporting being able to access Xbox Live with other titles, and only encountering problems with Titanfall. Others have been able to continue playing the new EA game irregardless of the issues their fellows have had.

Whatever the cause, the downtime is unlikely to impress those who have been waiting for Titanfall to arrive.

If you’re having trouble getting in – or aren’t sure whether Titanfall is for you – our full review has all the details (and there’s a summary of the key points if you’re short on time).

Update: Microsoft addressed the problem, and recommends a power cycle if you’re still having issues connecting. Happy gaming!