The city of Detroit reached tentative agreements to preserve pensions for retired police office and firefighters but cut monthly payments for other former employees, key deals that could accelerate the largest public bankruptcy in U.S. history, officials said Tuesday.

Negotiators for the general pension fund, which pays benefits to retirees who didn't work in public safety, agreed to a 4.5 percent cut and the elimination of cost-of-living payments, fund spokeswoman Tina Bassett said.

Despite the cuts, it's a vast improvement over the drastic 26 percent reduction that had been proposed by Detroit emergency manager Kevyn Orr, who is guiding the city through the bankruptcy process.

"This was the best possible agreement we could make," Bassett told The Associated Press.

Hours earlier, a group representing retired police and firefighters said it had reached its own deal to preserve pension benefits but slightly trim cost-of-living payments.

Together, the agreements would cover more than 20,000 Detroit retired workers. Those retirees, as well as thousands of active employees who qualify for a future pension, will get an opportunity to vote as creditors in the bankruptcy in the weeks ahead. Judge Steven Rhodes also needs to review the deal as part of Detroit's broader plan to emerge from bankruptcy by fall.

Orr's spokesman, Bill Nowling, declined to comment on the announced deal with the general pension fund.

Earlier Tuesday, Nowling said Detroit believes it can afford the deal with retired police and firefighters partly because their pension fund's financial performance has improved along with Wall Street markets.

"Sooner or later reality sinks in," Don Taylor, president of the Retired Detroit Police and Fire Fighters Association, told The Detroit News. "The city's in bankruptcy so you have to do the best you can for the majority of your members."

Association attorney Ryan Plecha said preserving the pension benefits was the "crown jewel."

Both pension deals are contingent on Detroit getting $816 million from foundations, philanthropists and the state of Michigan. Lawmakers still haven't approved the state's $350 million share, which has been endorsed by Gov. Rick Snyder.

The pot of money would prevent the sale of city-owned art and be earmarked solely for retirees who draw benefits from the two pension funds.

The average annual pension for police and fire retirees now is $32,000, while most other retired city workers get $19,000 to $20,000.

"Judge Rhodes will not approve a plan that over-promises," bankruptcy expert Doug Bernstein said. "They are definitely going to have to back up their numbers."

Detroit filed for bankruptcy last July, citing $18 billion in unmanageable long-term liabilities.

The city last week settled with holders of $388 million in bonds, agreeing to pay 74 cents for each dollar owed. Separately, the judge signed off on an $85 million agreement that releases Detroit from a disastrous debt deal made years ago that carried high rates of interest.