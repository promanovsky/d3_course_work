Many women who have both breasts removed after a cancer diagnosis may be wrong to take such a drastic step

Nearly three-quarters of women who have both breasts removed after a cancer diagnosis may be wrong to take the drastic step, a study has suggested.

Researchers who studied 1,447 women treated for breast cancer found that 8% of them had undergone a double mastectomy.

But 70% of these women did not meet the medically approved criteria for losing both breasts – a family history of breast or ovarian cancer, or BRCA 1 or BRCA 2 gene mutations.

They had a very low risk of developing cancer in the healthy breast, the US scientists said.

Study leader Dr Sarah Hawley, from the University of Michigan, said: "Women appear to be using worry over cancer recurrence to choose contralateral prophylactic mastectomy.

"This does not make sense, because having a non-affected breast removed will not reduce the risk of recurrence in the affected breast. For women who do not have a strong family history or a genetic finding, we would argue it's probably not appropriate to get the unaffected breast removed."

The research, published in the journal JAMA Surgery, also found that 18% of the women studied had considered a double mastectomy. About three-quarters of the patients reported being very worried about their cancer. Those who chose to have both breasts removed were significantly more likely to be concerned.

Belfast Telegraph