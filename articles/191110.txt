Saudi Arabia replaced the head of Jeddah's King Fahd Hospital on Tuesday as it struggled with mounting deaths from the SARS-like Middle East Respiratory Syndrome ahead of an influx of Muslim pilgrims in July.



The Health Ministry said on its website the move aimed to fight the spread of the virus and would "guarantee the immediate improvement of the medical care service" in the hospital, where a number of MERS patients are being treated.



In a separate statement, the World Health Organization (WHO) said outbreaks of MERS in Jeddah's two main hospitals —- King Fahd and King Faisal — were partly due to "breaches" in its recommended infection prevention and control measures.



But current evidence indicated there has been no significant change in the virus' ability to spread, the WHO said after a

five-day mission by a team of experts to Saudi Arabia.



"The majority of human-to-human infections occurred in health care facilities. One quarter of all cases have been

health care workers," WHO said.

There was a clear need to improve health care workers' knowledge and attitudes about the disease and systematically

apply WHO's recommended measures in health care facilities.



Saudi Arabia has reported 431 cases of MERS since the disease was identified in 2012, of which 117 have been fatal,

according to the latest figures posted on the ministry website.



The spread of new infections slowed during the winter, but there has been a sudden increase since last month, with many of the new cases recorded in Jeddah, the kingdom's second city.



Two deaths were reported on Tuesday, along with 10 new cases in Jeddah, in the capital Riyadh, in the western city of Taif and in the holy cities of Mecca and Medina.

Hajj pilgrim concerns



The upsurge is of particular concern because of the influx of pilgrims from around the world expected in July during the

Muslim fasting month of Ramadan.



Amid growing public disquiet at the spread of the disease, King Abdullah sacked his health minister on April 21.



The authorities have at times struggled to counter rumours swirling on social media that they have not been transparent

about the spread of the disease and the effectiveness of the prevention measures implemented so far.



MERS is a coronavirus like SARS, which killed around 800 people worldwide after first appearing in China in 2002. It can cause coughing, fever and pneumonia and there is no vaccine or anti-viral treatment against it.



Scientists say MERS does not transmit easily between people, although it could mutate. The most likely animal reservoir from which new cases are becoming infected is Saudi Arabia's large population of camels.



Arab countries including Qatar, Kuwait, the United Arab Emirates, Oman and Tunisia have all reported cases of MERS, as well as several countries in Europe.



Last week, the United States confirmed its first case, a man who had been a health worker in Saudi Arabia.