Apple has lowered the price of its MacBook Air laptops by $100 and also upgraded its processor to attract new buyers.

Apple, the finest computer and laptop making company, is upping the ante on its smallest Macs that are now available at a reduced price and with an upgraded processor. In a bid to attract new buyers, the Cupertino tech giant is offering all four of its pre-configured MacBook Air laptops at $100 lower than the usual selling price. Another notable change is that the processor now clocks speeds up to 2.7GHz, up from the previous 2.6GHz Turbo Boost speed.

Apple silently updated its online store with the new changes. Customers looking to buy a MacBook Air might find the new change enticing compared to the standard offerings of last year's models. The 2014 MacBook Air still uses Intel's Haswell architecture with a slight improvement to 1.4GHz Core i5 CPU, which replaces last year's 1.3GHz processor. It translates to a higher Turbo Boost speed up to 2.7GHz on both 11-inch and 13-inch MacBook Air models. The upgraded processors can help the laptops in better power efficiency and cooling, CNET reports.

The 11-inch laptop still comes in two standard variants, one which offers 128GB solid-state drive with 4GB RAM and the higher-end 11.6-inch model that packs 256GB solid-state drive. Additionally, the bigger 13-inch MacBook Air models comes in two options, 128GB SSD and 256GB SSD configurations, both of which have been upgraded with 1.4GHz Intel Core i5 processor.

Other specifications remain unchanged in both MacBook Air models, which include a 1,366x768 resolution for the 11-inch model and 1,440x900 pixels for the bigger version. Both laptop displays are powered by Intel's HD5000 graphics. Sadly, there is still no retina display. Both MacBook Air models feature Thunderbolt and USB 3.0 ports and a SDXC card reader.

After the $100 reduction on price in the U.S. or £100 if you are in the U.K., the 11-inch 128GB SSD MacBook Air costs $899 and the 256GB SSD variant costs $1,099. For buyers in the U.K., the lower-end 11-inch MacBook Air costs £749.00 and the higher-end model costs £899.00.

The 13-inch MacBook Air in the U.S. starts at $999, down from its previous $1,099 price tag, and the higher-end model now costs $1,199.00. In the U.K., the 128GB SSD model costs £849.00 and the 13-inch model with 256GB SSD costs £999.00. All products include free shipping and tax.

If you are looking for a change, then it is best to wait it out. According to CNET News, KGI Securities and NPD DisplaySearch have reported that an all-new MacBook Air is due with a thinner build and a 12-inch Retina display.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.