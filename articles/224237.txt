Blackshades raids confirmed, detailed by FBI

Earlier this week, sources speaking to The Wall Street Journal tipped that raids were taking place on the hacking collective surrounding Blackshades, a remote access tool that made spying on others a simple matter. Today the FBI confirmed the raids, and detailed a bit about what went down.

The raids were officially revealed by the U.S. Attorney’s Office for the Southern District of New York and the related FBI field office. It was also revealed that Michael Hogue, a US citizen, had plead guilty for co-developing Blackshades, as well as the indictment of co-developer Alex Yucel of Sweden.

According to the FBI, Blackshades was sold and distributed through over 100 countries, and that — perhaps the most surprising number — it has been placed on more than 500,000 computers around the world. As part of the raid, more than 100 search warrants, and more than 100 interviews took place, and 1900+ domains were seized.

The co-developers weren’t the only ones arrested, with another unnamed individual said to have aided in marketing Blackshades and two users who infected computers also having been arrested and charged. This is said to be part of an “unprecedented law enforcement operation involving 18 other countries,” and more than 90 arrests have been made through it.

SOURCE: FBI