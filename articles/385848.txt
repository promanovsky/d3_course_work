Another day, another "Weird Al" Yankovic video.

This time, the parody master is spoofing Lorde's breakout hit "Royals" with "Foil," an ode to dinner leftovers and a PSA for proper food preservation — oh, and a theory or two on the government, the Illuminati and aliens.

Released Wednesday, the video also features Patton Oswalt acting as the director, plus Thomas Lennon and Ben Garant as government security detail.

PHOTOS 10 Global Pop Icons: Exclusive Portraits of Lorde, Ellie Goulding, Phoenix

The Grammy winner is promoting his new album Mandatory Fun (RCA Records) with eight music videos in eight days. Earlier this week, Yankovic shared "Tacky" and "Word Crimes," his takes on Pharrell Williams' "Happy" and Robin Thicke's "Blurred Lines." The videos feature cameos by Jack Black, Eric Stonestreet, Aisha Tyler, Margaret Cho and Kristen Schaal.

The album also includes parodies of such songs as Iggy Azalea's “Fancy” (“Handy”) and Imagine Dragons' “Radioactive” (“Inactive”), as well as style takeoffs on Southern Culture on the Skids (“Lame Claim to Fame”), Foo Fighters (“My Own Eyes”), Crosby, Stills & Nash (“Mission Statement”), Pixies (“First World Problems”) and Cat Stevens (“Jackson Park Express”). The “NOW That’s What I Call Polka!” medley features Al on accordion running through snippets of “Wrecking Ball,” “Pumped Up Kicks,” “Best Song Ever,” “Gangnam Style,” “Call Me Maybe,” “Scream & Shout,” “Somebody That I Used to Know,” “Timber,” “Sexy and I Know It,” “Thrift Shop” and “Get Lucky.”

Watch the "Foil" video below:

Email: Ashley.Lee@THR.com

Twitter: @cashleelee