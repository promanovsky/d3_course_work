Mail Online has apologised to George Clooney for publishing an inaccurate story about him, his fiancee and her mother.

It has also taken down the article from its website and is planning to give the actor an "opportunity to set the record straight."

The story, published originally by Mail Online and then published, in part, by the Daily Mail, stated that the mother of Amal Alamuddin, who is engaged to Clooney, had opposed their forthcoming marriage on religious grounds.

Clooney issued a statement accusing the Mail of irresponsibility in publishing what he said was "a completely fabricated story."

It stated that Alamuddin's mother, Baria, was a member of Lebanon's Druze community and had been telling "half of Beirut" that she was against the wedding.

But Clooney pointed out that Baria Alamuddin, who edits a newspaper in London, is not Druze and "has not been to Beirut since Amal and I have been dating" and that "she is in no way against the marriage."

In response, Mail Online have issued this statement:

"The Mail Online story was not a fabrication but supplied in good faith by a reputable and trusted freelance journalist. She based her story on conversations with a long-standing contact who has strong connections with senior members of the Lebanese community in the UK and the Druze in Beirut. We only became aware of Mr Clooney's concerns this morning and have launched a full investigation. However, we accept Mr Clooney's assurance that the story is inaccurate and we apologise to him, Miss Amal Alamuddin and her mother, Baria, for any distress caused. We have removed the article from our website and will be contacting Mr Clooney's representatives to discuss giving him the opportunity to set the record straight."

The Daily Mail newspaper and Mail Online are separately edited, by Paul Dacre and Martin Clarke respectively. It would appear that the paper picked up the Mail Online story and, against normal internal rules, did not attribute it to Mail Online.