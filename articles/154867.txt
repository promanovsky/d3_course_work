In the endless war of incumbents versus insurgents, Tuesday's oral arguments at the US supreme court – America's broadcast TV networks against a video-streaming startup called Aereo – will ultimately be one small battle. But they remained a useful, if complex, illustration of the way a supposedly free-market economy has become so beholden to the needs, and whims, of entrenched interests.



And American Broadcasting Companies v Aereo reminds us how innovators in all sorts of arenas so often skirt the edges of legality – indeed, how they regularly skip right over laws and regulations that are designed to protect the business of incumbency as much as, if not more than, to serve the public interest.

For those who are new to the Aereo case, it's all about broadcast-network panic over a service that bypasses telecom control over how and where viewers watch programming. "Free TV" broadcast over

airwaves lets you set up an antenna to collect the signal; you're allowed to then stream it from a device in your home to your laptop or other device. In giant clusters, Aereo sets up a tiny antenna for each customer, who can then stream it elsewhere in the same way.



Yes, it's a loophole. And the broadcasters went ballistic, because they make billions from cable and satellite companies that retransmit the same signals to their own customers. In their typically over-the-top way, the people who run ABC, NBC, CBS and Fox are insisting that an Aereo win would mean the demise of broadcast TV as we've known it. To which the proper response should be: it's about time.

As I wrote here a year ago, I hope Aereo wins the legal case. If that happens, however, I also expect Congress to intervene on behalf of the broadcasters, which, history demonstrates, have unusually powerful sway over the lawmaking process.



The early word out of the supreme court chamber is that it's probably too soon to tell. Some justices worried about the future of the cloud – "why aren't they a cable company?" asked Justice Sonia Sotomayor; the broadcasters' line of reasoning "makes me nervous", said Justice Stephen Breyer – while others hammered on the Barry Diller-backed streaming company.

"There's no reason for you to have 10,000 dime-sized antennas except to get around the Copyright Act," Chief Justice John Roberts told Aereo's lawyer. "Your technological model is solely based on circumventing" the law.

Aereo is not alone. There are similar fights playing out all over America, and around the world, as companies like Uber, the quasi-taxi service, and Airbnb, the increasingly popular quasi-hotel operation, carve out significant new market shares.



In Uber's case, the entrenched and politically well-connected taxi interests are trying to get cities to ban it or force the company's freelance drivers to comply with tight regulations aimed, in part, at restricting the number of cabs in a town. Uber's CEO is an in-your-face libertarian, and he operates the company that way. Its "surge pricing" – charging what the market will bear depending on the conditions and number of available cars – generates ill will at times, but it's also logical as a matter of basic economics.



Airbnb has operated at the edge in some ways, too. Zoning laws that restrict lodging establishments to certain areas aren't entirely wrong, even though they also face another precedent-setting legal challenge today. We'd be fine with our neighbors renting out their place periodically, but if new Airbnb customers showed up every day, we'd take issue with that. Moreover, it's entirely fair for communities to require Airbnb to add lodging taxes others must pay to its own charges.



There's a small public-interest issue with Aereo, too. Broadcast TV does serve communities with programming – often beyond dreadful, especially what passes for local "news" – that requires no cable or satellite subscription, just the ability to get the signal over the air.



For Aereo, the stakes in this case are simple: the business lives or dies. It "probably will go out of business," the broadcasters' attorney proclaimed outside the court this afternoon, "and nobody should cry here."

That's not true for the broadcasters, however much they bleat to the contrary. They are still part of an interlocking cartel that exists entirely because Congress has given a public resource – the airwaves – to a small collection of commercial interests. Yes, TV's broadcast spectrum licenses, worth uncountable billions of dollars, were handed over to these robber barons at no charge. Their claim that Aereo is "stealing" is laughable given the heist they pulled off years ago.

(Speaking of incumbents-v-insurgents, today's court arguments should be an embarrassment to the legacy journalism trade. The media troglodytes who decide which organizations get supreme court credentials pulled a heist of their own by barring ScotusBlog, which provides some of the best court coverage, and stonewalling questions about their retrograde move.)

So in the event that Aereo wins and Congress unaccountably does the right thing, and then the networks threaten to take their programming off the air, we should welcome that result. Because then we'd be closer to a genuinely free market for programming – assuming, of course, that the cable industry doesn't simply take control itself.

Better still, we could get the spectrum back and put it to use in productive ways – freeing it for digital uses that would generate vast new innovation. Real innovation, the legal way.

