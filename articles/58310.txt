Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Gwyneth Paltrow and Chris Martin have broken their silence and thanked their fans for support.

Sneaky traffic driver Gwyn took to her website (where she announced the split) on Friday morning to share a heartfelt message, entitled One Bird, Three Meals, with her followers.

"Our moms were raised by depression-era ladies who used a single tea bag for two cups of tea and never met a leftover they didn’t eat. This issue of three delicious chicken recipes is in honor of our grandmothers, who taught us the good old-fashioned art of not wasting a scrap," she wrote.

"P.S. CM and I in deep gratitude for the support of so many," she added.

The pair announced their split "with hearts full of sadness" on Tuesday evening after more than a decade of marriage.

The glamorous pair, who have two children Apple, 9, and Moses, 7, revealed they had been battling to save their marriage for a year.

In a joint statement released on Tuesday night they said they had finally decided to separate despite saying they still loved each other ‘very much’.

Shakespeare in Love star Gwyneth, 41, and Coldplay frontman Chris said they had split in a joint statement titled ‘conscious uncoupling’.

(Image: Getty)

They said: “It is with hearts full of sadness that we have decided to separate.

"We have been working hard for well over a year, some of it together, some of it separated, to see what might have been possible between us, and we have come to the conclusion that while we love each other very much we will remain separate.

“We are, however, and always will be a family, and in many ways we are closer than we have ever been.

“We are parents first and foremost, to two incredibly wonderful children and we ask for their and our space and privacy to be respected at this difficult time.

“We have always conducted our relationship privately, and we hope that as we consciously uncouple and coparent, we will be able to continue in the same manner.

"Love, Gwyneth & Chris.”

(Image: Goop.com)

Gwyneth and Chris, 37, met three weeks after her father Bruce died in October 2002.

They married just over a year later in December 2003 in a ceremony at a hotel in California.

In May 2013 she admitted they had marriage troubles, telling Glamour magazine: “It’s hard being married.

"You go through great times, you go through terrible times. We’re the same as any couple,” she said.