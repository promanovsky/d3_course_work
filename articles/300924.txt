After what some are calling a horrendous abuse of power by the state, a Massachusetts judge has ordered that custody of Justina Pelletier be returned to her parents. She goes home Wednesday.

It has been a harrowing 16-month long ordeal for the Pelletier family, beginning when Justina’s parents took their daughter to Boston Children’s Hospital for cold and flu symptoms. She had been previously diagnosed by her own physicians with mitochondrial disease, but the doctors at Boston decided that the diagnosis was wrong, and that Justina suffered from a mental condition.

Dr. Simona Bujoreanu, psychologist, diagnosed Somatoform Disorder in Justina after only spending 25 minutes with her, reports WND, absent any consultation with other physicians, and without a thorough review of her records, according to Liberty Counsel. However, her parents refused to go along with the diagnosis and sign the new treatment plan proposed by Boston Children’s Hospital. The new treatment plan called for a discontinuation of all medical care and disallowed any second opinions.

When the Pelletiers requested that their daughter be discharged so that they could take her back to her own doctors at Tufts Medical Center, the Massachusetts Department of Children and Families were called. Justina was forcibly removed from her parents’ custody and given over as a ward of the state under the care of the Boston Hospital, citing claims of “medical child abuse.”

Not coincidentally, Dr. Bujoreanu is working on a research project on Somatoform Disorder, and being paid with grant money. The law allows for any ward of the state, which Justina now was, to be subjected to medical research without their consent, whether or not that research is primarily intended for the ward’s benefit.

Over the course of a number of months, the Pelletiers and their legal counsel were able to piece together the motives for the unjust kidnapping of their child, learning just how deep the rabbit hole went. All the while, Justina’s physical condition deteriorated drastically, because she was not getting the medical treatment that she actually needed, treatment that her parents had been providing for her.

Justina went from being a competitive figure skater to being a wheelchair-bound invalid, afraid for her very survival, while under the care of the Nazi scientist experimenters , Boston Children’s Hospital.

Her desperate father, Lou Pelletier, finally risked imprisonment and defied court orders to go public with information about what was happening to his daughter. As reported by The Inquisitr, that courageous decision ultimately led to an important step in the right direction, of Justina being ordered in March to have her medical care returned to Tufts instead of Boston.

Now, according to the Boston Globe, Massachusetts juvenile court Judge Joseph Johnston has written the ruling that Justina Pelletier’s family, and the world, have been hoping and praying for:

“I find that the parties have shown by credible evidence that circumstances have changed since the adjudication on Dec. 20, 2013, that Justina is a child in need of care and protection pursuant to G.L. c. 199, 24-26. Effective Wednesday, June 18, 2014, this care and protection petition is dismissed and custody of Justina is returned to her parents, Lou and Linda Pelletier.”

This ruling follows a videotaped plea to Judge Johnston from Justina herself that was posted on the Miracle for Justina Facebook page. From her wheelchair, the 15-year-old pleaded, “All I really want is to be with my family and friends. You can do it. You’re the one that’s judging this. Please let me go home.” Her wish has finally been granted.

Personhood USA reports that the family spokesperson for the Pelletiers, Reverend Patrick Mahoney, stated:

“We are so thankful and thrilled that Justina is at long last being returned to the loving arms of her family. The Pelletiers are relieved that this 16-month nightmare is finally over. Now Justina can begin the healing process.”

[images via bing and Facebook]