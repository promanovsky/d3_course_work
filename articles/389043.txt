Marvel Comics has revealed the identity of Captain America's replacement.

Sam Wilson - better known as the Falcon - will replace an ailing Steve Rogers as part of the publisher's new Avengers NOW! initiative.



Writer Rick Remender (Captain America) and Stuart Immonen (All-New X-Men) will be working together on All-New Captain America.

"I don't have to tell you that the world is falling apart under Barack Obama's leadership," said Stephen Colbert on The Colbert Report, where the new Captain America was announced.

"We're losing one of our greatest leaders, Captain America. He won our hearts back in 1941 by punching Hitler in the face!"



He continued: "Unfortunately, Cap's days of protecting America are numbered.

"With Steve Rogers brought low there's a huge void in the Captaining of America. Who among us is prepared to step in?

"You'd have to be extremely patriotic, look decades younger than your actual age, and you'd have to own Captain America's actual shield."



This week's announcements have seen Marvel attempting to broaden its diversity, with a new female Thor and now a black Captain America.

The Falcon is a long-time ally of Captain America.

In Marvel's continuity, there was also a black Captain America in World War II - Isaiah Bradley.

All-New Captain America #1 will be released in November.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io