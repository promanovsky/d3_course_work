Google announced at its annual I/O developer conference yesterday that it would be integrating Samsung's Knox technology into its latest Android L mobile operating system as part of a bid to make the world's most popular smartphone operating system more secure and more appealling to business.

The integration of Knox is part of the new Android for Work program which will allow business and personal content to coexist on the same device - essentially ridding the need for two separate smartphones - as well as the bulk deployment of apps across enterprises.

Perhaps more importantly, however, Google will be hoping that the integration of Knox will give a much needed boost to Android's security, which has come in for some heavy criticism in recent months.

At Apple's developer conference earlier this month, Apple CEO Tim Cook took the opportunity to slam the security flaws in the world's most popular operating system.

Quoting self-confessed Android fan Adrian Kingsley-Hughes of ZDNet, Cook said that the fragmentation of Android was turning devices into a "toxic hellstew of vulnerabilities."

Cook then produced a pie chart that demonstrated how 99% of all mobile malware incidents were to be found on Android.

In response to these accusations, senior vice president at Google Sundar Pichai acknowledged Android's malware issues but claimed they were an inevitable biproduct of its success.

"Android from the ground up is designed to be very, very secure," Pichai said in a recent interview with BusinessWeek. "History shows typically that malware is also targeted at the more popular operating system. So you know there is that."

'Killer app'?

Knox is currently the only Android provider of defence-grade mobile security system that complies with the US government and Department of Defence's standards for mobile security,

With these credentials, Samsung believes that it is enough to put Android devices in the best position to fill the space left by the beleaguered BlackBerry.

"We are delighted with the opportunity to work with Google to help build Android's enterprise eco-system and establish Android device as the leading choice for businesses," Injong Rhee, senior vice president of KNOX Business Group at Samsung, said in a statement. "This represents an amazing transformation in workforce mobility."

Sean Sullivan, a mobile security expert at F-Secure, believes that the integration is a "move in the right direction" but is unconvinced that it will solve all the problems.

"I don't know if this will be the 'killer app' which makes the difference," Sullivan told IBTimes UK. "Many security-minded businesses are already allowing personal devices in their networks. Perhaps Knox won't move them further, but it could allow them to more easily manage what they've already allowed. And things can then evolve from there."