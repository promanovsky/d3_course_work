Microsoft has made the source code for its early versions of MS-DOS and Word for Windows available to the public.

The news came from Microsoft's Technet blog on which Roy Levin, distinguished engineer and managing director at Microsoft Research, stated, "On Tuesday, we dusted off the source code for early versions of MS-DOS and Word for Windows. With the help of the Computer History Museum, we are making this code available to the public for the first time."

The Computer History Museum will be making the source code of MS-DOS 1.1 and 2.0 along with Microsoft Word for Windows 1.1a available "to help future generations of technologists better understand the roots of personal computing."

The website further details how the MS-DOS (where DOS stands for disk operating system) came into existence. In 1981, IBM approached Microsoft to create an operating system for its IBM PC range, which led Microsoft to license 86-DOS from Seattle Computer Products, and modify it to meet IBM's requirements. It was OEM-branded by IBM as PC-DOS, and differently by Compaq and Zenith, apart from being released in a non-OEM version as MS-DOS.

In 1983, Microsoft created the first MS-DOS based version of Microsoft Word, which was "designed to be used with a mouse." However it was only in 1989, with the new Microsoft Word for Windows, that the software gained importance around the globe.

In the related news, Microsoft is reportedly expected to announce the rebranding of "Windows Azure" to "Microsoft Azure" during this week. Reports suggest that the announcement is expected on Tuesday at the second day of Microsoft's Build Developer Conference in San Francisco. The change is expected to come in effect from 3 April 2014.

This move helps not just to differentiate Azure from Windows, but also highlight's the company's place as a cross platform software and services provider, something Satya Nadella talked about in his first public remarks as Microsoft's new CEO.