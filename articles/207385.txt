Dean Kamen’s incredible robot arm is good to go

Dean Kamen’s robotic prosthetic limb, the DEKA Arm System, has been granted FDA approval, with the DARPA-sponsored project controlled by electrical signals from sensors where it meets the wearer’s limb. Dubbed “Luke” – a reference to Luke Skywalker from the Star Wars universe – the arm is a huge step forward from existing mechanical prosthetics, allowing for more detailed uses like turning keys and pulling zippers.

The arm is roughly equivalent in weight to a human limb, but is packed with sensors and motors. EMG electrodes pick up the contraction of muscles in the arm close to the point of attachment, with up to ten powered movements being differentiated between.

Those sensors are accompanied by a range of movement and force sensors, as well as switches, which allow the arm and the hand to react to whatever it is gripping.

Six different grips can be fitted, to suit different purposes; the FDA’s trials of 36 people showed that 90-percent of people taking part with prototypes could do tasks that they were simply unable to with their current prosthesis.

For instance, the DEKA Arm can pick up delicate objects, such as fruit, without crushing them.

The lingering question around the prosthetic is cost, of course. DEKA is yet to specify exactly how much it may cost to end-users, though speaking to SlashGear earlier this month, Dean Kamen said he envisaged it being broadly available.

“Probably all veterans will easily have it. Some people with particularly good insurance will probably have it. And as you come down the line from there, some people, sadly… the ability to access it will be limited by finance and public health policy. It’s something we’re always trying to push, but it’s not my area of expertise” Dean Kamen, DEKA

Limb loss at the shoulder joint, mid-upper arm, or mid-lower arm is supported by the DEKA system; however, elbow or wrist loss is not supported. FDA testing also included its resilience to dust, light rain, impact, and other environmental issues.

VIA Engadget

SOURCE DARPA