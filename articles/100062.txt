President Barack Obama praised outgoing Health and Human Secretary Kathleen Sebelius for helping to steer his healthcare law's comeback after a rocky rollout, even as he nominated a successor aimed at helping the White House move past the political damage.

"Under Kathleen's leadership, her team at HHS turned the corner, got it fixed, got the job done," Obama said in a Rose Garden ceremony Friday morning. "And the final score speaks for itself."

About 7.5 million people have signed up for health insurance through the new law, exceeding expectations after website woes stymied signups for the week when enrollment opened last fall.

Obama nominated his budget chief, Sylvia Mathews Burwell, to replace Sebelius, calling her "a proven manager" who knows how to get results. The nomination of Burwell, who was unanimously confirmed by the Senate for her current post last year, appeared aimed at avoiding an election-year confirmation fight.

"Last time, she was confirmed unanimously," Obama said. "I'm assuming not much has changed since that time."

Sebelius, who has served as Obama's health and human services chief for five years, was instrumental in steering the sweeping health law through Congress in 2010. But her tenure was marred by widespread technical problems that crippled the signup website for weeks, sparking calls from Republicans for Sebelius' resignation.

While Obama stood by Sebelius publicly throughout the troubled rollout, it became clear that her close relationship with the White House had frayed. West Wing officials said they felt blindsided by the extent of the technical problems and installed a longtime Obama adviser to oversee fixes.

As she announced her resignation Friday, Sebelius called her work on the health law the "cause of my life."

"We are at the front lines of a long-overdue national change," she said.



