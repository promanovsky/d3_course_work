(Updates with company comment, background)

By Toni Clarke

WASHINGTON, June 27 (Reuters) - The U.S. Food and Drug Administration said on Friday it has approved MannKind Corp's inhaled insulin, Afrezza, capping an arduous journey for the company and its octogenarian founder, Alfred Mann.

Afrezza is delivered via a whistle-sized inhaler. It acts more rapidly than injectable insulins such as Eli Lilly and Co's Humalog and Novo Nordisk's NovoLog.

The FDA said the device offers a new treatment option for patients with diabetes.

"Today's approval broadens the options available for delivering mealtime insulin in the overall management of patients with diabetes who require it to control blood sugar levels," Dr. Jean-Marc Guettier, director of the FDA's endocrinology division, said in a statement.

Afrezza will carry a boxed warning, the strongest advisory available, of the risk of acute bronchospasm, or constriction of the airways of the lung, in patients with asthma and chronic obstructive pulmonary disease.

According to the prescribing information, patients with chronic lung disease such as asthma or chronic obstructive pulmonary disease should not use Afrezza. Physicians should take a detailed medical history, physical exam to identify potential lung disease before prescribing the product.

The label also recommends it not be used in patients who smoke or who have recently stopped smoking.

The company will also be required to conduct four additional studies: one to evaluate Afrezza's safety and efficacy in children; another to assess any potential increased risk for lung cancer and heart problems; and two to further examine the way the drug works and is dosed in the body.

MannKind's stock, which was trading down about 10 percent before news of the approval was announced, pared those losses to close down 5.5 percent at $10.00 on the Nasdaq Stock Exchange.

In April, an advisory committee to the FDA recommended approval of the device, though analysts warned at the time that the company would likely have to conduct additional studies to demonstrate its safety over the long term. The cost of these studies could keep potential partners on the sidelines, they said.

Story continues

The stock is up 43 percent since the positive advisory committee vote.

The road to approval has been a tough one for MannKind. The product was developed in the shadow of Pfizer Inc's failed inhaled insulin Exubera, which was approved in 2006 and expected to generate annual sales of $2 billion. But the inhaler was big and bulky and patients were put off by the need for periodic lung function tests. Eventually it was withdrawn.

That failure, together with concerns the inhaled powder could increase the risk of lung cancer, has led to considerable investor skepticism about Afrezza's future.

In 2011, the FDA rejected the device and asked for more clinical trials. Despite repeated set-backs, Mann, who is 88 and the company's biggest shareholder, pumped close to $1 billion of his own money into the Valencia, California-based company to keep it afloat.

For Mann, the FDA's decision is a vindication.

"Today's FDA action validates the years of clinical research and commitment that powered the development of this unique therapy," he said in a statement, adding that Afrezza "has the potential to change the way that diabetes is treated." (Additional reporting by Bill Berkrot in New York; Editing by Sandra Maler, Bernard Orr)