Nothing says, "Happy Mother's Day," quite like giving birth to new life.

Ohio-native Sarah Thistlewaite became the proud mother of "mono-mono" twins on the eve of Mother's Day.

Thistlewaite had been on bed rest for nearly two months prior to having her twin mono-mono twin girls via Caesarean section.

So what exactly does having mono-mono twins mean?

Having monoamniotic, aka "mono-mono," twins is extremely rare. These sets of twins usually occur in 1 in every 10,000 births. While growing inside of their mother's uterus, mono-mono twins share the same amniotic sac and placenta but have separate umbilical chords.

Becoming pregnant with mono-mono twins can be classified as a high-risk pregnancy because of several complications that can occur during the pregnancy that include cord entanglement, umbilical chord compression and twin-to-twin transfusion syndrome where one twin receives the majority of the nourishment, while the other is left undernourished.

Though the twin girls named Jenna and Jillian were born premature and will have to stay in the hospital for up to four weeks, they came out already full of love and holding hands.

How adorable!

"They're already best friends," Sarah Thistlewaite told The Akron Beacon Journal. "I can't believe they were holding hands. That's amazing."

Twins Jenna and Jillian are not the only children that Sarah and her husband Bill have. The twins can look forward to their older brother Jaxon who is only 15 months old.

Jaxon was looked after by friends and family while Sarah remained in the hospital for two months.

During her stay at the hospital, Sarah told News Net 5 that she and Bill were trying to keep Jaxon's life "as normal as possible."

"He comes up and visits for a couple of hours," Sarah said. "It's hard to be here, but we know, in the end, we're going to end up with two twin girls, and they're going to be healthy, and everything is great," Sarah said.

Congratulations to Sarah and Bill! And Happy Mother's Day to all!