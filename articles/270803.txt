Actor Jonah Hill has apologized for using a homophobic slur towards a paparazzo who mocked his floral print shorts.

TMZ posted a video on Tuesday that shows paparazzi following Hill in Los Angeles. One person can be heard heckling Hill, at one point saying: "I like the shorts though, bro. They are pretty sexy."

At the end of the minute-long video, Hill, a two-time Academy award nominee, tells the heckler: “Suck my dick, you faggot.”

Hours after the video was posted, Hill went on The Howard Stern Show to apologize for using the slur.

“What I said in that moment was a disgusting and hurtful term,” Hill said. “I should have said either nothing or just ‘Fuck you’.”

Hill said the person had been following him around making comments about his family. “I played into exactly what he wanted and lost my cool and in that moment, I said a disgusting word that does not at all reflect how I feel about any group of people,” Hill said.

He added that he has been a longtime gay rights activist. In November, Hill posted his opposition to Russia’s anti-gay laws in a tweet supporting Human Rights Campaign’s Love Conquers Hate campaign.



TMZ clarified that the person who clashed with Hill is not a TMZ employee.

