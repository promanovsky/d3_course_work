The June 30 editorial “Ebola, unchecked” highlighted the severity of an Ebola outbreak in West Africa but inadequately portrayed the challenges faced by the governments of the affected countries.

In its early stages, Ebola can be mistaken for other illnesses that plague the region: malaria, typhoid, Lassa. It took months to spot the rise in fever-related deaths and to identify Ebola — never before seen in the area — as the cause. By then it had spread across borders, and contact tracing became a major undertaking. Ebola has historically emerged deep within a country, rather than on the border of three, and the level of inter-country coordination needed was unprecedented.

As an added challenge, Liberia and Sierra Leone are still recovering from civil wars that decimated health infrastructure, brought government services nearly to a halt, prompted the exodus of educated professionals and interrupted the education of an entire generation. Today, these countries are peaceful and making great strides in strengthening their health systems, but the lingering effects of war can still be felt. A mostly rural population, high adult illiteracy and limited access to electricity — much less TV — make promoting public awareness particularly challenging.

Meredith Dyson, Freetown, Sierra Leone

The writer is a health program manager with Catholic Relief Services.