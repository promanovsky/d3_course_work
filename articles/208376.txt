Another week has gone by and little has been resolved in the markets.

Pretty much everyone is clueless and making up explanations for every movement that takes place. This is the type of environment in which it is wise to be patient in, and cut back on trading activity. The PowerShares (QQQ) - Get Report, which tracks the Nasdaq 100 index and iShares Russell 2000 (IWM) - Get Report are still removed from the action in S&P 500 IndexI:GSPC and Dow Jones Industrial Average (DOW) - Get Report whereas the former is still experiencing a pull-back and the later still very rangebound at or near highs.

The one change that was noticeable last week was that the Russell 2000 not only got below the 200-day, but closed below it. The small caps typically lead the way in a raging bull market and as you can see below by this monthly chart, rage they did.

Now that the bull market is slowing down, a natural progression after five years, it should be no surprise that we're experiencing a pull-back. If you step back and view the bigger picture on this monthly chart, it doesn't seem as ominous as when you view it from a daily perspective.

After weeks of underperformance in the Nasdaq, the Russel, the iShares NASDAQ Biotechnology Index (IBB) - Get Report, and high beta discretionary without pulling the S&P 500, Dow Jones Industrial Average, or value names down with them, it seems appropriate to visit the question again of whether this can go on for much longer. Although for weeks my bias was that it can't and that the S&P 500 and the Dow Jones would likely follow, I have so far been proven wrong.

Of course that could all change in a heartbeat, but after assessing the strength of certain sectors last week and the open interest of the indexes and many stocks for this coming May expiration, I have switched to short-term bullish. Currently the S&P 500 is stuck in a box and if this week should see a breakout than my current bias is a breakout to the upside, which would entail a break of 1890 that doesn't fall right back into the box.

What further supports this idea is that the market tends to be stronger during options expiration week when the VIX does not expire in the same week. The CBOE Volatility IndexI:VIX, a popular measure of the implied volatility of S&P 500 index expires Wednesday May 21st and I think the market can stay fairly strong until then. After that though I believe there is considerable risk that a healthy pull-back across the bored of indexes will take place as the summer doldrums kick in.

Supportive of that view is the lack of signs that are typical of a healthy uptrend. For instance, the issues making 20-day highs along with the S&P 500 continue to be very weak as seen below. Furthermore, there has not been any accumulation days in weeks to support the uptrend. And finally, there is still a lot of complacency by market participants that likely needs to be washed out before a longer term uptrend can resume.



What Would Shift My Bias? Although I'm going into this week with a bullish bias I am open to changing that view upon seeing the new leaders not lead or seeing unusual strength in the CBOE Volatility Index, which closed Friday at 12.92.

Prior to last week I had been looking toward the iShares Barclays 20+ Year Treasury Bond ETF (TLT) - Get Report to help me gauge market risk because they have been highly correlated with the S&P 500. However, I noticed last week that their seemed to be somewhat of a disconnect between them and history suggests that the bonds/yields are not always correlated with the market which leads me to believe that a period of non-correlation may soon be upon us.

Where is Money Rotating? Going back to the title of my post, next week I want to follow where leaders have been recently distinguishing themselves. I see that mainly in the industrials, the transports, and select discretionary such as beverages and some tech.

Furthermore, there appears to be an opportunity to short areas that the money is beginning to flow away from, such as the utilities that have greately benefitted from the first quarter of this year. Money seems to rotate in out of sectors fairly quickly so my suggestion is to not overstay your welcome when trading any one sector.

To learn more about my trading process, please visit my web site and follow me on Twitter.

At the time of publication, the author held no positions in any of the stocks mentioned.