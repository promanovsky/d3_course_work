Tokyo stocks rose to a fresh six-month high on Tuesday morning, as strong earnings from the likes of Nissan Motor Co suggested Corporate Japan has managed to weather a recent rise in the national sales tax. The Nikkei gained 0.5 percent to 15,612.66 in mid-morning trade, the highest level since January 23. Nissan jumped 4.5 percent and was the second-most traded stock by turnover after the automaker's April-June operating profit rose a higher-than-expected 13.4 percent to 122.6 billion yen ($1.20 billion). Market players said that investors were trading on individual ...