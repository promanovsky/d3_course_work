Apple is working on a system that automatically unlocks iPhones when they are in safe locations like home or work, but locks them again when they leave the area.



The system works by identifying where the iPhone is and allows users to set “safe” locations, where settings including security can be automatically changed removing the need for a passcode or Touch ID, for example.

“Based on the detected current location, the mobile device can modify settings and configurations. Security settings are one example of device behaviour that can be modified in accordance with embodiments of the present invention,” said Apple in the patent filing.

‘Some locations may be inherently more secure’

Security requirements help ensure that a mobile device is in the hands of the appropriate party. Often the security level remains the same regardless of the location of the mobile device.

“Because some locations may be inherently more secure, such as a user's home or office, these locations may be considered "safe" and require less stringent security,” explains the patent. “Conversely, some locations may be considered higher risk or "unsecure." In these locations, it can be desirable to implement stronger security protections.”

The patent could see iPhones unlocked within the vicinity of the home but locked while out on the street. The smartphone could determine its location using mobile phone signal, Wi-Fi networks, GPS, Bluetooth or the phone’s proximity to other phones.

A home zone defined by Wi-Fi networks and other location factors. Photograph: USPTO Photograph: USPTO

At least two location indicators will be required to verify location and create the changes in security or other software features.

The location accuracy of the phone is a potential drawback, however, as Wi-Fi networks often extend for some distance outside the home. It is unknown whether the system will be intelligent enough to realise that the phone has been stolen, but is still within range of its home Wi-Fi network, potentially giving thieves complete access to the phone.

Change a user’s home screen when at home or work

The automatic switching on or off of features could extend to security within individual apps, as well as the entire phone. The contacts, calendar or notes apps could lock down with a passcode when not in a “safe” zone, for instance. Work applications could also be fired up when the phone enters the proximity of the office.

The patent also describes a system that could change a user’s home screen depending on where the phone is, with work related apps front and foremost when in the office or games and TV apps when in the home.

The iPhone could also detect when it is in a car and automatically turn off some features that could be distracting, much like Apple’s CarPlay currently promises to do.

Apple follows suit

Google applied for a very similar patent in August 2013, which looked to lock down a device with a strong password when it was away from home or the office, but while in safe zones the Android device would revert back to a simple swipe to unlock.

Apps and services like Airpatrol’s ZoneDefense also offer similar location-based security policies, which open up or lock down devices based on their locations.

Some Android devices have also had the capability to turn on or off Wi-Fi depending on location for several years, but that location sensing does not yet extend to security settings.

A few Android smartphones, including Samsung’s Galaxy S5, can disable security when in the proximity of a Bluetooth device, such as Samsung’s Gear smartwatches or a pair of Bluetooth headphones, re-enabling a passcode lock when outside the 10m range of Bluetooth. This kind of pairing works well unless both devices are stolen at the same time.

It is possible that Apple could include more advanced location services, including device security settings as described in the patent within the next version of the iPhone software, iOS 8, which was shown off at WWDC in June. Both the next series of iPhones and the new iPads for 2014 are expected to include Apple’s Touch ID fingerprint sensor, although it is unknown how Touch ID will interact with any new location-based security.

• Apple iPhone 'kill switch' cuts thefts a Microsoft and Google to follow