Brett Molina

news

International Business Machines reported a second quarter boost in net income and revenue that topped Wall Street forecasts.

IBM reported non-GAAP diluted earnings per share of $4.32 off revenue of $24.4 billion. Both numbers beat analyst estimates compiled by Bloomberg.

Shares of IBM surged as high as 2.34% in after-hours trading before sliding. IBM stock is down 1.2%.

"In the second quarter, we made further progress on our transformation," said Ginni Rometty, IBM chairman and CEO, in a statement. "We performed well in our strategic imperatives around cloud, big data and analytics, security and mobile."

IBM made big news earlier this week after announcing an enterprise partnership with Apple. The tech giant will work with Apple on a series of business apps that will run on iPads and iPhones.

Like many PC companies, IBM is shifting more of its attention to the lucrative and mobile and cloud spaces. IBM says mobile revenues doubled during the second quarter, while revenue from cloud-based services jumped 50%.

Follow Brett Molina on Twitter: @brettmolina23.