Welcome to the world,Tristan and Sasha! Chris Hemsworth and Elsa Pataky reveal names of their twin boys as she tweets snap of tiny feet



Chris Hemsworth and Elsa Pataky have revealed the names of their newborn twin sons.



The 37-year-old actress announced the babies' names Tristin and Sasha alongside a sweet picture of their tiny feet via her Instagram account on Monday.



'They are already home,' she wrote alongside the snapshot. 'Tristan and Sasha arrived in this world 18 March just after the full moon. Complete happiness!!'

Scroll down for video



New babies! Elsa Pataky tweeted an adorable snapshot of her new sons little feet on Monday while announcing their names Tristan and Sasha



Chris and Elsa welcomed the babies on Friday at Cedars-Sinai Medical Center in West Hollywood.



And in a blog post written for Glamour Spain on Tuesday, Elsa recommends new mothers practice 'skin to skin' with their babies from birth, reveals she likes to dress her babies in 100 per cent organic cotton and favours the bugaboo range of pushchairs.



She adds: 'Enjoy every second with your baby because it is the most wonderful thing of the world.'



The couple, who have been married for three years, announced their second pregnancy in late November.



And in January, they happily confirmed the exciting news that they were expecting twins.

Family of five: Chris Hemsworth and Elsa Pataky, pictured at the Oscars ceremony earlier this month, are now parents to three children

While it's unclear exactly how far along the Spanish Fast & Furious 6 star was when she confirmed her pregnancy, it's not unusual for multiple births to be premature.

The two new additions to the family have joined big sister India Rose, 22 months.



The babies are the first Americans in the family, with Elsa giving birth to the couple's daughter

in London, where her Australian husband was filming at the time.

On February 22, the star enjoyed a baby shower at her LA home with A-list guests including Matt Damon and wife Luciana, and Vin Diesel and his long-time partner Paloma Jimenez , in attendance.

Ready to pop! The heavily-pregnant star - pictured here in Malibu on March 9 - enjoyed a baby shower at her home with A-list guests including Matt Damon and wife Luciana, and Vin Diesel on February 22







