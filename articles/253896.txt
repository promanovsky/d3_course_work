China, welcome to the world of PlayStation

Share this article: Share Tweet Share Share Share Email Share

Beijing - Sony will bring its PlayStation consoles to China through two joint ventures, the Japanese gaming giant and its Chinese partner both said on Monday, as they seek to tap a newly opened market. China in January formally authorised the domestic sale of game consoles made in its first free trade zone (FTZ) in Shanghai, opening up a market with an estimated 500 million players to foreign companies including Sony, Microsoft and Nintendo. Shanghai-listed tourism and culture firm Oriental Pearl said it would set up two joint ventures with Sony in the FTZ, one for hardware and one to handle software and services, according to a statement filed to the stock exchange. Sony will take a 49 percent stake in one venture and a majority 70 percent in the other, the statement said, to make and market PlayStation consoles and related software in China. Sony confirmed the announcement separately in an email to AFP.

Despite the news, Oriental Pearl stock closed down 0.54 percent on Monday. Sony shares ended up 3.12 percent in Tokyo trading.

The ventures will help introduce “quality and healthy” gaming products to Chinese players, the Oriental Pearl statement said, one of the requirements of authorities for selling in China.

Some foreign industry officials fear that such regulatory approval - conceivably to censor game content which China deems too violent, obscene or politically sensitive - could be used as a potential trade barrier.

Competitor Microsoft said in late April that it will start offering its Xbox One game console in China from September through a joint venture in the FTZ.

The relaxation of the decade-long sales ban does not apply to console imports, though the devices are already widely available through unofficial sales channels after being smuggled into China.

China's game revenue jumped 38 percent year-on-year to 83.2-billion yuan in 2013, according to one industry estimate, although the market was dominated by online computer games.

Analysts say Chinese consumers are unlikely to want to pay high prices for foreign consoles and authorised software, especially if it is slow coming to the market. - AFP