For years, people have depended on Starbucks to be a place where they can get energized for the day ahead, or receive an extra boost when motivation is waning. Soon, customers will also be able to refuel their smartphones, all thanks to the installation of innovative wireless phone chargers.

The new addition is the result of a partnership between Starbucks and Duracell Powermat. As you might expect if you’ve ever visited a Starbucks before, these are not just any wireless phone chargers. Impressively, customers will be able to simply place their phones on designated spots built into tabletops. There’s no need to even plug anything in. What’s more, the wireless phone chargers are free to use.

The gadget will begin being rolled out in the San Francisco Bay area. However, over the next three years, the cool wireless phone chargers will be placed in 7,500 Starbucks-owned stores in the United States. In total, that means there will be about a dozen per store, so you shouldn’t have to fight too hard to get the chance to power up your phone.

As quoted in an article from USA Today, chief digital officer Adam Brotman, said, “Starbucks believes this is another step in staying ahead of the curve when it comes to in-store technology.” That makes sense, especially because Starbucks has been showing an interest in how to keep customers happy and encourage them to want to linger.

In addition to adding the wireless phone chargers, Starbucks also recently announced plans to expand a pilot program which saw alcohol being served during evening hours in particular markets. Now, if everything goes as expected, visitors can get a latte, potentially order a beer, and most importantly, keep their phones powered up with help from the snazzy wireless phone chargers.

Almost everyone who owns a smartphone has experienced the intense frustration of trying to make a call or send a text message, only to find that the battery life is nearly depleted. Now, thanks to the installation of the wireless phone chargers, patrons can fix that problem, even if they’ve left their charging cables at home.

Keep in mind that in order to use the wireless phone chargers, your phone must be compatible with that type of wireless technology. Fortunately, even if it isn’t, there are some supplemental battery packs that allow popular gadgets to adapt. Starbucks has said it may begin selling those accessories, at least until wireless charging capabilities become more commonplace in phones.

[Image Credit: idownloadblog.com]