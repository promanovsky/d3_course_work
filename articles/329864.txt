Los Angeles billionaire-inventor Alfred Mann’s almost decade-long quest to develop an inhalable form of insulin for diabetics won approval Friday from U.S. regulators.

His company, MannKind Corp. of Valencia, got the OK on Friday to sell the drug called Afrezza, although regulators warned the product shouldn’t be used by those diabetics with asthma or a serious lung disease.

The Food and Drug Administration said it cleared Afrezza for Type 1 and Type 2 diabetes. The drug is a powder that is inhaled. It would be most often used to help control blood-sugar levels at mealtime, a quick puff replacing an injection before a meal.

The drug labeling will warn that spasms in the airways of the lung have been seen in patients with asthma and chronic obstructive pulmonary disease and will advise against smokers using the medicine, the agency said in a statement.

Advertisement

It’s been a long, expensive journey to get Afrezza to this stage. MannKind has spent about $1.8 billion developing the drug, with about $975 million of that coming from Mann’s personal wealth. It took almost eight years seeking approval of the diabetes therapy since starting late-stage clinical trials.

Mannkind’s share price has ranged from a high of $21.70 to a low of $1.60 as Pfizer Inc. pulled the only inhaled insulin from the market and the FDA twice rejected MannKind’s therapy, most recently in 2011, after the company switched inhalers during the review process.

Follow @wjhenn for L.A. business news.