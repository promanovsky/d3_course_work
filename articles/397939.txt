Running for as little as a few minutes a day significantly reduces one's risk of dying from heart disease when compared with someone who does not run at all.

Exercise is a known way to prevent heart disease, but researchers were unsure if activity below the level of 75 minutes per week made a difference, the American College of Cardiology reported. The findings were published in the Journal of the American College of Cardiology.

The research team looked at 55,137 people between the ages of 15 and 100 over a 15-year period. Participants were asked to fill out a questionnaire about their running habits. Over the course of the study 1,217 participants died from cardiovascular disease.

Compared with non-runners the people who ran were found to have a 30 percent lower risk of death from all causes and a 45 percent reduced risk of death from cardiovascular disease. The runners also lived an average of three years longer than non-runners.

Patients who ran for less than 51 minutes, only once or twice a week, or fewer than six miles had a lower chance of dying than those who did not run at all. Runners who ran less than an hour per week had the same mortality benefits as those who ran more than three.

"Since time is one of the strongest barriers to participate in physical activity, the study may motivate more people to start running and continue to run as an attainable health goal for mortality benefits," said DC (Duck-chul) Lee, Ph.D., lead author of the study and an assistant professor in the Iowa State University Kinesiology Department. "Running may be a better exercise option than more moderate intensity exercises for healthy but sedentary people since it produces similar, if not greater, mortality benefits in five to 10 minutes compared to the 15 to 20 minutes per day of moderate intensity activity that many find too time consuming."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.