Ebola virus disease is a severe and often fatal illness. There is no specific treatment and outbreaks have a mortality rate of up to 90%.

Ebola virus disease is a severe and often fatal illness. There is no specific treatment and outbreaks have a mortality rate of up to 90%....

IT’S running rampant across west Africa and has already claimed nearly 500 lives. As the UN struggles to contain the deadliest ever outbreak of Ebola, the question on everyone’s lips is: Are we next?

A sense of panic is beginning to pervade Guinea, the epicentre of the outbreak, and neighbouring Sierra Leone and Liberia.

The death toll from the deadly haemorrhagic virus now stands at 467. There been a 38 per cent rise in the number of deaths in just a week.

And now, there are growing fears Ebola could spread further than the small corner of west Africa reeling from the current outbreak.

AN ISOLATING DESPAIR: Dr William Fischer speaks of horror Ebola outbreak

So, is it possible cases of Ebola could begin turning up in Australia soon? Is there a chance it will spread through Europe before making its way to our shores?

If it does spread to the West it’s likely Paris will be the first city to see the virus according to infectious disease specialist Kamran Khan.

“We look at many outbreaks and decide what paths they’re going take,” he told NPR.

“The big question is whether sick people are going to get on a plane and spread the disease.”

In Guinea’s capital Conakry the international airport is not a central hub but there are some flights direct to the French capital.

“The volume of travel in the Conakry airport is low,” Dr Khan said.

“Most of the flights are local. But 10 per cent of the traffic goes to Paris.”

There is a chance someone could board the plane while unknowingly carrying the virus, which has an incubation period of up to three weeks and a fatality rate of 90 per cent.

But unlike other deadly outbreaks, such as the SARS virus and avian flu, transmission of Ebola involves exposure to bodily fluids including blood and other secretions.

World Health Organisation Regional Director for Africa Dr Luis Gomes Sambo said critical action was needed to stop the spread of the epidemic.

“The current trend of this epidemic and the potential of cross-border and international spread constitute a public health matter of grave concern,” he told an emergency summit in Ghana.

“The current Ebola outbreak has the potential to spread outside the affected countries and beyond the region if urgent and relevant containing measures are not put in place.”

He urged authorities to “leave no stone unturned” in the fight to contain the virus.

Pulmonary diseases and critical care specialist Dr William Fischer, who was working to contain the epidemic in Gueckedou, told News Corp it was tough to pick a worse spot for the outbreak to have occurred.

“One of the reasons is the location. Gueckedou is right on the border of Sierra Leone and Guinea. It’s tough to pick a worse place, virus and illness crosses borders easily,” he said.

“The resources and collaboration don’t follow as easily.”

He said MSF and the WHO were working together with officials from all three countries to improve treatment and prevention.

“It is absolutely controllable it is not difficult to break the chain of transmission if you have the right resources and people are buying in and participating.”

He said there was a great deal of fear around the disease, but that he didn’t expect it would spread to the west.

“It’s not as scary as we think it is,” he said.

The United Nations today moved to reassure west Africa that the deadly epidemic could be stopped, telling the region’s health ministers: “We can handle this.”

“These kinds of outbreaks, these diseases, can be stopped,” Keiji Fukuda, assistant director-general for health security at the WHO, told AFP, as 11 west African health ministers gathered for a two-day conference in Accra.

“This is not a unique situation — we have faced it many times — so I’m quite confident that we can handle this.

“This is, however, the most complicated Ebola outbreak ever because it is spreading so fast in both urban and rural areas.”