Reality TV star Kourtney Kardashian's husband Scott Disick has reportedly left their house and checked into a nearby motel.

The 35-year-old star, who is expecting her third child with fiance Disick, 31, was spotted fighting with him inside their rental home, reported Radar Online.

Kourtney and sister Khloe Kardashian, 29 are currently filming spinoff reality series, 'Kourtney & Khloe Take The Hamptons' at a rented accommodation.