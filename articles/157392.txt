Joan Rivers' joke a 'new low' says Ariel Castro victims

Joan Rivers says she won't apologize to Amanda Berry and Gina DeJesus for remarks she made on the 'Today' show. Rivers shocked the show's hosts when she compared the girls' captivity in Ariel Castro's home to living in her daughter's guest room.