The late actress' cranky character managed to make the horrible hilarious.

No one could put 30 Rock's Jack Donaghy in his place quite like mom.

Elaine Stritch, who played Alec Baldwin's irascible onscreen mother Colleen Donaghy in nine episodes, died Thursday at the age of 89 in her home in Birmingham, Mich.

PHOTOS Hollywood's Notable Deaths of 2014

Here, The Hollywood Reporter looks at Stritch's 10 best (and most acid-tongued) one-liners from 30 Rock.

“Haitus” Season 1 Episode 21

— “Tell him his mother’s here, and she loves him, but not in a queer way.”

“Ludachristmas” Season 2 Episode 9

— "Nice? I’ll show you nice. Let’s all meet down at the soda shop while this country turns into Mexico. "

— "You give me ten minutes with the Lemon family, and I’ll have them tearing at each other like drag queens at a wig sale."

PHOTOS Hollywood's 100 Favorite Films

“The Moms” Season 4 Episode 20

— "Oh, for crying out loud, Liz. You see, that’s what feminism does. It makes smart girls with nice birthing shapes believe in fairy tales. Stop waiting for your prince, Liz. "

— "Two women, Jack! At the same time. What are you, Italian?"

“Christmas Attack Zone” Season 5 Episode 10

— "I see you brought the bag that my bastard grandchild will come in."

— "Is she drunk Jack? Because you know when you’re pregnant, one bottle of wine a day, and that’s it!"

STORY Broadway Legend Elaine Stritch Dies at 89

“My Life is Thunder” Season 7 Episode 8

— "I have a few things I want to say to you before I meet the Grim Reaper, who is black, I assume, what with the hoodies he wears."

— "I was watching TV, and they started interviewing an Asian Santa Claus, and my arm went numb. "

— "Don’t talk to me like that Jack. I breastfed you for nine years!"

What were your favorite Stritch one-liners? Let us know in the comments.