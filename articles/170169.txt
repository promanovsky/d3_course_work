Young girls who have been called “too fat” are more likely to be obese as young adults, according to a new research letter.

The early stigma of being labeled that way may worsen the problem rather than encouraging girls to become healthier, but more research is needed to be sure, the study authors say.

“This study is one step closer to being able to draw that conclusion, but of course we can't definitively say that calling a girl "too fat" will make her obese,” said senior author A. Janet Tomiyama of the University of California, Los Angeles.

“This study recruited girls when they were age 10 and followed them over nine years, so we know it's more than just a one-time connection, which makes me believe that it's an important question to continue researching,” Tomiyama told Reuters Health in an email.

She and her coauthor examined data from an existing study that followed girls through their teen years. At age 10, the girls answered the question, “have any of these people told you that you were too fat: father, mother, brother, sister, best girlfriend, boy you like best, any other girl, any other boy, or teacher?”

Out of just over 2,000 girls, a total of 1,188 answered “yes” to any of the choices.

Those girls were more likely to have a body mass index (BMI) – a measure of weight relative to height - in the obese range ten years later than girls who answered “no,” according to the results in JAMA Pediatrics.

“We know from considerable evidence that youth who feel stigmatized or shamed about their weight are vulnerable to a range of negative psychological and physical health consequences,” said Rebecca Puhl, deputy director of the Rudd Center for Food Policy & Obesity at Yale University in New Haven, Connecticut.

“This study suggests that negative weight labels may contribute to these experiences and have a lasting and potentially damaging impact for girls,” said Puhl, who was not part of the study.

Girls who had been labeled “fat” were still at higher risk of obesity even when researchers accounted for their BMIs at age 10, household income, race and parental education level.

The effect seemed to be strongest when the labels came from family members, which increased the risk of obesity later by 60 percent, compared to 40 percent when the comments came from friends or teachers. But it’s not wise to make too much out of the difference between those numbers, since this was only an exploratory study, Tomiyama said.

She was not at all surprised that over half of girls had been labeled “fat.”

“The pressure to be thin in our society is intense, and other research shows that people label both themselves and others as 'overweight' even if their objective body mass index is in the 'normal weight' range,” she said.

Females are exposed to weight stigma more often, but the connection may be present for boys as well, she noted.

There are ways for parents to address weight and health issues with their children that don’t involve labeling, Tomiyama said.

“There's no need to say the ‘f’ word at all if you want to improve your child's health,” she said.

Parents could instead focus on the health of the family as a whole, said Angelina Sutin, who was not involved in the new study.

Sutin studies psychological wellbeing and health disparities at Florida State University College Of Medicine in Tallahassee.

“The best approach would be to start kids early on a path toward healthy living by eating healthy food and being physically active,” Sutin told Reuters Health in an email.

“This applies equally to parents as it does to kids – children model their parents’ behavior, so if kids see their parents making healthy choices, they are more likely to also make healthy choices,” she said.

Parents could identify activities the child enjoys and work on ways to do more of them, she added.

“I think the focus of the conversation needs to change,” Tomiyama said. “Right now, we have a laser focus on weight instead of health, but many studies show that weight is a really imprecise indicator of actual health.”

“Parents can talk to their child about adopting healthy behaviors without once mentioning weight,” she said.