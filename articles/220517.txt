Microsoft and SAP today announced a partnership which will let Microsoft's cloud compete more heavily with Amazon in the coveted enterprise cloud computing market.

SAP's popular enterprise apps will soon be available on Microsoft's cloud, Azure. This includes its popular financial software, enterprise resource planning (ERP) software, SAP's mobile app development software and a version of its super speedy database, SAP HANA.

Advertisement

In theory, this means that a company that runs SAP's software in Microsoft's cloud can get all kinds of fancy reports on how their business is doing, delivered to a Windows phone, PC or tablet, with a few clicks of a mouse.

Advertisement

This could help Microsoft and SAP compete with a new crop of cloud-based startups trying offering similar mobile business analysis, including Anaplan (which just raised $100 million) and Tidemark , both run by former SAP execs.

The SAP apps will be available on Azure in June. The integration between SAP's apps and Microsoft's cloud version of Office, Office 365, will be available by the end of this year, Microsoft says.

Disclosure: Jeff Bezos is an investor in Business Insider through his personal investment company Bezos Expeditions.