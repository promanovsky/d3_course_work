Nike has denied recent reports that it will cease to produce technology-focused fitness hardware.

On Friday (April 18), it was claimed that the sports apparel company would lay off the majority of staff behind its Nike+ FuelBand SE wrist device.

Nike



In a statement to Re/code, Nike said there would be a "small number" of layoffs but it would continue to make fitness hardware.

The company explained: "The Nike+ FuelBand SE remains an important part of our business.

"We will continue to improve the Nike+ FuelBand App, launch new METALUXE colors, and we will sell and support the Nike+ FuelBand SE for the foreseeable future."

However, sources close to the publication claim that a decision over the FuelBand's future has been the subject of debate at Nike for several months due to high expenses, manufacturing challenges and a struggle to reach adequate business margins.

Apple CEO Tim Cook sits on the Nike board, leading to speculation that the companies could be working together on a wearable device.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io