Lindsay Lohan’s rumoured to have an account. All manner of Made In Chelsea types are out there for the swiping. Even Ashton Kutcher is thought to have used the dating app after splitting up with Demi Moore and getting together with his That 70s Show co-star Mila Kunis.

But Tinder poses grave problems for some famous people hoping to find potential hook-ups via their mobile phones. So much so, that many have complained to the company that they often have difficulty convincing potential partners that they are who they say they are.

“We’ve had celebrities reach out to us frequently throughout the last year, sort of calling out various frustrations convincing users that they were actually who they are,” co-founder Sean Rad, who first received the gripes two months after it launched in Autumn 2012, told Time magazine.

“One impediment is that sometimes their Facebook accounts, which we pull information from, includes different names than their actual likeness… So [celebrities] were asking for the ability to modify their Tinder name and maybe have a verified badge.”

He stopped short of listing exactly who these grumbling celebrities where, but reassured Time: “These are A-listers.”

But it’s not just the Hollywood ‘elite’ who have encountered problems identifying themselves. When Rad and his co-founder Justin Mateen gave Tinder a go, they too found it difficult to convince their matches that they invented the whole thing.

“No one believed us, they thought it was lying and say ‘funny joke loser,’” Mateen has been quoted as saying.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Mateen himself wants a verification badge, which may also be given to recognisable figures in other sectors than showbusiness, but, according to Rad, “we haven’t decided if he qualifies yet; his account is in review,”

“This isn’t something that we are going to loosely hand out,” he added.

Still, like many of us, he takes comfort in the fact that the famous people are on Tinder in the first place. Not just because their endorsement will probably inspire thousands of apprehensive singletons to sign up, but also because it proves that the successful and good-looking face much the same romantic trials as we do.

“It was awesome [to know celebrities are on Tinder] because it sort of validated our theories that everyone, even people of influence, need help forming relationships,” Rad added.