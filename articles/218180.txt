View video

The Solange vs. Jay-Z elevator slap-fest? That was all just about a spider, according to the wags at “Saturday Night Live.”

“It tells a completely different story,” “Solange,” played by Sasheer Zamata, “explains” of what she claims is the actual audio for the elevator surveillance video.

“Oh my God, there’s a spider on you!” Zamata shouts in the faux audio, as she starts slapping Jay Pharoah, who’s playing Jay Z.

“What?” Pharoah shouts back. “Get it! You know I hate spiders.”

The clip, a highlight of Saturday’s season finale, also included a cameo of show veteran Maya Rudolph doing a dead-on parody of Beyoncé.