CNET

Apple plans to start production on its long-rumored big-screen iPad early next year, according to a new report.

Suppliers of the Cupertino, Calif., electronics giant should start building the new 12.9-inch tablet by the first quarter, Bloomberg reported. The publication, citing anonymous sources, said Apple "has been working with suppliers for at least a year to develop a new range of larger touch-screen devices."

Apple has long been rumored to be working on a larger screen "iPad Pro" for release later this year or next year. That could help attract users of Apple's Macintosh computers, as well as boost its sales of its tablet line. It also would accompany Apple's new bigger screen iPhones, which the company is expected to unveil September 9.

Apple declined to comment.

The iPhone and iPad account for about two-thirds of Apple's revenue. But Apple's tablet has struggled over the past couple quarters. Sales of the tablet fell short of analyst expectations in the fiscal second and third quarters, which Apple blamed both on the number of devices held in channel inventory (which means it's either sitting in stores or on trucks) and weaker demand. It sold 13.3 million iPads in the most recent quarter, down 9 percent from a year ago. Analysts expected the company to sell about 14.4 million iPads.

Apple likely has been hurt by a few factors that could continue to plague iPad sales. It's easy for people to pass older tablets to relatives or friends when they upgrade. People also don't have the two-year upgrade incentive that smartphones get from wireless carriers, and Apple hasn't made big enough changes to the iPad to compel even their most ardent fans to immediately buy the newest model. In addition, most people who crave a tablet likely already have one, and Apple is going up against dozens of new, inexpensive devices that run Google's rival Android mobile operating system.

But Apple CEO Tim Cook has said he's "bullish" about Apple's prospects in the tablet market. "We still feel the category as a whole is in its early days, and there's still significant innovation that can be brought to the iPad and we can do that," he said during the company's earnings call in July. Cook expects Apple's recently announced partnership with IBM to also help iPad sales. The two companies will work together on pushing Apple devices and iOS apps with business users.