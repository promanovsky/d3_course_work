Growth in euro zone factory activity eases

Share this article: Share Tweet Share Share Share Email Share

London - Euro zone manufacturing growth slowed more than initially thought last month, a business survey showed on Monday, likely fuelling expectations that the European Central Bank will ease policy this week. Markit's final Manufacturing Purchasing Managers' Index (PMI) slipped to a six-month low of 52.2 in May from 53.4 in April as strong figures from Germany failed to offset a contraction in activity in France. The final figure was below the initial reading of 52.5 but held above the 50 mark that separates growth from contraction for the 11th straight month. A subindex measuring output fell to 54.3 from 56.5, weaker than the initial reading of 54.7. “The May drop in the manufacturing PMI will inevitably add to the clamour for policymakers to provide a renewed, substantial boost to the region's economy and ward off the threat of deflation,” said Chris Williamson, Markit's chief economist.

To spur growth and boost lending, the ECB is widely expected to cut its deposit rate to below zero, reduce its main borrowing rate and launch a refinancing operation aimed at businesses when it meets on Thursday.

Still, factories increased prices marginally last month. The output price index rose to 50.3 from 49.2.

Inflation in the 18 nations using the euro is predicted to have held steady at just 0.7 percent in May, well within the ECB's “danger zone” of below 1 percent and also below its preferred 2 percent ceiling.

The tepid overall growth was again supported by Germany, the euro zone's largest economy.

But in France, the second-largest, the PMI sank back below the 50 mark after just two months of expansion. - Reuters