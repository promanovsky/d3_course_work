Washington: Inspired by natural materials like bone, engineers from MIT were able to coax bacterial cells to produce biofilms, which can incorporate nonliving materials, such as gold nanoparticles and quantum dots.

The paper`s lead author is Allen Chen, an MIT-Harvard MD-PhD student. Other authors are postdocs Zhengtao Deng, Amanda Billings, Urartu Seker, and Bijan Zakeri; recent MIT graduate Michelle Lu; and graduate student Robert Citorik, Timothy Lu , an assistant professor of electrical engineering and biological engineering.

Lu and his colleagues chose to work with the bacterium E. coli because it naturally produces biofilms that contain so-called "curli fibers" - amyloid proteins that help E. coli attach to surfaces.

First, the MIT team disabled the bacterial cells` natural ability to produce CsgA, then replaced it with an engineered genetic circuit that produces CsgA but only under certain conditions - specifically, when a molecule called AHL is present.

This puts control of curli fiber production in the hands of the researchers, who can adjust the amount of AHL in the cells` environment. When AHL is present, the cells secrete CsgA, which forms curli fibers that coalesce into a biofilm, coating the surface where the bacteria are growing.

The researchers then engineered E. coli cells to produce CsgA tagged with peptides composed of clusters of the amino acid histidine, but only when a molecule called aTc is present. The two types of engineered cells can be grown together in a colony, allowing researchers to control the material composition of the biofilm by varying the amounts of AHL and aTc in the environment.

The paper has been published in the journal Nature Materials.