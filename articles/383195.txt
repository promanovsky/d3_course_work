Bank of America said Wednesday that its second quarter earnings were hit by higher litigation expenses.

The bank, based in North Carolina, earned $2 billion in the second quarter after payments to preferred shareholders, compared with $3.6 billion in the same period a year earlier.

Per share, that worked out to 19 cents, compared with 32 cents a year ago.

The bank's litigation costs of $4 billion crimped earnings by 22 cents a share.

The bank also said that it had reached a settlement Tuesday with American International Group Inc. to resolve all outstanding residential mortgage-backed securities litigation between the two companies.

Revenue fell 4 percent to $21.9 billion from $22.9 billion.

Get the Biz Briefing newsletter! The latest LI business news in your inbox Monday through Friday. By clicking Sign up, you agree to our privacy policy.

Bank of America's stock was little changed at $15.91 premarket trading.