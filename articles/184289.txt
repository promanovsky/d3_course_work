The United Nations' World Health Organization (WHO) said Monday that the US IHR National Focal Point has informed it of the first laboratory-confirmed case of human infection with Middle East respiratory syndrome (MERS-CoV) in the country.

According to the UN agency, the patient is a male US citizen in his 60s, who lives and works in Riyadh in Saudi Arabia. He traveled to the US from Riyadh to Chicago on April 24 via London Heathrow with travel from Chicago to Indiana by bus.

US health officials do not know at this time whether he directly cared for MERS-CoV confirmed cases and what infection control precautions were used. The CDC Team is working with the Indiana State Health Department to collect more data on the patient, including this information.

WHO said the patient began feeling unwell on or around April 14 with a low-grade fever without any respiratory symptoms. He then developed shortness of breath, cough, increasing fever, and mild runny nose on April 27.

A chest x-ray taken later showed infiltrates in the right lung base. He was then admitted to hospital and placed in a private room. Negative pressure room and airborne precautions were reportedly implemented on April 29 and full isolation (standard, contact, and airborne) precautions were implemented on April 30. A chest computed tomography on April 29 showed bilateral lung infiltrates. Currently the patient is stable with shortness of breath; he is not intubated.

CDC's Division of Global Migration and Quarantine (DGMQ) has been and continues to work with local, state, and international partners, as well as airlines and the bus company to obtain the passenger manifests from the two flights and information from the bus company to help identify, locate, and interview contacts.

This is the first report of an imported case of MERS-CoV in the United States and in the Americas Region.

Coronaviruses are a family of viruses that can cause a range of ailments from the common cold to severe acute respiratory syndrome (SARS), which became an epidemic in 2003. The virus could be transmitted between people in close and prolonged contact. The sources of infection for the new coronavirus are still unclear.

The deadly Novel Coronavirus (NCoV) strain, recently renamed MERS-CoV, reflects the fact that most of the reported cases are from that region, mainly Saudi Arabia.

Nevertheless, France, Germany, Italy, Tunisia and the United Kingdom have also reported laboratory-confirmed cases of MERS-CoV infections. Those patients were either transferred there for care of the disease or returned from the Middle East and subsequently became ill.

For comments and feedback contact: editorial@rttnews.com

Market Analysis