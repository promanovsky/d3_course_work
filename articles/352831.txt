‘West Africa lacks resources to battle Ebola’

Share this article: Share Tweet Share Share Share Email Share

Accra - West African states lack the resources to battle the world's worst outbreak of Ebola and deep cultural suspicions about the disease remain a big obstacle to halting its spread, ministers said on Wednesday. The outbreak has killed 467 people in Guinea, Liberia and Sierra Leone since February, making it the largest and deadliest ever, according to the World Health Organisation (WHO). West African health ministers meeting in Ghana to draw up a regional response mixed appeals for cash with warnings of the practices that have allowed the disease to spread across borders and into cities. Abubakarr Fofanah, deputy health minister for Sierra Leone, a country with one of the world's weakest health systems, said cash was needed for drugs, basic protective gear and staff pay.

Sierra Leone announced on Wednesday that President Ernest Bai Koroma, his vice-president and all cabinet ministers would donate half of their salaries to help fight the outbreak, though the total amount of the donations was not disclosed.

“In Liberia, our biggest challenge is denial, fear and panic. Our people are very much afraid of the disease,” Bernice Dahn, Liberia's deputy health minister, told Reuters on the sidelines of the Accra meeting.

“People are afraid but do not believe that the disease exists and because of that people get sick and the community members hide them and bury them, against all the norms we have put in place,” she said.

Authorities are trying to stop relatives of Ebola victims from giving them traditional funerals, which often involve the manual washing of the body, out of fear of spreading the infection. The dead are instead meant to be buried by health staff wearing protective gear.

Neighbouring Sierra Leone faces many of the same problems, with dozens of those infected evading treatment, complicating efforts to trace cases.

The Red Cross in Guinea said it had been forced to temporarily suspend some operations in the country's south-east after staff working on Ebola were threatened.

“Locals wielding knives surrounded a marked Red Cross vehicle,” a Red Cross official said, asking not to be named. The official said operations had been halted for safety reasons. The Red Cross later said only international staff were removed.

A Medecins Sans Frontieres (Doctors Without Borders) centre in Guinea was attacked by youths in April after staff were accused of bringing the disease into the country.

Ebola causes fever, vomiting, bleeding and diarrhoea and kills up to 90 percent of those it infects. Highly contagious, it is transmitted through contact with blood or other fluids.

The WHO has flagged three main factors driving its spread: the burial of victims in accordance with tradition, the dense populations around the capital cities of Guinea and Liberia and the bustling cross-border trade across the region.

Health experts say the top priority must be containing Ebola with basic infection control measures such as vigilant handwashing and hygiene, and isolation of infected patients.

Jeremy Farrar, a professor of tropical medicine and director of The Wellcome Trust, an influential global health charity, said people at high risk should also be offered experimental medicines, despite the drugs not having been fully tested.

“We have more than 450 deaths so far, and not a single individual has been offered anything beyond tepid sponging and 'we'll bury you nicely',” Farrar told Reuters in an interview. “It's just unacceptable.” - Reuters