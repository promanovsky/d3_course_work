AbbVie boss increased its offer to Shire to £30billion after its boss talked with UK shareholders [GETTY]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Make the most of your money by signing up to our newsletter fornow

Illinois-based AbbVie said it hoped its new 5115p-a-share offer would bring Shire to the negotiating table. AbbVie has already had three offers turned down, with the last worth £27billion, because it undervalued Shire and its prospects. Shire also has concerns about AbbVie making the move for tax purposes.

I believe Shire investors are generally supportive of this transaction. I urge them to push the Shire board to engage in talks

AbbVie chief executive Rick Gonzalez, who met with Shire investors in London recently to reiterate its case for a deal, said the feedback had helped it shape the new offer.

“I believe Shire investors are generally supportive of this transaction. I urge them to push the Shire board to engage in talks,” he said. He did not rule out going hostile if Shire refused.

In a statement Shire said its board would meet to consider the bid and that it will make a further announcement in due course. Its shares dropped 121p to 4530p.

Analysts said the sweetened offer improved AbbVie’s chances of getting Shire into discussions but was unlikely to be a “knockout price”.

AbbVie has to make a firm offer by July 18 or walk away.