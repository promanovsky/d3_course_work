Shutterstock photo

Investing.com -

Investing.com - The dollar edged lower against the other major currencies on Monday after data showed that U.S. pending home sales fell in June but losses were limited ahead of a series of key economic events later in the week.

EUR/USD edged up 0.06% to 1.3437, hovering just above Friday's eight month lows of 1.3420.

The dollar dipped after the National Association of Realtors reported that U.S. pending home sales fell 1.1% last month, disappointing expectations for a 0.5% gain. The weak data pointed to underlying weakness in the housing sector.

Demand for the dollar continued to be underpinned ahead of the latest U.S. employment report later in the week and the upcoming Federal Reserve statement on Wednesday.

Investors were also awaiting final data on U.S. second-quarter growth on Wednesday.

Earlier this month Fed Chair Janet Yellen said that rates could rise sooner if the recovery in the labor market continued.

The euro remained under heavy selling pressure amid concerns over the divergence in monetary policy between the European Central Bank and its major peers.

USD/JPY edged down 0.04% to 101.78, while USD/CHF slid 0.10% to 0.9037 following the release of the housing data.

Sterling pushed higher, with GBP/USD rising 0.14% to 1.6999, recovering from Friday's one month lows of 1.6960.

The New Zealand dollar pared losses, with NZD/USD at 0.8550, off session lows of 0.8530, while USD/CAD eased 0.09% to 1.0803. The Australian dollar edged higher, with AUD/USD rising 0.10% to 0.9402.

The US Dollar Index, which tracks the performance of the greenback versus a basket of six other major currencies, slid 0.09% to 81.08, off Friday's six month highs of 81.20.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.