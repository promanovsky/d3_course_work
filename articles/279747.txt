Actor and comedian Tracy Morgan is showing signs of improvement following a highway crash that badly injured him and killed one of his friends, his spokesman said Thursday.

Morgan, a former star of "Saturday Night Live" and "30 Rock," remained hospitalized in critical but stable condition on Thursday but his medical team is "optimistic that his recovery is progressing," according to spokesman Lewis Kay.

"Today was a better day," Kay said.

He added that Morgan's fiancee remained by his side and was relaying everyone's prayers and good wishes.

Morgan, 45, suffered broken ribs and a broken leg in the Saturday morning crash on the New Jersey Turnpike. The crash killed fellow comedian James McNair and seriously injured two other people in Morgan's SUV limo. The group had been returning to New York after performing at a Delaware comedy club Friday evening.

Prosecutors say Wal-Mart driver Kevin Roper's truck hit the vehicle. The Jonesboro, Ga., resident has pleaded not guilty to death by auto and assault by auto charges. A criminal complaint alleges Roper hadn't slept for more than 24 hours before the accident when he swerved to avoid slowed traffic on the turnpike and plowed into Morgan's SUV limo.

Wal-Mart Stores Inc., based in Bentonville, Ark., has not explained what Roper's driving route was. The company has said it believes Roper was in compliance with federal safety regulations.

Morgan's assistant, Jeffrey Millea, of Shelton, Conn., and comedian Ardie Fuqua Jr., of Jersey City, N.J., were also injured in the crash.

Kay said Millea's wife had reported that her husband "has also shown much improvement over the past few days" and that she was very thankful for the love and support their family has received.