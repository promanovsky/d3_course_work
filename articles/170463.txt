Women with breast cancer receiving chemotherapy may find themselves encountering an unexpected side effect, researchers say: long-term unemployment.

In a University of Michigan study, researchers followed more than 2,000 women in Detroit and in Los Angeles who had received a diagnosis of breast cancer from 2005 to 2007.

Speaking with almost 1,500 of them 4 years later, they found of the 76 percent who were employed prior to being diagnosed, 38 percent were jobless at four years after the diagnosis.

That was higher than the 27 percent job loss rate of the women who had not undergone chemo, they found.

Many of those in the study said they had hoped to continue working but found themselves significantly more likely to experience a loss of employment and financial hardship.

Although it is generally against the law to fire an employee over an illness, many women -- and doctors -- underestimate how debilitating a long-term course of chemotherapy can be, both physically and financially, the researchers said.

"Many doctors believe that even though patients may miss work during treatment, they will 'bounce back' in the longer term," Michigan researcher Reshma Jagsi said.

"The results of this study suggest otherwise. Loss of employment is a possible long-term negative consequence of chemotherapy that may not have been fully appreciated to date."

With many patients taking time off from their jobs because of the initial side effects of the chemotherapy, there is a strong possibility of that continuing into long-term unemployment, the researchers said.

There is also the possibility of ongoing side effects from chemotherapy treatments such as cognitive or neuropathy issues that could impact job prospects, they said.

The findings suggest better strategies are needed to determine which patients might garner little benefit as a result of such treatments and might best be served by avoiding them, Jagsi said.

"We also need to ensure that patients who are deciding on whether to receive chemotherapy understand the potential long-term consequences of receiving treatment, including possible implications for their employment and financial outcomes," she said.

Breast cancer experts say they are not surprised by the study's findings.

"For the vast majority of patients, side effects are manageable and they can improve after, but some patients don't feel fully functional for the long term," says Dr. Jennifer Litton, a breast oncologist based in Houston. "I sometimes have to advocate for long-term disability on behalf of my patients."

"So I think it's really good that the study looked at employment as long as four years after diagnosis."

Almost a quarter of a million American are diagnosed with breast cancer each year, and more than 40,000 are predicted to die from the disease this year, the American Cancer Society says.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.