Facebook plans to bring Internet to the third-world via drones, satellites, lasers, and more. Today Mark Zuckerberg unveiled Facebook’s Connectivity Lab which will work on the Internet.org project. It’s powered by talent acqhired from solar-powered drone maker Ascenta as well as poached from NASA.

Internet.org, a partnership between Facebook and telecom industry giants like Nokia and Qualcomm, hopes to use these air- and space-born methods to bring Internet to the 5 billion people who currently lack it. Zuckerberg says that Internet.org and Facebook will work on inventing new technologies to complete the mission.

While they both have somewhat altruistic objectives, Facebook’s Connectivity Lab could compete with Google’s Project Loon, which uses huge helium balloon vessels to beam Internet to the developing world.

To bolster its talent in aerospace engineering, Facebook has acqhired the five-member team of UK-based startup Ascenta, whose members previously worked at QinetiQ, Boeing, Honeywell and the Harris Corporation. There they tackled on projects including the Breitling Orbiter and prototypes of the Zephyr, the longest-flying solar powered drone. Members of NASA’s Jet Propulsion Laboratory, NASA’s Ames Research Center, and the National Optical Astronomy Observatory have also come aboard the Facebook project.

Here’s a video explaining some plans for the Connectivity Lab:

Internet.org will use different vehicles to deliver Internet to different types of locals. In suburban areas it will use “solar-powered high altitude, long endurance aircraft” that can stay in the air for month, are easily deployed, and can provide reliable Internet connectivity. Less populated areas will be served by low-Earth orbit and geosynchronous satellites.

Both will employ “Free-space optical communication”, or FSO, which uses invisible, infrared laser beams to transmit data using light. Note that Facebook made no mention of Titan Aerospace, the drone-maker it was said to be in advanced acquisition talks with a few weeks ago.

Some will immediately think the idea of Facebook controlling a fleet of web-ready aircraft and satellites is a bit creepy. Other will assume the point is to get more people online so they can become Facebook users. Neither are necessarily wrong. But when I spoke to Mark Zuckerberg at an event at Facebook headquarters last year, he seemed earnestly adamant about the potential for Internet.org to empower the world through access to the web. In his Facebook post today, Zuckerberg explains: