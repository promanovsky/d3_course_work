London: The rare alignment of Sun, Mars and Earth, called "an opposition", which will happen today, has reawakened some `end of the world` believes since it is occurring a week before the sighting of the first of four dark red `blood moons` lunar eclipses.

According to the King James Bible, when the "sun turns dark" and the "moon turns red", it will be the second coming of Christ that represents the End of Days.

Confirming the news, NASA`s rep said that a highly unusual `Tetrad` will happen a week from today and finish on September 28 2015.

Pastor John Hagee, who has written a book on the phenomenon, believes that tonight marks the dawn of a `hugely significant event` for the world and it has only happened three times in over 500 years.

The first had happened when the Jews were expelled by the Catholic Spanish Inquisition, second when the State of Israel was established and last when happened around the Arab-Israeli war.

Hagee explained that according to the Biblical prophecy, world history is about to change dramatically and to get four blood moons you need something absolutely extraordinary in astrological terms.

Hagee added that the he first of the four blood moons will come on April 15 this year, second on October 8, third on April 4 2015, and then the last one on September 28.