GoPro Inc. (GPRO) reported second quarter non-GAAP EPS of $0.08 after the close Thursday, compared to the loss of $0.03 in the prior year. Revenue came in at $244.6 million, up 38.1% from $177.1 million last year. The consensus estimates were for EPS of $0.06 and revenues of $237.73 million. The stock is now down 3.72 on 1.5 million shares.

GoPro posted gains around midday Thursday and finished higher by 1.65 at $47.97. The stock climbed to nearly a 1-month high.

For comments and feedback contact: editorial@rttnews.com

Business News