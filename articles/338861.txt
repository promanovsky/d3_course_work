NEW YORK (TheStreet) --Shares of Caesars Acquisition Co. (CACQ) are higher by 2.64% to $12.42 in mid-afternoon trading on Monday after the company announced a proposed $880 million casino resort in Woodbury, NY.

Through its subsidiary, Caesars Growth Partners, Caesars Acquisition submitted a gaming license application to the New York State Gaming Facility Location Board this afternoon.

The resort would be located 50 miles from New York City and is expected to attract over 10 million visitors each year.

Must Read: Warren Buffett's 25 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.



The company said the project will benefit New York State, Orange County, and Woodbury with its anticipated $230 million in annual tax revenue, with $29 million annually going toward the local community, and $10 million to Orange County.

CACQ

data by

YCharts



STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.