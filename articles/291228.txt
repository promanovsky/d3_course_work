Apple CEO Tim Cook seems to get a lot of grief from investors and analysts because he’s not Steve Jobs. However, as a new profile of Cook in The New York Times makes clear, Cook is smart enough to understand that he’s not Steve Jobs and that nobody alive right now really is. However, just because there’s no Steve Jobs that doesn’t mean Apple is devoid of vision or talent and the company under Cook is relying more on teamwork than it did when Jobs was running the show.

U2 lead singer Bono, who has worked closely with both Jobs and Apple over the years, tells the Times that Cook isn’t trying to singlehandedly replace Jobs because he knows such a thing is impossible.

“[Cook is] saying, ‘I’ll try to replace him with five people,'” Bono explains. “It explains the acquisition of Beats.”

The way the Times‘ piece makes it sound, it seems one of Cook’s biggest roles within Apple is managing some of the big egos and personalities to help keep operations running smoothly. Jony Ive, meanwhile, has taken on the role of perfectionist design overseer for both hardware and software and he hasn’t hesitated to put his own personal stamp on iOS. Apple software engineering boss Craig Federighi, on the other hand, seems to have evolved into the role of being the company’s charismatic public face and he earned broad kudos for his presentation at this year’s WWDC.

As for where Beats fits in, it seems plausible that Jimmy Iovine will be Apple’s main negotiator with record labels and the entertainment industry as a whole as Apple tries to reassert its importance in the music world.

Even if this approach is successful, however, it probably won’t stop some people from whining that Cook just isn’t as exciting as Steve Jobs was. As one software engineer told the Times, “Jobs is to Lennon what Cook is to Ringo.”