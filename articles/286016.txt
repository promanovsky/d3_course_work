CHICAGO (CBS) — All lanes on the Kennedy Expressway have reopened ahead of schedule as construction crews demolished the 55-year-old Ontario Street Bridge. The Ohio Street exit was also reopened on Sunday.

The work began Friday night at 10 p.m. and was originally scheduled to continue until early Monday morning.

IDOT said the volume of traffic on the Kennedy saw a 33 percent decrease and travel times from O’Hare saw a 10 minute increase.

This is the first of two weekends for construction on the Kennedy. IDOT was originally planning on three weekends for construction, but the pace of the workers has enabled them to condense it into two.

The Kennedy was reduced to two lanes, beginning a mile before the Ohio Street feeder ramp.

In an effort to accommodate people who normally take the Kennedy on weekends, the CTA was adding extra bus and train service each weekend of the project. Metra also will have extra cars on some of its trains on the weekends of the demolition project.

The traffic tie ups on the Kennedy caused headaches, especially for Jillian Chrischilles, who was moving Saturday.

“It’s been a fun one, trust me. It took about two hours longer than it should have,” said Chrischilles.

River North resident Jeff Weiss says he doesn’t think it has been that bad.

“I think they gave us plenty of notice and I think it’s good for the city. We have to keep our infrastructure up and it’s part of living in the city,” said Weiss.

At Taste of Randolph, some weren’t letting the Kennedy construction stop a good time. Morgan Diamond took the bus to get to the event.

“I think it will be fine. I’m looking forward to still taking advantage of the summer, it might take a little while longer but I still will come out to the festivals,” said Morgan Diamond.

But the construction has an impact, in other ways. Jessica Green of the Grange Hall Burger Bar says getting food deliveries has been impacted.

“So there are lots of vendors that I count on being there earlier in the morning and so it’s backing us up, two to three hours sometimes,” said Green.

“I don’t think it’s taken any major tolls on attendance. The weather’s been fantastic. It’s patio season, people are ready for cocktails. Just doing what we do.”

The second phase of the bridge demolition begins next Friday and the outbound Kennedy will be reduced to two lanes, starting at Congress Parkway. The westbound Ontario ramp to the reversible expressways also will be closed.