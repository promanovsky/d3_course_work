News of the latest health crisis affecting Lee Kun-Hee, the chairman of Samsung Electronics, has reignited speculation about his successor as leader of the world’s biggest smartphone maker.

Lee, 72, was being treated at a hospital in Seoul on Sunday after a heart attack. He was reported to be in stable condition after undergoing a procedure for acute myocardial infarction.

Lee built Samsung into a global brand that sells everything from semiconductors to televisions and household appliances.

But while Samsung is the largest seller of mobile phones, with about 33 percent of the global smartphone market, compared to about 15 percent enjoyed by Apple in 2013, a number of smaller Asian vendors such as LG are starting to play catch-up.

Lee has reportedly been grooming his youngest son, Jay Y. Lee, to take eventual control of South Korea’s largest company by market capitalization.

Jay Lee, 45, served as Samsung’s chief operating officer until 2012 and is now vice chairman, although he doesn’t hold an operational title.

This isn’t the first time Lee Kun-Hee has been hospitalized. In August of last year he suffered from pneumonia. He was also treated for lung cancer in the late 1990s.

Whenever Lee is ill, attention shifts to how ready Samsung is for a potential change in leadership.

Industry experts say Samsung has been taking a number of steps recently that will ease the way for the younger Lee to gain control of the company.

One of the latest moves includes the planned IPO of Samsung SDS Co., which provides IT services for Samsung Electronics. The parent owns a 22.6 percent stake.