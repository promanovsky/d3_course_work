Android L was launched last week by Google at its annual developer conference, but there are still a lot of questions to be answered.

Will it be called Android 4.5 or Android 5.0? Will it be codenamed Liquorice, Lollipop or Lemon Pie? When will it be available and will my smartphone ever get the update.

Here we try to answer some of these questions but first here's a quick recap of what features Android L will bring:

What is Android L?

Android L is the latest version of Android which was previewed at Google I/O last week and which will be released to consumers later this year.

It features a number of significant updates, the most obvious of which is a new look with Google rolling out a new design language called Material Design which allows developers create layers within their apps.

Google has also deeply integrated its Android Wear platform with Android L, allowing users unlock smartphones without a pin code if they are wearing a smartwatch.

Notifications have also been given a make-over, allowing users respond to notifications directly from the lock screen.

Under the hood, Google is also working to improve battery life (Project Volta) and now supports 64-bit chips and promises to improve performance thanks to a move entirely to Android Runtime (ART).

Android L: Release Date

Google decided to make a change to the way it releases its major annual Android update in 2014 - falling in line with Apple by announcing the update at its summer developer conference before releasing the final version to consumers later in the year.

At Google I/O, the search giant said that the final version would be released "this fall" which for those of us not from the US or Canada means autumn.

Now the exact dates and months that constitute autumn vary greatly depending on where you are in the world, but considering Google is based in North America, let's go with what they say.

This mean that autumn begins with the September equinox, which in 2014 occurs on 23 September at 2.29am (GMT).

This essentially means Android L could arrive any time between 23 September and 23 December.

With Google launching the Nexus 4 and Nexus 5 in the month of November in the last two years, we could see Google launch the big Android update for 2014 alongside the Nexus 6 - or even the first Android Silver device.

So, which phones and tablets will get Android L update?

Here's a round up of what we know so far:

Nexus 4, 5, 7, 10

If you are a developer with a Nexus 5 or Nexus 7 (2013 version) then you are likely already running Android L, with Google releasing versions of the update for these devices last week.

If you are not a developer but do own one of the Nexus smartphones or tablets, then you will likely get the Android L update whenever the official version is released later this year - or very shortly afterwards.

HTC

Aside from Google itself, HTC was the quickest off the mark with announcements about Android L updates to its phones. In a statement issued soon after the Google I/O announcement, it said:

"HTC is excited about the new features in Android L and we can't wait to share them with our customers. We are committed to updating our flagship HTC One family as fast as possible. We will begin rolling out updates to the HTC One (M8) and HTC One (M7) in regions worldwide within 90 days of receiving final software from Google, followed shortly thereafter by other One family members and select devices."

So it means the HTC One Mini and One Mini 2, as well as the large screen HTC One Max will also get the Android L treatment, but you could be looking at an early 2015 update schedule for these phones.

Motorola

Motorola - soon to be consumed by Lenovo - also seemed to be quick off the mark with an announcement about its Android update schedule, but the company soon clarified that the source of the story was a fake Motorola Germany Facebook account.

The update suggested the Moto X and Moto G would both get the Android L update later this year, and while the update may have turned out to be fake, we wouldn't bet against it turing out to be true.

One of the benefits of having an almost pure version of Android on your phones is that it allows for a much faster upgrade time frame, which is why Motorola should be able to give customers an estimated upgraded schedule sooner rather than later.

LG

We've asked LG for any news on its plans for Android L for the likes of the G3 and G2 smartphones but we've heard nothing so far.

It should be noted that LG also makes the Google Nexus 4 and 5 smartphones, meaning the company should have a head-start on other manufacturers when it comes to getting Android L ported to its other devices.

Sony

Sony has resopnded to our request for details on its plans, telling IBTimes UK:

"We're enthused by early reaction to the Android L preview. Whilst we can't share roadmap specifics yet, we'll continue to bring unique Sony software matched with the latest Android experiences to as many Xperia users as possible - so stay tuned. Most recently, we're pleased to have started rolling Google's freshest Android version (4.4.4; KitKat) for Xperia Z1, Xperia Z1 Compact and Xperia Z Ultra."

While giving us nothing concrete, Sony's statement suggests that it is likely the Xperia Z2, Xperia Z1 Compact, Xperia Z Ultra and Xperia Z1 will all get the update, along with the company's Xperia Tablet Z and Xperia Z2 Tablet - though of course this is just speculation at this stage.

Samsung

And finally, the big one. Samsung traditionally brings the latest version of Android to only a very small subset of its smartphone offering, and so it is likely to be the same with Android L.

Just like LG and Sony, we have asked Samsung about its plans for the Android L update but so far the South Korean giant has remained tight-lipped about its plans.