NEW YORK (CBSNewYork) — Stunning developments were revealed Thursday about autism in America.

As CBS 2’s Dr. Max Gomez reported, autism is now thought to affect roughly 1.2 million children and teens in the United States – a huge increase.

But that includes all children on the autism spectrum, a very wide range of disease severity.

What has researchers excited is a new finding that brain changes in autism begin even before birth, Gomez reported.

New figures from the Centers for Disease Control and Prevention show 1 in 68 children have an autism spectrum disorder in the U.S.

That’s about a 30 percent increase from two years ago.

“We believe part of the increase in prevalence is due to better detection of autism. However, we still feel we are underestimating the prevalence of autism in the U.S.,” said Autism Speaks Associate Director Public Health Research Michael Rosanoff.

The report also highlights autism as almost five times more common among boys than girls.

While the disorder can be diagnosed as early as age 2, most children aren’t diagnosed until 4 years old.

Now, researchers have begun to find signs of autism in even younger children.

A study in the New England Journal of Medicine finds that changes in the brains of autistic children actually begin in the womb.

“We know there are places in the brain where they’re supposed to be and they are not. It’s like the pathways are not able to form in the proper way. We’ve been doing research at NYU that shows there’s problems in the connectivity of the brain, so that means that social centers, and language centers, and cognitive centers aren’t talking to each other in the way that they need to,” explained Dr. Melissa Nishawala, of NYU Child Study Center.

In normal fetuses, brain cells develop and migrate into specific layers. This study found that in the second trimester, some of the brain cells in autistic children did not go where they were supposed to, Gomez reported.

According to the CDC report, Alabama has the lowest prevalence of autism, while New Jersey has the highest.

However, the disparity could be due to different reporting methods as well as the fact that states like New Jersey provide more service for autism and affected families often move to those areas.

Check Out These Other Stories From CBSNewYork.com:

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]