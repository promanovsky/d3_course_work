Kate Winslet‘s ready for her post-pregnancy close up! As her sure-to-be-a-blockbuster film Divergent prepares to hit theaters this Friday, Winslet, 38, stepped out looking slim and trim just three months after giving birth to son Bear Winslet.

The wife of Brit Ned Rocknroll was photographed at LAX airport on Sunday, March 16 in fitted skinny jeans, knee-high boots, an oversized printed T-shirt, and a midnight blue suede jacket.

Winslet’s dramatic slim down might be due to her years of practice. The Titanic star is already mom to daughter Mia Threapleton, 13, and Joe Mendes, 10, with ex-husbands Jim Threapleton and director Sam Mendes respectively.

But this weight loss might be her most important yet. The Oscar winner received her star on the Hollywood Walk of Fame on Monday, March 17. At the ceremony, her Divergent costar Shailene Woodley praised Winslet in an impassioned speech, which brought her to tears.

“I have never met a more empowered, beautiful, humble and selfless [woman],” Woodley said. “As a young woman — I’m going to start crying — it’s inspiring to see somebody who has navigated this industry in such a beautiful and professional way. I couldn’t be more honored to know you and to work with you.”

In Divergent, she stars as Tris Prior’s main antagonist, the crafty Erudite leader Jeanine Matthews, in the film adaptation of the popular young adult series by Veronica Roth.

Divergent hits theaters Friday, March 21.