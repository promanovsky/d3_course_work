A federal task force recommended Monday that some pregnant women take low-dose aspirin daily to avoid getting preeclampsia, a condition that can lead to preterm birth and other complications.

Aspirin in general isn't recommended during pregnancy because it can contribute to maternal and fetal bleeding. However, low-dose aspirin is sometimes prescribed for pregnant women with certain health conditions.

The recommendation by the U.S. Preventive Services Task Force comes after the nation's biggest obstetrics association issued similar advice last fall.

The task force said Monday that women who have had preeclampsia during previous pregnancies should take a pill of 81 milligrams—often called "baby aspirin"—each day after the 12th week of pregnancy. The recommendation applies to pregnant women whose doctors consider them at high risk for the condition, as long as they haven't had previous bad medical experiences with aspirin.

Preeclampsia affects about 4 percent of pregnant women in the U.S., and is most often managed with drugs and other medical therapy. Yet "it is actually one of the more common conditions that lead to complications in the mother and baby," said Michael L. LeFevre, chairman of the federal task force.

More On This... Secondhand smoke linked to higher rate of miscarriages, stillbirths

Click for more from The Wall Street Journal.