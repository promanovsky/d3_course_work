Actress Christine Baranski's husband, "All My Children" star Billy Clyde Tuggle, died May 22, 2014 at age 69. UPI/John Angelillo | License Photo

NEW YORK, May 26 (UPI) -- Matthew Cowles' manager announced via Twitter the television actor and husband of actress Christine Baranski has died. He was 69.

The cause of his May 22 death was not disclosed.

Advertisement

Cowles is best known for playing Billy Clyde Tuggle on the soap opera, All My Children, from 1977 to 2013. He was also a popular New York stage actor and playwright.

He is survived by Baranski, his wife of more than 30 years, and their two daughters.