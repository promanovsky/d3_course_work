Photo: Yan Liang

The week prior to the holiday weekend has been rife with food recalls, virus outbreaks and warnings against possible food-borne illnesses. It also precedes one of the country’s biggest barbecue weekends: Memorial Day.

Sprouts, walnuts, hummus and ground beef have all been recalled this week either due to possible contamination from E. coli or Lysteria. E. coli is an intestinal bacteria that can cause gastrointestinal illnesses in humans. Symptoms can include abdominal pain and diarrhea. Lysteria is a type of food poisoning that most affects pregnant women and children. Symptoms include fever, muscle aches and nausea but can become more severe if the infection spreads to the nervous system.

Sprouts

Recall: No official recall has been announced yet, however preliminary investigations from the Centers for Disease Control and Prevention (CDC) point to the Evergreen Fresh Sprouts in Idaho.

The problem: There have been seven confirmed cases and at least three possible cases of E. coli in raw clover sprouts, according to the CDC. If that wasn’t bad enough, an investigation launched by the Canadian Broadcasting Corporation said of the 44 sprout packages tested across Canada, “93 percent tested positive for bacteria, and in some cases, high levels of enterococci bacteria, which is an indicator of fecal contamination.”

The verdict: Cooking sprouts, including mung beans and alfalfa sprouts at high temperatures can kill the bacteria, however most sprouts can only be eaten raw. So, if your sprouts aren’t cooked, steer clear this weekend.

Hummus and other dips

Photo: FDA

Recall: Around 14,860 pounds of Landal, Inc. (Hot Mama’s Foods) hummus and dip were recalled because of a Lysteria outbreak, according to a report released by the U.S. Food and Drug Administration. Popular retailers like Trader Joe’s, Target and Giant Eagle Supermarkets are pulling their items off the shelves.

The Problem: No illnesses were reported and the contamination was found from a routine testing of a Target Archer Farms traditional hummus product, which means many have already purchased the products.

The Verdict: Check the list of recalled products here, and discard them if necessary.

Walnuts

Recall: Sherman Produce said it would recall 241 cases of walnuts packaged in 25lb bulk cardboard boxes. In addition, it would recall Schnucks brand 10oz trays with expiration dates between 3/15 and 4/15, according to CNN.

The Problem: The FDA sampled the products and found traces of listeria, however it seems to only have affected Illinois and Missouri.

The verdict: Opt for a different nut this Memorial Day weekend.

Ground Beef:

Photo: Reuters

Recall: 1.8 million pounds of beef from Wolverine Packing in Detroit was recalled earlier this week due to E. coli risk.

The Problem: According to the USDA’s Food and Drug Administration, nine states may have received beef contaminated with E. coli.The USDA said the meat had already been shipped to restaurants in Massachusetts, Michigan, Missouri and Ohio. In addition, the Wall Street Journal reported Friday that an increase in the price of ground beef has caused a resurgence of the beef scraps of slaughtered cattle, dubbed "pink slime."

The verdict: A USDA representative told CNN that contaminated meat has begun to be removed from shelves, but if you have already purchased it, throw it out immediately.The full list of affected products can be found here.