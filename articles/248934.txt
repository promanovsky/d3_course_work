Canadian director Xavier Dolan won the Jury Prize for his film Mommy at the Cannes Film Festival Saturday, sharing the honour with French filmmaker Jean-Luc Godard.

There were high hopes that Dolan would win the festival's top award, the prestigious Palme d'or, but that honour went to the Turkish drama Winter Sleep.

Dolan was moved that he shared his prize with Godard, the 83-year-old film icon.

"What's beautiful in sharing a prize with Godard is that he, in a different era, tried to reinvent cinema and I like to think that we are in a moment where cinema is going in a different direction and I would be happy to be part of that."

Dolan, 25, was the youngest director at the festival, while Godard (Goodbye to Language), 83, was the oldest.

Dolan was very emotional when he went on stage, having to interrupt his speech a few times to regain his composure.

"The emotion that is sweeping over me as I think of this mythical room is overwhelming," he said.

'Everything is possible for those who dream, who dare, who work and who never give up,' said Xavier Dolan. (The Associated Press)

"I am lost in gratitude thinking of the recognition of the jury, the love we have felt over the last week, which has made me realize that we do this job to love and be loved in return."

He also seized the opportunity to urge his peers, in terms of age, to think big.

"There are no limits to our ambitions, other than those we impose upon ourselves and those that others impose upon us. Everything is possible for those who dream, who dare, who work and who never give up.

"In my generation, I see a desire for greatness, and I want to celebrate that so much, and if this prize inspires people to accomplish great things, so much the better."

Dolan also told jury head Jane Campion that her films inspired him to write strong women characters. Campion's The Piano won the Palme in 1993, the sole female director win.

A veteran of Cannes

Dolan is no stranger to the Cannes festival.

In 2009 he made a splash with his directorial debut, I Killed My Mother, written when he was just 17. The film — a semi-autobiographical story about a boy discovering his homosexuality and battling with his mother — reportedly earned an eight-minute standing ovation at the festival and ultimately won three awards in the Director's Fortnight Program.

Dolan's list of films also include Heartbeats, Laurence Anyways and Tom at the Farm.

The Montreal-born filmmaker began acting in commercials at age four, appeared in the TV movie Misericorde in 1994, and the divisive 2008 horror film Martyrs in 2008.

I want to dedicate the prize to the young people in Turkey and those who lost their lives during the last year. - Nuri Bilge Ceylan, Palme d'or winner

At Cannes this weekend, he was beaten for the Palme d'or by Nuri Bilge Ceylan's Winter Sleep, which is about a family running a hotel in the snowy Turkish hills and their strained relationship with their village tenants.

In his speech, Ceylan alluded to anti-government protests in Istanbul that began a year ago and have raged following a recent mining disaster that killed hundreds.

"I want to dedicate the prize to the young people in Turkey and those who lost their lives during the last year," said Ceylan.

Accepting the award, Ceylan, who has twice won Cannes' second highest honour, the Grand Prix, noted it was the 100th anniversary of Turkish cinema.

"It's a beautiful coincidence," he said. Winter Sleep is the second film by a Turkish director to win the Palme d'or following Yilmaz Guney and Serif Goren's The Way in 1982.

Cronenberg film gets actress nod

Julianne Moore won best actress for her performance in David Cronenberg's dark Hollywood satire Maps to the Stars. Screenwriter Bruce Wagner accepted the award for Moore and cheered the town he savagely parodies in the film: "Vive Los Angeles. Vive David Cronenberg. Vive Julianne Moore. And vive France," he said.

Best actor went to Timothy Spall who stars as British painter J.M.W. Turner in Mike Leigh's biopic Mr. Turner. He spoke emotionally about a long, humble career that has often gone without such notice.

"I've spent a lot of time being a bridesmaid," said the veteran character actor Spall, whose phone rang as he tried to read his speech from it. "This is the first time I've ever been a bride."

Bennett Miller won the award for best director. His wrestling drama Foxcatcher stars Channing Tatum and Steve Carell.

The Italian family drama The Wonders by Alice Rohrwatcher, was the surprise winner of the Grand Prix. Rohrwatcher was one of two female directors among the 18 films in competition for the Palme d'Or.

Andrey Zvyagintsev's Leviathan, a tragic satire about small-town corruption in Russia, took best screenplay. Though the film depicts corrupt local officials in Vladimir Putin's Russia, it was made with financial support from the Ministry of Culture.