GERMANTOWN, MD - JULY 6: The menus at Red Robin lists the calories for each item as required by Montgomery County. Charlsie Sienkwicz delievers burgers and fries to a large party at lunchtime Wednesday, July 6, 2011 in Germantown, MD. .Photo by Katherine Frey/The Washington Post via Getty Images)

A sick restaurant worker in Missouri may have exposed thousands of Red Robin customers to hepatitis A.

On Wednesday, county health officials announced that people who visited the chain's location in Springfield between May 8 and May 16 may have come in contact with the virus. According to the Springfield News-Leader, as many as 5,000 people may have been affected.

However, since the employee worked at the restaurant for several months before the infection was discovered, it's possible that many others who visited the Red Robin were exposed to hepatitis A, local news outlet KSPR notes.

The contagious virus, which upsets liver function, is spread by touch and also through contaminated food and water. Symptoms of fatigue, abdominal pain and nausea typically do not appear until several weeks after exposure to the virus, according to the Mayo Clinic.

County health officials first learned of the hepatitis A exposure on Tuesday after it was self-reported by Red Robin. Four thousand doses of hepatitis A vaccine have been shipped to Springfield to head off a possible outbreak.

Though officials are unsure if the sick worker passed on the infection, the Springfield-Greene County Health Department is setting up an immunization clinic on Thursday and Friday for Red Robin patrons who may have been exposed. Customers who visited the restaurant between May 8 and 16 are encouraged to get vaccinated. Any others who may have come in contact with the virus before May 8 will not be helped by the vaccine, since it must be administered within two weeks after exposure.

"If the vaccine is not taken within 14 days of the time exposed, it doesn't work,” county health department director Kevin Gipson said during a press conference, according to OzarksFirst. "So we are really on the clock, which is why we are doing this very quickly and we need your help to get the information out."