German retailers Aldi and Lidl have continued to increase their share of Ireland’s multi-billion euro grocery market.

New data out this morning from research group Kantar Worldpanel shows that Aldi’s share of the market here rose 21.9pc to 7.9pc in the 12 weeks to March 30, while Lidl’s climbed 11.1pc to 7.5pc, giving the pair a combined 15.4pc share of the Irish grocery market.

Tesco, the biggest operator here, saw its position at the top further eroded. Its market share in the period fell 6.6pc to 26pc. Supervalu’s share slipped 1.6pc to 25.2pc, and even though it declined, Tesco’s bigger drop means the Supervalu chain is continuing to close the gap on the market’s biggest player.

Cork-based retail group Musgrave controls the Supervalu brand and earlier this year rebranded all its Superquinn outlets under the Supervalu name.

Dunnes Stores saw its share of the market fall 3.9pc to 21.9pc.

“Aldi has maintained a growth rate of over 20pc throughout 2014,” according to

David Berry, commercial director at Kantar Worldpanel.

“This has boosted its market share from 6.4pc last year to a record 7.9pc. Aldi has capitalised by capturing more spend from its shoppers. Each shopping trip has grown by an average €2 per trip with two additional items being added to baskets.”

Online Editors