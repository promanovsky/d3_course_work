Herbalife, the controversial nutritional supplements company, is pulling out all the stops to buy back its shares as the cost of its battle with activist Bill Ackman and multiple regulators continues to mount.

While reporting record quarterly “adjusted” earnings on Monday, the company said it has abolished its $1.20 a share annual dividend and will instead plow that cash into a stock buyback plan.

The plan allows it to more aggressively repurchase stock even in the face of civil and criminal investigations regarding allegations it is a pyramid scheme.

Shares initially rose in after-hours trading, but were down 0.7 percent around 8:00 p.m., to $58.44.

The implementation of a special trading plan allows the Los Angeles company to buy back stock under a pre-arranged program even if it is in the possession of material nonpublic information and would normally be prohibited from trading.

Herbalife has been aggressively buying back stock under the plan, spending $255 million in April, it said.

Another $60 million will be spent in the next two days.

Herbalife spent $685.8 million of a $1.1 billion convert offering in February to buy back shares, boosting its long-term debt to $1.85 billion.

Herbalife reported record adjusted net income, earning $151.1 million up from $137.4 million during the year-earlier period.

But GAAP net income fell off a cliff — to $74.6 million from $118.9 million. One big reason was a $66.6 million net loss in Venezuela as a result of its currency devaluation.

Another $4 million was spent fighting attacks it is a pyramid scheme, including $670,000 related to the Federal Trade Commission probe.