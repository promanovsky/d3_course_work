An experimental therapy that was once hailed as a possible cure for people whose high blood pressure does not respond to medication, has been shown to be ineffective, a US study said Saturday.

Known as renal denervation, the process involves inserting a catheter into a patient's arteries and delivering radiofrequency energy that inactivates kidney nerves.

It was believed to offer a pathway to lowering blood pressure by interrupting electrical signals to and from the kidney, an organ that is a key player in regulating blood pressure.

It has been approved for use in 80 countries, but is still considered experimental in the United States.

In a randomized trial of 535 people, in which some were treated with the procedure and others received a fake therapy, both groups saw decreases in blood pressure after six months, but the difference between them was not statistically significant.

In other words, the real thing did not lower their blood pressure any better than a placebo.

The findings were published in the New England Journal of Medicine, and were presented at the American College of Cardiology annual meeting.

"This is the first blinded trial or sham controlled trial in the field of renal denervation," said Deepak Bhatt, professor of medicine at Harvard Medical School, and co-principal investigator.

"We saw no added treatment benefit of renal denervation for patients with severe resistant hypertension who were closely monitored and optimally treated with medications."

The company that owns the technology, Medtronic, first announced in January that findings from Bhatt's study, called SYMPLICITY HTN-3, had shown the product to be ineffective and said it would halt enrollment in studies using it in Japan, India and the United States.

Medtronic bought the technology, known as the Symplicity Catheter System, in 2011 for $800 million dollars when it acquired the California-based company Ardian, Inc.

Franz H. Messerli, director of the hypertension program at Mount Sinai hospital in New York, said this finding could spell the end for renal denervation, even though it contradicts most previous research on the therapy.

"Previously, renal denervation has been widely touted as the next big therapy for millions of patients with resistant hypertension. It has even been called a possible new 'cure,'" said Messerli.

"However, the SYMPLICITY HTN-3 study results now may close the book on renal denervation and bring the renal-denervation train to a grinding halt."

High blood pressure raises the risk of heart attack and stroke for up to a billion adults across the globe.

About 10 percent of high blood pressure patients are diagnosed with resistant hypertension, which means their systolic blood pressure is 140 mm Hg or higher even when they are taking maximum doses of at least three medications to lower it.