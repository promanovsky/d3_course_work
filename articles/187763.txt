The numbers of teen pregnancy, births and abortions in the United States are all far lower than their rate peaks in the 1990s, according to a new study released by the Guttmacher Institute, an international advocate for sexual and reproductive health and rights.

According to the report, "U.S. Teenage Pregnancies, Births and Abortions, 2010: National and State Trends by Age, Race and Ethnicity," approximately 614,000 pregnancies occurred among U.S. teenage women aged 15-19 in 2010, for a rate of about 57.4 pregnancies per 1,000 women of that age -- a 51 percent drop from its high in 1990.

The teen birthrate as well also declined 44 percent from its high in 1991, declining from 61.8 births to 34.4 births per 1,000 women, while the teen abortion rate declined 66 percent, falling from 43.5 abortions per 1,000 women in 1988 to 14.7 abortions out of every 1,000 women in 2010.

"The decline in the teen pregnancy rate is great news," lead author Kathryn Kost said in a news release announcing the study. "Other reports had already demonstrated sustained declines in births among teens in the past few years; but now we know that this is due to the fact that fewer teens are becoming pregnant in the first place. It appears that efforts to ensure teens can access the information and contraceptive services they need to prevent unwanted pregnancies are paying off."

The research data also showed teen pregnancy rate declines across all racial and ethnic groups, with teen pregnancies dropping 56 percent among non-Hispanic white teens -- from 86.6 to 37.8 pregnancies out of every 1,000 women -- and among black teens -- from 223.8 to 99.5 per 1,000 -- between 1990 and 2010.

Hispanic teen pregnancy rates lowered 51 percent, from 169.7 to 83.5 per 1000 women, between peak year 1992 and 2010.

The study revealed teen pregnancy rates declined in all 50 states between 2008 and 2010, though there were significant disparities between states, which the authors of the research concluded were determined by a combination of state demographic characteristics, the availability of comprehensive sex education, knowledge about and availability of contraceptive services and cultural attitudes toward sexual behavior and early childbearing.

New Mexico in 2010 had the highest teenage pregnancy rate, at 80 per 1,000 women; followed by Mississippi, with 76; Texas, with 73; Arkansas, also with 73; and Louisiana and Oklahoma, both with 69 pregnancies per 1,000 women.

The lowest rates were found in New Hampshire, with 28 per 1,000; Vermont, with 32; Minnesota, with 36; and Massachusetts and Maine, each with 37.