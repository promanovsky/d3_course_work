"That Awkward Moment" star Zac Efron reportedly was injured in a fight in downtown Los Angeles earlier this week.

Citing law enforcement sources, TMZ.com said Thursday that police patrolling a neighborhood under the Harbor Freeway/Interstate 110 broke up a fight involving the actor, 26, and another man against at least three transients. The website said the incident occurred just after midnight Sunday.

TMZ said Efron told police he and his companion had run out of gas and were waiting for a tow truck when they threw a bottle out of the window. When it smashed on a pavement, some transients, believing the bottle had been aimed at them, retaliated by attacking the companion, whom the website described as Efron's bodyguard.

Efron reportedly told police that when he exited the car to help his friend, he was struck in the mouth, describing it as "the hardest I've ever been hit in my life." No one was arrested, according to TMZ.

People magazine said Los Angeles Police Department officials it contacted had no record of the incident. E!, however, said an LAPD report confirmed that two unidentified males were stranded when their vehicle ran out of gas and then became involved in an altercation with what the report called "a couple of transients in the area."

Efron's spokeswoman did not respond to a request for comment.

Sign up for Newsday's Entertainment newsletter Get the latest on celebs, TV and more. By clicking Sign up, you agree to our privacy policy.

Several hours afterward, Efron and friends attended the Laffest show at the Hollywood Improv, E! News said.