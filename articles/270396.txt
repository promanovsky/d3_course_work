Rihanna has yet again proved her status as a style icon by wowing in the most daring of ensembles at Monday evening's CFDA Fashion Awards ceremony. The 26 year-old pop star attended to collect the Fashion Icon Award at the annual Council of Fashion Designers of America Awards last night (2nd June) in New York City.

Rihanna already had the award in the bag therefore she had to do little to prove her eligibility but the 'S&M' hitmaker decided to cement her reign in the fashion kingdom by wearing an outfit no one could take their eyes off.

The custom made, Adam Selman-designed sheer gown comprised of 216,000 Swarovski crystals and left little to the imagination. In a soft shade of pink and with a flapper girl-inspired headscarf and stole, the outfit was part Jessica Rabbit and part futuristic space goddess.

More: Tom Ford says "Rihanna clothing choices are more important than reviews."

Rihanna eschewed all underwear beneath her very see-through dress, apart from a flesh-coloured thong. The unusual jewel-studded material extended to her fingers via gloves and her outfit was complete with an array of sparkling rings and smouldering, smoky eyes. She kept her footwear comparatively simple with golden sandal stilettos and white toenail varnish.

The Barbadian singer looked thrilled to have been honoured at the awards and gave a speech after she had been presented with the trophy by Vogue's Anna Wintour: "I grew up in a really small island and I didn't have a lot of access to fashion but as far as I can remember, fashion has always been a defence mechanism. I remember thinking "she can beat me, but not my outfit,"' Rihanna revealed.

More: Rihanna recruits Emeli Sande to write hits for next album.

Commenting on the creation, Rihanna's stylist acknowledged that the design was bound to generate a storm of attention: "We definitely wanted to make it a little bit scandalous," they admitted, via the Mirror.

"The dress is just fishnet and crystals and a couple of fingers crossed. But fashion is about beauty, and the [female] body is party of that."



Rihanna Made Another Daring Choice With This Adam Selman Sheer, Sparkling Dress.