A bio-secure laboratory able to handle some of the world’s most infectious diseases has opened in Geelong as scientists around the world scramble to understand and contain the latest virus to go global: MERS.

The first case of Middle East Respiratory Syndrome, or MERS, was confirmed in America last weekend, taking to 401 the number of people diagnosed with the virus, which has spread to at least a dozen countries.

The multi-million dollar facility at CSIRO’s Australian Animal Health Laboratory is the only one of its kind in the country. It will enable scientists to work with live viruses, including MERS, SARS and avian influenza, as they try to understand how the viruses work and why some animals carry and transmit them but don’t get sick or die.

CSIRO immunologist Andrew Bean said about 70 per cent of emerging infectious diseases come from animals and moved to humans, making the research undertaken at the new laboratory just as important to human health as to animal health.

‘’There’s probably a greater risk now than there ever was in emerging infectious diseases being transmitted to humans,’’ Dr Bean said. ‘’Factors like the expansion of urban areas, intensification of agriculture and even climate change are all contributing.’’