Be careful what you feed your dog.

The U.S. Food and Drug Administration recently issued another warning about toxic jerky treats, and it seems the poisonous pet snacks are still a problem.

As of May 1, more than 1,000 dogs have died as a result of the toxic treats. The FDA has received 4,800 reports of pet illnesses since 2007, 1,800 of which were submitted in the months following the agency's last warning in October 2013. Even though 25 percent of these complaints were "historic" claims -- meaning the illness occurred some time in months or years past -- the jerky products appear to be an ongoing source of sickness for both cats and dogs.

So far, the FDA has been unable to pinpoint why so many pets are getting sick and developing kidney-related diseases. However, the majority of cases appear to derive from consumption of a jerky treat that was imported from China.

Though pet owners could avoid Chinese-made nibbles, that's not a surefire way to avoid the toxic treats since ingredients in the product could still be produced abroad, Fox 5 News reports.

Since there is not one particular brand that has been singled out as the main culprit, the agency is asking owners who feed their dogs jerky treats to keep a close eye on their pets. (The Humane Society keeps a running list of recalled pet food and treats on its website.)

If symptoms such as decreased appetite, increased water intake, vomiting or diarrhea show up and persist for more than 24 hours after a dog or cat has consumed a jerky treat, pet owners are urged to contact a veterinarian immediately.

Pet owners who have a cat or dog may have consumed a toxic jerky treat may report their findings to the FDA by calling 1-888-INFO-FDA.