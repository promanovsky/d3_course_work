Update (4:05 p.m.): Updated with Thursday market close information.

NEW YORK (TheStreet) -- Apple (AAPL) - Get Report dipped slightly Thursday after Amazon (AMZN) - Get Report released its streaming music service called Prime Music.

The service is free for Amazon Prime customers, who already pay $99 a year. Amazon promises more than one million songs from approximately 90,000 albums and hundreds of pre-made playlists. Prime Music is now one more option for some streaming music users in a space that already includes Apple's iTunes Radio, Spotify and Pandora (P) .

The stock closed down 1.7%, or $1.60, to $92.26.

Must Read: Warren Buffett's 25 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Separately, TheStreet Ratings team rates APPLE INC as a "buy" with a ratings score of B+. TheStreet Ratings Team has this to say about their recommendation:

"We rate APPLE INC (AAPL) a BUY. This is driven by a few notable strengths, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its revenue growth, largely solid financial position with reasonable debt levels by most measures, notable return on equity, expanding profit margins and good cash flow from operations. Although no company is perfect, currently we do not see any significant weaknesses which are likely to detract from the generally positive outlook."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

AAPL's revenue growth has slightly outpaced the industry average of 2.4%. Since the same quarter one year prior, revenues slightly increased by 4.7%. Growth in the company's revenue appears to have helped boost the earnings per share.

Although AAPL's debt-to-equity ratio of 0.14 is very low, it is currently higher than that of the industry average. Along with the favorable debt-to-equity ratio, the company maintains an adequate quick ratio of 1.32, which illustrates the ability to avoid short-term cash problems.

The return on equity has improved slightly when compared to the same quarter one year prior. This can be construed as a modest strength in the organization. When compared to other companies in the Computers & Peripherals industry and the overall market, APPLE INC's return on equity exceeds that of the industry average and significantly exceeds that of the S&P 500.

43.45% is the gross profit margin for APPLE INC which we consider to be strong. It has increased from the same quarter the previous year. Along with this, the net profit margin of 22.39% is above that of the industry average.

Net operating cash flow has slightly increased to $13,538.00 million or 8.26% when compared to the same quarter last year. In addition, APPLE INC has also modestly surpassed the industry average cash flow growth rate of 5.45%.

You can view the full analysis from the report here: AAPL Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.