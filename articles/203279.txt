While fitness bands like Jawbone and FitBit can provide valuable information, not everyone will want to wear a piece

But now you have the option of leaving the gear at home, and simply wearing a smart shirt.

Advertisement

OMsignal created biosensing shirts that connect to an app and provide you with useful information like heart and breathing rates.For instance, you can have the shirt alert you when your heart rate goes above or below your ideal training zone. It will also advise you how to improve irregular breathing.

You can wear it outside the gym too using "Lifestyle mode" to monitor your body's energy, physical stress, and activity levels.

Advertisement

The shirts use low-power Bluetooth to send the information to your phone via a "little black box" that's inside the shirt. It can go up to three continuous days without having to recharge.

The shirt itself is also supposed to have cool features like improving blood circulation and posture. And it's machine washable so there's no crazy upkeep.

Here's the app that goes along with the shirt:

Advertisement

Check USA Today's video of the smart shirt:

As of now OMsignal has created a tank, short-sleeved, and long-sleeved shirt. It is planning to release a women's line in the near future. If you go to the site you can pre-order the mens shirts, a smartphone sleeve, the tracking module, and some starter kits that cost $199. OMsignal claims it will begin shipping in the summer.