Google has released a Google Chrome update that adds a new feature to their Chrome OS and also the Chrome Browser, you can now use OK Google to search using your voice.

Google will be rolling out this new feature will be available in the Chrome Browser and Chrome OS in the US and also users who have set Chrome to US English.

With the latest release of Chrome, all #Chromies can now search by voice in Chrome. Just open a new tab or visit Google.com, say “Ok Google” and take it for a spin.

Whenever you visit the Google homepage you will be able to say OK Google, and then perform a search, schedule a reminder or ask a question.

Source Google+, Engadget

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more