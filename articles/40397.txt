Here are our 5 Takeaways From the New X-Trailer:

1. Magneto and Xavier: The best of frenemies

“It’s going to take the two of us. Side by side at a time when we couldn’t be further apart,” Magneto says of the future. The trailer shows that “X-Men: DOFP” will feature the Professor X/Magneto relationship in two ways: In the future, the two will be allies; they look old, tired, defeated and desperate as they let Wolverine know he must travel in time and reunite them in the past if there is any hope of saving the future for mutants.

AD

AD

In the past, well, Wolverine is going to have his work cut out for him, with the younger professor and a younger, much stronger Magneto clearly at odds with each other. Xavier seems to have taken quite personally the alliance (and possibly something more) that Mystique has with Magneto — while Magneto lets the professor know that he should have paid more attention to those he cared about. Xavier, despite his powers, seems unsure of himself in the past, while Magneto of the past has clearly embraced his role as the master of magnetism.

2. Sentinels

We knew we’d be getting a better look at the Sentinels in the latest trailer once the “X-Men: DOFP” production team had more time to work with special effects — though the Sentinels still don’t get much screen time. (Of course, that’s not necessarily a bad thing, given how much fanboys can complain about [pseudo-]spoilers in trailers and other clips.)

The sentinels are going to be a problem. They are the weapon of choice in the war against mutants, who are perceived as a threat by Bolivar Trask (Peter Dinklage). Again, not much to be seen regarding the X-Men’s greatest threat, but it seems as though we can look forward to intense battles (Sentinels vs. Colossus and of course, Sentinels vs. Magneto).

AD

AD

3. A quick look at Quicksilver

The Internet sure got ornery when a first look at Evan Peters as Pietro Maximoff — a.k.a. Quicksilver, the super-speedster son of Magneto — was revealed. Some fanboys had declared that the version of Quicksilver who appears in “Avengers: Age of Ultron” would be better — without ever seeing any big-screen images. The only thing we’ve seen of “X-Men’s” Quicksilver is his choice of wardrobe, which seems to have upset many. Let’s cut him a little slack, given that it’s the ’70s and what he’s wearing is probably the best of what was available in silver (plus, you know, points for that Pink Floyd/”Dark Side…” T-shirt). And once we actually see Quicksilver in super-speed action, most of us will likely get over Pietro’s ’70s duds. (Now, do we forgive Mystique/Jennifer Lawrence for her “I totally got to keep my ‘American Hustle’ threads” fashion choice?)

4. Iceman … the real Iceman

Seeing that Iceman/Bobby Drake (Shawn Ashmore) is a part of this “X-Men” is wonderful. Seeing that he’s the real Iceman (and by real, we mean covered head to toe in ice) and not the “frosty-hands Iceman” of the first two “X-Men” films is, well, awfully cool. Ashmore’s frosting up completely into ice at the end of “X-Men: The Last Stand” was one of the few highlights of that film, which director Bryan Singer let get away from him. And not only do we get the real Iceman — we also get him zooming around on an ice-slide, just as in the old “Spider-Man and His Amazing Friends” cartoon.

5. A bright cinematic future?

The future may appear doomed for mutants (even with Ellen Page back as our Kitty Pryde), but the future of X-Men movies looks strong with Singer, who seems poised to make this his triumphant return to the franchise. It’s too soon to guess whether a movie stuffed with so many mutants can be effective. Nevertheless, with “X-Men: Apocalypse” already announced for 2016, Singer should feel at ease back in the X-saddle.