The new live-action Teenage Mutant Ninja Turtles movie has premiered its first trailer.

The latest incarnation of the half-shelled heroes, who began life in Kevin Eastman and Peter Laird's 1984 comic book, will star Megan Fox as April O'Neil and William Fichtner as Shredder.

In the new update, O'Neil's father plays directly into the turtles' origins.

Alan Ritchson will play Raphael, Noel Fisher is Michelangelo, Jeremy Howard is Donatello, Pete Ploszek is Leonardo and Danny Woodburn is starring as Splinter, with the characters being brought to life through motion-capture for the first time.

Will Arnett is in the supporting cast as April's cameraman Vernon Fenwick, and Whoopi Goldberg is Bernadette Thompson.

Michael Bay is producing the movie, with Jonathan Liebesman in the director's chair. The film will open in cinemas on August 8 in the US and October 17 in the UK.

William Fichtner on Ninja Turtles: 'You find out who Shredder is'

Will Arnett teases bruising Shredder fight in Ninja Turtles

Photo gallery - Ninja Turtles evolution in pictures:

Movies: Teenage Mutant Ninja Turtles on-screen evolution - in pictures

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io