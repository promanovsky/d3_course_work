Joan Rivers has sounded off against "Girls" star Lena Dunham's weight. Speaking during an interview with Howard Stern, Rivers claimed that Dunham sets a bad example for young girls by promoting an overweight lifestyle.

"Lena Dunham, who I think is again terrific, how can she wear dresses above the knee?" Rivers asked Stern, later adding: "You're sending a message out to people saying it's OK, stay fat, get diabetes, everybody die. Lose your fingers."

"I think the thing we love about her is she doesn't give a s--t," Stern responded. "Do you watch Girls? Do you watch when she's naked on there? She did a whole episode in a bikini, it was the funniest f--king thing I've ever seen."

For comments and feedback contact: editorial@rttnews.com

Entertainment News