BlackBerry is bulking up its security assets with the acquisition of anti-eavesdropping firm Secusmart, which developed technology to protect the smartphones of German government officials, including Chancellor Angela Merkel.

The Waterloo, Ont.-based company revealed plans to add extra power to its mobile security foundation on Tuesday at an event held in New York.

Chief executive John Chen said buying Secusmart addresses growing concerns about threats to individual privacy and national security.

"We are always improving our security solutions to keep up with the growing complexity of enterprise mobility, with devices being used for more critical tasks and to store more critical information, and security attacks becoming more sophisticated," Chen said Tuesday in a news release.

Financial details of the agreement were not disclosed and BlackBerry (TSX:BB) said the acquisition still faces some conditions, including regulatory approval.

The move positions BlackBerry against competitors like Apple and Samsung, which have been making inroads into mobile security, an area that BlackBerry once almost exclusively dominated.

Secusmart provides high-level voice and data encryption for governments, businesses and telecommunications service providers. The company developed special technology for BlackBerry 10 phones that were distributed to several German government agencies, ministries and leaders such as Merkel.

Corporate security has become a hot topic after businesses like Target Corp. faced massive data breaches that saw private information of its customers stolen. On Tuesday, the Canadian government announced that Chinese hackers infiltrated computer systems at the National Research Council.

With more government agencies and businesses relying on smartphones to transfer confidential information, the likelihood of third parties obtaining private information has increased.

"We see significant opportunities to introduce Secusmart's solutions to more of BlackBerry's government and enterprise customers around the world," said Secusmart managing director Hans-Christoph Quelle.

While it wasn't immediately clear how BlackBerry benefits from having Secusmart under its umbrella, the security developer could help build an all-encompassing infrastructure that would be the foundation of BlackBerry's future.

BlackBerry executives have placed a greater emphasis on moving into the "Internet of Things," a buzzy phrase used to describe the technology which connects various objects to one network. Still in its infancy, it's widely expected that more devices used throughout our daily lives -- from automobiles to fridges -- will be linked on a network which allows remote access and control of information.

In 2010, BlackBerry acquired QNX Software Systems, an Ottawa-based company that develops technology for the automotive industry, including dashboard systems that connect directly with a driver's mobile phone.

Since its acquisition, QNX has played a key role in creating BlackBerry's latest smartphone operating system and a separate one developed specifically for medical devices.

Independent technology analyst Carmi Levy said the acquisition of Secusmart marks a step forward for BlackBerry as it tries to launch a return to profitability by focusing on its loyal enterprise customers, rather than the more fickle consumer market.

"In and of itself it doesn't complete BlackBerry's security road map, but it is a solid move that sets the stage quite nicely for additional, similarly-themed moves," Levy said in an email.

"As a key pillar of its fast-coalescing security strategy, it serves an important purpose as both an indicator of how much the company has changed since John Chen came aboard, and where he intends to take it next."

Chen was hired last year to reshape BlackBerry, cut costs and lead an effort to find a better footing in the highly competitive tech sector. Since joining the company in November, he has hired several executives from software maker Sybase, a struggling company he helped reshape into a profitable operation focused on mobile business technology.

BlackBerry also plans to launch a new program designed to monitor the security of apps installed on its devices.

The company said BlackBerry Guardian will protect users from malware and privacy issues by scanning for any unusual activity and reacting accordingly.

"If we identify a suspicious app, we investigate and take whatever action is needed to protect BlackBerry customers, including either denying the app or removing it from BlackBerry World," wrote David MacFarlane, senior director of BlackBerry security, on one of the company's blogs.

BlackBerry Guardian will monitor apps available on its BlackBerry World store as well as ones distributed through the Amazon Appstore, which will be available this fall on phones, giving users easy access to thousands more of the most popular Android apps and games.

Shares of BlackBerry fell 11 cents to $10.64 near midday on the Toronto Stock Exchange.