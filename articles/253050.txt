Thomas Piketty, the economist whose bestselling book on inequality was criticised by the Financial Times for using erroneous data, turned on his accusers yesterday to declare the newspaper’s analysis of his work “just ridiculous”.

The French economist’s book Capital in the Twenty-first Century took the economic and political world by storm with its doctrine that wealth inequality is widening globally. His theory has struck a chord around the world amid a widespread feeling on the left that the poorest have suffered worse than the richest from a financial crisis that was created by wealthy elites.

The Financial Times’ economics editor claimed at the weekend that Mr Piketty made mistakes in his 696-page book and altered some figures without explaining why. Chris Giles also accused Mr Piketty of “cherry-picking” his sources.

Mr Piketty, a 43-year-old professor at the Paris School of Economics, has hit back by posting all his data on his website for peer review and declared to the Bloomberg news agency that “there’s no mistake or error” in his work.

The dispute has sent the world of economic academia into a whirl, with professors who had praised the work poring over Mr Piketty’s data over the weekend.

One of the Financial Times’ examples of alleged inaccuracies in Mr Piketty’s work was in his figure for the UK, which suggested that the top 10 per cent of the population holds 71 per cent of national wealth. The Office for National Statistics says the figure is 44 per cent.

Mr Piketty responded that the ONS survey was “based on self-reported data and is very low quality”.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here