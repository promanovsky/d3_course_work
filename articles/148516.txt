Kim Kardashian’s latest fashion blunder has seriously set back her recent style triumphs. The Keeping Up With The Kardashians star was spotted hitting up a Starbucks in Los Angeles with her sister, Kourtney, and she seems to have taken ripped jeans to a whole new level.

Kim Kardashian has worked hard to be seen as a high fashion icon

Teamed with a smart blazer and a pair of pink stiletto high heels, Kim stepped out wearing what can best be described as barely there ripped jeans. Her thighs were wholly visible through the denim pants, which looked more torn open than fashionably distressed.

Kanye West’s fiancée has recently been hailed as an emerging fashion icon with her series of sophisticated ensembles. Appearing on the April cover of US Vogue cemented Kim’s style status, although her latest look may temporarily silence her supporters. Kim appears to have set aside her bid to become a high fashion clothes horse, reverting to her less sophisticated wardrobe from the pre-Kanye days.

Kim Kardashian with her daughter, North West

Kim’s sister Kourtney didn’t seem to have been affected by Kim’s poor fashion choice. The mom-of-two looked chic in a leather perforated skirt, white blouse and two part stiletto heels.

Kim and Kanye are due to be wed on May 24. The pair have a daughter together, North West, born in June 2013.

MORE: Jay-Z won't attend Kanye West and Kim Kardashian's wedding

MORE: John Legend warns Kanye West of reality TVs impact on relationship

Watch Kim Kardashian wearing floral with Kanye West at Met Gala 2013

