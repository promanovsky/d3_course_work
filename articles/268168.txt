The federal government may open the skies to Hollywood.

The Federal Aviation Administration said Monday that it would consider a request by several companies to use drones for filming movies and TV shows.

The agency said it was reviewing a request from seven aerial and photo and video production companies that seeks permission to use “unmanned aircraft systems” for the first time.

Although the FAA already allows law enforcement agencies, fire departments and other public agencies to use drones, it has effectively banned their use for commercial purposes since 2007.

Advertisement

The FAA has been weighing allowing commercial uses for drones for several years, but has delayed issuing regulations out of concern that doing so could create safety hazards for manned commercial aircraft.

Now, the FAA is facing heavy pressure from the entertainment, agriculture and the oil and gas industries, as well as other sectors, to allow them to use drones for commercial uses.

The FAA plans to propose a formal rule for commercial drones by the end of the year, but regulations aren’t expected to be finalized until 2015.

The Motion Picture Assn. of America, whose members have worked with the seven companies, supported the request.

Advertisement

“Unmanned aircraft systems (UAS) offer the motion picture and television industry an innovative and safer option for filming,” Neil Fried, a senior vice president at the MPAA, said in a statement. “This new tool for storytellers will allow for creative and exciting aerial shots, and is the latest in a myriad of new technologies being used by our industry to further enhance the viewer experience.”

The companies that have filed petitions to receive exemptions are: Aerial Mob, Astraeus Aerial, Flying-Cam Inc., HeliVideo Productions, Pictorvision Inc., Vortex Aerial, and Snaproll Media, the FAA said in a statement.

They are seeking exemption from regulations that address general flight rules, pilot certificate requirements, manuals, maintenance and equipment mandates.

To qualify for the exemptions, the companies must show their drone operations won’t “adversely affect safety,” and would be in the public interest, the FAA said.