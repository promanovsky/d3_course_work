CARTERSVILLE, Ga., July 31 (UPI) -- Two U.S. aid workers in Liberia who were infected with the Ebola virus will be evacuated back to the United States.

Both Nancy Writebol and Dr. Kent Brantly were working in Liberia with Samaritan's Purse to treat Ebola victims when they were diagnosed with the virus last week, the organization said.

Advertisement

A source speaking on condition of anonymity told CNN a medical charter flight left Cartersville, Ga., to travel to Liberia to pick up the two aid workers.

It's not clear where Writebol and Brantly will be taken, but hospital officials at Emory University near the U.S. Centers for Disease Control and Prevention in Atlanta said at least one of the patients would be taken there.

Once the two arrive in the United States, it will be the first time a patient infected with the Ebola virus will be known to be in the country.

The aid workers are in stable but grave conditions, Samaritan's Purse said.

The World Health Organization believes some 1,323 people in Guinea, Liberia, Sierra Leone and Nigeria have been infected with the virus since March. Of those, 729 are believed to have died.