Waffle Tacos with Bacon, anyone ?

Yum! Brands' (NYSE:YUM) Taco Bell rolled out its much-anticipated breakfast menu this week. It's an interesting move and points out that doing breakfast right can be quite lucrative for fast-food restaurants. Just ask McDonald's (NYSE:MCD), where the morning menu accounts for 15% to 20% of total revenue. Mickey D's is so successful that Yum! Brands targeted it by using real people named Ronald McDonald to tout Taco Bell's breakfast.

How well did the Bell do with its new menu? Motley Fool managing editor Eric Bleeker has the answer in this video -- and says competing restaurants such as Chipotle Mexican Grill (NYSE:CMG) can learn a lot from Taco Bell's efforts.