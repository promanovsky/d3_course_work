MUNSTER, Ind., May 5 (UPI) -- A man, who has the first confirmed case of Middle East Respiratory Syndrome in the United States, is improving and is in good condition, said hospital officials in Munster, Ind.

The male patient, a healthcare provider who had been in Saudi Arabia, whose identity is not being made public, is hospitalized in good condition in Community Hospital and is improving each day, officials said.

Advertisement

“We are very pleased the patient is improving and no other cases have been identified at this time,” Dr. William VanNess II, Indiana state health commissioner, said in a statement.

“The individual has received excellent care while at Community Hospital in Munster. The swift diagnosis and precautionary measures taken have undoubtedly greatly helped reduce the risk of this potentially serious virus spreading.”

RELATED First Middle East Respiratory Syndrome Coronavirus case in the United States

Dr. Anne Schuchat, assistant surgeon general and director of the Centers for Disease Control and Prevention's National Center for Immunization and Respiratory Diseases said the patient took a plane April 24 from Riyadh to London, then to Chicago and then took a bus to Munster, Ind.

A week ago Sunday, the patient began to suffer symptoms -- a fever, cough and shortness of breath -- and last Monday he went to Community Hospital's emergency room and was later admitted.

The patient is isolated from other patients, is receiving oxygen, but is not on a ventilator. There are no specific treatments recommended for the MERS-CoV coronavirus and no vaccine.

RELATED MERS virus reported in Egypt

The Indiana state Department of Health and the Centers for Disease Control and Prevention are investigating. As of Sunday, no other cases were identified in the United States.

Hospital staff, who had direct contact with the patient, prior to the patient being placed in full isolation were taken off duty and placed in home isolation and are being closely monitored for any signs or symptoms. They will be allowed to return to work once the incubation period -- usually five days but can be as long as 14 days -- is over, and they return negative laboratory results.

“The patient is in full isolation and presents no risk to patients, staff or the general community,” Don Fesko, chief executive officer of Community Hospital, said. “We are thoroughly prepared to handle respiratory infections. We continue to work closely with the CDC and state Health Department and are following every recommendation. Safety is our top priority.”

RELATED Uptick in MERS cases in Saudi Arabia has health officials worried

The MERS-CoV virus does not appear to spread easily from person to person, but in other countries it has spread to family members and to healthcare workers who treated patients with the virus.

"This first U.S. case of MERS-CoV infection represents a very low risk to the general public," Schuchat said in a telephone news conference Friday.

RELATED Camels linked to Middle East respiratory syndrome coronavirus