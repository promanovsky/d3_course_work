Fraudulent steps in ‘breakthrough’ paper

Share this article: Share Tweet Share Share Share Email Share

Tokyo - Japan's top research body on Tuesday accused the lead writer of stem cell papers hailed as a game-changer in the field of medical biology of misconduct involving fabrication, but the scientist called the findings unacceptable. Two papers published in the scientific journal Nature in January detailed simple ways to reprogramme mature animal cells back to an embryonic-like state, allowing them to generate many types of tissues. Such a step would offer hope for a simpler way to replace damaged cells or grow new organs in humans. But reports have since pointed out irregularities in data and images used in the papers, prompting RIKEN, a semi-governmental research institute and employer of the lead writer, to set up a panel to look into the matter. The panel said, for example, that one of the articles reused images related to lead writer Haruko Obokata's doctoral dissertation, which was on different experiments.

“Actions like this completely destroy data credibility,” Shunsuke Ishii, head of the committee, told a news conference.

“There is no doubt that she was fully aware of this danger.

We've therefore concluded this was an act of research misconduct involving fabrication.”

In a statement, Obokata said she would soon file a complaint with RIKEN, challenging the findings.

“I'm filled with shock and indignation,” she said. “If things stay as they are, misunderstanding could arise that the discovery of STAP cells itself is forgery. That would be utterly unacceptable.”

Obokata, 30, refers to the reprogrammed embryonic-like cells in her team's research by the term Stimulus-Triggered Acquisition of Pluripotency, or STAP, cells.

RIKEN may reinvestigate the matter if a complaint is filed. It has not decided what penalty may be imposed on the researcher, the research body said.

Obokata became an instant celebrity in Japan after the publication of her papers, with television broadcasting images of her wearing a traditional Japanese apron, rather than a lab coat, and working in a laboratory with pink-painted walls.

RIKEN did not confirm or deny the existence of STAP cells, but said it planned to launch a verification process to see if they were real.

That will take about a year to complete and will be led by RIKEN President Ryoji Noyori, a 2001 Nobel laureate in chemistry.

“This is truly regrettable,” Noyori said, referring to the probe panel's conclusions.

“I would like to apologise afresh that articles RIKEN researchers published have damaged the credibility of the scientific community,” he said, bowing to reporters as camera flashes went off.

According to the Nature papers and media briefings, Obokata and other researchers took skin and blood cells, let them multiply and then subjected them to stress “almost to the point of death” by exposing them to events such as trauma, low oxygen levels and acidic environments.

Within days, the scientists - Japanese researchers joined by others from Brigham and Women's Hospital and Harvard Medical School in the United States - said they had found the cells had not only survived but had also recovered by naturally reverting to a state similar to that of an embryonic stem cell.

These stem cells were then able to differentiate and mature into different types of cells and tissues, depending on the environments they were put in, they said.

RIKEN said outside researchers had been unable to replicate the research. - Reuters