Beirut -

Lebanon's Health Ministry says it has recorded the country's first case of the Middle East respiratory virus.

It says the potentially fatal virus was detected in a man who had checked into a local hospital after feeling ill. It says the man's case was not severe, and he has since been discharged.

A ministry official said on Friday the patient had recently returned from a visit to several Gulf countries, including Saudi Arabia.

The kingdom has been the focal point of the outbreak of the virus, known as the Middle East Respiratory Syndrome (MERS).