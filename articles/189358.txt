Nintendo has released its latest annual financial results, the company posted a loss of 46.4 billion Yen, which is around $457 million US dollars.

The company made a net loss of 23.2 billion Yen, around $228 million US dollars, and Nintendo also revealed that they sold only 310,000 Wii U consoles in the quarter to the end of March.

This is down 20 percent on the previous year, and Nintendo have sold a total of 6.17 million Wii U consoles around the world since it was launched.

Nintendo managed to sell 590,000 units of its 3DS handheld console, and the company has said it expects to return to profit in its 2014 fiscal year.

Nintendo will focus on efforts that seek to stimulate the platform,” the company said in its release, promising to expand Wii U sales “by providing software that takes advantage of the Wii U GamePad, utilizing its built-in functionality as an NFC reader/writer, and adding Nintendo DS Virtual Console titles to the Wii U software lineup.”

Whether Nintendo will be able to boost interest in its Wii U console remains to be seen, the console has underperformed since launch, and considering that the original Wii was one of the most popular consoles, Nintendo may have some serious work to do.

Source The Verge, Techmeme

Image Credit Wired

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more