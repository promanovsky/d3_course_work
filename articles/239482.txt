Conrad Hoskin

Although there is an estimated 10 million to 12 million living species in the world, we know surprisingly little about them. According to Quentin Wheeler, founding director of the International Institute for Species Exploration, we have only identified around two million -- and species are going extinct faster than they are being identified.

That said, on average 18,000 new species are discovered every year; and, every year since 2008, the IISE and the SUNY College of Environmental Science and Forestry announce a list of the top 10 species discovered the previous year, in order to draw attention to those discoveries. This occurs on 23 May -- the birthday of Carolus Linnaeus, the 18th-century Swedish botanist who devised the modern system of scientific names and classifications.

"The majority of people are unaware of the dimensions of the biodiversity crisis," Dr Wheeler said. "The top 10 is designed to bring attention to the unsung heroes addressing the biodiversity crisis by working to complete an inventory of earth's plants, animals and microbes. Each year a small, dedicated community of taxonomists and curators substantively improve our understanding of the diversity of life and the wondrous ways in which species have adapted for survival."

Click through the gallery below to see the institute's selections for discoveries in 2013. Note that the species are not ranked, and the list is presented in no particular order.