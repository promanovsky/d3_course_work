NEW YORK (TheStreet) -- Shares of Bank of America (BAC) - Get Report closed down -1.90% to $15.51 today on very heavy trading volume after it was reported that lawyers for the bank and the Justice Department met on Tuesday to continue negotiating a potential mortgage-securities settlement but remain far apart on the size and scope of the penalty, sources say, the Wall Street Journal reports.

The stock is slightly lower in after-hours trading.

The bank is offering $13 billion, including cash and consumer relief, but the Justice Department wants a much larger cash penalty, sources added.

How the settlement would be structured is unclear, with Bank of America pushing for a bigger portion of the penalty to be paid in so-called soft money-such as consumer relief, sources told the Journal.

Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates BANK OF AMERICA CORP as a Buy with a ratings score of B. TheStreet Ratings Team has this to say about their recommendation:

"We rate BANK OF AMERICA CORP (BAC) a BUY. This is driven by multiple strengths, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its reasonable valuation levels, expanding profit margins, notable return on equity and increase in stock price during the past year. We feel these strengths outweigh the fact that the company has had sub par growth in net income."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

The gross profit margin for BANK OF AMERICA CORP is currently very high, coming in at 84.82%. It has increased from the same quarter the previous year. Regardless of the strong results of the gross profit margin, the net profit margin of -1.08% is in-line with the industry average.

BAC, with its decline in revenue, slightly underperformed the industry average of 2.6%. Since the same quarter one year prior, revenues slightly dropped by 4.6%. Weakness in the company's revenue seems to have hurt the bottom line, decreasing earnings per share.

The return on equity has improved slightly when compared to the same quarter one year prior. This can be construed as a modest strength in the organization. Compared to other companies in the Commercial Banks industry and the overall market on the basis of return on equity, BANK OF AMERICA CORP underperformed against that of the industry average and is significantly less than that of the S&P 500.

Compared to where it was a year ago today, the stock is now trading at a higher level, regardless of the company's weak earnings results. Looking ahead, the stock's rise over the last year has already helped drive it to a level which is relatively expensive compared to the rest of its industry. We feel, however, that the other strengths this company displays justify these higher price levels.

You can view the full analysis from the report here: BAC Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.