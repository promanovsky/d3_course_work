A simple blood test might soon be enough to determine whether a patient has cancer and how advanced the disease is.

Researchers at Stanford University School of Medicine have reportedly developed a test to monitor levels of tumour DNA in the bloodstream, which can quickly tell doctors how large a tumour is and how it is responding to treatment.

It is expected that the new test could be applied in the treatment of common types of cancer, including breast, lung and prostate and could also be used to screen healthy or at-risk patients for signs of the illness.

Describing the technique, assistant professor of radiation oncology Dr Maximilian Diehn said: "We set out to develop a method that overcomes two major hurdles in the circulating tumour DNA field. First, the technique needs to be very sensitive to detect the very small amounts of tumour DNA present in the blood.

It may be possible to develop assays that could simultaneously screen for multiple cancers. This would include diseases such as breast, prostate, colorectal and lung cancer, for example. - Dr. Maximilian Diehn

"Second, to be clinically useful it's necessary to have a test that works off the shelf for the majority of patients with a given cancer."

The new test is also able to identify if cancers are developing resistance to a particular treatment, allowing doctors to adapt the type of care being given.

Dr Diehn added: "If we can monitor the evolution of the tumour, and see the appearance of treatment-resistant subclones, we could potentially add or switch therapies to target these cells.

"It's also possible we could use CAPP-Seq to identify subsets of early stage patients who could benefit most from additional treatment after surgery or radiation, such as chemotherapy or immunotherapy.

"It may be possible to develop assays that could simultaneously screen for multiple cancers. This would include diseases such as breast, prostate, colorectal and lung cancer, for example."

Assistant professor of medicine Dr Ash Alizadeh added: "We're trying to develop a general method to detect and measure disease burden. Blood cancers like leukemias can be easier to monitor than solid tumors through ease of access to the blood.

"By developing a general method for monitoring circulating tumour DNA, we're in effect trying to transform solid tumours into liquid tumours that can be detected and tracked more easily."

The study was published online in the journal Nature Medicine.