NEW YORK (TheStreet) -- Shares of Google Inc. (GOOGL) - Get Report are slightly lower after the U.S. Supreme Court today rejected the company's bid to dismiss a lawsuit accusing it of violating federal wiretap law when it accidentally collected emails and other personal data while building its Street View program, Reuters reports.

The justices left intact a September 2013 ruling by the 9th U.S. Circuit Court of Appeals, which refused to exempt Google from liability under the federal Wiretap Act for having inadvertently intercepted emails, user names, passwords and other data from private Wi-Fi networks to create Street View, which provides panoramic views of city streets, Reuters said.

Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates GOOGLE INC as a Hold with a ratings score of C. TheStreet Ratings Team has this to say about their recommendation:

"We rate GOOGLE INC (GOOGL) a HOLD. The primary factors that have impacted our rating are mixed some indicating strength, some showing weaknesses, with little evidence to justify the expectation of either a positive or negative performance for this stock relative to most other stocks. The company's strengths can be seen in multiple areas, such as its robust revenue growth, largely solid financial position with reasonable debt levels by most measures and reasonable valuation levels. However, as a counter to these strengths, we also find weaknesses including a generally disappointing performance in the stock itself, disappointing return on equity and feeble growth in the company's earnings per share."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

Despite its growing revenue, the company underperformed as compared with the industry average of 21.2%. Since the same quarter one year prior, revenues rose by 19.1%. Growth in the company's revenue appears to have helped boost the earnings per share.

Although GOOGL's debt-to-equity ratio of 0.07 is very low, it is currently higher than that of the industry average. Along with this, the company maintains a quick ratio of 4.17, which clearly demonstrates the ability to cover short-term cash needs.

Net operating cash flow has increased to $4,391.00 million or 20.86% when compared to the same quarter last year. Despite an increase in cash flow, GOOGLE INC's average is still marginally south of the industry average growth rate of 23.16%.

GOOGL's stock share price has done very poorly compared to where it was a year ago: Despite any rallies, the net result is that it is down by 33.07%, which is also worse that the performance of the S&P 500 Index. Investors have so far failed to pay much attention to the earnings improvements the company has managed to achieve over the last quarter. Despite the heavy decline in its share price, this stock is still more expensive (when compared to its current earnings) than most other companies in its industry.

The company's current return on equity has slightly decreased from the same quarter one year prior. This implies a minor weakness in the organization. When compared to other companies in the Internet Software & Services industry and the overall market, GOOGLE INC's return on equity is below that of both the industry average and the S&P 500.

You can view the full analysis from the report here: GOOGL Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.