Quickie divorce? Gwyneth Paltrow 'will end marriage to Chris Martin fast to keep stories of infidelity and OTHER secrets from slipping out'



Gwyneth Paltrow and Chris Martin want to make certain their dirty marital laundry does not get aired for millions to gossip about.

That's why the couple of 10 years will get divorced quickly and quietly, according to a Monday report from TMZ.

Paltrow, 41, and Martin 37, will reportedly not haggle over custody of their two young children or their massive fortunes that have been said to total nearly $300m so that they can make certain the split does not get ugly.

Scroll down for video...



Headed to divorce court very soon?: Chris Martin and Gwyneth Paltrow, pictured on Friday in the Bahamas, will end their 10-year-marriage as quickly as possible and without fanfare, according to a Monday report from TMZ

The site pointed out that neither the Coldplay singer nor the Oscar-winning actress want any reports of infidelity to come about, which could happen if they had a long, drawn-out, nasty divorce.

Several years ago Martin was accused of romancing Kate Bosworth, which he adamantly denied, and, more recently, there was talk he had a connection with Alexa Chung. Paltrow was linked to billionaire Jeffrey Soffer (who is now wed to Elle Macpherson) and attorney Kevin Yorn, though she has denied being with these men.



There was also mention of 'other issues' that have plagued the photogenic pair but no details were given.

No bad talk here: Paltrow (left) and Martin (right) don't want whispers of infidelity making the rounds



The duo - who had moved from New York City to the Brentwood section of Los Angeles a year ago to work on their marriage - will reportedly end their union as 'amicably' as they can 'in large part because they've each done things during the marriage they don't want ending up in legal docs.'

Sources tell TMZ the split is 'not a separation' and they are 'definitely getting divorced.'

This contradicts what the Sunday Mirror reported last week when they claimed, 'Chris still loves Gwyneth and would still like the marriage to survive.'

No arguing over money or kids: The couple, seen here in 2013, will not duke it out in court

Both are concerned with publicity and their image, which might explain why the two looked so happy on Friday as they dined on the island of Eleuthera in the Bahamas, where they have been holed up with their children since revealing their split earlier this week.

Chris had Gwyneth - who had removed her wedding ring - in gales of laughter, cracking joke after joke as they enjoyed a relaxed evening out with friends.



The couple sat beside each other and appeared relaxed and happy, giving no sign of the turmoil that caused the breakdown of their marriage.



Both looked quite casual with Gwyneth opting to leave her blonde hair loose in natural waves around her shoulders.



Chris was unshaven and casual in a long-sleeved T-shirt and he and Gwyneth were spotted deep in conversation.



She was clearly enjoying herself, and at one stage was giggling so uncontrollably that she had to cover her face with her hands.



It was evident that both are determined to put on a brave face for the sake of their children Apple, aged nine, and seven-year-old Moses.



And Chris' jokes were so hilarious that at one stage Gwyneth was spotted wiping tears from her eyes.

An eye-witness who saw the group at dinner told The Sun on Sunday newspaper: 'You would never have guessed they had just split up. They looked like they were getting on OK.'

The source added: 'Chris seemed in good form and was telling lots of jokes. Gwyneth pulled a few faces but it was all in good spirits.'



Not everyone is happy with the change in their relationship, however.

Gwyneth's mother Blythe Danner is bothered that the two will be no more.



'[She's] really upset. She loves Chris,' a source told Us Weekly. 'She begged Gwyneth not to end it, that he's a good man. But Gwyneth had to do what she felt was best.'

Gwyneth announced her split from Martin last Tuesday, via her healthy-living site Goop.com which promptly crashed due to the overwhelming traffic.

'They really tried to make it work,' the insider added. 'You could tell something was wrong, though, over the last few months. You could see a sadness in each of their eyes.'

Emmy Award-winning actress Danner, 71, was married to Gwyneth's dad Bruce Paltrow for over 30 years until his death in 2002.

Last year, Gwyneth revealed that her parents' relationship served as an inspiration for her own.

She said: 'I asked my dad once, "How did you and Mom stay married for 33 years?" And he said, "Well, we never wanted to get divorced at the same time."

'I think that's what happens,' the Iron Man star added. 'When two people throw in the towel at the same time, then you break up, but if one person's saying, "Come on, we can do this," you carry on.'

One less tenant: The Coldplay singer will move out of this $10.45m Brentwood mansion the couple purchased over a year ago



