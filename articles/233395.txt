TEHRAN, May 21 (UPI) -- Iranian authorities have arrested three men and three women for their roles in creating an online music video set to Pharrell Williams' hit song "Happy."

The Iranian Student News Agency reported that Tehran Police Chief Hossein Sajedinia had ordered the arrests for making an "obscene video clip that offended the public morals and was released in cyberspace."

Advertisement

Pharrell Williams wrote on his Facebook page Tuesday: "It is beyond sad that these kids were arrested for trying to spread happiness."

23-year-old fashion photographer Reihane Taravati, who claimed on social media to have been involved in the video project, posted a link to the video on May 17 to her Facebook page, thanking fans for the 178,000 views. Her last Facebook post appears to have been May 18.

The National Iranian American Council condemned Iran for arresting the young adults and released a statement that said, in part:

"The irony that Iranian youth were arrested for dancing to a song called 'Happy' seems to be lost on the Iranian authorities. The Iranian people cannot be forced to live in a world where enrichment is a right, but happiness is not."

The NIAC also noted that the arrests "comes just days after [Iranian President Hassan] Rouhani publicly called for an end to restrictions against Internet communications in Iran, saying 'We must recognize our citizens' right to connect to the World Wide Web.'"

A hash-tag movement -- #FreeHappyIranians -- was spawned in response to the arrests, and has gone viral.