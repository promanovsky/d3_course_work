Five portions of fruit and vegetables a day – a familiar mantra for those concerned about their own and their children's health – may not, after all, be enough, according to a new report by scientists, who suggest we should instead be aiming for seven a day, and mostly vegetables at that. Alarmingly for some who thought they were doing the right thing, tinned and frozen fruit may not be helpful at all.

The latest wisdom – guaranteed to raise a groan from those already perplexed over stories of suspect sugars and dodgy fats – arises from a study carried out by experts at University College London, who analysed the eating habits of 65,000 people, revealed through eight years of the Health Survey for England, and matched them with causes of death.

The clear finding was that eating more fresh fruit and vegetables, including salads, was linked to living a longer life generally and in particular, to a lower chance of death from heart disease, stroke and cancer.

Eating at least seven portions of fresh fruit and vegetables a day was linked to a 42% lower risk of death from all causes. It was also associated with a 25% lower risk of cancer and 31% lower risk of heart disease or stroke. Vegetables seemed to be significantly more protection against disease than eating fruit, they say.

There was a surprise finding – people who ate canned or frozen fruit actually had a higher risk of heart disease, stroke and cancer.

The authors, Dr Oyinlola Oyebode and colleagues from the department of epidemiology and public health at UCL, said they were unsure how to interpret the findings on canned or frozen fruit . It could be that people eating canned fruit may not live in areas where there is fresh fruit in the shops, which could indicate a poorer diet.

Alternatively, they could be people who are already in ill-health or they could lead hectic lifestyles. There is also another possibility: frozen and tinned fruit were grouped together in the questions, but while frozen fruit is considered to be nutritionally the same as fresh, tinned fruit is stored in syrup containing extra sugar. More work needs to be done to see whether sweetened, tinned fruit is in fact the issue, the researchers say.

Oyebode and colleagues took into account the socio-economic background, smoking habits and other lifestyle factors that affect people's health. What they have found, they say, is a strong association between high levels of fruit and vegetable consumption and lower premature death rates – not a causal relationship.

But the strength of the study, published in the Journal of Epidemiology and Community Health, is in the big numbers and the fact that the data comes from the real world – not a collection of individuals who had a particular health condition or occupation, but a random selection.

The "five a day" advice was launched by the government in 2003, after the World Health Organisation advised in 1990 that our minimum daily intake of fruit and vegetables should be 400g a day.

France and Germany also recommend five a day, while the US abandoned the numbers in favour of a "fruit and veggies – more matters" campaign in 2007. But Australia advises people to eat substantially more. In 2005, the Australian government launched "Go for 2+5", meaning two 150g portions of fruit and five 75g portions of vegetables. That is 675g, the equivalent in the UK of 8.5 portions.

Oyebode said she thought the Australian example was probably the one to follow. "I think it makes a lot of sense," she said. "It is aiming for more and the balance is two fruit and five veg. From our study it looks like vegetables are better than fruit. But I don't feel very strongly that the guidelines should be changed because the majority of people know they should eat five a day and only 25% manage that."

Changes in policy, she said, would be needed to improve the UK score. "Anything that could increase the accessibility and affordability of fruit and vegetables would be very helpful, such as working with corner shops to make sure they stock them," she said. Petrol stations could also offer fruit and vegetables and maybe the Healthy Start scheme – which gives families on less than £16,000 vouchers for fruit and vegetables – could be extended.

Other experts agreed that the study was sound and representative of the population, but cautioned that in a study of the habits of people in the real world, it is hard to take full account of complications, such as education, smoking habits and people failing to tell the exact truth about their diet. "A key outstanding question is whether this [reduced risk of disease] is entirely attributable to these specific foods, or whether they are acting as a marker of a broader dietary pattern associated with improved health," said Professor Susan Jebb of the Nuffield department of primary care health sciences, University of Oxford.