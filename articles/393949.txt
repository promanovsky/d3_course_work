Any airline would struggle with the devastating impact of losing one jet full of passengers, especially if it had already been bleeding money for years.

But losing another just months later is pushing crisis-hit Malaysia Airlines to the brink of financial collapse, airline experts said, spotlighting whether it can steer its way out of extended turmoil as once-troubled carriers such as Korean Air and Garuda Indonesia did before.

The flag carrier needs an immediate intervention from the Malaysian government investment fund that controls its purse strings, and likely deep restructuring, to survive the twin tragedies of flights MH370 and MH17, analysts added.

Malaysia Airlines (MAS) was already struggling with years of declining bookings and mounting financial losses when MH370's mysterious disappearance in March with 239 people aboard sent the carrier into free-fall.

The July 17 downing of flight MH17 over Ukraine, which killed all 298 people on board, deeply compounds those woes.

"The harrowing reality for Malaysia Airlines after MH17 is that if the government doesn't have an immediate game plan, every day that passes will contribute to its self-destruction and eventual demise," Shukor Yusof, an analyst with Malaysia -based aviation consultancy Endau Analytics, told AFP.

Shukor said MAS was losing "one to two million US dollars a day," and has "the bandwidth to stay afloat for about six more months based on my estimates of its cash reserves".

While the MH17 disaster was beyond the airline's control - pro-Russia separatists in Ukraine stand accused of shooting it down with a missile - bookings are expected to take a further significant hit, as they did in MH370's wake.

"I'm not considering going to Malaysia in the next few years. Not unless Malaysia Airlines acts or does something in the future that will allow people to feel more relaxed about travelling there," said Zhang Bing, a Chinese national in Beijing.

Jonathan Galaviz, a partner at the US-based travel and tourism consultancy Global Market Advisors, said "perception is key in the airline industry".

"Unfortunately for Malaysia Airlines, potential international customers are now going to link the brand to tragedy."



The airline already has announced refunds for ticket cancellations following MH17, which Galaviz said would cost millions of dollars.

Speculation is rife that state investment vehicle Khazanah Nasional, which owns 69 per cent of the airline, will delist its shares and take it private, which could set the stage for painful cost-cutting measures and other reforms.

Analysts have long blamed poor management, government interference, a bloated workforce, and powerful, reform- resistant employee unions for preventing the airline from remaining competitive.