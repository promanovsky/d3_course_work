THERE are bad PR stunts and then there are BAD PR stunts.

This stunt failed on a spectacular level when the bomb squad was called in.

Ninemsn employees were evacuated yesterday afternoon from the Australia Square building in Sydney when an entertainment journalist was sent a suspicious package. A safe was accompanied by a note which said “check your voicemail” amid a raft of blacked-out lines.

As the package couldn’t be opened and was said to be making a strange noise, Ninemsn immediately called in the NSW police, which in turn called the bomb disposal unit. All staff on the floor were sent home at 3pm.

Ninemsn staffer Natasha Lee tweeted that the journalist the package was addressed to was in tears.

@Brentus88 @drpiotrowski Yup. Stood next to the journo who received it - she was in tears. Really creepy. — Natasha Lee (@tashlee) May 28, 2014 Sub-type: comment CAPTION: @Brentus88 @drpiotrowski Yup. Stood next to the journo who received it — she was in tears. Really creepy.— Natasha Lee (@tashlee) May 28, 2014 Sub-type: comment CAPTION: @Brentus88 @drpiotrowski Yup. Stood next to the journo who received it — she was in tears. Really creepy.— Natasha Lee (@tashlee) May 28, 2014

Once the bomb disposal unit had gained access to the safe, it contained Ubisoft’s Watch Dogs game along with some promotional items.

Watch Dog is an action-adventure game which follows a character seeking revenge for the death of his niece.

News.com.au understands Ubisoft, or any of its agencies, did not reached out to Ninemsn to explain or apologise for the incident. Ninemsn has also been unable to reach the game company.

Ubisoft has been contacted for comment.

UPDATE: Ubisoft has since issued this statement to news.com.au:

“As part of a themed promotion for Watch Dogs, our team in Australia sent voicemail messages to some local media alerting them that they’d receive a special package related to the game. Unfortunately, the delivery to Ninemsn didn’t go as planned, and we unreservedly apologise to Ninemsn’s staff for the mistake and for any problems caused as a result. We will take additional precautions in the future to ensure this kind of situation doesn’t happen again.”