Disclosure: The organizers of ChinaJoy paid my way to Shanghai. Our coverage remains objective.

SHANGHAI — Microsoft announced today that it will launch the Xbox One video game console in China on Sept. 23 for 3,699 renminbi. That amounts to $600, or $100 more than what Microsoft charges in the U.S.

Xbox One is the first game console approved by the Chinese government since it banned game consoles as harmful to children back in 2000. The launch will be one of the first attempts to chip away at the stronghold of PC gaming in one of the world’s largest video game markets.

Microsoft said it will build the machine with China’s BesTV in a joint venture. More than 25 developers are building 70 games for the Xbox One in the Chinese market. Microsoft said the content will include exclusive titles from Microsoft Studios as well as titles from Electronic Arts, Ubisoft, and the 2K Games label of Take-Two Interactive.

Chinese developers include Perfect World, Gamebar, and Tencent. Microsoft also launched the ID@Xbox program in China in an effort to stoke indie gaming on the Xbox One in China.

Microsoft announced the launch on the eve of ChinaJoy, the huge game expo taking place in Shanghai this week.

The console is more expensive than in the U.S. in part because of a 17 percent value-added tax on imports.