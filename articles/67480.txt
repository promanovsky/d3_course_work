We round up the latest music video premieres from the past week.

Lady Gaga: 'G.U.Y'

To make up for never getting a visual for one of her best songs, 'Do What U Want', Gaga has chucked three tracks into one epic music video. Naturally, the star plays the role of a phoenix stumbling out of the ashes to the soundtrack of ARTPOP, before posing with the ancient gods for a rendition of 'Venus' and 'G.U.Y.'

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

To complement the Far East undertones of their new single, Bombay Bicycle Club have set their new visual in the heart of a stunning Bollywood wedding.

Rixton: 'Me and My Broken Heart'

They may well see themselves as chumps, but if someone can charm and out-smart a poker table of gorgeous female con-artists, it's Shane Richie's son.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Sam Smith: 'Stay With Me'

As far as music video treatments go, re-enacting the walk of shame after a one-night stand would be pretty low on the list. That said, when it's soundtracked by Sam Smith's very good new single about unrequited love, it actually ends up being surprisingly endearing.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Birdy: 'Words As Weapons'

She's the 17-year-old balladeer who looks sweeter than a Chelsea bun, but as her latest visual clearly shows, mess with her heart and you will be haunted forever. Quite literally.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

What are your favourite music videos from the past week? Leave your comments in the box below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io