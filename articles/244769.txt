ALBANY, N.Y., May 23 (UPI) -- Sleep apnea is linked to increased risk of heart disease and diabetes, but a large U.S. study found the sleeping disorder is also linked to hearing loss.

Lead author Dr. Amit Chopra of the Albany Medical Center in New York said all subjects underwent successful in-home sleep apnea studies.

Advertisement

Sleep apnea is the temporary cessation of breathing during sleep, which in some cases is due to obstruction of the upper airway by enlarged tonsils causing loud snoring and fighting for breath. Chopra and colleagues assessed sleep apnea with the apnea-hypopnea index, which indicates sleep apnea severity based on the occurrences of apnea -- complete cessation of airflow -- and hypopnea -- partial cessation of airflow -- per hour of sleep. Sleep apnea was defined as an apnea-hypopnea index of more than 15 events/hour.

"In our population-based study of 13,967 subjects from the Hispanic Community Health Study/Study of Latinos, we found that sleep apnea was independently associated with hearing impairment at both high and low frequencies after adjustment for other possible causes of hearing loss," Chopra said in a statement.

"Patients with sleep apnea are at increased risk for a number of co-morbidities, including heart disease and diabetes, and our findings indicate that sleep apnea is also associated with an increased risk of hearing impairment."

Sleep apnea was independently associated with a 31 percent increase in high frequency hearing impairment, a 90 percent increase in low frequency hearing impairment and a 38 percent increase in combined high and low frequency hearing impairment.

The findings were presented at the American Thoracic Society International Conference in San Diego and are in American Thoracic Society International Conference Abstracts.

RELATED High levels of a stress hormone linked to risk taking in those who stay up late