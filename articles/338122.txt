Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Mick Jagger has taken a break from prancing around stage reliving the old days to have a playful pop at Monty Python for, er, prancing around on stage reliving the old days…

The 70-year-old rocker released a tongue-in-cheek video on the eve of the comedy show’s live gig at the O2 in London on Tuesday night mocking the stars John Cleese, Michael Palin, Eric Idle, Terry Gilliam and Terry Jones.

Jagger also has a dig at his own reputation for being frugal. In the sketch he is told the Rolling Stones’ lighting engineer is working on Python’s one-off run of 10 shows.

In a reply that could have been about the Stones themselves, he said: “Monty Python, are they still going?

"Ten shows, they will coin it in. I bet it is expensive. But, I mean, who wants to see that again?

“It was funny in the 60s. They are a bunch of wrinkly old men trying to relive their youth.

"I mean the best one died years ago. We’ve seen it all before, they’ve put it all up on YouTube.”

(Image: Reuters)

But the Python stars laughed off Jagger’s jibes. Idle, 71, said their fans wanted to see the classic sketches, just like Stones lovers turned up to their reunion gigs to hear great hits from all those years ago.

He added: “There are similarities. They want to see the parrot sketch, like you want to see Let’s Spend The Night Together.

“It would be odd to try to write better things than our best at this age.”

Jagger’s mick-taking video will not be shown at the Python gigs. But scientist Stephen Hawking makes an ­appearance. He will be in a video with Professor Brian Cox.

At a press conference in London, Idle said of Hawking: “He’s a big Python fan so he was asked if he would and within a minute he said yes.”

A different celebrity guest will also appear each night in a sketch called ­Blackmail.

Cleese, 74, said the show, which includes 10 male dancers and 10 female performers who writhe around in lingerie, costs around £4.5million to stage.

One source who saw a dress rehearsal at the weekend said it mixes live sketches with “lots of video and animation”.

Songs including I’m A Lumberjack and the dead parrot sketch will also feature.

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

The source added: “If you are a fan of Python you won’t be disappointed.”

It will be the goup’s first UK ­performance in more than 40 years. And they insist it will be their last.

Cleese revealed how he has been preparing. He said: “I’ve been sleeping a lot. I don’t feel so nervous. At our age if they have bought a ticket they like you.”

Idle added: “We wanted it to be energetic. We leap around. I am quite worn out even after the first number.”

Monty Python’s Flying Circus was a TV hit between 1969 and 1974. Sixth Python Graham Chapman died of cancer in 1989 aged 48.

A new box set of the album Monty Python Sings (Again) featuring three unreleased songs and three new recordings, is out now.

Did you know we have a TV & film page on Facebook? Check us out here.