Amazon Prime Data: this phone may have “free” internet

This week a tip on Amazon’s first smartphone has made its way public, revealing the name “Amazon Prime Data.” This data service has been suggested to be tied to AT&T exclusively, and very possibly related to AT&T’s Sponsored Data program. With this program, certain apps and traffic have their data traffic bills footed by the company that they benefit.

According to BGR’s tip, Amazon’s phone could launch with Amazon Prime and Prime Data right off the bat. If Prime Data is similar to Sponsored Data, it could mean that Amazon would offer a phone in which all of their own products and services are served to the consumer free of any extra data costs.

Imagine a smartphone made by Amazon in which all Amazon services cost you nothing to run wirelessly.

Nothing beyond the cost of Amazon Prime, or whatever other service you’re subscribing too. Even a phone in which access to Amazon.com and the Amazon apps for product purchase would be a real change for the industry.

Now imagine a phone where, if you subscribe to Amazon Prime’s services, you can stream their videos and listen to music served by Amazon anywhere you go, at no additional data bill-related cost.

Amazon’s recent annual fee for Amazon Prime may be an indicator of this service being real. At $99 a year instead of $79 a year, could this be enough cash to get consumers interested in a Prime Amazon phone? We shall see!