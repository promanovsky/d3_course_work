The U.S. Food and Drug Administration (FDA) has warned that some topical acne products can cause potentially life-threatening allergic reactions.

The Administration warned that consumers should stop using their topical acne medications and seek medical attention immediately if they experience symptoms such as "throat tightness; difficulty breathing; feeling faint; or swelling of the eyes, face, lips, or tongue," the news release reported. Use of the product should also be discontinued if the consumer experiences hives or itching, an FDA news release reported.

A hypersensitivity reaction could occur within minutes or up to a day after product application. The products of concern are under names such as "Proactiv, Neutrogena, MaxClarity, Oxy, Ambi, Aveeno, Clean Clear, and store brands," the FDA reported. The products are available as "gels, lotions, face washes, solutions, cleansing pads, toners, face scrubs, and other products."

The dangerous symptoms differ from the labeled side effects, which are: "redness, burning, dryness, itching, peeling, or slight swelling," the news release reported.

The FDA cannot determine at this time if these hypersensitivity reactions are caused by the active ingredients benzoyl peroxide or salicylic acid or by other inactive ingredients. The FDA is planning to continue monitoring this issue and is working with manufactures to provide warning labels that alert consumers to the possibility of the dangerous reaction.

"Before using an OTC topical acne drug product for the first time, apply a small amount to one or two small affected areas for 3 days to make sure you don't develop any hypersensitivity symptoms. If no discomfort occurs, follow the directions on the Drug Facts label," the FDA reported.

Healthcare professional are urged to report adverse events or side effects to the FDA's MedWatch Safety Information and Adverse Event Reporting Program. Reports can be submitted at www.fda.gov/MedWatch/report.htm or forms can be requested at 1-800-332-1088.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.