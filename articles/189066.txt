NEW YORK (TheStreet) - Judging by the number of cheesy headlines coming from investment banking research departments this morning, it will be a good day for King Digital Entertainment (KING) after the company's shares tumbled as much as 30% since its late-March initial public offering.

>> Read More: King Digital's Candy Crush Saga Too Good to Be True

But a string of analyst upgrades, some with headlines that would enrage a business desk editor, simply aren't convincing. King Digital appears as speculative a company as it was when it went public. It also carries a market capitalization of nearly $6 billion even as post-IPO headwinds such as lockups on employee stock options and venture capital investors loom.

King Digital, given its business model and limited history of earnings may simply not be worth about as much as First Republic Bank (FRC) - Get Report, Goodyear Tire (GT) - Get Report, Frontier Communications (FTR) - Get Report or NasdaqI:IXIC.

According to Deutsche Bank "It's Good To Be The King." To be clear, they are talking about King Digital, the publisher of Candy Crush Saga, and not Burger King Worldwide (BKW) .

Analysts at the bank, which was an underwriter of King Digital's IPO, believe that Candy Crush Saga peaked in the third quarter, but that the company's installed base is growing at compound annual rate of 24%.

If King Digital can prove more than a one-hit wonder after Candy Crush, for instance the company's Farm Heroes Saga, an apparent rip-off of Zynga's (Z) - Get Report declining FarmVille game, Deutsche Bank believes the company's shares are undervalued. A slower than expected decline rate of King Digital's games, and growth opportunities in Asian countries like Japan, South Korea and China also present opportunities, according to analysts at the firm.

"We see meaningful optionality from new geographies," Deutsche Bank states. They give King Digital shares a $27 price target.

Credit Suisse analysts, citing a twist to a popular tee shirt slogan created by the British Ministry of Information during World War II, note investors should "Keep Calm and Play On."

Analysts at the firm believe King Digital's mobile games will have a total addressable market of $100 billion by 2017 and that the company's valuation is compelling. Even under an assumed 90% decay rate of King Digital's addictive Candy Crush Saga game, Credit Suisse finds value in the company's shares.

"Our sensitivity analysis for the implied multiples for not only King in its entirety but also on the non-Candy Crush assets suggest that KING shares even under the most egregious decay assumptions (90% linear decay rate from 2014 to 2015 and onward) are cheap at ~7x EV/EBITDA on 2015 estimates," Credit Suisse says. They give King Digital a $28 price target.

Barclays analysts initiated King Digital with a $23 price target and the hypothesis that the company's current valuation is compelling.

"In an industry characterized by heavy competition and low barriers to entry, we believe King stands out as a business that has created a sustainable competitive advantage through an industry-leading player network that it can leverage to cross-promote its new games and further diversify its revenue mix," Barclays analysts wrote.

They believe Farm Heroes Saga will begin to show up in King Digital's earnings, helping investors to look past Candy Crush and think about the big picture. With over 300 million monthly unique users and a valuation of just 5.2 times 2014 earnings before interest, depreciation, amortization and taxes, they see upside.

The Reality

There is little compelling new information in King Digital's flurry of upgrades on Monday morning. Valuations of King Digital still rely almost exclusively upon revenue and profits the company earns from Candy Crush Saga, which as analysts state, have peaked.

Meanwhile, the notion that King Digital has transcended a low-barrier of entry business model is highly questionable. Weren't bullish analysts saying the same thing about Zynga after its IPO?

Last we checked, the Apple (AAPL) - Get ReportiTunes store and Google (GOOG) - Get ReportPlay store are rife with apps encountering a meteoric rise and those that look like falling stars.

Furthermore, King Digital has done little to diversify its earnings. As TheStreetnoted when first analyzing King Digital's IPO documents, the company derives pretty much all of its revenue from 4% of its monthly users, or a bit over 12 million paying users.

That analysts even discount forward analysis of those paying users seems highly questionable. Meanwhile, valuations hinge on assumptions such as a 2% terminal growth for King Digital.

King Digital shares surged as much as 5% on Monday on a flurry of bullish research notes from the company's underwriters. However, there was little substance or new information presented in those reports, and assumptions that the company is undervalued based on 2015 consensus seem a bit abstract.

Bottom Line: In the wake of King Digital's IPO and a wave of bullish analyst reports, the only certainty is the cash in the company's bank account. After raising $326 million in net proceeds in an IPO that valued the company at over $7 billion, the King hasn't left the building... How's that for a terrible pun.

>> Read More: King Digital's Candy Crush Too Good to Be True

>> Read More: Zynga, Struggling to Remain Relevant, Returns to FarmVille

-- Written by Antoine Gara in New York.

Follow @AntoineGara