The annual Lyrid meteor shower peaks this year on the morning of Tuesday, April 22 (also Earth Day).

It should also be visible for the rest of the week.

Advertisement

The shower happens as Earth passes through a stream of debris from the comet Thatcher, which makes a full orbit of the sun every 415 years. The debris field produces up to 20 meteors per hour during the shower's peak, according to NASA Video footage of a possible meteor flying over Russia , captured by dashcams early Saturday morning, is believed to be an early visitor from the shower.

The meteor shower gets is name because the space rocks stream out from a point to the right of Vega, which is the brightest light in the constellation Lyra.

Advertisement

Unfortunately, the half-moon will brighten the sky, making it more difficult to see meteors, especially very dim ones. But the moonlight shouldn't discourage you from looking up.

Advertisement

The best viewing "will be midnight until dawn on the morning of April 22, provided you have clear, dark skies away from city lights," NASA said . "Northern Hemisphere observers will have a better show than those in the Southern Hemisphere."

If you can't step outside, there are two options for viewing the meteor show online. The Slooh Space camera will be live streaming a view of the Lyrids, weather permitting. The broadcast starts at 8 p.m. EDT.

Advertisement

NASA will also be live streaming the meteor shower . Their live webcast begins at 8:30 p.m. EDT.

If you take awesome pictures of the Lyrid meteor shower, send them to dspector@businessinsider.com and we'll share them here.