NEW YORK -- Walter Dean Myers, a bestselling and prolific children's author and tireless champion of literacy and education, has died. He was 76.

Myers, a longtime resident of Jersey City, New Jersey, died Tuesday at Beth Israel Medical Center in Manhattan after a brief illness, publisher HarperCollins announced.

A onetime troublemaker who dropped out of high school, the tall, soft-spoken Myers spent much of his adult life writing realistic and accessible stories about crime, war and life in the streets. He wrote more than 100 books, his notable works including "Monster" and "Lockdown," and was the rare author -- black or white -- to have a wide following among middle-school boys. He was a three-time National Book Award nominee, received five Coretta Scott King awards for African-American fiction and in 2012-13 served as National Ambassador for Young People's Literature, a position created in part by the Library of Congress.

Well before that, he travelled the country, visiting schools and prisons and libraries.

"He wrote with heart and he spoke to teens in a language they understood. For these reasons, and more, his work will live on for a long, long time," Susan Katz, president and publisher of HarperCollins Children's Books, said in a statement.

Myers' books were usually narrated by teenagers trying to make right choices when the wrong ones were so much easier. There was the 17-year-old hiding from the police in "Dope Sick," or the boarding school student in "The Beast" who learns his girlfriend is hooked on drugs. He is careful not to make judgments, and in the crime story "Monster" left doubt over whether the narrator was really guilty.

One of five siblings, he was born Walter Milton Myers in Martinsburg, West Virginia, in 1937. His mother died when he was 18 months old, and he was sent up to Harlem and raised in a foster home by Herbert and Florence Dean, a janitor, and a cleaning woman and factory worker, respectively. In honour of his foster parents, he took the pen name Walter Dean Myers.

Over 6 feet tall by middle school, he was a basketball star, but also a stutterer who was teased often and often fought back in return. Meanwhile, back home, he was happy to sit quietly and read.

"There were two very distinct voices going on in my head and I moved easily between them," Myers wrote in his memoir, "Bad Boy," which came out in 2001. "One had to do with sports, street life and establishing myself as a male. ... The other voice, the one I had from my street friends and teammates, was increasingly dealing with the vocabulary of literature."

Myers was gifted enough to be accepted to one of Manhattan's best public schools, Stuyvesant. But he was also shy, too poor to afford new clothes and unable to keep up with the work. Myers began skipping school for weeks at a time and never graduated.

"I know what falling off the cliff means," he told The Associated Press in 2011. "I know from being considered a very bright kid to being considered like a moron and dropping out of school."

He served three years in the military, and later was employed as a factory worker, a messenger on Wall Street and a construction worker. Anxious to be a writer, he contributed to Alfred Hitchcock's mystery magazine and numerous sports publications. When his half-brother Wayne was killed in Vietnam, he wrote a tribute for Essence magazine. His first book -- "Where Does the Day Go?" -- was published in 1969 after he won a contest for children's literature by people of colour.

His visits with students and inmates not only gave him the chance to help others straighten out their lives, but also inspired some of his work. "Lockdown," a National Book Award finalist, began after Myers met a kid who was afraid to get out of jail because he would only get in trouble again. For "Monster," he remembered a boy who would talk about the crimes he committed in the third person, as if someone else had committed them.

"Then I found out that all the guys could do that. They could separate themselves from their crimes," Myers told the AP. "We come up with strategies for dealing with our lives and my strategy might be different because my life has been different."

Myers' novel "On a Clear Day" is scheduled to come out in September.

Survivors include his wife, Constance, and two sons. A daughter, Karen, died earlier.