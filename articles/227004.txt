Analysts at Evercore Equity Research suggest that AT&T Inc. (NYSE:T)’s proposed merger with DIRECTV (NASDAQ:DTV) will impact the trajectory and overall nature of the firm’s capital expenditure before and after the deal.

In a recent note to investors, Evercore analysts Mark McKechnie and Zachary Amsel speculated that the impending transaction already has already impacted the spending trajectory of AT&T Inc. (NYSE:T).

Combined company becomes second largest spender

According to the analysts, the proposed DIRECTV (NASDAQ:DTV) merger might have already impacted the front-end loaded Q1 spend of AT&T Inc. (NYSE:T) and increased the urgency towards its Domain 2.0 Network Function Virtualization (NFV) architectural shift.

McKechnie and Amsel noted that the combined AT&T Inc. (NYSE:T) and DIRECTV (NASDAQ:DTV) capex is ~$25 billion—making the combined company the second largest global spender behind China Mobile Ltd. (ADR) (NYSE:CHL) (HKG:0941) ~$35 billion.

AT&T to accelerate NFV initiative

McKechnie and Amsel also suggest that AT&T Inc. (NYSE:T) will accelerate its NFV initiatives to operate more of its network on data centers. According to the analysts, they “envision a video cloud–essentially data centers which ingest and store video.” They emphasize that the video cloud enabled IP video distribution across AT&T’s last mile connections including 4G/Wireless, Fiber-and-4G connected homes and businesses.

The analysts said the key will be the ability of AT&T Inc. (NYSE:T) to consolidate its billion and policy across its fixed, wireless and broadband customers and monetize different qualities of service through content provider relations.

McKechnie and Amsel said, “Bottom line – look for a shift in spending towards data center architectures (switches, storage and servers) in line with its Domain 2.0/NFV plans.” They noted that Ericsson (ADR) (NASDAQ:ERIC) was chosen as key integration partner (OSS/BSS Telcordia) and Juniper Networks, Inc. (NYSE:JNPR) as the first NFV/Data Center supplier. The analysts assumed that JNPR will supply QFabric DC switches and Contrail SDN controllers. They also believed that Cisco Systems, Inc. (NASDAQ:CSCO) will also bid for AT&T’s Data Center switches. Below is the estimated exposures to AT&T Inc. (NYSE:T)

Last mile investments

The analysts suggested that AT&T Inc. (NYSE:T) will step up its “broadband over 4G” efforts as an alternative last mile link in rural areas. They noted that AT&T’s plan for an incremental 15M broadband locations over the next four years and a broader video offering to business customers.

In additions, McKechnie and Amsel speculated that AT&T Inc. (NYSE:T) might choose Alcatel Lucent SA (ADR) (NYSE:ALU) (EPA:ALU) as key supplier for its FTTH initiative and 4G LTE network and Ciena Corporation (NYSE:CIEN) as supplier for its FTTB/transport project, and Ericsson (ADR) (NASDAQ:ERIC) as 4G LTE RAN supplier.

Furthermore, the analysts said that they will not be surprised to see Google Inc (NASDAQ:GOOG) (NASDAQ:GOOGL) step up its Google Fiber efforts in response.

Near-term concern

According to the Evercore analysts, the uncertainty of the deal could cause a temporary disruption in capex spending. They expect AT&T Inc. (NYSE:T) to have a flat-to-down capex number for the June quarter.