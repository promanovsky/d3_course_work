Chris Pratt takes the starring role of space adventurer Peter Quill. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LOS ANGELES, Aug 1 — In the world of superheroes, the ragtag “Guardians of the Galaxy” are a bit less polished than the A-list “Avengers”, but they are nevertheless set to conquer US cinemas this weekend.

The latest film featuring heroes from the Marvel universe — due to hit North American screens today — is a bit of a risk for Disney, which now owns the stable of comic book characters.

Rather than beloved superheroes such as Iron Man, Captain America and Thor — all headliners of successful films and the wildly popular “Avengers” movie — it features lesser-known adventurers, including a gun-toting raccoon voiced by Oscar nominee Bradley Cooper.

But that has not stopped filmgoers from snapping up advance tickets, setting “Guardians” on course to be one of the runaway blockbusters of the summer.

According to Hollywood industry journal Variety, the film has already set a record for advance online ticket sales for an August release, and is on track to rake in about US$65 million (RM207.8 million) in its opening weekend.

So far this summer, the fourth instalment in the “Transformers” series has earned the top spot for a debut weekend with US$100 million in ticket sales.

But Disney has high hopes for “Guardians” and has already given the green light to a sequel for summer 2017.

Early reviews have been positive. Entertainment Weekly calls it a “giddily subversive space opera that runs on self-aware smart-assery”. The New York Times says director James Gunn has given the film “a pulse, wit, beauty and a real sensibility”.

The live-action animation mash-up recounts the space adventures of Peter Quill (played by Chris Pratt) who, when faced with the destructive plans of the evil Ronan the Accuser, allies with four aliens to try and save the galaxy.

Ragtag team of heroes

“They’re such a ragtag group of people, a group of misfits,” says professional wrestler and mixed martial arts star Dave Bautista, who plays the muscular Drax the Destroyer, a character hell-bent on revenge over the murder of his family.

“It’s cool — I think people are going to relate to them much easier than they would with Iron Man or Thor or Captain America. I think they’re just going to be able to relate to these characters,” he told AFP.

He describes Drax as “a very literal character, meaning he takes every thing you say very literally and in turn, he means everything that he says”.

Along with Quill and Drax, the Guardians include the assassin Gamora, played by Zoe Saldana who — after going blue for her breakout role in James Cameron’s “Avatar” — is this time green from head to toe.

The top-flight cast for the comedy also includes Cooper — a two-time Oscar nominee for his work in “Silver Linings Playbook” and “American Hustle” — who goes off-camera this time to voice the obscenity-spewing raccoon Rocket, and Vin Diesel playing the tree-like creature Groot.

The interaction between the titular Guardians “is a little rough at first... because they definitely are not friends or looking to be friends in this movie”, Bautista says.

“Towards the end, they come together as a group for the greater cause, but they also kind of learn to love one another and rely on one another.” — AFP