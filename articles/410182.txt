NEW YORK (TheStreet) -- Chevron Corp. (CVX) - Get Report today said asset sales and demand for crude oil increased second quarter earnings, although its crude oil production fell.

The oil major reported earnings of $5.67 billion, or $2.98 a share, up from $5.37 billion, or $2.77 a share, a year ago.

Revenue was up nearly 1% to $57.94 billion.

Analysts polled by Thomson Reuters forecast earnings of $2.66 a share and revenue of $59.3 billion.

Chevron said its global oil equivalent production for the quarter fell to 2.55 million barrels a day from 2.58 million a year ago, as planned maintenance activity took place at Tengizchevroil in Kazakhstan.

Shares of Chevron are currently down -0.45% to $128.66.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates CHEVRON CORP as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate CHEVRON CORP (CVX) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its largely solid financial position with reasonable debt levels by most measures, attractive valuation levels, good cash flow from operations and increase in stock price during the past year. We feel these strengths outweigh the fact that the company has had somewhat weak growth in earnings per share."

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.