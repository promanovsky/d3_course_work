The launch of an unmanned Delta 2 rocket from Vandenberg Air Force Base in California was called off less than a minute before liftoff on Tuesday when the pad's water system failed, a live Nasa Television broadcast showed.

The rocket, built and flown by United Launch Alliance, a partnership of Lockheed Martin Corp and Boeing Co, was due to lift off at 2:56 a.m. PDT (5:56 a.m. EDT, 0956 GMT) from a launch pad that had not been used in nearly three years.

The pad's water system is needed in case of a fire and to help suppress potentially damaging acoustic vibrations from launch.

The rocket carries Nasa's $465 million Orbiting Carbon Observatory. Built by Orbital Sciences Corp, it is designed to measure where carbon dioxide, a greenhouse gas tied to climate change, is moving into and out of the atmosphere. United Launch Alliance had just 30 seconds to get the rocket off the launch pad to properly position the OCO satellite at the front of a train of polar-orbiting spacecraft that passes over Earth's equator at the same time every afternoon.

(Also Read: Nasa Launching Satellite Tuesday to Track Carbon Pollution)



"As we only have a 30-second launch window, launch will not be occurring this morning," said Nasa launch commentator George Diller.

The launch was tentatively rescheduled for Wednesday, but engineers first have to track down the cause of the water system problem, he added.

© Thomson Reuters 2014

