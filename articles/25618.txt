Fans of the popular and crushingly difficult, Flappy Bird, can sing songs of praises today as creator Dong Nguyen has confirmed that the game will return to the app store. It's what many mobile gamers have been waiting for ever since the game was pulled back in February of this year.

Nguyen was asked by a fan on Twitter on whether or not he plans to bring the game back to the various mobile stores it was on. To that, Nguyen replied; "Yes. But not soon."

Yes. But not soon. — Dong Nguyen (@dongatory) March 19, 2014

As to when players can expect to see Flappy Bird back on the app stores, we're not sure as Nguyen did not give a date. However, since he said, "soon," it is likely players might have to wait a very long time before Nguyen decides to end mobile gamer suffering.

In a recent interview with The Rolling Stone, Nguyen hinted at the possibilities of the game returning to the app stores. Though he did mention that should he choose to bring the game back, it would feature a warning message to inform players of how addictive Flappy Bird is. In fact, it's mainly due to the addictiveness why Nguyen removed the game in the first place.

Bring Flappy Bird back to the app stores is after over a month is not a surprising move. This tiny game alone makes Nguyen over $50,000 per day, and he is still making money despite the game's takedown from the various app stores.

The money he was making allows him to have the idea of purchasing an apartment and a car, so it is very easy to see why Nguyen is playing with the idea of returning the game from where it belongs.

No doubt Flappy Bird will once again return to the top of the rankings after being available for download once more. Millions who haven't played it before would want to see what the fuss is all about. Some might like it, and others may come to the realization that there's nothing unique about Flappy Bird.

It is understood that Nguyen is working on three other games. Kitty Jetpack, Checkonaut, and a cowboy shooter.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.