Miley Cyrus has a new tattoo in memory of her dead dog.

The 'Wrecking Ball' hit-maker, who was devastated when her Alaskan Klee Kai, Floyd, was killed by a coyote in April, unveiled the new inking on Instagram.

The tattoo features a drawing of her beloved pet's head with a speech bubble that reads, "With a little help from my fwends," under her left armpit.

Some of the 21-year-old star's friends also got matching tattoos of the dog at a house party on Saturday night, and Cyrus posted several other images of the process before unveiling the finished product with the caption: "#withalittlehelpfrommyfwends #housepartygreentonguefloydtats (sic)"

The 'We Can't Stop' singer already has several other tattoos, including a cat on the inside of her bottom lip, a portrait of her grandmother, a dream catcher, a quote by Theodore Roosevelt, a skull, the word 'Karma' and a mini replica of Leonardo da Vinci's anatomical heart drawing.

Cyrus, who erected a shrine for Floyd last month, welcomed a rough collie named Emu Coyne into her large family of dogs recently, but insisted he wasn't a replacement for Floyd.

She shared a snap of her new pet cuddling up to her on Instagram with the caption: "Time to start making Emu accounts smilers. He's here and here to stay. I welcomed Emu Coyne Cyrus into the family a couple days ago. Been keepin him a secret cause sometimes I'm weird like that.

"It's taken some time to be ready for this next step and loving again. There will never be anyone like Floyd. There was a bond that was so deep it's irreplaceable but that's not what I am trying to do. Never will I replace Floyd and that's something I had to take time to understand."

Emu joins Cyrus's three other canines, Mary Jane, a pitt bull, Happy, a rottweiler-beagle cross, and Bean, a chihuahua mix.