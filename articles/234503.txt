Anglo-Dutch consumer goods giant Unilever Plc (UN, ULVR.L,UL) Thursday said it would sell its North America pasta sauces under the Ragu and Bertolli brands to Mizkan Group, a privately held Japanese food manufacturer, for nearly $2.15 billion.

The sale which is expected to close by the end of June, includes a sauce processing and packaging facility in Owensboro, Kentucky, and a tomato processing facility in Stockton, California. Ragu and Bertolli's annual turnover is more than $600 million.

Commenting on the transaction, Kees Kruythoff, president of Unilever North America, said: "This sale represents one of the final steps in reshaping our portfolio in North America to deliver sustainable growth for Unilever, and enables us to sharpen our focus within our foods business. The Ragu and Bertolli business leads the pasta sauce category in the United States, and we believe that the potential of both brands can be fully realized with Mizkan."

For comments and feedback contact: editorial@rttnews.com

Business News