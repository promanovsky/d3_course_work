★★★ "Heaven Is For Real" (PG) "Viewers expecting 'Heaven Is For Real,' adapted from Todd Burpo’s best-selling memoir by the director Randall Wallace, to be an anodyne allegory of reassurance and unconditional faith are in for something a little bit edgier, and that turns out to be a good thing." – Ann Hornaday

AD

AD

★★1/2 "Alan Partridge" (R) "Partridge is such a fatuous, superficial figure that the trick is to make him palatable enough to sustain interest for more than an hour. The filmmakers meet with uneven success, putting him into a barely there wisp of a story, but allowing (Steve) Coogan’s own complicated mix of nastiness and disarming self-awareness to percolate through and carry the day." – Ann Hornaday

★★★ "Bears" (G) "Parents, take note: The movie is rated G, but there could be tears, protests and loud questions from sensitive young animal lovers — particularly during one nearly unendurable scene, when a male bear pokes through fallen logs in an attempt to eat one of the cubs. Don’t be surprised if older kids, on the other hand, want to Google 'filial cannibalism' the moment the credits roll." – Sandie Angulo Chen

★★1/2 "The Railway Man" (R) "It’s easier to like 'The Railway Man' than it is to love it. Despite solid performances by Colin Firth, Nicole Kidman and Stellan Skarsgard, and a handsome cinematic sheen burnishing the shocking, true-life tale of wartime torture and reconciliation, the film is less deeply affecting than merely admirable. It’s a good, slick and well-intentioned film that wants so hard to be an important one that the slight feeling of letdown it leaves is magnified." – Michael O'Sullivan

AD

AD

★ "Make Your Move" (PG-13) "If you want a dance drama, Duane Adler is the man for the job. He wrote 'Save the Last Dance,' 'Step Up' (and its many sequels), 'The Way She Moves' and 'Make It Happen.' Now he’s back at it, writing and directing 'Make Your Move,' a 'Romeo and Juliet'-inspired addition to his guilty pleasure niche. But he’s also venturing into another genre: unintentional comedy. Adler’s cheesy script was probably partially responsible for the giggles (and one snort) during dramatic scenes at a recent screening. But the casting doesn’t help." – Stephanie Merry

★★★ "Only Lovers Left Alive" (R) "As unlikely as it sounds in the era of 'Twilight' and its defanged imitators, Jarmusch’s 'Only Lovers Left Alive' proves there are still new sights and sounds and meanings to be derived from the conceit of characters who rarely sleep, never die and feast on the blood of others. In the hands of the godfather of late 20th-century American independent cinema, the sensory pleasures are extravagant, the approach both wry and profound, and the greater meaning well worth searching for, even within a tired, overworked genre." – Ann Hornaday