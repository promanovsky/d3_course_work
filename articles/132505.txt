The number of Americans filing new claims for unemployment benefits rose less than expected last week and remained near its pre-recession levels, offering further evidence of the economys underlying strength.

Initial claims for state unemployment benefits ticked up 2,000 to a seasonally adjusted 304,000 for the week ended April 12, the Labor Department said on Thursday. They stayed close to a 6-1/2 year low touched the prior week. Claims for the week ended April 5 were revised to show 2,000 more applications received than previously reported.

Economists polled by Reuters had forecast first-time applications for jobless benefits rising to 315,000.

The four-week moving average for new claims, considered a better measure of underlying labour market conditions as it irons out week-to-week volatility, fell 4,750 to 312,000, the lowest level since October 2007.