Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Doctors fear Ebola victim Patrick Sawyer may have sparked a worldwide spread of the killer disease after being allowed on two flights while infected.

And tonight a desperate race was on to find dozens of passengers who flew on the same jets as the 40-year-old American.

British doctors and border officials have been warned to be on the lookout for people in the UK showing signs of the disease.

Mr Sawyer was allowed to board an ASKY Airlines flight in Liberia, where Ebola is rife, despite vomiting and suffering from ­diarrhoea. His sister was recently killed by the virus.

He had a stopover in Ghana then changed planes in Togo and flew to the international travel hub of Lagos in Nigeria. The dad-of-three died five days after arriving in the city.

Lancaster University virologist Derek ­Gatherer said passengers, crew and airport ground staff who came into contact with Mr Sawyer could be in “pretty serious danger”. Ebola is fatal in 90% of cases.

Doctors have identified 59 people who were near him and have tested 20. But they are struggling to find the others, who could have flown to anywhere in the world from Lagos.

Update: Doctors are reportedly investigating the first possible case of the Ebola virus in Hong Kong. A man has also been tested at a hospital in Birmingham and has been given the all clear. Get latest news in our live coverage and on our dedicated page.

There were today questions over how Liberian government worker Mr Sawyer was let on flights while clearly showing symptoms of Ebola – which has killed 672 people in Liberia, Guinea and Sierra Leone since it broke out in February.

Experts from Public Health England have met UK Border Agency officials to make sure staff are aware of the signs to look for in Ebola sufferers.

PHE has also used its national medical alert system to advise all UK doctors to “remain vigilant for unexplained illness in those who have visited the affected area”.

Symptoms include vomiting and ­diarrhoea, fever, weakness, headache and sore throat.

Those struck down can also suffer internal and external bleeding. The virus is spread through human contact. There is no cure.

(Image: Getty)

PHE director of global health Dr Brian McCloskey described the Ebola outbreak as the most “acute emergency” facing Britain.

The expert said he had briefed David Cameron.

He added: “When these things start to escalate we work with everybody to ensure they are aware of what needs to happen.”

Tonight, it emerged one of Sierra Leone’s top doctors, Sheik Umar Khan – who has treated more than 100 Ebola patients – had died from the disease.

ASKY airline, which operates from West Africa, said it is halting flights to Liberia and Sierra Leone because of the crisis.

And Liberian football chiefs banned all games in a bid to stop Ebola spreading through player contact.

Meanwhile a doctor who treats patients with the virus is “terrified” after catching the disease himself.

(Image: Reuters)

US medic Kent Brantly, 33, fell ill despite wearing full protective gear.

He wrote in an email to a former colleague in Texas: “I’m praying fervently God will help me survive this disease.”

Dr Brantly is being treated in an isolation unit in the Liberian capital of Monrovia. He moved to the country in October to work with a Christian group.

Wife Amber and their two young children had returned to the US for a trip last week before he found out about his condition. They have shown no signs of the highly contagious viral infection.

His proud mother Jan, 72, said: “Kent prepared to be a lifetime medical missionary. His heart is in Africa.”