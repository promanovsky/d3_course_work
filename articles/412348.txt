Statistics Canada says municipalities issued building permits worth $8 billion in June, up 13.5 per cent from May.

The agency says the increase in June resulted primarily from higher construction intentions for institutional and industrial buildings in Quebec and commercial buildings in Alberta.

It says the value of non-residential building permits was up 32.5 per cent to $3.8 billion in June, the third consecutive monthly gain.

Meanwhile, the value of residential building permits was up 0.4 per cent to $4.2 billion, a fourth consecutive monthly increase.

Gains were posted in five provinces in June, led by Quebec with Alberta trailing a distant second, while the biggest drop was in Manitoba.

The agency says the value of permits was up in 20 of the 34 census metropolitan areas in June.