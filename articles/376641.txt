EDGEWATER, Md. (WJZ)– Chemical exposure. Anne Arundel County campers were sickened by fumes at a pool.

Rochelle Ritchie has the latest on this scary situation.

Several of the swimmers were taken to Anne Arundel Medical Center. One person was treated and released. The 18 people who were taken to Baltimore Washington Medical Center have also been treated and released.

Abandoned backpacks and swim gear remain at the Edgewater YMCA pool after swimmers, including children as young as six years old, were exposed to dangerous levels of chemical compounds in the water.

“Of the two chemicals in use in the metering system, sodium hypochlorite and muriatic acid,” said Capt. Michael Pfatzgraff.

Officials with Anne Arundel County fire and rescue say 31 children and four of their camp counselors started coughing profusely and vomiting after being exposed to the chemical’s vapors.

Anne Arundel County officials say the victims were immediately removed from the pool and brought to the showers, where they were decontaminated and taken to the hospital.

“Thirteen children with serious but non-life threatening injuries and thirteen children with minor injuries,” said Pfatzgraff.

The incident is still under investigation and the gates of the pool remained closed pending an inspection.

The vice president of operations say Tuesday night’s storm may be to blame.

“We had a situation where the pump went out, probably due to the storm that took place last night and then what happened was, the vapor of the chlorine was left in the pipe,” said Carla Larrick, YMCA VP.

But the HAZMAT team determined chlorine was not a factor. Instead, sodium hypochlorite—which is used to disinfect the water—and muriatic acid—which helps to lower the PH balance in the pool.

Thirty-five people were taken to various hospitals, including Anne Arundel Medical Center.

“The two sickest children had more respiratory distress and required closer observation than we could provide…so we moved them up to Johns Hopkins,” a doctor said.

He also said that antibiotics do nothing in a case like this and that the body will naturally heal itself but that those exposed were given oxygen and will be monitored through the night.

Howard County emergency personnel also assisted in this incident.

Other Local News:

[display-posts category=”local” wrapper=”ul” posts_per_page=”5″]