The French government is scrambling to stickhandle its way through a complex tie-up that could see iconic engineering firm Alstom be broken up or sold outright to either a German or an American suitor.

Rumours of a bid from U.S.-based conglomerate General Electric surfaced Friday, with a price tag of $14 billion for Alstom's lucrative power generation assets. Alstom shares have been halted on Paris's Stock Exchange ever since.

But there's also another player in the works, with German-based conglomerate Siemens AG reportedly looking to buy those same assets. Siemens tried to buy Alstom about a decade ago but was rebuffed by the government at the time, and over the weekend the company reportedly mounted another bid to counter GE's actions.

Late Monday, Reuters reported that Siemens would table a formal offer on Tuesday, citing a source close to those negotiations.

Siemens is offering comparable money, plus an agreement to transfer its train assets to Alstom. That would be enough to make Alstom leapfrog past Bombardier to become the second-largest train manufacturer in the world. (Alstom was a pioneer in the modern train industry with its high-speed TGV line, which it has since exported to many other countries.)

In exchange, Siemens would become Europe's largest power generator.

President Francois Hollande is meeting Monday with the three CEOs in an all-out effort to orchestrate a deal for the company that would keep the engineering-portion of the company firmly rooted at home. He held an hour-long meeting with Jeff Immelt, the CEO of General Electric Co., which was preparing a merger with Alstom before the government intervened, citing "strategic interests."

Two officials close to the talks said Hollande was open to the deal, despite Economy Minister Arnaud Montebourg's decision to halt it until the government could review the sale. The officials spoke only on condition of anonymity because the talks are sensitive.

"We realize that the stockholders have an interest, but the government also has interest and ours are for economic sovereignty," Montebourg, whose job includes industrial renewal, told RTL radio on Monday. "French companies are not prey."

Montebourg, an outspoken Socialist who has scuttled deals before, acknowledged that the government had practically no stake in Alstom but said there was "strategic national interest" in making sure the company, which pioneered TGV high-speed trains, later exporting them around the world, and builds power generating turbines.

In a statement, GE said the meeting between Hollande and its CEO was "open, friendly and productive."