Twitter's messaging service will have close to 400 million global users by 2018, mostly in Asia, Latin American and the Middle East, consulting firm eMarketer estimated on Tuesday, far short of the 1 billion once expected.

The new study by the digital advertising industry research firm suggests that Twitter's user growth will have plateaued in major developed markets within five years.

By 2018, user growth in Twitter's key markets of the United States and Japan will have steadily declined to 6.4 percent and 6.1 percent, respectively, while its global user count will have reached just 386.9 million users, eMarketer said.

The United States accounts for three-quarters of Twitter's revenue, the messaging service disclosed last year.

Read MoreFood for thought: How to stop world hunger

Once championed by its Silicon Valley boosters as "the next Facebook," capable of reaching more than 1 billion users, Twitter has instead grappled with stagnant user growth in its first two quarters as a public company, with its stock trading near its post-IPO low.

But eMarketer said the social media company has "significant potential" in emerging markets, with user growth forecast to accelerate in Asia. India and Indonesia are primed to surpass the United Kingdom with the third- and fourth-largest Twitter populations this year, the research firm said.

Twitter, which eMarketer said could see nearly 60 percent user growth in India this year, played a role in the recently concluded elections.

Analysts consider Twitter's reliance on developing countries as a weakness, because digital advertising prices remain far lower in emerging markets.

Read MoreTwitter CEO: New users just as engaged as old users

One unknown factor that could skew the forecasts is a change in Twitter's status in China, where Twitter and Facebook are banned, eMarketer said.

Twitter Chief Executive Dick Costolo visited China for the first time in March and met with local government officials. The company dismissed the likelihood of launching operations there, and the Chinese state media downplayed the visit.

— By Reuters