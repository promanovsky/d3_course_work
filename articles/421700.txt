Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

While a lot of celebs have used the Ice Bucket Challenge to show off their abs and cleavage and what not, Matt Damon has used it for the greater good.

Of course the challenge was already for charity but the actor decided to use it to raise awareness for another issue – the lack of clean water available in developing countries around the world.

And what better way to do that than to throw toilet water all over yourself? Seriously that’s what he did and while normally this kind of thing would gross us out, it has in fact made our heart swell at his goodwill and excellent attitude.

In the video Matt says his nomination, which was from Ben Affleck and Jimmy Kimmel FYI, was slightly awkward for him due to his charity work.

He said: “It posed kind of a problem for me not only because there’s a drought here in California but because I co-founded Water.org and we envision a day when everybody has access to a clean drink of water.

“And there are about 800 million people in the world who don’t – so dumping a clean bucket of water on my head seemed a little crazy.”

The Good Will Hunting star can then be seen bent down over the seat taking water from the bowl and putting it in the bucket.

Handsome Matt turns to the camera and says: “This is truly toilet water, I’ve been collecting it from various toilets around the house.

“For those of you like my wife who think this is really disgusting, keep in mind that the water in our toilets in the West is actually cleaner than the water that most people in the developing world have access to. So hopefully this will highlight the face this is a big big problem and together we can do something about it.”

Matt then proceeded to throw the water on him, much to both his and the lady filming’s amusement.

Once he was thoroughly wet and covered in the lovely toilet water, showing off his hard abs in the process, he suddenly remembered he had to nominate .

He picked his “favourite actor” George Clooney, Bono his “favourite musician” and Tom Brady the “greatest quarterback of all time”.

We’re looking forward to seeing George and Tom get wet, Bono not so much (yes, we know it's for charity but still).