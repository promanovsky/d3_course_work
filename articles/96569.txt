PERTH, Australia -- Authorities are confident that a series of underwater signals detected in a remote patch of the Indian Ocean are coming from the missing Malaysia Airlines plane's "black boxes," Australia's prime minister said Friday.

But another Australian official said Friday there had been "no major breakthrough" in the search.

Prime Minister Tony Abbott told reporters in Shanghai, China, that crews hunting for Flight 370 have zeroed in on a more targeted area in their search for the source of the sounds, first heard on Saturday.

"We have very much narrowed down the search area and we are very confident that the signals that we are detecting are from the black box on MH370," Abbott said.

"Nevertheless, we're getting into the stage where the signal from what we are very confident is the black box is starting to fade," he added. "We are hoping to get as much information as we can before the signal finally expires."

The plane's black boxes, or flight data and cockpit voice recorders, could help solve the mystery of why the Boeing 777 veered so far off course when it vanished on March 8 on a trip from Kuala Lumpur, Malaysia, to Beijing with 239 people on board. But the batteries powering the devices' locator beacons last only about a month - and it has been more than a month since the plane disappeared.



The hunt for Flight 370 "has sparked the most expensive search and rescue operation in aviation history," the Reuters news agency points out.

Finding the black boxes after the beacons blink off will be extremely difficult because the water in the area is 15,000 feet deep.

The Australian ship Ocean Shield, which is towing a U.S. Navy device that detects black box signals, first picked up two underwater sounds on Saturday that were later determined to be consistent with the pings emitted from the flight recorders. The ship's equipment detected two more sounds in the same general area on Tuesday.

"We are confident that we know the position of the black box flight recorder to within some kilometers, but confidence in the approximate position of the black box is not the same as recovering wreckage from almost 4-1/2 kilometers beneath the sea or finally determining all that happened on that flight," Abbott said.

An Australian air force P-3 Orion, which has been dropping sonar buoys into the water near where the Ocean Shield picked up the sounds, detected another possible signal on Thursday, but Angus Houston, who is coordinating the search for Flight 370 off Australia's west coast, said in a statement that an initial assessment had determined it was not related to an aircraft black box.

The sonar buoys each have a hydrophone listening device that dangles about 1,000 feet below the surface that transmits its data via radio back to a search plane, Royal Australian Navy Commodore Peter Leavy explained.

The Ocean Shield was still using its towed pinger locator to try to locate additional signals on Friday, and the Orions were continuing their hunt, Houston said. The underwater search zone is currently a 500-square-mile patch of the ocean floor, about the size of the city of Los Angeles.

"It is vital to glean as much information as possible while the batteries on the underwater locator beacons may still be active," Houston said in a statement.

The searchers are trying to pinpoint the location of the source of the signals so they can send down a robotic submersible to look for wreckage and the flight recorders. Houston said on Friday that a decision to send the sub could be "some days away."

The Bluefin 21 submersible takes six times longer to cover the same area as the ping locator being towed by the Ocean Shield and would take six weeks to two months to canvass the current underwater search zone.

Meanwhile, Houston's coordination center said the area to be searched for floating debris on Friday had been narrowed to 18,036 square miles of ocean extending from 1,400 miles northwest of Perth. Up to 15 planes and 13 ships were involved in Friday's search.

Thursday's search of a 22,300 square mile area of ocean in a similar location reported no sightings of potential wreckage, the center said.

"On the information I have available to me, there has been no major breakthrough in the search for MH370," Houston said.

Separately, a Malaysian government official said Thursday that investigators have concluded the pilot spoke the last words to air traffic control, "Good night, Malaysian three-seven-zero," and that his voice had no signs of duress. A re-examination of the last communication from the cockpit was initiated after authorities last week reversed their initial statement that the co-pilot was speaking different words.

The senior government official spoke on condition of anonymity because he wasn't authorized to speak to the media.

Investigators believe the plane went down in the southern Indian Ocean based on a flight path calculated from its contacts with a satellite and analysis of its speed and fuel capacity.

The hunt for the black boxes has grown increasingly urgent, given that the batteries powering their beacons are likely to fail soon, if they haven't already.

An Australian government briefing document circulated on Thursday among international agencies involved in the search said the acoustic pingers likely would continue to transmit at decreasing strength for up to 10 more days, depending on conditions.

Once there is no hope left of hearing more sounds, the Bluefin sub will be deployed.

Complicating matters, however, is the depth of the seabed in the search area. The sounds detected earlier are emanating from 15,000 feet below the surface, which is the deepest the Bluefin can dive.

The search coordination center said it was considering options in case a deeper-diving sub is needed.