In its opening weekend Shailene Woodley’s new film The Fault in our Stars has taken over $48 million. Directed by Josh Boone and starring Woodley and Willem Defoe, the book is based on the young adult novel of the same name. Centred around Hazel, a 16-year-old cancer patient, played by Woodley, and the relationship she forms with 17-year-old Augustus Waters, an amputee and ex-basketball player, the film is widely considered to be the winner of tear-jerker of the year.

Shailene Woodley has been highly praised for her performance as Hazel in The Fault in our Stars

Woodley has been praised for her performance while the film on the whole has won over the critics. Rolling Stone’s Peter Travers wrote in his review of the film: “It’s a fresh, lively love story, brimming with humor and heartbreak, and lived to the heights by Shailene Woodley, a sublime actress with a resume that pretty much proves she’s incapable of making a false move on camera.”

MORE: Chloe Grace Moretz listens to Columbine 911 call before stage performance

The film currently holds an 82% fresh rating on Rotten Tomatoes, a surprisingly high score for a chick flick based on a young adult novel. There’s definitely something about The Fault in our Stars that has won over critics and moviegoers.

Before Boone and co get too comfortable with their tear jerker no.1 spot they should look into the film that could be a serious contender for the top spot. Due for release in August this year, If I Stay is yet another film adaption of a young adult novel, but unlike your action flicks like The Hunger Games, Divergent and The Maze Runner, it’s more along the lines of The Fault in our Stars AKA another one that’s bound to tug at your heartstrings.

Based on the 2009 novel by Gayle Forman, If I Stay follows the story of Mia, a 17-year-old girl involved in a car crash with her family. While in a coma Mia has an out-of-body experience where she sees her friends and family, as well as Adam, her handsome musician boyfriend, gather around her bedside. Seeing memories of her life as well as the life she could leave behind, Mia must decided whether to wake up to a life that she had never imagined would be hers, or let herself die.

If I Stay stars 17 year old Chloe Grace Moretz as Mia

Starring Kick-Ass’s Chloe Grace Moretz as Mia, the If I Stay trailer is already gathering a lot of attention. One YouTube commenter wrote, “this brings me to tears. i have to read the book.”

We’re recommending buying a box of Kleenex before going to see either movies.

Do you think If I Stay will usurp the tear-jerker top spot from The Fault in our Stars?

MORE: The Fault in our Stars beats Edge of Tomorrow to top North American box office

Click here to watch the trailer for If I Stay