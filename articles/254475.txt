Celebrity

Rap Genius co-founder and CEO Tom Lehman announces that Moghadam has resigned from the company after he was accused of making insensitive comments about the UCSB shooter.

May 27, 2014

AceShowbiz - Mahbod Moghadam, a co-founder of Rap Genius, has been let go after some people said that his comments on manifesto written by Elliot Rodger, a 22-year-old man who killed six people in a shooting spree near the University of Santa Barbara on Friday, May 23, were insensitive and creepy. The company's co-founder and CEO Tom Lehman announces Mogadham's resignation on the company's site.

"Mahbod Moghadam, one of my co-founders, annotated the piece with annotations that not only didn't attempt to enhance anyone's understanding of the text, but went beyond that into gleeful insensitivity and misogyny. All of which is contrary to everything we're trying to accomplish at Rap Genius," the statement read. "In light of this, Mahbod has resigned - both in his capacity as an employee of the company, and as a member of our board of directors, effective immediately."

Moghadam previously received backlash after writing some annotations about Rodger's manifesto. Moghadam praised some passages in Rodger's manifesto, calling them "beautifully written." He also commented that Rodger's sister must be "smokin hot."

Moghadam quickly apologized for his comments. In a statement to Valleywag's Nitasha Tiku, he explained, "I was fascinated by the fact that a text was associated with such a heartbreaking crime, especially since Elliot is talking about my neighborhood growing up."

"I got carried away with making the annotations and making any comment about his sister was in horrible taste, thankfully the rap genius community edits out my poor judgment, I am very sorry for writing it," he added.

Rodger, the son of an assistant director of "The Hunger Games", sent the manifesto to his therapist and parents before he went on the rampage last Friday in Santa Barbara, Calif. and killed himself. A video he recorded and uploaded on YouTube before the shooting shows him talking about loneliness, hatred to women and his "revenge against humanity, against all of you."