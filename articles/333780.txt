By Marcy Kreiter - Nestle USA recalled 10,000 containers of Haagen-Dazs ice cream because the carton is labeled Chocolate Chip Ice Cream while it actually contains Chocolate Peanut Butter Ice Cream.

The affected products involve 14-ounce cartons with a UPC of 74570-08400 and a lid labeled Haagen-Dazs Chocolate Peanut Butter along with a best buy date of 13May2015 and a manufacturing code of 24-52 4133580418D.

"These mismatched packages contain Chocolate Peanut Butter ice cream, and the ingredient statement on the carton applies to Chocolate Chocolate Chip Ice Cream and does not identify peanuts," Nestle said. "People who have an allergy or severe sensitivity to peanuts may run the risk of serious or potentially life-threatening allergic reaction if they consume this product."

The ice cream was produced May 13, 2014. It was distributed in the District of Columbia, Delaware, Florida, Maryland, North Carolina, New Jersey, New York, Pennsylvania, South Carolina, Virginia and West Virginia.

Consumers with peanut allergies are advised not to eat the ice cream. More information is available by calling 800-993-8924 or on the Haagen-Dazs Web site.