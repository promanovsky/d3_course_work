London: US drugs titan Pfizer is pursuing plans for a blockbuster merger with British rival AstraZeneca to fuel cancer treatments despite having a $US100-billion ($A108.07 billion) bid rejected, the pair revealed on Monday.

Pfizer chairman and chief executive Ian Read said that a tie-up between two of the world's biggest pharmaceutical groups would "help to fight some of the world's most feared diseases, such as cancer".

A logo sits on the labcoat of a Pfizer technician unit in Cambridge, United Kingdom. Pfizer has had its $US100 billion takeover bid of British rival AstraZeneca rejected. Credit:Bloomberg

It would benefit also the shareholders of both companies, he claimed in a Pfizer statement.

AstraZeneca hit back, arguing that the informal offer made by Pfizer in January "very significantly" undervalues the company.