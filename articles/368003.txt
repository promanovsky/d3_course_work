Advertising

The Sony Xperia Z2 is the current flagship smartphone from Sony. The handset sports some pretty impressive hardware specs and also shares some features of its predecessor, namely the ability to resist both water and dust, thanks to its IP58 rating. That’s great for those who accidentally drop their phones in the pool or spill their drinks on it, but what about the more hardcore users?

While we doubt most Xperia Z2 owners will ever find their phones encased in a block of ice, the folks over at Nixanbal have decided to put it to the test anyway, by freezing the Sony Xperia Z2 in an entire block of it, having it melt, and seeing if it continues to work after that. Interestingly and impressively enough, despite having been frozen for about 12 hours, it still works!

In fact they decided to test it out by calling the phone while it was encased in the block of ice, which was actually pretty cool since it did give off some pretty nice lighting effects. Now we can’t be sure if the folks at Nixanbal didn’t switch out the phones, but given the phone’s IP58 rating, we can believe that it is the real deal.

For those who are wondering about the IP-ratings, the first number denotes dust resistance while the second number denotes liquid protection. Unlike other phones such as the Galaxy S5 which sports an IP67 rating (meaning it is both water and dust resistant), the IP58 rating on the Xperia Z2 means that while it might not resist dust as well as the Galaxy S5, it will be able to withstand water immersion much better than the Galaxy S5.

In any case if you have a few minutes to spare, you can check out the video above to see it for yourself.

Filed in . Read more about Sony and XPERIA Z2.