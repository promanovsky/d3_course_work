Sales of previously-built homes jumped in May from the previous month, the highest monthly rise since August 2011, suggesting that the spring selling season won’t be a bust after a slow start.

The National Association of Realtors reported Monday that sales rose 4.9 percent to a seasonally adjusted rate of 4.89 million in May, the second monthly gain a row. Moderating home prices, a better job market, and a larger supply of homes are helping lure buyers to the market, along with a “temporary” decline in mortgage rates, the group said.

The sales are better than they were at the same point in 2009 through 2012, as the graph below illustrates. They even edged ahead of the market’s performance in May 2010, when a buying frenzy from a federal tax credit for first-time buyers was in place.

But last year's steady growth in sales was interrupted in 2014 and May sales were slower than they should be at this point in the recovery. Sales were 5 percent below where they were last year, when the housing recovery took off, in part spurred by investors who were buying foreclosures and other distressed properties at firesale prices. Based on the size of the population, a normal sales rate would be closer to 5.5 million.



Lawrence Yun, the Realtor group’s chief economist, said the biggest factor working against the recovery is the lack of homes for sale. The housing inventory is where it should be in a healthy market. If sales were to continue at the current pace, there would be a 5.6 month supply, the group said.

But potential home buyers are saying that there still isn't enough to choose from on the market, Yun said. The Realtors says builders are not breaking ground on enough homes.

“Realtors indicate there are ready buyers but not enough supply of homes to visit,” Yun said. “If we had strong inventory, the home sales would be stronger.”

The most acute shortage is in the lower price ranges, in part because there are fewer foreclosures on the market. But it be the thinning foreclosure inventory alone explain the dearth of homes below $1 million? Take a look at this scary break-down:

“There’s certainly a bifurcated recovery, and that’s been a continuing trend,” Yun said. The stock market’s stellar performance in recent years has helped drive sales in the upper brackets, while tight lending standards have been pushing down activity on the lower end. “The high-end market has impacted less by strict underwriting. They have better credit history, and generally higher credit scores.”

Earlier this month, Federal Reserve Chair Janet Yellen cited the credit score issue as one of the factors holding back the housing market: “It is difficult for borrowers without pristine credit to get mortgages,” she said.