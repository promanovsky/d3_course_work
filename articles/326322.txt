Big sodas can stay on the menu in the Big Apple after New York state’s highest court refused Thursday to reinstate the city’s first-of-its-kind size limit on sugary drinks. But city officials suggested they might be willing to revisit the supersize-soda ban.

The Court of Appeals found that the city Board of Health overstepped its bounds by imposing a 16-ounce cap on sugary beverages sold in restaurants, delis, movie theaters, stadiums and street carts.

“By choosing among competing policy goals, without any legislative delegation or guidance, the board engaged in lawmaking,” the court wrote in a majority opinion. “… Its choices raise difficult, intricate and controversial issues of social policy.”

Indeed, debate over the soda size cap pitted health officials who called it an innovative anti-obesity tool against critics who considered it paternalistic. Even a Court of Appeals judge, during arguments earlier this month, wondered aloud whether regulators would target triple-decker burgers next.