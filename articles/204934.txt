AUTOMOTIVE

Ford, Chrysler issue recalls over defects

Ford recalled almost 700,000 Escape SUVs and C-Max hybrid cars to fix a defect that could prevent air bags from deploying in a rollover.

The fix involves reprogramming vehicle software, not a mechanical repair, Dearborn, Mich.-based Ford said in a statement Friday. The action includes 627,275 Escapes and 65,192 C-Maxes from the 2013 and 2014 model years. It applies to vehicles sold in the United States, Canada and Mexico.

Also on Friday, Chrysler recalled 780,000 minivans to replace window switches that could short-circuit and overheat if exposed to moisture. Chrysler said it knows of 36 incidents related to the minivan’s faulty switches but is unaware of any related injuries or accidents. The recall involves certain 2010 through 2014 Dodge Grand Caravan and Chrysler Town & Country minivans.

Ford’s action is the latest in a series of recalls for the Escape and follows actions by multiple automakers over air-bag defects. General Motors, the largest U.S. automaker, is in the middle of recalling 2.59 million small cars, including the Chevrolet Cobalt and Saturn Ion, over a defective ignition switch that can lead to air-bag failure. That defect has been linked to at least 13 deaths.

In March, Nissan recalled almost 1 million cars, including the 2014 Altima sedan, because software can incorrectly classify a passenger seat as empty, leading to an air-bag failure. Faulty software also was cited in a petition to the U.S. National Highway Traffic Safety Administration asking for a defect investigation into the Chevrolet Impala.

Separately from the air-bag issue, Ford recalled 692,744 Escapes to fix an exterior door-handle that can bind, preventing the latch from working, the company said. The doors could open while driving or during a crash, and they may be difficult to close.

— Bloomberg News

Retail

Judge says Wal-Mart suit should continue

A U.S. judge said Wal-Mart does not deserve dismissal of a lawsuit claiming it defrauded shareholders by concealing suspected corruption at its Mexico operations, even after learning that a damaging news report detailing alleged bribery was being prepared.

U.S. Magistrate Judge Erin Setser in Fayetteville, Ark., on Thursday recommended denying Wal-Mart’s request to dismiss the lawsuit, led by a Michigan pension fund against the world’s largest retailer and former chief executive Mike Duke.

A Wal-Mart spokesman said the company disagrees with Setser’s recommendation, which is subject to review by U.S. District Judge Susan Hickey.

Wal-Mart’s stock price, which is not known as particularly volatile, fell 8.2 percent in the three days after the New York Times reported on its investigation into the alleged bribery on April 22, 2012. That decline wiped out roughly $17 billion of market value.

Plaintiffs, led by the City of Pontiac General Employees Retirement System, alleged that Duke and other Wal-Mart officials knew as early as 2005 that the Walmart de Mexico unit might have been bribing local officials to open stores faster but did not probe the matter adequately in 2005 and 2006.

They said Wal-Mart should have “come clean” in a quarterly report filed Dec. 8, 2011, soon after the company had learned of the Times’ investigation.

Instead, they said the report, known as a 10-Q, was a “phony demonstration of vigilance and virtue” that made it appear that Wal-Mart learned of suspected corruption in 2011, addressed it appropriately and reported its findings to the U.S. Department of Justice and the Securities and Exchange Commission.

— Reuters

Also in Business