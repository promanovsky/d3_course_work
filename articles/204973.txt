Fancy golf? Fancy a $1,500 device that can help you record your swing while you traipse around the fairways? If so, then you might have been one of Google's sales targets at this weekend's Players Championship PGA event at TPC Sawgrass in Ponte Vedra Beach, Florida. The event saw the first official debut of a retail display for Google Glass  the first such time the company has "sold the device in a public, face-to-face setting," reports Marketing Land.

News of the display first hit Twitter, where it was picked up by Business Insider. But don't read too much into our use of the word "display." We're not speaking of some kind of giant, cardboard-built presentational setup with lights and LCD screen showing off Google Glass and all that. In this case, the display was pretty much the following: A lit-up Glass sign overtop a standard table with boxes of Google Glass for sale and a few models unpacked for demonstration.

Simple, easy, efficient  and, apparently, lucrative. Sales for Google Glass devices were "much better than expected," reports Richard Ranick via Twitter.

Additionally, Google was also offering up a bit of a promotion for those interested in plunking down the not-so-inexpensive amount for Glass that day. Buy one, and Google would throw in a miniature camera accessory for one's golf club absolutely free  insert your own jokes about Google Golf here (or Google Glass Golf).

If the time and location sounds a bit odd for Google and the selling of its Glass device, it's actually not. At least, not based on a Google+ post that Google made yesterday, where it teased the various ways it plans to start rolling out Glass to interested parties in the future.

"In the last six months, we've been amazed and inspired by our Explorers and the individual perspectives you bring. A few weeks ago, we opened up our site for a single day. The response was overwhelming  we almost ran out of inventory and had to close things down early. We've since built our inventory back up and plan to continue to accelerate new ways to expand the program in the weeks and months ahead," Google wrote.

"Our hope is to bring Glass to new Explorers, like optometrists, sports lovers, online retailers, cooks and travelers, who (like you!) can get in early and help make Glass better as part of our open beta, ahead of a wider consumer launch. These are exciting times and we're glad to have you along for the ride  keep the feedback coming!"

Further Reading

Computers & Electronic Reviews