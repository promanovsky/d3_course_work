A new study claims that dinosaurs fit in an intermediate class between warm and cold blooded animals.

Scientists compared the growth rates of hundreds of living and extinct species, using growth rings and bone size to calculate the rates for dinosaurs, the BBC reported.

They linked growth rate to metabolic rate, the measure of energy use that divides warm and cold blooded animals.

The study suggests that the dinosaurs fall into a middle category, in a fresh contribution to an enduring debate.

Warm blooded animals, like mammals and birds, need a lot of fuel and use that energy to their advantage, including faster movement and boosted brain power. In burning all that food they also maintain a high, stable body temperature.

Cold blooded animals are more economical, but lack those advantages.

Scientists define these different strategies as "endothermy" (endo for inside; therm for heat) and "ectothermy".

The study's first author and a PhD student at the University of New Mexico, John Grady's paper proposes that dinosaurs may have used a not-too-hot, not-too-cold approach: "mesothermy".

The evidence for this idea comes from a big survey of the growth rates in 381 different species, including 21 dinosaurs.

Because bones show growth rings much like trees, the size of fossilised dinosaur bones at different ages allows palaeontologists to calculate how fast they put on weight over a lifetime.

So he and his colleagues took growth rate as an indicator of metabolism, and found that dinosaurs occupied a middle ground, somewhere in between modern reptiles and mammals.

Their results also place several living animals with unusual energy habits into the proposed mesothermic category.

The study is published in the journal