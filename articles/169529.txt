Two Ontarians are among the nominees for the 68th annual Tony Awards, celebrating the best in Broadway theatre: Ramin Karimloo and Nick Cordero.

Karimloo, who grew up in Richmond Hill and Peterborough, is nominated for best lead actor in a musical for his performance as Jean Valjean in the revival of Les Misérables that thrilled audiences in Toronto before heading to New York. "Just woke up to a plethora of messages on all available apps," the actor tweeted. "The global support already will be the best memory of today."

Les Misérables received just three nominations (best revival and best sound design are the others) – and Karimloo will face stiff competition from How I Met Your Mother star Neil Patrick Harris's critically lauded turn in the title role of Hedwig and the Angry Inch, as well as Rocky's Andy Karl and A Gentleman's Guide to Love and Murder stars Jefferson Mays and Bryce Pinkham.

Story continues below advertisement

Cordero, meanwhile, is up for best performance by an actor in a featured role in a musical for his widely praised work as a gangster-turned-playwright in Woody Allen's own musical adaptation of his film Bullets over Broadway. (The musical, co-produced by Montreal's Just for Laughs Theatricals, is up for six awards in total.)

The Hamilton native's toughest challenger in the category will be James Monroe Iglehart, whose show-stopping performance as the Genie in Aladdin (five nominations in total) was also seen in Toronto this fall on its way to Broadway. Cordero's fellow nominees include another Canadian, Winnipeg-born Joshua Henry (for Violet); Jarrod Spector (for Beautiful: The Carole King Musical); and Danny Burstein (for Cabaret).

The Globe and Mail spoke with Karimloo and Cordero before they were to begin their Broadway runs in January. A Gentleman's Guide to Love and Murder, a dark comedy about a man who murders his way to a dukedom, is the most nominated of this year's crop of Broadway musicals. It leads the way with 10 nominations – including one for director Darko Tresnjak, whose Titus Andronicus was seen at the Stratford Festival in 2011.

In the play categories, two revivals lead the way: Tim Carroll's "original practices" production of Twelfth Night night is tied with John Tiffany's reimagined The Glass Menagerie with seven nominations each. (Carroll will be back at the Stratford Festival this summer, directing King John.)

The full list of Tony nominations is available online. The awards will be handed out at a ceremony hosted by Hugh Jackman and broadcast on June 8 on CBS.

Follow me on Twitter: @nestruck