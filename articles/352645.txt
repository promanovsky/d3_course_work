Bob Hastings, a character actor best known for his role on 1960s sitcom “McHale’s Navy” as Lt. Elroy Carpenter, the bumbling, sycophantic aide to Captain Binghampton, died June 30 in Burbank, Calif., of pancreatic cancer. He was 89.

Hastings also recurred on “All in the Family” as Tommy Kelsey, the owner of the bar that Archie frequents and later buys.

The actor worked in television from its very earliest years, with a role as Hal on the DuMont network’s “Captain Video and His Video Rangers” in 1949.

Hastings recurred on “The Phil Silvers Show” in various roles in the late 1950s and guested on “Gunsmoke,” “Ben Casey,” “Dennis the Menace” and “Twilight Zone.”

He was a series regular on “McHale’s Navy” from 1962-66 and appeared in the two features spun off from the series in 1964 and 1965.

Hastings got his start in radio as the voice of the title character in the Archie Comics spinoff “Archie Andrews,” and he did a great deal of voicework during his long career in television for series including “The Munsters” (he was the uncredited voice of the raven); 1966’s “The New Adventures of Superman”; 1978’s “Challenge of the SuperFriends”; and a number of Batman-related series, including “Batman: The Animated Series,” for which he was the voice of Commissioner Gordon.

In the late ’60s he guested on series including “Hogan’s Heroes,” “I Dream of Jeannie” and “The Flying Nun” and appeared repeatedly in various roles on “Green Acres” and “Adam-12.” He was busy in the 1970s with guest roles on shows including “My Three Sons,” “Room 222,” “Love, American Style,” “The Odd Couple,” “Marcus Welby, M.D.,” “Ironside,” “The Rockford Files,” “The Love Boat,” “Quincy, M.E.” and many more.

As the years progressed Hastings spent more of his time doing voiceover work. He had most recently provided a voice for the videogame “Mafia II” in 2010.

Hastings appeared on the bigscreen in films including “The Bamboo Saucer,” “Angel in My Pocket,” “The Love God?,” “The Boatniks,” “How to Frame a Figg,” and “The Poseidon Adventure.”

Robert Francis Hastings was born in in Brooklyn.

He is survived by his wife, Joan, whom he married in 1948, and four children.