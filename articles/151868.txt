NEW YORK - Stephen Colbert, who will take over for a retiring David Letterman on CBS' "Late Show" next year, revealed Tuesday that he almost worked for Letterman - twice.

Colbert, the Comedy Central host, visited the "Late Show" for the first time since it was announced earlier this month that he will replace Letterman sometime next year. He was greeted warmly, with Letterman introducing him as "very talented" and "always entertaining," and the two men posed for the obligatory selfie -- already a nod to the younger demographic colbert is expected to attract.

It was a different visit in 1986, the year that Colbert, now 49, graduated from Northwestern. Letterman was working at NBC then, and Colbert said he accompanied his girlfriend to New York, where she was interviewing for an internship at Letterman's show.

In an odd turn of events, Colbert said he was hired instead of his soon-to-be ex-girlfriend. But he turned the job down.

"I did not take the internship because you do not pay people," he said. "The next job I'm taking, that pays, right? Because I've already signed."

Eleven years later, Colbert said he and his writing partner submitted a packet of material to the "Late Show," hoping to join the writing staff. He said no one from the show got back to them for four months, and by then Comedy Central had already hired Colbert to do the show "Strangers With Candy."

Colbert pulled out a card and read the Top Ten list that he and his partner had submitted 14 years ago to try to get the job. The list, written around the holidays, was "Top Ten Cocktails for Santa."

"Late Show" producers inserted the traditional prepared intro that runs before Letterman's Top Ten list every night, for which the current host feigned outrage.

"He doesn't have the job yet," he said. "Don't let the door hit you in the ass."

The list topper at the time: "Number one cocktail for Santa -- Silent Nighttrain!"

Colbert said that, "Obviously, I'm thrilled" to have the job.

The "Colbert Report" on Comedy Central will continue through the end of this year. The dates of Letterman's exit and Colbert's debut on the "Late Show" haven't been set.

He told Letterman that "I'm going to do whatever you have done," and Letterman protested, "You don't want to do that."

"It seems to have gone pretty well, Dave," Colbert said.

"It's gone on," Letterman replied.

Outside the Ed Sullivan Theater, where the show is taped, fans told CBS News they liked Colbert as the next host.

""I think it would be great to bring a younger crowd in and still keep the name of the show," said one. "I'm sad letterman's leaving, but Colbert's hysterical. Love him," said another.