Want to start your own electric car company? You now have the technology to do so.

Tesla has made the unorthodox move of pledging not to pursue litigation against any company that uses its technology "in good faith."

In a blog post cheekily titled "All Our Patent Are Belong To You," Tesla founder and CEO Elon Musk wrote that the company wants to encourage the proliferation of electric cars by allowing other companies to build off its patented technology. The headline is a reference to "All your base are belong to us."

"Tesla will not initiate patent lawsuits against anyone who, in good faith, wants to use our technology," Musk wrote.

While Musk has said he hopes to shift the world away from combustion engines, he also admitted that Tesla will not be able to do it alone.

"Our true competition is not the small trickle of non-Tesla electric cars being produced, but rather the enormous flood of gasoline cars pouring out of the world’s factories every day," he wrote.

There is also a business case for Tesla's move. Musk recently said that Tesla was considering allowing other companies to use its charging stations.

Patent technology is a valuable asset for any company that is seeking to disrupt a particular industry. Litigation related to patents often end up costing companies millions of dollars and are seen by some as stifling innovation. Some companies have emerged that almost entirely depend on patent lawsuit settlements for income.

Musk wrote that a patent is "a lottery ticket to a lawsuit," and that to achieve the company's ultimate goals, it needed to allow others to build on its technology.

"Tesla Motors was created to accelerate the advent of sustainable transport. If we clear a path to the creation of compelling electric vehicles, but then lay intellectual property landmines behind us to inhibit others, we are acting in a manner contrary to that goal," he wrote.

Tesla is not the first technology that Musk has decided to make publicly available. The Hyperloop high speed transportation system conceived by Musk is an open source project that has been pursued by some companies including Hyperloop Transportation Technologies.

Investors had little if any reaction to the move. Shares in Tesla remained up 0.3% on the day, having more than doubled in the past 12 months of trading.