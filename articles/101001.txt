After waging a four-year public relations campaign, researchers from the Cochrane Collaboration, an independent, global health care research network, were given access to the full records of more than 24,000 research participants in 20 Tamiflu and 26 Relenza trials — about 150,000 pages of data — from manufacturers Roche and GlaxoSmithKline.

The US government has spent more than $1 billion over the past decade stockpiling the antiviral drugs Tamiflu and Relenza to prepare for a flu pandemic, but a new review of the manufacturers’ clinical trial data finds that the drugs do not reduce flu-related hospitalizations or serious complications such as pneumonia, sinus infections, or death.

Advertisement

They found that compared with a placebo, Tamiflu shortened the duration of flu symptoms by a little less than a day on average — from 7 to 6.3 days — but led to more side effects. These included nausea and vomiting and, in those who took the drug for weeks to prevent the flu, headaches, kidney problems, and psychiatric conditions such as depression or confusion, according to the findings published Wednesday in the British Medical Journal. Relenza, which is inhaled, had a similar effect on shortening symptoms, with no increased risk of side effects.

“This is a situation where the effectiveness of the drugs have been overplayed and the harms underplayed,” said Dr. Fiona Godlee, editor in chief of the British Medical Journal, which worked with Cochrane to get manufacturers to release the full data. “It brings into question the decision to stockpile the drugs worldwide and gives us unflattering insight into the system itself.”

Since 2005, the Department of Health and Human Services spent $1.3 billion to purchase 50 million doses of antiviral drugs and to subsidize states to purchase 31 million additional treatment doses for their own stockpiles. Massachusetts does not have its own supply.

Advertisement

After distributing 11 million doses during the 2009 pandemic caused by the H1N1 flu strain, the federal government replenished its stockpile.

“In future pandemic responses, HHS purchases of antiviral drugs and vaccines will be based on risk-benefit analyses using all product safety and efficacy and disease severity and transmission data available at that time,” said Gretchen Michael, an HHS spokeswoman, who declined to comment on whether the policy would change based on the new data.

Researchers from the Centers for Disease Control and Prevention estimated in a 2011 study that the use of antiviral drugs during the H1N1 outbreak prevented 8,400 to 12,600 hospitalizations in the United States from flu-related complications, but the new Cochrane review found that probably wasn’t the case.

It also found that taking the antivirals as a preventive measure lowered a person’s risk of developing flu symptoms but didn’t appear to stop the transmission of the virus.

While information on the Food and Drug Administration website about Tamiflu and Relenza doesn’t mention that the drugs can prevent flu-related complications, the CDC’s website states that for people with high-risk medical conditions, “treatment with an antiviral drug can mean the difference between having milder illness instead of very serious illness that could result in a hospital stay.”

CDC officials contend those statements are based on studies that looked at flu patients treated with antiviral drugs in real-world clinical practice, which found that when the drugs were administered within the first day or two of symptoms appearing, they reduced the risk of complications compared to when they weren’t given at all.

Advertisement

An analysis published last month in the journal Lancet Respiratory Medicine found that those who were treated with an antiviral drug were 50 percent less likely to die from their infections than those who weren’t.

“When the CDC is formulating antiviral treatment recommendations, we look at all the evidence available,” said Dr. Timothy Uyeki, chief medical officer for the CDC’s influenza division and an author of the Lancet analysis. He said that he plans to examine the latest

Cochrane review but that the agency will “continue to emphasize antiviral treatment for any hospitalized patients” who are diagnosed with the flu.

Other researchers, however, contend that relying on population study data — rather than clinical trials— can be misleading.

Doctors might choose to use antiviral drugs in more robust patients and skip it in those less likely to survive a bout of the flu, said Harlan Krumholz, a professor of medicine at Yale University School of Medicine, who wrote an editorial that accompanied the new study.

“This is a very contentious issue to say that the evidence is strong enough to be taken as a justification for stockpiling billions of dollars of these drugs,” Krumholz added. He called for federal funding of an independent clinical trial to determine whether antiviral drugs prevent pneumonia and deaths in those hospitalized with the flu.

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.