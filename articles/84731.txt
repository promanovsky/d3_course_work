Back at the Mobile World Congress 2014 in Barcelona, Samsung unveiled the Galaxy S5. There had been a lot of rumors about the expected specifications prior to the announcement, and not all rumors turned out to be true. Since then many have been wondering what this means for the Galaxy Note 4, and if its possible that the popular phablet might not come with all that its expected to. Partial Galaxy Note 4 specs have been purportedly leaked, but its still too soon to be certain of what Galaxy Note 4 has in store.

Advertising

Folks at CNMO claim that the Samsung Galaxy Note 4 is going to have a 1,440×2,560 Quad HD display. This is one of the most rehashed rumors about the Galaxy S5, but ultimately it came with a full HD display. Its not surprising that the 2K speculation is now focused on the Note 4, given that the S5 let everyone who was wishing for it down.

It is also claimed that Galaxy Note 4 is going to be dustproof and waterproof, just like the Galaxy S5. This would be the first time the phablet would be able to better resist the elements. Not much else is known as yet. It is unclear if Samsung intends to keep the faux leather back it introduced last year. Given that it has extended it to laptops as well, there’s a chance that faux leather might not go anywhere.

Samsung usually unveils a new iteration of the Galaxy Note phablet at IFA in Berlin. This isn’t due until September, so we have quite a bit of waiting to do.

Filed in . Read more about Galaxy Note 4 and Samsung.