ROCHESTER, Minn., June 24 (UPI) -- Never stop learning. That's the takeaway from a new study that suggests a lifetime of feeding the brain new information can delay the onset of dementia by up to nine years.

That staying mentally active helps keep dementia and Alzheimer's at bay isn't entirely new wisdom; it's been a refrain of both expert and amateur health advocates for years. But now there's some concrete evidence, thanks to researchers at the Mayo Clinic.

Advertisement

The new study, published this week in JAMA Neurology, compared the cognitive histories of nearly 2,000 Minnesotans, aged 70 to 89, with their current mental capacity.

Factors such as level of education, complexity of occupation, as well as previous cognitive activities -- both midlife and more recent -- were taken into account. Cognitive activities included things like playing music, reading books, working on computers, arts and crafts or even regular socializing.

RELATED Phillies sign Sizemore to minor league contract

Once all the info was collected, participants participated in a serious of tests to measure cognitive function. As a result, researchers were able to show that mental stimulation throughout one's life can delay the onset of dementia and Alzheimer's.

"Doing cognitive activities at least three times a week was highly protective," said lead author Dr. Prashanthi Vemuri, a radiologist and researcher at the Mayo Clinic.

Midlife activities had less drastic delaying effect on the well-educated than it did for those with less formal education. Education, though, still offered a nice buffer of five years.

RELATED Ian Somerhalder tesfies before Congress about endangered species

"This was a little surprising," said Vemuri. "But it turns out that even if you don't have a lifetime of educational and occupational development, intellectual activity in later life can really help -- perhaps delaying cognitive impairment by at least three years."

RELATED FAA bans drones from delivering packages