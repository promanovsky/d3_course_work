According to the Japanese business newspaper Nikkei, Alpine will release a standalone aftermarket console in both the United States and Europe sometime this fall for somewhere between $500 to $700. The system with its 7-inch screen will allow users to view their phones’ maps and use voice commands to play music, make calls, and access messages. Additionally, it will read messages and maps out loud as you motor down the road.

Not just for new cars anymore

The report suggests that Alpine is keen on becoming the first company to embrace Apple Inc. (NASDAQ:AAPL)’s new iOS, but they are not alone in this quest. According to MacRumors, Clarion is also developing a CarPlay enabled in-dash system of its own and is planning to support the OS in its aftermarket and OEM products as early as the end of this year.

Half Moon Capital Returns 12.2% In 2020 Despite Short Position Drag Eric DeLamarter's Half Moon Capital produced a return of 8% net of fees in the fourth quarter of 2020, bringing the full-year return to 12.2%, according to a copy of its fourth-quarter letter, which ValueWalk has been able to review. The fund maintained an average net exposure of 45% during the period. Q4 2020 hedge Read More

When CarPlay was introduced in March, Apple Inc. (NASDAQ:AAPL) described it as a “smarter, safer way to use your iPhone in the car.” The iPhone is synced with new cars’ built-in display and features the iconic voice of Siri while allowing you to get directions, make calls, send and receive messages, and listen to music with nothing more than your voice. As more and more reports suggest that people continue to text and drive, Apple decided to step it up with a host of car manufacturers.

Carmakers on-board

Select new models from manufacturers Ferrari, Mercedes-Benz, and Volvo will be the first to offer CarPlay displays while BMW, Ford, General Motors, Honda, Hyundai, Jaguar, Land Rover, Kia, Mitsubishi, Nissan, PSA Peugeot Citroen, Subaru, Suzuki, and Toyota will be jumping on the new technology beginning next year. Until today’s report those not planning on purchasing a new car were left in the dark, but now thanks to Clarion and Alpine, older car owners can get in on the fun and safety features of CarPlay.