Federal Reserve Board Chairwoman Janet Yellen said that while there was significant improvement in the economy, underutilization in the labor market was holding the central bank back from raising rates. UPI/Kevin Dietsch | License Photo

WASHINGTON, July 30 (UPI) -- The Federal Reserve ended its two-day FOMC meeting by cutting back its bond-buying program by $10 billion, saying it's happy with the economy's progress.

In a statement issued Wednesday afternoon, the Fed cited falling unemployment rate and rising inflation, but said that there was still considerable weakness in the labor market. It reduced its bond-biying program to $25 billion starting in August, and is expected to end the program this October.

Advertisement

However, the Fed did note that "labor market indicators suggests that there remains significant underutilization of labor resources," an indication that the central bank is not happy with the country's low labor participation numbers.

Fed Chair Janet Yellen the number of part-time and long term unemployed Americans, who would prefer full-time employment, is reaching historic highs and that the labor department was far from normal.

Economists had expected that declining unemployment and rising inflation would lead the Fed to increase short-term rates sooner rather than later, but the Fed's assertion that the labor market is not fully recovered, means rate hikes may not happen this year.

Charles Plosser, president of the Philadelphia Federal Reserve Bank, was the only dissenting voice on the statement. Plosser objected to the central bank's guidance that they would keep rates low even after scaling back its bond program, suggesting that it did not reflect the economy's progress in the past few months.