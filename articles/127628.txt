A new app that uses mathematical models to help overcome jetlag by telling you when to sleep and how long for is set to see globetrotters the world over rejoice (after a good night's sleep).

Currently it can take up to 13 days to get into sync with a new time zone but mathematicians from the University of Michigan have created an algorithm that calculates the hours of sleep and exposure to daylight necessary for travellers to acclimatise in just 48 hours.

Jetlag is the often debilitating result of breaking our “circadian rhythm” – the natural body clock that tells us when to sleep and when to wake. Travelling several thousand miles violently disrupts a pattern that from an evolutionary perspective has never been broken at all.

Scientists have been aware for some time that exposure to light warps the body’s sense of time and confuses its circadian cycle, likening it to having every clock in your house set to a different time.

Overcoming jetlag is about returning to a rhythm that our bodies can understand, or if possible, preventing too much deviation before the jetlag can worsen.

The mathematicians from Michigan developed a model capable of evaluating different scenarios to determine how our bodies react when thrown out of rhythm and how it can be remedied.

Their tests, published in the journal PLOS Computational Biology, found that in order to restore individuals' circadian rhythm, periods of light and dark need to be simulated to trick the body clock.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Daniel Forger, a mathematical biologist at the University of Michigan, told Scientific American that the solution was actually simpler than they expected: “We thought the answer would be extremely complicated: At 3:40, you need to get this much light, and at this time, you need to get this much."

Another student, Olivia Walch, then synthesised these results into an app known as “Entrain” which has red eyed passengers sleeping properly within two days and fully recovered in four.