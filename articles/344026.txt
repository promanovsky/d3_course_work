Many physicians believe pelvic examinations should be a routine part of a well visit, but new research suggests they can do more harm than good.

The American College of Physicians (ACP) released an evidence-based clinical practice guideline that reveals the risks associated with routine pelvic exams,.

"Routine pelvic examination has not been shown to benefit asymptomatic, average risk, non-pregnant women. It rarely detects important disease and does not reduce mortality and is associated with discomfort for many women, false positive and negative examinations, and extra cost," said Doctor Linda Humphrey, a co-author of the guideline and a member of ACP's Clinical Practice Guidelines Committee. The researcher noted the guidelines are only for pelvic exams and do not include pap smears.

The ACP stated that when screening for cervical cancer the examination should be limited to a visual examination of the cervix and cervical cancer, bimanual examinations are not required in most cases. The College said these types of exams have a low accuracy in detecting cervical cancer and other infections.

A pelvic exam is advised for women with symptoms such as "vaginal discharge, abnormal bleeding, pain, urinary problems, or sexual dysfunction," the college said.

"Screening pelvic examination exposes adult, asymptomatic, average risk, non-pregnant women to unnecessary and avoidable harms, including anxiety, embarrassment, and discomfort, and may even prevent some women from getting needed medical care," Doctor Molly Cooke, ACP's Immediate Past President and a member of ACP's Clinical Practice Guidelines Committee, said. "False positive findings can lead to unnecessary tests or procedures, adding additional unnecessary costs to the health care system."

The work was published in today's issue of the journal Annals of Internal Medicine.

"The pelvic examination has held a prominent place in women's health for many decades and has come to be more of a ritual than an evidence-based practice...With the current state of evidence, clinicians who continue to offer the examination should at least be cognizant about the uncertainty of its benefits and its potential to cause harm through false-positive testing and the cascade of events it prompts," Doctor George Sawaya and Dr. Vanessa Jacoby of the Department of Obstetrics, Gynecology and Reproductive Sciences at the University of California, San Francisco wrote in an editorial.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.