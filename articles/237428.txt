NEW YORK (TheStreet) -- An insurance trade group harshly criticized Gilead (GILD) - Get Report for charging too much for its hepatitis C drug, while Florida has reportedly restricted the ability of Medicaid recipients to receive the drug.

WHAT'S NEW: The price that Gilead is charging for its HCV treatment Sovaldi is "astronomical," America's Health Insurance Plans stated on its blog yesterday. The drug has shown "tremendous results" and is the "kind of medical innovation we need to sustain," but the price is not sustainable for consumers or society and is the result of Gilead taking "advantage of a lack of competition," according to the group, which represents the health insurance industry. Meanwhile, Florida has restricted the ability of Medicaid patients in the state to access Sovaldi, Bank of America Merrill Lynch reported in a note to investors earlier today. The firm wrote that Florida's decision supports its thesis that payers will increasingly limit access to expensive drugs.

WHAT'S NOTABLE: In late March, shares of Gilead fell after three Democratic members of the House Committee on Energy and Commerce said they were concerned about the price the company is charging for Sovaldi. In a letter to Gilead's CEO, the House members stated that reports have indicated that Gilead plans to charge $84,000 per treatment for Sovaldi and that they were concerned that the drug would not be able to cure patients if it cannot be afforded. Sovaldi's costs are likely to be too high for many patients, including those with both public and private insurance, the lawmakers contended, adding that the FDA's decision to speed the approval of Sovaldi makes the high price of the drug even more problematic. They requested that the company provide more information about its methodology for determining Sovaldi's prices and they sought information about any discounts the company is providing to low income patients. Other companies that are developing HCV drugs include AbbVie (ABBV) - Get Report), Bristol-Myers Squibb (BMY) - Get Report, and Merck (MRK) - Get Report.

PRICE ACTION: In mid-morning trading, Gilead rose 24c, or 0.3%, to $81.38. Over the last three months, Gilead shares are down about 1.5%.

Reporting by Larry Ramer.

The Fly

provides comprehensive coverage of stock news and Street research and delivers it in real-time. The Fly breaks market-moving news and explains sudden stock movements in a rapid-fire, short-form story format. Follow @theflynews on Twitter. For a free trial, click

here.