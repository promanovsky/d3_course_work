The Heartbleed bug just went from bad to worse to truly, utterly terrifying.

The National Security Agency knew of the existence of the catastrophic bug for at least two years and kept it a secret from the public and the cybersecurity community in order to exploit it, according to a bombshell report from Bloomberg News. However, the agency is denying the story.

It's unclear what the agency might have been able to do with its knowledge of the exploit, but if the report is true, then it means the NSA knew about one of the most dangerous bugs in Internet history and did nothing to warn us about it.

"NSA was not aware of the recently identified vulnerability in OpenSSL, the so-called Heartbleed vulnerability, until it was made public in a private sector cybersecurity report," the Office of the Director of National Intelligence said in a statement to HuffPost. "Reports that say otherwise are wrong." A White House spokesperson also stated that no federal agency was aware of the bug.

First discovered by Google and Codenomicon, a security firm, the Heartbleed bug is a flaw in the encryption used to protect vast number of websites from hackers. The fear is that the bug may expose credit card numbers, passwords and more.

Yahoo, Amazon and many, many other major websites used the free code, called OpenSSL, since encryption software is notoriously difficult to write.

Immediately after news of Heartbleed broke, some suspected that the NSA was exploiting the security lapse to access people's private data. Others saw it coming even before that: The documents leaked by former NSA contractor Edward Snowden indicated that the NSA partnered its British spying equivalent, the GCHQ, to try to crack SSL and other encryption standards that protect the Internet.

The two sources who spoke to Bloomberg seemed to confirm those fears on Friday.

Before Bloomberg's story, Wired reported that the bug might not be all that handy for the NSA. Heartbleed lets an attacker scoop up data from a website, but according to Wired's Kim Zetter, "the data that’s returned is random — whatever is in the memory at the time — and requires an attacker to query multiple times to collect a lot of data."

The piece of the data that had security experts most worried -- the private SSL keys -- may be safe from the NSA's clutches. Theoretically, with a website's private key, a bad actor could steal information from a website months or years after the Heartbleed bug has been patched in its system. But after several tests, the online security company CloudFlare said it was unable to use Heartbleed to extract those keys. However, another researcher at Errata Security was much less sure about private keys being safe.

But the bits of data the agency might have been able to vacuum up with Heartbleed could be used in its many other data-gathering initiatives. "Putting the Heartbleed bug in its arsenal, the NSA was able to obtain passwords and other basic data that are the building blocks of the sophisticated hacking operations at the core of its mission," Bloomberg's Michael Riley wrote.

This story was updated with comment from the NSA, and language has been changed throughout the story to reflect its denial.