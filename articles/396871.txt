Software sales executive Nick Viall and former professional baseball player Josh Murray both fought for Andi Dorfman’s heart, but which man did she choose?

Andi accepted Josh's proposal in a teary, dramatic season finale of "The Bachelorette" in the Dominican Republic.

"My life changed forever from the moment I pulled up in the limo," Josh said. "You took my breath away. It was a feeling unlike anything I felt before in my entire life. Andi, you answered all my prayers, you're the woman of my dreams."

PHOTOS: Andi -- before she was the Bachelorette!

"I live every second of every day just to see that smile again," he continued. "Deep down I know I'm the only man who can make you smile the way you do. I see the woman I want to spend the rest of my life with."

"It's definitely been a journey and definitely more than I ever anticipated," Andi said. "I started this journey to find love and my hope has been tested throughout. It's been a struggle and it's been a challenge. The truth is that from the first time I met you I was scared."

Josh started to look nervous.

"You being open is something I'll always remember and always appreciate. It has taken a lot of thought for me to get here and I knew the feeling from the very first time I saw you ... I know that that feeling is love!"

"Josh, I've loved you since the moment I laid eyes on you," Andi said. "I know I love you. I'm madly in love with you. You're the one I want spend the rest of my life with."

"I can't wait to start our lives together," Josh said, beaming as he got down on one knee and pulled out a giant Neil Lane diamond. "Andi Jeanette Dorfman, will you marry me?"

"Yes!" Andi said. "I have one final question for you: Josh, will you accept this final rose?"

Andi jumped into Josh's arms and the newly engaged couple sat on a dock overlooking a gorgeous sunset.

Andi spared runner-up Nick from getting rejected during a proposal by letting him down in his hotel room earlier that day.

"I thought about how you said last time you got engaged you woke up that morning and you didn't feel that something was right," Andi said. "I woke up this morning and didn't feel that something was right."

"It's clear to me that the things I see in you and the things I see in us is ultimately not what is best for us," she added.

"When did you start feeling this?" Nick asked, looking shocked and confused.

Andi told Nick she wanted to have fun and relax on their last date but couldn't.

"Life with you would be me over-analyzing every single moment," she said.

"Is this about us or about someone else?" Nick said angrily. "I woke up so confident. There are certain things I wish you wouldn't have said or done."

During the live "After the Final Rose" show that followed the finale, Nick got a chance to talk with Andi about their failed relationship.

"Did you love Nick?" host Chris Harrison asked.

"I was not in love with him and that's why I didn't tell him I was in love with him," Andi said. "I know this sounds harsh, but how do you compare with a greater relationship, a greater love? There was nothing wrong with Nick and our relationship, but it's just that something else was more right."

Then it got really uncomfortable.

"It's hard to hear that. If you weren't in love with me, I'm just not sure why you made love to me," Nick said, alluding to the couple's night in the fantasy suite the week before.

"I think that should be kept private," Andi said angrily. "Those things were real, the things I said to you were real."

"I'm not trying to be below the belt," Nick said.

"You already have," Andi responded, adding that she didn't have Nick pick out an engagement ring or walk outside to propose because, "I did care about [him.]"

The mood got lighter after a much-needed commercial break and Andi's new fiancé swapping places with Nick on the couch.

"She's beautiful, she's so intelligent, she's really funny," Josh said.

Chris joked that Andi traveled the world on "The Bachelorette," only to find someone who lives 5 minutes away from her in Atlanta.

"To think he was only in my backyard," Andi said.

"We're madly in love," Josh said. "This show works. I'm very blessed to be with her."

In a random twist to wrap up "After the Final Rose," Internet sensation Grumpy Cat was brought on stage as a tribute to Andi's amusing frown she often displayed on the show.

ABC's new spinoff "Bachelor in Paradise" premieres Aug. 4.