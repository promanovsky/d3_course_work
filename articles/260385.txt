Ubisoft may have gone too far with their "Watch Dogs" PR stunts when they sent a suspicious package to Ninemsn's offices in Australia, Mumbrella reports.

Ubisoft's marketing team allegedly sent a black safe with no markings on it along with a "suspicious" letter instructing a reporter to "check your voicemail." According to Mumbrella, the staff entered a code in the safe, which caused it to beep but not open. The staff was sent home and a bomb squad was called in after employees feared it was an explosive device.

Authorities eventually were able to open the safe, which contained a "Watch Dogs" game, a baseball cap and a note from the Ubisoft. Ubisoft has yet to comment about the safe sent to Ninemsn's offices.

"This is definitely the other side of the line in terms of what it's safe for a PR company to send anonymously to a newsroom. The thing was black, heavy and slightly creepy," Ninemsn editor Hal Crawford told Mumbrella."We did check with other newsrooms to see if they had received a similar package as we thought it was a PR stunt, but no-one else had. We weren't panicked at any point, but given there was no note explaining what it was, we had to take sensible precautions."

A bomb squad was called out over a Watch Dogs PR stunt. Kinda good AND bad for publicity? http://t.co/QwKDUXNLds pic.twitter.com/pbvsErMreG — Byte (@WordofByte) May 28, 2014

It isn't the first time Ubisoft has attempted grand PR stunts. Ubisoft pranked a few unsuspecting citizen in the United States by making Aiden Pearce's in-game hacks a reality on the streets of California.

A tech shop was set up in Los Angeles to lure in customers who were having trouble with their phones. A retail associate would then take their phones to the back room and install an "amazing app" for free. The associate then takes the customers onto the streets of L.A. to demonstrate the app, but ends up causing an accident and the police show up. The events were caught on hidden camera footage below.

"Watch Dogs" is now available for the PS3, PS4, Xbox 360, Xbox One and PC.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.