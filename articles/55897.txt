Autism may actually begin to develop when certain cells in the brain fail to properly develop in the womb, according to a new study from researchers at the University of California at San Diego's Autism Center of Excellence.

For the study the researchers examined brain tissue from 22 deceased kids between the ages of 2 and 15, all of whom had been diagnosed with autism before their death. They found that brain cells from the kids with autism lacked a key gene variant that develops in a healthy brain in early in utero.

"We found a novel aspect of cortical development never seen before that provides clues to the potential cause of autism and when it began," the researchers explained. "The type of defect we found points directly and clearly to autism beginning during pregnancy."

For comments and feedback contact: editorial@rttnews.com

Health News