Walmart Stores has sued Visa, accusing the credit and debit card network of inflating card swipe fees, which the retail giant claims has hit sales.

Walmart filed a complaint filed in federal court in Fayetteville, Arkansas, asking for at least $5bn (£3bn, €3.6bn) in damages for what it claims are violations of federal antitrust laws that could triple that sum, according to media reports.

Visa and other card networks charge retailers fees, called interchange fees or swipe fees, each time a shopper uses a credit or debit card to pay.

In its lawsuit, Walmart has argued that Visa, together with banks, sought to prevent retailers from protecting themselves against those swipe fees, eventually impacting sales.

Visa spokesman Paul Cohen refused to comment on the Wal-Mart complaint.

Wal-Mart, in its complaint, said "Visa's monopoly power has enabled it to dictate price and inhibit competition."

"The anticompetitive conduct of Visa and the banks forced Walmart to raise retail prices paid by its customers and/or reduce retail services provided to its customers as a means of offsetting some of the artificially inflated interchange fees," Wal-Mart in court documents.

"As a result, Wal-Mart's retail sales were below what they would have been otherwise," Walmart added.

2013 Settlement

Walmart, Amazon and Target are among dozens of large merchants who opted out of a nationwide, multibillion dollar antitrust settlement with Visa and MasterCard to seek damages on their own.

The settlement Wal-Mart withdrew from, initially valued at $7.25bn, was approved by a Brooklyn, New York, federal judge in December 2013. It is now worth about $5.7bn after reductions for the merchants that dropped out.

Visa sued Walmart in June 2013, to prevent it from bringing its own case, saying in a complaint filed in Brooklyn that it sought to prevent "the continuation of endless, wasteful litigation between the parties."