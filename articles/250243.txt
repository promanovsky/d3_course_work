Bank of Japan officials confident that consumer spending will recover owing to a tight labour market and rising wages. — Reuters file pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

TOKYO, May 23 — Japan’s core consumer prices rose the most in almost 23 years in April after an increase in the nationwide sales tax, a Reuters poll showed today.

Excluding the impact of the April 1 tax increase, the data is expected to show underlying inflation remained firm in a sign of steady progress in meeting the Bank of Japan’s 2 per cent inflation target.

Household spending in April is also expected to show the steepest decline in more than 2½ years as consumers who stocked up on daily goods before the tax hike spent less immediately afterwards.

BoJ officials are increasingly confident that consumer spending will quickly recover due to a tight labour market and rising wages. An increase in capital expenditure in the first quarter shows companies share the central bank’s confidence that demand will rebound.

“The consensus is that whatever downturn is caused by the sales tax increase will start to ease in the second quarter,” said Shuji Tonouchi, senior fixed income strategist at Mitsubishi UFJ Morgan Stanley Securities.

“Demand in the labour market will continue to improve. Mid-year bonus payments will also soften the blow to consumption.”

The nationwide core consumer price index, which includes oil products but excludes volatile prices of fresh fruit, vegetables and seafood, likely rose 3.1 per cent in the year to April, according to a Reuters poll of 29 economists.

That would be the fastest since July 1991 as the sales tax hike pushed up prices across the board. In March, core consumer prices rose an annual 1.3 per cent.

Core consumer prices in Tokyo, available a month early, rose an annual 2.9 per cent in May, according to the survey. That would be the fastest rise since April 1992.

The BoJ estimates that the sales tax rise will add 1.7 percentage points to Japan’s annual consumer inflation in April and 2.0 points from the following month. The government ministry issuing CPI data does not provide similar estimates.

The consumer inflation data will be released at 8.30am on May 30 (2330 GMT on May 29).

The jobless rate, which is due at the same time as the consumer price index, is expected to remain unchanged from the previous month at 3.6 per cent. The jobs-to-applicants ratio in April is also expected to remain unchanged at 1.07.

Also next week, the government will announce industrial production for April at 8.50am on May 30 (2350 GMT on May 29).

Factory output is forecast to have fallen 2.0 per cent in April, which would be the biggest fall since a 2.8 per cent decline in June 2013.

Companies that ramped up production and increased inventories before the sales tax increase are expected to reduce their output, which will lead to the decline in output in April, economists said.

The government raised the sales tax on April 1 to 8 per cent from 5 per cent to pay for rising welfare costs. A second tax hike to 10 per cent is scheduled for October next year.

Politicians and some economists worried that the April tax hike would derail the economy, but anecdotal evidence and corporate sentiment surveys suggest the dip in activity after the tax increase is not as large as some pessimists had feared. — Reuters