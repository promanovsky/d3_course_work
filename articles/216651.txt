Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Treatment with human stem cells has allowed mice crippled by a version of multiple sclerosis to walk again after less than two weeks.

Scientists admit to being astonished by the result and believe it opens up a new avenue of research in the quest for solutions to MS.

The University of Utah's Professor Tom Lane said: "My postdoctoral fellow Dr Lu Chen came to me and said 'the mice are walking'. I didn't believe her."

The genetically engineered mice had a condition that mimics the symptoms of human MS.

They were so disabled they could not stand long enough to eat and drink on their own and had to be hand-fed.

The scientists transplanted human neural stem cells into the animals expecting them to be rejected and provide no benefit.

Instead the experiment yielded spectacular results. Within 10 to 14 days, the mice had regained motor skills and were able to walk again. Six months later, they showed no sign of relapsing.

The findings, published in the journal Stem Cell Reports, suggest the mice experienced at least a partial reversal of their symptoms.

A similar outcome in humans could help patients with potentially disabling progressive stages of the disease for which there are no treatments.

"This result opens up a whole new area of research for us to figure out why it worked," said co-author Dr Jeanne Loring, director of the Center for Regenerative Medicine at The Scripps Research Institute in La Jolla, California.

"We've long forgotten our original plan."

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

MS is an auto-immune condition caused by the body's own defences attacking myelin, the fatty insulation surrounding nerve fibres.

As myelin is stripped away, nerve impulses can no longer be transmitted properly leading to symptoms ranging from mild tingling to full-blown paralysis.

Drugs that dampen the immune system can slow early forms of the disease, but little can be done for patients in the later stages.

Members of the team believe the surprising success may be linked to the way the stem cells were grown in an unusually uncrowded lab dish.

This led to stem cells that were highly potent, with an enhanced ability to mature and develop.

Chemical signals from the stem cells instructed each mouse's own cells to repair the damage caused by MS, said the scientists.

One signal was identified as a protein called TGF-beta, raising the prospect of delivering a similar therapy in the form of a drug.

"Rather than having to engraft stem cells into a patient, which can be challenging from a medical standpoint, we might be able to develop a drug that can be used to deliver the therapy much more easily," said Prof Lane.

He added: "We want to try to move as quickly and carefully as possible. I would love to see something that could promote repair and ease the burden that patients with MS have."