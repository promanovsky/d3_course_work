Tesla's critically-acclaimed all-electric Model S sedan can travel roughly 265 miles on a single charge, according to the EPA, but CEO Elon Musk last month said "it will be possible to have a 500-mile range car," adding "in fact, we could do it quite soon."

According to China's Xinhua news agency (via Gas2 and Clean Technica), Tesla could soon achieve this 500-mile battery thanks to a development in graphene-based anodes, which can reportedly quadruple the density and output of lithium-ion batteries.

Graphene, for those who don't know, is a carbon-based "super material" that's roughly 200 times stronger than steel but nearly transparent when laid out in sheets. First isolated in 2003, graphene is as an excellent conductor of heat and energy, and certainly an ideal material for batteries.

Advertisement

Researchers have been hard at work experimenting with graphene compounds for batteries that can be scalable, cost-efficient, but most of all, powerful. In 2011, Northwestern University engineers found graphene anodes are better

at holding energy than anodes made of graphite - with faster charging up to 10x. And just last May, Rice University researchers found that graphene mixed with vanadium oxide (a relatively inexpensive solution) can create battery cathodes that recharge in 20 seconds and retain more than 90% of their capacity, even after 1,000 cycles of use.

When it comes to Tesla's electric vehicles, graphene could allow the cars to charge faster but also get more out of each charge. Even though the average driver rarely travels 100 miles each day, Musk is reportedly dissatisfied with the current range of the Model S's 85 kWH battery pack, and would like the double its capacity.

While Musk has repeatedly made note of Tesla's plans to build an affordable electric car, a battery even partly made of graphene would still make the car very expensive. Most Tesla Model S sedans cost about $100,000, but graphene is exorbitantly expensive at the moment, which means it would be difficult to produce a sub-$100,000 vehicle. So despite its benefits, the cost of graphene simply wouldn't justify the cost of disrupting Tesla's existing industrial processes to make way for this new material - at least for the immediate future.

Advertisement

It may take years before Tesla can create graphene-based batteries on a large scale, but if it ever happens, electric car critics would suddenly have little to gripe about.