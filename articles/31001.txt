Microsoft has tightened up its privacy policy after admitting to reading emails from a journalist’s Hotmail account while tracking down a leak.

The new rules prevent the company from snooping on customers’ communications without first convincing two legal teams, independent of the internal investigation, that they have evidence sufficient to obtain a court order were one applicable.

The company did not apologise for the search.

John Frank, vice president and deputy general counsel at the firm, says that following coverage of the case in the Guardian and elsewhere, Microsoft wants “to provide additional context regarding how we approach these issues generally and how we are evolving our policies.

“Courts do not issue orders authorising someone to search themselves, since obviously no such order is needed,” he continues.

“So even when we believe we have probable cause, it’s not feasible to ask a court to order us to search ourselves. However, even we should not conduct a search of our own email and other customer services unless the circumstances would justify a court order, if one were available.”

As well as requiring an internal and external review of the evidence, Frank also promises to confine any future searches “to the matter under investigation and not search for other information”. Finally, he says that the firm will begin to “report the data on the number of these searches that have been conducted and the number of customer accounts that have been affected” in its bi-annual transparency report.

“The only exception to these steps will be for internal investigations of Microsoft employees who we find in the course of a company investigation are using their personal accounts for Microsoft business,” Frank concludes. “And in these cases, the review will be confined to the subject matter of the investigation.”

The initial search occurred in September 2012, when the company was attempting to discover who had handed an anonymous blogger the source code to Windows 8, its then-upcoming operating system. It discovered that the blogger was using a Microsoft Hotmail email address, and that they had used it to send the code to a third party.

“After confirmation that the data was Microsoft’s proprietary trade secret, on September 7, 2012 Microsoft’s office of legal compliance (OLC) approved content pulls of the blogger’s Hotmail account”, said FBI agent Armando Ramirez III in court papers filed Monday.

The company’s user agreement reserves the right to carry out such searches, even after the changes Frank announced. “We may access information about you, including the content of your communications, to protect the rights or property of Microsoft,” it reads.

The news of the search sparked immediate reaction following the Guardian’s report on Thursday. Parker Higgins, an activist for San Francisco-based pressure group EFF, wrote that the decision was a “very bad move” and that he was “reeling from this Microsoft story. Journalists need to be wary of government and corporate espionage. Knowing your source’s ID is a liability.”

Microsoft has taken a further PR hit due to the fact that email privacy has been a key weapon in its fight against Google’s dominance of the sector.

In the company’s “Scroogled!” campaign, it emphasises that “Outlook.com [the service which replaced Hotmail] prioritises your privacy… Your email is nobody else’s business.” It even offers a petition asking customers to “tell Google to stop! Let them know they shouldn’t go through your email”.

Full statement by John Frank, Microsoft’s vice president and deputy general counsel

We believe that Outlook and Hotmail email are and should be private. Today there has been coverage about a particular case. While we took extraordinary actions in this case based on the specific circumstances and our concerns about product integrity that would impact our customers, we want to provide additional context regarding how we approach these issues generally and how we are evolving our policies.

Courts do not issue orders authorizing someone to search themselves, since obviously no such order is needed. So even when we believe we have probable cause, it’s not feasible to ask a court to order us to search ourselves. However, even we should not conduct a search of our own email and other customer services unless the circumstances would justify a court order, if one were available. In order to build on our current practices and provide assurances for the future, we will follow the following policies going forward:

• To ensure we comply with the standards applicable to obtaining a court order, we will rely in the first instance on a legal team separate from the internal investigating team to assess the evidence. We will move forward only if that team concludes there is evidence of a crime that would be sufficient to justify a court order, if one were applicable. As an additional step, as we go forward, we will then submit this evidence to an outside attorney who is a former federal judge. We will conduct such a search only if this former judge similarly concludes that there is evidence sufficient for a court order.

• Even when such a search takes place, it is important that it be confined to the matter under investigation and not search for other information. We therefore will continue to ensure that the search itself is conducted in a proper manner, with supervision by counsel for this purpose.

• Finally, we believe it is appropriate to ensure transparency of these types of searches, just as it is for searches that are conducted in response to governmental or court orders. We therefore will publish as part of our bi-annual transparency report the data on the number of these searches that have been conducted and the number of customer accounts that have been affected.

The only exception to these steps will be for internal investigations of Microsoft employees who we find in the course of a company investigation are using their personal accounts for Microsoft business. And in these cases, the review will be confined to the subject matter of the investigation.



The privacy of our customers is incredibly important to us, and while we believe our actions in this particular case were appropriate given the specific circumstances, we want to be clear about how we will handle similar situations going forward. That is why we are building on our current practices and adding to them to further strengthen our processes and increase transparency.

• Former Microsoft employee arrested over Windows 8 leaks