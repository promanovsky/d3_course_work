WASHINGTON (MarketWatch) — U.S. producer prices rose in March at the fastest rate in nine months, owing largely to higher costs experienced by clothing retailers, grocers and wholesalers of chemical goods, the government said Friday.

The producer price index advanced a seasonally adjusted 0.5% after falling slightly in February, the Labor Department said. That’s the largest increase since last June and surpassed the 0.1% estimate of economists polled by MarketWatch.

The wholesale cost of services surged 0.7% — the biggest spike in more than three years — to push the index higher.

Yet the cost of goods was flat, as a 1.1% increase in wholesale food was offset by a 1.2% drop in the price of energy.

Excluding the volatile categories of food and energy, so-called core producer prices jumped 0.6% last month.

The spike in prices pushed the year-over-year increase in U.S. wholesale costs to 1.4% from just 0.9% in February. That’s the highest rate since last August.

What’s more, some inflationary pressure could be building in the pipeline. The price of unprocessed goods such as animal feed or sheet metal has risen by 5.8% in the past 12 months, the biggest increase since last summer. If those costs keep rising, they could eventually push up the price of wholesale goods and filter into consumer products.

Yet the relationship is not precise because companies raise or lower prices for a number of reasons. And by historical standards, wholesale prices remain quite low. The Federal Reserve has actually been trying to nudge inflation higher but with little success.

Some economists also find hard to decipher the government’s new method of calculating wholeprice prices — the report was overhauled in January for the first time in years. They question the usefulness of trying to measure the wholesale price of services and say it may take awhile to determine if the new approach is useful. Read: Economists struggle to make sense of producer price jump.

Meanwhile, a new index that tracks prices of goods and services meant to be sold to consumers also rose sharply in March. The index, known as personal consumption, climbed 0.6%. It’s expected to give a heads up if consumer inflation is about to shift up or down.

The true cost to Americans is better seen through the consumer price index, which measures what people actually pay. The consumer price index, to be released Tuesday, is expected to rise a scant 0.1% in March for the third month in a row. Consumer prices have risen just 1.1% in the past year.

More news from MarketWatch:

Hawaii, Texas banks outperform Wall Street titans

Natural gas supply shortage could jack up prices