At this year's American Society for Clinical Oncology (ASCO) meeting, a medical conference that starts Friday in Chicago, some of the world's biggest drugmakers will show off the latest data from a new class of cancer drugs that could bring new hope to patients — and enrich investors.

These drugs are part of a field called immuno-oncology (I-O), which uses technologies that manipulate the patient's immune system to help it fight cancer.

Bristol-Myers Squibb (BMY) has already launched one such product, Yervoy, which is expected to crack the $1 billion sales mark this year thanks to its FDA approval for fighting melanoma. Bristol, though, has much bigger ambitions for Yervoy, especially in combining it with nivolumab, its newer immuno-oncology candidate.

Nivolumab is a PD-1 inhibitor, a drug that attacks the mechanism that lets cancer cells fool the immune system into leaving them alone. Merck (MRK) also has a PD-1 inhibitor in its pipeline called MK-3475, and AstraZeneca (AZN) and Roche are developing similar drugs called PD-L1 inhibitors.

Until recently, Bristol-Myers was assumed to be the leader in the field for two reasons.

The first is that the company is one of the furthest along in clinical trials, and its excellent past results against squamous non-small-cell lung cancer (NSCLC) — an especially virulent form of the disease that has few treatment options — led investors to hope that Bristol would file for approval based on phase-two data and be first to market.

The second is that Bristol is testing nivolumab combined with Yervoy for lung cancer. If successful, the combination would mean revenue from both drugs in a much larger market than for melanoma.

In January, however, Merck surprised the Street by starting a rolling submission process for MK-3475 in melanoma. That means it will add data as they come in until the application is complete, which it expects to happen before the end of June. Bristol announced in April that it is starting the same process for nivolumab in squamous NSCLC, but it still seems likely to get to market second.

Story continues

Then on May 14, the abstracts for presentations at theASCO meeting, which runs through Tuesday, went online. The abstracts gave investors their first good look at the combination trial of nivolumab and Yervoy in NSCLC and kidney cancer. They also cover trials testing the effect on lung cancer of nivolumab alone and combined with Roche's Tarceva.

The result: Bristol stock fell 6%.

In a May 15 research note, BMO Capital Markets analyst Alex Arfaei downgraded Bristol's stock to market perform.

"In NSCLC, the focus was on the Nivo+Yervoy data, which showed a modest 22% response (rate) and a poor adverse event (AE) profile: 48% grade 3/4 AEs (on a scale of 1 to 5 by severity), most leading to discontinuation, and 6.5% drug-related deaths," he wrote. "Similarly, the addition of Tarceva to nivo doesn't seem to add much, and the additional benefit of combining nivo with chemo(therapy) does not seem worth the added toxicity.

Arfaei was also underwhelmed by the nivolumab-Yervoy data on kidney cancer, which was better than on lung cancer but not better than nivolumab had done with other agents.

T.J. Qatato, co-manager of the Frost Growth Equity Fund, told IBD that he was disappointed for the same reasons. But he advocates patience, pointing to the learning curve of clinical trials.

"You still have Bristol-Myers experimenting with what the right dosage is with Yervoy, given it is their first I-O drug," he said. "We've also known that it does have higher adverse events than some of the other I-O drugs. You're dealing with some small sample sizes as well — try not to draw too many conclusions.

Qatato, who owns Bristol-Myers shares, says that he'll be looking for the late-breaking abstracts, which will be made public on Friday. Morgan Stanley analyst David Risinger echoed that sentiment when he reiterated his overweight rating on May 15, writing in a note that Bristol should unveil more mature data based on longer-term treatment. Risinger added that "management continues to feel confident about moving nivo + Yervoy combo into Phase 3 for lung cancer later this year.

Merck's abstracts, by contrast, were in line with expectations, though Qatato says he's disappointed that the company isn't speeding up its lung-cancer timeline to compete with Bristol's.

That's not a complaint that anyone can lodge against AstraZeneca. On May 8, the company announced that it will start a phase-three trial of its PD-L1 inhibitor, MEDI4736, in NSCLC, skipping phase two.

At the ASCO meeting, investors will get a look at some of the phase-one data, which so far consist of only a couple of small studies. One looks at 26 patients with lung cancer, melanoma or other cancers. The response rate was only 15%, which compares unfavorably with the response rates surpassing 30% for Bristol's and Merck's drugs.

Morningstar analyst Damien Conover, however, points out that the AstraZeneca study's patient count is tiny and that typically only a subset of patients respond to these drugs — especially those who express the PD-L1 biomarker. (A biomarker is a measurable substance or characteristic that indicates the presence of a disease or another biological condition or process.) In fact, Bristol's study of nivolumab as a monotherapy (nivolumab without other drugs) for lung cancer found that while the overall response rate was 30%, the rate for PD-L1-negative patients was zero.

Fund manager Qatato hopes that the ASCO meeting will bring more information on this biomarker.

"I don't have a good sense of the population of cancer prevalence for the PD-1 expression," he said. "What's difficult is that PD-1 expression isn't just positive or negative. You can have 1%, 5%, 50%. If we got a better sense of how big that population is, we would get a better feel for what kind of market opportunity we can throw on this whole I-O revolutionary treatment for cancer.

But Conover says that in the near term, this ignorance of PD-1 prevalence will likely keep the market big.

"My sense is because there are no treatments out there, and because it (the family of PD-1 drugs) works very well if you get the right patients, it's going to be used across the board until the scientific community figures out PD-L1 status a little bit better," he said.