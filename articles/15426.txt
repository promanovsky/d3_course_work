NEW YORK — More than two dozen attorneys general sent letters Sunday to five of the country’s largest retailers, encouraging them to stop selling tobacco products in stores that have pharmacies, which would follow the example CVS Caremark set with its announcement this year that it would stop selling such products in its drugstores.

The letters were sent to Rite Aid, Walgreen, Kroger, Safeway, and Walmart, companies that are among the biggest US pharmacy retailers.

“There is a contradiction in having these dangerous and devastating tobacco products on the shelves of a retail chain that services health care needs,” the letters said.