Almost four out of 10 Canadians who don't have a job have completely given up hope of ever finding one, a new survey suggests.

In a poll carried out by Harris Poll and published Friday by employment agency Express Employment Professionals, the company surveyed 1,502 unemployed Canadians. None of them had a job, and not all of them were receiving EI benefits.

The results were eye-opening.

That headline jobless rate doesn't necessarily capture how weak the jobless picture really is. - Doug Porter, Bank of Montreal economist

Some 39 per cent of those polled were in agreement with the statement that "I've completely given up on looking for a job" with five per cent saying they "agree a lot" 11 per cent saying they "agree somewhat" and 17 per cent saying they "agree a little."

In the poll, which saw people respond to questions online over a week in April, more than a third responded they hadn't had a job interview in over a month. A full 13 per cent of respondents said they hadn't had a job interview since 2012 or before — well over a calendar year ago.

"The results of this survey should serve as a wake-up call to policymakers that some unemployed Canadians are falling behind," Express CEO Bob Funk said. "If left unchecked, they could fall into a trap of prolonged unemployment and risk being left out of the workforce entirely."

With numbers like that, it's not hard to detect the bleakness emanating from employment centres across the country.

"I'm not even going to look for a job anymore, because I've been searching for six months and nothing is happening," says Shirlon Marshall, an unemployed short order cook looking for a job at a Toronto-area job centre.

Lack of jobs

Marshall has been out of work for a year but says despite ample experience and resume help from job centre JVS Toronto in the city's Jane and Finch area, he hasn't been able to even get interviews.

"I have experience, I've been to school for it," Marshall says. "I don't see the reason why I can't even get a dishwashing job." And that, despite sending out 60 resumes in a single week, he says.

Would-be dental assistant C.C. Hooper tells a similar story. She went to school for training in her desired field, but hasn't been able to find anything but part-time temporary work in the eight years since. "It's very frustrating," Hooper says. "I am exhausted of looking for jobs in general but at the same time I'm open-minded."

Marshall and Hooper are certainly not alone. According to official Statistics Canada data, the number of people who the agency defines as "long-term unemployed" (having not had a job for more than half a year) has almost doubled from 142,000 before the recession, to 272,000 today.

The number of people who've been jobless for a year has increased by even more, from 46,000 in 2007 to 96,000 last year.

Numbers like that have the central bank worried. In its spring review, the Bank of Canada pointed out that while the jobless rate has fallen to 6.9 per cent, the long-term share of the total unemployment rate is closer to 20 per cent.

"There's a concern that those people might never be employable again," BMO economist Doug Porter said in an interview. "The longer they are out, the harder it is to get back into the labour force and this is what we saw in the 1990s as well."

That's bad for those people, but also bad for Canada as a whole because it becomes a permanent drag on the economy, he said.

"That headline jobless rate doesn't necessarily capture how weak the jobless picture really is," Porter said.