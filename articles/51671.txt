Rumors have been circulating for years that Microsoft was working on a version of Office for the iPad, with speculation getting so intense at one point that the company went out of its way to categorically deny it. Still, once Microsoft released Office for iPhone, an iPad version seemed more plausible; indeed, credible sources said it would arrive in the first half of this year. Turns out, there was something to all that scuttlebutt: Microsoft today announced Office for iPad, a trio of apps that bring Word, PowerPoint and Excel to tablets. Those should be showing up in the App Store shortly -- around 11AM Pacific (2PM Eastern), to be exact.

As with the iPhone version, these apps are free to download, and you can view documents/give presentations without a paid subscription. As ever, you'll need to sign in with a OneDrive or SharePoint account to retrieve your documents, or else have your stuff stored on the iPad itself. However, if you ever want to edit a doc or create something from scratch, you'll need an Office 365 subscription (you can even use Microsoft's cheaper Office 365 Personal plan when it launches later this spring). If you like, you can also sign up for a 30-day trial at Office.com.

Gallery: Office for iPad screenshots | 10 Photos

















/10 Gallery: Office for iPad screenshots | 10 Photos

















/10

Once that expires, though, you'll need to pony up, at which point Office for iPad will count toward a new five-tablet limit -- not your five PC/Mac limit. As you may have gathered by now, this is all par for the course for Microsoft, though it's worth pointing out that Apple offers its iWork suite for free on iOS devices purchased on or after September 1st, 2013. And it's a fairly feature-rich suite, too.

What's especially interesting about Office for iPad is that it's not just a blown-up version of the iPhone version, which we pooh-poohed for offering a watered-down feature set. Here, Microsoft was careful to take advantage of the iPad's extra screen real estate: whereas the iPhone version wouldn't even let you add rows in the middle of a spreadsheet, the iPad edition includes advanced features like Sparklines and author blocking, which only power users might appreciate. Throughout, too, you'll find an impressive array of formatting options, including custom text colors, dozens of PowerPoint transitions and a full suite of fonts. In PowerPoint, there's also a presenter mode where you can write off-handed remarks in "white board mode" and use your finger as a laser pointer (though that last feature doesn't seem that useful, at least not without AirPlay support). Excel, meanwhile, brings a custom numerical keyboard alongside the traditional QWERTY one. And of course, all the apps rock the ol' Ribbon UI -- it wouldn't be an Office product without the Ribbon.

Update: Our review of Office for iPad is now live. Check it out!