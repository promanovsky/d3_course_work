Cervical cancer occurrence rates in American women, particularly in older women and in black women, may be significantly higher than was previously believed, a new study indicates.

Women over the age of 65 -- the age at which women are normally told they can stop regular screening -- were found to have high rates of occurrence, the study in the American Cancer Society's journal Cancer said.

Women have for years been told, upon reaching 65, they could dispense with Pap screening if 10 years of the procedure showed no serious pre-cancer evidence.

"Yet our corrected calculations show that women just past this age have the highest rate of cervical cancer," senior author Dr. Patti Gravitt, of the Johns Hopkins Bloomberg School of Public Health says.

Previous studies had underestimated cancer rates because they included women who had their cervixes removed in a hysterectomy -- as one in every three women has by the age of 60 -- with therefore no chance of having cervical cancer, the researchers said.

In the current study, when those women were removed from the data, the researchers determined an overall cervical cancer rate of 18.6 cases out of 100,000 women, with the rate of cases increasing with age and peaking at 27.4 cases per 100,000 between ages of 65 and 69.

That was an increase of almost 60 percent in the rate when the women who had hysterectomies were factored out, the researchers said.

Adjusting for the data for hysterectomy also substantially emphasized the rate disparity between white and black women, caused by a higher prevalence of black women having hysterectomies as compared to white women, they said.

The study results suggest cervical cancer screening strategies may need to be reconsidered, with special attention given to recommendations about when women should cease seeking screening, researcher Anne F. Rositch of the University of Maryland School of Medicine said.

"It's really important to understand who in the U.S. is currently at the highest risk of invasive cervical cancer," she said. "By looking at the patterns after correction, by both age and race, we have better identified women who are at highest risk and that are at a higher risk than we previously recognized."

Regular screenings have played a big part in reducing the number of women dying from cervical cancer, once a leading cause of death from cancer for American women, the Centers for Disease Control says.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.