CNET

Turns out Nest's smart devices will share some of your household data with parent company Google after all -- at least if you use some newly announced integrations with Google services.

Nest, the maker of a smart thermostat and smoke detector, launched on Tuesday a new program that will let outside developers integrate their services and devices with Nest's products. According to the Wall Street Journal, part of that program means sharing 'limited user data' with its developer partners, including, for the first time, it's new parent Google.

Some privacy advocates howled at the thought of Google collecting personal data from people's houses when the search giant bought Nest, the maker of smart home devices, in January for $3.2 billion.

To be clear, Google is just one of many newly announced Nest partners. Users will be able to program their thermostats using Google Now, the search giant's personal assistant app. Other partners include Whirlpool and Mercedes, who's products will also be able to communicate with Nest's growing product line.

Matt Rogers, Nest co-founder and vice president of engineering, told the Journal that the Google integration would allow the search giant to know when a user is home or not. He also said the company would share the data only when a user opts into using a device integration, and emphasized that the Google integration would be like any other third party partnership.

A Nest spokesperson reiterated the point that Google, as a developer, is no different than any of Nest's other partners."Google can use the platform and create an interesting offering in the same way that any developer might," she said. "Google is subject to the same terms and conditions as other developers who are part of the program."

Lee Tien, senior staff attorney at the Electronic Frontier Foundation, was slow to rush to judgement, saying that much is still unclear about Nest's plans. But he expressed concern over Google knowing when users leave the house. "Assuming that Google and Nest now better know whether you are home or not, what could Google or anyone with access to that information do?"

"Will we see insurance companies and cops and other entities trying to get this data?" he added.

The wariness around the announcement points to concerns that security and privacy advocates have about data collected from the home, and the kind of relationship Nest will have with Google. For Google, which makes much of its revenue on targeted ads, data plays are nothing new. The company already has mapping and location services that can determine where user live. But intimate data about things like a Nest user's energy consumption, would be lucrative information.

Google is expected to highlight its push onto new platforms, including in the living room and home, on stage tomorrow at I/O, the company's annual developer conference in San Francisco.

Nest has been adamant about it's independence."We're not becoming part of the greater Google machine," Rogers told the Journal. In a tweet on Monday, Nest CEO Tony Fadell also stressed that he runs Nest as a separate business, with its own management and brand.

"[Nest] has said there is a significant operational separation," said Tien. "But who knows how long that lasts?"

Nest has tried to quell privacy concerns from almost the very moment the Google acquisition was announced. While the company stressed that it would not change its privacy policy, the company never outrightly said it would not open itself up to Google in future.

"We had long discussions [with Google] about what they do with users' data today," Fadell told CNET at the time of the acquisition. "And we had them read our privacy policy, which clearly limits [use of Nest owners' data] only to improving Nest users' [experiences] they looked at us and said that's what they wanted to do."