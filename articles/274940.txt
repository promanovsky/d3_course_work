Miranda Lambert naming her new album Platinum might turn out to be a self fulfilling prophesy. Not only is the record, which came out on Tuesday, already on top of the iTunes sales chart, the critics are in love as well. Lambert’s strength has always been in her clever lyrics, her fiery sound and, well, her sass. Fans will be happy to hear that, despite her being a married woman now, the sass is still strong with this one.



The new record shows a maturity that really suits Lambert, say reviewers.

Says Jamieson Cox of TIME Magazine: “Platinum doesn’t reinvent the wheel in any sense, but it doubles down on the qualities that have helped Lambert become one of country’s most reliable stars: broad, plain-spoken feminism, rousing choruses that split the difference between archetypal country and contemporary pop-rock, and frank, funny, fully realized depictions of the protagonist at the heart of these songs.

As for the lyrical component, the now 30-year-old Lambert spends more time looking back than she did on any previous record, but it certainly doesn’t hurt the album’s depth.

More: Despite Tabloid Reports, Miranda Lambert Is Not Getting Divorced, Pregnant Or Anorexic



Lambert's duet with Carrie Underwood divides critics.

“Lambert uses whatever country-leaning style suits her material best. She may be most effective, though, when, like her husband, Blake Shelton, she romanticizes the glory days of country, as she does in the first single, "Automatic," and the gorgeous "Another Sunday in the South, writes Glenn Gamboa of Newsday. “But considering how great Platinum is, Lambert better watch out. She may be leading country into a new Golden Age.”

More: Miranda Lambert Talks Admiration For Beyonce And Relationship With Husband, Blake Shelton

There’s a new maturity and depth to Lambert’s familiar barn-burning style, which makes everything sound more genuine. Per the LA Times: “”Platinum” takes on the indignity of aging with wry humor, as in Gravity Is a B**ch, and cheerful determination, as in the title track, where the 30-year-old Lambert insists, “What doesn’t kill you only makes you blonder.”” The one song that falls flat, according to Mikael Wood, is the Carrie Underwood duet Something Bad, although several other reviewers hail the song s a defining piece of the album.



Lambert draws on themes of ageing, feminism and her relationship with Blake Shelton for inspiration.