Made In Space 3D printer for ISS gets NASA nod

NASA has given the green light to a 3D printer specially designed for the International Space Station, with the microgravity-ready fabricator now expected to blast off in August this year. Made In Space announced plans to put a 3D printer in orbit last year, working with the US space agency on the idea that, rather than ship specific components and parts to the ISS, it makes more sense to produce them on-demand.

The result is a carefully crafted 3D printer that has been fettled to take into account the unusual environment of little to no gravity, among other factors.

Like many Earth-based 3D printers, the Made In Space model uses an extrusion-based additive process in which layers are built up in series. The first tests in-situ will be of 21 demonstration parts, including parts and tools of the sort that the ISS crew might need to commonly replace.

As it stands, any supplies or components the ISS needs have to be shipped up to the orbiting research platform in advance. That demands foresight and expensive launches, particularly with the NASA Space Shuttle program now no longer in operation.

NASA is looking to private resupply firms, like SpaceX, to fill the gap. However, that still requires significant advance planning.

Made In Space’s approach is more responsive to the day to day needs of the ISS and its crew, including catering to unusual or unique tools or parts. After the tests of the extrusion process and the accuracy of the resulting prints, the goal is to establish a permanent “Additive Manufacturing Facility (AMF) on the station, which would be able to produce larger objects.

Meanwhile, counterpart models on Earth could be used to prototype components in space, with Made In Space planning to make the hardware available to researchers, businesses, and even individuals.

NASA’s approval – testing the printer’s resilience to EMI and vibration, that its materials and interfaces comply with ISS standards, and that it’s readily usably by ISS crew – means that the printer can go up ahead of schedule. Instead of SpaceX’s CRS-5 mission, expected in late November, it will now be transported by the CRS-4 mission in August.

SOURCE Made In Space