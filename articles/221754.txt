Now that he’s demanding a trial by combat, Tyrion Lannister (Peter Dinklage) must find a champion who will battle to the death on “Mockingbird,” Episode 37 of HBO’s “Game of Thrones.”

Falsely accused of poisoning his nephew, King Joffrey Baratheon (Jack Gleeson), Tyrion will let the gods decide his fate. If his fighter prevails, Tyrion goes free. If his fighter fails, they’re both dead.

Tyrion’s first choice is older brother Jaime (Nikolaj Coster-Waldau), an expert swordfighter until his right hand was severed.

“My training has proved I can’t beat a stable boy with my left hand,” Jaime admits.

Advertisement

Tyrion’s second choice is Ser Bronn (Jerome Flynn), a crafty swordsman capable of besting most men. But can Bronn defeat Ser Gregor “The Mountain” Clegane (Hafpor Julius Bjornsson)?

He’s the champion fighting on behalf of Cersei Lannister (Lena Headey), who desperately wants Tyrion beheaded to avenge the murder of her son Joffrey.

“One misstep and I’m dead,” Bronn says, refusing to battle such a “freakish big, freakish strong” opponent.

“I suppose I’ll have to kill The Mountain myself,” dwarf Tyrion says. “Won’t that make for a great song?”

Advertisement

But hold the music! Tyrion surprisingly finds a defender in Prince Oberyn Martell (Pedro Pascal). The Mountain slaughtered Oberyn’s sister and her children. And now it’s payback time.

“All those who have wronged me are right here” in King’s Landing, Oberyn says, referring to The Mountain and to devious patriarch Tywin Lannister (Charles Dance), who ordered the brutal killings.

“I will be your champion,” Oberyn declares.

Elsewhere in Westeros, The Mountain’s equally huge brother, Sandor “The Hound” Clegane (Rory McCann), is on the road with young Arya Stark (Maisie Williams). Their destination is The Eyrie, where he expects to receive a ransom for delivering Arya to her wealthy Aunt Lysa Arryn (Kate Dickie).

Advertisement

In a rare bit of candor, The Hound tells Arya how one side of his face was nearly burned off by his cruel sibling.

“Pressed me to the fire like I was a nice, juicy mutton chop,” The Hound recalls. Thus he’s disfigured for life and terrified of fire – all because he dared to play with his brother’s toy.

At The Eyrie, meanwhile, Arya’s older sister Sansa (Sophie Turner) fears she’ll never again see Winterfell, her family stronghold in the North.

“A lot can happen between now and never,” points out Petyr “Littlefinger” Baelish (Aidan Gillen), who rescued Sansa so she wouldn’t be tried for Joffrey’s murder.

Advertisement

Sansa hated Joffrey and his kin for killing her parents and brother. But Petyr orchestrated the teen king’s assassination.

“Why did you really kill Joffrey?” Sansa asks.

“I loved your mother more than you can ever know,” he tells her. And in a better world, “you might have been my child.”

With that, Petyr grabs Sansa and kisses her on the mouth. Observing the passionate embrace is loony Lysa, who’s fiercely jealous of her new husband.

Advertisement

Lysa summons Sansa to join her beside the infamous Moon Door, which opens to the valley floor thousands of feet below.

“It’s fascinating what happens to bodies when they hit the rocks from such a height,” Lysa morbidly observes. Then she erupts in anger, calling Sansa a “little whore” and struggling to push her out the hatch.

Petyr quickly intervenes at this juncture, swearing he’ll send Sansa away. Once she’s out of danger, however, Littlefinger utters his final words to Lysa.

“I have only loved one woman – only one woman my entire life"” he reveals. “Your sister!”

Advertisement

With that, he shoves Lysa through the Moon Door and listens to her scream as she plunges to a horrific, bone-shattering death.