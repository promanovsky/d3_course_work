While Microsoft is expected to announce the long-rumoured Surface Mini tablet on Tuesday, it seems the Surface Pro 3 might join the company's new Surface line-up as well.

A Twitter user named @NotCassim on Wednesday spotted a reference to the Surface Pro 3's camera on a Microsoft support article titled 'Add recording support H.264 for modern communication apps in Windows 8.1 or Windows Server 2012 R2.' Microsoft removed the reference on the page soon after, but not before a screenshot was taken (seen above).

Earlier a Bloomberg report that cited sources familiar with the Microsoft's plans had claimed that the Redmond giant will also introduce, apart from the Surface Mini tablet, other new Surface models including the tablets powered by Intel chipsets at its May 20 event. So far, no further details have been leaked about the Surface Pro 3.

Microsoft had recently sent invites to a small and closed event in New York City. The text of the invite reads "Join us for a small gathering," which could very well be Microsoft hinting at the announcement of the rumoured Surface Mini device. The device is also rumoured to be powered by Qualcomm-based chipsets, instead of Nvidia Tegra SoCs.

In addition, Paul Thurrott of Windows IT Pro reports that the rumoured Surface Mini tablet will come with stylus support and OneNote integration, which is Microsoft's note-taking software.

According to previous leaks and rumours, the Microsoft Surface Mini will sport an 8-inch display, run Windows RT 8.1 and be based on an ARM chipset. It is also rumoured that the tablet will hit market shelves by late June and will be available with a click-in cover which will support multi-position kickstands.

We had recently reported also about how an accessory manufacturer named Vostrostone (VSTN) had listed its smart cover and Bluetooth keyboard for an alleged Surface Mini 8-inch tablet on Amazon, further cementing claims.