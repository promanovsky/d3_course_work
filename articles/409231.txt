Keith Urban's recent "Raise Em Up" tour stop in Mansfield, Massachusetts resulted in a number of alcohol related illness and arrest. The show took place on July 26, and, according to various reports, as many as 46 fans were treated for alcohol related illness.

22 of those treated on the spot were later transferred to local hospitals.

Mansfield Police Chief Ron Sellon and Fire Chief Neil Boldrighini released a joint statement on the incidents:

"Last evening's Keith Urban concert was not anticipated to present with the volume of issues handled, but measures were in place to provide appropriate and adequate care and protection to all patrons. As a result, although there was a higher than expected volume, appropriate care was available through the expandable model."

They added: "The large number of medicals in a shortened time of approximately three hours caused the activation of a phase one EMS plan utilizing ambulances from five mutual aid communities. Police dealt with a steady stream of intoxicated persons as well, resulting in over 50 people being taken into protective custody and a number of others arrested for alcohol-related issues."

For comments and feedback contact: editorial@rttnews.com

Entertainment News