In theaters Friday, May 23

2 1/2 stars (out of 4 stars)

The carcasses left behind from Adam Sandler's movies aren't pretty. You Don’t Mess With The Zohan. That's My Boy. Grown Ups. Grown Ups 2. The one with Keri Russell. The one where he played fraternal twins.

But even those befuddled by the appeal of SNL's guitar-playing man-boy (now 47!) must admit that he’s never been more charming than when he flirts with Drew Barrymore in front of a camera. Whether he’s serenading her on an airplane in 1998’s The Wedding Singer or wooing her in a marina in 2004's 50 First Dates, the guy knows how to raise his game—and put on a nice dress shirt—in the company of a beauty.

You’ve likely seen the trailer for their third rom-com collaboration. Consider this review as confirmation of your fears and hopes: The flick is really really really really dumb, yet gets by thanks to the sweet chemistry of its stars.

Barrymore is Lauren, a divorcee with two prepubescent boys on her hands. She goes on a blind date with Sandler’s Jim, himself a widower with three adorable girls. It’s a disaster with a capital D. He’s put off by her uptight attitude and matronly outfit; she’s repulsed that he took her to a Hooters and bogarted her beer. (Sadly, Big Daddy's Corinne is not their waitress.). They’re just not that into each other. At all.

In a ridiculous only-in-the-movies contrivance, they end up on vacation with their respective families at the same South African resort. And. . . wait for it. . . the staff is under the impression that they’re a couple! Soon, the group reluctantly goes on safaris and sits together during dinners under the stars. The sight of Sandler falling off a giant ostrich amuses for about two seconds. More demoralizing is the thought that the cast got a paid trip to an exotic location for months while you’re sitting in a dark theater watching Barrymore nearly slit her pants on the horn of a rhinoceros. Isn’t she too old for silly slapstick?

Ah, but a few extra years can be a good thing. In 2014, these stars are aware they can’t just be cute and fall in love and call it a movie. To the movie’s credit, Jim and Lauren’s relationship with each other’s kids holds the same weight as their relationship to each other. It’s touching to see Jim show her son how to hit a baseball and talk to girls, while Lauren gives his tomboy 15-year-old daughter (Bella Thorne) a makeover and sings a lullaby to his youngest. The family bonding allows the budding romance to feel organic and playful.

The film also gets it right when Jim and Lauren share their last supper, still as friends who haven’t even kissed. Both of them have dressed up for the occasion and exude a nervous energy in a way that most singles do in the first-date phase. It's a little heart-melting.

But let's not get carried away. No Sandler film would be complete without a spate of eye-rolling boob and pee jokes. And do-me-a-favor roles for pals Kevin Nealon and Steve Buscemi. A few familiar characters pop by, too. (Do you really want to find out here? Thought not.)

Here’s hoping Sandler and Barrymore have more reunions in store. Because no matter how lame their material is, it’s a delight growing old with them.