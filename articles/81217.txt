This is no April Fool’s joke: The Standard & Poor’s 500 index is at a record high and the Dow industrials aren’t far behind.

Encouraging manufacturing data pushed up the major indexes Tuesday morning, stoking optimism that the economy is moving in the right direction after weather-related disruptions in the first quarter.

As of 8 a.m. PDT, the S&P was up 8.37 points, or 0.5%, to 1,880.71, just off its intraday peak of 1,884.60.

The Nasdaq composite index surged for the second day in a row. It was up 48.25 points, or 1.2%, to 4,247.25.

Advertisement

PHOTOS: World’s most expensive cities

The Dow Jones industrial average, which has lagged this year, extended its recent rally. The index rose 62.87, or 0.4%, to 16,520.53. It’s within 57 points of its Dec. 31 closing high.

Investors were pleased by a report showing a solid jump in manufacturing activity.

An index from the Institute for Supply Management rose to 53.7 in March, from 53.2 in February. That was slightly below estimates, but enough to assuage fears that inclement weather early in the year had thrown the U.S. economic recovery off course.

Advertisement

ALSO:

Q&A on paying for college: Financial aid, loans, FAFSA confusion

Value of stock buybacks up sharply in fourth quarter, led by Apple

Can’t hold a good stock market down: S&P 500 sets intraday record

Advertisement

Follow Walter Hamilton on Twitter @LATwalter