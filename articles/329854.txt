After more than a decade of work and nearly $1 billion of his personal fortune, Los Angeles inventor Alfred Mann’s quest to develop an inhalable form of insulin for people with diabetes won approval Friday from U.S. regulators.

His company, MannKind Corp. of Valencia, got the OK on Friday from the Food and Drug Administration to sell the drug called Afrezza for Type 1 and Type 2 diabetes. But the agency added a warning that the product shouldn’t be used by those with diabetes who also have asthma or a serious lung disease.

The drug is a powder that is administered through a device about the size of an asthma inhaler. It would be most often used to help control blood-sugar levels at mealtime, a quick puff replacing a needle injection before a meal.

“We are excited for patients,” said Mann, who is the company’s founder and chief executive. Afrezza “will address many of their unmet needs for mealtime insulin therapy, and has the potential to change the way that diabetes is treated.”

Advertisement

The drug label will also warn that spasms in the airways of the lung have been seen in patients with asthma and chronic obstructive pulmonary disease and will advise against smokers using the medicine, the agency said in a statement.

In recent weeks, MannKind’s stock has been languishing. It closed Friday at $10, down 5.5%, or 58 cents. But the market responded in after-hours trading, with the company’s shares jumping 10%.

It’s been a long, expensive journey to get Afrezza to this stage. MannKind has spent about $1.8 billion developing the drug, with about $975 million of that coming from Mann’s personal wealth. It took almost eight years of seeking approval for the diabetes therapy since starting late-stage clinical trials.

A large portion of money was used to fund more than 60 clinical trials, which have involved about 6,500 patients, Mann said.

Advertisement

The company has also suffered regulatory setbacks; it imposed layoffs to deal with massive financial losses, and it was sued by a former executive who said he was unjustly fired after complaining about MannKind’s research practices. (The lawsuit was settled for undisclosed terms.)

The FDA decided not to approve the drug twice before Friday’s action — most recently in 2011, after the company switched inhalers during the review process.

“I have never considered abandoning the effort because I firmly believe that Afrezza has the potential to bring significant benefits to the still growing and enormous population of people with diabetes,” Mann said recently in response to written questions.

Currently, diabetes affects 29.1 million people in the United States, according to the U.S. Centers for Disease Control and Prevention. Worldwide, about 382 million have the disease, according to the International Diabetes Federation.

Advertisement

“Given the current epidemic size and predicted growth of the diabetes patient population we are confident that Afrezza presents a significant commercial opportunity,” Mann said.

Adnan Butt, an analyst with RBC Capital Markets, said in a June 12 report that he sees huge potential demand for the drug.

“Afrezza would be the most convenient, discreet and rapid-acting insulin alternative for diabetes, one of the largest pharmaceuticals markets,” Butt said.

He forecast that Afrezza could eventually reach $5 billion to $7 billion in annual sales. He estimated that the company’s stock will be trading at $16 a share within a year.

Advertisement

Mann, who lives in Las Vegas, declined to estimate Afrezza’s potential sales, but said the drug “addresses a poorly met need in this community and the entire world.”

Not everyone is sold on the idea.

Dr. Paris Roach, an Indiana University professor and doctor who specializes in the treatment of diabetes, said he believes Afrezza will have a difficult time making a dent in the market if its pricing is not competitive. Many people with diabetes already use convenient insulin pens at meal time, he said.

“It’s kind of like who really needs inhaled insulin at this point with the convenience of the pens and the small size of the needles,” Roach said.

Advertisement

In 2006, Pfizer Inc. introduced the world’s first inhaled insulin product, Exubera, which a year later was yanked from the market because of poor sales, absorbing a loss of more than $2 billion.

MannKind’s share price has ranged from a high of $21.70 to a low of $1.60 in late 2012.

Mann said Afrezza will succeed where Exubera failed because Afrezza acts much faster than insulin injections, providing an additional benefit to its convenience.

Mann is an 88-year-old physicist, inventor and philanthropist whose previous companies have produced pacemakers, hearing aid implants, insulin pumps and other devices. His net worth is valued at $1.7 billion.

Advertisement

John Mastrototaro spent eight years working for Mann at MiniMed Inc., which developed a successful insulin pump and was eventually acquired by Medtronic Inc. for more than $3 billion.

Still, Mastrototaro said he was concerned when he first heard of Mann’s plans for inhaled insulin.

“I actually thought it was a bad idea for him to get into the pharmaceutical space after all his success with devices,” he said. “Pharmaceuticals are just a different beast in terms of the time and money involved. But if anyone could get it done, it’s Al.”

stuart.pfeifer@latimes.com

Advertisement

william.hennigan@latimes.com