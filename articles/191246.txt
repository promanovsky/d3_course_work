Scientist have found moderate coffee consumption can reduce degenerative eye diseases, May 5, 2014. — AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

WASHINGTON DC, May 5 — A coffee a day could help keep eye diseases at bay.

That’s according to a joint study out of South Korea and the US, which concluded that powerful antioxidants found in coffee can play a role in preventing age-related eye diseases and the degeneration of eyesight.

For their research, scientists looked at the impact of chlorogenic acid or CLA, a strong antioxidant that has been shown to prevent retinal degeneration in mice.

To conduct their experiment, mice were treated with nitric oxide, which creates oxidative stress and free radicals and leads to retinal degeneration.

Those that were pretreated with CLA developed no sign of retinal damage.

The retina is a thin tissue located on the back wall of the eye that receives and organizes visual information, researchers explain.

It’s also one of the most metabolically active tissues and requires high levels of oxygen. Without it, the tissue is particularly vulnerable to oxidative stress and prone to the production of free radicals, which leads to tissue damage and loss of sight.

However, it’s not yet known whether or not drinking coffee delivers CLA directly to the retina, researchers stress.

Future studies could lead to the development of a special brew customized for retinal support, or CLA delivery via eye drops.

The study, published in the Journal of Agricultural and Food Chemistry, is the latest to vaunt the nutritional merits of coffee.

Another large scale, US-led study published recently in the journal European Association for the Study of Diabetes found that people who drank three or more cups of coffee a day had the lowest risk of type 2 diabetes -- 37 percent lower than those who consumed one cup or less per day.

Scientists looked at the coffee consumption patterns of 95,000 women and 28,000 men. — AFP-Relaxnews