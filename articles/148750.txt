Researchers from UCSF Benioff Children's Hospital, part of the University of California San Francisco, reported that although codeine is associated with harmful and potentially deadly effects in children, doctors continue to prescribe it.

Codeine is used for treating pain and cough but the American Academy of Pediatrics (AAP) issued a guideline in 1997 which was later reaffirmed in 2006 saying it does not recommend the use of codeine because of associated risks and questionable effectiveness in treating cough. The American College of Chest Physicians has likewise issued a guideline advising against the use of the drug for treating pediatric cough.

Some individuals metabolize the drug too quickly and this could result in harmful side effects such as excessive sleepiness and breathing problems which could potentially cause death.

Sunitha Kaiser, assistant clinical professor of pediatrics at UCSF Benioff Children's Hospital, and her colleagues sought to find out how many times doctors prescribed codeine to children between ages 3 and 7 in emergency rooms from 2001 to 2010. Researchers found that although codeine prescriptions declined during the period, many doctors still prescribed the drug to children, with as many as 877,000 receiving codeine prescriptions per year.

Reporting their findings in the May issue of Pediatrics, Kaiser and her colleagues said they did not see any reduction in prescriptions for codeine that are associated with the guidelines from the academy and the chest physicians group.

The researchers proposed several solutions to reduce codeine prescription to children, including promoting safer alternatives such as hydrocodone as an opiate alternative and ibuprofen for treating injury pain. They also suggested making changes in insurance reimbursement policies and removing the drug from hospital formularies.

"Many children are at risk of not getting any benefit from codeine, and we know there are safer, more effective alternatives available," Kaiser said. "A small portion of children are at risk of fatal toxicity from codeine, mainly in situations that make them more vulnerable to the effects of high drug levels, such as after a tonsillectomy."

Joseph Tobias, chairperson of AAP's Anesthesiology and Pain Medicine section, said it isn't clear why doctors keep prescribing codeine to children despite the risks associated with it. He said that doctors possibly do it out of habit as they were once taught that the drug was a mild and effective opiate for children.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.