"Who are we?" Carl (Chandler Riggs) asks his father Rick (Andrew Lincoln) early in The Walking Dead's fourth season closer - it's a deeper and more pertinent question than one might immediately suspect, since 'A' is all about what our band of survivors have become and what they are willing to do when backed into a corner.

What does it mean to be human in a world where we're a dying breed? What constitutes good and bad anymore? With men driven to extremes, who are the real monsters?

Gene Page/AMC



These questions have always been crucial to The Walking Dead - in essence, the walkers are simply mindless beasts, looking for good, whereas a man like The Governor committed evil acts with full knowledge and deliberation.

This season ender throws these themes into sharp relief - particularly the blurring of the lines between man and flesh-hungry beast, with a desperate Rick sinking his teeth into an opponent's neck and tearing his throat out.

The Walking Dead is frequently grisly - packed with gore and shocks - but rarely is it unsettling in the manner of the scenes featuring Joe and his pack, with their chilling Straw Dogs / Deliverance-esque atmosphere.

The distinction between an anti-hero and a villain has grown increasingly murky on this show - what separates men like Daryl (Norman Reedus) and Rick from men like Joe (Jeff Kober) isn't always clear - but once it is made clear, it becomes abundantly so.

Gene Page



Rick has committed terrible acts, mauling Joe possibly the worst of them, and so has to some extent become a 'bad man' - "That ain't all of it," he says of the darkness in his soul. "But that's me."

But Rick has always acted in self-defence - even when his actions were pre-meditated, as with Shane's death, he was seeking on some level to protect others. It's a thin line but it's there - and on The Walking Dead it is all that now remains to divide 'heroes' and 'villains'.

The scenes in 'A' which see Rick forced to discard his valuable vestiges of humanity are placed in stark contrast to the prison flashbacks, which see the former police officer and family man making his final desperate endeavour to lead a 'normal' life, working the land.

It's neat thematically - a reminder of what Rick is giving up in his violent effort to survive and protect his loved ones - and as an added bonus, affords us the unexpected opportunity to see the wonderful Scott Wilson as Hershel once again.

Gene Page



'A' also defies expectations when it comes to the culmination of the Terminus arc - any hope or happiness is short-lived on this series, so you might expect the make-shift family unit of Rick, Carl and Michonne (Danai Gurira) to be suddenly and permanently torn apart.

Instead, with almost the entire group of survivors now reunited, the episode ends on an almost rousing cliffhanger, but one that links neatly into the episode's overarching theme - who are the real victims, and who should we really fear?

Co-written by showrunner Scott Gimple and producer Angela Kang, 'A' is a superb standalone episode - its strength is that, like the best Walking Dead episodes, it finds time for cathartic moments of real pathos amidst all the horror and madness and mayhem.

As a finale, it does leave an awful lot hanging - perhaps too much - and the complete absence of Tyreese (Chad L Coleman) and Carol (Melissa McBride) feels odd, but these are far from disastrous flaws in what is otherwise a stirring season ender.

4

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io