Shutterstock photo

Investing.com -

Investing.com - The dollar was trading close to four-and-a-half month lows against a basket of currencies on Wednesday, ahead of the Federal Reserve's policy announcement later in the trading day.

The U.S. dollar index, which tracks the performance of the greenback versus a basket of six other major currencies, edged up 0.10% to 79.60, not far from the trough of 79.37 reached last Wednesday.

The Fed was widely expected to continue to roll back its bond purchasing program by $10 billion at the conclusion of its monthly meeting later Wednesday, the first with Janet Yellen at the helm. The central bank was also expected to confirm that recent softness in U.S. economic reports was due to severe winter weather.

The dollar pushed higher against the yen, with USD/JPY rising 0.25% to 101.67, pulling away from last week's lows of 101.19.

The yen shrugged off data on Wednesday showing that Japan reported a larger-than-forecast trade deficit in February, as spiraling energy imports offset import costs.

EUR/USD slid 0.14% to 1.3914, holding below last week's two-and-a-half year peaks of 1.3966.

Overall market sentiment continued to remain supported as tensions over the crisis in Ukraine eased after Russian President Vladimir Putin said Tuesday that Russia is not seeking the partition of Ukraine.

Elsewhere, USD/CHF climbed 0.21% to 0.8748, extending its pullback from last week's two-and-a-half year lows of 0.8699.

The dollar was lower against the pound, with GBP/USD rising 0.21% to 1.6625, ahead of the U.K.'s latest labor market report later in the session.

The Australian dollar slipped lower, with AUD/USD losing 0.12% to trade at 0.9112.

The New Zealand dollar and the Canadian dollar were also slightly lower, with NZD/USD sliding 0.13% to 0.8608 and USD/CAD edging up 0.12% to 1.1148.

The loonie, as the Canadian dollar is also known, remained under pressure after Bank of Canada Governor Stephen Poloz said Tuesday that the bank could have to cut rates if inflation remains low.

Meanwhile, China's yuan fell to eight-month lows against the dollar on Wednesday, with USD/CNY rising above 6.20 for the first time since July 2013, amid speculation that China's central bank is allowing the currency to weaken.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.