Being an early adopter of new tech often means living with glitches. At least, that's what some of the first users of Google Inc.'s (NASDAQ:GOOGL) Android Wear smartwatches are finding out. Early reports say new Google-powered wearables from LG and Samsung are both subject to software issues.

Android Wear sends app notifications, like a missed call or new message, from Google-powered smartphones to your wrist. However, a bug spotted by Android Police in Android Wear prevents users from installing any paid apps from Google's Play Store on the LG G Watch and Samsung Gear Live, which began shipping to customers this week.

The issue stems from a security feature that Google implemented in its app store that was intended to prevent app piracy, a widespread issue on Android devices.

While Google’s app encryption allows paid apps to be downloaded to Android devices, it prevents the installation of companion apps for Android Wear to be installed onto the new smartwatches. Google has not publicly commented on the glitch and did not immediately reply to a request for comment. When Android’s app encryption caused problems during its initial rollout in 2012, Google suspended the encryption's implementation.

Update (7/9): Google has responded to the bug, asking developers to edit their apps and include a temporary fix to allow paid apps to function properly on Android Wear devices. "We're working to make this easier in the future," a Google spokesperson said, "but apologize for the inconvenience in the meantime."

The Samsung Gear Live has also encountered a hardware issue, which was noted by one early adopter on Google+. The charging port built into the Gear Live is easily broken due to a “design flaw,” a user who received the Samsung smartwatch for free at Google I/O said.

Photo: Rory Glynn / Google+

The Samsung Gear Live was given a teardown by iFixIt, which revealed its components and gave it a score regarding its repairability. The site gave Samsung’s Android Wear an 8/10, slightly lower than the LG G Watch’s 9/10 repairability score, but higher than the manufacturer’s Galaxy S5 smartphone, which received a score of 5 out of 10.

The Samsung Gear Live is the Korean manufacturer’s fourth smartwatch release so far this year, and it is the only one to run on Google’s Android Wear platform. Unveiled at the search giant’s annual I/O conference in June, the Gear Live was previously unannounced, unlike the LG G Watch and Motorola Moto 360 watches, which were revealed during Google’s Android Wear announcement in March.

The Gear Live implements a heart rate sensor -- one of the defining features of Samsung’s smartwatch line. International Business Times broke down the differences between Android Wear models from LG and Samsung, and they suggested that customers wait until the Moto 360 is released later this year before making a purchase.

Have you already ordered an Android Wear device? Worried about the initial issues? Let us know in the comments below.