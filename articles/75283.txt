Celebrity

The legendary DJ, who is dubbed Godfather of House, dies from complications with diabetes at the age of 59.

Apr 1, 2014

AceShowbiz - DJ Frankie Knuckles, who is widely dubbed Godfather of House, has passed away. As reported by The Wrap, his longtime Chicago promoter and associate Robert Williams confirmed that Frankie passed away from complications with diabetes. He was 59.

Chicago DJ Vince Lawrence took to Facebook to share the sad news. "A legend has fallen. All hail Frankie Knuckles an inspiration to us all," he wrote. "30 years ago on April Fools Marvin was taken away & i didn't believe it...now this horrible news. #RIPFrankieKnuckles," Questlove wrote after learning of Frankie's passing. "Jesus man. Frankie Knuckles was so under-appreciated. he was the dj that dj's aspired to be. true dance pioneer."

David Morales added, "I am devastated to write that my dear friend Frankie Knuckles has passed away today. Can't write anymore than this at the moment. I'm sorry." Trance music DJ Markus Schulz also took part in the pouring tribute, writing, "RIP Frankie Knuckles. One of the most beautiful human beings to have ever graced this earth. I'll miss that smile."

Born in the Bronx, New York in 1955, Frankie started being a DJ in the early 1970s. The term "house" music is believed to be originated from his popular music performance in a nightclub called The Warehouse in Chicago.