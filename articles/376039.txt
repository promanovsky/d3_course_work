London: Frances Fisher will be starring alongside Helen Mirren and Ryan Reynolds in the upcoming film ' Woman In Gold.'

The American-British drama will tell the real-life story of a Holocaust survivor, Maria Altmann , who fight the authorities to reclaim her family possessions seized by the Nazis during World War II, including Gustav Klimt's famous painting 'The Lady In Gold', the Deadline.com reported.

The film, which will be helmed by director Simon Curtis, will see Mirren take up the role of Altmann and Reynolds as her lawyer, while the 62-year-old actress will play Reynolds' mother.

The BBC Film and Weinstein Company project, which was penned by Alexi Kaye Campbell and will be produced by David M. Thompson and Kris Thykier, also stars Tatiana Maslany, Daniel Bruhl, and Katie Holmes