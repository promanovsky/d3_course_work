MIAMI, March 10 (UPI) -- Video of Justin Bieber's infamous deposition regarding a lawsuit filed against one of his bodyguards has finally made it to the Internet.

On Thursday it was reported that the 21-year-old Canadian pop star was sassy, disrespectful and contentious while speaking to an attorney about an alleged altercation between his bodyguard and paparazzo Jeffrey Binion. The photographer claims Bieber had his bodyguard beat up him and threaten him with a gun last June.

Advertisement

Clips of the deposition show Bieber undermining the attorney every chance he gets and refusing to answer questions because he doesn't understand them or simply refuses to acknowledge them. The singer also failed to answer any questions about his ex girlfriend Selena Gomez and can be seen repeatedly telling the attorney to not "ask [him] about her."

You can check out the clips below.

[TMZ]