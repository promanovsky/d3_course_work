WASHINGTON -- A quick candy bar may stave off more than hunger. It could prevent major fights between husbands and wives, at least if a new study that used voodoo dolls is right.

That's because low blood sugar can make spouses touchy, researchers propose.

In fact, it can make them "hangry," a combination of hungry and angry, said Ohio State University psychology researcher Brad Bushman.

"We need glucose for self-control," said Bushman, lead author of the study, which was released Monday in the Proceedings of the National Academy of Sciences. "Anger is the emotion that most people have difficulty controlling."

The researchers studied 107 married couples for three weeks. Each night, they measured their levels of the blood sugar glucose and asked each participant to stick pins in a voodoo doll representing his or her spouse. That indicated levels of aggression.

The researchers found that the lower the blood sugar levels, the more pins were pushed into the doll.

In fact, people with the lowest scores pushed in twice as many pins as those with the highest blood sugar levels, the researchers said.

The study also found that the spouses were generally not angry at each other. About 70 per cent of the time, people didn't put any pins in the doll, said study co-author Richard Pond Jr. at the University of North Carolina at Wilmington. The average for the whole study was a bit more than one pin a night per person.

Three people put all 51 pins in at one time -- and one person did that twice -- Pond said.

He said there's a good physical reason to link eating to behaviour: The brain, which is only 2 per cent of the body weight, consumes 20 per cent of our calories.

Bushman said eating a candy bar might be a good idea if spouses are about to discuss something touchy, but that fruits and vegetables as a better long-term strategy for keeping blood sugar levels up.

Outside experts gave the study, funded by the National Science Foundation, mixed reviews.

Chris Beedie, who teaches psychology at the Aberystwyth University in Britain, said he thought the study's method was flawed and that his own work disagrees with Bushman's conclusions. The better way to test Bushman's concept is to give people high glucose on some occasions and low glucose on others, and see if that makes a difference in actual acts of aggression, he said.

But Julie Schumacher, who studies psychology and domestic violence at the University of Mississippi Medical Center, called the study well-designed and said it is reasonable to conclude, as the study did, that "low glucose levels might be one factor that contributes to intimate partner violence."

Still, she and Beedie said it might be a big leap to interpret the results with voodoo dolls as indicating risk for actual physical aggression against a spouse.

The study procedure also raised another problem. Bushman had to handle a call from his credit card company, which wanted to make sure it was really he who had spent $5,000 to buy more than 200 voodoo dolls.