UPDATE, Mar. 25, 7PM EST: In a later statement, Sony clarified that while it's currently focused on its own platform, there's still a possibility it might work with Google's Android Wear in the future.

"We're excited about the potential of Android Wear to extend the mobile OS experience into wearable devices. While we are currently focused on our in-market wearable offering, including SmartWatch 2, we continue to work closely with Google as a key partner and continue to evaluate opportunities across a number of areas as we extend our SmartWear Experience," the company told CNET in a follow-up statement.

Sony will not use Google's Android Wear platform for its upcoming wearable products, CNET reports.

Instead, Sony will keep using its Android-based SmartWatch platform, which was the basis for the company's SmartWatch and SmartWatch 2 products.

"We've already invested time and resources on this platform, and we will continue in that direction," Ravi Nookala, head of Sony Mobile's U.S. arm, told CNET.

Google has launched its Android Wear platform for wearables in March 2014. Consumer electronic bands including HTC, Asus and Samsung have jumped on the bandwagon, and Motorola and LG have already announced the first products based on Android Wear — Moto 360 and LG G Watch.

Sony was one of the first major brands to launch a smartwatch, and it already has more than 200 apps for its platform. The most recent wearable Sony has announced is the SmartBand, a wristband which enables you to log your daily activities.