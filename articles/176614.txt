Sotheby's agreed Monday to appoint hedge fund activist Dan Loeb and two allies to its board, averting a potentially ugly proxy battle over control of the prestigious fine art auctioneer.

A day before a contentious Loeb-pushed vote could have upended company management, Sotheby's bowed to pressure and agreed to appoint Loeb along with restructuring expert Harry Wilson and Olivier Reza, a renowned jeweler and former banker, to the board.

The deal will take the size of its board from 12 to 15.

Loeb, head of the hedge fund Third Point, meanwhile will be allowed to build his stake to 15 percent from just below 10 percent, having been blocked earlier by a "poison pill" defense the board had set up to fend off his push for more influence in the company.

"We welcome our newest directors to the board and look forward to working with them, confident that we share the common goal of delivering the greatest value to Sotheby's clients and shareholders," said chief executive Bill Ruprecht.

Loeb, who last year launched a harsh attack on company management after amassing his stake, said he was "delighted" and agreed to end his legal challenge to the board and to the poison pill defense, which will also be terminated on Tuesday.

A major art collector as well as one of Wall Street's most aggressive investors, Loeb pledged to work together with Sotheby's "to unlock shareholder value by pursuing a strategy of sound capital allocation and growth while respecting the best of the company's rich history, tradition and culture."

The conflict was set to come to a head at Tuesday's annual meeting at Sotheby's headquarters on the Upper East Side of Manhattan. Loeb had proposed a slate of three nominees in opposition to three company-sponsored board members.

On Friday, a Delaware court rejected Third Point's efforts to delay the annual meeting and overturn the poison pill, which set up barriers to anyone like Loeb hoping to build a stake large enough to seize control.

But Sotheby's executives had nevertheless been girding for a tight vote in Tuesday's contest, and moved ahead over the weekend to fashion a compromise.

The 270-year-old auction house, the oldest company listed on the New York Stock Exchange, found itself embroiled late last year in a battle with Loeb, one of Wall Street's most aggressive activist investors.

Loeb built a stake just shy of 10 percent and hit out acidly at a "complacent" board that greenlighted bloated executive pay packages and fumbled opportunities in a fast-changing art world that left it lagging rival Christie's.

He likened the company in an October letter to "an old master painting in desperate need of restoration."

Sotheby's countered that Loeb was proposing changes that "reflect a lack of understanding" about the business.

Sotheby's shareholders reacted positively to the deal, sending the shares up 1.4 percent to $44.06 in early trade Monday.