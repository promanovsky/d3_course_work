Shutterstock photo

Investing.com -

Investing.com - Manufacturing activity in France contracted at the fastest pace in six months in June, underlining concerns over the economic outlook of the euro zone's second-largest economy, preliminary data showed on Monday.

In a report, market research group Markit said that its preliminary French manufacturing purchasing managers' index fell to a seasonally adjusted 47.8 this month from a reading of 49.6 in May. Analysts had expected the index to dip to 49.5 in June.

Meanwhile, the preliminary services purchasing managers' index weakened to a seasonally adjusted 48.2 this month from 49.1 in May and worse than expectations for a reading of 49.4.

A reading above 50.0 on the index indicates industry expansion, below indicates contraction.

Commenting on the report, Paul Smith, Senior Economist at Markit said, "The data are consistent with another disappointing GDP outturn for Q2 following stagnation in the first quarter."

Following the release of the data, the euro turned lower against the U.S. dollar, with EUR/USD shedding 0.03% to trade at 1.3596, compared to 1.3610 ahead of the data.

Meanwhile, European stock markets were modestly lower after the open. The Euro Stoxx 50 dipped 0.2%, France's CAC 40 declined 0.1%, London's FTSE 100 slumped 0.1%, while Germany's DAX fell 0.1%.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.