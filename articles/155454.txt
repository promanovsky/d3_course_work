Sarepta Therapeutics Inc. said Monday it would submit a new drug application for its experimental treatment for a fatal degenerative disease that afflicts children to the U.S. Food and Drug Administration by the end of the year.

Shares surged 69% in premarket trading.

The company, which has no drugs on the market, said it plans to submit the application for the Duchenne muscular dystrophy treatment, called eteplirsen, based on guidance from the FDA.

"We are very pleased with the detailed guidance the FDA has provided us on a potential eteplirsen approval pathway and their support of a historically controlled eteplirsen confirmatory study," Sarepta Chief Executive Chris Garabedian said in a release.

The announcement and guidance come after more than a year of pressure from experts, patient advocates and lawmakers who have pleaded for the FDA's approval of the drug. Earlier this year, a group called Race to Yes collected more than 100,000 signatures for a petition to urge the White House to push for an accelerated approval process.

Duchenne muscular dystrophy, a fatal disease that destroys muscle tissue in children, afflicts about one in every 3,500 boys born across the globe, according to Sarepta. No other genetic disease kills more people, with death occurring as patients enter their late teenage years or early 20s.

Sarepta said eteplirsen is intended to enable the production of a functional dystrophin protein, addressing the underlying cause of the disease.

Clinical studies of the treatment have demonstrated "a broadly favorable safety and tolerability profile and restoration of dystrophin protein expression," the company said. Still, the FDA has sought more data. In November, Sarepta said the FDA said a new drug application for eteplirsen was premature, prolonging the process.

The company said it plans several additional clinical studies later this year, with the intention of starting dosing in the confirmatory study in the third quarter.

Write to Michael Calia at michael.calia@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires