Warren Buffett gave a nice, simple explanation for why Berkshire Hathaway Inc. didn’t buy back any of its own shares in 2013: The stock just wasn’t cheap enough.

Buffett told shareholders in his annual letter that in order to trigger repurchases, the stock would have to trade for 120 percent of book value or less. (A company’s book value refers to its assets minus liabilities, and not the size of the contract Michael Lewis could fetch for writing about it.) Berkshire is trading at 138 percent of book, or a price-to-book ratio of 1.38. The multiple hasn’t dipped below 1.2 since 2012.

Of course, price-to-book valuations don’t come one-size-fits-all. Ratios far above 1.2 for indexes of consumer and technology companies, even in bear markets, show investors are OK with putting premiums on their brand names, patents and other assets that don’t change hands often. And Buffett himself hunts elephants trading far above 1.2 times book, as the takeovers of H.J. Heinz Co. and Burlington Northern Santa Fe LLC show.

Still, with the Standard & Poor’s 500 Index knocking on the door of another record, the 1.2 multiple does offer a fun limbo stick to send the market under as you wait for the closing bell signaling it’s safe to wear white again. (Picture Warren and Charlie Munger in resort wear holding the ends of the limbo stick to participate fully in this exercise.)

Limbo Kings

Though bankers aren’t known for dance skills, in this case they are the Limbo Kings. The KBW Bank Index hasn’t traded above 1.2 times book since around the time of Lehman Brothers Holdings Inc.’s meltdown in 2008, as the world learned how fast bank assets can morph into “troubled assets” and likely won’t forget anytime soon. It’s at 1.09 now, compared with a peak of 3.29 in 1996 and 3.19 near the end of the bull market in 2000. In 2007, it collapsed from 1.99 in February to 1.27 by the end of the year.

There aren’t a lot of other dancers on the floor today coming in under 1.2. About 91 percent of the S&P 500 is trading above the ratio, including 11 companies with negative book values. On March 9, 2009, the best day for bargain hunters of this century, maybe any century, almost half were below.

At the peak of the previous bull market in 2007, about 90 percent of the S&P 500 had a price-to-book above 1.2. Among those below the limbo stick then, note why some of them were there: they were falling on their butts. Just look at Countrywide Financial Corp. (price/book of 0.77) and Ambac Financial Group Inc. (1.2). Now, like then, probably all bargains ain’t bargains.

In the Nasdaq 100 Index, there are two stocks below 1.2 times book: Liberty Media Corp. and Vodafone Group Plc., the same number as the 2007 peak. At the bottom in 2009, you had a choice of 19. The entire Nasdaq Composite’s ratio is currently 3.7, still well below the Internet bubble peak of 8.9.

Even after a 14 percent drop from a February record, you won’t find any members of the Nasdaq Biotech Index below 1.2. But maybe someone’s working on a pill for that.