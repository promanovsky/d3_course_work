Have Sony and director Drew Goddard settled on their Sinister Six? Not exactly.

“The Amazing Spider-Man 2” opens this Friday and while the Green Goblin, Doctor Octopus, Rhino, Vulture, Kraven the Hunter and Mysterio are teased in a post-credits sequence that fans will be able to unlock by Shazaming the closing credits song by Alicia Keys and Kendrick Lamar, those are not necessarily the villains that will be in “Sinister Six,” an individual familiar with the project told TheWrap.

Also read: ‘Amazing Spider-Man 2’ Reviews: Should Audiences Get Caught in Sequel’s Web?

The post-credits stinger includes a virtual schematic created to tease fans and whet their appetites regarding the possibilities that await them in the Spider-Man universe — but those are merely possibilities.

TheWrap has learned that the “Sinister Six” have not been decided on, as Goddard is still finalizing the script. One veteran talent representative told TheWrap that production could begin as early as January 2015.

Also read: ‘Amazing Spider-Man 2’ Swoops to $67 Million at Overseas Box Office

“Sinister Six” will present a fresh point of view of the Spider-Man universe, as it focuses on the web slinger’s most famous villains. The credits of “Amazing Spider-Man 2” feature blueprints of Rhino, the Goblin glider, Doc Ock’s arms, Kraven’s spear and Mysterio’s mask, not Chameleon’s, as some sites have speculated.

It remains unclear whether Andrew Garfield will appear as Spider-Man in “Sinister Six,” or how long a possible appearance may last.