NEW YORK -- It's a tough time for a tech debut.

As e-commerce giant Alibaba gets ready for a blockbuster stock sale in the next few months, technology shares are retreating. For two years, investors bid up biotechnology and internet companies, enticed by their strong growth prospects in an otherwise weak U.S. recovery. But they have sold off those stocks since late February, realizing they can find better value elsewhere. So-called growth stocks like Amazon and Groupon are out of favour. Companies that pay healthy dividends and have a long record of profitability, like utilities, are in.

Amazon has dropped 26 per cent since the beginning of the year; Netflix is off 10 per cent; more risky dotcom companies such as Groupon have plunged more than 50 per cent.

Into this brutal environment comes Alibaba, a company that propelled the rise of online shopping in China and is now preparing an initial public offering that could become the biggest in U.S. history.

It's not an optimal time to pitch tech. Internet companies, whose market values ballooned in 2013 amid high hopes, have pulled back in 2014 as investors reassess their prospects. Twitter, which held its IPO last November, peaked at $74.73 a month later. But the stock down more than half from that high, including a plunge Tuesday after company insiders were allowed to sell stock for the first time since the offering.

Roughly $149 million has been pulled out of science & technology funds the last two months, according to mutual fund data provider Lipper. Over the same period, about $5.2 billion has flowed into value-focused mutual and exchange-traded funds. The lopsided moves show that investors are skittish about tech.

Tech is the highest-profile casualty of a fundamental shift in investor behaviour, market watchers say. Instead of putting money in growth stocks -- companies whose earnings rise at above-average rates -- fund managers now want shares of safer companies. Instead of hunting for stocks whose prices could double this year, investors want so-called value stocks, companies that are undervalued by the market but pay relatively high dividends, sell necessities, and have mature business models.

"They're killing everything high-growth," says Ian Winer, director of trading at Wedbush Securities. "It's the same story, no matter what company you look at: investors want out."

What they want are utilities, energy and health care. As a result, the three are the best performing industries in the S&P 500 this year -- up by 13 per cent, 6 per cent, and 5 per cent, respectively. By comparison, the Standard & Poor's 500 index is up 2 per cent.

Despite the tough conditions for Internet stocks, most analysts expect Alibaba's IPO to raise at least $10 billion for the company. The figure to beat is Facebook, which brought in $16 billion when it went public in May 2012.

"It might be safer for Alibaba to do a more conservative pricing of its IPO in this environment," says Kevin Landis, portfolio manager of Firsthand Funds. He would consider buying Alibaba after its IPO, saying that the company will benefit from the explosive growth of online shopping in China.

The recent skepticism about tech companies is a reversal of the last two years.

Money poured into high-growth tech companies starting in 2011 because they were the only parts of the economy that seemed to be expanding, market strategists say. Netflix, Tesla Motors and Amazon.com were posting double-digit sales growth while non-technology companies were eking out profits by cutting costs.

"Many flocked to a few areas -- including social media, cloud computing and biotechnology -- of the market where high growth seems well assured," says Ed Cowart, portfolio co-manager at Eagle Asset Management. "(But) concentrated and aggressive interest drove those stocks to dizzying heights."

Technology stocks grew expensive, at least by one measure called the price-earnings ratio, which investors use in deciding whether to buy or sell a stock. To calculate a P/E, you divide the price of a stock by its annual earnings per share.

The P/E for the Nasdaq composite, which is loaded up with tech stocks, hit a four-year high of 24-to-1 on Feb. 28. That meant investors were willing to pay $24 for every dollar of earnings the companies in the index generated. While tech stocks usually are more expensive than the rest of the market because of their high growth potential, a P/E of 24-to-1 was well above the index's 10-year average of 19-to-1.

Some tech stocks remain wildly expensive, even after a recent sell-off. Amazon.com, for example, trades at around 470 times its earnings per share for last year.

Besides high prices, another trend has drawn investors' away from Internet stocks. Non-technology companies are starting to see modest increases in sales and profits. Evidence is also emerging that overall U.S. growth is picking up after a tough winter.

"If the economy is getting better, big traditional large-cap stocks are going to look pretty cheap," says Bob Doll, chief equity strategist at Nuveen Asset Management.

The Russell 1000 index, a value-oriented index made up of mostly large companies, trades at 14.5 times earnings, versus the S&P 500's 16.5.

Greenlight Capital's David Einhorn, the hedge fund manager who correctly called the collapse of Lehman Brothers, says technology stocks are in a bubble that is an "echo of the previous tech bubble" in 2000.

He thinks that these "bubble stocks" will fall further, although he did not disclose which companies they are. "While we aren't predicting a complete repeat of the (dotcom) collapse, history illustrates that there is enough potential downside in these names to justify the risk of shorting them," or betting that their stock prices will fall.

Even if technology stocks are in a hypothetical bubble, it's a tiny one compared with 2000. When the bubble was at its biggest, stocks in the Nasdaq composite traded at an average of 194 times their earnings, compared with the current 20. That level is a more a normal valuation, based on historic averages.

Still, fund managers warn that investors should expect more tech stock declines, even with the recent sell-off and the early enthusiasm for Alibaba.

"I don't think we are quite finished selling these yet," Nuveen's Doll says. "And when we do finish this rotation, it's going to take a long time to repair the damage."