Research has uncovered yet another gene link regarding Alzheimer's disease and it's protein that was actually discovered less than 10 years ago.

Most Alzheimer gene investigation has focused on two proteins, beta amyloid and tau, which are largely found in Alzheimer patient brains. Research is still ongoing to devise a medication to eliminate and control those genes. Amyloid has long been viewed as the plaque defining the disease. The latest protein now under review is TDP-43, and appears to be abnormal in form in those likely to develop memory loss

"This injects new vigor in the fight against Alzheimer's disease," said Keith Josephs, the lead author and a neurologist at the Mayo Clinic in Rochester, Minn. "The world has focused on two proteins, beta amyloid and tau. TDP-43 is going to be the new kid on the block."

Biogen, a Massachusetts research company, is running preclinical tests to learn more about TDP-43 and its relationship in neurodegenerative disease. Boston University's Alzheimer's Disease Center is aiming to develop compounds to target and control the protein and its leaders believe TDP-43 may be a "major player" in cognitive loss.

TDP-43 lives in the nucleus of the cell and transcribes DNA and is active during the production and programming of other proteins that have specific roles in the cell, say researchers. But when it's not in the nucleus, it is considered an abnormal form and it can't do what it needs to do, say researchers.

The study involved 342 brains that had been donated to science. The abnormal form of TDP-43 was found in 57 percent of the group. The brains with the abnormal TDP-43 showed more memory loss and severe shrinkage of the hippocampus. In addition, 98 percent of the 57 percent likely were cognitively impacted before death.

The decades of Alzheimer research has focused on amyloid with the intent to prevent the protein from accumulating and even removing it once it is discovered in a brain. Investigating TDP-43 will be much harder, says researchers, as the quest will be looking for the trigger that damages the protein which is essential for normal cell function.

For now, "we really need to understand what this protein is doing and its relationship to other proteins," said Josephs.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.