Reynolds American Inc. said Tuesday that its profit rose by 6.7 percent in its second quarter as higher cigarette and smokeless tobacco prices helped to offset a decline in the number of cigarettes it sold.



The Winston Salem, North Carolina-based company said it earned $492 million, or 92 cents per share, in the period ended June 30, up from $461 million, or 84 cents per share, a year ago.



Adjusted earnings were 89 cents per share, topping analyst estimates of 87 cents per share, according to Zacks Investment Research.



The nation's second-biggest tobacco company reported revenue, excluding excise taxes, of $2.16 billion, down from $2.18 billion a year ago. Wall Street predicted $2.22 billion in revenue.



Its shares rose 28 cents to $56.95 in early trading Tuesday.



Earlier this month, Reynolds announced plans to buy Newport cigarette maker Lorillard Inc. for $25 billion. The tie-up would create a formidable No. 2 tobacco company in the U.S. behind Richmond, Virginia-based Altria Group Inc. It also creates a powerhouse in menthol cigarettes, which are becoming a bigger part of the business and gives the combined company some breathing room even as people smoke fewer cigarettes every year.



The maker of Camel and Pall Mall cigarettes said its R.J. Reynolds Tobacco subsidiary shipped 8 percent less cigarettes during the quarter, compared with an estimated decline of 5.5 percent for the industry as a whole.



Volumes for Camel fell more than 4 percent and volumes for Pall Mall decreased 6 percent. The brands account for more than 60 percent of the company's total cigarette volume.



But Camel's market share increased 0.4 percentage points to 10.2 percent of the U.S. market, while Pall Mall's market share grew 0.1 percentage points to 9.3 percent.



The number of Natural American Spirit cigarettes sold by its Santa Fe Natural Tobacco subsidiary grew nearly 8 percent.



Tobacco companies are also focusing on cigarette alternatives such as snuff, chewing tobacco and electronic cigarettes as tax hikes, smoking bans, health concerns and social stigma make the cigarette business tougher.



Shipments of its Grizzly and Kodiak smokeless tobacco brands fell less than a percent. The brands had a 34.4 percent share of the U.S. retail market, though that market is tiny compared with cigarettes.



The company said the national rollout of its Vuse brand electronic cigarette is progressing and is in about 21,000 stores. The second wave of the brand's expansion is set for early September. Reynolds noted it is spending more to expand and promote the brand, which will be its sole e-cigarette once it sells Lorillard's Blu e-cig brand to U.K.'s Imperial Tobacco as part of the deal.



Reynolds American also narrowed its 2014 adjusted earnings forecast to a range of $3.35 to $3.45 per share. Its previous outlook was $3.30 to $3.45 per share. Analysts polled by FactSet expect $3.35 per share.



© Copyright 2021 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.