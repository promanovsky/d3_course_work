A clinical trial of drug manufacturer Pfizer's palbociclib showed that the medication delayed breast cancer progression by nearly two years.

The trial was conducted as part of Phase 2 of a study on 165 women with breast cancer. Researchers found that the women who were treated with the drug plus the hormone drug letrozole lived for approximately 20.2 months before their cancer got worse compared to 10.2 months for patients given letrozole alone.

Pfizer noted that palbociclib was one of its most important drugs. Some analysts also claim that once the drug is approved by regulators, it will most likely claim annual sales of more than $5 billion, according to a Reuters report.

The pill is meant for post-menopausal women with locally advanced or newly diagnosed breast cancer that has spread to other parts of the body. All women that took part in the clinical trial had cancer that was estrogen receptor positive and HER2-negative. Statistics show that 60 percent of advanced breast cancer cases are estrogen receptor positive and HER2-negative

Letrozole is an estrogen-blocker drug that is sold by Novartis AG under the brand name Femara. Those treated with this drug alone have an overall survival of 33.3 months. Researchers found that when this drug was given in combination with palbociclib, overall survival increased to 37.5 months.

The U.S. Food and Drug Administration has granted the drug a "breakthrough" status. Manufacturers of palbociclib are yet to decide whether they should seek accelerated approval based on Phase 2 trial results, according to a Forbes report. However, researchers said that even though only 30 patients in each arm of the 165-patient trial had died, it was still too early to define the drug's impact on survival.

"The curves are starting to separate ... It hasn't reached statistical significance, but patients are still being followed," said Dr. Richard Finn, associate professor of medicine at the University of California, Los Angeles, and a lead author of the study.

Today, breast cancer is the second most deadly cancer among American women, affecting over 200,000 and killing 40,000 each year. It is estimated that one in every eight women will develop the disease at some point in their life. While the cause remains unknown, risk factors include early puberty, late menopause, and certain genes. Lifestyle factors like calorie intake and alcohol consumption have also been implicated in higher risk for diagnosis.

Though hormone treatments like letrazole have been shown to extend survival among patients, no real breakthrough therapy has appeared in the past 20 years, said Dr. Judy Garber, a breast cancer specialist who was not involved in the current study.

"This is garden variety breast cancer," Dr. Garber said, according to American Live Wire. "When it recurs, all we really have are other hormonal agents ... This is the first new drug to really show promise."

Side effects of palbociclib include low blood cell counts and fatigue. However, officials confirmed that these occurrences were manageable. The findings of the study will be presented in San Diego at a meeting of the American Association for Cancer Research.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.