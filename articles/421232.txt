Celebrity

Hayden, who was arrested in Louisiana for allegedly molesting a girl two weeks ago, is charged with aggravated rape of a 12-year-old child.

Aug 27, 2014

AceShowbiz - Will Hayden, the founder of Red Jacket Firearms who appeared on Discovery Channel's "Sons of Guns", has been arrested again. According to WAFB, the reality star was arrested on the charge of aggravated rape of a 12-year-old child on Tuesday, August 26. After an arrest warrant for Hayden was issued, the U.S. Marshal's Office nabbed him in Livingston, LA.

Earlier this month, Hayden was accused of molesting his daughter. He was released after posting a $150,000 bond. Back then, Hayden claimed that the accusations were false.

Info regarding the alleged rapes came out when the police were investigating the first case. According to the affidavit from the new arrest, the alleged victim told the authorities that she was raped almost daily between March 2013 to July 2014 when she was 11 and later turned 12. She did not report it because Hayden threatened her. "Don't tell them nothing, because I'm all you got," Hayden allegedly said.

Following the legal trouble, Red Jacket Firearms released a statement on Facebook. "Today, Red Jacket Firearms has received complete legal separation as an entity, from Will Hayden. We will continue to operate, although with heavy hearts and promise to do everything in our power to fill customer orders, back orders and provide support to those affected by these new developments," the statement read.

Meanwhile, a spokesperson for Discovery Channel, Laurie Goldberg, said that she was aware of the news, but refused to comment on the allegations.