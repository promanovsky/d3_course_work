Colombian novelist Gabriel Garcia Marquez left behind an unpublished manuscript that he chose not to print while he was alive, an editor told The Associated Press on Tuesday.

Cristobal Pera, editorial director of Penguin Random House Mexico, said that Garcia Marquez's family has not yet decided if the book should be released posthumously, or which publishing house would get the rights. Garcia Marquez died at his Mexico City home on April 17 at 87.

The manuscript has a working title of "We'll See Each Other in August," ("En Agosto Nos Vemos").

An excerpt of the manuscript published in Spain's La Vanguardia newspaper contains what appears to be an opening chapter, describing a trip taken by a 50-ish married woman who visits her mother's grave on a tropical island every year. In the chapter, she has an affair with a man of about the same age at the hotel where she stays.

The manuscript apparently dates to about the time the Colombian author was penning his last novel, "Memories of My Melancholy Whores," which was published in 2004, and deals with similar themes of forms of love. Garcia Marquez, beset by a failing memory, apparently did not write much in recent years.

Garcia Marquez biographer Gerald Martin said the manuscript apparently started as a short story.

"This has come as a surprise to me. The last time I talked to Gabo [Garcia-Marquez's nickname throughout Latin America] about this story it was a stand-alone which he was going to include in a book with three similar but independent stories," Martin said.

"Now they're talking about a series of episodes in which the woman turns up and has a different adventure each year," he wrote in an email. "Obviously it makes sense and presumably Gabo really did play with it, presumably some years ago."

Around Colombia, a marathon public reading of Garcia Marquez's "No one writes to the Colonel" is planned Wednesday at 1,400 public libraries. The Culture Ministry has distributed 12,000 copies of the book for the occasion.

In Cuba, the state news agency Prensa Latina announced that this year's Havana Film Festival will be dedicated to the late Nobel Prize-winning author.

Garcia Marquez was a longtime friend of former leader Fidel Castro and also a major backer of Cuba's marquee international cinema bash, held every December. Festival official Giroud says Garcia Marquez's widow and children have been told of the decision to honor him.