Variety writes that Toby Kebbell is in talks to play FANTASTIC FOUR villain Victor von Doom, aka Doctor Doom, in the upcoming reboot, directed by Josh Tank for Fox.

Read the original report here.

Kebbell would join Miles Teller (Reid Richards/Mr. Fantastic), Kate Mara (Sue Storm/The Invisible Woman), Jamie Bell (Ben Grimm/The Thing) and Michael B. Jordan (Johnny Storm/The Human Torch) in the latest installment in the comicbook franchise.

Based on the comic "The Ultimate Fantastic Four," the contemporary update will focus on the characters as young men and women. Josh Trank will helm the pic with Simon Kinberg, Matthew Vaughn and Gregory Goodman serving as producers.

The original Fantastic Four movie and its sequel The Fantastic Four: Rise of the Silver Surfer took a collective $619 million at the box office when released.

"The Fantastic Four" is slated to bow June 19, 2015.

Kebbell's other upcoming projects include Dawn of the Planet of the Apes, Buddha's Little Finger and Warcraft. Past credits include The Counselor, The East, Wrath of the Titans, War Horse, The Veteran, The Conspirator, The Sorcerer's Apprentice, Prince of Persia: Sands of Time and more.

Image source.