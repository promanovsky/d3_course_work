Washington: Google has reportedly rolled out a modified Android OS version for its new range of wearables.

The search giant has partnered with HTC, LG, Samsung, and others for the first phase of its Android Wear smartwatches.

According to Cnet, the modified Android OS would be heavily based on Google Now voice-recognition technology and the voice command, 'Ok Google' to ask questions or fire off a text message.

Broadcom, Imagination, Intel, MediaTek, and Qualcomm are poised to become the search giant's chip partners.

The modified Android OS would focus on health and fitness tracking and has been designed to provide relevant information, as well as notifications from social apps, alerts from messaging apps, and notifications from shopping, news, and photography apps.

The report said that the Fossil Group would bring Android Wear-powered watches later this year.