Gordon Willis, the groundbreaking cinematographer who shot the “Godfather” movies for Francis Ford Coppola and “Annie Hall” and “Manhattan” for Woody Allen, has died, American Society of Cinematographers President Richard Crudo confirmed Monday. He was 82.

No other details were immediately available.

Nicknamed the “Prince of Darkness” by fellow cinematographer Conrad Hall, Willis was known for his daring use of shadow and striking chiaroscuro.

His work in the 1970s with such directors as Coppola, Allen and Alan J. Pakula -- for whom he shot “Klute,” “The Parallax View” and “All the President’s Men” -- helped define the aesthetics of an era.

Advertisement

Willis’ other credits include “Pennies from Heaven,” “Zelig,” “The Purple Rose of Cairo,” “Malice” and “The Devil’s Own.”

He was nominated for the Oscar for best cinematography twice: for 1983’s “Zelig” and 1990’s “The Godfather: Part III.” He was awarded an honorary Oscar in 2009.