Hey hey, you you, we’ve seen this all before, Avril. The other Ontario-born pop star that Canadians love to hate has released a new music video, and it’s getting lots of attention for all the wrong reasons.

The video for “Hello Kitty,” the latest single off her self-titled album, which was released last year, hit YouTube this week and one fan’s comment pretty wells sums up the whole situation: “It’s like watching a trainwreck, I just can’t look away.”

While the thumping dance beat is a departure from Lavigne’s usual pop-punk princess sound, she appears to be trapped in her teen years when “Sk8erboi” was a hit. In “Hello Kitty,” she’s singing about sleepovers and playing spin the bottle. But the song is also an homage to Japan and Hello Kitty, two things fans insist the 29-year-old is obsessed with.

Sample lyric: “Come, come Kitty, Kitty, you’re so pretty, pretty.”

Poetry.

While Lavigne may have a fondness for Japan, the video seems a poor imitation of Gwen Stefani’s “What You Waiting For” and “Hollaback Girl,” two tracks off her debut solo album that also heavily referenced Japanese culture.

Both singers include silent and expressionless Asian women as props in these videos and seem to have no problem sending up cultural stereotypes.

Lavigne shot the video in Japan last month, and tweeted her excitement:

I LOVE JAPAN !!!! This music video shoot is so fun! Hello Kitty!!! — Avril Lavigne (@AvrilLavigne) March 18, 2014

The video has been pulled from the singer's official YouTube channel, but remains on her website.

While she doesn’t seem to mean any harm, the video doesn’t seem to be much of a tribute to a country or its culture, either.

Judge for yourself below.