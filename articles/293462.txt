Researchers are conducting the first-ever human trial of a pro-vitamin A-enriched banana.

"Human trial is a significant milestone for this project which started in 2005 and should see pro-vitamin A-enriched banana varieties being grown by Ugandan farmers around 2020," Professor James Dale, said in a Queensland University of Technology news release.

The East African cooking banana is a staple food of many nations in the region, but has very low levels of micronutrients such as iron and vitamin A.

"The consequences of vitamin A deficiency are dire with 650,000 [to] 700,000 children world-wide dying from pro-vitamin A deficiency each year and at least another 300,000 going blind," Dale said.

Vitamin A deficiencies can lead to serious health issues such as immune system impairment and problems with brain development.

"Good science can make a massive difference here by enriching staple crops such as Ugandan bananas with pro-vitamin A and providing poor and subsistence-farming populations with nutritionally rewarding food," Dale said.

The human trial will be six weeks long; the enriched banana has shown success in past studies with Mongolian gerbils.

"The banana flesh of a pro-vitamin A-enriched banana is orange rather than the cream colour we are used to and in fact the greater the pro-vitamin A content the more orange the banana flesh becomes," Dale said.

Legislation to allow genetically modified crops to be commercialized is currently in the committee phase within the Ugandan parliament; regulations that allow for commercialization of genetically modified crops could be in place by 2020.

Once approved in Uganda these crops could most likely be distributed to surrounding East African countries such as "Rwanda, parts of the Democratic Republic of Congo, Kenya and Tanzania," the news release reported.

"In West Africa farmers grow plantain bananas and the same technology could easily be transferred to that variety as well," Dale said. "This project has the potential to have a huge positive impact on staple food products across much of Africa and in so doing lift the health and wellbeing of countless millions of people over generations."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.