Jon Favreau has gone back to his comedy roots with new movie ‘Chef’ which premiered at SXSW this weekend. Mixing the world’s growing obsession with food and rockstar chefs with an all-star cast, Favreau has already won rave reviews for the film and his return to comedy. So what can we expect from a movie that features food trucks and Scarlett Johansson?

Jon Favreau in 'Chef'

Jon Favreau has gone from comedy actor and writer, to being the director of one of the biggest comic book franchise films of all time, ‘Iron Man’, to walking away from it to direct a movie about a food truck chef. Reckless career move? You might have though so, but Favreau’s latest offering has earned him the favour of critics after its SXSW premiere.

‘Chef’ tells the story of Carl Casper, a chef who, after having his fill of Los Angeles, moves to Miami with his ex wife to enter the food truck business. Written and directed by Favreau, the premise might seem a million miles away from his previous ‘Iron Man’ success, but the two films are not that far removed from each other. Starring alongside Favreau in ‘Chef’ are ‘Iron Man’ cast members Robert Downey Jnr and Scarlett Johansson and then there’s also Sofía Vergara as Casper's ex wife, Dustin Hoffman, and John Leguizamo, added to the cast, making this one of 2014’s most mouth watering ensembles so far.

Jon Favreau with Sofia Vegara in 'Chef'

The idea from the story comes from our new obsession not just with food itself, but with the people that make it. Pictures of food are all over social media, so much so it's even being dubbed ‘food porn’. Then there’s the chefs on television, whose often over the top personalities have made them the new rockstars, albeit in less cool clothing. Like the rest of us Favreau has gained a new found obsession with food so much so it led into his work. It's said to have taken the film maker just ten days to write the script about a chef moving into the food truck business.

More: Everything you need to know about Jon Favreau's The Jungle Book.

For those not that familiar with food trucks, they've become very ‘hip’ in the past few years. This is in part thanks to chef Roy Choi whose ‘Kogi BBQ’ Korean Taco trucks are credited with starting the craze of taking food out of the restaurants and onto the road. In the United States there are said to be over 3 million food trucks, with the biggest cities for on-the-go food being Los Angeles, New York and Austin, Texas. So it's no surprise that this movie chose SXSW for its debut voyage. Food trucks offer a variety of different delicacies, there are some which are more traditional serving tacos, hotdogs even plain old coffee and doughnuts, then others get more elaborate with everything from curries to seafood to purely vegetarian offerings.

Behind the scenes of Jon Favreau's 'Chef'

As for the film, ‘Chef’ sees Favreau return to the fast paced style that worked so well in ‘Swingers’. The movie has Favreau going back to making smart and funny films geared towards a more adult audience. However alongside the laughs expect this to be a character study of a man at a certain time in his life when family and career problems collide. Also expect many food porn shots and social media references as this is very much an 'of the now' feature. A return to indie film making also marks a refreshing change of pace for Favreau something which he clearly seems to be relishing in for this more simplistic offering. He probably wont have any time to sit back an enjoy it however, his next project is a live action remake of Rudyard Kipling’s ‘The Jungle Book’, due for release in 2015.

‘Chef’ will be released in US theatres on May 9th. Until then you can take a look at a clip from the film here:

