The most revealing image of Robin Thicke at Sunday’s BET Awards wasn’t the pained expression he wore while singing “Forever Love,” a forlorn piano ballad the R&B star introduced as an apology to his estranged wife, Paula Patton.

It was instead a reaction shot that showed Thicke sheepishly laughing at a joke by the show’s host, Chris Rock, about Donald Sterling’s never having met a black man until he received one for Christmas. The most visible white person in the room, Thicke at that moment was being called upon to demonstrate some chagrin about sharing a skin color with the L.A. Clippers co-owner, who was recently caught making controversial comments. And the singer pulled it off.

The scene suggested Thicke knows what it is to be embarrassed, something you’d never glean from listening to his new album. Titled “Paula,” the disc culminates Thicke’s very public campaign to reconcile with Patton after the couple separated in February – the result, according to the tabloids, of such un-husbandly acts as grabbing a female fan’s behind and grinding against Miley Cyrus at last year’s MTV Video Music Awards.

At the time of the latter, Thicke was riding high with “Blurred Lines,” a smash-hit single that oozed an alpha-dude bravado – some said the song condoned rape – that he’d never previously displayed. (The son of “Growing Pains” star Alan Thicke, the singer released his debut album in 2003, then spent the next decade as a proudly wimpy cult favorite.)

Advertisement

Here, though, that swagger has evaporated so completely that he’s often begging Patton (literally so in “Still Madly Crazy”) to take him back. “Don’t leave me out here in the cold,” he pleads in “Lock the Door,” before adding with breathtaking disregard for his dignity, “At least open the doggy door / Throw a friend a juicy bone.” If only Chris Rock had heard these lyrics before his BET gig.

Then again, perhaps Thicke’s bravado hasn’t fully evaporated. There’s a creepy grandstanding quality to much of the groveling on “Paula,” as though he were convinced that the scale of his gesture, rather than its sincerity, is what matters. (It’s anyone’s guess where a child of show business picked up that idea.)

“I never should’ve raised my voice or made you feel so small,” he sings in “Get Her Back,” which seems heartfelt enough. But then, evidently unsatisfied, he supersizes his regret to absurd dimensions: “I never should’ve asked you to do anything at all.” “Black Tar Cloud” is even splashier, a greasy funk-rock jam in which the singer, after driving his lady to chase him around the house with his favorite golf club, finds himself “face down in a puddle of shame.”

There’s also the handful of songs in which Thicke goes weirdly off-topic, including “Living in New York City,” about how psyched he is to walk his bulldog through Central Park; the chintzy Rat Pack homage “Time of Your Life” and “Something Bad,” which he may as well have called “More Blurred Lines.”

Advertisement

“Take a leap of faith and, baby, land in my bed,” he advises over thumping drums, “But don’t you be surprised if I end up messing up your head.”

Speaking of which, here’s Thicke in the surprisingly restrained “The Opposite of Me”: “All that she needs is something that I can’t give her.” And here he is two songs later, in “Forever Love”: “You can lean on me anytime, baby, for anything you want and need.”

What’s a listener – let alone a wife – to make of these mixed messages?

That’s not to say that incoherence is necessarily a problem for a pop star. Indeed, the valorization of clarity is how we end up with songs as dull as John Legend’s “All of Me.” Yet Thicke’s slipperiness on “Paula” raises the question of what exactly this album is supposed to do.

Advertisement

If his goal was to convince Patton that he’s changed for the better, the stalker-ish “You’re My Fantasy” makes that outcome unlikely. And with its cheap-sounding production and tiresome self-flagellation, “Paula” seems equally ill-suited to extending the mainstream success of “Blurred Lines.”

By those metrics (and others), the record is a failure, a virtual what-not-to-do guide for both songwriters and spurned lovers. But in an age when appearance rules, there’s something kind of fascinating about Thicke’s willingness to look this bad.

-----------------------

Robin Thicke

Advertisement

“Paula”

(Star Trak/Interscope)

1 1/2 stars out of 4

Twitter: @mikaelwood