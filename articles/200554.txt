Timothy Geithner says in a new memoir that he considered stepping down as Treasury secretary in 2010 after the financial crisis and suggested Hillary Rodham Clinton as a possible successor.

President Barack Obama rebuffed Geithner's suggestion, and he remained at Treasury until 2013.

Geithner's memoir will be published next week. On Thursday, The Associated Press bought an early copy.

Geithner writes of the incident in "Stress Test," which explores his turbulent four years at Treasury. During his tenure, the Obama administration faced the worst recession and most severe financial crisis since the Great Depression.

In proposing that the White House consider Clinton as his successor, Geithner cited her star power as secretary of state. Among the other names Geithner suggested was Jack Lew, who succeeded him last year.

Urgent:

Assess Your Heart Attack Risk in Minutes. Click Here.