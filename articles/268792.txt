Facebook has agreed to acquire Pryte, a mobile data plan firm based in Finland, to help with its Internet.org effort. Terms of the deal were not immediately disclosed.

Pryte was founded in 2013 with the goal of working with wireless operators to offer mobile data plans to users on a per app basis rather than forcing people to buy it in bulk. Reuters, which was first to report the deal, notes that Pryte hadn't launched publicly yet and there are fewer than 30 employees on staff.

In a blog post, Pryte's team wrote that it will be working with Facebook to help with its Internet.org project to bring the entire world online by making Internet access more affordable and available to everyone.

"Since we launched Pryte we have worked to reimagine the way mobile data works in an app-driven world, by enabling partnerships between app and content providers, and mobile operators," Pryte's team wrote in the post. "Now, we’re joining Facebook, whose mission to connect the world by partnering with operators to bring people online in a profitable way aligns closely with our team’s goals."

A Facebook spokesperson confirmed the acquisition to Mashable, noting that Pryte's team will be part of the Internet.org effort and work out of Facebook's offices in London and Singapore.

"The Pryte team will be an exciting addition to Facebook," the spokesperson said in an emailed statement. "Their deep industry experience working with mobile operators aligns closely with the initiatives we pursue with Internet.org, to partner with operators to bring affordable internet access to the next 5 billion people, in a profitable way.”