Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Gwyneth Paltrow and Chris Martin sang a romantic duet for a fund-raiser – only three weeks before announcing their marriage was over, reports the Sunday People.

The smiling couple showed no sign of their impending split as they crooned Cruisin’ – a hit Gwyneth previously belted out with co-star Huey Lewis in the 2000 film Duets.

Coldplay star Chris, 37, and actress Gwyneth, 41 – parents to daughter Apple, nine, and son Moses, seven – sang loving lyrics including: “Baby, tonight belongs to us, ­everything’s right, do what you must.”

Their show of unity was at the John Thomas Dye School in Los Angeles just days before the news that their 10-year ­marriage was over appeared on Gwyneth’s website Goop on Tuesday.

It shows how close they still are . Despite rumours their split was fuelled by each striking up affectionate friendships with others, they remain friends.

Hollywood star Cameron Diaz said: “They’re kind, generous, loving people, who really care about one another.

"Just because they don’t want to be a couple together, doesn’t mean that they have to hate one another.

"They are really good friends.”

Julia Ruiz, who has been close to Gwyneth since caring for her as a 15-year-old on an exchange trip to Spain, said: “She continues to be a marvellous person.

"I’m sure they will be focusing on their children to make sure they don’t suffer.”