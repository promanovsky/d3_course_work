While Nigerian officials work to prevent what could be a potential outbreak of the Ebola virus in Africa's most populous country, some aid workers fear national and international health agencies are not prepared for the worst.

Yet with only one reported case, which may have been contained, some Nigerians object to the panic, saying it’s bad press for Nigeria’s financial capital and largest city, Lagos.

"My only worry has been the media is blowing this out of proportion,” says Bolaje Alonje, a 30-something artist and activist in Lagos. “There's a kind of romance with Nigeria and [with] bad news. Are we making Nigeria…from a Boko Haram country to a[n] Ebola country?"

Until last week, Boko Haram insurgents dominated international news about Nigeria. But when Patrick Sawyer, a civil servant from Liberia, died of Ebola in a Lagos hospital last Friday, officials and health experts were immediately alarmed, fearing Ebola may have spread into the crowded, chaotic urban zone of 21 million. The Ebola outbreak in West Africa has caused alarm, with more than 1,200 cases in four countries, resulting in at least 670 deaths.

“Nigeria is not like any other West African country,” said Dr. Unni Krishnan, Plan International’s head of disaster response and preparedness, referring to its population density. “The government needs to act quickly.”

The Nigerian government is currently working to identify every person that came into contact with Mr. Sawyer as he traveled to from Liberia to Nigeria en route to Minnesota. His plane stopped in Ghana and again in Togo, where he debarked and went on to Lagos, according to Lagos State Heath Commissioner Jide Idris.

So far, 59 people, including Nigeria’s ambassador to Liberia, have been identified as having contact with Sawyer and none have shown signs of Ebola, raising hopes that the disease may not have spread.

But finding all those other people, some potentially in Ghana or Togo, is no small task, Mr. Idris told reporters Monday.

“The airline manifest has not been provided by the airline at the time of this report,” Idris says. “Therefore the precise number of passenger contacts has yet to be ascertained.”

Ebola can take from two days to three weeks to present symptoms, so potential victims are being monitored carefully, he said.

But Ebola is not transmitted casually, according to the World Health Organization. The virus is contracted via infected animals and transmitted through bodily fluids, like blood, vomit, or sweat. It can also be transmitted through objects, like contaminated linens or needles.

The disease is feared since in some outbreaks up to 90 percent of those infected have died. But the death rate in the recent outbreak in West Africa is just over 60 percent. Nearly 700 people have died since an outbreak began in February, spreading from Guinea to Sierra Leone and Liberia.

There is no known cure for Ebola, but doctors say that with early detection they can sometimes mitigate the worst.

Health workers also can offer some badly-needed mental support while patients fight the disease, according to Dr. Lance Plyler, who heads the Ebola medical efforts in Liberia for aid group Samaritan's Purse. “They are quarantined so they are susceptible to depression,” he says.

The Nigerian government says it is currently screening people as they enter the country, raising awareness about disease prevention and preparing an isolation unit and an "emergency command center” in case of an outbreak.

Efforts to screen people as they cross borders can be useful, but pose challenges, according to Yan St. Pierre, the CEO of Berlin-based security firm MOSECON. For instance, at its onset, Ebola looks a lot like more common diseases.

“Focusing on airports,” he added, “is also a poor solution because most people travel by bus or other means.”

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Many Nigerians are nervous because the nation’s doctors have been on strike for nearly a month, with some in the medical establishment saying the Nigerian healthcare system is in a state of near-collapse.

However, much of the present fear is less about an Ebola outbreak than it is of cholera, which infected 200 people last week and killed five – or of malaria, which kills hundreds of thousands of Nigerians a year.