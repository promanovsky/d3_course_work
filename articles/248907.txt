Bryan Alexander

USA TODAY

CANNES, France — The Turkish drama Winter Sleep took the Palme d'Or, the highest honor at the Cannes Film Festival at its closing ceremony on Saturday night.

Director Jane Campion, representing a nine-member jury, presented the award to Turkish director Nuri Bilge Ceylan for the three-hour, 16-minute drama.

Ceylan dedicated the award to "the young people in Turkey and those who lost their lives in the last year," referring to a coal mine disaster in May in Turkey that killed 301 workers.

The sheer length of the film was initially intimidating, Campion said at a press conference after the awards program.

"I was a little afraid of the film. I was like, 'I am going to need a toilet break,' " Campion said. "But the film had such a beautiful rhythm and took me in. I could have stayed there for a couple of more hours. It was masterful."

The jury named Julianne Moore best actress for her role in David Cronenberg'sMaps to the Stars. British actor Timothy Spall was presented with best actor for his role in Mike Leigh'sMr. Turner, a biopic looking at the life of famed British landscape artist J.M.W. Turner.

Bennett Miller took best director for Foxcatcher. The film follows the downward spiral of chemical heir John du Pont and stars Steve Carell, Channing Tatum and Mark Ruffalo. Miller dedicated the award to his actors for their performances from the stage. "It's really something to have people who put their faith in you like that," he said.

The Jury Prize was dramatically split between the youngest and oldest directors in the 18-film competition. Canadian Xavier Dolan, 25, and his film, Mommy, shared the award with French directing legend Jean-Luc Godard, 83, and his film, Goodbye to Language.

Campion called Mommy "brilliant" and said the jury was aware of the signal they were sending by putting the young director with the director of the 1960 groundbreaking film Breathless.

"We know we owe our lifeblood to Godard,'' said Campion. "Breathless changed cinema. It was wonderful we had this opportunity to give him a prize."

In a year where women's roles (or lack thereof) in the festival have been under a microscope, the five-woman, four-man jury gave Italian director Alice Rohrwacher the Grand Prix prize for The Wonders.

Jury member and director Nicolas Winding Refn called it an "incredible and spiritual film."

"I cried at the end," said Refn. "That (movie) said something I couldn't understand. It was a combination of everything. I was taken into another world."

Campion, the only female director to win the Palme d'Or (for 1993's The Piano), said the gender of the filmmaker "never entered our discussion. Not once."

The Russian film Leviafan was granted the best screenplay for Andrey Zvyagintsev and Oleg Negin.