?The problem is insidious,? Baumgartner said. ?Now it is amateur hour. Everybody is doing it.?

Security experts warn there is little internet users can do to protect themselves from the recently uncovered Heartbleed bug that exposes data to hackers, at least not until vulnerable websites upgrade their software.

Researchers have observed sophisticated hacking groups conducting automated scans of the internet in search of web servers running a widely used web encryption program known as OpenSSL that makes them vulnerable to the theft of data, including passwords, confidential communications and credit card numbers.

OpenSSL is used on about two-thirds of all web servers, but the issue has gone undetected for about two years.

Kurt Baumgartner, a researcher with security software maker Kaspersky Lab, said his firm uncovered evidence on Monday that a few hacking groups believed to be involved in state-sponsored cyber espionage were running such scans shortly after news of the bug first surfaced the same day.

By Tuesday, Kaspersky had identified such scans coming from tens of actors, and the number increased on Wednesday after security software company Rapid7 released a free tool for conducting such scans.

The problem is insidious, Baumgartner said. Now it is amateur hour. Everybody is doing it.

OpenSSL software is used on servers that host websites but not PCs or mobile devices, so even though the bug exposes passwords and other data entered on those devices to hackers, it must be fixed by website operators.

There is nothing users can do to fix their computers, said Mikko Hypponen, chief research officer with security software maker F-Secure.

Representatives for Facebook, Google and Yahoo told Reuters they have taken steps to mitigate the impact on users.

Google spokeswoman Dorothy Chou told Reuters: We fixed this bug early and Google users do not need to change their passwords.

Ty Rogers, a spokesman for Amazon.com, said Amazon.com is not affected.

Kaspersky Lab's Baumgartner noted that devices besides servers could be at risk because they run software programs with vulnerable OpenSSL code built into them.

They include versions of Cisco Systems's AnyConnect for iOS and Desktop Collaboration, Tor, OpenVPN and Viscosity from Spark Labs. The developers of those programs have either updated their software or published directions for users on how to mitigate potential attacks.

Steve Marquess, president of the OpenSSL Software Foundation, said he could not identify other computer programs that used OpenSSL code that might make devices vulnerable to attack.

Bruce Schneier, a well-known cryptologist and chief technology officer of Co3 Systems, called on internet companies to issue new certificates and keys for encrypting internet traffic, which would render stolen keys useless.

That will be time-consuming, said Barrett Lyon, chief technology officer of cybersecurity firm Defense.Net. There's going to be lots of chaotic mess, he said.

Symantec and GoDaddy, two major providers of SSL technology, said they do not charge for reissuing keys.

Mark Maxey, a director with cybersecurity firm Accuvant, said it is no easy task for large organisations to implement the multiple steps to clean up the bug, which means it will take some a long time to do so.

Due to the complexity and difficulty in upgrading many of the affected systems, this vulnerability will be on the radar for attackers for years to come, he said.

Hypponen of F-Secure said computer users could immediately change passwords on accounts, but they would have to do so again if their operators notify them that they are vulnerable.

Take care of the passwords that are very important to you, he said. Maybe change them now, maybe change them in a week. And if you are worried about your credit cards, check your credit card bills very closely.