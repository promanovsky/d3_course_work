Rihanna Blasted by Charlie Sheen for Snubbing Fiance, Singer Responds? Rihanna Blasted by Charlie Sheen for Snubbing Fiance, Singer Responds?

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Rihanna is the subject of actor Charlie Sheen's public rant.

Rihanna, the 26-year-old Barbadian singer, reportedly dined in the same restaurant as 48-year-old actor Sheen recently. Sheen's fiance, 25-year-old Brett Rossi, was present in the establishment with Sheen on the day that happened to be her birthday.

While Rossi wanted to meet Rihanna on her special day, the singer reportedly refused her which led to Sheen giving an account of the entire ordeal to over 11 million followers on Twitter.

"So, I took my gal out to dinner last night with her best friends for her Bday. we heard Rihanna was present as well," Sheen tweeted. "I sent a request over to her table to introduce my fiancé Scotty to her, as she is a huge fan (personally I couldn't pick her out of a line-up at gunpoint). well, the word we received back was that there were too many paps outside and it just wasn't possible at this time."

Sheen went on to blast Rihanna for the decision to not meet his fiance.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"AT THIS TIME?? lemme guess, we're to reschedule another random 11 million to 1 encounter with her some other night," he questioned. "No biggie for me; it would have been 84 interminable seconds of chugging Draino and 'please kill me now' that I'd never get back. My Gal, however, was NOT OK with it."

The actor was sarcastic in letting Rihanna know about her seemingly pompous behavior.

"Nice impression you left behind, Bday or not," Sheen wrote. "Sorry we're not KOOL enough to warrant a blessing from the Princess. (or in this case the Village idiot)"

While Sheen went on to speak badly about Rihanna's latest pink wig, he chastised her for not being humble enough to respect a veteran in the entertainment industry.

"Here's a tip from a real vet of this terrain; If ya don't wanna get bothered

DONT LEAVE YOUR HOUSE," he wrote. "And if this 'Prison of Fame' is soooooooo unnerving and difficult, then QUIT, junior!"

While Rihanna did not directly respond to Sheen's rant she did take to Twitter to post a cryptic message that could be aimed at the actor.

"If that old queen don't get ha diapers out of a bunch," Rihanna tweeted. "When I'm about to go off and my subconscious b like: nah chill b."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit