Presenting findings from a study of almost 6,000 smokers over five years, the researchers said the results suggest e-cigarettes could play an important role in reducing smoking rates and hence cutting tobacco-related deaths and illnesses.

London: Smokers trying to quit are 60 percent more likely to report success if they switch to e-cigarettes than if they use nicotine products like patches or gum, or just willpower, scientists said on Wednesday.

Presenting findings from a study of almost 6,000 smokers over five years, the researchers said the results suggest e-cigarettes could play an important role in reducing smoking rates and hence cutting tobacco-related deaths and illnesses.

As well as causing lung cancer and other chronic respiratory diseases, tobacco smoking is also a major contributor to cardiovascular diseases, the world's number one killer.

"E-cigarettes could substantially improve public health because of their widespread appeal and the huge health gains associated with stopping smoking," said Robert West of University College London's epidemiology and public health department, who led the study.

Mainly funded by the charity Cancer Research UK and published in the journal Addiction, West's study surveyed 5,863 smokers between 2009 and 2014 who had tried to quit without using prescription medicines or professional help.

The results were adjusted for a range of factors that might influence success at quitting, West said - including age, nicotine dependence, previous attempts to give up smoking, and whether quitting was gradual or abrupt.

They showed that 20 percent of people trying to quit with the aid of e-cigarettes reported having stopped smoking conventional cigarettes.

That compared with just 10.1 percent of those using over-the-counter aids such as nicotine replacement patches or gum. Of those using willpower alone, 15.4 percent had managed to stop.

E-cigarettes contain nicotine - a stimulant not thought to be particularly harmful, although it is addictive - delivering it in a water vapour rather than in smoke from burning tobacco.

A relatively new product, they have become highly controversial, with public health opinion split over whether they might be a powerful tool in helping those hooked on cigarettes to finally give up, or whether they simply replace one bad habit with another.

Because switching to e-cigarettes from tobacco ones does not entail kicking the addiction to nicotine, some specialists say they could spell the end of smoking - which the World Health Organisation (WHO) calls "one of the biggest public health threats the world has ever faced".

But critics point to a lack of long-term scientific evidence to support the safety and effectiveness of e-cigarettes, and warn they may also re-normalise smoking, enticing children or other non-smokers to take it up.

West agreed that evidence about long-term use is e-cigarettes is scant, but stressed the balance of risks had to be weighed against the very strong evidence of tobacco's harms.

"It's not clear whether long-term use of e-cigarettes carries health risks, but from what is known about the contents of the vapour these will be much less than from smoking," he said.

Smoking tobacco kills half of all those who do it, according to the WHO, and has a death toll of 6 million people a year.

West said evidence shows that smokers who seek professional help from doctors or health clinics that provide stop-smoking services have the highest success rates in quitting.

"These almost triple a smoker's odds of successfully quitting compared with going it alone or relying on over-the-counter products," he said.

Reuters