TORONTO -- Pregnant women who drive appear to be at a significantly higher risk of being in a serious car crash during the second trimester of their pregnancies, a new study suggests.

The researchers, who believe fatigue and sleep disruption are probably responsible, say the increase in risk is similar to what is seen for people who suffer from sleep apnea.

"These findings suggest that pregnancy might contribute to the risk of a serious motor vehicle crash," they say in the study, published in the Canadian Medical Association Journal.

Due to the design of the study, the authors cannot say there is a cause-and-effect relationship at play, only that pregnancy appears to be associated with an increase car crash risk. Nor can they say that the pregnant women were more likely to cause crashes; it could be they were simply less able to respond fast enough to avoid being in a collision while they were pregnant.

The findings relate only to motor vehicle accidents that were serious enough to require emergency department care for the pregnant driver.

First author Dr. Donald Redelmeier of the University of Toronto and the Institute for Clinical Evaluative Sciences said earlier studies have shown that for every life-threatening car crash there are about 13 others that don't end up in a visit to the emergency department.

"We estimate that about one in 50 women will be involved in a crash of some severity during some month of the average pregnancy, i.e. a one in 50 absolute risk, making it more common than pre-eclampsia or blood clots during pregnancy," said Redelmeier, who practises internal medicine at Sunnybrook Health Sciences Centre in Toronto.

Redelmeier is an epidemiological whiz, known for teasing out of data observations like the fact that Oscar-winning actors live longer and talking on a cellphone while driving is as dangerous as driving while intoxicated.

His Sunnybrook colleague, Dr. Jon Barrett, planted the seed for this study when Barrett -- head of the hospital's high risk pregnancy program -- mentioned he was working on a study looking at whether pregnancy altered women's reflexes -- their reactions to routine tasks.

Barrett thought his study -- still underway -- would probably find it did. The most common complaint he hears from his patients is how exhausted they are.

"Studies have shown time and time again that when people are tired, it's the routine tasks that become affected," Barrett said.

Redelmeier wondered about the effect on driving and the idea for a study was born.

Along with two other colleagues, the doctors looked at data for half a million women who gave birth in Ontario from April 1, 2006 to March 31, 2011. They looked at hospital records data for five years for each woman, using the three and a quarter years before the pregnancy and the year after the birth as comparisons.

They saw that as a group, the women had a baseline accident rate of about 4.55 serious crashes per every 1,000 women. During pregnancy, that went up to 6.47, a 42 per cent increase. The greatest effect was seen during the early part of the second trimester.

The authors said they aren't trying to suggest pregnant women stop driving. If fact, Redelmeier noted that men of a similar age have even higher rates of accidents than pregnant women do, so the answer is not turning over the keys to one's husband, if a husband or male partner is in the picture.

But pregnant women should be aware of the elevated risk, and ensure that they drive with more caution, Redelmeier said -- stop at every stop sign, use indicator lights to signal turns, wear seat belts at all times.