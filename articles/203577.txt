By Sneha Shankar - The fell the most in the past seven weeks against the dollar, after the European Central Bank, or ECB, signaled that it would ease monetary policy in June.

The euro weakened 0.7 percent to $1.374 while the local currency also fell from almost a 2.5-year high of just below $1.40 as Mario Draghi, ECB president, said that policymakers would be “comfortable” with further taking steps to support the euro-zone economy. Such a signal has made rival currencies more attractive.

“The ECB must take action for this to be a more sustained move lower [for euro-dollar]," Ian Gordon, FX strategist at Bank of America Merrill Lynch said, according to the Wall Street Journal, and added: "Otherwise the market will keep testing the ECB's willingness and resolve to deal with the low inflationary pressure."

German exports also fell 1.8 percent in the month, while imports declined 0.9% from February, Federal Statistics Office data showed, according to the Journal. The British pound fell 0.5 percent against the dollar, to $1.684.

“This is the last time that Draghi can maintain credibility with verbal intervention, and so it’s really time for them to deliver,” Mark McCormick, a macro strategist at Credit Agricole, said, according to Bloomberg, and added: “What’s been interesting, at least in the dollar framework, is U.S. rates have decoupled from economic data.”

The U.S. Labor Department reported that 288,000 jobs were created in April, making it the biggest increase since January 2012, while unemployment dropped to 6.3 percent, the lowest level since September 2008, Bloomberg reported.

“I have some sympathy for the view that the Fed is making a mistake about how much slack there is in the labor market,” Steven Englander, managing director and global head of Group of 10 FX Strategy at Citigroup in New York, said, according to Bloomberg, and added: “But you have to admit that the Fed seems utterly convinced that that slack is there, so until you find something that that refutes their view, I don’t think investors are going to be fighting that fight.”