OTTAWA -- The federal tax agency says it expects to have its online services running again this weekend after blocking public access on April 9 due to the so-called Heartbleed bug.

The Canada Revenue Agency says services will resume once it feels sure the security risk has been addressed.

The government says interest and penalties will not be applied to individual taxpayers filing their 2013 tax returns after April 30 for a period equal to the length of the service interruption.

The Treasury Board issued an order on Thursday to all federal departments using software vulnerable to Heartbleed to disable public websites.

It did did not say how many departments are involved.

The Heartbleed bug is caused by a flaw in OpenSSL software, which is commonly used on the Internet to provide security and privacy.

The bug is affecting many global IT systems in both private and public sector organizations and has the potential to expose private data.

The Canada Revenue Agency had shut down its online filing services on Wednesday, including the tax-filing systems E-file and Netfile.