Microsoft's Surface Pro 3 was the star of a New York City event earlier today, but it's not like the tablet had to work hard to steal the spotlight as it was the one and only device Redmond revealed.

Absent was the widely expected Surface Mini and what's usually the perfunctory showing of a Windows RT variant, in this case the Surface 3.

The assembled Microsoft crew (mainly an extra-exuberant Panos Panay) gushed over the new Surface's 12-inch screen, 10% performance boost, "lapability," and more. Hell, even the N-trig pen got so much stage time, other styli were probably weeping with envy.

We were left with a singular device to scrutinize, but that doesn't mean there isn't plenty to postmortem.

Microsoft isn't ready to go mini

In fact, Microsoft seems to be saying, "Small tablets?? Pah! We laugh at your puny 7- and 8-inch devices!"

The Surface Pro 3 is Microsoft's largest tablet yet and seems to be a not-so-subtle protest to the idea it's going to give us the fabled Surface Mini any time soon.

That doesn't mean Microsoft won't ever show a tablet designed to compete with the likes of the Nexus 7 and other small slates. In fact, the device reportedly almost was shown today, until CEO Satya Nadella and EVP Stephen Elop nixed it.

So, for the foreseeable future, Microsoft's tablet stratagem is clear: kill the laptop, even if it means going bigger.

Today's presentation started with an exposition on the conflict consumers experience when they're shopping for a new device. Do they want a tablet or do they want a laptop? There's an inherent conflict in having to choose, one that apparently has consumers tearing their hair out and leaving the store sacrificing one for the other.

Microsoft is coming for you, laptops

Microsoft's solution? Get rid of the conflict between tablet and laptop by making a product - the Surface Pro 3 - that offers the best of both.

"This is the tablet that can replace your laptop," Panay said anthemically.

Unfortunately, for those seeking a smaller Surface, it doesn't look like Microsoft has figured out how to include everything it uses to define its tablets in a tinier form factor. It doesn't mean it won't, just that it's still at the drawing board.

Sticking with big (and getting bigger) tablets that can replace laptops may serve Microsoft well since having a message that can be beat into consumers' heads can work wonders in sales. Or, it may be completely misreading the market and missing a golden opportunity to cash in on the smaller slate craze.

Even if that turns out to be the case, Microsoft's made its bed. And we all know what a chore it is to rouse them from it.

Hello, Windows RT coffin? This is your final nail

The Surface Mini was/is expected to launch with an ARM-based chip, meaning we're in for a Windows RT tab and not one with full-fledged Windows 8.1.

Even with the Surface Mini missing from the day's proceedings, we can usually rely on Microsoft to throw us a non-Pro bone with a full-sized WRT tablet. A new Surface Pro is announced, ipso facto there's a regular Surface too, right?

Wrong. Today, there was no "comes with the territory" Surface 3, signaling Microsoft is as ready as the rest of us to be done with RT.

The door is always open for the OS to show its face down the line, but there's been so little attention paid to it over the course of the year - I can't remember a mention during Build 2014 - that I'd be surprised if Microsoft resurrected RT in any new device from here on out.

Today's Surface Pro 3 event wasn't the beginning of the end for Windows RT; it was the end of the end for the much-maligned operating system.

The big question

Going bigger and abandoning Windows RT are fairly cut-and-dry takeaways, but I'm puzzled by what seems to be Microsoft's cost conundrum.

I understand that it wants to offer as much in tablet form as it can, but at a starting price of $799 (about £474, AU$863) for the lowest-end variant and the near-need to buy a $130 (about £80, AU$140) Type Cover, the Pro 3 is pushing laptop prices.

The company is determined to build great "mobile first productivity experiences" and that's going to come at a cost, but consumers - even ones who want to "watch a movie and make a movie" - might not be able to reconcile the price with the package.

The takeaway? Microsoft has some cuddly thoughts around its Pro tablet line, but the cold, hard truth is that the Pro 3 is an expensive piece of hardware. Redmond has decided big is in when it comes to Surface size, but consumers may beg to differ when it comes to actually pay for it.