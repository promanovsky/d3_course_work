Regular eye tests could in future be used to diagnose early-stage Alzheimer's, new research suggests. Picture posed.

Regular eye tests could in future be used to diagnose early-stage Alzheimer's, new research suggests.

Early trials of two different techniques show that a key Alzheimer's biomarker can be identified in the retina and lens of the eye.

Both methods were able to distinguish between probable Alzheimer's patients and healthy volunteers with a high level of accuracy.

Although the research is still at an early stage, further work could see such tests used as a first step in identifying individuals with Alzheimer's.

After an initial eye test, more expensive and costly procedures such as PET (positron emission tomography) scans or spinal fluid analysis would then be used to confirm the disease.

Early diagnosis of Alzheimer's is essential to developing effective treatments that do more than alleviate the condition's symptoms.

Virtually every trial of a drug designed to halt or reverse Alzheimer's progression has ended in failure because the patients taking part have already suffered too much damage to their brains, scientists believe.

Shaun Frost, from the Australian science agency the Commonwealth Scientific and Industrial Research Organisation, who led one of the studies, said: "We envision this technology potentially as an initial screen that could complement what is currently used: brain PET imaging, MRI imaging, and clinical tests.

"If further research shows that our initial findings are correct, it could potentially be delivered as part of an individual's regular eye check-up.

"The high resolution level of our images could also allow accurate monitoring of individual retinal plaques as a possible method to follow progression and response to therapy."

The eye tests exploit the fact that the eye is, in effect, an extension of the brain.

In both studies, scientists looked for signs of beta-amyloid protein, which forms clumps in the brains of Alzheimer's patients and is a key hallmark of the disease.

The Australian team used the turmeric spice ingredient curcumin as a fluorescent tag that allowed beta-amyloid to show up in the retina.

A total of 200 volunteers were first asked to take a supplement of curcumin, which binds strongly to beta-amyloid. The protein was then detected in the eye using a novel imaging system.

Levels of beta-amyloid in the retina mirrored those shown in the brain by PET imaging.

Preliminary results on 40 participants showed that the test picked up every participant with Alzheimer's and correctly identified more than 80% of those who did not.

In the other study, researchers from the US company Cognoptix Inc used an ointment to apply a fluorescent label to beta-amyloid in the lens of the eye.

Laser scanning was then able to detect the protein. In tests of 40 volunteers with and without Alzheimer's, it identified those having the disease with 85% accuracy.

Paul Hartung, president and chief executive of Cognoptix Inc, said: "This system shows promise as a technique for early detection and monitoring of the disease."

Both tests showed that levels of beta-amyloid in the eye mirrored those seen in the brain by PET imaging.

The results were presented at the Alzheimer's Association International Conference in Copenhagen.

Dr Simon Ridley, head of science at the charity Alzheimer's Research UK, said: "It is difficult to diagnose Alzheimer's disease accurately and, in many cases, by the time the symptoms have developed, damage has already been going on in the brain for a number of years.

"The development of a quick, cheap, non-invasive test to detect Alzheimer's would be an important step in helping people to receive an early diagnosis, and helping to improve clinical trials so that potential new treatments have the best chance of success.

"This research is promising but is in the very early stages and involves very small sample sizes.

"It is too soon to determine whether these types of tests will be useful for diagnosis of dementia and we would need to see the results of larger trials before drawing any firmer conclusions."

Dr Doug Brown, director of research at the Alzheimer's Society, said: "Finding new and better ways to detect the early stages of Alzheimer's disease could be a game changer for both future research and for people who will develop the condition.

"These studies provide proof of principle that scanning the eye for amyloid could give us insight about what is going on in the brain.

"However as they are only preliminary studies, the eye scans will need further validation before they could be used on people with dementia."

Other research presented at the meeting suggested that a reduced sense of smell could be an early indicator of dementia.

Scientists from Columbia University in New York found that poorer performance in smell tests was significantly associated with increased incidence of the condition.

For each point lower scored on the test the risk of developing dementia went up by 10%.

In a separate study US researchers at Harvard Medical School showed that people with a poorer sense of smell had smaller brain volumes in two key regions linked to memory.

Online Editors