Some parents of children with severe epilepsy say that cannabis helps prevent their children's seizures, but experts caution that it remains unproven whether using the drug to treat epilepsy is safe or effective.

One mother in Colorado turned to cannabis to treat her daughter's intractable seizures, which had started when her daughter was 3 months old, and occurred as often as 50 times a day, according to a new report of the girl's case, published online May 22 in the journal Epilepsia.

The mom gave her daughter doses of the cannabis extract by mouth, using a strain of cannabis with high levels of cannabidiol (CBD), a compound in the plant that does not produce a "high." After taking the drug along with her typical anti-epilepsy drugs, the child, named Charlotte, experienced a dramatic drop in her seizures, to just two to three per month, according to the report.

Other parents have reported similar experiences. In a recent survey of 19 parents who gave CBD-enriched cannabis to their children to treat severe epilepsy, more than half said their child's seizure frequency dropped by at least 80 percent after starting the drug. [Trippy Tales: The History of 8 Hallucinogens]

However, experts stress that such anecdotal reports are not enough to prove that cannabis can treat epilepsy. For example, it's possible that some patients experienced a placebo effect (when patients' symptoms improve even after taking "dummy pills").

Anecdotal reports "can give a potential signal of efficacy and safety, but doctors, patients and parents are all biased," Dr. Maria Roberta Cilio, director of pediatric epilepsy research at the University of California, San Francisco, wrote in a commentary accompanying the new report in the journal. "Rigorous investigation of the safety and efficacy of medical marijuana or individual components such as CBD are necessary for patients with epilepsy before any conclusion is made."

Animal studies of THC, the main ingredient in marijuana responsible for the drug's mind-altering effects, as a treatment for seizures have had mixed results some studies suggest it has anti-epileptic effects, but others suggest it can actually promote seizures, Cilio said. On the other hand, animal studies of CBD have more consistently shown that this compound reduces seizures.

There were some trials in which CBD and other cannabis compounds were used to treat people with epilepsy in the 1970s, 1980s and 1990s, but these studies had flaws, Cilio said. The studies were small each one included just nine to 15 people and one of them was never peer-reviewed. The only conclusion that can be drawn from these studies is that CBD appears to be well tolerated in adults over the short term, Cilio said.

The concentration of CBD and THC in cannabis can vary widely, Cilio said, so more rigorous studies are needed to examine how the composition of the drug affects its safety, and whether the drug interacts with other medications.

After the safety studies, future studies should randomly assign epilepsy patients to receive either CBD or another cannabis compound, or a placebo, Cilio said. The studies also should be double-blind, meaning neither the patient nor the doctor knows which treatment the patient got, Cilio said. These studies should also consider whether cannabis could be harmful to a young child's brain, she added.

"There is a critical need for new therapies, especially for childhood-onset treatment-resistant epilepsies that impair quality of life and contribute to learning and behavioral disorders," Cilio said. "Patients, families and the medical community need objective and unbiased data on safety and efficacy to endorse a new drug to treat epilepsy," she said.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.