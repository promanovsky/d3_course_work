"I am so devastated that Garth Brooks has had to cancel his shows in Ireland. I am a huge fan and couldn't wait to see him perform. It was a dream come true when I was asked to support him. It's a sad day for everyone in Ireland who had been looking forward to these concerts for so long."

– Shane Filan (who was due to support him)

"Naturally, like everybody else, it's disappointing. It's obviously very sad that it came to that. It's disappointing to hear the news, but we fully understand Garth's position at the same time."

– Spokesperson for Nathan Carter (also due to support him on stage)

"Interesting to see that if Garth Brooks has to choose between his children, he'd sooner they all died. #sophieschoice #friendsinlowplaces"

– Dara O Briain

"He was coming here and it was going to be part of history for him. This will go around the world now that we couldn't run five concerts. Maybe Bob Geldof was right – banana republic."

– Louis Walsh

"I hold Garth Brooks fully responsible for this debacle. He was happy to do two concerts and the accountants would have assured him that this would have yielded a nice profit. Then he got three – the icing on the cake. Then four – super profit territory. Then five – almost enough profit to pay off our national debt."

– Councillor Nial Ring

"This is to me like a funeral without a corpse, there is a sadness throughout the nation."

– Lord Mayor Christy Burke

"Dear Mum, I got you surprise tickets for Garth Brooks on the Saturday."

– Bressie

"Gardai have appealed to motorist to stop and tell any Leitrim folk they see walking to Dublin that the Garth gigs are off. #Garth Brooks"

– Dustin the Turkey

"It is difficult to understand why Mr Brooks made the decision to have no concerts at all, when he was refused only two of the five consecutive concerts sought on the grounds that more than three would be 'unacceptable' and unprecedented'. His determination to have 'five or none' smacks of petulance and arrogance, with scant regard for his paying fans."

– Minister Joe Costello

Irish Independent