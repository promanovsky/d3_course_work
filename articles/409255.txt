Researchers have found a way to make see-through mice, but you won't find these critters scampering in your kitchen.

The transparent rodents aren't alive and they're for research only, to help scientists study fine details of anatomy.

Before they are treated with chemicals, the animals are euthanized and their skin removed. Researchers made their inner organs transparent, but not their bones.

Story continues below advertisement

The results look like a rodent-shaped block of gelatin with the organs held in place by connective tissue and a gel used in the procedure.

Mice are mainstays of biomedical research because much of their basic biology is similar to ours and they can be altered in ways that simulate human diseases.

Scientists have been able to make tissues transparent to some degree for a century, and in recent years several new methods have been developed. Last year, for example, a technique that produced see-through mouse brains made headlines. Such treatments reveal far more detail than X-rays or MRI exams could deliver.

The new work is the first to make an entire transparent mouse, experts said.

It should be useful for projects like mapping the details of the nervous system or the spread of cancer within lab animals, said Vivian Gradinaru of the California Institute of Technology, senior author of a paper describing the work. It was released Thursday by the journal Cell.

It might also help doctors analyze biopsy samples from people someday, she said.

The see-through technique involves pumping a series of chemicals through blood vessels, as well as other passages in the brain and spinal cord. Some chemicals form a mesh to hold tissue in place. Others wash out the fats that make tissue block light. It takes about a week to create a transparent mouse, Gradinaru said. The researchers have also made transparent rats, which take about two weeks, she said.

Story continues below advertisement

Scientists can use stains to highlight anatomical details like the locations of active genes.