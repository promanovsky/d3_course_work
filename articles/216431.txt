AHMEDABAD: The most common symptom of hypertension is no symptom at all! On World Hypertension Day on Saturday, experts warned that like diabetes , high blood pressure too is a silent disease that can put people on a high risk of heart attack, stroke and kidney failure.

Kidney diseases experts said that 60% of the patients suffering from chronic kidney disease (CKD) in India are either diabetic or suffer from high blood pressure. About 139 million Indians with uncontrolled hypertension are suspected to have CKD or have a chance of developing CKD.

Nearly 14%-22% patients who have hypertension have a high possibility of developing CKD.

"Every month, we get at least five patients who have no idea that they have been suffering from uncontrolled high blood pressure and their kidneys have been badly damaged due to the hypertension. This is serious,'' said noted kidney diseases specialist Dr Prakash Darji.

Every adult, man or woman, should get their blood pressure checked at least once a year - whether they have any symptoms or not, said Dr Darji.

Nephrologist Dr Siddharth Mavani said that most people associate symptoms like headache and giddiness with blood pressure.

"The most common symptom of hypertension is that a person may not have any symptom at all. People should also look out for postural unsteadiness, transient blackouts and early morning headaches in the back of head, especially around the neck par,'' he said.

Early detection and diagnosis of hypertension can prevent burden of kidney failure. While the normal BP is 120/80, those detected with 130-140/80-90 blood pressure falls in the pre-hypertensive range and should manage it in order to avoid renal, heart and stroke complications.

According to EMRI 108 data, the state has seen a slight rise in the hypertension patients. Compared to 1,944 cases reported in the city in 2012-13, the number has increased to 2,302 in 2013-14.

For the state, the figures are 8,021 and 9,537 respectively.

"This year's theme is 'Know Your Blood Pressure' as most don't see it coming. In its worst form, it can lead to kidney, brain, cardiovascular and eye diseases," said an EMRI official .

