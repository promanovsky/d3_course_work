U.S. stocks were mostly positive on the first trading session of June in which investors were left confused over ISM's May Manufacturing data which was revised twice.

Both the S&P 500 and Dow Jones indices reached new highs. The S&P 500 index reached a new all time high of 1,925.88 while the Dow hit 16,756.64.

Investors and traders face a busy week with a plethora of economic data, including Friday's Nonfarm payroll data.

Recommended: Do The Bulls Have The Edge?

The Dow gained 0.16 percent, closing at 16,743.63.

The S&P 500 gained 0.07 percent, closing at 1,924.97.

The Nasdaq lost 0.13 percent, closing at 4,237.20.

Gold lost 0.22 percent, trading at $1,243.30 an ounce.

Oil lost 0.34 percent, trading at $102.36 per barrel.

Silver gained 0.26 percent, trading at $18.73 an ounce.

News of Note

U.S. May PMI Manufacturing rose to 56.4 from 55.4 in April and topping estimates for 56.2.

May ISM Manufacturing Index slipped to 53.2 from 54.9 in April, missing estimates for 55.5. However, ISM reported in the late morning that it erroneously used the April seasonal adjustment for May's data, and the corrected number has been revised to 56. As if this wasn't enough, the ISM for the second time revised May's report to 55.4 in the early afternoon.

April Construction Spending rose 0.2 percent month over month to $953.55 million, missing estimates for a 0.7 percent gain and lagging February's 0.6 percent gain.

Canada's May PMI slipped to 52.2 from April's 52.9.

China's May PMI rose to 50.8 from 50.4 in April and topping estimates for 50.7.

Gazprom extended its deadline for Ukraine to pre-pay for gas supplies by one week after receiving a $786 million partial payment.

Japan's Government Pension Investment Fund is expected to sell $200 billion in domestic bonds to buy overseas assets.

Analyst Upgrades and Downgrades of Note

Analysts at Bank of America upgraded Alcoa (NYSE: AA) to Neutral from Underperform with a price target raised to $14 from a previous $9.50. Shares gained 1.65 percent, closing at $13.84.

Analysts at Renaissance Capital upgraded Coach (NYSE: COH) to Buy from Hold with an unchanged $54 price target. Shares hit new 52-week lows of $40.34 before closing the day at $40.44, down 0.66 percent.

Analysts at Deutsche Bank maintained a Hold rating on Dean Foods (NYSE: DF) with a price target raised to $15 from a previous $14. Shares lost 1.78 percent, closing at $17.07.

Analysts at Goldman Sachs reiterated a Conviction Buy rating on Dollar General (NYSE: DG) with a $72 price target. Shares gained 0.99 percent, closing at $54.31.

Analysts at Cowen & Company downgraded Express Scripts (NASDAQ: ESRX) to Market Perform from Outperform with a price target lowered to $77 from a previous $79. Shares lost 2.00 percent, closing at $70.04.

Recommended: Are Leveraged ETFs Really Going To 'Blow Up'?

Analysts at Deutsche Bank maintained a Buy rating on General Mills (NYSE: GIS) with a price target raised to $59 from a previous $56. Shares lost 0.04 percent, closing at $54.91.

Analysts at Deutsche Bank maintained a Hold rating on J.M. Smucker (NYSE: SJM) with a price target raised to $102 from a previous $98. Shares lost 0.12 percent, closing at $102.48.

Analysts at Deutsche Bank maintained a Hold rating on Kellogg (NYSE: K) with a price target raised to $66 from a previous $64. Shares hit new 52-week highs of $69.37 before closing the day at $69.12, up 0.20 percent.

Analysts at Oppenheimer maintained an Outperform rating on Netflix (NASDAQ: NFLX) with a price target raised to $500 from a previous $435. Shares gained 1.01 percent, closing at $422.06.

Analysts at Deutsche Bank initiated coverage on Orbitz Worldwide (NYSE: OWW) with a Hold rating and $8 price target. Shares gained 1.62 percent, closing at $7.55.

Analysts at Deutsche Bank maintained a Hold rating on Pinnacle Foods (NYSE: PF) with a price target lowered to $35 from a previous $36. Shares gained 1.41 percent, closing at $31.75.

Analysts at Macquarie downgraded T-Mobile U.S. (NYSE: TMUS) to Neutral from Outperform with an unchanged $38 price target. Shares gained 0.64 percent, closing at $34.55.

Analysts at Pacific Crest upgraded TripAdvisor (NASDAQ: TRIP) to Outperform from Sector Perform with a $116 price target. Shares gained 0.62 percent, closing at $97.77.

Analysts at Barrington Research initiated coverage of Williams-Sonoma (NYSE: WSM) with an Outperform rating and $80 price target. Shares gained 0.10 percent, closing at $66.99.

Analysts at Pacific Crest downgraded Zillow (NYSE: Z) to Sector Perform from Outperform while removing a previous $115 price target. Also, analysts at RBC Capital Markets downgraded Zillow to Sector Perform from Outperform with an unchanged $115 price target. Shares lost 0.03 percent, closing at $117.98.

Equities-Specific News of Note

Apple (NASDAQ: AAPL) revealed its new Mac OS X 10.10 named Yosemite. The company also announced the launch of a cloud storage service which supports both Mac and Windows systems. Additionally, Apple's AirDrop service will now be compatible between iOS and Mac OS devices. Also announced includes a home automation platform, upgrades to iOS 8, a health app, search improvements. The company's CEO Tim Cook said that more than 800 million iOS devices have been sold and the company added 130 million new users over the last year. Shares lost 0.69 percent, closing at $628.65.

Marathon Oil (NYSE: MRO) will sell its Norwegian business to Det Norske Oljeselskap for $2.7 billion as part of a strategy to focus on U.S. business. Shares lost 0.63 percent, closing at $36.43.

Recommended: Doug Kass On A Rigged Market And High-Frequency Trading

According to CNBC, Starwood Hotels & Resorts (NYSE: HOT) is not interested in pursuing M&A activity, specifically in acquiring InterContinental Hotels Group. (NYSE: IHG) Shares of Starwood gained 0.80 percent, closing at $80.49 while shares of InterContinental lost 1.18 percent, closing at $39.33.

B/E Aerospace (NASDAQ: BEAV) has agreed to purchase EMTEQ, a provider of aircraft lightning systems. Also, the company acquired Fischer + Entwicklungen, a manufacturer of seating products intended for helicopters. The two acquisitions come at a combined price tag of $470 million. Shares lost 0.14 percent, closing at $96.61.

Marriott International (NYSE: MAR) plans to invest around $15 billion and build 200 upper-scale hotels over the next three years. Shares lost 0.13 percent, closing at $61.54.

The FTC officially approved the Jos. A. Bank (NASDAQ: JOSB) and Men's Wearhouse (NYSE: MW) merger. The agency noted that the combination of the two companies will not eliminate competition within the segment. Shares of Jos. A. Bank lost 0.03 percent, closing at $64.93 while shares of Men's Wearhouse gained 2.21 percent, closing at $50.88.

U.S. Steel (NYSE: X) announced it will idle two facilities in Texas and Pennsylvania due to what the company claims are unfair imports from foreign manufacturers in to the U.S. Shares lost 1.26 percent, closing at $22.75.

Cheniere Energy (NYSE: LNG) announced that its subsidiary Corups Christi Liquefaction has entered into a 20-year agreement to sell 1.5 million tonnes annually to Spain based Fenosa. Shares hit new 52-week highs of $69.40 before closing the day at $68.50, up 0.57 percent.

Google (NASDAQ: GOOG) announced plans to spend $1 billion to $3 billion to construction 180 satellites orbiting Earth to provide Internet access to regions that are currently not connected. Shares lost 1.06 percent, closing at $553.93.

Related: Google To Add Satellites To Its Global Outreach Efforts

KKR (NYSE: KKR) said that it will terminate its KKR Equities Strategies hedge fund which was launched less than three years ago. Shares gained 0.75 percent, closing at $22.90.

Winners of Note

This morning, Conn's (NASDAQ: CONN) reported its first quarter results. The company announced an EPS of $0.80, beating the consensus estimate of $0.73. Revenue of $335.4 million beat the consensus estimate of $328.52 million. Net income for the quarter rose to $28.47 million from $22.18 million in the same quarter a year ago as the company successfully transitioned sales away from electronics into its better performing furniture and mattress division. Conn's saw its furniture and mattress sales rise from 23.5 percent of total revenue to 29.2 percent of total revenue. The company issued guidance and sees its fiscal 2015 comps rising by five percent to ten percent in the year. Shares gained 6.93 percent, closing at $49.87.

Activist hedge fund Starboard Value reported a 5.6 percent stake in MeadWestvaco (NYSE: MWV). Starboard said that it sees the company being undervalued and intends to assume an activist role to initiate changes at the board level. The company responded by stating it will consider Starboard's suggestions. Shares hit new 52-week highs of $43.21 before closing the day at $42.96, up 5.86 percent.

Ventas (NYSE: VTR) has agreed to acquire American Realty Capital Healthcare (NYSE: HCT) in an all cash and stock deal for $2.6 billion. The deal implies a value of $11.33 for ARC Healthcare. Shares of Ventas lost 2.80 percent, closing at $64.93 while shares of ARC gained 9.65 percent, closing at $10.91.

Ariad Pharmacueticals (NASDAQ: ARIA) announced that its Iclusig drug showed a clinical benefit of 50 percent of patients with refractory metastic and/or unresectable gastrointestinal stromal tumors in a Phase 2 trial. Shares gained 7.43 percent, closing at $6.94.

Broadcom (NASDAQ: BRCM) has hired bankers from JPMorgan to explore strategic alternatives for its cellular baseband business. A sale of the division or wind-down is estimated to save the company $600 million in annualized expenses. Broadcom also reaffirmed prior guidance for the current quarter including revenue of $2 billion to $2.1 billion, but gross margin is guided to be at or above the high end of its guidance. Shares gained 9.32 percent, closing at $34.84.

Decliners of Note

MasTec (NYSE: MTZ) lowered its second quarter earnings and revenue forecasts. The company lowered its EPS guidance to $0.40 from $0.53. Revenue is now guided at $1.1 billion from a previous $1.15 billion to $1.2 billion. Shares lost 10.94 percent, closing at $32.06.

Puma Biotechnology (NASDAQ: PUMA) reported that some of its patients in a metastatic breast cancer Phase 2 study experienced grade 3 diarrhea. Additionally, only three of 40 patients in the study had a partial response to the company's PB272 therapy while four patients showed a prolonged stable disease of more than six months. Shares lost 25.34 percent, closing at $57.06.

Clovis Oncology (NASDAQ: CLVS) announced that “three or four” lung cancer patients in its Phase 1 and Phase II study are now taking insulin to control high blood sugar. Shares lost 6.76 percent, closing at $47.75.

Earnings of Note

After the market closed, Krispy Kreme Doughnuts (NYSE: KKD) reported its first quarter results. The company announced an EPS of $0.23, in-line with the consensus estimate. Revenue of $121.6 million missed the consensus estimate of $126.68 million. Shares were trading lower by 9.74 percent at $17.15 following the earnings report.

Recommended: Earnings Expectations For The Week Of June 2: Ciena, FuelCell Energy And More

Quote of the Day

"As a captain, I think it's important that the players really know who you are and what you stand for, what your beliefs are, and to be consistent in those if things are going good or things are going bad." - Former New York Rangers Captain Mark Messier, in honor of the Rangers advancing to the Stanley Cup finals.

Bonus Quote of the Day

"You miss 100% of the shots you don't take." - Former Los Angeles Kings Captain Wayne Gretzky, in honor of the Kings advancing to the Stanley Cup finals.