NEW YORK (CBSNewYork/AP) – A Manhattan judge has sentenced SAC Capital on criminal fraud charges Thursday under a $1.8 billion deal that prosecutors say included the largest criminal fine ever imposed in an insider trading case.

U.S. District Judge Laura Taylor Swain formally administered the sentence on Stamford, Conn.-based SAC Capital LP and three related entities based on pleas made by a lawyer for the companies last fall to wire fraud and securities fraud.

“These crimes clearly were motivated by greed,” Swain said.

The judge said the nearly $400 million earned illegally by one of the four entities was a “staggering amount” and reflected a corporate culture that sought out portfolio managers and analysts capable of capitalizing on corrupt employees at public companies who would feed them inside information.

Both sides had asked Swain to follow the terms of the agreement announced in November.

In a statement issued immediately after the sentencing, U.S. Attorney Preet Bharara said: “Today marks the day of reckoning for a fund that was riddled with criminal conduct. SAC fostered pervasive insider trading and failed, as a company, to question or prevent it.”

The government said in court papers that the majority of money managed by the defendants during a decade-long fraud that began in 1999 belonged to the hedge fund’s billionaire owner and founder, Steven A. Cohen. It noted eight employees have been convicted of insider trading.

“To have eight criminal convictions in a single institution is remarkable,” Assistant U.S. Attorney Antonia Apps told Swain. She said it reflected the “pervasive” nature of insider trading going on in the companies.

Apps told the judge that a $900 million fine that was part of the $1.8 billion that SAC must pay was believed to be “the largest fine imposed in an insider trading case in history.”

The government said the fine, combined with a $900 million judgment in a forfeiture action, was several times larger than the illicit gains and avoided losses alleged in an indictment brought against the companies last year.

Prosecutors in court papers called it an appropriate punishment and said it sends a “strong message of deterrence” to other institutions that may engage in insider trading or fail to act aggressively to identify and prevent it among its workers.

Representing the companies, longtime SAC General Counsel Peter Nussbaum told the judge the companies “accept responsibility for the misconduct of our employees.” He said the companies were paying a significant penalty and that the crimes had put a “stain on the reputation” of honest and hardworking employees.

Cohen, who lives in Greenwich, Conn., is one of the highest profile figures in American finance and one of the richest men in America. He is among the handful of upper-tier hedge fund managers on Wall Street who pull in about $1 billion a year in compensation.

The deal between SAC and the government did not resolve a civil case that the Securities and Exchange Commission brought last July against him. He was accused of failing to prevent insider trading at the company, which he founded in 1992 and which bears his initials. Cohen has disputed the SEC’s allegations. He has not been charged with a crime.

The government brought the charges after concluding that numerous SAC portfolio managers and research analysts between 1999 and 2010 engaged in insider trading in at least 20 publicly traded stocks.

Six SAC portfolio managers and analysts admitted their roles in insider trading schemes during guilty pleas to criminal charges while two portfolio managers were recently convicted at separate trials of insider trading charges.

You May Also Be Interested In These Stories

[display-posts category=”news” posts_per_page=”4″]

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)