DETROIT -- New Ford CEO Mark Fields will get a pay package worth more than $5.25 million this year as he takes over for the retiring Alan Mulally.

He'll also get stock and options that could drive that figure much higher. And the company will restore his use of a private jet to travel back and forth from his home near Miami.

Ford revealed a portion of Fields' compensation Wednesday in a filing with the U.S. Securities and Exchange Commission. The Dearborn, Michigan, company says it will disclose the total value of Fields' package in its annual proxy statement next year.

Fields made $10.2 million last year as chief operating officer, including stock options and performance-based stock awards, the company said.

For 2014, he'll get a $1.75 million base salary, plus $3.5 million in incentive compensation. The company also is offering him options to buy more than 710,000 shares of stock at Tuesday's closing price of $17.21 per share, an incentive to move the stock price higher.

Ford also said in the filing that Fields will use private aircraft when travelling "for safety and to maximize his availability for company business."

Fields use of corporate jets to travel home on weekends was halted in 2006, when the company was struggling financially. Fields, at the time head of the Americas unit, agreed to give up that perk in 2007 after an outcry from dealers and others.

Fields took over for Mulally on Tuesday. Fields' pay package has a long way to go to match the former Boeing executive, who received compensation valued at $23.2 million last year.

As of March, Mulally had made $197.65 million since joining Ford in 2006, according to calculations by The Associated Press. He led a major restructuring at the automaker, cutting costs by closing factories and globalizing its product offerings worldwide. Ford has earned $42.4 billion since returning to profitability in 2009.

The company also said in the filing Wednesday that Mulally will keep performance-based stock awards he received in March, and the company will pay for housing and travel through Aug. 31.