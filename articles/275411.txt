RICHMOND, Va., June 3 (UPI) -- Dave Brockie's death was caused by an accidental heroin overdose.

The Gwar frontman died at age 50 on March 23, and the Virginia State Medical Examiner ruled Tuesday that "acute heroin toxicity" was responsible for his death. Police reportedly found evidence of heroin use at the musician's home, but the official toxicology reports weren't completed until this week.

Advertisement

"Dave was one of the funniest, smartest, most creative and energetic persons I've known," former Gwar bassist Mike Bishop said of Brockie after his death. "He was brash sometimes, always crass, irreverent, he was hilarious in every way. But he was also deeply intelligent and interested in life."

"He was a criminally underrated lyricist and hard rock vocalist, one of the best, ever!" he lauded. "A great man, a great painter, writer, he was also a hell of a bass guitarist. I loved him. He was capable of great empathy and had a real sense of justice."