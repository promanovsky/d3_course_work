India's manufacturing sector growth eased in March from a one-year high in February largely due to a decline in new order inflows and a shortage of raw material, an HSBC survey said.

The HSBC India Manufacturing Purchasing Managers' Index (PMI), a measure of factory production, stood at 51.3 in March, down from 52.5 in the previous month, signalling a "slight and weaker" improvement of business conditions across the country's goods producing sector.

Activity in the sector expanded for the fifth consecutive month in March. A PMI reading above 50 indicates growth while a lower reading means contraction.

"The momentum in the manufacturing sector eased on the back of a slowdown in order flows and raw material shortages," HSBC chief economist for India and Asean Leif Eskesen said.

According to HSBC, the survey participants commented on higher underlying demand, but indicated that increased competition for new work and the elections weighed on growth.

Meanwhile, export orders increased at the strongest pace in almost three years in March on the back of improved demand conditions in key markets, HSBC said.

On price rise, the report said inflationary pressures eased in March. Input costs rose at the weakest rate in nine months and output prices increased at the slowest pace since last June. "Growth is likely to remain moderate in coming months as fiscal tightening, relatively high corporate leverage, and rising non-performing loans in the banking system pose headwinds to growth," Eskesen said.