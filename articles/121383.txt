Beijing - China said on Wednesday that it will push forward with reform of its renminbi exchange rate mechanism after the United States criticised Beijing for keeping its currency weak.

China hopes the United States can see this clearly and “properly handle the relevant issues”, China's Foreign Ministry spokeswoman, Hua Chunying, said at a daily news briefing.

The Obama administration on Tuesday stopped short of declaring China a currency manipulator, but expressed doubt over the resolve of the world's second-largest economy to let market forces guide the value of the yuan.

Reuters