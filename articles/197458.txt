E-commerce giant Alibaba Group Holding Ltd. has officially filed for an initial public offering in the US.

The fast growing Chinese internet company has been touted as the largest marketplace in the world and valued itself at around $109 billion in April.

On Tuesday, Alibaba announced that it was planning to raise $1 billion, but many expect that the IPO will raise more than $20 billion. While all of the details for the sale haven’t been released, analysts don’t expect the IPO until the end of the summer at the earliest.

Top News

In other news around the markets:

Tension in Asia escalated on Wednesday after Philippines police confiscated a Chinese fishing boat in the South China Sea. The waters, rich in oil and natural gas, are in the middle of a territorial dispute between China, Vietnam, the Philippines, Taiwan Malaysia and Brunei. The boat in question is said to have had around 500 turtles on board, some of which are protected under Philippine law.

Hewlett-Packard said it was planning to invest over $1 billion in cloud computing development over the next two years. The plans include expanding its OpenStack cloud services and developing new products which will make it easier for businesses and individuals to manage and control cloud computing.

A preliminary investigation into the fire which killed dozens of pro-Russian separatists in Ukraine last week showed that the blaze was likely ignited by several rebels who accidentally dropped Molotov cocktails on the roof. The finding will probably to add fuel to the growing tension between rebel groups and the Ukrainian government as the conflict between the two sides continues to escalate. With a Presidential election set for this month, many are worried that the country could soon slip into a civil war.

as the conflict between the two sides continues to escalate. With a Presidential election set for this month, many are worried that the country could soon slip into a civil war. Thai markets were on a downward slope on Wednesday as investors revisited their concerns about political turmoil in the small Asian nation. After a court ruled that Prime Minister Yingluck Shinawatra has violated the nation’s constitution and should step down, many worried that instances of political violence will increase which in turn will add uncertainty to the nation’s markets.

Asian Markets

Asian markets were down across the board, the Japanese NIKKEI was down 2.93 percent, the Shanghai composite lost 0.89 percent and the Shenzhen composite was down 1.47 percent. The Hang Seng index fell 1.05 percent, and the South Korean KOSPI was down 1.00 percent.

European Market

European markets were mostly lower with the exception of the German DAX which eked out a 0.02 percent gain. The UK’s FTSE lost 0.29 percent, the eurozone’s STOXX 600 was down 0.22 percent, Italy’s MIB lost 0.86 percent and the Spanish IBEX fell 0.53 percent.

Commodities

Energy futures improved after US inventory data showed a drop; Brent futures gained 0.27 percent and WTI futures were up 1.02 percent. Gold and silver were up 0.31 percent and 0.38 percent respectively while industrial metals were mixed. Copper lost 0.38 percent and Aluminum was down 0.11 percent, but zinc gained 0.64 percent and tin was up 0.32 percent.

Currencies

Currency markets were quiet; the euro was steady at $1.3919 and lost 0.05 percent against the pound and 0.15 percent against the yen. The dollar was also lower against the yen, down 0.09 percent, but the greenback gained 0.08 percent against the franc.

Earnings

Notable earnings released on Tuesday included:

Walt Disney Company (NYSE: DIS) reported second quarter EPS of $1.11 on revenue of $11.65 billion, compared to last year’s EPS of $0.79 on revenue of $10.55 billion.

(NYSE: DIS) reported second quarter EPS of $1.11 on revenue of $11.65 billion, compared to last year’s EPS of $0.79 on revenue of $10.55 billion. DirecTV (NASDAQ: DTV) reported first quarter EPS of $1.63 on revenue of $7.86 billion, compared to last year’s EPS of $1.20 on revenue of $7.58 billion.

(NASDAQ: DTV) reported first quarter EPS of $1.63 on revenue of $7.86 billion, compared to last year’s EPS of $1.20 on revenue of $7.58 billion. Emerson Electric Company (NYSE: EMR) reported second quarter EPS of $0.80 on revenue of $5.81 billion, compared to last year’s EPS of $0.77 on revenue of $5.96 billion.

(NYSE: EMR) reported second quarter EPS of $0.80 on revenue of $5.81 billion, compared to last year’s EPS of $0.77 on revenue of $5.96 billion. Whole Foods Market, Inc (NASDAQ: WFM) reported second quarter EPS of $0.38 on revenue of $3.30 billion, compared to last year’s EPS of $0.38 on revenue of $3.03 billion.

Pre-Market Movers

Stocks moving in the Premarket included:

Yahoo! Inc. (NASDAQ: YHOO) gained 1.81 percent in premarket trade after falling 1.14 percent on Tuesday.

(NASDAQ: YHOO) gained 1.81 percent in premarket trade after falling 1.14 percent on Tuesday. Carnival Corp (NYSE: CCL) was up 0.33 percent in premarket trade after losing 1.24 percent over the past five days.

(NYSE: CCL) was up 0.33 percent in premarket trade after losing 1.24 percent over the past five days. Bank of America Corp (NYSE: BAC) fell 0.75 percent in premarket trade after losing 2.32 percent on Tuesday.

(NYSE: BAC) fell 0.75 percent in premarket trade after losing 2.32 percent on Tuesday. Transocean Ltd (NYSE: RIG) was down 0.58 percent in premarket trade after falling 0.49 percent on Tuesday.

Earnings

Notable earnings releases expected on Wednesday include:

Prudential Financial, Inc. (NYSE: PRU) is expected to report first quarter EPS of $2.26 on revenue of $11.72 billion, compared to last year’s EPS of $2.28 on revenue of $11.83 billion.

(NYSE: PRU) is expected to report first quarter EPS of $2.26 on revenue of $11.72 billion, compared to last year’s EPS of $2.28 on revenue of $11.83 billion. 21 st Century Fox (NASDAQ: FOXA) is expected to report third quarter EPS of $0.35 on revenue of $7.99 billion, compared to last year’s EPS of $0.36 on revenue of $9.54 billion.

(NASDAQ: FOXA) is expected to report third quarter EPS of $0.35 on revenue of $7.99 billion, compared to last year’s EPS of $0.36 on revenue of $9.54 billion. Anheuser-Busch Inbev SA (NYSE: BUD) is expected to report first quarter EPS of $1.03 on revenue of $10.50 billion, compared to last year’s EPS of $1.16 on revenue of $9.17 billion.

(NYSE: BUD) is expected to report first quarter EPS of $1.03 on revenue of $10.50 billion, compared to last year’s EPS of $1.16 on revenue of $9.17 billion. Duke Energy Corporation (NYSE: DUK) is expected to report first quarter EPS of $1.12 on revenue of $6.44 billion, compared to last year’s EPS of $1.02 on revenue of 5.90 billion.

Economics

Wednesday’s economic calendar will be relatively quiet with most investors focused on US oil inventory data. Other notable releases include, US consumer credit, German factory orders, the French trade balance, and French industrial production.

For a recap of Tuesday’s market action, click here.

Tune into Benzinga’s #PreMarket Prep show with Dennis Dick and Joel Elconin here.