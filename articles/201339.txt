Could Dr. Dre be the first billionaire in hip hop? On May 9, Apple got closer to buying the rap mogul’s company, Beats Electronics for $3.2 billion. Tyrese celebrated with Dre right after the deal reportedly went through — watch the video here!

Dr. Dre is calling himself the “first billionaire in hip hop” in a new video, and if his deal with Apple is finalized, he may be! On May 9, multiple outlets reported that Apple was in the last stages of purchasing Beats Electronics for $3.2 billion. However, will that really land Dre on the “Forbes list,” as his friend Tyrese Gibson says in their celebratory video?

Dr. Dre’s Beats Electronics — Sold To Apple For $3.2 Billion?

“First billionaire in hip-hop, right here on the motherf—ing west coast,” Dr. Dre is yells into the camera in the video, that Tyrese shared on his Facebook page.

This follows the report that came out on May 8 by the Financial TImes that the deal is about to be finalized for billions of dollars. While Dre has earned the congratulations of his friends on the video, he also got a shout out from P. Diddy.

Diddy posted a pic of TMZ’s article, captioning it, “This is BIG! This is Historical! If anyone deserves it, it’s you! Congratulations Dr. Dre! #fromthesoontobe#DrCombs #wemadeit #proudofyou#thebarhasbeenrsised #motivated #timetogotowork.”

Dre celebrated in the studio with movie director F. Gary Gary and Tyrese until 4 A.M., sources told TMZ.

Will He Really Make Forbes List?

“Oh s–t, the Forbes list just changed!” Tyrese yelled in the video, but that may not be the case.

Dre, whose net worth was listed as $550 million in 2014 by Forbes, currently is no. two on the list of hip hop artists, behind Diddy. His stake in the company currently stands around 25%, and with its value at $2 billion, that means his network would come out to about $800 million, the site stated.

So yes, that would make him no. 1 on the hip hop list, but not a billionaire just yet.

Do you think the deal should happen, HollywoodLifers? Let us know!

— Emily Longeretta

More Dr. Dre News: