Shutterstock photo

Investing.com -

Investing.com - The euro edged higher against the dollar and the yen on Tuesday, but gains were held in check after data showed that German business confidence deteriorated this month, amid concerns about the impact of crises in Iraq and Ukraine.

EUR/USD edged up 0.13% to 1.3620, up from session lows of 1.3598.

The pair was likely to find support at 1.3540 and resistance at 1.3630.

The single currency shrugged off a report showing that the German Ifo business climate index fell to a six-month low of 109.7 this month from 110.4 in May and compared to estimates of 110.3.

"Assessments of the current business situation remained good, but companies were less optimistic about future business developments. The German economy fears the potential impact of the crises in Ukraine and Iraq," Ifo President Hans-Werner Sinn said.

Sinn added that although the export outlook for manufacturing had "clouded over considerably, the majority of manufacturers remain optimistic."

The manufacturing component of the index fell to 15.7 from 19.0 in May, but the construction, wholesaling and retail components all improved.

The report came one day after data showed that the euro zone's composite index of service and manufacturing sector activity fell to a six month low of 52.8 in June.

France's private sector continued to contract this month, dragging on the euro zone as a whole, while Germany's private sector continued to expand this month, but at a slower than forecast rate.

The shared currency edged higher against the yen, with EUR/JPY inching up 0.11% to 138.81, off Monday's lows of 138.27.

Elsewhere Tuesday, the euro gained ground against the broadly weaker pound, with EUR/GBP rising 0.31% to 0.8014.

Sterling weakened across the board after Bank of England Governor Mark Carney said there appeared to be more slack in the U.K. labor market than previously thought, tempering expectations for a rate hike this year.

In testimony to parliament's Treasury committee, Carney said the exact timing of rate rises would be driven by data and reiterated that when rate hikes did come they would be limited and gradual.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.