A long-standing mystery about the formation of "fairy circles" in Africa's Namibian grasslands has been reignited after a scientist quashed the only plausible theory put forward.

The fairy circles are barren patches of land that are frequently found in certain grassland regions in Africa. Size-wise, some of the fairy circles are just a few metres in diameter, but others can reach up to 20m.

It was previously thought the circles were formed by termites, with the insects nibbling away the grassroots, causing dieback of vegetation.

Another theory suggested that hydrocarbons from the depths of the earth were responsible - the gasses were thought to rise to the surface resulting in the disappearance of vegetation. Some scientists also believe the self-regulating grass growth is the cause, as the fairy circles are found in transition areas between grasslands and desert regions.

Stephan Getzin from the Helmholtz Centre for Environmental Research (UFZ) in Leipzig, said: "Although scientists have been trying to answer this question for decades their mystery remains as yet unresolved."

Published in the journal Ecography, researchers carried out a detailed analysis of the spatial distribution of the fairy circles and found a remarkably distribution pattern.

Getzin looked into which hypothesis was most likely. With aerial images, the researchers looked at the exact distribution of the fairy circles: "The occurrence of such patterning in nature is rather unusual. There must be particularly strong regulating forces at work".

Discrediting the termite theory, the authors said no one has observed these creatures creating the holes, let alone making such consistent patterns. Getzin said: "There is, up to now, not one single piece of evidence demonstrating that social insects are capable of creating homogenously distributed structures, on such a large scale."

In terms of abiotic gas emissions, the team say this is unlikely to lead to such evenly dispersed fairy circles.

They said the most likely cause was the resource competition among plants and vegetation, which could potentially result in regular circles. Offering an example, they said a mature tree needs sufficient space and nutrition, so can only survive an appropriate distance from its neighbour.

"We consider this at present being the most convincing explanation," Getzin said.