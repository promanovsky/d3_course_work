Celebrity

Harris showed no remorse when he was found guilty of abusing a dozen of girls, some of which were as young as eight years old.

Jul 5, 2014

AceShowbiz - Shamed British entertainer Rolf Harris was sentenced on Friday, July 4 to five years and nine months in prison for 12 indecent assaults against four girls. The 84-year-old was found guilty on Monday in a London court of sexually abusing women and girls, including one aged just seven or eight.

Four young women fell victim to Harris' crime in the U.K. between 1968 and 1986. Dozens more women came forward with statements that they had been abused by Harris during the trial. Tonya Lee said she was a 15-year-old aspiring actress when Harris groped her in a London pub. A woman who was 8 years old when she was abused said, "On that day and in the space of a few minutes my childhood innocence was gone."

Harris was charged under Operation Yewtree which investigates allegations of abuse by public figures. "For well over 50 years you have been a popular entertainer and television personality of international standing - with a speciality in children's entertainment," Mr Justice Nigel Sweeney said. "You are also an artist of renown. You have been the recipient of a number of honors and awards over the years. You have done many good and charitable works and numerous people have attested to your positive good character."

He went on, "You took advantage of the trust placed in you, because of your celebrity status, to commit the offenses against three of your victims. In every case the age gap between you and your victim was a very considerable one. You clearly got a thrill from committing the offenses whilst others were present or nearby. Whilst such others did not realize what you were doing, their presence added to the ordeal of your victims."

The judge added, "You have shown no remorse for your crimes at all. Your reputation now lies in ruins, you have been stripped of your honors but you have no one to blame but yourself." Harris' daughter Bindi watched the trial from the public gallery while his wife Alwen did not attend the court despite attending the eight-week trial. Harris will likely serve only half of his sentence before released on probation.

Harris escaped child porn prosecution although police found indecent images of children upon raid to his home. Sasha Wass QC told Southwark Crown Court, "In the light of the 12 unanimous convictions on the counts that Mr Harris faced, the Crown Prosecution Service has decided that it is no longer in the public interest to proceed with a trial on these four charges."