Pippa Middleton has revealed that her bridesmaid dress which she wore for her sister's wedding to Prince Williams was meant to be "insignificant" and "blend in" with the train of the wedding dress.

She received a huge amount of worldwide attention following the wedding, which she said was "completely unexpected".

Middleton appeared on the Today Show in the US, where she told Matt Lauer about the attention she received: "It was completely unexpected. You know, I think the plan was not really for it to be a significant dress. Really just to sort of blend in with the train."

She revealed that the attention is both "flattering" and "embarrassing" as "it wasn't planned".