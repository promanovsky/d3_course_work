Washington: Facebook is buying the maker of the virtual reality headset, the Oculus Rift, for about two billion dollars in cash and stock.

The social networking company announced it has agreed to buy the virtual reality technology company Oculus VR.

According to a website, Facebook said with the acquisition, it plans to extend Oculus` virtual reality capabilities beyond gaming into areas such as communications, media, entertainment and education.

Facebook CEO Mark Zuckerberg views virtual reality as the next big thing in social. In a statement he said Oculus has the chance to create the most social platform ever, and change the way people work, play, and communicate.

The deal, valued at about two billion dollars, includes 400 million dollars in cash and 23.1 million shares of Facebook common stock, and allows for an additional 300 million dollars earn-out in cash and stock based on milestones.

Oculus was founded by 21-year-old Palmer Luckey in 2012. The company launched a Kickstarter campaign to fund development and raised more than 2.4 million dollars from online backers.

The deal for Oculus is expected to close in the second quarter of this year, the report added.