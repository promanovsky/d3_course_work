Herbalife Ltd. (NYSE:HLF)’s stock price had been recovering in recent weeks after bulls got over the initial shock that the Federal Trade Commission has launched a probe into the multi-level marketing firm’s business practices, but all that ground was lost again today after Fox Business News journalist Charlie Gasparino tweeted that ABC’s 20/20 intended to run a segment making similar allegations to those of hedge fund manager Bill Ackman, specifically that the company makes fraudulent claims to recruit distributors and acts more like a pyramid scheme than a legitimate business.

20/20 may have found a whistleblower for Herbalife

“Company officials at Herbalife are bracing for a 20/20 expose on the company,” Gasparino said on Fox Business News, who heard about the segment from people at Herbalife Ltd. (NYSE:HLF) who had been interviewed. “20/20 is looking at how Herbalife recruits its distributors, and that is at the heart of both Ackman’s critique of the company that it’s a pyramid scheme and the Federal Trade Commission investigation.”

Barron’s Mailbag June 1962: Irving Kahn On False Comparisons The following letter from Irving Kahn appeared in the June 25, 1962, issue of Barron’s. Irving Kahn wrote to Barron's criticising the publication’s comparison of the 1962 market crash to that of 1929. Irving Kahn points out that based on volume and trading data, the 1962 decline was a drop in the ocean compared to Read More

If 20/20 were simply making the same arguments as Ackman or LULAC (League of United Latin American Citizens), then the bad publicity on such a big stage would likely depress the stock price temporarily, but it might not have much long-term impact.

But according to Gasparino, Herbalife is “bracing for a possible whistleblower to emerge out of this thing.”

Nothing is certain until the segment runs

Gasparino is quick to clarify that none of this is certain because the story might not run for any number of reason (he also isn’t accusing Herbalife of anything), but people at Herbalife Ltd. (NYSE:HLF) have heard that the possible whistleblower is someone who has worked behind the scenes (so not just a disgruntled distributor whose venture didn’t work out) and that this person is cooperating with the Federal government.

We won’t know for sure until the segment actually runs, and it’s easy to imagine how a combination of rumors about a major negative story plus the pressure of being under investigation could make Herbalife Ltd. (NYSE:HLF) officials assume the worst case scenario, but if a whistleblower really has come forward with evidence that documents executives intent to defraud distributors, it would be a game changer, and exactly the kind of break that Ackman and other Herbalife shorts have been seeking for more than a year.

Watch the latest video at video.foxbusiness.com