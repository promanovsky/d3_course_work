Five-time Oscar nominee Amy Adams displayed some very un-Hollywood behavior Friday on a flight from Detroit to Los Angeles.

Adams, who was born and raised on military bases, gave up her first class seat for a solider who was sitting in coach.

ESPN personality Jemele Hill witnessed the act of kindness and tweeted about the incident to her 244,000 followers:

Advertisement

Just saw actress Amy Adams do something incredibly classy. She gave her 1st class seat to an American soldier. I'm an even bigger fan now. - Jemele Hill (@jemelehill) June 27, 2014

I don't think he knows she did it RT @cryengine1007: @jemelehill if i was the soldier i would have refused, she is after all a lady. - Jemele Hill (@jemelehill) June 27, 2014

Hill later explained to ABC News : "I noticed Ms. Adams was in first class and as I was getting seated, I saw the flight attendant guide the soldier to Ms. Adams' seat. She was no longer in it, but it was pretty clear that she'd given up her seat for him. I was incredibly impressed, and I'm not even sure if the soldier knew who gave him that seat. I guess he will now!"

Looks like Adams also took time for a selfie with a fan: