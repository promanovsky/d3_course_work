The fifth India-US Strategic Dialogue was hailed by many as the turning point in India-US bilateral ties after it underwent a period of deep strife and discontent for past couple of years, even as both sides raised a laundry list of issues concerning boosting ties in key areas of energy, trade and investment and security.

Reminiscing India's 'suspicion' towards the US after the Cold War, US Secretary of State John Kerry, who co-chaired the strategic dialogue with India's external affairs minister Sushma Swaraj, said both the countries are now indispensable partners in the global economy.

"US and India is and should be indispensable partners in the 21st century… Now that India's new government has won an excellent mandate to deliver change and reform, together we have singular opportunity, to help India meet its challenge, to boost trade and investment, to support South Asia's connectivity, to develop cleaner energy, to develop security," Kerry told reporters here on Thursday during a joint press briefing with Swaraj.

He highlighted that both sides have "a lot of homework to do" before Prime Minister Narendra Modi visits the US on September 30 to meet US President Barack Obama. He said the US is viewing Modi's upcoming visit as the beginning of a "new chapter in our ties".

During his visit to the US, Modi he will also address the UN General Assembly in New York.

"We all have a lot of homework to do. We need to put specifics on the table for PM Modi's visit to US to meet President Obama. Prime Minister (Narendra) Modi's visit "will pave the way to a new chapter in our ties of two great democracies," he stated.

According to the joint statement issued by the ministry of external affairs, both sides vowed to expand the commercial dialogue so that more businesses and investment can flow between the two countries.

On the civil nuclear cooperation, both India and the US reaffirmed their commitment to implement the agreement expeditiously. Both Kerry and Swaraj urged the Nuclear Power Corporation of India and US companies Westinghouse and General Electric-Hitachi to expedite the necessary work to conclude pricing and contractual details.

On snooping, Swaraj raised strong objections to such moves made by the US and stated "this is unacceptable", especially when there is sharing of information. According to Kerry, the US is reviewing the strategy deployed by US intelligence authorities on matters related to sharing of information on terrorism and counter-terrorism.

Kerry also pressed the need for greater cooperation in areas such as new and renewable energy. He applauded the new initiative on Promoting Energy Access through Clean Energy in bringing Indian citizens in rural areas access to off-grid sources of clean energy.

"Climate change and energy shortages are not something that will happen way up in the future. It's here with us now," said Kerry.