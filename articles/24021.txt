This is the skin of a patient after 3 days of measles infection; treated at a New York hospital. (CDC/file)

NEW YORK (CBSNewYork/AP) — Medical facilities may be partly to blame for a rare outbreak of measles, a New York City health official says.

There have been 20 confirmed cases of measles in the city during the past several weeks.

Deputy Health Commissioner Dr. Jay Varma told The New York Times that investigators are looking into whether some patients were exposed to the virus in hospitals or doctors’ offices.

Varma said there were measles patients who were not quarantined as quickly as they should have been. He did not identify any medical facilities that may have been at fault.

The Times reported that NewYork-Presbyterian Hospital/Columbia University Medical Center alerted staffers by email last week that hundreds of patients could have been exposed to measles.

The hospital said it’s contacting the patients as a precaution.

Measles is a very contagious viral infection. Symptoms typically include a rash and high fever along with a cough, red eyes and runny nose and usually lasts five to six days, according to health officials.

The illness usually begins with a rash on the face and then moves down the body and may include the palms of the hands and soles of the feet.

You May Also Be Interested In These Stories

[display-posts category=”news” posts_per_page=”4″]

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)