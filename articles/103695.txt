Andrew Garfield surprised theatre-goers by making a cameo appearance in a London play on Tuesday (April 8).

The Amazing Spider-Man 2 actor made an unannounced appearance as part of the ensemble in Punchdrunk's The Drowned Man at Temple Studios.



The interactive play took place across four floors of a former sorting office behind Paddington Station. The play is currently scheduled to run until June, having opened in 2013.

The production includes 40 cast members, who move around the building for an audience of 600.

Garfield is currently in London to promote his second Amazing Spider-Man film, which also premiered on Tuesday.

The British actor was nominated for a Tony Award in 2012 for his role of Biff Loman in Death of a Salesman.

The Amazing Spider-Man 2 opens at UK cinemas on April 16, and in the US on May 2. Watch a trailer below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io