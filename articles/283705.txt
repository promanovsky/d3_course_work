Heads up, Twitter users. A potentially serious cross-site scripting (XSS) vulnerability has been discovered in Twitter's popular client TweetDeck.

Twitter on Wednesday afternoon temporarily shut down TweetDeck to investigate the issue, though the service is now back up and running. Twitter said it has fixed the problem, and that users should log out of TweetDeck and log back in to fully apply the patch.

"We've verified our security fix and have turned TweetDeck services back on for all users," Twitter said. "Sorry for any inconvenience."

Earlier, Twitter pushed out a fix that was supposed to close the hole, though it appears that initial patch didn't work. Many users said they logged out of TweetDeck and logged back in, as directed, but were still getting a pop-up message that said "XSS in Tweetdeck."

At this point, details about the flaw are scant, though security researcher Graham Cluley said it does not appear to have been exploited maliciously.

"That doesn't mean you should rest on your laurels — after all, information about how to exploit the flaw is out there, and it is easy to imagine how someone could take advantage of it with malicious purposes," Cluley wrote in a blog post Wednesday.

Cluley earlier on Wednesday urged users quit TweetDeck immediately and revoke the platform's access to your Twitter account. If you'd still like to do so as an added precaution, go to the Apps section under Settings on the Twitter website, find Tweekdeck in the list, and select "revoke access."

Twitter itself suffered an XSS vulnerability back in 2010. The flaw was used to mess with the Twitter page of Sarah Brown, wife of the former British Prime Minister, among others. The attack redirected users to a porn site based in Japan, though the flaw could have been exploited to send users to phishing or malware-laden sites.

Twitter acquired TweetDeck in May 2011 for about $40 million.

For more, watch PCMag Live in the video below, which discusses the TweetDeck outage.

Further Reading

Software & Service Reviews