Jenny McCarthy Slammed for Anti-Vaccine Stance on Twitter Jenny McCarthy Slammed for Anti-Vaccine Stance on Twitter

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Jenny McCarthy was slammed Thursday when she asked her Twitter followers an innocent question. Instead of the easygoing answers she was expecting, the "View" co-host was inundated with criticism regarding her views on vaccines and their dubious connection to autism.

Jenny McCarthy posed a question to her 1.13 million Twitter followers on Thursday: "What is the most important personality trait you look for in the mate? Reply using #JennyAsks."

Many users on the social media network attacked the 41-year-old for believing that a vaccine caused her son's autism and her staunch promotion of anti-vaccine sentiments.

"Somebody who gets that refusing vaccines because of 'toxins' and then shilling for e-cigs makes you a pathetic hypocrite #JennyAsks," @KiwiLawBird wrote.

"Someone who puts the interests of the community ahead of preserving their own absurd unshakeable ignorance #Jenny Asks," another posted on the microblogging site.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"My ideal partner understands that 'doing my own research!!1!' isn't cherry picking blurbs from blogs #JennyAsks," @KellieRyanB tweeted in response.

"Someone who vaccinates, b/c I'd want our kids to survive," @sethmnookin replied.

The seemingly endless deluge of critical and even vitriolic tweets prompted the former Playboy playmate to respond on Twitter to those she dubbed "haters."

"Thank you to all the haters who tweet my name. You make my Q SCORE higher and higher," McCarthy wrote, referencing the statistic that tracks the value of celebrity endorsements and brands. "It's because of you I continue to work. Thank you!"

McCarthy has campaigned against vaccines, claiming that mercury and other chemicals in vaccines cause autism in children. Despite the evidence— any connection between the rise in the detection of autism and more vaccines is coincidental; 90 percent of the children who die from influenza are not vaccinated; the countless studies pointing to the statistical safety of vaccinations— the anti-vaccine crowd is growing.

Besides McCarthy and her ex-boyfriend, comedian Jim Carrey, Kristin Cavallari of "The Hills" is also anti-vaccine. She hasn't vaccinated her son, Camden Jack Cutler, even though he is 18 months old.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit