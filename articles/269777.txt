UPDATE, June 2, 2014, 5:52 p.m. ET John Oliver asked for the trolls, and the trolls obliged. A day after a segment on Last Week Tonight featured Oliver putting out the bat signal for Internet trolls, the Federal Communications Commission website has been deluged by comments. The site is experiencing intermittent outages.

John Oliver took on net neutrality on Last Week Tonight in the way only he can on Sunday evening. "They should call it cable company fuckery," Oliver said.

The man who brought you the "97 scientists versus three scientists" climate debate laid out in 13 minutes what just about every media organization has been trying to describe with thousands of words.

After tearing into the FCC, Comcast, Time Warner Cable and the Obama administration, Oliver called for help from someone unexpected — Internet trolls. Oliver pointed viewers (and, more importantly, web commenters) to visit www.fcc.gov/comments to vent about the FCC proposal, which was opened for comment in mid May.

"This is the moment you were made for, commenters," Oliver said.

BONUS VIDEO: Mashable Explains Net Neutrality