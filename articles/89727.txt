Academy Award winning actor Mickey Rooney arrives at the world premiere of the 40th anniversary restoration of the film "Cabaret"

FILE - In this March 19, 1957, file photo, actor, singer and dancer Mickey Rooney, wearing spats and a pinstriped suit, performs a dance routine during rehearsal for the television show "George M. Cohan Story" in Hollywood, California. AP Photo.

FILE - In this Jan. 5, 1942, file photo, Mickey Rooney, 21, Movieland's No. 1 box office star, and Ava Gardner, 19, of Wilson, N.C., pose together in Santa Barbara, California, shortly after the couple applied for a marriage license.

Mickey Rooney, right, and Jane Rooney arrive during the 82nd Academy Awards, in the Hollywood section of Los Angeles in 2010. AP Photo.

Mickey Rooney the diminutive Hollywood star has died at the age of 93, according to his family.

Rooney might be best remembered for his ceaseless ups and downs, his dramatic failures and his many comebacks. But his roller-coaster melodrama – he was married eight times and quickly spent the fortune he amassed – wouldn't have mattered if he hadn't also had genuine, enduring talent.

In the late 1930s and early 1940s, while under contract for MGM, Rooney was one of the most popular stars on the planet. At just 19, he was the top box-office draw.

He became the United States' biggest movie star while a brash teenager in the 1930s and later a versatile character actor in a career that spanned 10 decades. Rooney died on Sunday of natural causes in Los Angeles.

He developed a reputation as a hard-partying, off-screen brat in his heyday. "He went through the ladies like a hot knife through fudge," said Ava Gardner of Rooney, whom she'd married when she was 19. It may seem disrespectful to dwell on this aspect of Rooney's reputation so soon after his death, but it was hardly a secret in his lifetime, he was notorious.

The question is: how did his on-screen image remain squeaky clean despite his real-life endeavours? The answer is a case history in the workings of the Hollywood super-studio MGM.

But first: how did he do it? Rooney was 5ft 2in, red-headed and all teeth, the goofiest kid in California. He was said to be, "pound for pound, the most talented performer in the history of Hollywood". Even Rooney himself wondered, in his 1991 memoir, 'Life Is Too Short': "What was my appeal?"

The question wasn't entirely rhetorical, as he went on to explain: "I was a gnomish prodigy – half-human, half-goblin, man-child, child-man."

Those qualities were as nothing compared to his flirting technique, which he characterised as "a combination of early Neanderthal and late Freud". It was this, perhaps, that led his lover and co-star Lana Turner to dub him, in reference to his best-known role as Andy Hardy, "Andy Hard-on".

To modern eyes, Andy Hardy is as dated as the characters played by Shirley Temple, and twice as grating. Each film – and there were 16 of them between 1937 and 1958 – was a small-town fable, in which Andy, the son of an indulgent, well-respected judge and his calm, homemaker wife, gets into some sort of scrape and learns a lesson from it in the end.

Usually the scrape had something to do with love, which was presented innocently, as a perennial conundrum. "Dad, I don't understand these modern girls," he whines in 'Love Finds Andy Hardy' (1938), as the judge prepares his parental lesson from an armchair.

In Rooney's subsequent decades, things would rarely come as easily as his early stardom. But across movies, Broadway and television, his manic energy rarely flagged. Rooney remained working into his 90s, still driven to "put on a show".

Here are five of Rooney's most memorable movie roles:

* 'A Midsummer Night's Dream' (1935) – The production had to be rearranged after Rooney broke his leg while skiing, enraging Warner Bros head Jack Warner. But as the mischievous sprite Puck, Rooney excelled in the dreamy film and it remains one of his finest and enchanting performances.

* 'A Family Affair' (1937) – The film that birthed Rooney's most famous role, Andy Hardy. l 'Boys Town' (1938) – Rooney shared top billing with Spencer Tracy for his performance, and won a special Juvenile Oscar for his efforts in this classic.

* 'Babes in Arms' (1939) – This big-screen version of the Broadway musical also paired Rooney and Garland. Rooney earned his first lead actor Oscar nomination for the film.

* "National Velvet" (1944) - As a former jockey (a common role for the diminutive Rooney), the actor starred opposite an 11-year-old Elizabeth Taylor in her screen debut. The adaptation of Enid Bagnold's tale was Rooney's last film before heading to war, a rare two-year gap in his otherwise constant output. (© Daily Telegraph, London)

Irish Independent