Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Peaches Geldof once said becoming a mother had turned her from an iron lady into a more emotional person.

But it did far more than that for Peaches. It changed everything.

While all mothers adore their new baby, with Peaches Geldof it went way deeper.

She appeared all-consumed by her love for Astala, who’ll turn two this month. Almost his every move was captured on camera and uploaded to Peaches’ Instagram account.

It was the same when Phaedra followed a year later, born so poignantly on Paula Yates’s birthday.

Now looking back at those Instagram pictures of the two babies laughing in the sunshine, sitting by daffodils or soundly sleeping, you can’t help but wonder if their mum wanted those moments recorded so her children could look back one day and console themselves with how much their mother loved them.

Maybe she already feared she wouldn’t be here to tell them herself .

Peaches became a poster girl for Attachment Parenting – an idea she copied from her mother – and championed the idea of bed sharing with babies, skin-to-skin contact and breastfeeding on demand.

She happily appeared on TV shows and in newspaper articles to get a kicking from those who felt her parenting style would produce a generation of brats.

Peaches didn’t care. She believed going to bed at 8pm and sleeping apart from her husband to care for her babies was the right thing to do. She was a mother now and that was her purpose in life.

Because Peaches knew far better than most how important that role was. She knew the power and security a child gains from its mother.

Because from the age of 11 she’d had no mother’s skin to touch, no maternal, unconditional love. And it had broken her.

Even before Paula’s death Peaches had been traumatised at the age of six by her parents’ divorce. Then the chaos of her mother’s domestic life in stark contrast to the discipline of her father’s can only have increased her confusion and insecurity.

Then came Paula’s death and Bob’s insistence that the family should “keep calm and carry on”.

She didn’t cry at the funeral and felt she didn’t start grieving until she was 16.

By then she was mixing with a wild set and appeared to be hurtling far too fast down the same tracks as her mother. She appeared an accident waiting to happen.

But then came the thing that seemed to avert that fate – motherhood.

Peaches once said having children ‘healed many things in me which were painful’. Certainly she hoped it had.

Looking at her agonisingly thin body and deep dark circles beneath her eyes in recent pictures it appears Peaches was still living with pain.

The demands of coping with two children under the age of two, lack of sleep and sustaining a still relatively new marriage could have only exacerbated any difficulties.

And as a new mum, the time in any woman’s life when she needs her own mother the most, she had none.

How that must have hurt.

The tragedy is Peaches knew just how hard it is to grow up without a mother – but it is now the fate that awaits Astala and Phaedra.

These dreadful similarities in the lives of Paula and Peaches must be obvious to no one more clearly than Bob Geldof. His loss must be overwhelming.

We can only hope this inheritance of tragedy ends here. And that while Astala and Phaedra must endure the terrible loss Peaches knew all too keenly, somehow they will be able to overcome it and emerge without the scars that so damaged their mother.