The world is getting used to the term "same sex," so why not "same emoji"? That's the idea behind Facebook's unveiling of some new emoji-like stickers you can share as part of the site's chat feature.

We're talking same sex couples, rainbows and LGBT-esque cartoons, as Facebook sees this as a way to make the social network more a part of LGBT Pride Month.

The White House recently announced that June is LGBT Pride Month, a celebration for same-sex couples that will be marked with parades and parties around the country.

In a statement that Facebook released regarding the launching of the LGBT stickers, the social network explained, "Facebook is celebrating Pride by adding these free Facebook Messenger stickers to the Sticker Store. We see this as one more way we can make Facebook a place where people can express their authentic identity."

There are now 19 states in the United States that allow same-sex marriages, with the most recent state being Illinois this past weekend.

"As progress spreads from state to state, as justice is delivered in the courtroom, and as more of our fellow Americans are treated with dignity and respect -- our nation becomes not only more accepting, but more equal as well. During Lesbian, Gay, Bisexual, and Transgender (LGBT) Pride Month, we celebrate victories that have affirmed freedom and fairness, and we recommit ourselves to completing the work that remains," reads a statement by President Barack Obama regarding the LGBT Pride Month announcement.

According to the most recent research conducted by the Pew Research Center there are at least 71,165 same-sex marriages in the United States. The state of Massachusetts leads the way with a reported 22,406 marriages, followed by California with approximately 18,000. New York is next at just over 12,000.

The Pew Research Center report on same-sex marriages also cited the impact these unions are having on the country.

"The growing availability of marriage to more U.S. same-sex couples has economic, as well as social and demographic, impacts," the report explained. "For instance, the Williams Institute estimated that within three years after Washington state legalized same-sex marriage in late 2012, some 9,500 in-state same-sex couples would get married, spending an aggregate $88 million (in wedding arrangements and tourism expenditures by their guests) to do so."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.