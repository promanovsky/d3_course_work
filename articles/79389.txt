Britons should eat seven portions of fresh fruit and vegetables a day, according to new research into healthy eating, published on Tuesday.

The state-run National Health Service currently recommends each person eats five 80-gramme (three-ounce) helpings of fruit and vegetables daily.

But researchers at University College London (UCL) found that eating seven portions or more could reduce the risk of dying from cancer by 25 percent and of heart disease by 31 percent compared to people who consume less than one portion of fruit or vegetables a day.

"We all know that eating fruit and vegetables is healthy, but the size of the effect is staggering," said lead author Oyinlola Oyebode of UCL's Department of Epidemiology and Public Health.

"The clear message here is that the more fruit and vegetables you eat, the less likely you are to die at any age. Vegetables have a larger effect than fruit, but fruit still makes a real difference."

However, the researchers found that eating frozen and canned fruit increased the risk of death by 17 percent compared to those who ate less than a portion a day.

In an email exchange with AFP, Oyebode cautioned that the relative benefits for Britons may not be the same in other countries with a healthier diet.

She added she was "very surprised" by the findings about frozen and canned fruit and said further research was needed to explain it.

File picture shows bananas for sale at Eastern Market on Capitol Hill in Washington, DC, on June 27, 2008 Saul Loeb, AFP/File

"This is the first time that such an association has been found," she said. The study took into account various lifestyle factors like smoking and drinking which drive up the risk of illness and early death.

"Most canned fruit contains high sugar levels and cheaper varieties are packed in syrup rather than fruit juice. The negative health impacts of the sugar may well outweigh any benefits," Oyebode said.

"Another possibility is that there are confounding factors that we could not control for, such as poor access to fresh groceries among people who have pre-existing health conditions, hectic lifestyles or who live in deprived areas."

- Diet high in fat -

UCL experts examined the dietary habits of more than 65,000 people in England between 2001 and 2008.

Britain has one of the highest rates of heart disease in Europe, with a diet high in fat and sugar considered one of the major factors.

The researchers found that seven or more portions a day can reduce a person's overall risk of death by 42 percent compared to people who manage just one portion.

People who eat between five and seven a day have a 36 percent reduced risk of death and those who eat three to five have a 29 percent decreased risk. It fell to 14 percent for those who eat one to three portions.

The study, published in the Journal of Epidemiology and Community Health, showed that fresh vegetables had the strongest effect, followed by salad and then fruit.

On Friday, US researchers reported that young women who ate eight to nine daily servings of fruit and vegetables in their 20s were 40 percent less likely to have hardening of the arteries 20 years later compared to those who ate just three or four portions a day.

However, men did not appear to benefit the way women did, raising questions about why a heart-healthy diet may benefit one gender over the other, according to the study presented at a conference of the American College of Cardiology.

The research was based on 2,508 participants in the government-sponsored Coronary Artery Risk Development in Young Adults (CARDIA) study, which began in the 1980s with the aim of tracking heart health among 18-30 year olds over time.