The world’s second largest social network has decided to display pop-up notifications on its website when someone interacts with a user’s tweets. Twitter hopes that this will make it easier for users to stay connected to what’s relevant. Whenever someone engages with a user’s tweets, they will receive real-time pop-up notifications on Twitter.com, provided that they are logged in and someone has replied, favorited or retweeted one of their tweets.

Advertising

Users will also be able to receive notifications for new followers and direct messages. The best part is that the pop-up notifications are fully interactive. Users can thus reply, retweet, favorite and even follow new people from the notification itself. This feature will be gradually rolled out to all users within the coming weeks. To ensure that its turned on, users can head to the settings page and choose the type of notifications they want to receive on Twitter.com.

Its about time that Twitter adopted in-browser notifications as well, Facebook has had them for quite a long time. This happens to be a tried and tested method of not only retaining users, but getting them to spend more time on the website. Twitter has already been testing quite a few features to let users know just how visible their tweets are, it has even tested a view counter to show how many times a particular tweet has been read by a user’s followers.

Filed in . Read more about Twitter.