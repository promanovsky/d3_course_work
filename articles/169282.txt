The Amazing Spider-Man 2 lands early in digital form for Xbox One

It’s not uncommon for Hollywood movies to get a video game as a companion when they hit theaters. The Amazing Spider-Man 2 is one of the big films hitting the theater soon that is getting a companion game for consoles and the PC. That game is coming for the PS4 and other consoles, but the Xbox One version is on indefinite hold.

That means that it won’t be coming to store shelves anytime soon, but reports indicate you can download the digital version of the game right now. The Amazing Spider-Man 2 has landed on the Xbox Games Marketplace. That marketplace for UK gamers reportedly has a broken link for purchase, but a workaround that will let gamers in the UK buy and download the game is available.

Gamers who have downloaded the game report that it works fine on the Xbox One with no major bugs noted so far that might keep the game off store shelves around the world. It remains unclear exactly why the Xbox One version of the game was put on hold. Developer of the Xbox One version, Beenox, has only said that the delay has been “frustrating.”

Gamers can still pre-order the physical version of the game at retailers like GAME and Amazon in the UK. As of now, the launch date listed for the physical Xbox One version is to be announced. The game is set to hit the PS4, PS3, Xbox 360, Wii U, 3DS, and PC on May 2. With that launch still days away, it is likely an error that has allowed early digital downloads of The Amazing Spider-Man 2 for the Xbox One. UK gamers who want to buy the digital version and have a broken link for the purchase can hit UK on the footer of the marketplace and then select United States to get a working link.

VIA: Eurogamer