Saudi Arabia's Health Ministry said Sunday that eight more people have died after contracting a lethal Middle East virus related to SARS as the kingdom grapples with a rising number of infections.

The ministry reported the deaths in a statement on its website late in the evening. It said it had detected a total of 16 cases of the Middle East respiratory syndrome coronavirus over the past 24 hours.

The latest cases bring to 102 the number of people who have died after contracting the disease in Saudi Arabia since September 2012. A total of 339 cases have been recorded to date in the kingdom, which has been the site of the bulk of confirmed infections.

Among the latest dead were a child in the capital, Riyadh, and three people in the western city of Jiddah, which has seen a spike in infections in recent weeks. The ministry also reported confirmed cases in the city of Tabuk, near the border with Jordan.

On Saturday, the ministry reported that a Saudi man died in Riyadh and another in Jiddah.

Egyptian authorities on Saturday said officials there had detected the country's first confirmed infection in a 27-year-old civil engineer who recently returned from Saudi Arabia.

Other Mideast countries that have past reported cases of infection include Jordan, Kuwait, Oman and Qatar. A small number of cases have been diagnosed in Europe and Asia.

MERS belongs to the coronavirus family that includes the common cold and SARS, or severe acute respiratory syndrome, which caused some 800 deaths globally in 2003.

There is no vaccine or cure for MERS, though not all those who contract the virus become ill. It is still unclear how it is transmitted.

Virus not mutating, not the reason for surge of cases

A German coronavirus expert says the virus responsible for the MERS infection appears not to have changed.

Dr. Christian Drosten says based on what his laboratory has seen so far, this month's surge in MERS cases cannot be explained by mutations in the virus.

Drosten's lab at the University of Bonn has been looking at genetic sequences of RNA drawn from samples from 30 recent cases from Jeddah, Saudi Arabia, where the largest increase in cases has occurred.

In an email, Drosten says the lab has sequenced three nearly full genomes and they see no signs of significant changes that could account for the increase in cases.

Earlier work on the samples showed no major changes in any of sequences, though at that point only a small part of the genome of each had been sequenced.

Drosten says the increase in cases may be due to infection control problems in hospitals where the virus has spread as well as milder cases coming forward as the public has become more aware of and concerned about MERS.

This is the first analysis of MERS viruses from cases that have occurred in 2014 and fills an important information gap.

The number of new cases has spiked sharply this month, leaving experts worried about the possibility that the virus might have become more transmissible among people.

There have been as many cases reported so far this month as in the 24 previous months combined. The earliest known cases of MERS occurred in April 2012.