Beijing: In a setback to software giant Microsoft, China Tuesday said it will prohibit Windows 8 on new government computers apparently to minimise computer security risks that may arise as the US-based firm last month ended support to Windows XP.

All desktops, laptops and tablet personal computers purchased for government use must have an operating system (OS) other than Windows 8, according to an online statement issued by the Central Government Procurement Centre.

The Chinese decision came a day after the US indictment of five Chinese military officers for alleged cyber-espionage of six American entities.

The US says the officers, belonging to Unit 61398 of the People`s Liberation Army, indulged in cyber thefts to benefit Chinese state-owned companies.

Beijing, in retaliation, has suspended the China-US Cyber Working Group.

Cyber-spying has long been a sticking point between the two largest economies, but the indictment appears to have triggered a major escalation in the dispute.

The Chinese decision to bar Windows 8 targets computers used in government offices, while the personal computer market is expected to remain unaffected, state-run Xinhua news agency reported.

Microsoft last month ended support to 13-year-old Windows XP, a major OS in China. The software giant wants users to shift to Windows 8.1 platform, or get a new PC if necessary.

The Chinese government has said it is making efforts to solve possible security risks following the Microsoft move.

"Security problems could arise because of a lack of technical support after?Microsoft?stopped providing services, making computers with XP vulnerable to hackers," Yan Xiaohong, National Copyright Administration deputy director said.

Some users have shown reluctance to move to the Windows 8 platform, citing expenses and inconvenience. Windows 8 costs 888 yuan (USD 142) in China.

"Windows 8 is fairly expensive and will increase government procurement costs," he said, adding that relevant authorities are negotiating with?Microsoftover the issue.

To help users continue using Windows 8, Chinese security providers have released specialised Self-protection products.