Asian shares higher

Investing.com - Asian stocks rose Friday in tandem with overnight gains in the U.S. after an upbeat employment report.

Japan's rose 0.5%, hitting a five-month high in initial trade. Hong Kong's Index rose 0.2%.

A thinly traded session is expected in Asia Friday ahead of the U.S. Independence Day holiday that will close both stock and bond markets there.

The June jobs report sent U.S. stocks climbing, a day after private-sector employment data beat market expectations as well.

The rose 0.54%, the index rose 0.55%, while the index rose 0.63%.

Stocks rose after the U.S. Department of Labor reported that non-farm payrolls rose by 288,000 in June, easily surpassing expectations for an increase of 212,000. May's figure was revised up to a gain of 224,000 from 217,000.

The unemployment rate ticked down to 6.1% from 6.3% in May. Analysts had expected the jobless rate to hold steady at 6.3% last month.

A day earlier payroll processor ADP reported in its nonfarm payrolls report that the U.S private sector added 281,000 jobs last month, beating expectations for an increase of 200,000.

The numbers gave stock prices a shot in the arm by fueling hopes that the U.S. economy continues to recover, which will reflect in corporate earnings going forward.

Also on Thursday, the Institute of Supply Management said its non-manufacturing purchasing managers' index fell to 56.0 in June from 56.3 in May. Analysts had expected the index to hold steady at 56.3 in June, though investors shrugged off the data.