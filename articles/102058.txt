NEW YORK (TheStreet) -- Shares of Herbalife Ltd. (HLF) - Get Report are down -5.10% to $56.79 after it was reported that the Justice Department and the FBI are investigating the multi-level marketing company that hedge fund manager Bill Ackman has alleged is a pyramid scheme, sources say, the Financial Times reports.

The criminal investigation by the FBI and U.S. attorney's office in Manhattan raises the stakes for Herbalife, which is already facing civil inquiries from multiple government agencies that are looking into the Los Angeles-based company and its associated network of independent distributors, the FT said.

Must Read: Warren Buffett's 10 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates HERBALIFE LTD as a Buy with a ratings score of B. TheStreet Ratings Team has this to say about their recommendation:

"We rate HERBALIFE LTD (HLF) a BUY. This is driven by a number of strengths, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its robust revenue growth, solid stock price performance, impressive record of earnings per share growth, increase in net income and good cash flow from operations. We feel these strengths outweigh the fact that the company has had generally high debt management risk by most measures that we evaluated."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

The revenue growth came in higher than the industry average of 9.9%. Since the same quarter one year prior, revenues rose by 19.8%. Growth in the company's revenue appears to have helped boost the earnings per share.

Investors have apparently begun to recognize positive factors similar to those we have mentioned in this report, including earnings growth. This has helped drive up the company's shares by a sharp 50.80% over the past year, a rise that has exceeded that of the S&P 500 Index. Regarding the stock's future course, although almost any stock can fall in a broad market decline, HLF should continue to move higher despite the fact that it has already enjoyed a very nice gain in the past year.

HERBALIFE LTD has improved earnings per share by 15.0% in the most recent quarter compared to the same quarter a year ago. The company has demonstrated a pattern of positive earnings per share growth over the past two years. We feel that this trend should continue. During the past fiscal year, HERBALIFE LTD increased its bottom line by earning $4.91 versus $3.95 in the prior year. This year, the market expects an improvement in earnings ($6.05 versus $4.91).

The net income growth from the same quarter one year ago has significantly exceeded that of the Personal Products industry average, but is less than that of the S&P 500. The net income increased by 10.1% when compared to the same quarter one year prior, going from $112.21 million to $123.54 million.

Net operating cash flow has increased to $195.89 million or 16.67% when compared to the same quarter last year. The firm also exceeded the industry average cash flow growth rate of -23.38%.

You can view the full analysis from the report here: HLF Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.