Box Office Preview: 'Guardians of the Galaxy' Tracking for $65M-Plus Debut

UPDATED: The Marvel and Disney tentpole will play in more than 4,080 theaters, the widest August release in history; "It's much more of a space opera than a superhero movie," says director James Gunn.

Gunning for one of the top August openings of all time in North America, Guardians of the Galaxy is expected to open to at least $65 million this weekend, a strong start for Marvel Studios' new franchise.

Some believe director James Gunn's Guardians has a shot at approaching, or crossing, $70 million, although no one is placing any bets considering the steep downturn overall at the summer box office. If it does, it would eclipse the $69.2 million domestic debut of record-holder The Bourne Ultimatum on the same weekend in 2007. The No. 2 August title is Rush Hour 2, which opened to $67.4 million in 2001.

PHOTOS Titans of Comic-Con

The 3D tentpole is opening in a raft of international markets this weekend as well, including the U.K., Russia, Mexico and Brazil. It has been tentatively approved to play in China, but no date has been set.

The weekend's other new wide offering is James Brown biopic Get On Up, from The Help director Tate Taylor and starring Chadwick Boseman as the legendary godfather of soul. Brian Grazer's Imagine Entertainment produced the $30 million film with Mick Jagger. Universal is handling the release of Get On Up, which is expected to open in the low teens.

Guardians should help stem the decline at the North American box office, where summer revenue is down more than 20 percent, one of the steepest dips ever recorded. The critically acclaimed tentpole will unspool in more than 4,080 theaters, the widest August release ever (G.I. Joe: The Rise of the Cobra played on 4,007).

Get On Up will roll out in 2,466 theaters and is likewise drawing glowing notices.

Guardians, starring a rag-tag group of unlikely galactic heroes, is the first in-house title from Marvel that isn't part of the Avengers universe and stars Chris Pratt, Zoe Saldana, Dave Bautista, Lee Pace and Bradley Cooper, who voices the role of Rocket the raccoon. Vin Diesel, Djimon Hounsou, John C. Reilly, Glenn Close and Benicio del Toro also star.

PHOTOS 35 of 2014's Most Anticipated Movies

The film's tone is much different than the Avengers franchise, and is laced with humor. "It's much more of a space opera than a superhero movie," Gunn said in a recent interview with The Hollywood Reporter.

Hype around the movie has been so high that Marvel and Disney have already announced plans for a July 28, 2017, sequel, with Gunn returning to direct.

Guardians wasn't a cheap proposition and cost $170 million to produce.