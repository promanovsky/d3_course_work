Sprint chairman Masayoshi Son is a man and every man wants to be No. 1, not No. 2 or No. 3. That said, Son is smart enough to know that Sprint’s own 4G network is currently a No. 4 when it comes to mobile data speeds, which is something he’s vowing to fix if he’s allowed to merge Sprint with T-Mobile. In an interview with Re/code’s Walt Mossberg, Son acknowledges that “of course, Sprint is horrible” but also says that wireless networks throughout the United States are similarly “horrible.”

“Every time I come here from Japan, I say, ‘Oh my God, the same iPhone, the same iPad, does not function the way it should be,” said Son, who went on to describe Sprint as America’s “miserable No. 3” network.

To bring Sprint up to speed with its competitors, Son told Mossberg that he would need to acquire T-Mobile so he could build a network with the same scale as AT&T and Verizon’s. Son also said that he has experience fixing “horrible” wireless wireless networks in his native Japan, such as when his company SoftBank bought up Vodafone Japan.

“When we acquired Vodafone Japan, it was horrible,” he said. “It was a horrible brand going south and I had to fix it. Now we have a very strong fight and our network is No. 1 in Japan… and the next generation that we are preparing is fantastic.”

Son certainly makes a very impassioned, compelling case for why his company should be allowed to acquire T-Mobile. Whether this is enough to convince skeptical federal regulators, however, is anyone’s guess.