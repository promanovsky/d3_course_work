Facebook Inc (NASDAQ:FB) said Thursday that it will once again clean up its News Feed. Just like previous changes, it will weed out “spammy” posts that users don’t want to see. The social networking giant said the latest changes will make sure that users don’t miss relevant and important stories. Facebook engineers Erich Owens and Chris Turitzin said in a blog post that the new improvements will reduce stories that users frequently mark as spam. Below is an example of the type of posts that will be eliminated.

Facebook to counter three kinds of spam

Facebook Inc (NASDAQ:FB) said many of spammy stories are published mostly by Pages that try to game the News Feed to get more likes, shares and comments than they normally would. Facebook will counter three kinds of spam: Like-baiting, spammy links, and frequently circulated content. Like-baiting is when posts ask users to like, comment or share the post to get additional distribution.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Facebook Inc (NASDAQ:FB) said the improvements will better detect such stories and make sure that they are not shown more prominently than the relevant stories from friends and other Pages. The social networking giant clarified that the changes won’t affect Pages that genuinely try to encourage discussions among their fans. Moreover, the updated News Feed won’t focus on Pages that reshare content because most users don’t find such content relevant.

No, Facebook won’t remove or reduce the number of News Feed ads

The Menlo Park-based company will also restrict stories that mislead users into clicking on links which lead to ads, or a combination of ads and frequently circulated content. However, the type or number of ads in your News Feed won’t change (because Facebook gets paid for them). The improvement comes as the social networking giant is waging a war on low-quality content.

Facebook Inc (NASDAQ:FB) shares were down 0.10% to $59.10 in pre-market trading Friday. The stock has declined more than 17% over the last one month.