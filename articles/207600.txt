Marisol Bello

USA TODAY

Sarah and Bill Thistlethwaite spent a very special Mother's Day in the nursery of Akron Children's Hospital, holding their identical twin girls for the first time two days after they were born.

For 14 weeks, the couple lived in fear and vigilance. The girls, Jenna and Jillian, were monoamniotic twins, also called mono mono twins, who had a rare and dangerous condition in which they shared the same amniotic sac and placenta. They risked being strangled by each other's umbilical cords.

The Thistlethwaite twins proved an affectionate pair who weren't going to let birth separate them. When the girls were born, they immediately reached out to each other and grabbed hands.

"First to see them, the anticipation was crazy, I was so happy," the proud papa said. "And then to see them hold hands. It was such a wave of emotion. It couldn't have been more perfect."

Their mom's reaction: "They are best friends already."

Their feel-good story has resonated around the world, leaving the couple in awe, Bill Thistlethwaite said.

On Sunday, they spent 30 minutes visiting their girls. The day capped an excruciating time that began when Sarah, 32, was 19 weeks pregnant. It was their son Jaxon's first birthday, and they had gone to the doctor's office for a third ultrasound that they hoped would tell the them the sex of the baby.

That's when they found out they were having monoamniotic twin girls who had a high risk of death.

"We went from the shock of twins to excitement to finding out the rarity and risk with this type of pregnancy and that they don't always survive," said Bill Thistlethwaite, 35. "It was nerve-wracking."

At 25 weeks, Sarah was admitted to Akron General Hospital for round-the-clock monitoring. She wore a fetal heart monitor around her belly. The only time she was not strapped to it was when she had visits from her husband and son. The threesome would walk around in the hospital, or if it was nice out they'd walk the grounds.

"We'd walk for half an hour to 45 minutes, at most an hour," Bill Thistlethwaite said. Because the twins could get entangled and suffocate in a matter of minutes, Sarah couldn't be off the heart rate monitor any longer, he said.

He said it was hard on Sarah to be away from her little boy and not be there to feed him or bathe him or put him to bed, but the family made do. They brought toys for Jaxon to play with in the hospital room.

Sarah was in the hospital for 57 days before the girls were born by a C-section. She's expecting to go home Tuesday, Bill Thistlethwaite said.

The girls are doing well, too. Jenna was born weighing 4 pounds, 3 ounces, and Jillian followed 45 seconds later at 3 pounds, 13 ounces. On Saturday, they were taken to Akron Children's Hospital, where Jenna received extra oxygen to help her breathe. Their dad says the girls are expected to stay in the hospital as long as four weeks.

He already has the perfect line for the inevitable sibling conflict: "How could you two be fighting? You came out holding hands."

But for now, he says, he's just going to admire his "two perfect little girls."