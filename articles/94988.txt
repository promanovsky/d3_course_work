Ebola could continue to spread in West Africa for months in one of the most challenging outbreaks of the disease the international community has ever faced, health experts said Tuesday.

Dr. Keiji Fukuda of the World Health Organization said that while other outbreaks have seen more cases, the current one is remarkable for the wide area over which it has spread — from Guinea's remote tropical forests to the country's capital and over the border to Liberia. The fact that it has touched the teeming Guinean capital, Conakry, is also a concern.

More than 100 deaths in Guinea and Liberia have been linked to the current outbreak, which may have begun in January, said Tarik Jasarevic, a spokesman for the World Health Organization. Because those who died early on in the outbreak will never be tested, the precise start of the outbreak will never be known, he said.

Officials got some good news this week when tests showed suspected cases in Ghana and Sierra Leone were not Ebola, the World Health Organization said. It added that two of nine suspected cases in Mali were also cleared.

Outbreak far from over

But Fukuda warned that it's too early to say whether the rate of transmission is slowing, and the outbreak is far from over.

"We fully expect to be engaged in this outbreak for another two, three, four months," Fukuda, who is the assistant director-general of the health security and environment cluster at the UN health agency, told reporters on a conference call from Geneva.

The disease incubates in people for up to 21 days, and outbreaks are generally not declared over until there have been no transmissions for at least two incubation periods.

As of Tuesday, the WHO says there have been 157 suspected or confirmed cases in Guinea, where 101 people have died. Liberia has recorded 21 cases and 10 deaths.

Fukuda said that one of the biggest challenges for health workers has been combatting the fear and rumors that have sprung up around the outbreak. An angry crowd attacked a treatment center in Guinea last week, accusing Doctors Without Borders of bringing the virus to the country.

Unknown how disease came to West Africa

He emphasized that experts know how to stop infections and that, with the right preventative measures, the risk of infection is low. But he warned that rumours — like one that says eating two raw onions a day keeps the disease away — could make it harder to stem the spread of the virus.

The disease also carries a stigma, and a Liberian official has urged the sick to seek treatment.

"The act of denial is a very critical instrument that will spread the disease wider in our country," Thomas Nagbe, director of disease control and prevention at the Liberian Health Ministry, told radio listeners.

In Guinea, the governor of Conakry said he fired a district official whom he accused of covering up a death potentially linked to Ebola and warned other officials they must report illnesses.

In addition to isolating the sick, experts, including those from the U.S. Centers for Disease Control, are looking for the source of every transmission. So far, experts have linked each case back to another known sick person, an important step toward containment, said Dr. Stephane Hugonnet, a World Health Organization medical officer who returned from Guinea this past weekend.

Experts still do not know, however, how the disease came to West Africa in the first place. While there was an Ebola case in Ivory Coast two decades ago, the current strain is different and is the kind that usually turns up in Central Africa, according to the World Health Organization.