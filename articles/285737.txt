This week, the US Department of Agriculture issued a class II voluntary recall of rib eyes from Fruitland American Meat, a Jackson, Mo. beef company. The recall involved more than 4,000 pounds of beef that may have a potential risk for Bovine Spongiform Encephalopathy (BSE), also referred to as mad cow disease.

The USDA previously included a Whole Foods distribution center in Central Connecticut that serves New England stores in its list of businesses that may have sold the recalled beef.

Today, the grocery store chain said it has official documentation stating that the 39 cattle whose meat was recalled were all under 30 months old.

Advertisement

Regulations require that cattle 30 months and older have their dorsal root ganglia removed, the branches of the cow’s central nervous system where if a cow had the virus, it could potentially spread to the meat. That procedure was not followed for the cows in question, but since each of the cattle was under 30 months old, Whole Foods said there is less of a risk that the cattle would be infected with BSE. The beef specifically in question is the bone-in “Rain Crow Ranch Ribeye’’ and quartered beef carcasses bearing establishment number 2316.

“It is unfortunate the processor could not confirm accurate information at the time, but we fully support the issuance of the USDA Class II voluntary recall to quickly address the issue out of an abundance of caution,’’ said Heather W. McCready, spokesperson for the North Atlantic Whole Foods, in an email exchange Friday afternoon.

According to the USDA, the cattle in question did not have appropriate paperwork documenting that the proper procedures had been followed to insure the safety of the beef for consumers. There have been no reports of illness or adverse reactions to the beef, but according to the USDA, the recall is currently still in place.

Advertisement

The USDA today published the full list of retailers where the recalled beef was distributed.

Here is the USDA’s official statement: