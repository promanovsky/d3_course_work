NVIDIA (NASDAQ: NVDA) reported second-quarter revenue of $1.1 billion, beating the Street's estimates and up 13 percent from the year ago period as it focused more on the cloud.

NVIDIA also guided higher, expecting revenue of $1.2 billion for the third quarter, versus expectations of $1.16 billion.

NVIDIA's net income was $128 million, or $0.22 per share, compared to $96 million, or $0.16 share, for second quarter 2013.

NVIDIA said it expects third quarter GAAP margins of 55.5 percent, also a slight bump up.

Shares of NVIDIA recently traded at $18.18, up 4.12 percent.