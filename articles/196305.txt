The proposed mega-merger of two of the world’s leading advertising holding companies -- Omnicom Group of New York and Publicis Groupe of Paris -- has collapsed.

Late Thursday, the two companies announced the termination of their proposed marriage, which had been announced with great fanfare in July.

The companies said the breakup was mutual. They blamed “difficulties in completing the transaction within a reasonable timeframe” as the reason for their decision to call it quits.

The companies reportedly encountered resistance from foreign regulators over tax issues and antitrust concerns. The market value of the combined behemoth was expected to exceed $35 billion.

Advertisement

On an earnings call two weeks ago, an Omnicom executive expressed frustration over the difficulty of the approval process, prompting broad speculation that the deal was on the verge of falling apart.

“The challenges that still remained to be overcome, in addition to the slow pace of progress, created a level of uncertainty detrimental to the interests of both groups and their employees, clients and shareholders,” Maurice Levy, chief executive of Publicis Groupe, and John Wren, chief executive of the Omnicom Group, said in a joint statement.

“We have thus jointly decided to proceed along our independent paths,” the two men said.

The proposed merger was intended to create an ad goliath with enormous clout when negotiating advertising rates and placements with television networks and Internet companies.

Advertisement

Publicis Groupe boasts such prominent advertising agencies as Leo Burnett, Saatchi & Saatchi and the prominent ad buying agency Starcom MediaVest Group.

Omnicom is the parent company of Los Angeles’ largest creative advertising agency, TBWA\Chiat\Day, as well as the New York firm BBDO Worldwide and the OMD media buying firm.

The boards of both companies have approved the termination of the transaction. Neither side will have to pay a breakup fee, the companies said.