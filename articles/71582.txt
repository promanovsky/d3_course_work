Typo, a startup that manufactures an iPhone case with a built-in keyboard -- resulting in a strangely familiar look to a BlackBerry -- was halted by a court order that banned the importation, sale and marketing of the case.

Judge William Orrick, of the United States District Court for the Northern District of California, found that BlackBerry was likely to be successful with its copyright claims against the company, according to The New York Times.

"While we are flattered by the desire to graft our keyboard onto other smartphones, we will not tolerate the deliberate use of our iconic design without proper permission," BlackBerry told the Times in an email. "We are proud of our keyboard and will vigorously protect our intellectual property."

The company, co-founded by "American Idol" host Ryan Seacrest, first unveiled the case in January at CES. Despite its obvious celebrity connection, it has not been well-received with reviewers -- with many citing the price and design -- and the company has only produced 4,000 since January, according to the court ruling.

In the initial claims, many of which Orrick rejected, Typo held that BlackBerry had little to lose, and as such, could not be harmed. On Friday, BlackBerry announced that it had lost $5.9 billion during the company's last fiscal year.

When the case had been filed in January, BlackBerry lawyers called the case design a "blatant infringement," and said that the company will vigorously protect their intellectual property.

"From the beginning, BlackBerry has always focused on offering an exceptional typing experience that combines a great design with ergonomic excellence," Steve Zipperstein, BlackBerry's general counsel, said in an earlier statement. "We are flattered by the desire to graft our keyboard onto other smartphones, but we will not tolerate such activity without fair compensation for using our intellectual property and our technological innovations."

The company accused Typo of willfully misappropriating its trade dress -- the distinctive look of the product, an attribute protected similarly trademarks. BlackBerry's design patents from 2009, 2012 and 2013 cover the look of the keyboard, according to CNET.

Typo plans to appeal in order to "continue to make and sell innovative products that busy people can't live without," according to the company's statement on Friday.

The court order will not go into effect until BlackBerry posts a bond, covering any possible damages for Typo should the ban be overturned in the near future.