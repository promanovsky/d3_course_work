Apple Inc. has approved another $30 billion in share buybacks till the end of 2015 and authorized a rarely seen seven-for-one stock split, addressing calls to share more of its cash hoard while broadening the stock's appeal to individual investors.



The company also approved a roughly 8 percent increase in its quarterly dividend to $3.29 per share.



Activist investor Carl Icahn, who had famously called on the iPhone maker to boost its buyback program, tweeted his approval of the move on Wednesday.



Shares of the company, which have remain mired around the $500-$550 range since the start of the year, jumped 7.9 percent to $566 in after-hours trading Wednesday, after falling 1.3 percent during the regular Nasdaq session.



Apple also reported sales of 43.7 million iPhones in the quarter ended March, far outpacing the roughly 38 million that Wall Street had predicted. That drove a 4.6 percent rise in revenue to $45.6 billion — a record for any non-holiday quarter — and beating Wall Street's projections for about $43.5 billion.



But whether Apple can again produce a revolutionary new product remains the central question in investors' and Silicon Valley executives' minds. The smartphone market is maturing and rivals like Samsung Electronics Co Ltd and Google Inc are taking chunks out of its mobile-device market share.



Many hope that the next iPhone, which sources have said will sport a larger screen with new display technology, will provide a timely lift to the company's bottom line come September, when Apple usually introduces the latest version of its core product.



Speculation persists that the company will take the lead in wearable devices with a smartwatch or other gadget, given that Chief Executive Officer Tim Cook has spoken about "new product categories" for 2014.



For now, the company's momentum in China and emerging markets has been the topic of much discussion in investor circles. On Wednesday, Chief Financial Officer Luca Maestri told Reuters the jump in iPhone sales was "very broad-based," but singled out greater China and Japan, where business got a boost from the recent inclusion of NTT Docomo and China Mobile as carrier partners.



Overall revenue from greater China, which includes Hong Kong and Taiwan, climbed 13 percent to $9.29 billion in the quarter. Japanese sales rose 26 percent to $3.96 billion.

© 2021 Thomson/Reuters. All rights reserved.