Nightmarish dreams, from losing your house keys to falling into a pit of spiders, may one day be a thing of the past thanks to a new study.

By applying an electrical current to a sleeping person’s brain, scientists claim they are able to induce so-called ‘lucid’ dreams.

During this type of dream, depicted in the Oscar-winning film Inception, a person becomes aware they are unconscious, and can sometimes control their visions.

Scientists believe their findings are an important leap towards understanding the connection between lucid dreaming and brain waves of a specific frequency.

To make their findings, the team built on lab studies which measured electrical activity in the brains of volunteers as they slept.

Those who took part in the experiment reported that they had experienced lucid dreams during the Rapid Eye Movement (REM) stage of sleep.

The results from electroencephalograms revealed that those dreams were accompanied by tell-tale electrical activity called gamma waves – usually linked to higher-order thinking, and awareness of one’s mental state.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Scientists led by psychologist Ursula Voss of JW Goethe-University in Frankfurt, Germany, used the outcome to hypothesise that lucid dreams could be brought on by exposing a sleeping person to gamma waves at the same frequency documented in the volunteers.

When the correct gamma wave frequency was applied, the 27 volunteers reported that they were aware that they were unconscious.

The volunteers were also able to control the plot of their dream, and said they felt as if their dream self was a third party whom they were merely observing.

While safety concerns surrounding applying electrical currents to the brain mean that dream-controlling machines are unlikely to be on sale in the near future, the technique could soon be used to treat debilitating mental illnesses.

The method may help people with post-traumatic stress disorder, who often have terrifying dreams in which they re-play a harrowing experience. If they can dream lucidly, they might be able to bring about a different outcome.

“By learning how to control the dream and distance oneself from the dream,” Voss said, PTSD patients could reduce the emotional impact and begin to recover.