Handout photos taken from YouTube footage of Joanne Milne, who was deaf since birth and having never heard a sound, as she hears for the first time

Born with the rare condition Usher Syndrome 39-year-old Joanne Milne from Gateshead has been profoundly deaf since birth, and in her mid-20s the disease also claimed her sight.

Now the incredible life-changing moment that cochlear implants allowed her to hear for the very first time has been captured on video.

The moving footage, which was filmed by her mother at the Queen Elizabeth Hospital in Birmingham, shows the moment the implants were turned on - changing her life forever.

The video shows Ms Milne breaking down as she becomes overwhelmed with emotion at being able to hear.

Ms Milne was fitted with the implants last month and faced an agonising wait after the operation before the device could be switched on and tested. The video, which has been viewed thousands of times on YouTube, shows a nurse saying the days of the week after the implants were switched on.

"The switch-on was the most emotional and overwhelming experience of my life and I'm still in shock now. The first day everybody sounded robotic and I have to learn to recognise what these sounds are as I build a sound library in my brain," she told The Journal.

"Hearing things for the first time is so, so emotional, from the ping of a light switch to running water. I can't stop crying," she added.

Usher syndrome is a rare genetic disorder associated with a mutation in any one of 10 genes resulting in a combination of hearing loss and visual impairment and is a leading cause of deafblindness.

It is incurable at present. The condition is characterized by hearing loss and gradual loss of vision.

University Hospitals Birmingham, which runs QEHB, is one of only 20 centres in the UK which carry out cochlear implants. A cochlear implant is a surgically implanted electronic device that provides a sense of sound to a person who is profoundly deaf or severely hard of hearing.

The quality of sound is less clear than that of natural hearing, but patients fitted with the implant are able to hear and understand environmental sound and and speech.

Developments in the devices mean that recipients are able to hear and enjoy music and some people are fitted with bilateral implants for hearing stereo sound.

A friend of Joanne's successfully applied for her to appear on DJ Lauren Laverne’s BBC 6music radio feature, Memory Tape.

Taking to Twitter yesterday Miss Laverne told her 300,000 followers how Ms Milne's story had moved her crew to tears.

Joanne Milne's mother, Ann, who recorded the video of the implants being switched on said: “It was just wonderful and I had tears running down my face. We all did."

“She has been deaf since birth and had never heard sounds before this."

Video courtesy of Tremayne Crossley.

Belfast Telegraph