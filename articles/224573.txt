Touch interface for Office possibly leaked

Office, Microsoft’s powerhouse enterprise solution, is likely getting a touch interface soon. Microsoft Research recently released (then retracted) documents showing a new interface for all Office utilities. It’s not clear just when we’ll see this on our various Windows powered devices, but it’s another step in Microsoft’s aim to provide a single experience across all devices.



Though Office for iPad is technically touch, it was meant as an introduction to a wider audience more than a wholly new Office product. These leaked docs focus on the desktop version, where Microsoft is dogged by touchscreen devices that just don’t make good use of Office. Seemingly codenamed “Gemini”, the documents below show a new Office for all devices and (hopefully) platforms.

Even more interesting is that the information about the screenshots note it’s used for both touch and pen interface, which could mean a lot of things. For now, it looks like a simple option for using a stylus to write notes on documents.

A Surface event tomorrow should give us more details, and we’ll be on-hand to bring you all the latest info. This could be another proof-of-concept announcement from Microsoft, essentially giving an update on their progress, or ready for prime time. We’ll hopefully know a whole lot more tomorrow.

Via: GigaOM

