Several Internet users are outraged following the reveal of Facebook's secret mood manipulation experiment it conducted in early 2012.

According to CNN Money, researchers from Cornell, the University of California, San Francisco, and Facebook conducted the experiment. The results were published in the academic journal Proceedings of the National Academy of Science.

During one week in January 2012, the data scientists altered the news feeds of nearly 700,000 users. Some people's news feeds had a higher number of positive posts, while the rest were shown more negative posts.

The study found that users were slightly more likely to post their own negative posts when shown more negative content. On the other hand, users who were shown positive content responded with more upbeat posts.

Many Facebook users are upset at the social media giant for conducting the experiment without the permission of the 690,000 users it studied. The company's terms of service, however, allow Facebook to perform experiments like this one, CNN reported.

But Adam D.I. Kramer, the Facebook researcher who designed the experiment, argued that the research wasn't meant to anger users but rather to improve the company's service.

"I can understand why some people have concerns about it, and my co-authors and I are very sorry for the way the paper described the research and any anxiety it caused," Kramer wrote in a post Sunday. "In hindsight, the research benefits of the paper may not have justified all of this anxiety."

Facebook regularly conducts similar research to "improve (its) services and to make the content people see on Facebook as relevant and engaging as possible," a company spokesman said in a statement.

"We carefully consider what research we do and have a strong internal review process," the spokesman said. "There is no unnecessary collection of people's data in connection with these research initiatives and all data is stored securely."