From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Intel processors will be used in 130 new Android and Windows tablets in 2014, Intel President Renee James announced today at the Computex computer trade show in Taiwan.

More than a dozen of those are launching at Computex, James said in her keynote speech. Those new tablets will reflect the growing diversity of the machines that handle our daily computing and communications tasks.

Intel’s No. 2 executive also said that those who are predicting the computer has hit the end of the road — because of the disruption brought by mobile devices — have always been wrong. While Apple’s Worldwide Developer Conference revealed the latest about iPhones and Macs, the Computex trade show will show us more about the future of mainstream computing devices — at least that’s Intel’s view.

Another piece of brand new technology James showed off was an Intel processor that will be the world’s first made with 14-nanometer manufacturing.

That’s a big deal because it represents another turn of the screw for Moore’s Law, the observation made by Intel Chairman Emeritus Gordon Moore that the number of transistors on a chip doubles every couple of years. Intel will soon transition to chips where the circuits are just 14 nanometers apart (a nanometer is a billionth of a meter).

“Innovation waits for nobody, and stops for nothing,” James said.

James said she wants the industry to move forward with innovating around “seamless and personal” computing experiences. She said that Intel tech would scale from smaller chips in “Internet of things” devices, where computing smarts are put into everyday objects, to high-end infrastructure for cloud computing.

“The lines between technology categories are blurring as the era of integrated computing takes hold, where form factor matters less than the experience delivered when all devices are connected to each other and to the cloud,” said James, in her speech. “Whether it’s a smartphone, smart shirt, ultra-thin 2 in 1 or a new cloud service delivered to smart buildings outfitted with connected systems, together Intel and the Taiwan ecosystem have the opportunity to accelerate and deliver the value of a smart, seamlessly connected and integrated world of computing.”

James said Intel will deliver a broad choice of system-on-a-chip and communications options for tablets and smartphones across a range of form factors, price points, and operating systems. About 35 percent of the 130 tablets coming this year will use Intel Atom processors.

Intel also announced that it is shipping a key chip in its attempt to move beyond the PC into mobile devices. James said the Intel XMM 7260 LTE-Advanced chip platform is now shipping to customers for device testing. Mobile devices with the tech are expected to appear in the months ahead.

Foxconn executive Young Liu joined James on stage to showcase 10 Intel-based tablets available now or coming soon. They include Atom processors (code named “Bay Trail” or “Clovertrail+” SoCs, including some with the LTE chips.

James said Intel plans to ship its first mobile SoC platform with integrated LTE in the fourth quarter. She made the first public phone call using a smartphone reference design based on the dual-core Intel Sofia 3G solution. Intel will launch a quad-core version in the first half of 2015. And Intel will ship a quad-core Sofia 3G version for entry-level tablets in the first half of the year.

James showed the 14-nanometer code-named Broadwell processor in a fanless mobile PC prototype from Intel. The device hybrid 2-in-1 device can be used as a laptop or a tablet. It has a 7.2 millimeter thick screen and a detachable keyboard. It weighs just 670 grams. It has a companion media dock that provides additional cooling for better performance.

Broadwell chips, which will be officially called the Intel Core M processor, will be available later this year. James said the chips are the most energy-efficient Core processors ever, and they will make possible razor-thin processors.

At the high-end of computing, Intel showed off its fourth-generation Intel Core i7 and i5 processors, with quad-core configuraitons that can run at a 4-gigahertz base frequency. Production shipments begin in June.

James also showed off the latest Intel RealSense 3D cameras, which can detect your hand gestures and facial expressions. The RealSense software development kit will be available in the third quarter.

Image Credit: Intel