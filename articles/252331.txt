The cause behind the endemic obesity problem in the U.S. is too much availability and affordability of all kinds of food, the CA: A Cancer Journal for Clinicians reveals in a report made available online on May 22.

While other researches blame several other factors such as, lack of exercise, easy access to unhealthy fast food, shortage of access to healthy foods, prepared meal sizes, suburban sprawl, affluence and poverty, for the rising cases of obesity among Americans, the latest study suggests another story.

"The high cost of healthy food may not be the problem as far as obesity is concerned, rather it is the excess availability and affordability of all types of food," Roland Sturm, PhD, study report's lead author, says in a statement.

"Not only has food been getting cheaper, but it is easier to acquire and easier to prepare. It's not just that we may be eating more high-calorie food, but we are eating more of all types of food," Sturm also says.

Along with co-author of the study [pdf], Ruopeng An, PhD, from the University of Illinois at Urbana-Champaign, Sturm went to analyze the economic factors that may contribute to obesity. They discovered weight gain was strikingly analogous across all geographic areas and socio-demographic groups, instead of being specific to particular groups, suggesting that obesity is more so propelled by environmental factors affecting not merely a few but all groups included. It affects both the more-educated group and the less-educated group, including ethnic groups.

Weight differences though were evident, the report says, based on education levels and race. Regardless of this, study discloses that all Americans have been gaining more weight at around the similar ratio in the last 25 years. The result is the same, regardless too of the seen increases in physical activity, in availability of vegetables and fruits and in leisure time.

Sturm is also a senior economist at a nonprofit research organization called RAND, whose researchers claim that the US now have the cheapest food in its history, measuring by the fraction of disposal income.

"So a smaller share of Americans' disposable income now buys many more calories," An points out.

Sturm and An claim that efficient economic policies in the prevention of obesity are still vague. While measures such as tax imposition on foods with little nutritional value could prod consumer behavior to go for healthier food choices that could be also discounted or subsidized, the report claims popular and political support for these approaches remain low.

In the U.S., there's a recent bill due for final vote on state senate next week that would require manufacturers of soda and other sugary beverages to carry a warning label. The bill is known as SB 1000 or Sugar-Sweetened Beverages Safety Warning Act, authored by state senator Bill Monning.

"We need to consider strategies that replace calorie-dense foods with fruits and vegetables, rather than just add fruits and vegetables to the diet," Sturm suggests.

Colleen Doyle, who is American Cancer Society's director of nutrition and physical activity, couldn't agree more.

"What this article reinforces is that we need to continue our research to find what combination of strategies will be most effective long term in helping all of us live healthier lives," Doyle says on the endemic obesity problem.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.