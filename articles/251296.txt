Wait, this can’t be true. This is the last episode until the show returns for the final seven next spring. Ah well, I can’t blame AMC for stretching out its last prestige series, milking it for an extra year of Emmy nominations and another season of ads. Plus, in this era of binging TV shows and getting them “on demand,” maybe having to wait isn’t a bad thing? I loved a lot about last week’s episode, as Peggy and Don found some kind of lovely, earned peace. Is it just me, or does Don seem bound to continue changing for the better?

Advertisement

The Dirty Dozen Saturday at 8 p.m., TCM

Lee Marvin, Ernest Borgnine, Charles Bronson, John Cassavetes, and Donald Sutherland star in this 1967 war film directed by Robert Aldrich. It’s about a dozen death-row convicts who are put on an assassination mission in World War II. And why am I mentioning it? Because there’s a “Mad Men” connection. A short-lived TV version in 1988 starred a Boston native named John Slattery, currently Roger Sterling on “Mad Men.”

Chrome Underground Friday at 10 p.m., Discovery

Cars are cars all over the world.

Real Time With Bill Maher Friday at 10 p.m., HBO

Last week tonight.

Diggers Friday at 9 p.m., National Geographic Channel

They dig artifacts.

Matthew Gilbert can be reached at gilbert@globe.com. Follow him on Twitter @MatthewGilbert.