This was true at any age, for women as well as for men, and regardless of other heart disease risk factors they had such as high cholesterol or diabetes, researchers found. ‘‘It might be that if someone is married, they have a spouse who encourages them to take better care of themselves,’’ said Dr. Jeffrey Berger, a preventive cardiologist at NYU Langone Medical Center in New York. But ‘‘we can’t prove by any means cause and effect,’’ he said.

Love can sometimes break a heart, but marriage seems to do it a lot of good. A study of more than 3.5 million Americans finds that married people are less likely than singles, divorced, or widowed folks to suffer any type of heart or blood vessel problem.

Advertisement

This is the largest look at marriage and heart health, said Dr. Carlos Alviar, a cardiology fellow who led the study with Berger.

Past studies mostly compared married to single people and lacked data on divorced and widowed people. Or they looked at heart attacks, whereas this one included a full range from clogged arteries and abdominal aneurysms to stroke risks and circulation problems.

Researchers used health questionnaires that people filled out when they sought various types of tests in community settings across the nation from an Ohio firm, Life Line Screening. Some of these screening tests, for various types of cancer and other conditions, are not recommended by top medical groups, but people can still get them and pay for them themselves.

The study authors have no financial ties to the firm and are not endorsing this screening, Berger said. Life Line gave its data to the Society of Vascular Surgery and New York University to help promote research.

The study results were released Friday before the American College of Cardiology’s annual meeting this weekend in Washington.

Advertisement

The results are from people who sought screening from 2003 through 2008. Their average age was 64, nearly two-thirds were female, and 80 percent were white. They gave data on smoking, diabetes, family history, obesity, exercise, and researchers had blood pressure and other health measures.

The study found:

■ Married people had a 5 percent lower risk of any cardiovascular disease compared with single people. Widowed people had a 3 percent greater risk of it and divorced people, a 5 percent greater risk, compared with married folks.

■ Marriage seemed to do the most good for those under 50; they had a 12 percent lower risk of heart-related disease than single people their age.

■ Smoking, a major heart risk, was highest among divorced people and lowest in widowed ones. Obesity was most common in those single and divorced. Widowed people had the highest rates of high blood pressure, diabetes, and inadequate exercise.

Researchers do not know how long study participants were married or how recently they were divorced or became widowed. But the results drive home the message that a person’s heart risks cannot be judged by physical measures alone — social factors and stress also matter, said Dr. Vera Bittner, of the University of Alabama at Birmingham.