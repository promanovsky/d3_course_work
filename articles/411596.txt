(Newser) – Guardians of the Galaxy—what the AP describes as a "3-D space opera" starring Chris Pratt, Zoe Saldana, and the voices of Vin Diesel and Bradley Cooper—opens today, and reviewers seem happy. As of this writing, 91% of critics on Rotten Tomatoes approve of the Marvel flick, mainly because it does what a summertime movie inspired by a comic series is supposed to: entertain, excite, and elicit a few laughs. Examples:

story continues below

Peter Travers writes at Rolling Stone that, "through dazzle and dumb luck, it turns the cliches of comic-book films on their idiot heads and hits you like an exhilarating blast of fun-fun-fun." He adds that it's "insanely, shamelessly silly" and that this is the movie where a ripped Pratt transforms into a "full-fledged movie star."

Summer and cinema metaphors as over the top as the movie abound. Tom Long writes at the Detroit News that the film "will pop your corn and leave you hungry for more," while David Hiltbrand writes at the Philadelphia Inquirer declares that Guardians "is an open fire hydrant of a summer movie: more fun than should be allowed in hot weather." Tom Huddleston writes at Time Out London that the movie is like the "box of chocolates" from Forrest Gump: "A vast, family-sized festive crate of chocolates, all wrapped in the shiniest packaging, all exploding with sweetness and surprises."

Bilge Ebiri offers more of a backhanded compliment, writing at Vulture that the "very fun" Guardians "is the class clown of the Marvel Universe," wistfully adding that, "like most class clowns … you wish it would apply itself, because it seems capable of being so much more."

And then—the naysayers. Mick LaSalle at the San Francisco Chronicle writes that the movie "feels like lots of people pumping air into a tire that has a hole in it," while Kyle Smith at the New York Post calls the movie "space junk" and makes the wince-inducing comparison of the film to sci-fi fiascos Howard the Duck and Green Lantern.

(Read more Guardians of the Galaxy stories.)