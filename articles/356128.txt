LONDON -- It may not destroy your soul, but it turns out heavy metal music can be hazardous to your brain. At least in some rare cases.

German doctors say they have treated a Motorhead fan whose headbanging habit ultimately led to a brain injury, but that the risk to metal fans in general is so small they don't need to give up the shaking.

Last January, doctors at Hannover Medical School saw a 50-year-old man who complained of constant, worsening headaches. The patient, who was not identified, had no history of head injuries or substance abuse problems but said he had been headbanging regularly for years -- most recently at a Motorhead concert he attended with his son.

After a scan, doctors discovered their patient had a brain bleed and needed a hole drilled into his brain to drain the blood. The patient's headaches soon disappeared. In a follow-up scan, the doctors saw he had a benign cyst which might have made the metal aficionado more vulnerable to a brain injury.

"We are not against headbanging," said Dr. Ariyan Pirayesh Islamian, one of the doctors who treated the man. "The risk of injury is very, very low. But I think if (our patient) had (gone) to a classical concert, this would not have happened."

Islamian said the violent shaking of the head in headbanging can sometimes be enough to cause damage as the brain bumps up against the skull and noted a handful of previous injuries, also in heavy metal fans. The latest case was described in a report published online Friday in the journal Lancet.

Motorhead is a British metal band known for helping create the "speed metal" genre, which inspires extremely fast headbanging. Islamian described the band as "one of the most hard-core rock 'n' roll acts on earth."

Doctors said headbangers shouldn't be discouraged from enjoying their favourite bands.

"There are probably other higher risk events going on at rock concerts than headbanging," noted Dr. Colin Shieff, a neurosurgeon and trustee of the British brain injury advocacy group Headway. "Most people who go to music festivals and jump up and down while shaking their heads don't end up in the hands of a neurosurgeon."

Islamian agreed heavy metal fans shouldn't necessarily skip the headbanging.

"Rock 'n' roll will never die," he said. "Heavy metal fans should rock on."