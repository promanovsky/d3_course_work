Taco Bell

Taco Bell is continuing its assault on the fast-food breakfast landscape with yet another commercial taking aim at McDonald’s—and more breakfast items in its arsenal.

Test market items that have the potential to emerge soon: More premium coffee offerings, oatmeal and even yogurt.

Restaurant News reports that during a press call, Taco Bell touted its A.M. Crunchwrap, Waffle Taco, Cinnabon Delights and coffee offerings as current bestsellers.

“We’re just getting started with breakfast,” said Chris Brandt, Taco Bell’s chief marketing officer.

The new ad that Taco Bell is rolling out takes a direct swipe at McDonald’s yet again — with its push into the breakfast arena.

Watch the ad:

Via Restaurant News:

Brandt said the latest Taco Bell breakfast television ad takes targeted aim at McDonald’s with “a wink and sense of humor.” This next salvo, he said, is “to get people’s attention, turn people’s heads and hopefully get them to turn into Taco Bell” as the brand positions itself as the “Next Generation of Breakfast.”

Sales figures on its breakfast menu items are not currently available, but the YUM brand believe that the breakfast domain is ripe for the taking.

Taco Bell has no immediate plans for a full rollout of the yogurt, oatmeal and additional coffee offerings, but Brandt told press on the call that they’ll be coming with the rest of the breakfast changes.