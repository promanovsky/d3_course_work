Obsessing over calories alone has left dieters with an empty feeling.

The calorie counting that defined dieting for so long is giving way to other considerations, like the promise of more fiber or natural ingredients. That is chipping away at the popularity of products like Diet Coke, Lean Cuisine and Special K, which became weight-watching staples primarily by stripping calories from people’s favorite foods.

Part of the problem: “Low-calorie” foods make people feel deprived. Now, people want to lose weight while still feeling satisfied. And they want to do it without foods they consider processed.

Kelly Pill has been dieting since her son was born in 1990. But the 54-year-old resident of Covina, Calif., made changes to her approach in recent years. She doesn’t eat Lean Cuisine microwavable meals as often because she doesn’t find them that filling. She also switched to Greek yogurt last year to get more protein.