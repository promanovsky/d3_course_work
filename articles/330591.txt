Nairobi: UN Secretary-General Ban Ki-moon capped off a week of high-level UN discussions on the environment by "adopting" a 6-month-old lion cub today.

The young lioness, which was found abandoned in Nairobi National Park, will be raised by the Nairobi Animal Orphanage. Ban named the cub Tumaini, which means "hope" in Kenya`s language of Kiswahili, after his "hope that all people around the world will be able to live harmoniously with nature."

"I sincerely hope this lion will grow healthy, strong and even fierce," Ban said, drawing parallels with his hopes for the environment after this week`s first UN Environment Assembly.

The assembly was the highest-level UN body ever convened on the environment. More than 1,200 participants from 193 member states spent the week in Nairobi, where the UN Environment Program is headquartered. Ban said he hopes to see

UN member states adopt a climate change deal during formal talks in Lima in December.

International environmental crime and terrorism ranked high on the assembly`s agenda. Illegal timber and products of wildlife crime such as elephant ivory and rhino horn are smuggled through the same routes as illegal weapons. The Somali militant group al-Shabab makes tens of millions of dollars a year from the illegal charcoal trade. The Lord`s Resistance Army, which US forces are helping fight across central Africa, trade in elephant ivory.

Ban warned that popular discontent and criminal activity within countries could spawn international terrorism. He urged world leaders to address terrorism comprehensively, beginning within their countries` own borders.

"United Nations, through its global counter terrorism strategy, is trying to provide necessary assistance and work with African countries," Ban said. "The political leaders should always have inclusive dialogue and inclusive policies embracing all different groups of people. That is one fundamental principle which I have been looking to the world leaders to practice."

Citing Boko Haram`s kidnapping of more than 200 Nigerian schoolgirls in April, Ban also said no country should have to handle counter terrorism efforts alone. A UN special envoy has met twice with Nigerian President Goodluck Jonathan to offer a support package to the country.