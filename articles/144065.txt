Game of Thrones Season 4 Episode 3 Preview: Watch Online HBO Go for 'Breaker of Chains' Game of Thrones Season 4 Episode 3 Preview: Watch Online HBO Go for 'Breaker of Chains'

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Game of Thrones Season 4 Episode 3 will air on Sunday night, with fans still on the edge of their seats from the dramatic opening two episodes. There are no signs that the excitement will stop anytime soon, and Episode 3, titled "Breaker of Chains" promises to be another tense episode. The new episode will air on TV on the HBO network at 9 p.m. ET.

This season of Game of Thrones has already been a huge success. The season premiere saw more than 6.6 million people tune in to watch the show in the United States.

There was not much drop as well for episode 2 last weekend, when the number of viewers was only marginally below the premiere episode.

So far with just two episodes gone in the new season, the hit HBO show has recorded its most popular episode ever (the premiere), as well as its second most popular episode ever (the second episode titled "The Lion and the Rose").

The show is a hit on TV's across the country, but also the second episode broke the Torrent Swarn record after last Sunday night's airing.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

According to reports, more than 193,000 people shared a recording of the episode through BitTorrent, and within just 24 hours of the episode airing more than 1.5 million downloads had taken place on the new episode.

Torrent Freak has reported, "A few hours after the second episode came online the Demonii tracker reported that 193,418 people where sharing one single torrent. 145,594 had a complete copy of the episode and continued to upload, while 47,824 were still downloading the file."

Last week's downloads broke the previous record, which was set by the Season 3 finale episode, which 171,572 people shared.

Tonight's new episode of Game of Thrones – Season 4 Episode 3 – titled "Breaker of Chains," will air on TV on HBO at 9 p.m. ET. The show will also be available to watch online on HBO Go for subscribers.

Here is a video preview of the promo trailer for Game of Thrones Season 4 Episode 3:

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit