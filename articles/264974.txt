Scientists say a new theoretical study suggests laser-plasma particle accelerators, which could outperform traditional particle accelerators, might be easier to construct than previously thought.

Such accelerators would use lasers to energize and accelerate electrons over extremely short distances, rather than needing the miles-long tunnels of particle accelerators like the European Large Hadron Collider that use high-power radio-frequency waves to accomplish the same thing.

A laser accelerator could accomplish similar science to the LHC's discovery of the Higgs boson in a facility no longer than a football field, the study suggests.

And it could to it for far less than the $9 billion cost of the LHC, the researchers said.

In a laser-plasma accelerator, a potent laser beam would be blasted into a cloud of floating, unattached ions and electrons -- a plasma cloud -- pushing the electrons in front of it.

"The effect is like the wake of boat speeding down a lake," says Wim Leemans, a physicist at Lawrence Berkeley National Laboratory's Berkeley Lab Laser Accelerator.

"Imagine that the plasma is the lake and the laser is the motorboat. When the laser plows through the plasma, the pressure created by its photons pushes the electrons out of the way," he says. "They wind up surfing the wake, or wake field, created by the laser as it moves down the accelerator."

As the electrons are accelerated and leave the heavier ions behind, their rapid separation would create huge electric fields up to 1,000 times more powerful that those created by conventional accelerators, the researchers said.

Although the theory is sound, there are considerable technical challenges that would have to be overcome, they admitted.

Such an accelerator would need of laser in the petawatt power range capable of delivering thousands of pulses per second.

A petawatt laser at the Berkeley lab currently has the highest repetition of any such device in the world, but it's just one pulse per second, the researchers said.

Some suggest an array of lower-powered lasers -- the lower the power, the quicker the recharge rate -- might be combined to produce one enormous pulse, but to synchronize them to all fire during one femtosecond is a huge obstacle, they said.

However, they added, is such devices could be created they would lower the cost of accelerators and make them cost effective for uses in many different scientific, medical and industrial fields.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.