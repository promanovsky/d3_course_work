Movie

Which of the three 'Star Wars' spin-offs that Disney and Lucasfilm had announced is still unknown.

May 23, 2014

AceShowbiz - Young director Gareth Edwards' name rose after directing box office champion "Godzilla" and now he's tapped to direct a "Star Wars" spin-off. Disney and Lucasfilm made the announcement on the official Star Wars website on Thursday, May 22 but kept the details a secret.

"Ever since I saw Star Wars I knew exactly what I wanted to do for the rest of my life - join the Rebel Alliance!" Edwards said in a statement after the announcement. "I could not be more excited & honored to go on this mission with Lucasfilm."

Legendary Entertainment's Thomas Tull who produced "Godzilla" also commented on the hire, saying, "Gareth's filmmaking talent makes him one of his generation's most creative and visionary directors. The plan has always been for Gareth to direct a different film before we started on another Godzilla, but who knew it would a Star Wars installment? We have a great plan in store for Godzilla fans and I am looking forward to seeing Gareth's imprint on the Star Wars universe."

Previous rumors said bounty Hunter Boba Fett, Han Solo, Luke Skywalker and Jedi master Yoda would be getting their own movies. Disney has confirmed earlier this month that at least three spin-off movies are on schedule and would be released in between the main installments.

The untitled "Star Wars" spin-off is written by "Book of Eli" scribe Gary Whitta and will be released on December 16, 2016.