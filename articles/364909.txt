Some of the biggest volcanic eruptions of the last 2,000 years have left their indelible mark deep within the pristine ice sheet of the Antarctic, a study has found.

Scientists have been able to trace the most complete history of volcanic eruptions since the birth of Christ by analysing deposits of sulphate dust in a series of ice cores drilled deep into the West Antarctic ice sheet.

The time series from 26 separate ice cores drilled out from 19 different sites shows that there were 116 volcanic eruptions in the past two millennia that were big enough to result in plumes of volcanic sulphate dust being transported as far as the South Pole.

Most the eruptions cannot be identified, however the biggest, in 1257, was already hinted at from medieval chronicles and tree rings. Scientists identified the sulphate deposits as coming from the Samalas volcano on Lombok Island of Indonesia.

The third biggest eruption, according to the ice cores, was Tambora in Indonesia, which exploded in 1815 and caused the “year without a summer” in 1816 as a result of volcanic dust in the atmosphere cutting out sunlight and lowering summer temperatures around the world. The atmospheric dust created spectacular sunsets, which were captured by the artist William Turner, and the prolonged summer rainfall forced Mary Shelley indoors where she wrote her most famous literary work, Frankenstein, or The Modern Prometheus.

The second largest eruption in terms of sulphate deposits found in the Antarctic ice cores was the explosion of the Kuwae caldera near the Pacific island of Vanuatu, which erupted in 1458. Local folklore passed on through the generations described the near total destruction of the island leaving several dead and causing many others to flee to nearby islands. The volcanic record will be used to assess the impact that volcanoes have had on the recent past in terms of affecting global temperatures, which are depressed for several years as a result of volcanic dust in the upper atmosphere reducing sunlight and causing other weather disturbances such as prolonged rainfall.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here