Since the start of the 21st century, dozens of incoming asteroids have slammed into Earth, some of them packing far more energy than a city-destroying atomic bomb, a new animation illustrates.

The visualization was released in honor of Earth Day by the B612 Foundation — an asteroid-hunting non-profit organization founded by former NASA astronauts — to highlight the alarming frequency of these extraterrestrial collisions.

The video is based on new data from a network of sensors around the globe that is designed to detect nuclear detonations and is operated by the Nuclear Test Ban Treaty Organization. Between 2000 and 2013, these instruments detected 26 explosions on the planet ranging in energy from 1 to 600 kilotons, all caused by asteroid impacts, B612 Foundation officials said. For comparison, the nuclear bomb that flattened Hiroshima in 1945 burst with the energy of 15 kilotons.

[Watch the animation of Earth's asteroid impacts]

Many of these asteroid collisions go unnoticed because they explode too high up in the atmosphere to cause damage on the ground. What's more, these impacts often occur above remote parts of the ocean. But as the new animation shows, sometimes a powerful collision occurs over an area heavily populated by humans. A throbbing red dot over Chelyabinsk, Russia marks the spot where a 600-kiloton meteor impact occurred in February 2013, damaging hundreds of buildings and injuring more than 1,000 people.

With the video, the B612 Foundation hopes to raise awareness about why Earthlings need asteroid-hunting systems.

"While most large asteroids with the potential to destroy an entire country or continent have been detected, less than 10,000 of the more than a million dangerous asteroids with the potential to destroy an entire major metropolitan area have been found by all existing space or terrestrially operated observatories," former NASA astronaut Ed Lu, who started the B612 Foundation in 2002 with fellow astronaut Rusty Schweickart and colleagues, said in a statement. "Because we don't know where or when the next major impact will occur, the only thing preventing a catastrophe from a 'city-killer' sized asteroid has been blind luck."

The video was presented at a press conference Tuesday at Seattle's Museum of Flight by Lu and two of his fellow former astronauts supporting the B612 Foundation: Tom Jones, President of the Association of Space Explorers, and Bill Anders, an Apollo 8 spaceflyer who was the first chairman of the Nuclear Regulatory Commission.

The B612 Foundation is trying to build a privately funded infrared space telescope to find dangerous asteroids when they are still millions of miles away. This Sentinel Space Telescope mission would ideally give humans years to devise a plan to deflect killer space rocks, officials with the non-profit say. The organization is aiming for a 2018 launch.