Triclosan, a chemical with antibacterial and antifungal properties, has been banned by the state of Minnesota. This makes the North Star State this first to pass a law prohibiting the additive, found in many soaps and shampoos.

Controversies over triclosan include allegations the chemical may affect hormones in the human bodies, as shown in some animal studies. There are concerns the antibiotic may contribute to increasing resistance to the drugs among bacteria. The FDA is currently reviewing the safety and effectiveness of triclosan.

The measure became law on May 16, and will go into effect at the beginning of 2017.

New Food and Drug Administration regulations will require manufacturers of antibacterial products to prove the safety and effectiveness of their goods. Without such successful testing, products will need to be reformulated, removing the chemicals, and re-labeled.

"In order to prevent the spread of infectious disease and avoidable infections and to promote best practices in sanitation, no person shall offer for retail sale in Minnesota any cleaning product that contains triclosan and is used by consumers for sanitizing or hand and body cleansing," the bill read.

Triclosan was first used as a hand cleanser in hospitals, starting in 1972. Today, it is found in a wide variety of cleaners and detergents, as well as toys, garbage bags and soaps.

Questions remain about the usefulness of triclosan, including its effects on the environment and on human health. The Material Safety Data Sheet (MSDS) for the chemical states triclosan should be treated as an eye irritant. An investigation by researchers at the University of Minnesota found the chemical is leaking into waterways, and breaking down into dioxins, which are hazardous to the environment.

"Studies have increasingly linked triclosan (and its chemical cousin triclocarban), to a range of adverse health and environmental effects from skin irritation, endocrine disruption, bacterial and compounded antibiotic resistance, to the contamination of water and its negative impact on fragile aquatic ecosystems," Beyond Pesticides, a health and environmental advocacy group, wrote on a Web page detailing the chemical.

The Minnesota law was sponsored by State Sen. John Marty, who believes the impact of its passage will be felt beyond the borders of the nation's 12th most-populous state.

"While this is an effort to ban triclosan from one of the 50 states, I think it will have a greater impact than that," Marty told the Associated Press.

Procter & Gamble has started advertising the toothpaste as being triclosan-free. The company plans to drop the agent from all its products by the end of 2014. Johnson and Johnson is scheduled to follow suit two years later.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.