If Rosie O’Donnell is indeed returning to The View, don’t expect Elisabeth Hasselbeck to be tuning in. On Wednesday, the Fox & Friends host called into her show while on vacation to comment about the rumors that the comedian was coming back to the Hot Topics table.

There is no love lost between the two TV personalities after they had a verbal disagreement on air back in 2007. The split-screen debate resulted in O’Donnell exiting the show.

On the Fox News show, Hasselbeck made it clear that she has not made up with the 52-year-old star.

She said, “What could ruin a vacation more than to hear news like this? I know Rosie very well. We worked quite closely. Talk about not securing the border!”

The 37-year-old mom of three didn’t end there. The vitriol continued to spew with Hasselbeck sharing, “Here in comes to The View the very woman who spit in the face of our military, spit in the face of her own network, and really in the face of a person who stood by her and had civilized debates for the time that she was there, coming back with a bunch of control ready to regain a seat at The View table.”

Even though the news may be surprising to fans, Hasselbeck admitted, “Not surprising. I think this has been in the works for a long time.”

She praised Whoopi Goldberg for being “the leader” on the show and that O’Donnell better behave while she’s working with the longtime moderator of the table.

Hasselbeck warned, “I don’t think it would be wise for Rosie O’Donnell to challenge Whoopi Goldberg on anything.”

It’s probably a safe bet that Elisabeth Hasselbeck won’t be invited to Rosie O’Donnell’s welcome back party.