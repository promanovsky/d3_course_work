Yum Brands' (NYSE:YUM) Taco Bell rolled out its new breakfast menu last week after a big push that included a poke at McDonald's (NYSE:MCD) and a baker's dozen of new menu items. The early morning crowds didn't exactly overwhelm the Bell, though, and the early reviews have been middling to brutal. Based on reports from my fellow Fools and my own visit to my nearest Taco Bell in Austin, the chain has a long way to go before it poses a threat at dawn to competitors like McDonald's, Starbucks (NASDAQ:SBUX), Jack in the Box (NASDAQ:JACK), and Sonic (NASDAQ:SONC). Here are four big ways Taco Bell's breakfast menu falls short:

1. The commuter-food mess factor

The messiness of a menu item may not seem like a big deal, but for commuters who are eating on the way to work, it's huge. Taco Bell's Cinnabon Delights are the analog to Sonic's Cinnasnacks, but with a twist. Rather than keep the icing on the outside for dipping, as Sonic does, Taco Bell's offerings are filled with drippy icing. They're also covered with churro-style cinnamon sugar. My test crew (aka my kids) and I ended up with icing and sugary grit all over our hands and shirts. That's fine for a lazy day at home, but not for someone headed for a client meeting or a presentation—or who likes a clean car.

The A.M. Grilled Taco presented similar issues. Advertised as "grilled for portability," this folded-over soft-tortilla breakfast taco spilled eggs and bacon as soon as it was unwrapped. Two possible solutions: more cheese for better cohesion, and traditional burrito-style folding to keep everything in place.

Mess-free breakfasting is one area where McDonald's is ahead of the game. The saving grace of items like Egg McMuffins and hash-brown patties is that they hold together well enough that customers can eat them one-handed without looking like they lost a food fight.

2. Strange menu choices

What's particularly perplexing about Taco Bell's breakfast items is how weird some of them are, as if the company made a conscious and perverse decision to avoid traditional Mexican-style breakfast dishes that customers—especially in Texas, California, and the Southwest—know and love. Migas, traditional mix-and-match breakfast tacos, and empanadas aren't hard to make, but customers won't find them on Taco Bell's breakfast menu.

Where Sonic makes breakfast work is by offering actual breakfast burritos with traditional ingredients -- Taco Bell's grilled taco seems like a failed mash-up between a breakfast taco and a cheese-free quesadilla, while the waffle taco offerings looked so wrong I couldn't even bring myself to order them.

3. Only one kind of coffee

Taco Bell offers exactly one hot caffeine option for morning customers, and while classic coffee will never go out of style, it's not a draw in a landscape where Starbucks and McDonald's offer lattes and iced coffee for commuter caffeination. Customers now expect premium coffee options and will go elsewhere if they can't find the favorites they love or the variety they want. It's no surprise, then, that McDonald's countered Taco Bell's breakfast-menu launch by giving away coffee to morning customers through April 13, to remind them of what they'd be giving up by straying to Taco Bell.

As with the food choices, it's puzzling that Taco Bell didn't roll out a Mexican-style cinnamon flavored coffee or hot chocolate option. No aguas frescas or horchata, either—cold drinks made with fresh fruit, nuts, and grains that might appeal to health-conscious commuters and hot-weather customers.

4. Inadequate hours

Considering this is the chain that launched the notion of a late-night "fourth meal," it's surprising that Taco Bell didn't just roll right on into breakfast before sunup. Taco Bell's breakfast offerings are available from 7 to 11 a.m. Starbucks, meanwhile, starts slinging java at 5 a.m., meaning that early morning commuters can down their Frappucinos and chonga bagels before the Bell even starts turning out its one coffee offering and messy grilled tacos.

At the other end of the day part, Taco Bell's 11 a.m. cutoff time is half an hour later than McDonald's, but considering how many people need a quick breakfast at 6 a.m. compared to 11 a.m., it seems Taco Bell would do better to start earlier or offer breakfast all day the way Sonic and Jack in the Box do.

The takeaway

Taco Bell just doesn't seem to be taking the breakfast market seriously. Its ad featuring guys named Ronald McDonald is clever in the same way that pre-game trash talk can be amusing. But so far the chain has over-hyped and under-delivered on its morning menu, a big mistake in a $50 billion market filled with established competitors and customers who are spoiled for choice. If Taco Bell wants to be a real player in the breakfast game, it needs to make a run back to the drawing board.