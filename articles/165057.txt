NEW YORK (TheStreet) -- In the midst of Netflix's (NFLX) - Get Report public rejection of the proposed $40 billion Comcast (CMCSA) - Get Report and Time Warner (TWC) cable merger, Netflix has announced a a new deal of its own.

According to a Washington Post report, Netflix has struck a deal with three smaller cable MSOs (multiple-system operators) to get its own "channel" of sorts on home cable TV boxes. The companies -- RCN, Grande Communications and Atlantic Broadband -- have agreed to be the first cable providers to include a special application on a number of their TiVO (TIVO) - Get Report set-top boxes so customers can stream Netflix programming.

Shares of Netflix were off more than 1% to $339.10 in premarket trading in New York.

A total of more than 500,000 customers will be covered by the new offer. Those customers will need to be both cable TV and Netflix subscribers to take advantage. Netflix will appear as another TV channel in the menu that customers could "tune" to on their cable boxes. When selected, the new Netflix application would launch.

The idea is based upon Netflix's deal with Virgin Media in the U.K. and Com Hem in Sweden, and is similar to what is currently available on separate, third-party TV streaming devices from Apple (AAPL) - Get Report TV, Google (GOOG) - Get Report Chromecast and Roku devices as well as game consoles (Microsoft's (MSFT) - Get Report Xbox, Sony (SNE) - Get Report PlayStation, Nintendo Wii) and a number of "smart TVs."

Google Fiber services up and running in Kansas City and Provo, Utah, already offer its subscribers a link to watch Netflix programming to those set-top boxes.

TiVO CEO Tom Rogers told Multichannel.com the timing was right for just such a deal.

"Our view has long been that the marriage of linear television and streaming over-the-top (OTT) TV is the future of television, and Netflix has clearly emerged as a must-have over-the-top service," Rogers said.

RCN and Atlantic Broadband will reportedly begin the new Netflix service as early as next week. Grande Communications will follow in May.

A Netflix spokesperson said the company will not benefit from any special Internet "fast-lane" treatment from its three new cable partners similar to an arrangement Netflix has -- and what Apple is reportedly negotiating -- with Comcast.

Netflix and Comcast are in the midst of a very vocal feud over whether charging for higher speed interconnections is actually "mutually beneficial" to both sides. Netflix opposes the Comcast/Time Warner merger claiming that if approved by antitrust authorities it would give Comcast too much control over the majority of U.S. high-speed Internet pipelines, as well as Netflix and other streaming providers' access to them.

-- Written by Gary Krakow in New York.

To submit a news tip, send an email to tips@thestreet.com.

Gary Krakow is TheStreet's Senior Technology Correspondent.