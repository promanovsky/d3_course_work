NEW YORK (TheStreet) -- It may give you cool points or label you a "Glasshole" but here's your chance to buyGoogle's (GOOG) - Get Report Glass, which Time Magazine hailed as one of the "Best Inventions of the Year" in 2012.

Today, there's a one-day special promotion making Google Glass available to a limited number of Average Joes. You just have to be a U.S. resident, 18 years or older, and able to fork over the $1,500 plus tax. Don't expect a price cut or special deal.

To sign up, check this Web site.

USA Today reports buyers will also get a sunglasses shade or one of Google's new prescription glasses frames with their purchase.

Besides the fact that users will be able to get news alerts, take pictures and video and even record private conversations, wearing a mini-supercomputer on your face is thought by some to be pretty cool.

Previously, the exclusive wearable technology was only made available to the 10,000 developers and contest winners. There's also a select few chosen to try out the device, called "Explorers."

Tuesday's offer is an expansion of that program. In a Google+ posting, the company says Explorers have ranged from moms, to bakers, to rockers who have all played a role in making the product better.

When you sign up, it'll ask you one simple question, "Why are you interested in Glass?"

Are you ready with your answer?

At the time of publication the author had no position in any of the stocks mentioned.

This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff.