It’s been nearly a year since Google released the Google Glass face computer, and still only a select few have had access to the thing. But that’s all about to change.

Right now (perhaps to take your mind off of taxes), Google is opening up a new batch of Google Glass units to explorers, according to an email sent out to those who had signed up for more information on the product.

You must still apply and be selected, so start thinking of why you deserve the opportunity to pay Google for one of their products.

I lied. Google is opening up Glass purchases to anyone who wants to be an Explorer, no application necessary. You will, however, need some cash or plastic to make this thing happen.

As has been the case, this new Google Glass batch carries the same price tag as before: $1,500 + tax. This includes one Glass, a charger, a “pouch”, a mono earbud, and your choice of a shade or frame.

In the email, Google reminds us that this isn’t the same piece of hardware that was shipping a year ago.

“In the past year, we’ve released nine software updates, 42 Glassware apps, iOS support, prescription frames, and more, all largely shaped by feedback from our Explorers,” the email said.

You can purchase Glass here. Now, try not to get beat up.