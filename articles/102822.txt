The U.S. government warned banks and other businesses on Friday to be on alert for hackers seeking to steal data exposed by the "Heartbleed" bug, as a German programmer took responsibility for the widespread security crisis.

On a website for advising critical infrastructure operators about emerging cyber threats, the Department of Homeland Security asked organizations to report any Heartbleed bug -related attacks, adding that hackers were attempting to exploit the bug in widely used OpenSSL code by scanning targeted networks.

(Also see: Heartbleed bug: What you need to know)

Federal regulators also advised financial institutions to patch and test their systems to make sure they are safe.

OpenSSL is technology used to encrypt communications, including access to email, as well as websites of big Internet companies like Facebook Inc, Google Inc and Yahoo Inc.

(Also see: Gmail, Facebook, Twitter, Yahoo, others: What passwords to change in Heartbleed aftermath?)

The bug, which surfaced Monday, allows hackers to steal data without a trace. No organization has identified itself as a victim, yet security firms say they have seen well-known hacking groups scanning the Web in search of vulnerable networks.

"While there have not been any reported attacks or malicious incidents involving this particular vulnerability at this time, it is still possible that malicious actors in cyberspace could exploit unpatched systems," said Larry Zelvin, director of the Department of Homeland Security's National Cybersecurity and Communications Integration Center, in a blog post on the White House website.

The German government released an advisory that echoed the one by Washington, describing the bug as "critical."

Technology companies spent the week searching for vulnerable OpenSSL code elsewhere, including email servers, ordinary PCs, phones and even security products.

Companies including Cisco Systems Inc, International Business Machines Corp, Intel Corp, Juniper Networks Inc, Oracle Corp Red Hat Inc have warned customers they may be at risk. Some updates are out, while others are still in the works.

That means some networks are vulnerable to attack, said Kaspersky Lab researcher Kurt Baumgartner.

"I have seen multiple networks with large user bases still unpatched today," he said. "The problem is a difficult one to solve."

OpenSSL software helps encrypt traffic with digital certificates and "keys" that keep information secure while it is in transit over the Internet and corporate networks.

The vulnerability went undetected for several years, so experts worry that hackers have likely stolen some certificates and keys, leaving data vulnerable to spying.

(Also see: 'Heartbleed' computer bug threat spreads to firewalls and beyond)

In their advisory, the Federal Financial Institutions Examination Council regulatory group suggested that banks consider replacing those certificates and keys.

"Financial institutions should operate with the assumption that encryption keys used on vulnerable servers are no longer viable for protecting sensitive information and should therefore strongly consider requiring users and administrators to change passwords after applying the OpenSSL patch," said the FFIEC, a consortium of regulators including the Federal Reserve and the Treasury Department.

Comodo Group, the No. 2 provider of SSL certificates, said customers have requested tens of thousands of replacements this week.

"We are very busy, but we are coping. My gut feeling is that we are going to be very busy all the way through next week," said Comodo Chief Technology Officer Robin Alden.

Taking responsibility

Robin Seggelmann, a German programmer who volunteers as a developer on the OpenSSL team, said in a blog post published on Friday that he had written the faulty code responsible for the vulnerability while working on a research project at the University of Munster.

"I failed to check that one particular variable, a unit of length, contained a realistic value. This is what caused the bug, called Heartbleed," said Seggelmann, now an employee with German telecommunications provider Deutsche Telekom AG.

He said the developer who reviewed the code failed to notice the bug, which enables attackers to steal data without leaving a trace. "It is impossible to say whether the vulnerability, which has since been identified and removed, has been exploited by intelligence services or other parties," he said.

Seggelmann said such errors could be avoided in the future if OpenSSL were to get more support from developers around the world.

OpenSSL is an open source project, which means that it is supported by developers worldwide who volunteer to update and secure its code. It is not as well tended to as programs such as Linux, which is widely supported by a flourishing developer community around the globe and corporate backers.

"OpenSSL in particular still lacks the support it needs, despite being extremely widely available and used by millions. Although there are plenty of users, there are very few actively involved in the project," Seggelmann said in a post on a Deutsche Telekom website.

© Thomson Reuters 2014