Chicago: Four young women born with abnormal or missing vaginas were implanted with lab-grown versions made from their own cells, the latest success in creating replacement organs that have so far included tracheas, bladders and urethras.

Follow-up tests show the new vaginas are indistinguishable from the women's own tissue and have grown in size as the young women, who got the implants as teens, matured.

Part of the process of engineering a vagina at Wake Forest Institute for Regenerative Medicine. Credit:Reuters

All four of the women are now sexually active and report normal vaginal function. Two of the four, who were born with a working uterus but no vagina, now menstruate normally.

It is not yet clear whether these women can bear children, but because they are menstruating, it suggests their ovaries are working, so it may be possible, said Dr Anthony Atala, director of Wake Forest Baptist Medical Centre's Institute for Regenerative Medicine in North Carolina.