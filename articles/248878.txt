An Anatolian story of resentment and seclusion called Winter Sleep took this year’s Palm d’Or Prize at the close of the 67th-annual Cannes Film Festival.

Turkish writer and director Nuri Bilge Ceylan filmed Winter Sleep over two winters in Anatolia’s dramatic and bleak Cappadocia World Heritage Site (above).

On cinematography alone, the filmmaker was hailed for how Winter Sleep captures the dramatic features and bleak nature of the setting. But what really charmed the judges and audiences is the richly drawn characters that emerge in Ceylan’s narrative-driven tale of a young hotelier couple (an isolated actor and his young newlywed) with a rocky marriage who turn their hotel into a shelter when winter comes. Along for the ride is the husband’s recently divorced sister, adding the perfect tinge of mystery and pessimism to the shadowy nowhere’s nowhere of a landscape.

Though some have criticized the three-hour, 16-minute length of Winter Sleep, this is how TIME‘s Richard Corliss describes the effect:

Winter Sleep is no chore to sit through. Most of its characters are complex and compelling, and the actors’ faces, craggy or lustrous, reward fascinated study. The movie indulges one frustrating narrative trope in too many Cannes contenders: the unexplained disappearance of a major figure more than halfway through the story, as with the Kristen Stewart character in Clouds of Sils Maria and the lawyer friend in Leviathan. But as austere soap opera or probing character study, Winter Sleep validates the viewer’s attention, if not its nearly 200-min. running time — make that ambling time.

Quentin Tarantino and his muse Uma Thurman awarded Ceylan his prize. After saying, “this is a great surprise to me” and thanking the jury, he presented his award to “the young people of Turkey, those who lost their lives during the last year.”

Tarantino presented the Sergio Leone film Per un pugno di dollari (A Fistful of Dollars) to close the festival’s ceremony.

In second place, with the jury’s Grand Prix award was The Wonders by Italian Alice Rohnwacher. That’s another character-centric tale about a beekeeping family dealing with a world that’s grown inhospitable to bees. Further down the totem were a few English-language movies: Julianne Moore won for her acting in Maps to the Stars, and the director’s award went to Bennett Miller and Foxcatcher. Leviathan‘s writers Andrey Zvyagintsev and Oleg Negin won best screenplay, and Timothy Spall won best actor in Mike Leigh’s Mr. Turner.

Go here for more Winter Sleep details and all the Cannes winners.

[Image courtesy of Wikipedia]