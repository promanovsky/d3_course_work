Privacy chiefs are awaiting an explanation from Facebook over an alleged psychological experiment which recorded users' moods as news feeds on the social network were manipulated.

The Data Protection Commission (DPC) has confirmed it has been in contact with the web giant's Dublin headquarters to ask for a comprehensive explanation of the secret test.

The question of user consent is understood to be a major issue for the Irish watchdog.

In the UK the Information Commissioner's Office is also said to be looking into how the social network and two US universities altered news feeds of almost 700,000 users and monitored their reactions.

The study was said to be designed to measure the impact of ''emotional contagion''.

A spokeswoman for Ireland's DPC said: "I can confirm that this office has been in contact with Facebook in relation to the privacy issues, including consent, of this research. We are awaiting a comprehensive response on issues raised."

The UK inquiry will look at what data protection laws, if any, may have been broken, the Financial Times reported.

The experiment, carried out in one week during January 2012 in collaboration with Cornell University and the University of California, sparked a social media furore.

It is understood the aim of the government-sponsored study was to see whether positive or negative words in messages would lead to positive or negative content in status updates.

Many users reacted angrily following online reports of the findings, which were published in the June 17 edition of the prestigious Proceedings of the National Academy of Sciences.

Some referred to it as ''creepy'', ''evil'', ''terrifying'' and ''super disturbing''.

A spokesman for the Information Commissioner's Office (ICO) said: "We're aware of this issue and will be speaking to Facebook, as well as liaising with the Irish data protection authority, to learn more about the circumstances.

Kevin Macnish, a teaching fellow and consultant in applied ethics at the University of Leeds, was asked by Liberal Democrat MP David Heath at a Commons select committee hearing to comment on the concept of informed consent.

"I do not think you have to be informed about the exact process that your data will go through, I do not think you have to be informed about all the legal aspects surrounding it," he told the science and technology select committee.

"But I do think it is reasonable to be informed about the risks and the benefits that will occur to you as a result of your data having been used.

"So, for instance, the case with Facebook that has been in the press recently over their use of the Facebook wall to manipulate emotions, or at least that is the way it has been presented, based on one word of research in their 9,400 word terms and conditions, seems not to be informed consent by anybody's stretch of the imagination.

"So I think it seems reasonable to suggest that if you are going to go into some sort of experiment like this, to be able to tell the users 'We are going to engage in an experiment, these are the harms that might occur, these are the benefits, do you consent to doing this?'.'"

Asked by an MP if this was a "bit like a health warning on a packet of cigarettes", Dr Macnish replied: "Not far off.

"Universities have been doing this with ethics research boards for quite some time. It seems quite reasonable to expect that companies should obey the same standards, the same requirements as universities do."

In response Richard Allan, of Facebook, said the company will answer any questions regulators have.

"It's clear that people were upset by this study and we take responsibility for it," he said.

"We want to do better in the future and are improving our process based on this feedback. The study was done with appropriate protections for people's information and we are happy to answer any questions regulators may have."

Facebook said the research was conducted for one week in 2012 and none of the data used was associated with a specific individual's account.

It said research is part of improving its services and making content as relevant and engaging as possible. It said understanding users' responses is a big part of this.

Facebook also said there is no "unnecessary" collection of individuals' data during its research.

The company also said any suggestion that it carried out research without users' knowledge and permission is complete fiction.

A spokesperson added: "Companies that want to improve their services use the information their customers provide, whether or not their privacy policy uses the word 'research' or not."

Facebook also set out the ground rules for its use of information, which it claimed was primarily to develop the social media network.

Online Editors