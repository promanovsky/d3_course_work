A new study from a University of Missouri doctorate student suggests that frequent Twitter use can lead to strained relationships, cheating, and divorce. In other words, all that tweeting could be ruining your relationship.

Or at least that's what the study claims, and what several hyperbolic stories reporting the findings concluded. "Tweeting this story could lead to your divorce," blared a Washington Post headline. The truth, though, is probably far more nuanced.

My skepticism lies with the methodology. After gauging how often respondents used the social networking tool, the study asked questions like, "How often do you have an argument with your significant other as a result of excessive Twitter use?" and "Have you emotionally cheated on your significant other with someone you have connected or reconnected with on Twitter?" The results: People who used Twitter more often were more likely to report Twitter-related conflict and more likely to hook up with Twitter friends.

I'm not sure why those findings should be all too surprising. You should absolutely expect that people who use Twitter more often would have more "Twitter-related conflicts" since there are simply more opportunities for those problems to arise. Same goes for cheating: If you never engage with people on Twitter, you're probably not going to hook up with anyone via Twitter either.

Moreover, those constantly glued to their iPhones are by extension leaving less time for their partners. That could certainly strain a relationship — but so too could, say, excessive-video game playing or any other such solitary behavior once it becomes pervasive.

Phrased another way, the study essentially concluded, "People who ignore their partners to spend more time interacting with other people are more likely to have problems with their partners." Jon Terbush