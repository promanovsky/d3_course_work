Dakar/Monrovia - Two more people have died of Ebola in Guinea, bringing to 61 the death toll from the deadly virus, while neighbouring Liberia reported one new case, health officials said on Wednesday.

Liberia reported 11 suspected cases and five deaths this week.

The cases were detected in the districts of Zorzor, Lofa and Foyah, near Liberia's northern border with Guinea.

Last week, Guinea's Ministry of Health registered almost 100 infections since the virus was first reported last month, noting the outbreak has reached “epidemic proportions.”