A handwritten list containing the names of Lindsay Lohan`s lovers till date has surfaced.

The 27-year-old `Mean Girls` star was allegedly fooling around with her girlfriends when she jotted down the names at the Beverly Hills Hotel on January 30 last year and later tossed the paper aside reportedly.

Lohan named 36 of her famous lovers but only some have been revealed as the blurred ones are censored for "legal reasons."

Some of the names were either married or in a relationship and the list "could rock several Hollywood relationships to the core."

"They were giggling and talking about people in the industry. It was her personal conquest list. She was trying to impress her friends with the list and then tossed it aside,reportedly.

Found among the list is Justin Timberlake who was linked to Lohan back in 2009. The list extends to Joaquin Phoenix, Lukas Haas, Paul Charles, Evan Peters, Nico Tortorella, Wilmer Valderrama, Jamie Dornan, Zac Efron, Jamie Burke, Heath Ledger, Colin Farrell, Ryan Rottman, Max George, Guy Berryman, James Franco, Adam Levine and Garrett Hedlund.

So far only Hedlund`s representative has come forward with strong denial. Lohan and Hedlund were co-stars in 2001 film `Georgia Rule`.