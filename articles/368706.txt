Newly single Sofía Vergara has moved on from her ex-fiancé Nick Loeb with “True Blood” star Joe Manganiello, according to The New York Post.



The “Modern Family” actress and hunky Manganiello, who flashes his washboard abs on the cover of People magazine’s Hollywood’s Hottest Bachelors issue, were spotted having brunch Sunday afternoon at a popular New Orleans spot.



A photo popped up on Instagram of Vergara and Manganiello huddled together at a table, a plate of fried chicken between them. The photo has since been deleted.



Vergara is in New Orleans filming the comedy “Don’t Mess With Texas” alongside Reese Witherspoon.

One source told The Post that Vergara and Manganiello met at the White House Correspondents’ Dinner in DC this year, which Vergara attended with Loeb, the creator of Onion Crunch condiments. The source says the two were “flirting” and “exchanged numbers.” Another source tells Page Six, “It is early days, but there is a big mutual attraction.”