Movie

The second 'Captain America' movie scores the top April opening of all time, beating the $86.2 million debut posted by 'Fast Five' back in 2011.

Apr 7, 2014

AceShowbiz - "Captain America: The Winter Soldier" makes a record-breaking entry at the top spot on North American box office over the first weekend of April. The comic book movie directed by the Russo brothers scores a grand debut of an estimated $96.2 million.

Although It couldn't quite match "The Avengers" and the three "Iron Man" movies, the second "Captain America" film did enough to outrank some of the stand-alone Marvel movies, including both "Thor" films and 2011's "Captain America: The First Avenger".

On top of that, the Chris Evans starrer posts the biggest April opening of all time. The $170M tentpole shatters the previous record held by Universal's fifth installment of "Fast and Furious" franchise, "Fast Five", which opened domestically in 2011 with $86.2 million.

The film that sees The Cap struggling to fit in the modern world and facing off an unlikely foe gets an A CinemaScore across all demos and stellar reviews from critics. It appeals mostly to male (64 percent) and proves to be a date movie (58 percent of audience are couples).

Overseas, "Captain America 2" arrived roughly a week ahead of its North American release. After 10 days of hitting theaters across the globe, it earned a total of $207.1 million for a worldwide gross of $303.3 million. It's only $70 million behind the $370.6 million earned by the first film in its entire run.

"Noah" follows The Cap's domination at the runner-up place, slipping one spot lower with a distant $17 million. "Divergent" rounds up the top 3 with an estimated $12 million, while "The Grand Budapest Hotel" and "Muppets Most Wanted" trail closely behind with approximately $6.3 million and a little over $6.2 million respectively.