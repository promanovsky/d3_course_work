French aerospace firms Airbus Group (EADSF.PK,EADSY.PK) and Safran (0IU8.L,SAFRF.PK) are reportedly planning a joint venture for space launchers, and the heads of the two companies are expected to meet French President Francois Hollande on Monday regarding this.

While no official reason has been given for the meeting, the discussion is expected to include mainly space.

Both companies are leading contractors on the Ariane space launcher, manufactured by Airbus Defence & Space. Safran's Herakles space propulsion unit makes solid rocket motors.

Industrial groups in Europe have been clamoring for a change in Europe's public-private system of building rockets, after the launch of the low-cost Space Exploration Technologies or SpaceX in the U.S.

Currently, government agencies design launchers, and the designs are handed over to Airbus for manufacturing. The product is given to a third party, Arianespace, for marketing. Europe plans to replace the Ariane 5 rocket launcher with an Ariane 6 by 2021.

Airbus fell 1.1 percent on Friday to close at 51.28 euros. Safran fell 1.5 percent to settle at 49.12 euros.

For comments and feedback contact: editorial@rttnews.com

Business News