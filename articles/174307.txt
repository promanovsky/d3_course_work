The Heartbleed security flaw that compromised swaths of the Internet has created surprisingly little resonance with U.S. adults, less than half of whom took action after learning of the bug, according to a new study.

The security opening, which was found in widely used, open-source code, is believed to have affected more than half a million websites, including some of the highest-trafficked destinations on the Internet like Yahoo and Google. A study by the Pew Research Center found that only 39% of Internet users took steps to protect themselves, such as changing passwords or canceling accounts.

Users, of course, cannot take action against problems they are not aware of. While there was no shortage of media coverage of Heartbleed, the study, which polled 1,501 adults, found that only 64% of Internet users had at least heard of the bug.

The numbers dwindle as the survey asked about the severity of concern. Almost one-third of users polled believe that personal information was put at risk due to the bug, with 6% adding that they think some of their info was stolen.

Password services have seen an uptick in demand as people begin to take digital security more seriously, but the Pew study highlights the challenges of spurring action on Internet security issues.

Pew added that comparatively fewer people heard about the Heartbleed bug than the situation in Ukraine or the leaks from Edward Snowden.