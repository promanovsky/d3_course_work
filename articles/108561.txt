Amazon's long rumoured smartphone is back again with fresh rumours regarding the launch of the device.

There have been rumours over quite a long time that the American tech major is working on its smartphone. Now it appears that the Amazon smartphone will arrive in the second half of this year.

According to WSJ, the company has been demonstrating early versions of its handset to developers in San Francisco and Seattle currently.

Amazon Smartphone Launch

Amazon reportedly plans to make an announcement about its smartphone by the end of June with shipment of the device to begin by the end of September, ahead of holiday season.

The mass production of the new smartphone will reportedly begin later this month, with an initial order of 600,000 units. Amazon is also in talks with selected app and software developers.

Features

Amazon intends to distinguish its smartphone in the world market. There is no much information about the technical aspects of Amazon smartphone, but the new disclosure claims its screen will be capable of displaying 3D images, without the use of special glasses.

3D Screen Technology

Additionally, the phone would feature retina-tracking technology embedded in four front-facing cameras in order to make photos appear to be 3D, similar to hologram.

The 3D screen technology is said to be capable of tracking the movement of the user's eye and move the screen accordingly. Also, the smartphone will be able to zoom automatically into images as it moves close to a user's face and could manipulate text and images along with the movement of the phone.

Such 3D technology would be ideal for gaming. The smartphone's software is believed to be optimised for visual games.

The news about the smartphone surfaced due to Amazon's move into designing and hardware making. Recently, the company launched its Fire TV set-top box and confirmed it will begin distributing a wand, which customers can use to scan product barcodes at home to re-order groceries and other goods without logging into computers. Also, it launched the new Kindle Fire tablets last year.

Pricing

There is no word so far about the pricing of the smartphone. However, the source mentioned that Amazon might alter its launch plans due to the performance or, other aspects.

Operating System

Also, there is no information available as to what operating system the device will run, though the Kindle Fire tablets and Fire TV set-top box both powered by Android.