The best women broadcasters of the last 50 years turned out today to surprise Barbara Walters on her final day as co-host of The View.

“We all have been influenced by you,” said Oprah, “And we all proudly stand on your shoulders.” Diane Sawyer, Katie Couric, Maria Shriver and more than a dozen more broadcaster journalists came on stage to honor Walters, whose half-century career inspired thousands of women to go into the profession.

Hillary Clinton also came to surprise Walters, and even did a little “surprise dance” as Whoopi Goldberg announced her as a special guest. Clinton was one of Barbara’s “Most Fascinating People” many times, and so Barbara asked her the question on everyone’s mind: Are you going to run?

“I am running… around the park,” Clinton said.

View co-host Sherri Shepherd took a different tack, asking Clinton what she wanted to be called as a grandmother when her daughter Chelsea’s baby is born. “You like Nana?” asked Shepherd, pausing a bit before adding: “You like President Clinton?”

Clinton also describes how her new book Hard Choices incorporated her personal history into her political decisions.

“You go through these experiences carrying everything you are and everything you believe in…looking at decisions that are both very personal, and decisions that are more broad, I tried to connect it to people’s lives. Because everybody has choices they have to make: becoming a mother, getting married, staying married, getting a job, keeping a job, there’s a lot of hard choices that every one of us goes through. and then nations have to make hard choices, and how they make them, and what their values are, so I try to talk about both the personal and the more international.”

You can see the full clip here:

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Charlotte Alter at charlotte.alter@time.com.