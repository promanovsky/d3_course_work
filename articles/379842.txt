Apple Inc. has struck an unlikely alliance with International Business Machines Corp. to produce business-focused apps for iPhones and iPads, a threat to BlackBerry Ltd. as it tries to refocus and target government and corporate clients.

The deal between Apple and IBM will lead to more than 100 "enterprise mobility" apps for Apple's enormously popular iPhones and iPads.

The U.S. companies, former rivals in the personal computer business, called the agreement a "landmark partnership" that will also see IBM sell Apple devices to its business clients – as well as leverage IBM's strengths in cloud computing, big data and analytics.

Story continues below advertisement

The businesss arrangement between Apple and IBM could allow the two companies to carve away even more market share from BlackBerry's former stronghold: security-conscious corporations and government departments that used to order BlackBerrys en masse from the now-struggling Waterloo, Ont.-based smartphone pioneer.

BlackBerry shares, which were up 0.7 per cent during the day, fell about 4 per cent in after-hours trading after the Apple-IBM announcement.

"I think it's going to increase their challenges, because it takes out one of the competitive responses that BlackBerry had, which was 'Apple does consumer stuff and doesn't take the enterprise seriously,' " says Frank Gillett, an analyst with Forrester Research.

Apple "didn't have the type of enterprise sales force and service support that businesses expect," Mr. Gillett continued. "But if you have IBM doing your sales and your services – how can you top that?"

For years, large corporations that previously had BlackBerry-only work forces have turned to smartphones with iOS devices from Apple, as well as phones from Samsung Electronics Co. Ltd. and other manufacturers that run Google Inc.'s Android software. As BlackBerry delayed its BlackBerry 10 phones – and financial turmoil darkened the company's reputation among big clients – even more companies began to implement "Bring your own device" (BYOD) policies that allowed employees to bring in iPhones or Android devices.

Apple chief executive officer Tim Cook said in statement on Tuesday that more than 98 per cent of Fortune 500 companies now use Apple devices.

"For the first time ever we're putting IBM's renowned big data analytics at iOS users' fingertips, which opens up a large market opportunity for Apple," Mr. Cook said. "This is a radical step for enterprise and something that only Apple and IBM can deliver."

Story continues below advertisement

Under new CEO John Chen, BlackBerry has attempted to orchestrate a renewed focus on the company's core, corporate customers, which formed the cornerstone of BlackBerry's meteoric rise in the years before the iPhone was released in 2007. The company posted better-than-expected earnings in the first quarter of this year, partly on this strategy – which relied on first reassuring corporate customers that the company wasn't about to go out of business as it outsources manufacturing and releases new smartphones. BlackBerry also targeted and won back some corporate customers who had left BlackBerry for third-party mobile device management firms, which allow companies to securely provide a mix of mobile phones to their employees.

In a statement on Tuesday, BlackBerry spokesperson Kiyomi Rutledge said the Canadian firm has long been the go-to provider of enterprise mobility services, and that corporate and government clients should not trust anything built on top of Apple's consumer-focused technology.

"The news that Apple is partnering with IBM to expand into the enterprise mobility market only underscores the ongoing need for secure end-to-end enterprise mobility solutions like those BlackBerry has delivered for years," Ms. Rutledge said. "Enterprises should think twice about relying on any solution built on the foundation of a consumer technology that lacks the proven security benefits that BlackBerry has always delivered."

BGC Financial LP analyst Colin Gillis says he wouldn't sell BlackBerry shares on the news.

"Does this really mean more competition?" asked Mr. Gillis, who is based in New York. "Apple's in the enterprise, anyway. Now there might be some more apps out there ... It shows that the space has potential, that other people want to enter it."