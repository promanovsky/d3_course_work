The major U.S. index futures are pointing to a higher opening on Tuesday, with sentiment getting a solid lift from better than expected earnings released by companies, both domestically and overseas. Meanwhile, ambassadors from EU nations are meeting today to finalize sanctions to be imposed on Russia for its tacit support to the Ukrainian rebels. With domestic home price data coming in softer than expected, traders could also focus on the consumer confidence data due shortly after the open.

U.S. stocks ended Monday's session on a narrowly mixed note amid the release of some mixed economic data and M&A news. The major averages opened little changed but declined sharply in early trading. However, the averages trimmed their losses over the course of the session and moved above the unchanged line by late afternoon trading.

The Dow Industrials and the S&P 500 Index remained mostly above the unchanged before closing slightly higher. The Dow edged up 22.02 points or 0.13 percent to 16,983, and the S&P 500 Index closed 0.57 points or 0.03 percent higher at 1,979. Meanwhile, the Nasdaq Composite retreated after the late afternoon recovery and ended down 4.65 points or 0.10 percent at 4,445.

Notwithstanding the Dow's advance, the breadth was in favor of the decliners, as 15 of the 30 Dow components closed lower and one stock ended unchanged, while the remaining 14 stocks rose. UnitedHealth (UNH), Disney (DIS) and Exxon Mobil (XOM) were among the biggest gainers of the session, while Microsoft (MSFT) ended down 1.19 percent.

Transportation and housing stocks declined sharply, while utility stocks gained ground.

On the economic front, the National Association of Realtors reported that pending home sales fell 1.1 percent month-over-month in June, with the North East and South seeing notable declines. On a year-over-year basis, pending home sales were down 4.5 percent.

Meanwhile, flash estimates revealed that Markit's non-manufacturing index for July came in at 61 compared to 61.2 in June. Despite the drop, the index came in above economist estimates for a reading of 60.0.

Commodity, Currency Markets

Crude oil futures are sliding $1.01 to $100.74 a barrel after slipping $0.42 to $101.67 a barrel on Monday. Meanwhile, gold futures are climbing $6.10 to $1,309.40 an ounce. On Monday, gold ended unchanged at $1,303.30 an ounce.

Among currencies, the U.S. dollar is trading at 102.04 yen compared to the 101.86 yen it fetched at the close of New York trading on Monday. Against the euro, the dollar is trading at $1.3416 compared to yesterday's $1.3440.

Asia

Most Asian markets ended higher amid the release of solid domestic economic data and earnings. The markets in the region also received encouragement from Wall Street's recovery. The Indian, Malaysian and Indonesian markets remained closed for public holidays.

The Japanese markets reacted to the weakness of the yen, which retreated in reaction to a rise in risk appetite. The Nikkei 225 average opened higher and advanced steadily over the course of the session before closing up 88.67 points or 0.57 percent at a new 6-month high of 15,618.

A majority of stocks advanced, led by export stocks. Domestic corporate earnings have been encouraging, increasing optimism concerning corporate profit growth. Nissan Chemical rallied 6.35 percent and was the biggest advancer among the index components.

Australia's All Ordinaries, which moved nervously back and forth across the unchanged line till late trading, advanced thereafter, ending up 10.70 points or 0.19 percent at 5,581. Most sectors advanced, with the exception of energy stocks.

Hong Kong's Hang Seng Index ended up 211.90 points or 0.87 percent at 24,641, and China's Shanghai Composite Index closed 5.24 points or 0.24 percent higher at 2,183.

On the economic front, a slew of data released by Japan was mixed. The Ministry of Internal Affairs and Communication reported that the jobless rate in Japan came in at 3.7 percent in June compared to the consensus estimate of 3.5 percent.

A separate report showed that Japanese household spending fell 3 percent year-over-year in June, a smaller drop than the 3.9 percent decline expected by economists and the 8 percent decrease in May.

Meanwhile, the Ministry of Economy, Trade and Industry said Japanese retail sales fell 0.6 percent year-over-year in June compared to the 0.5 percent drop expected by economists.

New home sales in Australia increased in June as sales of multi-unit homes surged, according to a report released by the Housing Industry Association. Home sales were up 1.2 percent month-over-month, driven by a 15.9 percent jump in multi-unit homes.

Europe

European stocks have seen considerable volatility over the course of the trading day. The major averages in the region are currently posting notable gains.

In corporate news, Swiss investment bank UBS (UBS) reported higher second quarter earnings. Meanwhile, Germany's Deutsche Bank saw its second quarter profits and sales drop.

French telecom company Orange said its second quarter earnings declined modestly, although it maintained its guidance. BP's (BP) second quarter replacement cost profit beat estimates.

Notwithstanding a decline in sales, carmaker Renault reported higher profits for its first half. Linde's first half profits declined year-over-year.

On the economic front, the German Federal Statistical Office reported that import prices in Germany were down 1.2 percent year-over-year in June, slower than the 2.1 percent decrease seen in May. The rate of decline was in line with expectations.

At the same time, export prices were down 0.1 percent in June compared with a 0.6 percent decline in May.

U.S. Economic Reports

Standard & Poor's released the results of its S&P/Case-Shiller house price survey, which showed that house prices rose an unadjusted 9.3 percent year-over-year, softer than the 9.9 percent increase expected by economists.

The Conference Board is scheduled to release the results of its consumer confidence survey for July at 10 am ET. The consensus estimate calls for an increase in the consumer confidence index to 85.5 from 85.2 in June.

The consumer confidence index rose 3 points to 85.2 in June, reaching the best level since January 2008. The present situation index rose 4.8 points to 85.1, and the expectations index was up 1.7 points to 85.2.

Stocks in Focus

Earnings

Merck (MRK) reported better than expected second quarter results and also raised its earnings guidance for 2014. Pfizer's (PFE) second quarter results were also better than expected and it affirmed its earnings per share guidance for 2014.

Jacob's Engineering (JEC) reported third quarter results that trailed estimates, and the company said it now expects its adjusted earnings for 2014 to be below the midpoint of its previous guidance range.

Aetna's (AET) second quarter results beat estimates and it also raised its full year guidance. While Wynn Resorts' (WYNN) second quarter earnings beat estimates, its revenues missed expectations.

Crane's (CR) second quarter adjusted earnings trailed estimates, while its revenues exceeded estimates. The company maintained its 2014 adjusted earnings per share guidance, which is in line with estimates. The company also announced a 10 percent increase to its quarterly divided to 30 cents per share.

Owens & Minor (OMI) reported second quarter earnings that trailed estimates, while its revenues were ahead of expectations. The company lowered its adjusted earnings per share guidance for 2014. The company also announced the promotion of COO James Bierman to the role of COO, effective September 1. The company's current CEO Craig Smith will transition to the role of executive Chairman.

Amkor (AMKR) reported below consensus earnings for its second quarter, while its revenues beat estimates. The company's third quarter guidance was in line.

XL Group (XL) reported better than expected second quarter operating income and net earned premiums. Meanwhile, Partner Re's (PRE) second quarter operating earnings trailed estimates but its revenues were ahead of expectations.

J&J Snack Foods (JJSF) reported third quarter earnings and sales that were ahead of estimates.

Plum Creek's (PCL) third quarter results exceeded estimates, while it lowered its 2014 earnings outlook below the consensus estimate.

Amdocs (DOX), Ameriprise Financial (AMP), Amgen (AMGN), Anadarko Petroleum (APC), Arthur J. Gallagher (AJG), Big 5 Sports (BGFV), Boston Properties (BXP), C.H. Robinson (CHRW), CB Richard Ellis (CBG), Compuware (CPWR), Cray (CRAY), Edward Lifesciences (EW), Equity Residential (EQR), Express Scripts (ESRX), Genworth Financial (GNW), International Game Technology (IGT), Marriott (MAR), NCR Corp. (NCR), Newmont Mining (NEM), Panera Bread (PNRA), Renaissance Re (RNR), Ruby Tuesday (RT), Twitter (TWTR) and U.S. Steel (X) are among the companies due to release their financial results after the close of trading.

Other Corporate News

Darden Restaurants (DRI) announced that its Chairman and CEO Clarence Otis is stepping down from both positions. The company also said its independent lead director Charles Ledsinger Jr. will serve as non-executive Chairman.

For comments and feedback contact: editorial@rttnews.com