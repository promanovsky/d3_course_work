Government researchers have traced 132 cases of Salmonella infection back to contact with pet bearded dragons.

The Centers for Disease Control and Prevention reported Thursday, April 24, that between Feb. 21, 2012 and April 21, 2014, 132 people have been infected with the strain of Salmonella Cotham in 31 states. The cases have been linked to contact with pet bearded dragons, which are pet lizards.

More than half of these infections occurred in children ages 5 and younger, and 42 percent of people who were infected have been hospitalized. There have been no deaths linked with this specific outbreak.

Researchers tested isolates taken from three of the cases, and found that one of the three was resistant to the antibiotic ceftriaxone.

According to the CDC, Salmonella Cotham is rare. Before this particular outbreak, there were only 25 or fewer reports of infection with this type of Salmonella a year.

To protect yourself, the CDC recommends washing hands with soap and water after touching reptiles, or areas where reptiles inhabit.