Solange Knowles Instagram Photos, Video: Pictures, of Solange and Her Son (Extended Attack Video)

Solange Knowles is Beyonce’s younger sister, and a number of people have been searching for her Instagram photos after she allegedly attacked Jay Z.

She’s also a singer and songwriter, as well as a model, and is well-known as simply Solange.

She had several stints in Destiny’s Child when she was younger before signing with Music World Entertainment, her father’s label, and releasing her first studio album at age 16 in 2003.

Knowles married Daniel Smith in 2004 and later gave birth to their son Daniel Julez Smith Jr. In 2007 the couple divorced and Solange moved to Los Angeles.

Her second studio album, titled Sol-Angel and the Hadley St. Dreams, was released in 2008 and made it to number 9 on the Billboard 2000.

Her third album was released in 2012 and was titled True. She is working on her fourth album.

Solange is in the news today because she reportedly attacked Jay-Z, Beyonce’s husband.

It’s unclear why she attacked but she smacked Jay-Z with her purse–causing the contents to spill all over the floor of the elevator–and also punched and kicked him.

A large man who appears to be a bodyguard tries to hold her back but she still manages to connect at least three times. After they walk out of the elevator, Solange and Beyonce got in one car while security walked Jay-Z to another. Representatives for all three of them haven’t responded to inquiries.

Solange Knowles performs onstage at the Vulture Festival Presents MIA + Solange on May 10, 2014 in New York City. (Brian Ach/Getty Images for New York Magazine)

Solange Knowles performs onstage at the Vulture Festival Presents MIA + Solange on May 10, 2014 in New York City. (Brian Ach/Getty Images for New York Magazine)

Solange Knowles performs onstage at the Vulture Festival Presents MIA + Solange on May 10, 2014 in New York City. (Brian Ach/Getty Images for New York Magazine)

Singers Solange (R) and Beyonce perform onstage during the 2014 Coachella Valley Music & Arts Festival on April 12, 2014 in Indio, California. (Imeh Akpanudosen/Getty Images for Coachella)

Solange considers Beyonce one of her role models but has stressed in the past that she and her sister are different.

Indeed on her second album Solange refers to the difference in her opening track.

“I’m not her and never will be/Two girls gone in different directions, striving towards the same galaxy/Let my star light shine on its own/No, I’m no sister, I’m just my God-given name,” Solange sings.

Solange Knowles attends the ‘Charles James: Beyond Fashion’ Costume Institute Gala at the Metropolitan Museum of Art on May 5, 2014 in New York City. (Dimitrios Kambouris/Getty Images)

Solange Knowles arrives at the H&M show as part of the Paris Fashion Week Womenswear Fall/Winter 2014-2015 at Le Grand Palais on February 26, 2014 in Paris, France. (Vittorio Zunino Celotto/Getty Images)

Solange Knowles attends Miu Miu Women’s Tales 7th Edition – ‘Spark & Light’ Screening – Arrivals at Diamond Horseshoe on February 11, 2014 in New York City. (Bryan Bedder/Getty Images for Miu Miu)

Solange has also addressed the issue a number of times over the years.

“People think there should be this great rivalry between us, but there’s never been any competition. There’s a big age gap and we are two very different characters,” she told the Daily Mail.

“I try to stand on my own two feet and not talk too much about my sister. But the two of us are still close.”