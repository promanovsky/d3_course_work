Auction house Sotheby's (BID) and activist investor Daniel Loeb's hedge fund Third Point LLC said Monday that they have reached an agreement to include three of Third Point's nominees to Sotheby's board of directors. The deal thus ends a proxy battle just a day before the company's scheduled annual meeting of shareholders.

Under the deal, Sotheby's board has been expanded to include Daniel Loeb, former investment banker Olivier Reza, and restructuring expert Harry Wilson. The three nominees will be included in Sotheby's slate of director nominees for election at the 2014 annual meeting of shareholders.

With these additions, Sotheby's board will expand to 15 members, 13 of whom are independent. Third Point has agreed to customary standstill and voting commitments and terminated its proxy contest, in connection with these appointments.

Sotheby's will accelerate the termination of its one-year shareholder rights plan or "poison pill", concurrent with the 2014 annual meeting.

Third Point, whose ownership stake in Sotheby's will be allowed to be raised to a maximum of 15 percent under the agreement, has withdrawn its litigation with respect to the poison pill. Third Point currently holds a 9.6 percent stake in Sotheby's.

Sotheby's slate will include John Angelo, Jessica Bibliowicz, Kevin Conroy, Domenico De Sole, The Duke of Devonshire, Daniel Loeb, Daniel Meyer, Allen Questrom, Olivier Reza, William Ruprecht, Marsha Simms, Robert Taubman, Diana Taylor, Dennis Weibling and Harry Wilson.

Sotheby's expects to convene the annual meeting on May 6, 2014 and then adjourn it to later in the month of May to allow for updated proxy solicitation materials to be distributed to the company's shareholders. The company will announce the adjourned meeting date in due course.

Bill Ruprecht, Chairman, President and Chief Executive Officer of Sotheby's said, "We welcome our newest directors to the Board and look forward to working with them, confident that we share the common goal of delivering the greatest value to Sotheby's clients and shareholders. This agreement ensures that our focus is on the and that we will benefit from five fresh voices and viewpoints."

Daniel Loeb, CEO of Third Point said, "Harry, Olivier and I are delighted to join the Sotheby's Board. As of today we see ourselves not as the Third Point Nominees but as Sotheby's directors, and we expect to work collaboratively with our fellow board members to enhance long-term value on behalf of all shareholders."

In February 2014, Third Point launched a proxy fight at Sotheby's by nominating three of its candidates, including Loeb, to stand for election to Sotheby's board of directors at the company's 2014 annual meeting.

In response, Sotheby's said at that time that it had made several efforts over the past months to reach an agreement with Loeb, and was disappointed that Third Point has chosen that path. However, Sotheby's added that its board's nominating and corporate governance committee would consider the Third Point nominations in due course.

BID is trading at $43.51, up $0.12 or 0.28 percent on a volume of 599,075 shares.

For comments and feedback contact: editorial@rttnews.com

Business News