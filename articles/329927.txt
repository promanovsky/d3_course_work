Some paraplegics could be closer to walking now that U.S. authorities approved a new exoskeleton device that will help some of them stand and walk.

The ReWalk device, which has been available in Europe since 2012, can be worn over legs to help people with certain types of injuries regain their mobility. On Thursday, the Food and Drug Administration approved marketing for the product, developed by Argo Medical.

“For the first time individuals with paraplegia will be able to take home this exoskeleton technology, use it every day, and maximize on the physiological and psychological benefits we have observed in clinical trials,” Argo Medical Chief Executive Larry Jasinski said in a public statement Thursday.

The company’s founder, Amit Goffer, started developing the device in 2001 after an accident in an all-terrain vehicle left him paralyzed.

The device has been available in Europe since 2012. Claire Lomas, a paralyzed British woman, made headlines that year when she completed the London marathon in 16 days using a ReWalk model.

Today, the model sells for $69,500, but the company is trying to work out insurance deals with various companies, according to Gizmodo.

Argo said the ReWalk is meant for people with certain types of spinal injuries, specifically between the seventh thoracic vertebra to the fifth lumbar vertebra, who still have use of their hands and shoulders. People with more severe injuries can use the device for rehabilitation purposes.