Microsoft isn't backing down in the next-gen console war just yet as the company has announced their Xbox One will be available in 26 additional markets by September.

According to a news release, those who own Microsoft's gaming system are using it an average of five hours per day. With the launch of "Titanfall" and the Twitch broadcasting feature, Xbox Live has seen a boast in its users, and naturally the company wishes to keep expanding their audience.

"Last week we saw the highest number of hours logged on Xbox Live since the launch of Xbox One," Microsoft said in a statement on Xbox Wire. "Now it's time to ensure even more fans around the world can enjoy Xbox One. So today we're proud to share that it will be available in 26 additional markets in September this year."

The consoles will be made available in the following markets:

Argentina

Belgium

Chile

Colombia

Czech Republic

Denmark

Finland

Greece

Hungary

India

Israel

Japan

Korea

Netherlands

Norway

Poland

Portugal

Russia

Saudi Arabia

Singapore

Slovakia

South Africa

Sweden

Switzerland

Turkey

UAE

Since the release of "Titanfall," Microsoft has reportedly seen its Xbox One sales double since its 2013 launch.

"Titanfall also gives a colossal boost to Xbox One hardware sales with a huge jump of over 96 percent thanks largely to the new Titanfall bundle (including a download token of the game) accounting for over 70 percent of all Xbox One hardware sold this week," Gfk reports.

The Xbox One might be closing the sales gap between in their sales competition with Sony's PlayStation 4. Sony recently announced the console has sold more than 6 million units worldwide since their big launch in 2013.

Did you purchase the "Titanfall" bundle for the Xbox One? What do you think of the new title? Let us know in the comments section below.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.