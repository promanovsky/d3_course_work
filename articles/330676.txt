A Facebook News Feed packed with negative status updates from peers is more likely to spur individual users to follow suit, a new study carried out by the social network has shown.

The company's experiment artificially tweaked the feeds of 689,003 users to show more downcast or more positive updates from their friends over the course of one week.

The results, published in the Proceedings of the National Academy of Science showed a direct correlation between the emotions conveyed in the users' own posts over that period of time.

While it's unsurprising that something sad or happy happening to a large number of close friends and acquaintances can affect our emotions, the study does offer a modicum of proof social media is affecting the mood of users, for better our worse.

The authors wrote: "In an experiment with people who use Facebook, we test whether emotional contagion occurs outside of in-person interaction between individuals by reducing the amount of emotional content in the News Feed. When positive expressions were reduced, people produced fewer positive posts and more negative posts; when negative expressions were reduced, the opposite pattern occurred."

Guinea pigs

While the study yields perhaps unsurprising results, it's somewhat unsettling that Facebook is turning users into guinea pigs for psychological experiments without proper consent.

Naturally, as Gizmodo points out, the machine-based testing is all covered under Facebook's privacy policy.

"We show, via a massive (N = 689,003) experiment on Facebook, that emotional states can be transferred to others via emotional contagion, leading people to experience the same emotions without their awareness," the study claimed.

Lovely.