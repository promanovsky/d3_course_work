Peaches Geldof's sister Fifi Trixibelle has offered an emotional tribute to the socialite following her shock death on Monday (07Apr14).

Bob Geldof's 25-year-old daughter was pronounced dead after police were called to her home in Kent, England. Her death has been described as "sudden and unexplained" but not suspicious.

As officials continue to investigate the cause of the tragedy, Geldof's devastated elder sister Fifi took to her account on Instagram.com on Tuesday (08Apr14) to post a childhood picture of the two alongside the touching message, "My beautiful baby sister... Gone but never forgotten. I love you, Peaches x"

Also on Tuesday, Bob Geldof's fellow Live Aid organiser Midge Ure recalled his shock at the tragic news and how, for him, it echoes the death of Peaches' mother Paula Yates, who passed away from an accidental drug overdose in 2000 at the age of 41.

He tells Sky News, "I think disbelief is the first sensation you get and when you check it on the Internet and television, I think the horror is just astounding. She was great, she was a bouncy, lovely child... (Paula Yates) was this 'earth mother' and that's exactly what Peaches has turned into.

"It's uncanny. The fact that Peaches posted that photograph of Paula and her together the day before it happened, and the fact that they embraced this whole motherhood thing so incredibly well after what had been maybe ropey (difficult) beginnings, they had completely found their niche in life. A tragedy to happen twice in slightly different circumstances is just beyond belief."

Peaches posted a picture of herself as a toddler in her mother's arms on her Instagram.com page just a day before she passed away.

A post mortem examination to establish the cause of death is expected to take place on Wednesday (09Apr14).