Google is rumored to be prepping an 8.9-inch Nexus 8 or Nexus 9 tablet – the name of the device is not clear yet – for launch later this summer, and Myce speculates that the tablet has been spotted online already in a Chromium issue tracker report. The device has codename “Flounder,” which is an oceanic fish, and an indication that it may be a device part of the Nexus family considering that previous Nexus devices were also named after fish internally.

While it’s not clear what company manufactures the Flounder, and other details aren’t offered about the device, the Chromium issue tracker reveals Flounder runs an AA branch build which is the development build for the Android SDK tools. Google’s Android build naming convention reveals that Flounder has an OS version made on May 6, 2014.

Myce further notes that the build fingerprint includes a “mysterious” google/volantis mention, although it’s not clear what that signifies.

Previous reports said Google will release a high-end tablet this year, with HTC rumored to make it. Furthermore, Google is expected to release an Android 4.5 update this summer, alongside new Nexus hardware.

The company could also unveil a Nexus 6 smartphone based on the LG G3 later this year, although nothing is confirmed yet. What Google said so far on the matter is that a new Nexus smartphone isn’t expected to launch in the first half of the year.

A screenshot showing the Flounder sighting and its OS version follows below.