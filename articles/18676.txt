"Two and a Half Men" has built quite a reputation for unceremoniously losing its leading actors, but unlike the infamous Charlie Sheen, former star Angus T. Jones isn't asking for his job back.

Jones, who used to play the show's young character Jake, and who has since joined the Seventh-Day Adventist church, announced in 2012 that he thought the series was "filth" and stated outright that he did not want to be on the show. "You cannot be a true God-fearing person and be on a television show like that," he said. In the video, Jones also implored viewers to quit watching "Two and a Half Men."

After the remarks went public, Jones' character was demoted from a main to a supporting role, and he saw less and less air time; Jones' character has not been featured in any episodes yet this season. The 19-year-old actor is currently attending school in Colorado and focusing on his Christian faith.

Now, Jones is speaking up about the show once again, and standing by his original words. In an interview with Houston's KHOU, Jones stated why he was so unhappy with his role on the show. "It was making light of topics in our world that are really problems for a lot of people, and I was a paid hypocrite because I wasn't okay with it, but I was still doing it," he said.

Those harsh words aside, Jones did admit that he could have been a little nicer with his original remarks because the show is so important to its creator, Chuck Lorre. "That's his, like, baby, and I just totally insulted his baby and to that degree I am apologetic," he stated, "but otherwise I don't regret saying what I said."

Jones may be done with "Two and a Half Men," but he has not necessarily left the acting spotlight altogether. He told KHOU that he would be interested in pursuing productions with religious themes and "Bible-based stories."