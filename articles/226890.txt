From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

The Chinese government has banned the use of Microsoft’s Windows 8 operating system on its computers, the country’s official news source Xinhua announced today.

The move comes after Microsoft’s long crusade to get everyone to upgrade from Windows XP to one of its newer operating systems. The company ended support and security services for Windows XP last month but allowed certain clients to continue using the operating system provided they spend millions for the extended support.

Cost may have factored into China’s decision, but the country said not using Windows 8 had to do with keeping its computers secure. It was announced under a set of energy-saving initiatives, although China didn’t state exactly how its ban on Windows 8 would be involved with that.

The government’s decision is just the latest setback for Microsoft within the country, which it has struggled to do business in for years.

Via Reuters