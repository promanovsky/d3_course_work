Earlier today, FuelFix's Rhiannon Meyers reports, A federal judge ordered U.S. Marshals to seize any barrels offloaded from a tanker carrying oil originating from Kurdistan, after the Iraqi government successfully sued to halt the shipment.

Baghdad said the crude had bypassed the state-owned oil company, which claims the right to broker all oil produced inside Iraq, Meyers says. Judge Nancy Johnson of the Southern District of Texas ruled the Iraqi government has the right to contract with the U.S. Marshals Service to take custody of any barrels that come within her court's jurisdiction, which runs nine miles into the Gulf.

It is not known who the buyer is.

Advertisement

Advertisement

And here's the zoomed out view.