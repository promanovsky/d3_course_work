David Mulroney, Canada's ambassador to China from 2009 to 2012, is a distinguished senior fellow at the Munk School of Global Affairs at the University of Toronto. He tweets @David_Mulroney

Ottawa's allegation that "a highly sophisticated Chinese state sponsored actor" targeted computers at Canada's National Research Council threw a wrench into Foreign Minister John Baird's visit to Beijing this week. We're assured that his exchange with his Chinese counterpart was "full and frank." We could use some of some of that frankness here in Canada.

This latest incident, and remember that it is hardly the first example of Chinese espionage in Canada, should prompt serious thinking about how we manage a relationship that presents such a precarious balance of risk and reward.

Story continues below advertisement

Let's start by ignoring China's sanctimonious denials, including the one Thursday from the foreign ministry accusing Canada of lacking 'credible evidence' for its most-recent allegations regarding the NRC. Although China invokes the doctrine of non-interference, it has a somewhat one-sided sense of how this works. China's industrial development strategy has long relied on spying as a means of acquiring key technologies.

Of course, China uses its long reach for objectives other than espionage. It feels free to confront any Canadian who shows undue interest in "sensitive" topics. Members of Parliaments, mayors, academics and community leaders have been bullied for displaying interest in the Dalai Lama, conditions in China's restive Xinjiang region, or the plight of Falun Gong practitioners.

This is unacceptable, but here's the hard part: we can expect more of the same. A rising but insecure China will not shrink from clandestine and downright unfriendly tactics to advance its interests.

We need to be clear-eyed in facing up to this. But we also need to recognize that our future prosperity, security and well-being depend on maintaining our own intelligently self-interested relationship with China.

So let's start by banishing the rhetoric. China is not our best friend, any more than it is the sum of all fears. We do need to acknowledge and address the real threat China poses to our security.

Government needs to lead the way, but Canadian companies also need to step up their game. Enhanced security consciousness starts at the top. There are all too many anecdotes about security minded employees being over-ruled by senior executives who are worried about offending inquisitive Chinese visitors. That exquisite sensitivity is never reciprocated when it is the turn of the Chinese to host foreign guests.

Being clear is also important. Chinese officials are not afraid of frank talk. I was summoned for finger-wagging lectures whenever the Chinese government wanted to send a tough message to Ottawa. China's ambassador to Canada typically gets the kid gloves treatment. He's a tough and experienced professional. If we're angry, he needs to feel our pain.

Story continues below advertisement

We should support multilateral efforts to reduce cyber spying, acknowledging that, in the wake of the Edward Snowden revelations, there is work to do on both sides of the ideological divide. But we are more than disinterested observers. We should step up work with allies like the U.S. and Australia in combating threats to our collective security.

The one thing that we should avoid doing is closing doors to co-operation. Unfortunately, that's already happening, and companies on both sides of the Pacific are paying a price. The Chinese media are portraying the U.S. technology sector as a major security threat. This makes it fair game for overly zealous regulators, and plays into the longstanding Chinese inclination to make life tougher for foreign firms. This week, investigators descended on Microsoft offices in China. Meanwhile the China operations of U.S.-based chip maker Qualcomm are also under review. Firms like Apple and Google have felt a similar chill.

Here in North America, China's telecom giant Huawei is our bête noir, accused of being a proxy for the Chinese security apparatus. These allegations find a ready audience among a Canadian public that, as recent polling has shown, is increasingly wary of China.

It's hard to argue against caution when it comes to China. But we're jumping from naive acceptance to complete risk avoidance. There is an intermediate step – risk mitigation. Although its approach is not without controversy, the U.K. has opted for a partnership with Huawei that sees the Chinese company funding an inspection process in Britain designed to reduce security risks.

Complete risk avoidance, or shutting our door to China, comes at a cost that falls on consumers, on smaller companies seeking access to global markets, and on communities seeking investment.

Canada's self-interested agenda with China necessarily involves a range of players in government, at universities and in the private sector. This, in turn, exposes us to a higher degree of risk than we have faced in the past. As difficult as this is becoming, we have no choice but to keep channels open.

Story continues below advertisement

There is no easy way forward. China is at the heart of changes that expose us to new levels of threat and uncertainty. We need to respond with skill, purpose and confidence. The only thing more dangerous than engaging China is not engaging it.