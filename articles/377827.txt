“This biggest and most important strategic acquisition to date in Lindt & Sprüngli’s history is a unique opportunity for us to expand our North American chocolate business and will greatly enhance the group’s status in the world’s biggest overall chocolate marketplace,” said Ernst Tanner, the Lindt chairman.

The deal to acquire Russell Stover, which is based in Kansas City, Mo, will make Lindt the third-largest chocolate manufacturer in North America. Lindt already owns the Ghirardelli Chocolate Co., whose former factory is a tourist attraction in San Francisco.

LONDON — The Swiss chocolate maker Chocoladefabriken Lindt & Sprüngli said Monday it had reached an agreement to acquire Russell Stover Candies, the US maker of boxed chocolates.

Advertisement

Terms of the deal were not disclosed.

Russell Stover was founded in 1923 in Denver by Russell and Clara Stover, who originally marketed their product under the name Mrs. Stover’s Bungalow Candies. The Stover family sold the business in 1960 to Louis L. Ward, who expanded the Russell Stover brand across the United States.

The company’s gift boxes were made famous in the film “Forrest Gump,” in which the title character, played by Tom Hanks, says “My mama always said, life was like a box of chocolates: You never know what you’re gonna get.”

Ward died in 1996, but his family continued to run the business.

Russell Stover, which acquired the Whitman’s brand of boxed chocolates in 1993, has annual revenue of about $500 million, according to Lindt. Russell Stover operates four chocolate factories in the United States, employs about 2,700 people, and has 35 retail outlets. After the transaction, Russell Stover will maintain its headquarters in Kansas City.

As a result of the deal, Lindt said it expected to reach annual revenue of $1.5 billion in North America in 2015.

Lindt, founded in 1845, reported revenue of about $3.2 billion in 2013; it employs about 9,000 people.