Paul Simon and Edie Brickell will face no further charges in their disorderly conduct case.

The married couple were charged in April following an incident at their home, in which Simon reportedly shoved Brickell and she allegedly slapped him.



Prosecutors said on Tuesday (June 17) that they will no longer pursue the case.

At the time, Brickell told officers that she had confronted the legendary singer after he had "broken her heart".

Simon was found with a small cut to his ear, while Brickell had a bruise on her wrist.

Brickell had previously told reporters: "We're fine. We love each other. We're fine. We had an argument. It's over.

"Neither one of us has any fear or any reason to feel threatened."

The charges against the singers will be deleted after 13 months.

Simon and Brickell married back in 1992 and have three children together.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io