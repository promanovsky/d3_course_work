Top Consumer Shares:

WMT: flat

MCD: flat

DIS: -0.05%

CVS: flat

KO: -0.37%

GE: -0.33%

Consumer shares were flat to lower in pre-market trade on Monday.

In consumer stocks news, Hillshire Brands ( HSH ) said it has formally withdrawn its offer to acquire Pinnacle Foods ( PF ) after Tyson ( TSN ) Foods disclosed a $63-per-share proposal to acquire it.

And, Star Bulk Carriers ( SBLK ) said it agreed to buy Oceanbulk Shipping and Oceanbulk Carriers for 54.10 million in stock to create the largest U.S-listed dry bulk carrier.

Starbulk was up 1.8% after an earlier halt. The stock closed at $12.07 on Friday, giving the transaction a value of about $653 million based on that market price.

Finally, International Game Technology ( IGT ) says it is looking at its strategic options. Options include but are not limited to business combinations, changes to our capital structure and adjustments to our portfolio of businesses, with the goal of maximizing shareholder value.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.