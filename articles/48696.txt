Lori Grisham

USA TODAY Network

Facebook announced Tuesday the purchase of Oculus, a virtual-reality goggle start-up, for $2 billion. Here's a list of notable Facebook acquisitions that have made a few start-ups very rich:

Company: Oculus

Founder: Palmer Luckey, Brendan Iribe, Michael Antonov, Nate Mitchell, and John Carmack

Price: $2 billion

Acquired: March 2014

Details: The Oculus VR, a virtual-reality goggle headset, was 21-year-old Palmer Luckey's brainchild and it garnered attention from veterans in the tech field. Oculus got its initial funding from a Kickstarter campaign launched in September 2012 where it raised $2.4 million.

"Mobile is the platform of today, and now we're also getting ready for the platforms of tomorrow," Facebook's company blog quotes founder and CEO Mark Zuckerberg as saying.

Company: WhatsApp

Founders: Brian Acton and Jan Koum

Price: $19 billion

Acquired: February 2014

Details: Former Yahoo engineers Koum, 38, and Acton, 42, started WhatsApp in 2009. The messaging service allows users to send texts, photos and videos to contacts without carrier fees. One of the things that made WhatsApp appealing to Facebook is its popularity internationally. It has a global base of 450 million monthly users and is adding more than 1 million new users each day. Their Facebook deal was the largest buyout ever for a venture-backed company.

Company: Face.com

Founders: Gil Hirsch, Yaniv Taigman, Moti Shniberg, Eden Shochat

Price: Financial terms not disclosed. Estimated to be between $55 to $60 million, according to TechCrunch.

Acquired: June 2012

Details: The Israeli start-up, formed by Gil Hirsch and fellow techies in 2007, created facial recognition software that automatically identified people in photos based on the individual's facial features. Facebook used the software in many of its photo apps prior to purchasing the company in 2012, according to CNN.

Company: Instagram

Founders: Kevin Systrom and Mike Krieger

Price: $1 billion

Acquired: April 2012

Details: Stanford fellows Systrom was 27 and Krieger was 25 when Facebook bought their photo-sharing app Instagram. The app allows users to apply filters to photos and share them with location specific tags. It launched in 2010 and roughly 30 million Apple users had downloaded the app by the time Facebook purchased it.

Instagram continues to function today largely as it did back in 2012, but with more users and some upgrades. Instagram and Facebook agreed to run the product as an independent company while also working to integrate its features with Facebook.

Company: FriendFeed

Founders: Bret Taylor, Paul Buchheit, Jim Norris, and Sanjeev Singh

Price: Financial terms were not disclosed. Reported to be roughly $50 million by the Wall Street Journal.

Acquired: August 2009

Details: Four former Google employees teamed up to launch FriendFeed in 2007. Taylor and Norris helped program Google Maps, and Buchheit and Singh were on the original Gmail software team, according to TheNew York Times. Buchheit is credited with creating Google's now-famous slogan, "Don't be evil."

FriendFeed allowed users to share content across multiple social networks at once and also view what their friends were doing on various platforms. There was some crossover on Facebook prior to the company formally purchasing FriendFeed. For example, the "like" option existed on FriendFeed before Facebook introduced a similar feature, according to a 2009 TechCrunch article.

Follow @lagrisham on Twitter.