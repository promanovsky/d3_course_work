GENEVA -- Texas blues legend Johnny Winter emblazoned himself into the world's consciousness with his tattooed arms churning out lightning-fast guitar riffs and his striking long white hair flowing from under his cowboy hat.

His contrasting appearance and devotion to the blues pioneers of the 20th century turbo-charged a career in which he emulated and, ultimately, championed, his childhood hero Muddy Waters and other icons. Winters carved out a wide niche -- and became an icon himself -- starting in the late 1960s and 1970s with a sound that blues and country singer Tracy Nelson, prominent during the same era with her band Mother Earth, described as "Texas second generation."

"He did not overplay, like a lot of white blues guitarists," she said of Winter, who collaborated with the likes of Waters, John Lee Hooker and Jimi Hendrix. "His tone was a little more modern, more electric, but I could see the influences. He stayed faithful. People idolized him."

Winter's representative, Carla Parisi, confirmed Thursday that he died in a hotel room just outside Zurich a day earlier at age 70. The statement said his wife, family and bandmates were all saddened by the loss of one of the world's finest guitarists.

The cause of death was unclear and authorities have ordered an autopsy, said Zurich police spokeswoman Cornelia Schuoler. She said investigators are mainly looking at "medical causes" and there is no indication that anyone else was involved.

Winter was a leading light among the white blues guitar players, including Eric Clapton and the late Stevie Ray Vaughan, who followed in the footsteps of the earlier Chicago blues masters. He idolized Waters -- and got a chance to produce some of the blues legend's more popular albums. Rolling Stone magazine named Winter one of the top 100 guitarists of all time.

Music writer Fred Schruers said Winter played a major role in introducing the blues to a new audience.

"The real legacy of Johnny Winter is that he brought the blues to an audience in tie-dye that might otherwise have neglected the entire genre -- and his timely work producing Muddy Waters only deepened that contribution," said Schruers, author of an upcoming biography of Billy Joel.

Winter had been on an extensive tour this year to celebrate his 70th birthday. His last performance was on Saturday at the Lovely Days Festival in Wiesen, Austria.

He had recently announced that he would follow up his 2011 album "Roots" with a new studio album, "Step Back," in September featuring collaborators such as Eric Clapton, Ben Harper, Joe Perry, Dr. John and Joe Bonnamassa.

John Dawson Winter III was born on Feb. 23, 1944 and raised in Beaumont, Texas. He was the older brother of Edgar Winter, who like him was an albino, and rose to musical fame with the Edgar Winter Group.

"Made my first record when I was 15, started playing clubs when I was 15. Started drinking and smoking when I was 15. Sex when I was 15. Fifteen was a big year for me," Winter recalled with a laugh in a documentary released this year, "Johnny Winter: Down & Dirty."

"I love playing guitar. It's the only thing I've ever really been great at," he said.

His career received a big boost early on when Rolling Stone singled him out as one of the best blues guitarists on the Texas scene. This helped secure a substantial recording contract from Columbia Records in 1969 that led to an appearance at the Woodstock Festival and gave him a wide following among college students and young blues fans.

He was one of the most popular live acts of the early 1970s, when his signature fast blues guitar solos attracted a wide following. Crowds were dazzled by the speed -- and volume -- of his guitar playing, which had its roots in urban blues but incorporated elements of rock 'n'roll. But his addiction problems with heroin during that decade and later battles with alcohol and prescription medication also drew attention.

Winter performed often with blues and rock singer Janis Joplin and the two became close during the 1960s.

Among the blues classics that Winter played during that era were "Rollin' and Tumblin'," "Bad Luck and Trouble" and "Good Morning, Little Schoolgirl." He also teamed up with his brother Edgar for their 1976 live album "Together."

He was inducted into the Blues Foundation Hall of Fame in 1988, and his devotion to the music never wavered.

"To me, the blues has more emotion in it than any other music I've ever heard," Winter told Guitar World. "You can tell that the people that sing and play the blues mean what they are saying."