Walgreen Co. (WAG) said that its net earnings attributable to the company for the fiscal 2014 second quarter declined to $754 million or $0.78 per share, from $756 million or $0.79 per share in the year-ago quarter.

Adjusted fiscal 2014 second quarter net earnings dropped to $880 million or $0.91 per share, from $915 million or $0.96 per share in the year-ago quarter. Analysts polled by Thomson Reuters expected the company to report earnings of $0.93 per share for the quarter. Analysts' estimates typically exclude special items.

This year's second quarter earnings adjustments had a net positive impact of $126 million or $0.13 per share. Last year's second quarter earnings adjustments had a net positive impact of $159 million or $0.17 per share.

But, net sales for the quarter grew to $19.605 billion from $18.647 billion in the prior year. Eighteen analysts estimated revenues of $19.61 billion for the quarter.

Front-end comparable store sales, those open at least a year, increased 2.0 percent in the second quarter, customer traffic in comparable stores decreased 1.4 percent and basket size increased 3.4 percent, while total sales in comparable stores increased 4.3 percent.

The combined synergies for Walgreens and its strategic partner, Alliance Boots, in the first half of fiscal 2014 were approximately $236 million. The joint synergy program is now estimated to deliver second-year combined synergies of $375-$425 million, an increase from the previous second-year estimate of $350-$400 million.

Alliance Boots contributed 8 cents per diluted share to Walgreens second quarter 2014 adjusted results. The company estimates that the accretion from Alliance Boots in the third quarter of fiscal 2014 will be an adjusted 13 to 14 cents per share. This estimate does not include amortization expense, the impact of AmerisourceBergen warrants or one-time transaction costs, and reflects the company's current estimates of IFRS to GAAP conversion and foreign exchange rates.

For comments and feedback contact: editorial@rttnews.com

Business News