BEIJING -- Chinese authorities have cut water supplies to a district in a northwestern city after excessive levels of a toxic chemical were found in tap water.

The official Xinhua News Agency says tests conducted by environmental officials in the city of Lanzhou on Friday showed that tap water there contained 200 micrograms of benzene per litre -- 20 times the national limit.

Xinhua says the water supplier is a Chinese subsidiary of the French company Veolia Water and that it provides water to 2 million people in Lanzhou. An investigation is underway.

China News Service says the water supply to a district of about 300,000 people has been cut.

Calls to Veolia Water's office in Lanzhou rang unanswered, as did calls to the city government and its environmental protection bureau.