“Definitely there are things that we do that are infused with meaning,” says “Mad Men” creator Matthew Weiner. “This gallery shoot is not necessarily infused with meaning.”

Weiner spoke to TheWrap about the brand-new promotional photos for the seventh and final season of his closely watched drama. Despite his warning, we couldn’t help but try to interpret them.

Also read: Matthew Weiner Has Already Heard Your ‘Mad Men’ Ending

Since last season ended with Don Draper ejected from his own ad firm, we assumed that the images of him sitting alongside Roger Sterling and Peggy Olson must be part of a dream.

But no, Weiner said: The images aren’t from upcoming episodes, and don’t hint at plot developments. They’re standalone images meant to evoke a feeling.

Also read: Matthew Weiner Hopes to End ‘Mad Men’ in Modern Times

“It’s more designed as something we do every year before we launch the the show to present the characters in an abstract setting and remind everybody who’s there, and show off our hair and makeup and costumes and photography and the incredible cast, in a way that whets your appetite for the show,” he explained. “One of the great things we get to do… is kind of show off the glamour of the characters in a way we don’t always in the show.”

But surely the airport represents… um… Don’s… journey?

“We picked the airport this year because it harkens back to something that has truly disappeared, which is – not the adventure of travel, hopefully that will never go away – but certainly the idea of glamour and travel is gone. We don’t see Peggy waiting in line with her iPod and sweat pants,” Weiner said.

So really, no symbolism at all?

“I have to disappointingly say that it’s not particularly symbolic,” Weiner said. “For me the answers to the show get answered when you watch the show.”

Oh well. The photos are still gorgeous. Enjoy: