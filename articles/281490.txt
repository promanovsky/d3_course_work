Two outbreaks involving foods made from either sprouted chia seeds or clover have sickened more than 70 people in the United States and Canada, and more than 10 of them have been hospitalized.

A salmonella outbreak has been linked to a powder made from sprouted, ground chia seeds and another product made from sprouted chia and flax seeds, USA Today reported.

The chia products have been linked to 21 illnesses in 12 states and 34 infections in Canada.

In the other outbreak, sprouted clover has been linked to an E. coli outbreak that has sickened 17 people in five states. Nearly half of those people have been hospitalized, USA Today reported.

Officials traced the outbreak to raw clover sprouts produced by Evergreen Fresh Sprouts of Moyie Springs, Idaho.