Tokyo: Japan eased its weapons export restrictions on Tuesday in the first major overhaul of arms transfer policy in nearly half a century, as Prime Minister Shinzo Abe seeks to fortify ties with allies and bolster the domestic defence industry.

In a move likely to anger China, where bitter memories of Japan’s past militarism run deep, the government decided to allow arms exports and participation in joint weapons development and production when they serve international peace and Japan’s security.

That is a shift from a decades-old policy of banning all weapons exports in principle, although quite a few exceptions to the rule have been made over the years, such as the transfer of arms technology to the United States, Japan’s closest ally.

“This is beneficial for Japanese companies in that they can take part in joint development and joint production and have access to cutting-edge technology,” Takushoku University Professor Heigo Sato said.

“If you live in a closed market like the Japanese defence industry does, you clearly lag behind in technological development.” But even under the new regime, Japan is to focus mainly on non-lethal defence gear such as patrol ships and mine detectors and says it has no plan to export such weapons as tanks and fighter jets.

The move comes when Sino-Japanese ties have been chilled due to a territorial dispute over a group of East China Sea islets and Abe’s visit in December to Tokyo’s Yasukuni Shrine, seen by critics as a symbol of Japan’s wartime aggression.

Japan’s self-imposed restrictions on arms exports have virtually excluded defence contractors such as Mitsubishi Heavy Industries, Kawasaki Heavy Industries and IHI from the overseas market and made it difficult for them to cut costs and keep abreast of technological development.

COMPETITION TOUGH Japan’s defence budget slipped for a decade through 2012, raising concerns that some of the smaller and less diversified arms makers might be forced to go out of business.

The new export policy alone will unlikely help Japanese defence makers establish a big presence overseas, although some high-performance Japanese components, such diesel engines for ships, stand out among potential competitors.

“It’s not as if Japanese (defence) goods will start selling right away because of this. The government still needs to play a leading role in their overseas expansion. Various governments are already competing fiercely out there,” said Bonji Ohara, research fellow at the Tokyo Foundation, a think tank.

“Competition dictates prices. Of course, they cannot set the kind of prices they are setting for the domestic market,” said Ohara, who once served as a Japanese navy attache in China.

One of Japan’s potential defence gear exports is Kawasaki Heavy’s submarine diesel engines, which do not require air and allows submarines to stay submerged for an extended period.

When Japanese Defence Ministry officials visited Australia last year, Canberra showed interest in them, a Japanese government official said.

Another one is ShinMaywa Industries’ US-2 amphibious aircraft. India is already in talks with the Japanese government for possible procurement.

Under the new rules, Japan still bans weapons exports to countries that are involved in international conflicts and shipments that breach UN Security Council resolutions such as exports to North Korea and Iran.

In announcing the new rules, Japan stressed that it would remain a nation “striving for peace” and screen each case to see if exports should be allowed, a move to assure its neighbours that Japan is not taking a path to a military power.

Besides the easing of arms export rules, Abe last year raised Japan’s defence budget for the first time in 11 years and aims to lift its ban on exercising the right of collective self-defence, or aiding an ally under attack.

These moves, coupled with his Yasukuni visit, prompted criticism in China that Japan is lurching toward militarism.

Under its post-war, pacifist constitution, Japan drew up the “three principles” on arms exports in 1967, banning sales to countries with communist governments or those involved in international conflicts or subject to UN sanctions.

Over time, the rules became tantamount to a blanket ban on exports, but that later became riddled with exceptions.