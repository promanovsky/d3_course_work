LOS ANGELES, April 23 (UPI) -- Jodie Foster is a married woman.

The 51-year-old actress's rep told Us Weeky that Foster married her girlfriend of six months, Alexandra Hedison, over the weekend.

Advertisement

Reports that Foster and Hedison were dating began in October 2013. The duo made their first public appearance together at the Wallis Annenberg Center for the Performing Arts gala in Beverly Hills later that same month.

The Elisyum star previously dated Cydney Bernard from 1993 to 2008. They have two children together, Charlie, 14, and Kit, 11. Hedison, on the other hand, dated Ellen DeGeneres for three years.

News of Foster's marriage comes more than a year after she came out in an emotional speech during the Gloden Globes 2013.