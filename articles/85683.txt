The Game of Thrones cast do their own 'shipping:

As part of one of the biggest shows on television, the Game of Thrones cast have become very familiar with the US TV fan craze for 'shipping.

'Shipping is where fans put together two characters they would love to see get it on, share a bromance or just generally hang out more. It's basically about creating a fantasy 'relationship', hence, 'shipping.

So when we caught up with the cast of the HBO fantasy show at the UK launch, we asked them who they would most like to be 'shipped with.

And they certainly had some shocking ideas. From unlikely romances (Davos and Cersei) to weird bromances (Samwell and Joffrey), they had some cracking and original ideas. Hopefully George RR Martin is taking notes.

How should we punish Game of Thrones spoilers: The cast decide

Game of Thrones: The cast reveal who they want on the Iron Throne

Game of Thrones season 4 starts tonight (April 6) at 2am on Sky Atlantic. It is repeated at 9pm on Monday (April 7).

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io