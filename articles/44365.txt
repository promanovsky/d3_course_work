The balance of power on Wall Street may be shifting in the battle of the bulge(s).

JPMorgan’s Mike Cavanagh is exiting the bank to become co-chief operating officer of Carlyle Group.

“The regulatory landscape definitely makes it less enticing for some of these great managers to stay at the big banks, and the talent drain will continue,” Glenn Schorr, an analyst at International Strategy & Investment Group told Bloomberg. “It’s a significant loss for JPMorgan and a huge win for Carlyle, no two ways about it.”

Cavanagh, 48, was seen internally as a potential successor to Dimon, 58, who has told people he wants to remain CEO for another five years, sources said.

Top bankers and traders have left the biggest Wall Street firms for more lucrative jobs in private equity and at hedge funds, as regulation of lenders has intensified following the financial crisis and

lower returns have pushed banks to cut the amount of revenue they set aside for compensation.

Cavanagh’s exit is seen as a palace coup for Matt Zames, 43, head of mortgage banking, in consolidating his position as Dimon’s successor.

Zames has climbed JPMorgan’s ranks as other long-time executives left. Frank Bisignano, who had been co-COO with Zames, departed in April 2013 to become CEO of First Data Corp. James E.

Staley, 57, who also had been seen as a successor to Dimon, quit in January of last year to join BlueMountain Capital Management, the hedge-fund firm that profited from the London Whale loss.

Dimon ousted Bill Winters, 52, and pushed Steven Black, 61, into a lesser role to install Staley atop the investment-banking unit in 2009.

Chief Investment Officer Ina Drew, 57, left in May 2012, within a week of the disclosure of the London Whale losses.

The London Whale task force led by Cavanagh produced a 129-page report that cited poor risk controls and Dimon’s failure to question information he received from deputies as contributing to the more than $6.2 billion loss.