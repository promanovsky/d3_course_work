Last week rumors surfaced that Selena Gomez had ended her friendship with reality stars Kendall and Kylie Jenner. The trio was spotted hanging out at Coachella Music Festival but apparently something went down that caused the girls to go their separate ways.

A source told Hollywood Life that Gomez "went off" because she found out that Kylie was sending Justin Bieber sexy selfies.

"Kylie sent sexy pics of herself to Justin and that's what started the fight," a source told the Hollywood Life. "Selena saw the pictures on Justin's phone and she freaked out and left immediately."

The source also claims that Jenner and Bieber "hooked up" at Coachella.

"I've heard it happened," the source added.

Neither Bieber nor Jenner has addressed the rumor but the two are said to be close friends. The source told Hollywood Life that the Jenner sisters "pretty much adore Justin and he can do no bad in their eyes."

"Selena feels like she doesn't have any support right now and it sucks," the source said.

Following Coachella, Gomez unfollowed the Jenners on Instagram and deleted every picture she had taken with them. She then unfollowed Bieber and everyone else on the photo sharing site.

Earlier in the year, Jenner defended Bieber after he was "attacked" for egging his neighbor's house. The "Keeping Up With the Kardashians" star told E! News that she felt like people were attacking the "Boyfriend" singer because he was a celebrity.

"I feel strongly. Just because I know when we're in positions like that," she said. "And I just feel like people feel like they have the opportunity to, like, kind of mess with you just because of your status."

"So I don't know, I feel strongly," she added. "What are you raiding his house for, to find eggs?"

She also gushed about the pop star last year at the premiere of "Believe." Jenner told E! that the singer had "always been so nice and he's such a good guy despite what everyone says about him.

"I think he's amazing. He's just been a great friend," she said.

Do you think Justin Bieber would really cheat on Selena Gomez with Kylie Jenner?

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.