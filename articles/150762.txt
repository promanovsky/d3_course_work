All praise the comedy gods, for today they hath brought us a gift.

"The Tonight Show" has a hilarious habit of mashing up Brian Williams broadcasts with songs that the newsman probably wouldn’t sing in real life (although he certainly does have a sense of humor). But staff writers may have outdone themselves with the show’s latest, which has Mr. Williams getting funky in a rousing rendition of Snoop Dogg’s 1993 classic, “Gin and Juice.”

All we can say is that “with so much drama in the L-B-C, it’s kinda hard bein’ Snoop D-O-double-G.” So big ups to Brian Williams for giving it a shot. We’ll be toasting him with, what else, a large glass of gin and juice.

Specifically, a sprightly cocktail of ginger beer, gin, orange and lemon juice, and just a touch of Grand Marnier. Snoop, we hope, would approve.

[via Jezebel]

Photo credit: StockFood/Colin Cooke

Ginger, Gin, and Juice

Adapted from Gourmet

Serves 1

3 ounces (6 tablespoons) chilled ginger beer

1 1/2 ounces (3 tablespoons) gin

1 tablespoon fresh orange juice

1 teaspoon fresh lemon juice

1/2 teaspoon Grand Marnier or other orange-flavored liqueur

Garnish: an orange slice

1. In an old fashioned glass filled with ice, pour all ingredients and mix well with a bar spoon. Garnish with orange slice and serve.