Perfect match! Husbands and wives have similar DNA, research shows

Scientists compared DNA of 800 married couples to random couples

Found spouses have more DNA in common than the strangers



Husbands and wives have similar DNA, researchers at the University of California have found (library image)

It seems birds of a feather really do flock together. Husbands and wives are more similar to each other than would be expected by chance, research has revealed.

The finding comes from behavioural scientists who compared the DNA of more than 800 married couples with that of people paired together at random for the study.

Despite the old adage that opposites attract, the husbands and wives had more DNA in common than the strangers.

This means, in the words of another saying, that birds of a feather flock together.

The researchers, from the University of California, said that the finding can be partly explained by people tending to marry someone who lived nearby or of the same ethnicity.

However, despite this, we still seem to be drawn to spouses whose genes are similar to our own.



Researcher Ben Domingue said he does not know how we sniff out the right person – but it may be as simple as the genes involved affecting something as obvious as height.

If someone who is short or tall then sets their sights on someone of similar height, they would unwittingly be picking a partner with similar DNA.

It is also possible that people with similar genes are thrown together by their hobbies or educational interests.

His study, published in the journal Proceedings of the National Academy of Sciences, clashes with previous research which has found evidence for the theory that opposites attract.



These focus on the genes that control the body’s ability to fight disease and show we seem to be programmed to seek out partners whose immune systems are very different to our own.

The researchers said the finding can be partly explained by people tending to marry someone who lived nearby or of the same ethnicity (library image)

It is thought that seeking out a mate with a different immune system ensures any children a couple has will have the broadest possible immunity against disease.

But a study in 2010 found people do tend to settle down with partners whose tastes mirror their own. Researchers from Michigan State University tried to find out if couples grew more alike with each wedding anniversary, or if it was their similarities that brought them together in the first place.