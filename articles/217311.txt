Update 5 p.m. EDT: On Twitter, SpaceX released a photo of the Dragon spacecraft's splashdown in the Pacific Ocean.

[PHOTO] Splashdown! Dragon lands in the Pacific Ocean with 3,500 pounds of ISS cargo. pic.twitter.com/X4fuUQm247 May 18, 2014

The SpaceX Dragon spacecraft was released from the International Space Station at 9:26 a.m. EDT on Sunday, completing the third commercial resupply (CRS-3) mission, reports NASA. The capsule was carrying 3,500 pounds of supplies and science experiments and splashed down in the Pacific Ocean at 3:05 p.m. EDT.

On Twitter, SpaceX announced the return of the Dragon capsule and a successful splashdown in the Pacific Ocean on Sunday.

Splashdown is confirmed!! Welcome home, Dragon! — SpaceX (@SpaceX) May 18, 2014

On Sunday, the space station's robotic arm, Canadarm2, was put into the release position remotely and ISS Expedition 40 Commander Steve Swanson of NASA released the Dragon spacecraft, with flight engineer Alexander Skvortsov assisting, at 9:26 a.m. EDT. Shortly after its departure, Dragon completed two departure burns while a final deorbit burn occurred at 2:12 p.m. EDT. The cargo will be delivered to the Johnson Space Center within 48 hours.

The third SpaceX cargo mission to the ISS reached the space station on April 20, bringing close to 5,000 pounds of fresh supplies and science experiments. NASA reported progress on several research fronts: the Antibiotic Effective in Space (AES-1) project, which studied the reduced effectiveness of antibiotics in space; the T-Cell Activation in Aging (T-Cell Act in Aging) investigation, which analyzed immune-system depression in microgravity; and the Biological Research in Canisters (BRIC) experiment, which grew antibiotic-resistant bacteria in microgravity. Of the 3,563 pounds of cargo returning to Earth, the completed science investigations weigh 1,600 pounds.

Science conducted aboard the space station has plenty of benefits for Earth. For example, AES-1 data could be used to improve antibiotic development while T-Cell Act in Aging data could be used to improve treatment of autoimmune diseases.

The next ISS launch will occur on May 28 and will bring NASA astronaut Reid Wiseman, cosmonaut Maxim Suaraev and European Space Agency astronaut Alexander Gerst to the space station. The trio will serve as Expedition 40/41 crew members. Following the May 28 launch, the next cargo mission will be Orbital Science's second Commercial Resupply Services (Orbital 2) mission on June 10.

A video of the Dragon's departure can be viewed below.