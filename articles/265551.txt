If you follow Kristen Stewart’s career with the irrational passion that this writer does, you’ll no doubt have already seen the trailer for her new movie (well, technically, Juliette Binoche’s new movie) Clouds of Sils Maria. And you will have cheered.

Look at our girl, moving past the unfortunate cloud of Twilight fame to act in a serious, psychological film. We couldn’t be more proud. If you’re not quite as obsessive and have not seen the trailer just yet, you can check out out below.

Watch the trailer for The Hills of Sils Maria below.



Stewart plays Val, the personal assistant of ageing Euro starlet Maria Enders (Juliette Binoche), as the latter faces a turbulent time in personal her life and has to make a decision on a crucial role for her career. Maria is offered a starring role in a revival of the play that made her famous two decades earlier. But while then she played Sigrid, a chaotic young woman, who fascinates the elder Helena and eventually pushes her to suicide, this time around, she is offered the elder role, while Sigrid will go to Jo-Ann (Chloe Grace Moretz). The role and Maria’s questionable relationship with Val combine and force her to face the passage of time and come to terms with age and maturity.



Kristen Stewart [l] and Juliette Binoche [r] team up in this psychological drama.

More: Kristen Stewart Stars In Fleshy Perfume Ad

More: Juliette Binoche Cries In Interview Over Father's Ill Health

The casting is a stroke of genius and it will be interesting to see Stewart and Moretz acting opposite the larger-than-life presence of Binoche, as well as tackle the questions posed by fame and infamy – particularly for Stewart, whose private life and personality have been under constant scrutiny more than most. According to the LA Times, Stewart was originally offered the role of Jo-Ann, but turned it down, in order to distance her public persona from her character – a completely unknown, somewhat reclusive young woman.