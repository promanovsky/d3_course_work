Snapchat, the vanishing message app popular with young people, has quietly raised new funds that value the company at nearly $10 billion, according to a new report.

Venture capital firm Kleiner Perkins Caufield & Byer agreed to invest up to $20 million in Snapchat in May, the Wall Street Journal reported on Tuesday, citing people familiar with the matter.

Last year, Facebook founders Evan Spiegel and Bobby Murphy, who created the app in their Stanford University dorm room, turned down a reported $3 billion buyout offer from Facebook.

The $10 billion valuation means the co-founders are billionaires — at least on paper.

With the new funds, Snapchat joins an elite club of startups with valuations of $10 billion or more, including car service Uber and room rental startup AirBnB.

Kleiner Perkins was also an early investor in Google, Facebook, Uber and Twitter.

Snapchat users can send photo-messages that vanish within seconds — making the app infamous for “sexting.” The service doesn’t make money but is expected to open to advertisers soon.

Neither Snapchat nor Kleiner would comment on the company’s valuation or the new funds raised.