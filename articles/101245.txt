Subway said Friday that its sandwich shops will stop using bread that contains a chemical found in yoga mats by next week, following a rash of bad publicity earlier this year.

The company, the single largest chain restaurant in the world with 40,000 locations worldwide, had signaled in February that it would stop using the bread, after a food blogger launched an “eat fresh—not yoga mats” campaign—playing on Subway’s company motto. Chief marketing officer Tony Pace told the Associated Press the company would be done phasing out the bread in a week.

“You see the social media traffic, and people are happy that we’re taking it out, but they want to know when we’re taking it out,” Pace said. “If there are people who have that hesitation, that hesitation is going to be removed.”

The FDA-approved ingredient, azodicarbonamide, is banned in the European Union and Australia due to health concerns.

Subway says that the ingredient is helpful in making dough more stable, and is safe, common and acceptable, but that it began removing the ingredient last year. It’s known as a “chemical foaming agent” for plastics and is found in over 130 other brands besides Subway, including Jimmy Dean, Ball Park, Pillsbury Wonder and supermarket breads, according to the Environmental Working Group.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.