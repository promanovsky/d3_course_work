UNIVERSAL CITY, Calif. -- Soulful rocker Josh Kaufman has the winning voice.

The 38-year-old from team Usher was crowned the season six winner of "The Voice" Tuesday.

"It was a lot of nerves and heart-racing and it felt like forever," Kaufman said during a post-show press conference.

This is the first win for Usher, who also coached during season four.

Usher relished his triumph over previous winners Adam Levine and Blake Shelton.

He joked that the Maroon 5 frontman was likely "behind the stage counting votes."

Shelton's country crooner Jake Worthington came in second place, and singer-songwriter Christina Grimmie from team Levine took third.

Coldplay, Ed Sheeran, OneRepublic, Tim McGraw and Robin Thicke performed during the finale.

Gwen Stefani and Pharrell Williams will join Levine and Shelton for the sing-off's seventh season.