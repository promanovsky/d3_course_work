2.5 times as many women provide 24-hour care for Alzheimer's patient. Maria Shriver testifies before a Special Senate Aging Committee hearing on Alzheimer's in Washington. (UPI Photo/Kevin Dietsch) | License Photo

CHICAGO, March 19 (UPI) -- U.S. women in their 60s are twice as likely to develop Alzheimer's disease as they are breast cancer, researchers say.

The Alzheimer's Association Alzheimer's Disease Facts and Figures report estimated a woman's lifetime risk of developing Alzheimer's at age 65 is 1-in-6, compared with nearly 1-in-11 for a man.

Advertisement

"Through our role in the development of The Shriver Report: A Woman's Nation Takes on Alzheimer's in 2010, in conjunction with Maria Shriver, we know that women are the epicenter of Alzheimer's disease, representing majority of both people with the disease and Alzheimer's caregivers," Angela Geiger, chief strategy officer of the Alzheimer's Association, said in a statement.

"Well-deserved investments in breast cancer and other leading causes of death such as heart disease, stroke and HIV/AIDS have resulted in substantial decreases in death. Comparable investments are now needed to realize the same success with Alzheimer's in preventing and treating the disease."

There are 2.5 times as many women than men providing intensive "on-duty" care 24 hours a day for someone living with Alzheimer's disease, and among caregivers who feel isolated, 17 percent of women are likely to link isolation with feeling depressed versus 2 percent of men.

Among caregivers who have been employed while they were also caregiving:

-- 20 percent of women vs. 3 percent of men went from working full-time to working part-time while acting as a caregiver.

-- 18 percent of women vs. 11 percent of men took a leave of absence from work.

-- 11 percent of women vs. 5 percent of men gave up work entirely.

-- 10 percent of women vs. 5 percent of men lost job benefits.

There are currently 15.5 million U.S. caregivers providing 17.7 billion hours of unpaid care, often at the detriment of their own health. The physical and emotional impact of dementia caregiving resulted in an estimated $9.3 billion in increased healthcare costs for Alzheimer's caregivers last year.