Michael Barnier, the European Union's financial services chief, has promised that the EU will get tough on high frequency trading.

Barnier has criticised the controversial trading tactic just a day ahead of a European parliament vote to place curbs on it.

HFT is the act of using advanced computers to jump in front of investors to transact a large number of orders at very fast speeds – often buying and selling at a fraction of a second – sometimes for a profit of just one cent.

"With these rules the EU is putting in place one of the strictest set of regulations for high-frequency trading in the world. Although HFT trading might bring some benefits, we need to make sure that it doesn't cause instability," Barnier said.

The new EU rules will include restrictions to guarantee that price increments for securities don't become too small. And there will be mandatory tests of HFT algorithms if the vote is passed, which it is more than likely to be.

One of the key rules that could be implemented is traders will be required to stay in the market for a certain period of time to prevent volatility.

The practice of HFT originally came into the public eye during the 2010 Flash Crash when a mutual fund firm were selling an unusually high volume of E-Mini S&P contracts.

High frequency traders then started selling aggressively, accelerating the price decline. This lead to an average reduction of 9% on stocks.

Regulators have been focusing on the use of HFT for some time. But scrutiny is now shifting towards whether it is an unfair advantage or has been used criminally.

In Michael Lewis' latest book, Flash Boys, he claims that Wall Street is rigged and that a few companies are dominating the market in order to make a super-fast profit.