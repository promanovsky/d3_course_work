Nasdaq-listed Zebra Technologies Corp. stock fell 10 per cent on Tuesday after entering into an agreement to buy Motorola’s Enterprise business for $3.45 billion.

Zebra Technologies’ main business is bar code technology that can help companies track assets internally. Motorola Enterprise has a mobile platform and advanced data capture technology that will broaden Zebra’s reach.

Zebra stock dropped $7 to $61.44 on Tuesday afternoon, an indication of market hesitation about the cost of the deal and the prospects for Motorola, which is experiencing soft demand in its North American government business. Motorola lowered its revenue forecast for the first quarter to $1.8 billion.

Zebra sees significant upside in the Motorola division, despite its recent difficulties, calling it a strategic acquisition.

"This acquisition will transform Zebra into a leading provider of solutions that deliver greater intelligence and insights into our customers' enterprises and extended value chains," said CEO Anders Gustafsson in a press statement.

"The enterprise business will generate significant value for our shareholders by driving further product innovation and deeper engagement with our customers and partners. It positions Zebra as a leading technology innovator, with the accelerating convergence of mobility, data analytics and cloud computing," he said.

The Illinois-based firm expects to fund the transaction with $200 million of available cash on hand and $3.25 billion to be raised through a new credit facility and debt securities.

The transaction will expand Zebra’s geographic reach to more than 100 countries and gives it access to 4,500 U.S. and international patents.

Zebra recently has made series of moves to position itself to take advantage of advances in cloud computing and the Internet of Things.

In October, it launched Zatar, a software that allows companies to manage devices and sensors connected to the internet. It also bought Hart Systems, a maker of web-based inventory management software.