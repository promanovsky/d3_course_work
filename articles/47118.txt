Virtual currencies like bitcoin will be taxed in the United States like property — not currency, the Internal Revenue Service said Tuesday.

The IRS says bitcoin is not legal tender. You can't use it to pay your taxes.

However, if you receive wages in bitcoin, you have to pay taxes on it just like you would if you got paid in dollars. Or if you got paid in chickens.

The IRS issued a series of 16 questions and answers Tuesday to clarify the tax treatment of virtual currencies like bitcoin. In general, the IRS says it will apply the same rules that govern other barter transactions.

If you receive wages in bitcoin, they would be taxed at their fair market value at the time you were paid, the IRS said. If you use bitcoins to pay for goods or services, the vendor must report the income, using the fair market value of the bitcoins at the time of the transaction.

Bitcoin as an investment

For investors, bitcoins will be treated like other commodities, the IRS said. If they increase in value, you have to pay capital gains taxes after you sell them. If they lose value, you can recognize a capital loss.

In Canada, the Canada Revenue Agency treats bitcoin in much the same way, applying capital gains taxes when the digital currency is treated as an investment.

Created in 2009, bitcoin is an online currency that allows people to make one-to-one transactions, buy goods and services and exchange money across borders without involving banks or other third parties. Bitcoins have become popular with libertarians, tech enthusiasts and speculators.

Regulators worry about criminals using them to avoid detection.

In February, one of the largest bitcoin exchanges, based in Tokyo, filed for bankruptcy, adding to mistrust of the currency. Supporters say problems at the exchange were isolated.