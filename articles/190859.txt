The "Mighty Morphin Power Rangers" franchise has teamed up with "Hunger Games" film studio Lionsgate to release a live-action reboot of the 1990s classic kids television show.

The first of the "Power Rangers" television series premiered more than 20 years ago and has spawned multiple films, comic books, toys, apparel, costumes and much more. The original "Power Rangers" starred Amy Jo Johnson, Austin St. John, Walter Emanuel Jones, David Yost, Thuy Trang, Jason David Frank and Steve Cardenas.

"Lionsgate is the perfect home for elevating our Power Rangers brand to the next level," "Power Rangers" Creator Haim Saban said in a news release. "They have the vision, marketing prowess and incredible track record in launching breakthrough hits from The Hunger Games to Twilight and Divergent. In partnership with the Lionsgate team, we're con!dent that we will capture the world of the Power Rangers and translate it into a unique and memorable motion picture phenomenon with a legacy all its own."

The new "Power Rangers" film will "re-envision" the original franchise's story line, which followed a group of high school kids who used their unique super powers to help protect, and sometimes save, the world.

"We're thrilled to be partnering with Haim Saban and his team to maximize the potential of this immensely successful and universally recognized franchise," Lionsgate Chief Executive Oﬃcer Jon Feltheimer said in a news release. "The Power Rangers stories and characters have been embraced by generations of audiences for more than 20 years, and today they are more powerful than ever. We have the ideal partner and the perfect brand with which to create a motion picture event that will resonate with moviegoers around the world for years to come."

The upcoming "Mighty Morphin Power Rangers" reboot does not have an official cast, director or release date.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.