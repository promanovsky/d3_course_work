Paul Davidson

USA TODAY

Citing an improving economy and labor market, the Federal Reserve agreed Wednesday to continue to wind down bond purchases intended to hold down long-term interest rates and spur growth.

In a statement after its two-day meeting, the Fed acknowledged the recent rapid fall in unemployment and removed its observation from June that the jobless rate "remains elevated."

But it added an emphatic note of caution.

"A range of labor market indicators suggests that there remains significant underutilization of labor resources," the statement said.

Fed Chair Janet Yellen has noted that despite falling unemployment — now 6.1% — the ranks of the long-term unemployed and part-time workers who prefer full-time jobs remain at historically high levels.

The assertion that the labor market is still far from normal could dampen speculation that declining unemployment and rising inflation will force the Fed to start raising its benchmark short-term interest rates earlier than expected in 2015.

The statement conceded that inflation "has moved somewhat closer" to the Fed's 2% annual target. But it added that long-term inflation expectations remain stable.

The statement thus provided some fodder for both camps of policymakers — those arguing that rock-bottom interest rates are needed longer and those saying a warming economy will prompt an earlier rate hike.

The lone dissenting vote was cast by Charles Plosser, president of the Philadelphia Federal Reserve Bank. The Fed's statement said he objected to the Fed's guidance indicating that it expects to keep rates low for "a considerable time" after the bond purchase program ends. He said that does not reflect the economy's progress toward the Fed's goals for maximum employment and price stability, according to the statement.

Earlier Wednesday, the government said the economy grew at a 4% seasonally adjusted annual rate in the second quarter, much better than expected. That was a sharp rebound from a first-quarter contraction of 2.1% — the worst in five years but less severe than the government's previous 2.9% estimate.

The Fed's statement acknowledged the bounce-back, saying economic activity rebounded in the second quarter.

Fed policymakers, as anticipated, agreed to reduce purchases of Treasury bonds and mortgage-backed securities to $25 billion a month from $35 billion. The Fed has said it expects to end the program in October.

Launched in September 2012, the purchases were designed to jolt a sluggish economy by lowering borrowing costs for consumers and businesses. By pushing down bond yields, they also have driven investments to stocks, goosing markets and propping up household wealth.

Since the program began, the unemployment rate has fallen to 6.1% from 8.1% and average monthly job growth this year has accelerated to a 200,000-plus pace. The Fed, pointing to the gains, has tapered down the bond purchases from $85 billion a month since December.

Fed policymakers have begun to debate when to raise the central bank's benchmark short-term interest rate, which has hovered near zero since the 2008 financial crisis. According to their median forecast, they expect the first rate hike in the second half of 2015.

Some economists, however, say the strengthening job market and rising inflation will force the Fed to raise its short-term rate early next year.

Yellen has emphasized that the timing of any rate increase will depend on the pace of the recovery.