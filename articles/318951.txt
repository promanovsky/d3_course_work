10 Things to Know for Thursday - 26 June 2014

Your daily look at late-breaking news, upcoming events and the stories that will be talked about Thursday:

1. WHY US WAS CAUGHT FLAT-FOOTED BY IRAQ INSURGENCY

Officials tell the AP that the CIA has pulled back its spies since American troops left the country.

Kurdish peshmerga fighters clean their weapons at a base on the front line with militants from the al-Qaida-inspired Islamic State in Iraq and the Levant (ISIL), in Tuz Khormato, 100 kilometers (62 miles) south of the oil rich province of Kirkuk, northern Iraq, Wednesday, June 25, 2014. A defiant Prime Minister Nouri al-Maliki rejected calls Wednesday for an interim "national salvation government" intended to undermine the Sunni insurgency by presenting a unified front among Iraq's three main groups, calling it a "coup against the constitution." (AP Photo/Hussein Malla)

2. JUSTICES SIDE WITH PRIVACY

The Supreme Court rules that police generally can't search the cellphones of people they arrest without first getting search warrants.

3. FEDERAL APPEALS COURT RULES FOR GAY MARRIAGE

The three-judge panel declares that states cannot deprive people of the right to marry simply because they choose a partner of the same sex.

4. WHO LOST A FIGHT TO STREAM TV

The Supreme Court sides with broadcasters and says Internet startup Aereo will no longer be able to take programming from the airwaves for free.

5. VETERAN DEMOCRAT STAVES OFF PRIMARY CHALLENGE

With the narrow win, New York Rep. Charles Rangel moves a step closer to what he says will be his 23rd and final term in the House.

6. WHAT RUSSIA IS DOING TO AVOID SANCTIONS

The Kremlin renounces the right to send troops into Ukraine and voices support for a peace plan.

7. MORE VIOLENCE WRACKS NIGERIA

An explosion blamed on Islamic extremists rocks a shopping mall in Nigeria's capital, Abuja, killing at least 21.

8. NEW HOPE FOR BONE MARROW TRANSPLANT RECIPIENTS

Scientists are developing injections of cells specially designed to fend off viruses patients are vulnerable to in the months after surgery.

9. 'I NEVER LOST MY APPETITE FOR ACTING'

Masterful character actor Eli Wallach, whose most notable role was playing a Mexican outlaw in "The Good, the Bad, and the Ugly," dies at 98.

10. GIANTS' TIM LINCECUM NO-HITS PADRES — AGAIN

The 30-year-old righty becomes just the second pitcher in MLB history to twice no-hit the same team.

Jake Miller, 30, and Craig Bowen, 35, right, kiss after being married by Marion County Clerk Beth White, center, in Indianapolis, Wednesday, June 25, 2014. A federal judge struck down Indiana's ban on same-sex marriage Wednesday in a ruling that immediately allowed gay couples to wed. (AP Photo/Michael Conroy)