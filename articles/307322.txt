Popular social network Facebook went off-line on Thursday morning, leaving millions of its users briefly unable to connect with friends.

Facebook users in several countries, including Thailand, Hong Kong, Australia, Japan, Indonesia and the Netherlands, could not access the website for about half an hour.

Several of them turned to Twitter or Instagram after seeing the message, "Something went wrong. We're working to get it fixed as soon as we can."

Facebook issued a statement of apology for the brief outage without specifying its cause.

"Earlier this morning, we experienced an issue that prevented people from posting to Facebook for a brief period of time. We resolved the issue quickly, and we are now back to 100%. We're sorry for any inconvenience this may have caused," the Nasdaq-listed internet giant said.

On Wednesday, Facebook launched Slingshot, a Snapchat-like mobile app that lets users trade temporary photo- and video-based messages.

For comments and feedback contact: editorial@rttnews.com

Political News