An all-ages wish-granting organization in the Netherlands took a former zoo worker dying of cancer back to the zoo where he used to work, and one of the giraffes gave him a good-bye kiss.

The tender moment between the giraffe and the 54-year-old zoo worker, identified only as Mario, was caught on camera.

Advertisement

“These animals recognized him, and felt that [things aren’t] going well with him," said Kees Veldboer, founder of the Ambulance Wish Foundation, which provided transportation for Mario to go to the zoo.

Some scientists say there's evidence animals can sense illness in humans.

[Yahoo] [KRON-TV, San Francisco]