Nearly everyone knows a healthy diet consists of ample fruits and vegetables, but how much kale should a person really eat? The answer might be a lot.

A new study, published in the Journal of Epidemiology and Community Health, finds five servings of fruits and vegetables -- what's typically recommended -- may not actually be enough to stave off common diseases and chronic health woes. According to researchers, a healthful diet should include seven or more portions of fruits and vegetables a day. In particular, the researchers say eating a diet rich in vegetables offers the most protective health benefits.



The researchers say their study's findings should influence changes to the U.K.'s recommended daily allowance. Currently the U.K., as well as the U.S., recommends people consume five servings of fruits and vegetables a day.

For this study, the authors looked at data for more than 65,000 adults, who were 35 and older. The researchers pulled this data from England's annual health surveys conducted between 2001 and 2008, which included records of fatalities over 7.5 years. The data reported that 4,399 people died during this time period, or 6.7 percent of the population surveyed.

On average, the researchers found most people ate under four servings of fruits and vegetables a day -- and the more they consumed the healthier they were. People who ate seven servings of fruits and vegetables appeared to lower their risk for death from common diseases by about 42 percent. Additionally, they reduced their risk for heart disease and cardiovascular problems by 31 percent and their risk for cancer by 25 percent.

Eating vegetables had a more positive impact on health than fruit, according to the study. Two to three portions of vegetables reduced risk of death from common diseases by 19 percent, compared with 10 percent for people who favored fruit.

"The clear message here is that the more fruit and vegetables you eat, the less likely you are to die at any age," Dr. Oyinlola Oyebode of UCL's department of epidemiology and public health and lead author of the study, told The Telegraph. "My advice would be however much you are eating now, eat more."

In particular, the authors stress the value of sticking with fresh fruit. They found people who cheated and grabbed canned peaches rather than fresh ones actually increased their risk for dying premature by around 17 percent.

However, adhering to the seven or more a day rule will likely feel overwhelming to a person who struggles to choke down an artichoke. The good news is one doesn't need to eat an entire cantaloupe or head of broccoli in one sitting to meet this recommended allowance. The CDC has a handy chart and the portions are reasonable, even for a busy person who can barely remember to grab an apple. Or stick with First Lady Michelle Obama's rule of thumb: fill half your plate with fruits and vegetables.

