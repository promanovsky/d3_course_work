Global warming is one giant climate change hoax that’s being promoted by “unpatriotic racists” who are just trying to line their own pockets according to Pat Sajak, famous host of the Wheel Of Fortune TV show.

In a related report by The Inquisitr, while some people are bashing CNN for giving so-called “science deniers” a voice, others claim Bill Nye is being accused of bullying anyone who disagrees with him over climate change. The antagonistic political landscape has caused harm to the careers who do not agree with the consensus. For example, renowned Swedish climatologist Lennart Bengtsson says he was forced to resign from a global warming think tank called the Global Warming Policy Foundation because he was attempting to “bring more common sense into the global climate change debate.” Instead, Bengtsson claims the political pressure forced upon him was so overwhelming that he’s reminded of the McCarthy era:

“The problem we have now in the scientific community is that some scientists are mixing up their scientific role with that of climate activist … It is an indication of how science is gradually being influenced by political views.”

Well, Pat Sajak would probably agree with that climatologist in regards to the nature of his accusation. The Wheel Of Fortune host has been claiming a global warming hoax repeatedly from his Twitter account:

I now believe global warming alarmists are unpatriotic racists knowingly misleading for their own ends. Good night. — Pat Sajak (@patsajak) May 20, 2014

Very hot weather: “We’re all going to die!” Very cold weather: “There’s a difference between climate & weather, moron!” — Pat Sajak (@patsajak) May 11, 2014

In the second Tweet, Sajak is probably referring to how the chilly polar vortex was blamed on global warming. But in general, climate change is being blamed for everything from the California heat waves to a fire tornado that shot flames in all directions.

According to polls, an increasing number of Americans are now believing that global warming is a hoax and would agree with Sajak that politics is to blame for the alarmism. Politicians like Marco Rubio have also made strong statements against climate change science. But this attitude is not limited to the average Joe. For example, the founder of the Weather Channel is claiming climate change is a scam. Even NASA scientist Dr. Leslie Woodcock laughs off global warming as being nonsense.

Do you agree with Pat Sajak that global warming “alarmists” are purposefully misleading everyone? Or do you think everyone needs to cool off a bit and stop calling each other “alarmists” and “science deniers”?