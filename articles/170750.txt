As the demand and price of energy continues to surge so too does the sector's need for new staff. Many energy firms have an ageing workforce nearing retirement, and a shortage of trained graduates has left the booming industry facing a skills shortage with companies battling to attract new talent into the industry.

As the UK's largest industrial employer, energy firms have more than 175,000 workers across the UK in a wide range of roles and specialisms. We caught up with just a few of them to find out what they do – is the solution to the energy crisis is in their hands?

The geologist

Huw Clarke Photograph: Huw Clarke

Name: Huw Clarke

Age: 27

Location: Lichfield

Company: Cuadrilla Resources

Salary: Undisclosed

"It's not always obvious to people how you make a living out of geology and rocks, but all our plastics and metals have to come from somewhere – and that all probably started with someone looking at rocks.

"My job is to find areas with prospective rocks for shale gas. That could be anything from looking at well data or geological maps through to playing with advanced 3D seismic data and looking for clues for where to drill. It's a really wide-ranging job and I'm away from the office half the time, either on the rigs, at conferences or at community meetings.

"The idea that we're sleepwalking into an energy crisis and that the lights might go out is a bit abstract. For me, it's more about fuel poverty and the price of fuel for low-income earners. Cuadrilla's work in the UK has got a lot of potential in terms of indigenous resources and I can only see benefits from that."

The public affairs manager

Anna Stanford Photograph: Anna Stanford

Name: Anna Stanford

Age: 46

Location: Hertfordshire and Westminster

Company: RES

Salary: £45-70K

"I have to make the case for wind energy to decision-makers and the public. The nice thing is that there's very little routine to it because the political world moves very quickly and energy policy is high up the agenda. It's not all meeting MPs and civil servants in Westminster, but meeting the public, doing media work and consultations right through to taking school children to see a windfarm.

"It's always throwing up the unexpected: I might wake up to a resignation of a minister or a wildly inaccurate wind energy story. It's the kind of job that gives more back than most. It's made me very positive about humankind's ability to find elegant engineering solutions to our problems and we should be very optimistic about our ability to do that. After 20 years I still get a thrill from going to a windfarm and standing under the blades."

The offshore technician

Ben Rose Photograph: Ben Rose

Name: Ben Rose

Age: 25

Location: Flintshire, north Wales

Company: RWE Innogy UK

Salary: £30k

"It's a 40-minute sail to the windfarms and a trip I make at least three times a week. It's a different way of getting to work and the first couple of times were a bit odd, but you get used to it pretty quickly.

"I'm normally going onto the turbines looking at equipment, testing it to make sure it functions and has been built properly. I worked on hydro electric station before this job but I like both – it's just a different way to get the motors going.

"Some people presume it's dangerous but its not something that goes through my mind. It is a bit dangerous but we're well trained and well prepared. We don't set out on the boat if the weather's too bad and you can't climb the turbines without going through the proper training. With coal and oil prices going up we need to be developing other types of electricity and I think there's a lot of potential in offshore wind to power the country."

The environmental safety engineer

Hannah Brown Photograph: Hannah Brown

Name: Hannah Brown

Age: 26

Location: Hartlepool

Company: EDF Energy

Salary: £25-35K

"When I went to university I wanted study something environmental and the more you learn the more you realise that the biggest change you can make is within industry. With nuclear being one of the greenest and most low-carbon forms of electricity generation, it felt like natural progression. I've friends who went into coal and gas industries but that didn't feel right for me. I feel like I'm making a difference.

"My team is responsible for putting mitigations in place that protect the community, the environment, our employees and the power plant itself. We do a lot of work to increase awareness and training courses on environmental and radiation protection, but we also do outreach work in the local community and mentoring schemes with sixth formers interested in the industry.

"To me, the energy crisis is about the future of generating electricity and specifically low-carbon electricity."

The solar engineers

Jason Arnold Photograph: Jason Arnold

Name: Jason Arnold

Age: 39

Location: London

Company: Solarcentury

Salary: £40-50k

"I think I've been in the office just three times in the last three months. I spend a lot of time travelling but you do get to see some fantastic locations. My day will be travelling to solar farms, going to meetings and generally making sure everything's running OK.

"A lot of people think we stand around in muddy fields all the time, but we only do that for two months of the year. Some people are concerned about what the solar farm will be like once we've built it; they don't realise that it's no taller than a hedgerow, that it makes no noise, it's not ruining walks or views and can be built in 10 weeks. We do public consultations where we explain what we do and how we leave a natural habitat for animals in the process. I can get quite enthusiastic about it and hopefully I affect how they look at what we do."

Steve Smith Photograph: Steve Smith

Name: Steve Smith

Age: 43

Location: Machynlleth, Wales

Company: Dulas

Salary: £35-40k

"I used to be as civil engineer but decided it wasn't for me. I was concerned for the environment and what we, as a race, were doing to it so decided I wanted to get into renewables and went back to university. I'm now involved in looking after engineering quality standards, the resources and suitability of products, as well as a fair chunk of project management.

"There's still a perception that I work in an alternative industry, or that it's one-man operation putting solar panels on houses, when actually it's a mainstream industry on major commercial projects. Our complete failure to decarbonise our energy, which is a finite resource, is the real issue. But my opinion has changed over the years. "When I came into the industry I thought renewable is absolutely the way forward, but as time goes and no major plan are made for decarbonisation, all energy sources have to be in the mix for now otherwise the kettle won't boil."

This article is part of the Guardian's #bigenergydebate series. Click here to find out more about this project and our partners.