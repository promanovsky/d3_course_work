Known for high-energy shows that pack thousands into concert venues, superstar DJ Avicii’s concert in Boston on Wednesday night ended with several people in the hospital.

Emergency crews took dozens of concert goers, mostly minors, to local hospitals suffering mainly from drug and alcohol problems, authorities said. Local media put the number of patients between 30 and 80.

Roisin Saratonion, 18, told the Boston Globe that she left the concert earlier than planned because “it was just too crazy in there.”

“You get really hot in there and you just pass out. You couldn’t breathe,” Saratonionsaid. “It was claustrophobic. People get dehydrated.”

Avicii, the Swedish artist behind the hit song Wake Me Up, performed at the TD Garden venue as part of his True Tour.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.