Justin Bieber and Selena Gomez raised online temperatures on Tuesday.

The reunited couple heated up their respective Instagram accounts with provocative photos that saw the “Confident” superstar modeling Calvin Klein underwear, while around the same time the “Come & Get It” starlet flashed a little cheek to tease a “Secret project.”

First up, Bieber. Treading in Mark Walhberg and David Beckham’s footsteps, the 20-year-old showed off his naked chest as he posed in the first of two shots wearing an open shirt, low-slung black pants and peek-a-boo black Calvin Kleins.

Justin is seen looking down reflectively at his ripped abs, recent cross and “Forgive” tattoos inked at 40,000 feet on a jet, perhaps musing how grateful he is to his personal trainer.

“What if I do a Calvin Klein campaign?” the Canadian asked fans in the photo caption which was tweeted and Instagrammed.”Comment below yes or no.”

Not surprisingly, “Beliebers” lost it in comments on the image-sharing app and Twitter. Most are NSFW, so we can’t repeat them. Suffice it to say most were variants on “For the love of God Justin, yes!”

About an hour later Justin’s Klein question fell off Twitter, while over at his Instagram page his caption also went AWOL. However, Twitter later revealed a bug is deleting some tweets on the network.

In his second photo, Bieber puts his hands behind his head and comes on like a underwear campaign pro.

So far Calvin Klein have given “No comment” to media requests for clarification.

Meanwhile, GossipCop notes Bieber’s photos are positive signs something official could be in the works with the brand.

Over in Gomez’s neck of the woods, things were equally steamy.

And like her beau, black was the order of the day.

Selena’s Instagram photo, styled as if behind-the-scenes in a theater, revealed the brunette in a sexy, burlesque-type ensemble.

Sheer black tights, hip-hugging shorts with booty hint and her slender back on show, Selena is pictured looking into a stage-lit mirror as she plays with her Bardot-styled hair.

The gorgeous 21-year-old captioned the shot “… Secret project.”

(Photo via Instagram)

Nothing more appeared on Gomez’s social media accounts so we’ll have to wait and see what shakes out.

There’s a lot of anticipation not just around Justin and Selena’s solo projects, but also joint.

Following their widely reported, PDA-packed weekend in McAllen, Texas, earlier this month which was capped by two (since deleted) intimate, dance videos of the pair, their de facto reunion is big news.

The cherry? A Jelena duet called “Unfamiliar,” unless the title changes before its speculated release.

As previously reported, Bieber randomly played the track to a group of Toronto fans around two weeks ago. Those fans told the Internet. Since then, the heartthrob appeared to confirm the song on an amateur video filmed by fans as he left a recording studio.

Now, lingering doubts about the Jelena duet can be discarded. As per WeKnowTheDJ.com — which is the website of Tay James, the DJ on the Believe world tour and a close friend of Bieber — details on the Justin-Selena collaboration will be posted by the site this week.