LOS ANGELES, CA - SEPTEMBER 7: Marijuana plants grow at Perennial Holistic Wellness Center, a not-for-profit medical marijuana dispensary in operation since 2006, on September 7, 2012 in Los Angeles, California. A group of activists has submitted about 50,000 signatures in an effort to force a referendum on the marijuana ordinance. A minimum of 27,425 valid signatures from registered voters is needed to let voters decide on the issue in March. The ban would not prevent patients or cooperatives of two or three people to grow their own in small amounts. Californians voted to legalize medical cannabis use in 1996, clashing with federal drug laws. (Photo by David McNew/Getty Images) A budtender handles marijuana at Perennial Holistic Wellness Center, a not-for-profit medical marijuana dispensary in operation since 2006, on September 7, 2012 in Los Angeles, California. (Photo by David McNew/Getty Images)

CHICAGO (AP) — Illinois regulators crafting the first rules for the state’s new medical marijuana industry have lowered patient fees and deleted a section that angered gun owners.

Patients would pay $100 a year to apply for a medical marijuana card in Illinois under the revised preliminary rules unveiled Friday for the state’s four-year pilot program. Disabled people and veterans would pay $50 annually.

The fees were reduced from the $150 and $75 first proposed by the Illinois Department of Public Health following complaints from patients.

Guns had overshadowed other issues when the draft regulations were introduced in January. Some patients had said they would rather continue to use marijuana illegally rather than give up their firearms owners ID cards, as the draft rules had first required. Complaints poured in from gun owners. Many said their rights were being trampled.

“I’m happy to see that they have changed the provision,” said Rep. Lou Lang, a Skokie Democrat, who sponsored the medical cannabis legislation and sits on the committee that will vote on the rules. “I did ask them to remove it. I’m not the only person who did.”

The removal of the gun language was greeted warmly by advocates for patients. “Anything that makes it less burdensome for the patients is always a good thing,” said Julie Falco of Chicago, who speaks openly about how she has used cannabis to control her pain from multiple sclerosis.

The public has 45 days to comment on the current draft rules, which also lay out how entrepreneurs can apply for a limited number of licenses to grow or sell cannabis products.

Other changes include:

— Requiring businesses interested in growing marijuana to have $500,000 on hand, double the amount required in a previous draft. Growers would be required to show they could get enough insurance to cover losses. They would be required to submit a written explanation if they failed to maintain production for more than 90 days.

— Prohibiting cannabis packaging with cartoons, the Illinois flag or any images other than a grower’s logo. Products couldn’t resemble “a commercially available candy.”

— A larger advisory board for reviewing petitions on adding medical conditions approved for marijuana. Instead of a nine-member board of mostly doctors, a 15-member board would include doctors, nurses and patients.

— The new draft rules state several times that cannabis is a controlled substance under federal law and that growing, distributing or possessing it, except through a federally approved research program, violates federal law.

(TM and © Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)