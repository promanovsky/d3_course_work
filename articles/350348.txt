LOS ANGELES (AP) – Amazon said Wednesday that it is prepared to go to court against the Federal Trade Commission to defend itself against charges that it has not done enough to prevent children from making unauthorized in-app purchases.

The FTC alleged in a draft lawsuit released by Amazon that unauthorized charges by children on Amazon tablets have amounted to millions of dollars.

Seattle-based Amazon.com Inc. said in a letter Tuesday to FTC Chairwoman Edith Ramirez that it had already refunded money to parents who complained. It also said its parental controls go beyond what the FTC required from Apple when it imposed a $32.5 million fine on the company in January over a similar matter.

Amazon’s Kindle Free Time app can limit how much time children spend on Kindle tablets as well as require a personal identification number for in-app purchases, said Amazon spokesman Craig Berman.

“Parents can say — at any time, for every purchase that’s made — that a PIN is required,” he said.

By not agreeing to a settlement with the FTC, the company faces a potential lawsuit by the FTC in federal district court.

Apple complained when the FTC announced its settlement with the company in January.

CEO Tim Cook explained to employees in a memo that the settlement did not require the company to do anything it wasn’t doing already but he added that it “smacked of double jeopardy” because Apple had already settled a similar class-action lawsuit in which it agreed to refunds.

The FTC wouldn’t comment on whether it is investigating Amazon’s in-app purchase policies, but said in a statement: “The Commission is focused on ensuring that companies comply with the fundamental principle that consumers should not be made to pay for something they did not authorize.”

In the FTC’s draft complaint, the commission said that after Amazon began billing for in-app purchase in November 2011, an Amazon Appstore manager described the level of complaints about unauthorized purchases by children as at “near house on fire” levels.

It said Amazon began requiring password entry for in-app charges above $20 in March 2012, and then for all purchases in early 2013. However, for smaller purchases, entering a password once left open a billing window of 15 minutes to an hour in which new charges wouldn’t require a password, the FTC said in the complaint.

Prompts from kid-targeted games like “Pet Shop Story” would sometimes not give a price for purchases, the FTC said. Sometimes the purchase prompts from games like “Tap Zoo” were easily mistaken for those using “coins,” ”stars,” and other virtual currency, it said.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending:

PHOTOS: Your Pet Pictures