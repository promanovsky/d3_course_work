NEW YORK (MarketWatch) — When it comes to ads, don’t expect Amazon to knock Google off its throne just yet.

Amazon AMZN, +0.48% is making strategic moves to more directly challenge Google’s online ads platform, including possibly developing its own internal software that would replace Google AdWords, according to a report by The Wall Street Journal.

The platform would leverage Amazon’s treasure trove of consumer data, giving Madison Avenue access to even more targeted ads that are tied directly to shopping behavior.

“That’s an incredibly powerful thing,” said Ajay Agarwal, managing director of Bain Capital Ventures.

So powerful, in fact, it could dent Google’s GOOG, +0.39% $50 billion-a-year advertising business, said Larry Kim, founder and chief tech officer of Internet marketing software company WordStream.

However, the shopping portal has a ways to go before it can materially nudge Google off its advertising throne. Ads comprised less than 5% of Amazon’s $74.5 billion revenue last year, a much smaller margin than Google, which raked in some $59.8 billion in revenue comprised more than 93% of ads. Amazon also operates on notoriously razor-thin margins, whereas Google’s veteran ad business is highly profitable amid robust demand from advertisers.

Another potential disadvantage for Amazon is that it will likely only materially compete with Google on the e-commerce vertical, whereas Google also draws in major ad revenue from insurers like Progressive PGR, -0.12% and Geico, travel booking sites such as booking.com and Expedia EXPE, -2.27% , and for-profit educators like the University of Phoenix US:APOL .

“Don’t expect this to be a Google killer by any means,” Kim said.

The Uber of shopping

But it’s not a zero-sum game, said David Hirsch, managing partner at Metamorphic Ventures and former ad sales leader at Google.

As Amazon doubles down on ads, Google is investing in e-commerce, including adding same-day delivery for Google Shopping Express and pinging Android users when they are physically near a product they searched online.

Their business models continue to morph together in their battle to control the consumer, with the two now competing on various fronts spanning e-commerce, ads, mobile and gaming.

Hirsch says this is leading toward a future where products find customers, rather than the other way around, as loads of consumer data coupled with predictive analytics enable Amazon and Google to retarget ads and push out recommendations to potential buyers.

“The next version of search isn’t about people finding things, it’s about things finding people,” he said. “Google and Amazon are both in an interesting position to do this.”

Hirsch likened this to what Uber did to taxi driving. Whereas people used to hunt down a cab on the busy streets of Manhattan, they now plug in a request and wait for a car to be delivered to them.

In a sense, Amazon and Google want to similarly bring in-demand items to consumers, only they want to do it before the customer even realizes they want it.