Scientists have developed a testing system that can predict if teenagers will turn to binge drinking by the time they reach age 16.

An international group of scientists say that the testing system takes into account 40 variables and has an accuracy of 70 percent in finding out if 14-year-old teens will take onto binge drinking later in their lives.

Scientists included over 2,000 participants from Ireland, Germany, England and France, who were aged 14 years. The participants were given an assessment and were also observed for a few years. The test involved taking brain scans of the participants. The scientists suggest that the study cannot be conducted on a large scale due to the high costs related to the brain scans.

Some of the factors taken into account by scientists include the brain structure of an individual, personality, family history, certain events in life and more. Scientists revealed that teenagers with a bigger brain are more prone to binge drinking in the future. Usually, the brain size is reduced during puberty and is linked to immaturity.

The scientists also indicate that teenagers who lead stressful lives and have encountered major life changing events are also facing higher risks of binge drinking.

"Predictors for current and future adolescent alcohol abuse can be generated using machine learning methods that analyze multiple data sets - such as neuroimaging, family history and life events," says Robert Whelan of University College Dublin. "The final model was very broad - it suggests that a wide mixture of reasons underlie teenage drinking. That type of risk-taking behavior - and the impulsivity that often accompanies it - was a critical predictor."

The researchers also say that early drinking experience is one of the other factors considered to predict in a person will take on binge drinking in the future. Many parents allow their children to drink a bottle of beer or a glass of wine occasionally when they reach 14 years. Scientists believe that delaying drinking for even a few months or a year may affect a teenager's future drinking habits.

Suzanne Costello, Chief executive of Alcohol Action Ireland, also agrees that exposure to drinks at an early age may result in binge drinking in teens.

Governments spend a lot of money every year to tackle drinking abuse. The findings of the latest study can form a base for healthcare professionals and social activists to address drinking habits of teenagers at an early age.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.