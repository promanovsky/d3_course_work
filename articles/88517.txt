Samsung has officially slashed the price of its Galaxy Note 3 Neo to Rs. 33,990, down from the previous price tag of Rs. 38,990.

Notably, the Galaxy Note 3 Neo was launched in India alongside the Galaxy Grand Neo in February. However, the smartphone was released for the Indian markets only in March.

Like other members of the Galaxy Note phablet series, the Samsung Galaxy Note 3 Neo comes with an S Pen stylus. The Galaxy Note 3 Neo is powered by a hexa-core processor (dual-core 1.7GHz Cortex A15 + quad-core 1.3GHz Cortex A7). It runs Android 4.3 Jelly Bean out-of-the-box.

Also known as the 'budget' variant of the Samsung Galaxy Note 3, the Galaxy Note 3 No comes with a 5.5-inch HD (720x1280 pixels) Super AMOLED display; 2GB of RAM; 16GB inbuilt storage, which is further expandable up to 64GB via microSD card, and a 3100mAh battery. The phablet from Samsung sports an 8-megapixel rear camera with LED flash and BSI sensor, while there is a 2-megapixel front-facing camera also featuring BSI sensor. Connectivity options on the device include Wi-Fi, GPS, GLONASS, NFC, Bluetooth, Infrared and 3G.

The Samsung Galaxy Note 3 Neo comes with numerous preloaded apps: Samsung ChatON, Samsung WatchON, Samsung Link, Screen Mirroring, S Voice, S Health, Group Play, Smart Scroll, Smart Pause, Air View, Story Album and S Translator among others. The S Pen comes with Air Command and Easy Clip feature, and also supports Multi Window and S Note.

The Galaxy Note 3 Neo also features access to Club Samsung content store, where users can enjoy music, movies, mobile TV, concerts in nine Indian languages.

Earlier on Monday, Samsung Galaxy S5's official India price was confirmed as Rs. 51,500. The company confirmed to NDTV Gadgets that the phone will be priced at Rs. 51,500 and said this is the "best buy" price.