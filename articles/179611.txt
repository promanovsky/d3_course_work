Which of these plates looks like a better meal to you?

According to science, you'd probably be happier with meal No. 2, since it's more expensive. A new study out of Cornell University found diners tend to rate the quality of their food higher if they pay more for it, and people who pay less for the same exact meal report feeling more guilty, bloated and uncomfortable.

“We were surprised by the results. Especially, we found that pricing has little impact on how much one eats, but a huge impact on how one interprets the experience,” Brian Wansink, Ph.D., a co-author of the study and professor of consumer behavior at Cornell University, told The Huffington Post.

The researchers offered 139 diners an all-you-can-eat buffet at an upscale Italian restaurant for either $4 or $8 dollars. Though the diners ate the same food and the same amount, those who paid $8 rated their meal an average of 11 percent higher. The $4 diners were more likely to feel guilty and that they'd overeaten.

“If the food is there, you are going to eat it, but the pricing very much affects how you are going to feel about your meal and how you will evaluate the restaurant," Ozge Sigirci, one of the researchers who conducted the study, told Newswise.

The study adds to the body of research around how to maximize restaurants' bottom lines while managing customers' waistlines. Price cuts can actually hurt people's perception of a restaurant and cause diners to feel their meal was less worthwhile, the researchers found.

“Simply cutting the price of food at a restaurant dramatically affects how customers evaluate and appreciate the food," Wansink told Newswise.

For another example of this phenomenon, just consider lobster. Despite the fact that the price of lobster has plummeted to record lows, you'll rarely see those savings passed on in a restaurant. James Surowiecki, writing for the New Yorker, explains this paradox:

"High prices became an important part of lobster’s image. And, as with many luxury goods, expense is closely linked to enjoyment. Studies have shown that people prefer inexpensive wines in blind taste tests, but that they actually get more pleasure from drinking wine they are told is expensive. If lobster were priced like chicken, we might enjoy it less ... It’s a surprisingly complex attempt to both respond to and shape what customers want."

Putting an expensive item like lobster on the menu can also make it easier for restaurants to sell mid-tier items by making them seem cheaper by comparison. And when it comes to steep price cuts, diners can often react with suspicion.

The same phenomenon infamously occurs with wine. Knowing that people don't want to look cheap by ordering the least expensive bottle, restaurants will strategically mark up their "second cheapest" wine. The folks at College Humor even took on this well-documented economic trend with their trademark wit in a video:

“If you’re a consumer and want to eat at a buffet, the best thing to do is eat at the most expensive buffet you can afford. You won’t eat more, but you’ll have a better experience overall,” Wansink told Newswise.