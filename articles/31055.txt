MICROSOFT has skewered rival Google for going through customer emails to deliver ads, but it's acknowledging that it searched a blogger's Hotmail account to track down who was leaking company secrets.

John Frank, deputy general counsel for Hotmail's owner, Microsoft, said in a statement Thursday that the company “took extraordinary actions in this case.”

He says that in future, Microsoft would consult a former judge to determine if a court order would have allowed such a search.

Microsoft employee arrested for leaking trade secrets

The case involves former employee Alex Kibkalo, a Russian native who worked for Microsoft in Lebanon.

According to an FBI complaint against Kibkalo filed in federal court in Seattle on Monday, Microsoft found him in September 2012 after examining the Hotmail account of the blogger with whom Kibkalo allegedly shared proprietary Microsoft code.