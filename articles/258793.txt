By Stephanie Stahl

PHILADELPHIA (CBS) — Diet drinks verses water, which is best for weight loss? New research says diet drinks aren’t as bad as some might think.

Mark Huxsoll no longer fits in his old suit pants, after losing about 50 pounds. He was among a group of diet soda drinkers who participated in a 12 week weight loss study conducted by Temple University’s Center for Obesity Research and Education (CORE) and the University of Colorado.

Half of the 300 participants had to switch from diet drinks to water, the others including Mark, continued to drink at least two diet beverages a day. “I liked getting that sweeter taste and no guilt about that is helpful,” Mark said.

Dr. Sharon Herring with Temple University said, “The diet beverage group lost more weight, 13 pounds compared to 9 pounds in the water group.” Dr. Herring says the study shows diet drinks don’t interfere with weight loss as some earlier, limited research had indicated.

Mark and the others in the study kept detailed diaries and limited calories to no more than 1800 a day.

The new study was funded by the American Beverage Association, which has some questioning the reliability of the results. Dr. Herring says, “All of the results were by investigators, not the Beverage Association, the way the results are presented are from investigators not Beverage Association.”

Why did the diet drink group lose more weight? Theories include: the diet beverage drinkers reported feeling less hungry than those who drank only water and the water group may have felt more deprived and made up for that with a few extra treats.

For more information on the research, click here.