General Motors Co. announced another huge recall Friday, this time for almost 465,000 late-model Chevrolet Camaros sold in the U.S. that have an ignition problem similar to the one linked to a string of deadly accidents in older GM cars.

The automaker is recalling the 2010-14 Camaros because a driver’s knee can bump the key fob and suddenly shut off power steering, air bags and the power brake assist.

GM said it knows of three crashes, resulting in four minor injuries, that may be linked to the Camaro key fob.

The issue was discovered by GM during internal testing after its recall of 2.6 million older small cars this year for a faulty ignition switch linked to more than 50 crashes and at least 13 deaths. The switch in those cars could shift suddenly, also turning off critical safety functions.

Advertisement

The automaker knew about problem in the older cars for more than a decade before recalling the vehicles. It now faces ongoing investigations by the National Highway Traffic Safety Administration, the Justice Department and Congress into why GM delayed fixing the cars for so long.

But GM said the Camaro problem is unrelated to the system used in Chevrolet Cobalts and other small cars included in the ignition-switch recall. Unlike the ignition in the older cars, the Camaro system meets all GM engineering specifications, the automaker said. It also is a problem with the construction of the fob, rather than the ignition switch.

The automaker will fix the problem by changing the Camaro key to a standard design from one in which the key is concealed in the fob and is opened by pushing a button.

GM on Friday also announced smaller recalls of new Buick LaCrosse sedans as well as older Chevrolet Sonics and Saab convertibles.

Advertisement

This latest round brings GM’s total for the year to 38 separate recalls in the U.S. involving 14.4 million vehicles, a record for the automaker. Including these recalls, the auto industry has recalled about 25 million vehicles this year — about 1 out of 10 vehicles on the road in the U.S.

Jeff Boyer, vice president of GM Global Safety, said that discovering the Camaro problem and acting quickly “is an example of the new norm for product safety at GM.”

An GM internal probe into the delayed recall of older cars found a pervasive atmosphere of incompetence and neglect that led the company to allow the problem to fester for 11 years. GM faces more congressional hearings on the issue next week.

The automaker’s other recalls Friday included 28,789 Saab 9-3 convertibles from the 2004-11 model years because an automatic tensioning system cable in the driver’s side front seat belt retractor could break. GM said it learned of the problem from customer complaints to the National Highway Traffic Safety Administration. GM said it is unaware of any crashes, injuries or fatalities related to the problem. GM no longer owns Saab.

Advertisement

Dealers will replace the driver’s side retractor in the recalled vehicles. GM also will cover the same repair if needed for the passenger seat belt during the life of the vehicle.

The Chevrolet Sonic recall involves 21,567 cars from the 2012 model year equipped with a six-speed automatic transmission and a 1.8-liter four-cylinder engine. The cars can have a condition where the transmission turbine shaft fractures “as a result of a supplier quality issue,” GM said.

Dealers will replace the transmission turbine shaft. GM is unaware of any crashes, injuries or fatalities related to this condition.

GM said 14,765 model-year 2014 Buick LaCrosse sedans will have to be repaired because a wiring splice in the driver’s door may corrode and break, creating an electrical problem with circuits that control the door chime, passenger windows, rear windows and the sunroof. Dealers will inspect the driver door window motor harness and, if necessary, replace an electrical splice.

Advertisement

jerry.hirsch@latimes.com

Twitter @latimesjerry