There's now a way to predict which teenagers will become binge-drinkers, according to an international study led by researchers from the University of Vermont.

Published this week in the journal Nature, the findings are based on the most comprehensive imaging study yet conducted of adolescent brains, which revealed over 40 variables scientists can use to predict -- with about 70 percent accuracy -- which teenagers will be prone to binging.

Variables such as sensation-seeking behaviors, lack of conscientiousness and familial drug history provided some of the most reliable predictors, although whether an individual enjoyed even a single drink at age 14 also was a powerful indication of potentially problematic future consumption.

The study's first author, University College Dublin lecturer Robert Whelan, explained that he and his colleagues conducted 10 hours of examinations -- including "neuroimaging to assess brain activity and brain structure," along with IQ, cognitive task performance, personality, blood and other assessments -- on 2,400 14-year-olds at eight sites across Europe.

"Our goal was to develop a model to better understand the relative roles of brain structure and function, personality, environmental influences and genetics in the development of adolescent abuse of alcohol," Whelan said in a news release. "This multidimensional risk profile of genes, brain function and environmental influences can help in the prediction of binge drinking at age 16 years."

A 2012 Nature Neuroscience paper by Whelan and Hugh Garavan, the new study's senior author, identified "brain networks that predisposed some teens to higher-risk behaviors like experimentation with drugs and alcohol."

This research expanded on that work by following youths involved in the 2012 study for years and identifying those who became binge drinkers.

The 2014 effort's goal was to determine who would drink heavily at age 16 using only data collected at age 14. The result is a list of factors that appear to work well when combined but fail to individually predict risky behavior.

"Notably, it's not the case that there's a single one or two or three variables that are critical," Garavan said. "The final model was very broad -- it suggests that a wide mixture of reasons underlie teenage drinking."

Garavan also noted that bigger brains tended to belong to binge drinkers.

"There's refining and sculpting of the brain, and most of the gray matter -- the neurons and the connections between them -- are getting smaller and the white matter is getting larger," he said. "Kids with more immature brains -- those that are still larger -- are more likely to drink."

The researchers believe that by "better understanding the probable causal factors for binge-drinking, targeted interventions for those most at risk" may be developed.