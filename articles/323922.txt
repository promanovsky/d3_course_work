The next major release of Google’s mobile OS is now available to developers, Android 5.0 L, and the guys over at Ars Technica, have released a bunch of photos showing off some of the changes in the latest Android release.

The photo above shows the new home screen in Android 5.0, there are some small changes to the home screen, but the major changes come further on.

The next photo shows a new menu for recent applications, this works in a similar way to the Chrome for Android tab switcher, you can scroll vertically on your device, and swiping left or right will clear and item.

The next photo shows the new quick setting panel, which allows you to quickly change a number of things on your Android smartphone, like turn Airplane mode on and off, plus you have access to things like WiFi, Bluetooth, the Google Cast and more.

Android 5.0 L comes with a wide range of new features, it comes with over 5,000 new APIs for developers, and you can see some more photos of what is new in the latest version of Google’s mobile OS over at Ars Technica at the link below.

The new Developer Preview of Android 5.0 plus Google Play Services will be available to download later today, there is more information over at the Android Developers.

Source Ars Technica

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more