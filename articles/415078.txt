The “True Blood” series finale will air on Sunday, Aug. 24. But “Truebies” don’t have to wait until the weekend to get a bite of the final episode. HBO released a sneak peek clip from the upcoming episode, “Thank You.”

“World used to be a whole lot simpler, didn’t it?” Sookie asks Reverend Daniels. “Till a couple of years ago we thought there was no such thing as vampires. Let alone werewolves, or were-panthers, or shape shifters like that.”

“Uh huh,” he responds. “Why do you think these sermons of mine are getting harder and harder to write.”

But Sookie doesn’t think that he understands.

“You know what I am, right?” she questions him.

The reverend believes he does, answering that she’s a telepath. However, he’s surprised when Sookie breaks the news that she’s actually a faerie. Watch the sneak peek video below to see how the rest of episode 10 scene plays out:

As we previously reported, the Season 7 final -- which is also the “True Blood” series finale -- will find Sookie weighing her future “with and without Bill.” Eric and Pam will “struggle with their uncomfortable partnership with Mr. Gus” and Andy will come upon “an unexpected inheritance.”

Fans left off in episode 9 with Bill heading to Sookie’s house to discuss his decision regarding the Hep-V cure, and Bill meeting with the Yakuza about his involvement with Sookie. The episode concluded with Mr. Gus threatening to kill Pam and demanding Sookie’s address.

The “True Blood” series finale will air on HBO on Sunday, Aug. 24, at 9 p.m. EDT. What do you think will happen at the end of Season 7? Tweet your thoughts to @AmandaTVScoop.