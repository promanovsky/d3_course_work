Google has finally rolled out the long awaited XE16 KitKat update for Google Glass with a host of new features including Bluetooth 4.0 Low Energy, Photo Bundles, Photo sharing in Hangouts, new boot animation and voice commands.

According to Android Central, Google Glass users will be first updated to XE 12.1 which is the base firmware that essentially lays the platform for the big XE16 update.

Once the Kitkat installation begins, the entire process roughly takes 20 minutes to complete. Factory Restore and root bootloader images are not yet available for the new update on Glass Developers firmware page, as was the case with previous XE12 update.

As XDA Developers reports, the captured OTA update links from XE12 to XE12.1 and from XE12.1 to XE16 are now available, thanks to XDA Recognised Contributor TheManii.

Due to the lack of pre-rooted bootloader images, the official root solution is currently unavailable for the XE12.1 and XE16 updates. Those who missed the OTA update notification can still manually install the latest XE16 update on Google Glass via stock firmware thread on XDA Developers forum.

Here is the complete list of changes ported to the new XE16 update for Google Glass: