

Get out the European flags, pop open the Bucks Fizz, switch on a bit of ABBA... Eurovision weekend is upon us!

For the first time ever, Digital Spy has flown out to the Eurovision Song Contest to follow around the UK entry ahead of the big night on May 10.

Aside from stuffing our face with Danish bacon and Danish pastries in Copenhagen, we'll be filling this page with behind-the-scenes shots and backstage updates as Molly prepares to showcase 'Children of the Universe' to 125 million Europeans.

May 10, 11pm

Molly gave a great performance and made the UK proud this evening, but nothing was going to stop the fabulous Conchita Wurst. It's not over for our Molly, though. Not one bit. Find out what we can expect from her debut album later this year and her hopes for a Glastonbury appearance in our exclusive chat.



May 10, 5pm

Just three hours to go until the Eurovision Song Contest grand final 2014 here in Copenhagen. The stage they've constructed for this year's event is seriously impressive. It's housed in a traditional shipyard hall, which has been completely renovated over the past few months specially for tonight's event. The steel structure on stage is 20 meters high and is built up of 40 tonnes of steel. The stage floor is covered in LED plates and there are 32 projectors around the arena.



May 10, 3pm

Eurovision is famous for its flamboyant and quirky costumes, and this year is no different. Keep an eye out for French act Twin Twin, who look like the love children of Slash and LMFAO. Anyway, we've taken a look back at the most eye-catching fashion choices in Eurovision history. Who could forget Verka Serduchka?



May 10, 2pm

Molly is currently waiting to perform at the third and final dress rehearsal ahead of tonight's televised show. The backstage area is basically a MASSIVE marquee that will disappear on Monday. All 26 of the final artists' dressing rooms face into a square and Molly is housed in number 22. The final will begin at 9pm Copenhagen time, but at 8pm on BBC One back in the UK.



May 10, 10am

Today is the big day! When we recently caught up with Molly, we talked through some of the more memorable performances old and new. Can we just say, Lordi's 'Hard Rock Hallelujah' is still a tune. After watching the full run through last night, there are definitely more memorable moments to come in tonight's show.



May 9, 11pm

Good news from Copenhagen: Molly's jury performance was fantastic and had a great reaction from the crowd. Added pyrotechnics to go with the anthemic chorus makes it a great closing number.

Austria's Conchita Wurst had the audience on her side once again, but The Common Linnets from the Netherlands are quickly gaining favour. Sweden's Sanna Nielsen still seems to be leading the pack, but after Ruth Lorenzo's wet hair flipping and impressive belting range, we still wouldn't rule out Spain climbing higher.

May 9, 7pm

If you weren't already in a Eurovision mood, these pictures of dogs dressed up as European countries should get you right in the mood.



May 9, 6,30pm

Good news for Molly, the bookies have her has fifth favourite right now. Sweden are the hot favourites, while Austria are the biggest mover after Conchita Wurst's semi-final performance. Molly can be backed at 8/1 with Bet Victor, though. Worth a cheeky punt if you're into that sort of thing, surely?

Eurovision Song Contest 2014 - Outright Winner

Sweden - 11/4

Netherlands - 3/1

Austria - 3/1

Armenia - 5/1

UK - 8/1

Ukraine - 12/1

Hungary - 14/1

Greece - 20/1

Denmark - 20/1

May 9, 5pm

We've had a trawl through some nightmares of Eurovision past and found 12 "nul points" entries. The list obviously includes the United Kingdom's 2003 fright fest that was Jemini.



If you don't want to watch all the performances, this is the pick of the bunch. An absolute corker from Turkey in 1983. Just wait for the chorus.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

May 9, 4.30pm

Good news for Austria's Conchita Wurst. They are currently the most buzzed about act in Eurovision, just ahead of Poland and their impressive, ahem, milking techniques. Will the buzz translate to votes? Quite possibly, but Eurovision likes a great track as well as novelty so it's not all about causing a stir.



Molly is the sixth most talked about act. Not bad considering the UK weren't competing in the semi-finals. She looks well set for a solid finish on Saturday night. Hurrah!

May 9, 4pm

Molly just had her first performance on the Eurovision 2014 stage in Copenhagen. 'Children of t9he Universe' got one of the biggest reactions in the arena, which is great considering we're on last! Let's hope we have some of that 'golden X Factor slot' syndrome with tomorrow night's public vote.

Speaking of The X Factor, Ruth Lorenzo also got a great reaction for her first outing on the Eurovision stage for Spain. Wet-look hair and some big belting notes won her some fans.

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Molly's in full outfit for the dress rehearsal. Golden armour with soft feathers. It has a Khaleesi meets Katniss look, which is more than fine with us!

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

We're now backstage at Eurovision and all 26 performers are having a briefing on the opening segment. The UK will perform last at this year's show. May the odds be ever in our favour!

Backstage fact: Molly's dressing room is sandwiched between Hungary's and Belarus's.

May 9, 12.30pm

Molly is on voice rest ahead of tonight's jury vote, which counts for 50% of the final score. It won't be televised, but it's vitally important she does her best.

Blue's Lee Ryan fluffed his lines during their jury performance back in 2011, placing them near the bottom of the vote. Had that not happened, they could have finished third instead of 11th at the televised final the following night.

May 9, 12pm

Molly is currently in hair and make-up for the full dress rehearsal, which starts at 2pm Copenhagen time.

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Hallo! We've just landed in Copenhagen for this year's Eurovision Song Contest. We bumped into UK hopeful Molly in the hotel lobby, who said she's had a "crazy week" so far, and is sporting rather nice henna tattoos on her arms. We'll be catching up with her again in rehearsals very soon.

This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io