New York: Tibetans living on the “roof of the world” can thank an extinct human relative for providing a gene that helps them adapt to the high altitude, a study suggests.

Past research has concluded that a particular gene helps people live in the thin air of the Tibetan plateau. Now scientists report that the Tibetan version of that gene is found in DNA from Denisovans, a poorly understood human relative more closely related to Neanderthals than modern people.

Denisovans (de-NEE’-soh-vens) are known only from fossils in a Siberian cave that are dated to at least about 50,000 years ago. Some of their DNA has also been found in other modern populations, indicating they interbred with ancient members of today’s human race long ago.

But the version of the high-altitude gene shared by Denisovans and Tibetans is found in virtually no other population today, researchers report in an article released on Wednesday by the journal Nature.

That suggests that Denisovans or close relatives of theirs introduced the gene variant into the modern human species, but that it remained rare until some people started moving into the Tibetan plateau, said study main author Rasmus Nielsen of the University of California, Berkeley.

At that point, it conferred a survival advantage and so spread through the Tibetan population, he said in an email. It’s not clear whether the Denisovans were also adapted to high altitudes, he said.

The results show that as early members of today’s human species expanded outside of Africa and encountered new environments, they could call on their genetic legacies from other species, he said. That’s easier than waiting for a helpful genetic mutation to arise, he said.

The Tibetan plateau rises above 13,000 feet (4,000 metres) in elevation. The genetic variant helps survival there by affecting the amount of oxygen the blood can carry when a person is in thin air. Apart from Tibetans, it is found very rarely in Han Chinese and also exists in Mongolians and Sherpas, who are also related to Tibetans and may have picked it up relatively recently, Nielsen said. The researchers found no trace of it outside East Asia.

Todd Disotell, an anthropology professor at New York University who didn’t participate in the study, called the new work “one of the coolest scientific results I have seen in a while ... This is a slam-dunk case.”

David Reich, an expert on ancient DNA at Harvard Medical School, called the paper “important and exciting” in showing the gene came from an ancient human relative. But he said that relative could have been Neanderthals, who are also known to have contributed DNA to modern people.

Nielsen said the Tibetan gene variant doesn’t match any known Neanderthal DNA, but Reich said maybe scientists just haven’t yet found DNA from a Neanderthal who carried it.