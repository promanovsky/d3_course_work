U.S. stocks rose on Monday, with the S&P 500 netting its first five-day winning streak since October, as investors looked to a slew of quarterly earnings reports from companies including online film service Netflix.

"We're modestly higher because the worst-case scenario didn't happen over the weekend," said Art Hogan, chief market strategist at Wunderlich Securities, referring to geopolitical tensions involving Russia and Ukraine.

"And, on balance, earnings have been better than expectations," Hogan added.

"Investors were braced for poor results, and at least so far, the numbers have been better than feared. We'll have a little more information to act on after the close and into the morning as we get into company-specific results," said Lawrence Creatura, portfolio manager at Federated Investors.

Read MoreHere's what will drive stocks this week

Netflix, among the momentum names recently hit, reports after the close.

"Many companies in the hyper-extended corner of the technology sector were approaching valuations that were rarified, so it was probably natural and even healthy for those shares to retreat a bit," said Creatura.

Halliburton gained after the oilfield services company Athenahealth declined after posting quarterly earnings below expectations. Facebook gained after Goldman Sachs reiterated its buy recommendation. The social networking company is reportedly readying to unveil plans for a mobile advertising networking at the end of the month. Ford shares were slightly lower in the wake of a Bloomberg report that the auto manufacturer would name COO Mark Fields as successor to CEO Alan Mulally as soon as May 1.

Read More

Merger talks between Barrick Gold and Newmont Mining fell through in recent days, according to the Wall Street Journal, which cited unnamed sources. But shares of Newmont Mining rallied, while those of Newmont Mining fell, after Bloomberg News cited people with knowledge of the matter in reporting deal talks between the gold producers could be rekindled.