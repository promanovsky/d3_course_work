Internet retailer Amazon is launching a smartphone with a screen capable of displaying 3D images without the need for special glasses.

The company recently demonstrated the handset to developers in San Francisco and Seattle, according to the Wall Street Journal.

To stand out from an already highly competitive market dominated by Apple and Samsung, industry sources say the Amazon phone will have retina-tracking technology embedded in four front-facing cameras or sensors, to make some images seem 3D, similar to a hologram.

The technology would be a good match for gaming, an area of recent focus for the company.

Amazon is moving increasingly into the hardware market, seeking to boost its digital sales content. The retailer recently launched its $99 Fire TV video-streaming box to join its existing Kindle e-readers and Fire tablets.

Chief Executive Jeff Bezos has said he prefers Amazon to profit from customers buying services through Amazon hardware, rather than profit from the devices themselves.

A company spokesperson says it will soon begin distributing a wand customers can use to scan product barcodes at home to re-order goods without logging into their computers.

It hasn't been revealed what operating system the phone will use or which wireless carriers Amazon is working with. The Kindle Fire tablet and the Fire TV set-top box both rely on Google Inc.'s Android mobile operating system. However, Amazon created its own app store for the Kindle devices and does not offer access to Google's Play Store.

On Twitter, technology analyst, Kurt Marko, said: "So the ecosystem battlelines have been set [with] Amazon to enter the phone market. Which walled garden will flourish: Amazon, Apple or Google?"

Amazon has told one of its suppliers it expects the device to go into mass production later this month, with an initial order of 600,000 units. According to the Wall Street Journal, the company has lined up several display makers for the smartphone, including Japan Display, the maker of displays for Apple's iPhone 5C and 5S.