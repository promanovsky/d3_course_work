Jolie Lee

USA TODAY Network

NASA's Kepler space telescope discovered another Earth-size planet that is in the "habitable zone," a planet's distance from its star where conditions are ideal for liquid water.

This planet -- called Kepler-186f -- is the closest to Earth in size of all the previous discoveries of planets in this Goldilocks zone, said Elisa Quintana of NASA's Ames Research Center and the SETI Institute, lead author of the study reported in the journal Science.

The discovery of Kepler-186f is "a major step towards finding that Holy Grail planet" that is both close in size to Earth and orbiting around a star similar to the Earth's sun, Quintana said. Instead of calling it an Earth twin, NASA considers Kepler-186f an Earth cousin.

Here are five things to know:

1. The planet is 1.1 times the size of Earth. Past discoveries have been 1.4 times the size of Earth or larger, Quintana said.

2. Kepler-186f has a shorter orbital pattern: 130 days to circle its star, versus 365 days for Earth.

3. The star that Kepler-186f orbits is "smaller, cooler, dimmer" than the Earth's sun, said J.D. Harrington, NASA spokesman.

4. The mass and composition of Kepler-186f are unknown. But based on planets of similar size, it's likely the planet is made up of a rocky material, Quintana said.

5. The planet is 500 light-years from Earth.

Follow @JolieLeeDC on Twitter.