Brad Pitt and Angelina Jolie are teaming up on the big screen once again.

The engaged Hollywood power couple is set to star together in a new project written by Jolie, according to reports.

The project is said to be in the early stages and details are scarce, but insiders speculate it may be a relationship drama that Jolie wrote several years ago, per The Hollywood Reporter

If the film does indeed get made, it will mark the first time Pitt and Jolie appeared onscreen together since the 2005 action flick "Mr. & Mrs. Smith."

Jolie is currently working in post-production on "Unbroken," the survival tale she directed about Olympian and World War II hero Louis Zamperini. That film is slated to be released this Christmas. She'll also appear in theaters later this month in the Disney villain story "Maleficent."