Shutterstock photo

Investing.com -

Investing.com - Solid U.K. retail sales figures sent the pound firming against the dollar on Thursday despite upbeat economic growth and weekly jobless claims reports out of the U.S.

In U.S. trading on Thursday, GBP/USD was trading at 1.6617, up 0.21%, up from a session low of 1.6556 and off a high of 1.6647.

Cable was likely to find support at 1.6466, Monday's low, and resistance at 1.6597, Wednesday's high.

The pound shot up against the dollar after the Office for National Statistics revealed that U.K. retail sales rose 1.7% in February, taking back most of January's 2.0% decline, and up 3.7% from a year earlier.

Markets were expecting a 0.5% monthly increase a 2.5% on-year gain.

Core retail sales, which exclude automobile sales, jumped 1.8%, far outstripping forecasts for a 0.3% gain, after falling 2.0% in January.

The data eclipsed healthy growth and jobless claims reports out of the U.S.

The Commerce Department reported earlier that U.S. gross domestic product was revised up to 2.6% in the final three months of 2013, up from a preliminary estimate of 2.4%. Market expectations had been for an upward revision to 2.7%.

The report also showed that personal spending was revised up to 3.3% from 2.6% initially, the fastest rate of growth in three years, which drew applause from investors.

Separately, the Labor Department reported that the number of individuals filing for initial jobless benefits in the U.S. last week declined by 10,000 to a 311,000 from the previous week's revised total of 321,000.

Analysts were expecting jobless claims to rise by 4,000.

Thursday's sunny data fueled already growing opinions that a spate of disappointing economic indicators released earlier in the year were the product of rough winter weather and not due to an underlying decline in demand.

Elsewhere, sterling was up against the euro, with EUR/GBP down 0.59% at 0.8265, and up against the yen, with GBP/JPY up 0.30% at 169.72.

On Friday, the U.K. is to release data on the current account and final data on fourth quarter growth.

The U.S. is to round up the week with a report on personal spending and revised data on consumer sentiment.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.