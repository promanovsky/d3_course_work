Eli Blumenthal

Special for USA TODAY

Microsoft's Windows Phone still has a ways to go in closing the gap of popular apps available on iOS and Android, but it is making strides. Last week popular ride-sharing service Uber released their first app for Windows Phone, and Fitbit, makers of the popular wearable fitness trackers, released their first app for the platform Monday.

The new app allows users of Microsoft's mobile platform to use and sync Fitbit in nearly the same way Android or iOS users can. With the app Fitbit, users can track steps, miles and calories burned while users of the company's Aria Wi-Fi scale can sync their weight to the app.

Using the app requires the new Bluetooth LE support offered in Windows Phone 8.1, an update to Windows Phone 8 that also includes a new keyboard, battery saver mode and Cortana, Microsoft's new personal assistant. Microsoft has recently begun rolling out the update to all Windows Phone 8 phones.

Fitbit is the first company with a wearable app for Windows Phone and comes as Microsoft continues to play catch-up in the app and wearable space. It remains to be seen if other wearable companies like Pebble and Jawbone will release their own Windows Phone apps now that support for the Bluetooth LE technology used by wearable devices has come to the platform.

Microsoft is also expected to release its own wearable device later this year.