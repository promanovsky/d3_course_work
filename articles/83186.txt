MannKind Corp. 's ( MNKD ) shares were down more than 7% following the release of briefing documents ahead of an FDA advisory panel meeting where the company's diabetes candidate, Afrezza (inhaled insulin) will be reviewed. The FDA's Endocrinologic and Metabolic Drugs Advisory Committee meeting is scheduled for Apr 1.

MannKind is looking to get Afrezza approved for the improvement of glycemic control in adult patients suffering from type I or type II diabetes. The FDA has mentioned certain pharmacology issues regarding the safety and efficacy of Afrezza that are to be discussed at the upcoming meeting.

Regarding Afrezza's efficacy, the FDA stated in its briefing document that although the candidate demonstrated statistically superior HbA1c reduction compared to placebo, the candidate failed to demonstrate a higher HbA1c reduction compared to Novo Nordisk 's ( NVO ) NovoLog (insulin aspart) in type I diabetes patients. Moreover, the advisory committee was asked to put emphasis on the observed safety findings on Afrezza including pulmonary safety, lung cancer risk and disease specific safety issues.

The concerns raised by the FDA in its briefing document can be a warning ahead of the scheduled meeting. We note that MannKind has already received two complete response letters (CRL) in the past for Afrezza. While issuing the last CRL in Jan 2011, the FDA asked the company to conduct two phase III trials with the next-generation inhaler.

Although a final decision from the U.S. regulatory body is expected by Apr 15, 2014, a further delay in approval or even a third CRL cannot be entirely ruled out. MannKind is dependent on the success of Afrezza and an additional delay in gaining approval will weigh heavily on the stock.

The track record of previously approved inhaled insulin drugs like Pfizer 's ( PFE ) Exubera (the first inhaled insulin to be approved in the U.S. in 2006) has been pretty disappointing. Pfizer was forced to stop marketing the drug after disappointing sales of the drug. Exubera accounted for a mere $12 million in the first three quarters after launch. Pfizer also faced severe safety issues while marketing the drug.

Following the failure of Exubera, several other big companies discontinued their inhaled insulin programs citing lack of confidence in the regulatory environment and market prospects of an inhaled insulin product. Currently, we expect investor focus to remain on the outcome of the FDA advisory panel meeting.

MannKind, a biopharmaceutical company, presently carries a Zacks Rank #3 (Hold). Some better-ranked stocks in the same sector include Alexion Pharmaceuticals, Inc. ( ALXN ) with a Zacks Rank #1 (Strong Buy).

ALEXION PHARMA (ALXN): Free Stock Analysis Report

MANNKIND CORP (MNKD): Free Stock Analysis Report

NOVO-NORDISK AS (NVO): Free Stock Analysis Report

PFIZER INC (PFE): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.