From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

At just 9.1 millimeters thick, Microsoft’s Surface Pro 3 is the thinnest computer to ever pack in Intel’s Haswell desktop processors.

Its size and weight (just 1.7 pounds) were the most noticeable features when I first picked up the Surface Pro 3 yesterday. And after speaking with Microsoft’s Ed Giaimo, the general manager of the Surface Pro product line, it’s impressive that Microsoft managed to squeeze all of this into that thin metal case.

During yesterday’s Surface Pro 3 launch event, Giaimo gave me an overview of the Surface Pro 3’s innards. Its motherboard is actually half the size of the Surface Pro 2’s, which makes room for the battery and other components to sit side-by-side in the case. Microsoft also developed a custom fan because none of the existing options fit inside such a thin case.

I’m still testing out the Surface Pro 3 for my full review, but at this point, the slick design and bigger screen is winning me over far more than any previous models.