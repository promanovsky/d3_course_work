tech2 News Staff

Watch the live stream of the event below and join us past it for all the announcements since the event began. If you are joining us late, you can start from the bottom to see what's been announced so far.

11:30pm - And that's it folks

LG name drops the G Watch before wrapping up the main event. Time to take a closer look at the phone. Stay tuned for more on the LG flagship.

11:20 pm - A flatter UI, new software features

LG has completely revamped the UI of the G3 as compared to the G2. It's a lot flatter than anything from the company in the past and is seemingly inspired by the classic LG logo. LG is adding some new software features in terms of the virtual keyboard, smart notifications and security.

LG's Smart Keyboard allows users to customise the layout, most likely so that people can have it as big as they want. LG has added some gesture-based controls to quickly change typos, selecting words and other actions.

Secondly, we have Smart Notice, which is LG's way of taking on contextual or query-less searching. It will suggest users with information before you search for it. It sounds like a stretch at the moment, but LG's software will take care of weather notifications, traffic info. It will show you notifications for calling back when you dismiss calls when in a meeting, or to add newly called numbers to the address book. Additionally, it will also tell the users which apps they haven't used in a while so they can free up space.

And lastly, there's security. Knock Code, which we have already seen, lets you pre-assign a code so you can directly unlock the phone to your homescreen, but doesn't allow anyone else access. LG says there are 80,000 possible combinations. There's something called Content Lock, to keep your personal files safe, even when connected to a computer. And finally, there's LG's version of a remote device manager, which is just like any other offering in the market.

11pm - Fast autofocus

That laser autofocus is really fast it would seem. It is supposed to take around .276 seconds to focus on an object. It's said to be faster than the human eye. We are not completely sold on it till we see it in action next to what looks like a Galaxy S5. It has no trouble focussing, and seemingly beats the already fast S5. Let's see it in real life first though. The 13MP camera also gets enhanced OIS, so expect less shakiness in video and stills.

LG has done something for those selfie lovers too. Gesture shooting makes it easier to get the pose right, which we all know is the most crucial part in a selfie.

When it comes to videos, LG is touting adaptive recording when it comes to sound, reducing the ambient noise in any setting.

10:55 pm - Talking technical

LG is not sparing us any technical details when it comes to the display. It's going over the pixel density, the underlying RGB layout and the pixel size. It's also telling us about the power consumption optimisation. A lot of 'adaptives' thrown around, in typical marketing fashion.

For all the dull talk, we absolutely love the idea that a G3 will have a higher-resolution display than most TV sets in the world. Dying to see this wonder in real life. The G3's screen to body ratio is in the region of 75 percent, which is one of the highest we have seen so far.

10:45 pm - Gorgeous 2K display starts things off

The highlight feature of the G3 is the Quad HD display, which LG announced a while ago.

This is the first mainstream phone with such a high-resolution display. We'll learn more about the technical details, but for now here are the cold specs: resolution is 2560x1440 pixels, and the pixel density has breached the 500 mark and sits at 538 PPI.

Another highlight is the laser autofocus and we will find out more about this later in the evening.

10:40 pm - LG G3 is official

The G3 is official. LG has gone with 'Simple is the new smart' tagline and it sure does look it.

The first impression is of a very unassuming phone. But we all know the G3 is packed to the gills with top-end specs. Just a look at that display should be enough to silence most critics. LG is launching it in five colours to begin with: black, white, gold, red and blue.

10:30 pm - Moments away

We are just minutes away from the official announcement of the LG G3. But it's not like the phone will be a complete surprise. Through months of leaks, we know a lot about the G3 and here's a quick recap. The G3 is more than just a phone for LG. It's for the first time that an LG announcement has drawn so much attention. We are excited to see what LG has prepped for the flagship battle. We already know a lot, but as we so often see, the magic is in the details. We'll get to know about them soon enough.