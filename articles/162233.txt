"Human Barbie" Valeria Lukyanova has said some outlandish things of late, but now she’s back with a video. The bizarrely proportioned Ukrainian model just won’t go away!

In her new video, Lukyanova doesn't say anything, which is probably just as well because most of the things that came out of her mouth recently were offensive or bizarre.

In the new clip, which is more than four minutes long, the 28-year-old jogs on a beach to show off her body’s extreme proportions. She also nonsensically shows off her unreal waist size off in the mirror. The "Barbie Style" video already has gained more than 10,000 views since it was posted to YouTube on Wednesday.

Even though her look is extreme, Lukyanova has only admitted to getting breast enhancement surgery. She’s been trying a new look recently, posting barefaced selfies.

She also claims to try to survive on light and air alone. This type of diet even has a name: Breathatarianism. Participants apparently live on “cosmic micro-food.”

“The capacity to live on light is directly related to our ability to attract, absorb and radiate Divine Love, which is determined by our lifestyle,” writes Jasmuheen, a proponent of Breatharianism. “When we are well tuned and our body fit, we can access an inner power, which has the ability to love us, guide us, heal us and also nourish our cells.”

More recently, Lukyanova, who once claimed to be an alien, disturbed people when she talked about true beauty to GQ, and how it’s apparently hard for many people to achieve that these days.

"Ethnicities are mixing now, so there's degeneration, and it didn't used to be like that," she told GQ about modern beauty. "Remember how many beautiful women there were in the 1950s and 1960s, without any surgery? And now, thanks to degeneration, we have this."

Well, at least this time no one had to worry about her saying anything offensive. Her new video can be viewed below:

Follow me on Twitter @mariamzzarella