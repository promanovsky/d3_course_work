Are the millions of dollars spent to try to reverse childhood obesity a good investment? One answer might be found in the cost if the condition goes unchecked: about $19,000 per obese child in lifetime medical costs, researchers reported Monday.

That’s $14 billion just for the obese 10-year-olds in the United States, according to researchers at the Duke Global Health Institute and the Duke-NUS Graduate Medical School in Singapore. They reported their results in the journal Pediatrics.

A less costly estimate, $12,900 per child, was made for normal-weight children who gain weight in adulthood, they said.

For comparison, the researchers noted that the 2012 fiscal year budget for the Head Start Program was $7.8 billion, and the cost of a year at a public four-year college was almost $17,000.

Advertisement

“Reducing childhood obesity is a public health priority that has substantial health and economic benefits,” said Eric Andrew Finkelstein, the lead author of the study. He also said that efforts to reduce obesity should not be based solely on dollar savings.

To reach their estimates, the researchers analyzed and updated previous estimates by reviewing the scientific literature outlining the medical costs of childhood obesity over a lifetime. They took into account such factors as a decreased life expectancy for obese people and the rising of costs with age. Obesity is a risk factor of such illnesses as heart disease, Type 2 diabetes and some cancers.

While some progress was recently reported in some states to reduce childhood obesity, one in five children and one in three adults are obese, according to the U.S. Centers for Disease Control and Prevention.

The researchers noted also that the prevalence of extreme obesity, children who score at or above the 99th percentile of body mass index measures, is rising. And estimates are that half of the adult U.S. population could be obese by 2030, barring successful efforts to change.

Advertisement

Costs of controlling obesity are “significant,” the researchers noted, offering a couple of examples, including a 10-year, $1 billion program to build health-promoting communities by the California Endowment.

Knowing the costs of obesity is one way to justify the expenditures to prevent it, the researchers wrote. They measured direct medical costs of obesity, not costs of such things as lost or reduced productivity. They recommend additional research to account for those.

Mary.MacVean@latimes.com

Twitter: @mmacvean