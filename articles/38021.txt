Gold settles at lowest levels in five weeks

prices ended substantially lower at Comex on Monday, 24 March 2014. Gold prices fell by nearly 2% on Monday to settle at their lowest level in more than five weeks as some traders bet on a U.S. interest-rate hike as early as next year, which could boost the dollar and pressure dollar-denominated prices for the metal.

April gold fell $24.80, or 1.9%, to settle at $1,311.20 an ounce on the Comex division of the New York Mercantile Exchange.

May silver fell 24 cents, or 1.2%, to end at $20.07 an ounce, with prices marking their sixth session decline in a row.

The economic data point of the day Monday saw the China HSBC preliminary purchasing managers' index drop to an eight-month low of 48.1 in March from 48.5 in February. This continues a trend of weaker-than-expected economic data coming out of China and raises the question whether will China move to use monetary stimulus to boost its economy. The spate of weaker data from China has been a bearish underlying factor for many raw commodity markets.

The U.S. flash PMI index came in Monday at 55.5 in March versus 57.1 in February. That data had little impact on the market place. Meantime, the European Union's March manufacturing flash PMI came in at 53.2 versus expectations of a 53.3 reading and a February figure of 53.3. A PMI reading below 50.0 suggests contraction and above 50.0 suggests growth.

The Ukraine-Russia conflict is still on the radar screen of the market place. Any significant escalation of tensions between Ukraine and Russia would quickly put keener risk-aversion into the market place and would likely benefit safe-haven gold.

Powered by Capital Market - Live News