An Indian woman, inspired by the immense popularity of the ALS ice-bucket challenge, has come up with yet another bucket challenge, to bring in funds to help India’s poor.

Manju Latha Kalanidihi, a journalist from the city of Hyderabad in southern India, came up with the idea for a Rice Bucket Challenge that is growing in popularity across the country where more than 179.6 million people live below the poverty line, according to a 2014 World Bank survey.

The Rice Bucket Challenge has a Facebook page, which describes it as an “Indian version (of the ice-bucket challenge) for Indian needs” and has received more than 8,000 likes since the inaugural donation was made on Sunday, the Independent reported.

To take part in the charity challenge, a participant is required to take a bucket of rice to a person who needs it, click a picture of the donation and post the picture on Facebook along with the hashtag #RiceBucketChallenge while tagging and nominating friends to take up the challenge.

The super-popular ice bucket challenge, which involves dumping a bucket of ice water over one's head, has so far raised more than $70 million around the world in support of sufferers of amyotrophic lateral sclerosis, or ALS, as celebrities around the world have also embraced the idea that's gone viral over social media. ALS is a progressive neurodegenerative condition that affects nerve cells in the brain and spinal cord.

However, critics of the ice-bucket challenge have dismissed it as a gimmick as far more people die of other conditions such as heart disease and Alzheimer's than ALS, and it has been scorned by those living in developing countries where many people suffer from immense poverty and lack basic needs like drinking water, food and sanitation.