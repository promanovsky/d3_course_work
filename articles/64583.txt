Twitter made some big changes this week.

It killed Twitter Music after a year and is going with a new strategy. It's testing out killing the Retweet button and replacing it with a Share button (and everyone is freaking out, even though the two buttons do the same thing).

Advertisement

And it's rolling out a way to tag people in photos and add up to four photos in one tweet. This will be available on the iPhone first, with Android and Web users getting the feature later.Here's what it looks like:

It's easy to tweet just like Britney:

Advertisement





Advertisement

When you're done, hit the Tweet button to send your tweet.

Advertisement

Collage of animals because that's all I have in my camera roll. pic.twitter.com/Edzw5oNpgP - Karyne Levy (@karynelevy) March 27, 2014

Here's what the end result looks like: