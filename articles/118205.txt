Today, April 15th, anyone can buy Google Glass in the US and you don’t even need to line up in a store to do it. All you need to do is to hit Google’s Glass page and purchase the device from there. However, note that Google is allowing users to purchase Glass only for today, and only while supplies last.

Up until now only a select few were allowed to purchase Glass, but Google for some reason has removed this restriction only for today. If you’re not in the Explorers program, or didn’t get an invitation from a friend until now, then this is the right time to get your hands on this new wearable tech by Google.

Is It A Good Time to Be a Stock Picker? Interview With Worm Capital ValueWalk's Raul Panganiban interviews Eric Markowitz, Director of Research, and Dan Crowley, Director of Portfolio Management, at Worm Capital. In today’s episode they discuss their approach at Worm Capital and where they find opportunities. Q4 2020 hedge fund letters, conferences and more Interview with Worm Capital's Eric Markowitz and Dan Crowley

Google Glass available buyers in US

Google is allowing anyone in the US to get Glass, provided you’re ready to shell out $1500. Along with the Glass, you’ll also get a spectacle frame or sunglass-style shade included in the package. For now, there’s no indication on how many units Google has in stock for sale, but stocks are limited, and might be sold out too soon. Of course, you need to be at least 18 years old to buy Glass.

“Every day we get requests from those of you who haven’t found a way into the program yet, and we want your feedback too,” Google said in a post on Google+ last Thursday. “We’re excited to meet our new Explorers, and we can’t wait to hear your thoughts about Glass.”

Google Glass update

Google will also be pushing new KitKat update to Google Glass next week. This new update will bring improved battery life, new photo sharing feature in hangouts and an updated SDK for developers. Recently Google removed the ability for video calls, citing a poor user experience. As per Google, only 10% of Glass Explorers use video calls.

So are you going to be buying Glass? Do you think that Glass will usher in a new era of wearable devices? Let us know your thoughts in the comments below.