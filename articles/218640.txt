Ann Oldenburg

USA TODAY

She was the no-nonsense woman who put Juan Pablo in his place during the last season of The Bachelor.

Now, Andi Dorfman embarks on her own love quest in Season 10 of The Bachelorette, premiering on Monday (9:30 p.m. ET/PT on ABC).

In a press call Wednesday, the Atlanta assistant district attorney said she realizes that "America was left with the impression that I'm a tough, no-nonsense girl. (But) you're going to see a softer side of me. I was a lot more vulnerable than I ever thought I'd be."

Dorfman, 26, didn't gush too much or let slip whether she has a ring on her finger. "I'll just say I'm very happy. America's about to meet a bunch of great guys. I'm really excited to put it out there and let everyone see this journey."

When we asked about the overall experience, she said, "It was incredible. A once-in-a-lifetime experience."

Other highlights from the chat:

On her dad, who was tough in The Bachelor: "My dad's a reasonable guy and you'll see a softer side to him for sure, but he's always going to be my dad and always going to be protective."

On what she's looking for: "A family-oriented person. I definitely want a family some day. Also, a guy that's strong and confident, and able to share his feelings with me. Communication was definitely something I was looking for in a guy coming into this."

Did it make her understand Juan Pablo a bit better? "It's definitely tougher than I thought. It's not easy to play host and be responsible for a lot of people. I wanted to make sure I knew as much as I could about each and every one of these individuals."

On looking for a Jewish man because she's Jewish: "I am definitely proud of my heritage and I grew up with great traditional and cultural things. ... I'm really looking for somebody that's kind and I get along with and completes me."

On the death of bachelor Eric Hill during filming: "Obviously it had a huge effect on all of us. We heard the news and it was just crushing. ... He became a part of our family."

On how she felt when the first limo pulled up on premiere night: "Oh my gosh, my heart was pounding out of my chest. Standing in front of that house was so surreal. I didn't know if I was going to faint, or if I was going to be able to talk."

Once the season starts, be sure to check back on USA TODAY's Life section for a recap of the previous night's, um, rosy, events!