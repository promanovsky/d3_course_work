We have already seen some photos of the new LG G3, and now we have a couple more of the handset with a case on the device, the photos apparently came from a smartphone accessory maker.

The LG G3 is said to come with a 5.5 inch full high definition display with a resolution of 2560 x 1440 pixels, the handset will apparently feature Qualcomm’s latest mobile processor.

Under the hood of the LG G3 will be the new Snapdragon 805 processor with a clock speed of 2.5GHz, the device is also said to come with 3GB of RAM.

There are no details on what storage will be available with the handset, although we suspect it will come with a few options, plus a microSD card slot.

The LG G3 is expected to feature a 2 megapixel front facing camera and a 13 megapixel rear camera, the device will apparently come with Android 4.4 Kit Kat.

LG are holding a press event next month on the 27th of May, the company is expected to make the new LG G3 official and this event, and we are also expecting to find out more information about the new LG G Watch.

Source ortud , GSM Arena

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more