As Windows XP winds down, Microsoft has a problem: People still use the operating system. Many people. Given that support for Windows XP ends on April 8, the number of days left in which the OS is mostly safe to use are all but spent.

To combat that, Microsoft has been offering discounts, in increasing increments, to induce consumers to get their backsides off the bench and into a computer that will be safe. So, Microsoft is handing out $100 discounts on a number of computers, including the Surface Pro 2. How is this economically feasible? All the machines cost $599.

As ZDNet’s Mary Jo Foley points out, the new computers come with “90 days of free support for their new Windows 8 devices from Microsoft.” Call that the Here’s How New Windows Works period.

The danger here is that, once general support for Windows XP ends, everyone not paying Microsoft large sums for custom support will be using an un-patched operating system that is incredibly insecure. Old code without patches means exploit heaven. Consumers at risk of being attacked isn’t great if you want to keep them as customers.