Beyonce Goes Apartment Hunting in Chelsea Without Jay Z? Beyonce Goes Apartment Hunting in Chelsea Without Jay Z?

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Beyonce was recently spotted apartment hunting in New York amid claims that she and Jay Z are having marriage problems.

The "Partition" singer, 31, reportedly looked at a $21.5 million Chelsea penthouse last month, but Jay Z, 44, was not present. The couple is currently in the midst of their "On The Run" tour and they have been subject to ongoing split reports in recent months.

"She was very quiet, as if she was looking on the sly," a source told the New York Post's Page Six. "There's no way a $20 million apartment is for her mother or her sister. That would be wildly unlikely."

In May, the hip-hop power couple sparked reports of marital woes due to Jay's infamous elevator incident involving Beyonce's sister Solange.

While they all insist they have moved on from what occurred, insiders claim the incident is a reflection of the problems within Beyonce and Jay's marriage.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

A new report claims the pair will announce their separation after their current tour ends but the couple has yet to publicly address split rumors.

"They are trying to figure out a way to split without divorcing ... This is a huge concert tour and they've already gotten most of the money from the promoters up front," a source told the New York Post's Page Six. "There are no rings, if you haven't noticed."

In May, photos showing Beyonce's faded ring tattoo surfaced online which did little to dispel strained marriage rumors. The "Drunk In Love" singers wed on April 4, 2008 and they have matching tattoos on their ring fingers of Roman numerals "IV" for the number 4 to signify their union.

"Jay's been telling his friends that Beyonce is getting her ring tattoo removed, the one they got when they were married," a source told RadarOnline.com.

"He just sorta shrugs it off though saying 'Nothing is forever,'" the source added.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit