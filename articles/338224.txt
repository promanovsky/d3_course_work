A medical group has issued new guidance regarding pelvic exams for women. (Credit: CBS 2) A medical group has issued new guidance regarding pelvic exams for women. (Credit: CBS 2)

NEW YORK (CBSNewYork) — A medical group has made a controversial recommendation regarding women’s health, claiming that the long-held practice of the annual pelvic exam may do more harm than good.

As CBS 2’s Dr. Max Gomez explained, the annual gynecological exam involves an inspection of the uterus and the ovaries.

“It was pretty easy, smooth, basic, She did the pelvic exam, a swab, a pap smear,” patient Amy Yee said.

But based on a review of 60 years of scientific studies, The American College of Physicians has recommended against pelvic exams for most women.

“[A pelvic exam] rarely detects important disease and does not reduce mortality and is associated with discomfort for many women, false positives, and negative examinations, and extra cost,” the guideline said.

The guideline applies to women who are not pregnant, those who are at average risk for cancer, and women who do not show any symptoms.

However, many gynecologists say pelvic exams can help detect common problems like uterine fibroids and endometriosis.

Dr. Taraneh Shirazian at Mt. Sinai Hospital said that she still plans to perform the exams despite the new recommendation, and that the American College of Obstetricians and Gynecologists continues to recommend the annual pelvic exam in its guidelines.

“For the vast majority of women that I’ve seen, they don’t express any you know large issue with having the exam performed. And really very many are very grateful to have an assessment of the uterus and ovaries done,” Dr. Shirazian said.

Yee agreed and said that she wants to keep getting the exam for the reassurance it provides.

Pelvic exams should be continued for women with problems such as abnormal bleeding or pelvic pain, according to the new guidance, but critics point out that those problems are often discovered for the first time in low risk women having a routine pelvic exam.

You May Also Be Interested In These Stories

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]