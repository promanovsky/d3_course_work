As Diablo 3 gamers have probably heard by now, the Auction House will be closing on the 18th of March and true to their word, it looks like Blizzard has since taken steps to deactivate the Auction House.

Advertising

As you can see in the image above, Blizzard has kept the Auction House up and running but has since removed the ability to bid, buyout, or to list new auctions. Essentially what the Auction House is good for now is to help clear up any remaining auctions that are still running at the moment.

According to the red words placed beneath the Auction House UI, it reads, “The Auction House is shutting down. You must claim your items from the Completed tab by 6/24. Existing auctions will complete based on the time remaining.”

Gamers who have placed items up for sale will eventually be able to claim them from the Completed tab as well as any gold that they might have gained or been refunded from auctions. However it should be noted that this will only be valid until the 24th of June, meaning that if you do not collect your completed items or gold in a timely manner, they will be gone for good, or according to Blizzard, consumed by Treasure Goblins.

Naturally Blizzard couldn’t remove the Auction House just like that since it will be quite a shock to gamers, so with the 24th of June cut off date, we expect the Auction House window to remain around to allow gamers to collect any items or gold they might have left.

The decision to close the Auction House was made last year where it was believed to have undermined the game as opposed to helping it grow. According to Blizzard’s John Hight, the production director for the upcoming Reaper of Souls expansion, “It became increasingly clear that despite the benefits of the Auction House system and the fact that many players around the world use it, it ultimately undermines Diablo’s core gameplay: kill monsters to get cool loot.”

So, who else is sad to see the Auction House go? Or are you happy that it will finally be going?

Filed in . Read more about Blizzard and Diablo 3.