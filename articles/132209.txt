Earnings, hopes for end to Ukraine crisis send U.S. stocks rising

Investing.com - U.S. stocks finished Thursday mixed to higher, buoyed by solid data and first-quarter earnings, while rising hopes for a way out of the Ukraine crisis boosted share prices as well.

At the close of U.S. trading, the fell 0.10%, the index rose 0.14%, while the index rose 0.23%.

General Electric Company ( ) and Morgan Stanley ( ) released earnings that topped Wall Street expectations and lifted stocks up, though disappointing results from Google Inc ( ), International Business Machines ( ) and elsewhere tempered the rally, which allowed the Dow Jones Industrial Average to end in slightly negative territory.

Meanwhile, hopes for an end to the Russian standoff over Ukraine allowed stocks to post modest gains.

The U.S., Russia, the European Union and Ukraine held crisis talks in Geneva on Thursday, and afterwards, diplomats from all sides agreed on steps to ease tensions that include demobilizing militias, vacating seized government buildings and opening future conversations to address autonomy for Ukraine's regions.

Positive U.S. data supported stocks as well.

The Federal Reserve Bank of Philadelphia reported earlier that its manufacturing index rose to 16.6 in April, the highest level since September, from 9.0 in March. Analysts had expected the index to tick up to 10.

Separately, the Labor Department reported that the number of individuals filing for initial jobless benefits in the week ending April 12 rose by 2,000 to 304,000, better than analysts' forecasts for a rise to 315,000.

Leading Dow Jones Industrial Average performers included General Electric Company ( ), up 1.74%, Chevron Corporation ( ), up 1.51%, and Boeing Company ( ), up 1.50%.

The Dow Jones Industrial Average's worst performers included International Business Machines ( ), down 3.21%,UnitedHealth Group Incorporated ( ), down 3.08%, and American Express Company ( ), down 1.36%.

European indices, meanwhile, finished higher.

After the close of European trade, the rose 0.52%, France's rose 0.59%, while Germany's rose 0.99%. Meanwhile, in the U.K. the rose 0.62%.