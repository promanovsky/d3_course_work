NEW YORK (TheStreet) -- Boeing (BA) - Get Report shares appear ready for a flight higher following the company's announcement that it sold 150 model 777X airplanes to Emirates Airline. The total list price for all of those planes is $56 billion.

On July 7 I wrote, "Dow 30 Higher as Earnings Begin," and in this post I said that Boeing was the weakest of all Dow Jones Industrial Average components. As of July 3, its shares had lost 5.8% since the start of the year.

Although the Emirates Airline order will not affect Boeing's second-quarter results, it should be talked about in the company's forward guidance. The company plans to report quarterly results before the opening bell on July 23.

Investing 101: Refine Your Research and Focus, Focus, Focus

Greenberg: Maybe It Wasn't the Weather, After All

Analysts expect Boeing to report earnings per share of $2.00. This is up a penny from the estimate shown in my July 7 post. On June 23 the company announced a 73 cent-per-share dividend payable on Sept. 5 to shareholders of record on Aug. 8.

The stock is up 21% over the last 12 months and has a price-to-earnings ratio (based on trailing 12-month earnings) of 17.0. TheStreet Quant Ratings has a buy rating for Boeing.

Let's take a look at Boeing's daily chart:

Courtesy of MetaStock Xenith

Boeing opened at $128.31 this morning but faded to as low as $126.04 as of 10:30 a.m. The stock remains below its 21-day, 50-day and 200-day simple moving averages at $130.26, $131.64 and $129.84, respectively. The green line is the 200-day.



The study below the daily bar chart is a 12x3x3 daily slow stochastic. This measure of momentum shows a reading of 21.46 moving above the oversold mark of 20.00.

An earnings beat and positive guidance should move Boeing back above these moving averages.

Let's take a look at Boeing's weekly chart:

Courtesy of MetaStock Xenith

The weekly chart for Boeing is negative with its five-week modified moving average at $129.93 and its 200-week simple moving average shown in green at $87.65. The 12x3x3 weekly slow stochastic is declining at 52.00.

Investors looking to buy Boeing shares should consider using a good 'til cancelled limit order to buy weakness to its semiannual value level at $122.99.

Investors looking to sell Boeing shares should consider using a good 'til cancelled limit order to sell strength to semiannual and monthly risky levels at $132.27 and $134.22, respectively.

At the time of publication the author held no positions in any of the stocks mentioned.

Follow @Suttmeier

This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff

Now let's look at TheStreet Ratings' take on Boeing stock.

TheStreet Ratings team rates BOEING CO as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate BOEING CO (BA) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its revenue growth, good cash flow from operations, largely solid financial position with reasonable debt levels by most measures, solid stock price performance and notable return on equity. We feel these strengths outweigh the fact that the company has had sub par growth in net income."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

BA's revenue growth has slightly outpaced the industry average of 3.2%. Since the same quarter one year prior, revenues slightly increased by 8.3%. This growth in revenue does not appear to have trickled down to the company's bottom line, displayed by a decline in earnings per share.

Net operating cash flow has significantly increased by 112.21% to $1,112.00 million when compared to the same quarter last year. In addition, BOEING CO has also vastly surpassed the industry average cash flow growth rate of 44.19%.

BOEING CO's earnings per share declined by 11.1% in the most recent quarter compared to the same quarter a year ago. This company has reported somewhat volatile earnings recently. But, we feel it is poised for EPS growth in the coming year. During the past fiscal year, BOEING CO increased its bottom line by earning $5.97 versus $5.12 in the prior year. This year, the market expects an improvement in earnings ($7.68 versus $5.97).

The debt-to-equity ratio is somewhat low, currently at 0.62, and is less than that of the industry average, implying that there has been a relatively successful effort in the management of debt levels. Even though the company has a strong debt-to-equity ratio, the quick ratio of 0.38 is very weak and demonstrates a lack of ability to pay short-term obligations.

Return on equity has greatly decreased when compared to its ROE from the same quarter one year prior. This is a signal of major weakness within the corporation. When compared to other companies in the Aerospace & Defense industry and the overall market, BOEING CO's return on equity exceeds that of the industry average and significantly exceeds that of the S&P 500.

You can view the full analysis from the report here: BA Ratings Report

Richard Suttmeier is the chief market strategist at

ValuEngine.com

.