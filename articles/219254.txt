Too much exercise could be linked to an increased risk for heart failure, according to a new study from researchers at the NYC Langone Medical Center. For the study the researchers observed 1,000 patients that had been diagnosed with heart disease but were in stable condition.

About 40 percent of all the participants exercised an average two to four times a week. Of the remaining 60 percent, half exercised more, and half less. They found that the most sedentary participants were the most likely to have a heart attack, but those who exercised the most were the second most likely.

"Moderate physical activity is the most protective for the heart," the researchers explained. "If you're a couch potato, that's something that's particularly dangerous for your heart. You're going to have a higher risk for heart attack and a lower survival rate. You don't need to super-size your exercise because we find those people have also a higher risk of having heart problems."

For comments and feedback contact: editorial@rttnews.com

Health News