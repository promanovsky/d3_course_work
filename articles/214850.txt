America Ferrera has responded to an incident during a recent red carpet walk at Cannes.

The actress was posing for photos at the premiere of How to Train Your Dragon 2 when notorious red carpet prankster Vitalii Sediuk crawled under her white gown.



Sediuk was quickly apprehended and booted from the event shortly after the incident occurred, though Ferrera was initially unaware of the man's actions.

Speaking about the incident to The Hollywood Reporter, Ferrera said: "It's fine -- I'm over it."

Ferrera explained that her cast mates, including Cate Blanchett, all checked in on her afterwards.



"They were all so supportive," she said. "They all just wanted to know I was okay."

How to Train Your Dragon 2 hits cinemas on June 13.

Watch a trailer for How to Train Your Dragon 2 below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io