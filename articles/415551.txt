Beyoncé performed on stage at the Video Music Awards in a multicolored mosaic bodysuit by designer Tom Ford. Her magnificent booty gyrated and her powerful legs twisted to the beat of her prodigious pop oeuvre with Bob Fosse-like precision and sex appeal. Her mane of chestnut hair blew in a wind kicked up by her own energetic head-snapping. The dancing was dynamic as shirtless men strutted in baggy shorts and women in glorified underwear slithered around stripper poles. The entertainment was off the charts; the talent was obvious; the raunchiness was all in good fun — because who can resist a troupe of metallic gold-painted, perfect female tushes all humping the floor in unison.

Then things got all gummed up with a single, distracting word.

For a jarring moment, Beyoncé paused — and posed with hip jutting out — silhouetted against a one-story tall, one-word declaration: Feminist.

The scene was paired with the spoken words of novelist Chimamanda Ngozi Adichie, in which the author laments that “girls cannot be sexual beings the way boys are.” It’s a powerful and complicated statement plopped down about two-thirds of the way through Beyoncé’s MTV song and dance and not long after thonged women have swished their derrieres in unison and viewers may still have been trying to figure out how Beyoncé could be wearing a leotard that appeared to be completely bedazzled with bits of colored glass — including all the way down there.

There are no rules that say a woman cannot be a feminist and still begin a stage performance with her legs spread wide and end it having gone all weak in the knees over motherhood and marriage. There is a long history of female empowerment through sexual provocation. And railing against the patriarchy is not a feminism requirement. In fact, what makes Beyoncé’s embrace of feminism so interesting is her willingness to play to the male gaze, proclaim her right to strut and do it all while master of her own growing bank account.

It’s just that in the context of a 16-minute mash-up that had “Yonce down on her knees,” Beyoncé swerving on her “surfbort” and later consorting with a chaise, a little elucidation on her definition of “feminist” would have elevated the experience.

As Beyoncé expertly brought her visual album to life on the MTV stage, her dancers were particularly mesmerizing to watch. Sometimes they were styled like ghostly apparitions. At other moments, their faces were trussed up like denizens of an S&M club. Sometimes they were done up like a bunch of guys from the neighborhood, with the women in cut-off jeans and the guys in gold chains. With their ever-changing look, the dancers said a lot about the shifting tone of the performance, the many postures of Beyoncé. The star, however, looked the same throughout.

Indeed, Beyoncé almost always looks the same. Her signature performance style is a leotard that comes up high on her hips and is bejeweled, embroidered or inset with lace. She wore one to perform at the Super Bowl; she wears them on tour. Over the years, they have become more elaborate — or specifically, more expensive, as they have been created by designers such as Givenchy and now Ford.

A performer can’t stand in the middle of the stage a deliver a treatise on equal rights or sexuality — how godawful ponderous would that be. But they can use their costuming to provide a fuller subtext to all the messages flying during a concert at the speed of a laser light show.

Beyoncé’s bodysuit hugged her curves in all the right places, but as a costume that could have given her performance the kind of nuance and thought-provoking context that she seemed intent on delivering, it was a missed opportunity. Not a lot of performers are willing to delve into the intellectual waters of feminism and make an effort to incorporate such complexity into a stage performance. But rather than simply slinging the word onto a backdrop as a graphic statement — or as a kind of disclaimer — why not weave the message through the entire performance with inspired costumes and styling?

That bodysuit made Beyoncé look great. It made her look like the Queen Bey that her fans know and adore. But in the context of visual story-telling, it did nothing to move her narrative forward.

READ MORE: