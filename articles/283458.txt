Roger Yu

USA TODAY

The Federal Communications Commission has begun reviewing who's at fault in Netflix's video streaming battle with Internet service providers, an indication that the issue may be subject to broader regulatory oversight as the agency recasts new net neutrality rules.

Comcast, Netflix and Verizon Communications have agreed to the FCC's review, and the agency will ask other companies, the agency said.

Netflix recently struck deals to pay Comcast and Verizon to connect its servers directly with the Internet providers' networks to improve steaming speeds. Netflix has said it signed the deals reluctantly.

Earlier this month, Netflix also began issuing messages directly to some Verizon customers on its buffering screen, shortly before the movie starts, saying that Verizon's crowded networks are to blame for slow streaming speeds. Verizon responded with a cease-and-desist letter, and Netflix agreed to pull the message.

"We don't know the answers, and we are not suggesting that any company is at fault," FCC Chairman Tom Wheeler said in a statement Friday. "The bottom line is that consumers need to understand what is occurring when the Internet service they've paid for does not adequately deliver the content they desire."

"I have experienced these problems myself and know how exasperating it can be," he said. "What we are doing right now is collecting information, not regulating. We are looking under the hood."

The FCC has been receiving comments and consumer feedback as it takes steps to adopt new net neutrality, or Open Internet, rules that will mandate Internet providers to refrain from discriminating against various types of legal content by blocking it or lowering transmission speeds.

Wheeler highlighted a consumer e-mail that he said summarizes general confusion over Netflix's kerfuffle with Verizon about its on-screen message. The e-mail read: "Is Verizon abusing Net Neutrality and causing Netflix picture quality to be degraded by 'throttling' transmission speeds? Who is at fault here? The consumer is the one suffering! What can you do?"

In a statement, Netflix said it "welcome(s) the FCC's efforts to bring more transparency."

Consumer advocacy groups applauded Wheeler's move and said the financial agreements between Netflix and Internet providers have not been clearly explained.

"The truth of the matter is that anyone not directly involved with a specific agreement has no way to know if the agreement represents a reasonable agreement between the parties or a degradation of the open Internet," said Michael Weinberg, vice president of consumer technology advocacy group Public Knowledge.

In formulating net neutrality rules, the FCC has focused on the Internet pipes between the broadband provider and consumers' homes, which is often called "the last mile." But content distributors rely on other networks to move their video or audio files way before they reach the last mile. They send their content to third-party distributors, which then deliver it to Internet providers' networks.

Netflix has relied on such agreements for years. In recent months, Netflix sought to lessen its dependence on third-party distributors by urging cable companies to let Netflix's servers be placed directly in their networks. Several cable companies, including Cablevision and Cox Communications, agreed to do it for free. Comcast and Verizon refused until Netflix agreed to pay for it.

"Internet traffic exchange has always been handled through commercial agreements," said Verizon spokesman Bob Elek. "This has worked well for the Internet ecosystem and consumers. We are hopeful that policymakers will recognize this fact."