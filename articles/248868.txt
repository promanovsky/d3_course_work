The Cannes Film Festival wrapped up Saturday night after 11 days of films with a closing ceremony. According to The Hollywood Reporter, the Palme d'Or went to Winter Sleep.

Winter Sleep was the creation of filmmaker Nuri Bilge Ceylan. The film is a meditation on marriage and is reminiscent of his earlier films, like Clouds of May. Ceylan dedicated his award "to the young people of Turkey who have lost their lives during the last year" and thanked everyone for sitting through such a long film (with a running time of 3 hours and 16 minutes).

Click here to read the full article.

The runner up and winner of the Grand Prize was The Wonders, by Alice Rohrwacher, which is a coming of age story set in Italy.

The Jury Prizes were awarded to Xavier Dolan for Mommy and Jean-Luc Godard for Goodbye to Language.

Other awards went to Bennett Miller for Best Director (Foxcatcher), Andrey Zvyagintsev and Oleg Negin for Best Screenplay (Leviathan), Julianne Moore for Best Actress (Maps to the Stars), Timothy Spall for Best Actor (Mr. Turner), and Marie Amachoukeli, Claire Burger and Samuel Theis for Camera d'Or (Party Girl).