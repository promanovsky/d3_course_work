Two of the year's most anticipated games close out the month of May; while Wii U owners miss out on Watch Dogs, the arrival of Mario Kart 8 should keep them busy for a long time.

Watch Dogs

Release date: May 27 (worldwide)

Platforms: PS4, Xbox One, PS3, Xbox 360

Following a six-month delay and thousands of tweaks, Ubisoft is finally set to release their open-world creation.

Set in a fictitious, technology-driven version of Chicago, the single-player story follows Aiden Pearce, an immensely skilled hacker who can hack into the city's centralised system.

Having received more than 170 pre-release awards and nominations, big things are expected from Ubisoft's new release.

Our Watch Dogs preview: Hands-on with hacking, hallucinations, multiplayer

Mario Kart 8

Release date: May 30

Platforms: Wii U

The Wii U debut gives the fan-favourite racer a high-definition makeover and the addition of anti-gravity courses, where karts can race upside or up vertical services for innovative new track ideas.

As with previous Mario Kart games, there is a mix of 32 new and classic tracks, as well as a host of local and online multiplayer options.

Our Mario Kart 8 review: The best looking, most dazzling entry yet

Also out this week:

Mind Zero (Vita) - May 27

Worms Battlegrounds (PS4, Xbox One) - May 30

What games are you buying this week? Add a comment below!

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io