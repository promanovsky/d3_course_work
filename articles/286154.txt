Medical-device maker Medtronic Inc. is in advanced talks to combine with rival Covidien PLC in a deal valued at more than $40 billion, according to people familiar with the matter.

The deal, which could be announced Monday, would be structured as a so-called tax inversion, according to one of the people.

In such deals, acquirers buy companies domiciled in countries with lower corporate tax rates than their own as a means of lowering their overall rate. Covidien US:COV is based in Ireland, which is known for having a relatively low tax rate.

There is no guarantee the talks won’t fall apart at the 11th hour.

There has been a flurry of merger activity lately among big medical-device makers as they seek to bulk up and lower costs as the overhauling of health care puts pressure on the prices they can charge.

Zimmer Holdings Inc. US:ZMH this year agreed to purchase Biomet Inc. for $13 billion. In 2011, Johnson & Johnson JNJ, +0.31% agreed to buy Synthes Inc. for about $21 billion.

Medtronic MDT, +0.86% , which is based in Minneapolis, makes cardiovascular and orthopedic devices. It has a market value of about $61 billion, compared with about $32 billion for Covidien, which makes staplers, feeding pumps and other devices used in surgery.

Medtronic had $14 billion of cash as of April, much of it held outside the U.S.

Inversion deals have become increasingly popular, especially among health-care companies, many of whom have ample cash supplies they would be taxed on should they bring them back to the U.S.

An expanded version of this report appears at WSJ.com