My fellow critics Willa Paskin and Linda Holmes had prepared me for the convoluted sexual ethics of the show, which simultaneously encourages the participants to have sex and acts oddly coy about whatever it is that they get up to, and their characterizations are absolutely correct.

But the show’s antiquated notions do not end there. “The Bachelorette” relies on modes of courtship from the 1950s and earlier. In this bizarro world, administered by host Chris Harrison in a pseudo-parental role, commitment comes later in the cycle of a relationship, but marriage comes quicker, and the reality of living together arrives last in this cycle.

AD

AD

We are supposed to be convinced that these processes lead to true love, and judging by the enthusiasm of the live studio audiences who turn out for certain episodes, plenty of people do. But much of the show’s drama comes from the incredible stresses those traditions place on the people who have to carry them out. The promise of “The Bachelorette” is a shiny, pavé-d to the max Neil Lane engagement ring. The reality is heavy, and not quite a perfect fit.

None of this is to say that contemporary dating has somehow solved the mysteries of courtship and paired us all up peaceably and with a minimum of tsuris. “The Bachelorette,” though, makes an unintentional case for the uncertainties of hookup culture and the possibility that OkCupid will use you as an involuntary research subject.

Not only do the men on the show know that the Bachelorette is dating all of them simultaneously, but they also have to date her collectively, torture themselves when she goes off by herself with one of their rivals, and socialize with their competitors. It is a supercharged resurrection of a dating tradition that treated going steady as a big deal, to be entered into only after much consideration and exploration of one’s other options. I am amazed that we got through this season with just one incidence of mild, awkward racism, a lot of fishwifey sniping, and what appears to be some real feelings of hurt, rather than cries of “come at me, bro!” and attempted violence.

AD

AD

And it never stops. The competing bachelors never get a chance to date the Bachelorette feeling secure in her attention and affections, without knowing that she is running through the routine with other men. They have no idea what it is actually like to be in a relationship with her, rather than a participant in a social experiment.

A yenta would be preferable to this. A yenta is at least making efforts to get to know the people in question and assess whether they are compatible. A yenta needs her customers to be satisfied so that she can drum up repeat business. “The Bachelorette” does not need to concern itself with something so mundane as whether the relationships it brokers actually continue.

This is a self-sustaining enterprise, and as such it is free to be totally ludicrous. In the finale, Hy Dorfman, Andi’s fabulously sensible father, reminded both of her remaining suitors that pretending to be a mime or boating around Venice is not the day-to-day stuff of which marriage is made. But because the competition goes up to the very last moment, neither man can pause and acknowledge that Hy might have a point. Instead, they have to manically insist that they have no doubts.

AD

AD

When a proposal comes, there is an air of queasy anti-climax to it. In this, “The Bachelorette” seems to conform to contemporary ideas that the specifics of a proposal should be a surprise, but the idea of marriage should not. The surprise on the show, though, is not the design of the ring, or the bride’s entire family emerging from bushes and cupboards to offer congratulations: It is the identity of the groom. As long as an engagement is achieved, though, “The Bachelorette” can deem itself a wild success.

Maybe Laura Ingalls Wilder, who wrote the “Little House” series of novelistic memoirs about her life in the 1870s and 1880s, can convince us that you can conduct a constrained courtship, then get to know your spouse for real after the wedding. Andi and her future spouse are not quite up to the task of selling us on this ideal.