Police were called to Selena Gomez's suburban Los Angeles home Tuesday in response to neighbors' complaints of a noisy party, while reports surfaced that day that the singer-actress and pop star Justin Bieber may again be a couple.

TMZ.com, citing law-enforcement sources, said Wednesday that a high-volume party prompted Gomez's neighbors to call police late Tuesday night. A spokesman for the Malibu/Lost Hills Station of the Los Angeles County Sheriff's Department confirmed to Newsday that deputies responded to a noise complaint that night "in the Calabasas area" where Gomez lives.

E! News said the party of about 20 people had not gotten out of hand but that it did include live, amplified music. The guests were cooperative with police, E! added, and turned the volume down.

Gomez, 21, Wednesday posted on Instagram a black-and-white image of two men holding microphones to their faces and another man playing a white piano. "Tacos, live music and candles. Such a beautiful night with beautiful people," Gomez wrote in a caption.

The night before, Gomez attended a birthday party with Bieber, 20. An Instagram video posted by the rapper Khalil, captioned "Happy Birthday Alfredo Flores!" shows what Us Weekly describes as Khalil, music-video director Flores, Gomez and Bieber dancing in a private room at a Los Angeles nightclub.

E! added that Gomez and Bieber were by each other's side most of the evening and were seen leaving together afterward.

Sign up for Newsday's Entertainment newsletter Get the latest on celebs, TV and more. By clicking Sign up, you agree to our privacy policy.

Gomez's spokeswoman had no comment. Bieber's representative did not return a Newsday request for comment.