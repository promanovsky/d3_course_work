More than one out of ten children living near the 2013 Boston Marathon Bombings exhibited signs of post-traumatic stress disorder, according to research conducted by the American Academy of Pediatrics. The study, published in Pediatrics, surveyed 460 parents of children (aged 4-19) who lived within 25 miles of the manhunt area.

Experts say a similar percentage of kids experienced PTSD symptoms following the World Trade Center attacks in New York.

"Children near and far throughout the Boston area showed a range of mental problems, not just PTSD symptoms," said study author Jonathan Comer. "Exposure to the bombing itself and the manhunt events [on the media] were associated with considerable PTSD symptoms."

The study authors recommend that parents limit TV exposure of children to violent events. Just one-third of Boston parents said they limited the exposure of their kids to the news.

According to the U.S. National Institute of Mental Health, symptoms of PTSD include re-experiencing an event (such as having a flashback), avoiding people or situations or being hypervigilent.

For comments and feedback contact: editorial@rttnews.com

Health News