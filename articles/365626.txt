As the final season of True Blood unfolds, deaths of major characters are inevitable.

Warning: Article contains major spoilers from True Blood Season 7, Episode 3 'Fire In The Hole'.



Even though fans are shocked over the death of Alcide Herveaux, played by Joe Manganiello, the actor has claimed he knew it was coming.

"I called it as soon as [Sookie and Alcide] got together [in the Season 6 finale]," Manganiello tells Zap2it. "As soon as I read that they were together, the first thing I thought was, 'They're going to kill him.' ... As soon as that happened, I went, 'Aw man, this is not going to last long.'"

Manganiello isn't as broken up about Alcide's death as fans are because, according to him, True Blood is ultimately the story about Sookie and Bill, not Sookie and Alcide.

"As soon as they got together, I thought, 'Well, you have to get him away from Sookie at some point so she can go deal with Bill and Eric,' just on a storytelling standpoint," he said.

"You can't have her hurt him, because then the fans will get mad at her for breaking his heart, so you've got to kill him."

Check out fans' reaction on Twitter about Alcide's death on the show:

Alcide's character deserved so much more than what he got in this series. I'm just...devastated. #trueblood — Tasha L. Harrison (@tashalharrison) July 7, 2014

dear #trueblood, must we kill alcide and eric off just to get sookiexbill back together? — Denise Reina (@denisereina) July 7, 2014

god. now I can't even have naked #alcide to make this show go by quicker. #TrueBlood has really taken everything good away from this world — Chelsea Anne (@chelseanne23) July 7, 2014

THIS IS YOUR FAULT AGAIN SOOKIE! She killed my Tara Mae and now she killed my Alcide. ALCIDE NOOOO!!! #TrueBLOOD — Goooooooooorl! (@Crotch0_0Watcha) July 7, 2014

ALCIDE!!!!!!! I literally just screamed #TrueBlood — Najimah ناجئماة 4.25 (@Wiild_Flower) July 7, 2014

Omg they just killed alcide!!!!!!!!!! Wtf well there goes half if the female viewership for the rest of the season #TrueBlood — KhalSwivel34 (@Bigswivel34) July 7, 2014