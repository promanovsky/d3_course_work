NEW YORK (MarketWatch) — U.S. stocks recovered most of their losses but still finished Monday generally lower as weak data from China reignited worries about a global economic slowdown.

The S&P 500 SPX, +0.47% retreated from its record closing level and ended the day less than a point lower at 1,877.18. The Dow Jones Industrial Average DJIA, +0.09% fell more than 100 points at one point Monday but closed 34.04 points, or 0.2%, lower at 16,418.68.

The Nasdaq Composite COMP, +0.50% closed down 1.77 points at 4,334.45.

Read the recap of our live stock market coverage.

Weighing on stocks was a report from China, the world’s second-largest economy, over the weekend that showed its exports unexpectedly skidded 18.1% in February over a year earlier. Economists had expected an increase of 5%. The surprise drop also hit copper prices.

“In the short term, China’s trade data was disappointing, however, at this point it is unclear how much of it was distorted by the Lunar New Year holiday,” said John De Clue, chief investment officer at U.S. Bank Wealth Management.

“U.S. equities still remain the market of choice for a lot of international money, which is why we have seen more multiple expansion over the past few weeks,” he added.

Among individual stocks, shares of Chiquita Brands International Inc. US:CQB jumped 11% after the company announced an all-stock merger deal with Fyffes PLC UK:FFY worth $1.07 billion on Monday.

FMC FMC, -2.34% shares climbed 6.2% after the company said Monday it will split into two entities by early next year, with one company consisting of FMC’s agricultural solutions and health and nutrition units and the other will focus on its minerals business.

Cliffs Natural Resources Inc. CLF, -0.72% shares dropped 3.8% in the wake of weak data out of China.

Boeing Co. BA, +0.15% shares fell 1.3% after The Wall Street Journal reported late Friday that the company has discovered cracks in some 787 Dreamliner jets, which will lead to a delayed delivery of some aircraft.

Alexion Pharmaceuticals Inc. ALXN, +2.46% shares gained 7.1% after the biopharmaceutical company on Monday raised its 2014 adjusted earnings outlook .

Plug Power Inc. PLUG, -1.25% shares jumped 25%, adding to last week’s gains of 77%. Plug Power last week scored a contract with six distribution centers at Wal-Mart Stores Inc. WMT, +0.31% and the price target on its shares was boosted by Cowen & Co. analysts on hope of more contracts.

Facebook Inc. FB, +0.04% shares rose 3.2% after UBS raised the stock’s price target to $90 from $72. Analyst Eric Sheridan said Facebook shares are undervalued but is expected to accelerate in value going forward.

In overseas markets, Asian stocks fell sharply, with the Shanghai Composite Index SHCOMP, +1.43% down nearly 3%. The Nikkei 225 index NIK, -0.14% lost 1%. European markets fell.

Oil prices fell, with April crude CLJ24, -1.88% off more than 1%. Gold prices regained foot and rose.

More must-reads from MarketWatch:

Deutsche Bank: Next 5% move in stocks will be down

Forecaster of the Month: The economy’s not going anywhere

Warning sign for stocks: Margin debt at a record