Swiss watchmaker TAG Heuer has lost one of its top executives to Apple.

The boss of TAG Heuer's parent company LVMH, Jean-Claude Biver, told CNBC that its sales director left to join the Cupertino-based iPhone maker.



iWatch rumour roundup: Release date, features, specifications and more

Biver said the sales director left as recently as last week "to take a contract with Apple", in the build-up to an iWatch launch.

The move suggests that Apple hopes to stand out in the iWatch market with a 'Swiss made' label and is rumoured to have approached several executives from Swiss watchmaker companies.

Apple has been speculated to launch an iWatch for some time and is tipped to finally reveal the device in October.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io