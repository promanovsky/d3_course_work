Google (NASDAQ:GOOG)(NASDAQ:GOOGL) is currently working on a new set-top box solution, according to The Verge: Android TV is an upcoming version of Google's mobile operating system optimized for set-top boxes. With a heavy focus on Internet video, Android TV is poised to compete with devices like the Fire TV and the Roku.

When it comes to mobile computing, Google's Android has been remarkably successful -- it powers over 80% of the smartphones, and more than 60% of the tablets, sold worldwide. By attracting a variety of hardware OEMs, including Samsung (NASDAQOTH: SSNLF), LG, and Sony (NYSE:SNE), Google's operating system has exploded in popularity.

Will the same thing happen with Android TV? While certainly possible, there's reason to be skeptical -- Google's hardware partners may not be so enthusiastic about supporting its new platform.

Samsung's Smart Hub

Samsung is Google's largest hardware partner. The millions of tablets and hundreds of millions of smartphones it sells every year are powered by Google's mobile operating system. If it wasn't for Samsung, it's difficult to imagine that Google's operating system would be so dominant (Google's competitors call Android phones Galaxies).

But Samsung may not be so eager to support Google's new platform. The South Korean tech giant is already a force when it comes to the living room -- more than one in four televisions sold worldwide is manufactured by Samsung -- and many of them are powered by its own, proprietary platform. Purchase a new Samsung TV or Blu-ray player, and chances are it will come with half a dozen apps already installed. In its present state, Samsung's smart TV platform is far from perfect, but includes many advanced features, such as voice and gesture controls. Samsung seems to have big ambitions for its TV platform. Last October, Samsung held its first developer's conference, encouraging mobile app creators to make their apps compatible with Samsung's TVs.

To some extent, Samsung's reliance on Google's operating system makes it vulnerable to competition in the smartphone and tablet market. For that reason, Samsung has gone to great lengths to customize Android for its own benefit, even provoking Google in the process. With an already dominant position in the TV market, Samsung may not be so eager to cede control of the living room to Google.

LG has brought webOS back from the dead

LG doesn't sell nearly as many Android-powered smartphones or tablets as Samsung, but the company has released a few highly regarded Android devices, including the G2 and the GPad, and has collaborated with Google on its Nexus 4 and 5 handsets. But like Samsung, LG already has its own smart TV platform, and it may not be so eager to support Google's new initiative.

In 2013, LG purchased webOS -- a now defunct mobile operating system that once competed with Google's Android. It took a year, but the first TVs powered by webOS have made their debut, and like Android TV, serve as a gateway to Internet video.

The new version of webOS hasn't blown reviewers away, but its improved on LG's existing smart TV platform quite substantially. With its recent investment in the operating system, it's difficult to imagine that LG would be a big supporter of Android TV.

Sony has backed Google's efforts in the past, but this time could be different

Google's last attempt at breaking into the living room -- its ill-fated Google TV initiative -- was supported by Sony. In fact, the Japanese electronics giant was one of the only manufacturer to release a Google TV product, including the 2013 Bravia Smart Stick. But I'm skeptical Sony will be on board this time.

Android TV is said to support Android-based video games, which would make sense given Google's recent purchase of Green Throttle Games, a video game controller manufacturer. If so, it will compete head-to-head with Sony's PlayStation brand. While it's hard to imagine that Android gaming will take much away from the full-featured experiences offered by Sony's high-powered PlayStation, as the technology develops, it could prove to be a long-term threat to one of Sony's most iconic brands.

But even more than gaming, Sony is positioning the PlayStation as rival smart TV platform in and of itself. The PlayStation already works as a set-top box, offering access to the most popular streaming apps, but Sony could be about to double down -- according to The Wall Street Journal, it's looking to add original shows and an Internet-based cable alternative. Sony's CEO, Kaz Hirai, told CNBC in January that the PlayStation could serve as a smart TV platform, extending Sony's dominance in the living room.

Google might have to build the hardware itself

Sony, LG, and Samsung aren't Google's only hardware partners, but they are three of the largest. They're also the most established when it comes to the living room, as each is a major player in the TV industry.

In time, they could come around to supporting Google's latest effort, but given that they're all already offering competing platforms, it's difficult to imagine them jumping on board -- at least initially. Google is said to be working on its own set-top box, and it may need to -- if Android TV is going to succeed, Google may have to manufacture the hardware itself.