For U.S. college students, a month of Spotify streaming now costs a little more than a large frappuccino.

The Swedish digital music company, which launched in the United States in 2011, is offering college students its ad-free streaming service for $5 a month as it tries to get more young people to subscribe.

The standard price for Spotify’s so-called premium offering, which provides on-demand access to 20 million songs online via personal computers and mobile devices, is $10 a month. Spotify also offers a free version with commercials.

INTERACTIVE: Discover songs of L.A.

Advertisement

College students are a key demographic for Spotify, as young consumers increasingly go online and use smartphones for music. Meanwhile, formidable competitors continue to emerge in the digital music industry.

The heavily promoted Beats Music launched in January, boasting new ways for listeners to discover music, including playlists culled by artists and taste-maker publications. The service, spun out of Dr. Dre and Jimmy Iovine’s headphone seller Beats Electronics, completed a round of roughly $60 million in new funding earlier this month.

Like Spotify and San Francisco-based Rdio, Beats Music charges $10 a month for access on the Web and mobile devices. But, unlike those competitors, Beats Music does not offer a free, advertiser-supported version. The company also teamed with AT&T to offer a $15-a-month family plan.

CRITICS’ PICKS: What to watch, where to go, what to eat

Advertisement

Spotify currently has about 24 million active users and 6 million paying subscribers, according to its website. Beats Music has not disclosed how many people have signed up, but Bloomberg News has reported that around 28,000 people subscribed in its first month.

Separately, Spotify said it has stopped taking submissions from third-party developers who make applications that run on its desktop service.

“Partners have been asking us for a cheaper and easier way to own a presence inside Spotify and developers would like more ways to integrate Spotify with their own applications,” the company said in a statement. “Pair that with the growing importance of mobile and we realized we needed to adapt.”

The apps that currently exist, including those from Last.fm and Rolling Stone, will still be available, the company said.

Advertisement

ALSO:

Fox News to launch Sunday show with Maria Bartiromo

TV ratings: ‘Dream Builders’ opens low; basketball boosts CBS

Morning Fix: Disney buys Maker Studios. Sherwood upped at ABC.

Advertisement

ryan.faughnder@latimes.com

Twitter: @rfaughnder