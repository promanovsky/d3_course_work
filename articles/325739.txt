Eli Wallach, who died Tuesday at the age of 98, and his wife Anne Jackson, 86, had stellar solo acting careers, but their projects together had a special magic.

Married 66 years and the parents of three children, Wallach and Jackson were able to translate their personal chemistry and dynamism into their roles whether on screen, stage and television.

Over the decades, there have been several renowned acting couples, including Broadway superstars Alfred Lunt and Lynn Fontaine; Vivien Leigh and Laurence Olivier; Jessica Tandy and Hume Cronyn; Paul Newman and Joanne Woodward; Richard Burton and Elizabeth Taylor. The recently deceased Ruby Dee had a longtime acting partnership with her husband, Ossie Davis.

But few had a creative partnership that lasted as long as that of Wallach and Jackson. Charter members of the famed Actors Studios in New York, they met in 1946 when they appeared together in the Tennessee Williams’ one-act “This Property Is Condemned” at the Equity Library Theatre in New York and married two years later.

Advertisement

Jackson won the Obie in 1963 for the off-Broadway production of the Murray Schisgal one-acts “The Typists,” which they reprised for TV, and “The Tiger,” which was expanded into their 1967 feature comedy “The Tiger Makes Out.”

Wallach and Jackson starred together in a Broadway production of “Rhinoceros.” They were joined by their daughters, Katherine and Roberta, in the 1978 off-Broadway production of “The Diary of Anne Frank” and reminisced about their lives and careers off-Broadway in 1993 in “In Persons.” They also starred in Anne Meara’s 2000 off-Broadway production of “Down the Garden Path.”

Their TV and movie collaborations were rare -- their last appearance was in a 2003 episode of NBC’s “ER,” in which they played -- of course -- a married couple.

Twitter: @mymackie