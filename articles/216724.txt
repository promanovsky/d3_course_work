An EMT dances while behind the wheel in a video uploaded to YouTube. (credit: YouTube/mastekz hv)

NEW YORK (CBSNewYork) – An enthusiastic emergency medical technician from New Jersey got the attention of the pop star Rihanna, after a video of him dancing behind the wheel went viral.

But as CBS 2’s Tony Aiello reported, the EMT, Jonathan Williams, also got reprimanded by his boss.

A camera was rolling as Williams heard Rihanna on the radio while behind the wheel of a private ambulance.

You can see his dance moves by watching the video below:

Caution: some strong language

His hands move in every direction as he tries to keep one hand on the wheel and the other on his handheld radio.

“Wow, he’s gone viral – that’s great,” said Williams’ friend, Bianca Lotin.

The video has gone viral, racking up hundreds of thousands of views on YouTube. It even garnered Rihanna’s attention, who took to Twitter to mention it.

This Paramedic guy just reminded me why God sent me here! I can't stop now! Not now, not nevah!!!! —> http://t.co/PSfDDzZCGA — Rihanna (@rihanna) May 14, 2014

“Very, very entertaining,” Lotin said of her friend. “The way you see him on the video, that’s the way he is in life.”

But what about Williams’ driving – taking his hands off the wheel and using a two-way radio while driving.

Driving an ambulance, and working as an EMT, is a very demanding job. Online, many have commented: “What’s the big deal? The guy was just letting off some stress.”

But at the Montclair Ambulance Unit – which is not the agency for which Williams works — officials were not amused.

“It doesn’t help our industry,” said Montclair Ambulance Unit Deputy Chief Frank Carlo.

Carlo said if Williams worked for him, he would be in trouble.

“It creates an unsafe environment,” Carlo said. “We stress safety with our staff. The public puts their trust in us. We put our trust in our EMTs, and we want that returned.”

On the phone, Williams told CBS 2’s Aiello: “I was out of service and heading ho9me. I’d never do that with a patient on board.”

Still, Williams has been reprimanded by his employer.

But he was shocked and impressed that he also drew attention from Rihanna.

“I was flabbergasted – totally, utterly flabbergasted,” he said.

The New Jersey Department of Health licenses EMTs. A spokeswoman told CBS 2 the department had no comment on the video.

You may also be interested in these stories:

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]