Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Charlie Sheen is really, really angry with Rihanna - and he has let his feelings out in a long and explosive rant on Twitter.

Where all these celebrity arguments end up.

The actor has described stunning Riri as "the village idiot" and couldn't resist making a dig at her new pink hair in the worst way possible.

Taking to TwitLonger, he began by setting the scene - where him and his porn star fiancé Brett Rossi had gone out for dinner together to celebrate her birthday with some friends.

All starting off OK then.

But then, after hearing Ri was in the building too, Charlie claims he asked for Brett to meet her - but he was denied.

He wrote: "I sent a request over to her table to introduce my fiancé Scotty to her, as she is a huge fan.

"(Personally I couldn't pick her out of a line-up at gunpoint)." - First dig.

(Image: FameFlynet)

He apparently heard back that there were "too many paps" outside so they couldn't. Which unleashed some serious fury.

He wrote: "At this time? AT THIS TIME?? lemme guess, we're to reschedule another random 11 million to 1 encounter with her some other night...?

"No biggie for me; it would have been 84 interminable seconds of chugging Draino and 'please kill me now' that I'd never get back.

"My Gal, however, was NOT OK with it. Nice impression you left behind, Bday or not. Sorry we're not KOOL enough to warrant a blessing from the Princess. (or in this case the Village idiot)."

WOW.

And still it goes on, because by now we can see, this isn't something Charlie's going to let go by without comment.

(Image: Twitter/@rihanna)

He said he "always" took the time to meet fans, and that's why he's been going "31 awesome years", before advising Rihanna that Halloween isn't for a while so "good on you for testing out your costume in public".

Not a fan of the pink hair then Charlie?

He finished with: "See ya on the way down, (we always do) and actually, it was a pleasure NOT meeting you. clearly we have NOTHING in common when it comes to respect for those who've gone before you.

"I'm guessing you needed those precious 84 seconds to situate that bad wig before you left the restaurant.

"Here's a tip from a real vet of this terrain; If ya don't wanna get bothered DONT LEAVE YOUR HOUSE! and if this 'Prison of Fame' is soooooooo unnerving and difficult, then QUIT, junior!"

Oh em GEE. So not best pals then?

Riri didn't address the claims directly, but tweeted an hour later: "Goooorrrrrrlllllll......"

And added: "If that old queen don't get ha diapers out of a bunch..."

We have contacted Rihanna's reps for comment.