Congratulations go out to Drew Barrymore, who welcomed her second child -- a daughter named Frankie -- on Tuesday, April 22., Gossip Cop has confirmed the news with the actress' rep, as did People magazine.

In a statement to People, Barrymore's rep made an announcement on the behalf of the happy family:

"Happy to announce that today we are the proud parents of our second daughter, Frankie Barrymore Kopelman. Olive has a new little sister, and everyone is healthy and happy!"

This is a second child for Barrymore and husband Will Kopelman, whose daughter Olive is almost two years old.

Earlier this month, the 39-year-old celebrated her baby shower with some close, and famous, friends, including Cameron Diaz, Gwyneth Paltrow and Reese Witherspoon, according to E! News.