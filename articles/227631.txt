No film-maker has won the Palme d'Or three times. But if ever there were men to do it, those men would be Jean-Pierre and Luc Dardenne, the Belgian brothers and Cannes perennials who previously triumphed with Rosetta in 1999 and in 2005 with The Child. Two Days, One Night is the shortest film in competition (just 95 minutes, 101 minutes less than sometime bookies' favourite Winter's Sleep), and the one which has most united the critics. No review has yet given it fewer than five stars.

The film follows factory worker Sandra (Marion Cotillard) over a weekend in which she tries to persuade her colleagues to give up a 1,000-euro bonus so she can keep her job. The experience is traumatic for all involved. The workers are by turns belligerent and pitying; Sandra – much supported by her husband and two children – has only just recovered from a bout of depression.

The film's story was inspired by what Jean-Pierre called "the obsession with performance and violent competition between workers everywhere in the workplace, in Belgium and elsewhere".

Neither brother had personal experience of such a dilemma, said Luc. "And we would have never suggested the agreement – it's a kind of blackmail and that's not what we do. The crisis doesn't foster solidarity but it's always been something that was not a given, even in the past when there were more major social movements. In the 60s when a strike was organised because a plant was to be closed, workers had to speak first with their spouses. It wasn't easy. Solidarity is a sort of moral commitment, based on moral decisions."

Yet the timidity exhibited by the workers – who are too few in number to have a formal union – does appear to be something the brothers perceive as a new phenomenon. "The absence of collective reaction," said Jean-Pierre, "of any struggle against the principle behind that vote, reveals a very contemporary lack of solidarity."

They first met Cotillard, with whom they have never previously worked, on the set of Jacques Audiard's Rust and Bone, which they helped produce. It was "love at first sight", said Jean-Pierre. The film is their first since Rosetta to focus so solely on a female lead – necessary, said Jean-Pierre, because "this would never happen to a man". "In real life women are more fragile in employment. They find it more difficult to find another job and there are many more women unemployed than men."

Cotillard in Two Days, One Night



Cotillard said she felt the part was of a piece with those characters she had played in recent films Rust and Bone and The Immigrant. "I view these women as people who are fighting for survival and they discover things they didn't realise they had. That's what interests me in the human condition. I'm deeply touched by survivors, [those] who manage despite circumstances and handicaps. I learn a lot about human beings when I explore people's souls."

The role requires a substantial downgrading of Cotillard's natural glamour – Sandra is rake thin and washed out, emotionally bedraggled and popping Xanax. "I don't view myself as someone who is ugly," said the actor. "I can transform myself and that's an asset in the job I do. I know I can manage if the part requires me to be quite beautiful and I can also be quite ugly." She expressed great admiration for the dressing-down of Penelope Cruz, all but unrecognisable in the 2005 Italian romance Don't Move. "It's really hard to make her look ugly," said Cotillard. "But she looks absolutely hideous."

This is the third year in a row that Cotillard has given an acclaimed lead performance at the festival (The Immigrant was here in 2013) yet the actor said she hadn't considered the possibility of an award for her performance. "I protect myself by not thinking about things like that. It's a natural defence."

The competition runs through the week, with the Palme d'Or – and best actress prize – being handed out on Saturday night.

• Peter Bradshaw's five star review