CINCINNATI, Ohio -- A 12-year-old girl with a rare case of medically induced obesity that pushed her weight past 200 pounds is recovering after weight-loss surgery.

Alexis Shapiro, from Cibolo, Texas, was sedated and on a ventilator late Friday after undergoing weight-loss surgery at Cincinnati Children's Hospital Medical Center, NBC News reported. A message left by The Associated Press at the hospital Saturday seeking an update on her condition wasn't immediately returned.

The 4-foot-7-inch girl was stable and comfortable after the surgery but expected to remain in the intensive care unit at least through the weekend, Dr. Thomas Inge said Friday. Doctors expect Alexis to lose weight and resolve many health problems, such as type 2 diabetes and pulmonary issues.

The condition arose two years ago after brain surgery to remove a tumour damaged her pituitary gland and part of the brain that signals hunger, WCPO-TV in Cincinnati reported. The condition called hypothalamic obesity also inhibits Alexis' ability to produce adrenaline, limiting her energy so she can't burn off the extra calories, the station reported.

Doctors had planned to perform a gastric bypass operation and procedure to cut part of her vagus nerve, but they had to change their plan because her liver was bigger and fattier than anticipated. They switched to a sleeve gastrectomy to remove up to 80 per cent of her stomach.

Inge said the change of plans wasn't a medical complication but rather a clinical decision.

Shapiro's mother, Jenny Shapiro, told NBC News in an email Friday that she and her husband, Ian Shapiro, are fine with the change.

"And it was what's best for her," she wrote.

Inge said doctors will wait to see how Alexis responds to the surgery before deciding whether to go ahead with the gastric bypass and vagus nerve operations.

"I think she will have a new normal," Inge said. "The new normal for her will be at a healthier weight perhaps with less damaging conditions."

Shapiro was expected to remain hospitalized for about a week and could return to Texas in two weeks, NBC News reported.