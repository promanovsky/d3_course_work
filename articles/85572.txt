The hotly anticipated seventh instalment of the famed 'Star Wars' movie franchise has begun filming. The exciting news came from Disney Studios chairman Alan Horn, and he also revealed that most of the casting, which is still relatively unknown, has been completed.



J.J. Abrams will be directing Episode VII

When speaking to The Hollywood Reporter this past Wednesday (April 2nd), Horn confirmed, "We have a lot of [the cast in place] . We're just not completely done yet."

Back in January (2014) it was reported that the final script had been written by the film's director, J.J. Abrams, and the 'Star Wars: Episode V' and 'Episode VI' scribe, Lawrence Kasdan.

But according to Horn, several issues had arose with the screenplay and getting it right was crucial. "It's all about the screenplay," he said. "It has to be screenplay, screenplay, screenplay." But when asked if the script was on schedule, he replied, "It actually is now."

The Disney executive also confirmed the movie would be sticking to its slated released date of December 18th 2015, and he also shed some light on when the story would take place. "[Episode VII will begin} where 6 left off -- and where 6 left off is 35 years ago by the time this is released," he confirmed.

Watch Alan Horn's interview below



Abrams is known for keeping his film's surrounded in mystery, and fans are still none the wiser to who is actually starring in 'Episode VII', but there has been several rumors of big Hollywood stars being cast.

'Girls' actor Adam Driver reportedly took the villain role, and as predicated no further information is known, but his character will shadow the sci-fi franchise's most notable villain, Darth Vader.

Gary Oldman confirmed to have received a call from Abrams to discuss a possible role in the revived iconic sci-fi film series. "I'll believe it when I'm on the plane home. The deal isn't done, but yeah, they've inquired," he said at the time.



Could Gary Oldman have a starring role?

Zac Efrom also revealed he has held talks with the production team about possibly starring in the space epic. The 26 year-old told MTV News, "I just went and met with them. So I don't know. It would be cool. I love [the Star Wars movies], I love them, but. who knows?"

The latest rumoured cast member is recent Oscar winner Lupita Nyong'o. The '12 Years A Slave' actress reportedly held a meeting with Abrams last month to read for a major female role.



Lupita Nyong'o is the latest name that has been rumored to have a starring role