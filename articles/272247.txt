People who speak more than one language are more likely to remain mentally alert in old age, according to a study from the University of Edinburgh in Scotland.

Based on tests carried out with a group of 853 native English speakers in the United Kingdom, the new findings, published in the Annals of Neurology, suggest being conversant in an additional language slows down the decline of thinking skills in later life.

Researchers examined the results of standardized intelligence tests taken by the study group at age 11 and compared them with results of thinking tests given the same participants at age 73.

As they progressed through their lives, the study subjects had been been tested on a series of physical and mental functions, including reasoning, memory, speed of thinking, various aspects of fitness and health, eyesight and blood composition.

The university scientists who studied the test results found that those who spoke two or more languages demonstrated significantly better thinking skills in later life when compared with what was predicted through their childhood IQ results.

The analysis showed there was even notable cognitive improvement in those who acquired an added language in adulthood.

"These findings are of considerable practical relevance. Millions of people around the world acquire their second language in later life," said study researcher Thomas Bak, from the Center for Cognitive Aging and Cognitive Epidemiology. "Our study shows that bilingualism, even when acquired in adulthood, may provide a small benefit to the aging brain."

It's estimated over one million people in the U.K. aged 65 and over suffer some degree of cognitive impairment.

"We urgently need to understand what influences cognitive aging so that we can give people better advice about protecting their cognitive health," said Caroline Abrahams, charity director for Age UK, the largest senior-oriented help organization in the nation. "This latest breakthrough is another stride forward in finding out how thinking skills can be preserved in later life."

The people inlcuded in the latest study were part of the Lothian Birth Cohort 1936, a group of individuals who were born in 1936 and took part in the Scottish Mental Survey of 1947.