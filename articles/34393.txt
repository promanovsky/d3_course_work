Moving pictures of a giraffe saying goodbye to a terminally ill zoo worker has touched hearts across the globe.

It was the man's dying wish to be taken to the giraffe enclosure at Rotterdam's Diergaarde Blijdorp zoo where he had spent his entire adult life cleaning after the animals.

In a heartbreaking scene the giraffe appears to be giving him a kiss goodbye.

The Ambulance Wish Foundation transported the maintenance worker, known as Mario, who has terminal cancer, to the zoo.

The foundation's founder Kees Veldboer said the 54-year-old could hardly walk and found speaking very difficult but his face 'spoke volumes' when the animal touched him.

Veldboer told Dutch newspaper Algemeen Dagblad: "These animals recognised him and felt that things aren't going well with him

"It was a very special moment. You saw him beaming.

"It was very nice that we were able to work on the last wish of this man".