The company that had a much anticipated IPO earlier this year but has yet to see the mark that it went public ($22.50) on March 26th is losing further ground in trading today.

As of this writing (3:03PM EDT), is trading at $15.96. That puts the stock down $2.80 on the day or 14.98%. Volume is quite high with nearly 5.4 million shares changing hands today compared to an average trading volume of 3.3 million shares since it went public.

Great numbers not enough for King

King Digital Entertainment PLC (NYSE:KING) reported revenue of $607 million for the quarter while FactSet’s estimate was for $602 million. Gross bookings were $641.1 million which is up a good amount from the last quarter when it reported bookings of $632 million. Earnings per share of $0.61 were also reported while analysts were expecting earnings of $0.57. The company reported that it had increased its active monthly users to 481 million from the previous quarter when it had 408 million. Daily active users were also up markedly from 124 million to 143 million.

That, however, was not a enough to stop the drubbing that King Digital Entertainment PLC (NYSE:KING) is taking today. Clearly investors think that King is far too reliant on Candy Crush Saga.

Company’s warning

The company seems concerned as well when they said, “The fact that a relatively small number of games continue to account for a substantial majority of our revenue and gross bookings, and declines in popularity of these games could harm our financial results.”

Additionally, King Digital Entertainment PLC (NYSE:KING) is suffering today due to the fact that people are making less in-app purchases. King Digital Entertainment PLC (NYSE:KING) reported 11.9 million “unique payers” down from 12.2 million last quarter and 13 million at its peak two quarters ago.

“There’s a lot of companies in the technology, media and technology space this quarter that delivered fine results and the stocks tumbled,” Doug Creutz, an analyst with Cowen and Company, said. “The market seems to be in a phase where it hates anything that sounds like growth right now … Frankly, this is silly.”

While that may be silly, one could certainly argue that millions of adults moving pieces of candy around was considerably more silly and quite possible unsustainable. Zynga Inc (NASDAQ:ZNGA)’s Farmville and Rovio’s Angry Birds have shown the fickle nature of mobile gaming in the last years.