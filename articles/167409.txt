FILE - In this July 20, 2010 file photo, a Netflix subscriber turns on Netflix in Palo Alto, Calif. Netflix's streaming-video audience of more than 20 million subscribers has led many to label it a kind of digital TV network, and one that may grow into an HBO rival _ if it's not already. (AP Photo/Paul Sakuma, file)

LOS ANGELES, April 28 (Reuters) - Netflix Inc has reached a deal to pay Verizon Communications Inc for faster delivery of its TV shows and movies, the second arrangement to pay fees for quicker access that the company argues should be free.

Financial terms were not disclosed.

In February, Netflix struck a deal with Comcast Corp to pay for faster delivery over the Internet through a practice known as interconnection. Weeks later, Netflix Chief Executive Reed Hastings said he had reluctantly agreed to pay the fees so his customers would get better service.

Hastings, in a blog post in March, called on broadband providers to make adequate connections available to Netflix for free, but said Netflix might reach deals to pay other providers in the short term.

On Monday, Netflix spokesman Joris Evers said "we have reached an interconnect arrangement with Verizon that we hope will improve performance for our joint customers over the coming months."

Verizon spokesman Bob Varettoni confirmed the deal.

"We reached this agreement to deliver improved service for our combined customers," he said. (Reporting by Lisa Richwine; Editing by Bernard Orr)