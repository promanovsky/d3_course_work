Government warnings a decade ago about the risks associated with children and adolescents taking antidepressants appear to have backfired, causing an increase in suicide attempts and discouraging many depressed young people from seeking treatment, according to a study published Wednesday in the academic journal BMJ.

Researchers said their findings underscore how even well-intentioned public health warnings can produce unintended conseque­n­c­­es, particularly when they involve widespread media attention and sensitive topics such as depression and suicide.

In 2003 and 2004, the Food and Drug Administration issued a series of warnings based on data that pointed to an increase in suicidal thinking among some children and adolescents prescribed a class of antidepressants known as selective serotonin reuptake inhibitors, or SSRIs. They included such drugs as Paxil and Zoloft. In late 2004, the agency directed manufacturers to include a “black box” warning on their labels notifying consumers and doctors about the increased risk of suicidal thoughts and behaviors in youths being treated with these medications.

The FDA warnings received a flood of media coverage that researchers said focused more on the tiny percentage of patients who had experienced suicidal thinking due to the drugs than on the far greater number who benefited from them.

“There was a huge amount of publicity,” said Stephen Soumerai, professor of population medicine at Harvard Medical School and a co-author of Wednesday’s study. “The media concentrated more on the relatively small risk than on the significant upside.”

The researchers pointed to headlines in publications such as the New York Times (“FDA links drugs to being suicidal”) and The Washington Post (“FDA confirms antidepressants raise children’s suicide risk”), that, they wrote, “became frightening alarms to clinicians, parents and young people.”

As a result, antidepressant prescriptions fell sharply for adolescents age 10 to 17 and for young adults age 18 to 29. At the same time, researchers found that the number of suicide attempts rose by more than

20 percent in adolescents and by more than a third in young adults.

Researchers tracked the rise in suicide attempts by examining reports of non-fatal poisonings involving psychiatric medicines — a common indicator of attempted suicides. They said the likely number of suicide attempts probably was much higher, given that the study didn’t account for other suicide methods and poisonings that went unreported.

“There was a sort of overreaction by the media, but also an excessive caution on the part of patients,” said Christine Lu, a Harvard Medical School researcher and co-author of Wednesday’s study. “Lots of people who needed treatment steered clear because of the fear factor . . . For any drug, there are risks, for sure. But there’s also the risk of leaving the underlying condition untreated.”

Wednesday’s findings are in line with research published in 2007 that documented a precipitous drop in antidepressant prescriptions in the wake of warnings from federal regulators.

That study, published in the American Journal of Psychiatry, found that the sharp decrease in antidepressant use coincided with an increase in the number of suicides among children. While researchers said the data did not prove that suicides rose directly because of the drop in prescriptions, experts said there were few other plausible explanations.

“We may have inadvertently created a problem by putting a ‘black box’ warning on medications that were useful,” Thomas Insel, director of the National Institute of Mental Health, told The Post at the time. “If the drugs were doing more harm than good, then the reduction in prescription rates should mean the risk of suicide should go way down, and it hasn’t gone down at all — it has gone up.”

The FDA issued a series of warnings in 2003 and 2004 about children taking antidepressants, such as Paxil. Antidepressant prescriptions decreased and suicide attempts increased. (Joe Raedle/Getty Images)

The authors of Wednesday’s study wrote that while the government’s actions in 2003 and 2004 were legitimate and thorough, “FDA advisories and boxed warnings can be crude and inadequate ways to communicate new and sometimes frightening scientific information to the public.” Likewise, researchers argue that while media attention can create much-needed awareness — for example, warning about the link between Reye’s syndrome and aspirin use in children — sometimes “the information may be oversimplified and distorted when communicated in the media.”

Ultimately, Soumerai said, regulators and reporters must find ways to accurately convey the risks of certain drugs without unnecessarily alarming patients, their families and doctors.

“Some of these topics are complex and scary. The whole subject is fraught with so much emotion,” he said.“A lot of this has to do with how we communicate,” he said.