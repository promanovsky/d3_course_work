Want a seat at the table?

Every morning, the editorial team at public radio’s international news show The World meets to plan what they'll cover that day. Want to see what's on deck?

Sign up for our daily newsletter TOP OF THE WORLD and get the big stories we’re tracking delivered to your inbox every weekday morning.