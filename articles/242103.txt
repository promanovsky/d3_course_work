Following news of eBay's security breach, three U.S. attorneys general are teaming up to lead a probe into the commerce site.

Officials in Illinois, Florida, and Connecticut have joined an investigation into the circumstances surrounding the hack, which occurred earlier this year and compromised encrypted passwords and non-financial data.

The company has not made clear how many people were affected, but plans to email customers with additional details.

Connecticut Department of Consumer Protection Commissioner William Rubenstein urged all 660,000 state users to change their account details, saying that anyone who uses their eBay passcode for other Internet accounts "should immediately assign different passwords to protect them from being accessed through this breach."

Few details of the hack have been revealed; eBay admitted Wednesday that it only recently detected the breach, which occurred between late February and early March. Following "extensive tests" on its networks, the company claimed there is no evidence of compromised financial data or unauthorized activity on users' accounts.

But internal testing isn't enough proof for some people, like Connecticut AG George Jepsen, who announced this week that he will be "looking into the circumstances surrounding this breach," as well as the steps eBay is taking to prevent future incidents.

Part of the investigative team, Florida Attorney General Pam Bondi said the eBay breach "could be of historic proportions," and promised to "do everything in our power to protect consumers' personal information."

Lisa Madigan, Illinois attorney general, has also joined the fight, a spokeswoman confirmed to PCMag.

EBay is not fighting the probe, according to a spokeswoman, who said that the commerce site has "relationships with and proactively contacted a number of state, federal, and international regulators and law enforcement agencies. We are fully cooperating with them on all aspects of this incident."

Meanwhile, New York AG Eric Schneiderman said in a statement on Thursday that the news of eBay's breach "is deeply concerning," and asked that the company provide free credit monitoring.

"New Yorkers and eBay customers around the country trust that retailers will protect their personal information when they shop online," he said. "Our office has asked and fully expects eBay to provide free credit monitoring services to customers impacted by this breach."

The eBay hack comes in the wake of the Heartbleed Bug, which, discovered in OpenSSL in April, left encrypted data open to scammers for more than a year.

For more, check out some of the biggest hacks of 2013 in the slideshow above.

Editor's Note: This story was updated at 12:55 p.m. ET with comment from eBay and confirmation from Madigan's office.

Further Reading

Security Reviews