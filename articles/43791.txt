U.S. new home sales fall 3.3% in February to 440,000

Economic Indicators Mar 25, 2014 10:09AM ET

U.S. new home sales fall to 440,000 units in February Investing.com - U.S. new home sales fell more than expected in February, underlining concerns over the health of the housing sector, official data showed on Tuesday. In a report, the U.S. Commerce Department said new home sales declined by 3.3% to a seasonally adjusted 440,000 units last month, compared to expectations for a decline to 445,000.



New home sales in January were revised down to 455,000 units from a previously reported 468,000 units. Following the release of the data, the U.S. dollar held on to gains against the euro, with shedding 0.42% to trade at 1.3782. Meanwhile, U.S. equity markets were higher after the open. The Industrial Average rose 0.7%, the index added 0.7%, while the index rallied 1.1%.

U.S. new home sales fall 3.3% in February to 440,000

Related Articles