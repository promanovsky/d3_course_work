Are the price of flagships about to drop?

When it comes to flagship Android smartphones there's really only one chip vendor in play, Qualcomm, but the new MediaTek MT6795 is looking to challenge that dominance.

You'll find a Qualcomm chip inside all of 2014's big handsets including the Galaxy S5, One M8, Xperia Z2, LG G3 and even the affordable OnePlus One and Windows Phone-toting Nokia Lumia 930.

MediaTek then, has a serious uphill battle on its hands, but it reckons its MT6795 System on Chip (SoC) for Android is just the ticket for high-end handsets with a 64-bit true octa-core (yes EIGHT cores) processor plus Cat 4 LTE and 2K display support.

Strangely, MediaTek is touting the MT6795 as the SoC "with the world's first 2K (2560 x 1600) display support" but a quick look at the product page for Qualcomm's Snapdragon 801 processor which features in all the Android devices we've just listed suggests otherwise.

2K confusion

You'll find the text "2560 x 2048 + 1080p and 4K external displays supported" under 'display' on the 801 page, which makes MediaTek's claim slightly baffling - although it is the first 64-bit chip to support 2K.

With a 64-bit architecture and octa-core setup the MT6795 is certainly future proof, and opens the door to Android handsets boasting more than 4GB of RAM and some seriously powerful applications.

It also supports 480fps, 1080p full HD Super-Slow Motion video capture and Ultra HD (4K) video playback - although the Cat 4 LTE isn't a match for the Cat 6 support on the Snapdragon 805.

The key selling point for the MT6795 however could be its price, as it's expected to come in cheaper than Qualcomm's upcoming Snapdragon 805 and in turn that could make for a flurry of cheaper flagship devices.

That said, the big companies are pretty entrenched with the Qualcomm setup, so it will be interesting to see if MediaTek can win over some hearts.