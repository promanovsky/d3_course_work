A comedy about three different murder plots might not be the most likely candidate for a sequel, but since Horrible Bosses proved to be an unexpected hit for New Line in 2011, Horrible Bosses 2 is slated to hit theaters in November of this year.

The trailer hit YouTube today and despite being saddled with PG constraints, it's remarkably packed with laughs:

For those who don't recall: Jason Bateman, Charlie Day and Jason Sudeikis turned out to be pretty inept at murder (even with the help of Jamie Foxx's Dean "Motherf--er" Jones), so two of the 3 bosses reprise their roles for Horrible Bosses 2.

This time around, Jennifer Aniston's depraved dentist is kinkier than ever, inviting Jason Bateman to...well, just watch the trailer and see.

Kevin Spacey is back again, as well. This time, serving out a life sentence for murdering Colin Farrell in the first film.

The guys are their own bosses now, and the plot reportedly revolves around their revenge on an investor who screws them over by pulling out of their business venture.

Bateman, Day and Sudeikis kidnap the man's son - played by Chris Pine - and we're guessing all doesn't go according to plan.

Horrible Bosses 2 is set for release on November 26.