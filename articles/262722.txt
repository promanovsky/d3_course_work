The Centers for Disease Control and Prevention (CDC) acknowledged today that the agency had goofed when it told the world of the first case of person-to-person transmission of the deadly Middle East Respiratory Syndrome (MERS) virus in the United States.

The CDC sent out an alert on May 17th saying an Illinois man showed evidence of infection by the coronavirus that causes MERS. It was believed he acquired it from an Indiana doctor who imported the first case of MERS into the U.S. from Saudi Arabia in late April. The two were business associates and had two extended face-to-face meetings just before the Indiana man was admitted to the hospital.

As a result of the CDC’s preliminary findings, the Illinois man was told to ‘self-isolate’ – to stay at home or wear a surgical mask when he was around other people. The CDC also tracked down every person he had been in contact with and tested them for the presence of the virus.

Today, the CDC walked all of that back, announcing that the Illinois man was never infected with the MERS virus. The reason for the discrepancy appears to be a false positive test on the man’s blood.

The CDC conducted two types of tests on the Illinois man after he was reported to have been in close contact with the Indiana patient. The first test, a DNA assay of mucus samples from his upper respiratory tract showed no sign of infection with MERS. But a serology test on his blood showed that he had antibodies to the MERS virus, suggesting he had previously been infected.

Though the man showed none of the hallmark symptoms of MERS, the CDC was concerned enough about the possibility of human-to-human transmission that it sent out an urgent alert. But today, the CDC received the results of the more definitive serology test – the neutralizing antibody test – on more samples of the man’s blood. Those tests, considered the ‘gold standard’ for disease detection, came back negative for the MERS virus.

Dr. David Swerdlow, who is leading the CDC’s response to MERS, defended the initial alert.

“While we never want to cause undue concern among those who have had contact with a MERS patient, it is our job to move quickly when there is a potential public health threat,” Swerdlow said in a written statement.

Swerdlow also indicated there could be more false positives as the global presence of MERS spreads.

“Because there is so much we don’t know about this virus, we will continue to err on the side of caution when responding to and investigating cases of MERS in this country,” he said.

To date, there have been two cases of MERS in the United States – the Indiana doctor, and a man in the Orlando area. Both are believed to have been imported from Saudi Arabia, where the number of MERS cases has been rapidly escalating. Both have been released from the hospital and are now in good health.

According to the World Health Organization, there have been 636 cases of MERS confirmed around the world. Of those, 193 cases have resulted in death.