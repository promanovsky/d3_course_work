Elon Musk's solar power company SolarCity has acquired solar panel manufacturer Silevo and may be planning to open one of the world's largest solar energy plants in New York State. Word the merger came in an official statement posted to SolarCity's official blog, and the firm says the massive plant could be the tip of the iceberg.

"We are in discussions with the state of New York to build the initial manufacturing plant, continuing a relationship developed by the Silevo team," they wrote in the post, according to BusinessInsider.com. "At a targeted capacity greater than 1 GW within the next two years, it will be one of the single largest solar panel production plants in the world. This will be followed in subsequent years by one or more significantly larger plants at an order of magnitude greater annual production capacity."

Silevo is know specifically for creating high efficiency solar panel, which SolarCity says could help shape the future of solar power:

"Without decisive action to lay the groundwork today, the massive volume of affordable, high efficiency panels needed for unsubsidized solar power to outcompete fossil fuel grid power simply will not be there when it is needed."

For comments and feedback contact: editorial@rttnews.com

Technology News