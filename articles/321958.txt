New research reports that adults who watch three or more hours of TV a day may double their risk of premature death.

The new study published in the Journal of the American Heart Association studied 13,284 young and healthy Spanish university graduates and assessed risk of early death from three sedentary behaviors: TV watching, computer time and driving time. They didn’t find any associations with computer time and driving, but they report that the risk for death was two times higher for participants who watched three or more hours at a time, even when the study authors accounted for other factors related to early death.

The findings are still considered preliminary, though this is not the first time researchers have found seriously worrisome effects from watching too much TV (for instance, it can go along with eating too much junk).

The reality is that there’s nothing coming out of the TV that’s going to kill you, but sitting in front of the TV for hours on end means you are not basically not moving at all. We already know that sitting for prolonged periods is really bad for your health, and TV is one of the most common ways to forget about exercise.

The American Heart Association says it recommends people get at least 150 minutes of moderate-intensity aerobic activity or at least 75 minutes of vigorous aerobic activity each week.

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.