Los Angeles: Reality TV star Kim Kardashian was joined by her closest girlfriends for dinner and a bachelorette party.

The 33-year-old bride to be was accompanied by siblings Khloe, Kendall and Kylie as well as friends LaLa Anthony, Joyce Bonelli, Monica Rose, Brittny Gastineau, and more on May 22, reported Us magazine.

Earlier in the evening, Kim received a personal delivery of 15 bottles of Veuve Clicquot champagne to her room at the hotel before heading out to an all girls dinner, a source said.

The girls then dined on "light dishes" while sipping "mineral water and a bit of wine to drink," a member of the staff revealed, adding, "they clearly all had a lot to talk about -- all were in a very good mood."

Kim took to her Instagram page, sharing a pic surrounded by all her girls at the dinner, with the caption, "I`m so lucky! I have the best friends in the whole wide world! #ParisNights #LastSupper."

Kim is set to wed Kanye West in Florence, Italy on May 24.