Consumer Reports has ranked the best-tasting burgers, sandwiches, burritos, and chicken at 65 national and regional fast food chains.

Big national and international chains including McDonald's, Taco Bell and Subway ranked far lower in the taste test than smaller chains including In-N-Out Burger (which won second place for best burger) and El Pollo Loco (second place for best chicken).

Advertisement

"Americans are spending more than ever to dine out - topping $680 billion per year," according to the survey . "And they are demanding more for their money, higher-quality fast food, and greater variety than can be found at titans such as Burger King, KFC, and McDonald's."The graphic below lists the top rankings. Head over to Consumer Reports to find out more about the survey.