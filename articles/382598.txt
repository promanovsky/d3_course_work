Wondering which hospital provides the best possible treatment for your health condition? U.S. News & World Report today released its annual scorecard that ranks the best hospitals in the country.

Topping this year's list is the Mayo Clinic in Rochester, Minn. It was followed by Massachusetts General Hospital in Boston and Johns Hopkins Hospital in Baltimore. Johns Hopkins previously held the top spot for 21 years straight.

New York City, Los Angles and Boston are the only cities in the country that had more than one hospital on the list.

U.S. News ranked each facility on both skill and experience in 16 medical specialties including cardiology, diabetes, gynecology, neurology and orthopedics. Overall, 144 different hospitals ranked in at least one medical specialty out of the nearly 5,000 medical centers evaluated.

The report took into account data on patient survival rates, hospital performance surveys and other records that detail staffing and the facility's available resources.

"Central to understanding the rankings is that they were developed and the specialties chosen to help consumers determine which hospitals provide the best care for the most serious or complicated medical conditions and procedures -- pancreatic cancer, for example, or replacement of a heart valve in an elderly patient with comorbidities," write the authors of the report. "Medical centers that excel in relatively commonplace conditions and procedures, such as noninvasive breast cancer or uncomplicated knee replacement, are not the focus."

U.S. News published its "Honor Roll" of the top hospitals:

1. Mayo Clinic, Rochester, Minnesota

2. Massachusetts General Hospital, Boston

3. Johns Hopkins Hospital, Baltimore

4. Cleveland Clinic

5. UCLA Medical Center, Los Angeles

6. New York-Presbyterian University Hospital of Columbia and Cornell, New York

7. Hospitals of the University of Pennsylvania-Penn Presbyterian, Philadelphia

8. UCSF Medical Center, San Francisco

9. Brigham and Women's Hospital, Boston

10. Northwestern Memorial Hospital, Chicago

11. University of Washington Medical Center, Seattle

12 (tie). Cedars-Sinai Medical Center, Los Angeles

12 (tie). UPMC-University of Pittsburgh Medical Center

14. Duke University Hospital, Durham, North Carolina

15. NYU Langone Medical Center, New York

16. Mount Sinai Hospital, New York

17. Barnes-Jewish Hospital/Washington University, St. Louis

A companion report also scored the best hospitals for children. Take a look and see how hospitals in your area measure up.