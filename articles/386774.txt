The possible end to an epidemic that has taken the lives of over 39 million people since its onset in the 1980s may be in sight.

A report from the United Nations claims with new human immunodeficiency virus (HIV) infections and deaths from AIDS decreasing, it may be possible to control the epidemic by 2030. The report added that once under control the hope is the epidemic could even vanish completely "in every region, in every country."

"More than ever before, there is hope that ending AIDS is possible. However, a business-as-usual approach or simply sustaining the AIDS response at its current pace cannot end the epidemic," read a statement by the U.N. AIDS program UNAIDS.

This optimism surrounding the eventual end of the human immunodeficiency virus is due to a stabilizing in the number of people infected with HIV around the world. That number currently stands at 35 million worldwide. Additional numbers show that there were 2.1 million new cases in 2013 -- 38 percent less than the 3.4 million figure in 2001.

AIDS-related deaths have fallen as well in the past three years by approximately one-fifth, currently standing at 1.5 million a year. The report adds that new HIV infections are indeed showing a steady decline over the last decade-plus.

"The AIDS epidemic can be ended in every region, every country, in every location, in every population and every community," added Michel Sidibe, the director of UNAIDS. "There are multiple reasons why there is hope and conviction about this goal."

AIDS can be transmitted via blood, breast milk and by semen during sex, and the virus can be kept in check with drugs known as antiretroviral therapy or ART. The UNAIDS report explains that at the end of 2013 there were 12.9 million HIV-positive people that had access to ART. That number is up dramatically from the 5 million who were getting AIDS drugs in 2010. These numbers provide yet another key in the optimism surrounding the eventual end of the epidemic.

The report adds that there have been more medical achievements with regard to AIDS in the last five years than there had been in the previous 23 years.

With around 69 percent of all people living with HIV residing in sub-Saharan Africa, that particular region of the world currently carries the greatest burden of the epidemic.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.