The European Central Bank kept interest rates unchanged, giving itself more time to gauge whether the euro-area recovery has reignited inflation.

The 24-member Governing Council, meeting in Brussels, left the main refinancing rate at a record low of 0.25 percent. The outcome was forecast by 56 of 58 economists in a Bloomberg News survey. The deposit rate and the marginal refinancing rate were left unchanged at zero and 0.75 percent, respectively.

The wait-and-see stance is supported by an uptick in price gains last month and signs the year-old recovery is continuing. Even so, inflation has been below 1 percent since October and fresh staff forecasts next month will show whether the medium- term outlook has worsened, a contingency that ECB President Mario Draghi has said would require action.

“The economic data since the ECB’s April meeting do not point to immediate action,” said Holger Schmieding, chief economist at Berenberg Bank in London. If the ECB cuts its inflation and growth forecasts next month “that could possibly serve as a pretext for loosening policy,” he said.

Inflation Target

Inflation in the euro area was 0.7 percent in April, according to an initial estimate from the European Union’s statistics office. While that’s up from 0.5 percent in March, it was below economists’ forecasts and compares with the ECB’s goal of just under 2 percent.

At the same time, economic data have signaled that the currency bloc is recovering from its longest-ever recession, which ended in the second quarter of 2013, and survey indicators point to a further pickup in activity.

Service industries in Spain and Ireland, which have both exited international bailout programs in the past six months, showed the fastest growth since before the financial crisis in March, according to purchasing managers indexes by Markit Economics this week. Consumer confidence in the euro area unexpectedly increased in April, a European Commission report showed last month.

June Forecasts

“Although inflation remains well below target and looks set to remain weak, the economic recovery gradually continues to gain momentum,” said Ben May, an economist at Oxford Economics Ltd. in London. “This suggests that the ECB will refrain from providing further policy stimulus in May.”

The ECB gathering in Brussels is one of two monetary-policy meetings a year that it holds outside its Frankfurt headquarters.

The Bank of England kept its benchmark rate at a record-low 0.5 percent, while its bond-purchase plan will stay at 375 billion pounds ($636 billion). Norway’s central bank kept its main interest rate at 1.5 percent. In Indonesia, where economic growth has slowed to the weakest in more than four years, the central bank held its reference rate at 7.5 percent.

Federal Reserve Chair Janet Yellen said she believes the U.S., the world’s largest economy, still requires stimulus five years after its last recession ended as unemployment and inflation are still well short of the central bank’s goals.

The ECB will publish revised economic projections after its June 5 policy meeting. It predicted in March that gross domestic product in the euro area will expand 1.2 percent this year, accelerating to 1.8 percent in 2016, and that the inflation rate will average 1 percent this year, rising to 1.5 percent in 2016.

Draghi Contingencies

ECB Governing Council member Ewald Nowotny said last month that the ECB’s growth forecasts may be raised, and that the central bank should wait until at least June before acting. Council member Ardo Hansson said policy makers should see how the data evolve over the next few months. Jens Weidmann, Germany’s representative at the ECB, said officials must focus on the path of inflation rather than past figures.

Speaking in Amsterdam on April 24, Draghi provided his most-detailed explanation yet on which contingencies could trigger more stimulus.

A worsening of the medium-term inflation outlook would require the ECB to “meaningfully” ease policy and would justify a broad-based asset-purchase program, he said. That’s unlikely to be an early choice, according to Elga Bartsch, chief European economist at Morgan Stanley in London.

Small Steps

“Instead of launching into full-blown QE, we expect the ECB to adopt a number of smaller policy steps in the coming months to further support the recovery, address the much-debated lowflation problem, and to contain the strength of the currency,” Bartsch said. “We expect another small reduction in the refinancing rate in June, followed or possibly even accompanied by a deposit-rate cut.”

Rate cuts or liquidity injections could be used to counter any “unwarranted” monetary tightening caused by higher money- market rates, higher bond yields or a stronger euro, Draghi said in Amsterdam.

The euro has climbed about 9 percent against the dollar since July, curbing the price of imported goods and undermining the competitiveness of euro-area companies. It advanced as high as $1.3951 this week, taking it close to the $1.40 level that some investors have identified as a trigger that may spur a further rally.

Euro Trigger

German industrial output unexpectedly fell in March, data showed. That follows a report yesterday showing a drop in factory orders, adding to signs that Europe’s largest economy is cooling after a strong start to the year.

One ECB action might be to halt the drain of liquidity added by bond purchases under the expired Securities Markets Program. Pausing the weekly operation, which has failed for the past four weeks, would add about 168 billion euros ($234 billion) into the financial system, though Draghi said in February that the effects would be “relatively limited” as the debt matures.

In the money markets, the average cost of overnight, unsecured lending between banks in the euro area, known as Eonia, rose to 0.254 percent last month. That’s three times as high as a year earlier and above the ECB’s benchmark rate for the first time since October 2008, signaling investors may be returning to pre-crisis behavior before the recovery is entrenched.

“While Eonia settings have risen and become somewhat more volatile of late, we believe the ECB views this as associated with a reactivation and reintegration of the euro-area interbank market,” Huw Pill, chief European economist at Goldman Sachs Group Inc. in London, said yesterday, adding that there’s a chance the central bank will act in the coming months.

“Should the ECB view the upward drift in Eonia settings seen since the start of the year as undesirable — given concerns about deflation risks — it may choose to shift its key interest rates down,” Pill said in a note to clients. “The probability rises to around 40 percent in June or July, even as no change remains our base case.”