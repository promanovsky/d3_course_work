Verizon Wireless has given customers in Indiana the option to send text message alerts to 911 for emergencies.

The extended service, which launched Wednesday, was announced by the Indiana Statewide 911 Board, according to The Indy Channel.

The service has been tested for over a year, after the board reported that 75 to 80 percent of all 911 calls came from a cell phone.

Verizon, along with technology partner TeleCommunication Systems, is the first carrier to provide customers with the 911 texting option, The Courier-Journal reported.

The Federal Communications Commission (FCC) is looking to have all wireless carriers provide the service by the end of the year. Sprint, AT&T and T-Mobile have also volunteered to provide text-to-911.

"Sprint and T-Mobile we expect in the coming days," said Barry Ritter, executive director of the board. He added that the service has limited availability and that users could get a bounce-back message instructing them to call 911.

"We're phasing it in as it becomes available in all 92 counties," Ritter said.

So far, the service has been tested in 28 Indiana counties, The Indy Channel reported. Dispatchers said there were a couple of problems with the service, such as auto-correct features causing texts to be delayed or confusing. They added that it is easier for them to track a landline than it is to track the location of cell phone.

Officials said that calling is the best choice for emergencies, but that texting is a good alternative for people without other options.

Brad Meixell, director of Clark County's office of emergency communications, said that information sent to dispatchers should be as clear as possible, and that users shouldn't send text abbreviations or slang, The Courier-Journal reported.

Clark County launched a pilot program in January 2013 for the service. Meixell said that improvements and training have helped public safety officials meet the FCC's May 15 deadline.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.