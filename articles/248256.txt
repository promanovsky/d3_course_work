Over the last couple of years, the movement against the company Monsanto has increased in their vocal outreach. Within that time period, Monsanto has been linked to hurting people through manipulating food sources. Now for this year’s March Against Monsanto, taking place today, a series of events and rallies will take place in over 400 cities in over 50 countries. In the United States alone, the March Against Monsanto is confirmed in 47 of the 50 states.

By now, it is clear that Monsanto is considered to be a dangerous company primarily through Facebook memes, multiple internet reports and articles, and the flood of horror stories associated with them. But what is Monsanto, exactly? In short, they are a publicly traded American multinational chemical and agricultural biotechnology corporation. They are known for producing the genetically engineered seed, genetically modified organisms, or GMO, and chemical pesticides used on food. What Monsanto does not want you to know is they have spent hundreds of millions of dollars to obstruct all labeling attempts in the United States. This means Monsanto is spending a lot of money to make sure products that are technically a GMO are not recognized as a GMO, something that must be done in 62 other countries.

Tami Canal, the founder of March Against Monsanto, explained a little bit more about the business in a report by the International Business Times:

“Monsanto’s predatory business and corporate agricultural practices threaten their generation’s health, fertility, and longevity.”

That is why Canal made the International Grassroots Movement (the company behind the March Against Monsanto). She believes that the food Monsanto makes will potentially hurt her two daughters if they consume it. She even added this extra statement about the movement in a directive with the March Against Monsanto:

“MAM supports a sustainable food production system. We must act now to stop GMOs and harmful pesticides.”

This is actually a big deal since Monsanto does have a hold on a lot of the food in the United States. They have a swinging door when it comes to positions of power in the Federal Drug Administration (FDA) and Environmental Protection Agency (EPA).

Besides their controversial association with GMOs, Monsanto’s other big controversy has to do with polychlorinated biphenyl chemicals, or PBCs. Back in 1983, PBC remains were found in the soil of Times Beach, Missouri. As a result, the entire town had to relocate. Environmental protection agencies insultingly tagged Monsanto as one of the most dangerous companies in the state because of this incident, which Monsanto still denies to this day.

I am sure some of you may be asking what the heck PBCs are. For those who know about or experienced the Vietnam War, you probably know PBCs by its common name, Agent Orange. Today, there are over three million people in Vietnam with cancers, severe physical and mental disabilities, and other health issues believed to be caused by Agent Orange exposure.

Now you know the basics of Monsanto and what the March Against Monsanto stands for. For more information about the March Against Monsanto, GMOs, Monsanto, or the crimes against humanity they committed, check out the video by Abby Martin of Breaking The Set below.

[Images via Bing]