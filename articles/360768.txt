Georgia prosecutors formally charged film producers Randall Miller, Jody Savin and Jay Sedrish with involuntary manslaughter and criminal trespassing in wake of the death of camera assistant Sarah Jones.

According to Georgia law, if found guilty of manslaughter, Miller, Savin and Sedrish can serve up to 10 years in prison. The lesser charge, criminal trespassing, carries a maximum 12-month-long prison stay, Yahoo Movies reported.

The tragedy struck during the first day of production on musician Gregg Allman's biopic "Midnight Rider." Jones, 27, was a camera assistant for the film.

The crew was preparing to shoot a scene that took place on what was supposed to be a lightly used railway bridge over the Altamaha River in Georgia. The crew wisely waited for two trains to pass before they began setting up the bed onto the tracks. However, a train was in the immediate vicinity, and the crew heard its whistle.

The Los Angeles Times reported that producers didn't have permission to film on "the railway trestle itself." The crew was expected to have 60 seconds to clear the track, but the train came barreling down the tracks. A last ditch effort to remove props, including a bed frame to be used by William Hurt as Allman as a mental hospital patient, and clear the tracks was unsuccessful. The train collided with the bed and ate it up. It spit out shrapnel that struck Jones and pushed her into harms way. Seven other crew members were injured as well, but Jones was the only fatality.

In addition to the criminal case, multiple lawsuits have been filed against the producers and their company Unclaimed Freight Productions, Inc. The lawsuit on behalf of Jones' family also lists Greg Allman, distributor Open Road Films, landowner Rayonier and a railroad company as defendants.

Since the accident, no other filming has taken place, and production is officially suspended. The Hollywood Reporter ran a story saying that Hurt dropped out of the movie. Whether "Midnight Rider" will be made at all is anyone's guess, but in light of the tragic events, it's certainly not going to be a popular decision.

Members of the Hollywood film community have banded together in the wake of this tragedy. They aim to raise awareness regarding safety issues during film production. One of those ways is via video. Several days ago, a two-minute public service announcement, titled "We Are Sarah Jones," was released and starred prominent actors and crew members advocating for change.

Watch it below:



Do you think "Midnight Rider" should ever resume production? Would such an action dishonor Jones' memory? Let us know in the comments section below.