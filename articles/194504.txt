According to The Hollywood Reporter, HGTV has axed plans for a new reality series titled FLIP IT FORWARD, which was set to premiere this October. The decision followed recent anit-gay remarks by the show's hosts, twin brothers David and Jason Benham.

The network announced the news on their Official Facebook page , commenting "HGTV has decided not to move forward with the Benham Brothers' series."

Reports began to surface this week that at a 2012 rally organized by their father, Flip Benham, promoting a constitutional amendment in North Carolina that would define marriage as being between a man and a woman, David Benham opined: "We have no-fault divorce; we have pornography and perversion; we have homosexuality and its agenda that is attacking the nation." He went on to add that the Christian majority must repent for standing idly by and "tolerating" such things.

The pilot for the series was described as follows: "Star real estate entrepreneurs and brothers David and Jason Benham as they Leverage their good-natured sibling rivalry to help families find a fixer-upper and transform it into the dream home they never thought they could afford."