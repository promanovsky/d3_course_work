UPDATED: The network pulls the plug on three comedies and one drama and officially passes on pilots "Dead Boss," "Fatrick" and "No Place Like Home" while "Cabot College" and "Sober Companion" remain in contention.

Less than a day after handing out a batch of new series orders, Fox is cutting ties with several of its series. The Hollywood Reporter has learned that the broadcast network has canceled comedies Dads, Enlisted and Surviving Jack as well as drama Rake. Of the network's freshman comedies for the 2013-14 season, only Brooklyn Nine-Nine is returning.

PHOTOS: Broadcast TV's Returning Shows 2014-15

Not a big hit to start, Dads ended up having an abbreviated freshman season. Seth MacFarlane's live-action debut on the network that's been home to three of his animated series averaged a 1.7 rating with adults 18-49 and 4.1 million viewers with seven days' worth of DVR. Dads' early exit from the Tuesday block made it look even more competitive in hindsight. Its season average trails The Mindy Project, which scored an early renewal with comedies Brooklyn Nine-Nine and New Girl, by only 10 percent.

Fox's military comedy Enlisted -- one of the best reviewed freshman shows of the year -- was buried on Fridays and moved from its November bow to January. A semi-autobiographical comedy from Kevin Biegel about three very different brothers (Geoff Stults, Chris Lowell and Parker Young) serving in the Rear Detachment unit, Enlisted averaged a 0.7 rating among adults 18-49, growing an impressive 71 percent with DVR to a 1.2 and 3 million total viewers. Meanwhile, leads Stults and Young have starring roles in CBS comedy pilot Cuz-Bros, which they took in second position to Enlisted.

PHOTOS: Faces of Pilot Season 2014

Fox entertainment chairman Kevin Reilly expressed his affection for Enlisted in January, noting he would stick with the show through 13 episodes before making a decision based on its DVR performance. Biegel, meanwhile, has led the cast and producers' live-tweet of episodes on Hulu after Fox pulled the beloved comedy from the schedule April 11, with the rest of the episodes expected to return after sweeps for a summer burn-off, and has been vocal about Nielsen not counting viewing on military bases, with service members regularly praising the series.

Surviving Jack, the network's second-to-last scripted entry of the season, got its American Idol lead-in a little too late. By the time the reality show dropped to its half-hour results, ratings were already suffering, and Surviving Jack saw a soft start because of it -- pulling just a 1.3 rating with adults 18-49 and an admittedly solid 5.1 million viewers.

Season-to-date, the comedy has averaged just a 1.5 rating in the demo. And that's with a very modest 25 percent DVR boost, dangerously small. The show marked Chris Meloni's TV return after departing his longtime Law & Order: SVU gig in 2011.

STORY: Complete Network Scorecard

A sophomore season for Rake was never in the cards. Fox declined comment, but sources tell THR that the drama is done. An American spin on an Australian series of the same name, it opened to a very modest 1.7 rating among adults 18-49 despite an American Idol lead-in. Fox quickly shuffled the series to Friday -- and ultimately Saturday -- and the season has all-told averaged just a 1.2 rating in the demo and 4.7 million viewers. It was a high-profile play for the network, finally luring star Greg Kinnear to television after he turned down several other offers. But the production had its share of troubles, and the pilot was ultimately swapped with the fourth episode for the premiere.

Of Fox's comedy pilots, the network has passed on Dead Boss, starring Jane Krakowski, Nahnatchka Khan's Fatrick and No Place Like Home, starring Jane Kaczmarek. The network is still in talks to pick up Sober Companion, starring Justin Long, and Cabot College, starring Margaret Cho and Fortune Feimster. The latter, from 30 Rock producers Matt Hubbard, Tina Fey and Robert Carlock, has been generating buzz that the network could pick up the comedy -- which has a hefty series commitment penalty attached -- for midseason with a likely six-episode order. Khan, meanwhile, still has NBC comedy pilot Far East Orlando in the mix.

Fox bulked up its drama offerings earlier in the week. The official order came down for Batman prequel Gotham. The network also picked up hip-hop drama Empire and its remake of Spanish drama Red Band Society starring Octavia Spencer.

On the comedy front, Fox added 10 episodes to the previously ordered John Mulaney comedy -- bringing its freshman episode count to 16. Other new comedies include Last Man on Earth and Weird Loners.

Fox previously canceled freshman drama Almost Human.