Let’s talk about the best Game of Thrones finale in the show’s history. Unless you haven’t seen it yet, in which case be warned – EVERYTHING BELOW THIS IS SPOILERS.

Valar Morghulis and as it turned out, some women. The show’s motto proved true again last night, as the writers broke tradition and actually gave us some crucial plot twists in the season wrapup.

The episode opened with Jon Snow north of the wall and trying to negotiate a truce with Mance Rayder, aka the king of the Free Folk. Save for a few bruises to Jon’s ego, the whole thing seemed to be going pretty well. That is, until Stannis and his sizeable army came in for the most boring invasion ever, in classic Stannis style. The whole truce thing went down the drain, but it’s ok because it turns out that Stannis has no beef with the crows whatsoever. He doesn’t even have any beef with Mance, after Jon vouched for him. Isn’t it great when everyone’s being all honorable?



You thought it was bad before? It's only going to get worse for all of our favorite characters.

And on that note, let’s move on to King’s Landing, where no one is honorable and everyone hates Tywin Lannister. Pushed into a corner and asked to abandon Tommen, Cercei finally confronts her dad with an ultimatum. Tywin is forced to face the fact of his eldest children’s not-so-secret affair, as Cercei tells him – either he cancels the wedding to Loras Tyrell, or she tells the whole world about her children’s true parentage. She then storms off to Jaime and creepy brother/sister action ensues.

Jaime’s loyalties are also divided and later, in the dead of night, he helps Tyrion escape his prison. But before that…

We’re reunited with Arya and the Hound – and, as it turns out, Brienne and Pod. This could have been the dream team. But that’s not how Thrones works, unfortunately, so instead Brienne and Sandor get into an argument about Arya’s safety… and so she bludgeons him to death. If you’re counting, that’s one down so far. Unfortunately for Bri, during the scuffle, Arya manages to escape. She then stays with the Hound in his final moments, steals his silver and is off on a solo adventure.

No adventures for Dany, who is having some major ruling difficulties over in Meereen. She finds that sometimes, breaking the chains isn’t the best thing to do as a former slave begs to return to his master. Keep an eye on the chain symbolism, that will be important in a second. So the slave dilemma was hard enough, but then Daenerys has to face the father of a child, burned to death by Drogon – one of Dany’s dragon babies. Ouch. Unable to find Drogon, who has gone rogue, the Khaleesi waslks down to the catacombs and, with tears in her eyes, chains the other two dragons, Viserion and Rhaegal. Did this show just make us cry about CGI dragons? This can’t be healthy.

Meanwhile in King’s Landing, Tyrion is supposed to go straight to Varys, but we all know that’s not happening. He takes a detour to his father’s bedchamber, where he finds… drumroll please… a naked Shae, eagerly awaiting Tywin in his bed. So he kills her. Natch. Ok, we get that Tyrion is scorned and betrayed, but the scene where he strangles the alleged love of his life is still disturbing as heck. Luckily, then he finds Tywin and after a brief air clear (appropriate, since the whole scene takes place with Tywin on the toilet) Tyrion kills his dad. Yep. That happened. Three down.

Bran and co. find the heart tree, just before a bunch of re-animated skeletons find them. Meera, Jojen and a disembodied Bran fight valiantly, but the bags of bones just keep coming. Just as Jojen is stabbed to death and everything is about to hit the fan, a tiny ethereal creature comes to the rescue, raining fire on the wights. Meet The Children – a race older than the First Men. And the Three-Eyed Raven, who actually looks remarkably like an old man. Ok, you’ve met them, more in Season 5.

With everything else in the season pretty much done and dusted, it is time for Arya to set out for the North, which she does, with the help of a Braavosi merchant and Jaqen H’ghar’s coin. Remember – a man must add highlights?

So that’s the season finale, done and dusted. Let the season 5 theories begin!