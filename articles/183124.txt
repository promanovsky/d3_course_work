Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The difference between red carpet fame and red carpet shame is roughly measured by an inch of cloth.

It's a fine margin, and Kim Kardashian was left red-faced after she flashed her knickers at the Met Ball on Monday night.

The unfortunate wardrobe malfunction happened before the reality TV star even arrived at the star-studded bash

Kim and her partner Kanye West were leaving the Lanvin store where she had her striking custom made frock fitted, when she gave onlookers a bit of an eyeful.

However, Kim wasn't the only star to suffer a bit of a faux pas at the event, with Rita Ora's style choices not exactly going down a storm either.

Her gold Donna Karan sequinned silk tulle gown and gold lace boot combo fell flat, but not dressing to impress was the least of her worries.

Her style credentials took another bashing when she followed Kim's example by accidentally flashed her knickers on the red carpet.

Check out the most jaw-dropping celebrity wardrobe malfunctions below (and be grateful this didn't happen to you!).