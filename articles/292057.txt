Brett Molina

news

Rovio's Angry Birds are ready for Prime time.

The mobile game company announced it will partner with Hasbro on the mash-up Angry Birds Transformers, bringing together Rovio's hit franchise with Optimus Prime and the rest of the "robots in disguise."

The partnership will include a mobile game, licensed goods and a special product line from Hasbro, the companies revealed in a statement Monday.

"This mash-up is going to appeal to those who grew up with the brand in the 80s, as well as fans of the brand today," says Rovio chief marketing officer Blanca Juti.

The team-up will leverage Rovio's Telepods, small toys players can place on a tablet or smartphone to bring into the game.

Naturally, the announcement is timed to the arrival of summer film Transformers: Age of Extinction on June 27. Rovio and Hasbro did not say when the Angry Birds Transformers will make their debut.

Follow Brett Molina on Twitter: @brettmolina23.