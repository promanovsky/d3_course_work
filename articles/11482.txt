The doors to the MashHouse have closed, but SXSW is far from over. With Film running through Saturday and the Music festivities just heating up, there are plenty of sights, sounds, and BBQ to enjoy — and to capture with your camera.

We want to see what South By looks like through your eyes, so this week we’ve teamed up with Esurance to bring you a SXSW-inspired Mashable Photo Challenge. Whether it’s endless taco trucks and tech talks, or late-night dance parties and crazy concerts — we want to see it.

Upload your photo to Twitter or Instagram with the hashtag #MySXSW before 9 a.m. on March 17 and we’ll share our favorites on our site and in our Instagram feed.

Oh, and if you’re battery starts to go while you’re documenting your day, be sure to swing by the Esurance Fuel Lounge at 6th and Trinity to recharge your gear and take a break from the madness.

3 Ways to Enter the Challenge