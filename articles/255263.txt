Dollar index edges lower in holiday-thinned trade

Investing.com - The dollar edged lower against a basket of other major currencies on Monday as trade remained subdued with markets in the U.K. closed for a national holiday and U.S. markets to remain shut for the Memorial Day holiday.

The euro was steady near three-month lows against the dollar, with at 1.3639.

The pair showed little reaction after European Central Bank President Mario Draghi said Monday the bank saw a risk of a deflationary cycle taking hold in the euro zone, the latest indication that the ECB could ease monetary policy at its upcoming meeting next week.

Draghi said the ECB is ready to act should it see signs of a negative inflation spiral taking hold, and indicated that the bank is weighing a wide range of policy options, including interest rate cuts, and liquidity injections or broad-based asset purchases to help shore up the fragile recovery in the euro area.

The comments were made at the new ECB annual conference in Sintra, Portugal.

The euro touched fresh three month lows earlier Monday after anti-European Union and far-right parties performed strongly in elections to the European Parliament in some countries over the weekend, amid voter anger over austerity and high unemployment.

Elsewhere, dipped 0.07% to 101.89, while was almost unchanged at 0.8951.

The pound inched higher, with easing up 0.09% to 1.6843. Demand for sterling continued to be underpinned after Bank of England Deputy Governor Charles Bean said Saturday that U.K. interest rates could start rising before next spring.

Meanwhile, was trading at 0.9239 and was at 0.8548. was flat at 1.0861.

The , which tracks the performance of the greenback versus a basket of six other major currencies, slid 0.07% to 80.37.