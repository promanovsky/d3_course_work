Apple Inc. (NASDAQ:AAPL) and Samsung Electronics Co., Ltd. (LON:BC94) (KRX:005930), two of the biggest global smartphone players, will once again face each other this week in a court battle. The rivals will start a fresh trial accusing each other of copying designs and features. The trial is the latest in the series of lawsuits that the both smartphone makers have been fighting for years now.

A landmark case for Android handset makers

The whole tech industry will eagerly watch the battle as the suits between Apple and Samsung Electronics Co., Ltd. (LON:BC94) (KRX:005930) to address a larger concern about the features and technology that could be patented.

David Einhorn’s Greenlight had a strong fourth quarter; Gains on Neubase Therapeutics [Q4 Letter] David Einhorn's Greenlight Capital was up 5.2% in 2020, underperforming the S&P 500's 18.4% return. For the fourth quarter, the fund was up 25%, which was its best quarterly result ever. Longs contributed 42% during the fourth quarter, while shorts detracted 15% and macro detracted 1%. Q4 2020 hedge fund letters, conferences and more Growth Read More

The trial at hand will be one of the most crucial to date as it could pave the way for bringing similar lawsuits against other Android handset makers. After a successful win here, Apple Inc. (NASDAQ:AAPL) could sue other manufacturers using the Android platform as the patent in question is not specific to Samsung’s TouchWiz software, but to Google’s Android software. It must be noted that the iPhone maker cannot sue Google directly as the patent is breached only when Android code is implemented in hardware.

Apple / Samsung battle, unending

Similar to the trial, in summer 2013, the latest trial will be presented to Judge Lucy Koh in a California district court in San Jose. In the trial, Samsung will argue against the earlier verdict that favored Apple on every count, however, the damages were reduced.

In the latest trial, Apple Inc. (NASDAQ:AAPL) plans to claim damages worth $2 billion for the five patents breached by Samsung devises, Galaxy smartphones and tablets, sold between 2010 and 2012 in the U.S. On the other hand, the Korean major is accusing the U.S. firm of infringing two of its patents. On Monday, the process for jury selection will kick off based on data transmission and the use of video, audio and photos.

Apple Inc. (NASDAQ:AAPL) sued the Korean electronics giant in February 2012, which Koh referred to as “one action in a worldwide constellation of litigation between the two companies.”

Despite the hype surrounding the patent fight between Apple Inc. (NASDAQ:AAPL) and Samsung, the effectiveness of such trials are always in question owing to the slow pace of lawsuits. Though Apple did have many trials in its favor, the truth is Google’s Android is still the leader powering billions of devices worldwide.