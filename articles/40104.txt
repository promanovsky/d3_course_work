Dave Brockie, also known as Oderus Urungus, the founder and lead singer of metal band GWAR, has died according to local police.

Speaking to Times Dispatch, Richmond police spokeswoman Dionne Waugh explained that officers were called to a residence in the 4800 block of the city's West Seminary Avenue at 6.53pm on Sunday. Roommates reportedly found Brockie's body at his home on 23 March. He was 50-years-old.

No cause of death has been determined, and Waugh reports that detectives do not suspect foul play at this time.



"I wish it was a joke," the band's former bassist Chris Bopst and music writer for Style Weekly wrote. "Everyone is in shock."



The Grammy-nominated thrash band - known for their rabble rousing live performances featuring bloodied fake executions - released their 13th album in 2013 and mark their 30th anniversary this year. Brockie was formerly in the punk band Death Piggy, who staged mini-plays and used crude props to punctuate their music.

Vocalist Randy Blythe of fellow metallers Lamb Of God, posted his respects to the musician on Instagram:



When someone dies, a lot of the time people will say "Oh, he was a unique person, really one of a kind, a true original"- most of these people no idea of what they are talking about- they obviously had never met Dave. Dave TRULY WAS ONE OF A KIND- I can't think of ANYONE even remotely like him. That's VERY HARD TO SAY. My band learned how to become a real touring band from GWAR- they gave us out first shot at this thing. I learned many things from Dave, many of which I am eternally grateful for, and some of which I am deeply ashamed of.



