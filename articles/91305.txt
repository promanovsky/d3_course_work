Good morning, AdLand. Here's what you need to know today:

Taco Bell released a behind-the-scenes video showing how it made its popular "Ronald McDonald" ad, which promoted its new breakfast menu with real people named Ronald McDonald. In the video, the people used in the ad explain how they thought Taco Bell's outreach was initially a scam and what their reactions were when they realized all the people they were meeting up with were also named Ronald McDonald.

is launching a new ad campaign to promote three of its most popular creators: Michelle Phan, Bethany Mota, and Rosanna Pansino. The ads will run online, on TV, and in print.

Audience measurement firm Quantcast has appointed former The Weather Channel Companies CEO Mike Kelly to its board directors. Kelly will focus on "growth planning at scale," according to Quantcast.

is kicking off its World Cup campaign with a documentary series it filmed of Coke reps surprising recreational soccer players in remote corners of the world with World Cup tickets. The project comes from Wieden+Kennedy.

Ad Age reports that T-Mobile has seen an uptick in consumer perception of late, with the brand catching up to category leaders Verizon in YouGov's BrandIndex.

advertising

tapped FCB West to run its integratedaccount, which was previously held by Lowe Campbell Ewald. was forced to lay off staffers across several of its North American agencies. According to AgencySpy, the cuts affected less than 5% of the North American staff.

Motive hired Kohl Norville to be its first content director. Norville has freelanced for agencies like Pereira & O'Dell, Grey, SS+K, and Big Fuel.