The maker of Four Loko, the malt liquor beverage that once was made with caffeine, on Tuesday agreed to sharply curb its marketing of the fruit-flavored drinks as part of a legal settlement with 20 state attorneys general and San Francisco City Atty. Dennis Herrera.

The settlement comes nearly four years after Phusion Projects, based in Chicago, voluntarily removed caffeine from its drinks in 2010 shortly before the U.S. Food and Drug Administration definitively banned the stimulant as an ingredient in alcoholic beverages.

“This is an important step toward ending the irresponsible marketing of alcohol to young people,” Herrera said in a statement. “I’m grateful that we were able to get this industry leader on the same page with consumer protection offices in San Francisco and 19 states. The result will be better informed consumers and a safer, healthier marketplace.”

Four Loko beverages are sold in tall, brightly colored cans and come in a variety of fruit flavors. The drinks have been blamed in scores of deaths and hospitalization of young people since they were introduced in 2005. In recent years several states banned the sale of the beverage and regulators began looking into the safety of Four Loko.

Advertisement

The FDA eventually pointed to studies highlighted the danger of ingesting drinks that combine caffeine and alcohol. Researchers said the effects of the caffeine masked extreme intoxication among consumers.

Tuesday’s settlement is far-reaching. Under the terms of the agreement, the company cannot market Four Loko on college campuses or promote binge drinking and is forbidden from hiring actors under the age of 25 or who appear to be under 21 to promote its products.

It also cannot use any college logos or mascots in its marketing materials and must monitor its social media feeds and remove any references or photos of people drinking caffeinated alcoholic beverages.

The company also agreed to pay $400,000 to each of the states involved in the settlement.

Advertisement

[Updated 2:51 p.m. PDT March 25: In a statement, Jim Sloan, president of Phusion Projects, said, “While our company did not violate any laws and we disagree with the allegations ... we consider this agreement a practical way to move forward and an opportunity to highlight our continued commitment to ensuring that our products are consumed safely and responsibly only by adults 21 and over.”]