Click Here for More Articles on ARIANA GRANDE

Broadway star and breakout music performer Ariana Grande has at long last unveiled the cover for her hotly anticipated solo album, MY EVERYTHING, and the album is now available for pre-order with special bonus incentives on her official website.

Of note, Grande is set to duet with Justin Bieber on the forthcoming solo album, according to the performer herself in a recent interview, which also boasts the current chart-topping hit duet "Problem", featuring Iggy Azalea.

MY EVERYTHING is set to be released on August 25.

Pre-order Ariana Grande's MY EVERYTHING at the official site here.

In 2007, Grande appeared in the original Broadway production of Jason Robert Brown's 13 in the role of Charlotte, as well as starred along with Neil Patrick Harris, among others, in the Los Angeles stage spectacle A SNOW WHITE CHRISTMAS in 2012. Subsequently, Grande collaborated with pop star Mika on a cover of "Popular" from WICKED titled "Popular Song".

View a larger version of the cover for Ariana Grande's MY EVERYTHING below.

Photo Credit: Ariana Grande