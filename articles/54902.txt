Forget the craft and toil behind Bruce Springsteen's incomparable live act, or the everlasting commitment to creativity shown by the likes of Neil Young, PJ Harvey or Prince. Forget the business acumen and endurance of The Rolling Stones or Madonna, or even the bizarre media baiting by brilliant hip hop star Kanye West.

No thanks. Of all the many hundreds of genuine musical geniuses who American university Skidmore College could have chosen as the focus of a tertiary sociology course they settled on hammer-licking pop princess Miley Cyrus.

Case study ... Miley Cyrus' behaviours will be the subject of a sociology paper. Credit:Getty Images

Skidmore, in upstate New York, will offer a 2014 summer course entitled 'The Sociology of Miley Cyrus: Race, Class, Gender and Media'. It is the "brainchild" of Skidmore's visiting Assistant Professor of Sociology, Carolyn Chernoff.

American music website Billboard.com is reporting that Chernoff got the idea "after teaching a course on youth culture that featured video of Cyrus twerking at the 2013 MTV Video Music Awards".