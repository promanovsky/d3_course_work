Facebook Inc (NASDAQ:FB) announced that mobile app ads will have a link to a Facebook page, as well as social context and sharing options, starting from August 6th. Facebook’s desktop version already shows these links while displaying the ads. Desktop ads have options of like and share, and a link to the advertiser’s Facebook page and social context.

“You should create new mobile app ads and desktop app ads between July 2nd and August 6th as this is the easiest way to ensure your ads will be in the new format, associated with the Pages you want,” says an official blog post.

Blue Eagle Capital Partners: Long Thesis For This Lending Stock Blue Eagle Capital Partners was up 17.7% net for the third quarter of 2020, bringing its return to 49.1% for the first nine months of the year. During the third quarter, longs contributed 28.15% to the fund's performance, while shorts subtracted 7.36%. The S&P 500 was up 8.93% for the third quarter. Q4 2020 hedge Read More

Facebook to enhance mobile ad revenue

After July 2, advertisers will not be allowed to create ads based on the old norms that used to be context-less, and older ads will be automatically transformed to the new format, which is a context-rich version. Facebook Inc (NASDAQ:FB) has demonstrated the growing importance of ads for the social networking site, and taking this step will attract advertisers who want to reach mobile users.

The social networking giant is planning to grab larger share of the mobile space as it currently contributes 59% of company’s revenue. The contribution surged 30% over the past year alone. Facebook Inc (NASDAQ:FB)’s advertisement income surged 72% over the past 12 months and its total net income has been $2.5 billion between January and March this year according to a report from the BBC.

More insight to advertisers

Separately, to enrich its target advertising system, Facebook Inc (NASDAQ:FB) is planning to collect information outside Facebook. The information gathered will include personal information based on activities outside of Facebook. To date, the social networking website maintained the internal profile of customers based on their posts and the content liked by them. However, the method is going to change soon, with information gathering outside the boundaries of individuals Facebook profile.

Marketers will be able to target ads to particular customers more precisely, according to a Facebook blog post. For instance, if a user is searching for a new TV on any other website, Facebook Inc (NASDAQ:FB) will show the interest of the individual in electronics on his profile page, thereby giving more information to advertisers.

However, knowing that this new policy is likely to spark privacy concerns, Facebook Inc (NASDAQ:FB) has allowed its users to review and edit their internal advertising profile. When a user clicks an ad button he or she can see the “interest” on their record and remove unwanted categories.