Sens. John McCain and Jeff Flake are calling for a Senate investigation and hearings into allegations that up to 40 Arizona veterans died while awaiting medical appointments at the Phoenix VA Medical Center.

The two Republican senators from Arizona sent a letter Wednesday to leaders of the Senate Committee on Veterans' Affairs requesting a probe into "recent reports of gross mismanagement and neglect" at the facility.

McCain also wrote to Veterans Affairs Secretary Eric Shinseki, citing a report in the Arizona Republic on whistle-blower allegations about veteran deaths and accusations about VA administrators keeping secret waiting lists.

"I am appalled by the number of veterans who stated to my office that the VA was just “waiting” or 'hoping' that they would die and be one less burden on the system," McCain wrote. "These increasing individual delays clearly illustrate systemic problems with how effectively the VA is providing care to our veterans."

Last week, Jeff Miller, R-Fla., chairman of the House Committee on Veterans Affairs, said the panel's investigators concluded as many as 40 Arizona veteran deaths could be related to VA delays in providing them medical care.

Some veterans say wait times now average 55 days.

Miller also said panel investigators had evidence that officials at the Phoenix VA Health Care System kept two sets of records to hide lengthy wait times for patients seeking doctor appointments and treatment, the Arizona Republic reported.

Dr. Sam Foote, a former VA doctor in the Phoenix system, told Megyn Kelly on "The Kelly File" Thursday night "we believe senior management obviously had knowledge of this."

Earlier, he told CNN that the alleged secret waiting list was used by senior management to conceal the fact that 1,400 to 1,600 sick veterans were forced to wait months to see a doctor.

Phoenix VA Health Care System officials said they've asked for an external review by the Inspector General and will address any problems quickly.

Sharon Helman, director of the Phoenix VA system, told the Arizona Republic on Tuesday she is unaware of any patients who died awaiting care, and that she does not know of any improper manipulation data related to patient access.

Flake told a news conference last week that the Inspector General for the Department of Veterans Affairs also is investigating the complaints about Arizona VA facilities.

In a statement, the VA said it "cares deeply" for veterans and added that the Phoenix VA took the issues "very seriously" and invited the independent VA Office of the Inspector General to complete a comprehensive review of these allegations."

The Associated Press contributed to this report.