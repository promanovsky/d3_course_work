A closeup of one of the bills left by the mystery donor leaving cash in public places and tweeting its location. (CBS)

SAN FRANCISCO (KPIX 5) — An unidentified Twitter user has been hiding money in San Francisco and Oakland on Friday, leaving it for anyone to find.

The scavenger hunt began Thursday night from Twitter account @Hiddencash. Dozens of tweets have been posted, containing clues to $100 bills stashed throughout both cities.

Cash was apparently dropped off at Bush and Powell Streets, but Shannon Brown missed the cash.

“We literally ran out of Bloomingdales, down the road, through the whole mall, and we got there. And there were a whole crowd of people around looking for it, but it had already been taken,” Brown told KPIX 5.

Read More:

RadioAlice.com: A Very Rich Person Is Apparently Planting Money Around SF Today

Noe and 15th Street was another location where cash was found.

“We got there in about four minutes, but it was just too late,” said Anna Powell.

As the hours ticked by on Friday, the buzz around the anonymous donor grew, as did the crowds looking for the cash.

Paul Garves missed the cash that was located at Mint Plaza.

“They were checking Twitter and said that right outside in the Plaza, somebody was hiding $100 bills and there was one right underneath the chair out here. I guess we were just a little too late to grab it today,” Garves said.

Alfie Estrada was one of the lucky ones. Estrada said he found a $20 bill marked with the hashtag #hiddencash at the Lake Merritt BART Station in Oakland. He paid it forward in the form of an online donation.

“I donated it to AIDS/LifeCycle. It’s one of my favorite charities,” Estrada said.

The identity of the mystery tweeter is unknown. He or she told The Bold Italic via email that they had made millions in the San Francisco real estate market and wanted to do some good.

“Awfully nice, I kind of wish I could do the same thing. I don’t know that I necessarily would if I had that kind of money, but I’d like to think I would,” Garves said.

The only request from the shy millionaire was that people who find the cash should post a picture of themselves on Twitter with the green.

Maybe Alfie’s donation was just what the mystery donor had in mind.

“I thought it was an easy way to turn something fun into something good,” Estrada said.

At least $1,000 in cash has been given away as of Friday night. The person said they plan to give out cash through the Memorial Day holiday weekend.