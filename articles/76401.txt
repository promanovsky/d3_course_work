WASHINGTON -- New research is boosting hopes that weight-loss surgery can put some patients' diabetes into remission for years and perhaps in some cases, for good.

Doctors on Monday gave longer results from a landmark study showing that stomach-reducing operations are better than medications for treating "diabesity," the deadly duo of obesity and Type 2 diabetes. Millions of Americans have this and can't make enough insulin or use what they do make to process food.

Many experts were skeptical that the benefits seen after a year would last.

Now, three-year results show an even greater advantage for surgery.

Blood-sugar levels were normal in 38 per cent and 25 per cent of two groups given surgery, but in only 5 per cent of those treated with medications.

The results are "quite remarkable" and could revolutionize care, said one independent expert, Dr. Robert Siegel, a cardiologist at Cedars-Sinai Medical Center in Los Angeles.

"No one dreamed, at least I didn't," that obesity surgery could have such broad effects long before it caused patients to lose weight, he said. Some patients were able to stop using insulin a few days after surgery.

At three years, "more than 90 per cent of the surgical patients required no insulin," and nearly half had needed it at the start of the study, said its leader, Dr. Philip Schauer of the Cleveland Clinic. In contrast, insulin use rose in the medication group, from 52 per cent at the start to 55 per cent at three years.

The results were reported Monday at an American College of Cardiology conference in Washington. They also were published online by the New England Journal of Medicine.

Doctors are reluctant to call surgery a possible cure because they can't guarantee diabetes won't come back.

But some patients, like Heather Britton, have passed the five-year mark when some experts consider cure or prolonged remission a possibility. Before the study, she was taking drugs for diabetes, high blood pressure and high cholesterol; she takes none now.

"It's a miracle," said Britton, a 55-yeear-old computer programmer from suburban Cleveland.

"It saved my life. I have no doubt that I would have had serious complications from my diabetes" because the disease killed her mother and grandmothers at a young age, she said.

About 26 million Americans have diabetes, and two-thirds of them are overweight or obese. Diabetes is a leading cause of heart disease, strokes, kidney failure, eye trouble and other problems.

It's treated with various drugs and insulin, and doctors urge weight loss and exercise, but few people can drop enough pounds to make a difference. Bariatric surgery currently is mostly a last resort for very obese people who have failed less drastic ways to lose weight.

It costs $15,000 to $25,000 and Medicare covers it for very obese people with diabetes. Gastric bypass is the most common type: Through "keyhole" surgery -- also known as laparoscopic surgery -- doctors reduce the stomach to a small pouch and reconnect it to the small intestine. Another type is sleeve gastrectomy, in which the size of the stomach is reduced less drastically.

Schauer's study tested these two operations versus medication alone in 150 mildly obese people with severe diabetes. Their A1c levels -- a key blood-sugar measure -- were over 9 on average at the start. A healthy A1c is 6 or below and the study aimed for that, even though the American Diabetes Association sets an easier target of 7.

After three years, researchers had follow-up on 91 per cent of the original 150 patients. The medication group's A1c averaged 8.4; the surgery groups were at 6.7 and 7, with gastric bypass being a little better.

The surgery groups also shed more pounds -- 25 per cent and 21 per cent of their body weight versus 4 per cent for the medication group.

Some cholesterol and other heart risk factors also improved in the surgery groups and they required fewer medicines for these than at the start.

Doctors don't know how surgery produces these benefits, but food makes the gut produce hormones to spur insulin, and trimming away part of it affects many hormones and metabolism.

Four patients needed a second surgery within a year but none did after that. Out-of-control diabetes has complications, too -- many patients lose limbs or wind up on dialysis when their kidneys fail, and some need transplants.

An obesity surgery equipment company sponsored the study, and some of the researchers are paid consultants; the federal government also gave grant support.

Dr. Robert Ratner, chief scientific and medical officer for the American Diabetes Association, said he was "very encouraged" that so many stayed in the study, and said it will remain important to follow participants longer, because many people who have weight-loss surgery regain substantial weight down the road.

"Any way you lose weight is beneficial" for curbing diabetes, he said, but "we need to be concerned about the cost and complications" of treatments. Diets cost less and have fewer side effects, Ratner said.

One other common type of obesity surgery, stomach banding, was not part of this study. Its use has declined in recent years as other types of surgery have shown long-term benefits for keeping weight off.