Talk about dirty money: Scientists are discovering a surprising number of microbes living on cash.

In the first comprehensive study of the DNA on dollar bills, researchers at New York University's Dirty Money Project found that currency is a medium of exchange for hundreds of different kinds of bacteria as bank notes pass from hand to hand.

By analyzing genetic material on $1 bills, the NYU researchers identified 3,000 types of bacteria in all—many times more than in previous studies that examined samples under a microscope. Even so, they could identify only about 20 percent of the non-human DNA they found because so many microorganisms haven't yet been cataloged in genetic data banks.

Easily the most abundant species they found is one that causes acne. Others were linked to gastric ulcers, pneumonia, food poisoning and staph infections, the scientists said. Some carried genes responsible for antibiotic resistance.

"It was quite amazing to us," said Jane Carlton, director of genome sequencing at NYU's Center for Genomics and Systems Biology where the university-funded work was performed. "We actually found that microbes grow on money."

Their unpublished research offers a glimpse into the international problem of dirty money. From rupees to euros, paper money is one of the most frequently passed items in the world. Hygienists have long worried that it could become a source of contagion.

"A body-temperature wallet is a petri dish," said Philippe Etienne, managing director of Innovia Security Pty Ltd., which makes special bank-note paper for 23 countries.

Central banks and state treasurys usually worry more about counterfeiting and durability than microbiology, several currency experts said. With nearly 150 billion new bank notes circulated every year around the world, governments spend nearly $10 billion annually to provide people with notes that are fit to hold.

A U.S. one-dollar bill, printed on a cotton-linen blend, lasts little more than 21 months. In all, the U.S. Federal Reserve System is spending $826.7 million on new money this year to make 7.8 billion bank notes with a total face value of $297.1 billion.

Click for more from The Wall Street Journal.