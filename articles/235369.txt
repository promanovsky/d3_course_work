Researchers have been trying to come up with an explanation for the mysterious "fairy circles" that pop up in the African grassland regions; so far they know that the vegetation-free circles are not caused by termites.

A research team performed an analysis of the circles and found they had a "remarkably regular and spatially comprehensive homogenous distribution pattern," a Helmholtz Centre for Environmental Research news release reported.

The researchers believe the phenomenon could be tied to competition for water between local vegetation.

"Although scientists have been trying to answer this question for decades their mystery remains as yet unresolved," Doctor Stephan Getzin from the Helmholtz Centre for Environmental Research (UFZ), said in the news release.

The most popular theory in the past has been that termites cause the mysterious circles; hydrocarbons coming from deep within the Earth have also been considered as a possibility.

These circles only occur in arid regions where grasslands transition into desert. Some researchers believe they are caused by vegetation competition that leeches moisture from the soil.

Researchers looked at aerial images of the plains to analyze the spatial location of the bare circles. They found the circles had a regular and homogenous distribution.

"The occurrence of such patterning in nature is rather unusual,"Getzin said. "There must be particularly strong regulating forces at work"

This new research discredits the popular termite theory. Researchers have never witnessed the insects grazing in the region.

"There is, up to now, not one single piece of evidence demonstrating that social insects are capable of creating homogenously distributed structures, on such a large scale," Getzin said.

The researchers used a computer model to look at below ground water competition between various plants. The model showed this process would be capable of creating "homogeneously scattered circles."

"Each mature tree, after all, needs sufficient space and nutrition for its development and will therefore be able to survive only at an appropriate distance to its [neighbor]. A similar process of resource-competition may consequently also be the real cause for a self-[organizing] formation of the mysterious fairy circle patterns," the news release reported.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.