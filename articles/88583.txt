By Ian Bush

PHILADELPHIA (CBS) — Tuesday’s the day: Microsoft is X-ing out Windows XP, pulling support for the operating system nearly 13 years after its release. But with some estimates showing XP is still installed on more than a quarter of the world’s PCs, will they now become a prime target?

This is not another Y2K — there’s no question XP will work as it’s left in the dust.

“But you do have to accept a certain amount of risk in that you’re not going to have a patched system anymore,” says Chris Pogue, a director at the information security company Trustwave.

Despite years of warnings from the software giant and a recent PR push, millions of computer users seem content to stick with XP. Studies estimate between a quarter and a third of the world’s PCs are still using that version of Windows.

“Seventy-seven percent of organizations reported that they are still running some machines with Windows XP,” says Scott Kinka, the chief technology officer at Evolve IP. “Eight in 10 reported that they plan on continuing supporting XP internally for some period of time,” according to a recent report from the Wayne, PA-based cloud services company.

On web forums, some commenters say the hubbub is overblown — that it’s just fear-mongering from Microsoft, which wants people to buy new software.

“I certainly would not be taking it lightly,” Kinka says. “It makes me feel very uncomfortable. Particularly in businesses that have access to sensitive information on their customers, like credit cards and social security numbers.”

Even though major antivirus companies have promised to continue their support of the operating system, Kinka says attackers could try to reverse-engineer patches Microsoft puts out for its newer software to strike the same flaws in XP.

Pogue isn’t so sure.

“Windows 7 and 8 are not XP with more bells and whistles,” he argues. “They’re completely different file systems, the registry’s totally different, the user interface is different. You’re dealing with a lot of new code. To say that a vulnerability that exists in 7 or 8 that is patched and can subsequently reverse-engineered and applied to XP — I honestly doubt it’s going to be able to be executed in any sort of large volumes.”

But Kinka says an operating system will continue to be a vulnerable target for attack.

“The majority of attacks are passive, not active,” he explains. “The difference is in an active attack, I’m looking to get you — I’m going to monitor you and find a way in. A passive attack usually is nothing more a would-be hacker scanning a list of addresses on a network, pinging machines to see if they respond and tell me that they’re running XP. It doesn’t matter if they’re a big business, a small business, or an individual in their home — they’re just looking for end units that respond positively to having a particular OS or vulnerability. Often, they just sell that information to a list.”

While XP users might be exposed to new issues, that doesn’t mean others are out of the woods.

“I’ve been investigating computer crimes and data breaches for close to nine years,” Pogue says. “A big percentage of them have been on fully patched, fully up-to-date Windows XP systems.”

He suggests businesses especially arm themselves with firewalls, strong password and network access policies, and to align themselves with a firm whose business is cybersecurity. Kinka encourages companies to investigate desktop virtualization – keeping the operating system in the cloud.