Apple is now selling the 4th-generation iPad for $399 with 16 GB of storage and Wi-Fi connectivity, or $529 with 4G LTE, essentially replacing Apple’s iPad 2 in the lineup.

The 4th-generation iPad debuted in 2012, but disappeared last fall, when Apple launched the iPad Air. At the time, most pundits agreed that the iPad 2 was a bad deal at $399, especially when you could get an iPad Mini for $100 less, or a state-of-the-art iPad Air for $100 more. Still, the iPad 2 remained popular, and had appeal for education and point-of-sale markets where the latest technology is less important.

But perhaps Apple was just clearing inventory, and is now ready to move on to selling a newer, better iPad in the $399 slot. It’s definitely worth considering as a cheaper alternative to the iPad Air; both tablets have Retina displays, fast processors and similar camera quality. The biggest difference between them is weight and thickness, and the inability to get more than 16 GB of storage with the 4th-generation model.

If you want a thin and light tablet with more storage options, the smaller iPad Mini with Retina display has many virtues, and the same $399 base price as the 4th-generation iPad.

Apple is also adding an 8 GB iPhone 5c to its lineup, at least in some markets. The U.K. Apple Store is now selling the 8 GB model for £429 — £40 cheaper than the 16 GB model — while the iPhone 4S remains the cheapest option at £349. The 8 GB iPhone 5c is also on sale now in Australia, China and France, but so far it’s not available in the United States.

Already, U.S. retailers often sell the 16 GB iPhone 5c at a discount. A recent sale at Walmart, for instance, brought the price down to $29 with a two-year contract. As long as retailers are offering the 16 GB model on the cheap, and the 8 GB iPhone 4S for free, it doesn’t make much sense for Apple to slot in the 8 GB iPhone 5c.

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.