Tuesday's report of a lower than expected growth in home prices is just the latest sign that the housing sector can't be expected to drive strong economic growth in the U.S.

The S&P/Case-Shiller Home Price Index survey of 20 cities showed a 9.3 percent increase for May compared to a year earlier. While this is a decent number in itself, it is down from a 10.8 percent yearly pace in April and lower than the 9.9 percent expected by many economists.

It is also the slowest rate of increase since February 2013. Of the 20 cities surveyed, 18 had slower year-over-year growth in May than in April, according to the report. San Francisco and San Diego saw their year-over-year figures decelerate by about three percentage points, the report said.

While these are weaker than expected numbers, overall they indicate a market which, like the rest of the economy, continues on a slow recovery.

"This time last year, prices began to grow at double-digit yearly rates; with this in mind, a smaller increase over a higher base is a healthy sign," analysts at IHS Global Insight said in a note to investors. "Since home price appreciation is closely tied to the economic health of the cities covered in the index, this report suggests that home price growth is fueled by fundamentals, and not a speculative bubble."

Not surprisingly the price gains have been slowing as a result of fewer sales. Monday the National Association of Realtors reported pending home sales were down 1.1 percent in June from the month before and 7.3 percent below year-ago levels.

It is worrisome that prices gains have been slowing even as the number of foreclosed homes on the market is decreasing. Foreclosed homes, which sell at lower prices, had been considered a significant drag on average housing prices for several years. But, according to the NAR, so-called "distressed" sales accounted for just 11 percent of sales in June, down from 15 percent last year, 25 percent in 2012, and 30 percent in 2011.

A report earlier this month from the Commerce Department suggested there is little likelihood of a dramatic rebound in sales volume anytime soon. The report found housing starts unexpectedly declined 9.3 percent in June from the month before, led by a record plunge in the South. This is the lowest rate in nine months.

The reason for all this weakness in number of sales: "Supply shortages still exist in parts of the country, wages are flat, and tight credit conditions are deterring a higher number of potential buyers from fully taking advantage of lower interest rates," Lawrence Yun, chief economist for the NAR, said in a statement.

Mortgage lenders, still smarting from their own overly loose lending policies which caused the financial crisis, are keeping a tight leash on home loans, according to a report from Goldman Sachs last week. The report found mortgage credit has become slightly more available in the past two years, but it is still tighter than it was during 2000-2002, right before lenders began easing mortgage requirements.

Another trend that may be holding back a more aggressive rebound in real estate -- most of the price gains in housing have been among the most expensive homes. The majority of home owners haven't seen an increase in what they can sell their houses for. That gives them little leverage to move to a newer or larger home.

As a result of the housing crisis, followed by an uneven recovery, the homeownership rate in the U.S. has fallen to a 19-year low. According to the Census Bureau, the share of Americans who own their homes was 64.7 percent in the second quarter, down from 64.8 percent in the previous three months. The rate is now at the same level as it was in the second quarter of 1995.