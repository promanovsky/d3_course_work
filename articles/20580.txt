Parents may be at least partly to blame for the obesity risk of their infant children, according to a new study from researchers at the University of North Carolina. For the study the researchers surveyed nearly 900 parents in low-income families on the feeding and exercise routines they use for their infants.

They found that many of the parents used feeding techniques that can lead to obesity. For example 43 percent of the parents put their kids to bed with a bottle and 19 percent propped bottles up for their kids, allowing greater flow of milk or formula faster.

They also found that 90 percent of the kids were exposed to television while 50 percent watched television actively on a daily basis. The researchers conclude that while genetics play a large part in obesity risk, parents can do more to prevent it:

"These results from a large population of infants - especially the high rates of television watching - teach us that we must begin obesity prevention even earlier," they explain in their study report.

For comments and feedback contact: editorial@rttnews.com

Health News