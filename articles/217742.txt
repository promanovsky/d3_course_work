Former Disney Store chief creative officer and marketing exec Ivy Ross today announced her next endeavor: Google Glass.

Starting Monday, Ross will replace Babak Parviz as head of the Glass team, working under Google X and reporting to Astro Teller and Sergey Brin, according to BBC News.

"With your help, I look forward to answering the seemingly simple, but truly audacious questions Glass poses," Ross wrote in a Google+ post. "Can technology be something that frees us up and keeps us in the moment, rather than taking us out of it?" she asked. "Can it help us look up and out at the world around us, and people who share it with us?"

Ross has worked for a number of big-name employers, including Calvin Klein, Swatch, Coach, Mattel, Bausch & Lomb, Gap, and Art.com, where she worked "at the intersection of design and marketing."

Now, she'll use that advertising know-how to boost Google Glass, an "especially cool" challenge, Ross said, "as no one has really tried to answer [those questions] with a product like this before."

The high-tech headset may still be in its infancy, but it is finding itself in unique situations. The University of California Irvine recently announced plans to hand out close to 40 pairs of Glass to med students participating in the school's four-year program. Pupils will integrate the technology into their labs and lectures, learning to perform hands-free tasks while caring for patients.

Plus, Google this week reopened public sales of the spectacles, offering Explorer Program spots to anyone in the U.S. with $1,500 to spare.

"I'm just getting started on Glass, but, because of all of you, and your thoughtful and smart feedback, I feel like I have an incredible head start," Ross said. "And I look forward to learning even more from you, and experiencing Glass together."

As TechCrunch notes, meanwhile, the Google Glass team recently lost electrical engineer Adrian Wong, who is moving to Oculus VR.

For more, see PCMag's review of Google Glass Explorer Edition Version 2.0. Also check out Google Glass: Everything You Need to Know.

Further Reading

Computers & Electronic Reviews