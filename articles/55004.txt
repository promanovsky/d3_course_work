Swiping Cynthia! Veteran ABC News anchor/correspondent Cynthia McFadden is jumping ship to NBC News to be the network's Senior Legal and Investigative Correspondent, NBC announced Thursday, March 27. In her place, Juju Chang has been promoted to the position of Nightline co-anchor, ABC News said in a separate statement.

"After 20 incredible years at ABC News I've decided to take on new challenges at NBC News," McFadden, 57, said in NBC's statement. "While it is not easy leaving the Nightline anchor chair, the opportunity NBC offered to make a deep dive into the kind of reporting I am most passionate about — legal and investigative — was just too appealing to resist. I'm grateful to everyone at ABC for a marvelous journey and very much looking forward to joining my new colleagues at NBC."

NBC News President Deborah Turness called McFadden a "rare talent," adding that the journalist was "passionate about journalism, compassionate in her view of the world, truly gifted in the art of storytelling."

McFadden will be working with NBC News' Chief Legal Correspondent Savannah Guthrie, who is currently a cohost on the Today show.

McFadden's decision to go to a competitor had no ill bearing on ABC News President Ben Sherwood's remarks Thursday, in which he praised her for being a "key member of the ABC News team for the past 20 years." Sherwood graciously added, "Cynthia has been an amazing colleague, a loyal mentor to so many of our up and coming producers and I am personally grateful for her profound contributions to ABC News. It is with great fondness that I wish Cynthia the very best in her new adventure."

According to Sherwood's statement, McFadden informed him of her decision to join NBC News earlier this week.

He went on to praise Chang, 48, as a "vital member of the Nightline team for many years." Chang has been with ABC News for her entire career, starting at the network as a desk assistant. "She is also the ultimate team player," Sherwood raved.