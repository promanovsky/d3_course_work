A combination of increased supply and relatively static demand should reportedly help keep gas prices stable , as the nation enters into the prime, summer driving months.

According to the Department of Energy's forecast, regular gasoline retail prices are expected to average around $3.57 a gallon during the upcoming summer season – or about one cent less than they were last summer.

The price for Brent crude oil – which typically accounts for about two-thirds of the retail price of gasoline – is expected to average $105 per barrel this summer driving season (April through September), which is about $2 below its level last summer,” said Monday's entry in a DOE blog.

But those lower crude prices are expected to be “almost fully offset” by higher wholesale margins, compared to last summer.

Related: Consumers Are Less Confident In Automotive Industry, But Is That Affecting Sales?

Gas prices, according to the Department, are expected to peak in May – with an average of $3.66 per gallon – and then decline steadily to around $3.46 in September. And, on average, the highest gas prices this summer will probably be found on the U.S. West Coast – with prices along the Gulf Coast as much as 48 cents per gallon lower.

Gas consumption this summer is expected to be close to last summer's level of nearly nine million barrels per day, despite a 0.7 percent expected rise in highway travel. But improvements in vehicular fuel efficiency, according to the DOE, is taking care of that discrepancy.

Global economics and geopolitics are also in play with this summer's forecast. The ongoing oil and gas boom in the U.S. and Canada are certainly benefiting American consumers. Meanwhile, unrest in parts of the Middle East, including Libya and Sudan, have disrupted some oil production, which The New York Times says has prompted OPEC to increase its overall oil production.

At the same time, as Europe struggles to recover from its debt crisis and the Chinese economy slows down, international oil demand has remained stable.

Of course, the ongoing Ukraine/Russia crisis still remains as this summer's wild card, when it comes to any possible fluctuations in overall gas prices.