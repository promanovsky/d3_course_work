Naina Khedekar

The Rubik’s Cube now turns 40, and Google is paying homage for the classic brain teaser with a doodle. The Google Doodle is a replica of the cube and lets you play virtually on your computer screens.

Needless to say, the goal of the game is to successfully bring similar coloured nine boxes on each side of the cube. In the online gameplay, users can click and drag outside the cube to rotate it and similar drag inwards to twist it. Users will require the latest version of Chrome, Firefox or Safari to play the game.

Rubik's Cube was invented in 1974 by Erno Rubik, a Hungarian inventor, architect and professor of architecture. It was originally called Magic Cube, and Rubik licensed it to Ideal Toy Corp in 1980. In fact, Rubik was known for inventing mechanical puzzles - Rubik’s Cube Invention, the Rubik’s Magic 3D combination puzzle, Rubik’s Magic: Master Edition and the Rubik’s Snake. While Rubik’s Cube attracted people across the globe, the other inventions weren't really popular.

Google has been quite innovative with its animated doodles, which sometimes also include online gameplay of some classic games. In 2010, Google had doodled the Pac-man game, allowing users to play the game on their desktop screens. It was reportedly accused to wasting 4.82 million hours.