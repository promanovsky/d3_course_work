Autism begins during pregnancy. Olivia Kelly (6) takes a kiss from Buck the therapy dog at the Life Skills/Touch Point Autism Services "Milk and Cookies with Santa" event in Maryland Heights, Missouri. UPI/Bill Greenblatt | License Photo

SAN DIEGO, March 27 (UPI) -- U.S. researchers say they found new evidence that autism begins during pregnancy and affects brain development.

First author Rich Stoner and Eric Courchesne of the Autism Center of Excellence at the University of California, San Diego and Ed S. Lein of the Allen Institute for Brain Science in Seattle analyzed 25 genes in post-mortem brain tissue of children with and without autism.

Advertisement

These included genes that serve as biomarkers for brain cell types in different layers of the cortex, genes implicated in autism and several control genes.

“Building a baby’s brain during pregnancy involves creating a cortex that contains six layers,” Courchesne said in a statement. “We discovered focal patches of disrupted development of these cortical layers in the majority of children with autism.”

The scientists said such patchy defects might help explain why autism spectrum disorder is so varied and why many toddlers with autism show clinical improvement with early treatment and over time.

“The most surprising finding was the similar early developmental pathology across nearly all of the autistic brains, especially given the diversity of symptoms in patients with autism, as well as the extremely complex genetics behind the disorder,” Lein explained.

The study, published online in the New England Journal of Medicine, found in the brains of children with autism, key genetic markers were absent in brain cells in multiple layers.

RELATED Region of the brain essential for social memory is identified

“This defect, indicates that the crucial early developmental step of creating six distinct layers with specific types of brain cells -- something that begins in prenatal life -- had been disrupted," Courchesne said.

RELATED Maternal infections during pregnancy linked to autism risk

RELATED Changes in bacteria in the gut can influence autism behaviors

RELATED Signs of autism present in the first months of life

RELATED High air pollution areas linked to double the risk of autism

RELATED Higher antibodies to gluten proteins in children with autism