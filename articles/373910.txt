Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Sick death threats were sent to the man behind a court challenge to Garth Brooks’ shows, he revealed yesterday.

As a bitter blame game kicked off, Brian Duff, 43, said he and his family were cruelly harassed after he sought a High Court injunction to stop the three permitted gigs.

As Brooks last night begged for an 11th-hour deal to make his five Croke Park shows happen, the dad of four revealed online trolls threatened to kill him and his family for going to court.

Dublin's Lord Mayor and the chief executive of the city council are to hold crisis talks today to see if the gigs can be saved.

But in the meantime, Brian Duff revealed how his life has been turned upside by his legal action to stop the gigs.

He said: “The injunction I put in wasn’t about Garth Brooks – it was my way to get the GAA legally chastised for breaking the law. If people break the law, they are chastised. Is the GAA bigger than the law? I went for it.

“It escalated and went a bit too far. I’ve had threats against my life now. Others were saying they were going to p**s in my garden or get sick in it. But the death threats are too far.”

The scaffolder, who claimed he collected €15,000 from friends to support the legal action which will now be called off, added he wants to put the issue behind him.

He is now staying in a B&B until the intense pressure recedes.

Mr Duff, of North Cumberland Street in Dublin’s North inner city, plans to pull the injunction from the courts today.

His solicitor Anthony Fay said: “It will all be sorted. There will be a statement released. As it is currently before the courts, that’s all I will say.”

The Lord Mayor of Dublin has slammed reported death threats against the man who tried to stop the Garth Brooks gigs in court.

Christy Burke defended Mr Duff, who has received a number of threats against his life, including on social media.

He appealed to the people of Dublin "to resist this type of behaviour" as it is "totally unacceptable".

As the blame game intensified last night, Enda Kenny and Environment Minister Phil Hogan were also in the firing line, accused of doing nothing to save the concerts.

The Taoiseach – who finally intervened last night – admitted the Government had chickened out of getting involved as it risked being accused of interfering in the planning process.

As the main players in the debacle will be grilled by TDs in the Dail tomorrow, the Government was slammed for not rushing through legislation which would have enabled Mr Hogan to reverse Dublin City Council’s decision to allow just three concerts.

But nothing was done and last night the country icon stood by his word that he would only play five – or none at all.

(Image: Getty Images)

Promoter Peter Aiken said it was “game over” for the comeback shows, leaving 400,000 fans crushed.

Senator Averil Power branded the Coalition “spineless” for “sitting on their hands and waiting for our economy to take another battering”.

She told how a Bill drafted in the Seanad could have saved the shows, adding: “This is a complete lack of

political leadership. We are talking about €50million at a time when it’s hard to get cash from anywhere.

“The Government should have introduced legislation which could have changed things.”

Fan Neal Barry told the Irish Mirror’s Facebook poll: “My vote went to the Government for not overturning it.”

Aoife Duffy said: “How can people say the Government is not to blame. It makes me sick they don’t even have control of the county council.”

Fianna Fail leader Micheal Martin also demanded answers.

He said: “Surely the Government should have intervened at some stage in this debacle? Hoteliers, restaurant owners, publicans, young people looking for work simply can’t believe the country can almost nonchalantly say, ‘We don’t need that’.”

“Is anybody going to do anything about it?”

But Mr Kenny said the Coalition risked being accused of “doing down” the rights of Croke Park residents and interfering with the planning process if it stepped in.

Describing the fall-out as a “bitter economic lesson to have learned”, the Taoiseach said he has ordered a review of the planning process for major events.

He added: “It’s a major loss to the country, to the goodwill and good feeling of all those fans of Garth Brooks that this is lost, not to mention the hard economic loss to people. It’s a mess.”