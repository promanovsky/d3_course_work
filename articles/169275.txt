'Amazing Spider-Man 2' Game: Carnage Confirmed in New Trailer 'Amazing Spider-Man 2' Game: Carnage Confirmed in New Trailer

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

The Amazing Spider-Man 2 game for various consoles will feature the vicious Carnage.

A new trailer was released by Activision earlier this week that shows a number of villains that will appear in the game including the Green Goblin, Shocker, Electro, Venom, Kraven and a showdown with Carnage at the end.

The new Spider-Man games are loosely based on the films. They feature completely different storylines and have villains in them that haven't appeared in any Spider-Man movie. The Spider-Man 2 game is available for the PS4, PS3 and Xbox 360.

Carnage might also make an appearance in a spin-off Spider-Man movie that tells the story of one of his worst enemies, Venom.

The creators of "The Amazing Spider-Man 2" hinted at the possibility earlier this month after The Daily Bugle tumblr ran a story about notorious serial killer Cletus Kasady that was written by Eddie Brock.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

For those unfamiliar with Spider-Man cannon, Kasady is the human vessel that the Carnage alien symbiote that came from Venom, or Eddie Brock, clings onto. Carnage is a product of Venom so featuring him the film will come as no big surprise.

IGN asked co-producers Avi Arad and Matt Tolmach about the possibility of this happening and Arad replied by saying "What a great idea." Tolmach elaborated stating "Exactly. The idea of Venom and Carnage … taking it into consideration. Watch out for this Venom movie. We are crazy excited."

Both of them expressed their confidence in the two major villains in Spider-Man lore.

"We did Venom in the U.K. as a special issue based on the '94 show: the three parter," said Arad. "And I think if you check the record book it was probably one of the best-selling videos. Way before DVD. Because the character is interesting. Even Venom is not- we call them villains, but he's really not a villain."

The Venom film will be directed by Alex Kurtzman and does not have a confirmed release date at this time.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit