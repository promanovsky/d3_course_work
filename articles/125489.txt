Google Inc.'s off-white Google Glass models were all sold out during a special one-day sale event on Tuesday.

Google announced Thursday it is holding a special one-day sales event featuring the Glass. This is to give those who want to be part of the Explorer program, but haven't found a way in yet. The company decided to continue with the event after being pressured when The Verge published some details about its "Explorer Program" even before any formal announcement.

Google+ stated in a blog post, "Next Tuesday, April 15th at 9 a.m. EDT, we're opening up some spots in the Glass Explorer Program. Any adult in the U.S. can become an Explorer by visiting our site and purchasing Glass for $1,500 + tax -- and it now comes with your favorite shade or frame, thanks to feedback from our current Explorers."

During the sales event, Google told initially planned to extend the sales of its Glass. However, after selling out all of its off-white Google Glass models in just a few hours, the company decided not to continue with the extension and just end its one-day sales event, Mercury News reports.

Google did not reveal the total number of units sold during the event, but it posted a message in Google+, "Wow, what a morning! We're happy to see so many new faces (and frames) in the Explorer community. Just a quick update that -- ack -- we just sold out of Cotton, (a color of Google Glass) so things are moving really fast."

J.P. Gownder, vice president and principal analyst at Forrester, told Mercury News that the demand for Glass will give the company an idea of the possible sales turnout when it officially launch the device to the market.

Aside from its capability to film surgical operations and basketball games, or for getting directions while driving, Google Glass also lets its users access the same tools enjoyed by smartphone and tablets just by using voice and touch activation. However, it will not work with lighter, wearable packages.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.