The Toronto stock market continued its climb into record territory Tuesday, boosted in part by major corporate deal making as Burger King announced it's buying Tim Hortons.

New York's S&P 500 index closed above 2,000 for the first time and the Dow and the Nasdaq market up at near-record levels.

Toronto's S&P/TSX composite index was up 20.47 points to a record-high close of 15,619.21 but off early highs with losses growing in the financial sector as traders also looked over mixed earnings results from Bank of Montreal and Scotiabank.

The Canadian dollar rose 0.24 of a cent to 91.31 cents US.

The $11-billion US deal for Tim Hortons by Burger King drove sentiment as analysts predicted it will improve the coffee chain's prospects in the U.S..

In New York, the Standard & Poor's 500 index closed a hair above 2,000 points Tuesday, 16 years after it closed above 1,000 points for the first time. It closed the day at 2,000.02.

The milestone added to the market's gains from the day before and extended the stock index's record-shattering run this year.

The S&P 500 index, a widely followed barometer of the stock market, has closed at a new high 30 times this year. By this time last year, it had done so 25 times..

"There's perhaps a small psychological boost when you get over such a significant price level," said Cameron Hinds, regional chief investment officer at Wells Fargo Private Bank.

U.S. stocks, in the midst of a five-year rally, have surged in the final weeks of the summer after dipping earlier this month on concerns about geopolitical tensions in Russia and the Middle East.

The latest string of shattered market benchmarks comes as investors cheered new indications that the economy is strengthening, setting the stage for stronger company earnings.

Consumer confidence up

The Conference Board said Tuesday that its consumer confidence index rose this month to the highest point in nearly seven years. A separate report showed that orders of durable manufactured goods surged by a record 22.6 percent in July, thanks to a jump in aircraft sales. A third report showed U.S. home prices rose in June, although at a slower pace.

Stocks opened slightly higher and remained in the green the rest of the day. The S&P crossed above 2,000 points early on, and hovered at or above the mark as it approached the close of regular trading.

Moments before the close it dipped below 2,000, then inched up just above.

The Dow Jones industrial average rose 29.83 points, or 0.2 percent, to 17,106.70. The Nasdaq composite gained 13.29 points, or 0.3 per cent, to 4,570.64.

The string of record highs this year isn't unusual when a market is recovering from a downturn, said Kate Warne, an investment strategist at Edward Jones.

Climbing markets

In the past, once stocks have hit a new high after a downturn, they have continued higher for about two years, on average, she said. The first time the S&P 500 hit a new high after the financial crisis was March 2013. So this year's record run is still within the average range.

"Markets don't climb sharply. They tend to climb slowly, and that's probably good news for a continued climb in the future," Warne said.

While the market is setting records, many stock watchers believe equities remain fairly valued, though not cheap.

The S&P 500 is trading around 16 times its forward-operating earnings, or over the next 12 months. The historical average on that measure is about 15 times.

"That says stocks are no longer cheap, but we also don't think they're expensive," Warne said. "Historically, when the price-earnings ratio has been in that range, returns over the next year have been around 7 percent. That's not bad."

Among the stocks making big moves Tuesday: