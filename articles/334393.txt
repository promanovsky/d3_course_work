California governor Jerry Brown has officially signed into law a bill approving bitcoin and other digital currencies as valid forms of payment in the state.

The California Senate Banking and Financial Institutions Committee earlier passed the Assembly Bill 129 by a 7-1 vote.

The bill that was authored by Assembly member Roger Dickinson is aimed at recognising digital currencies along with a number of other alternatives to the US dollar, such as coupons. The bill is not aimed at regulating digital currencies.

However, the mighty dollar will still have legal superiority over other forms of money.

Thank you Gov Brown & CA legislature! "AB 129 allows Bitcoin and other digital currency to be legally used in transactions in California." June 29, 2014

The bill repeals an outdated restriction on the use of anything but the US dollar as currency. The restriction indicates that anyone using alternative currency is in violation of the law.

"In an era of evolving payment methods, from Amazon Coins to Starbucks Stars, it is impractical to ignore the growing use of cash alternatives," Dickinson said.

"This bill is intended to fine-tune current law to address Californians' payment habits in the mobile and digital fields."

The state senate passed the bill on 19 June by a 7-1 vote, and the Assembly floor passed it on 23 June by 52-11.

California, which is home to the country's tech hub Silicon Valley, has been a hotbed of bitcoin activity. According to a CoinDesk analysis, 40% of bitcoin professionals hail from California.

Based on its research titled 'New Private Monies: A Bit-Part Player?' the Institute of Economic Affairs said governments should welcome competition between state-backed and private currencies in order to "offer consumers greater choice and improved quality".

Bitcoin was launched in 2008 and is traded within a global network of computers. Virtual currencies are not backed by any country or central bank and can be transferred without going through banks or clearing houses, and without the associated fees.