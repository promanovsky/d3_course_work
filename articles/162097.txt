PEOPLE Magazine has named Academy Award winner Lupita Nyong'o the 2014 'Most Beautiful Woman.' The 12 Years a Slave star, who recently signed a modeling deal with Lancome Paris, says she "never dreamed" she would receive the title.

"It was exciting and just a major, major compliment," said the 31-year-old, who will grace this year's annual edition. "I was happy for all the girls who would see me on [it] and feel a little more seen."

Born in Mexico and raised in Kenya, Nyong'o says the greatest compliment she can receive is "when I have been called beautiful with not one drop of makeup on." She adds. "And also before I comb my hair or put on a pretty dress. Happiness is the most important thing."

Nyong'o made her acting debut with the short film East River and subsequently starred in the Kenyan television series Shuga (2009). Also in 2009, she wrote, produced and directed the documentary film In My Genes.

Nyong'o later completed a master's degree in acting from the Yale School of Drama, followed by her first feature film role in Steve McQueen's historical drama 12 Years a Slave (2013). Her role in the film was widely acclaimed, earning her the Academy Award for Best Supporting Actress, among numerous other awards and nominations.

Photo courtesy of PEOPLE