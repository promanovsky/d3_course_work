Google on Monday declined to comment on reports that it is working on a billion-dollar deal to add video game play streaming star Twitch TV to YouTube

US media reports of Google aiming to buy Twitch come with a boom in online viewing of real-time video game or computer game play as "e-sports" attract growing crowds as spectator events.

Asked about reports of the talks in the Wall Street Journal and Variety, Google said it did not comment on rumors.

(Also see: Google Looking to Buy Twitch Game Streaming Service: Report)



PlayStation console maker Sony made an alliance with Twitch in 2012 to let gamers broadcast online play live for others to watch, and made the feature one of the selling points of its successful new PlayStation 4 model.

Microsoft also put Twitch capabilities into its new-generation Xbox One console.

(Also see: Xbox One finally gets Twitch live broadcasting app alongside Titanfall)



Twitch bills itself as the world's largest video game broadcasting network.

Millions of people monthly watch video game play streamed using Twitch, which boasts partners such as the Electronic Sports League, Major League Gaming and IGN Pro League.

San Francisco-based Twitch was created by the founders of live video streaming platform Justin.tv Justin Kan and Emmett Shear.

TwitchTV launched in 2011 and has made a partnerships to get money-making video and display ads to weave in during breaks in live game play.

Twitch "broadcasters" range from professional gaming leagues to individual players.