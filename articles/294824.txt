Argentine President Cristina Fernandez said her country won't honor its debt to various hedge funds - even after the Supreme Court's refusal to hear the case Monday.

BREAKING: Argentine leader says she won't honor $1.3 billion debt ruling despite US Supreme Court loss - The Associated Press (@AP) June 17, 2014

At issue is whether Argentina is compelled to pay the full face value of bonds to bondholders who refused to participate in a debt restructuring.

Advertisement

While Fernandez said Argentina remains open to negotiations, she also admitted the country simply can't pay off the disputed bonds in full, calling the situation "extortion," according to the Associated Press "What I cannot do as president is submit the country to such extortion," she said in a national address Monday night.

Until the uncertainty clears up, the situation remains chaotic, as Business Insider's Linette Lopez noted Monday afternoon.