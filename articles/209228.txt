Come May 13th, Motorola is expected to announce a new smartphone. It is unclear as to which device (or devices) will be revealed – could it be the rumored Moto X+1 or the Moto E? Well neither device has been confirmed, but thanks to a recent sighting on Indian import and export database, Zauba, the Moto E name has been spotted.

Advertising

The database listing also reveals its model number(s): the XT1022 and the XT1025. The former will be the “normal” model while the latter is the dual SIM model with support for digital TV, but other than that we expect that both devices should share similar hardware, at least for the most part.

The listing also reveals some of the phone’s specs, such as its 4GB of onboard storage and that it is expected to retail for around $130-$140, making it a pretty affordable handset, much like its predecessor, the Moto G. However 4GB is less than what is offered on the Moto G, but with rumors suggesting support for microSD, we’re guessing this might be a non-issue.

Other rumored specs of the handset includes a 4.3-inch 720p HD display, a 1.2GHz dual-core processor, 1GB of RAM, a 5MP rear-facing camera, a 1,900mAh, and will run on Android 4.4 KitKat. In any case hopefully Motorola will share additional details come 13th of May, so check back with us then for the details.

Filed in . Read more about Moto E (2015) and Motorola.