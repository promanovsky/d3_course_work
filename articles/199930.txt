Attempts to improve sanitation among the poorest have long focused on building latrines, but the United Nations says that money literally went down the toilet. Reuters

One billion people worldwide still practise "open defecation" and they need to be told that this leads to the spread of fatal diseases, U.N. experts said on Thursday at the launch of a study on drinking water and sanitation.

"'Excreta', 'faeces', 'poo', I could even say 'shit' maybe, this is the root cause of so many diseases," said Bruce Gordon, acting coordinator for sanitation and health at the World Health Organization.

Societies that practice open defecation - putting them at risk from cholera, diarrhoea, dysentry, hepatitis A and typhoid - tend to have large income disparities and the world's highest numbers of deaths of children under 5 years old.

Attempts to improve sanitation among the poorest have long focused on building latrines, but the United Nations says that money literally went down the toilet. Attitudes, not infrastructure, need to change, it said.

"In all honesty the results have been abysmal," said Rolf Luyendijk, a statistician at the U.N.'s children's fund UNICEF.

"There are so many latrines that have been abandoned, or were not used, or got used as storage sheds. We may think it's a good idea but if people are not convinced that it's a good idea to use a latrine, they have an extra room."

Many countries have made great progress in tackling open defecation, with Vietnam and Bangladesh - where more than one in three people relieved themselves in the open in 1990 - virtually stamping out the practice entirely by 2012.

The global number has fallen from 1.3 billion in 1990. But one billion people - 90 percent of them living in rural areas - "continue to defecate in gutters, behind bushes or in open water bodies, with no dignity or privacy", the U.N. study said.

The practice is still increasing in 26 countries in sub-Saharan Africa. Nigeria was the worst offender, with 39 million open defecators in 2012 compared to 23 million in 1990.

INDIA NO.1

Although the prevalence of open defecation is in decline, it is often common in fast-growing populations, so the total number of people doing it is not falling so fast, or is even rising.

The country with the largest number of public defecators is India, which has 600 million. India's relatively "hands off" approach has long been at odds with the more successful strategy of neighbouring Bangladesh, which has put a big focus on fighting water-borne diseases since the 1970s, Luyendijk said.

"The Indian government did provide tremendous amounts, billions of dollars, for sanitation for the poorest," he said.

"But this was disbursed from the central level to the provinces and then all the provinces had their own mechanisms of implementing. And as their own data showed, those billions of dollars did not reach the poorest," added Luyendijk.

India's government has now woken up to the need to change attitudes, he said, with a "Take the poo to the loo" campaign that aims to make open defecation unacceptable, helped by a catchy Youtube video. http://www.youtube.com/watchv=_peUxE_BKcU

"What is shocking in India is this picture of someone practising open defecation and in the other hand having a mobile phone," said Maria Neira, director of Public Health at the WHO.

Making the practice unacceptable has worked in more than 80 countries, the U.N. says. The goal is to eliminate the practice entirely by 2025. Poverty is no excuse, the study said, noting the role of cultural differences.

In the Democratic Republic of Congo, 14 percent of the population are open defecators. But where the head of the household is an Animist, the figure is twice as high, at 30 percent. Among households headed by Jehovah's Witnesses, it is only 9 percent.