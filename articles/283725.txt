Seeing weird code tweets? TweetDeck hack spams Twitter [Updated]

An exploit in popular Twitter client TweetDeck has seen the app taken offline and many users’ timelines spammed with automatically retweeting code, though more dangerous exploits are also feared. The hack takes advantage of a flaw in XSS code used in several version of TweetDeck, popping up unexpected messages on users’ computers, as well as triggering a retweeting storm as vulnerable machines propagated the tweet.

Twitter’s advice earlier was to log out of TweetDeck and then back in, which the company promised would address the issue. However, that was quickly followed by users reporting no change in the app’s behavior.

Now, Twitter has taken TweetDeck services offline, so as to “assess today’s earlier security issue.”

The fear is that, while the current exploits are annoying but not especially dangerous, the same method of triggering Javascript code could be used for more nefarious purposes. Users could be automatically followed or unfollowed, it’s suggested, or indeed have links posted to malware or otherwise compromised sites that might be followed by trusting followers.

One implementation of the vulnerability changed the font to the much-reviled Comic Sans.

Although begun as an independent project, TweetDeck was acquired by Twitter back in 2011 as the short-messaging network tried to take better control of the apps used to access it. The app had proved popular – and remains a favorite – with power-users, thanks to its potentially complex column system, keyword tracking, alias support, and other advanced features.

Those power-users are now being left to struggle with the regular Twitter web interface, something that although redesigned recently, still lacks the more advanced features of TweetDeck.

Update: Twitter has turned TweetDeck services back on.

SOURCE TweetDeck