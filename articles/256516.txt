Time to embrace internet, Iran leader says

Share this article: Share Tweet Share Share Share Email Share

Dubai - Iran should embrace the Internet rather than see it as a threat, President Hassan Rouhani has said, in remarks that challenge hardliners who have stepped up measures to censor the Web. Rouhani, a comparative moderate elected last year, said trying to win the battle for public influence by restricting the Internet was like bringing a wooden sword to a gunfight. The speech distances Rouhani from rival conservative clerics, some close to Supreme Leader Ayatollah Ali Khamenei, who promote censorship as a tool for protecting the 1979 Islamic revolution which brought the Shi'a Muslim clergy to power. It was also his most forceful signal yet of a break with the social media policy of predecessor Mahmoud Ahmadinejad, who rounded up bloggers and tightened online controls in an eight-year term, especially after protesters used social media to organise mass street demonstrations in 2009. “We ought to see (the Internet) as an opportunity. We must recognise our citizens' right to connect to the World Wide Web,” said Rouhani according to the official IRNA news agency.

“Why are we so shaky? Why have we cowered in a corner, grabbing onto a shield and a wooden sword, lest we take a bullet in this culture war?” he said in his weekend speech.

“Even if there is an onslaught, which there is, the way to face it is via modern means, not passive and cowardly methods.”

Iran has long had a contradictory attitude towards the Internet. Access to sites like Twitter, Facebook and Youtube is blocked for most Iranians, but Khamenei himself joined Twitter and Facebook in 2009 and is now a prolific user of both.

These days the Supreme Leader often sends out more than a dozen tweets a day in English, Farsi and Arabic. His latest informed his 53 900 followers that “Despite industrial progress in the #West, negligence & humiliation of #family & its values will cause West to collapse in the long run.”

On his Facebook page, where he has 82 000 “likes”, Khamenei offers spiritual guidance, telling those seeking a spouse to accept compromise: “a perfect flawless wife or a perfect flawless husband cannot be found anywhere in the world.”

Yet Abdolsamad Khoramabadi, secretary of a state committee tasked with monitoring and filtering sites, last year called Facebook a US espionage project.

Iran's leadership cracked down hard against Internet users in 2009 following Ahmadinejad's disputed 2009 re-election that year, when a violent crackdown on street protests led to the worst unrest in the Islamic Republic's history.

Many bloggers were jailed and at least one person was sentenced to death for running a website seen by the authorities as subversive.

Iran's Internet users face slow, patchy connections as well as heavy filtering. Still, they can evade controls by using virtual private networks which provide encrypted links that allow a computer to behave as if it is based in another country, giving them access to blocked sites.

In his speech, Rouhani compared the effort to restrict access to the Internet to an earlier, failed attempt to combat the spread of satellite television.

“First, our entire obsession was video - how to keep it out of our youth's access and protect our faith and identity. Then satellite dishes shot up on roofs,” Rouhani said. “Today, the Internet and smart phones have become the foremost woe.”

Rouhani said Iran could not develop without embracing the digital world and criticised the idea that students should just take notes from books rather than go online.

“Are our PhD students still expected to use library archives like in the old days to take notes for research?”

Internet censorship has eased somewhat under Rouhani's new government, Iranians say, but he lacks the power to open it up completely.

At the apex of Iran's power structure, caution abounds. Decisions on key strategic matters fall under the authority of Khamenei, who set up an internet oversight agency, the Supreme Council of Virtual Space, two years ago.

In a decree, he said the agency would shield Iran from harm from “the increasing spread of information and communication technologies, particularly that of the global Internet network and its important role in personal and social life.”

Iranian media have said the agency includes the president, the information and culture ministers, and the heads of the police and the conservative Islamic Revolutionary Guards Corps (IRGC), a powerful military organisation close to Khamenei.

Iran has also been the target of cyberwar attacks, including a computer virus that corrupted software in nuclear centrifuges and caused them to self-destruct. Security experts believe the United States and Israel were behind it. - Reuters