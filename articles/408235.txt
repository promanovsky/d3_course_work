When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

In a lengthy statement, the School's Out rocker, who co-wrote hits like Only Women Bleed and I Never Cry with Wagner, calls his late friend a "one of a kind" talent.

Cooper writes, "Even though we know it's inevitable, we never expect to suddenly lose close friends and collaborators.

"Dick Wagner and I shared as many laughs as we did hit records. He was one of a kind. He is irreplaceable. His brand of playing and writing is not seen anymore, and there are very few people that I enjoyed working with as much as I enjoyed working with Dick Wagner.

"A lot of my radio success in my solo career had to do with my relationship with Dick Wagner. Not just on stage, but in the studio and writing. Some of my biggest singles were ballads what I wrote with Dick Wagner. Most of (album) Welcome to My Nightmare was written with Dick. There was just a magic in the way we wrote together. He was always able to find exactly the right chord to match perfectly with what I was doing.

"I think that we always think our friends will be around as long as we are, so to hear of Dick's passing comes as a sudden shock and an enormous loss for me, Rock N Roll and to his family."

Wagner, 71, passed away from respiratory failure in a Phoenix, Arizona hospital.