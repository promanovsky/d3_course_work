Seth Rogen and James Franco’s brand of stoner comedy isn’t for everyone, but it rarely goes beyond immature jokes, underdog characters and a sideways look at America. Now, it would seem, they’ve annoyed one of the world’s superpowers.

Hey, it's Seth Rogen

North Korea, according to the AP, is warning that the release of ‘The Interview’ – in which the U.S government charge Rogen and Franco with assassinating Kim Jong Un – would be an “act of war”.

“If the U.S. government doesn't block the movie's release, it will face "stern" and "merciless" retaliation, an unidentified spokesman for North Korea's Foreign Ministry said in state media Wednesday,” read the AP’s report on the situation.

More: North Korea's Stance on Seth Rogen & James Franco's The Interview: Unhappy

The spokesperson added that the "reckless U.S. provocative insanity" in aiding a "gangster filmmaker" to challenge North Korea is "a gust of hatred and rage" for the North Korean people its soldiers in what the AP described as “typically heated propaganda language.”

This isn’t the first time comment has arrived on ‘The Interview’ from North Korea, although their stance on Rogen’s new movie has certainly stiffened. “There is a special irony in this storyline as it shows the desperation of the US government and American society,” a spokesman for Mr Kim told The Telegraph earlier this month.

More: Kim Jong-un Will Watch 'The Interview'. With A Bowl Of Ice Cream

“A film about the assassination of a foreign leader mirrors what the US has done in Afghanistan, Iraq, Syria and Ukraine,” he added. “And let us not forget who killed [President John F.] Kennedy – Americans. In fact, President [Barack] Obama should be careful in case the US military wants to kill him as well,” Kim Myong-chol said.

Now, the film's release would be considered an "act of war that we will never tolerate,” is the message from N. Korea.

The Interview is set for release on October 10 in the U.S, and October 29 in the U.K.