Mila Kunis has used a chat show to launch a comic routine where she berated expectant fathers.

The actress, who is expecting a baby with Ashton Kutcher, delivered the pre-prepared rant on US talk show host Jimmy Kimmel's show.

When Jimmy told the star: "My wife and I are pregnant. We are having a baby very soon as well," Mila, 30, replied: "Oh, you both are having a baby? You and your wife are pregnant?"

Getting up to address the audience, she said: "Hi, I'm Mila Kunis, with a very special message for all you soon-to-be fathers. Stop saying, 'We're pregnant.' You're not pregnant!

"Do you have to squeeze a watermelon-sized person out of your lady-hole? No. Are you crying alone in your car listening to a stupid Bette Midler song? No.

"When you wake up and throw up, is it because you're nurturing a human life? No. It's because you had too many shots of tequila. Do you know how many shots of tequila we had? None. Because we can't have shots of tequila," she added.

She told viewers: "We can't have anything because we've got your little love goblin growing inside of us. All you did was roll over and fall asleep."

As other pregnant women appeared next to her eating tubs of ice-cream, she added: "You're not pregnant, we are."