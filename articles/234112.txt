With its arsenal of health benefits, Minnesota has come out on top in a national ranking of the country's healthiest states for those 65 and older for the second year in a row.

Rounding out the top five healthiest states for older adults were Hawaii, New Hampshire, Vermont and Massachusetts, in that order. At the other end of the spectrum was cash-strapped Mississippi, which was the unhealthiest state at number 50. Also making up the bottom five were Louisiana, Kentucky, Oklahoma and Arkansas.

The not-for-profit United Health Foundation released its 2014 "America's Health Rankings Senior Report" -- a special second edition of the annual "America's Health Rankings" -- on Wednesday. The report's aim is to shine a light on the well-being of America’s fastest-growing demographic group, those 65 and older.

"Minnesota is great at a lot of things including dental visits. They've made dentistry for seniors a priority," said Dr. Reed Tuckson, external senior health advisor to the United Health Foundation, in an interview with Huff/Post 50. "They also have a high percentage of volunteerism. There is a culture there of seniors engaging with other people."

In addition, Minnesota and the other healthiest states boast a greater availability of home healthcare workers, who help people age in place, as well as a greater proportion of high quality nursing homes. The top states also enjoy strong networks of community organizations designed to promote healthy living among seniors.

In compiling the rankings, researchers considered 34 measures of senior health, including dental care, physical activity, availability of home healthcare workers, food security, prescription drug coverage, volunteerism, quality of nursing homes and flu shots. (Weather wasn't a factor.) The data was mined from more than a dozen government agencies and private research organizations.

Tuckson emphasized that even the healthiest states face challenges as they deal with a population that's living longer -- but that's increasingly hampered by obesity, diabetes, arthritis, and other chronic conditions.

"There is still an incredible burden of illness that has the potential to frustrate the ability of seniors to live a full and meaning life. This also has economic repercussions," he said. "We're expecting Medicare expenditures to increase quite significantly... by about 80 percent over the next decade. We need to focus on not only living longer, but living better."

But there was some good news in the report as well. Not only are more seniors exercising but there also is a rising number of seniors engaged in end-of-life planning.

When people don't communicate their wishes, they often wind up dying in hospitals. "But this year we're seeing an increase in the number of those ending their lives in hospice care, which is often more caring," he said.

Meanwhile, the report -- which can be found in its entirety here -- highlighted a few more sobering statistics, including the following:

--- Today, more than 1 in 8 Americans are aged 65 or older. By the year 2050, this age group is projected to more than double in size, from 40.3 million to 88.5 million.

--- More than 25 percent of seniors are obese.

--- Nearly 36 percent of seniors are living with four or more chronic illnesses. About two-thirds are living with two or more chronic illnesses.

"We have to turn the spigot off when it comes to people with chronic diseases," Tuckson said. "Every state has successes to build on and challenges they'll need to address.

"If we don't all attend to this, the quality of life for seniors will be diminished, and that won't be good for anyone," he said.

In another ranking of states released late last year by the United Health Foundation, American Public Health Association and the Partnership for Prevention, Hawaii came out on top as the healthiest state in general, for all ages, followed by Vermont, Minnesota, Massachusetts and New Hampshire.

To find out more about the top five healthiest states for those 65 and older, see below.

1. Minnesota.



In the past year, the percentage of quality nursing home beds increased, from 55.2 percent of beds rated four or five stars to 59.8 percent. It also has the lowest prevalence of seniors with cognitive difficulties in the nation.

2. Hawaii.



Hawaii has the lowest prevalence of depression among seniors in the nation. It also has the lowest prevalence of obesity among seniors in the country; 29,000 seniors are obese in the state.

3. New Hampshire.



Smoking prevalence among seniors is low in New Hampshire; fewer than 15,000 adults aged 65 and older smoke. It also has the highest percentage of quality nursing home beds.

4. Vermont.



In the past year, food insecurity decreased from 11.6 percent of adults aged 60 and older to 10.8 percent. It also has a low prevalence of smoking.



5. Massachusetts.



In the past year, the percentage of seniors using hospice care increased by 39 percent and the percentage of hospital deaths among seniors decreased by 16 percent. It also has the second-lowest suicide rate at 7.6 deaths per 100,000 adults aged 65 and older.