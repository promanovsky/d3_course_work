DOHA, Qatar -- The aviation industry is marking 100 years since the first scheduled commercial flight took off, making a 23-minute journey across Florida's Tampa Bay.

A roundtrip ticket on the Jan. 1, 1914, flight from St. Petersburg, Florida, to Tampa, Florida, was priced at $10. Abe Pheil, then mayor of St. Petersburg, bid $400 to be the first passenger on the two-seat plane as part of an effort to raise money for two harbour channel lights for the city, which are still in use.

The International Air Transport Association is marking the 100th anniversary of that flight at its annual conference in Doha, the capital of Qatar, showing how far the industry has evolved despite questions over safety and profitability.

Association director Tony Tyler said Monday that the airline industry will carry 3.3 billion passengers and 52 million tons of cargo over 50,000 routes this year. The group represents 240 airlines carrying 84 per cent of all passengers and cargo worldwide.

St. Petersburg native Robert Walker, who is director of the Florida Aviation Historical Society, was invited by the IATA to attend the conference and showcase a model of the aircraft that made the historic trip a century ago.

Walker passed out copies of the original flier marketing the flight, which described the "Airboat Line" as a "fast passenger and express service."

"They made a scheduled service twice a day each way and that's the secret to the airline: its schedule," Walker said.