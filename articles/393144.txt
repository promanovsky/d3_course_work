Zillow will also become king of real estate listings available on smartphones and tablets — the fastest-growing area for listings. Both Zillow and Trulia Inc. were founded nearly a decade ago and have capitalized on Americans’ increasing preference for researching purchases, including homes, online rather than relying solely on a real estate agent.

The real estate website operator Zillow Inc. is buying its rival in a $3.5 billion deal that will create the biggest player in the online real estate information market.

NEW YORK — Zillow and Trulia, two companies that changed the way people shop for homes, are combining.

‘‘It’s a very sound business move by Zillow. They wiped out their closest competitor,’’ said a Benchmark analyst, Daniel Kurnos.

Advertisement

According to Benchmark estimates, Zillow and Trulia are number one and two in the online real estate market, followed by Move Inc.

Zillow reported nearly 83 million monthly unique visitors in June. Trulia reported 54 million.

‘‘We’re moving away from word-of-mouth, or calling an agent to try to find a home,’’ Kurnos said. ‘‘Now people realize, ‘Hey, I can go look for houses online.’ ’’

Zillow, which debuted in late 2004, became well known for its ‘‘Zestimate’’ housing price estimate for 100 million homes nationwide. The number is based on geographic data, user-submitted information, and public records. Zillow says the Zestimate has a 6.9 percent median error rate and should be used as a starting point in determining a home’s value.

Both Zillow, which went public in 2011, and Trulia, which had its stock market debut in 2012, offer similar information such as neighborhood school details, crime reports, and mortgage calculators.

Both Seattle-based Zillow and San Francisco-based Trulia generate revenue through advertising and subscription software and services sold to real estate agents.

Trulia shareholders will receive 0.444 shares of Zillow common stock for each share they hold and will own approximately 33 percent of the combined company. Zillow Inc. shareholders will receive one comparable share of the combined company and own the other two-thirds of the business.

Advertisement

The combined company will keep both the Trulia and Zillow brands.

The companies said there is limited consumer overlap of their brands, as about half of Trulia.com’s monthly visitors don’t visit Zillow.com.

It’s not Zillow’s first expansion through acquisition. The company bought the New York City-focused website StreetEasy in 2013 for $50 million.

Zillow plans to save $100 million in cost-cutting once the Trulia purchase is complete.

Trulia’s chief executive, Pete Flint, will stay in his post and join the board of the combined business. He will report to Zillow CEO Spencer Rascoff. Another Trulia director will join the board after the transaction is finalized.

Both companies’ boards approved the deal, but shareholders still must vote on it. Shares of Zillow rose $1.46, or 0.92 percent to $160.32. Shares of Trulia jumped $8.69, or 15.4 percent, to $65.04.