Imangi Studios says that its Temple Run franchise has been downloaded more than 1 billion times on mobile devices. Since it hit the market in August, 2011, the game has had a steady presence in the market with multiple versions that keep gamers coming back.

Built by just three people, the game has defied the odds, standing out among 1.2 million active apps in the App Store for years at a time when most games last for nanoseconds. But it’s an intriguing question about whether the adventure game will become an enduring global brand, or suffer an eventual decline like predecessor mobile brands like Angry Birds.

Temple Run was a pioneering 3D game that popularized the “endless runner,” or a game where a character is constantly running toward the horizon. You swipe the touchscreen to make them turn left or right. You swipe down to make them slide, or swipe up to make them jump. All the while they’re being chased by monkeys or other creatures. These simple mechanics have proven to be incredibly addictive.

Over time, that has translated into 50 trillion total meters run, 32 billion deaths and 140 billion “save me” options used, 147 trillion gold coins collected, 1 billion artifacts discovered, and 216,018 total years played.

“It snuck up on us a bit,” said Keith Shepherd, cofounder of Imangi Studios, in an interview with GamesBeat. “It’s kind of crazy….It’s changed so many things for us. It’s opened up cool opportunities. Over the past three years, we’ve spent a lot of effort on Temple Run. We created a sequel. We worked with Disney twice, doing Temple Run Brave and Temple Run Oz. We started this worldwide licensing and merchandise program. We have arcade machines and apparel and plush toys and board games. It’s been an amazing ride.”

The huge number of downloads translate into a lot of money for a small company. But Shepherd and his wife, Natalia Luckyanova, don’t want to turn their company in to a giant empire with hundreds of employees. Raleigh, N.C.-based Imangi has remained a small startup, with just 11 employees.

“We decided we wanted to keep working on Temple Run and keep our fans engaged and keep doing more with that intellectual property and continue to build Temple Run into a franchise that’ll be around for the long term,” Shepherd said. “At the same time, we wanted to bring some of that new game development back into our lives…. We opened up a studio and started hiring people about a year ago. We moved into our new office in January. These days we’re 11 people, still a really tiny team. But we’ve gotten to the point where we can continue doing these regular updates to Temple Run and building on that franchise. At the same time, we’ve gotten back to our roots and started prototyping a lot of new game ideas.”

Temple Run, built by Shepherd and Luckyanova with freelance artist Kiril Tchangov, was Imangi’s tenth mobile game. It was made in an apartment over five months, and the company still wants to stay true to those indie roots.

“We’re still grounded in the experience of wanting to be really creative and innovative and prototyping and messing around with what will ultimately become our next great product as well. It’s a cool time here at Imangi,” Shepherd said.

Image Credit: Imangi Studios

Imangi has been entirely bootstrapped. It has no investors.

“We always identified, and still do identify, with the indie spirit and the indie movement,” Shepherd said. “We want to maintain that. We could go so many directions with the success we’ve had. We could have tried to grow the team into 150 people and do all sorts of things and eventually try to sell the company for a zillion dollars. But that’s never been our goal. We’re just a group of people who are passionate about making games. Whether we’re starving or we’ve gotten rich, we still want to do the same thing. It’s a lot of fun making games. We love to be a part of that. That’s what the indie spirit is about.”

The idea for the first Temple Run came after the company created a title called Max Adventure.

“As we started playing around with it, we zeroed in on using the swipe up to jump the character, swipe down for a slide, and swipe left or right to turn the character 90 degrees, as he’s always walking around,” Shepherd said. “We built out this prototype inside of that last game, Max Adventure, as an alternate way to control the character, and while it didn’t really work with that specific game, it gave us some ideas. It felt like a neat way to control a character, an innovative control scheme.”

He added, “Next we thought about how to build a game around that control scheme. If your character’s always walking, and he can only turn left or right and maybe jump, what kind of environment would be fun or challenging to run in? With a lot of right and left turns? A maze was the first thing we hit on. We set out to build this environment of an endless maze. The more and more we went down that path, the more and more it looked like a temple wall or something like that. The environment and theme and a lot of that stuff clicked into place as we were going along. It really started with that mechanic, though, the swipes to move the character around.”

Image Credit: Imangi Studios

Each game is getting more in tune with the models that work. With Temple Run 2, the company did about a dozen updates, releasing new content every six to eight weeks. The games have ads as well as in-app purchases. The original Temple Run debuted as a 99 cent app, but the company eventually shifted to a free-to-play version. That’s when it hit viral critical mass, Shepherd said.

The whole team works on Temple Run titles, but everyone also works on creating new game prototypes. The team has internal game jams, but Shepherd wants everyone to feel part of both the franchise and the new creativity.

Meanwhile, the endless runner genre has blossomed in lots of titles from many different competitors. But Shepherd doesn’t seem bothered by that or the chance that the brand might one day run out of steam. In 2013, Temple Run 2 was the No. 3 top-downloaded free app, and Temple Run was still in the top 100. The Oz and Brave titles were also in good positions on the paid charts.

“Games always have their peaks and valleys. It’s seasonal,” he said. “You generally spike at launch and slowly taper off. With a game like Temple Run, I think one of the things that allows us to hit something like a billion worldwide downloads is the incredible longevity of it. Temple Run has established itself as one of these go-to games that everyone seems to have played. Everybody that gets a new phone, it’s one of the first games they get. It adds a lot of stability in terms of its chart position.”

With the Disney deals, Imangi pursued the licensed Oz and Brave games because they were a good fit with the Temple Run theme. But Imangi isn’t trying to overexpose its brand.

He said, “Mobile games, especially games that can be operated as a service in the long term, you can have a really engaged player base for a long time.”

And right now, the fans are still asking for more Temple Run content.

“We have a lot of cool ideas that we’re thinking about for the future related to the story of Temple Run and expanding the franchise,” Shepherd said. “That’s really exciting. But at the same time, when it comes to company-building, I’m not someone who thinks, ‘Yeah, we need to grow this into a 500-person team and buy Rovio or buy EA or have one of them buy us.’ As long as I can continue to work on what I love and make great games, that’s more important to me. I’d like to build a small team that can be around for the long term. We can be successful. We can continue to focus on making innovative games. That’s what I love doing and what I’d like to do. But I have no ambitions to be a 100-person or 500-person company. That’s just not what I’m interested in.”

Image Credit: Imangi Studios