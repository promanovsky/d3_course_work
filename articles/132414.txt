Ireland's most unpopular banker was yesterday found not guilty of involvement in financial irregularities in the management of the institution he headed, Anglo-Irish Bank.

Sean FitzPatrick, the former chairman of the now defunct bank, walked free from Dublin's Circuit Criminal Court after a jury acquitted him on charges of giving illegal loans to customers to prop up its share price.

The jury has yet to deliver verdicts on two of the bank's former directors following a 10-week trial which has attracted intense media and public interest. All three had denied giving illegal loans to customers in 2008 to buy bank shares.

Anglo-Irish was one of Dublin's most aggressive banks during the years of the Celtic Tiger, when the Irish economy experienced its biggest boom, followed by its greatest crash. After its collapse Mr FitzPatrick was declared bankrupt.

The much-reviled bank has been widely blamed for contributing to the colossal banking collapse which brought the country to the brink of bankruptcy. It was wound up at huge cost, while other banks also went under.

Ireland has only recently begun to show the beginnings of a recovery following its borrowing of billions from international financial institutions. The lack of jobs in recent years has led to emigration on an enormous scale, particularly of young people.

Outside the court Mr FitzPatrick, 65, cut his usual dapper and debonair figure as, smiling broadly, he read a prepared statement thanking family and friends for their support "during six years of great personal difficulty".

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

He said he hoped that "the privacy of my family, which has been intruded on over the past six years, will now cease". He did not give evidence during the trial.

The three defendants were charged with perhaps the biggest white-collar crime ever committed in Ireland, involving millions of euros. The trial was told that €650m (£535m) had been lent by the bank to Sean Quinn, then one of Ireland's richest businessmen, members of his family and others. The purpose of the loans, it was said, had been to buy Anglo shares and thus give an impression of stability at a time when the Quinn empire was financially exposed.

The prosecution said this was illegal in that it had been done in extraordinary circumstances, so was in breach of the Companies Act. A prosecution lawyer alleged that almost everything about the arrangements pointed to "a scheme obviously and spectacularly not in the ordinary course of business".

A defence lawyer insisted the case was not about getting vengeance for the collapse of Irish banks. He said a spectator had reportedly said, after watching proceedings, "Give them a fair trial and then hang them." He warned the jury: "It is vital that you divorce yourselves from that kind of thinking – that you are here to satisfy the baying for blood of the mob." Sean FitzPatrick had been singled out and picked on by police who he claimed, had taken up "the cudgels of the man on the barstool".