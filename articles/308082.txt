BNP Paribas nears up to $9bn settlement

Share this article: Share Tweet Share Share Share Email Share

New York/Paris - French bank BNP Paribas SA is likely to pay $8 billion to $9 billion as part of a potential settlement with US authorities over violations of sanctions, according to a person familiar with the matter. US authorities are probing whether BNP Paribas evaded US sanctions relating primarily to Sudan between 2002 and 2009, and whether it stripped out identifying information from wire transfers so they could pass through the US financial system without raising red flags, sources have said. BNP Paribas has been negotiating on an almost daily basis with US authorities for weeks. The investigation has turned up more than $100 billion in books and records violations in transactions involving Sudan, Iran and Cuba, one source said on Sunday. The potential settlement could include BNP Paribas pleading guilty to a criminal charge of violating the International Emergency Economic Powers Act, another source familiar with the matter has said.

The potential settlement was first reported by the Wall Street Journal.

French Finance Minister Michel Sapin on French radio did not comment on a possible deal but repeated the French position that any penalty should be fair and proportionate.

“The French state, the government ... has played its role in telling the Americans: 'Be careful - by all means punish the past but don't punish the future',” Sapin told France Info.

Earlier in the month, Reuters reported that US authorities negotiating with BNP Paribas at one point suggested that France's biggest bank pay a penalty as high as $16 billion, although that was viewed as a negotiating tactic in response to an offer from BNP Paribas of about $1 billion.

The probes are being conducted by authorities including the US Justice Department, the US Attorney's office in Manhattan, the US Treasury Department, the Manhattan District Attorney's office, and the New York Department of Financial Services.

The New York Department of Financial Services, which oversees certain banks in New York, has said it will not revoke the bank's license to operate in New York if BNP Paribas agrees to other stiff penalties, a source has said.

The state regulator also has sought the termination of more than a dozen employees as part of the settlement, at least some of whom have already left.

A BNP Paribas spokeswoman declined to comment. - Reuters