With temperature likely to rise in the coming years because of climate change, essential food crops such as wheat are likely to witness a drastic fall in production, according to a report by the Inter-Governmental Panel on Climate Change (IPCC).

The report, released on Monday, said the yield on wheat crop in India would drop drastically by 2020.

In the Indo-Gangetic plain, which includes India's major wheat-producing regions of Uttar Pradesh, Punjab and Haryana, wheat yields might come down by as much as 10 per cent in the most favourable and high-yielding areas due to heat stress, the report noted.

"About 200 million people (using the current population numbers) in this area (Indo-Gangetic plain) whose food intake relies on crop harvests would experience adverse impacts," said the report.

It added that sorghum yields might also drop by 2-14 per cent by that year due to reduction in monsoon on account of climate change. Besides, there will be a 3-5 per cent impact on maize and around 10 per cent drop in soybean yields due to climate change, the report noted.

The report said that emissions of carbon dioxide are often accompanied by ozone (O3) precursors, which have driven a rise in tropospheric O3 that harms crop yields. The report said climate change can impact the amount and quality of livestock produce, profitability and reliability of production of the dairy, meat and wool systems.