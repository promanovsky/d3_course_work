Last night's season finale of The Bachelor was both surreal and shockingly real: For the first time in the show's 12-year history, the show's final episode (and subsequent "After the Final Rose" special) anointed the protagonist Bachelor a bad-boy antihero.

Before he gave registered nurse Nikki his final rose (but not an engagement ring), Venezuelan-born retired soccer player Juan Pablo endured no small amount of controversy. From calling gay people "more pervert" [sic] to slut-shaming a contestant who romped in the ocean with him, Juan Pablo had certainly been playing by his own rules. And ABC, for the most part, let him dig his own grave.

As a series of smart, sensible women decided to leave The Bachelor of their own accord, says Linda Holmes at NPR, "the show actually allowed their eligible bachelor, who represents the prize for which all these contestants are competing, to be portrayed as (1) not too bright, (2) kind of self-centered and boring, and (3) coarse and vulgar."

For a show so set on portraying a perfect vision of romantic love, the fact that ABC decided to turn on its Bachelor is pretty groundbreaking. "Generally," says Holmes, "the near-universal desirability of the Bachelor/ette is sacrosanct." Holmes continues:

The true story is that somewhere, somebody who makes The Bachelor decided that they needed a different framing for the season — one in which their bachelor was kind of...a jerk. They made a choice to cut him loose. To throw him, in reality show terms, under the bus." [NPR]

Read the rest of this fascinating piece at NPR. Samantha Rollins