Based on a new study, dogs have a highly accurate sense for detecting prostate cancer.

It is still a test that needs to be refined further but research suggests that it is also a new, accurate and quick way to diagnose the disease. Prostate cancer kills 29,000 men in the United States each year. The report is an addition to an increasing list of studies that show dogs can smell cancer byproducts of different types.

With a combined accuracy of 98 percent, two trained canines were able to sense organic chemicals from prostate cancers to the urine. The dog snout has 220 million olfactory cells while humans only have 50 million. This is why canines have always helped on search and rescue operations. Research suggests that dogs may be used in clinics. There is also an application in the works for the U.S. to approve a kit that finds breast cancer through breath samples.

"We have demonstrated that the use of dogs might represent in the future a real clinical opportunity if used together with common diagnostic tools," study author and head of urology pathology Gianluigi Taverna, said at the American Urological Association in Boston. Trained dogs appear to detect volatile organic compounds (VOCs) in the urine from prostate tumors. "Our standardized method is reproducible, low cost and non-invasive for the patients."

A system is now waiting for the U.S. Food and Drug Administration clearance that would allow the use of the dogs' unique olfactory talents in medical care. Dog trainer Dina Zaphiris is in charge of this application. She explains that patients would exhale through a tube to a cloth to capture VOCs or molecules of a malignancy. The dogs would sniff the cloths in their presence. The canine screening would be considered as an early warning test and cold possibly be used with a mammogram to review results before a patient is asked to go through a biopsy.

Zaphiris said there are a lot of patients saying they got an unclear mammogram and are unsure if they want to proceed with a biopsy to have canines screen their breath samples.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.