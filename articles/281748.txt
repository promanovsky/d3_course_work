By Letitia Stein

TAMPA, Fla., June 12 (Reuters) - A painful, mosquito-borne viral illness has surfaced across the United States, carried by recent travelers to the Caribbean where the virus is raging.

Health officials in North Carolina, Nebraska and Indiana this week reported the first confirmed chikungunya cases in those states, along with Tennessee, which has suspected cases.

Chikungunya has rapidly spread in the Caribbean in recent months, sending thousands of patients to hospitals with painful joints, pounding headaches and spiking fevers.

Florida's 25 cases account for the majority reported in the United States, according to state health officials and the U.S. Centers for Disease Control and Prevention.

The cases in the continental United States have not been transmitted by local mosquitoes, which would raise the threat.

"It will be more difficult for the virus to establish itself here," said Dr. William Schaffner, an infectious disease specialist at Vanderbilt University Medical Center in Nashville, Tennessee.

Along with new reports, the CDC is monitoring chikungunya in Arkansas, California, Connecticut, Maryland, Minnesota, Nevada, New York, Virginia, Puerto Rico and the U.S. Virgin Islands.

Symptoms surface within three to seven days after a bite from an infected mosquito and typically dissipate within a week. There is no vaccine, and the virus is not deadly. Medications can help treat high fevers.

The Caribbean Public Health Agency said this week the number of confirmed and suspected cases had risen to 135,651, up from just over 100,000 on June 2. The virus has been detected in 20 countries and territories, with the largest outbreak of suspected cases in the Dominican Republic.

Health officials in the Dominican Republic said they detected more than 77,000 suspected cases since the virus reached the country five and half months ago, including 20,000 new suspected cases in the last week alone, according to the Public Health Ministry.

World Health Organization representatives in Haiti said chikungunya will continue to spread as mosquitoes breed in standing water and in the open containers used in many Haitian homes that lack running water.