FILE - In this Aug. 16, 2012, file photo, mosquitos are sorted at the Dallas County mosquito lab in Dallas. Infectious diseases that used to be unknown in Oklahoma are now a threat to the health of its residents as maladies such as the West Nile virus, chikungunya virus and Heartland virus spread around the globe. (AP Photo/LM Otero, File)

By Daniel Kelley

July 30 (Reuters) - Cases of chikungunya virus, a painful, mosquito-borne disease that has spread rapidly through the Caribbean in recent months, spiked higher in New York and New Jersey in the past week, according to new federal data.

The number of cases in New Jersey more than doubled to 25, while New York has recorded 44 cases, the highest number outside Florida, where the disease first established a toehold in the United States, according to data released late Tuesday by the Centers for Disease Control and Prevention.

Officials in New Jersey and New York do not believe any of the cases originated in their state.

Symptoms, which develop three to seven days after being bitten by an infected mosquito, include high fever, headache, muscle pain, back pain and rash. In rare cases it is fatal. Small children and the elderly are more likely to develop severe cases, according to the Centers for Disease Control.

The CDC said the United States averaged 28 cases of chikungunya each year since 2006 but until recently all have been travel related. Three people in Florida contracted the disease from local mosquitoes this month in what the CDC said are the first cases of the disease to originate in the United States.

The CDC said it has seen 601 cases this year in the 37 states and two territories. Puerto Rico has reported 215 cases, 199 contracted locally, the CDC said.

New Jersey regularly tests mosquitoes for another disease, West Nile Virus and this year, health officials have begun testing traps for chikungunya.