New York: American teens are smoking less, drinking less and fighting less. But they’re texting behind the wheel and spending a lot of time on video games and computers, according to the government’s latest study of worrisome behavior.

Generally speaking, the news is good. Most forms of drug use, weapons use and risky sex have been going down since the government started doing the survey every two years in 1991. Teens are wearing bicycle helmets and seat belts more, too.

“Overall, young people have more healthy behaviors than they did 20 years ago,” said Dr. Stephanie Zaza, who oversees the study at the Centers for Disease Control and Prevention.

The results come from a study of 13,000 U.S. high school students last spring. Participation was voluntary and required parental permission, but responses were anonymous.

Highlights of the study, released Thursday:

Smoking

Fewer than 16 per cent of the teens smoked a cigarette in the previous month - the lowest level since the government started doing the survey, when the rate was more than 27 per cent. Another CDC study had already put the teen smoking rate below 16 per cent, but experts tend to treat this survey’s result as the official number. It’s “terrific news for America’s health,” said Matt Myers, president of the Campaign for Tobacco-Free Kids. Even so, there are still about 2.7 million teens smoking, he said.

The survey did not ask about electronic cigarettes, which have exploded in popularity in the past few years.

Meanwhile, more than 23 per cent of teens said they used marijuana in the previous month - up from 15 per cent in 1991. CDC officials said they could not tell whether marijuana or e-cigarettes have replaced traditional cigarettes among teens.

Texting

Among teen drivers, 41 per cent had texted or emailed behind the wheel in the previous month. That figure can’t be compared to the 2011 survey, though, because the CDC changed the question this time. The latest survey gives texting-while-driving figures for 37 states - ranging from 32 per cent in Massachusetts to 61 per cent in South Dakota.

Drinking

Fewer teens said they drank alcohol. Drinking of soda was down, too. About 35 per cent said they had had booze in the previous month, down from 39 per cent in 2011. About 27 per cent said they drank soda each day. That was only a slight change from 2011 but a sizable drop from 34 per cent in 2007.

Sex

The proportion of teens who had sex in the previous three months held steady at about 34 per cent from 2011. Among them, condom use was unchanged at about 60 per cent.

Suicide

The percentage who attempted suicide in the previous year held steady at about 8 per cent.

Media use

TV viewing for three or more hours a day has stalled at around 32 per cent since 2011. But in one of the largest jumps seen in the survey, there was a surge in the proportion of kids who spent three or more hours on an average school day on other kinds of recreational screen time, such as playing video or computer games or using a computer or smartphone for something other than schoolwork. That number rose to 41 per cent, from 31 per cent in 2011.

Health experts advise that teens get no more than two hours of recreational screen time a day, and that includes all screens - including Xboxes, smartphones and televisions.

Although video-gaming is up, particularly among teen boys, some researchers believe most of the screen-time increase is due to social media use. And it’s probably not a good thing, they say.

Through texts and social media, young people are doing more communicating and living in an online world in which it’s easier to think they’re the center of the universe, said Marina Krcmar, a Wake Forest University professor who studies teen screen time. That can lead to a form of extended adolescence, she said.

It can also distract youngsters from schoolwork, exercise and other healthy activities, she said.

Fighting

Fights at school fell by half in the past 20 years. And there was a dramatic drop in kids reporting they had been in a fight anywhere in the preceding year - about 25 per cent, down from 33 per cent two years earlier. The addition of more guards and other security measures may be a factor, said school violence expert Todd DeMitchell of the University of New Hampshire.

Fighting may be down, but it’s not uncommon, according to some teens at the High School for Fashion Industries in lower Manhattan, where most of the students are girls. Two students said they saw roughly one fight a week.

“It’s like ‘The Hunger Games,’” said 14-year-old Maya Scott. She said she had been in a fight during the current school year.

A few minutes later, as if to prove her point, three girls exchanged words and nearly came to blows outside the front entrance before a school lunch worker stepped in and separated them.