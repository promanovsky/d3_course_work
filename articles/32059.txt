Box Office: 'Noah' Off to Strong Start in South Korea, Earning $1.1 Million Thursday

UPDATED: Darren Aronofsky's big-budget biblical epic -- inspired by the story of Noah's Ark -- stars Russell Crowe.

Darren Aronofsky's Noah has landed in its first international market to strong results. The biblical epic debuted Thursday to $1.1 million from 750 screens in South Korea -- the top opening-day gross of 2014.

PHOTOS: 'Noah's' Berlin Premiere: Emma Watson, Jennifer Connelly, Douglas Booth Flood the Red Carpet

The turnout in South Korea -- a haven for tentpoles -- bodes well for the big-budget film, which hopes to play both as a mainstream event movie and as an offering for faith-based audiences (South Korea also has a large Christian population). Inspired by the story of Noah's Ark, the Paramount and New Regency film stars Russell Crowe in the title role.

Thursday's gross in South Korea was on par with Gravity's opening day in South Korea last year, and 15 percent ahead of Inception, which earned $950,000 on its opening day. And it came in with three times as much as Snow White and the Huntsman.

Noah opens in Mexico -- a predominantly Catholic country -- on Friday, a week before it debuts in the U.S. and other key foreign markets.

The biblical epic has received a great deal of attention because of Aronofsky's unique take on the well-known religious story, including portraying Noah as a man concerned with environmental issues as he prepares his ark for the flood.

Working with church leaders, Paramount altered its marketing campaign in recent days to say that Aronofsky's movie is inspired by the story of Noah's Ark, versus being a literal adaptation.

And on Wednesday, Crowe, Aronofsky, producer Scott Franklin and Paramount vice chairman Rob Moore -- who were in Rome for a screening of the film -- briefly met with Pope Francis when attending the weekly papal general audience in St. Peter's Square.

The film has been banned in several Middle Eastern countries for contradicting Islamic law by portraying a prophet. Earlier in March, censorship boards in Qatar, Bahrain and the United Arab Emirates informed Paramount they will not allow the release of the film. Similar rulings are expected in Egypt, Jordan and Kuwait, according to Paramount insiders.

Noah also stars Anthony Hopkins, Emma Watson, Jennifer Connelly, Douglas Booth, Logan Lerman and Ray Winstone.