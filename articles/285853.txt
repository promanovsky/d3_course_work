People who consume diets with higher levels of protein are less likely to suffer from a stroke, the findings of a new study reveal.

In the study, researchers from Nanjing University School of Medicine found that it is more beneficial to gain maximum proteins from consuming fish. This not only reduces the risk of stroke but also provides other health benefits like a sharper mind and a fitter body.

"The amount of protein that led to the reduced risk was moderate-equal to 20 grams per day," said study author Xinfeng Liu, MD, PhD, of Nanjing University School of Medicine in Nanjing in a press statement. "Additional, larger studies are needed before definitive recommendations can be made, but the evidence is compelling."

For the study, researchers conducted a meta-analysis of seven studies that looked into the association between protein in the diet and the risk of stroke. A total of 254,489 participants were included in the study. These participants were followed for an average of 14 years. After comparing data from all the studies, researchers found that the participants who had the highest amount of protein in their diets were at a 20 percent lower risk of developing a stroke compared to the participants with the lowest amount of protein in their diets. This was after other influencing factors like smoking and cholesterol levels were taken into consideration. Researchers noted that there was a 26 percent decrease in stroke risk for every additional 20 gm of protein consumed in a day.

"If everyone's protein intake were at this level, that would translate to more than 1.4 million fewer deaths from stroke each year worldwide, plus a decreased level of disability from stroke," said Liu.

Though red meat is touted to be a good source of protein, researchers confirmed that the findings of this study do not suggest an increased consumption of red meat. In fact, two of the seven studies that were considered in this research were conducted in Japan where people eat very less meat and more fish. Therefore, Liu and his team recommend substituting red meat with fish. They also said that other proteins that help reduce blood pressure may also be effective in reducing stroke risks.

The study was funded by the National Natural Science Foundation of China and the Natural Science Foundation of Jinling Hospital in Nanjing, China. Findings were published online in Neurology, the medical journal of the American Academy of Neurology.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.