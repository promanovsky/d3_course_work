The concept of transparency in scientific exploration has taken on a whole new meaning at the California Institute of Technology in Pasadena.

That where researchers have developed a new way to create transparent rodents with their internal organs and skeletal structures intact.

Unfortunately for the thrill-seekers reading this report, the process for creating see-through lab mice and rats isn't as gruesome as it sounds; the critters are euthanized before their skin removed and internal organs -- but not their bones -- are given a texture similar to gelatin.

As well, the mice and rats are kept in a laboratory and used only for scientific study, regardless how popular they'd prove as conversational party pieces.

The results look like a rodent-shaped block of Jello with the organs held in place by connective tissue.

The see-through technique involves pumping a series of chemicals through the rodents' blood vessels, in addition to other passages in the brains and spinal cords.

Some of the applied chemicals form a mesh to hold tissues in place while others wash out the fats that make tissue block light. It takes about a week to create a transparent mouse, Viviana Gradinaru, the senior author of a research paper describing the work, told reporters, adding her team has also made transparent rats, which take about two weeks.

Mice hold a special place in biomedical research because much of their basic biology is similar to that of humans -- and they can be altered in ways that simulate human diseases.

Scientists have, to various degrees, been able to turn tissues transparent for about 100 years, with far improved techniques developed in recent years.

Last year, a breakthrough technique was developed in order to create see-through mouse brains.

However, the new work from Cal Tech, as the Institute is often called, is the first to make an entire mouse transparent.