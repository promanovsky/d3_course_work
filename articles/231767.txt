Online auctioneer eBay is urging users to change their passwords after being hit by a cyberattack.

The company says that hackers “compromised a database containing encrypted passwords and other non-financial data” using “employee log-in credentials”. The breaches were first detected two weeks ago.

In a statement, eBay reassured users that they were not able to detect any “unauthorized activity for eBay users” or “unauthorized access to financial or credit card information”. PayPal information was also unaffected.

"For the time being, we cannot comment on the specific number of accounts impacted. However, we believe there may be a large number of accounts involved and we are asking all eBay users to change their passwords," eBay spokeswoman Kari Ramirez told Reuters.

The company says that beginning on Wednesday it will be sending warnings out to all of its 112 million customers, urging them to change their passwords for the site as well as any other online logins using the same password.

“The same password should never be used across multiple sites or accounts,” said eBay Inc.

Compromised information included customers’ names, encrypted passwords, email addresses, physical addresses, phone numbers and individuals’ date of birth.

This won't be the first time this year that internet users have been asked to reset their passwords, with the Heartbleed bug, discovered in April, triggering widespread cybersecurity worries.

Source: Independent

Belfast Telegraph