An emotionally charged Tyrese opened up about how the cast of ‘Fast and Furious 7’ will pay tribute to fallen co-star Paul Walker.

It is with heavy hearts that Tyrese Gibson, and the cast of Fast & Furious 7 will return to the set March 31, in Atlanta, without their beloved Paul Walker. But the devoted and loving team plans to give tribute to Paul in the best way they know how — with prayer.

‘Fast & Furious’ Cast Will Say A Prayer Before Shooting

Paul’s tragic and ironic death in a sports car accident left the world devastated, but it also begged the question of whether or not Fast & Furious 7 would go on.

After taking a break in order to grieve, the film’s team is heading back to work in an effort to do what Paul would have wanted, and finish what they started. Tyrese, who plays (Roman) in the film, explains how they plan to honor Paul’s legacy on set.

“God’s grace is all we have,” Tyrese revealed to toofab.com. “Before we start filming we are going to have a strong prayer. That will be the start — before we film anything. That’s what is needed for us to do Fast & Furious 7.”

The cast will turn to God in this difficult time because Paul was more than just a co-star to them — he was family.

“This is the only movie I ever worked on where we are all real close — even after the movie is over,” Tyrese said. “It wasn’t just losing a co-star, he was family for real. That’s why it took so long to recover.”

Vin Diesel Promises ‘Fast & Furious 7’ To Be The Best One Yet

Like Tyrese, Vin Diesel has also opened up about the hardship of returning to set and facing the reality, yet again, that Paul is gone.

“The transition into that Dom state of mind has always been an interesting one,” he wrote on his Facebook page. “Only this time there is added purpose, a collective goal to make this the best one in the series.”

Though we do not yet know exactly how Paul’s character’s storyline will be resolved in the latest film, we have all the faith in the world that the entire Fast & Furious team will honor their fallen friend’s memory gracefully.

RIP Paul.

— India Irving



More Paul Walker News: