Meshach Taylor, the actor best known for playing Anthony Bouvier on "Designing Women," died on Saturday, June 28 after a battle with cancer. His agent confirmed the news to the Hollywood Reporter. He was 67.

Taylor died in his family's home in Altadena, Calif. and his family posted a note on his Facebook page, saying, "It is with love and gratitude that we sorrowfully announce that our darling, amazingly brilliant and dynamic Meshach, the incredible father, husband, son and friend, has begun his grand transition."

Taylor's Bouvier was one of the most lovable characters on "Designing Women," the '80s sitcom about Atlanta interior designers, and he was nominated for an Emmy for outstanding supporting actor in a comedy in 1989. He followed that role with a part on another CBS comedy, "Dave's World," about the life of a Miami Herald columnist, Dave Barry. Taylor played Barry's high school best friend and a local plastic surgeon.

He is also well-known for playing Hollywood Montrose, a window dresser, in 1987's "Mannequin" and its 1991 sequel. Taylor's most recent role was in two episodes of CBS' "Criminal Minds" this year. His small-screen credits include "Buffalo Bill," "Barney Miller," "Hill Street Blues," "The Golden Girls," "Cagney and Lacey" and "ALF," and he appeared in films such as "Damien: Omen II," "The Howling" and "Tranced."

He is survived by his wife, actress Bianca Ferguson, and his four children.

CORRECTION: A previous version of this article noted the wrong date of Taylor's death.

