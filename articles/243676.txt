“X-Men: Days of Future Past” is dominating movie ticket pre-sales as the hotly anticipated sequel barrels towards its Memorial Day weekend debut.

The comic book adventure unites several generations of mutants in a plot that bends time, space and scientific logic. It is expected to rule over the holiday box office, and is on pace to debut to $100 million or more over the four day period.

Online ticketer Fandango reports that “X-Men: Days of Future Past” is the strongest selling film in the superhero franchise’s decade and a half long history, trumping 2006’s “X-Men: The Last Stand.” It currently accounts for more than 90 percent of this weekend’s ticket sales.

Sales have also been robust at MovieTickets.com. The ticket service reports that the film accounts for 50 percent of all tickets sold domestically in the last 24 hours. “Godzilla,” which debuted last weekend and topped the box office with a $93 million premiere, is next up at 21 percent of tickets sold in the last day.

“X-Men” stars Hugh Jackman, Jennifer Lawrence and Michael Fassbender. Bryan Singer, who oversaw the first and second films, returned to direct the latest sequel.

20th Century Fox will begin screening the film Thursday evening at 10 p.m.