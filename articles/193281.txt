Galaxy S5 deluge: vs iPhone 6, Developer Edition, Mini leak

An unofficial collection of bursts in the world of the Galaxy S5 have appeared today, each of them with little fanfare from Samsung itself. One of these is the release of the Developer Edition of the Samsung Galaxy S5. This device will be hitting Verizon starting today, complete with the ability to get deep within its software build for heavy smartphone app development.

The second of three oddities to appear this morning is the resurgence of the Samsung Galaxy S5 mini. This device has appeared in listings for South Korea, for starters. This is generally the case with the “mini” version of Samsung’s flagship, with the device being tested on a small scale internationally, eventually being adopted then by USA-based carriers.

As luck would have it, we’ve not too long ago seen the Samsung Galaxy S4 mini in action. December, if you’ll remember. Therefore it’ll be a while before the small version of the Galaxy S5 hits our shores.

From the same Macitynet source as the “VS iPhone 5s” article we’ve posted this morning comes a set of iPhone 6 (4.7-inch display edition) mock-up VS Samsung Galaxy S5. You’ll see that the Galaxy S5 trumps the iPhone 6 for height, suggesting this version does, truly, work with a 4.7-inch display while the GS5 works with a 5.1-inch display.

These two devices will be doing battle by the end of the year. With the iPhone 6 leaks coming in full-swing, we can expect a full release right on schedule. For the past 2 years the iPhone has been released in September – there’s no undeniable reason (at the moment) to suggest this year will be any different.

VIA: GS5Abonnement