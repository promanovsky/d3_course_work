The first promotional trailer for the blockbuster adaptation of author E.L. James erotic novel Fifty Shades of Grey was unveiled at the 2014 CinemaCon convention in Las Vegas Tuesday.

The clip, which is not yet available online, reportedly featured more romantic scenes than BDSM (dominance/submission, and sadism/masochism) ones, and prominently focused on a dinner date between main characters Christian Grey and Anastasia Steele.

Advertisement

"A majority of the preview finds Grey wining and dining Anastasia with a helicopter ride, a fancy new dress and - surprise - a secret room he wants to show her," reads a description from the trailer by Hitfix. "As for what everyone wants to know about - the sex - at one point we see a ghost image of Anastasia with what can only be described as a horse hair soft whip being brushed across her face. That's it. Anyone who thinks they will get more in a trailer will be very disappointed."

Last year, it was reported that Universal Studios planned to release two separate version of the film to avoid an NC-17 rating that could turn away more mainstream moviegoers.

"We do not want this film to be seen as mommy porn -- we want to keep it elevated but also give the fans what they want," said producer Dana Brunetti at the time.

The trailer showed during CinemaCon was perhaps from the R-rated version of the film -- the one that will be released to cinemas. A more "gritty" version of the story could be released two weeks later with an NC-17 rating.

The Fifty Shades books tell the story of Steele, a college student who gets swept off her feet by the S&M-obsessed, billionaire entrepreneur Grey.

Fifty Shades of Grey hits theaters Feb. 14, 2015.

"Fifty Shades of Grey" is scheduled to be released on Valentine's Day 2015. (Universal)

[Hitfix]