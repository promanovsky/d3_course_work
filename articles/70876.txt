On the last day to sign up for Obamacare, evidence appears to be mounting that what started as a disaster may turn out a success.

Monday is the deadline to enroll in health insurance for 2014 via the health insurance exchanges created by President Barack Obama's Affordable Care Act, and it's clear that many waited until the last minute. The looming deadline and fear of the penalty for not getting covered has driven millions of people to the exchange websites, enrollment events and health insurance companies over the past few days.

HealthCare.gov and some state-run health insurance exchanges suffered software glitches and buckled under heavy demand Monday.

The final rush could push the total number of private insurance enrollments well past the 6 million figure touted by the Obama administration last week. Obamacare sign-ups may wind up closer to the 7 million originally predicted for the first year.

"It's kind of the lead-up to Christmas right now, and all the last-minute shoppers are out," said Brian Lobley, senior vice president for marketing and consumer business at Independence Blue Cross in Philadelphia.

The company, which serves the city and its surrounding suburbs, signed up 28,000 customers in less than three weeks in March. It signed up 90,000 in the five months prior.

Edward Patton, 43, became one of those last-minute sign-ups Saturday afternoon. Patton, who works at a gas station, stumbled onto an enrollment event at a ShopRite grocery store near his home in Philadelphia.

"I just came to pick up a couple things," said Patton, who has never had health coverage before. He also picked up a "silver" level health insurance plan, the second-lowest level of coverage available on the exchanges, for $77.49 a month, including tax credits, which was far less than he expected to pay, he said. "This is good, just in case, because I barely get sick," he said.



Paydon Miller of Enroll America, left, and enrollment counselor Francine Piesto at a ShopRite in Philadelphia Saturday.



Signing up the healthy and the young is critical to the health of the healthcare law. And based on anecdotal accounts from health insurance companies, the surge is also bringing along young adults in greater numbers.

"We're definitely seeing some younger consumers, as our average age of an applicant is going down," said Kurt Kossen, the vice president for retail marketing at Chicago-based Health Care Service Corp., which operates Blue Cross and Blue Shield companies in Illinois, Montana, New Mexico, Oklahoma and Texas. Online insurance broker eHealth reported a similar trend last week.



Insurance agents working for Independence Blue Cross assist customers signing up for health coverage at the company's Philadelphia headquarters Saturday.

In order to make premiums affordable in future years, insurers need healthier people to offset the high medical costs of older, sicker people who now have guaranteed access to coverage. Adults 18 to 34 years old made up one-quarter of nationwide enrollments through March 1, according to the Department of Health and Human Services, a proportion lower than the roughly 40 percent the White House is seeking.

Not everyone is so pleased with their new health plans. Households with incomes above four times the poverty limit, or about $95,400 for a family of four, don't qualify for financial assistance and can face hundreds of dollars in premiums every month for even basic coverage. And many people who previously purchased their own coverage directly from insurance companies saw their policies canceled last year because they didn't meet ACA standards, and had to replace them with plans that are often costlier because the mandated benefits are more generous.

The vulnerability of Obama's signature domestic policy achievement was evident during the early hours of Monday morning, when HealthCare.gov went down due to what the White House described as a software error not related to traffic on the website. The outage echoed the technical failures that marred the debut of the health insurance exchanges on Oct. 1 and hampered the first two months of the six-month enrollment period.

HealthCare.gov also drew record demand Monday when 1.2 million users visited the website by noon Eastern Time. First-time users of the website weren't able to create accounts for about an hour Monday afternoon and administrators twice activated the site's "virtual waiting room" when the number of people trying to log in surpassed its capacity, which is estimated at 100,000 users at once. State-run exchange websites in places like California and Maryland also experienced some difficulties. Telephone call centers for the exchanges were swamped by consumers seeking help, as well.

Although Monday is the nominal deadline for anyone who doesn't have health coverage to get insured this year, enrollments will continue through the coming weeks, since the Obama administration and most state-run health insurance exchanges are leaving the systems open for those who already started their applications but have not completed them by the end of the day.

Consumers in states using federally managed exchanges via HealthCare.gov can self-attest that they began the process prior to the deadline to qualify for a so-called special enrollment period. Across the remaining states, the rules vary. For example, in Connecticut, Monday remains the hard deadline, while Oregon residents have until the end of April.

Those who don't get health benefits from their jobs or through a government program won't be able to purchase private health insurance until the next open enrollment period begins Nov. 15 for policies that will take effect on Jan. 1, 2015.

Most people who fail to enroll in coverage for this year will face tax penalties under the ACA's individual mandate. There are a slew of exemptions from the mandate, however, and the health insurance exchanges will remain open for people who experience "qualifying life events," such as moving to a new state or losing their current health insurance plan. In addition, individuals eligible for Medicaid and the Children's Health Insurance Program can enroll year-round.

Despite the apparent turnaround since the disastrous October launch of the exchanges, the future of Obamacare is uncertain. Comprehensive data on how many people fully enrolled by paying their insurance premiums and how many of the previously uninsured gained coverage because of the law won't be available for some time, nor will information about the medical costs of those who purchased private insurance via the exchange or directly from an insurer.

The process of preparing for the 2015 enrollment period will start almost immediately after the inaugural sign-up period ends, as health insurance companies must begin making projections about how much their new customers will cost them, and therefore how much to raise rates for next year. The federal government and states like Oregon and Maryland, which faced major technical problems, also must decide how to improve their performance so that next year's enrollment period goes more smoothly than this year's.

This post has been updated with information on traffic to Healthcare.gov on Monday afternoon.