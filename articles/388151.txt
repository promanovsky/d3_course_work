When Microsoft first launched their Xbox One console it was available for $499, and it came with the new Microsoft Kinect, the console launched back in November of last year.

Microsoft recently launched a new version of the Xbox One, without the Microsoft Kinect, this saw the consoles starting price drop down to $399, and this has helped the company increase sales.

The company has now confirmed that since the price drop, their sales of the console have doubled in the U.S over the previous month, you can see a statement from Microsoft below.

Over the past month, we’ve seen a strong spike in interest in our Xbox One console options, including the new $399 offering, and the amazing lineup of games announced during E3. Since the new Xbox One offering launched on June 9th, we’ve seen sales of Xbox One more than double* in the US, compared to sales in May, and solid growth in Xbox 360 sales.

It will be interesting to see some actual sales figures on how many Xbox One consoles have been sold, compared to Sony’s PlayStation 4. Sony has been selling more consoles than Microsoft, and we wonder if this new cheaper Xbox One will help Microsoft catch up.

Source Microsoft

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more