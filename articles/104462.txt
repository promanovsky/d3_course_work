Americans who eat out at restaurants are twice as likely to get food poisoning compared to those who eat at home according to a new study.

The study, conducted by the Center for Science in the Public Interest (CSPI), analyzed 10,408 food poisoning outbreaks based on data from the Centers for Disease Control and Prevention (CDC). The data included cases from 2002 through 2011. Over the nine-year period, researchers found that more than 1,610 outbreaks, which sickened more than 28,000 people, occurred in restaurants. In the same time frame, 893 outbreaks, which sickened approximately 13,000 people, occurred at home.

The research also showed that reporting of foodborne illness outbreaks has decreased between 2002 and 2011, by 42 percent.

CSPI researchers believe food poisoning numbers may actually be higher, as many cases go underreported.

For comments and feedback contact: editorial@rttnews.com

Health News