Timothy Karr

AT&T wants to spend nearly $49 billion to buy DirecTV. If you throw in DirecTV's debt, this deal would cost AT&T a whopping $67 billion. Earlier this year, Comcast proposed a merger with Time Warner Cable that would cost the company a total of about $70 billion.

For the enormous amount of money AT&T and Comcast are shelling out for their respective mega mergers, they could deploy super-fast gigabit-fiber Internet services to every single home in America. But these companies don't care about providing better and faster services, or connecting more Americans to the Internet. These mergers are about eliminating the last shred of competition in a communications sector that's already dominated by too few players.

We have the technological capability to connect more people nationwide to fast and affordable services. And we can do it in a way that still returns large profits for Internet service providers.

These deals aren't about serving customers. They're about increasing shareholder value. That's what publicly listed companies do. The job of regulators at the Department of Justice and the Federal Communications Commission is to decide whether the public interest benefit of any merger outweighs the cost.

It's pretty clear what's in the public interest here: reliable, cheap and fast connections to the open Internet. We also need a choice of providers, not a few bloated companies controlling access.

Millions of people have pushed the FCC to adopt strong open Internet protections. Hundreds of thousands oppose the Comcast-Time Warner Cable merger. It's only a matter of time before many more protest AT&T-DirecTV.

Our agencies should try listening. This recent wave of mergers is a result of more than a decade of shortsighted FCC policies that have encouraged consolidation over competition. FCC Chairman Tom Wheeler — who has stated that his mantra is competition, competition, competition — has the power to block wasteful and anti-competitive deals such as AT&T-DirecTV. He should use it to reject these mergers.

Timothy Karr is senior director of strategy for Free Press, a public advocacy group focused on Internet access and media ownership issues.