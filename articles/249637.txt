Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Frostie the snow goat was down in the dumps after an infection left him without the use of his hind legs.

Just a few days old, the little fellow was found lice-infested and with a life-threatening condition contracted from his mother's umbilical cord.

Animal lovers at a rescue sanctuary in Australia feared the kid was not progressing quick enough despite antibiotics and painkillers and reckoned he was frustrated at not being able to move around.

They hit upon the idea of fitting him with a 'wheelchair' borrowed from his pig pal Leon Trotsky who used it to recover after his mother sat on his back legs.

The trick worked and now the two little animal pals run around together to the delight of workers at the Edgar's Mission Sanctuary just outside of Melbourne.

Founder Pam Ahearn, who looks after 350 animals including pigs, goats, cows, alpacas, deer, sheep, chicken, geese, duck, rabbits, guinea pigs, peacocks, and horses, said: 'It's lovely to see them together but Leon is far too fast for Frostie to keep up with him. He's so much spunk.'