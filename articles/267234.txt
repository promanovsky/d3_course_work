Continuous tracking of aircraft following the mysterious disappearance of MH370, carbon- neutral air travel and financial stress hitting the airline industry would be the focus of debate among global aviation chiefs at the IATA meet starting here tomorrow.

At the three-day International Air Transport Association's (IATA) 70th Annual General Meeting and World Air Transport Summit, heads of over 300 airlines, aircraft manufacturers and other related organisations, would also deliberate on issues like the "economic shock" hitting the industry, high taxation and burgeoning fuel costs.

On the Malaysian Airlines' flight, IATA Director General and CEO Tony Tyler said a working group of top global experts has been set up to examine the entire gamut of technological issues on aircraft tracking. It is expected come out with their recommendations by September this year.

"The loss of MH370 continues to be on everybody's mind. IATA is working with (UN body) International Civil Aviation Organisation to find a global solution to improve aircraft tracking," Tyler told reporters here.

The search for the Malaysian national carrier's Boeing 777, which went missing on March 8 with 239 people on board, in a huge tract of the southern Indian Ocean, has remained fruitless. The massive search area on the high seas stretches from near Indonesia to the south towards Antarctica.

The global airline industry, which celebrates its 100th anniversary this year, is on track to deliver profits of USD 18.7 billion, the highest in its history.

"But for an industry that generates USD 745 billion in revenue, that is a net profit margin of just 2.5 per cent," IATA's Economics division officials told PTI here quoting latest global figures.