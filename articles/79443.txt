Patients have their blood pressure taken at a large healthcare clinic set up by Remote Area Medical at the Forum in Inglewood, California. (File/UPI/Jim Ruymen) | License Photo

Being heart healthy as a young adult may increase the odds of staying mentally sharp in mid-life.

“It’s amazing that as a young adult, mildly elevated cardiovascular risks seem to matter for your brain health later in life,” study author Dr. Kristine Yaffe, a neuropsychiatrist, epidemiologist and professor at the University of California-San Francisco, said in a statement. “We’re not talking about old age issues, but lifelong issues.”

Advertisement

The study, part of the ongoing multi-center Coronary Artery Risk Development in Young Adults Study, tracked 3,381 U.S. adults ages 18- to 30 for blood pressure, blood sugar and cholesterol every two to five years over a 25-year study period.

The American Heart Association defines ideal cardiovascular health as systolic blood pressure less than 120 millimeter Hg, diastolic blood pressure less than 80 mm Hg, blood sugar less than 100 mg/dL and cholesterol less than 200 mg/dL.

At the end of the study, participants took three tests measuring memory, thinking speed and mental flexibility.

The study, published in the journal Circulation, found the middle-age adults with blood pressure, blood sugar and cholesterol slightly higher than the Heart Association’s recommended guidelines, scored lower on cognitive function tests in their 40s and 50s.

Elevated blood pressure, blood sugar and cholesterol are three major risk factors for atherosclerosis, the slow narrowing of arteries caused by a build-up of plaque in the artery walls leading to the brain and heart.

The narrowing of the arteries leading to and in the brain is the most likely explanation for the link between cardiovascular health and cognitive function, Yaffe said.

[University of California-San Francisco]

RELATED Air pollution kills 7 million prematurely worldwide annually

RELATED Teens eating high amounts of salt speed aging

RELATED Lack of sleep in obese teens predicts heart disease risk