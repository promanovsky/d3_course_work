By John Ostapkovich

HARRISBURG, Pa. (CBS) — Pennsylvania health officials says the groundwork is in place to deal with any cases of the Middle East Respiratory Syndrome (“MERS”) that show up here, although they haven’t seen any yet.

A conference call by the Pennsylvania Medical Society connected experts that included Pennsylvania physician general Dr. Carrie DeLone, who says that while MERS has a 30-percent mortality rate, not everyone with a cough has it:

“So the case definition for MERS would be an individual who has a fever, pneumonia, or acute respiratory distress, but as well they have to have a personal history of travel from the Arabian Peninsula or a country very close by, within 14 days.”

Or, she points out, close contact with someone who made that trip.

Should someone meet those criteria, medical personnel are to call the state health department to arrange for a confirming test.

Infectious disease specialist Dr. John Goldman of Harrisburg advises vigilance.

“The best thing the public can do right now is simply to be aware that this is a possibility,” he said. “You have to remember that there have been three cases reported in all of the United States, none of which are in Pennsylvania, so at this moment it is very unlikely that someone from Pennsylvania would come in contact with it.”

Still, Dr. DeLone wants the public and the medical community to incorporate the possibility of MERS into their thinking.