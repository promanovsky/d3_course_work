The Obama administration’s drive to regulate global-warming gases won a surprising victory in the Supreme Court on Monday with the support of two conservative justices.

Chief Justice John G. Roberts Jr. and Justice Antonin Scalia joined the court’s moderates and liberals in a 7-2 vote to uphold most of an Environmental Protection Agency rule that requires new or rebuilt factories and power plants to use the “best available technology” to limit their emissions of carbon dioxide and other greenhouse gases.

Because these “major polluters” are already required to obtain clean-air permits from the government, the EPA is justified in adding greenhouse gases to the list of restricted pollutants, Scalia wrote for the court.

It was the third time the high court has upheld the use of the 1970s-era Clean Air Act to combat challenges posed by 21st century climate change. Environmental advocates were elated, saying the decision showed that a strong majority of justices support the EPA’s authority to regulate greenhouse gases.

Advertisement

“The EPA’s foundational authority under the Clean Air Act to protect Americans’ health from the clear and present danger of climate pollution is rock solid,” said Vickie Patton, a lawyer for the Environmental Defense Fund.

Patton called the ruling a “big win” not just for the agency’s efforts to regulate new power plants, but also for the Obama administration’s sweeping climate change rules aimed at the nation’s existing power plants. The proposed rules, announced this month, call for an average 30% cut in greenhouse gases nationwide by 2030.

Monday’s ruling also served as a legal vindication of sorts for President Obama, who has sought to use his executive and regulatory powers to fight global warming because he says Congress has been unwilling or unable to act on the issue.

The Supreme Court opened the door for a national drive against global warming in 2007 when it ruled for the first time that greenhouse gases could be restricted as “air pollutants” under the Clean Air Act. Until then, the law had been applied to conventional pollutants such as ozone, lead or carbon monoxide. That 5-4 ruling arose when Democratic-led states — including Massachusetts, California and Illinois — sued to challenge what they called the Bush administration’s inaction.

Advertisement

Later, when Obama took office, the court expanded on that decision, ruling that the EPA could restrict greenhouse gases from power plants and refusing to block the agency’s carbon emissions standards for cars and trucks.

Even so, industry advocates hoped a conservative-leaning Supreme Court would veto the more expansive liberal regulations coming from Obama’s agencies, noting that Roberts and Scalia had disagreed with the 2007 ruling that labeled carbon dioxide as a dangerous air pollutant. But in Monday’s decision, both justices accepted that ruling as law.

Conservatives were left only with passages in Scalia’s opinion warning that the EPA does not have “free rein” to regulate millions of small businesses nationwide. Scalia devoted 25 pages of his 30-page opinion to explaining why the EPA may not extend its permitting regulations to require — in theory — that ordinary small businesses, such as hotels and office parks, show how they will reduce their carbon emissions.

“It would be patently unreasonable — not to say outrageous — for EPA to insist on seizing expansive power,” he wrote.

Advertisement

By a 5-4 vote, the court said this permitting authority is limited to major polluters, such as factories and power plants.

But Obama’s regulators said they never intended to target smaller businesses and had written the now-stricken provision of the permitting regulation in order to focus only on major polluters. But in doing so, the court said, the EPA exceeded its authority by altering the emissions limit set by Congress under the Clean Air Act. The majority concluded that this attempt to “tailor” the language of the law to prevent the rule from applying to smaller businesses was improper, even if well-intended.

At the same time, however, the majority agreed that nothing in the law prohibits the EPA from requiring greenhouse-gas permits from major polluters, which already must obtain them for conventional pollutants. The decision effectively gave the agency a different way to reach the same goal.

“It bears mentioning that EPA is getting almost everything it wanted in this case,” Scalia said in the courtroom. “It sought to regulate sources that it said were responsible for 86% of all greenhouse gases emitted from stationary sources. Under our holdings, EPA will be able to regulate sources responsible for 83% of those emissions.”

Advertisement

Only Justices Samuel A. Alito Jr. and Clarence Thomas disagreed.

Roberts and Justice Anthony M. Kennedy joined Scalia in full, while the court’s liberal bloc — Justices Stephen G. Breyer, Ruth Bader Ginsburg, Sonia Sotomayor and Elena Kagan — agreed with the part upholding EPA’s permits for major polluters.

Business leaders took comfort from Scalia’s criticisms about the EPA’s overregulation and said his comments could prove useful in the upcoming legal battle over the Obama administration’s climate change rules, which are expected to be finalized next year.

Thomas Donohue, president of the U.S. Chamber of Commerce, said the court “recognized that EPA’s attempt to sweep small business into its greenhouse gas agenda was an unconstitutional power grab. EPA is now on notice it does not have unlimited authority to impose massive costs on the U.S. economy.”

Advertisement

Industry lawyers said Monday’s decision offered a potential legal line of attack against Obama’s broader rules.

“There is plenty of ammunition” in Scalia’s opinion, said Timothy Bishop, a Chicago attorney. He predicted the new rules “will get very close scrutiny from a court that doesn’t think, as Scalia wrote, that EPA has ‘extravagant statutory power over the national economy.’”

The ruling, in Utility Air Regulatory Group vs. EPA, was the second major win for the EPA this spring. In April, the court upheld the cross-state air pollution law that requires states in the Midwest to do more to reduce emissions that drift east from their power plants.

The decisions show again that regulatory cases are notoriously hard to predict in the Supreme Court because the conservative justices are often split.

Advertisement

On the one hand, they are skeptical of far-reaching government regulations. On the other hand, Scalia, Roberts and Kennedy say judges should defer to an agency’s regulations so long as they fit reasonably within the words of the law.

Scalia said the Clean Air Act requires permits “for each pollutant subject to regulation” before building a “major emitting facility.” So once greenhouse gases are determined to be air pollutants, it is reasonable, he said, for the agency to require factories and power plants to reduce their carbon emissions along with the other pollutants.

