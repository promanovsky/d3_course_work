Contributors

Stephanie Callahan is a native Bostonian who loves cooking, traveling, spa treatments, and being on the ocean.

Meghan Colloton is a Bostonian who loves traveling, channeling her inner Julia Child, and trying weird things -- from food to bungee jumping.

Milva DiDomizio is a New England native who's fond of cooking, singing, and Boston's arts and culture scene.

Rachel Raczka is a Bostonian who enjoys buttercream frosting, gin cocktails, and conquering cobblestone streets in high heels.

Emily Sweeney is a Boston native who goes out all over, from Irish pubs in Southie to the roller rink in Dorchester.

Emily Wright is a native Cape Codder who enjoys exercising, baking, and the occasional guilty pleasure action movie.