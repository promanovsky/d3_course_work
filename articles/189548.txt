Scientists have cooked up a new time-lapse simulation of the universe's evolution, a computerized view that shows how the cosmos may have looked over the course of billions of years.

The cosmic simulation models the large-scale changes in the structure of the universe, tracing 13 billion years of evolution starting 12 million years after the Big Bang. The computer model, called Illustris, is special because it covers a wide area — a cosmic cube that is about 350 million light-years on each side — and also focuses on some details that are difficult to calculate while looking at such a wide swath of the universe.

You can watch a video of the simulation on Space.com. The new simulation shows a time-lapse view of the universe moving into its current form in brilliant colors. Galaxies move apart, and explosions take place in the cosmic web of galaxies simulated by the researchers. [See images from the Illustris universe evolution simulation]

Illustris shows small-scale galactic evolution, and tracks the formation of elliptical and spiral galaxies, said Mark Vogelsberger, a researcher at MIT and the lead author of a new study detailing the simulation.

"One of the main reasons why we did the simulation is because we learned a lot about the physics over the last years, and we think we have a very good understanding of the composition of the universe," Vogelsberger told Space.com. "We think there is dark matter. We think there is dark energy and we think that there are ordinary atoms. We also think we have a pretty good understanding of the initial conditions of the universe because we can measure those with satellites."

While the new simulation doesn't cover the entire universe, Illustris does model a wide-enough part of the universe to be representative of the entire cosmos, Vogelsberger said.

For the simulation, Vogelsberger and his team were able to model the behavior of dark matter and dark energy, the mysterious substances that make up about 95 percent of the universe. They also modeled the way that baryonic matter — matter composed of protons, electrons and neutrons — behaves. The model actually looks very similar to what scientists observe in the actual universe.

The research team used 8,192 computer cores (which have about the same computing power as a desktop) running simultaneously to create the new simulation, which shows the way the distribution of galaxies has changed over time. It would have taken 2,000 years using one ordinary desktop computer to create the new simulation, Vogelsberger said, but it took about six months to create the Illustris simulation on the stable of computer cores.

"If all of these ingredients and all of these things that we think about the universe are correct, then with one big model, it should reproduce our universe," Vogelsberger said. "No one has tried that on this scale before and with that level of detail. [It's] a test of all our theories of how the universe should evolve and how the galaxies grow, in one go, essentially because we tested with one big model.

"If the model would have gone very wrong and we did not produce what we see in the universe, then probably something about the composition of the universe would be wrong, or our understanding of how galaxies grow would be wrong, or something with the initial conditions would be wrong," he added. [How Computers Simulate the Universe (Infographic )]

As it stands, each particle of gas in the Illustris simulation actually represents 1 million solar masses. While this may sound like poor resolution, it actually represents an incredibly detailed view of the evolution of the cosmos, said astrophysicist Michael Boylan-Kolchin.

"Frequently, in simulations like this, each simulation particle represents something like 10 or 100 times that amount of gas," Boylan-Kolchin, who was unaffiliated with the research but wrote a commentary piece about it, told Space.com. "The goal is always to push this down to smaller numbers so that you're always getting closer and closer to something that represents reality in that sense.

"The same thing is true for the physics that you include in the simulation," he added. "You'd like to represent as many physical processes as you possibly can, so they have tried to include a wide variety of them."

The new simulation isn't perfect, however. Stars seem to form too early in low-mass galaxies, and the simulation cannot model rare, extremely massive galaxy clusters. Vogelsberger does not currently have the computing power to conduct that kind of simulation right now.

But the simulation's issues aren't necessarily failures. The problems could help scientists focus in on what new simulations should look for in the future. By focusing on those issues, researchers might be able to create new models that more closely resemble cosmic evolution.

"I think, in general, each of these simulations is really setting in place the goals for the next generation of these kinds of simulations," Boylan-Kolchin said.

The new study appears in the journal Nature: http://dx.doi.org/10.1038/nature13316

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Follow Miriam Kramer @mirikramer and Google+. Follow us @Spacedotcom, Facebook and Google+. Original article on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.