Microsoft is reportedly planning to dish out a preview version of Windows 9 in the autumn.

ZDNet brings word from sources close to the computing giant that a tester version of the software will be rolled out later this year, ahead of a full launch next spring.



The website's source also claims that Windows 9 will take a back-to-basics approach with greater support for keyboard and mouse users.

Touchscreen functionality is said to be purely optional, and the Mini-Start menu of old is expected to be restored.

Windows 9 is also rumoured to transform its user interface based on the hardware it is running on. For instance, the Metro UI will appear on touch-based devices, while a desktop mode will activate when a physical keyboard is present.

Microsoft is in the process of improving its Windows 8 operating system, with the latest 8.1 update reportedly due in August.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io