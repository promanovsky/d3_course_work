SAN JOSE, CA - JANUARY 18: Hookah pipes are seen at the Hookah Nites Cafe January 18, 2006 in San Jose, California. The ancient Middle-Eastern practice of smoking flavored tobaccos through a tall, ornate water pipe, better known as hookah, has become increasingly popular throughout the United States. Hookah lounges are becoming trendy in college towns and are appealing to people from all walks of life. (Photo by Justin Sullivan/Getty Images) Hookah pipes --The ancient Middle-Eastern practice of smoking flavored tobaccos through a tall, ornate water pipe, better known as hookah. (Photo by Justin Sullivan/Getty Images)

DETROIT (WWJ) – Nearly one in five American high school seniors smoked a hookah, or water pipe, in the past year. That according to a new study published Monday in the journal Pediatrics.

The study shows that smoking hookah is rapidly gaining popularity rapidly — especially among teens from families with above-average incomes — cigarette use among teens has declined in recent years.

Dr. Zyad Kafri, a hematologist-oncologist at St. John Providence Hospital, says many people believe hookah is a safer alternative to cigarettes — but it’s not.

“The fact is, studies from Mayo Clinic show that, if anything, you get more exposure than cigarette smoking by smoking hookah, and the water in the hookah actually does not necessarily filtrate all these poisons,” Kafri told WWJ’s Zahra Huber, “and in general you get more exposure to toxic chemicals than cigarette smoking.”

The authors of the report describe hookah as “an ancient form of smoking in which shisha (an herbal material that can be tobacco- or non-tobacco-based) smoke is passed through water before inhalation.”

The water pipes — which originated in ancient Persia and India and is popular in the Middle East — has found its way to metro Detroit. Hookah smoking can be a social activity, and with “hookah bars” popping up all over the area, the pipe is not hard to get a hold of.

Data from 5,440 students who took the annual “Monitoring the Future” survey show that 18 percent of 12th graders had tried smoking hookah in the 12 months before answering the questionnaire, researchers said. That figure is the average of survey responses for the years 2010, 2011 and 2012.

White males were more likely to use the hookah, according to study data, and students who identified themselves as black were less likely.

Kafri says education about the dangers of smoking hookah has to start at a young age. He urges parents to talk to their kids about it now.

“Giving them more information about the risks of smoking in general…I think just all these habits need to be replaced, probably, at any early age by playing specific sports or being engaged in healthy habits that keep our children more busy with other things,” Kafri said.

Kafri believes the variety of fruit flavors offered in hookah probably make it more appealing to young people.

[Learn more about the study HERE].