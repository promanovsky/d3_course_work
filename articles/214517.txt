Brett Molina

news

Shares of networking giant Cisco are surging. Let's take a look at the five technology stocks to watch Thursday:

Cisco. The company's stock jumped 7.5% in pre-market trading after its third quarter earnings beat estimates. The networking company reported an earnings per share of 42 cents off revenue of $11.5 billion. Overall, revenue was down 5% from the same time last year.

Apple.Re/code reports Apple may not wrap up its $3.2 deal to acquire Beats Electronics until next week. This follows a separate report that the company plans on announcing executive roles for Beats co-founders Dr. Dre and Jimmy Iovine at the Worldwide Developers Conference in June.

Google. The company will start releasing diversity data on its workforce, as people question whether the tech giant is doing enough to hire more minorities and women. Google says the diversity data will be released next month.

Zendesk. The customer service software maker launches its initial public offering Thursday. The company will trade on the New York Stock Exchange under the ticker symbol ZEN. The Wall Street Journal reports Zendesk set their IPO price at $9.

Comcast.Ars Technica reports the cable giant plans on issuing data caps for all customers in five years. Limits on broadband data usage are already in place in several Comcast markets, including Maine, Memphis, Tenn., and Charleston, S.C.

Follow Brett Molina on Twitter: @bam923.