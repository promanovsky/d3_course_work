Yet another mumps outbreak has hit a college campus. The Stevens Institute of Technology in Hoboken, N.J., has confirmed that eight students are ill after being infected with the mumps virus.

However, all eight of the students -- who are between ages 18 and 21 -- were fully vaccinated against the mumps, Stevens reported. All students are required to receive the measles, mumps and rubella (MMR) vaccine before attending the university.

The same was the case at Fordham University, which experienced its own mumps outbreak earlier this year, as well as Ohio State University, where there are more than 100 reported mumps cases linked directly to the university. (There are now 225 mumps cases in central Ohio, USA Today recently reported.) Just like at Stevens, most or all of the students infected with mumps at Ohio State and Fordham had been vaccinated before.

So if the vaccine is supposed to protect you from catching the mumps, why are vaccinated people still getting sick?

It's because the vaccines we have -- while highly effective -- do not confer 100 percent immunity, explains Dr. Roberto Posada, M.D., associate professor of pediatric infectious diseases at Mount Sinai Hospital.

"With measles, it's about 95 percent of people who are vaccinated [who] have immunity and lifelong protection," Posada tells HuffPost. "But with mumps, it’s a little less -- it’s probably 85 to 90 percent."

In addition, protection against the mumps sometimes does decrease slowly over time. So a child who's vaccinated at age 1 and 4 -- which are the typical ages of vaccination -- will be highly protected, but that protection will wane a bit with age.

However, it's important to keep in mind that vaccination is still important in protecting against diseases like the mumps. According to the Centers for Disease Control and Prevention, getting two doses of the MMR vaccine will make you about nine times less likely to actually get the mumps, compared with someone who hasn't been vaccinated at all, given the same exposure.

"If it wasn't for immunization the outbreaks would be much worse," Posada says. "Eight people are infected right now [at Stevens] and that's unfortunate, but if no one was immunized, it would be spreading to many more students."

Herd immunity also plays a role in protecting people from catching diseases like the measles and mumps. It works like this: Say 95 percent of people who get measles are immune to the disease, which means that 5 percent of people are not. But if enough people are protected from the measles, then that means there aren't enough people who can attain measles in a community who can then pass it on.

But when the number of people who can get measles starts to increase -- in other words, the number of people who aren't protected from the disease -- then at some point, you lose that herd immunity, Posada says.

That's why it's so important for people to receive their vaccinations, considering how transmissible the disease is, says Dr. Paul Graman, M.D., a professor of medicine in the Division of Infectious Diseases at the University of Rochester Medical Center. "Because it's not 100 percent effective, some people view that as an excuse or reason that it's not worth getting vaccinated," he tells HuffPost. But that logic doesn't hold up considering how many more people would be infected by mumps if exposed to an infected person, without the existence of vaccines.

So what can people do to protect themselves from catching the mumps? Aside from making sure you've received the MMR vaccination, Posada recommends avoiding sick people (since mumps is spread through saliva via sneezes and coughs, or through contaminated silverware or utensils), and frequently washing your hands with soap and water.