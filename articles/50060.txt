A Colorado third-grader whose freshly shaved head violated her school’s dress code will be granted a waiver from the policy because of extraordinary circumstances -- the fact that she cut off her hair to empathize with a cancer-stricken friend.

After a closed-door discussion, the board of directors for Caprock Academy in Grand Junction said they voted 3-1 Tuesday night to exempt Kamryn Renfro from the no-shaved-heads rule.

“Compassion and selfless acts of courage are to be commended and encouraged -- in children and in adults,” the board said in a statement after announcing the decision. “And we apologize that our policies and following our process for exceptions to those policies has, in any way, suggested that supporting anyone’s but particularly a child’s, brave fight against cancer is anything less than an extraordinary cause worthy of our highest regard.”

The case of 9-year-old Kamryn attracted nationwide attention this week after her mother, Jamie, announced on social media that the charter school told her to keep Kamryn away until her hair grew back.

Advertisement

“We do sign that we understand and agree to the rules every year ... but honestly, I never thought my 9-year-old daughter would do something so courageous, brave and selfless,” Renfro said in a public post on Facebook, referring to her acknowledgment of the rule.

Renfro said school officials showed compassion and never any ill-will toward Kamryn. But the Renfro family did request that the policy be reviewed. The board members called a special meeting Tuesday night. And they also welcomed Kamryn back to school Tuesday.

As per the school’s order, Kamryn didn’t attend classes Monday. She instead played with Delaney Clements, the 11-year-old friend who started her third round of chemotherapy last month. Delaney is fighting a fourth relapse of the childhood cancer known as neuroblastoma.

Renfro couldn’t be reached for comment immediately after the vote.



Advertisement

Caprock Headmaster Kristin Trezise told the Los Angeles Times early Tuesday that school officials “fully support” Kamryn. She declined to discuss the case in detail because of education confidentiality laws.

The school has said that dress code rules are intended “to promote safety, uniformity and a non-distracting environment” and that exceptions are made in “exigent and extraordinary circumstances.”

Follow LATimes National on Facebook

ALSO:

Advertisement

FBI clears agent in killing of Boston bombing suspect’s friend

Mudslide 911 calls: ‘There’s so many people yelling for help’

Michigan’s same-sex marriage ban to stay in place for now, court says