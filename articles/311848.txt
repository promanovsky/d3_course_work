Apple Inc. (NASDAQ:AAPL) has long been rumored to be working on two bigger iPhone 6 models for this year, and according to a new report, the company’s Chinese suppliers are expected to begin production of the new handsets in July.

In addition to the bigger displays, the upcoming iPhone 6 models are also expected to sport a rounder and thinner form factor compared to the current-generation versions of the device, Bloomberg reported late Monday, citing people familiar with Apple’s plans. The report also supported earlier speculation by other Apple watchers that the new iPhones would feature 4.7-inch and 5.5-inch screens to compete with Android-powered smartphones, a majority of which come with larger displays.

While the existing iPhone 5s and iPhone 5c have 4-inch screens, rival devices such as the Samsung Galaxy Note 3 and HTC One Max feature a 5.5-inch and 5.9-inch screen, respectively.

Some reports have also suggested that it is time for Apple to introduce some changes to the design of its flagship smartphone lineup as larger-screen devices have a growing appeal among consumers. According to a recent estimate by Forrester Research, 40 percent of Android-based mobile devices sold in China this year had a screen size of more than 5 inches.

Although production of the 5.5-inch iPhone 6 model is said to be more complicated than the smaller version, which could hurt efficient production, both versions are expected to ship to retailers around September, Bloomberg reported.

Meanwhile, Foxconn Technology Co Ltd (TPE:2354), one of Apple’s primary partners, was also reported to soon begin a massive hiring spree to staff the next-generation iPhone's assembly line. According to a report from Taiwan's Economic Daily News, Foxconn is expected to produce 70 percent of iPhone 6 units, while the remainder will be supplied by Pegatron Corporation (TPE:4938).

The iPhone 6 models, which could feature “enhanced sensors that can detect different levels of pressure,” are rumored to also come with a curved-glass display called “2.5-dimension glass” that would allow manufacturers to taper the edges of the handsets’ screen where the bezel meets the casing of the phone, according to Bloomberg.

“Large-screen envy is prevalent among the iPhone installed base and we believe a ~5″ form-factor iPhone would spark a massive upgrade cycle as well as many ‘Android switchers’ returning back to the iPhone,” Brian Marshall, an analyst at ISI Group, said in a statement recently, according to MarketWatch.