SHARE THIS ARTICLE Share Tweet Post Email

In 1986, Honda Motor Co. built a pair of robotic legs that could walk in a line. A decade later, it added an upper body. Yesterday in Tokyo, Honda’s latest robot, Asimo, met its first world leader: It chatted in English with U.S. President Barack Obama, then ran, jumped and kicked a soccer ball.

For its next trick, Honda believes, Asimo will give the automaker an edge in building the car of the future.

The kid-size robot, which can also acknowledge a raised hand and track three conversations at once, is the product of three decades of work in image processing, voice recognition and artificial intelligence -- essentially, the pursuit of judgment by a machine. Honda says it can apply much of that knowledge to driverless cars, which many automakers and industry analysts believe will be a fixture of the next decade.

“Cars until now have had only rudimentary recognition and judgment abilities. The strength of robots is they can work out really sophisticated reactions,” said Hiroshi Kawagishi, 51, a longtime Honda engineer. “If we can apply this kind of sophistication on cars, we could come up with something completely different.”

Honda, Volvo Cars, Nissan Motor Co. and others are jockeying against the likes of Google Inc. to roll out hands-free cars. Many are also working on technology that will let these vehicles talk to, and avoid, each other. Honda’s driverless car push expanded in 2011, when it paired auto engineers with Kawagishi and other robotics researchers.

That team’s success will help determine if Honda will see a return on three decades worth of work on robots that has cost, by conservative estimate, hundreds of millions of dollars.

Robot Benefits?

“What’s going to be the payback?” said Edwin Merner, president of Atlantis Investment Research Corp., which manages about $3 billion in assets and who doesn’t own Honda shares. “If you can’t show me that’s going to help the company in the future, within a few years, then you should stop.”

Honda, which hasn’t had an unprofitable year in at least five decades, declined to break out the cost of its robotics efforts. It is estimated to spend about 1 percent of its annual research and development budget on robots, said Koji Endo, senior analyst at Advanced Research Japan in Tokyo. That would come out to about $50 million annually, based on the company’s 2012 R&D spending.

Asimo is good for marketing and also generates technologies that can be used in vehicles, Tetsuo Iwamura, the Honda executive vice president, said in an interview. “Investors understand that these two things are meaningful.”

Crowded Field

While Honda is a prime mover in robotics and autonomous driving, it will have to focus as others work on similar technologies, said Sethu Vijayakumar, professor of robotics at the University of Edinburgh. “They will have to start looking into where they will take this technology,” he said.

As commuters across the world face concerns as diverse as urban gridlock and highway safety, the driverless car provides one possible answer. Such cars could shuttle themselves between customers, reducing vehicle ownership and parking snarls. Cars communicating in a common language would move more efficiently, reducing gridlock.

Global sales of such cars are expected to reach 11.8 million in 2035, said Egil Juliussen, an analyst at IHS Automotive, who projects that by 2050 almost all cars will be self-driving.

Slow Steps

Honda’s robotics program is a fixture at a company that has a tradition of keeping its engineers challenged by giving them stints away from auto projects. The company also has teams that design jet planes and racing cars.

The robotic legs that Honda engineers designed in 1986 took five to 20 seconds to take each step. An early humanoid version, P3, was an almost-adult-sized 63 inches.

“It looked a bit dictatorial,” said Satoshi Shigemi, who leads the humanoid robot development team. Asimo a foot shorter. “We wanted to make it look like a primary school kid helping his father pick up a newspaper in the morning.”

Honda has said it envisions its robots performing dangerous tasks or assisting the elderly or bedridden.

“I keep training every day so that sometime in the future I can help people in their homes,” an Asimo told the U.S. president yesterday. Obama faced the robot and bowed slightly.

Early Stumbles

There have been stumbles. A YouTube video of an Asimo demonstration from the middle of the last decade shows the robot climbing steps, then buckling and falling over. The latest Asimo can run on uneven surfaces and avoid spills by kicking a leg to counterbalance itself. Honda’s Iwamura said the company can apply that stability technology to its cars and motorcycles.

Made of magnesium alloy covered with white plastic resin, Asimo is fitted with eight microphones, 14 power sensors that read the direction and amount of force, sonic-wave sensors that detect obstacles as far as three meters (almost 10 feet) away, and two stereo cameras that can sweep 120 degrees.

That information is processed by software that lets the robot negotiate obstacles and interpret postures, gestures and faces. Honda researchers are fine-tuning Asimo’s ability to distinguish between a person walking past and one who wants to stop and chat, said Kawagishi.

That’s the sort of judgment capability that can be applied to cars: Asimo’s image-processing technology can recognize whether a pedestrian is leaning forward to cross a street. Artificial intelligence software can judge quickly enough to react, said Yoshiharu Yamamoto, the president of Honda R&D.

Speed Test

The challenge will be to adapt those capabilities to cars’ faster speeds, said Takashi Morimoto, consultant for Frost & Sullivan International, a consulting company.

“Yes, Honda’s Asimo technology is more advanced than others, but the challenge is how fast and accurate it can be,” said Morimoto. “It has to make judgment by processing five or six different factors around the car.”

In October, Honda allowed reporters to ride in the rear seat of an autonomous Accord fitted with a stereo camera, two radars and other sensors. Driving in a demonstration area, the car paused to let a pedestrian cross the road. It slowed so a motorcycle could pass. It stopped to make way for an unseen vehicle -- obscured by a mockup building -- as WiFi devices on both vehicles communicated with each other.

Car Talk

Honda is facing increasingly intense competition from companies and academics. Google, the Mountain View, California-based operator of the largest Web search engine, has bought at least seven companies for a robotics project to expand beyond online search, including Schaft Inc., a Tokyo-based maker of two-legged robots, and Boston Dynamics Inc. Google has been testing driverless cars mounted with cameras, radar sensors and lasers on U.S. roads.

Honda today forecast a full-year profit that missed analysts’ estimates, as benefits from a weaker yen fade and it increases incentives in the U.S. The shares rose 1 percent to 3,470 yen at the close in Tokyo trading, before the earnings announcement.

An edge for its car over rivals can help Honda move up the rankings among carmakers. In the year ended March 2013, Honda posted an operating profit margin of 5.52 percent, behind Toyota Motor Corp.’s 5.99 percent over the same period, according to data compiled by Bloomberg.

“I don’t think it will be profitable anytime soon,” Mitsushige Akino, chief fund manager at Ichiyoshi Asset Management Co. said, referring to the Asimo project. “But it is good for companies to try and invest in new technologies as long as the company itself is profitable.”