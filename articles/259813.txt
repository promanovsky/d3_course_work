Apple reportedly wants to put the iPhone and iPad at the center of the connected home. ― AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SAN FRANCISCO, May 27 ― The Financial Times claims that when Apple's World Wide Developers Conference gets underway on June 2, the big reveal won't be a smartwatch or a 12-inch tablet, but a new initiative that puts the iPhone at the heart of the connected home.

There are already a number of “smart” products, from lights to door locks and even security systems that can all be controlled remotely by launching a dedicated app.

However, according to sources that spoke to the FT, Apple is planning to simplify things. So, for example, as long as you're carrying your handset the door is open and the lights automatically come on. No need to dig out the phone and start swiping through screens.

The FT also claims that the Apple TV set-top box will play a crucial role in Apple's platform, acting as the menu and control panel for connected appliances. The belief is that the feature will become an Apple standard that companies can adopt and integrate into their existing smart appliances or connected systems.

This unification would also no doubt appeal to iPhone and iPad owners who would be able to buy smart gadgets in confidence, knowing that each of the different devices worked in concert with their phone or tablet.

The FT has a pretty solid track record when it comes to Apple-based rumours. Although the iPhone maker is yet to confirm the news, the publication was the first to break the story about Apple's pursuit of Beats Audio. ― AFP-Relaxnews