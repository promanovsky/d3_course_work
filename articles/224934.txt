Minnesota is banning the germ killer triclosan, which is found in many soaps and body washes. Governor Mark Dayton signed the bill on Friday, but the ban won’t take effect until January 2017.

State senator John Marty, who sponsored the bill, said that the impact of the bill — which is the first statewide ban in the nation — would be felt across the board. “While this is an effort to ban triclosan from one of the 50 states, I think it will have a greater impact than that,” he told the Associated Press.

According to the Food and Drug Administration (FDA), triclosan is not known to be harmful to humans, but a growing number of studies suggest that the chemical may cause hormone disruption. The FDA has said it is “engaged in an ongoing scientific and regulatory review,” but that it doesn’t have enough information to recommend that consumers stop using it.

Some consumer products have already begun dropping the chemical, which is in about 75% of all antibacterial soaps sold in America. Procter & Gamble already sells triclosan-free products, including toothpaste, and plans to completely eliminate its use by 2014.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.