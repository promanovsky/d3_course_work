The Wall Street Journal reported on Tuesday that the Obama administration has cleared the path for U.S. energy companies to export condensate, an ultralight form of crude oil, in what may be the most significant revision to the nearly 40-year ban on U.S. crude oil exports. Let's take a closer look at the new ruling and how it will impact U.S. oil and gas producers and refiners, along with other energy industry players.

A first step toward crude exports

Federal officials told two energy companies -- Irving, Texas-based exploration and production specialist Pioneer Natural Resources (NYSE:PXD) and Houston-based midstream operator Enterprise Products Partners (NYSE:EPD) -- that they can export condensate to foreign markets after minimally processing it in a refining unit known as a distillation tower.

The U.S. Commerce Department's Bureau of Industry and Security granted the permission through a process known as a private ruling. However, the rule applies only to condensates that are run through a distillation tower and not to unprocessed condensates that flow directly from oil and gas wells. Processing condensate through a distillation tower creates "a petroleum product (that) is no longer defined as crude oil," Commerce spokesman Jim Hock told Reuters.

Shipments of condensate could commence as early as August, the Journal reported, citing an industry executive with knowledge of the matter. Under rules imposed in the aftermath of the 1973 Arab oil embargo, U.S. companies are banned from exporting crude oil abroad, with a few limited exceptions, mainly for shipments to Canada. They can, however, export refined products such as gasoline and diesel.

Effect on oil prices

As the markets eagerly await further details, investors will surely want to know how the decision could affect their portfolios. While the true impact will obviously depend on the volume of condensate exports, Tuesday's announcement is generally bullish for West Texas Intermediate, or WTI, the main U.S. crude oil benchmark, and bearish for Brent, the global crude benchmark.

Indeed, WTI futures for August settlement rose by as much as 1.4% in late trading on Tuesday, while Brent for August delivery slipped 0.4% on the ICE Futures Europe exchange. The logic is that exports will increase the demand for domestic crude oil and condensates, lifting the price of WTI, while at the same time boosting global crude supplies and exerting downward pressure on the price of Brent.

Impact on oil producers, refiners, and others

If additional details emerge suggesting that condensate export volumes will be significant, WTI could rise further, lifting the share prices of WTI-levered companies. Brent, meanwhile, would theoretically fall, though the threat of a supply disruption in Iraq, OPEC's second-largest oil producer, continues to support its price.

If the Iraq conflict were resolved peaceably, however, with no impact on that nation's oil production and exports, Brent would likely fall back down to roughly $110 a barrel. This would lead to a compression of the Brent-WTI spread, the chief determinant of U.S. refiners' profits, and weigh on refining stocks such as Valero (NYSE:VLO), Marathon Petroleum, and HollyFrontier.

Not surprisingly, Valero, the nation's largest independent refiner, is leading the charge against lifting the crude oil export ban. Not only would large-scale crude and condensate exports reduce the company's margins and earnings, but they could also put at risk its planned investments in topping units -- refining units designed to boost light oil processing capacity -- at its Houston and Corpus Christi refineries.

Lastly, large-scale condensate exports could also make redundant a type of mini-refinery known as a condensate splitter, which processes condensate just enough to create readily exportable products. Companies planning to build -- or currently building -- these splitters include Kinder Morgan Energy Partners (NYSE: KMP), Targa Resources Partners, and Magellan Midstream Partners.

Luckily, all three companies have already inked firm long-term supply contracts with their respective customers, which should ensure their projects come to fruition. For instance, Kinder Morgan's $360 million splitter -- being constructed near the Houston Ship Channel and expected to start up this fall -- is supported by a long-term, fee-based agreement with BP (NYSE:BP). Still, large-scale condensate exports could seriously challenge the economics of new condensate splitters or capacity expansions at planned plants.

The takeaway

The Obama administration's decision to allow exports of minimally processed condensates is a crucial first step in updating the 40-year ban on crude exports to reflect the nation's newfound bounty of oil. While additional details regarding federal regulators' plans need to surface before one can draw firm conclusions, the move is generally bullish for U,S, oil producers and generally bearish for the nation's refiners.