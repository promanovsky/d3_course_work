Tags: | | |

Triclosan Ban: Minnesota Outlaws Chemical Found in Antibacterial Soaps Soaps containing the antibacterial chemical triclosan are displayed on a shelf at a Minneapolis pharmacy.





On Friday, Minnesota Gov. Mark Dayton signed the prohibition into law, making the Gopher State the first in the nation to ban the use of triclosan in most retail consumer hygiene products,



But why?



Urgent: Do You Approve Or Disapprove of President Obama's Job Performance? Vote Now in Urgent Poll



On tests in lab animals, triclosan has been shown to disrupt hormones that are critical for reproduction and development. The substance also reportedly contributes to the development of resistant bacteria. So far, studies have not shown that triclosan poses any similar risk to humans, but it hasn't stopped some from raising concerns over the use of the chemical.



One of those individuals is Minnesota State Sen. John Marty, who was one of the lead sponsors on the triclosan ban bill.



"While this is an effort to ban triclosan from one of the 50 states, I think it will have a greater impact than that," Marty said following the signing, adding that he believes soap manufacturers will phase out the use of triclosan altogether over time.



One such manufacturer to begin the process of eliminating triclosan is Procter & Gamble, which recently announced it would be phasing the chemical out from all consumer products by the end of 2015.



Not everyone, however, agreed with the bill or Marty's assertion that triclosan would be phased out over time.



Douglas Troutman, vice president and counsel for governmental affairs for the American Cleaning Institute, reportedly attempted to dissuade the governor from signing the bill into law with a letter.



"Instead of letting federal regulators do their jobs, the legislation would take safe, effective, and beneficial products off the shelves of Minnesota grocery, convenience, and drug stores," Troutman wrote.



According to the Food and Drug Administration, triclosan is currently used in an estimated 75 percent of anti-bacterial liquid soaps and body washes sold across the United States, the AP noted. Last year, the FDA announced that it would reevaluate the safety of triclosan and other germ-killing ingredients that are used in personal cleaning products.



Urgent: Assess Your Heart Attack Risk in Minutes. Click Here.



A ban on triclosan, the germ-killing agent that is found in antibacterial soaps and antifungal ointments, has been put into effect in Minnesota.On Friday, Minnesota Gov. Mark Dayton signed the prohibition into law, making the Gopher State the first in the nation to ban the use of triclosan in most retail consumer hygiene products, The Associated Press reported But why?On tests in lab animals, triclosan has been shown to disrupt hormones that are critical for reproduction and development. The substance also reportedly contributes to the development of resistant bacteria. So far, studies have not shown that triclosan poses any similar risk to humans, but it hasn't stopped some from raising concerns over the use of the chemical.One of those individuals is Minnesota State Sen. John Marty, who was one of the lead sponsors on the triclosan ban bill."While this is an effort to ban triclosan from one of the 50 states, I think it will have a greater impact than that," Marty said following the signing, adding that he believes soap manufacturers will phase out the use of triclosan altogether over time.One such manufacturer to begin the process of eliminating triclosan is Procter & Gamble, which recently announced it would be phasing the chemical out from all consumer products by the end of 2015.Not everyone, however, agreed with the bill or Marty's assertion that triclosan would be phased out over time.Douglas Troutman, vice president and counsel for governmental affairs for the American Cleaning Institute, reportedly attempted to dissuade the governor from signing the bill into law with a letter."Instead of letting federal regulators do their jobs, the legislation would take safe, effective, and beneficial products off the shelves of Minnesota grocery, convenience, and drug stores," Troutman wrote.According to the Food and Drug Administration, triclosan is currently used in an estimated 75 percent of anti-bacterial liquid soaps and body washes sold across the United States, the AP noted. Last year, the FDA announced that it would reevaluate the safety of triclosan and other germ-killing ingredients that are used in personal cleaning products. Related Stories: FDA Antibacterial Soap Rule Suggests Concern Over Chemicals



Does Antibacterial Soap Kill Germs? © 2021 Newsmax. All rights reserved.



1 Like our page 2 Share

TheWire

A ban on triclosan, the germ-killing agent that is found in antibacterial soaps and antifungal ointments, has been put into effect in Minnesota.

triclosan, ban, minnesota, antibacterial

377

Wednesday, 21 May 2014 07:48 AM

2014-48-21

Wednesday, 21 May 2014 07:48 AM