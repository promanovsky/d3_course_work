An artist’s reconstruction of the world’s largest-ever flying bird, Pelagornis sandersi, identified by Daniel Ksepka, Curator of Science at the Bruce Museum in Greenwich, Connecticut is pictured in this undated handout image provided by Bruce Museum. With a wingspan of about 6.1 to 7.4m, Pelagornis sandersi was more than twice as big as the Royal Albatross, the largest living flying bird. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

WASHINGTON, July 8 — The wandering albatross, a magnificent seabird that navigates the ocean winds and can glide almost endlessly over the water, boasts the biggest wingspan of any bird alive today, extending almost 3.5 metres.

But it is a mere pigeon compared to an astonishing extinct bird called Pelagornis sandersi, identified by scientists yesterday from fossils unearthed in South Carolina, that lived 25 to 28 million years ago and boasted the largest-known avian wingspan in history, about 6.1 to 7.4m.

Size alone did not make it unique. It had a series of bony, tooth-like projections from its long jaws that helped it scoop up fish and squid along the eastern coast of North America.

“Anyone with a beating heart would have been struck with awe,” said palaeontologist Daniel Ksepka of the Bruce Museum in Greenwich, Connecticut, who led the study published in the Proceedings of the National Academy of Sciences. “This bird would have just blotted out the sun as it swooped overhead. Up close, it may have called to mind a dragon.”

With its short, stumpy legs, it may not have been graceful on land, but its long, slender wings made it a highly efficient glider able to remain airborne for long stretches despite its size.

It belonged to an extinct group called pelagornithids that thrived from about 55 million years ago to 3 million years ago. The last birds with teeth went extinct 65 million years ago in the same calamity that killed the dinosaurs.

But this group developed “pseudoteeth” to serve the same purpose. They lived on every continent including Antarctica. “The cause of their extinction, however, is still shrouded in mystery,” Ksepka said.

“All modern birds lack teeth, but early birds such as Archaeopteryx had teeth inherited from their non-bird, dinosaurian ancestors. So in this case the pelagornithids did not evolve new true teeth, which are in sockets, but rather were constrained by prior evolution to develop tooth-like projections of their jaw bones,” said Paul Olsen, a Columbia University palaeontologist who did not take part in the study.

These birds lived very much like some of the pterosaurs, the extinct flying reptiles that lived alongside the dinosaurs that achieved the largest wingspans of any flying creatures, reaching about 11m.

Its fossils were found in 1983 when construction workers were building a new terminal at the Charleston International Airport. Its skull is nearly complete and in great condition, and scientists also have important wing and leg bones, the shoulder blade and wishbone.

Until now, the birds with the largest-known wingspans were the slightly smaller condor-like Argentavis magnificens, which lived about 6 million years ago in Argentina, and another pelagornithid, Pelagornis chilensis, that lived in Chile at about the same time.

At about 22-40 kg, Pelagornis sandersi was far from the heaviest bird in history, with numerous extinct flightless birds far more massive. — Reuters