Meteor observation doesn't have to be rocket science: All you have to do is lie back in a comfortable place and look up at the sky with the naked eye. Every so often, a meteor will flit across the stars. You simply make a note on a clipboard or speak into a tape recorder.

On Friday night and early Saturday morning (May 23-24), Earth will plow through debris shed over the years by Comet 209P/LINEAR. The result likely will be a new meteor shower, and possibly a spectacular meteor storm of 1,000 or so shooting stars per hour, experts say.

Even if you can't watch the meteor shower in person, you can catch the celestial showcase live online via a few webcasts. The Slooh community is hosting live views of the shower beginning at 11 p.m. EDT (0300 May 24 GMT), and they will also stream a webcast about Comet 209P/LINEAR before that at 6 p.m. EDT (2200 GMT). Watch the webcasts directly through Slooh.com, or catch the Slooh meteor shower feed on Space.com. The Virtual Telescope Project will also host a meteor shower webcast. [See maps and pictures about the new meteor shower]

How to prepare

No two observers prepare for a meteor vigil the same way. It will help if you can take a late-afternoon nap and a shower, and wear all fresh clothing. The ground can get cold, so heavy blankets, sleeping bags, cushions and even pillows are all essential. Sleeping bags provide some mosquito protection, but don't forget the insect repellent!

A long, reclining lawn chair makes a good observation platform because it's comfortable, portable and, in most cases, relatively inexpensive. It also allows you to move your head toward any section of the sky. A Thermos of hot coffee, tea or juice is a welcome comfort. Avoid alcohol; it impairs night vision.

The darker, the better

Find a safe observing site that provides a wide-open view of the sky. Once you arrive, allow 20 minutes for your eyes to become fully adapted to the dark. You can use a flashlight, but only after you make some key modifications: Cover the lens with a sheet of dark-red cellophane, since dim red light affects your eyes far less than lamplight.

Complete darkness is best for observing meteors. With light pollution so widespread, it's getting harder to find a truly dark sky. Use the bowl of the Little Dipper to help determine how dark the sky in your area is. The brightest star in the Little Dipper is Kochab, a second-magnitude star. The next brightest is Pherkad, at third magnitude. The next brightest star is fourth-magnitude, and the next brightest is fifth-magnitude. So, if you can see all four stars in the bowl, you're in a pretty dark observing site. Keep in mind that for an increasing number of locations, only Kochab and Pherkad are visible, meaning you're likely to miss many of the fainter streaks.

It really doesn't make much of a difference in which direction you face. You just don't want trees, buildings or sky glow blocking your field of view. Gazing directly overhead (at the zenith) might be best. Your clenched fist held at arm's length measures roughly 10 degrees; some groups recommend looking about 60 degrees up in the direction of the radiant; the radiant for the Comet 209P/LINEAR meteors is in the dim constellation of Camelopardalis, which will be located about one-third up in the sky from the north-northwest horizon.

Use a portable radio to stay updated on the weather. Before changing to what might be a better site, however, note whether the cloudiness is spotty. Sometimes, there are lengthy intervals when few meteors can be seen. Of course, hourly counts mean little if your sky is not entirely clear. [How to Pronounce the Camelopardalids (Video)]

How to count

If you just want to wait for the occasional shooting star, that's fine. But it's much more fun and interesting to make a useful meteor count that can be reported to the International Meteor Organization (IMO) and the American Meteor Society (AMS) to be compared with other people's results. You'll need a watch or a clock, a notepad, a pen or pencil, a red-covered flashlight and, if you have it, a tape recorder so you can dictate notes without taking your eyes off the sky.

Meteors have a way of coming in bunches that defy counting, magnitude estimating and plotting all at the same time. Often, people notice what some call the "clumping effect" — meteors arrive in groups of two or three, and will fall within a matter of seconds, followed by a lull period of several minutes or more before the sky again bears fruit. Some observers say this is an illusion; if you are observing alone, just make hourly counts and estimate magnitudes. It's helpful to use a hand counter to click off the number of meteors you see.

"Shower" with a friend?

Meteor observing can also be a fun social affair. But if you observe in a group, do not combine results! Group counts are worthless. If you're in a group, assign each observer a limited program. Every person should observe as if alone, preferably watching a different section of the sky. Try not to be influenced by somebody who might yell out something like, "Whoa, look at that!"

The simplest method is to just count the meteors you see. If a few obstructions intrude, they should block no more than 20 percent of your view, and you must note the percentage of the view they're covering. The same applies to clouds.

If you take any breaks, note the times each begins and ends. Your vigil should total at least an hour, not counting breaks, and preferably longer. Ever hour, separate your observing records with a time annotation.

For more details, check out the IMO website at http://www.imo.net/ and the American Meteor Society website at http://www.amsmeteors.org/.

Editor's Note: If you capture an amazing photo of the new meteor shower, or any other night sky view, that you'd like to share for a possible story or image gallery, please contact managing editor Tariq Malik at spacephotos@space.com.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Joe Rao serves as an instructor and guest lecturer at New York's Hayden Planetarium. He writes about astronomy for Natural History magazine, the Farmer's Almanac and other publications, and he is also an on-camera meteorologist for News 12 Westchester, N.Y. Follow us @Spacedotcom, Facebook andGoogle+. Original article on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.