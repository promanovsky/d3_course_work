It was a good day for world champion Marc Marquez in Texas as he finished fastest in both of Friday's free practice sessions for of Sunday's Grand Prix of the Americas.

Marquez, who won the season-opening race in Qatar last month, set a leading time of two minutes 3.490 seconds to lay down a marker ahead of Saturday's qualifying, with the Ducati of Andrea Dovizioso second quickest and Honda team-mate Dani Pedrosa third.

Andrea Iannone was fourth, with Valentino Rossi making up the top five. Cal Crutchlow made it into the top 10 with a ninth-placed run, one place ahead of fellow Briton Bradley Smith.