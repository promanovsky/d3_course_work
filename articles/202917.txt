David Letterman had the same question that almost everyone that saw James Franco's nearly-nude Instagram photo this past week had: "James, honest to God, why?"

"First of all, there's not a person in this room that hasn't had their hand in their own pants," Letterman said upon showing the photo on Thursday's "Late Show." "But I don't care what you do in your underpants."

Related Pics: Must-see celebrity selfies

"It's not like I'm putting that on billboards," the 36-year-old "This is the End" actor said, defending his selfie. "Ostensibly, Instagram is for my fans, but now all the bloggers are following me so they'll just take my image and use it for whatever they want."

Letterman attempted to get Franco to admit that the photo was "provocative," and that's when things got a little heated. "I didn't ask you to look at it! It's what the people want," the actor replied.

The "Late Show" host quipped back, "Why is it my fault now? I had nothing to do with this."

Franco said of his Instagram-posting strategy, "To me, it's just a fun thing. It's something I don't put a lot of thought into, but it gets a lot of attention. I have a lot of followers on Instagram."

Do you agree with Franco's explanation of his "provocative" selfie?