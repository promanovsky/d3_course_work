Microsoft's (NASDAQ:MSFT) Surface Pro 3 is a dramatic improvement over its prior Surface Pro models -- unfortunately, that's still not good enough. Although critics generally agree that Microsoft continues to make great strides in improving the laptop/tablet hybrid form-factor, most believe that the Surface Pro 3 still comes up short.

To make matters worse, Microsoft's decision to market the device as a direct alternative to Apple's (NASDAQ:AAPL) Macbook Air seems to have only exacerbated the problem.

Microsoft continues to iterate on its vision

Microsoft's Surface Pro 3 embodies, better than any other device, Microsoft's vision for personal computing. With a 12-inch touch-screen and optional keyboard that doubles as a protective cover, Microsoft's Surface Pro 3 may be the first device that can truly claim to double as both a laptop and a tablet, and make use of the hybrid nature of Windows 8.

The Surface Pro and Surface Pro 2 also tried to accomplish this feat, but were hamstrung by a number of limitations. Although they were capable of running just about any piece of software written for Microsoft's Windows operating system, they weren't ideal tablets -- too thick and too heavy, with bad battery life and few mobile apps. They didn't make for the best laptops, either, as their 10.6-inch wide screens were too small for productive work, and their kickstands required the use of a solid surface like a table or desk.

Microsoft's latest Surface Pro fixes many of these problems, with a larger display, thinner and lighter design, and better kickstand. Reviewers have almost unanimously praised these improvements.

"Writing on the Surface Pro 3 [is] easy," wrote the LA Times' Salvador Rodriguez. "[It] was more comfortable to use on my lap than any laptop I've tried."

PCWorld's Mark Hachman is a fan of the new form factor, claiming that the "slimmed-down Surface Pro 3 feels like a proper modern-day tablet."

Too many trade-offs

But in spite of the improvements, many reviewers remain wary of recommending the device, particularly when compared to Apple's Macbook Air. Microsoft itself is responsible for the comparison, going so far as to offer owners of Apple's laptop credit toward a Surface Pro 3 when they trade in their device.

"Microsoft Surface Pro 3 Doesn't Stand Up to MacBook Air" declared Re/code's Katherine Boehret. Although Boehret agreed that the device could, in theory, replace both a laptop and a tablet, she found it to be lacking on both fronts. As a tablet, Boehret found fault with the over-sized screen, and as a laptop, she took issue with its awkward top-heavy build and "flimsy keyboard."

AnandTech's Anand Lai Shimpi was more favorable in his review, but ultimately came to a similar conclusion, stating that he would "rather carry a good notebook and a lightweight tablet."

The New York Times' Farhad Manjoo called the Surface Pro 3's flaws "damning" and characterized the device as nearly worthless for intensive work. Rather than compare it to Apple's Macbook Air, Manjoo thought it more similar to Apple's iPad -- a much cheaper, far more portable device.

Microsoft's hardware ambitions

In so far as consumers use reviews to make gadget purchasing decisions, it doesn't appear that Microsoft's latest Surface Pro will be a runaway success. Holding the device up against what's arguably the best Ultrabook on the market, Apple's Macbook Air, hasn't resulted in many favorable comparisons.