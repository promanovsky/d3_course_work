Eating processed red meat, such as sausages or cold cuts, has been linked to an increased risk of heart failure or death in men, Medical News Today reported.

In a new study published in the journal Circulation: Heart Failure, researchers analyzed a cohort of 37,035 healthy men, ages 45 to 79, from The Cohort of Swedish Men Study at the Karolinska Institutet in Stockholm. The study followed the men from 1998 to 2010.

Throughout the study period, 2,891 men developed heart failure and 266 died from heart failure. Men who reported eating the most processed meat – 75 or more grams per day – were 28 percent more likely to develop heart failure compared to men who ate 25 grams or less of processed meat per day. Men who ate the most processed meat were also twice as likely to die from heart failure compared to men who consumed the least.

Furthermore, for every 50 g daily increase in processed meat consumption – the equivalent of one or two extra slices of ham – risk of heart failure increased by about 8 percent and risk of death from heart failure increased by 38 percent, according to the researchers.

No link between heart failure and death was found among men who consumed unprocessed red meat.

"To reduce your risk of heart failure and other cardiovascular diseases, we suggest avoiding processed red meat in your diet and limiting the amount of unprocessed red meat to one to two servings per week or less,” study author Joanna Kaluza, and assistant professor in the department of human nutrition at Warsaw University of Life Sciences in Poland, said in a statement. “Instead, eat a diet rich in fruit, vegetables, whole grain products, nuts and increase your servings of fish."

Click for more from Medical News Today.