LONDON (MarketWatch) — Continuing concerns about the fallout from the Ukraine-Russia standoff added pressure on European stocks on Friday, with German equities bearing the brunt of tension.

The Stoxx Europe 600 index SXXP, +0.64% fell 0.8% to close at 333.50, trimming its weekly advance to 0.3%. U.S. stocks were also lower. Read: Stock market live blog

The losses came as U.S. President Barack Obama consulted with key European leaders on the Ukraine crisis, warning Russia that they are prepared to proceed with more significant sanctions against the country.

The consultation came after U.S. Secretary of State John Kerry late Thursday accused Russia of violating its commitment to ease tensions in eastern Ukraine.

“If Russia continues in this direction, it will not just be a grave mistake, it will be an expensive one,” Kerry said, citing growing outflow of capital from Russia in recent months. “It’s a preview of how the free world will respond.”

Shutterstock

In response to the crisis-fueled volatility on financial markets, Standard & Poor’s Ratings Services on Friday cut Russia’s credit rating to one notch above junk.

S&P further put Russia in line for another rating downgrade, a move “that will heighten concerns about the financial impact of the conflict in eastern Ukraine,” Richard Perry, market analyst at Hantec Markets, said in a note.

Russian rate hike

Meanwhile, Russia’s central bank on Friday bumped up its key interest rate by a half-percentage point to 7.5%. The risk that inflation will exceed its 5% target at the end of this year has “increased substantially” because of exchange-rate dynamics and “unfavorable conditions” in trade markets, the Central Bank of the Russian Federation said in a statement on its website.

Russia’s blue-chip MICEX index XX:MCX fell 1.6% to 1,280.12, and the RTS index RTS, +0.05% pulled back 2.3% to 1,119.37.

Among other European benchmarks, Germany’s DAX 30 DAX, +0.06% was hit hard, down 1.5% to 9,401.55, as the country has a deep goods-trading relationship with Russia. For the week, the German benchmark lost 0.1%.



The U.K.’s FTSE 100 UKX, +0.94% gave up 0.3% to 6,685.69, but closed out the week 0.9% higher.

France’s CAC 40 PX1, +0.60% lost 0.8% to 4,443.63, reducing its weekly advance to 0.3%.

Cyprus’s CSE General Index XX:GENERAL rose 0.1% to 118.19, as S&P lifted the long-term credit rating on the Mediterranean country to “B” from “B-,” based on a better-than-expected economic and budgetary performance.

Goldman upgrade

But even with the Ukraine uncertainty sending most shares lower on Friday, Goldman Sachs remained broadly upbeat on European equities and upgraded its long-term targets for the Stoxx 600. The new forecasts for the pan-European benchmark are 410 and 450 for the end of 2015 and 2016 respectively, compared with 380 and 400 previously.

“The main driver of this is a larger expected decline in the equity risk premium, resulting in a higher P/E multiple,” the Goldman Sachs analysts said.

Movers

Among movers on Friday, shares of Tullow Oil PLC TLW, +1.84% fell 2.5%, after the company said it would abandon an exploration well in Mauritania because of a lack of discovery of hydrocarbons.

French auto maker Peugeot SA UG, -4.21% said its quarterly sales rose, but its shares fell by 3.6%.

Also in France, shares of Alstom SA ALO, -2.84% were halted, fulfilling a request from the country’s market authority AMF, according to exchange operator Euronext Paris. Alstom shares on Thursday rallied 10.9%, following a report by The Wall Street Journal that General Electric Co. GE, +2.36% is in talks to buy Alstom’s energy business. On Friday, The Wall Street Journal reported the French government is working on alternatives to that sale.

Neste Oil Oyj FI:NES1V slid 5.1% after the Finnish oil-refining company lowered its full-year guidance for operating profit.

Deutsche Bank AG DBK, +1.30% DB, +1.97% gave up 2% after German newspaper Handelsblatt reported that the bank is considering a capital increase of up to 5 billion euros ($6.92 billion). A representative from Deutsche Bank declined to comment on the report.

Bucking the negative trend, Electrolux AB ELUX.B, -0.66% jumped 11%. First-quarter earnings for the Swedish household-appliance maker outstripped expectations. The company said a pickup in demand in Europe offset a temporary decline in the U.S., which grappled with unusually cold weather this winter.

More from MarketWatch:

DNB Bank sells its Russian operations

No deal in So. Africa platinum miners strike