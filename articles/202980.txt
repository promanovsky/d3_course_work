Wonder what the hell James Franco is doing? Apparently not even his own brother, Dave, knows what’s going on with the eccentric actor, as revealed on Conan.

Whilst discussing the near-nude selfie James Franco put out to the online world, Dave tried to couch things in the right context for his host, Conan O’Brien.

“I’m not on social media so I wasn’t made aware of this until I was doing the junket for this movie [Neighbors] and… I don’t know. Compared to some of the weird things he’s doing, it seems like kind of a refined act.”

On the nude painting of friend and frequent co-star Seth Rogen (also in Neighbors):

“This is dignified for him.”

Is it weird that that actually makes sense?

Yes.

In addition to James’ antics, Dave discussed his first sex scene, in Neighbors, and his “concave butt,” an ailing trait about which he and Conan were happy to commiserate.

Check out the latest on the Francos in these clips.

