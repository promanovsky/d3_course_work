Google is announcing today its plans to offer targeted app install ads on mobile search and YouTube, following moves made by other tech industry companies, including Facebook and more recently Yahoo and Twitter. Google says that businesses looking to promote these app installs across the AdMob network will be able to target consumers based on data Google already has on file, like what apps they use, how often they use them and what purchases they’ve made.

For example, says Google, someone who regularly uses an app to measure their runs and other exercises, might see an app that lets you track your diet.

Google will help marketers target users using insights from Google Play, offering tips as to what keywords worked best to convert searches to downloads.

In addition, Google is also expanding its mobile app deep linking initiative to integrate deep linking with AdWords. What that means is that businesses can buy advertisements that, when clicked, will redirect users directly inside their already-downloaded and installed mobile apps.

For example, explains Google in a blog post detailing this program, if a consumer has HotelTonight installed on their phone, an ad for “hotels in San Francisco” on Google.com could take them directly to the page for San Francisco hotels in the HotelTonight app instead of to the app’s main landing page.

We had heard whispers of Google’s plans to integrate deep linking with AdWords earlier this year, but our understanding at the time was that the development was much further off. After all, before being able to offer ads using deep links, Google would have to actually get developers to integrate deep linking technology inside their apps. But Google has made several big steps toward that goal in recent weeks, following the initial introduction of mobile deep linking this fall, which saw participation from Allthecooks, AllTrails, Beautylish, Etsy, Expedia, Flixster, Healthtap, IMDb, Moviefone, Newegg, OpenTable, and Trulia.

Earlier this month, Google announced that 24 more apps had integrated this deep linking technology, including 500px, AOL, BigOven, Bleacher Report, Booking.com, Eventbrite, Glassdoor, Goodreads, Huffington Post, Merriam-Webster, Pinterest, Realtor.com, Seeking Alpha, TalkAndroid, TheFreeDictionary, The Journal, TripAdvisor, Tumblr, Urbanspoon, Wattpad, YP, Zagat, Zappos and Zillow.

The company notes that there’s demand for ads that can get consumers to re-launch their apps because today over 80% of apps are only used once after being downloaded. Businesses are struggling to convert application installs into regular, engaged customers.

The app stores are also ever-expanding, which increases these challenges. For instance, as of May 2013, Google Play had seen 50 billion app downloads of over 1 million applications, the company said this morning, and every day users in 190 countries are downloading apps.

In AdWords, businesses will soon be able to measure conversions across the entire app lifecycle thanks to these changes, says Google, starting from app installation to re-enagement to in-app purchases.

App install ads and deep linking to apps go hand-in-hand, of course, as the former gets the app on consumers’ mobile devices, then the latter points consumers to the right information in their app when searching.

Install ads, which have turned into a cash cow for Facebook, will be coming to YouTube via its TrueView functionality, the service that lets users skip through videos, in addition to Google search. Google had previously allowed publishers to promote their ads on Google search, pointing consumers to their app store landing page, but never had marketers been able to so narrowly target these ads in the way that’s being announced today.

Google says these features will roll out to AdWords over the next few months. The company is offering more details on this and other AdWords announcements at an event today, which is being live-streamed here.