BlackRock, Inc. (NYSE:BLK) reported an increase in first quarter earnings of almost 20%, outperforming consensus estimates. The surge was mainly due to the growth in asset under management. BlackRock posted total asset under management at $4.4 trillion at the end of the quarter, which is an increase of 12% from the corresponding quarter of the previous year and 1.8% increase from the last quarter.

The Blackstone Group L.P. (NYSE:BX) also reported its first quarter earnings today, posting a higher profit on the back of gains made from the sale of holdings.

BlackRock beats consensus estimates

Earnings for BlackRock, Inc. (NYSE:BLK) came in at $756 million, or $4.40 per share, an increase of $632 million, or $3.62 per share from the year ago quarter. Profit for the company came in at $4.43, up from $3.65 after excluding certain items. BlackRock recorded an increase of 9% in the revenue totaling $2.67 billion. Consensus analysts estimates polled by Thomson Reuters expected earnings of $4.11 and revenue of $2.66 billion. Total long term net inflows came in at $26.7 billion for the period. Retail inflows for BlackRock came in at $14 billion whereas assets under management were $508.7 billion.

At the end of the quarter, the iShares exchange traded fund business had assets under management worth $930.4 billion. Net inflows came in at $7.76 billion. On the basis of assets, BlackRock is the biggest ETF provider in the United States.

The world’s largest asset manager has impressive profit records over the year, and it recently changed some positions within the company to enhance its performance. Recently, the firm appointed a new co-president and an operating chief, and gave promotions to several other executives.

BlackRock, Inc. (NYSE:BLK) shares were inactive premarket. The stock, which closed at $310.15 Wednesday, is down 2% so far this year.

Blackstone posted higher profits

The biggest publicly traded private equity firm stated that its economic net income surged 30% to $814 million in the first three months of the year. Earnings per share for The Blackstone Group L.P. (NYSE:BX) came in at 70 cents per share, outperforming the estimates of 55 cents by analysts surveyed by Thomson Reuters.

The distributable earnings of the financial firm surged 24% to $485 million, but investment income that includes the unrealized gains and losses dropped 14% in the quarter to $116 million. The private equity and real estate segments of The Blackstone Group L.P. (NYSE:BX) performed lower in contrast to strong performances earlier. However, performance fees helped the company to grow its private equity economic income over 3 times to $319 million.

According to Sterne Agee analyst Jason Weyeneth, the ENI beat by the Blackstone is “performance driven,” but the distribution of $0.35 was on the lower end of the estimates. Weyeneth expects robust growth of the “franchise” in the quarter despite weakness in the stock recently.

Sterne Agee has a Buy rating on Blackstone with a price target of $42.