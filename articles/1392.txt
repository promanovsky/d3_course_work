Google has announced new plans to promote Android wearable technology, hoping to position the operating system as the default platform for smart devices in the same way that it already is for smartphones.

“We want to develop a set of common protocols by which they can work together,” said Google’s senior vice president of Android and Chrome Sundar Pichai at the SXSW conference in Texas, reported The Verge.

The internet gant will release a new software development kit (SDK) for wearable sensors this month, allowing this technology to more easily communicate with the Android operating system.

Although Android already has a presence in the wearables market through a number of smartwatches that operate on the OS (and, of course, through Google Glass) Picai stressed that the company’s vision extends to more exotic devices, which might include smart clothing.

Pichai also hinted that this new “mesh layer” of communications between Android and various sensors might incorporate the home automation devices built by Nest, the company that Google acquired in January this year for $3.2bn (£2bn).

Nest has so far only produced two products – a smart thermostat and a fire alarm – but has been praised for the quality of its design and for intuitive software that ‘learns’ users’ habits.

Pichai said that he sees a parallel between the smartphone ecosystem and the wearables ecosystem. While smartphones became tiny computers, wearables will become hubs for information about the real world fed by various sensors.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

However, in some ways smartphones are already usurping this territory. Just last month two products were launched that took the brains inside a normal smartphone and used them to power new sensors.