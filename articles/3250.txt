TV

Liam Nesson made a surprise appearance when the 'Girls' creator/star hosted the March 8 episode of the NBC variety show, while Louis C.K. is tapped to return as host for the March 29 outing.

Mar 10, 2014

AceShowbiz - Lena Dunham doesn't care about people who loathed her naked scenes on "Girls". In fact, the Golden Globe-winning actress stripped down to her birthday suit on "Saturday Night Live" when she hosted the show on March 8.

Appearing in a sketch with "SNL" regular Taran Killam, who acted as her "Girls" co-star Adam Driver, she told the story of Adam and Eve with "Girls"-esque dialogue. She and Killam's Driver were bickering about the nature of their relationship.

Vanessa Bayer reprised her role as Shoshanna which she portrayed in the previous "Girls" parody. This time, she acted as the snake in the Garden of Eden and convinced Dunham's Eve to eat the forbidden fruit. When God scolded Eve for eating the apple, she responded, "Can you please not apple shame me right now? Seriously, I know I committed original sin, but at least it's original."

Dunham did not only parody her own show, but also "Scandal". New cast member Sasheer Zamata played Kerry Washington's Olivia Pope, who assigned difficult tasks to her team. Dunham played a newest staff member who was clueless about how they would accomplish these tasks.

The latest episode of the variety show was boosted by Liam Neeson's surprise appearance. The actor played himself alongside Jay Pharoah as President Obama in a skit that took a jab at Russian President Vladimir Putin's action to order the troops to move into Crimea.

Another highlight from the episode was the Weekend Update segment with Killam impersonating Oscar best actor winner Matthew McConaughey. Former "SNL" regular Fred Armisen made a cameo as one of Putin's friends in another Weekend Update segment, while "Mad Men" star Jon Hamm made an appearance as the guest on a teen girls talk show.

The National served as the musical guest for the episode.

"SNL" will return with a new episode on March 29 with Louis C.K. as the host. It will be the second time the "Louie" creator/star takes the stage on the NBC long-running series.

"Biblical Movie":

"Scandal":

"Obama Ukraine Address Cold Open":

"Weekend Update: Matthew McConaughey":

"Weekend Update: Vladimir Putin's Best Friends From Growing Up":