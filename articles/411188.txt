“Please come back,” is quickly becoming Jimmy Fallon’s token line to celebrities as he makes them participate in crazy stunts.

Julia Roberts was his latest victim this week on The Tonight Show, and America’s sweetheart participated in a lovely game of “Face Balls” with Fallon.

Yes, they are very, very aware of how dirty it sounds.

In the innocent game, Fallon and Roberts take turns throwing clear inflatable balls of varying sizes at each other’s faces. All the while, a camera records their close-up expressions as the ball hits.

Roberts is understandably nervous when it’s her turn, and she even goes so far as to tell Fallon she’s scared. This is the point where the host tells her he loves her and hopes she comes back. Her fear, however, is not enough to get Fallon to back down. He chucks the ball at Roberts.

“I feel a Christmas card coming on,” Roberts says as Fallon begins the slow-motion replay of the ball hitting her face.

Then the two upgrade and go again with larger clear, inflatable balls.

“I think I’m better handling big balls,” Roberts says once Fallon pulls out the next level of the game.

“This is, like, the comedy version of Fifty Shades of Grey,” Roberts says before Fallon hits her with his bigger ball.

For the second time around, Roberts really tries to prepare herself to relax after her hilariously horrified expression during the first attempt. The duo uses the element of surprise, but Fallon throws a little more forcefully than expected, causing the audience to erupt in laughter. “Did I hurt you?” Fallon asks after embracing the actress. Roberts touches her tooth tentatively and then laughs, shaking her head.

The second slow-motion replay is by far the most hilarious. Roberts’ lip gets stuck to the ball for a second, pulling it off the Normal Heart star’s face. Her hair also flies in front of her pretty flawlessly.

Watch the full video below.

www.youtube.com/embed/dGbctMyrTN0?list=UU8-Th83bH_thdKZDJCrn88g