Indian shares are seen opening higher on Thursday, tracking firm global cues as investors digest comments from Federal Reserve Chair Janet Yellen on monetary policy and the economic outlook.

IT stocks are likely to be in focus after Tata Consultancy Services, India's largest software services exporter, reported a 48 percent jump in quarterly consolidated net profit, in line with consensus estimates. Painting a rosy picture for 2014-15, the company said that its growth would be higher than industry body Nasscom's prediction of 12-14 percent.

CRISIL, HCL Technologies, GlaxoSmithKline Pharma and Wipro are among the prominent companies that will unveil their quarterly results later in the day. The stock exchanges will be closed tomorrow on account of Good Friday.

The benchmark indexes Sensex and Nifty fell about 0.9 percent each on Wednesday, extending declines for a third consecutive session, as concerns about rising inflation and private forecasts that India is bracing for a below-normal monsoon this year overshadowed firm global cues.

Foreign institutional investors remained net sellers to the extent of Rs. 44.69 crore yesterday, while domestic financial institutions offloaded shares worth Rs.347.70 crore, provisional data released by BSE showed.

Asian Markets

Asian stocks are rising for a second day bolstered by overnight gains on Wall Street. Google and IBM posted disappointing results and concerns over the turmoil in Ukraine persist, keeping gains under check.

Japan's Nikkei Index is declining 0.4 percent after Wednesday's sharp rally. Bank of Japan Governor Haruhiko Kuroda said today that the central bank will maintain its quantitative and qualitative easing until necessary to achieve its 2 percent price target in a stable manner. Chinese shares are also subdued as Premier Li Keqiang once again ruled out major stimulus to counter slowing growth.

U.S. And European Markets

U.S. stocks extended gains for a third session on Wednesday as investors digested dovish remarks from Fed Chair Yellen and the latest Beige Book report showing "modest or moderate" improvement in most regions of the country. Investors cheered positive earnings results and solid industrial production figures and took disappointing housing starts figures in their stride. The Dow gained a percent, the tech-heavy Nasdaq advanced 1.3 percent and the S&P 500 added 1.1 percent.

Speaking to the Economic Club of New York, Yellen insisted again that the central bank will remain highly accommodative until employment and inflation reach healthier levels. "The larger the shortfall of employment or inflation from their respective objectives, and the slower the projected progress toward those objectives, the longer the current target range for the federal funds rate is likely to be maintained," she said.

The European rallied on Wednesday as solid earnings reports and encouraging economic news from China and the U.K. helped offset worries about increasing instability in Ukraine. The German DAX climbed 1.6 percent, France's CAC 40 rose 1.4 percent and the U.K.'s FTSE 100 gained 0.7 percent.

For comments and feedback contact: editorial@rttnews.com

Market Analysis