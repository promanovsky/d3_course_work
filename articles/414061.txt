A scientist has created the best-ever global color map of Neptune's big moon Triton, using images taken by a NASA spacecraft 25 years ago.

Paul Schenk of the Lunar and Planetary Institute in Houston produced the map after restoring photos snapped by the Voyager 2 probe during its flyby of Neptune and Triton on Aug. 25, 1989. The new map has also been turned into a minute-long movie of Voyager 2's historic Triton encounter — the first and only time a spacecraft has ever visited the Neptune system.

The new map, which has a resolution of 1,970 feet (600 meters) per pixel, may help bring enigmatic Triton back into the spotlight. [Photos of Neptune, The Mysterious Blue Planet]

"In the intervening quarter century and its many discoveries, I think we have tended to forget how strange and exotic Triton really is!" Schenk wrote in a blog post Thursday (Aug. 21).

"Its effective surface age may be a little as 10 million years [old], clearly implying that active geology is going on today," he added. "The cantaloupe terrain, which I interpreted back in 1993 as due to crustal overturn (diapirism), hasn't been seen anywhere else. The volcanic region, with its smooth plains and volcanic pits large and small, is the size of Texas. And the southern terrains still defy interpretation."

Schenk produced the map using green, blue and orange filters. Colors have been enhanced to accentuate contrast but still show Triton roughly as human eyes would see it, NASA officials said.

In an interesting twist, NASA's New Horizons probe is scheduled to cross the orbit of Neptune on Monday (Aug. 25), 25 years to the day after Voyager 2's encounter. New Horizons is streaking toward a flyby of Pluto on July 14, 2015 that should return the first good looks at the distant dwarf planet and its moons.

The connections between Voyager 2 and New Horizons don't stop there; Triton and Pluto are very similar to each other in some ways. Both are just slightly smaller than Earth's moon, possess thin, nitrogen-dominated atmospheres and have various ices (of carbon monoxide, carbon dioxide, methane and nitrogen) on their surfaces, Schenk noted.

"What will we see at Pluto? Guesses have ranged from active geology to cold and cratered, so we are in for a suspenseful summer next year!" he wrote on his blog.

"Triton is of importance as it offers clues to what geologic features might look like on Pluto, given that the icy crusts of both bodies are probably rather similar and would presumably react in similar ways under internal stress and heat," he added. "So if there were or are volcanoes on Pluto, they could look similar to those we see on Triton."

Voyager 2 launched in August 1977, a few weeks before its twin, Voyager 1. The pair conducted an unprecendented "grand tour" of the outer solar system, returning good looks at the Jupiter, Saturn, Uranus and Neptune systems.

Both Voyager 1 and Voyager 2 then kept right on flying. Voyager 1 entered interstellar space in August 2012, and Voyager 2 (which took a different route through the solar system) is poised to do so soon.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Follow Mike Wall on Twitter @michaeldwall and Google+. Follow us @Spacedotcom, Facebook orGoogle+. Originally published on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.