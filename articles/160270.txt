As Steve Jobs prepped Apple (Nasdaq: AAPL ) CEO Tim Cook to take the reins of the world's most innovative consumer electronics company a few years ago, we can speculate on a likely bit of advice:

"Focus on the business, not on Wall Street."

#-ad_banner-#To be sure, Cook was getting an earful from Wall Street analysts and activist investors such as Carl Icahn a year ago, when shares were sliding toward the $400 mark. At the time, I noted that respected tech analyst Steve Milunovich of UBS said Apple's high cash balances and slowing growth should fuel aggressive buybacks . CEO Cook initially appeared uninterested in such a suggestion, though Apple has gone on to be an aggressive buyer of its own shares (which I'll discuss in greater detail in a moment).

Cook instead aimed to shift investors' focus to Apple's legendary product innovation engine, dropping many hints in subsequent quarters that Apple's engineers were developing powerful extensions to the core iPhone/iPad/iTunes/MacBook platform. And though we've been hearing about rumors of a new television ecosystem and a wearable iWatch, Cook is still running a business based on products developed under Steve Jobs.

It's time for Cook to get cracking. Will the long wait for Apple's new category killers soon end? After listening to the company's quarterly conference call, Milunovich notes that "Tim Cook indicated that Apple is expanding the things it is working on and has room to monetize services, suggestive of new product categories."

Why are new products so important? Because the current roster is growing a bit tired. A few key data points to ponder:

• Shipments of 16.35 million iPads missed consensus forecasts by more than 3 million as consumers appear less inclined to keep trading up to the next iteration.

• The company's iTunes/services revenues grew just 11% from a year ago, the lowest rate in recent memory.

• Selling prices for the iPhone dipped 6% from a year ago. "The high end of the smartphone market is essentially stagnant," notes Credit Suisse analyst Kulbinder Garcha.

Although Garcha concedes that Apple just delivered a very strong quarter, led by a high volume of iPhone sales, he has "yet to be convinced around the sustainability of bottom line growth within Apple's core business."

Product Innovation Versus Financial Engineering

Make no mistake, Apple carries little risk of stumbling badly. The company's vast and loyal customer base ensures solid recurring revenues. And Apple's gross margins -- just reported at 39.3% -- continue to defy gravity.

So if you already own this stock, stay the course. But if you are thinking about buying the stock at this point, a bit of caution is warranted.

Flickr/mattbuchanan

The primary reason to be bullish now is if you have faith that an iWatch, an iTV or any other new product will not only be extremely cool and innovative, but also faces a very large and lucrative market opportunity.

That was the case with iTunes, the iPhone and then the iPad, but it's unclear if Apple's next product categories will reap billion-dollar markets. Consumers may just decide that all of their entertainment and communications needs are being met by current products. Barclays analyst Ben Reitzes is unconvinced: "We are not sure any new categories move the needle like the iPad did."

To extend that notion further, Apple's stock was a true underdog when it was trading at $430 a year ago and was the source of much concern. With shares now up above $560 roughly a year later, that underdog status is gone.

Rewarding Shareholders

Apple's recent embrace of buybacks and dividend hikes are laudable. The rolling buyback plan has been boosted to $130 billion (from $100 billion), which means that another $66 billion in shares will likely be bought back in the next six quarters.

By the time the process is complete, the share count will likely have fallen nearly 20%.

Of course it's fair to question the merits of buybacks when share prices are surging. As I noted 13 months ago, "Apple's board should actually show a great deal of flexibility on the matter. At times like this, when shares have fallen sharply in a short period of time, buybacks should be the main focus. When shares rebound (which they likely will if Apple pursues an IBM-style strategy), dividend growth should be the focus."

An increasing emphasis on the dividend, to an annualized rate of $13 a share, reflects a new yield of more than 2%. That's helpful, but few investors are buying this stock for its income streams. Buybacks and dividends are nice, but they're not the primary reason to own Apple (even if Carl Icahn made that his battle cry).

Incidentally, if you're wondering about the impact of the 7-for-1 stock split, you should know that it may open the doors for some investors who like to own more shares of lower-priced stocks -- but in the grand scheme of things, it has zero impact on Apple's total valuation. In any event, studies show that any gains that ensue from stock split announcements are confined to the first day of trading after the announcement.

Risks to Consider: Apple's gross margin profile remains extremely unusual in the field of consumer electronics. Competitors such as Samsung ( SSNLF ) and Sony (NYSE: SNE ) can only sigh with envy. But pricing power can prove to be challenging, especially if consumers feel less of a need to chase upgrade cycles. So a potential compression in Apple's lofty gross margins is among the biggest risks for the stock.

Action to Take --> If you currently own shares of Apple -- or are thinking about buying shares at current prices -- stay up to date on the company's product roadmap. Apple is notoriously tight-lipped about product plans -- for instance, I spent a day with Apple's investor relations team a decade ago in the company's headquarters in Cupertino, California, and learned absolutely nothing new. But the Internet has ample fodder for research, especially in terms of pre-production plans with contract manufacturers and key suppliers.

More importantly, you should be convinced that there will be robust demand for any new products yet to be released. Apple's surging share price provides less of a cushion for investors that willingly trust that the company can do no wrong when it comes to pioneering the next billion-dollar category.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

© Copyright 2001-2016 StreetAuthority, LLC. All Rights Reserved.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.