— Graco Children’s Products is recalling 1.9 million infant car seats, bowing to demands from U.S. safety regulators, in what is now the largest seat recall in American history.

The recall, announced Tuesday, comes after a five-month spat between Graco and the National Highway Traffic Safety Administration. Earlier this year the company recalled 4.2 million toddler seats because the harness buckles can get stuck. But it resisted the agency’s demand to recall the infant seats.

Buckles can get gummed up by food and drinks, and that could make it hard to remove children. In some cases parents had to cut harnesses to get their kids out. The agency says that increases the risk of injuries in emergencies.

Graco argued that infant seats are used differently, and in an emergency, an adult can remove the whole seat rather than using the buckle.