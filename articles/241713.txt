We have been hearing rumors for a while about a new device from Samsung called the Samsung Gear Solo, we previously heard that this would be a stand alone smartwatch that would work independently from a smartphone or tablet.

Now a report by the WSJ has revealed that Samsung are working on a new stand alone smartwatch, the device will be powered by Samsung’s Tizen OS, and we suspect it will be launched as the Samsung Gear Solo.

Samsung are apparently in talks will the various mobile carriers in South Korea for their stand alone smartwatch, they have also been talking to various carriers in Europe about launching the device, according to the report.

The Samsung Gear Solo will come with its own SIM card, this will mean that you will be able to make and receive calls direct from the device without the need to pair it up to a smartphone or tablet, how exactly this will works remains to be seen, although we suspect that it may launch with some sort of Bluetooth accessories for making and receiving calls.

As yet there are no details on when the new stand alone smartwatch from Samsung will launch, although we suspect that we will see the new Samsung Gear Solo before the end of the year.

Source TechCrunch

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more