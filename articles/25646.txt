Flappy Bird left us as quickly as it came on the scene. A few months back it was all the rage. The objective of the game was simple. Clear obstacles by tapping the display. Actually doing it was not so simple and it could be a frustrating game. Developer Dong Nguyen removed the game, but has now confirmed that Flappy Bird for iOS will make a return.

The developer says that the reason he took down the game is because people were getting too hung up on it. Although, many believe that he may have been threatened with legal action by Nintendo. We may never know the truth, but we do know that this game’s popularity is crazy. Many people are waiting for its return.

Nguyen hinted that Flappy Bird could be coming back in an interview earlier this month. Yesterday, when he was asked on Twitter if he had any plans of putting the game back on the App Store, he replied yes, “but not soon.” It seems like he has finally made up his mind. I bet gamers can not wait to play it again.

Source Ubergizmo

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more