Washington: What does your nose know? A lot more than you might expect.

Scientists studying the breadth of people's sense of smell said on Thursday the human nose can discern far more than the 10,000 different odours long cited as the outer limit of our olfactory abilities.

Andreas Keller in the lab surrounded by vials of odours he and his colleagues used to measure volunteers' ability to distinguish between scents. Credit:Reuters

They concluded that the human nose can differentiate an almost infinite number of smells - more than a trillion - based on their extrapolation of findings in laboratory experiments in which volunteers sniffed a large collection of odour mixtures.

"The single most important contribution of this research is that it revises this current idea that humans are terrible smellers," said Leslie Vosshall, who heads the Laboratory of Neurogenetics and Behaviour at Rockefeller University in New York that conducted the study published in the journal Science.