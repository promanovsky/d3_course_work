Rita Ora has admitted working on the Fifty Shades Of Grey movie was hard

Rita Ora has admitted she found her first major acting role in the Fifty Shades Of Grey movie "incredibly difficult" because she was so nervous.

The 23-year-old British singer plays Christian Grey's sister in the big screen adaptation of EL James' hit erotic novel, starring Jamie Dornan as deviant billionaire Christian and Dakota Johnson as Anastasia Steele, the young student he seduces.

Rita confessed to Access Hollywood she was "a bit stressed out" by the auditioning process, and then struggled to master the American accent and remember her lines because she was so anxious.

She revealed: "It was incredibly difficult. I had to have someone in my ear telling me what to say before I said it because I was honestly so nervous I forgot everything that I had learned."

But she revealed actress Dakota, 24, who is the daughter of Golden Globe-winning actor Don Johnson and Oscar-nominated actress Melanie Griffith, took her under her wing on set.

Rita said: "She's a great friend of mine and she really eased me. She was really nice to take care of me."

The Hot Right Now singer - who had a brief cameo in Fast & Furious 6 - had eight call backs before producers cast her as Mia Grey, her debut acting role.

She said: "Me being new to the world of auditioning, for actors this may be a reoccurring thing, but I didn't realise there were so many call backs.

"I was a bit stressed out during the audition process, but when I got that phone call I was so happy. I couldn't wait to tell everybody."

Rita is not allowed to reveal too much about the film, directed by Sam Taylor-Johnson, which has just unveiled its first trailer.

But having read all the books she said: "Without spilling too much, because I'll get a call going, 'What are you doing?!' the movie is as close as it can visually be. I think the actors are phenomenal in this and I'm just really honoured to be on the same kind of wavelength as them."

She added: "This being my first movie its pretty awesome. I'm still thinking about what I'm going to wear at the premiere!

"I think I might bring a fan or do something. It would be like a nice little experience for someone to come with me, you know?"