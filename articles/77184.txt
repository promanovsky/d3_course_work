DETROIT (WWJ) – Warmer weather pushed car sales back on track in March. The annual sales rate topping 16 million for the first time this year.

“We now can confirm cold weather was the primary factor holding back car sales at the start at the year,” says Kelley Blue Book analyst Karl Brauer. “The strong March numbers from nearly every automaker indicate ongoing pent-up consumer demand for new vehicles, along with readily available credit, growing consumer confidence and the most competitive marketplace we’ve ever seen. These factors, combined with the welcome spring thaw in the Midwest and Northeast, have people flocking to dealerships and leaving with a new car.”

Nissan and Chrysler were again the big winners. Nissan had its best month ever, with sales up 8 percent. Chrysler, meanwhile had a 13 percent sales increase.

“We are entering the spring selling season on a high note as our Jeep and FIAT brands recorded their best sales months ever and Chrysler Group extended its streak in March to 48-consecutive months of year-over-year sales increases,” said Reid Bigland, Head of U.S. Sales. “Our Ram pickup truck posted its best March sales in 10 years.”

Chrysler’s Jeep brand lead the way with a 47 percent sales increase. Overall, trucks were strong performers for Chrysler, while some of their car based products didn’t do so well. The one exception was Fiat, which was helped by new products.

Ford sales, meanwhile, are up 3 percent.

Ford analyst Erich Merkle says it appears that as the weather warms, people are starting to return to dealerships. March had a very strong close.

“We certainly saw a tremendous amount of strength in the second half,” said Merkle. “Then, the final weekend, the close of business for the month of March. It was our best weekend close in eight years.”

There’s a lot of interest in the GM sales report, as it could give analysts an indication of the impact of all of the negative publicity surrounding the company’s big recall. But, we’re going to have to wait a little longer than normal for that report.

“General Motors Co.’s March U.S. sales, originally scheduled to be released at 9:30 a.m. EDT today, will be delayed several hours due to a computer systems issue that impacted dealer sales reporting,” read a statement issued GM. “The company expects to reports its sales this afternoon before the close of business.”

Toyota sales rose 5 percent.

“Car shoppers generally have an easier time finding deals on Japanese brands in March because it’s the end of the Japanese fiscal year, and those brands step up their incentives to make one last sales push,” said Edmunds.com senior analyst Jessica Caldwell. “Nissan is usually more eager than its competitors to play this game, and according to its March sales report, this year was no different. If you’re a shopper who bought a Nissan last month, there’s a good chance you walked away with an excellent deal.”

In general, analysts expect sales to grow in April and May.

“We’re looking for a very strong spring selling season,” said LMC Automotive Analyst Jeff Schuster. “I think we’ll see the production side of the business on track as well.”

Connect with Jeff Gilbert

Email: jdgilbert@cbs.com

Facebook: facebook.com/carchronicles

Twitter: @jefferygilbert