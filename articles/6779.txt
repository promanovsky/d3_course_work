Justin Bieber’s Miami deposition, in which the singer was videotaped after his lawyers lost an appeal preventing it, has been released.

The extracts have been published by TMZ. While it’s obviously no fun being sued for civil assault by a photographer then questioned by his lawyer for between four and a half to six hours [reports vary] in front of cameras, there are not enough adjectives to describe just how awful this deposition is.

The memes, the GIF’s are coming.

However, to be fair to Bieber, it’s worth noting that on the day of the court hearing, Miami prosecutors released two videos that showed him urinating for a drugs test in a police station after his suspected DUI arrest in January. Bieber’s private parts were blacked out, but it was likely a violating experience to know it was happening while he was being grilled in a court hearing where his money is the jackpot.

Miami-based photographer Jeffrey Binion alleges he was attacked by Bieber’s bodyguards on the singer’s orders while encouraged by rapper pals ‘Lil Za and ‘Lil Twist, after he was spotted taking photos of the singer outside Miami’s Hit Factory recording studio on June 5, 2013.

(Photo: Bieber walks paparazzi gauntlet in London, March 2013.)

Binion claims he was thrown against a wall and held in a choke hold while his camera and memory card were taken by security guards.

Jeffrey is also suing one guard, Hugo Hesny, as well as Bieber. The photog alleges Hesny threatened him and displayed a firearm he is not authorized to carry in the state of Florida.

The lengthy deposition was filmed last Thursday (March 6) at the law offices of Roy Black, who is representing Bieber in the DUI case.

Binion is represented by Mark DiCowden, who questioned Bieber about previous incidents allegedly involving him and his bodyguards, countries he had visited during his Believe tour, names of members of his crew, or whether his bodyguards worked directly for him.

In response, DiCowden received sullen, angry, selectively “forgetful” answers from Bieber, who also reportedly took frequent breaks.

One such question to the pop star, “Do you remember being in Australia ever?” was met with a shrug and this reply, “I don’t know if I’ve been to Australia. Have I been to Australia?”

Tension in the hearing reached boiling point when Bieber was probed over his now reunited with girlfriend Selena Gomez.

“Don’t ask me about her again. Never… don’t ask me about her again,” the 20-year-old star replied in Don Vito Corleone fashion when asked if he had ever talked with Gomez about his feelings toward paparazzi.

(Video: Bieber calls Gomez “my baby” at an acoustic show during the SXSW festival in Austin, Texas, March 9.)

Visibly upset by the question, Bieber was told to leave the room by his Los Angeles attorney Howard Weitzman after they objected on grounds of harassment.

A recess was called and the court hearing resumed 15 minutes later.

(Photo: The singer lunges for a paparazzo who swore at him during his Believe tour, London, March 8, 2013)

TMZ uploaded four clips.

These are labeled “Love Sick Bieber,” “Contentious Bieber,” “Arrogant Bieber” and notably “Disrespectful/ Dumb Bieber” clip, in which the singer unfortunately mixes up “instrumental” with “detrimental” when asked about Usher’s contribution to his music launch.

“I was found on YouTube… I think I was detrimental to my own career,” a confused Bieber replied.

It’s a terrible, terrible moment that surpasses Bieber’s “sixteenth chapel” [instead of Sistine Chapel] debacle on David Letterman’s Late Show in 2012. And we have to say, at this point it’s clear very little attention was paid to Justin’s formal eduction once his career began.

Arguably, that failure falls to Bieber’s parents, his manager Scooter Braun and his mentor Usher, who perhaps should have swapped that “Swagger Coach” budget for a laminated dictionary instead.

And so, to the deposition clips.

Do you think Bieber acted unreasonably in the Miami deposition hearing, or do you think he was under pressure?

Add your comments below.

Clip 1: Bolshy Bieber

Clip 2: Love Sick Bieber

Clip 3: Snarky Bieber

Clip 4: Detrimental v Instrumental Bieber Also Has Choice Words For Court Reporter