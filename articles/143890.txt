Contradictions of human nature are the fodder that playwright Martin McDonagh often mines in his masterfully satirical dark comedies about quirky rural Irish characters.

Now director Michael Grandage has brought the original, mostly Irish cast of his recent sold-out London production of McDonagh's "The Cripple of Inishmaan" to Broadway, with a very talented ensemble featuring Daniel Radcliffe as Billy.

Grandage's lively production, both raucous and tender, opened Sunday night at the Cort Theatre. It's the first Broadway appearance of McDonagh's 1996 tale about the insular denizens of a remote Irish island in the 1930s. Previous New York productions were off-Broadway, in 2008 at the Atlantic Theater and in 1998 at the Public Theater.

A full Potter gold: Daniel Radcliffe delivers a sparkling performance in The Cripple Of Inishmaan

So boring is life for the residents of this small seaside village that a new feud between once-good friends is huge news, and they while away the time playing minor unkind tricks upon one another. A Hollywood movie being scouted on a nearby island soon sends them dreaming.

Radcliffe, who made his Broadway debut in a 2008 production of "Equus" and returned in 2011 to star in the Frank Loesser musical "How to Succeed in Business Without Really Trying," works as an integral part of the cast.

To his generally callous neighbors, teenage Billy is just a target of ridicule, and how he became handicapped is one of the secrets that twist and turn throughout the play. Without showboating his twisted arm and leg, Radcliffe gives Billy a physical frailty and inner toughness combined with yearning that makes him a very sympathetic figure. Billy's desire to escape from the stifling loneliness and tedium of his narrow-minded country village is at the core of the story.

McDonagh ricochets between crass humor, careless cruelty and tender sorrow, all the while poking fun at Irish folklore, toying with stereotypes, and setting his characters up to have their dreams crushed. He suddenly reverses their backstories or presents unseen sides to their personalities that upend what the audience thinks it knows.

Pat Shortt is blustery fun as the town's gossip-monger, who fancies himself the town crier while bartering his news tidbits for food. Far from being the loving son who lives at home with his sainted mother, he's been plying his foul-mouthed, ancient Mammy (a marvelously dour, rubbery-faced June Watson) with liquor for years, in hopes of killing her.

No chamber of secrets: The actor is refreshingly earnest in his portrayal of teenage Irish orphan Billy Claven

Billy's secret crush is Helen (played with gleeful meanness and a perfect touch of insecurity by Sarah Greene). She's a beautiful young redhead who seems unnecessarily cruel. Her sharp-tongued, egg-tossing ways too easily escalate to possible animal brutality. She also provides brittle, anti-Catholic comedy with her casual references to the clergy whose groping she's violently fending off since childhood, at one point boasting, "I ruptured a curate at age 6."

Padraic Delaney seems genuinely kindhearted as a boat owner who helps Billy. Conor MacNeill is quite funny as Helen's tormented, candy-obsessed younger brother Bartley, while Gary Lilburn provides a grounding presence as the town doctor.

Two of the most wonderfully wrought characters are Billy's comical yet brooding aunts who have raised him. Their repetitious, chorus-like banter is given delightful nuance by Gillian Hanna and Ingrid Craigie. Christiopher Oram's rustic, slightly decaying, stone-laden set and simple, drab costumes add to the downtrodden atmosphere.