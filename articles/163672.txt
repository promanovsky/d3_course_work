NEW YORK (TheStreet) -- Amazon (AMZN) - Get Report shares are climbing in early market trading following the announcement that the company has reached a multi-year content licensing agreement with Time Warner (TWX) owned HBO.

Amazon stock is down -0.6% to $327.63 in early market trading on Wednesday.

The deal makes Amazon's Prime Instant Video the exclusive online-only subscription home for HBO programming. The deal includes access to full seasons of all of HBO's shows, with back seasons of current shows slated to be available within three years of airing on HBO.

In addition to the online deal, HBO Go will be available on Amazon's Fire TV platform by the end of the year.



Must Read: Warren Buffett's 10 Favorite Stocks

SELL NOW: If you own any of the 900 stocks that TheStreet Quant Ratings has identified as a 'Sell'...you could potentially lose EVERYTHING in the next 6-12 months.Learn more.

TheStreet Ratings team rates AMAZON.COM INC as a Hold with a ratings score of C. TheStreet Ratings Team has this to say about their recommendation:

"We rate AMAZON.COM INC (AMZN) a HOLD. The primary factors that have impacted our rating are mixed ? some indicating strength, some showing weaknesses, with little evidence to justify the expectation of either a positive or negative performance for this stock relative to most other stocks. The company's strengths can be seen in multiple areas, such as its robust revenue growth, impressive record of earnings per share growth and compelling growth in net income. However, as a counter to these strengths, we also find weaknesses including generally higher debt management risk and poor profit margins."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

The revenue growth came in higher than the industry average of 8.8%. Since the same quarter one year prior, revenues rose by 20.3%. Growth in the company's revenue appears to have helped boost the earnings per share.

AMAZON.COM INC reported significant earnings per share improvement in the most recent quarter compared to the same quarter a year ago. The company has demonstrated a pattern of positive earnings per share growth over the past year. We feel that this trend should continue. This trend suggests that the performance of the business is improving. During the past fiscal year, AMAZON.COM INC turned its bottom line around by earning $0.58 versus -$0.10 in the prior year. This year, the market expects an improvement in earnings ($1.98 versus $0.58).

Compared to where it was a year ago today, the stock is now trading at a higher level, reflecting both the market's overall trend during that period and the fact that the company's earnings growth has been robust. We feel that the combination of its price rise over the last year and its current price-to-earnings ratio relative to its industry tend to reduce its upside potential.

The gross profit margin for AMAZON.COM INC is currently lower than what is desirable, coming in at 30.26%. Regardless of AMZN's low profit margin, it has managed to increase from the same period last year. Despite the mixed results of the gross profit margin, the net profit margin of 0.93% trails the industry average.

AMZN's debt-to-equity ratio of 0.71 is somewhat low overall, but it is high when compared to the industry average, implying that the management of the debt levels should be evaluated further. Despite the fact that AMZN's debt-to-equity ratio is mixed in its results, the company's quick ratio of 0.67 is low and demonstrates weak liquidity.

You can view the full analysis from the report here: AMZN Ratings Report

STOCKS TO BUY: TheStreet's Stocks Under $10 has identified a handful of stocks that can potentially TRIPLE in the next 12-months.Learn more.