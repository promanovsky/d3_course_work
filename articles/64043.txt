Less than a week after an oil spill in the Houston Ship Channel, environmentalists say there has been limited damage to nearby bird sanctuaries, but it is to soon to know whether there will be long-term problems to wildlife.

More than 200 birds have been fouled by oil from the spill, caused by a collision involving a fuel barge and a ship on Saturday, according to Richard Gibbons, conservation director of Houston Audubon. The birds are of a variety of species.

“It’s a terrible event,” Gibbons told the Los Angeles Times on Thursday. “It sure could have been a lot worse.”

But Gibbons and Ileana Peña, his counterpart with Audubon Texas, emphasized that the long-term effects of the spill aren’t known despite effective initial oil containment. “We’re being very watchful,” she said by phone from Sundown Island in Matagorda Bay down the coast from the spill, where booms are keeping the area free of oil, meaning the bird sanctuary seems to have been spared immediate contamination.

Advertisement

The Houston Ship Channel is the nation’s premier petrochemical waterway. On Saturday a barge collided with another vessel and about 168,000 gallons of oil spilled from the barge into the waters of Galveston Bay. The channel was immediately closed. It partially reopened days later and is now fully operational.

Officials, including the Coast Guard, moved quickly to protect environmentally sensitive areas such as the Bolivar Flats Shorebird Sanctuary, operated by Gibbons’ group, and about a mile from the collision. Authorities placed more than 70,000 feet of boom designed to protect specific areas from being fouled. Some of the spill moved into the Gulf of Mexico.

“What is happening now is most of the cleanup, the initial response, is finishing up,” Gibbons said. “A lot of oil has left the bay and gone out the shipping channel into the gulf.”

Gibbons described Bolivar Flats as similar to a convenience store stop-over for migrating birds. It is an area of salt marshes, mud flats and beach that attracts hundreds of thousands of birds that stop to feed, rest and nest.

Advertisement

It has been difficult for the group to determine an exact number or specific conditions of oiled birds because the birds remain mobile, flying in and out. The birds were most likely fouled by the Saturday spill, because no oiled birds have been sighted in the previous 10 months, he said.

If the group finds birds that are sluggish and they can be reached for treatment, it is reported to state officials, Gibbons said. State officials have estimated that they have dealt with fewer than two dozen birds that have been captured and perhaps half as many that have died.

“The response has been great,” Gibbons said. “The short-term impact is limited but the long-term impact is of more concern.”

The contamination is hard to trace. For example, birds clean themselves by using their mouths on feathers that may be fouled with oil. The birds may be ingesting some of the oil, potentially causing later problems.

Advertisement

Audubon Texas has been managing the bird sanctuary on Sundown Island, more than 100 miles away. Peña has been surveying the islands and agreed that the immediate response has been good.

There are 13,000 breeding pair of various birds in the sanctuary, Peña said. A large boom is protecting the area of about 674 acres and, so far, no oil has stained the boom, she said.

“They have done a great job of booming the island,” she said. “It’s clean, birds looking great and nesting.”

But birds in the area are in their breeding season and it is too soon to know if the oil will affect their food resources and eventually work its way into the food chain, she said.

Advertisement

“It’s hard to know what will be the impact on this year’s chicks,” she said.

ALSO:

Mudslide community copes with uncertainty

Number of Washington state mudslide missing drops to 90

Advertisement

Abu Ghaith verdict shows lingering impact of Sept. 11 to New Yorkers





