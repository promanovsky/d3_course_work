If at first you don't succeed (against your large smartphone rival), try again? Apple seems somewhat displeased by the $119.6 million decision that went in its favor earlier this month. Or, at least, perhaps Apple feels as if the decision wasn't enough given a jury's finding that Samsung infringed three (of five) patents that Apple brought to trial in U.S. District Court.

Consequently, Apple has filed for a permanent injunction against Samsung, claiming that the sale of Samsung's products  found to be infringing Apple's patents  continues to harm Apple. And it's the kind of harm that money isn't going to be able to fix, Apple alleges.

If successful, the move would prevent Samsung from selling nine smartphones and tablets in total, though the company's flagship devices  the Galaxy S4 and Galaxy S5  don't make the list. Additionally, the move is a wee bit of an about-face from Apple's statements immediately following the $119.6 million decision:

"We are grateful to the jury and the court for their service. Today's ruling reinforces what courts around the world have already found: That Samsung willfully stole our ideas and copied our products. We are fighting to defend the hard work that goes into beloved products like the iPhone, which our employees devote their lives to designing and delivering for our customers."

Apple is also asking for a retrial for the damages portion of the case  perhaps due to the fact that the $119.6 million award was quite a bit less than the full $2.2 billion figure that Apple was looking to get.

"Samsung's improper and prejudicial statements to the jury warrant a new trial on infringement for the '414 and '959 patents (in the event that the Court does not grant JMOL of infringement), a new trial on willfulness for all patents other than the '721 patent (and also other than the '647 patent if the Court grants JMOL of willfulness for that patent), and a new trial on damages for all five of Apple's asserted patents," reads Apple's filling.

Intellectual property analyst Florian Mueller, of FOSS Patents, doesn't believe that Apple's claims of prejudice are strong enough to warrant a retrial. It's more likely that U.S. District court Judge Lucy Koh might "make some minor adjustments" to the verdict, but any squabbling about a retrial will have to be dealt with if Apple or Samsung decide to take the ruling to the Federal Circuit Court of Appeals.

Samsung, like Apple, also filed a judgment as a matter of law on Friday. However, these documents remain sealed for the moment.

Further Reading

Computers & Electronic Reviews