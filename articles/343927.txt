Whether obstetricians-gynecologists change their practice based on this new recommendation remains to be seen: Their own medical group, the American College of Obstetricians and Gynecologists, stated that it stands by its own guideline issued last year, which recommends an annual pelvic exam for women 21 years of age or older.

New advice issued on Monday from the American College of Physicians urged doctors to stop performing routine pelvic exams in healthy women who aren’t pregnant and have no troubling symptoms to warrant the exam. The new guideline published in the Annals of Internal Medicine was based on a review of 52 studies that found no evidence that pelvic exams helped detect serious health problems like ovarian cancer, uterine cancer, or vaginal infections early enough to save a woman’s life or preserve her fertility.

Advertisement

I asked two gynecologists this question. Do you agree with the new guideline recommending against pelvic exams? Here are edited excerpts from my interviews with them.

YES

Dr. Daniela Carusi, director of general gynecology at Brigham and Women’s Hospital

The question of whether we should abandon annual pelvic exams in women who have no gynecological symptoms has been around for a few years — ever since gynecologists were told to stop doing Pap smears every year in women with normal test results and to instead do them every three to five years.

Annual pelvic exams feel like a core part of what we do as gynecologists even if no one can justify why we’re doing them. Some gynecologists feel that patients come in expecting to have a pelvic exam. Even the annual breast exams we perform have not been proven to save women’s lives by catching deadly cancers earlier.

I’ve started giving my patients a choice. I explain to them that annual Pap smears are no longer necessary and that while ACOG recommends that women still have pelvic exams, there’s no evidence for any benefits to performing them if they have no symptoms.

Advertisement

Just like with mammograms, we need to explain to patients the benefits and the risks of the pelvic exam — which is also a screening test — and allow women to participate in shared decision making.

The authors of the new guideline pointed out that pelvic exams can cause anxiety and distress in some women, perhaps making some reluctant to see their doctors. It can also lead to unnecessary testing like ultrasounds or perhaps even a biopsy if doctors feel something that needs to be further investigated.

Some of my patients tell me that they still want a pelvic exam, and I will perform it. Others tell me they’re happy to skip it.

The important thing to emphasize here is that the guideline only applies to women who don’t have symptoms, which means doctors need to educate women on what warning signs they need to discuss during their visit. A pelvic exam is likely warranted if new symptoms suddenly arise like vaginal soreness or itching, a change in vaginal discharge, bleeding irregularities, pain during sex, unusual bloating or pelvic pain — to name a few. I want to know about any gynecological symptom that’s new and hasn’t gone away immediately.

NO

Dr. Jill Rabin, head of urogynecology at Long Island Jewish Medical Center/North Shore-LIJ Medical Group

I agree with the American College of Physicians that studies are lacking on the benefits of pelvic exams and we need more research on this. But I still think annual pelvic exams are important for women, even those without any symptoms.

Advertisement

In my own personal experience through several decades of practice, I have found a lot of abnormalities on pelvic exams in women who didn’t tell me they were having symptoms. For example, I’ve detected uterine prolapse [where the uterus drops into the vagina as a result of weak pelvic floor muscles] that is severely affecting bladder function in patients who never told me they were experiencing urinary incontinence.

Sometimes patients are too embarrassed to discuss these symptoms even when a doctor asks about them. We can use the pelvic exam to build trust and help patients feel comfortable enough to discuss what they’ve been experiencing. There’s an art to touching and examining patients to open a relationship and help ease any anxiety or distress they may feel about having a pelvic exam.

I’m also concerned that patients aren’t going to come in for annual well visits if we stop performing physical exams. We could miss detecting silent sexually-transmitted diseases like chlamydia until it’s too late to prevent infertility. We might also miss providing women with a preconception visit in order to prescribe them prenatal vitamins and assess their health status before they become pregnant.

For many young women, their ob-gyn serves as a primary care provider who fulfills a variety of health needs like monitoring their weight and blood pressure. My philosophy is to treat the whole patient, not just certain parts.

Advertisement

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.