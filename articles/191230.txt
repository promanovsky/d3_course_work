A cup of coffee a day could keep eye problems away, a new research shows.

For the study, South Korean and American researchers examined a particular antioxidant found in coffee. It is known as chlorogenic acid (CLA).

Raw coffee is composed of one percent caffeine and seven to nine percent CLA. In order to test the effect of this powerful antioxidant, the researchers administered two groups of mice with nitric oxide. The first group had been pretreated with CLA and the second group acted as the control and received no treatment.

Nitric oxide activates oxidative stress and free radicals that can cause retinal degeneration. The retina is located at the back of the eye. It is made up of a very thin tissue that takes in and organizes visual data. The retina flourishes on high levels of oxygen, because of which oxidative stress can cause tissue damage that impairs sight.

The researchers found that the CLA-treated mice had no evidence of retinal damage. The researchers are unsure whether or not drinking coffee will have the same protective effects on the eye. They believe that further studies could focus on creating a specific brew of coffee for eye health or developing eye drops that can deliver CLA safely.

"(The study is) important in understanding functional foods, that is, natural foods that provide beneficial health effects," Chang Lee, professor of food science and the study's senior author, said reported by USA Today. "Coffee is the most popular drink in the world, and we are understanding what benefit we can get from that."

The study, 'Chlorogenic Acid and Coffee Prevent Hypoxia-Induced Retinal Degeneration,' was published in the Journal of Agricultural and Food Chemistry.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.