Jonah Hill has apologized for hurling a homophobic slur at a paparazzo who was hounding “The Wolf of Wall Street” star over the weekend.

Video shows the 30-year-old “22 Jump Street” actor, out walking with friends in Los Angeles’ Larchmont neighborhood, being followed by videographers while a running commentary is made about such things as his flashy floral shorts.

When one of the trailing voices finally signs off with “have a good day, enjoy,” an agitated Hill slings an explicit command, followed by a derogatory term for gay people, according to video footage obtained by TMZ. (Warning: Link contains explicit content.)

On Tuesday, Hill appeared on “The Howard Stern Show” and apologized for his remarks, calling the F-word he used “a disgusting word.”

Advertisement

“This is a heartbreaking situation for me,” an upset Hill said outright. “From the day I was born and publicly, I’ve been a gay rights activist.”

The “Moneyball” star went on to give the situation some context beyond what was released in the short video.

“Not excusing what I said in any way, this person had been following me around all day. Had been saying hurtful things about my family, really hurtful thing about me personally, and I played into exactly what he wanted and lost my cool,” Hill said.

“And in that moment I said a disgusting word that does not at all reflect how I feel about any group of people.”

Advertisement

Hill said he grew up with gay family members and that one of his coworkers -- who is also a best friend -- is gay and that Hill is going to stand at his wedding.

“I am not at all defending my choice of words, but I am happy to be the poster boy for thinking about what you say and how those words -- even if you don’t intend them in how they mean -- they are rooted in hate and that’s ... and I shouldn’t have said that.”

He and Stern commiserated about the harassment that celebrities get from shutterbugs, leading Hill to say that he’s not the most diplomatic public figure.

“After hours and hours of it, look, I think I’m pretty good at making movies. I am not good at being a famous person. I’m just not,” he said. “There are some people who are meant for it. [But] if you call me ugly, if you call family members of mine drug addicts and maniacs, I am eventually going to lose my cool. Now what I said in that moment was disgusting and a hurtful term, but I in no way intended it as a derogatory term towards anybody in the LGBT community.”

Advertisement

The actor said that he doesn’t use the word in his vernacular and personal life.

Incidentally, he believes he should have said “either nothing” or hurled a different F-word at the paparazzo.

“I’m happy to take the heat for using this disgusting word. What I won’t allow is for anyone -- it would break my heart to think that anyone would think, especially with all the work that I’ve done and all the loved ones that I have -- that I would be against anyone for their sexuality. That’s absurd to me. So it’s been a heartbreaking morning.”