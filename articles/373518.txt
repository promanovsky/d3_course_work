Rowling has remained mum, but publication of the 1,500-word story on her website, Pottermore.com, offering a glimpse of Potter as a father with “threads of silver” in his hair, has been met with speculation she is seeking some kind of commercial advantage for present and future Potter-inspired ventures.

But with the surprise release Tuesday of a new chapter in the wildly popular saga of the boy wizard and his friends, author J. K. Rowling’s fans may wonder what has caused the old crew to reappear suddenly, as if by magic.

Advertisement

Millions, though, probably will be pleased just to get a chance to catch up.

The new piece is presented in the style of a gossip column with the headline “Dumbledore’s Army Reunites at Quidditch World Cup Final.’’ Its author is Daily Prophet gossip columnist Rita Skeeter, a minor character in the Potter books, who casts a snarky eye on the group — and most pointedly on Harry, a Hogwarts-bred celebrity whose friends and family members, she writes, reap “the benefits and must pay the penalty of public interest.”

Readers encounter a Harry as a freshly scarred figure, nearly 34 years old, married, and the father of two young sons. He is attending the 2014 Quidditch World Cup Finals with his wife, Ginny, and a coterie of close friends, including Ron Weasley and his wife, the former Hermione Granger.

The chapter, free on her site, is the latest in a series of Quidditch World Cup stories launched by Rowling back in March, but the first to reintroduce several major characters from the seven Potter books. The last full-fledged novel, “Harry Potter and the Deathly Hallows,” was published in 2007, and the last Potter film, based on the book, came four years later. Rowling has said there would be no further novels featuring the boy wizard.

Advertisement

Perhaps Tuesday’s release means she had reconsidered? The closing line of Skeeter’s mock column promises a forthcoming biography titled “Dumbledore’s Army: The Dark Side of Demob,” allegedly to be published July 31, which happens to be Harry’s birthday. Might Rowling have a similar book and timetable up her writerly sleeve?

“There are no plans to publish a book,” affirmed Howard Polskin, United States representative for Pottermore.com, in an e-mail to the Globe. He declined to elaborate in a follow-up phone call.

Another possibility? Rowling is anticipating a film adaptation of her separate “Fantastic Beasts and Where to Find Them” series — the first in the expected trilogy has a 2016 release date — and by bringing back Harry and the rest for a World Cup-inspired cameo, she’s drawing future movie audiences into her wizardly orbit once again.

Also, Universal Studios Orlando theme park in Florida debuted a new attraction Tuesday, “The Wizarding World of Harry Potter — Diagon Alley.”

“Is she trying to get people back in?’’ asks Allison Pottern Hoch, a freelance writer and former children’s events coordinator for the Wellesley Booksmith, who remains a Potter fan. “I wouldn’t rule that out, but it’s really all speculation.”

What’s indisputable, says Hoch, is that Rowling, who has branched out into literary and suspense fiction in recent years, is continually finding new ways to connect with fans, through her website, social media, and other tools.

Advertisement

Using a faux gossip column to make sport of the Potter phenomenon is “pretty cool,” according to Hoch. For one thing, she says, it plays with all those what-if scenarios the books raised in readers’ minds. For another, it suggests that Rowling entertained a variety of ideas about her characters and plot developments that never made it onto the page. For whatever reason, “I think she’s trying to get fans riled up,” Hoch says.

One diehard fan, Paul DeGeorge, cofounder of the locally based rock group Harry and the Potters, read the story Tuesday and loved it, especially what he calls the “deep and goofy references” to more obscure parts of the Potter novels. “It’s such a nod to how people continue to engage with these books,” DeGeorge said yesterday from Salt lake City, where his band is on tour. Also, he added, “a cool recognition of the culture that exists within the fandom. A meta-commentary on how Harry Potter became such a celebrity, a cultural icon.”

Roger Sutton, editor in chief of The Horn Book and an authority on books for children and young adults, says he was more bothered when Rowling “outed’’ Dumbledore as a gay man than he is about Rowling projecting her characters into a future of marriage, jobs, and kids. In Dumbledore’s case, said Sutton, Rowling was “trying to impose a point of view that wasn’t in the story” as written. By contrast, ‘this is more of a summer lark,” he added. “There’s bound to be some risk, though. Some fans will probably be furious, because they’re obsessive and will wonder, ‘How does this change Harry Potter’s universe?’ ”

Advertisement

For the better, says Emeline Walker of Hull, a fashion blogger and saleswoman who has devoured all the books and movies and even attended Harry Potter-themed conventions.

“My initial reaction is, ‘This is awesome. I never wanted it to end,’ ” Walker said Tuesday. “We’ve all speculated about the future, but it’s cool to see what J.K. Rowling imagined.”

Her only disappointment, she said, was that more of the Potter novels’ minor characters did not pop up in Skeeter’s gossip column, too.

Related stories:

•Opinion: J.K. Rowling’s new story is creative media criticism

•Harry Potter returns in new short story by J.K. Rowling

•Book review: ‘The Silkworm’ by Robert Galbraith

•Universal’s new Harry Potter park: 1 ride, 7 shops

Joseph P. Kahn can be reached at jkahn@globe.com.