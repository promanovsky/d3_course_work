After dodging cameras for weeks, Rob Kardashian is back in the spotlight. The reality star, 27, hit Los Angeles International Airport this week with his mother, Kris Jenner.

The pair is en route to Europe for Kim Kardashian's upcoming nuptials to Kanye West. Click here for more recent photos of the Kardashian family.

Rob, who has been struggling with his weight, prioritized comfort for his Los Angeles-departing flight. He wore a black T-shirt and black pants, accessorizing with a blue baseball cap and neon and white Nike sneakers.

Jenner, 58, also went all-black for the international travel. She kicked her dark ensemble up a notch with black shades and black Alaia heels.

The one son of the Kardashian clan has been quietly staying out of the spotlight in recent months. There have been various reports that he enrolled in a weight loss program, and he expressed internal struggles over Twitter last month when he tweeted, "No one will ever understand how much it hurts."

He later deleted the tweet and has largely stayed off social media since.

Sources told Us in March that Rob has been doing daily workouts with sister Khloe Kardashian's trainer, Gunnar Peterson, for months. However, he has yet to drop the weight. An insider told Us, "Doctors think it could be a thyroid problem," but noted that the troubled star also "binge eats."

For more photos of Rob, head over to X17online.