FLIES appear to “think” before they act and, like humans, take longer to make trickier decisions, a study has found.

Scientists admitted to being surprised by the discovery, which indicates that even insects show signs of intelligence.

Gathering information before deciding on a course of action was previously thought to be the preserve of highly evolved species, such as monkeys and humans.

In a series of tests, the researchers required that fruit flies distinguish between ever closer concentrations of an odour they were trained to avoid.

When the concentrations were very different and easy to tell apart, the flies acted quickly to move to the end of a chamber furthest away from the strongest smell.

But when they were very close and difficult to distinguish, the flies took much longer to make a decision and made more mistakes.

Instead of responding impulsively, they seemed to accumulate information, weighing up what their smell sense was telling them before committing to a choice.

Professor Gero Miesenbock, from Oxford University’s Centre for Neural Circuits and Behaviour, said freedom of action from automatic impulses is considered a hallmark of cognition or intelligence.

“What our findings show is that fruit flies have a surprising mental capacity that has previously been unrecognised,” he said.

The researchers, whose findings appear in the journal S cience, showed that a gene called FoxP was involved in the decision-making process in the fly’s brain.

The gene was active in a small set of around 200 nerve cells.

Lead author Dr Shamik DasGupta, also from Oxford University, said before a decision is made, brain circuits collect information like a bucket collects water.

“Once the accumulated information has risen to a certain level, the decision is triggered,” DasGupta said.

“When FoxP is defective, either the flow of information into the bucket is reduced to a trickle, or the bucket has sprung a leak.”

Flies with a mutation in FoxP took longer than normal flies to make decisions when odours were difficult to distinguish.

Like a human paralysed by a difficult choice, they became indecisive.

Fruit flies have one FoxP gene, while humans have four related FoxP genes.