Another web company has announced its plans to go public.

Box Inc., a company that provides cloud storage and content management to business users, filed paperwork Monday for an initial public offering. The company, while promising, has yet to see a profitable year out of the last three. According to released financials, Box lost $168.6 million in its 2014 fiscal year on gross revenue of $124.2 million.

Box has worked on a freemium model. Most of the company's users get unpaid access to its content. A small percentage of users pay for the content, and that revenue must cover the cost of all operations plus, eventually, profit. According to Box's SEC files, it has 25 million registered users who, among other things, represent 99 percent of the Fortune 500. However, of the more than 34,000 paying organizations include only 40 percent of Fortune 500 companies.

Such a strategy puts a company like Box in a difficult position. It wants to expand and develop compelling new services, and yet its sales are low compared to its ambitions. Box has had $414 million in investment, which has given it some freedom to grow and develop its business model.

According to the company's filing, Box has an accumulated deficit of $361.2 million that reflects "the substantial investments we made to acquire new customers and develop our services." In addition, the company plans continued expansion and does not expect to be profitable "for the foreseeable future."

It might seem to be like an early Groupon (GRPN) in its need for high marketing expenses, but Box already has a presence in virtually all the Fortune 500. There are tens of thousands of middle-sized companies -- those between roughly $10 million and $1 billion in annual revenue -- that might need collaborative software, but the cost of addressing that market is high simply because it is so diverse.

Box had $109 million in cash and cash equivalents at the end of January with another $44 million in working capital. An IPO is necessary to bring in the capital needed for long-term viability, whether or not the company has reached profitability. Although the paperwork mentions a $250 million aggregate offering price, that is more a formality than final number.

Investors should notice that Box has some significant competition even now -- Dropbox, Microsoft (MSFT), Google (GOOG), IBM (IBM), and EMC, to mention a few.