Health authorities have played down an Ebola scare at the Commonwealth Games after it emerged a Sierra Leone athlete was tested for the killer virus.

Road cyclist Moses Sesay, 32, was admitted to a Glasgow hospital last week after feeling unwell and doctors tested him for various conditions, including Ebola - which is blamed for 729 deaths in an outbreak in four west African countries.

But Sesay, whose homeland has declared a public health emergency, was given the all-clear and released from hospital in time to compete in the men's individual time trial at the Games yesterday.

He told the Daily Mirror: "I was admitted for four days and they tested me for Ebola. It came back negative but they did it again and this time sent it to London, where it was also negative."

A spokesman for Glasgow 2014 said: "There is no Ebola in the Athletes Village of the Glasgow 2014 Commonwealth Games.

"We can confirm an athlete was tested for a number of things when he fell ill last week, including Ebola. The tests were negative and the athlete competed in his event on Thursday.

"We are dismayed by some of the sensational and misleading headlines to date."

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

A spokeswoman for Health Protection Scotland added it was an isolated incident and no other athletes were tested. "No one has tested positive for Ebola in Scotland," she said.

Meanwhile, Glasgow is gearing up to welcome celebrity diving star Tom Daley as the Commonwealth Games sporting action continues today.

The world's fastest man Usain Bolt is also on a mission to win back Glaswegian hearts when he finally takes to the Hampden Park track for the heats of the 4 x 100 metres.

Daley, 20, a mentor in reality TV show Splash!, has dismissed claims that he is to retire after Glasgow 2014, insisting he is ready to go on as "long my body holds out".

Teammate James Denny insists fun and not medals will be his number one priority when he lines up with Daley in the 10m synchronised platform final.

The Team England duo have had just seven days of training after only deciding to enter the competition last week.

But Denny - who finished sixth in Thursday's 3m individual springboard final - is not getting his hopes high, despite the fact he is lining up alongside the defending champion from Delhi.

Denny said: "We only trained for the first time last Friday. I qualified for the 3m final then Tom and I decided we would have some fun and go for the 10m final.

"We have only done a little bit of training so it will be fun just trying it out and we will see how it goes in competition."

Daley, who won Olympic bronze at London 2012, has vowed to continue diving until the Rio Games in two years' time at least.

But for Denny, Friday's final will be a new experience as he is thrown firmly into the limelight alongside the Team England poster boy.

"I'm not thinking about all that," he said. "I'm just concentrating on my own diving."

Daley, who announced he was in a same-sex relationship in December, has been held up as an example of gay rights at the Games which features Commonwealth countries with poor LGBT - lesbian, gay, bisexual, and transgender - rights records.

Bolt described the Games as "awesome" on Wednesday, having earlier taken to Twitter to deny a report in The Times which quoted him, speaking on Tuesday, saying they were "a bit s***" and he was "not really" having fun in Scotland.

Today is set to be the six-time Olympic champion's Commonwealth Games debut after he missed the event in Melbourne in 2006 through injury and opted to skip the 2010 Games in Delhi.

Rumours persist over whether the 27 year old will actually run, even though he confirmed on his arrival in Glasgow on Saturday that he would "definitely" do so and would go in the heats as well as Saturday's final because he needs the race practice after not competing all year due to a foot injury.