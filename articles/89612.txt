Twitter was originally created to help people feel more connected. The irony? Actively using the service may spell trouble for users' actual relationships.

New research from the University of Missouri found that active Twitter users are more likely to experience Twitter-related conflict in their romantic relationships, which in turn leads to other relationship issues like emotional or physical cheating, breakup, and even divorce.

See also: 25 Twitter Accounts to Make You Laugh

The study, which was conducted by Russell Clayton, a doctoral student at the University of Missouri School of Journalism, found a positive correlation between Twitter use and relationship woes. Clayton surveyed 581 Twitter users over the age of 18, asking questions like, "How often do you log in into Twitter?" and "How often do you have an argument with your signiﬁcant other as a result of excessive Twitter use?"

Clayton based his study on a previous, similar study that looked at relationship issues associated with Facebook use and how it leads to jealousy.

"Based on the ﬁndings from both studies, Twitter and Facebook use can have damaging effects on romantic relationships," Clayton concluded. "That is, when SNS [social networking site] use becomes problematic in one’s romantic relationship, risk of negative relationship outcomes may follow."

These types of relationships issues occur regardless of how long couples have been together, says Clayton. There are, of course, simple ways to avoid Twitter-related conflict. Couples could operate shared social media accounts, or simply cut back on their daily Twitter use, he said.