The blood test that predicts Alzheimer's

Share this article: Share Tweet Share Share Share Email Share

Paris - Researchers in the United States say they have developed a prototype blood test that can tell with 90-percent accuracy whether a healthy person will develop Alzheimer's disease within three years. The test looks for 10 signatures of fatty proteins called lipids, according to a study published in the journal Nature Medicine. It could help families of people developing the cognitive disorder make early decisions on how best to care for them and may also aid the search for treatment, the authors said. Several years of clinical trials are likely to be needed to assess the prototype technique, the first blood “biomarker” to predict the tragic degenerative disease. Alzheimer's, caused by toxic proteins that destroy brain cells, is a currently incurable and fatal degenerative disease.

Around 35 million people have the disease, a tally that is expected to reach 115 million people by 2050, according to the World Health Organisation.

“Our novel blood test offers the potential to identify people at risk for progressive cognitive decline and can change how patients, their families and treating physicians plan for and manage the disorder,” said Howard Federoff, a professor of neurology at Georgetown University Medical Center in Washington.

It could also help efforts to treat the disease, he said in a press release.

Attempts to develop drugs for Alzheimer's have failed possibly because they are tested when the disease has progressed too far, Federoff said.

These treatments may have a better chance of braking or reversing the disease if they are trialled at a much earlier stage, he said.

The researchers started by taking blood samples from 525 healthy volunteers aged 70 and older.

Three years later, they looked at a group of 53 volunteers who had developed symptoms of early Alzheimer's or a memory-affecting condition known as amnestic mild cognitive impairment (aMCI).

The blood samples from this group were compared against the samples from 53 otherwise healthy volunteers to see what the difference was.

From this, the scientists spotted the 10 telltale lipid proteins, which appear to be metabolised residues of brain cell membranes. - Sapa-AFP