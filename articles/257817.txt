Former child star Macaulay Culkin stormed off the stage during a gig in Manchester with band The Pizza Underground amid scenes of chaos.

The Home Alone actor was reportedly pelted with plastic glasses during the performance at the venue Zoo on Friday night.

But the troubled star returned to finish the set with his New York-based comedy rock band, who parody songs by The Velvet Underground with pizza-themed lyrics.

Culkin (33), who admitted drugs possession in 2004, sings in the band and plays the kazoo –which the former movie star was playing when he was jeered.

"It was the weirdest thing I've ever watched. It was just bizarre," Alessandro Lima told the Manchester Evening News.

Belfast Telegraph