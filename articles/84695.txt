This competition is now closed. The winner of the Galaxy S5 phone is Anne O’Callaghan.

The winners of tickets to the Dublin launch of the Galaxy S5 are: Stephen Brennan, Liliana Marcean, Niamh White, Lucy Smith and Andrew Wilson.

The winners of tickets to the Cork launch of the Galaxy S5 are: Carmel Nolan, Joe Booth, Tony Redmond, Deirdre Hanley and Sean Minihan.

The long awaited Samsung Galaxy S5 is almost here and Carphone Warehouse want you to be the first to own it!

To celebrate the arrival of Samsung’s latest flagship, Carphone Warehouse are hosting celebrities, tech experts and lucky competition winners in a special ‘Power of 5’ event in Dublin and Cork on Thursday April 10.

It will be Ireland’s first chance to check out the most anticipated smartphone of 2014. Both Clery's, Dublin and the Patrick Street, Cork stores will host an exclusive Power of 5 event from 10.30pm to midnight on April 10 ahead of the official launch on April 11.

The Galaxy S5 is designed for what matters to most people, providing simple and powerful benefits in everyday life - top drawer specs and new features like HD slow motion video, fingerprint scanner and weather proof features make this the handset to beat in 2014!

To celebrate the Carphone Warehouse Samsung Galaxy S5-Power of 5 Event, we have a Samsung Galaxy S5 up for grabs and 5 pairs of tickets to the exclusive event on Thursday, April 10 in Cork and Dublin!

If you don't win tickets or can't make either locations for the midnight selling, have no fear… there are big celebrations happening in Carphone Warehouse on Friday April 11 in Mahon Point - Cork, Crescent Shopping Centre - Limerick and Swords - Dublin.

Terms and conditions apply. General RTÉ Competition rules apply, a copy of which is viewable here. This competition will close on April 9 2014. One entry per person. Winners will be contacted by phone. There is no cash alternative and decision is final. Prize is non-transferrable.

Connect with Carphone Warehouse:

www.carphonewarehouse.ie

Facebook.com/CarphonewarehouseIreland

Twitter.com/Carphoneie