It's 1969 in Mad Men, and it’s a brave new world. The computer revolution is growing alongside the Free Love counterculture movement, and The Monolith (an episode title likely referencing both 2001: A Space Odyssey and computer processing lingo) traces these two seemingly antithetical historical moments through the eyes of Don and Roger, respectively.

A computer has arrived at SC&P, and in 1969, that entails noisy construction, ceremonial discussion of the computer’s benefits, and a whole lot of fear of the digital unknown. (Oh, and hard hats — comical, given today’s notion of what a computer is.)

SEE ALSO: 'Mad Men' Episode 3 Recap: Don's Life With a Chaperone Still Better than No Life

Symbolism is heavy-handed throughout The Monolith (albeit in a self-aware way), as creative is forced to take apart its office space at SC&P in order to accommodate the massive machine. Consequently, Lou’s team of creatives worry that they will be wholly replaced, not just when it comes to their office real estate, but also their integrality at the firm.

No one embodies the “je ne sais quoi” creative finesse of SC&P more than Don, whose own brave new world involves a slew of rules designed to keep him in line following his wobbly return. Don never embraced a data-centric approach to his ad copy, instead opting for an organic, emotionally-driven creative process that showcased his natural talent for the work — in other words, the opposite of the computer being buzzed, hammered and screwed into the office nearby. Don readily displays his suspicion towards such advanced technology, remarking that computers “replace people.”

Don’s talent is virtually castrated in The Monolith as Lou places Peggy in charge of Don for a Burger Chef campaign. Don not only finds himself reporting to his former secretary, but also asked to follow the dull creative process of Lou, who looks for “tags” instead of broader, abstract campaign concepts (not unlike a processing program). Don is none too happy with this, arrogant and defiant. When Bert Cooper rejects Don’s efforts to take computer company Lease Tech on as a client, Don loses it, stealing a bottle of booze from Bert’s office to get obliterated on the job ... so much for those carefully laid out rules.

Meanwhile, Roger learns from his ex-wife Mona that their daughter Margaret has abandoned her husband and son and moved to a commune in upstate New York with a group of hippies. Roger and Mona drive up to fetch Margaret, looking utterly out of place at the commune home. Roger has dabbled in the hippie culture, but from a far more recreational standpoint — for him, drugs and sex are simply another manifestation of his taste for fleeting forms of happiness. For Margaret, however, her immersion in this movement is a cause for serious concern.

For as much as Margaret appears to be embracing a compassionate lifestyle, her bitterness toward her parents — her mother’s past drinking and her father’s absence — still shine through, leading Mona to quickly throw in the towel in her efforts to get Margaret to come home.

Roger, however, decides to stay the night at the commune to warm Margaret up to leaving — he smokes pot with the group, peels fresh fruits and vegetables. He and Margaret turn in for a night of sleep inside a barn, curled up in sleeping bags staring up at the moon, the positions of their bodies and their urge to find companionship in alternative realms strangely connecting them as father and daughter. “I am happy,” Margaret earnestly tells Roger, who seems relieved to hear his daughter say that.

But when Roger finds Margaret slipping away from his side in the middle of the night to have sex with one of the men at the commune, his fatherly instincts kick into high gear. The next morning, he demands that Margaret leave with him, and Margaret unloads all of her pent up aggression toward Roger and her upbringing in front of the group. Roger finally leaves — alone, covered in mud and unable to rescue his daughter from a reality that he perhaps ultimately inflicted upon her.

Completely blitzed, Don phones Freddy from SC&P to head to a Mets ball game. The game never materializes due to Don’s blackout-drunkenness, and the next morning, Freddy acts as the voice of reason for Don, offering him black coffee and tough words that can be best described as, “What the hell are you doing, Don? Get your life together.”

“The Monolith” ends with Don heeding Freddy’s advice, arriving back at SC&P sober and humble. He takes a seat in front of the typewriter he tried to wreck earlier in the episode and begins to write tags for Lou and the Burger Chef campaign, his brain — his talent — focused as he types.

“Human existence is finite,” Lease Tech employee Lloyd says in The Monolith. “But isn’t it God-like that we’ve mastered the infinite?” Both Don and Roger are unable to stave off the onslaught of change, unable to exact that God-like control over situations in their life that once fell under their domain — Don cannot change the new structure at SC&P, and Roger cannot force his daughter away from this new hippie lifestyle. And yet, they try — the grasp for a reality now positioned squarely in the past, and by the end of The Monolith, must accept a level of defeat with humility, whether covered in mud or simply brutally hungover.

The philosophical talk spewed by Lloyd at SC&P and the hippies at the upstate compound both represent groups of people trying to come to terms with advances in technology. For Lloyd and tech gurus, computers are wholly embraced; for the hippies, a lifestyle focused on nature is the new way.

Don and Roger fall somewhere in between on the spectrum, participants in the changes of 1969, be it by working at SC&P’s savvy offices or flirting with drugs and the Free Love movement. But a wariness of change is also present in both men.

By the end of The Monolith, Don and Roger realize they cannot hold back the waters of a new era.