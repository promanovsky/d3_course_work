The Loch Ness Monster is a legend not unlike the Chupacabra or Big Foot crazes. Pseudoscientists and researchers clamor for its existence year-after-year, while their more respected peers disregard any such evidence pointing to its existence. But what if Nessie, as the Loch Ness Monster was so nicknamed in the 1940s, really does exist? There's new hope among cryptozoologists that it does after a curious Apple maps image surfaced (pun intended).

The image depicts a top-down view of the large, freshwater body of water. But the picture gets interesting once you delve just underneath the surface of the water. A mysterious, unidentified shape is present in the picture and it looks like some kind of menace. Some are saying that it depicts Nessie as having two fins that flank alongside a larger, elongated body.

Members of the Official Loch Ness Monster Fan Club are certainly convinced it is after conducting a SIX MONTH STUDY of the grainy image. It's just too bad they didn't put as much effort into updating their 1990s-era website as they did their Iinvestigation."

'We've been looking at it for a long time trying to work out exactly what it is. It looks like a boat wake, but the boat is missing. You can see some boats moored at the shore, but there isn't one here. We've shown it to boat experts and they don't know what it is," club president Gary Campbell told the Daily Mail. "Whatever this is, it is under the water and heading south, so unless there have been secret submarine trials going on in the loch, the size of the object would make it likely to be Nessie."

Deep Sea Biologist Andrew David Thaler is positive it isn't Nessie. In fact, he all but debunked Nessie's existence (at least in this case) in a matter of minutes.

"The accompanying image is a low-resolution satellite image of a boat wake, available, apparently, only on Apple Maps. There's really no deconstruction needed, it's a boat wake," Thaler explained in a post published on his blog.

And while a giant, trunk-like sea monster may not exist, Thaler's reasoning behind why the mysterious object exists in the first place is quite interesting in and of itself.

"Apple doesn't have it's own imaging satellites. The fact that the image only shows up in Apple Maps, not Google, is due to Apple either using a slightly different image set to stitch together a picture of the loch, or has a less robust algorithm for dealing with artifacts," Thaler said.

He further explained that "if it looks like a duck, swims like a duck, and quacks like a duck," it is... a boat.

"Both the boat in the northern picture and the 'ghost boat' in the monster picture are about 20 meters long. There are no 20-meter long catfish. There are no whale sharks in Loch Ness (how would they survive in freshwater?). It's a boat," Thaler said. "If something looks exactly like a boat wake, in a place where there are plenty of boats, when a similar boat can actually be seen in the same region, it's a boat."

Do you believe in monsters, ghosts, or the undead? Feel free to share your superstitions with us and other Latin Post readers in the comments section below.