Mozilla is giving up on metro-mode, touch-centric Firefox browser owing to a lack of interest by testers in the pre-release versions.

Mozilla, an open source software company, announced Friday that it is giving up on a project to build a touch-friendly version of the browser for Windows 8's Metro environment, after two years of extensive hard work. The decision, according to Firefox VP Johnathan Nightingale, is attributed to a lack of user interest, which is described as "pretty flat."

"In the months since, as the team built and tested and refined the product, we've been watching Metro's adoption," Nightingale wrote in a company's official blog, Friday. "From what we can see, it's pretty flat. On any given day we have, for instance, millions of people testing pre-release versions of Firefox desktop, but we've never seen more than 1000 active daily users in the Metro environment."

Mozilla started working on a Metro mode Firefox version in 2012 with the aim of winning more customers. But months after testing the product, Mozilla realized that it was just not picking up. "To ship it without doing that follow up work is not an option. If we release a product, we maintain it through end of life. When I talk about the need to pick our battles, this feels like a bad one to pick: significant investment and low impact," Nightingale explained.

If Mozilla had decided to give it a green signal, Firefox would be sailing in the Windows 8 browser market along with Internet Explorer, which is the only browser offered with the new desktop OS. Besides that, Chrome offers a special mode for the Windows 9 Start Screen, but is more inclined toward its own Chrome OS than Microsoft's UI guidelines.

Given the baggage with the Metro-style Firefox browser, Nightingale decided to pull the plug instead of releasing an inferior product. With this, Mozilla will be able to focus more on projects that are favoured by users. The company recently decided to integrate advertisements into the browser after years of resistance.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.