Comcast Corp.’s first-quarter profit grew 30%, as it continued to add video subscribers and enjoyed strong growth in its NBCUniversal entertainment arm, thanks to the Winter Olympics.

The company added 24,000 video customers in the first quarter, after losing subscribers a year earlier, although most of the increase was due to a change in how Comcast counts its subscribers. Last quarter, Comcast CMCSA, added video subscribers for the first time after 26 straight quarters of decline.

Overall, Comcast reported a first-quarter profit of $1.87 billion, or 71 cents a share, up from $1.44 billion, or 54 cents a share, a year earlier. Excluding gains on sales and acquisition-related items, adjusted per-share profit rose to 68 cents from 51 cents.

Revenue increased 14% to $17.41 billion, although excluding the effect of the Winter Olympics it grew 6.5% to $16.3 billion.

Comcast is seeking to convince regulators in Washington to approve its $45 billion deal to buy Time Warner Cable Inc US:TWC . On Monday, opposition among media companies crystallized when Netflix Inc. said it opposes the merger because of the dominance the combined company would have in the U.S. broadband market. Comcast has argued the merger doesn’t change the current state of pay TV and broadband competition because Time Warner Cable and Comcast’s service areas don’t overlap.

TWC has more than 11 million subscribers; Comcast has roughly 22 million. The company has promised to divest about three million subscribers after the merger, disclosing on Tuesday it was considering “a number of potential structures” for the divestitures, including a sale or a spin-off.

Comcast Chief Financial Officer Michael Angelakis told analysts that “key considerations” for divestitures would include structures that were “most tax efficient” and help “deliver cash for our shareholders,” as well as allowing Comcast to “maximize our presence in our most strategic markets.” He said the company wasn’t working on any particular timeline relating to the divestitures.

Charter Communications Inc., whose own pursuit of TWC was trumped by Comcast, is expected to try to buy the subscribers being divested.

Comcast also has dangled the possibility of offering a new wireless service as it seeks regulatory approval. Such a service would use connections on Comcast’s more than one million Wi-Fi hotspots and would switch to traditional wireless airwaves where Wi-Fi isn’t available.

On a conference call, Chief Executive Brian Roberts said Comcast is studying the wireless market and is “encouraged by it.” With the wireless assets Comcast has, long term “we are in a position to think about where wireless is going and how we can participate in a way to build value and whether that is through our existing products or it’s a new product,” Roberts said.

By adding video subscribers in the past two quarters, Comcast is bucking a trend. In recent years most cable operators have been losing video subscribers to phone and satellite-TV companies.

Write to Shalini Ramachandran at shalini.ramachandran@wsj.com

An expanded version of this article appears at WSJ.com.

MORE MUST-READS FROM MARKETWATCH:

5 dividend stocks with yields of up to 10%

Puerto Rico woos rich with hefty tax breaks

Retire overseas on less than $25,000 a year