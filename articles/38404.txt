Naina Khedekar

The next-gen HTC One known by the codename M8 is all set to be announced today. Through its Twitter account, last month, the company had announced an event to be held on March 25. The “Save the date” tweet was followed by the company sending out invites to several outlets. In the past couple of months, we've seen a string of fluctuating rumours around its specs. With the ‘D-Day’ almost upon us, here’s all we know about the upcoming HTC One.

Name

After the popularity of its Desire series, HTC went through a slump. The brand then reinvented itself with the One series. After putting in so much thought and resources in building the “One” brand, the company surely wouldn’t want to move away from it. Now M8 is the codename of the device. The rumoured HTC One 2 nomenclature also sounds quite silly. If HTC is to name its next flagship One 2, the company would definitely be a butt of jokes in the future. More likely is the option that HTC will go with the same name i.e. One. Noted tipster Evleaks claims the name will be the All New One, which sounds a bit too mouthful to us.

Display

The HTC flagship is expected to feature a 5-inch display, which is bigger than the 4.7-nch display we saw on the HTC One. The display will come with full resolution of 1920 x 1080 pixel resolution. According to a leaked 14-minute hands-on video, the display can be turned on by double tapping, something we've already seen in the LG G2. In addition to this, simply swipe upwards from the bottom to turn on the display. Other accessibility features include, swiping from left to right when the screen is off to bring up the home screen and right to left to bring up Blinkfeed. The display type is most likely to be SuperLCD, as we’ve seen in previous HTC devices. Whether it is a new panel i.e SLCD4 or the fantastic SLCD3 from the HTC One, is as yet unknown.

User interface

The HTC flagship will run on nothing less than the Android 4.4 KitKat version of the OS. Obviously, it will get a layer of the HTC Sense UI on top of it, and all UI refreshments that it brings along. A leaked image had earlier shown the completely revamped HTC Sense 6 UI running on the M8. The UI showed KitKat-style translucent system bars and white icons. Interestingly, the screenshot displayed software navigation keys, indicating that HTC is all set to replace the capacitive buttons seen on its previous phones. In addition, we expect HTC to make changes in the BlinkFeed homescreen functionality, as well as some design changes, when it comes to icons and themes.

Processor

After a round of false rumours about a Snapdragon 600 and 800 SoC, the All New HTC One is likely to come powered by quad-core Snapdragon 801 processor clocked at 2.3GHz, coupled with 2GB of RAM. It was also rumoured to employ the Snapdragon 805. However, it could be too early for that as the Snapdragon 805 will be available later during the first half of the year. It should be noted that most manufacturers are moving towards octa-core processors, we hope HTC doesn’t have a lot of catching up to do this time around as well.

Camera

One of the key features of the HTC One is said to be the dual camera setup on the back. According to a leaked video, the primary or the lower camera on the back has a 4.1-megapixel sensor with large pixel size of 2µm, resulting in the HTC Ultrapixel camera. The second camera on the back – the one on top – measures the distance between various points, which can allow you to shift focus after you have clicked the image, just as you can on Nokia Lumia cameras with the Retouch app. Because the second camera notes the distance between objects, you can also get a cool parallax effect as seen in 3D pictures, but it must be noted that this is not a 3D camera. The dual cameras should allow users to put the focus on the background or foreground, after the image has been taken.

The rest of the camera features appear to be same including a 1/3.0-inch size sensor and f/2.0 aperture. The video capture is said to be 1080p, and not 4K. Users will get live effects and filters inside the native camera app. One can also expect a 5MP front-facing camera, though we are not sure what software enhancements HTC has made for this camera.

Storage

On the storage front, HTC will mostly offer 16GB and 32GB options. Earlier rumours had hinted that HTC will take cues from the HTC One Max and the dual-SIM HTC One with a microSD card slot. The latest leak shows support for up to 128GB cards via the microSD card slots. Going by the latest trend, HTC may bundle in some cloud storage too.

Battery

The All New HTC One is now rumoured to get 2,600 mAh battery, and not 2900 mAh battery that was speculated earlier. The new One will also have an Extreme Power Saving mode, which promises to keep the phone alive for up to 15 hours with just 5 percent battery left. This is similar to the feature in the Samsung Galaxy S5 that offers 24 hours with 10 percent charge. The Extreme Power Saving mode keeps apps such as the Phone, News, Mail, Calendar and Calculator on, and displays them in a minimal interface.

Miscellaneous

The new leaked video showcases some new accessibility features for the All New HTC One like swiping from right to left to bring up Blinkfeed. Blinkfeed has been given additional support from third-party apps such as Facebook, Google+, Instagram, LinkedIn, Twitter and so on. Fitbit is a new addition as well. We’ve seen the device in a few leaks, and it looks quite slick, we must say. The device’s dimensions are said to be 146.36 x 70.6 x 9.35 mm.

There is one more thing that's still up in the air. The fingerprint scanner trend sparked by Apple’s latest iPhone 5s has trickled to other phones, like the upcoming Galaxy S5 and earlier in the 5.7-inch HTC One Max phablet. However, there's no word about the new HTC One having such a scanner. Perhaps HTC will reserve the scanner for the next-gen One Max.

Leaked images point to the HTC One being an all-aluminium device. Evleaks had hinted at grey, silver and gold variants of the device, and we aren’t surprised. On the connectivity front, it should most likely get all the usual suspects and more including 3G, LTE, Wi-Fi ac, Bluetooth, GPS/A-GPS, GLONASS and NFC, as well as an IR port like last year’s One. Read our review of the HTC One and HTC One mini to find out what HTC will be building upon this year.

The new One will be unveiled tomorrow and we will be bringing you all the coverage from the event. HTC has promised a live webcast for the announcement, which will take place at 8:30 PM IST.