If you love Samsung’s big, beautiful displays but don’t want to deal with Samsung’s TouchWiz interface, then this next leak is for you. @evleaks has posted a new picture of the Galaxy S5 Google Play Edition, which will have the same hardware as the Galaxy S5 but that will run on stock Android without any software additions from Samsung. This means that anyone who’s interested in using Samsung’s hardware but who isn’t interested in dealing with Samsung’s UI overlay can buy this phone from the Google Play store off contract.

When it’s released, this will already mark the sixth —yes, sixth — version of the Galaxy S5 that Samsung has put out in 2014. So far this year, Samsung released the original Galaxy S5, the Galaxy S5 Active, the Galaxy S5 LTE-A, the Galaxy S5 mini, and the Galaxy K Zoom, which really is just a variant of the Galaxy S5 with a hugely powerful 21-megapixel camera.

We’re not quite sure when this new device will be released but @evleaks doesn’t typically get these leaked press shots until pretty close to phones’ release dates, so we’ll guess that it will be out sooner rather than later.

Be sure to check out the leaked picture below.