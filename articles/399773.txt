FCC Chairman Threatens to Intervene Over Dodgers Pay TV Standoff

UPDATED: In a letter to Time Warner Cable, Tom Wheeler says he will do whatever it takes to provide relief to consumers, and demands a list of documents.

FCC Chairman Tom Wheeler has written a letter to Time Warner Cable expressing his "strong concern" because Los Angeles Dodgers games still aren't available to about 70 percent of the viewers in Southern California more than halfway through the baseball season.

While the FCC has hesitated to get into similar situations in the past, Wheeler said the agency would "intervene as appropriate" if that is what it takes to bring "relief to consumers."

Wheeler also gave Time Warner Cable 10 days to provide a long list of documents about the impasse and a detailed explanation as to why it has happened.

The letter was issued Tuesday, the same day that Wheeler met with two California Congressmen, Rep. Tony Cardenas and Rep. Henry Waxman.

Wheeler said in his letter he is "encouraged" by Time Warner's agreement to enter binding arbitration with DirecTV to make a deal, but he remains troubled by the "negative impact that your apparent actions are having on consumers and the overall video marketplace."

On Tuesday, Time Warner Cable responded to Wheeler: ""We're grateful for the FCC's intervention and happy to work with them to gain carriage for the Dodgers — that has been our goal all along. We hope that Chairman Wheeler is making similar inquiries of DirecTV and other LA television distributors to determine their rationale for refusing to carry SportsNet LA, which we have offered at terms similar to other regional sports networks, including those owned by DirecTV. We look forward to a productive discussion."

In addition to documents about the negotiations with DirecTV, Wheeler wants documents that relate to the agreement with Guggenheim Partners, owner of the Dodgers, which resulted in the creation of SportsNet LA — including those that govern the sale of advertising.

The list of documents the FCC wants also includes contracts between Time Warner Cable and any of its affiliates; term sheets sent to other distributors seeking to get them to carry the sports network; and any responses received from other multiple system operators with counter offers.

In recent days two different groups of California congresspeople have sent letters to the FCC as well as Time Warner Cable urging a resolution.

A lot of the focus has fallen on the tense talks that have taken place between Time Warner and DirecTV, which has the second-largest number of pay subscribers in the Dodgers' market area.

DirecTV, although it is known for carrying a lot of sports, has balked at the price being demanded for SportsNet LA, which reportedly is over $4 per subscriber per month from all subscribers.

DirecTV has so far declined to agree to binding arbitration and instead has called on Time Warner Cable to make the Dodgers available on an a la carte basis. That means putting the Dodgers on a tier which subscribers can elect to pay for if they want the games without forcing others who don't want the Dodgers to pay as well.

In a statement on Tuesday, DirecTV said: "We agree that any loyal Dodger fans deserve the opportunity to see games, yet not at the expense of the millions of other AT&T U-verse, Charter Communications, Cox Communications, DirecTV, Dish Network, Mediacom, Suddenlink Communications, Verizon FiOS and other families who have little or no interest in paying for Time Warner Cable's excess. Rather than force everyone to bail Time Warner Cable out, the simplest solution is to enable only those who want to pay to see the remaining Dodgers games to do so at the price Time Warner Cable wants to set."

The Hollywood Reporter and the Los Angeles Dodgers are both owned by affiliates of Guggenheim Partners.