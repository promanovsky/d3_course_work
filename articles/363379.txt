Here's a look at the week ahead on our "Sunday Morning" Calendar:



Monday sees the start of the Running of the Bulls in Pamplona, Spain, an eight-day series of races first made famous by Ernest Hemingway in "The Sun Also Rises."



Tuesday is the 60th anniversary of Elvis Presley's radio premiere, when a Memphis station played his version of Arthur Crudup's "That's All Right."

To hear Presley's performance of "That's All Right" click on the video player below.



On Wednesday, House and Senate leaders in both parties award the Congressional Gold Medal posthumously to Swedish diplomat Raoul Wallenberg, credited with saving as many as 100,000 Jews from the Holocaust.



Thursday sees the naming of this year's Prime Time Emmy Nominees. The awards will be presented August 25th in Los Angeles.



Friday is the 100th anniversary of Babe Ruth's major league debut, as a pitcher for the Boston Red Sox.



And Saturday sees the opening of a NASA exhibit about the International Space Station at the Reuben H. Fleet Science Center in San Diego.



Agenda

More events in the coming week



2014 Finger Lakes Wine Festival (July 11-13)

Watkins Glen, N.Y.

Amelia Earhart Festival (July 18-19)

Atchison, Kan.

The 20th Anniversary Canadian Badlands Passion Play (July 10-19)

Drumheller, Alberta, Canada

Hodag Country Festival (July 13)

Rhinelander, Wisc.

Circus City Festival (July 12-19)

Peru, Ind.

2014 Corn Hill Arts Festival (July 12-13)

Rochester, N.Y.