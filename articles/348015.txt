Life expectancy in Ireland has risen by four years.

LIFE expectancy in Ireland has increased by a full four years since 2000 to reach 80.6 today.

It compares with the Organisation for Economic Co-operation and Development (OECD) average of 80.1 years. Men's life expectancy at birth is 78.3 years while for women it is 83 years. The statistics are contained in the annual report of the Health Service Executive which has compiled a summary of where Ireland now stands healthwise.

• Ireland's population is increasing at the third highest rate in the EU and has the highest proportion of young people (0-14) and the second lowest proportion of old people (65 and over).

• Ireland's rate of ageing is higher than the average for EU countries.

• The population continues to grow with an expected increase of around 20,000 persons in the number of people over the age of 65. This will impact on the age distribution of the Irish population. It is projected there will be a 66pc increase in those aged over 65 years by 2026 and almost a 160pc increase by 2046.

• Ireland still has the highest fertility rate in the EU.

• 69,088 births and 31,191 deaths were registered in 2013.

• 1,541 marriages and 338 civil partnerships were registered in 2013.

• he death rate per 1,000 for over 65s has fallen from 46 to under 42.

• Death rates for circulatory system diseases in 2012 were virtually the same as that for cancer whereas it was 35pc higher ten years previously.

• Both circulatory system disease and cancer accounted for 62pc of all deaths registered in 2012.

Health at a Glance 2013: OECD Indicators shows that:

• Mortality due to cancer fell by 21pc, ischemic heart disease by 59pc and cerebrovascular disease by 54pc between 1990 and 2011. In all three instances, the rate of decline was greater than the OECD average.

• Although Ireland has experienced a downward trend in mortality since the 1980s and 1990s, significant health inequalities persist.

• From 2000-2006 the rate of mortality in the lower social class was 300pc higher for all causes of death compared with the higher social class.

• ET} The rate of mortality for cardiovascular disease was 368pc

higher in the lower social class.

• Males in the unskilled manual social class had a mortality rate 1.8 times higher than professional males.

• Health inequalities in Ireland are largely avoidable and require concerted efforts through the development of specific initiatives for

vulnerable groups and the implementation of the recommendations and actions outlined in Healthy Ireland, the Government's health plan.

Health & Living