Taylor Swift has been named America’s highest-earning musician over the last year. She surpassed the likes of Beyoncé, Bon Jovi and even the Rolling Stones to earn nearly $40m (£24m) from US tour dates, music sales, streaming and publishing royalties, according to Billboard’s annual Money Makers report.



The official total for Swift’s 12 months of earnings is $39,699,575.60, about 75% of which came from concert ticket sales. It’s clear that in 2014, playing gigs is the way to earn money: Swift tops the overall Billboard list despite ranking only sixth in digital music sales and eighth in physical album sales. What’s more, over the course of Swift’s 66 sold-out concerts last year, each ticket-buyer apparently spent an average of $17 (£10) on the singer’s tour merch.



Swift previously crowned the Billboard rich list in 2012, earning a similar amount – and again, mostly through touring. Last year she was unseated by Madonna, who also went on to top Forbes’ annual ranking of musicians’ earnings. Unlike Billboard, the Forbes list uses worldwide figures.



The difference between the US and international markets is also reflected elsewhere in the roster of Money Makers. Besides Swift, two other country stars appear in the top 10: Kenny Chesney at No 2, and Luke Bryan at No 3. And whereas Lady Gaga doesn’t appear among the finalists, having only played three US shows last year, the Rolling Stones’ 15 American gigs earned enough to put them at No 5.

