Elizabeth Weise

USATODAY

SAN FRANCISCO — Just days after the European High Court of Justice ruled that people have the "right to be forgotten," Google is getting requests from criminals wanting links to information about their crimes removed.

Tuesday's ruling stated that under European privacy laws, individuals can have the right to request search engines remove links to information they feel is no longer relevant.

As of Friday, Google had received multiple such requests from Europeans, a source close to the company familiar with the situation told USA TODAY.

How the requirement will be implemented isn't yet known.

"That's the question that everybody is asking themselves at this point," said Rita Di Antonio, European director for the International Association of Privacy Professionals in Giulianova, Italy.

It appears Google plans to comply with the court's ruling.

In a statement, the company said, "this is logistically complicated — not least because of the many languages involved and the need for careful review. As soon as we have thought through exactly how this will work, which may take several weeks, we will let our users know."

It will be difficult for Google to pick and choose which requests it will honor, said Eduardo Ustaran, a lawyer who specializes in privacy law in London.

If it rejects a request, "regulators in each member state would then have to fight in the citizen's corner," Ustaran, author of The Future of Privacy, said. "How are they going to deal with what could become tens of thousands of requests?"

The simplest and cheapest option would be for the Internet search company to "automate the takedown with some simple forms and then let others worry about whether it has public interest implications," said Lilian Edwards, a professor of Internet law at University of Strathclyde in Glasgow, Scotland.

The ruling came out of a request by Mario Consteja Gonzalez of Spain about a legal notice that appeared in La Vanguardianewspaper in Barcelona about his home's repossession and auction in 1998.

He said "proceedings concerning him had been fully resolved for a number of years, and that reference to them was now entirely irrelevant" and so requested that the notices be removed from the newspaper's website.

The court ruled that while the paper could keep the page up on its own site, Google must remove the listing from its search index.

The ruling only applies to information retrieved by searching on an individual's name, and requests would have to be by the individual, legal experts said.

For example, a Nazi war criminal could ask Google to remove the link to a Wikipedia page about him or her, but couldn't "ask Wikipedia to take down the page," said Edwards.

Examples of link removal requests Google has received:

-- A company wanted links about it in a forum discussing consumer ripoffs to be removed.

-- A former politician requested links to a news article about his behavior when he was previously in office be removed because he wants to run again

-- A physician requested that links to a review site about him be removed.

-- A man convicted of possession of child sexual abuse imagery requested links to pages about his conviction be removed.

-- A celebrity's child asked that links to news articles about a criminal conviction be removed.

-- A university lecturer who was suspended wants links to articles about the suspension removed.

-- A convicted cyberstalker mentioned in an article about cyberstalking laws requested that links to the article be removed.

-- An actor wants articles about an affair with a teenager removed.

-- A tax scammer requested that links to information about his crime be removed.

-- An individual who tried to kill his family requested a link to a news article about the event be removed.