One of the last things former Windows chief Steve Sinfosky did before he left Microsoft in November 2012 was trigger the investigation of a Microsoft employee who has now been indicted for allegedly leaking data about Windows 8 to a blogger.

Alex Kibkalo, a former Microsoft employee in Lebanon and Russia, has been accused by federal prosecutors of using a Hotmail account to send a software development kit related to Windows 8 to an unnamed blogger who had a reputation for leaking images of Microsoft software before it was officially launched.

Advertisement

The drama began in September 2012, according to a copy of the indictment. An unnamed source contacted Sinofsky to let him know that he had received from the blogger what he believed was proprietary source code for Windows:

(TWCI refers to Trustworthy Computing Investigations, a security company Microsoft employs to track down leakers.) The indictment implies that once Sinofsky had heard from the source, he alerted TWCI and Microsoft legal, at which point they began spying on the blogger's Hotmail account.

Advertisement

Advertisement

The obvious point here is that because the blogger was using Microsoft's Hotmail, it was relatively easy for Microsoft to access the account. Kibkalo worked with Microsoft for seven years but received a poor performance review in 2012 and threatened to resign, according to the indictment. Reuters reports Kibalko since relocated to Russia.

We contacted Sinofsky for comment but did not immediately hear back. There is no indication that Sinofsky was in any way involved in the leak beyond being the person at Microsoft who first discovered the problem. Sinofsky left Microsoft for unrelated reasons in November 2012.