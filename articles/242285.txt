Google Inc is developing a new 7-inch tablet that can capture 3-D images, and plans to produce about 4,000 of these prototypes beginning next month, the Wall Street Journal reported, citing people briefed on the company's plans.

The device, which is part of Google's Advanced Technology and Projects group 'Project Tango,' will have two back cameras, infrared depth sensors and advanced software that can capture precise three-dimensional images of objects, according to the Journal.

Google's Project Tango is a platform for Android phones and tablets designed to track the full 3-dimensional motion of the device as you hold it, while simultaneously creating a map of the environment around it.

The Project's flagship product, a prototype smartphone, released in February has similar sensors and is designed to create a three-dimensional map of its user's surroundings, the Journal said.

Google and other traditionally non-hardware companies such as Amazon.com Inc and Microsoft Corp have made inroads into mobile devices as consumers increasingly access the Web on the go.

Google was not immediately available for comment outside regular business hours.