NASA/JPL

NASA's Curiosity rover is still busy trucking around the surface of Mars, sending back fascinating images of the landscape. A set of photos from a few days ago are all pretty normal, except for one, which shows an apparent blip of light on the horizon.

The photo was taken by the rover's right navigation camera. A close-up look at the light source makes it look like it has a flat bottom and rises upward, where it dissipates. Whenever something unusual pops up in a Mars photo (remember the jelly doughnut?), people get excited.

In the case of the apparent light blip, UFO enthusiasts are speculating that it's an artificial light source, indicating possible intelligent life. Doug Ellison, visualization producer at NASA's Jet Propulsion Laboratory, took to his personal Twitter account to debunk the speculation.

Ellison joins in on a discussion about the light signal, writing, "It's not in the left-Navcam image taken at the exact same moment. It's a cosmic ray hit." Cosmic rays are high-energy particles that go flying through space.

There have been reports of astronauts seeing flashes of light in space, which have been attributed to cosmic rays. "When a cosmic ray happens to pass through the retina it causes the rods and cones to fire, and you perceive a flash of light that is really not there," astronaut Don Pettit wrote on his blog in 2012.

A cosmic ray hit isn't as mind-blowingly awesome as discovering an alien with a flashlight on Mars would have been, but it's still a pretty fascinating explanation.

(Via Reddit)