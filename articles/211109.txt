H.R. Giger has passed away aged 74 from injuries sustained from a stair fall, according to The Guardian. The Swiss artist, who was best known for lending terror to the Alien movies with his biomechanical designs, employed a chilling, surrealist style that evoked otherworldly horror and the depths of nightmare.



Surrealist Swiss Artist H.R. Giger Has Died Aged 74.

Giger gained attention with his 1977 book of paintings, Necronomicon, but when Ridley Scott's Alien was released in 1979, the wider public was given a window into the horrifically detailed alien monster designs and were introduced to the artist's dark imagination.

Swiss artist HR Giger - designer behind Ridley Scott's Alien - dies, reports say http://t.co/raUYBZPAw7 & pic.twitter.com/nc7z0M0iy7 — BBC News (World) (@BBCWorld) May 13, 2014

Though the artist was famed for his Alien creations and influencing later movies in the franchise, he also contributed memorable artwork for album, artwork including Debbie Harry's Koo Koo, the Dead Kennedys' Frankenchrist which prompted an obscenity trial, and the cover for Emerson Lake & Palmer's 1973 album Brain Salad Surgery.

More: New 'Predator' movie coming with possible Arnold Schwarzenegger return.

Born in the city of Chur in 1940, Giger was strongly pushed to enter a career in pharmaceuticals by his father yet he went on to study Architecture and Industrial Design at art school until 1970. He rose to international prominence as part of the Academy Award-winning special effects team that worked on the landmark sci-fi movie, Alien, his most memorable creations including the chest-exploding xenomorphs and alien eggs.

H.R. Giger, ALIENS production designer, RIP (1940-2014) pic.twitter.com/1jeEaFypGK — Cartoon Brew (@cartoonbrew) May 13, 2014

Moving from ink to oil to airbrush and then back to ink, Giger's macabre fantasy visions and depiction of human bodies with cold, mechanical features became instantly recognisable and were the subject of a 2007 documentary, H. R. Giger's Sanctuary.

Later in his life, he created a chain of Giger Bars in Switzerland which are kitted out in his designs. The artist disowned another bar in Japan after he claimed that they did not properly realise his designs. His wife, Carmen Maria Scheifele Giger, is the Director of the H.R. Giger Museum, which is housed in Château St. Germain in Gruyères, Switzerland.

More: Michael Fassbender confirms 'Prometheus 2' role.

Watch The 'Prometheus' Trailer:

