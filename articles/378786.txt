Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

A "supermoon" will light up the Irish sky tonight- if the clouds part to let it through.



The phenomenon, known as a perigee full moon, can make the moon appear up to 14% larger and 30% brighter than when it is farthest from the planet.

The Moon's distance from Earth varies because it follows an elliptical orbit rather than a circular one.

Scientists have dismissed notions that the phenomenon could cause bizarre behaviour or natural disasters. Its most significant impact is likely to be on the tide.

Also known as a Perigee moon, it occurs every 14 full moons, when the oval orbit of earth’s satellite brings it 28,000 miles closer than usual.

It occurs once every fourteen months.