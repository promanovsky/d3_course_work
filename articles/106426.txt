Khloe Kardashian might have a new man in her life. Emphasis on the “might” right now, but more like a “probably” by the end of this post.

The fellow is Moroccan-born rapper French Montana. At first, Kardashian was spotted with Montana at the sweet 16 bash of Sean “Diddy” Combs’ son Christian. Montana was invited to perform at the April 4 celebration, E! News reports. After the party, Kardashian posted an Instagram shot of attendees, including her close friend Malika Haqq, and, oh hey, Montana. (Nothing too obvious at that point.)

Kardashian and Montana then swung by Hooray Henry’s nightclub in West Hollywood on Wednesday night, TMZ reports. It appeared as though the pair made quite the effort to avoid being pictured together: Kardashian exited first and got into her car alone. A minute later, Montana came out of the nightclub and got into his car, where Kourtney Kardashian’s boyfriend, Scott Disick, was patiently waiting. (Definitely pushing the buttons here.)

PHOTOS: Kim Kardashian and Kanye West

Advertisement

Then on Thursday night, Khloe and French were photographed at Cecconi’s restaurant in West Hollywood, accompanied by sister Kourtney and Disick. (OK, Ms. Khloe, third time’s the charm.)

Given all that, there’s one more detail to the story. Montana, whose real name is Karim Kharbouch, has an estranged wife, Deen Kharbouch. With her, he is dad to a 4-year-old, Kruz.

According to Life & Style, Deen Kharbouch was Montana’s rock. “He and I worked together to put him where he is,” she told Life & Style. Montana is the founder and chief exec of Cocaine City Records and has a joint-venture recording deal with Rick Ross’ Maybach Music Group and Combs’ Bad Boy Records. The 29-year-old can be heard on tracks with Jennifer Lopez, Lil Wayne and Miley Cyrus.

As Montana gained popularity, Kharbouch said, he left her and their son behind. “As soon as he popped, it was as if Kruz and I didn’t exist,” she continued. “He practically abandoned us.” Kharbouch also revealed that Montana pays $7,000 a month for child support after “he was forced to do so.”

Advertisement

With that, she had a word of advice for Khloe. “She has to be careful,” she told Life & Style. “Things are not what they seem.”

Though Kardashian filed for divorce from estranged husband Lamar Odom in December, the split is still not legally final.

ALSO:

Yeah, baby! It’s a girl for Mike Myers and his wife, Kelly

Advertisement

Debbie Rowe engaged; is a fight for Michael Jackson’s kids next?

Duchess Kate beats Prince William in New Zealand yacht race

Follow Ministry of Gossip @LATcelebs.