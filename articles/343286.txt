Get weekly highlights from Mirror editor Alison Phillips direct to your email Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

A Mirror investigation helped change the course of the trial by proving that Rolf Harris lied to jurors.

He claimed he could not have groped a girl in Cambridge because he only visited the city for the first time several years ago.

But we uncovered a newspaper cutting showing the entertainer filmed ITV show Star Games there in 1978.

Our evidence was handed to police and passed by prosecutors to judge Mr Justice Sweeney. He ruled a video of Harris could be played in court – showing Harris on Jesus Green.

Roy Stamp, 61, sparked the probe after he got in touch with the Mirror.

He said: “When Rolf said he had never been to Cambridge until several years ago I thought that can’t be true.

“My and I wife remember him 100 per cent. A lot of the celebrities were quite snobby but he spoke to everybody.

"My daughter called him the ‘kangaroo man’. He gave us a signature but we lost it. He drew a picture of him wearing glasses or a kangaroo with glasses.”

The victim said she was around 14 years old when the star groped her bottom at an event in Cambridge.

But Harris claimed: “I went once, a couple of years ago, for an art exhibition of my paintings, and that’s the first time.”

Sasha Wass QC, prosecuting, accused him of “telling a deliberate lie”.