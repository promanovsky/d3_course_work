Brody Jenner Reveals Kim Kardashian Kissed Stepbrother Brandon 'Back in The Day' (VIDEO) Brody Jenner Reveals Kim Kardashian Kissed Stepbrother Brandon 'Back in The Day' (VIDEO)

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Brody Jenner revealed that his older brother Brandon kissed their stepsister Kim Kardashian many years ago.

The "Keeping Up With The Kardashians" star had an awkward run-in with Kardashian during a family vacation in Thailand, which is what prompted the revelation.

During a recent episode of the hit reality TV show Brody, who is the son of Kardashian's stepfather Bruce, accidentally walked in on the E! star taking explicit photos of herself, which prompted Brandon to tease him.

"I feel like Brody is crushing on Kim, look how red he is," Kardashian matriarch Kris Jenner teased after Brody's awkward run-in. "I think you have had a crush since you were like seven years old."

A red-faced Brody then revealed that Brandon, who is married to a singer named Leah, kissed Kardashian when they were younger to deflect attention.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"That was him and Kim. They kissed back in the day!" Brody said of Brandon and Kardashian.

A stunned Jenner then began quizzing Brandon about the encounter as the others burst into laughter.

"You kissed Kim?" Kris asked.

"It's on you now. And he knows it's true," Brody teased.

Despite the Kardashian-Jenner clan joking on their scripted show about Brody "crushing" on Kardashian, reports claim that in reality their relationship is far from pleasant.

Earlier this month Brody attended the wedding on Kardashian's ex-boyfriend Reggie Bush and professional dancer Lilit Avagyan weeks after snubbing Kardashian's wedding to Kanye West.

Bush, who once dated Kardashian for three years, married Avagyan at the Grand Del Mar Hotel in San Diego and Jenner's attendance raised eyebrows.

"Of course it's a huge diss to Kim that her step-brother went to her ex-boyfriend's wedding, and not hers!" a source told RadarOnline.com.

"But Brody has been friends with Reggie for years, and wouldn't have missed his wedding. It's no secret that Brody and Kim aren't close," the insider explained. "Brody believes Reggie dodged a huge bullet by not marrying her!"

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit