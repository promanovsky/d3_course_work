Josh Elliot to quit GMA for NBC Sports after 'he tried to negotiate raise following Robin Roberts' $14m deal'

Moving on: Josh Elliot is quitting GMA to join NBC Sports

GMA star Josh Elliott is quitting America's top rated breakfast show for NBC sports, MailOnline can confirm.



The 42-year-old news star, who has helped GMA take the ratings crown from NBC's Today Show is leaving ABC and signing with the rival network after weeks of negotiations.



He will anchor NBC's highest-profile sports including Sunday Night Football, the Olympics and Triple Crown Racing.



Josh battled to get a huge bump in his salary after his popular co-star Robin Roberts signed a deal believed to be worth $14million.



A source said: 'Josh knew that he wasn't going to get that amount - but what he wanted was an increase that was proportionate to Robin's boost.'

Josh's move to NBC means that he will be well lined up for the anchor role on the Today Show when Matt Lauer eventually leaves.

Matt's $25million contract is up later this year and while he may sign an extension to his contract - he may well depart within the next year or two, leaving the job wide open for Josh, according to TV sources.



One source told MailOnline: 'While the clear lead anchors on the Today Show are Matt Lauer and Savannah Guthrie, on GMA, Josh shared duties with Robin, Lara and George Stephanopoulos and they all pull their own weight together.

Scroll down for video

'Therefore, it was totally right he should ask for a raise equal in measure to Robin's.'



Meanwhile, ABC's Amy Robach, who is currently batting breast cancer, will replace Josh Elliot, it was announced inside the network today.

Josh has just weeks left on his GMA contract and will follow weatherman Sam Champion, who quit GMA for NBC-owned The Weather Channel.

Anchor wars: Josh, right, leaves as his colleague Lara Spencer, center, secured a lucrative multiyear deal announced on Thursday

Today, an NBC Sports source said the deal would be finalised this week, adding: 'Josh is going to be working on our highest profile sports coverage.



'He will be an addition to our team including Bob Costas - not a replacement.'



Josh leaves as his colleague Lara Spencer secured a lucrative multiyear deal announced on Thursday.

Spencer, meanwhile, seemed elated about her new deal, worth an estimated $2 million annually.

'You know you’re in the right job when getting up at 4 a.m. is a joy,' she said. 'This was a simple decision; I love what I do, and I adore the people I work with. I am thrilled to be on this team and look forward to many more years.'



It was reported that Josh was reportedly demanding more than $8 million a year — a whopping increase over his current annual salary of around $1 million.

