Microsoft shareholders look over Surface tablets on display before the annual shareholder meeting Tuesday, Nov. 19, 2013, in Bellevue, Wash. (AP Photo/Elaine Thompson)

Surface 3 Release Date to be Revealed Tuesday

During a small gathering in New York City on Tuesday, May 20, 2014, Microsoft is said to be revealing details of its upcoming Surface 3 tablet. It is also said to include the unveiling of the Surface Mini, reported Tech Radar.

Although Microsoft has been secretive about the products to date, there’s been speculation about what could be unveiled at the presser.

According to CNET, the “Surface Mini” is based on a Qualcomm chip, while Microsoft is rumored to also have a new Intel-based Surface product.

At the press event, which was announced on Bloomberg earlier in May, Microsoft will unveil the Intel product and might have audience walk away with it in hand.

In terms of new Surface, Microsoft will most probably unveil a “Surface 3 Pro” at the meeting. The new device is said to come with a larger screen as larger Type covers were recently released. The covers hint at the possibility of a 12-inch screen.

Depending on the RAM and amount of storage, the new Surface will cost anywhere from $799 to $1,949.