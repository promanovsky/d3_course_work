'This is the most extensive piece of science done on climate adaptation up until now'

India's high vulnerability and exposure to climate change and global warming will slow its economic growth, impact health and development, make poverty reduction more difficult and erode food security, a new report by scientists said on Monday.

The latest report from the UN Intergovernmental Panel on Climate Change (IPCC) stresses the risks of global warming and tries to make a stronger case for governments to adopt policy on adaptation and cut greenhouse gas emissions.

"This is the most extensive piece of science done on climate adaptation up until now," Aromar Revi, one of the lead authors of the report, told a news conference. "The key issue as far as India is concerned is vulnerability and exposure."

The report predicts a rise in global temperatures of between 0.3 and 4.8 degrees Celsius (0.5 to 8.6 Fahrenheit) and a rise of up to 82 cm (32 inches) in sea levels by the late 21st century due to melting ice and expansion of water as it warms, threatening coastal cities from Shanghai to San Francisco.

Experts say India is likely to be hit hard by global warming. It is already one of the most disaster-prone nations in the world and many of its 1.2 billion people live in areas vulnerable to hazards such as floods, cyclones and droughts.

Freak weather patterns will not only affect agricultural output and food security, but will also lead to water shortages and trigger outbreaks of water and mosquito-borne diseases such as diarrhea and malaria in many developing nations.

"All aspects of food security are potentially affected by climate change including food access, utilisation of land, and price stability," said Revi, adding that studies showed wheat and rice yields were decreasing due to climatic changes.

The IPCC lead authors said India, like many other developing nations, is likely to suffer losses in all major sectors of the economy including energy, transport, farming and tourism.

For example, evidence suggests tourists will choose to spend their holidays at higher altitudes due to cooler temperatures or the sea level rises, hitting beach resorts.

India ranked as the most vulnerable of 51 countries in terms of beach tourism, while Cyprus is the least vulnerable in one study which was examined by the IPCC scientists.

Extreme weather may also harm infrastructure such as roads, ports and airports, impacting delivery of goods and services.

"The world has realised mitigation is absolutely critical and probably the most effective form of adaptation but adaptation processes have to be accelerated, especially in ... lower middle-income countries like India," said Revi.