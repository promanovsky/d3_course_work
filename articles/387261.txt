UPDATE 1:22 p.m. EDT: Volkswagen responds to reports that Ferdinand Piëch, the company's main shareholder, was in talks with the Elkann and Agnelli families, the principal sharholders of Fiat SpA: "No merger plans or aquisition is on our agenda right now," the company said in a statement. "We are focusing on increasing efficieny within the group."

Original story begins here:

Volkswagen AG (FRA:VOW) is the world’s second-largest automaker by annual sales, but in the U.S. the German automaker’s flagship brand is losing market share. In the first six months of 2014, only the GTI Golf increased sales from the year-ago period, to 1,927 vehicles.

While the company’s luxury Porsches and Audi brands are doing well, Volkswagen is getting pummeled in key mainstream segments, like crossover SUVs. VW also doesn’t have a pickup truck to sell in a country where three of the five best-selling vehicles are the F-Series, Chevrolet Silverado and Ram.

Now, VW is reportedly eyeing Fiat-Chrysler in an effort to strengthen its U.S. presence. The German-language news site Der Spiegel reported Thursday that Ferdinand Piëch, VW’s largest single shareholder, has had conversations with the Elkann and Agnelli families, the principal owners Fiat SpA, over a full or partial takeover.

The news comes just six months after Fiat SpA (BIT:F) finalized acquisition of Chrysler Group LLC from a UAW retiree trust that acquired a stake in the company in the wake of Chrysler’s 2009 bankruptcy that cost U.S. taxpayers $1.8 billion.

The acquisition would spin off Fiat’s car business, leaving the industrial conglomerate agricultural, industrial and construction vehicles and equipment.

Acquiring Fiat-Chrysler would give Volkswagen the Jeep and Dodge brands, including the hot-selling Cherokee and Grand Cherokee SUVs and the Ram pickup truck. While Volkswagen’s market share has declined in the U.S. this year, Chrysler’s slice of the pie has increased to 12.5 percent from 11.6 percent, according to the automotive pricing and information provider Kelley Blue Book.

Volkswagen could also get Fiat’s Ferrari, Lancia, Alfa-Romero and Maserati brands to add to the German company’s holdings of ultra-luxury cars, including Bugatti, Lamborghini and Bentley. Or these companies could be sold off, leaving Volkswagen with the Chrysler Group cars and two Fiat minis.

But if the talks between the primary shareholders of the two companies are serious then they would contradict recent comments made by Volkswagen CEO Martin Winterkorn that he had no plans to add to Volkswagen’s 12 brands. The company acquired Porsche and truck-makers Scania and MAN within the past seven years. Meanwhile, VW is trying to reduce costs by $6.76 billion by 2018 to reverse shrinking operating margins. An acquisition of Fiat SpA’s car business would be a major investment by VW at a time when it’s trying to reduce costs.

Neither Volkswagen nor Fiat-Chrysler would comment.

On Monday, Volkswagen announced it will start building a new mid-sized SUV at its sole U.S. manufacturing plant in Chattanooga, Tennessee, by the end of 2016. The vehicle hasn’t been named yet, but it’s based on the CrossBlue concept SUV that debuted at the Detroit auto show last year aimed at buyers of Chrysler’s Jeep Grand Cherokee.