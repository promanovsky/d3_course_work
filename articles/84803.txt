Meet the XBox One; a new generation of games and entertainment. Where games push the boundaries of realism.

Meet the XBox One; a new generation of games and entertainment. Where games push the boundaries of realism....

A 5-year-old San Diego boy has outwitted the sharpest minds at Microsoft — he’s found a backdoor to the Xbox.

Kristoffer Von Hassel managed to log in to his father’s Xbox Live account.

When the password login screen appeared, Kristoffer simply hit the space button a few times and hit enter.

Robert Davies told KGTV-TV that just after Christmas he noticed his son started playing games he supposedly couldn’t access.

Davies, who works in computer security, says he reported the issue to Microsoft, which fixed the bug and recently listed Kristoffer on its website as a “security researcher.”

Kristoffer’s response: “I’m gonna be famous!”

A Microsoft spokesman didn’t immediately return a call for comment.

It’s not the Kristoffer’s first triumph. At one year old, he bypassed a mobile phone toddler lock by holding down the “home” button.