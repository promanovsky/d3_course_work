Allergan is surging on takeover news, and some option traders are looking at insane profits.

Just yesterday, optionMONSTER's Heat Seeker system detected a bullish combination trade. Investors bought the May 150 calls and sold the May 125 puts for a net cost of just $0.40. Today that position has inflated by more than 3,000 percent to about $14. A bullish call spread we recommended as an alternative strategy on our InsideOptions Pro subscription services more than tripled as well. (See our Education section)

Traders without access to optionMONSTER's exclusive data scrambled to react after the bell yesterday, when it was disclosed that Valeant Pharmaceuticals wanted to buy the Botox maker for $47 billion in stock and cash. AGN is up 16.05 percent to $164.79 in afternoon trading. VRX, which was recommended just hours before on our Market Action webinar, is ripping 7.4 percent to $135.33 as well.

The buyout would be a huge win for activist investor Bill Ackman, who reportedly started building a stake in AGN two months ago. He initially bought common stock but switched to options in March to keep his moves secret, according to Bloomberg News . Ackman's Pershing Square Capital was the biggest holder in the target company with a stake of almost 10 percent when the news broke yesterday.

The deal also represents the largest acquisition yet for Valeant, which has been buying its way into the big leagues of the pharmaceutical industry. After its purchase of AGN, revenue will total about $12 billion. That puts it on par with Gilead Sciences but still below larger players such as Bristol-Myers Squibb ($16 billion) and Eli Lilly ($23 billion).

Options continue to flow in AGN and VRX today, with more than 30,000 contracts trading in each. Both average fewer than 3,000 in a typical session.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright © 2010 OptionMonster® Holdings, Inc. All Rights Reserved.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.