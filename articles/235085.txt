According to sources, the FlipKart-Myntra deal could be worth about Rs 2,000 crore.

Marking the biggest consolidation in the e-commerce space in India, homegrown e-retailer Flipkart has acquired online fashion retailer Myntra in an estimated Rs 2,000 crore deal.

While the companies did not disclose the deal size, sources suggest the deal could be worth about Rs 2,000 crore.

"It is a 100 per cent acquisition and going forward, we have big plans in this segment. Flipkart and Myntra are getting together to create one of the largest e-commerce stories and together we will dominate the market," Flipkart co-founder and CEO Sachin Bansal told reporters here.

Asked about valuation, the companies declined to comment.

"We, at Flipkart, believe that we want to be leaders in every segment and fashion is a category of the future, this acquisition will help us become leaders in this category," he said.

Sachin said Flipkart will invest USD 100 million (around Rs 600 crore) in its fashion business in the near-term.

Flipkart, which started in 2007 as an online bookstore, sells products across categories, including fashion and electronics. It also sells white goods and furniture.

The move is expected to help Flipkart strengthen its apparel portfolio and compete more aggressively with peers like Amazon and Snapdeal.

Myntra will continue to operate as a separate entity with its co-founder and CEO Mukesh Bansal joining Flipkart board and heading the fashion business.

"It was very essential to keep Myntra a separate entity and preserve its culture. I'm here for the long haul and we will continue to grow in the market," Mukesh Bansal said.

Speaking about the Flipkart-Myntra deal, Suchi Mukherjee, CEO & Co Founder, LimeRoad.com, said: "The sequence of events in the Myntra fund raise journey and the Flipkart merger make two things clear (1) the lifestyle segment in online commerce is attractive on size, economics and margin pool relative to electronics etc, traditionally the mainstay of Flipkart - the merger therefore makes Flipkart's pro-forma economics better.

(2) building a real online lifestyle brand of size is hard work, and in the absence of anything other than discounts, takes a ton of cash to build. No one has truly won the hearts and minds of Indian users and this presents a huge opportunity for businesses like LimeRoad that are fundamentally product-centric, deeply differentiated and driven by organic growth."

India's e-commerce market has seen huge growth in the past few years as more people log on to the Internet to shop. While apparel and electronics are bestsellers for most e-commerce firms, categories such as home decor and household items are also popular.

The industry, estimated to be worth about USD 3 billion currently, has firms such as Snapdeal, eBay and Amazon which follow the marketplace model.

Led by increasing Internet penetration and youngsters shopping online, Flipkart's annualised sales crossed USD 1 billion (over Rs 6,100 crore) a year ahead of target. It had estimated to reach the billion dollar mark for gross merchandise value by 2015.

Flipkart, which also operates under the marketplace model allowing retailers to offer products on its platform, has since its inception raised over USD 500 million from investors.

The Bangalore-based firm, founded by Sachin Bansal and Binny Bansal, counts Naspers, Tiger Global, Accel Partners, Dragoneer, Morgan Stanley, Sofina and Vulcan Capital among its investors.

Last year, Flipkart raised USD 360 million from private equity firms, one of the largest funding deals in the Indian e-commerce space. Myntra sells products from over 650 brands like Nike, HRX by Hrithik Roshan, Biba and Steve Madden and clocked revenue of about Rs 1,000 crore in the previous financial year.

It aims to double its revenue in this financial year as it expands its seller base and adds products.

Myntra has about 100 sellers on board and plans to increase this number to 1,000 by fiscal end.

Facts about Flipkart

* Flipkart offers over 15 million products across 70+ categories.

* Registered users: 18 million

* Daily visits: 3.5 million

* Technology has enabled 5 million shipments per month

* Sellers on platform: 3,000

* Team strength: 10,000

Facts about Myntra

* Partnership with over 650 leading fashion and lifestyle brands in the country

* Myntra receives over 50 million visits every month and services over 9,000 pin codes across the country