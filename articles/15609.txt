BOSTON - Former Boston Mayor Thomas Menino has been diagnosed with an advanced form of an unknown cancer, the Boston Globe reported.

The newspaper says in a story on its website Saturday that doctors found "an advanced cancer of unknown origin" that had metastasized, and that they were unable to find its source. The cancer was discovered in February by Menino's primary physician, Dr. Charles A. Morris, the newspaper reported.

Menino, the longest-serving Boston mayor who retired from the office a year ago, is 71 and has had many health problems in recent years. He said Morris, after making the discovery, was more shaken that Menino.

"My attitude really is, we'll get through it," Menino told the Globe.

Mayor Marty Walsh released a statement Saturday saying his thoughts and prayers are with Menino and his family.

"I've never known Tom Menino to back down from a fight, and I don't expect him to start now," the statement said. "Mayor Menino has always been here for the people of Boston, and we're behind him today, 100%."

Menino was admitted to the hospital several times while in office.

In 2003, he underwent surgery to remove a rare sarcoma on his back. The following year, his doctors confirmed he has been diagnosed with Crohn's disease, a type of inflammatory bowel disease.

He spent six weeks in the hospital in 2012 for a series of ailments, including a respiratory infection. While he was in the hospital, he suffered a compression fracture in his spine and was diagnosed with Type 2 diabetes.

In May 2013, he was back in the hospital for surgery for an enlarged prostate.

Menino served as Boston mayor for more than 20 years. Health problems forced him to decline to run for a sixth term.