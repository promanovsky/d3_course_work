'He's my family': Angelina Jolie lifts the lid on rock-solid relationship with Brad Pitt as she confirms they'll star together in 'very experimental, independent film'

Angelina Jolie has opened up about what makes her eight-year relationship with Brad Pitt so strong as she confirms the pair will once again team up on the silver screen for the first time since 2005's

Mr & Mrs Smith.



In a new interview with Extra TV, the mother-of-six has revealed that her fiancé is so much more

than just her partner and father of her children.



'He's my family. He's not just a lover and partner, which he is, wonderfully, but he's my family now,' she gushes.

Scroll down for video

'He's not just a lover and partner; he's my family now': Angelina Jolie has opened up during an interview with Extra TV about what keeps her eight-year relationship with Brad Pitt so strong as she confirms the two will team up on the silver screen once more

While their relationship is rock-solid, the 38-year-old admits that they go through their struggles just like anyone else, but they're not about to give up at the first sign of trouble.



'We have history and work hard to make it great,' she says. 'We don't kind of relax about it and take each other for granted. Like everybody, we have our challenges, but we're fighting to make it great.'



With rumours continuing to build momentum that the couple will team up on the big screen, the actress and director confirmed that they will both appear in a new project that she's written.

'We have history and work hard to make it great': The star admits the couple, who have been engaged since April 2012 and have six children together - seen here in London on May 8 - face 'challenges' just like everybody else, but she says 'we're fighting to make it great'

'It's not a big movie and it's not an action movie': Of the pair's upcoming collaboration, in which they'll star together in a film she wrote, the 38-year-old says, 'It's a very experimental, independent type film where we get to just be actors together and be really raw and open and try things'

While she was unwilling to divulge any of the plot, she did reveal that it's the kind of project they've longed to do together.



'I really can't say very much, except that it's not a big movie and it's not an action movie,' she begins. 'It's the kind of movie we both kind of love but aren't often cast in.



'It's a very experimental, independent type film where we get to just be actors together and be really raw and open and try things.'



Despite talk that the Oscar-winner will soon quit acting altogether to focus on directing, she insists that time has not yet come and her movie with Brad, 50, will not be her last acting role.

'Knoxy loves it! He tells Dad he can't do it!' Ange revealed that her five-year-old loves her wicked character's catchphrase, 'Well, well', in upcoming movie Maleficent, adding that all her kids 'meet me with that when I walk in the door now'

'It was fun to share': The mom-of-six said having her children on the set of the Disney movie - in which daughter Vivienne co-stars and Zahara and Pax have minor cameos - enhanced her performance 'because if I think about making them laugh, I get a little crazier'

'No. I'm getting down there. I think at some point it will be,' she says. 'I love directing. Unbroken was really just one of the greatest experiences I’ve ever had working with film.'



Talking about her latest role, in which she plays the dastardly title character in Disney's Maleficent, the star revealed her children now greet her with her with the catchphrase, 'Well, well'.



'My kids meet me with that when I walk in the door now,' she laughs. 'Knoxy loves it, and then Knoxy tries to see if everybody else can do it. He tells Dad he can't do it!'

'I'm getting down there. I think at some point it will be': While she confirmed that her new film with fiancé Brad, 50, won't be her last acting role, she said she is heading towards the stage where she will give it up to focus on her love of directing

While daughter Vivienne, five, appears in the film as a young Aurora - played by Elle Fanning - nine-year-old Zahara and Pax, 10, also have minor cameos.



Speaking of what it was like to have her children on set with her, the doting mother - who also has Maddox, 12, Shiloh, seven, and Knox, five, with Brad - admitted they brought out the best in her.



'It was fun to share, but it was also that I'm better when they're there,' she says. 'My performance

was a little more enhanced, 'cause if I think about making them laugh, I get a little crazier.'

The highly-anticipated movie is set for release on May 30.



Mother Of The Year! On May 13, the Oscar-winner took five of her six-strong brood on a shopping trip to New York's famed FAO Schwarz - seen here with sons Knox (left) and Pax and daughter Vivienne, while Zahara and Shiloh rushed ahead and eldest child Maddox wasn't present



