Scientists analyzing data from the Kepler space telescope have discovered a first: an Earth-size planet orbiting in the habitable zone of its parent star, researchers announced Thursday.

The planet is one of five worlds orbiting a dwarf star known as Kepler-186 some 500 light years away in the constellation Cygnus. The other four worlds were discovered earlier in the Kepler data, orbiting close in to the parent star, well outside the so-called "Goldilocks" zone where water can exist as a liquid on the surface.

The newly discovered fifth planet, Kepler 186f, is roughly the same size as Earth and orbits in the star's sweet spot, at a distance comparable to Mercury's orbit in Earth's solar system, taking about 130 days to complete one orbit, or year.

But because Kepler-186 is a cooler star, about half the size of our sun, Kepler-186f is exposed to much more tolerable levels of solar heating even though it is relatively close to its sun.

Scientists do not know whether Kepler-186f is, in fact, habitable, or how it might be affected by solar radiation, flares and the parent star's gravity. They don't know whether the planet has an atmosphere at all, and if it does, how it might trap heat or otherwise affect the environment.

But it is the first exoplanet found so far that's the right size and right distance from its sun to support life as we know it.

"We know of just one planet where life exists -- Earth," Elisa Quintana, a research scientist at the SETI Institute, said in a statement. "When we search for life outside our solar system we focus on finding planets with characteristics that mimic that of Earth. Finding a habitable zone planet comparable to Earth in size is a major step forward."

Quintana is the lead author of a paper published Thursday in the journal Science that announced the discovery.

"This is the first validated detection of an Earth-size planet orbiting in the habitable zone of its parent star, a cool red dwarf," Douglas Hudgins, exoplanet exploration program scientist at NASA Headquarters in Washington, told reporters. "Second, this discovery establishes that Earth-size planets can and do exist in the habitable zones of other stars.

"Third, red dwarf stars, like the parent (of Kepler-186f) are by far the most abundant stars in our galaxy, making up about 80 percent of the nearest stars to the Earth. Thus, planets such as this one are almost certainly the most common type of habitable planet in our galaxy and may very well represent the closest habitable planets to Earth."

Kepler, launched in March 2009, is equipped with a 95-megapixel camera that was aimed at a patch of sky the size of an out-stretched hand that contains more than 4.5 million detectable stars. Kepler monitored the light from 160,000 of those suns.

The camera cannot directly "see" exoplanets, but if a planet passes in front of a star as viewed from the space telescope, the star's light will periodically dim as the planet moves through its orbit. By precisely measuring those tiny fluctuations, Kepler researchers can indirectly confirm a planet's presence, size and distance from its sun.

Kepler completed its primary three-year mission in November 2012. NASA managers promptly approved a four-year mission extension, but in 2013, the spacecraft was hobbled by the failure of a second gyro stabilizer, preventing the pointing accuracy required for planet detection.

But over the first four years of its mission, Kepler's observations have allowed researchers to confirm 950 actual exoplanets with another 3,800 "candidate" worlds requiring additional analysis.