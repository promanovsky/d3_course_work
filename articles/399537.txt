“We experiment on human beings!” cried OKCupid’s data guru Christian Rudder, in a blogpost that became one of the top stories on the BBC News website yesterday, and presumably didn’t hurt the promotion of his upcoming book.

The post was a response to the recent backlash against Facebook’s manipulation of news feeds to investigate “emotional contagion”, and described some of the many experiments OKCupid has run on its own users over the years. In one example, staff manipulated the site’s compatibility scores, deliberately giving bad matches a higher rating to see what the effect would be. Or as Dylan Matthews at Vox (or his sub-editor at least) phrased it, “Did OkCupid send a bunch of incompatible people on dates on purpose?”

“No” is the pedantic answer, because OKCupid doesn’t send anyone on dates. Users choose which crushing disappointment to meet next by using a generally ineffective combination of assessing candidates’ profiles, guessing what has been cropped out of the pictures, and their own free will. They have access to a wealth of information that extends far beyond a single, crude number with an uncanny habit of matching you to people you already know whom you don’t want to have sex with.

Leaving that aside, Matthews describes the experiment, in which “users were told they had different compatibility scores (on a scale of 0 to 100) than they in fact did,” as “concerning”. He then asks a superficially reasonable question: “whether the potential damage of the interventions justifies the lack of informed consent outside of OkCupid’s normal terms and conditions.” He gives a hypothetical example: “Users were told their true match scores after the experiment was concluded. What if an actual couple, whose relationship began when they thought they were a 90% match, were then informed they were a 30% match?”

It’s a decent enough point, but ignores one crucial fact – that the scores were an arbitrary experiment in the first place. What Matthews refers to as a “true match score” doesn’t exist. There isn’t one. There’s just whatever the site spits out at a given moment in time, based on the data available and an algorithm that’s been in continuous development for several years.

If you accept that we should be allowed to have dating websites, then you have to accept that they need some kind of algorithm to help users filter candidates, and therefore you have to accept that the company needs to be able to build, test and tweak that algorithm. That process involves experimentation from day one. In other words, if you think there are ethical problems with the tweaking, you have a problem with the ethics of there being an algorithm in the first place.

What Rudder says about this in his blogpost would be regarded as common sense by any software engineer used to developing user experiences: “OkCupid doesn’t really know what it’s doing. Neither does any other website. It’s not like people have been building these things for very long, or you can go look up a blueprint or something. Most ideas are bad. Even good ideas could be better. Experiments are how you sort all this out.”

These experiments are absolutely ubiquitous in the industry, because without them most of the web as we see it wouldn’t exist. OKCupid’s algorithms are just one in a vast class of recommender systems used to filter content and offer suggestions to the users of practically every major website and online service you can think of. They drive Amazon’s product suggestions, the discovery system in Spotify, and choose the TV shows Netflix thinks you should watch next.

And these algorithms aren’t just features; they’re the reason why sites like Netflix or Facebook can exist in the first place. As Rudder puts it: “guess what, everybody: if you use the internet, you’re the subject of hundreds of experiments at any given time, on every site. That’s how websites work.”

The trouble is, most people don’t realise that. In a post asking why the OKCupid story hasn’t had more traction given the response to Facebook’s “human experiments”, Tim Carmody notes that: “OKC’s matching algorithm may be at least as opaque as Facebook’s news feed, but it’s clearer to users that site matches and views are generated using an algorithm. Reportedly, 62% of Facebook users weren’t aware that Facebook’s news feed was filtered by an algorithm at all.”

Pundits don’t fare much better. “Facebook played with the emotions of over half a million users in the name of research, without their consent,” one writer asserted in the New Statesman, “But one key thing to remember here – and what becomes clear upon reading through half a decade’s worth of news reports – is that Facebook have been doing this for years.”



Well, duh – otherwise the service wouldn’t exist, or indeed most of the websites we use daily. Writers tackling this subject shouldn’t need to read five years’ of news reports to understand that.



Every editor, publisher and writer – on or off the Internet – is engaged in a lifelong process of “playing with the emotions of people” because that’s what figuring out the best way to publish content inevitably means. There’s no functional, practical or ethical difference between Facebook choosing what pops into your newsfeed, and the Guardian choosing whether this article appears on the front page. Both websites are trying things out – “experimenting” – on users, and collecting the results.

That doesn’t mean we should ignore any concerns about it, but it’s very difficult to have a meaningful public debate about an industry when neither journalists nor users have any clue how it operates. Similarly, discussion of “informed consent” around these algorithms is pretty moot if people have no idea what they are – you can’t consent to something in any meaningful way if you don’t understand what it is.



That’s barely the start of the vast, intractable problems around the idea of user consent. Let’s try a thought experiment. Imagine that a law were enacted requiring all online publishers carrying out similar experiments to OKCupid and Facebook to gain explicit consent from their users. Now bear in mind that given the ubiquity of these algorithms and data gathering practices, such a rule applied to Facebook would have to apply to virtually all social media sites, all news websites, all online video services, all search engines.

The “choice” to the extent it deserves the name, would be to click “yes”, or go back to 2003. Day one would witness a mass execution of popup windows; brief impressions of consent obliterated by the barely conscious mouse clicks of a billion mildly frustrated users. Like a flock of birds trying to land on a Gatling gun with a particularly poor attention span. You may as well ask people to opt out of electricity.

Maybe others have better ideas – some kind of voluntary best practice code perhaps – but the most frustrating thing about all of this is that the public debate on algorithms is about a decade behind the technology. Few columnists or bloggers have even the most rudimentary understanding of modern algorithms, data analytics, software engineering practices, or the practicalities of running a web service, and that illiteracy is spread far and wide with each new scare.

That’s a problem because now more than ever we need journalists equipped to examine the implications of the “big data” renaissance and the algorithms applied to it. One of the big ironies of the software industry in 2014 is that advances in data analytics software are outpacing the supply of the data scientists needed to make sense of it all. The danger is that we face a near future of increasingly sophisticated tools in the hands of people decreasingly equipped to understand them.

Not that we’ll know much about it, for a while at least. It’s taken mainstream journalists 10 years to spot the implications of business models and engineering practices devised in the early 00s, and start discussing them in any meaningful way. What will they notice in 2024?