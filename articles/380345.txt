Calling your cable service provider for any reason is never on anybody's list of things they enjoy doing, but this particular example turned into an epic nightmare.

When tech journalist Ryan Block and his wife, Veronica Belmont, tried to cancel their Comcast service over the phone, they did not have an easy time of it.

Not because they couldn't get through or were transferred to an overseas call center ... but because the representative refused to let them cancel it.

Seriously. He rudely, incessantly demanded to know why ... why?! WHY WOULD YOU CANCEL SOMETHING AS AWESOME AS COMCAST?!?

The more Block refused to engage in a discussion of why he was severing all ties with Comcast, the more aggressive and inappropriate the rep became.

Eight minutes later, he finally got what he wanted ... and posted the conversation on SoundCloud. He says he started the post about 10 minutes in.

It's not clear what happened in those first 10 minutes, but Block was adamant about bailing on the cable giant, and just as adamant about not saying why.

Block patiently, calmly and repeatedly asked what it would take to disconnect his service, which only made the world's worst customer service rep more irate.

You can listen above to fully appreciate this ... and share it with everyone else you know who hates cable companies, which should be everyone you know.

Epic fail, Comcast. Epic. Fail.