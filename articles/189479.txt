The U.S. Navy is now rolling out an e-reader to sailors that they can actually use while deployed at sea.

The Navy doesn't allow Kindles and iPads on submarines; an enemy could hack into an iPad or an e-reader connected to the web and even gain access to the device's camera. Furthermore, it's difficult to download or stream content to a tablet without an Internet connection, and Wi-Fi doesn't work very well on submarines or ships in the middle of the ocean.

Findaway World, the digital technology producer that built the Navy's e-reader, evaded both of those issues by constructing a device that is purposefully several generations behind its counterparts from companies like Apple and Amazon. The Navy asked Findaway World to develop an e-reader for sailors about two years ago, Ralph Lazaro, Findaway's vice president of digital products, told Mashable, because the company had already built similar devices for United States Army soldiers and other members of the military.

The Navy e-reader device, or "NeRD," doesn't have Internet access — or access to anything, for that matter, except the 300 books that are preloaded onto the e-reader by the time it reaches a sailor's hands. It's essentially an accessible digital library.

A mere 300 books out of the Navy's digital library of 108,000 is a relatively small percentage, but Lazaro said it's a big step up from the number of paper books that can be lugged onto a submarine. There is little space for anything in those cramped quarters, much less entertainment.

Sailors will have access to "the classics," Lazaro said, as well as books on naval history and some contemporary fiction such as The Girl With the Dragon Tattoo and books from the series A Song of Ice and Fire.

Findaway shipped the first 365 e-readers out to naval vessels last week, and it will take a few more weeks until sailors get ahold of them. For now, each ship and submarine will receive about five devices, meaning sailors will have to share. Lazaro said Findaway plans to continue rolling out its product until just about every sailor has his own e-reader.