Asian stimulus boosts factories

Share this article: Share Tweet Share Share Share Email Share

London/Tokyo - Manufacturing activity in Asia's industrial powerhouses China and Japan gained pace in June, fuelled by improving demand at home, but euro zone growth faltered as main motor Germany slowed. Business surveys published on Tuesday confirmed factory output expanded across Asia following months of decline in its two biggest economies, as massive stimulus packages from the authorities in Beijing and Tokyo begin to take effect. China's final HSBC/Markit Purchasing Managers' Index (PMI) rose to 50.7, slightly below a flash reading but above the 50 mark that separates growth from contraction for the first time in six months. The official China PMI, which is geared more towards bigger state-owned firms, hit a six-month high of 51.0. “The Chinese numbers were good. The authorities are helping, they are supporting, they are guiding the economy in the direction they want it to go in,” said Peter Dixon, economist at Commerzbank.

In contrast, the raft of measures announced last month by the European Central Bank to counter the threat of deflation and support growth by boosting lending to companies and households have yet to show any impact.

Markit's final Manufacturing PMI for the euro zone fell to 51.8 in June from May's 52.2, the lowest reading since November.

“The ECB is going to be looking at these numbers in the coming months and hoping that we see a bit more of a pick-up. Let's check in six month's time if the ECB needs to do any more,” Dixon said.

Stock markets firmed after the China data, which reinforced market views that the world's second-largest economy is steadying thanks to stimulus from Beijing after growth dipped to an 18-month low of 7.4 percent in the first quarter.

Those measures include reserve requirement cuts for some banks to encourage more lending, quicker fiscal disbursements and hastening construction of railways and public housing.

A downturn in the property market is clouding the outlook, however, and economists expect Beijing to stand ready to ease fiscal and monetary policy further to counter any major spillover into the broader economy.

“Efforts to slash overcapacities in old-fashioned industries, as well as the housing market downturn ... will continue to weigh on overall economic activity,” said Nikolaus Keis at UniCredit.

In Japan, central bank and PMI surveys painted a similar picture of improving factory activity, supported by continued hefty central bank money injections and government spending.

Japan's PMI topped the 50-point mark for the first time in three months but with an April sales tax rise still acting as a drag, the Bank of Japan's business optimism gauge dipped in the second quarter. Still, firms were optimistic about the outlook, declaring readiness to boost capital investment and output.

“It was still a good result. The Tankan result supports the Bank of Japan's upbeat view on the economy,” said Takuji Aida at Societe Generale.

In Indonesia, Southeast Asia's largest economy, factory activity grew at its fastest pace on record and in India, the continent's third-largest economy, it hit a four-month high.

Markets will also be looking ahead to US PMI data to confirm that the world's biggest economy has finally put its weather-affected start to the year firmly behind it.

BRITAIN LEADS EUROPE

British factories followed Asia's lead, increasing activity at the fastest rate in seven months while creating new jobs in at the briskest pace in more than three years.

Euro zone unemployment was stable for the second consecutive month in May at 11.6 percent but the pace of factory growth eased as a contraction in the bloc's second biggest economy, France, deepened.

Germany was again the driving force, helped by a resurgence in the bloc's periphery countries, although its PMI dipped due to public holidays.

“The slowdown will put pressure on policymakers at the ECB to do more to prevent the recovery from stalling, and we will no doubt see more calls for full-scale quantitative easing to be implemented,” said Chris Williamson, Markit's chief economist.

The chance of the ECB launching an asset purchase programme has risen to one-in-three, a Reuters poll taken last week found, ahead of this Thursday's policy meeting.

The ECB cut its deposit rate below zero last month and suggested rates will remain at record lows for years, while offering more long-term loans aimed at boosting bank lending. But it is still reluctant to conduct outright bond purchases.

By contrast, the Bank of England is widely expected to be the first major central bank to begin tightening policy, possibly as soon as this year. The pound has been surging as a result.

“Manufacturing is growing strongly, and work flows suggest this has legs,” said David Tinsley at BNP Paribas.

“As this news flow is absorbed further, rate hike expectations for the first hike in Q4 this year should harden.” - Reuters