Los Angeles: Hollywood actress Halle Berry believes that there are other life forms in other planets.

The 47-year-old actress shared her opinion about alien`s existence as she appeared in an episode of `Late Show with David Letterman`, reportedly.

"I don`t believe we are the only species in existence. My ego doesn`t tell me that we`re the only ones who survived... it might take us 20 years to get to those other life forms, but I think they are out there," Berry said.

The actress will be seen in upcoming sci-fi thriller TV series` Extant`, set in a not-so-distant future. It tells the story of Berry`s character Molly Woods who returns to the Earth after a one-year mission only to find that she is pregnant.