From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

The settlement price for the tech industry salary-suppression lawsuit is confirmed: It’s $324.5 million.

Apple, Google, Intel and Adobe will pony up that amount, which had previously been rumored but is now confirmed in settlement records filed Thursday in federal court. The settlement, which still needs to be approved by U.S. District Judge Lucy Koh in San Diego, avoids what could have been an embarrassing trial, scheduled to begin at the end of this month. Pixar, Lucasfilm and Intuit settled last year for $20 million.

Judge Koh had merged various lawsuits going back to 2011 into one class-action suit. The plaintiffs included engineers, designers, artists, editors, sys admins and others who worked at the companies from 2005 to 2010. The workers contended that the named companies promised not to poach each others’ workers, an agreement that the plaintiffs said made the market for their skills uncompetitive and suppressed their salaries.

As large as the figure appears at first glance, it is only a fraction the $3 billion sought by the lawsuit, which could have been tripled if antitrust had been proven.

Each of over 64,000 plaintiffs are expected to receive between $2,000 and $8,000. More than $80 million of the settlement is expected to be paid out in legal fees.

In 2010, six of the companies settled with U.S. Department of Justice because of these non-poaching agreements. That settlement, which involved no fine or cash payment, required the companies to abandon “no solicitation” recruiting agreements for five years.

On its Public Policy Blog in 2010, Google echoed the other companies in saying that it believed it had done nothing wrong.

“Our policy only impacted cold calling,” Google posted, “and we continued to recruit from these companies through LinkedIn, job fairs, employee referrals, or when candidates approached Google directly.” Additionally, the companies had contended that their agreements did not push down salaries, and they opposed the employees suing together in a class action.

But the cooperative agreements about workers had been important enough to attract the attention of the top people at these legendary companies.

In one document made public, for example, then-Google CEO Eric Schimidt emailed Apple chief executive Steve Jobs to report that a Google recruiter was about to be fired because he attempted to hire an Apple employee.

Jobs’ reply: ” : ) ”

In another, Schmidt told Google’s human resources director by email to share the companies’ no-cold-call agreements only verbally with competitors, so as not “to create a paper trail over which we can be sued later.”

From the evidence revealed in this lawsuit, Jobs was totally behind the non-compete effort. Google co-founder Sergey Brin, for instance, reported to other executives in his company that the Apple co-founder had declared, “If you hire a single one of [Apple’s] people, that means war.”

via