Drew Barrymore has welcomed baby number two.

The 39-year-old actress and her husband, art advisor Will Kopelman, 36, greeted their second child together on Tuesday. A representative for the "Blended" actress confirmed the news exclusively to People.

"Happy to announce that today we are proud parents of our second daughter, Frankie Barrymore Kopelman," the couple said in a statement. "Olive has a new little sister, and everyone is healthy and happy!"

Barrymore and Kopelman married in a small, private ceremony at the actress's home in Montecito, Calif., in June 2012 and gave birth to daughter Olive that September. Last November, the actress said she was happy to share the news about her pregnancy after being unsuccessful with keeping it a secret the first time.

"Last time I never commented on it and people just stalked me the entire time," the actress said at the time, according to People. "So yes, it's happening, it's true. I tried to keep it a secret for as long as possible."

The mom-to-be confirmed in December that she was expecting another girl.

"I'm relieved I'm having another girl. I have everything I need. It's all about hand-me-downs and room sharing and all that stuff," she told People in February. "I'm pretty psyched about that!"

In January, Barrymore opened up about her numerous pregnancy cravings.

"The [craving] with Olive was tamales," Barrymore told E! News. "Anything tamale. I love tamales. With this little lovely lady, it's like pan-fried noodles from any Chinese restaurant. I drive by them and I fantasize about them. I find myself, like, pulling over a lot for food. Like the car just goes off the lane and over to the restaurant."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.