Due to the fact that Game of Thrones is a highly popular TV series and that it requires watchers to have access to HBO in order to enjoy it, this means that those of us who do not have access, either because we don’t have it in our country or don’t want to pay for it, won’t be able to watch it. This also means that it can lead to piracy where users will have to resort to downloading the TV series via torrents.

Advertising

That being said, the piracy of Game of Thrones is hardly a secret. We have seen how the TV series has managed to break torrent records, but if you’re wondering how the latest episode of Game of Thrones did, you might be shocked. According to TorrentFreak, the season four finale, “The Children”, managed to break the previous record.

They have estimated that the episode was downloaded around 1.5 million times during the first 12 hours of the show airing. The episode also managed to break the record for the largest swarm, which is basically the number of people sharing the same file, with a total of 2 petabytes downloaded in 12 hours.

If you’re wondering how big that is, 1 petabyte is equivalent to 1,024 terabytes, which is equivalent to over a million gigabytes. Yikes! The report also found that Australia had the highest number of downloads, namely because local TV service, Foxtel, has an exclusive on the show in the country, meaning that if you’re not subscribed to Foxtel, you’d be out of luck.

Filed in . Read more about Piracy.