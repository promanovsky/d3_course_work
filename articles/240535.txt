Kim Kardashian and Kanye West have repeatedly said that they want a “small, intimate” wedding with only close friends and family. Well, that small, intimate wedding is quickly becoming one heck of a star-studded event if the guest-list rumors are true!

In a previous report by The Inquisitr, a source close to the couple allegedly told E! News that because Kim and Kanye wanted a quiet, small wedding, they were being very picky about who they were inviting. “The couple had to be very selective and cut it off at no work friends, no matter how close or cherished.”

That source may have been right when they said the couple was cutting people from the guest list, just take Brody Jenner. According to The Inquisitr, Kim’s step-brother reportedly isn’t going to be making the trip to Europe for the festivities. US Weekly reportedly spoke to someone close to Brody, who claimed that his girlfriend Kaitlynn Carter wasn’t officially invited to the event, so Brody may have opted to not attend instead of leaving his girl out of the festivities

Well, even if some of the family isn’t planning on attending the May 24th event, that doesn’t mean some big names won’t be making an appearance. US Weekly reported that quite a few stars have RSVP’d “yes” to the event, including Jay Z, Chrissy Teigen, and even Serena Williams.

“Others who have RSVP’d “yes” include fashion designer Rachel Roy, Vogue contributing editor Andre Leon Talley, celeb trainer Gunnar Peterson, stylists to the stars Chris McMillan and Jen Atkin, rapper Q-Tip, Girls Gone Wild founder Joe Francis and girlfriend Abbey Wilson, Tyga, and video-girl Blac Chyna. Longtime Kardashian pals Malika Haqq, Brittny Gastineau, Jonathan Cheban, Simon Huck, and Shelli Azoff will also be in attendance.”

With the original guest list supposedly supposed to remain around 100, many are wondering if this wedding will become more of a star-studded event instead of a close family and friend’s celebration.

Earlier this week, some of Kim’s family did start arriving in Paris to hang out and prepare for this weekend’s festivities. Some of those people included siblings Rob, Khloe, and Kourtney Kardashian, along with Kris, Kylie, Kendall and Bruce Jenner. Step-brother Brandon and his wife Leah, Kourtney’s man Scott Disick and their kids Mason and Penelope are all expected to attend the main ceremony, along with the pre and post-wedding festivities.

There are still reportedly quite a few things left to do before the big day finally arrives, so all those involved are sure to be busy for the next few days. Stay with The Inquisitr as the big wedding for Kim Kardashian and Kanye West draws closer!

[Image by Bing]