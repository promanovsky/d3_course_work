Okay, yes. In theory.

But I wish we wouldn’t.

AD

Louis CK joked about being a white man: “How many advantages could one person have? I’m a white man. You can’t even hurt my feelings! What can you really call a white man that really digs deep?” In general, you can tell you aren’t the Folks in Charge when there remains a word or two (or more) that can be spewed out to “put you in your place.” There’s a whole set of them for women, dangling off the ends of numerous letters.

AD

And now we want to add “bossy”?

Sandberg admits that it’s not as easy as getting rid of one word. If it’s not, then why are we doing it? Even assuming we eradicate “bossy,” that won’t finish the list of Potentially Hurtful Remarks. “Catty” will come back to prominence, or “shrewish,” or maybe we’ll see a resurgence of “harridan.” Don’t whack the word. Deal with it.

AD

There are a number of reasons this is a poor solution to the problem, the first of which being that the people who agree to “ban bossy” are not the people standing in the way of young women as they try to become leaders. If you are in a classroom setting where the news reaches you that Sheryl Sandberg wants to “ban bossy,” and everyone says, “Okay. Great idea, Sheryl! We’re striking it from the roster right now,” then you are probably not in the kind of environment that threatens female success. The people who won’t “ban bossy” are the problem. The people who are calling you “bossy” right now don’t get that what they’re doing is wrong.

AD

Look, I’m always mistrustful of solutions that haven’t been tested by the people proposing them. Sandberg didn’t become Sheryl Sandberg because no one called her “bossy” and so she felt empowered and became a leader. She rose in spite of that.

Sheryl Sandberg points out that a teacher called her “bossy” and told her friends not to be friends with her. I wish instead of telling people not to say “bossy,” she would tell us what she did next. How did she get past this to become the chief operating officer of Facebook? Yes, it hurts. It’s a problem that this is what little girls get called. But if you’re going to feel shooed out and shouted down when people say “bossy,” the jerks of the world are going to keep saying “bossy.” This is like dealing with the Sleeping Beauty curse by removing all the spindles from the land. The trick is not to remove all the spindles. The trick is to teach you how to handle a spindle safely so that it won’t sting you.

AD

I’m sure if you made it a rule that Everyone Henceforth Says Only Nice Encouraging Things, we would have a lot more people with the confidence to strike out and take charge.

AD

No one would ever be hurt if we could just sand off the edges where people are liable to get cut. If the only way to get women to the top of the workforce is to eliminate playground taunts and bad teachers who sap your confidence, I mean, forget it. Good luck with that. If you succeed, I’ll pay you what Warren Buffett is paying me for getting all the March Madness picks right.

I’m all for encouraging girls to lead. What do we think society is, a waltz? Of course women should be leading. Get them in the room solving the problems.