A Calgary-based, humanitarian organization says an outbreak of the deadly Ebola virus is spreading rapidly in West Africa.

Samaritan's Purse sent a team to the front lines in an effort to contain the highly-contagious virus.

Keren Massey, who's originally from Three Hills, is one of four Canadians working with the Liberian government.

She says the Ebola virus is easily spread and has a mortality rate of up to 90 per cent.

That has prompted victims to flee medical care in rural areas where cases are originating, says Massey.

She said they are instead crossing the border into Guinea, which makes it extremely difficult to follow up on them.

“It means the virus is spreading.”

Samaritan's Purse has started an awareness campaign that hopes to educate more than 200,000 about how to protect themselves.