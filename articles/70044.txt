The minimally invasive aortic heart valve replacement system from Edwards Lifesciences Corp. performed better than a rival product sold by Medtronic Inc. in the first head-to-head study of the two, according to data from a small German trial presented at a major heart meeting on Sunday.

The results are unlikely to be seen as decisive given the size and limited scope of the study. But they could provide the Edwards sales force with a valuable marketing tool as the two companies vie for market share with their competing transcatheter aortic valve replacement, or TAVR, systems.

Both systems employ a catheter to thread the new valve through an artery into place in the heart, sparing patients too frail for open heart surgery from the invasive chest cracking procedure. The Edwards Sapien XT uses a balloon to expand the compressed valve once it is in position in the diseased valve, while the Medtronic CoreValve has a special self expanding valve using an alloy that reacts to body heat to open.

The results were presented at the American College of Cardiology's annual scientific sessions in Washington.

In the 241-patient study conducted at five German hospitals, doctors using the Edwards balloon expandable system reported a success rate of 95.9 percent compared with a 77.5 percent success rate for those using the Medtronic alternative.

A successful procedure was defined as one in which the valve was implanted in the correct position and provided a tight enough seal to prevent blood leakage across the valve.

Patients receiving the Edwards valve also reported improvement in symptoms such as breathlessness, chest pain and dizziness 30 days after the procedure at a higher rate than those in the Medtronic group: 94.3 percent versus 86.7 percent.

"We have two main types of valves available for this procedure, and until now, there was no conclusive data about their relative effectiveness," Dr. Mohamed Abdel-Wahab, the study's lead investigator, said in a statement.

DEATH, STROKE RATES

But the definitions of effectiveness used as the primary goals for the German study may be viewed as limited in value.

Virtually all doctors who perform heart valve replacements, typically in patients aged 80 and older, agree that the most important clinical results for these procedures are rates of death and stroke. By those all-important measures, there was no significant difference between the two TAVR systems 30 days after the procedure.

"Medtronic believes that physicians will look to more rigorous trials with hard clinical endpoints when evaluating these valves to provide reliable guidance on TAVR device selection," Medtronic said in a statement.

Medtronic also took issue with the method used to assess aortic valve leakage in the independent German study, calling the angiography readings "the least effective measure for determining aortic regurgitation."

The researchers plan to follow the patients for five years to compare the longer-term health outcomes of the two groups, which is likely to yield more meaningful information.

"It's possible some of these differences might not persist over time," said Dr. Saul Vizel, a cardiologist with Cambridge Memorial Hospital in Cambridge, Ontario, who was not involved in the study. "The jury is still out for sure, because they're doing one-year and two-year follow-ups and people will wait for those."

Dr. Patrick O'Gara, incoming president of the American College of Cardiology from Brigham & Women's Hospital in Boston, also was not ready to declare a winner based on the German study.

"I would like to see a larger trial with more endpoints," O'Gara said. "It would be premature to overinterpret these findings."

Not surprisingly, Edwards Lifesciences had a more favorable view of the findings than Medtronic.

"This independent, multi-center randomized study is consistent with the experience in Europe, where more clinicians choose Sapien XT over CoreValve for the treatment of their patients," Edwards spokeswoman Sarah Huoh said in an emailed statement. "These data are reflective of the findings of multiple, real-world European registries."

TAVR procedures have been performed in Europe since 2007 but are relatively new in the United States. The first Sapien system gained U.S. approval in 2011, and Medtronic's CoreValve was approved by the Food and Drug Administration in January.

The CoreValve uses a smaller, potentially easier-to-maneuver, catheter than the original Sapien. Edwards expects U.S. approval of the next-generation Sapien XT with a similarly smaller catheter in the coming months.

More than 100,000 people in the United States have severe aortic stenosis, with about one-third too ill or frail for open-heart surgery, making them potential TAVR candidates.

Industry analysts expect the Sapien XT and CoreValve to be important future growth drivers for Edwards and Medtronic.

"We now have new valves coming out that will probably be even better," Abdel-Wahab said. "These results can help inform the design of future devices."