How does it feel? Like a new record for the sale of rock lyrics at auction, as a handwritten manuscript for Bob Dylan’s “Like A Rolling Stone” sold Tuesday for just over $2 million at Sotheby’s auction house in New York.

The identity of the buyer was not released, but the purchase price bested the previous record of $1.2 million paid in 2010 for John Lennon’s lyrics to “A Day in the Life” from “Sgt. Pepper’s Lonely Hearts Club Band.”

Lyrics to another Dylan classic, “A Hard Rain’s A-Gonna Fall,” sold for $485,000, according to Reuters, at Sotheby’s first dedicated music history sale in more than a decade.

Other items in the sale included memorabilia from the Beatles, the Rolling Stones and Elvis Presley, with pre-auction estimates ranging from as little as $200 to $300 to $1 million to $2 million for the draft of “Like A Rolling Stone” that Dylan wrote on stationery from the Roger Smith Hotel in Washington.

Advertisement

The manuscript shows, in Dylan’s hand, the lines of the song’s famous refrain “How does it feel/To be on your own/ No direction home/Like a complete unknown” as well as phrases he ulimately rejected including “Al Capone,” “ain’t quite real” and “dry vermouth, you’ll tell the truth.”

According to Sotheby’s, the lyrics were put up for auction by a man identified only as a fan from California “who met his hero in a non-rock context and bought [the lyrics] directly from Dylan.”

Follow @RandyLewis2 on Twitter for pop music coverage