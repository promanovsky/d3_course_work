The European Central Bank will brief Europe's banks next week on its plan for disclosing the results of a landmark asset quality review.

The ECB faces a delicate balancing act as it tries to maintain the secrecy of its work to avoid any breaches of market disclosure rules, while not blind-siding banks with unforeseen capital demands that they could struggle to fulfil.

An agenda for a meeting on July 10 shows the ECB's director general for Micro-Prudential Supervision, Jukka Vesala, will lead a three hour meeting about how the test results will be communicated.

The meeting will include a consultation on the way information will be disclosed to the public at the end of the exercise.

It will also include details of how the test results will be communicated to the banks themselves.

In some previous bank reviews, banks have been given 'preliminary' results several days before the official announcement of the final results to allow them to develop a strategy to deal with any capital shortfalls.

The ECB confirmed the meetings, with chief financial officers and chief risk officers, are taking place from July 8 to July 10.

The meetings will also cover other queries from the 128 banks undergoing the tests about the ECB's review, including the way 'challenger models' are used to validate the internal models banks use to value assets that have no market valuation, like some derivatives.

"There are a lot of routine meetings to establish the methodology of the process and the meeting of July 10 is one of them," said a source familiar with the meeting. "The banks have a lot of requests and the ECB will give the methodology (for the results disclosure)," the source added.

The ECB's asset quality review of the euro zone's 128 most important banks will be followed by EU-wide "stress tests" to see if the region's banks have enough capital to withstand another crisis.

The twin exercises are designed to restore confidence in a banking market that has traded at lower valuations than the US banking sector since the financial crisis, as investors pondered the real state of European banks' balance sheets.

All results will be released in October. Banks have made extensive preparations ahead of the tests, including raising over €100 billion in the nine months to April, shedding assets and writing off bad debts.

Read how the ECB's four-year loan scheme will work