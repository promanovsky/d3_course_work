"Twilight"star Nikki Reed and "American Idol" Season 10 finalist Paul McDonald have split after two years of marriage, People reports.

"After much consideration, Nikki Reed and Paul McDonald are ending their marriage. They have been living separately for the past six months due to work obligations," a rep for Reed said.

"They will continue to share their love of music, and are still working on their debut album, "I'm Not Falling," releasing in 2014. They remain best friends and look forward to their continued journey together."

According to TVGuide.com the couple met in March 2011, became engaged that June and tied the knot in October. Reed will next be seen in the paranormal romance "In Your Eyes."