Kim Kardashian celebrated her Bachelorette party with some of her best gal pals in Paris on Thursday. According to Us Weekly, Kim took to the streets of Paris with her sisters Khloe, Kourtney, Kendall, and Kylie and her friends — LaLa Anthony, Joyce Bonelli, Monica Rose, and Brittny Gastineau to name a few — in honor of her last few days as an unmarried woman (again).

The girls kicked things off in Kim’s room at the Four Seasons Hotel George V. Fifteen bottles of Veuve Clicquot champagne were delivered to the room and the girls toasted to Kim. After some girl talk and some sips of bubbly, the ladies headed to a cozy dinner at Hotel Costes.

Kim Kardashian’s Bachelorette party continued after dinner as the ladies got into private vans and checked out some of the most iconic sights in the city. They then spent some time on foot (in heels) and got together to take some photos in front of the Louvre. The best part of the whole night, however, had to be how the girls finished it. They attended a private party inside the Eiffel Tower!

Of course everyone wants to know what Kim was wearing. According to Mail Online, the bride-to-be wore a dress by Balmain. The long-sleeved mini-dress was from the fall 2012 collection and retailed for over $20,000. The baby blue piece was reportedly inspired by Fabergé eggs.

Kim Kardashian celebrated her Bachelorette party two days before her wedding to beau, Kanye West. As previously reported by The Inquisitr, Kim and Kanye arrived in the City of Love last weekend. They have been spotted at the gym, out to dinner, and doing regular day-to-day things that non-famous do. Except they’re in Paris… living the dream.

As far as the big wedding goes, there are several rumors about where Kim and Kanye will be tying the knot. Some outlets are still reporting that the couple will fly their guests to Florence, Italy where they will say their “I do’s” and other sources are convinced that Kimye will becoming man and wife at the Palace of Versailles in France. There also seems to be some confusion about whether or not the wedding is going to be filmed for an upcoming E! special. Kim took to her Twitter account a few weeks ago to let everyone know that she and Kanye decided not to allow E! cameras to film their big day… but there have been reports to the contrary since then.

Kim Kardshian already had her Bachelorette party and her bridal shower so all that is left is a rehearsal dinner (likely on Friday) before she will get to put on her wedding dress (rumor has it she has four) and marry the love of her life. Hopefully this is it this time.

[Photo courtesy of Marc Piasecki/GC Images]