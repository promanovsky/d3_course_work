A high-tech cooler dubbed the Coolest, designed to do more than just hold drinks, has raised over $4.1 million in a Kickstarter campaign.

The technology was invented by Ryan Grepper from Portland, Ore., who aimed to create a cooler that could go beyond simply keeping beverages cold, according to PC Magazine. Features for the cooler include a built-in blender for making drinks, a USB charger for powering devices, and a waterproof Bluetooth for playing music, which can be removed.

A LED lid light is also included, letting users see their drinks in the dark. Addtionally, the cooler has a built-in cutting board that can also be used as a cooler divider, a bottle opener, storage areas for knives and plates, and extra wide tires that allow the cooler to roll easily.

"The Coolest is a portable party disguised as a cooler, bringing blended drinks, music and fun to any outdoor occasion," the Kickstarter page reads. "The Coolest cooler is 60 quarts of AWESOME packed with so much fun you'll look for excuses to get outside more often."

The campaign has surpassed its original goal of $50,000, so far receiving support from 21,000 backers.

There are still 46 days left for people interested in the Coolest, also referred to as the 21st Century Cooler that's actually a cooler, to show their support for the Kickstarter campaign, The News Ledge reported. Those who place orders for the $185 cooler will also receive a Keep Calm and Blend On shirt.

Grepper is looking to use the money made from the fundraising campaign to establish deals for manufacturing and finish work on the prototype cooler, PC Magazine reported. He also plans to make the Coolest available by February 2015, so the cooler won't be seeing any beach parties until next summer.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.