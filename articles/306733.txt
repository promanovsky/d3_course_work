FORT MYERS, Fla., June 19 (UPI) -- New estimates put the number of Florida panthers in the wild at somewhere around 180. That was the news from Gil McRae, the director of the Florida Fish and Wildlife Conservation Commission's research institute, who spoke of the rebounding panther population at a commission meeting this week in Fort Myers, Florida.

"The population increased steadily between 1995 and 2012, and the biggest thing that brought the population back is a 10 percent increase in kitten survival," said McRae.

Advertisement

Florida panthers neared extinction in the early 1990s, but a concerted conservation effort by state and federal wildlife officials offered the majestic predator a lifeline, and populations have slowly rebounded since.

The Florida panther is still federally protected, a member of the endangered species list, and will remain so until two separate populations of at least 240 exist. Getting them off the list would call for the panther to be reintroduced elsewhere in the Southeast U.S.

RELATED Girl bitten by rattlesnake in University of Michigan botanical gardens

That's a reality that doesn't seem too far off, now. But it's a reality that also has its own set of problems, like increasing interactions between humans and panthers.

"Due to the expansive habitat needs of the Florida panther, the continued growth of their population presents a unique challenge to the FWC and U.S. Fish and Wildlife Service," explained FWC Commissioner Liesa Priddy. "As panther range expands, impacts on private landowners will continue to increase."

"We know panthers can prey upon pets and livestock, and we strive to find solutions that work for people who experience these very real losses," added Thomas Eason, director of the FWC's Division of Habitat and Species Conservation.

Officials say much of their future panther work will center on keeping human-panther conflicts to a minimum, which means educating the public but also finding additional space for the panthers to roam.

RELATED Idina Menzel recovers from wardrobe malfunction with grace