The Food and Drug Administration has approved a new type of bionic arm for amputees that promises an unprecedented level of control and mobility. The DEKA Arm, developed with funding from the Defense Advanced Research Projects Agency (DARPA), is controlled by electrical signals prompted by the contraction of muscles close to where the prosthesis is attached. Electrodes then send the signals to a computer processor that translates them into a specific movement or movements.

"The DEKA Arm System may allow some people to perform more complex tasks than they can with current prostheses in a way that more closely resembles the natural motion of the arm," said Christy Foreman, the director of the Office of Device Evaluation at the FDA's Center for Devices and Radiological Health, in a press release.

A 2008 study estimated that one in 190 Americans is living with the loss of an upper or lower limb. As of December 2013, more than 1,500 Americans had lost a leg or arm in combat in Iraq or Afghanistan. Those who suffered an amputation of the arm often end up wearing an artificial limb with a manual hook whose design hasn't changed much in decades.

Clinical research showed that with use of the DEKA Arm, approximately 90 percent of participants were able to perform common household activities that they couldn't do with a traditional prosthetic, such as using keys and opening locks, preparing food, combing their hair, and using zippers.

Roughly the same size as an average human's arm, the prosthesis can be configured for people with limb loss at the shoulder, the mid-upper or mid-lower arm. It cannot be used by people with limb loss at the elbow or wrist.

In a "60 Minutes" interview in 2009 , Dr. Geoffrey Ling, the leader of the project, told CBS News' Scott Pelley that when a human loses a hand -- an "incredibly complex" piece of machinery -- they lose something that separates us from other animals.

"What nature provides us is extraordinary. The opposable thumb, the five finger independently moving, articulating fingers. It's fantastic what this does," Dr. Ling said.

The team that was responsible for the creation of the DEKA arm also included Dean Kamen, who has invented dozens of medical devices as well as the Segway transportation system. However, after designing the arm -- altogether a tough task, Kamen admitted to Pelley -- they had some problems with getting the patients to accept it.

"Immediately, we were shocked to learn, even just the hollow plastic shell that they wear when they're out and about, it sweats, and it hurts, and it irritates. And we came back and realized that if we build the world's best nine-pound arm, but nobody will wear it because 24 hours a day, or 12 hours a day, of wearing a nine-pound arm is going to be irritating, and frustrating, we said, 'We've got a way bigger problem here,'" Kamen said in the 2009 interview.

Fred Downs, then the national director of the U.S. Department of Veterans Affairs' prosthetics division, had lost his arm when he stepped on a land mine in Vietnam. Before exchanging his hook to try out the DEKA Arm, he had admitted that he was a bit skeptical.

However, after ten hours of practicing, he was able to pick up a soda bottle. He told Pelley how good it felt to move "his" arm again. "It felt like my arm. It was me," he remarked.

"The feeling is hard to describe. For the first time in 40 years, my left hand did this. I almost choke up saying it now. It was just -- it was such an amazing feeling. I was 23 years old the last time I did that," Downs said in the interview.