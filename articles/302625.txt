ConAgra Foods (NYSE: CAG) shares were sliding fast in pre-market trading Wednesday after the company cut its outlook and said it would miss fourth-quarter expectations.

The packaged and commercial foods company will take a $681 million fourth-quarter impairment charge to reduce goodwill,and blamed faltering performance on a seven percent quarterly volume decline in consumer foods, as well as weak profits for private brands.

CEO Gary Rodkin said ConAgra was trying to improve its product mix, and as a result expects volume to improve in 2015 along with "mid-single-digit" growth in earnings per share.

Rodkin added that the current dividend policy is not at risk.

ConAgra now expects to post fourth-quarter earnings before items of $0.55 per share, down from its previous guidance of $0.60. ConAgra is slated to post results June 26.

Fourth-quarter operating profit for the private brands segment will fall by $60 million mainly because of price concessions. The company's profit projections for the segment are now "below original plans for the next several years."

In pre-market trading, ConAgra changed hands recently at $30.85 per share, down six percent.