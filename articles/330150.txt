Buenos Aires - Argentina's Vice President Amado Boudou was charged on Friday with corruption in his dealings with a company that printed the country's currency while he was economy minister in 2010.

The vice president will remain free while awaiting trial in the case along with five other defendants, according to a statement from Argentina's federal court system.

Boudou is accused of secretly buying Ciccone Calcografica, a company contracted to print Argentina's peso currency, while serving as the country's top economic policymaker. He denies the charge along with any wrongdoing.

Boudou has carried out minimal public functions in recent months as the investigation came to a head.