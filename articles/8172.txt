Teruhiko Wakayama, a respected stem cell scientist from Japan’s RIKEN Institute, said he is not certain about the methods used in two studies he co-authored with lead investigator Haruko Obokata.

In the ground-breaking work, heralded by some in the field as a game-changer in the way stem cells are made, Obokata and her team, which included researchers from Harvard University and other international institutes, detailed how they were able to coax already developed cells to revert back to an embryonic-like state to become stem cells by simply exposing them to chemical solutions (mostly acidic) or physical stress. Stem cells can be manipulated to develop into any of the body’s tissues to repair or replace diseased cells.

The controversy erupted when Obokata and her team published a tips sheet for other researchers to follow to replicate their work. But inconsistencies between the newly released methods and the original protocol in the papers, as well as questions about images in the published work, led some to wonder about the validity of the results. Wakayama himself said he was able to repeat the study only once, with Obokata’s assistance, but not on his own.

MORE: The Rise and Fall of the Cloning King

In a press conference in Japan last month, Wakayama, who is best known for using stem cell techniques to clone mice, said he asked all of the scientists involved to retract the papers, which were published in the journal Nature in January, and to have the data and results reviewed by other scientists. RIKEN is investigating the work, as is Nature.

The development adds another black eye to the field of stem cell science, which is ripe with possibility but has struggled to establish its credibility. In 2006, Korean researcher Woo Suk Hwang claimed he had become the first to successfully “clone” human cells, generating patient-specific lines of stem cells from a person’s skin cell. The work turned out to be fraudulent, and the stem cells derived from an already established technique of extracting them from existing embryos.

Since then, both policy makers and those in the field have been more skeptical of milestone claims – for good reason, as the latest study shows.

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.