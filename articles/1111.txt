Real-life “Death Stars” deadlier than Darth Vader astronomers find

Alderaan isn’t the only planet to fall victim to the “Death Star”, astronomers claim, though the vast O-type stars actually across the galaxy destroying young planets before they’ve had time to form don’t have convenient exhaust ports to stop them. Using the Atacama Large Millimeter/submillimeter Array (ALMA), a team of US and Canadian researchers discovered that huge quantities of ultraviolet radiation from stars at least 16-times the mass of our own sun can blast away the raw materials for new planetary systems.

Tracking stars and protostars in the Orion Nebula, researchers found that any protostars – that is, young stars the dust and matter around which often goes on to coalesce into planets – within 0.1 light years (around 600bn miles) of an O-type star would have their raw material cleared out by the radiation before it had a chance to cluster.

“Using ALMA, we looked at dozens of embryonic stars with planet-forming potential and, for the first time, found clear indications where protoplanetary disks simply vanished under the intense glow of a neighboring massive star” astronomer Rita Mann explained.

Usually, it takes a few million years for stellar dust and gas to begin to combine into something denser, and then over time form into planets. The raw material is believed to come from the explosion of huge stars going supernova, but ironically anything forming too close can end up being undermined by the radiation pumped out.

“Massive stars are hot and hundreds of times more luminous than our Sun. Their energetic photons can quickly deplete a nearby protoplanetary disk by heating up its gas, breaking it up, and sweeping it away” James Di Francesco, National Research Council of Canada

Although telescopes like Hubble had previously allowed astronomers to see very young protostars, known as proplyds, in Orion, they lacked the ability to identify how much mass each had. However, using ALMA meant researchers could look inside the teardrop-shaped forms, to reveal what dust was contained.

The differences proved striking. Within the extreme-UV read of a massive star, proplyds had less than half the dust mass required to form a planet roughly the size of Jupiter; beyond that, they could have anywhere up to 80x the mass of Jupiter in reserve.

Future investigation, the team says, will hopefully indicate how common solar systems like our own are, and perhaps help in hunting down areas of space habitable for life.