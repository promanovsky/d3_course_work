SAN FRANCISCO (MarketWatch) — SanDisk Corp. rallied on Thursday to the top of the S&P 500 in the wake of strong results while Google Inc. was one of the worst Nasdaq 100 performers following disappointing earnings.

SanDisk Corp. US:SNDK jumped 9.7%. The flash-memory maker said late Wednesday its profit surged 62% in the first quarter as revenue rose 13%, with results topping analyst forecasts.

Getty Images

Google GOOG, +0.39% GOOGL, +0.30% shares fell 2.8%. The technology giant posted first-quarter results late Wednesday that missed analyst estimates, with adjusted earnings coming in at $6.27 a share and revenue rising 19% to $15.4 billion. Read: Google’s growth in mobile ads hurts results

Gainers

Shares of chip maker Micron Technology Inc. MU, +1.77% gained 6.6%. Analysts at Sterne Agee raised the stock’s price target to $32 from $30 on Wednesday.

Netflix Inc. NFLX, -0.19% shares climbed 4%. The stock was upgraded to outperform from sectorperform with a price target of $500 at Pacific Crest. “Global expansion appears likely to accelerate, while pricing, margins and adjacent opportunities remain significant potential upside drivers,” said Andy Hargreaves at Pacific Crest.

Baker Hughes Inc. US:BHI shares advanced 3.8%. The oil field services company reported first-quarter earnings of 84 cents a share, ahead of 78 cents projected by analysts in a FactSet survey.

Decliners

Mattel’s Barbie sales drop 14%. AFP/Getty Images

Western Union WU, +1.83% shares fell 4.5% following an announcement from Wal-Mart that it is launching its own money transfer service. “Available April 24, the new low-cost service allows customers to transfer money to and from more than 4,000 Walmart stores nationwide for up to 50 percent less than similar offerings on the market,” said Wal-Mart in a statement. Shares of Wal-Mart edged up 0.2%.

Shares of Fifth Third Bancorp FITB, +0.81% dropped 3.8%. The regional bank reported first-quarter earnings of 36 cents a share, significantly below the 42 cents a share forecast by analysts in a FactSet survey.

Chipotle Mexican Grill Inc. CMG, -1.00% shares shed 4.4%, reversing earlier gains. The gourmet burrito chain on Thursday reported strong same-store sales but its first-quarter profit fell short of analysts’ estimates.

Alliance Data Systems Corp. ADS, -0.43% slumped 7%. The operator of loyalty programs and private-label credit cards on Thursday reported its first-quarter earnings rose to $2.08 a share from $1.92 a share. However, it also said its LoyaltyOne unit, particularly its AirMiles business, is likely to face headwinds during the year. “The first, and biggest challenge, is the Canadian dollar, down about 8% from a year ago. Ouch,” said the company in its earnings statement. The second challenge is weak consumer spending, it said.

Tickers to Watch

GS: Goldman Sachs Group Inc. GS, +1.32% shares edged up 0.2%. The bank said Thursday its first-quarter profit fell to $2.03 billion, or $4.02 a share, from $2.26 billion, or $4.29 a share, a year earlier. Total revenue fell 8% to $9.33 billion. Shares rose as Goldman’s results topped expectations of per-share earnings of $3.48 on revenue of $8.66 billion, as forecast by analysts polled by FactSet.

MS: Morgan Stanley MS, +1.21% shares rose 3.1%. First-quarter profit jumped to $1.51 billion from $962 million a year ago, with adjusted per-share earnings coming in at 68 cents. Quarterly revenue increased to $8.93 billion from $8.15 billion a year earlier. Results came in stronger than analyst expectations. The bank also said it would double its dividend to 10 cents a share and repurchase up to $1 billion shares.

MAT: Mattel Inc. MAT, +3.43% shares slid 1.1%. The toy company said Thursday it swung to a first-quarter loss of $11.2 million, or 3 cents a share, missing analyst expectations. Net sales fell 5% to $946.2 million, with sales of its Barbie doll brand dropping 14%.

PEP: PepsiCo Inc. PEP, -0.81% shares rose 0.5%. The food company said Thursday its first-quarter profit rose 13% to $1.22 billion, or 79 cents a share, from $1.08 billion, or 69 cents a share, in the year-ago period. Adjusted earnings were 83 cents a share and revenue inched up to $12.62 billion from $12.58 billion. PepsiCo’s first-quarter results beat expectations.

SABR: Sabre Corp. SABR, -1.50% rose 4.6% to $16.73 in its first few minutes of trading in the stock market. The travel-tech firm on Wednesday priced its initial public offering at $16 a share, below the expected range of $18 to $20.

WB: Weibo Corp. WB, +5.32% , the Chinese version of Twitter, rallied 23% to $20.97 on its first day of trading in the U.S. market. The social media company had priced its IPO at $17 a share.

WLP: WellPoint Inc. US:WLP shares declined 3.6%. Competitor UnitedHealth Group Inc. UNH, -1.45% shares are off 2.9% after the health-care insurer reported results that were generally in line with expectations. However, health insurance sector as a whole is weighed down by uncertainties stemming from Obamacare.

Read more on MarketWatch:

4 reasons the first quarter was better than it appeared

LIVE BLOG: Goldman Sachs Q1 earnings call

Hate your bank? Here’s why you won’t switch