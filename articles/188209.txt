A new study demonstrated a three percent drop in Massachusetts' death rate, following the passage of state health reform in 2006.

Lead author of the study, health policy and economics assistant professor at Harvard's School of Public Health Dr. Benjamin Sommers and his colleagues found that during the last four years, there has been a general drop in deaths of all causes.

For the probe, researchers compared the improvement of Massachusetts and other comparable states in America that did not make health insurance reforms of their own. The analysts studied death rates among those aged 20 to 64 before and after the health-reform law, and found that the lowest death percentages occurred in high poverty counties with more uninsured people post-reform. More people in Massachusetts also had life insurance after the law was brought into effect.

Furthermore, deaths in the state from preventable and treatable conditions such as infections and heart disease were also reduced by 4.5 percent. Around 320 deaths per year are prevented by the health insurance law, and for every 830 people who acquired insurance, one life was saved.

Massachusetts' health insurance reform law provided a precedent for the Affordable Care Act, which is also known as Obamacare. President Obama signed the Affordable Care Act in 2010, but Massachusetts' reforms took place in 2006 and 2007. It made health coverage personal and mandatory; it funded private health plans, and provided a larger scope for Medicaid.

"If you think health reform is what's causing this change, that's the pattern you should see, that the people who benefit the most are lower-income and uninsured," Sommers said to Healthday News.

This study was published on the May 6 issue of the Annals of Internal Medicine.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.