Josh Kaufman and coach Usher perform a soulful rendition of The Police‘s “Every Breath You Take” on during day one of The Voice Season 6 Finale on Monday (May 19) in Universal City, Calif.

During the finale, the 38-year-old singer also re-sang his battle round song “Signed, Sealed, Delivered I’m Yours” by Stevie Wonder and Adele‘s smash hit “Set Fire to the Rain.”

“Wow. Can’t believe this is it. Gonna be a great night! Hope you’ll be watching. #VoiceTop3,” Josh tweeted earlier in the day.

WHAT DO YOU THINK of Josh Kaufman’s performances on The Voice?



Josh Kaufman – ‘Every Breathe You Take’

Click inside for the rest of Josh Kaufman’s performances on The Voice…