A new study suggests being underweight may be even more dangerous than being overweight.

Being too thin may put one at just as high of a risk of death are overweight, especially when other health factors such as smoking and alcohol use are taken into consideration, a St.Michael's Hospital news release reported.

The researchers reviewed 51 studies link between Body Mass Index (BMI) and death from any cause.

The research team found that individuals who were underweight (a BMI of 18.5 or less) had a 1.8 times higher risk of death than those with a BMI in the "normal" range (between 18.5 and 24.9).

The risk of dying is 1.2 times higher in obese (with a BMI between 30 and 34.9) individuals and 1.3 times higher in those who are morbidly obese (a BMI of 35 or higher).

The researchers only looked at studies that followed the participants for at least five years, to factor out those who were underweight due to other health conditions such as cancer or heart disease.

Common causes of low BMI include: "malnourishment, heavy alcohol or drug use, smoking, low-income status, mental health or poor self-care," the news release reported.

"BMI reflects not only body fat, but also muscle mass. If we want to continue to use BMI in health care and public health initiatives, we must realize that a robust and healthy individual is someone who has a reasonable amount of body fat and also sufficient bone and muscle," study leader Doctor Joel Ray, a physician-researcher at St. Michael's Hospital and the hospital's Li Ka Shing Knowledge Institute,said in the news release. "If our focus is more on the ills of excess body fat, then we need to replace BMI with a proper measure, like waist circumference."

"We have obligation to ensure that we avoid creating an epidemic of underweight adults and fetuses who are otherwise at the correct weight. We are, therefore, obliged to use the right measurement tool," he said.

The study was published in the Journal of Epidemiology and Public Health.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.