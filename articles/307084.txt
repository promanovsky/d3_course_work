SEATTLE - T-Mobile (TMUS) is jumping into the music streaming game with a price and a package of features that set it apart.

At its "Un-carrier" event Wednesday, the fourth-ranked mobile carrier said customers of its Simple Choice plan will be able to stream music from popular services like Pandora, Spotify, Rhapsody, iHeartRadio, iTunes Radio and Slacker without it eating into their high-speed data caps.

It also unveiled a new service called unRadio that features streaming Internet radio without ads. The service provided by Rhapsody lets users skip tracks an unlimited number of times and download 25 songs to the device for playback after marking them as favorites when they come up randomly.

T-Mobile's highest-tier customers will get unRadio for free. It's $4 a month for other T-Mobile subscribers and $5 a month otherwise.

"It's designed to be a better Internet radio," T-Mobile CEO John Legere told The Associated Press. He said the main problem with Internet radio services like Pandora is that people dislike the ads and are afraid they'll burn through their data plans by listening to music on the go.

Rhapsody's chief financial officer, Ethan Rudin, said in an interview that the move should help it add paying subscribers to the 1.7 million it has now by bridging the gap between free Internet radio and on-demand subscription plans that cost $10 a month.

"We felt there was a gap in Internet radio and no one has taken the time to get it right," he said.

T-Mobile also announced it will allow people to take home the latest iPhone 5S for a seven-day free trial so they can see if they get good reception at their home and office.

The company said customers can sign up for a trial online starting Monday, and they'll receive a phone in the mail a few days later. After the test period, the phone will become inoperable. Customers that don't return them to stores will be charged $700.

T-Mobile calls the test a "seven-night stand."

"We think everybody should cheat on their carrier," Legere said.

T-Mobile announced the free trial program at the event it dubbed "Un-carrier 5.0" and unveiled the music twist immediately afterward in an announcement Legere called "Un-carrier 6.0."

A horde of screaming music fans were on hand, mainly because the evening was to be capped off with a performance by Washington state natives Macklemore & Ryan Lewis.

T-Mobile unleashed the first phase of "Un-carrier" in March 2013. It replaced two-year service contracts with phone installment plans, giving customers some flexibility. Three months later, the company introduced a plan for frequent phone upgraders that has been copied with some modifications by AT&T Inc. and Verizon Communications Inc.

"Un-carrier" has been a spectacular success in terms of customer appeal. In the 12 months that ended in March, T-Mobile added 6.1 million net customers, up from 112,000 in the previous 12-month period, to 49.1 million. But a big part of the appeal of "Un-carrier" is lower prices, which have cut into T-Mobile's already slim profit margins. That raises questions over whether the strategy is sustainable in the long run, or whether Legere is simply grooming the company for a sale.

Sprint Corp., the third-largest cellphone carrier, is reportedly in talks to buy T-Mobile US Inc. The companies haven't confirmed that, and analysts believe such a link-up would face stiff opposition from the same regulators who blocked AT&T from buying T-Mobile in 2011.

While he wouldn't directly address whether the two companies were discussing a merger, Legere told the AP that consolidation "absolutely makes sense for the industry."

"We need more spectrum, we need more scale," he said. "Ultimately, I think the industry will consolidate."