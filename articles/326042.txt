The test could be incorportated into the NHS smear test in as little as five years [GETTY]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

It could predict not only whether they will get the disease within five to 10 years but also whether it will kill them.

British researchers who made the breakthrough say it could revolutionise the treatment of breast cancer and save millions of lives by identifying who is at risk years before the ­disease develops.

It could also reassure others that they are unlikely ever to suffer from the disease.

Doctors hope the new test could be incorp­orated into the NHS cervical smear testing ­programme in as little as five years’ time.

Actress Angelina Jolie decided to have her breasts removed after discovering she carries a faulty gene responsible for 10 per cent of breast cancers. Now scientists at University College London have discovered that patients whose cancer is not hereditary also carry the same genetic marker in their blood.

Professor Martin Widschwendter, head of the Department of Women’s Cancer at UCL, said: “We identified a signature in women with a mutated BRCA1 gene that was linked to increased cancer risk and lower survival rates.

“Surprisingly, we found the same signature in large cohorts of women without the BRCA1 mutation and it was able to predict breast cancer risk several years before diagnosis.”