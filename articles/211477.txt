Another five people in Saudi Arabia have died after contracting a potentially fatal Middle Eastern respiratory virus that has sickened hundreds in the kingdom, according to health officials.

The Saudi Health Ministry provided the death toll in its latest update on the Middle East Respiratory Syndrome on Monday evening, saying that the deaths occurs in the capital Riyadh and the western cities of Jeddah and Medina.

Two of those who died were among eight new confirmed infections in the three cities, while the other three had been previously diagnosed.

A total of 147 people have died and 491 have been confirmed to have contracted the virus in Saudi Arabia since it was discovered in 2012. Most cases of the disease have been in the desert kingdom.

American health officials this week confirmed a second U.S. case of MERS. The virus was confirmed in a resident of Saudi Arabia who was visiting Florida. He is being treated in an Orlando hospital.

An earlier, unlinked U.S. patient diagnosed with MERS was released from an Indiana hospital late last week.

MERS is part of the coronavirus family of viruses, which includes the common cold and SARS, or severe acute respiratory syndrome, which killed some 800 people in a global outbreak in 2003. MERS can cause symptoms including fever, breathing problems, pneumonia and kidney failure.

Scientists believe camels likely play a role in initial infections. The disease can then spread between people, but typically only when they are in close contact with one another, such as with infected patients and health-care workers.