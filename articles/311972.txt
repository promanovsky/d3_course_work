Curiosity rover takes Mars selfie on first birthday

While the rover itself has technically been in service for far longer than a year, its Mars visit has now lasted one full Martian cycle. That’s a whole cycle around the sun for Mars, also known as a Martian year. To celebrate, the NASA Mars Curiosity rover stretched one of its arms out to take a lovely selfie.

In Earth days, the Curiosity mission has lasted over two years. That’s 687 Earth days and far more than that if you count the time it took for the rover to arrive on the planet.

Click or tap the thumbnail above to access a near-full-sized version of the photo taken on the Mars birthday of the Curiosity rover. The full-sized image can be found at NASA.

Below you’ll see Curiosity mission Rover Driver Matt Heverly and Deputy Project Scientist Ashwin Vasavada speaking about the future of the project. This video is being delivered on the Mars birthday of the project and serves to deliver mission goals and accomplishments as well.

Curiosity has accomplished one mission – discovering proof that life could possibly have existed on Mars at some point or another. While this life would have been microbial – and no longer exists there today – a lake bed was found with proof of the previous existence of water.

The map above shows how far the Curiosity rover has gone since it arrived at Bradbury Landing in 2012. Have a peek at a collection of Mars Curiosity moments in the timeline below to see where this adventure has taken us over the past year.