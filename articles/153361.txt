Users can use the clock icon in the top left to slide through time and select a thumbnail to see that same place in previous years or seasons.(Credit: Google)

MOUNTAIN VIEW, Calif., April 23 (UPI) -- Google has updated the desktop version of Street View to allow users to look at images from older versions of Street View.

The new feature came about after Google saved older images of Japan before the tsunami hit in 2011 in order to have a record of the country before the destruction. The saving of old data was a departure for Google, which works at a feverish pace of updating and forgetting old data.

Advertisement

"We have built this very complicated graph of imagery through time and space," says Luc Vincent, the director of engineering for Street View.

He said that Google Maps users have hounded the company asking to go back and forth between timelines -- initially for simple things like seeing older images of a house, school to their old neighborhood.

RELATED Apple opens up new OS X Beta Seed Program to all Mac users

"People would say, ‘My house, can you please preserve it? Because I like it this way,’" Vincent told The Verge. "We can show you everything now."

Since then, Google has kept older versions of Street View and is now letting users access the data. Street View users will see a tiny clock icon on the Google Maps page. On clicking the icon, a preview page appears letting users scroll throughout a timeline to see older images.

The feature gives users a time-warp effect seeing the same place under different circumstances. It seems Google’s Street View cars pass by most locations about twice per year, with some urban places having more historical images available.

Google says the new update is meant to be part of the company’s effort to “create a digital mirror and true record of the world.”

The update goes live Wednesday and will be only available to desktop users, and it is unclear if it will come to other devices.

RELATED Detroit judge picks Boston firm and NY lawyer to advise on bankruptcy plans