Pregnant Kelly Clarkson was photographed for the first time in four months on Easter Sunday, and Lindsay Lohan revealed she suffered a miscarriage on the season finale of her Oprah Winfrey Network docu-series Lindsay on April 20—plus other top stories from Monday, April 21!

1. Exclusive Photos: Kelly Clarkson Reveals Baby Bump on Boat Day With Brandon Blackstock, Reba McEntire

Bump along, baby! Pregnant Kelly Clarkson was photographed for the first time in four months on Sunday, April 20, when she and her husband, Brandon Blackstock, joined his family for an Easter outing in Nashville, Tenn.

2. Lindsay Lohan Miscarriage: Actress Reveals Lost Pregnancy on OWN Show

Lindsay Lohan made a shocking confession on the season finale of her Oprah Winfrey Network docu-series Lindsay on Sunday evening, April 20. The recovering actress, 27, was asked to reflect on her time filming the series, prompting her to reveal that she suffered a miscarriage during the duration of the show.

3. Real Housewives of Atlanta Reunion: Kenya Moore Might Not Return to RHOA After Porsha Williams Brawl

Kenya cares enough to walk away. Real Housewives of Atlanta firebrand Kenya Moore says in a new interview that she's considering leaving the Bravo reality show after her reunion brawl with castmate Porsha Williams was televised Sunday, April 20.

4.Dean McDermott Calls Affair "Worst Nightmare," Complains About Sex With Wife Tori Spelling in True Tori Clip

The show may be called True Tori, but Dean McDermott seemed to be having a bit of trouble leveling with his wife Tori Spelling in this new clip for the Lifetime show.

5. Richard Gere, Padma Lakshmi Secretly Dating Amidst His Divorce From Carey Lowell

Could Richard Gere's new pretty woman come with a dash of spice and plenty of flavor? After Page Six reported that the 64-year-old actor is dating Top Chef host Padma Lakshmi in the midst of his divorce from his wife of 11 years, Carey Lowell, Us Weekly confirmed the news.