A selfie video that a 49-year-old Toronto-area woman took to show numbness and slurred speech she was experiencing helped doctors to diagnose her as having a mini-stroke, after she had earlier been given a diagnosis of stress.

When Stacey Yepes’s face originally froze and she had trouble speaking in April, she remembered the signs of stroke from public service announcements. After the symptoms subsided, she went to a local emergency room, but the tests were clear and she was given tips on how to manage stress.

The numbing sensation happened again as she left the hospital. When the left side of her body went numb while driving two days later, she pulled over, grabbed her smartphone and hit record.

"The sensation is happening again," the Thornhill, Ont., woman says at the beginning of the video posted on YouTube by Toronto’s University Health Network. "It’s all tingling on left side," as she points to her lower lip, trying to smile.

Yepes remembers that doctors said to breathe in and out and to try to manage stress, and she says she's trying.

"I don’t know why this is happening to me."

About a minute later, she shows that it’s hard to lift up her hand.

"I think it was just to show somebody, because I knew it was not stress-related," she said in an interview. "And I thought if I could show somebody what was happening, they would have a better understanding."

After going to Mount Sinai Hospital in downtown Toronto, Yepes was referred to Toronto Western Hospital’s stroke centre.

"In all my years treating stroke patients, we’ve never seen anyone tape themselves before," said Dr. Cheryl Jaigobin, the stroke neurologist at the hospital's Krembil Neuroscience Centre. "Her symptoms were compelling, and the fact she stopped and found a way to portray them in such a visual fashion, we were all touched by it."

The video convinced doctors that Yepes was clearly having a transient ischemic attack or TIA, also called a mini-stroke.

Yepes had an MRI scan, which Jaigobin said is very sensitive in picking up a stroke. The stroke team identified a small injury and they were able to identify the cause.

Strokes happen at all ages

Jaigobin said Yepes's mini-strokes resulted from atherosclerosis — a buildup of plaque in her arteries. A blood clot formed on the plaque, then blocked a small artery leading to one side of her brain, resulting in paralysis on the opposite side of her body.

Studies suggest that strokes are occurring in people at younger ages, just as high blood pressure, high cholesterol and Type 2 diabetes are, too.

Yepes has been off work and hopes to return in July. Her diet, exercise routine and lifestyle have been given a makeover, she’s on cholesterol-lowering medication and blood thinners and she’s participating in the hospital’s stroke rehabilitation program.

The Heart and Stroke Foundation gives five warning signs of stroke:

Weakness: Sudden loss of strength, numbness in face, arm or leg, even if temporary.

Trouble speaking: Sudden difficulty talking or understanding spoken language, even if temporary.

Vision problems: Sudden trouble with vision, even if temporary.

Headache: Sudden severe or unusual headache.

Dizziness: Sudden loss of balance, especially with any of the above signs.

Doctors advise people to dial 911 at the first sign of a stroke.