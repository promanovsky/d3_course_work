Sony Corp (ADR) (NYSE:SNE) (TYO:6758) brought its PlayStation Network back up today after a strong cyber-attack took it offline over the weekend. At the same time as that attack, Reuters reports that there was a bomb scare on the same commercial flight that a top Sony executive was taking to the U.S.

PlayStation network hit by DDoS attack

PlayStation’s official blog states that its network was taken down by a denial of service attack. The gaming service said there are no signs of any intrusion into users’ personal information. The blog also states that because of the attack, the PlayStation network will not go offline for the regularly scheduled maintenance, which was due to take place today.

Livermore Strategic Opportunities February 2021 Update Livermore Strategic Opportunities, LP performance update for the month of February 2021. Q4 2020 hedge fund letters, conferences and more Many of you are witnessing first hand that our country, economy, (and now stock market) are all very fractured and becoming extremely challenged. Therefore, our hedge fund's theme remains focused on specific sectors and companies. Read More

Sony said it would give another update after it rescheduled the regular maintenance.

Twitter user claims responsibility

Twitter user @LizardSquad claimed responsibility for taking down the PlayStation network, as the user said he wanted to pressure Sony to inject more of its overall profits into the network:

Sony, yet another large company, but they aren’t spending the waves of cash they obtain on their customers’ PSN service. End the greed. — Lizard Squad (@LizardSquad) August 24, 2014

There’s also a tweet using a derogatory term for non-Muslims that suggests the user wants the bombing of the ISIL to stop.

The Twitter account has multiple posts that reference ISIS and other terrorist organizations. The user claims to have taken Vatican City “offline” and references Microsoft Corporation (NASDAQ:MSFT)’s Xbox Live network, which some users reportedly had trouble getting into on Sunday.

There’s also a post from Sunday that claims the hacker group Anonymous took over the @LizardSquad Twitter account.

Sony executive’s flight threatened

The same Twitter user also claimed responsibility for the bomb scare on Sony executive John Smedley’s flight. Officials diverted the plane, and all the passengers exited safely.

The Twitter account tweeted the bomb scare to American Airlines on Sunday, saying that explosives may be on board a flight boarded by Smedley. The tweet came after another tweet from a forum for gamers that threatened to “send a bomb on your plane.”

The FBI is investigating the incident, sparking taunts by @LizardSquad account.