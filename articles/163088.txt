Eleven new cases of the occasionally fatal Middle East Respiratory Syndrome, or MERS, were reported in Saudi Arabia on Wednesday, two days after the nation’s health minister was replaced.

In the week ending Monday, 67 cases were reported of the SARS-like virus. One patient died in Riyadh on Monday, the same day that King Abdullah replaced minister Abdullah Rabeeah. Saudi official news outlets reported no specific reason for the ouster.

Since the outbreak began in 2012, 272 people have been infected in the oil-rich kingdom, with 81 of them dying.

MERS, which originated in camels and usually causes coughing, fever and pneumonia, currently cannot be treated with a vaccine or anti-viral measures. But international and Saudi health authorities say it is not easily transmitted and that the outbreak could tail off in time. It can be transmitted either from infected persons or camels, which are common in most Saudi cities.

Advertisement

Labor Minister Adel Fakieh will serve as acting Health Minister. Rabeeah, in a news conference Sunday, emphasized that no infections have been reported in schools, and he downplayed concern that MERS could further spread after the arrival of millions of pilgrims for the Muslim fasting month of Ramadan, in July this year.

Millions more take part in the annual hajj in Mecca and Medina, in October. No infections were reported during last year’s hajj.

This month, the United Arab Emirates reported 10 laboratory-confirmed cases of MERS a few days after Yemen reported its first case of the virus. Cases have also been reported in Qatar, Kuwait, Jordan, Oman and Tunisia in addition to several European countries.

The World Health Organization last week reported 93 deaths worldwide since 2012.

Advertisement

sherif.tarek@latimes.com

Tarek, a reporter from Cairo, is a visiting journalist at The Times sponsored by the Daniel Pearl Foundation in partnership with the Alfred Friendly Press Fellowships.