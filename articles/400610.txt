Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Running for just seven minutes every day can slash your risk of dying from heart disease, research reveals.

The findings from a study of 55,000 people tonight showed for the first time how healthy exercise does not necessarily have to be time-consuming.

Researchers followed the adults - aged between 18 and 100 - over 15 years.

During this period, more than 3,000 died, of which 1,217 deaths were linked to heart disease.

Runners, who made up just under a quarter of those studied, had a 30% lower risk of death from all causes and a 45% lower risk of death from heart disease than non-runners. They also lived an average three years longer.

Running for less than 51 minutes a week - or about seven minutes a day - fewer than six miles, reduced the chances of dying.

And those who ran for less than an hour a week benefited just as much as those running more than three hours a week.

US lead scientist Dr Duck-Chul Lee, of Iowa State University, said: “Since time is one of the strongest barriers to participate in physical activity, the study may motivate more people to start running and continue to run as an attainable health goal for mortality benefits.

“Running may be a better exercise option than more moderate intensity exercises for healthy but sedentary people since it produces similar, if not greater, mortality benefits in five to 10 minutes compared to the 15 to 20 minutes per day of moderate intensity activity that many find too time consuming.”

The findings were tonight published in the Journal of the American College of Cardiology.

Christopher Allen, senior cardiac nurse at the British Heart Foundation, said: "Protecting ourselves against life-changing conditions like a heart attack or stroke should be everyone's top priority.

"But the reality is not everyone is managing to achieve their 150 minutes of physical activity a week.

""What this study proves is that when it comes to keeping physically active, every step counts towards helping you maintain a healthier heart.

"Breaking your exercise down into 10-minute chunks can make this goal much more achievable and can help prolong your life by reducing your risk of dying from cardiovascular disease."