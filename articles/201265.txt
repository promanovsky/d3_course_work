Do you remember the day the Internet went on strike against a bad piece of legislation? It may happen again. The Internet dream team behind 2011's SOPA protests has sent what may be the opening salvo in another pressure campaign, this time to stop the Federal Communications Commission's reportedly proposed "Open Internet" rules.

Internet Opposition to FCC's New Rules

More than 100 Internet sites co-signed an open letter to the FCC warning that its purported new Open Internet rules -- put together to replace the 2010 Open Internet rules that were struck down by a federal court in January -- "represents a grave threat to the Internet."

Among the 100 Internet co-signees are dozens of startups and a couple of sites and products you may have heard of: Google, Facebook, Amazon, Microsoft, Twitter, Netflix, Yahoo, Ebay, Dropbox, LinkedIn, Foursquare, Etsy, Reddit, Tumblr, Mozilla, BitTorrent, Digg, Pocket, Imgur, Meetup, and Lyft, along with some important "Internet backbone" companies like Vonage, Cogent and Level 3 -- the network interconnection middle-man which recently announced that, based on its research, up to five major U.S. Internet service providers were already purposely slowing Internet connections.

Notably absent from the list of big IT co-signing companies were Apple and Wikipedia -- the latter of which caused a big splash during the 2011 SOPA protest by making its articles inaccessible for a day, which affected a lot of government staffers who may not have noticed how much they rely on it every day.

The open letter to the FCC called on the commission to take more of a stand against the "fast lane" idea -- the name given to a part of the purported new FCC rules, which would allow ISPs to prioritize traffic for sites and services that can afford to pay extra. As we have argued, this would be anticompetitive and ruin the Internet as we know it. Apparently, Google, Facebook, Amazon, and others agree:

"Instead of permitting individualized bargaining and discrimination, the Commission's rules should protect users and Internet companies on both fixed and mobile platforms against blocking, discrimination, and paid prioritization, and should make the market for internet services more transparent. The rules should provide certainty to all market participants and keep the costs of regulation low."

"Such rules are essential for the future of the internet," continued the letter. Whether or not the companies mount a SOPA-like protest against the FCC's new rules remains to be seen, though there have been rumors of such action already, and the companies officially declaring their opposition is the first step towards that.

Opposition in Government

The coalition of Internet sites aren't the only notable critics of the FCC's new policies. One is Senator Al Franken, who joined the NoSlowLane.com campaign with a stern video calling net neutrality "the free speech issue of our time."



Another, Jessica Rosenworcel, who expressed her concerns in a speech to a national library association on Wednesday, is actually commissioner within the very same agency. "Libraries, of course, know that an open Internet is important for free speech, access to information, and economic growth. I also support an open Internet. So I have real concerns about FCC Chairman Wheeler's proposal on network neutrality-which is before the agency right now," said Rosenworcel.

She went on to call for a month delay on the process of considering the Open Internet rules, in order to respect the "torrent of public response" against the new policy.

The FCC will vote on the new rules at a meeting on May 15. If the FCC goes ahead with its planned meeting, which it looks like it will, that means there's only a week before the rules governing the Internet change dramatically. Expect an interesting week on the web.

For more stories like this, follow Latin Post Tech on Twitter.