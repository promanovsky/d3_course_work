The Vatican bank has blocked the accounts of over 2,000 clients and ended some 3,000 "customer relationships" as part of a clean-up process that nearly wiped out its profit.

This is according to its 2013 financial statement, published today.

The bank has been hit by years of scandal, including money laundering allegations and is about to be restructured with a new president and a new board.

All but about 400 of the 3,000 terminated accounts were "dormant" with small balances and had been inactive for years.

359 customer relationships are due to be terminated after staff and outside experts found they did not meet the criteria for holding accounts at the bank, which has about 15,500 customers, some with more than one account.

The stated purpose of the bank - formally known as the Institute for Works of Religion (IOR) - is to manage money for the Church, religious orders, charities and Vatican staff.

The statement showed that the ongoing clean-up process, started by its President Ernst von Freyberg after his arrival last year, has come with a heavy financial price, particularly for ridding the IOR of some dubious investments.

Profits in 2013 plummeted to €2.9m from €86.6m in 2012 as the IOR took huge write-downs to wind up investments made before the bank's reform programme started and when there was less vigilance.

Last year's profits were also hit by extraordinary expenses related to the hiring of external professionals, such as the Promontory Financial Group, to help in compliance and transparency issues and account closures.

The statement said 2013 profit would have been about €70m without the write-downs and the one-off expenses.

The Vatican has been saying for months that the 2013 figures would be bad.

Freyberg said in a statement he had "proceeded with zero tolerance for any suspicious activity", and that the "painful but very necessary process has opened the door for a new, unburdened future of the IOR".

The closing of accounts and the reform process in general resulted in an outflow of about €400m, which the statement said had been done in a way to "ensure traceability". Most of the money - 88% - had been moved to Italian financial institutions.

The statement indicated the IOR had turned the corner after the costly clean-up, saying that results for the first six months of 2014 showed a net profit of €57.4m.

This puts it on track to beat 2012 profits but that figure is expected to be hit by impending structural changes.

Australian Cardinal George Pell, who heads the Secretariat of the Economy set up this year to oversee Vatican finances and stem scandals, will announce a major down-sizing this week.

He is expected to end the IOR's asset management business and retrict its remit to providing payment services and financial advice for religious orders, charities and Vatican employees, sources said.

French businessman Jean-Baptise de Franssu is expected to be named as the new head and the Holy See's assets are expected to be managed by a newly created department, the sources added.