Scientists have discovered nearly 600 methane vents off the eastern seaboard of the United States, along the continental margin, which is the region of the ocean floor lying between the coast and the deeper Atlantic Ocean, according to a paper published Sunday in the journal Nature Geoscience.

“It is the first time we have seen this level of seepage outside the Arctic that is not associated with features like oil or gas reservoirs or active tectonic margins,” Adam Skarke, a geologist at Mississippi State and the lead author of the study, told BBC. “This is a large amount of methane seepage in an area we didn’t expect.”

The team of researchers, which studied an area covering over 35,000 square miles, discovered 570 methane vents, called seeps, off the coast between North Carolina and Massachusetts, at depths ranging from 50 meters to 1,700 meters. The paper claimed that “tens of thousands of seeps could be discoverable” worldwide, raising fears that potential sources of greenhouse gases, including methane, may have been underestimated.

“But it is important to say we simply don't have any evidence in this paper to suggest that any carbon coming from these seeps is entering the atmosphere,” Skarke reportedly said.

The scientists observed streams of bubbles, which have not yet been sampled, emanating from icy sludges called methane hydrates. These hydrates, believed to be the world’s largest reservoirs of natural gas, form when methane produced by microbes living within sea-floor sediments becomes trapped in ice. This ice may now be slowly breaking down because of the warming of overlying waters, the paper said.

“The methane is dissolving into the ocean at depths of hundreds of meters and being oxidized to CO2,” Skarke said. Methane reacts with dissolved oxygen in the water and creates carbon dioxide that acidifies the surrounding waters.

The discovery of the methane vents “highlights a really key area where we can test some of the more radical hypotheses about climate change,” John Kessler, a professor at the University of Rochester, who was not involved in the research, told The New York Times, adding that further “long-term studies” were needed to assess the link between climate change and the release of methane underwater.