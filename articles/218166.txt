Beyonce and Jay Z have just released a star-studded new video that serves as a spoof trailer for a movie called Run… that is “coming never.”

The video features celebs such as Sean Penn, Don Cheadle, Guillermo Díaz, Emmy Rossum, Jake Gyllenhaal, Blake Lively, Rashida Jones, and Kidada Jones.

It is set to the couple’s song “Part II (On The Run)” and is seemingly a new video to promote their upcoming On The Run Tour, which kicks off this summer.

Check out a ton of stills from the video in the gallery below!



Beyonce & Jay Z Release New Star-Studded Fake ‘Run’ Trailer!

FYI: Beyonce is wearing Levi‘s 501 shorts in the video.

20+ stills inside from Beyonce and Jay Z‘s brand new video…