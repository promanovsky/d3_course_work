Photo Coverage: Anthony Nunziata Brings LOVE SONGS FROM BROADWAY Live to the WICK Theatre

Anthony Nunziata is already known to area audiences for his performance collaborations with his identical twin brother Will. While the two brothers still perform together, Anthony has also launched a flourishing solo career as a vocalist, songwriter and recording artist.