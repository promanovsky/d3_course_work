TV

In a statement, the 'Today' host says he 'couldn't be happier to be staying' on the network.

Jun 14, 2014

AceShowbiz - Matt Lauer is staying on NBC. The "Today" anchor, who is also a contributor for "Dateline", has signed a new contract that will keep him on the morning show for multiple more years. The New York Times first reported the news.

In a statement, Deborah Turness, the president of NBC News, said, "We couldn't be more thrilled with Matt's decision. As I've said many times before, he's the best in the business, and there is nobody I would rather have in the 'Today' anchor chair than Matt."

Lauer also released his own statement, saying, "I consider this the best job in broadcasting. I love people I work with every day and I have such respect and gratitude for the people I work for. I couldn't be happier to be staying."

Details of the new deal are not disclosed. Lauer's previous deal was to end in the next few months.

The 56-year-old journalist joined "Today" as a co-host in 1997, replacing longtime host Bryant Gumbel. He was highly criticized by public following Ann Curry's departure from the show in 2012 and at one point was reported to leave the show.