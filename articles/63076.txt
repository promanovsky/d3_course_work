Friday sees the release of Darren Aronofsky’s controversial “Noah,” based on the well-known Bible story of the great flood in Genesis. Already this year, the word of God has popped up on the big screen with the life of Jesus in “Son of God,” and come December, Christian Bale will star as Moses in “Exodus: Gods and Kings.”

It got us thinking: What other biblical stories are ripe for new silver screen adaptations? Here are our fantasized best bets.

David and Goliath

Based on: 1 Samuel, Chapter 17

Synopsis: In this hilarious twist on the classic tale, we finally get to hear Goliath’s side of the story. Kevin James stars as the bumbling giant with a heart of gold who gets in with the wrong crowd. When an obnoxious little man-child named David (played by Adam Sandler) begins to taunt and harass him, an epic battle ensues. Remember, things are not always as they seem.

The Destruction of Sodom and Gomorrah

Based on: Genesis, Chapter 19

Synopsis: A thriving community of hard-partying biblical gay people (led by Neil Patrick Harris in a mesh robe) decide that life is more fun when living in sin. Their annual cities-wide orgy comes to an abrupt conclusion when God (Morgan Freeman, obvs) destroys the land with fire and brimstone, in a Michael Bay-esque explosion fest. Everyone dies, except Lot (Dwayne “The Rock” Johnson), a hetero who was forced to hide his attraction to women and whom, therefore, God saves.

Daniel and the Lion’s Den

Based on: Daniel, Chapter 6

Synopsis: Jonah Hill stars as a wisecracking Daniel is this comedic action adventure. When he’s caught praying to God instead of the flamboyant king of Babylon (Nathan Lane), the king throws him into a pit of lions to be eaten. Little does the king realize these lions are a bunch of CGI-enhanced jokesters who totally get Daniel’s sense of humor and love of God. They team up to take over the kingdom.

Mary Magdalene

Based on: The Gospels

Synopsis: Beyoncé Knowles stars as the original gangsta of the oldest profession in this wide-ranging biopic. This musical serves as Magdalene’s origin story, taking us through the stages of her career with instant classics like the glittery show-stopper “Tough Row to Hoe” and the sure-to-be-Oscar-nominated ballad, “Hook Before You Leap.” When Magdalene meets Jesus (Kanye West as himself), her life changes forever.

Cain and Abel

Based on: Genesis, Chapter 4

Synopsis: The sons of Adam and Eve, Cain (Liam Hemsworth) and Abel (Chris Hemsworth) were always at the top of their class, always the first ones picked for biblical dodgeball (they used rocks), and just all-round ideal children. But once they get to high school, Abel’s popularity starts to outshine Cain’s because Abel loves charity work and Cain just loves to get drunk and feel up girls behind the temple. Growing increasingly jealous, Cain devises a master plan to murder his brother. But will he succeed, and more importantly, what will that do to his chances of becoming prom king?