The Oppo Find 7 has finally been launched by the Chinese manufacturer and it means we now have two QHD (2K) display smartphones on the market (the Vivo Xplay 3S being the other).

With a resolution of 1440 x 2560 on a 5.5-inch display the Find 7 delivers a staggering 538ppi. To put that in some perspective the 5.1-inch Galaxy S5 boasts 432ppi while the 4-inch iPhone 5S clocks in at just 326ppi.

Oppo has managed to squeeze in a 2.5GHz quad-core Snapdragon 801 processor, 3GB of RAM, Adreno 330 GPU and 32GB of internal storage which can be expanded by up to 128GB with a microSD card.

There's plenty on power to put it up against the Sony Xperia Z2 and Galaxy S5, and a 3000mAh battery is tasked with keeping everything going.

Camera confusion

Oppo is boasting that the Find 7 has a 50MP camera, but if you flip the handset over you'll notice it only sports a 13MP lens (and dual-LED flash).

The Find 7 comes with a "Super Zoom" software enhancement suite, which stitches together four images into one 50MP image. When you hit the shutter the phone will take 10 quick-fire shots and automatically select the best four for your super image.

For selfie fans there's also a front-facing 5MP camera, while in terms of OS the Find 7 comes running Android 4.3 Jelly Bean with Oppo's Color OS 1.2 interface stuck over the top.

The Oppo Find 7 release date is expected to be around May or June, but if you're in China and can't wait that long you can always pick up the Find 7a in mid-April which sports a slightly older 2.3GHz Snapdragon 800, 2GB of RAM, 16GB of storage, 2800mAh battery and 1080p display.

There's currently no word on whether the Oppo Find 7 will make it out of China, but we'll update this article once we've found out.