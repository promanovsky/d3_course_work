Dark chocolate could make walking easier for the elderly: Eating a bar can help patients with artery problems in their legs

Patients with Peripheral artery disease, which affects leg arteries, tested



Twenty people aged 60 to 78 could walk better after eating dark chocolate



Researchers found elderly people with artery problems in their legs could walk a little longer after eating dark chocolate

For most of us dark chocolate is just a tasty treat – but for the elderly it could also help them to walk, according to a study.

Researchers found elderly people with artery problems in their legs could walk a little longer after eating dark chocolate.

Peripheral artery disease, or PAD, is a cardiovascular disease most commonly affecting the arteries of the legs and is strongly associated with age, with high rates in over-70s.

Reduced blood flow can cause pain, cramping or fatigue in the legs or hips while walking.

In a pilot study of patients with PAD, 14 men and six women aged from 60 to 78 increased their ability to walk unassisted after eating dark chocolate.



On separate days, they were tested on a treadmill once in the morning and again after eating a 40g bar of dark or milk chocolate.

After the dark chocolate they walked on average 39 feet further and 17 seconds longer than earlier in the day. There was no improvement after eating milk chocolate.

The researchers, whose findings were published in the Journal of the American Heart Association, suggest that compounds found in cocoa – polyphenols – may reduce oxidative stress and improve blood flow in peripheral arteries. improve arteries’ blood flow.

The dark chocolate in the study had 85 per cent cocoa so was rich in polyphenols. The milk chocolate had less than 30 per cent cocoa.

The researchers said that a larger study with long-term consumption is now needed to confirm the improvements.

The improvements were modest. However, the benefit of dark chocolate polyphenols is 'of potential relevance for the quality of life of these patients,' according to the study’s co-author Doctor Lorenzo Loffredo, assistant professor at the Sapienza University of Rome in Italy.

Levels of nitric oxide - a gas linked to improved blood flow - were higher when participants ate dark chocolate. Other biochemical signs of oxidative stress were also lower.



Based on these observations and other laboratory experiments, the researchers suggest that the higher nitric oxide levels may be responsible for dilating peripheral arteries and improving walking independence.



Doctor Francesco Violi, study senior author and professor of internal medicine at the Sapienza University of Rome, said: 'Polyphenol-rich nutrients could represent a new therapeutic strategy to counteract cardiovascular complications.'



In the pilot study of those with Peripheral artery disease, 14 men and six women aged 60 to 78 increased their ability to walk unassisted after eating dark chocolate

The researchers said the improvements linked to these compounds in dark chocolate need to be confirmed in a larger study involving long-term consumption.



American Heart Association spokesman Dr Mark Creager said that it’s far too early to recommend polyphenols or dark chocolate for cardiovascular health.

He said: “Other investigations have shown that polyphenols including those in dark chocolate may improve blood vessel function.



“But this study is extremely preliminary and I think everyone needs to be cautious when interpreting the findings.



“We know from other studies of antioxidants - vitamin C and vitamin E for example - that these interventions have not gone on to show improvement in cardiovascular health.”