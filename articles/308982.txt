Microsoft is providing not just business users but Office 365 Home and Personal subscribers with 1TB of OneDrive storage for free as well. UPI/Alexis C. Glenn | License Photo

REDMOND, Wash., June 23 (UPI) -- Microsoft entered the cloud storage battle by giving OneDrive users 15GB of free storage on its cloud service and up to 1GB for its Office 365 subscribers.

After Apple and Amazon announced plans to give users more cloud storage, Microsoft, which has made cloud services one of its primary focuses, said subscribers can get 100GB of storage for $1.99 a month, $3.99 for 200GB and 1TB of storage for only $6.99 with the Office 365 Personal version and for $9.99 with the Home version.

Advertisement

Microsoft allows users to share their subscription with up to five people, giving them a total of 6TB of cloud storage, whereas previously Microsoft would grant only 20 GB per person.

The company announced the aggressive pricing for cloud storage after Apple announced revised rates for its iCloud service at WWDC earlier this month. Apple gives users 5GB free, charging $0.9 for 20GB. The company doesn't have a 100 GB offering but charges the same rate as OneDrive for 200GB, $3.99.

Amazon last week announced that it will give Fire Phone users unlimited free cloud storage for pictures, but not videos and other kinds of files.

Google also offers similar pricing as OneDrive and also partners with Android and Chromebook manufacturers to offer customers additional storage when they buy Android-run devices.

OneDrive is lagging behind Apple, Google, Dropbox and Box, when it comes to providing cloud storage. Dropbox and Box offer more business-friendly tools that make them more attractive to small businesses.