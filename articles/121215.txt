Dutch semiconductor equipment maker ASML Holding NV (ASML) reported a surge in its first-quarter 2014 net income, on a GAAP basis, to 249.1 million euros, from 96.2 million euros a year before. In accordance with IFRS, quarterly net income totaled 283.3 million euros, higher than 155.7 million euros last year.

The firm clocked IFRS net sales of 1.40 billion euros in the quarter, a significant increase compared with 892.1 million euros in the previous year.

The company said its first-quarter sales came in as expected, driven mainly by memory customers; with its gross margin being better than expected owing to a favourable product mix compared with what it had projected and the contribution of its Holistic Lithography products.

Looking ahead to the second quarter of 2014, ASML projects net sales of around 1.6 billion euros, including EUV sales, a gross margin of 44 - 45 percent, R&D costs of about 270 million euros, other income of about 20 million euros, including contributions from participants of the Customer Co-Investment Program, and SG&A costs of nearly 85 million euros.

Commenting on the future, ASML stated: "Sales in the second quarter are however expected to be affected by adjustments of system demand from some logic customers. This means first-half 2014 sales will be around EUR 3 billion including sales from Extreme Ultraviolet (EUV) systems, versus earlier guidance of around EUR 3 billion which excluded EUV."

As part of the company's policy to return excess cash to shareholders via. dividend and regularly timed share buybacks, ASML announced its plans of purchasing up to 1.0 billion euros of its shares in 2013-2014. Pursuant to this program, ASML has purchased 6.9 million shares for 447 million euros through March 30, 2014. The repurchased shares would be cancelled.

For comments and feedback contact: editorial@rttnews.com

Business News