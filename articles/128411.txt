Updated at 5:20 pm ET

NEW YORK - Chinese social media company Weibo Corp.'s (WB) shares soared in their U.S. market debut Thursday.

After pricing American depository shares at $17, Weibo's stock price rose 19 percent Thursday to close at $20.24. The company raised $285.6 million, which was less than anticipated. Weibo had projected an offering of 20 million shares priced between $17 and $19. Its shares trade on the Nasdaq under the symbol "WB."

Weibo was launched four years ago by Chinese online media company Sina Corp. Weibo provides a Twitter-like service that allows users to post a feed of up to 140 Chinese characters to share with others. Users can also attach multimedia, such as photos and videos, to their posts.

The company has 61.4 million average daily active users, according to its filing with the U.S. Securities and Exchange Commission.

Weibo is incorporated in the Cayman Islands but conducts business in China. Chinese online media company Sina Corp., Weibo's parent, remains a majority owner. It also trades in the U.S.

Weibo is one of a number of Chinese companies that have recently gone public in the U.S. or that plan to list their shares on an American exchange.

Alibaba, China's largest e-commerce company, is expected to file its prospectus to go public as early as next week in what could emerge as the biggest tech IPO of all time.

The company, which will list on the New York Stock Exchange, is expected to be valued at between $150 billion to $200 billion. Facebook's (FB) 2012 IPO valued the social network at roughly $104 billion.