NEW YORK (CBSNewYork) — Alzheimer’s Disease is the sixth leading cause of death in the U.S. and a new report finds the disease affects many more women than men.

New figures from the Alzheimer’s Association show a woman’s risk for getting the disease is 1 in 6 compared to 1 in 11 for men, Dr. Max Gomez reported Wednesday.

“Women in their 60s are actually twice as likely over the course of their life to develop Alzheimer’s disease as they are breast cancer,” said Dr. Maria Carrillo of the Alzheimer’s Association.

Experts said more research is needed to figure out why women are disproportionately affected.

“Age is the greatest risk factor and women living longer is our primary reason right now for attribution of that difference,” Carrillo said.

More awareness is critical. The report finds about a quarter of people believe they are only at risk if it runs in their family.

Even more frightening is that Alzheimer’s numbers are projected to soar as baby boomers grow older, Dr. Gomez reported.

Right now, there are about 5 million sufferers. By 2050, that could more than triple to 16 million patients, Gomez reported.

The cost could also skyrocket from $214 billion to $1.2 trillion by 2050, which will bankrupt the health care system unless we find a cure or treatment, Gomez reported.

You May Also Be Interested In These Stories:



[display-posts category=”health” wrapper=”ul” posts_per_page=”4″]