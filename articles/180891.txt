SAN FRANCISCO — Soon, you will be able to shop on Amazon by Tweeting.

Amazon on Monday said it has tapped Twitter for a new system that would let users connect their Amazon and Twitter accounts. By using the hashtag #AmazonCart, they could then send an Amazon product on their Twitter feed to their online shopping cart.

“Enjoying your Twitter feed, but don’t want to forget to purchase something important? Now Amazon let’s you add items to your cart directly from Twitter,” a YouTube ad explaining the system says.

There is one catch: Your Twitter followers may see what you’ve added to your Amazon account.

“Most content is public on Twitter, so your #AmazonCart replies will be visible to whomever you replied, to those viewing the conversation, and on your own Timeline, unless your Twitter account is set to private,” Amazon said in a blog post.

The Twitter tie-up follows Amazon’s quarterly report in which the Internet giant put out a disappointing outlook amid worries about its costly expansion plans.

On the other hand, Twitter’s weaker-than-expected user growth spooked investors despite the microblogging site’s surging revenue.

This story originally appeared on Marketwatch.com