Twitter on Thursday announced plans to roll out real-time notifications on its Web version, so you'll always know when someone is engaging with your tweets.

"When it comes to your interactions on Twitter, it should be easy for you to stay connected to what's relevant," Twitter's Michael Ducker wrote in a blog post. "With this in mind, we're bringing you real-time notifications on Twitter.com."

This means that when you're logged in on Twitter.com, you'll receive a pop-up notification in your browser if someone has replied, favorited, or re-tweeted one of your tweets. You can also opt to get notifications for direct messages and new followers.

The new pop-ups are "fully interactive," meaning you can reply, favorite, retweet, and follow other users right from the notification, without having to leave the screen you're on.

On the down side, the new alerts might get a little annoying if your tweets spark a lot of engagement from other users. If that's the case, you can head over to the Settings menu on Twitter.com to choose which types of notifications you want to see on the Web, email, and mobile. You can, for instance, set it so you receive an alert when you're mentioned in a tweet, but not when people mark your tweets as favorites.

Meanwhile, if you use a Twitter app, you can also specify which types of push notifications you receive from the Settings menu.

If you don't see the real-time notifications on Twitter.com right away, don't fret. This new feature should be making its way to all Twitter users over the "coming weeks," the company said.

The new notifications come after Twitter earlier this week began rolling out Facebook-esque Web profiles, which should also reach everyone in the coming weeks. The company also recently added support for emojis on the Web.

Further Reading