Wolverine and company have been given a final trailer for X-Men: Days of Future Past, ahead of its release next month.

Bryan Singer's mutant epic sees the casts of the original trilogy of X-films meet the cast of Matthew Vaughn prequel X-Men: First Class, in an attempt to prevent an apocalyptic future.

It's easily the best trailer so far, showing a lot of unseen footage set to a version of Led Zeppelin's Kashmir. While it has been labelled as the final trailer, expect a bunch of TV spots to appear ahead of its release in little over a month.

The film opens in a dark future where the elder versions of Professor X (Patrick Stewart) and Magneto (Ian Mckellan) have gathered their mutant forces for a final desperate plan. That plan involves sending Wolverine's (Hugh Jackman) conscious back in time to the body of his 1973-self.

There he meets a younger Professor X (James McAvoy) and sets out to prevent an assassination that leads to the rise of the Sentinels, a robotic mutant-oppressing force created by Bolivar Trask (Peter Dinklage).

McAvoy plays a bitter and broken Charles Xavier, who Wolverine needs to force into action. They'll need help though, which takes them to Michael Fassbender's Erik Lehnsherr, aka Magneto and Jeffiner Lawrence's Mystqiue.

Halle Berry, Ellen Page, Nicholas Hoult, Anna Paquin, Shawn Ashmore, Omar Sy, Evan Peters and Daniel Cudmore also star in the film set for release on 22 May.