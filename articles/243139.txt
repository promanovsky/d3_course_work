Paul McCartney has canceled his Japanese tour due to a nagging illness the former Beatle just can't seem to kick.

The 71-year-old originally cancelled two shows of the Japanese tour, set to take place at Tokyo's National Stadium, but now Billboard is reporting that his condition has not improved and the entire week-long tour has been scrapped.

"I'd like to thank my Japanese fans for their love, messages of support and understanding," McCartney said in a statement. "I hope to see you all again soon. Love, Paul."

Sir Paul does however expect to be up and running on his 'Out There' tour next week. His official website still lists a May 28 tour stop at Seoul's Jamsil Sports Complex as moving forward as initially planned.

For comments and feedback contact: editorial@rttnews.com

Entertainment News