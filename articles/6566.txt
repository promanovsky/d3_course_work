Taylor Swift has beaten off competition from The Rolling Stones and Beyonce to return to the top of Billboard's annual list of music money makers.

Swift held the top spot in 2012 but was overtaken by Madonna last year. The 24-year-old earned $39.7m (€28.6m) in 2013 from a combination of music sales, royalties and touring, according to Billboard.

The study compiled data from Nielsen SoundScan, Nielsen BDS and Billboard Boxscore, using standard fees and formulas to work out royalties or management fees.

Top 10 music money makers 2013

Taylor Swift - $39.7m

Kenny Chesney - $32.9m

Justin Timberlake - $31.5m

Bon Jovi - $29.4m

Rolling Stones - $26.2m

Beyonce - $24.4m

Maroon 5 - $22.3m

Luke Bryan- $22.1m

Pink - $20m

Fleetwood Mac - $19m