By Cherri Gregg

NEWARK, N.J. (CBS) — New numbers from the Centers for Disease Control show a significant increase in the number of children diagnosed with autism, with New Jersey ranked number one among the 11 states studied.

According to the new statistics, one in 68 children have a disorder along the autism spectrum.

“In New Jersey, the prevalence is one in 45 children with autism,” says Dr. Walter Zahorodny, an assistant professor of pediatrics at the New Jersey Medical School.

He says the new numbers show a 30-percent increase nationally over the past two years, and a 50-percent increase over the past decade.

But, Zahorodny says, the Garden State leads the pack in part because they keep better records than most states and have a long history of providing services for those with autism.

“New Jersey has a very long history of helping and educating children with autism,” he tells KYW Newsradio. “We have schools that go back 40 years.”

The new numbers also showed boys five times more likely than girls to have autism. Zahorodny says of the ethnic groups, Hispanics are the least likely to be diagnosed with the disorder.

But should parents be concerned?

“I’m familiar with this and it does seem concerning to me,” says Zahorodny. “If parents have concerns about their child, they should contact their pediatrician.”

Zahorodny says there needs to be more research on the causes of autism. A recent study published in the New England Journal of Medicine found evidence that autism could be related to clusters of abnormal cells in certain areas of the brain, and may start to develop in the womb.