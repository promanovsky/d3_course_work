Ryan Lewis is shedding light on an important issue that hits him close to home. Lewis, who is part of an eponymous rap duo with Macklemore, revealed that his mother, Julie Lewis, has been HIV-positive for 30 years.

Lewis opened up about his mother in a post titled "A Message from Ryan Lewis," which appeared on the donation page for the 30/30 project - a non-profit organization founded by the two rappers. The project is seeking $100,000 in donations to build clinics worldwide, with the first planned for Malawi.

The rapper explained that his mother, who he said is the strongest woman he knows, contracted HIV from a blood transfusion while delivering his older sister.

"A huge part of what's made me who I am, is something I haven't talked about in interviews," Lewis wrote. "In 1984, my mom gave birth to my older sister, Teresa. Due to a complicated delivery, she needed a blood transfusion and at that moment, my mom had HIV+ blood put into her body. When she was finally diagnosed, she was given only a few years to live."

Lewis, who is partnering with Seattle nonprofit Construction for Change on the project, added: "We each had a 25% chance of being born HIV+, but we were extremely fortunate (today, the risk of a mother passing HIV to her baby is 2% or less if she is taking medication). Thanks to advanced medicine and healthcare available here in the U.S., my mom has lived despite her odds. "

30/30 project hopes to build clinics in areas such as Kangundo, Kenya; Mbita, Kenya and Ugenya, Kenya. Lewis said donors will receive gifts based on their monetary donation.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.