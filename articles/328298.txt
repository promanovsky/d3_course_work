“This week, we’re starting to take action on the removals requests that we’ve received," a Google company statement said. "This is a new process for us," the company said, noting that each request must be individually processed. The company declined to comment on the number of removal requests it has received so far or how many have been processed. It started accepting requests via a webform shortly after the initial ruling.

According to a FAQ posted on the French version of Google, the company will consider whether results "include outdated information about your private life" and if there's a public interest in the information remaining in search results -- for example, if it relates to financial scams, professional malpractice, criminal convictions, or the public conduct of a government official.

AD

AD

"These are difficult judgements and as a private organization, we may not be in a good position to decide on your case," the FAQ says, acknowledging that the ruling has put Google in the uncomfortable position of making what are essentially censorship decisions.

Some earlier reports suggested Google might display a notice letting users of the search engine know if results have been modified due to the "right to be forgotten." Instead, the company is using a blanket notice on searches for names on its European sites.

"When you search for a name, you may see a notice that says that results may have been modified in accordance with data protection law in Europe," the FAQ reads. "We’re showing this notice in Europe when a user searches for most names, not just pages that have been affected by a removal."

Here's what it looks like on the British version: