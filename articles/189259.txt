Nintendo's (OTC: NTDOY) latest console is struggling to keep up with its competitors. Will that change in 2014?

The company has repeatedly lowered its sales expectations for Wii U, all the while insisting that it would remedy the situation by releasing more games.

That strategy has currently failed. Delays plagued Wii U throughout 2013. Pikmin 3, for example, was delayed several times before its August release. Donkey Kong Country: Tropical Freeze was supposed to arrive in time for Christmas, but was ultimately delayed until February 2014.

Nintendo might have hoped that Super Mario 3D World -- Wii U's second Mario game -- would propel the console to new heights. But it was heavily based on the gameplay of the two-year-old Super Mario 3D Land. In November 2011, Super Mario 3D Land was a unique and groundbreaking game that went on to sell 9.42 million units. It also helped Nintendo increase the sales of the Nintendo 3DS.

Related: Could This Startup Save Nintendo?

Super Mario 3D World was much less successful for Wii U. Only two million units have been sold since its November 2013 release.

Wii U Game Sales Aren't Increasing

Mario is only part of the problem. According to The Verge, consumers actually purchased fewer Wii U games last fall (9.65 million units) than they did when the console was released in November 2012 (11.69 million units). This means that while overall hardware sales continued to go up slowly, interest in Wii U games declined dramatically.

By the time November 2013 rolled around, there should have been more people interested in buying Wii U games -- not less.

Struggling To Gain Traction

Nintendo only sold 310,000 Wii U units during the last quarter. That's a slight decline from the 390,000 units that were sold during the year-ago period.

Thus far, Nintendo has sold a total of 6.17 million Wii U consoles since November 2012. The company estimates that it will sell an additional 3.6 million units over the next 12 months.

GameCube Vs. Wii U

If successful, Wii U's lifetime sales will reach 9.77 million units by the end of March 2015. At that rate, the console might not be able to catch up to GameCube (Nintendo's least successful console), which sold 21.74 million units in five years. To match GameCube's sales, Wii U would have to sell more than 10 million additional units during the latter half of its lifecycle.

Can it be done? Can Wii U meet or exceed GameCube's sales?

Benzinga posed this question to famed game industry analyst Michael Pachter.

Related: Why Investors Shouldn't Hold Their Breath For A Nintendo Turnaround

"I think that the [current] Wii U forecast is reasonable, but we're talking about small numbers," Pachter, the Managing Director of Equity Research at Wedbush Securities, told Benzinga. "If they miss by a million, it sounds huge, but it's really not. I would be surprised if they exceed by much, if at all, but you never know how many people are still waiting for Super Smash Bros. or Mario Kart."

Mario Kart Wii sold 34.46 million units -- the most of any game in the franchise. The last Smash Bros. game sold 12 million units and also set a new record for that franchise.

This is a good sign, but it may not translate to improved Wii U performance at retail.

New Super Mario Bros. Wii sold 27.37 million units on Wii, but New Super Mario Bros. U sold only 3.9 million units on Wii U.

Nintendo's E3 Presence

For the second year running, Nintendo has decided to keep a low profile at the Electronic Entertainment Expo.

In previous years, Nintendo used E3 as a platform to introduce new software and hardware. The company kicked off its presentation with an annual press conference that drew hundreds (sometimes thousands) of attendees. Year after year, this was the event that showed Nintendo had what it takes to compete against Sony and Microsoft.

Nintendo has now replaced that in-person, invite-only media event with pre-recorded online video streams that anyone can watch online. Sony and Microsoft will hold their annual press conferences as planned, both of which will also be streamed online.

"I think Nintendo is hurting right now, and they don't have a lot of good news to share, so it's probably prudent to avoid the media," Pachter said of Nintendo's decision.

Disclosure: At the time of this writing, Louis Bedigian had no position in the equities mentioned in this report.