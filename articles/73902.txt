It would be pretty unthinkable for the CEO of a company to demand the resignation of employees because they supported political or social campaigns with which the boss disagreed.

So what has given a number of Mozilla employees the idea that it’s OK to call for the company’s new chief executive to step down because he supported and contributed to the Proposition 8 campaign?

A few employees tweeted about it, according to the story in the L.A. Times, and those were liberally re-tweeted. An employee went on unpaid leave in protest.

These are people who appear to have an extreme misunderstanding of how both the political system and the workplace operate. Personal is personal, people, and professional is professional. The two are separate.

Advertisement

Chief Executive Brendan Eich is entitled to the political views of his choice, and employees are entitled to theirs. Neither has anything to do with whether they should be allowed to hang onto their jobs or receive a raise or a promotion.

I doubt any of these people who are outraged by Eich’s elevation would think it fair if the situation were reversed — if the new head of a corporation had contributed to the anti-Proposition 8 campaign and was detested for it by employees who felt same-sex marriage was wrong and wanted him to lose his job over it. Apple was among the tech companies that contributed heavily to the anti-8 campaign; surely not all of the late Steve Jobs’ employees agreed with that decision.

Top executives and employees often aren’t perfect political or ideological matches. So what? Both should be able to pursue their beliefs and contribute to the causes and political campaigns of their choice without fear that it will affect their job or pay, as long as it doesn’t represent a conflict of interest.

It’s what Eich does in the workplace that should matter to his employees, not what his personal beliefs are. If he were to show bias against gay or lesbian employees or to somehow try to circumvent same-sex marriage protections — now that Proposition 8 is oh-so-rightfully dead — Mozilla’s employees would have plenty to protest (not to mention the trouble he’d be in with the state).

Advertisement

Protecting the rights of all voters to hold and express divergent opinions is paramount in a free society. Proposition 8 was a horribly divisive campaign over an emotional issue. Its passage was a travesty, and I celebrated when it died. But to think that the people who voted otherwise should lose their jobs, or not be promotable to the top positions in their organizations, sounds like pure discrimination.

ALSO:

The water revolution California needs

Warning: College students, this editorial may upset you

Advertisement

18 years of school, and now I’m an over-educated nanny