It's time again for my weekly gasoline update based on data from the Energy Information Administration (EIA). Rounded to the penny, Regular and Premium are unchanged from last week. Prices have been hovering in a narrow range for the past eight weeks. Regular is up 51 cents and Premium 48 cents from their interim lows during the second week of last November.

According to GasBuddy.com, Hawaii, Alaska and California have Regular above $4.00 per gallon, unchanged from last week, and four states (Washington, Oregon, Connecticut, New York) and DC are averaging above $3.90, down from five states last week. South Carolina has the cheapest Regular at $3.38.

How far are we from the interim high prices of 2011 and the all-time highs of 2008? Here's a visual answer.

Weekly U.S. Gasoline since 2000

The next chart is a weekly chart overlay of West Texas Intermediate , and unleaded gasoline end-of-day spot prices (GASO). WTIC closed yesterday at 105.52, down from 106.02 this time last week.

WTIC vs Brent vs GASO Weekly

The volatility in crude oil and gasoline prices has been clearly reflected in recent years in both the Consumer Price Index (CPI) and Personal Consumption Expenditures (PCE). For additional perspective on how energy prices are factored into the CPI, see What Inflation Means to You: Inside the Consumer Price Index.

CPI Headline and core since 2000



The chart below offers a comparison of the broader aggregate category of energy inflation since 2000, based on categories within Consumer Price Index (commentary here).