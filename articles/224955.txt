FORT COLLINS, Colo. (CBS4) – With the governor’s signature Colorado is now the first state to allow terminally ill patients to have access to medication that is not approved by the government.

Gov. John Hickenlooper stated he doesn’t want patients to die knowing there may have been a drug in existence to help them that they were not allowed to take.

Ron Patterson is a former engineer who never imagined he could become one of the 600,000 people who die each year from cancer.

“They did blood work and when he got the results he called me and said, ‘You better get into Poudre Valley Hospital right now. You got something really bad,’” said Patterson.

His only option was chemotherapy but Hickenlooper’s signing of the nation’s first “Right To Try” bill into law could provide Patterson with more options.

“Patients should be able to try a treatment even though it hasn’t been approved if it’s an attempt to save their life if they’re in a terminal situation,” said Hickenlooper.

Right now there are over 20,000 safe medications making their way through the Food and Drug Administration approval process but only three percent of the sickest patients are eligible for clinical trials.

Grace Taylor with Poudre Valley Hospital says the law could open new doors for terminally ill patients.

“It is frustrating, at times there is regulation that we do have to go through with reason, so hopefully this new bill will help navigate that path a little bit more cleaner, and a little bit more proactively now going forward,” said Taylor.

“It will definitely give people a sense of hope.”

At this point Patterson says he has nothing more to lose in order to gain a fighting chance.

“It just gives me another tool in the war chest to try,” said Patterson.

Although Colorado’s legislation clears the way for drug companies to provide experimental medications to patients outside of clinical trials, drugmakers would not be required to do so. And some critics say the legislation won’t lead to greater access.

Similar measures are pending in Louisiana and Missouri. Arizona voters will decide in November whether to set up a similar program.

Hickenlooper also signed another bill into law this legislative session that educates people on how to take their prescription medications safely.