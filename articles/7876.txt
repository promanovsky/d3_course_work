A test for healthy people to see if they are on track to develop Alzheimer's disease -- is here.

Researchers at Georgetown University in Washington, D.C., say they've developed a blood test that can predict with greater than 90 percent accuracy if a healthy person will develop mild cognitive impairment or Alzheimer's disease within three years.

Described in the April issue of Nature Medicine, the study looks to usher in a new era when those who know they are very likely to suffer the degenerative condition in the future can start developing treatment strategies for Alzheimer's at an earlier stage, when doctors say therapy is more effective at slowing or preventing symptoms.

The test -- which identifies 10 lipids, or fats, in the blood that predict disease onset -- is the first known published report of blood-based biomarkers for preclinical Alzheimer's.

"Our novel blood test offers the potential to identify people at risk for progressive cognitive decline and can change how patients, their families and treating physicians plan for and manage the disorder," Howard J. Federoff, professor of neurology and executive vice president for health sciences at Georgetown University Medical Center and the study's corresponding author, said in a news release.

Federoff explained there have been many attempts to develop drugs that slow or reverse the progression of Alzheimer's disease, but all of them have so far failed. He suggested such drugs were evaluated too late in the progression of the disease.

On the other hand, "the preclinical state of the disease offers a window of opportunity for timely disease-modifying intervention," he said.

The study included 525 healthy participants aged 70 and older who gave blood samples at various points through in the experimenting process.

Over a five-year period, 74 participants met the criteria for either mild Alzheimer's disease or a condition known as amnestic mild cognitive impairment, where memory loss is prominent.

Out of that 74 study subjects identified with elevated Alzheimer's risks, 46 were diagnosed upon enrollment and 28 developed memory-loss or symptoms associated with the milder form of the disease.

In the study's third year, researchers selected 53 participants who had developed cognitive dysfunctions and 53 cognitively-normal matched participants and again drew blood sample to check for differences in the two groups.

That's when the research group discovered the 10 lipids said to have shown the breakdown of neural cell membranes in participants who developed symptoms of the disease.

"The lipid panel was able to distinguish with 90 percent accuracy these two distinct groups: cognitively normal participants who would progress ... within two to three years, and those who would remain normal in the near future," Federoff said. "We consider our results a major step toward the commercialization of a preclinical disease biomarker test that could be useful for large-scale screening to identify at-risk individuals.

Federoff added that he and his team are already working on the next logical step in their work: designing a clinical trial "to identify people at high risk for Alzheimer's to test a therapeutic agent that might delay or prevent the emergence of the disease."