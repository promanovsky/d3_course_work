Over in the US, the 15th of April is the deadline in which people will have to file their taxes. This is pretty much a non-issue for most people, but at the same time for those who have been trading Bitcoin, what does this mean for them? Should they file Bitcoin as currency, capital gains, or can it be taxed at all (we guess this is obvious since it can be considered a source of income, much like trading shares and gaining interest)?

Advertising

These questions have been around for a while as governments have been working and trying to decide what to do about the crypto currency since it’s a relatively new thing and firm regulations have yet to be put into place. Well wonder no more because the Internal Revenue Service (IRS) has since announced that Bitcoin is considered property and will be taxed in the same manner.

As part of the IRS’ Q&A, they stated that “For federal tax purposes, virtual currency is treated as property. General tax principles applicable to property transactions apply to transactions using virtual currency.” Many experts have suggested that Bitcoin should be taxed, and why not? After all many people were making money off it, but until today, there has been no official policy.

We should note that this rule will only apply to the US as other countries around the world might come up with their own solution and could treat the crypto currency in a different way. Then again we reckon that countries should probably attempt to regulate or standardize their perceptions on Bitcoin. That way more people can get into it without having to wonder if they might get into some kind of legal trouble for not declaring their Bitcoin earnings.

Filed in . Read more about Bitcoin and Legal.