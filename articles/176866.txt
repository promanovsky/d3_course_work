A small asteroid about the size of a city bus zipped by Earth at a range closer than the moon early Saturday (May 3), but posed no threat to our planet.

The newly discovered asteroid 2014 HL129 came within 186,000 miles (299,338 kilometers) of Earth when it made its closest approach on Saturday morning, which is close enough to pass between the planet and the orbit of the moon. The average distance between the Earth and moon is about 238,855 miles (384,400 km).

You can watch a video animation of asteroid 2014 HL129's orbit around the sun on Space.com. The asteroid is about 25 feet (7.6 meters) wide, according to NASA's Asteroid Watch project based at the agency's Jet Propulsion Laboratory in Pasadena, Calif. It made its closest approach to Earth at 4:13 a.m. EDT (0813 GMT).

Saturday's close shave by asteroid 2014 HL129 came just days after its discovery on Wednesday, April 28, by astronomers with the Mt. Lemmon Survey team, according to an alert by the Minor Planet Center, an arm of the International Astronomical Union that chronicles asteroid discoveries. The Mt. Lemmon Survey team scans the night sky with a telescope at the Steward Observatory atop Mt. Lemmon in Arizona's Catalina Mountains.

NASA scientists and researchers around the world constantly monitor the sky for potentially dangerous asteroids that could pose a risk of impacting the Earth.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Email Tariq Malik at tmalik@space.com or follow him @tariqjmalik and Google+. Follow us @Spacedotcom, Facebook and Google+. Original article on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.