The "fattest" fast food chains of 2014 have been named by a group promoting healthy eating, with the Cheesecake Factory winning recognition in the Xtreme Eating Awards for serving dishes with the greatest number of calories.

The Center for Science in the Public Interest managed the event, which recognized nine categories of unhealthy meals. The Cheesecake Factory won in three of those categories.

The Bruléed French Toast served at the restaurant chain was found to contain 2,780 calories, five days' worth of saturated fat, more than a day's recommended intake of sodium and 24 teaspoons of sugar, most of it added during baking. The American Heart Association recommends American women consume just six teaspoons worth of sugar each day, and men limit themselves to nine.

The group estimates it would take seven hours of swimming to work off the calories present in just this one dish. The meal consists of fried bread, soaked in custard, covered in powdered sugar and maple syrup, with bacon on the side.

The Cheesecake Factory operates roughly 150 locations, mostly located in shopping malls.

Other "winners" from the Cheesecake Factory include a pasta plate which packs 2,400 calories on a single plate, one brunch item with 2,800 calories and a piece of cheesecake, with 1,500 calories.

Most health professionals recommend males consume less than 2,500 calories a day and women take in fewer than 2,000.

Even some healthy-sounding dishes may not be as good for diner as they sound. The Farfalle with Chicken and Roasted Garlic contains peas, tomatoes, carrots, mushrooms and caramelized onions. However, the recipe also contains a cream sauce that raises total calories in the dish to over 2,400 calories. The meal also contains 1,370 milligrams of sodium, and three days of the recommended doses of saturated fat.

Red Robin Gourmet Burgers offers a meal of an A.1. Peppercorn burger, Bottomless Steak Fries, and Monster Salted Caramel Milkshake. This was found to be the least-healthy meal of all the chains tested by CSPI, with 69 grams of fat (3.5 days' worth), 6,280 mg of sodium, equal to four day's supply, nearly three-quarters of a cup of added sugar, totaling 3,540 calories.

Famous Dave's serves "The Big Slab" of St. Louis-Style Spareribs, packing 4,320 mg of sodium, 54 grams of saturated fat, 14 teaspoons of sugar, and 2,770 calories.

"When French toast is 'Bruléed,' fries are 'bottomless,' and steaks are now garnished with not just one, but two Italian sausages, it's clear that caloric extremism still rules the roost at many of America's chain restaurants," Paige Einstein, dietician at CSPI, said.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.