NEW YORK (MarketWatch) — A late afternoon selloff on Wall Street sent U.S. stocks lower on Tuesday, with the Dow Jones Industrial Average falling by the most points in more than a month.

Some analysts pointed to escalation in Iraq as a possible trigger, while others said that it was a mere profit-taking at the end of the quarter.

The S&P 500 SPX, +0.47% ended the session 12.63 points, or 0.6%, lower, after briefly touching an intraday record. Energy sector stocks were hit the hardest, with the sub sector down 2%.

The Dow Jones Industrial Average DJIA, +0.09% dropped 119.13 points, or 0.7%, to 16,813.13. The Nasdaq Composite COMP, +0.50% lost 18.32 points, or 0.4%, to 4,350.36. As selling intensified, the CBOE Vix index VIX, -6.02% jumped 10% to above 12.

Read the recap of MarketWatch’s live blog of today’s stock-market action.

“Investors have generally paid little attention to the fighting in Iraq. But stocks dropped today due to the possibility of escalating violence and slightly higher oil prices,” said Kate Warne, investment strategist at Edward Jones.

Channing Smith, managing director at Capital Advisors, doubted it was Iraq news that triggered a selloff.

“If Iraq is a concern, why energy sector is sharply down? I think markets simply have been overbought and are now taking a breather, with investors taking profit at the end of the quarter” Channing said.

Stocks initially rose on the back of positive economic reports, including new-home sales, home prices and consumer confidence data, but gains evaporated by afternoon.

New U.S. homes sold at an annual rate of 504,000 in May to mark the fastest increase in six years, the government said Tuesday. Yet the surprising gain -- economists polled by MarketWatch expected a 440,000 increase -- was led by a huge surge in the Northeast. Sales of existing homes in May hit the fastest pace in seven months.

Home prices rose in April as the spring selling season got underway, even as annual growth skidded, dropping to the slowest pace in more than a year, according to data released Tuesday morning. April data show that home-price growth has turned

S&P/Case-Shiller’s price barometer tracking 20 cities showed that year-over-year price growth hit 10.8% in April — a fast pace but down sharply from annual growth of 12.4% in March and a recent peak of 13.7% in November.

A gauge of consumer confidence rose to 85.2 in June -- the highest level since January 2008 -- from 82.2 in May, the Conference Board Tuesday.

Walgreen disappoints, Vertex soars on drug trial news

In corporate news, cruise company Carnival Corp. CCL, -0.58% beat consensus profit and revenue estimates and raised its full-year outlook. However, shares fell 3%.

Shares of Walgreen Co. US:WAG also fell 1.7% after the drugstore chain operator missed Wall Street estimates.

Vertex announces that two Phase 3 trials of its cystic fibrosis treatment met their primary endpoint VertexPharma

Vertex Pharmaceuticals VRTX, +1.66% shares rocketed 40% to $95.53 after announcing that two Phase 3 trials of its cystic fibrosis treatment met their primary endpoint.

xG Technology Inc. US:XGTI shares soared 25% to $2.31 after the wireless communications firm said it was selected by the U.S. Army as a subcontractor to provide communications and network services. The deal is worth $497 million.

Elizabeth Arden US:RDEN stock fell 3.1% after the company’s board released details of a broad restructuring program, according to a filing with the U.S. Securities and Exchange Commission.

Asian stocks rise, European shares turn lower

In Asia overnight, equities ended mostly higher ahead of a presentation of an economic reforms package by Japanese Prime Minister Shinzo Abe. In Europe, the Stoxx Europe 600 SXXP, +0.64% closed lower after a downbeat German business confidence survey.

Among commodities, crude-oil CLQ24, -2.05% settled lower for a second straight session, but kept their grip on the $106-a-barrel. Gold futures US:GCQ4 settled higher to mark their fifth straight session gain. The shiny metal settled $2.9 higher at $1,321.30.

More news from MarketWatch:

This is what needs to happen for gold to rally

Goldman Sachs: 15 cheap stocks for an expensive market

Home prices up, but sales not so much