An Ebola cure was fast-tracked by the U.S. government earlier this month, but it will not come in time to help ease the West African outbreak that is now being called unprecedented in its scale. A new report from heath authorities in Guinea where the outbreak began and is centered, now puts the confirmed Ebola death toll at 78, up from 70 over the weekend, with two additional Ebola deaths now confirmed in neighboring Liberia, as the disease spreads at an alarming rate.

On March 5, the Canada-based pharmaceutical company Tekmira announced that its potential Ebola cure now known as TKM-Ebola had received fast-track status from the U.S. Food and Drug Administration. The anti-viral drug has proved to be a cure for Ebola when tested on monkeys infected with the “Zaire strain” of Ebola.

The highly lethal Zaire strain, researchers believe, is the virus responsible for the current West Africa Ebola outbreak. But in clinical trials, the Tekmira Ebola cure allowed all of the infected monkeys who received the drug to survive, according to the company’s own literature.

With no cure available, an Ebola outbreak can have a fatality rate as high as 90 percent. The current outbreak at last report was running at a 64 percent fatality rate.

But even though the Tekmira Ebola cure and several other Ebola drugs are in the works, there is currently no confirmed cure for Ebola, or vaccine to prevent the virus from causing the deadly and grotesque Ebola Hemmorrhagic Fever disease. That’s simply because drugmakers see no money in it.

Frightening as they are, Ebola outbreaks — which have been known for only 40 years — are rare. They usually affect only impoverished areas of Africa and because they are so lethal, patients tend to die before they can travel and spread the virus, so an outbreak generally burns out after a short period.

“If you count all the cases of Ebola since the discovery, it’s below 10,000, so it’s definitely not of commercial interest,” said Stephan Guenther of Germany’s Bernhard Nocht Institute for Tropical Medicine.

The Tekmira Ebola cure is being developed under a $140 million contract with the U.S. Defense Department, which fears Ebola could be used as a biological warfare weapon. The Center For Disease Control and Prevention lists Ebola along with smallpox and anthrax as “Class A” bioterrorism agent.

The San Diego-based company Mapp Biopharmaceutical is developing a similar drug with money from the Defense Deprtament and the National Institute of Health. The Mapp Ebola cure, MB-003 is a “cocktail” of antibodies which in trials prevented all Ebola-infected monkeys from dying if they were given the drug within an hour after exposure to the Ebola virus.

Of monkeys that received the Mapp drug within 48 hours, two of every three survived.

Over the weekend, the Ebola outbreak reached the teeming Guinea capital of Conakry, a congested city of 2 million, when an infected patient and his family traveled there looking for health care — raising the specter of an outbreak that could spiral out of control and even reach beyond Africa’s shores. With the West Africa Ebola outbreak already called “an epidemic of a magnitude never before seen,” by Mariano Lugli of Doctors Without Borders in Guinea, the race for an Ebola cure has taken on a whole new urgency.