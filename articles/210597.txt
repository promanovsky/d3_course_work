The Carters might seem like the perfect family to an outside observer, but they are not without their flaws, it would seem. Case in point: Jay Z and Solange Knowles’ less-than-graceful altercation after the Met Gala on May 5th. Footage has just recently been released of Solange, entering an elevator with Jay Z and Beyonce after the Met Gala and proceeding to attack her brother-in-law. Because the recording, obtained by TMZ, doesn’t include audio, the reason for the scuffle remains unclear.



Solange appears cool and composed in public. What could have set her off?

What can be seen in the video of Solange, Beyonce and Jay Z after an after party at The Boom Boom Room is that the scuffle is pretty dramatic. As the group walks into the elevator where Beyonce i already standing, Solange appears to immediately begin screaming and pointing in the "Holy Grail" rapper's face.

More: Solange beats Beyonce's husband Jay-Z - but why?

In the video, it looks like Jay Z is just trying to hold Solange’s arms back, while another man, presumed to be a bodyguard, holds her back. The man attempts to stop the elevator, possibly to keep things private.



The incident is yet to be acknowledged by anyone from the family.

Beyonce can also be seen in the video, exchanging words with her sister in the moments when the 27-year-old woman was held back by the bodyguard. She finally appears to say something to Solange after she brakes free and approaches Jay Z again, kicking and punching. At one point, the young woman can reportedly be seen hitting her brother-in-law with a handbag, until its contents fly out spilling all over the floor of the elevator. The "99 Problems" rapper eventually leans down and picks up the purse handing it to his wife.

Despite a couple of public appearances since then, no one from the family has spoken out about the incident yet.

More: Solange Knowles "Overreacted" To Jay Z's Comment

Watch Jay Z Unveil His 2013 Christmas collection, "A New York Holiday", In The Video Below.



