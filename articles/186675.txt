A Chrome OS tablet? Don’t hold your breath

Google is still hedging its bets on Chrome OS tablets, the company has confirmed, delivering touch initially but holding off on introducing new form-factors until it figures out if there’s really a demand for them. Speaking during Intel’s Chromebook event today, Caesar Sengupta, VP of product management at Google, insisted that the company was still in the “wait and see” stage.

“From a platform perspective, touch is a key part of the consumer experience,” Sengupta said, referring to his own children and how they even try to touch photo thinking they’re touchscreens. “Kids these days expect everything to have touch.”

“From that perspective, we want to make sure that when people want to have touch, we’ll support it,” he pointed out. “When it comes to new form-factors, we look at what people are using [it] for and will consider more from that.”

Although Intel and Google were keen to point out that there are many more Chrome products now than there were a year ago, they conceded that in terms of specifics like screen resolution, many are offering the same experience. While Chromebooks ranging from 11- through to 14-inches are offered from different OEMs, they all use the same 1366 x 768 resolution.

In fact, the only real outlier is Google’s own Pixel with its whopping 2,560 x 1,600 display – and comparatively huge price to match.

Speaking during the event, Lenovo’s Ashley Rodrigue agreed that the possibility of more detailed displays was interesting, but said that the company still wasn’t ready to announce anything outside of its N20 and N20p.

“There is quite a bit of range, and we’re looking at that closely as an OEM in terms of what people want as a Chrome experience,” Rodrigue said. “When you look at the Pixel, it’s clear that there’s a range of desires, and we’re really excited about that.”

The irony is that Chrome OS already supports tablet-friendly features like an on-screen keyboard, which was added to the platform back in April. At the time, it was suggested that this could be a move to blur the lines between Chrome OS and Android, something which Google has been reluctant to acknowledge.

“On Chrome OS we’re focusing on the clamshell form-factor, Chromeboxes, Android is designed wonderfully well for phones and tablets,” Sengupta concluded. “The world is in a state of great flux right now; computing itself is changing. People are trying out different form-factors, different types of experiences. As Google, we’re trying to see what really works well for users? We’re still innovating. We think the new form-factors [for Chromebooks] are interesting, like keyboards that fold behind. We want to make sure Android works really well for tablets.”