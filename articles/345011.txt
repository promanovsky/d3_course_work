Why did Google buy Songza? Rdio knows

There are a lot of streaming music services in the world, and Google’s acquisition of Songza came as no surprise. What is a bit befuddling to some is why Google would buy Songza instead of the others out there. Google’s pockets are deep, so cost wasn’t really an issue — they could have had any company willing to sell (and they all would for the right price). So why did Google grab Songza?



It’s simple when you consider what Songza offers, and what they don’t. Comparatively, Songza lags far behind the competition. Their selection isn’t as good as a Pandora, and their subscriber numbers lack just about all other major streaming services. They’re not saying how many subscribe to their ad-free solution, but even if it were close to everyone that uses their service — they don’t match up to a Spotify.

Rdio has also announced a very Google-y move today, acquiring TastemakerX. Their curation service offers up everything you’d want to get about an artist. Songs, videos, even news and concert info. Scraping the depths of ‘Robin Thicke’, Rdio will now let you obsess as you please.

Still, Google chose Songza for a reason, and it’s similar to the reason Rdio chose TastemakerX. By adding a bit of context to your music, you’re more likely to engage — and stay engaged. If Rdio can offer you up everything you need or want about a favorite set of artists, you’ll make that your default.

Google has a similar aim. They’d like you to make Play Music All Access your default streaming music provider. Though Google isn’t laying out plans for what they’ll do with Songza, the service’s core function tells us all we need to know. Their goals line right up with Google’s: context.

Songza lets you pick what you’re doing, and what kind of music you’d like to hear. If you want something to calm you down at work, Songza lets you choose those melodies. Need something for a mountain biking excursion? Great, they’ve got you covered. Everything about Songza aims to be contextually aware of you and what you’re up to. They then aim to give you a reason to be involved with their service, based on how well they can take that contextual info and turn it into a service you’ll like.

Just like Google.

