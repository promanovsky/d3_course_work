Apple launches new versions of the smartphone line that drives half its business around the fall of every year.

Apple Inc's suppliers will begin producing larger versions of the iPhone in China next month, Bloomberg reported citing sources.

Apple is ramping up production of iPhones with 4.7 and 5.5-inch screen sizes, which may be shipped to retailers around September, the report said.

Apple launches new versions of the smartphone line that drives half its business around the fall of every year.

The industry has speculated for some time now that Apple intends to design and sell a device with a larger screen, to fend off Samsung phones with much bigger displays that have proven popular in Asia and elsewhere.

Hon Hai Precision Industry will recruit over 100,000 people in mainland China to produce the newest iPhone from Apple, Taiwan's Economic Daily News reported on Monday, in what the report called the firm's largest single hiring spree in China.

Fellow Taiwanese contract manufacturer Pegatron Corp will also expand its workforce in one mainland factory by 30 percent, in response to expected high demand for the new iPhone.

Apple was not immediately available for comment outside regular U.S. business hours.