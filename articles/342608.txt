Pippa Middleton speaks!

Literally, she spoke — for the first time ever on television, as the “Today” show was quick to remind us several times during its worldwide exclusive interview that aired Monday morning.

Pippa — sister of Catherine, Duchess of Cambridge; sister-in-law of Prince William; aunt of Prince George — sat down with Matt Lauer because … well, actually we’re not sure. She was just in America for a charity bike ride, but Lauer still had to fly all the way to London to do the interview. (Of course, word on the street is that NBC is trying to lock down Pippa as a “Today” show contributor.”)

Anyway, what did we learn about Pippa? A few things: She’s extremely soft-spoken and apparently doesn’t like being in the limelight. She also came across as genuinely shy on camera, not one to expand with details while answering questions. Plus, she stuck to her message: Despite what you may think, she’s just a normal, everyday, 30-year-old woman who happens to be one of the most famous people in the world.

She spoke quietly and quickly as Lauer grilled her about the big two topics that have fascinated the world for the past three years: The fact that she’s Catherine’s sister, and that time she wore a very form-fitting bridesmaid dress at the royal wedding.

“This is the first time you’ve ever done a television interview,” he reminded her. “Why?”

“I don’t know. I had this nervousness of probably being in front of a camera,” she replied, indeed looking very nervous.

Visit NBCNews.com for breaking news, world news, and news about the economy

“It’s funny you say that, that you’re a nervous in front of a camera, yet you’ve become one of the most recognizable faces in the world based on what’s happened over the past couple years,” Lauer explained. “So has that been hard for you?”

“Yeah,” Pippa acknowledged. “It has been difficult I suppose, going to a very normal life to sort of managing everything that happened after the wedding.”

In case we forgot, Lauer enthused that it was wedding of the century in 2011, broadcast across the world — the nuptials of commoner Catherine and William, the presumptive future king of Britain. Lauer noted that Pippa stole the show: “Soon, everyone in the world wanted to know: Who was this beautiful woman in the flattering dress?”

For Pippa, however, the wedding wasn’t a big deal at first. “It sounds funny to say, but we saw it as a family as just a family wedding. And actually I didn’t realize perhaps the scale of it until afterwards,” she said. “And I had to make sure I helped my sister where I should, and look after the bridesmaids and pageboys. But we really saw i as sort of a family getting together … like any family.”

In fact, Pippa said, she didn’t realize how massive it was until the wedding party appeared on the balcony, looking out at the the thousands of people outside the Abbey. “It was slightly surreal,” she admitted. “I think it was when we saw the crowds rushing around the balcony. I suddenly thought, ‘Wow, this is pretty special.'”

“It wasn’t long until people started talking about you,” Lauer said, going in for the kill. “And for lack of a better way to explain it, the way your dress fit. How did you feel about that?”

Pippa confirmed it was completely unexpected. “The plan was not really for it to be a significant dress,” she explained. “Really just sort of blend in with the train. I suppose it’s flattering.”

“A little embarrassing?” Lauer prodded.

“Embarrassing, definitely,” Pippa confirmed. “It wasn’t planned. The dress was almost meant to be insignificant.”

In case you were wondering: The dress is home in Pippa’s closet, tucked away in case her children ever want to see it someday. No, Matt Lauer, she hasn’t worn it since.

Then a quick break to talk about the “real” purpose of Pippa’s interview, her charity work. That included recently biking across 12 U.S. states to raise money for the British Heart Foundation (to increase awareness of heart disease in women) and The Michael Matthews Foundation that raises money for childhood education. Yes, Pippa, a noted athlete, thought it was very difficult to ride a bike for a week; but knowing it was for those causes “gave a purpose to it.”

Then, it was back to the juicy stuff: “What’s your view on fame these days?” Lauer asked. “In a speech you gave which I read, you said fame can have an upside, a downside, and — you say it.”

“And a backside,” Pippa said shyly. Zing!

Pippa’s Thoughts on Fame are as follows: Paparazzi follow her around London, which is annoying, because she can’t ever have privacy. If she walks into a restaurant, people will immediately start tweeting about it, so cameramen will show up. And online bullies are very much not fun.

Visit NBCNews.com for breaking news, world news, and news about the economy

“The very same media and the very same people on the social media sites that want to make you the It Girl, at some point they decide, ‘We’re done,’ and they sometimes want to tear you down,” Lauer said. “Have you experienced that?”

“Yes, quite a lot, actually,” Pippa said, suddenly looking animated.

“How do you deal with it?” Lauer asked.

“It’s hard sometimes. I have felt publicly bullied a little bit. “When I read things that clearly aren’t true or that, you know, or whichever way someone looks at it on the negative side,” she explained. “It is quite difficult. I’m actually just paving my way just trying to live a life like any 30 year old.”

One more plug for Pippa’s writing career (a Vanity Fair column, her widely-mocked party planning book) and that was it. Yeah, the producers in their evil genius ways are holding all the Prince George stuff for part two of the interview — Tuesday morning.