NEW YORK (TheStreet) – Nothing beats higher profits. Investors are waking up to this reality as shares of Procter & Gamble (PG) - Get Reportare soaring close to 4% Friday following the company's 38% jump in profits for its fiscal fourth quarter.

Shares are trading at around $80 following Procter & Gamble's Thursday close of $77.32. Prior to Friday's bounce, shares were down more than 1% on the year to date and down 2% in the trailing 12 months. But as the company's results, which included a 4-cent beats in earnings just demonstrated, management has never lost focus on the bottom the line.

With the stock still trading at more than 6% under its 52-week high, Procter & Gamble's ongoing efficiency improvements makes it one of the best sure-things on the market. At around $80 per share, the stock should reach $85 on the basis of margin expansion and growing cash flow. And when you factor the company's 3.20% dividend yield, this stock is not much of a gamble at all.

Management remains committed to returning value to shareholders. To that end, they've identified ways to grow revenue and margins by utilizing assets more efficiently. Procter & Gamble's plan of attack is to focus on core operations while effectively managing its brand portfolio. Management believes they can achieve these goals while also growing share in developing markets.

In that regard, whether through divestment or discontinuation, the company management plans to shed 100 under-performing brands from its portfolio in the next two years. This makes sense when you consider that 90% of the company's revenue and 95% of its profits will come from the brands that will remain.

Procter & Gamble is following the same mantra that other large conglomerates like General Electric (GE) - Get Report have embraced, which is to return back to their strengths. And I believe management's willingness to admit that the company had gotten to stagnant with its size should be applauded.

During this transition, investors will see a more innovative Procter & Gamble -- a change that will help boost revenue and remove concerns about weak organic growth. This is because management has become more vocal about shifting portions of its advertising to mobile and social media. I believe this move will drive marketing expenses lower while also reducing sales and general & administrative expenses.

The other thing is that a slimmer version of Procter & Gamble will get an immediate boost in productivity that will be accretive to margins. When margins expand, it works wonders for cash flow that can be used for things like dividends and share buybacks.

The bottom line is that sometimes smaller is better.

Proctor & Gamble will still remain the world's largest consumer product maker. This means the company has a strong pulse on consumer sentiment. On Wednesday, investors learned the U.S. economy grew at an impressive 4% rate in the spring. So as long as the U.S. and world economies continue to improve, Procter & Gamble stock will continue to make sense.

In that regard, with signs of increased volumes on the horizon, now would be the wrong time to bail on Procter & Gamble, especially the company buying back $1.5 billion of its common stock. At around $80 per share, the stock remains a buy until the company's transformation hits a snag. But by then it may be too late.

At the time of publication, the author held no positions in any of the stocks mentioned, although positions may change at any time.

Follow @Richard_WSPB



This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff.

TheStreet Ratings team rates PROCTER & GAMBLE CO as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate PROCTER & GAMBLE CO (PG) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its increase in net income, good cash flow from operations, growth in earnings per share, largely solid financial position with reasonable debt levels by most measures and expanding profit margins. We feel these strengths outweigh the fact that the company has had lackluster performance in the stock itself."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

The net income growth from the same quarter one year ago has exceeded that of the S&P 500 and the Household Products industry average. The net income increased by 1.7% when compared to the same quarter one year prior, going from $2,566.00 million to $2,609.00 million.

Net operating cash flow has slightly increased to $4,109.00 million or 6.39% when compared to the same quarter last year. In addition, PROCTER & GAMBLE CO has also modestly surpassed the industry average cash flow growth rate of 6.02%.

PROCTER & GAMBLE CO's earnings per share improvement from the most recent quarter was slightly positive. The company has demonstrated a pattern of positive earnings per share growth over the past two years. We feel that this trend should continue. During the past fiscal year, PROCTER & GAMBLE CO increased its bottom line by earning $3.87 versus $3.12 in the prior year. This year, the market expects an improvement in earnings ($4.19 versus $3.87).

The current debt-to-equity ratio, 0.52, is low and is below the industry average, implying that there has been successful management of debt levels. Despite the fact that PG's debt-to-equity ratio is low, the quick ratio, which is currently 0.50, displays a potential problem in covering short-term cash needs.

The gross profit margin for PROCTER & GAMBLE CO is rather high; currently it is at 52.73%. Regardless of PG's high profit margin, it has managed to decrease from the same period last year. Despite the mixed results of the gross profit margin, PG's net profit margin of 12.69% compares favorably to the industry average.