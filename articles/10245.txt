Tim Berners-Lee has been an outspoken critic of online surveillance, ever since Edward Snowden made his revelations last year. (Credit: Gulltaggen)

LONDON, March 12 (UPI) -- Speaking on the 25th anniversary of the world wide web, Tim Berners-Lee -- its inventor -- said the web needs a digital bill of rights to protect its users.

Berners-Lee has been an outspoken critic of government surveillance of the Web, saying that new rules or an online "Magna Carta" need to be established to protect against such activities. He also likened the kind of surveillance being conducted to a human rights issue.

Advertisement

"The key thing is getting people to fight for the web and to see the harm that a fractured web would bring," Berners-Lee said. "Like any human system, the web needs policing and of course we need national laws, but we must not turn the network into a series of national silos."

Berners-Lee believes that the online community has come to a crossroads and need to decide which path they plan to take.

RELATED Congress to investigate GM ignition recall

"Are we going to continue on the road and just allow the governments to do more and more and more control -- more and more surveillance?" he told BBC Breakfast.

He hopes to use the "Web We Want" initiative to build national and international support for a universal "Internet Users Bill of Rights." This would bring the world wide web back to a free space where information can flow freely and open up creativity on the Web, without the fear of being watched by some entity.

According to Berners-Lee, without an open and free Internet the world cannot have a open government, good democracy or connected communities.

"It's not naive to think we can have that, but it is naive to think we can just sit back and get it," he said.

[BBC] [USAToday]