Kim Kardashian and Kanye West jetted south of the border to Punta Mita, Mexico, to celebrate Kanye turning 37 on June 8. We have photos from INSIDE the lavish, secluded beachfront estate where the newlyweds are currently celebrating!

Kim Kardashian, 33, and Kanye West, 37, jetted south of the border to Punta Mita, Mexico, to celebrate Kanye’s June 8 birthday! Warning: When you look at the photos where the newlyweds are celebrating, you’ll want to go on a beach vacation ASAP!

Kanye West & Kim Kardashian’s Vacation In Mexico: Inside Their $75k Trip

It’s no secret that Kim and Kanye love living a luxurious life. Their Italian wedding was beyond decadent, their gifts to each other are always lavish and they simply love the finer things. The famous duo are currently spending their days relaxing at Casa Aramara in Punta Mita, Mexico. Their private beachfront mansion was built by Joe Francis and is oh-so private. The residence even has it’s own 15-person staff (and 24 hour butler service!) on hand to cater to Kim and Kanye’s every whim.

As if all of that isn’t enough, the property sits directly on the beach and has it’s own infinity pool. Kim just Instagrammed two sexy bikini shots from the pool on June 12! We wonder if she’s working out at the mansion’s private gym? See all of the photos of the gorgeous Casa Aramara right here. Based on the mansion’s rates, it seems like Kimye most likely spent about $75,000 to jet down to Mexico for a few days.

The duo will head back to Los Angeles soon enough. A source tells HollywoodLife.com EXCLUSIVELY that Kim and Kanye will celebrate daughter North West‘s first birthday on June 15 with a small get together. “They’ll be home with North and having a party. Small, with family and and close friends,” a source spills to HollywoodLife.com EXCLUSIVELY.

How Kanye West Is Settling Down After Marriage

Kanye, who has never been married before, is absolutely loving behind Kim’s husband. “They’ve been chilling since they got married and are winding down as much as they can before work begins again for the both of them,” a source close to Kanye opens up EXCLUSIVELY to HollywoodLife.com. “Kanye never, ever, ever takes vacations. Kim has to hide his phone and computer so he won’t try to work.” We love that he’s taking vacations now and enjoying being a newlywed!

What do YOU think about Kim and Kanye’s Mexico mansion, HollywoodLifers? Let us know in the comments below!

— Megan Ross

More Kim Kardashian & Kanye West News: