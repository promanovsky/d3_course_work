Accra - A US citizen suspected of having Ebola because he fell ill after visiting West African states battling the disease has tested negative, a senior Ministry of Health official told Reuters on Monday.

The man, who has not being named, is in quarantine at Nyaho clinic in Accra and the blood tests were conducted at Noguchi Memorial Institute of Medical Research in the city.

“It is negative,” Badu Sarkodie, head of disease surveillance at the Ghana Health Service, told Reuters, adding that further tests would be conducted.

Health officials have called for regional action to halt the world's deadliest outbreak of the disease, which has spread across Guinea, Liberia and Sierra Leone, killing at least 467 people since February.

The highly contagious Ebola virus causes fever, vomiting, bleeding and diarrhoea and kills up to 90 percent of those it infects. It is transmitted through contact with blood or other fluids.