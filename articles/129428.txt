A newly discovered planet may be the most Earth-like yet found in another solar system, scientists believe.

Kepler-186f is almost the same size as the Earth and occupies its star's "habitable zone" where temperatures are mild enough to allow liquid surface water.

If the planet has lakes or oceans, it would increase the chances of life evolving there.

But anything living on the world may have to withstand extra large doses of radiation from its active sun, Kepler-186.

The find is described in the journal Science as "a landmark on the road to discovering habitable planets".

Smaller and cooler than our sun, Kepler-186 is classified as an M-dwarf star, is 795 light years away and is orbited by five known planets.

Kepler-186f, the latest to be discovered, is the outermost plant in the system.

The planet was found by astronomers scouring the Milky Way galaxy for potentially habitable worlds

Using Nasa's Kepler space telescope, they measured the very tiny dimming that occurs when a planet crosses or "transits" in front of its star.

The transit information allowed them to calculate the planet's size and estimate its mass and density.

Kepler-186f was found to be just 10% bigger than the Earth. While habitable zone planets have been identified around other stars, none of them so closely match the Earth in size.

US astronomer Dr Stephen Kane, a member of the Kepler team, said: "Some people call these habitable planets, which of course we have no idea if they are. We simply know that they are in the habitable zone, and that is the best place to start looking for habitable planets.

"What we've learned, just over the past few years, is that there is a definite transition which occurs around about 1.5 Earth radii. What happens there is that for radii between 1.5 and two Earth radii, the planet becomes massive enough that it starts to accumulate a very thick hydrogen and helium atmosphere, so it starts to resemble the gas giants of our solar system rather than anything else that we see as terrestrial."

The habitable zone has also been called the "Goldilocks" zone, because conditions there are just right to permit liquid surface water and, possibly, life.

Of our two closest neighbours in the solar system, Mars is just too cold and its water is locked up as ice, while Venus orbits closer to the sun than the Earth and is too hot.

Kepler-186f seems to orbit the outer edge of its habitable zone. However, being slightly larger than the Earth means it is likely to have a thick insulating atmosphere that would stop its surface water freezing.

Small stars such as Kepler-186 live a lot longer than larger stars, providing more time for biological evolution to take place. This makes them promising places to look for life, according to Kane.

On the other hand, small stars tend to be more active than the sun and liable to produce more solar flares and potentially harmful radiation.