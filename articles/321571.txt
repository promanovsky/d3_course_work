Women who are able to reproduce later in life also tend to live longer. This phenomenon may be linked to a genetic variant that facilitates exceptionally long lifespans.

Researchers found that women who were able to have children after the age of 33 had a higher chance of living longer than those who had their last child before the age of 30, reported Boston University Medical Center.

"Of course this does not mean women should wait to have children at older ages in order to improve their own chances of living longer," said corresponding author Thomas Perls, MD, MPH. "The age at last childbirth can be a rate of aging indicator. The natural ability to have a child at an older age likely indicates that a woman's reproductive system is aging slowly, and therefore so is the rest of her body."

To arrive at the findings, researchers examined data from the Long Life Family Study (LLFS), which looked at 551 families with many members who had lived exceptionally long lives. The team determined the ages at which 462 women had their last child as well as how old they lived to be. They found women who had their last child at 33 had twice the odds of living to be 95 years of age or older than women who had their last child at 29.

The finding also suggest that women are the "driving force behind the evolution of genetic variants linked to longevity.

"If a woman has those variants, she is able to reproduce and bear children for a longer period of time, increasing her chances of passing down those genes to the next generation," Perls, said. "This possibility may be a clue as to why 85 percent of women live to 100 or more years while only 15 percent of men do."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.