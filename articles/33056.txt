Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Chris Hemsworth and his wife Elsa Pataky have welcomed twin sons to their family.

The two boys were born in Los Angeles on Friday, the actor's representative confirms to PEOPLE.

Speculation mounted Elsa had gone into labour after she was seen at the city's Cedars Sinai Medical Center on Thursday.

The couple, who are already parents to 22-month-old daughter India Rose - were accompanied to the hospital by security guards and booked into their own private suite.

Proud father Chris, 30, previously gushed about family life and admitted he is happiest when he is with his Spanish actress wife and their daughter.

He said: "Being a father is certainly a task. But the best one that I could ever ask for. Being home, being with the family, that's what it's about ... It makes me much more relaxed with work, because I have something that is far more important to me now.

Chris and Elsa, 37, married in 2010 after a whirlwind romance.