The future of the Android operating system is currently up in the air, but a new clue from Google (NASDAQ: GOOG) may give some insight into its plans for its upcoming software.

Google developer Andrew Hsieh recently discovered some changes within the code for the Android Open Source Project (AOSP), the Linux-based platform on which the Android operating system is built, that suggest a number of changes to the operating system. According to Hsieh, the code indicates that Android operating systems may adopt 64-bit computing, which allows devices to process larger data capacities, such as RAM.

Google may soon follow the industry trend of making its devices compatible with 64-bit computing now that many chipset manufacturers are adding 64-bit capability to new and upcoming processors.

The 64-bit architecture has actually been developed on the Android platform for quite some time. Android is notably based on the Linux operating system, which has had 64-bit functionality for an even longer period of time than Android.

Android-based software simply needs compatible hardware to utilize full 64-bit processing, as the 64-bit framework is already within its code.

Google has also collaborated with hardware manufacturers such as LG Electronics Inc. (KRX: 066570) and Asus (TPE: 2357), which use processors by the chipset manufacturer Qualcomm.

Qualcomm has announced a number of 64-bit processors, which will soon be implemented into Android-powered devices. The manufacturer announced its high-end 64-bit processors, the Qualcomm Snapdragon 808 and Qualcomm Snapdragon 810, in April.

Developers have also discovered clues to a possible new naming sequence for the Android operating system. The newly discovered lines of code that mention 64-bit processing also indicate that Android may switch from a numerical naming system to an alphabetical naming system internally. In the past, the Android application programming interface (API), or the complete code for each Android version, were named API level 1 for Android 1.0, and API level 19 for Android 4.4 KitKat. The new code explicitly suggests that the 64-bit API level could be renamed as “L” or “Android L.”

Change 99021 – Merged

64-bit in android-L

Tentatively rename 64-bit API level to non-numeric: ‘L’

Change-Id: I676099e467d5426da6cd1d96d63fc201f78ce533 Change 99016 – Merged

64-bit in android-L

See https://android-review.googlesource.com/#/c/99021/

Change-Id: I10daf2da97aa9d3c99661b5d79080b96a0ae9f22

Although Google has not confirmed its plans for future Android software, many rumors are currently circulating. In recent months, there were several reports that said Google plans to scrap its Nexus device line for a new project, which has been referred to as “Android Silver.” Many tech informants and analysts have given their predictions as to what Android Silver might be and what the program might mean for Google. The Information, in particular, speculates that Google may release Android Silver to compete with the analogous iOS ecosystem that Apple Inc. (NASDAQ: AAPL) has developed.

Similar to how most iPhones and iPads are backward-compatible with each new iOS version, Google may roll out Android Silver in a manner that will allow all current Android devices to be updated with a single update.

The Google I/O conference will notably take place on Wednesday and Thursday; however, some believe little information about Android Silver or other operating system developments will be introduced at the conference. Keep in mind that Google did not announce any operating system news at I/O 2013 despite many rumors to the contrary.

Follow me on Twitter.