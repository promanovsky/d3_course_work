This past February Ohio State University was struck by a sudden case of the Mumps. With over 28 confirmed cases, what was most striking about this outbreak was that 97% of the cases had been vaccinated against Mumps. Unfortunately for the surrounding community, this strain of the disease has been spreading, leaving the campus grounds. As of March 25, 10TV announced that 69 cases have been reported in Franklin County, Ohio, with 52 of those linked to the University. CNN also reported that cases of Mumps have been found at Fordham University in New York.

With the disease spreading into the community and even surrounding states, many health officials are worried about how far this infestation could travel. Mumps is a highly contagious disease, spread through respiratory contact (like sneezing and coughing), just like the common cold or flu, and sometimes having the vaccine is not enough.

Ohio State has been talking with other Universities to discuss their preparedness level against Mumps. While covering your mouth when you sneeze and washing your hands a lot is a good way to avoid the disease, equally as crucial is making sure you have both your MMR vaccinations. This vaccine, protecting against Measles, Mumps, and Rubella (MMR), is given in two doses, usually during childhood. While the vaccine isn’t 100% effective – it’s 88%, to be exact – it seriously lowers your chance of contracting the disease. Along with lower stats, the CDC has informed the country that people who are properly vaccinated and still get Mumps may not get as sick.

What happens if you do come down with it? The CDC recommends the following:

1. Stay at home for five days after symptoms begin.

2. Cover your mouth and nose when you cough or sneeze.

3. Wash your hands frequently with soap and water.

All these things are similar to what you would do if you came down with a slight cold. The key difference is, if you find yourself battling the Mumps, you need to contact your health care provider immediately, as symptoms of this rare disease can get ugly. Other manifestations of the disease can be inflammation of the brain, ovaries, or testicles, and sometimes deafness. For these reasons it is all too important to make sure you are up to date on your Mumps vaccine (even take a booster if you want!) and exercise good hygiene in order to avoid contracting this spreading disease.

photo credit: Gene Hunt via photopin cc