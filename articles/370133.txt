Kanye West’s decision to add more shows to his tour to promote his latest album is said to have upset Kim Kardashian and she is reportedly concerned by her husband’s plea for her and North to join him on the road. — Cover Media pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LOS ANGELES, July 6 — Kim Kardashian reportedly doesn’t want Kanye West’s career to take “priority” over their family.

The 33-year-old star and the Yeezus rapper tied the knot in Florence, Italy, in May and have daughter North together, who celebrated her first birthday last month.

Kanye has been touring to promote his latest album and dates were meant to end in December 2013, but he has added more shows for 2014. This decision is said to have upset Kim and she is reportedly concerned by her husband’s plea for her and North to join him on the road.

“Kim knows Kanye wants her and North with him. But she feels he needs to understand that she and North have a life too and his career can’t take priority over them,” a source told British magazine Closer. “Kanye’s told Kim he needs her by his side. He performs better when she’s watching. Plus, North changes and grows so much, he doesn’t want to miss a thing. He’s said they can hire a team of nannies who will ensure North’s sleeping pattern isn’t disturbed. He’d told her money’s no object.”

North’s birthday party was an example of Kanye’s view on money, with him and Kim throwing the tot a lavish do titled ‘Kidchella’ after the popular music festival Coachella. They also went all out with their gifts for their little girl and reportedly splashed out on fun activities for the special day.

“They had a £1,500 (RM8,199) custom-made, three-tiered tie-dye cake and spent thousands on food, hair braiding, face painting and a karaoke booth,” the source added. “They also splashed out £25,000 on hiring a Ferris wheel, £1,000 on a bouncy castle and £1,500 on candy floss machines.” — Cover Media