World stocks set for quarterly gains

Share this article: Share Tweet Share Share Share Email Share

London - Global stocks rose on Monday, on track for their fourth straight quarter of gains, as investors bet that a raft of US and European economic data due this week will soothe recent worries over the pace of growth. MSCI's world equity index, which tracks shares in 45 countries, rose 0.15 percent. It has risen over 4 percent this quarter, aided by the prospect that monetary policy in the major economies will remain accommodative for longer. European stocks rose, with the FTSEurofirst 300 index of top European shares up 0.3 percent at 1,375.49 points, steadying after a 1.7 percent slide last week. Investors will focus on euro zone inflation data for June, due at 11:00 SA time, which should show inflation stuck at 0.5 percent, its ninth consecutive month in the European Central Bank's “danger zone” below 1 percent.

That should keep the threat of deflation in the euro zone at bay for now.

But with inflation nowhere near the ECB's target of just under 2 percent, expectations are the central bank will keep policy loose for longer, underpinning demand for riskier assets and peripheral euro zone bonds, analysts said.

“If anything, we see risks to our forecast as skewed to the upside, mainly related to energy prices,” said Francois Cabau, an analyst at Barclays.

German inflation was above forecasts on Friday, fuelling expectations that the numbers for the whole of the euro zone could rise above the 0.5 percent predicted in a Reuters poll.

As a result, German Bund yields edged up while the euro held steady against the dollar at $1.3646.

“Germany having higher than expected inflation pushed up expectations that we are not going towards a deflation scenario,” said Christian Lenk, a strategist at DZ Bank.

The ECB cut all its interest rates earlier in June and promised more liquidity for banks that lend to businesses and households as it tries to revive growth and bring inflation closer to target.

HOPES FOR A US REBOUND

French bank BNP Paribas will be in focus after sources said the US Justice Department is expected to announce on Monday a settlement involving a record fine of nearly $9 billion over alleged US sanctions violations.

Investors are hoping to see evidence of an economic rebound in the United States in this week's busy calendar of data that includes the June non-farm payrolls report on Thursday, a day earlier than usual due to the July 4 holiday.

Economists polled by Reuters expect 213,000 jobs to have been added in June, for the fifth straight month of gains above 200,000, a run unmatched since the period from September 1999 to January 2000. A weaker-than-expected payrolls report could see the US dollar suffer more.

The US earnings season also starts in the next couple of weeks, which will provide evidence on how the economy and profits are faring.

Globally, purchasing managers' indices (PMIs) for manufacturing are out on Tuesday and services on Thursday.

They are expected to show a picture of growth or at least stability despite geopolitical tensions around Ukraine and Iraq.

In commodity markets, gold was steady at $1,315.40 an ounce, underpinned by geopolitical unrest in Iraq and Ukraine and a soft dollar.

The safe-haven yen was at a five-week high against the dollar at 101.35 yen per dollar.

Brent crude oil lost 42 cents to 112.88 a barrel, while US crude futures fell 49 cents to $105.25.

Oil prices have come off recent highs as fighting in Iraq stayed away from the country's south, where most of its oil is produced. - Reuters