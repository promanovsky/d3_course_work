General Motors Company (NYSE:GM) is clearly facing massive problems with its ignition switches, as three million more cars were recalled by the company on Monday. This effectively doubles the number of GM vehicles facing ignition switch issues. This situation has become a major crisis for the iconic automaker and its new Chief Executive Mary Barra this year.

Following Monday’s recall, Senator Richard Blumenthal, a Democrat from Connecticut, said “The recall is just sort of the tip of the iceberg in terms of what has to be done” at GM.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

More recalls coming?

About 3.36 million cars, both mid-size and full-size, with the problem of ignition switch were recalled on Monday by General Motors Company (NYSE:GM). The malfunctioning ignition switches could have a negative imnpact on power steering, power brakes and/or air bags. The Chevrolet Cobalt and other small cars were part of the 2.6 million vehicles recalled earlier. The issue has been linked to the deaths of 13 people.

The Cobalt problem was noted by the General Motors Company (NYSE:GM) engineers more than a decade ago, but the company’s response to the problem was not as quick as it should have been. This situation eventually led to investigations by the company and also by Congress and federal agencies, according to Reuters.

General Motors (GM) sales unaffected

About 20 million vehicles across the world have been involved in the 44 recalls made by General Motors Company (NYSE:GM) this year. This recall figure exceeds the figure of total annual vehicle sales made in the U.S. Of the total vehicles recalled this year, about 6.5 million vehicles were recalled due to ignition-switch related issues. This includes more than half a million Chevrolet Camaros that were recalled last Friday.

The recall-related write-offs at General Motors have been raised from $400 million to $700 million for the second quarter by the automaker. The total recall-related charges have gone up to $2 billion for GM for the year to date.

However, General Motors Company (NYSE:GM) sales in the U.S. surprisingly reached their highest level since August 2008 in May, despite the multiple recalls by the company. The Detroit-based automaker announced on Monday that it would replace or rework ignition keys to solve the problem.