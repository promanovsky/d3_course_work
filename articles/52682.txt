The Obama administration has listed the lesser prairie chicken as a threatened species but has minimized the restrictions on energy production, agriculture and other activity in Oklahoma and neighboring states where the bird’s historical habitat has been.

The category of threatened is a step below endangered.

Sen. Jim Inhofe, R-Tulsa, and state officials had been trying to keep the bird off the endangered or threatened list, hoping the administration would agree to a voluntary protection plan.

In its announcement on Thursday, the U.S. Fish and Wildlife Service said the bird would be listed as threatened under a rule that “will limit regulatory impacts on landowners and businesses from this listing.”

The distinction was the result of “significant and ongoing efforts of states and landowners to conserve the lesser prairie chicken” and will allow Oklahoma and four other range states to continue to manage conservation efforts and avoid further regulation.

Dan Ashe, director of the fish and wildlife service, said, “The lesser prairie chicken is in dire straits. Our determination that it warrants listing as a threatened species with a special rule acknowledges the unprecedented partnership efforts and leadership of the five range states for management of the species.”

Working through the conservation plan, “the states remain in the driver’s seat for managing the species _ more than has ever been done before _ and participating landowners and developers are not impacted with additional regulatory requirements.”

Inhofe called the decision “purely political.”

States spent $21 million to implement a range-wide plan that covered millions more acres than required under the listing demand, Inhofe said.

“My greatest concern in today’s decision is that it is easier to list a species than it is to delist it,” Inhofe said.

“Just look at the American Burying Beetle in eastern Oklahoma. The beetle has been listed as endangered since 1989, even though newly published population surveys show a population 10 times larger than the conservation goals outlines in the original ABB conservation plans.”