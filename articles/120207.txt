Ebola fears change the menu in Ghana

Bushmeat is still on sale in Ghana despite worries that fruit bats and other bushmeat may be behind the spread of an Ebola outbreak in nearby Liberia and Guinea which has claimed around a hundred lives. Ciara Sutton reports.