The British website Mail Online apologized Wednesday to George Clooney for alleging that his fiancee's mother opposed their marriage on religious grounds — a story Clooney called both wrong and irresponsible.

Clooney is engaged to Beirut-born London lawyer Amal Alamuddin, whose father Ramzi belongs to a prominent Druse family. The Druse are adherents of a monotheistic religion based mainly in Lebanon, Syria and Israel.

Citing unnamed family "friends," Mail Online, which is affiliated with Britain's Daily Mail tabloid, reported this week that Baria Alamuddin wanted her 36-year-old daughter to marry a Druse man.

It said Amal Alamuddin risked being "cast out of the community" if she wed Clooney and claimed that several Druse women had been murdered for not abiding by strict Druse rules.

Clooney called the story "completely fabricated."

In a statement issued to USA Today, he said Baria Alamuddin — a well-known journalist — was not Druse and "is in no way against the marriage."

Clooney, 53, added that "to exploit religious differences where none exist is at the very least negligent and more appropriately dangerous."

"We have family members all over the world, and the idea that someone would inflame any part of that world for the sole reason of selling papers should be criminal," he said.

Mail Online said Wednesday in a statement that the story had been "supplied in good faith by a reputable and trusted freelance journalist."

"We accept Mr. Clooney's assurance that the story is inaccurate and we apologize to him, Miss Amal Alamuddin and her mother, Baria, for any distress caused," it said.

Mail Online said it had removed the article and will contact Clooney's representatives "to discuss giving him the opportunity to set the record straight."

Clooney and Alamuddin have not announced a date yet for their nuptials. It is the second marriage for him and the first for her.

The Druse are a close-knit community and rarely marry outside their sect, but some Druse have welcomed Clooney.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Walid Jumblatt, political leader of the sect in Lebanon, told The Associated Press recently he hoped the couple would soon visit the Druse heartland.

Clooney will bring us "great publicity," Jumblatt said. "He can make a movie about the Druse sect."