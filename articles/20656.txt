British archaeologists have found what they say is the world's oldest complete example of a human being with metastatic cancer and hope it will offer new clues about the now common and often fatal disease.

Researchers from Durham University and the British Museum discovered evidence of tumours that had developed and spread throughout the body in a 3,000-year-old skeleton found in a tomb in modern Sudan in 2013.

Analyzing the skeleton using radiography and a scanning electron microscope, they managed to get clear imaging of lesions on the bones which showed the cancer had spread to cause tumours on the collar bones, shoulder blades, upper arms, vertebrae, ribs, pelvis and thigh bones.

"Insights gained from archaeological human remains like these can really help us to understand the evolution and history of modern diseases," said Michaela Binder, a Durham PhD student who led the research and excavated and examined the skeleton.

Bone lesions showed cancer had spread

"Our analysis showed that the shape of the small lesions on the bones can only have been caused by a soft tissue cancer ... though the exact origin is impossible to determine through the bones alone."

Despite being one of the world's leading causes of death today, cancer is virtually absent in archaeological records compared to other diseases — and that has given rise to the idea that cancers are mainly attributable to modern lifestyles and to people living for longer.

According to the World Health Organization's cancer research agency, new cancer cases rose to an estimated 14 million a year in 2012, a figure seen rising to 22 million within the 20 years.

Yet these new findings, published in the Public Library of Science journal PLOS ONE on Monday, suggest cancer is not only a modern disease, but was around in the Nile Valley even in ancient times.

Binder said the discovery should help scientists explore the underlying causes of cancer in ancient populations and give fresh clues about the evolution of cancer in the past.

Ancient DNA analysis of skeletons and mummies with evidence of cancer can be used to detect mutations in specific genes that are known to be associated with particular types of cancer.

The skeleton is of an adult male estimated to be between 25- and 35-years-old when he died. It was found at the archaeological site of Amara West in northern Sudan, on the Nile, 750 kilometres downstream from the capital Khartoum.

The researchers said they could only speculate on what may have caused of the young man's cancer, but it may have been as a result of environmental carcinogens such as smoke from wood fires, or due to genetic factors, or from an infectious disease such as schistosomiasis, which is caused by parasites.

Schistosomiasis would be a plausible explanation, they said, since the disease has plagued inhabitants of Egypt and Nubia since at least 1500 BC and is now recognized as a cause of bladder cancer and breast cancer in men.