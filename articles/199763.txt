Although syphilis was nearly eliminated from the U.S. about a decade ago, the sexually transmitted disease has resurged in recent years, mainly due to a rise in cases among gay and bisexual men, according to a new report.

The report, from the Centers for Disease Control and Prevention, examined the number of people with the first two stages of the disease, known as primary and secondary syphilis, during which the condition is most contagious.

Overall, the rate of primary and secondary syphilis cases in the U.S. increased from 2.9 cases per 100,000 people in 2005, to 5.3 cases per 100,000 people in 2013, the report said. In 2013, there were more than 16,000 cases of primary and secondary syphilis, 91 percent of which were in men. [Quiz: Test Your STD Smarts]

Most male syphilis cases were among men who have sex with men, according to the report. In 2012, these men accounted for 84 percent of male cases among those where the sex of the patient's sex partner was reported. (Some men may not consider themselves as gay or bisexual, so to get an accurate picture of men's lives, researchers often just ask if they've had sex with other men.)

While cases of primary and secondary syphilis rose in men, cases in women declined between 2009 and 2013, the report said.

The highest rate was in the West, making 2013 the first time in at least 50 years that the South did not have the highest rate of syphilis, the report said.

The findings indicate that efforts to prevent syphilis among men who have sex with men should be strengthened, the report said. These men should be screened for syphilis with a blood test at least once a year, and more frequent screening is recommended for those who have multiple partners, CDC researchers said.

Doctors and public health programs should promote safe sex practices, including using latex condoms and reducing the number of sex partners, the CDC said. The sex partners of those who test positive for syphilis should also be notified and treated, the researchers said.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.