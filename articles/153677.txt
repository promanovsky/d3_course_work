The leaks about the alleged iPhone 6 have picked up their pace in the past few weeks. We’ve seen a few concept renders pop up in the past few weeks, created based on the leaked information.

Recently, Martin Hajek and the folks at NoWhereElse have teamed up again to create a concept render of the upcoming iPhone 6, which is expected to come with a larger display.

We’ve seen a lot of rumors about the alleged iPhone 6, which is expected to come in two sizes: 4.7-inch and 5.7-inch. Apple is rumored to increase the size of the device to compete in the smartphone industry, where smartphones are getting bigger day-by-day.

The alleged iPhone 6 is rumored to come with a sapphire glass display, an A8 processor, ultra thin retina display, an improved camera, and should come with the latest iOS 8 as its operating system. As far as the launch date is concerned, iPhone 6 is expected to see the daylight in September, which is the usual iPhone upgrade cycle.

Here are some more images created by the talented Martin Hajek:

What do you think about the concept images posted above? Do note these are just concept images and not the official product, so the final version of the device could be totally different.

Source: NoWhereElse

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more