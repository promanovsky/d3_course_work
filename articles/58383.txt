Warning: This recap contains spoilers.

People in glass houses shouldn’t throw stones…or punches. But we’ll get to that.

Three seasons in and we finally met the elusive Grant children — Jerry and Karen! Mellie preps Fitz for dinner with the children like he’s about to go into the situation room. Who wants to bet that the atmosphere is warmer there than it was at the family dinner table?

Who invited Awkward to the Grant family dinner? #AskScandal — Angela Sullivan (@asullivn) March 28, 2014

That Jerry and Karen actually exist wasn’t the only big reveal of this week’s episode, which confirmed that no one in the Grant White House locks doors or uses opaque curtains (partition please!). No amount of power walking by Olivia could stop the deluge of secrets as the first family prepped for a live interview.

Now that we’ve finally met them, I can’t say I really blame these kids for staying at boarding school. Jerry is so disgruntled he’s been manning an anti-Fitz Twitter account and ordering Vote Reston T-shirts online (Jerry, I’m sure there’s a free stockpile of those somewhere in the White House). Karen thinks her mom should leave her lying, cheating father until she discovers that Mellie is also enjoying a little something on the side. “You were on your knees with Uncle Andrew!”

Welp.



“Eli” “working” at the Smithsonian. Credit: Eric McCandless/ABC

Outside of 1600 Pennsylvania Avenue, the Gladiators look into B613’s funding. Rowan, who seems to have really thrown himself into his work at the Smithsonian, warns Olivia that investigating the organization is a very bad idea.

Huck also warns Olivia to stop looking into B613, but he has an ulterior motive — getting Quinn back. Seems their little kiss-fight last week got him wishing Quinn was a gladiator again. He sends Jake a little message — via a B613-style back alley brawl — which leads to another Huckleberry Quinn encounter and some face licking, with Quinn taking the charge this time. Charlie is getting creepier by the day, demanding to know everything about Quinn’s “argument” with Huck and eventually moving himself into her apartment, where he proceeds to make noodles. Security!

Speaking of people we need protection from: Mama Pope is still milling about and scheming with Adnan Salif. Adnan goes to Pope & Associates to seek imm-un-ity , but ends up seducing Harrison with a pair of Louboutins and some slow jams. Poor Harrison, who confesses he owes Adnan from their corruption-filled days of yore, ends up with a syringe to his back. Adnan delivers the Grant campaign’s full schedule to Mama Pope.

Mama Pope takes some time from her busy terrorist-abetting schedule to place a call to her dear daughter. It’s obvious the Pope parenting style revolves around real talk: “I’d rather be a traitor than what you are, Livvie. You think you’re family, but you’re nothing but the help.”

That probably hurt more than the stones being thrown around in the glass house that was the setting for this episode. First, Olivia tries to call out Andrew for his dalliances with Mellie. “She’s the first lady of the United States and you’re trying to be vice president. Stay away from her.” And then, the real “glass house” of the night — Fitz giving Andrew a shiner for sleeping with the wife he’s been cheating on for the last four years.

Oh the hypocrisy in this episode! And the self-righteousness! I can’t even, #Scandal — Kerry Kupecz (@kerryku) March 28, 2014

I could have chalked that up to adrenaline or guilt, but Fitz took it a step further, delivering a sob story that made me wonder where he was in seasons one and two of this show. He tells Mellie that she ruined their marriage because she wouldn’t let him touch her after she gave birth to Jerry. Since I saw Karen and Baby Teddy with my own eyes just hours ago, I’d have to call foul on that. Mellie looked like she was ready to tell Fitz that Big Jerry raped her, but Olivia burst through the door and interrupted an emotional conversation. “I’m talking to my wife!”

Liv sought solace from the one person who could understand her plight: Cyrus. Aside from trying to avenge James’s death by going Monday Night Raw on Jake in the Oval Office, Cy’s holding up pretty well considering he just buried his husband. The good news is: He’s still not one to sugarcoat the truth. The bad news is: He’s still not one to sugarcoat the truth.

Liv: “Tell me we’re not the help. If you can do that for me…” Cyrus: “I’d be lying.”

Cyrus is able to convince Olivia to go back to work and she pledges to do just that, telling Fitz to cut the apologies and focus on fixing his family. “I cannot afford to fail at my job right now because it is all I have left.” Well, almost all.

After Liv manages to get the entire Grant family, even Baby Teddy, in front of the camera in time for the interview, she gets a call from Rowan, who shares some tips on where to look for B613’s funding (in a word: everywhere).

“We’re family, Olivia,” he tells her. “And despite everything, families stick together.”

Lingering questions: How is Ella a (really adorable) toddler and Baby Teddy is still a baby? How many episodes before Jake has a nervous breakdown? So, Rowan (Eli) has an actual lab at an actual Smithsonian museum? How will Harrison — and Abby — deal with Adnan?