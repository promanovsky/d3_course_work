Warning issued to GPs, A&E departments and all NHS trusts across the UK

No cases have been confirmed in UK but 672

British airlines are also on 'red alert' for cases of the deadly virus

He will chair an emergency meeting on how to boost defences

He warned the spread of Ebola could become a global pandemic

Expert claims panic over death of U.S. man in Nigeria is 'justified'

Hong Kong woman quarantined when she fell ill after returning from Kenya

Fears of a global Ebola pandemic are 'justified' an expert has said as Nigerian health officials try to trace 30,000 people at risk of contracting the deadly disease following the death of Patrick Sawyer.



The U.S. citizen boarded a flight in Liberia carrying the disease to Nigeria, potentially infecting 'anyone on the same plane'.

It comes as Nigerian actor Jim Iyke sparked outrage, posting a picture of himself wearing an Ebola mask while sitting in a first class airport lounge as he fled Liberia.



The 'Nollywood' star posted a message on his Instagram page saying he had cut short a business trip to Monrovia in Liberia - where at least 600 people have already died from the disease.

The death toll for this, the worst outbreak recorded since the Ebola virus was discovered in 1976, stands at 672, while more than 1,200 people have been infected.

Scroll down for videos

The latest outbreak of Ebola is the most severe since the disease was discovered in 1976. So far the disease has spread from a village in Guinea to Sierra Leone, Liberia and Nigeria

Nigerian health officials are in the process of trying to trace 30,000 people, believed to be at risk of contracting the highly-infectious virus, following the death of Patrick Sawyer in Lagos

Ebola (above) has already killed 672 people in Guinea, Liberia, Sierra Leone and Nigeria and infected more than 1,200 since it was first diagnosed in February. Symptoms include sudden fever, vomiting and headaches Medical personnel at the Doctors Without Borders facility in Kailahun, Sierra Leone, where leading Ebola doctor Sheik Humarr Khan died

The disease has swept through Western Africa, having first been detected in Guinea in February.

Since then victims have succumbed to the incurable illness, which starts with flu-like symptoms before evolving to cause catastrophic internal bleeding, in Sierra Leone and Liberia.

But it was the death of a U.S. citizen in the Nigerian captial of Lagos on Friday, that has prompted fears the disease could be on the brink of spreading to the West, as experts warn it could be carried across international borders by air travellers.

Mr Sawyer, a consultant for Liberia's Finance Ministry, died on Friday after arriving at Lagos airport on June 20, having vomited and suffered diarrhoea on two flights.

The 40-year-old U.S. citizen had been to the funeral of his sister, who also died from the disease.

A woman quarantined at the Queen Elizabeth Hospital in Hong Kong has tested negative for the disease, despite returning from a trip to Kenya with Ebola-like symptoms.

Meanwhile two suspected patients in the UK have also tested negative.

But the panic sparked by Mr Sawyer's death is 'justified' says Dr Derek Gatherer of the University of Lancaster, claiming the virus is as infectious as flu.



He warned each person infected with the disease could spread the virus to at least two other people.

'Anyone on the same plane could have become infected because Ebola is easy to catch,' he said.

'It can be passed on through vomiting, diarrhoea or even from simply saliva or sweat - as well as being sexually transmitted.

'That is why there is such alarm over Mr Sawyer because he became ill on the flight so anyone else sharing the plane could have been infected by his vomit or other bodily fluids.'

Doctors Without Borders (MSF) echoed the concerns, warning the crisis gripping West Africa will only get worse, adding it is impossible to rule out the disease spreading to other countries.

Bart Janssens, MSF's director of operations, warned there was no overarching vision of how to tackle the outbreak, in an interview with La Libre Belgique newspaper.

'This epidemic is unprecedented, absolutely out of control and the situation can only get worse, because it is still spreading, above all in Liberia and Sierra Leone, in some very important hotspots,' he said.

'We are extremely worried by the turn of events, particularly in these two countries where there is a lack of visibility on the epidemic.

'If the situation does not improve fairly quickly, there is a real risk of new countries being affected.

'That is certainly not ruled out, but it is difficult to predict, because we have never known such an epidemic.'

'IT SCARES THE JESUS OUTTA ME': OUTRAGE AS NIGERIAN 'NOLLYWOOD' STAR POSTS PICTURE WEARING EBOLA MASK AS HE FLEES LIBERIA

A Nigerian actor has sparked outrage after posting an image of himself wearing an Ebola mask while sitting in a first class airport lounge as he flees Liberia. 'Nollywood' star Jim Iyke posted a message on his Instagram page saying he had cut short a business trip to Monrovia in Liberia - where at least 600 people have already died from the disease. The image of Iyke sitting on green leather-clad seats in the airport's luxury first class lounge while wearing an expensive designer watch and sunglasses was accompanied with the caption: 'Not ashamed to admit this scares the Jesus outta me #Ebola.' Nigerian actor Jim Iyke posted this picture on his Instagram account, revealing he had cut short a business trip to Liberia over fears the Ebola virus is spreading in the West African country The contrast between Iyke's image of first class luxury is in stark contrast to the thousands of terrified Liberians who are living in fear of contracting the deadly disease. However, much of the anger about his image stemmed from fear among Nigerian citizens that Iyke appeared to be travelling back to the country without having been tested to see if he was infected. Twitter user @Avariberry posted a message reading: 'Jim Iyke or Not... he gotta be screened. #TestJimIyke.' Meanwhile @IcallDibbz_ said: 'Please ooo, James Ikechukwu, aka Jim Iyke, should be quarantined.' Others picked up on the fact Iyke had an expensive face mask to protect himself, but was wearing a short-sleeved T-shirt.

Health campaigners have petitioned U.S. authorities, calling for the Food and Drug Administration to fast-track their approval of a new Ebola drug, which could be the first cure for the disease Professor Peter Piot, the director of the London School of Hygiene and Tropical Medicine, said the virus, although deadly, is 'in theory easy to contain'

It comes as h ealth campaigners today called for U.S. authorities to speed up their approval of a new drug hoped to be the first cure for the deadly Ebola virus.

They are calling on the Food and Drug Administration (FDA) of the United States to fast-track their authorisation of the TKM-Ebola drug.



The petition, created on change.org, states: 'One of the most promising is TKM-Ebola manufactured by Tekmira Pharmaceuticals.

'This drug has been shown to be highly effective in killing the virus in primates and Phase 1 clinical trials to assess its safety in humans were started earlier this year.'

VIRUS 'EASY TO CONTAIN IN THEORY' SAYS MAN WHO DISCOVERED IT

Professor Peter Piot, the director of the London School of Hygiene and Tropical Medicine and the scientist who discoverd the Ebola virus in 1976, in Zaire, said the disease, although aggressive is 'in theory easy to contain'. He told CNN: 'Well it’s spectacular because once you get it, at least with this strain of Ebola, you’ve got like a 90 per cent chance of dying.

'That’s spectacular by any standard – one of the most lethal viruses that exist.

'On the other hand, you need really close contact to become infected.

'So just being on the bus with someone with Ebola, that’s not a problem.

'It’s also not iatrogenic [ph] so it’s not transmitted through, you know, droplets and so on. So it is really something that in theory is easy to contain.'

In July the FDA put clinical trials on hold, despite the face 14 research participants had already safely tolerated the drug, campaigners said.

Those responsible for the petition added: 'Given that at least one patient has transferred the disease from Liberia to Nigeria by air travel, the possibility of a global pandemic becomes increasingly likely.



'In view of this it’s imperative that the development of these drugs be fast-tracked by the FDA and the first step should be releasing the hold on TKM-Ebola.



'There is a precedent for fast tracking anti-Ebola drugs in emergency cases as happened last year when a researcher was exposed to the virus and received an experimental vaccine.'

Mr Sawyer was put in isolation at the First Consultants Hospital in Obalende, one of the most crowded parts of the city, home to around 21 million people.

He took two flights to reach Lagos, from Monrovia to Lome and then onto the Nigerian capital.

So far 59 people who came into contact with Mr Sawyer have been identified by Nigerian health officials, and are under surveillance.

But health officials have said they are looking at contacting 30,000 people who could be at risk of contracting the disease.

Professor Sunday Omilabu, from Lagos University Teaching Hospital, said health officials are in the process of tracing all those people who are thought to have been in contact with Mr Sawyer.

He said: 'We've been making contacts. We now have information about the (flight) manifest.

'We have information about who and who were around.

'So, as I'm talking, our teams are in the facility, where they've trained the staff, and then they (are) now asking questions about those that were closely in contact with the patient.'

Public health adviser, Yewande Adeshina, added: 'We're actually looking at contacting over 30,000 people in this very scenario.

'Because any and everybody that has contacted this person is going to be treated as a suspect.'

A number of patients have been discharged from Ebola treatment centres in Guinea after successfully beating the Ebola virus, says Médecins Sans Frontières

U.S. citizen Patrick Sawyer, pictured with his daughter Ava, died on Friday in the Nigerian capital of Lagos having become infected with the Ebola virus. His death prompted fears of a global pandemic after he flew from Liberia to Nigeria

Decontee Sawyer, the wife of Liberian government official Patrick Sawyer, said she shudders to think how easily her husband could have returned to the U.S. carrying the disease

Foreign Secretary Philip Hammond today declared the disease a 'very serious threat' to Britain as he prepared to chair an emergency meeting on how to bolster the country's defences against the vicious virus.

The meeting came as the European Union today allocated an extra two million euros to help fight the Ebola outbreak, bringing total funding to 3.9 million euros.

EU Humanitarian Aid Commissioner, Kristalina Georgieva, said: 'The level of contamination on the ground is extremely worrying and we need to scale up our action before many more lives are lost.'

The European Union has deployed experts on the ground to help victims and try to prevent contagion but Georgieva called for a 'sustained effort from the international community to help West Africa deal with this menace'.



British airlines are on alert for cases of the deadly virus, after tests revealed a man died in Nigeria from the disease, having been allowed to board an international flight from Liberia.

The International Civil Aviation Organisation (ICAO) has met global health officials on implementing measures to halt the spread of the disease, as the pan-African ASKY airlines suspended all flights to and from the capitals of Liberia and Sierra Leone.

British Airways said it was maintaining its flights to west Africa but would monitor the situation closely.



A British man has also been tested for the Ebola virus, putting doctors on red alert that it could be on its way to the UK.

A spokesman for Hong Kong's Queen Elizabeth Hospital, said the Centre for Health Protection (CHP) will be notified if it is confirmed the patient is suffering from the Ebola virus.

In Nigeria health officials said today, they are in the process of tracing 30,000 people at risk of contracting the disease after coming into contact with a Liberian man who died on Friday.



Meanwhile, the British man was taken to hospital in Birmingham after complaining of feeling ‘feverish’ on a flight back to the Midlands from West Africa.

He had been travelling from Benin, Nigeria via Paris, France when he became unwell on Monday.

However, after undergoing a number of tests he was given the all-clear for the virus which has already killed 672 people in Guinea, Liberia and Sierra Leone and infected more than 1,200 since it was first diagnosed in February.

In another scare, medical staff at Charing Cross Hospital in London became concerned a man in his twenties had caught the virus this week.

But his symptoms were quickly confirmed as not being linked to the bug and doctors ruled out the need for an Ebola test.

Tragic: US citizen Patrick Sawyer (pictured with his wife Decontee) died after contracting Ebola in West Africa

Fears: Medical staff at Charing Cross Hospital in London became concerned a man in his twenties had caught the virus this week. However, his symptoms were later put down to another bug and Ebola was ruled out

Fears over the ability to contain the spread of Ebola were augmented last night as it emerged the body of a young stowaway was found hidden in on a U.S. military plane.

The Pentagon said the young boy, believed to be of African origin, was found near the wheel of a cargo plane which landed in Germany.

AIRLINES ON EBOLA RED ALERT

British airlines are on alert for cases of the deadly virus, after tests revealed a man died in Nigeria from the disease, having been allowed to board an international flight from Liberia. Patrick Sawyer, a consultant for Liberia’s Finance Ministry, had been in Liberia for the funeral of his sister, who also died from the disease, and was on his way back to his home in the US. The 40-year-old arrived in Lagos, Nigeria, on July 20 and had suffered from vomiting and diarrhoea on two flights. He was put in isolation in hospital and died on Friday. Nigeria has closed the Lagos hospital where Mr Sawyer was treated and put its airports and ports on 'red alert'. ASKY airlines, the carrier which flew Mr Sawyer, suspended flights to the capitals of Liberia and Sierra Leone yesterday. In Britain, the Department of Transport said UK airlines are 'monitoring the situation'. Virgin Atlantic told the Daily Express their staff have been trained to spot the signs and symptoms of the virulent disease, which has claimed the lives of 672 people in West Africa since February.

The plane was on a routine mission in Africa, and had made stops in Senegal, Mali, Chad, Tunisia and the Naval Air Station Sigonella in Sicily before arriving at Ramstein.

It is thought the boy climbed aboard in Mali, which borders Guinea - where the current Ebola outbreak originated at the end of last year.



It comes as hospitals and medical centres across the UK remain on red alert for the virus, with doctors being told to look out for symptoms of the disease which can go unnoticed for three weeks and kills 90 per cent of victims.

The Department of Health confirmed protections have been put in place to deal with the deadly bug, should it spread to Britain.

A spokesman said: ‘We are well prepared to identity and deal with any potential cases of Ebola, although there has never been a case in this country.’

The Government’s chief scientific advisor also issued a frank warning about the disease, which he said could have a ‘major impact’ on the UK.

Sir Mark Walport said: ‘The UK is fortunate in its geographical position. We’re an island. But we are living in a completely interconnected world where disruptions in countries far away will have major impacts.

‘The most dangerous infections of humans have always been those which have emerged from other species,’ he told the Daily Telegraph, referring to the virus originating in fruit bats and monkeys.

He said the Government was ‘keeping a close eye’ on the outbreak and was prepared for the disease spreading to Britain, but insisted any risk was ‘very low’.

He added: ‘We have to think about risk and managing risk appropriately.’

Public Health England has added to fears about the spread of the virus by saying it was ‘clearly not under control’.

Virus: Symptoms of Ebola include high fever, bleeding, damage to the nervous system and vomiting

Outbreak: There is no vaccine or cure for Ebola, which is spread by contact with infected blood or bodily fluids

The Government agency’s global health director, Dr Brian McCloskey, said: ‘It is the largest outbreak of this disease to date, and it’s clear it is not under control.

‘We have alerted UK medical practitioners about the situation in West Africa and requested they remain vigilant for unexplained illness in those who have visited the affected area.’

The current outbreak started in Guinea in February and spread to neighbouring Liberia and Sierra Leone in weeks. Symptoms include high fever, bleeding and damage to the nervous system.

There is no vaccine or cure. It is spread by contact with infected blood or other bodily fluids.

All outbreaks since 1976 – when Ebola was first identified – have been in Africa, with the previous highest death toll being 280.

However, authorities around the world have been put on high alert in recent weeks after an American doctor working in Liberia became infected and passed through an airport.

Nigerian health officials yesterday admitted they did not have a list of all the people who came into contact Patrick Sawyer, prompting fears the outbreak could spread.

But the manifesto appears to have been disclosed as Professor Sunday Omilabu, from Lagos University Teaching Hospital, said health officials are in the process of tracing all those people who are thought to have been in contact with Mr Sawyer.



He said: 'We've been making contacts. We now have information about the (flight) manifest.



'We have information about who and who were around.



'So, as I'm talking, our teams are in the facility, where they've trained the staff, and then they (are) now asking questions about those that were closely in contact with the patient.'

Public health adviser, Yewande Adeshina, added: 'We're actually looking at contacting over 30,000 people in this very scenario.



'Because any and everybody that has contacted this person is going to be treated as a suspect.'

Spreading: The outbreak has hit Guinea, Sierra Leone, Liberia and has now killed a man in far more densely populated Nigeria. The outbreak is the deadliest ever of the terrifying disease as the death toll crept past 670

Mr Sawyer, a consultant for Liberia’s Finance Ministry, had been in Liberia for the funeral of his sister, who also died from the disease, and was on his way back to his home in the US.

The 40-year-old arrived in Lagos, Nigeria, on July 20 and had suffered from vomiting and diarrhoea on two flights. He was put in isolation in hospital and died on Friday.

So far 59 people who came into contact with him have been identified and are under surveillance. But the airlines have yet to release flight information naming passengers and crew members.

Dr David Heymann, head of the Centre on Global Health Security, said every person who had been on the plane to Lagos with Mr Sawyer would need to be traced.

Sierra Leone’s top doctor fighting Ebola died yesterday after he contracted the virus just days ago. Sheik Umar Khan was credited with treating more than 100 patients.

Liberia closed most of its border crossings on Sunday and Nigeria’s airports and borders have been on full alert since Friday.