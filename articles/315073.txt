Ben Mutzabaugh

USA TODAY

More than 70 disgruntled passengers of Chinese carrier Hong Kong Airlines staged an 18-hour sit-in on one of the airline's jets to show their displeasure about a canceled flight this past weekend.

It apparently is the second such incident to hit Hong Kong Airlines in the past two months.

That's according to the South China Morning Post of Hong Kong, which writes:

"In the latest incident, some 200 passengers were aboard Flight 234, which had been due to depart at 9 p.m. on Friday, and most accepted an offer of a transfer to a later flight."

"But some refused and complained about the arrangements. They said they had been offered only HK$200 (about $25 U.S.) in compensation and not offered accommodation."

The passengers finally left the plane around 3 p.m. Saturday after the airline agreed to up its compensation offer, according to the Morning Post.

The Morning Post quotes one protesting passenger as telling China's People's Daily that the group's protest was "not about the money", but rather what the passengers say was the airline's "attitude." The group says it felt the airline should have offered "a reasonable amount of financial compensation."

Apparently Hong Kong Airlines eventually gave letters to the sit-in fliers that apologized and promised compensation of HK$800 — or about $103 U.S. — per passenger.

The Standard of Hong Kong says the flight was canceled because of weather.

"As there was no scheduled time for taking off, the company asked passengers to leave the plane and to wait, and distributed breakfast and lunch tickets to those leaving," Hong Kong Airlines says in a statement to the Standard.

That apparently was not satisfactory to the 70 of the flight's 276 passengers who stayed onboard for the sit-in protest.

Not all were sympathetic to the effort, according to posts on social media sites.

The Standard quotes one Sina Weibo user as saying: "Are they really willing to suffer that much to get so little in compensation? Is time not money?"

"It feels like extortion," another posted on Facebook, according to the Standard.

The Morning Post says a similar incident hit Hong Kong Airlines about two months ago, when 31 passengers refused to exit a flight from Bali after it suffered an eight-hour delay.