The minutes of the Bank of Japan board meeting held on April 30 showed that most of the members said risks on the price front could be assessed as being largely balanced, but one member viewed risks to prices as tilted to the downside.

Takehiro Sato proposed to change the current expression about the inflation, suggesting around 2 percent inflation will come into sight around the middle of the projected period.

Further, Sato said outlook for economic activity and prices should be adjusted to suggest that the Japan's "is judged as likely to head toward achieving around 2 percent inflation". But eight other members of the board rejected his proposals.

Takahide Kiuichi formulated a proposal to change inflation expectations and the outlook for prices.

Kiuichi also proposed that BoJ "will designate quantitative and qualitative monetary easing as an intensive measure with a time frame of about two years, and will review the monetary easing measures in a flexible manner when this time frame comes to an end." But the proposal was defeated by a majority vote.

Board member Sayuri Shirai also repeated her objections to the inflation outlook. She said "the inflation rate of 2 percent is likely to be reached toward the end of the projection period as the Bank continues with quantitative and qualitative monetary easing, aiming to achieve the price stability target of 2 percent."

One member expressed the view that a considerable number of firms had not yet passed on the tax increase to prices, and they would gradually do so over time as consumption bounced back.

Another member noted that the extent to which the improvement in the employment and income situation could absorb the effects of the consumption tax hikes was important.

At the April meeting, the policy board unanimously decided to leave the monetary policy unchanged. The bank voted to raise the monetary base at an annual pace of about JPY 60-70 trillion.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.