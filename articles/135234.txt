Since its premiere in March, the eight-part documentary series "Lindsay" has offered a candid look at Lindsay Lohan's personal journey through recovery, as well as her efforts to rebuild her career.

"It's a really f------up disease and it's really scary," a tearful Lohan says about alcoholism in the above sneak peek of the two-part season finale.

In the episode, Lohan is thrust into the media spotlight when scandal erupts after a trip to Miami. Back in New York, she introduces Miley Cyrus at Jingle Ball -- her biggest public appearance since leaving rehab -- and is itching to begin work on her starring role in a new thriller. "I'm seeing all of these other actresses do all of these things, and then, just realizing -- Oh [expletive], I don't have that anymore," Lohan says in the above video.

Later, Lohan addresses the public leak of her alleged private list of lovers and is overwhelmed with emotion when revealing the reason she turned cameras away during production. As the documentary comes to a close, Lindsay will reflect on what she has learned.

The finale of "Lindsay" will air this Sunday, April 20 at 9 p.m. ET on OWN.