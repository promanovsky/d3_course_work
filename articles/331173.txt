Amazon continues to grow larger and faster, but how big will the company get? The online retailer's growth has been at an exceptional pace within the last 20 years, making some people excited but others nervous.

Amazon is dabbling in many projects and ideas simultaneously these days, from shopping for almost everything online to creating electronic devices such as the Kindle and Kindle Fire, selling produce online (only available in parts of California and Seattle), making TV shows and movies, a rumored upcoming restaurant ventures, drones and now a smartphone called the Amazon Fire Phone.

The smartphone is Amazon's latest business venture. In an interview with The New York Times, Amazon founder and chief executive Jeff Bezos spoke about why they made the Amazon Fire and the company's hopes for the future.

Bezos said in making products, especially like the new smartphone, the company considers uniqueness. Bezos said the company asks themselves questions, such as, "If we were going to build an X, how would it be different? How would it be better?" Bezos said Amazon does not want to be "just different," and products must be geared toward the customer and useful, The New York Times reported.

The price of the Amazon Fire starts at $199.00 with 32 gigabytes. The user gets direct Amazon and Amazon Prime access, and new and existing customers get 12 months free of Prime.

About the hope for success of the Fire phone, Bezos said he is patient and very optimistic. He cited that they have had a long time to get things started.

"It's our job to keep inventing and to be patient. One thing leads to the next," Bezos said.

There are also rumors of Amazon going into a restaurant business. TechCrunch.com reported that Amazon is already offering similar coupons on food and business services like Groupon, as well as rolling out something similar to a food takeout service in the realm of GrubHub, Seamless and others.

Bezos started Amazon years ago by renting a house with a garage, hoping to have the same luck of Apple, HP and Microsoft, which have grown from that perspective. Bezos started off by selling books, The Economist reported. The name Amazon came from its geographical origin, and like the Amazon, Bezos wanted the company to grow like a giant river, along with huge goals.

Since then, Amazon has become one of the world's largest online distributions, and is involved in online and cloud computing, electronic books, video streaming and music downloads. The global electronic commerce is estimated at $1.5 trillion, according to The Economist.

Like with all large companies, success comes with competition, disagreements and accusations. The company has been accused of unfair competition, treating its employees unfairly, tax issues and pushing around its rivals, The Economist reported.

Still, after 20 years, Amazon is now rivaling Google, Apple and others.