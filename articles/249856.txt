Healthcare stocks were slightly higher, with the NYSE Healthcare Sector Index holding a 0.3% gain and shares of healthcare companies in the S&P 500 up 0.2% as a group.

In company news, PTC Therapeutics Inc. ( PTCT ) rallied Friday after European regulators gave conditional marketing approval for its Translarna drug candidate to treat Duchenne muscular dystrophy in patients five years and older.

PTCT shares jumped as much as 87.5% this morning to reach $28.73 apiece, more recently trading around $22.30 a share, up almost 46%. The stock has a 52-week range of $1.56 to $34.65 a share, declining about 4.3% through Thursday's close since pricing its initial public offering of stock last June at 16.00 a share.

The move reverses a 2013 decision by European Medicine Agency's Committee for Medicinal Products for Human Use, which rejected Translarna after saying a Phase II study did not provide convincing proof of efficacy. The company resubmitted essentially the same data again although this time the agency was satisfied with the data.

The European Commission typically endorses agency recommendations within about three months.

Other drug-makers working on prospective Duchenne muscular dystrophy treatments also are higher, with Sarepta Therapeutics ( SRPT ) ahead nearly 5% at $34.06 a share this afternoon while Prosensa Holding ( RNA ) was up more than 21% at $8.05 a share.

In other sector news,

(+) ZIOP, (+8.5%) Announces results demonstrating anti-tumor effects of its Ad-RTS-IL-12 DNA-based therapeutic candidate in a brain cancer model, reducing tumor mass and glioma stem cells as well as extending survival compared with other treatments.

(-) INO, (-5.9%) Shareholders approve a 4-for-1 reverse split of the company's common stock

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.