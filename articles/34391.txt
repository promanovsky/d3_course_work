Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

He cared for them all his working life, now giraffes at a Dutch zoo have bade a final farewell to their keeper dying of cancer.

In a tender moment one even leaned over the rails of their cage and gave their keeper, only named as Mario and gave him a farewell kiss as he was wheeled into their enclosure on his hospital bed.

The heartbreaking moment came at Rotterdam’s Diergaarde Blijdorp Zoo, in Holland after a charity made the 54-year-old’s last wish come true.

The maintenance worker’s request to see his beloved giraffes whose enclosure he cleaned out every day resulted in the unexpected touching moment when one of the animals gave him a last nuzzle and a final farewell kiss.

The goodbye trip was organised by Dutch charity Ambulance Wish Foundation.

(Image: Stichting Ambulance wens Nederland)

Founder Kees Veldboer, said: “These animals recognised him, and felt that things weren’t going well with him.

“It was a very special moment. You saw him beaming.”

Mario, who is also mentally disabled, then asked for a moment to say goodbye to his colleagues at the zoo, where he has worked for 25 years.

Kees said: “It was very nice that we were able to work on the last wish of this man.”