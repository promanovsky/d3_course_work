From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Amazon has just launched its new smartphone, the Fire Phone.

The 32GB version of the device is available exclusively on AT&T for $200. A 64GB version on the phone sells for $400. Preorders start today, AT&T says.

The phone is available with a two-year contract or via AT&T’s Next phone trade-in plan at $27 per month (32GB version).

New Fire Phone owners will also get 12 months of free Amazon Prime.

Firefly

The new phone’s sexiest feature is Firefly. At a push of a button on the side of the phone, the back camera recognizes products in the real world, with bar codes and other product information, then displays ways to buy the product on Amazon.

Firefly also can listen to a song and recognize it and then show you how to buy it on Amazon. It can watch a scene from a TV program and tell you how to buy the same episode on Amazon. It also recognizes works of art, Amazon CEO Jeff Bezos points out.

Another nice feature is semantic boosting. Your Fire can look at a phone number, on a sign or written on a piece of paper, and then recognize and store it. You can also point your phone at a posted sign, and Firefly will pick out just the important info and images and then store them.

Amazon is releasing an SDK so that third-party developers can develop apps using the Firefly audio, video, and product recognizers. MyFitnessPal has already developed an app that recognizes food using Firefly, for example.

“We can’t wait to see how the developer community will surprise us with what they do with Firefly,” Bezos said.

3D functionality: ‘Dynamic perspective’

As expected the Fire Phone uses front-facing cameras to detect where the viewer’s head is pointing, so that the picture on the screen can change according to the viewer’s perspective. In fact, the phone makes these adjustments 60 times per second, Bezos said.

The cameras are situated at the four corners of the front of the phone. They are infrared, so they can work in the dark too. They detect faces and derive a series of three numbers that together represent the user’s side-to-side and near-to-far position in relation to the phone.

So you see a depthy 3D image that moves as your perspective moves. These images can be used as the front screen image on the phone.

Dynamic Perspective, as Amazon calls the sensor technology, seemed effective when using maps. Structures like buildings appear in 3D. You can tilt the phone to reveal various types of label information on the map.

The feature can be used for shopping applications, in a similar way. Bezos demonstrated how tilting the phone would reveal different aspects and information about a product. He pulled up a picture of a dress, then with a slight tilt brought up a view of the back of the dress, along with sizing information and pricing at the bottom of the screen.

For reading books, the tilt functions can be used to scroll through text as you read. You control the speed and direction of the scrolling by tilting the phone. You can also set a constant scrolling speed, so the text moves as you read. To stop the scrolling you just press your thumb down on the text.

Other features

For navigation, the Fire Phone uses a top band on the display where you can scroll through major phone functions like camera and email. When email is fixed upon, for example, several recent emails show up in the area below the navigation band.

All functions in the phone are built on three panels. You can tilt the phone to reveal a left panel, like a list of songs in Music, for example. The middle panel works for the music-player controls. Swipe out the right panel and you see the song lyrics.

Dynamic Perspective also works for gaming for navigation and controls.

Bezos says Dynamic Perspective is completely dependent on knowing where the user’s head is.

Quick stats for the new Fire Phone:

4.7 inch IPS LCD HD display

Optimized for one hand use

590 nits of brightness for outdoor use

Everything works in portrait or landscape mode

Quad core 2.2 GHz processor

Adreno graphics chip

Dedicated buttons to launch camera and Firefly functions

Unlimited storage for photos on the amazon cloud

13 megapixel camera

Optical image stabilization

2GB RAM

Amazon Music and Spotify

Thousands of magazines and newspapers, for reading or listening, or both at once

The new phone marks Amazon’s first foray into the smartphone category, and it’s entering a crowded field. The big question is whether or not the new phone has enough going for it it make consumers give up their iPhone or fancy Android, and make the switch to an Amazon phone.

“If you’re Amazon your telling people ‘you’ve just bought a vending machine, now go buy stuff,'” said analyst Avi Greengart of Current Analysis after the event here in Seattle.

Greengart isn’t completely sold on the 3D angel, however. “They are mainly promoting the 3D technology, but other than the mapping functions, it’s not immediately clear if the technology is really useful, or just cool,” he said.

Amazon has had great success with its line of Kindle e-readers because the devices created new points of sale and consumption for Amazon digital products. Amazon hopes that the new phone will provide a similar opportunity for not only its digital assets, but for virtually any item in its online marketplace.

The Fire OS that runs on the phone focuses primarily on content consumption, with a customized interface and heavy ties to content available from Amazon’s own storefronts and services.