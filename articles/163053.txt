TORONTO -- Countries should be on the lookout for cases of MERS in people returning from Middle Eastern countries affected by the virus, the World Health Organization said Thursday in an updated risk assessment of the new coronavirus.

The number of known infections has skyrocketed in recent days, with Saudi Arabia alone reporting 48 cases on Wednesday and Thursday. In the 20 months since the world became aware a new coronavirus was infecting people, there has not been a single month where the total cases from all affected countries was as high as that two-day tally.

In the past two weeks alone cases were exported to Greece, Malaysia, Jordan and the Philippines, the global health agency said, warning that the virus may pop up in various parts of the globe carried by people who have been to countries like Saudi Arabia and the United Arab Emirates.

"It is very likely that cases will continue to be exported to other countries, through tourists, travellers, guest workers or pilgrims, who might have acquired the infection following an exposure to the animal or environmental source, or to other cases, in a hospital for instance," the risk assessment said.

The WHO noted that diagnosing cases rapidly may be a challenge because some have mild or atypical symptoms. The man detected in Greece, for instance, did not initially appear to have a respiratory infection. He had a protracted fever and diarrhea, but doctors were suspicious because he had travelled to Greece from Saudi Arabia. The man developed pneumonia while in hospital.

The risk assessment suggested that given the potential to initially miss MERS cases, health-care workers should apply infection control precautions consistently with all patients, all the time, regardless of their diagnosis.

The document said the WHO continues to recommend against border screening as a method of trying to detect incoming cases. As well, it recommends that governments not apply travel or trade restrictions against countries that are sources of MERS infections.

A senior official of the Public Health Agency of Canada said federal authorities are in regular communications with provincial and territorial counterparts about the situation and the possibility of imported cases of MERS or avian influenza in travellers.

Dr. Theresa Tam said since last September more than 3,000 people have been screened in Canada, but to date no cases of MERS have been found.

"The bottom line is that we've been ramped up for quite some time, not just on this bug but to deal with any emerging viruses," said Tam, who is the head of the Public Health Agency's health security infrastructure branch.

"Like Greece or France or Italy, it's possible to get a traveller for sure. And so the system is designed to try to pick that up."

France, Italy, Germany and Britain have also diagnosed MERS cases in people who travelled to the Middle East or flew from there to Europe for treatment. Those importations happened in 2012 and 2013.

The number of MERS cases has soared this month, fuelled in part by outbreaks in health-care settings in Jidda, Saudi Arabia, and Abu Dhabi, United Arab Emirates.

The WHO has confirmed 254 cases, but at this point its tally lags far behind the numbers reported by affected countries. The combined case count announced by countries has reached about 385 cases since the first known infections. The death toll to date is over 100.

More than 45 per cent of all MERS cases that have ever been diagnosed have been recorded this month. That enormous spike in cases is raising concerns about the virus, with experts wondering if it has changed to become more easily transmitted from person to person.

The WHO says three-quarters of the recent cases appear to be ones where the transmission was person to person, not from an animal or environmental source to a person.

In two cases, person-to-person-to-person spread has been suspected, the risk assessment said.

There have been no recent reports of scientists analyzing MERS viruses to see if they have changed. But on Thursday, German coronavirus expert Dr. Christian Drosten confirmed that he recently received a shipment of samples from Saudi Arabia and his laboratory has begun to sequence the material.

"Currently the sequences don't tell a lot. They look all the same," said Drosten, who is the head of virology at the University of Bonn.

He cautioned that the work has just begun, and only a small portion of the viruses have been sequenced, but said that what he has seen so far looks 100 per cent identical to viruses from earlier in the outbreak.

The WHO risk assessment said it appears that there is slightly more human-to-human spread than there has been in the past. But whether that is because the virus had become more transmissible or is due to a combination of outbreaks in hospitals and more aggressive searching for cases can't be determined at this point.

The global health agency suggested that people who are likely at high risk of becoming seriously sick if they contract the virus should take precautions when visiting farms, barn areas or markets where camels are present.

Camels have been heavily implicated in the MERS story, though it is still not clear how people are catching the virus from the animals.

The WHO said people at high risk should think about avoiding camels, and refraining from drinking raw camel milk or food that may have been contaminated with animal secretions until it has been properly washed, peeled or cooked.

People considered to be at high risk of becoming seriously ill with MERS include those with diabetes, chronic lung disease, kidney failure or who are immunocompromised.