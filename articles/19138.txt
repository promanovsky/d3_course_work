Rolling Stones frontman Mick Jagger has spoken out about the death of his girlfriend, fashion designer L’Wren Scott. Scott, 49, who dressed some of the most famous women in the world, was found dead in her Manhattan apartment in an apparent suicide.

Jagger posted a statement on his Facebook page Tuesday morning.

I am still struggling to understand how my lover and best friend could end her life in this tragic way. We spent many wonderful years together and had made a great life for ourselves. She had great presence and her talent was much admired, not least by me. I have been touched by the tributes that people have paid to her, and also the personal messages of support that I have received. I will never forget her,

Mick

The group has already canceled a show scheduled for Wednesday in Perth, Australia; on RollingStones.com, a statement says that “no further information is available at this time” regarding a rescheduled date.

Jagger and Scott had been dating since 2001, People reported; his rep has already put out a statement denying a report that the couple had recently split.

In The Post, Robin Givhan wrote that Scott “helped to establish a red-carpet vernacular for older actresses.”

“Her style was audaciously body-conscious but not revealing. Even though Scott was inspired by her own willowy frame, women with significant curves and confidence, such as first lady Michelle Obama, embraced her designs,” Givhan said. “The clothes weren’t easy, but they were welcoming.”