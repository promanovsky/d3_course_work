HTC’s finances haven’t been that great for the past few quarters. It has seen 28 straight months of year-over-year declining sales and it even posted a loss in the previous quarter. Though in the last quarter it was able to break through and reported that it unit sales were up 2.2 percent year-over-year. The company also placed its bet on the HTC One M8, forecasting that the smartphone will help push it into the green. That it has certainly done. Unaudited Q2 2014 financial results were posted online today and HTC has posted a profit.

Advertising

The streak of losses has been turned around in this quarter as HTC reports a total of $2.2 billion in revenue with a $92 million profit. Unit sales nearly match those from last year. The profit may not be as much as its rivals like Apple and Samsung pull in but at least its better than a loss. Moreover its likely to appease investors who were calling for the company to do more in order to push itself out of the red.

HTC now aims to cash in on the success of its 2014 flagship. It has released a variant that brings similar specifications in a plastic body with a relatively lower price point called HTC One M8 Ace, the toned-down HTC One mini 2 variant is also on offer.

There were rumors that the company had plans for a One M8 Prime as well but apparently that was shelved for good. The company would certainly want to keep this momentum going and raise its profits even higher in the next quarter, no doubt the market will be keeping a keen eye on its performance.

Filed in . Read more about HTC, HTC One (M8) and Htc One M8.