Apple’s mixed victory over Samsung

Share this article: Share Tweet Share Share Share Email Share

Joel Rosenblatt and Susan Decker Washington The award of $120 million (R1.25 billion) to Apple in its case against Samsung Electronics over smartphone technology may not give the mobile device giants an incentive to end their four-year global legal battle. Apple had wanted $2bn. After hearing almost four weeks of evidence in the companies’ second US trial, a jury in California found on Friday that Samsung infringed two of the four Apple patents it considered and Apple infringed one of two Samsung patents. The trial revolved around whether Samsung, the maker of Galaxy phones, used features in Google’s Android operating system that copied the iPhone maker’s technology.

The verdict sets the stage for each company to seek an order banning US sales of some of its competitor’s older devices.

“They’re each looking for that knockout punch and they don’t seem to be able to find it,” said Susan Kohn Ross, a lawyer with Mitchell Silberberg & Knupp in Los Angeles. “They feel they are on an even keel and need to keep punching.”

The world’s top two smartphone makers have spent hundreds of millions of dollars in legal fees on battles across four continents.

But the latest verdict was a narrow victory for Apple. Even if Apple persuades the judge to issue an injunction halting sales, most of the 10 devices it sought to ban during the trial are no longer sold by Samsung in the US. – Bloomberg