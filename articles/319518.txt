LOS ANGELES, June 25 (UPI) -- Hunger Games: Mockingjay Part 1 released a creepy new teaser Wednesday.

The video is presented as a public service announcement from President Snow (Donald Sutherland) to Panem. Peeta (Josh Hutcherson), who was captured and taken prisoner in Catching Fire, stands by silently as the ruthless tyrant delivers a thinly veiled threat to Katniss (Jennifer Lawrence) and other capitol dissenters.

Advertisement

"Ours is an elegant system conceived to nourish and protect," he says. "Your districts are the body; the capitol is the beating heart. Your hard work feeds us, and in return, we feed and protect you. But if you resist the system and starve yourself, if you fight against it, it is you who will bleed."

Hunger Games: Mockingjay Part 1 is scheduled for release November 21. The movie has also unveiled new posters that feature heroes from each Panem district.