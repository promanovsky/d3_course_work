Movie

The Oscar-winning movie had a successful run in its last market, passing 'Toy Story 3' and 'Pirates of the Caribbean: Dead Man's Chest' in worldwide position.

Mar 31, 2014

AceShowbiz - Disney's "Frozen (2013)" has officially become the highest grossing animated film of all time after opening at number one in Japan this weekend. The story about two sisters passed "Toy Story 3" as it pushed over $1 billion in worldwide box office.

"Frozen" now occupies the tenth spot in the global list, earning $398.4 million domestically and $674 million internationally for a total of $1.072 billion. The movie which was released in the U.S. and Canada in late November was coming in strong in its last market, Japan, earning $50.5 million so far.

This is the first billion-dollar title from Disney Animation Studios and the seventh from Disney. The Oscar-winning movie runs in 36 countries including U.K., Germany and South Korea. Internationally, "Frozen" is the biggest Disney or Pixar animated film of all time in 27 territories such as Russia, China and Brazil.

Released in 2010, "Toy Story 3" racked up $1.063 billion in sales. "Frozen" also took over "Pirates of the Caribbean: Dead Man's Chest" position and is on the tail of "The Dark Knight Rises". The success of "Frozen" has spawned a trending hastag on Twitter, #CongratulationsFrozen.