A new study at the University of Michigan Medical School found that infants may be able to detect their mothers' fear — through smell.

The study, published in the journal Proceedings in the National Academy of Sciences, suggests that infants, or at least infant rats, recognize their mothers' feelings of being threatened by identifying certain smells.

"Our research demonstrates that infants can learn from maternal expression of fear, very early in life," lead researcher Jacek Debiec told Business Standard. "Most importantly, these maternally-transmitted memories are long-lived, whereas other types of infant learning, if not repeated, rapidly perish."

The researchers studied rats that they taught to fear the smell of peppermint by "exposing them to mild, unpleasant electric shocks" before they were pregnant, according to Business Standard. The mother rats then passed the fear along to their offspring as they acted distressed at the smell.

Debiec told Business Standard that the study's findings may help "prevent children from learning irrational or harmful fear responses from their mothers." Meghan DeMaria