Justina Pelletier, the 16-year-old Connecticut girl taken from her parents more than a year ago after two hospitals clashed over her diagnosis, told reporters how she felt on her way home to her family Wednesday.

"Awesome!" the teen said from a car making its way to the family's home in West Hartford.

Moments before the reunion after 16 months in the custody of the Massachusetts Department of Children and Families, Justina's mother and sister told reporters she faces a long recovery from an ordeal they hope "never happens again" to another child.

Linda Pelletier, the girl's mother, told reporters she was “so grateful” her daughter is returning to the family’s West Hartford home following an order by a Massachusetts judge effective Wednesday.

“I’m just so grateful that today has finally come,” Pelletier said, referencing the ordeal that began when two respected Boston hospitals clashed over the girl’s diagnosis. “And hopefully she’ll walk again — I’m not sure.”

Pelletier said intense rehabilitation, including pool therapy, lies ahead for her daughter, who remains unable to walk. The girl’s long-running legal battle began when her parents disagreed with a psychiatric diagnosis provided by Boston Children’s Hospital, saying they wanted Justina returned to her original physician at Tufts Medical Center, where she was previously treated for mitochondrial disease, a group of rare genetic disorders that affect cellular energy production. The state’s Department of Children and Families ultimately took custody of Justina, claiming she was the victim of “medical child abuse.”

Jennifer Pelletier, the girl’s older sister, said Justina has lost significant weight since the dispute began.

“Yes, it’s great Justina is coming home, I’m still in a little shock that this chapter is over,” she said. “But the bad part about this is Justina has still suffered … She’s truly like a rag doll. She has no muscle on her bone.”

The case began when the girl's parents disagreed with a psychiatric diagnosis given by Boston Children's Hospital in February 2013 and said they wanted their daughter returned to her original physician at Tufts Medical Center, who had previously treated Justina for mitochondrial disease, a group of rare genetic disorders affecting cellular energy production.

In May, Justina was moved from Massachusetts to a facility in Thompson, Conn., allowing her parents to visit with their daughter, but doing little to dampen their determination to win her back for good. In a 45-second, videotaped plea, first posted on a Facebook support page last week, Justina is seen sitting in a chair and pleading plaintively with Massachusetts juvenile court Judge Joseph Johnston.

"All I really want is to be with my family and friends," the girl says in the video, her voice faltering at times. "You can do it. You're the one that's judging this. Please let me go home."

The judge granted the girl's wish after state officials filed a motion agreeing that the teenager should be returned to her family.

In his ruling, Johnston credited the Thompson facility, the Justice Resource Institute Susan Wayne Center for Excellence, saying it “proved to be an excellent placement in which Justina and her family have received necessary and appropriate support and services working towards the goal of Justina’s return home.”

Johnston’s decision, handed down Tuesday, also contained praise for the Pelletier family, saying Linda and Lou Pelletier, have been cooperative and engaged in services. He said the local school district in West Hartford 4. The West Hartford has created a comprehensive special education plan for Justina that includes small group and individual classroom instruction, occupational therapy, speech/language therapy, physical therapy, and individual counseling.

The school work will be part of what the Pelletiers say will be a long healing process. A former competitive skater, Justina is now confined to a wheelchair, unable to walk.

Rev. Patrick Mahoney of Liberty Counsel, which represented the Pelletiers in court, said Justina’s father was prepping their home for her arrival midday Wednesday.

“This is a joyous day,” Mahoney said. “The Pelletiers are thrilled — 16 months they waited for this day.”

Despite the joyous development, Mahoney said the family is determined to ensure that no one else experiences the same nightmare and accompanying setbacks as Justina, who previously read at a seventh-grade level. She’s now reading at a first-grade level, Mahoney said.

“They had to endure this general sense from the public that they weren’t loving, careful parents,” Mahoney said. “This is a loving family who waited for this day … If anyone had any doubts, today is vindication. The most important thing is Justina getting well, but the family has been totally vindicated today.”

Later Wednesday, the girl's father, Lou, reportedly vowed that she would one day return to the rink.

"She was a very active girl, and now as you saw me picking her up, she’s going to have a long way to go before she’s ice skating again,” he said. "But you know what? She will ice skate again."