Celebrity

The former husband of Jennifer Lopez has to pay the former Miss Universe $26,800 a month in child support for their two kids.

Jun 14, 2014

AceShowbiz - Marc Anthony has to double his child support to former wife Dayanara Torres. He originally paid the former Miss Universe $13,400 a month, but now is ordered by a judge to pay $26,800 after a court battle with the Puerto Rican beauty.

The couple got divorced in 2004 after four years of marriage. They have two sons Christian Anthony Muniz, 13, and Ryan Anthony Muniz, 10. In November last year, she went to court to seek an increase in child support payments from her ex-husband.

She wanted $123,426 in child support for their two kids, reasoning that her financial needs had changed over the course of 10 years. According to her, she required an estimated $4,000 - $7,000 a month for a nanny and housekeeper only. She also said Marc had only seen the children a total of 35 days, a claim he vehemently denied.

Marc also has two kids, twins Emme Maribel Muniz and Maximillian David Muniz, with Jennifer Lopez. He also paid child support to his second wife after filing for divorce in 2012.