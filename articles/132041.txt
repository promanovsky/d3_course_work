New York: Sina Weibo, China’s answer to Twitter, debuted on the Nasdaq exchange Thursday with a 19.1 per cent jump despite an IPO that went out undersubscribed and lower priced than hoped.

In a spate of buying that suggested that Wall Street’s waters are still welcoming to loss-making technology high flyers, and to Chinese firms as well, Weibo shares rose from the subscription price of $17 (Dh62) to as high as $24.28, before settling the day at $20.24.

That is good news for Alibaba, the immensely profitable Chinese online commercial gateway that is preparing its initial public offering for the US market later this year.

Weibo sought to raise $380 million by selling 20 million shares for as much as $19 each.

But underwriters could only find demand for 16.8 million shares at the low end of the offer, $17, bringing the company $287 million.

Charles Chao, the chief executive of Chinese online power Sina Corp, Weibo’s parent, shrugged off the undersubscription, saying it was mainly important to achieve the listing and establish Weibo’s separate identity.

“It’s a tough market... The entire IPO market is rather soft,” he said in an interview.

“To have a successful listing for us is probably the most important. We do not actually care too much about the temporary price for the stock,” he said.

“If we can over the longer term keep executing our strategies and innovating in a very focused way, we can create shareholder value.”

Weibo, launched in August 2009, is China’s largest social media service with 144 million active monthly users. It is most often compared to Twitter.

But by allowing more content and functionality, it overlaps with Facebook as well. Twitter and Facebook are banned in China.

Weibo has yet to make a profit, losing $38 million last year on revenues of $188 million, and another $47 million in the first quarter of this year, though revenues jumped to nearly $68 million for the three months.

Chao said Weibo is rapidly building income from display and performance-based ads.

“We are making great progress in both advertising areas, serving different clients bases from brand advertisers to SMEs (small and medium-sized firms) to e-commerce merchants,” he said.

“There is no question that (Weibo’s) revenue growth is much higher than our existing portal business.”

Slowdown no worry

Critics say Weibo faces a challenge from rival network WeChat, owned by China’s Tencent group, but Chao says the two are distinct.

While Weibo’s messaging is open and public, WeChat users confine their messages to their own selected groups of followers, keeping discussions private and offering more protection from Beijing’s censors.

Even so, he admits, the two compete with each other in terms of time spent by the users. Yet Weibo has the advantage for advertisers, he insisted.

“Fundamentally... a public network is much more efficient than a private network,” he said. “A private network can never be efficient in terms of content distribution.”

Chao also said China’s economic slowdown was also not a problem for Sina or Weibo, and could possibly work to their advantage.

“Maybe a downturn in the economy will have some impact on the advertising business we are doing, but over the longer term, we’re not concerned at all.”

Internet

“The internet has proven that it is very resilient... What the internet does is, it increases the efficiency of everything, and so sometimes in bad times Internet companies can perform better.”

In New York for the IPO, Chao ran into questions about China’s tough censorship of the internet. He argued that Weibo has been a key player in giving all Chinese a voice, and that China has progressed a lot.

“There is no question that each country will have their own rules in terms of regulating the content industry or content behaviour by users on the internet... China is no exception,” he argued.

“We are very experienced in terms of complying with the laws and regulations in this area. So I don’t think it’s a big concern for us.”