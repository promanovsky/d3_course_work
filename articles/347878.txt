NEW YORK -- Powerful painkillers have been driving the nation's rising rate of overdose deaths, and now the government is singling out the states where doctors write the most prescriptions.

A second report released Tuesday spotlights how a crackdown in Florida led to hundreds fewer overdose deaths from prescription painkillers in just a few years.

The reports are part of a campaign by the Centers for Disease Control and Prevention to combat deaths from prescription opioids like Vicodin and OxyContin. In 2011, drug overdose deaths reached 41,000 and 41 per cent of them involved prescription painkillers.

The state account comes from a database of U.S. retail pharmacies that fill the bulk of prescriptions.

THE NATIONAL PICTURE:

Southern states had the most prescriptions in 2012. Alabama was in the lead with 143 prescriptions per 100 people, followed closely by Tennessee. The other leading states were -- in ranking order -- West Virginia, Kentucky, Oklahoma, Mississippi and Louisiana. Doctors in the South have also topped prescription rate lists for other medications, including antibiotics and stimulants for children. Rates of chronic disease tend to be higher in the South, but past research has found that doesn't explain away the difference. Hawaii had the least prescriptions, at 52 per 100 people.

THE IMPACT

"Prescriptions go up, deaths go up. Prescriptions go down, deaths go down," said CDC Director Dr. Tom Frieden. While that seems logical, evidence of that link is incomplete. The CDC reports state death rates but combines all kinds of drug overdoses, including heroin and cocaine. And those rankings differ from the prescription list. But officials cite studies that show higher overdose rates when there are more prescriptions of painkillers and larger doses prescribed.

THE FIX

Officials say there's a need for more prescription drug monitoring programs at the state level, and more laws shutting down "pill mills" -- doctor offices and clinics that over-prescribe addictive medicines. The CDC points to a success story in Florida, where pill mills became a large problem in the last decade. In 2010-2011, the state enacted tougher pain clinic regulations and police did a series of raids. By 2012, prescriptions for oxycodone alone fell 24 per cent and the death rate for prescription drug overdoses dropped 23 per cent. "When you take serious action, you get encouraging results," Frieden said.