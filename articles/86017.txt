PITTSBURGH — John Pinette, the chubby stand-up comedian who portrayed a hapless carjacking victim in the final episode of “Seinfeld,” has died. He was 50.

Pinette died of natural causes Saturday at a hotel in Pittsburgh, the Allegheny County Medical Examiner’s Office said Sunday evening. Pinette’s agent confirmed his death.

The portly Pinette was a self-deprecating presence on stage, frequently discussing his weight on stand-up specials “Show Me the Buffett,” “I’m Starvin’!” and “Still Hungry.”

Pinette had been working on another stand-up project when he died, said his agent, Nick Nuciforo.

“He should be celebrated for the amazing comedian he was,” Nuciforo said.

The Boston native appeared in movies including “The Punisher” and had a trio of stand-up shows released on DVD, but was perhaps best known as the portly carjacking victim whose plight lands the “Seinfeld” stars before a judge for failing to help under a “good Samaritan” law. Pinette also appeared in the television series “Parker Lewis Can’t Lose.”

Pinette also appeared in a national tour of “Hairspray” as Edna Turnblad, the mother of the play’s heroine.

The medical examiner’s office said no autopsy was performed and Pinette’s own physician signed off on the cause of death.

Pinette had been preparing for a stand-up tour of the US and Canada, Nuciforo said.