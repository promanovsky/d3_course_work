Gary Oldman appeared on last night's Jimmy Kimmel Live and addressed his recent controversial comments he made in a new Playboy interview. Said the actor, "I am a public figure, I should be an example and inspiration, and I am an a-hole. I am 56. I should know better. I extend my apology and my love and best wishes to my fan base."

Check out a clip of last night's appearance below!

About Jimmy Kimmel Live:

Jimmy Kimmel serves as host and executive producer of Emmy nominated "Jimmy Kimmel Live," ABC's late-night talk show.

"Jimmy Kimmel Live" is well known for its huge viral video successes with 1.5 billion views on Youtube alone. Some of Kimmel's most popular comedy bits include - Mean Tweets, Lie Witness News, Jimmy's Twerk Fail Prank, Unnecessary Censorship, Youtube Challenge, The Baby Bachelor, Movie: The Movie, Handsome Men's Club, Jimmy Kimmel Lie Detective and music videos like "I (Wanna) Channing All Over Your Tatum" and a Blurred Lines parody with Robin Thicke, Pharrell, Jimmy and his security guard Guillermo.

Now in its eleventh season, Kimmel's guests have included: Johnny Depp, Meryl Streep, Tom Cruise, Halle Berry, Harrison Ford, Jennifer Aniston, Will Ferrell, Katy Perry, Tom Hanks, Scarlett Johansson, Channing Tatum, George Clooney, Larry David, Charlize Theron, Mark Wahlberg, Kobe Bryant, Steve Carell, Hugh Jackman, Kristen Wiig, Jeff Bridges, Jennifer Garner, Ryan Gosling, Bryan Cranston, Jamie Foxx, Amy Poehler, Ben Affleck, Robert Downey Jr., Jake Gyllenhaal, Oprah, and unfortunately Matt Damon.

Visit the Jimmy Kimmel Live WEBSITE: http://bit.ly/JKLWebsite

Like Jimmy Kimmel Live on FACEBOOK:http://bit.ly/JKLFacebook

Follow Jimmy Kimmel Live on TWITTER: http://bit.ly/JKLTwitter

Follow Jimmy Kimmel Live on INSTAGRAM:http://bit.ly/JKLInstagram