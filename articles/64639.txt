DETROIT -- General Motors announced two more recalls, bringing to 4.8 million the number of cars, trucks and SUVs the automaker has called back for repairs in the past month.

The string of recalls, topped by an ignition switch problem in compact cars now linked to 13 crash deaths, has embarrassed the company and sidetracked its new CEO, who started work just over two months ago. GM has admitted knowing about the switch problem a decade ago, yet it didn't recall any cars until February. The recall delay has brought two congressional investigations and probes by the Justice Department and the National Highway Traffic Safety Administration.

Late on Friday night, GM announced it would recall 490,000 late-model pickup trucks and SUVs because transmission oil cooling lines weren't secured properly in their fittings. Transmission oil can leak from a fitting and hit hot surfaces, causing fires, the company said in a statement. GM said it knows of three fires and no injuries. In Canada GM is recalling 51,115 vehicles.

The recall affects Chevrolet Silverado and GMC Sierra 1500 pickup trucks from the 2014 model year, as well as 2015 Chevrolet Suburban and Tahoe SUVs and the GMC Yukon and Yukon XL SUVs. All have six-speed automatic transmissions.

The Silverado is GM's top-selling vehicle and an important profit centre for the company. The GMC Sierra also is among GM's top sellers.

Dealers will inspect the transmission oil cooling line fittings and make sure they're securely seated, at no cost to owners.

Also on Friday night, GM announced the recall of 172,000 Chevrolet Cruze compact cars because the right front axle shaft can fracture and separate while being driven. In Canada, about 25,800 cars and about 950 service parts are also involved, GM said Saturday.

The recall affects cars from the 2013 and 2014 model years equipped with 1.4-litre turbocharged four-cylinder gasoline engines.

If a shaft fractures, the wheels would lose power without warning and the cars would coast to a stop.

GM says it has warranty reports of several dozen shaft fractures. It is not aware of any crashes or injuries.

Dealers will replace the shafts free of charge.

The recall allows dealers to resume selling affected Cruzes. GM issued a stop sale order on the cars Thursday night.

The recall also covers about 2,500 replacement shafts used to fix manual transmission Cruzes that were recalled last September.

In all, GM has recalled 4.8 million vehicles since last month, two million more than the company sold last year in the U.S. In addition to the recalls announced Friday night, they include:

2.6 million small cars because their ignition switches can move from the "run" to the "accessory" or "off" position, which causes the car to stall and disables the air bags and power steering. The recall includes the Chevrolet Cobalt, Chevrolet HHR, Pontiac G5, Pontiac Solstice, Saturn Ion and Saturn Sky from the 2003-2011 model years.

1.18 million SUVs because their side air bags, front centre air bags and seat belt pretensioners might not deploy if drivers ignore an air bag warning light on their dashboard. The recall includes the Buick Enclave and GMC Acadia (2008-2013); Chevrolet Traverse (2009-2013); and Saturn Outlook (2008-2010)

303,000 Chevrolet Express and GMC Savana vans (2009-2014) because the material on the instrument panel might not adequately protect unbelted passengers' heads in a crash.

63,900 Cadillac XTS sedans (2013-2014) because a plug in the brake assembly can get dislodged and short, increasing the risk of an engine compartment fire.

GM also said Friday that it has found another death attributed to the ignition switch recalls, bringing the company's count to 13. The additional fatality happened in 2013 and involved a 2007 Chevrolet Cobalt in Quebec, Canada. The company didn't give further details of the crash.

GM says dealers will start getting replacement ignition switches on April 7, but it will take until October to repair all of the vehicles. CEO Mary Barra says they are safe as long as drivers remove everything from their key rings. Weight on the rings can wear down the inside of the switches, causing them to slip out of the run position.

Trial lawyers, however say the cars should be parked because the ignitions can slip out of the run position on bumpy roads.