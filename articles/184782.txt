Twitter's stock was trading at its lowest point ever on Tuesday as the lock-up period expired that required insiders to hold on to their shares after the IPO.

Twitter's shares were trading hands at $34.24 on Tuesday, down more than 11 per cent on the day, and the lowest they've been since their initial public offering in November.

The IPO was priced at $26 a share but quickly zoomed to the mid-40s on its first day, and cracked the $70 a share level in the weeks that followed.

But earnings numbers since the company went public have been underwhelming, as the company is having trouble monetizing its growing customer base.

Lock-up periods prevent company insiders from selling stock following an initial public offering. CEO Dick Costolo and co-founders Jack Dorsey and Evan Williams have said that they have no plans to sell their stock when the lock-up expired, 180 days after Twitter's initial public offering.

About 480 million shares sold to insiders were affected by the lock-up and could not be sold until today.

More than 65 million shares have already been traded by midday on Tuesday. That's more than five times the usual daily volume.

Twitter went public on Nov. 7. The stock later soared as high as $74.73.

San Francisco-based Twitter's latest earnings report surpassed expectations, but worries about user growth and engagement weighed on its stock.