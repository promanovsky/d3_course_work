On odd occasions it seems like the good things in life arrive quietly and unexplained as if they floated in through a window on a passing breeze. At least, that's how the announcement of 250 new emoji has been greeted by the internet - but who chooses these ubiquitous pictograms anyway? And why did they feel the need to include a fax machine?

You can find a comprehensive list of the new additions over at Emojipedia but it seems that the majority are simple nouns (there’s Bed, Book and, um, Passenger Ship for example) or abstract symbols (are you excited for North West Pointing Vine Leaf? If so you’re going to flip when you see Heavy North West Pointing Vine Leaf).

However, don’t rush to your smartphone just yet to try out Reversed Hand With Middle Finger Extended, as the slightly bureaucratic world of emoji-implementation means it may be a while before Apple or Google update their keyboards.

Emoji as rendered by Unicode.

Essentially what emoji you can use depends on which are chosen by Unicode: an industry standards body that makes sure text is rendered and read by computer software in a consistent manner. Like most standard bodies they're low-key but a big deal: every time you type something on a laptop, tablet or smartphone, your device will be using Unicode's 'dictionary' to match what you and other people see.

Emoji themselves first gained popularity in Japan where they were introduced by mobile networks as a fun feature for users (the word ‘emoji’ comes from the Japanese for ‘picture’, ‘writing’ and ‘character’) and were introduced by Unicode after pressure from developers into the October 2010 edition of their text-rendering rulebook.

However, although Unicode draws up the lists of new emoji, it’s up to individual companies to render them for users (this table compares how some major developers interpret different emoji). In terms of deciding which emoji make the cut, Unicode itself says only that this is a “long, formal process” where characters under consideration “must be in widespread use, as textual elements”.

This essentially that although the latest Unicode 7.0 standard includes a place set aside for a “Man In Business Suit Levitating” emoji, it’s up to Google and Apple to decide how far that man is levitating above the ground when you use it on a smartphone – or what race he is.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

A comparison of how some basic emojis are rendered by Apple, Android, Symbola and Twitter. Image credit: Tim Whitlock.

This latter point is particular important given that the diversity – or lack of – can be particularly noticeable in a communication tool developed firstly in a specific culture (Unicode’s first set of 722 emoji include a Japanese bento box but no Western Easter Egg) and then by a specific slice of society (the overwhelmingly white, male developers of Silicon Valley).

In March, a petition was created urging Apple to “add more diversity to the Emoji keyboard” arguing that it was ridiculous that a keyboard should have every phase of the Moon but only two non-white faces: “a guy who looks vaguely Asian and another in a turban.”

The iPhone maker duly apologised and promised to do more for its customers (whether or not the latest Unicode update will be used to create a more diverse array of emojis is anyone's guess), suggesting that despite Unicode's oversight, it's up to the public to make sure these ubiquitous symbols best reflect the society and culture they want to live in.

However, despite their cultural heft and zeitgeist-feelings surrounding emoji (everything from Beyoncé's Drunk in Love to Herman Melville's Moby Dick has been 'translated' using the pictograms) it's worth remembering that they're only part of the web's ever-growing tool-kit for intenrational communication.