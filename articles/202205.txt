Darn you, In Touch Weekly. You're forcing us to take the side of Kim Kardashian.

Earlier this week, the tabloid ran a cover story that claimed Kim and Kanye West were planning the "Wedding From Hell" because Kris Jenner keeps getting drunk and French Montana may show up as a guest.

Typical tabloid fodder, right?

Except the magazine also published a "photo recreation" of a woman trying to fit into a wedding dress and ran it alongside a picture of Kim crying. The obvious implication? That is Kim failing to fit into her gown! OH NO!

In response to what she describes as a “super lame” gimmick, Kardashian has taken to her blog and brought attention to the pathetic headline grab.

"It’s pretty unbelievable what people will do to sell a magazine… this picture says in really small print that it’s a “Photo Recreation.” It’s not a real photo of me, but they’re implying that it is," Kim writes.

Is there an irony in any member of the Kardashian family calling someone else out for desperately trying to garner some attention? Of course.

But still... this is super lame of In Touch Weekly.

As lame as Kim Kardashian first realizing racism is an issue now and trying to garner points for blogging about it? No. But pretty darn lame nonetheless.