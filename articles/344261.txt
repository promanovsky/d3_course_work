JP Morgan Chase CEO Jamie Dimon, a feisty leader who steered the banking company through the perils of the Great Recession, is battling throat cancer.

The condition is considered to be curable, according to a memo to JP Morgan Chase & Co employees and shareholders that Dimon released late Tuesday.

Dimon, 58, said he plans to remain on the job and be actively involved in key decisions while undergoing radiation and chemotherapy treatment. The eight-week treatment will begin soon at Memorial Sloan Kettering hospital in New York.

The cancer remains confined to Dimon's throat and adjacent lymph nodes on the right side of his neck, according to the memo.

"I feel very good now and will let all of you know if my health situation changes," Dimon said.

Despite the words of reassurance, Dimon's illness could raise leadership concerns at one of the world's biggest banks.

Those worries could be magnified by a recent succession of departures among high-ranking JP Morgan Chase executives once considered to be Dimon's potential successor. The list of defections have included former co-chief operating officer Frank Bisignano and former investment banking head Michael Cavanagh, who had been known as Dimon's "Mr Fix-it."

JPMorgan's stock fell 37 cents to $57.20 in Tuesday's extended trading.

Dimon, JPMorgan's CEO for the past eight-and-half years, won praise for his savvy in the aftermath of the mortgage-market meltdown that nearly toppled the banking industry before miring the U.S. in its deepest recession since World War II. His reputation, though, was tarnished in 2012 by the revelation of a stunning $6 billion loss triggered by risky bets made on complex debt securities by a JPMorgan trader in London.

The setback, known as JPMorgan's "London Whale," contributed to an unsuccessful shareholder campaign last year to separate the jobs of chairman and CEO — duties that Dimon still holds.

JPMorgan also reached a settlement with the Justice Department last year that required the bank to pay $13bn to cover losses from the sale of toxic mortgages that later collapsed in value.

Another legal settlement negotiated last year required JPMorgan to dole out $4.5bn to investors in bundles of shoddy home loans. JPMorgan said most of those troublesome mortgages were inherited from investment bank Bear Stearns and savings and loan Washington Mutual, companies that Dimon negotiated to buy during the financial crisis of 2008.

Even amid public outrage over bankers' role in the mortgage meltdown, Dimon remained one of the industry's staunchest advocates. He supported big paydays for bankers and derided some of the U.S. government's proposed reforms designed to minimize the chances of another financial crisis.

JPMorgan's stock is up 45% since Dimon took over the bank's helm on 31 December 2005. The S&P 500, meanwhile, has risen 58% in that period.