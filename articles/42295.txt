HTC One M8 priced and released: same day in-store

In a relatively unprecedented move for a top-tier smartphone in recent times, HTC has made the HTC One M8 available the same day it’s been revealed to the public. As HTC makes the HTC One M8 (the successor to the HTC One (codename M7) from 2013 their official newest smartphone, they also release it to pre-orders on some carriers and in-store availability in others. This availability includes both USA and UK carrier locations, as well.

Inside the United States, the HTC One M8 will be appearing in-store at Verizon by 1PM. This means it’ll have been shipped (or secretly held behind the counter) within hours of being officially announced by HTC the same day at Verizon stores nationwide.*

*Of course this doesn’t mean that every single Verizon in the United States will have the HTC One M8 this same day, but we are to understand that one whole heck of a lot of them will. You’d be best to call ahead – check to see if your local Verizon has the device in stock before trekking out, as it always is with these sorts of things.

As for AT&T and Sprint – both carriers will have the device for sale online starting at 1PM as well. In-store availability will begin for these two carriers starting on April 11th, 2014. T-Mobile USA will begin putting the HTC One M8 on sale starting April 11th.

Each carrier will have its own unique pricing scale, but you’ll find your chosen brand to roll with either $199 or $249 USD, dependent on how they’re marketing the machine. Those prices are on-contract, connected to a 2-year contract right out of the box.

In the UK, EE will have the HTC One M8 starting on the 26th of March online and on the 27th in-store. EE will be pushing this device with their £37.99/pm EE Extra plan (£29.99 up front cost), suggesting also that if customers pick the device up with this plan before the 10th of April, they’ll receive a special launch offer with twice the amount of data of EE’s standard 4G plans – that means 4GB instead of 2.

Also in the UK, the HTC One M8 is coming to Vodafone stores. The first 3000 customers to purchase the HTC One M8 with Vodafone will be given an HTC Boombass through the Boombass portal. The whole lot will be “free from £42 on Red 3G plans” – this includes unlimited calls and text as well as “loads of data” – limited, but plentiful.

Meanwhile the full off-contract MSRP is $649 USD. You’ll find the Developer Edition and the Google Play Edition of the HTC One M8 to be priced similarly, of course. Have a peek at the timeline below for more information and hands-on action with the HTC One M8 than you could possibly desire.