Oh it's on now.

Buffy the Kimye Slayer, Sarah Michelle Gellar, threw SHADE at the Kim Kardashian-Kanye West Vogue cover shortly after it was released today.

"Well……I guess I'm canceling my Vogue subscription," Gellar tweeted.

"Who is with me???"

While many fans agreed with Gellar, the actress' comment also elicited a defensive quip from Kardashian fans such as her best friend, Jonathan Cheban.

"Do u matter?" he said, re-tweeting Gellar.

Ooooooh.

Cheban, along with Kim Kardashian's closest family and friends, celebrated the inclusion of Kimye Vogue photos in the "Fashion Bible."

Vogue editor Anna Wintour also defended her controversial choice to place the reality TV star and rapper on the cover in her April Editor's Letter.

"As for the cover, my opinion is that it is both charming and touching, and it was, I should add, entirely our idea to do it," she said of the Kimye issue.

"You may have read that Kanye begged me to put his fiancée on Vogue’s cover. He did nothing of the sort," Wintour, also the head of Conde Nast, wrote.

Kim Kardashian, "through strength of character, has created a place for herself in the glare of the world's spotlight, and it takes real guts to do that."

Kanye, meanwhile, "is an amazing performer and cultural provocateur."

Gellar, for her part, has never covered Vogue, although she's covered plenty of other magazines, and her selfie with Freddie Prinze, Jr. did win the Internet, so there's that.