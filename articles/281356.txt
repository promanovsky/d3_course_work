Wed, 11 Jun 2014 12:49:33 PDT

The four-year wait is over. With Thursday’s opening ceremony of 2014’s FIFA World Cup in São Paulo almost here, soccer fans across the globe are filling up their coolers, stocking up on chips, and planning major couch-potato sessions to watch their favorite teams compete. While the Brazilian organizers have had their fair share of hurdles, the competition’s launch is set to make history—and we’re not talking about J.Lo’s impending performance.

Think Iron Man meets Pelé minus the rockets and rainbow kicks. A paraplegic individual is going to suit up in a brain-controlled robotic exoskeleton suit and make the ceremonial first kick of the games.

The groundbreaking suit is the invention of the Walk Again Project, which is led by renowned scientist Miguel Nicolelis. The Brazilian government financially supports the project, a nonprofit collaboration among world-renowned educational and neuroscience institutes. Nicolelis and his team have spent nearly a decade of research and labor to make the suit work.

How does the exoskeleton work? Once the person, whose identity is still unknown, is in the exoskeleton, a helmet of electrodes will be placed on his or her head to pick up brain signals. The brainwaves will transmit electrical signals that will trigger the legs of the suit. Once steps are taken, the suit’s sensors can tell that the exoskeleton has touched the ground. They then send a signal and vibration back to the limb.

This allows the suit’s user to associate and “feel” movement, simulating control of the suit’s legs—and enabling the person to move forward and kick the ball in front of tens of thousands of spectators and millions of television viewers.

According to the team’s social media posts, the project’s collaborators are in good sprits. They have posted exciting videos of promising steps taken with the robotic suit. However, it’s still up in the air how well the exoskeleton will allow the individual to kick the ball.

Story continues

“A significant challenge is to get such systems to work reliably in everyday use, with more electronic ‘noise’ to interfere with the tiny signals from the user’s brain and muscle activity,” said University of California, Davis, professor Sanjay Joshi, a key contributor to the project.

The suit’s wearer has spent months of training in a virtual reality simulator—the person has basically had to learn how to walk all over again. It’s a tall order for anybody to brain-control a machine with millions of eyes watching. Despite the stress, the project’s team believes the opening ceremony is a great platform to showcase its main mission: to develop an exoskeleton that can handle everyday situations (like kicking a ball) and enhance the lives of disabled people around the world.

Nicolelis and scientists have also been working furiously to make this happen in order to use the competition’s world stage as a way to inspire our next generation to explore science, engineering, and medical fields and take an active role in shaping the future of life-enhancing scientific innovations. “The main message is that science and technology can be agents of social transformation in the whole world,” Nicolelis told the BBC, “that they can be used to alleviate the suffering and the limitations of millions of people.”

Related stories on TakePart:





• 5 Apps to Download Before Your Next Trip to the Grocery Store

• With This Pedal-Powered Generator, Biking Gives Light to Kids Living Without Electricity

• Brazil Sports Fans Turn Stadium Pink After Rivals Chant "Faggot"

Original article from TakePart