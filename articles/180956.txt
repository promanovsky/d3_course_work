American International Group on Monday delivered quarterly revenue that missed analysts' expectations as the number of net premiums earned from its property casualty unit fell during the quarter.

After the earnings announcement, the company's shares fell more than 2 percent in after-hours trading. What is AIG stock doing now? (Click here to get the latest quotes.)

The insurer posted first-quarter earnings excluding items of $1.21 per share, compared to $1.34 a share in the year-earlier period.

Adam Jeffery | CNBC

Revenue decreased to $8.23 billion from $8.56 billion a year ago. Analysts had expected American International Group to report adjusted earnings of $1.07 a share on $9.36 billion in revenue, according to a consensus estimate from Thomson Reuters. AIG CEO Robert Benmosche told CNBC's "Closing Bell" foreign exchange plays a part in the firm's revenue slip.

He urged investors to "keep in mind our second largest market outside the United States is in fact Japan and so we have a tremendous amount of premiums and fees coming out of Japan," he said. "With the pressure on the yen and looking at the exchange for the dollar, our premiums are actually growing on the property casualty side, but you don't see it because of the effects of the dollar."