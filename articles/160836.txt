U.S. President Barack Obama fielded a soccer kick from a Honda Asimo robot during a tour of a Japanese science museum Thursday.

The robot also demonstrated its ability to run and jump.

Obama watched a video message from the crew on board the International Space Station and met with Japanese students.

In a speech after the Asimo presentation, the U.S. president joked that he found the robot "scary" and "too lifelike."

Watch video of the presentation in the player above.