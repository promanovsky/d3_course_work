Mortgage rates for 30-year U.S. loans fell to a six-month low, reducing borrowing costs for homebuyers as the property market cools amid a slow economic recovery.

The average rate for a 30-year fixed mortgage was 4.21 percent this week, down from 4.29 percent, according to a statement from Freddie Mac. The average 15-year rate dropped to 3.32 percent from 3.38 percent, the McLean, Virginia-based mortgage-finance company said.

Federal Reserve Chair Janet Yellen said that the economy still needs stimulus five years after the recession ended as housing demand slows. Rising property prices, along with mortgage rates that have climbed from near-record lows a year ago, are cutting into buyer affordability. U.S. home prices increased 11.1 percent in March from a year earlier, the 25th consecutive gain, CoreLogic Inc. said this week.

“Readings on housing activity, a sector that has been recovering since 2011, have remained disappointing so far this year and will bear watching,” Yellen said in testimony to the Joint Economic Committee of Congress. “The recent flattening out in housing activity could prove more protracted than currently expected rather than resuming its earlier pace of recovery.”

Completed purchases of previously owned U.S. homes fell for a third straight month in March to the slowest pace since July 2012, according to the National Association of Realtors. Sales of new houses dropped to an eight-month low, Commerce Department data show.

Yellen repeatedly declined in her testimony to specify when the benchmark interest rate might rise. The central bank’s policy of low rates and a bond-buying program pushed the average rate for a 30-year home loan to a near-record 3.35 percent last May. It has climbed since then as the Fed scales back its stimulus plan.

Employment remains well short of the Fed’s goals, according to Yellen. While the jobless rate dropped in April to 6.3 percent, the lowest since September 2008, the share of Americans who have been unemployed for more than six months as well as part-time workers who would prefer full-time positions “are at historically high levels,” she said.