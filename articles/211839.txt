A new study shows that cervical cancer rates in the U.S. may be higher than once believed. (Credit: CBS 2) A new study shows that cervical cancer rates in the U.S. may be higher than once believed. (Credit: CBS 2)

NEW YORK(CBSNewYork) — Invasive cervical cancer strikes about 12,000 women a year in the United States. That doesn’t seem like a large number when compared to more than a quarter million breast cancer cases, but a new study has shown that cervical cancer may be more common than once thought.

About 4,000 women die of cervical cancer every year. As CBS 2’s Dr. Max Gomez explained, that number is so low because pap smears have the ability to detect changes in the cervix before they become cancerous.

Cervical cancer rates in the U.S. were thought to be just under 12 cases for every 100,000 women, but new analysis presented in the journal ‘Cancer’ shows that the number is actually closer to 20 cases for every 100,000 women, an increase of nearly 60 percent.

The increase is not due to a spike in cancer cases, it’s because the rate included women who have had a hysterectomy.

“We’ve now removed hysterectomy women who’ve had their cervix removed and they’re not at risk. So although the numbers have remained the same the rates have increased because it’s spread out over a different population of women,” Dr. Tara Shirazian, Mt. Sinai Hospital, explained.

The increase was even greater in older women between the ages of 65 and 69. That number jumped 85 percent because more of them had hysterectomies.

Even more concerning is that current screening guidelines say that women over the age of 65 are told that they don’t need pap smears if their tests have been consistently negative.

“This means my insurance will probably no longer pay for this,” Carol Beach, 68, said.

Still, women in that age group had the second highest incidence of cervical cancer in the entire population.

“It just means we have to keep an eye on women over their entire life cycle, and make sure women who are at risk are still being screened,” Dr. Shirazian said.

American women in the older age group had an even greater jump in the corrected cervical cancer rates because they have hysterectomies more often with age due to fibroids.

All women should talk with their doctors to determine when and how often they need a pap test. Cervical cancer is caused by HPV, a sexually transmitted virus, but it can take decades for the cancer to actually develop.

You May Also Be Interested In These Stories

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]