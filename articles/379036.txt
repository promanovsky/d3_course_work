Healthcare investor David Miller believes Roche (RHHBY) will likely acquire Exelixis (EXEL) - Get Report in order to capture the full economics of the cancer drug cobimetinib. The two companies announced positive results from a phase III study of "cobi" in melanoma this morning.

Miller, a partner at Seattle's Alpine Bioventures hedge fund and long Exelixis, took to Twitter to make his point.

I spoke with him, asking him to elaborate on why he views Exelixis as a Roche takeover target. Miller believes the combination of cobi and Roche's already approved Zelboraf will become the standard of care, first-line therapy for BRAF-mutated melanoma patients. Zelboraf, on its own, is already a $400 million drug worldwide for Roche. The combination with cobi will be priced between $16,000 and $20,000 per month, or a premium to the $16,000 per month GlaxoSmithKline (GSK) - Get Report charges for a similar but inferior combination therapy, Tafinlar and Mekinist, Miller believes. Using the high end of the price range, Miller forecasts combi/Zelboraf in melanoma represents a $600 million to $1 billion revenue opportunity for Roche in the U.S. alone.

"Roche has been touting the importance of the [cobi/Zelboraf] combination trial for well over a year now. They see the importance of this therapy even if bank analysts don't, and 50% profit share in the U.S. and a low-teens royalty overseas is a lot for Roche to be giving away to Exelixis," says Miller. Historically, Roche has not acquired its cancer drug development partners, but Miller believes Exelixis could prove to be the exception given the large revenue opportunity for cobi.

A lot of investors have been focused on Exelixis' development of cabozantinib in prostate cancer -- and not necessarily in a good way. Results from two phase III prostate cancer studies of "cabo" are expected to be announced this year, and Wall Street (generally speaking) isn't very confident about positive data.

Miller acknowledges the risk overhang from the cabo prostate cancer studies but believes the drug is real (it's already approved for a form of thyroid cancer) and has a better shot at success in two other, ongoing phase III studies enrolling patients with kidney cancer and liver cancer. Roche may wait to make a bid for Exelixis until after the cabo prostate cancer studies read out results, but before interim results from the kidney cancer study are announced next year.

How much might Roche bid for Exelixis?

Exelixis' current market cap stands at approximately $680 million.

Adam Feuerstein writes regularly for TheStreet. In keeping with company editorial policy, he doesn't own or short individual stocks, although he owns stock in TheStreet. He also doesn't invest in hedge funds or other private investment partnerships. Feuerstein appreciates your feedback;

click here

to send him an email.