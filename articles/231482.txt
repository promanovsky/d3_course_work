UPDATE

TK Tech News reached out to IBTimes and to let us know that both parts of the Samsung Galaxy S5 Active hands on review are now live on YouTube. Check them out here and here.

Original story

The Samsung Galaxy S5 Active may become official as early as June, but a leaked video has surfaced giving the public an early look at the coming device.

TK Tech News, a source known for obtaining unreleased models of smartphones, got hold of a Galaxy S5 Active prototype model powered by AT&T (NYSE:T); this is one of the first appearances of the Galaxy S5 Active. The video shows a device that appears similar to the Samsung Galaxy S5 and the Galaxy S4 Active. It has such Galaxy S5 specific features as a heart rate sensor located below the camera model and sealing on the inner back panel, indicating that it's water-resistant. It also has three capacitive buttons on its front, like the Galaxy S4 Active.

The smoking gun that this device may actually be the Galaxy S5 Active is that once it’s turned on the boot display reads “Samsung Galaxy S5 Active” and “powered by Android.” The Android branding is notably a new feature as of the release of Android 4.4 KitKat.

From the settings menu we learn that the model number for this device is SM-G870A, which has been associated with the Galaxy S5 Active for several months. The device runs the Android 4.4.2 version of KitKat.

TK projects that the Super AMOLED display on this Galaxy S5 Active model may be greater than a 1920x1080 resolution, but this is something he is not been able to confirm. He followed the video with a two-part in depth review of the Samsung Galaxy S5 Active, but those videos have since been made private.

Several prior reports indicate that the Samsung Galaxy S5 Active will be released during 2014. Most recently, the GFXBench benchmark database tested a device under the model SM-G870, and determined that the device may feature 5.1-inch display, a Qualcomm Snapdragon 800 processor, 2GB of RAM and 16GB of internal storage, a 16-megapixel rear camera and 2-megapixel front-facing camera and the Android 4.4.2 operating system. Prior to that, the Samsung SM-G870A model was filed with the FCC and was determined compatible with the AT&T LTE bands (2, 4 and 17), NFC and dual-band Wi-Fi.

Samsung Electronics Co. (NASDAQ:SSNLF) recently announced its Samsung Galaxy Premiere press event will take place on June 12 in New York. It is likely the Samsung Galaxy S5 Active may be among the devices unveiled at that event. The Galaxy S4 Active was unveiled alongside the Galaxy S4 Mini and Galaxy S4 Zoom at Samsung Premiere last year.

Follow me on Twitter.