Japanese auto major Toyota Motor Corp. (TM) reported Thursday a surge in fiscal 2014 profit, benefited by increased vehicle sales mainly in Japan and North America and cost reduction activities. Looking ahead, for fiscal 2015, the company expects lower net income and flat revenues with nearly flat vehicle sales.

For the year ended March 31, net income attributable to the company climbed 89.5 percent to 1.82 trillion yen (around $17.87 billion) from 962.1 billion yen last year. Earnings per share was 574.92 yen, higher than 303.78 yen in the prior year.

Operating income increased 73.5 percent year-over-year to 2.29 trillion yen, helped by currency fluctuations, cost reduction efforts and marketing activities, the company noted.

On a consolidated basis, net revenues for the year totaled 25.69 trillion yen, an increase of 16.4 percent from the previous year's 22.06 trillion yen.

Net revenues for the automotive operations increased 16.5 percent to 23.78 trillion yen, and for the financial services operations, it climbed 21.4 percent. Net revenues in Japan grew 11.5 percent, in North America it went up 29.2 percent, and in Europe revenues increased 30.8 percent, and in Asia the rise was 11.2 percent.

In the year, consolidated vehicle sales increased 2.8 percent to 9.12 million units. In Japan, vehicle sales grew 3.8 percent to 2.37 million units mainly on the active introduction of new products. Toyota and Lexus brands' market share excluding mini-vehicles was 46.7 percent.

Overseas vehicle sales grew 2.4 percent to 6.75 million units because of sales expansion in North America, Europe and other regions. In Asia, vehicle sales, however, declined by 75,223 units.

Looking ahead for fiscal 2015, Toyota projects net income of 1.78 trillion yen or 561.56 yen per basic share, representing an year-over-year decline of 2.4 percent. Operating income is expected to be 2.3 trillion yen, up 0.3 percent from the prior year, while consolidated net revenue is seen at 25.7 trillion yen, unchanged from last year.

The company also estimates that consolidated vehicles sales for the year 2015 would be 9.1 million units.

Separately, Toyota said a year-end dividend of 100 yen per share for shareholders of record March 31, would be proposed at the general shareholders meeting to be held on June 17. This will make annual dividend at 165 yen per share.

In Japan, Toyota shares gained 17 yen or 0.31 percent on today's trading and settled at 5,528 yen.

For comments and feedback contact: editorial@rttnews.com

Business News