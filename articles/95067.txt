America's obesity problem appears to be serious as adults aren't the only ones gaining excess weight. Obese and severely obese children are also on the rise in the United States, which could mean that if nothing is done about the situation, America may continue to face obesity-related problems such as diabetes, cancer and cardiovascular illnesses.

Although data released by researchers from the Centers for Disease Control and Prevention (CDC) in February suggest there's a significant reduction in the prevalence of obesity among preschoolers, a new study published in JAMA Pediatrics on April 7 shows that childhood obesity in the U.S. has not dropped but has instead risen over the past 14 years.

In the study, the researchers sought to examine the prevalence and trend in childhood obesity in the U.S. from 1999-2012 by analyzing data from the National Health and Nutrition Examination Survey (NHANES) of more than 26,000 children between 2 and 19-years of age. They found an exponential increase in the obesity rates of children age 2 to 19 years old over the past 14 years.

The researchers have also observed a rise in the number of children who are severely obese or those whose body mass index (BMI) is 120 percent to 140 percent higher than other children their age. The researchers noted that in 1999-2000, less than one percent of the children had BMI 140 percent higher than their peers, but this doubled to 2.1 percent by 2011-2012.

"We were interested in the most severe forms of obesity, which are class 2 and 3, and this is what we think about with morbid obesity in adults. We see that over the last 14 years, there's definitely been an increase in both of these levels of obesity. We see a significant increase in severe obesity at a variety of different ages," said study researcher Asheley Cockrell Skinner from the Department of Pediatrics in the School of Medicine at the University of North Carolina at Chapel Hill.

The findings of the study paint a grim outlook of the future as studies suggest that children and adolescents who are obese are likely to remain that way until adulthood. Worse, they are also more likely to develop a number of health problems later in life, including high blood pressure, high cholesterol and type 2 diabetes.

TAG Obesity, CDC

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.