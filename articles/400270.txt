The current Ebola virus outbreak spreading across West Africa is the biggest that has ever been recorded, having killed more than 670 people at the time of writing.

It has led to a major international response focussed on the main affected countries: Sierra Leone, Guinea and Liberia.

But the aid organisation Médecins Sans Frontières (MSF) has described disease as “out of control”, and last week fears grew of a global outbreak after a case was confirmed in Lagos, Nigeria.

The disease first emerged in 1976, and has since then killed an estimated 2,000 people. Previous outbreaks have had mortality rates as high as 90 per cent, though the chance of dying from the most recent strand appears to be at around 60 per cent.

Doctors around the world, including now in the UK, have been urged to look out for patients exhibiting the early symptoms of the virus, which include fever, headaches, joint and muscle pain and lack of appetite.

Later symptoms include vomiting, diarrhoea, stomach pain, a rash and both internal and external bleeding – often from the eyes, nose or mouth.

There is no cure for the disease, nor a vaccine to prevent infection. The only treatment available is to keep the patient hydrated – as would be the case with any fever.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Professor David Heymann, head of the Centre on Global Health Security at Chatham House, London and the chairman of Public Health England, was working as an epidemics expert in sub-Saharan Africa during the first ever Ebola outbreaks in the 1970s and '80s.

He told The Independent that Ebola is a “terrible disease”, and condemned the lack of “international cooperation” that has allowed it to escape isolated hotspots in remote parts of West Africa.

But he said that the fact that the disease is not a respiratory virus – and that it either kills or is recovered from relatively quickly – means it does not have the same potential to become endemic in populations as something like Sars or Mers.

This photo provided by the CDC shows an ebola Virus (AP Photo/CDC)

Ebola is transmitted through direct contact with bodily fluids, making hospital transmission the likely method by which it would be passed on to large numbers of people.

Because the UK has such well-established systems for infection control, getting people to hospital quickly and tracing anyone who a suspected patient has had contact with, the chances of a widespread outbreak even if Ebola arrives in Britain are described as “very low”.

But these points are not the case in much of West Africa, where containing the virus has proved difficult – even among doctors and nurses.

Tarik Jasarevic, a spokesman for the World Health Organisation, said around 100 health workers had been infected by Ebola in the three countries, with 50 of them dying, in spite of “very strict procedures” governing how personal protection equipment is used.

Those deaths include the top doctor who had been leading the battle against Ebola in Sierra Leone, Sheikh Umar Khan, who was described as a “national hero” by ministers.

As well as poor hygiene standards, local and international health workers, led in many cases by MSF and the WHO, face a combination of fear, suspicion and local traditions for burying the dead as they try to prevent Ebola spreading further.

In Liberia, the anti-viral effort is being led by the Health Ministry – but part of its headquarters were destroyed last week when a man set in on fire in protest over the Ebola death of his 14-year-old brother.