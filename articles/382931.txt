CHICAGO — Unexplained rash? Check your iPad. It turns out the popular tablet computer may contain nickel, one of the most common allergy-inducing metals.

Recent reports in medical journals detail nickel allergies from a variety of personal electronic devices, including laptops and cellphones. But it was an Apple iPad that caused an itchy body rash in an 11-year-old boy recently treated at a San Diego hospital, according to a report in Monday's Pediatrics.

See also: Fleshlight Releases iPad Case You Can Hump

Nickel rashes aren't life-threatening but they can be very uncomfortable, and they may require treatment with steroids and antibiotics if the skin eruptions become infected, said Dr. Sharon Jacob, a dermatologist at Rady Children's Hospital, where the boy was treated. Jacob, who co-wrote the report, said the young patient had to miss school because of the rash.

The boy had a common skin condition that causes scaly patches, but he developed a different rash all over his body that didn't respond to usual treatment. Skin testing showed he had a nickel allergy, and doctors traced it to an iPad his family had bought in 2010.

Doctors tested the device and detected a chemical compound found in nickel in the iPad's outside coating.

"He used the iPad daily," she said.

He got better after putting it in a protective case, she said

Whether all iPad models and other Apple devices contain nickel is uncertain; Apple spokesman Chris Gaither said the company had no comment.

Nickel rashes also have been traced to other common products including some jewelry, eyeglass frames and zippers.

Jacob said evidence suggests nickel allergies are become more common, or increasingly recognized. She cited national data showing that about 25 percent of children who get skin tests for allergies have nickel allergies, versus about 17 percent a decade ago.

She said doctors need to consider electronic devices as potential sources when patients seek treatment for skin rashes.