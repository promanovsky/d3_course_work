Brussels: The European Union on Wednesday allocated an extra two million euros ($2.7mn) to fight the Ebola outbreak spiralling out of control in West Africa, bringing total EU funding to 3.9 million euros.

"The level of contamination on the ground is extremely worrying and we need to scale up our action before many more lives are lost," said the EU`s Humanitarian Aid Commissioner Kristalina Georgieva.

The European Union has deployed experts on the ground to help victims and try to prevent contagion but Georgieva called for a "sustained effort from the international community to help West Africa deal with this menace".

To date the epidemic has caused 670 deaths and 1,200 cases in Guinea, Liberia and Sierra Leone, among the world`s poorest countries, with one case confirmed in Nigeria.

The World Health Organisation says this is the largest recorded outbreak in terms of cases, deaths and geographical coverage.

The EU said the risk of the virus spreading to Europe was currently low, since most cases are in remote areas in the affected countries and those who are ill or in contact with the disease are encouraged to remain isolated.