When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

A rare full 'Honey Moon' has not fallen on Friday 13th in June since 1919 and it will not appear again until 2098.

The event will send shivers down the spine of horror story aficionados particularly after the advent of the recent Blood Moon which was said to herald the ‘End of Days’ in Christian writings.

According to ancient literature, the movements of the moon, stars and sun can be read as signs of what is to come for the people on earth.

The Bible even forewarns: “And there shall be signs in the sun, and in the moon, and in the stars; and upon the earth distress of nations, with perplexity; the sea and the waves roaring.” [Luke 21:25]

“For the stars of heaven and the constellations thereof shall not give their light: the sun shall be darkened in his going forth, and the moon shall not cause her light to shine. [Isaiah 13:10]

The Honey Moon however is somewhat less foreboding and is said to herald a fruitful summer, according to some ancient civilisations.