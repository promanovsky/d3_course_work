Problems conceiving? High cholesterol could be to blame for poor fertility



Women with high cholesterol take longer to conceive than healthy women

If her partner also has high cholesterol it is even harder for her to conceive

It is not yet known exactly why cholesterol affects fertility



Women with high cholesterol took longer to conceive than those whose readings were normal

It is known to be bad for the heart. But high cholesterol could also damage a couple’s chances of becoming parents.

A study found that women with high amounts of the artery-clogging fat in their blood took longer to conceive than those whose readings were normal.



And when both members had high readings, it took even longer.



The scientists, from the U.S. government’s health research arm, advised those who want to start a family to have a cholesterol test first.



The researchers tracked the fortunes of hundreds of 401 couples who were trying for a baby.



None were known to have fertility problems and all of the women were aged between 18 and 44.



Over the course of a year, 347 of the women became pregnant, while 54 did not.



Blood samples taken at the start of the study showed a clear link between cholesterol and fertility.



Importantly, the finding could not simply be explained away by those with high cholesterol being overweight.



Researcher Dr Enrique Schisterman, from the U.S. government’s health research arm, said: ‘We've long known that high cholesterol levels increase the risk for heart disease.



‘From our data, it would appear that high cholesterol levels not only increase the risk for cardiovascular disease, but also reduce couples' chances of pregnancy.



‘In addition to safeguarding their health, our results suggest that couples wishing to achieve pregnancy could improve their chances by first ensuring that their cholesterol levels are in an acceptable range.’



He added that if other studies confirm the finding, advice on watching cholesterol levels may no longer be centred on the middle-aged and elderly.

Similarly, young people may be given cholesterol-busting drugs to maximise their odds of having children, the Journal of Clinical Endocrinology and Metabolism reports.



Couples who both had high levels of artery-clogging fat (pictured) took even longer to conceive, regardless of their weight

It isn’t entirely clear why cholesterol affects fertility but the waxy substance is key to the synthesis of sex hormones like oestrogen and testosterone in the body.



One in six couples has trouble starting and family half of Britons have high cholesterol – defined as a total reading of above 5mmol/L.



Adults aged between 40 and 75 are entitled to cholesterol tests on the NHS every five years. People on cholesterol-lowering drugs can have a test each year.



Professor Neil McClure, a fertility specialist at Queen’s University Belfast, said it was to too early to advice the cholesterol testing of prospective parents.