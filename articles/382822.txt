Another federal investigation has found numerous safety problems at the Centers for Disease Control and Prevention’s bioterrorism lab after a lapse last month that potentially exposed several dozen workers to anthrax, according to a House memo posted Monday.

The problems included storing anthrax in unlocked refrigerators, transferring dangerous materials in Ziploc bags, and using expired disinfectants, the U.S. Department of Agriculture found.

The findings were summarized in the memo, released before CDC Director Thomas R. Frieden testifies before a House panel Wednesday about last month’s anthrax incident.

In June, the CDC said a problem at its main Bioterrorism Rapid Response and Advanced Technology anthrax lab in Atlanta could have exposed more than 80 workers to the pathogen. A lab preparing samples of anthrax for further study did not properly ensure the bacteria was dead, it said.

Advertisement

The USDA inspected the lab after that announcement. According to the memo, its findings included:

Containers of anthrax were missing, and inspectors had to track them down.

Anthrax was still sitting in an unlocked lab.

Researchers could not remember whether they had used expired bleach to decontaminate areas potentially exposed to anthrax.

At least some lab workers who potentially had been exposed to anthrax were not examined by a CDC clinic for five days.

There was confusion over who was leading the decontamination effort, what procedures to follow and even what decontamination chemical to use.

“Each layer we peel back in this investigation seems to reveal a new instance of carelessness in the CDC’s management of dangerous pathogens,” Rep. Tim Murphy (R-Pa.), chairman of the House Energy and Commerce subcommittee on oversight and investigations, said in a statement.

The USDA investigation was separate from the CDC’s internal inquiry, in which the CDC revealed that pathogens including anthrax, botulism and a deadly strain of bird flu were improperly sent among government labs in five incidents in the last decade.

“These events should never have happened,” Frieden told reporters Friday, when the CDC report was released. “I’m disappointed, and frankly I’m angry about it.”

Advertisement

The CDC is one of the government’s top health and research agencies.

For more news, follow @raablauren on Twitter.