Screenshot by Lance Whitney

Microsoft may finally unveil its long-awaited Office for iPad on Thursday.

An email sent Monday from Microsoft to members of the press confirmed the briefing for March 27 in San Francisco with new CEO Satya Nadella delivering the opening remarks. The topic of the event will be "news focused on the intersection of cloud and mobile computing," suggesting that Microsoft's Office for iPad will be the center of attention.

Scheduled to start at 11 a.m. PT, the event will be available as both a live and on-demand Webcast through the Microsoft News Center.