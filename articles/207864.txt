Euro extends losses as market braces for ECB rate cut

Investing.com - The euro carried last week's losses against the pound into Monday as investors continued to avoid the single currency on expectations for the European Central Bank to loosen policy in June.

In U.S. trading, was down 0.13% at 0.8154, up from a session low of 0.8143 and off a high of 0.8168.

The pair was likely to find support at 0.8134, the low from Jan. 9, 2013, and resistance at 0.8248, Thursday's high.

Last week, the European Central Bank left interest rates unchanged at 0.25%, though the euro dropped after ECB President Mario Draghi said the bank's governing council is comfortable with acting at its next meeting after the bank publishes fresh inflation and growth forecasts.

On Monday, ECB Vice President Vitor Constancio said medium-term inflation outlook will take center stage when monetary authorities consider implementing fresh policy measures.

He said the ECB was considering a wide range of policy options but stopped short of indicating what the bank may decide.

He stressed the ECB won't ignore the euro's strength but added that the exchange rate is not a policy target.

Elsewhere Monday, International Monetary Fund head Christine Lagarde renewed calls for more stimulus from the ECB, warning that persistently low inflation rates posed a serious threat to the European recovery.

The euro was flat against the dollar, with down 0.01% to 1.3756, and up against the yen, with up 0.26% at 140.46.

On Tuesday, the U.K. is to release private-sector data on retail sales.

The ZEW Institute is to release its closely watched report on German economic sentiment, a leading indicator of economic health.