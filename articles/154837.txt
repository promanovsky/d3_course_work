Grappling with fast-changing technology, Supreme Court justices debated Tuesday whether they can protect the copyrights of TV broadcasters to the shows they send out without strangling innovations in the use of the internet.

The high court heard arguments in a dispute between television broadcasters and Aereo Inc., which takes free television signals from the airwaves and charges subscribers to watch the programs on laptop computers, smartphones and even their large-screen televisions. The case has the potential to bring big changes to the television industry.

There was a good measure of skepticism about Aereo's approach, sometimes leavened with humor. Chief Justice John Roberts declared at one point: "I'm just saying your technological model is based solely on circumventing legal prohibitions that you don't want to comply with, which is fine. I mean, you know, lawyers do that."

But several justices expressed concern that a ruling for the broadcasters could hamper the burgeoning world of cloud computing, which gives users access to a vast online computer network that stores and processes information.

Justice Stephen Breyer said the prospect makes him nervous. "Are we somehow catching other things that would really change life and shouldn't?" Breyer asked.

Paul Clement, representing the broadcasters, tried to assure the court it could draw an appropriate line between Aereo's service and cloud computing generally. People who merely retrieve what they have stored should have no reason to worry, Clement said.

But David Frederick, representing Aereo, said the "cloud computing industry is freaked out about the case" because it sees its $10 billion investment at risk if the court were to hold that anytime music or an image is stored online and then retrieved, the copyright law would be implicated.

The discussion veered between references to Roku, a TV streaming device, and other high-tech gadgets on the one hand, and analogies to coat-check rooms and valet parking in an effort to make matters more understandable on the other. There was even Breyer's quaint reference to a "phonograph record store."

Aereo's service starts at $8 a month and is available in New York, Boston, Houston and Atlanta, among 11 metropolitan areas. Subscribers get about two dozen local over-the-air stations, plus the Bloomberg TV financial channel.

In each market, Aereo has a data center with thousands of dime-size antennas. When a subscriber wants to watch a show live or record it, the company temporarily assigns the customer an antenna and transmits the program over the Internet to the subscriber's laptop, tablet, smartphone or even a big-screen TV with a Roku or Apple TV streaming device.

The antenna is only used by one subscriber at a time, and Aereo says that's much like the situation at home, where a viewer uses a personal antenna to watch over-the-air broadcasts for free.

Chief Justice Roberts repeatedly asked Frederick whether the tiny antennas existed for any reason other than to avoid paying the broadcasters for their content. "Is there any reason you need 10,000 of them?" Roberts said at one point. He suggested that it might not affect his view of the case if there was no other reason.

But Frederick said it was much cheaper for Aereo, backed by billionaire Barry Diller, to add equipment as it grows, rather than start with a single large antenna.

Broadcasters including ABC, CBS, Fox, NBC and PBS sued Aereo for copyright infringement, saying Aereo should pay for redistributing the programming the same way cable and satellite systems must or risk high-profile blackouts of channels that anger their subscribers. Some networks have said they will consider abandoning free over-the-air broadcasting if they lose at the Supreme Court.

The broadcasters and their backers argue that Aereo's competitive advantage lies not in its product, but in avoiding paying for it.

There are signs people are starting to forgo pay-TV subscriptions by relying on Internet services such as Netflix and Hulu Plus for television shows. A service that offers live television, as Aereo does, could make such cord-cutting even more palatable. A study last year from GfK estimated that 19 percent of TV households had broadcast-only reception, up from 14 percent in 2010.

Broadcasters worry they will be able to charge cable and satellite companies less if they lose subscribers. But Aereo argues that broadcasters would benefit from increased advertising revenue from increased viewership. The company says many of its subscribers are under 30 and have never had cable service.

Aereo founder and CEO Chet Kanojia recently told The Associated Press that broadcasters can't stand in the way of innovation, saying, "the Internet is happening to everybody, whether you like it or not." Aereo plans to more than double the number of cities it serves, although the high court could put a major hurdle in the company's path if it sides with the broadcasters.

The federal appeals court in New York ruled that Aereo did not violate the copyrights of broadcasters with its service, but a similar service has been blocked by judges in Los Angeles and Washington, D.C. A district judge in Utah also ruled against Aereo, saying that Aereo's service is "indistinguishable from a cable company."

The 2nd U.S. Circuit Court of Appeals in New York said its ruling stemmed from a 2008 decision in which it held that Cablevision Systems Corp. could offer a remote digital video recording service without paying additional licensing fees to broadcasters because each playback transmission was made to a single subscriber using a single unique copy produced by that subscriber. The Supreme Court declined to hear the appeal from movie studios, TV networks and cable TV companies.

In the Aereo case, a dissenting judge said his court's decision would eviscerate copyright law. Judge Denny Chin called Aereo's setup a sham and said the individual antennas are a "Rube Goldberg-like contrivance" — an overly complicated device that accomplishes a simple task in a confusing way — that exists for the sole purpose of evading copyright law.

A decision is expected by late June.