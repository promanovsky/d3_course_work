Researchers found no significant association between the time spent using a computer or driving and higher risk of premature death from all causes.

Washington: Adults who watch TV three hours or more a day may double their risk of premature death from any cause, a new study has warned.

Researchers suggest adults should consider getting regular exercise, avoiding long sedentary periods and reducing TV viewing to one to two hours a day.

"Television viewing is a major sedentary behaviour and there is an increasing trend toward all types of sedentary behaviours," said Miguel Martinez-Gonzalez, the study's lead author and professor and chair of the Department of Public Health at the University of Navarra in Pamplona, Spain.

"Our findings are consistent with a range of previous studies where time spent watching television was linked to mortality," said Martinez-Gonzalez.

Researchers assessed 13,284 young and healthy Spanish university graduates (average age 37, 60 percent women) to determine the association between three types of sedentary behaviours and risk of death from all causes: television viewing time, computer time and driving time.

The participants were followed for a median 8.2 years. Researchers reported 97 deaths, with 19 deaths from cardiovascular causes, 46 from cancer and 32 from other causes.

The risk of death was twofold higher for participants who reported watching three or more hours of TV a day compared to those watching one or less hours.

This twofold higher risk was also apparent after accounting for a wide array of other variables related to a higher risk of death.

Researchers found no significant association between the time spent using a computer or driving and higher risk of premature death from all causes.

Researchers said further studies are needed to confirm what effects may exist between computer use and driving on death rates, and to determine the biological mechanisms explaining these associations.

"Our findings suggest adults may consider increasing their physical activity, avoid long sedentary periods, and reduce television watching to no longer than one to two hours each day," Martinez-Gonzalez said.

The research was published in the Journal of the American Heart Association.

PTI