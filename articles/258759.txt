A low-carbohydrate, vegan diet may reduce a person’s risk for heart disease by 10 percent over 10 years, Medical News Today reported.

In a study published in the journal BMJ Open, researchers followed 39 overweight men and women from April 2005 to November 2006. Part of the group followed a low-carb, vegan diet – referred to as the ‘Eco-Atkins’ diet – while the other group followed a high-carbohydrate, low-fat diet for the six-month period of time.

Participants on the Eco-Atkins diet selected their own foods from meal plans suggesting good options, and they consumed approximately 26 percent of their calories from carbohydrates, 31 percent from proteins and 43 percent from fats, which mainly consisted of vegetable oils.

Overall, participants on the Eco-Atkins diet had 10 percent lower cholesterol and lost an average of four more pounds than the participants on the high-carb, low-fat diet. Based on these findings, the researchers, including Dr. David Jenkins of St. Michael's Hospital in Toronto, Canada, calculated that the Eco-Atkins diet could be equated with a 10 percent reduced risk of heart disease over a 10-year period.

"We conclude that a weight-loss diet which reduced carbohydrate in exchange for increased intakes of vegetable sources of protein, such as gluten, soy and nuts, together with vegetable oils offers an opportunity to improve both LDL cholesterol and body weight, both being risk factors for heart disease,” the researchers wrote in their study. “Further trials are warranted to evaluate low-carbohydrate diets, including more plant-based low-carbohydrate diets, on heart disease risk factors and ultimately on heart disease."

Click for more from Medical News Today.