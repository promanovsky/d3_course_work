BP has warned the markets that additional sanctions on Russia could have a "material adverse" impact on its investments, business and financial position despite the oil giant reporting a surge in profits during the first six months of the year.

In its results statement, BP revealed that profits rocketed by 34% to $3.6bn (£2.1bn, €2.7bn) from the same period last year. This is also an increase of 13% from the first quarter in 2014.

However, the financial outlook warning comes as the EU and US agreed to implement fresh sanctions on the Russian Federation and individuals connected to the government.

BP's chief executive officer Bob Dudley confirmed that it had been "another successful quarter, delivering both operational progress and robust cash flow" but made it clear that the group faces a number of potential hurdles.

"If further international sanctions are imposed on Rosneft or new sanctions are imposed on Russia or other Russian individuals or entities, this could have a material adverse impact on our relationship with and investment in Rosneft, our business and strategic objectives in Russia and our financial position and results of operations," said BP in a statement.

"Any future erosion of our relationship with Rosneft, or the impact of further economic sanctions, could adversely impact our business and strategic objectives in Russia, the level of our income, production and reserves, our investment in Rosneft and our reputation."

BP owns a 19.75% share in Rosneft, the largest-oil company in the world, the rest of which is owned by the Russian government. The company along with its CEO Igor Sechen has already faced some economic sanctions, with speculation that it could be embargoed further still should tensions remain.

BP earned $1bn from Rosneft in the quarter, with a further dividend payment of $700m coming earlier this month.

The company's accounts confirm that it set aside an additional $260m for "the provision for litigation related to the Gulf of Mexico oil spill", taking the total figure spent on the disaster to $43bn (pre-tax).

Dudley said: "This operational momentum keeps us well on track to meet our 2014 targets and underpins our longer-term commitment to grow distributions to our shareholders."

The strong performance is attributed to rising production in higher-margin projects and upping production in the Whiting refinery in Indiana.

Early trading shows that BP's shares have risen by 0.6% on the results, despite the warning over the situation in Russia.

Governments from the EU and US agreed on 28 July to escalate economic sanctions on Russia, after months of relative appeasement. Leaders from the US, UK, Germany, France and Italy participated in a five-way video-conference ahead of an EU meeting today which will finalise their new sanctions regime.

The US government is set to follow suit later this week.