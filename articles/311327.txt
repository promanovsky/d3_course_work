Gold prices crawled to a 3-month peak Tuesday amid fears that Iraq will soon unravel due to sectarian conflict. Iraqi insurgents have reached the country's largest oil refinery and other territory in the country's north and west regions.

U.S. Secretary of State John Kerry says Iraqi leaders have agreed to form a new government by July 1, as the Malaki regime looks to quell the uprising.

With its safe haven appeal boosted by geopolitical tensions, U.S. Gold for August was up $6 at $1,324 an ounce, its highest since mid-April.

Silver for July rose 17 cents to $21.08 an ounce.

Meanwhile, copper is up marginally at $3.145 per pound.

Platinum prices were flat, even after unions announced the end a 5-month strike in South Africa. Platinum was holding at $1,456.60 a troy ounce, as traders continue to expect supply issues.

On the economic front, the results of two house price surveys will be out at 9 am ET. The Federal House Finance Agency's house price index is expected to have risen 0.5 percent month-over-month in April following a 0.7 percent increase in March.

The S&P/Case-Shiller's 20-city house price index may have risen 0.9 percent both on a seasonally adjusted and unadjusted basis compared to the previous month.

The Commerce Department is due to release its new home sales data for May at 10 am ET. The consensus estimate calls for new home sales to come in at a seasonally adjusted 441,000 compared to a 433,000-unit rate in April.

The Conference Board's consumer confidence data for June and the Richmond Federal Reserve's manufacturing index are also due at 10 am ET.

For comments and feedback contact: editorial@rttnews.com

Market Analysis