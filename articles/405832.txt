Both electric car maker Tesla Motors ( TSLA ) and social media firm LinkedIn ( LNKD ) reported Q2 earnings after the bell Thursday, and both representatives of the 21st century economy outperformed expectations on both top and bottom lines. Tesla posted a loss of 16 cents per share (accounting for stock-based compensation) on revenues of $858 million in the quarter. LinkedIn earnings reached 4 cents per share on $534 million.

Tesla's numbers look good on all important metrics: 8763 Model S cars produced in the quarter, up from an expected 8500, and the company delivered 7579 cars, slightly above the 7500 expected. Tesla is now on pace to generate 35,000 cars for 2014, where guidance had only expected 34,000. The company currently makes 800 Model S cars per week.

Tesla has confirmed it has broken ground on property in Reno, NV which may serve as the company's first gigafactory battery producing plant. It will be working with Panasonic ( P ) on creating more and higher-quality electric fuel batteries, which has been about the only thing hindering growth for Tesla so far. The company has no direct competitor at this time (even though CEO Elon Musk has given away his patents for free during the quarter, which may lead to rival electric car manufacturing at some point in the future), and the Zacks consensus estimate for 2014 of 8 cents per share zooms up to $2.37 per share for fiscal 2015.

For its part, LinkedIn followed along the successful path of social media firms already reported, such as Facebook ( FB ) and Twitter ( TWTR ), both of which handily beat earnings estimates for the quarter. All three sectors of LinkedIn's business -- Talent Solutions, Marketing Solutions and Premium Subscriptions -- were up 40+% in the company's Q2, amounting to a revenue increase of 47% from a year ago. LinkedIn now has 300 million members.

Further, the company raised its guidance for fiscal Q3. Not bad for a company that missed earnings in Q1 a whopping 900%. This is also the first positive earnings surprise for LinkedIn in the last four quarters.

Currently, Tesla is a Zacks Rank #3 (Hold) and LinkedIn is a Zacks Rank #2 (Buy). Tesla is trading down slightly in the after-market and is trading down over the past five days. LinkedIn was down in a bearish regular-day trading session, but is up over 7% after hours.

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

TESLA MOTORS (TSLA): Free Stock Analysis Report

LINKEDIN CORP-A (LNKD): Free Stock Analysis Report

PANDORA MEDIA (P): Free Stock Analysis Report

FACEBOOK INC-A (FB): Free Stock Analysis Report

TWITTER INC (TWTR): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.