A giant, mysterious crater in northern Siberia is probably a melted ice formation rather than a hole from a meteor, says an Australian polar scientist.

The Siberian Times reported the giant hole in the Yamal Peninsula - which reportedly translates to “end of the world” - is up to 80 metres wide and has an unknown depth.

A team of Russian scientists have been dispatched to investigate the crater but University of New South Wales polar scientist Dr Chris Fogwill says it’s likely to be a geological phenomenon called a pingo.

“Certainly from the images I’ve seen it looks like a periglacial feature, perhaps a collapsed pingo,” Dr Fogwill said.