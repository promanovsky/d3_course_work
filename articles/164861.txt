A TV interview turned into a kind of impromptu therapy session for Cody Walker on Friday, when he began crying while talking about his brother — the late Paul Walker.

While appearing on Baltimore’s Fox TV affiliate to promote Reach Out Worldwide, the charity started by the “Fast and Furious” star, Walker was unable to hold back tears when asked what he could learn from his late brother’s charitable work.

Also read: ‘Brick Mansions’ Review: Paul Walker Outclasses the Script by Leaps and Bounds

“He was just awesome… I’m sorry… just a super down to earth guy that… I’m sorry… He as a super down to earth guy and he did this without…” Walker struggled to say.

“He was a real man, a man of strength,” anchor Candace Dold told him.

“This is one thing he did without ever wanting recognition for it,” Walker added. “He could have easily thrown his name all over the organization, but he wanted it to grow organically and he wanted to make a difference… He was my big brother and I looked up to him very much.”

Also read: Paul Walker’s Brothers Stand-In For Him in Next ‘Fast & Furious’

The interview was arranged by Relativity Media, which is releasing one of Walker’s final films, “Brick Mansions,” this weekend.

Cody Walker will stand-in for Paul in some scenes in the seventh “Fast and Furious” film, which hits theaters April 10, 2015