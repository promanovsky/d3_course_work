PARIS, FRANCE - FEBRUARY 28: Miranda Kerr is seen leaving the Sonia Rykiel Fashion Show during Paris Fashion Week Womenswear Fall/Winter 2014-2015 on February 28, 2014 in Paris, France. (Photo by Vittorio Zunino Celotto/Getty Images)

Miranda Kerr would "explore" women.

The 30-year-old model is considered one of the most desirable ladies in showbiz and has posed for the likes of Victoria's Secret in the past.

Towards the end of last year, the star revealed she had split from her British actor husband Orlando Bloom and now it seems she's open to embracing her single status.

"I appreciate both men and women," she told the UK's GQ magazine. "I want to explore. Never say never."

The brunette posed in a sexy shoot for the publication, including a racy snap of her braless in a revealing white vest.

According to British newspaper Metro, she also opened up about life since her split from Orlando, 37, admitting that she's been dating.

Miranda continued to candidly explained that she felt less toned now that her sex life has quietened down, claiming that bedroom workouts work wonders for her arms and stomach.

The beauty added that the chemistry she had with Orlando was always great.

The former couple have three-year-old son Flynn together and remain on good terms for the sake of his upbringing.

Miranda and Orlando have been spotted out with Flynn since their breakup and she insists there's no animosity.

"It's so important, we will be family forever and we both really love and respect each other. We're really good; we got along better than ever. It's so great," Miranda told the Kyle and Jackie O Show recently.

Online Editors