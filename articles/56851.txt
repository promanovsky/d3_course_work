Tesla is one of the few car companies that sells its vehicles direct to the public in the US, other manufacturers sell their vehicles through franchised dealers.

This has lead to sales of Tesla being banned in states like New Jersey, this will come into place on the first of April, and other US states are also looking at similar bans.

Now Tesla has reached an agreement with the Ohio Automobile Dealers Association that will allow them to sell their electric vehicles direct to the public in Ohio.

The deal has also been approved by a Senate panel, and Tesla will be allowed to run three stores in Columbus, Cincinnati and Cleveland.

Ohio was one of the previous states that was looking to block sales of Tesla vehicles direct to the public by the manufacturer.

Automobile dealers in the US, which are mainly independent dealers or franchised dealers, saw Tesla’s sales model as a threat to their business, and have tried to stop Tesla selling their cars direct to the public.

This could also have a bearing on other US states who are also looking to stop Tesla from selling their cars through their own retail stores.

Source AP

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more