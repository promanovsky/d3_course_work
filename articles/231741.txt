EBay urged its users to update their passwords on Wednesday due a cyberattack that compromised its database, but only after an initial round of confusion when it posted cryptic messages about the incident and then quietly removed the messages soon after.

Although the initial message was slim on details, the company has since posted a note to its official blog page saying it will ask its 145 million eBay users to change their passwords later on Wednesday.

"[We] will be asking eBay users to change their passwords because of a cyberattack that compromised a database containing encrypted passwords and other non-financial data," the message reads.

The breach, which was confirmed by investigators this week, happened in late February and early March, when attackers snatched up information such as usernames, encrypted passwords, email addresses, physical addresses, phone numbers and dates of birth. But the company said no financial information was taken.

"There is no evidence that any financial information was accessed or compromised; however, we are taking every precaution to protect our customers," an eBay spokesperson told Mashable.

The spokesperson also said PayPal's database was not affected, but all eBay Marketplaces (Ebay.com) accounts have been compromised.

"[We have] no evidence of unauthorized access or compromises to personal or financial information for users of PayPal," the spokesperson added. "PayPal data is stored separately on a secure network, and all PayPal financial information is encrypted. Likewise, we have no evidence of any unauthorized access to other sites operated by eBay Marketplaces, such as StubHub, eBay Classifieds, Tradera, Gmarket, GumTree or GittiGidiyor."

The company declined to comment on why the first version of the blog post was removed, but said it is gearing up to let users know about the security breach as soon as possible.

"[We have] no evidence of the compromise resulting in unauthorized activity for eBay users, and no evidence of any unauthorized access to financial or credit card information, which is stored separately in encrypted formats," the company's statement reads. "However, changing passwords is a best practice and will help enhance security for eBay users."

The company also plans to ask eBay users who used the same password on other sites to update those too.