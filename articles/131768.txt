At least two hospitals, Massachusetts General and Brigham and Women’s, suspended the procedure, called power morcellation. “I have asked our doctors to stop the procedure immediately until more information is available,’’ Dr. Isaac Schiff, Mass. General’s chief of obstetrics and gynecology, said in an e-mail.

Dr. William Maisel, deputy director for science and chief scientist at the Center for Devices and Radiological Health, said new data show the procedure can dangerously spread undetected cancers more often than previously thought, significantly worsening a woman’s chances for long-term survival.

In a major safety advisory released Thursday, the Food and Drug Administration discouraged surgeons from using a powered cutting tool to remove uterine fibroids, leading two Boston hospitals to immediately suspend the common procedure.

Advertisement

Dr. Robert Barbieri, chairman of obstetrics and gynecology at the Brigham, said in a written statement that “we are immediately suspending use of this device in all cases until further notice.”

Both Boston hospitals were among a small group of US medical centers that put safety restrictions on the procedure this year.

Maisel said an agency review of available data revealed that 1 in 350 women who have fibroids removed or undergo a hysterectomy to treat fibroids have an unknown uterine sarcoma, a type of cancer that can be aggressive. If power morcellation is performed in these women, so that the tissue can be removed through small laparoscopic incisions, the cancer can be spread around the abdomen and pelvis, he said.

He said other treatment options are available for women, including traditional surgical hysterectomy and high-intensity focused ultrasound. Maisel said the agency will convene a public meeting of the Obstetrics and Gynecological Medical Devices Panel this summer to discuss further safeguards, including strong “black box’’ warnings from manufacturers.

Meanwhile, the agency has told device makers to review their current product labeling to ensure they contain accurate risk information for patients and doctors.

Advertisement

In a telephone call with reporters, Maisel said the agency decided not to order the devices off the market because “there still may be individual patients who benefit from the procedure.’’ He said “there is no medical device we have authorized for marketing that doesn’t have risk associated with it — some are severe life-threatening risks.’’

The agency’s review was prompted by media coverage of the case of Dr. Amy Reed, a Boston anesthesiologist who underwent a hysterectomy at the Brigham in October to treat what she was told were likely benign fibroids. An electric morcellator was used.

Follow-up tests on the removed tissue found that Reed had uterine leiomyosarcoma, a very aggressive tumor. Later imaging tests showed that the cancerous tissue had been spread throughout her abdominal cavity, giving her stage 4, advanced cancer. Reed, 41 and the mother of six children, is now undergoing chemotherapy.

Reed’s husband, Dr. Hooman Noorchashm, a cardiothoracic surgeon at the Brigham, started an online petition and has written hundreds of strongly-worded letters and e-mails to medical journals, government officials, doctors, and media organizations charging that morcellation is fundamentally unsafe and should be banned. His advocacy campaign led media outlets to report on other women who suffered similar outcomes to Reed.

Reed’s misfortune occurred at about the same time another woman, who also had her hysterectomy at the Brigham, died of leiomyosarcoma that had been spread around her abdomen during morcellation.

Advertisement

Power morcellation “is no longer a standard of care,’’ Noorchashm said Thursday. “This statement from the FDA formalizes that.’’

Last month, the Brigham and Mass. General each adopted a policy allowing surgeons to use the device only inside a bag, which is inserted into a patient to surround the fibroids before they are minced.

The safety of performing morcellation in a bag has not been studied, however, and the Brigham, Mass. General, Johns Hopkins Hospital, and Dr. K. Anthony Shibley, a Minnesota surgeon who invented a technique to perform morcellation in a bag, are collaborating on a study that will track patient results and complications.

Maisel said Thursday that the theory behind the bag technique “appears to be sound’' but that “it is important for people to understand the bag is not a panacea” because tissue can escape before the bag is put in place. He said bags can tear, obstruct a surgeon’s view, and potentially damage other organs. Surgeon training also can be an issue.

“It is something that will be discussed at the advisory board meeting,’’ he said. “The overall benefits and risks need to be taken into consideration.’’

Between 500,000 and 600,000 women a year undergo hysterectomies, up to 40 percent for painful fibroids, which can also cause bleeding, Maisel said. It’s unclear how many involve morcellation but estimates range from 50,000 to 150,000.

The procedure has grown over the past decade into the standard of care across the country. It has enabled doctors to move away from traditional surgery, using a long incision across the belly, to laparoscopic surgery, which requires several much-smaller cuts and allows patients to recover more quickly with less pain and fewer infections.

Advertisement

“The clinical community has been aware of risk of cancer spread since the advent of the procedure,’’ Maisel said. “The risk is not new. What is new is that the risk is higher than originally appreciated.’’

In response to the FDA alert, two organizations representing doctors who perform the procedure said they are conducting their own reviews of power morcellation. “We greatly appreciate the urgency behind this issue,’’ the American Congress of Obstetricians and Gynecologists said in a statement.

Liz Kowalczyk can be reached at kowalczyk@globe.com.