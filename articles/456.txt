Public transportation trips reached 10.7 billion last year, the greatest number of rides since 1956.

Overall public transportation use grew 1.1% in 2013, and is up 37.2% from 1995 when transportation use outpaced population growth in the U.S., according to new data from the American Public Transportation Association (APTA). Last year was the eighth year in a row that over 10 billion trips were taken on public transportation across America. There were even more trips than in 2008 when gas prices rose to $4-$5 a gallon, and more Americans were opting out of driving, USA Today reports.

The APTA says economic recovery is a predominant reason for more transit rides, since public transportation use increases when more people have jobs. The APTA reports that about 60% of trips taken last year were people commuting to work.

[APTA]

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.