U.S. regulators are apparently considering whether to take a fresh look at wireless and wireline networks and whether to apply the same net neutrality rules to both fixed and wireless Internet traffic, Reuters writes.

These two types of Internet providers offer access to similar services and have plenty of active users in the country, with some Internet users relying solely on a mobile device to get online. Furthermore, wireless carriers develop fixed broadband lines, while cable broadband providers create Wi-Fi hotspots to further improve their services and attract more customers, actions that further “blur” the lines between these two business types.

“It’ll be a topic that will have big resonance among the commissioners: why should wireless be treated differently than wireline in terms of net neutrality,” one senior FCC official told the publication.

Under the 2010 rules, both fixed and wireless ISPs were banned from blocking access to Internet websites, but wireless carriers had more freedom when it comes to managing Internet services. However, consumer groups are looking for stricter rules for wireless carriers as well that would match rules imposed to fixed ISPs, which may or may not be related to the FCC’s fast-lane-slow-lane Net Neutrality proposal.

“The distinction between wireless and wireline is certainly not the same as it was… The enforceable net neutrality rules should apply equally, whether you use the Internet on your mobile or home broadband,” Internet Association head Michael Beckerman, who represents various Internet companies including Amazon and Netflix, said. “There will be differences in terms of network management, but at the end of the day, the same fundamental principles … need to apply to the mobile world.”

Meanwhile, wireless carriers say that stricter rules would ultimately hurt their networks, leading to slower Internet speeds. “The FCC already acknowledged the unique nature of wireless, specifically the technical and operational challenges our industry faces, including the need to … actively manage networks to provide high quality service to a customer base that is constantly on the go,” CTIA CEO Meredith Attwell Baker said.

The FCC is currently collecting comments from the public until September 10, when a decision on its proposed Net Neutrality rules for fixed ISPs will be made.