Shortly after the announcement of its new Nike+ Fuel Lab initiative earlier this month, Nike is said to have dismissed a major chunk of employees from FuelBand's hardware tea.

CNET reported the development including a quote from Brian Strong, Nike's spokesperson, who in an email said "As a fast-paced, global business we continually align resources with business priorities. As our Digital Sport priorities evolve, we expect to make changes within the team, and there will be a small number of layoffs. We do not comment on individual employment matters."

It has been reported that out of Nike Fuelband's 70-people hardware team, around 55 were let go by the firm. However, it's not clear as to how many employees have been internally recruited in other Nike divisions and how many have been let go by the company.

CNET's source also added that the next version of FuelBand, said to be slimmer in design and expected to come out this fall, has also been shelved. The firm however confirms that all this would not influence the Nike+ FuelBand SE - currently on sale - in any way.

"The Nike+ FuelBand SE remains an important part of our business. We will continue to improve the Nike+ FuelBand App, launch new METALUXE colors, and we will sell and support the Nike+ FuelBand SE for the foreseeable future," added Strong.

Interestingly, this particular news was already making rounds on Secret, an anonymous networking website where tech industry insiders regularly post gossip. "The douchebag execs at Nike are going to lay off a bunch of the eng team who developed the FuelBand, and other Nike+ stuff. Mostly because the execs committed gross negligence, wasted tons of money, and didn't know what they were doing," read the post, which now has been taken down.

The timing of this development is strange, since this comes a time when all the other brands are coming out with their own wearables and health-tracking devices. HTC has confirmed its interest in wearable devices, Samsung recently announced the Gear Fit and other wearables. Apple is widely expected to come out with its first wearable - tentatively named iWatch - later this year.

It's worth noting that Apple CEO Tim Cook sits on Nike's board of directors and has himself been seen sporting a Nike FuelBand in public. Some have linked the latest development and iWatch rumours to indicate the companies may be ready for a closer relationship.

"Apple is in the hardware business. Nike is in the sneaker business. I don't think Apple sees Nike as competitive. It's likely that an Apple hardware offering would be supportive of the Nike software," Jim Duffy, a Nike analyst with Stifel, Nicolaus & Company, told CNET. "Nike would be content to let Apple sell devices, as long as they would be supportive of the apps."

"Partnering with industry-leading tech companies is nothing new for Nike," Strong is quoted as saying in the report. "We have been working with Apple to develop products since 2006, when we introduced Nike+ Running, and Nike has since created iOS Apps including Nike+ Training Club, Nike+ FuelBand and Nike+ Move."