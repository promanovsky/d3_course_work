Microsoft sent the press invitations tonight for a Surface tablet event on May 20 in New York.

The invitation reads, "join us for a small gathering." It's not a stretch to interpret that play on words to mean Microsoft will announce a smaller version of its Surface Windows 8 tablet.

Microsoft has long been rumored to be working on a smaller version of the Surface, so it's very likely that's what we'll see.

Windows 8 recently got a new update that makes it easier for desktop users to transition to the touch-based operating system. Now, the operating can automatically detect what kind of machine (tablet or desktop/laptop) you're using and adjust the settings appropriately.

By most signals, it looks like Microsoft's Surface tablets aren't selling in massive quantities like the iPad is. Last quarter, Microsoft only reported $500 million in Surface revenue, which includes tablets and accessories.