Lindsay Lohan is suing the makers of the Grand Theft Auto video games.

The Mean Girls star says that latest instalment Grand Theft Auto V based a character on her without her permission.



GTA 5: Best celebrity appearances in the game series

Lohan's suit against Take-Two Interactive Software Inc - which was filed today (July 2) - says the character Lacey Jonas, who seeks help skirting paparazzi, is an "unequivocal" reference to the actress.

She also claims her image, voice and styles from her clothing line are used in the game, with legal documents alleging that "the portraits of the Plaintiff [Lohan] incorporated her image, likeness, clothing, outfits, Plaintiff's clothing line products, ensemble in the form of hats, hair style, sunglasses, jean shorts worn by the Plaintiff that were for sale to the public at least two years".



The game also features a previous home of Lohan's: West Hollywood's Chateau Marmont hotel.

It was first reported in December that Lohan was considering taking legal action against the makers of GTA V.

Grand Theft Auto V was the biggest selling game of 2013, and has sold over 32.5 million units worldwide.

Watch the trailer for Grand Theft Auto V below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io