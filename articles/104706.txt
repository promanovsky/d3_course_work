If you could purchase a magical patch that would make you feel better about your body, would you do it?

In a new marketing campaign for Dove, psychologist and body image specialist Ann Kearney-Cooke presented "real women" with the "RB-X" patch, a "revolutionary product developed to enhance the way women perceive their own beauty." The women were asked to keep a video diary for two weeks chronicling any changes they felt. Every woman featured in the final spot agreed that wearing the patch made them feel better about themselves, which in turn affected their behavior. One participant was more open to approaching guys, another dreaded shopping less than before, and another proudly wore clothes that revealed her arms -- something she would not have done before wearing the patch.

At the end of the video, Kearney-Cooke revealed to each woman that the patch was just a placebo -- and that any change in attitude simply came down to the wearers' state of mind.

"I was really expecting there to be something," participant Brihtney says through her tears. "To see that there's nothing, it's just... it's crazy."

After watching the spot, The Huffington Post reached out to Dove's PR team to verify that the women featured are not actresses. "The women in the film are not actresses," a representative told HuffPost via email. "Dove hosted an open audition for an undisclosed documentary to find women to participate in the film. Dr. Ann Kearney-Cooke presented it as an experiment in which the women were invited to wear a beauty patch that was created to help them feel more beautiful."

In one sense, the ad is empowering, proving that women do have the ability to change how they feel about themselves simply through the power of their mind. "I'm beautiful, I'm strong, and I'm independent... I can just be whoever I want to," participant Katelyn said on discovering the patch had no effect. On the other hand, the "experiment" framework of the ad seems slightly manipulative -- and suggests to women that their low self-esteem can be easily-cured with a couple weeks of positive thinking.

Kate Dries at Jezebel critiqued the commercial, arguing that its underlying message isn't about women's empowerment, but Dove's branding:

It's definitely true that positive thinking works miracles. But that's not what this campaign is really about; it's about teaching women that Dove knows better. Dove is smarter. You should buy Dove because they're on your side and they can teach you things.

While the "beauty patch" experiment had good intentions, it's hard to watch the video without feeling a bit skeptical. Next time, we hope that Dove can make women feel good about themselves without manipulating them first.