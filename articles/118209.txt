Update: Supplies of Google Glass look to have held steady all day, but one color has dried up.

Glass in cotton (white) sold out this afternoon, though Charcoal, Tangerine, Shale and Sky are all still available.

Original story below...

Google Glass went on sale to the general public at 9 a.m. ET/6 a.m. PT today (Tuesday), so as you scramble to finish your taxes, you can start squandering your return money.

The sale marks the first time Glass will be available to the general public, but how exactly are you supposed to sign-up for the Explorer Program?

Lucky for you, your pals at TechRadar are here to help.

How to buy Google Glass

The sale is only slated to last for 24 hours, and space is limited, so acting quickly will be your best bet to get Glass.

If you started bysigning up for a reminder, you should have got a reminder to buy Glass or return to do so at the sale start time.

To buy Glass when the sale starts, you can head to this web page: https://glass.google.com/getglass/shop/glass.

Google has only promised to open "some spots" in the program, indicating that once supplies dry up, it plans to close the sale no matter how long it's lasted.

It's important to note that you have to be 18 or older to get a pair of Glass - so sorry, minors, the sale isn't for you.

What's more, this is a Glass "sale" in the sense that Google is literally selling the wearable to more people, not in a discounted sort of way. You'll need to fork over the usual $1,500 + tax to make the purchase.

According to TechRadar's resident wearables expert Matt Swider, the transaction can only be completed using Google Wallet. Having Wallet set up before the sale opened would have saved you precious time, but if you forgot, you can still do so right it here.

New Explorers will have the choice between free frames for prescription Glass or a sunglasses shade with their purchase. These normally sell for $225, but note you'll need to buy prescription Glass from Google when they're made available to actually use the frame.

We'll be keeping an eye on sales and whether the anticipated scramble leads to a server crash or not - have you made the mad dash for Glass? Let us know in the comments!