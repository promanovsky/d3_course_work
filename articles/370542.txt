NEW YORK (CBSNewYork/AP) — A major hospital’s former pharmacy chief stole nearly 200,000 oxycodone pills from the medical center over more than five years and has been charged under a state drug-kingpin law more often aimed at accused street dealers, narcotics prosecutors said Tuesday.

Anthony D’Alessandro was arrested early Tuesday, and had his hands cuffed behind him as he was arraigned on a 274-count indictment. His lawyer didn’t immediately return a call.

City Special Narcotics Prosecutor Bridget Brennan‘s office is still investigating what became of the drugs but believe they ended up on the thriving black market for prescription painkillers, where the more than 193,000 missing pills could fetch a total of about $5.6 million.

Hospital Pharmacist Charged In Theft Of 200K Pills

“This case underscores the vigilance required when addictive medication with a high resale value is readily available — even to licensed professionals and trusted employees,” Brennan said in a statement.

As director of pharmacy services, D’Alessandro was a long-time trusted employee responsible for all the medication stocked at Mount Sinai Beth Israel Medical Center, WCBS 880’s Irene Cornell reported. He served for 14 years before his firing this spring.

D’Alessando is accused of exploiting his access to the hospital’s drug vault to grab oxycodone pills on nearly 220 different dates: about 100 at a time when he started in January 2009, but 1,500 at a time when the scheme came to light this spring, Brennan’s office said. Hospital officials first approached him April 1 about the disappearing drugs, and he signed out another 1,500 pills the next day, prosecutors said.

To account for the missing medicine, D’Alessandro made phony entries in an electronic inventory system to indicate that the drugs were being sent to a research pharmacy within the hospital, prosecutors said. The pharmacy wasn’t doing any oxycodone research at the time, and its staffers were unaware of the phony requisition slips, according to prosecutors.

“He was the one who was in charge of the pharmacy so there was no one auditing him,” Brennan told 1010 WINS. “He knew the hospital systems, he knew where there would be a cross-check and where there wouldn’t be a cross-check. He really used that knowledge in order to further his criminal scheme.”

The hospital launched the investigation in response to an anonymous letter, which came after D’Alessandro’s longtime employer, Beth Israel Medical Center, merged into the Mount Sinai Health System in September.

The hospital noted in a statement that it brought the case to authorities and fired D’Alessandro.

D’Alessandro, 47, was facing charges including a major drug trafficking offense that carries the potential for life in prison. It was the first time Brennan’s office, which focuses on drug cases citywide, has deployed the 2009 law against a pharmacist. It couldn’t immediately be determined whether any other prosecutors around the state have done so.

Fewer than 60 arrests have been made statewide under the law since its inception, state Division of Criminal Justice Statistics data show.

Prosecutors said D’Alessandro also filled at least seven fraudulent prescriptions, written in a relative’s name, from the hospital pharmacy.

In court, D’Alessandro’s defense attorney, Joseph Sorrentino, argued that his client was not the head of a drug trafficking organization, but just an individual who pilfered drugs from a hospital pharmacy for his own use.

But Assistant District Attorney Ryan Sakacs scoffed at that claim, and told the court that over the past four months alone, D’Alessandro stole enough oxycodone to feed one person’s habit for 20 years.

If he consumed all he stole, Sakacs alleged, “we’d be at a funeral, not an arraignment.”

Around the country, at least a few other hospital pharmacists have been accused of stealing drugs they were supposed to dispense.

In 1998, a onetime pharmacy director at another Manhattan hospital pleaded guilty to a federal drug-distribution charge for taking about $60,000 worth of cancer drugs, antidepressants and other medications and selling them for cash, and he was sentenced to just over a year in prison.

More recently, a pharmacy manager was charged in 2008 with stealing a generic form of the stimulant Ritalin from a Raleigh, North Carolina, hospital.

You May Also Be Interested In These Stories:

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)