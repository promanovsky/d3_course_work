Two passenger planes nearly collided at an airport in Barcelona on Saturday when one airliner taxied across the runway while another was coming in to land.

A plane enthusiast caught the near-collision on video at the Barcelona El-Prat airport Saturday morning.

The video shows a plane on final approach to land, applying power and pulling up to avoid landing with the other aircraft in its path.

The plane on the runway, an Aerolineas Airbus A340 from Argentina, was taxiing to a waiting point before takeoff, while the UTair Boeing 767 on approach was arriving from Moscow.

The airliner from Moscow looped around and landed safely a short time later.