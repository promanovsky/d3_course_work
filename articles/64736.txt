A Russian spacecraft carrying three astronauts has docked with the International Space Station 250 miles over Brazil.

The crew aboard the orbiting outpost congratulated the latest members - Russians Alexander Skvortsov and Oleg Artemyev and Nasa's Steve Swanson.

"A flawless approach, a flawless docking," said mission control commentator Rob Navias.

The trio blasted off from Kazakhstan on Wednesday aboard a Soyuz rocket for what was supposed to be a six-hour straight shot to the space station. But an engine burn designed to fine-tune the craft's path did not occur as planned, delaying the docking.

Two rendezvous manoeuvres were executed to put the Soyuz on course. The astronauts were never in danger.

Jaunts to the space station used to take two days. Last year, engineers experimented with a way to cut the trip down to six hours by compressing the number of times the Soyuz fires its engines to raise its orbit to meet the space station.

Had the latest trip been on schedule, it would have been only the fifth time that a crew would have taken the "fast track" route.

Engineers were investigating the snag, but believe the burn didn't occur because the Soyuz was not in the right position.

The new crew, which will stay in orbit for six months, joined Japan's Koichi Wakata, Nasa's Rick Mastracchio and Russia's Mikhail Tyurin.

Since the retirement of the space shuttle fleet, Nasa has depended on the Russians to hitch a ride to the space station, paying nearly 71 million dollars (£42.7m) per seat.

Despite the frosty relationship on Earth between the US and Russia over the annexation of Crimea, politics has not affected the co-operation in space.

Nasa has been working to develop its own space taxi to the orbiting laboratory through contracts with two private companies including California-based SpaceX founded by billionaire Elon Musk.

SpaceX's fourth supply run set for Sunday was postponed because of a problem at the Florida launch site - the second delay this month. The company said earlier that it needed more time.

SpaceX's unmanned capsule named Dragon will haul two tons of supplies and experiments. It will also deliver a pair of legs for the humanoid robot at the space station. Until now, the torso-only Robonaut has been stuck on a pedestal.