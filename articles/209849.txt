Twitter has reportedly added 'mute' feature to its website and mobile apps, which allows one to remove news feeds from specific users without 'unfollowing' or blocking them.

According to CNET, Twitter announced the release of the 'mute' feature on Monday. The feature will start with the website, followed by iOS and then Android apps.

The mute icon will be displayed next to the setting gear, the icon will turn red if the person is muted, and grey if the person is not.

However, the muted person will never know that someone has muted them, which gives the benefit of blocking someone discreetly.