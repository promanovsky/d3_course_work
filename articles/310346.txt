San Francisco's city attorney Dennis Herrera issued a cease-and-desist demand Monday to a mobile app called Monkey Parking, which allows people to auction off public parking spaces that they're using to other nearby drivers, according to The Associated Press.

In a letter to the CEO of the Rome, Italy-based tech startup Paolo Dobrowolny, Herrera cited a provision in San Francisco's police code that prohibits people and companies from buying, selling or leasing public on-street parking and mandates fees up to $300 to drivers who violate the law, the AP reported.

Herrera has given Monkey Parking until July 11 to shut down its operations in San Francisco or possibly face a lawsuit under California's Unfair Competition Law, according to the AP. Herrera added that besides the violations, Monkey's app encourages drivers to unsafely use their mobile devices and engage in online parking bidding wars while behind the wheel.

Parking in San Francisco has long been known as a driver's worst nightmare. A recent San Francisco Municipal Transportation Agency parking census reported that the city has 440,000 parking spots available, but only 275,000 of those are street parking, the AP reported.

"As a general principle, we believe that a new company providing value to people should be regulated and not banned," Dobrowolny wrote, the AP reported. "This applies also to companies like Airbnb, Uber and Lyft that are continuously facing difficulties while delivering something that makes users happy. Regulation is fundamental in driving innovation, while banning is just stopping it."

The city attorney's warning to Monkey comes about a month after his office started investigating the startup, which began its San Francisco operations in April, according to the AP.

"Technology has given rise to many laudable innovations in how we live and work - and Monkey Parking is not one of them. It's illegal, it puts drivers on the hook for $300 fines, and it creates a predatory private market for public parking spaces that San Franciscans will not tolerate," Herrera said in a written statement, the AP reported.

"People are free to rent out their own private driveways and garage spaces should they choose to do so. But we will not abide businesses that hold hostage on-street public parking spots for their own private profit," he added.

Two other tech companies, Sweetch and ParkModo, parking apps that Herrera said also violate city and state law, will receive similar cease-and-desist warnings later this week, according to the AP.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.