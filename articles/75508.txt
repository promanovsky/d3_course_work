(On Monday mornings, Movies Now will be looking at some aspect of the No. 1 movie over the weekend. This week: “Noah” and the return of Russell Crowe as a box office force).

After what seemed like 40 days and 40 nights of controversy over Darren Aronofsky’s biblical epic “Noah” taking creative license in adapting the story of Noah’s ark to the big screen, the movie sailed to the No. 1 box-office spot over the weekend, taking in an estimated $44 million.

For star Crowe, “Noah” marks the second-highest opening of his career and a return to the ranks of high-profile leading men. In recent years, the 49-year-old New Zealand-born actor has mostly turned up in supporting roles, and with mixed results.

Crowe’s most recent film, “Winter’s Tale,” in which he played the demonic gangster Pearly Soames, was panned by reviewers and grossed only $12.6 million, on a $60-million budget.

Advertisement

PHOTOS: Religious movies at the box office

“Man of Steel,” in which Crowe played Superman’s biological father, was a box-office hit to the tune of $291 million but received mixed reviews and fell short of the success of such superhero blockbusters as “The Dark Knight Rises,” “The Avengers” and “Iron Man 3.”

“Les Miserables,” in which Crowe played the dogged policeman Javert, took in $148.8 million at the box office, earned mixed-to-positive reviews and was an awards-season contender in 2013, but Crowe’s singing was widely regarded as one of the film’s weak points.

For “Noah,” Crowe is earning generally favorable reviews, with many critics commending the intensity of his performance. The Times’ Kenneth Turan called Crowe a “fiercely masculine presence” who “also provides the film with some unexpected, if inevitably overwrought, emotional cliffhanger moments.”

Advertisement

Mick LaSalle of the San Francisco Chronicle wrote that “Crowe ably conveys the oppressive weight of Noah’s responsibility, the burden of enacting God’s will,” and Joe Morgenstern of the Wall Street Journal wrote, “the film gambles on its star being able to preserve the hero’s humanity without softening his fury. In doing just that, Mr. Crowe achieves something close to an acting miracle.”

BEST MOVIES OF 2013: Turan | Sharkey | Olsen

Crowe’s dedication to “Noah” has extended off-screen as well. In recent weeks, as the film faced criticism from religious groups for deviating from scripture, and studio Paramount added a disclaimer to the movie’s marketing materials, Crowe took to Twitter and personally lobbied Pope Francis to give the film his blessing.

Though Crowe hoped to meet with the pope privately, he and a “Noah” delegation ultimately had to settle for making Francis’ acquaintance, briefly, at his weekly audience in St. Peter’s Square in Vatican City. Thus far, “Noah” has generated strong turnout among Christian moviegoers, according to Paramount.

Advertisement

Despite Crowe and “Noah’s” initial success, smooth seas are far from guaranteed. Marvel’s latest superhero sequel, “Captain America: The Winter Soldier,” arrives Friday and could well make a splash of its own.

ALSO:

‘Frozen’ passes ‘Toy Story 3' as top grossing animated movie

A treasure trove of silent American movies found in Amsterdam

Advertisement

Emma Watson downplays ‘Little Mermaid’ casting rumors ‘for now’

Follow @ogettell