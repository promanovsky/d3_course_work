As expected, the Bank of Japan (BoJ) left monetary policy unchanged at today’s Policy Board meeting. On 30 April, the Board will meet again to update the bank’s medium term projections. At that occasion, it might have gained more insight in the impact of the three point consumption tax hike on 1 April.



For this reason, the monetary statement released at the end of the meeting was almost identical to those released since 22 January. Some words were added to emphasize that the Board was in particular interested in the underlying trend and would look through fluctuations caused by the consumption tax hike.



The Board was more prudent on industrial production, stating it to be “on a moderate increasing trend”. This followed the sharp decline in industrial output in February (-2.3%), which was partly related to bad weather. In addition, it is clear that manufacturers are bracing themselves for a sharp contraction in demand after the consumption tax hike. There are signs that they have been slowing down production and reducing inventories. It is the main reason for the decline in the coincident and leading indicators.



One phrase was added: “Business sentiment has continued to improve, although some cautiousness about the outlook has been observed.” This is a reference to the latest Tankan survey, which showed a further improvement in the current conditions index and a sharp fall in the expectations index.



Today’s Economy Watchers Survey confirms this picture. Enterprises were very positive about business conditions in March. In particular, the assessment of businesses mainly serving households gained almost seven points. However, the economy watchers are very concerned about the coming months. The overall outlook index lost more than five points, and is well below the 50 mark, which separates expansion and contraction.

BY Raymond VAN DER PUTTEN