HONG KONG (MarketWatch) — Hong Kong stocks on Wednesday jumped to the highest closing level in nearly seven months, after reports showed the manufacturing sectors in the U.S. and in China expanded last month.

The Hang Seng Index HSI, +0.45% rallied 1.6%, rising the most in more than seven weeks, and logging its highest settlement since early December last year.