ZURICH-- Credit Suisse Group AG said Monday its settlement with U.S. authorities for helping Americans evade taxes will slash about $1.8 billion from the bank's profit this quarter.

Zurich-based Credit Suisse said the $2.82 billion settlement, which includes a previously disclosed agreement with the Securities and Exchange Commission and includes a guilty plea, would cut its capital ratio, a key measure of a bank's ability to absorb losses. In a statement, the bank said its capital ratio would have stood at 9.3% if the settlement had been applied at the end of the first quarter, down from the 10% level the bank reported.

Credit Suisse said it would cut its risk-weighted assets, as well as sell real estate and other holdings, to bring its capital ratio to 10% by the end of 2014. The lender said it would follow through with plans to return roughly half of its profit to shareholders in the form of dividends once it had returned to the 10% level.

"We deeply regret the past misconduct that led to this settlement," Chief Executive Brady Dougan said in a statement. He added that the bank had seen "no material impact" on its business caused by attention to the settlement, details of which began leaking over the past few weeks.

The deal brings to a close an issue that has hung over Credit Suisse for about three years since it disclosed that U.S. authorities began investigating the bank for allegedly helping Americans use Swiss banking secrecy to avoid taxes. Earlier this year, a U.S. Senate subcommittee summoned Mr. Dougan and three other Credit Suisse executives to testify at a hearing, during which the CEO acknowledged some employees had improperly recruited U.S. clients but said top management was unaware of any misconduct.

As part of the settlement, Credit Suisse pleaded guilty to one count of conspiring to help U.S. customers present false income-tax returns. The guilty plea marked the first time in more than a decade that a financial institution plead guilty to a crime.

Credit Suisse already has set aside more than $800 million to settle the issue. It also previously agreed to pay $196 million to the SEC to settle related allegations that Credit Suisse bankers provided unregistered services to American clients.

Write to Andrew Morse at andrew.morse@wsj.com and John Letzing at john.letzing@wsj.com

Access Investor Kit for Credit Suisse Group AG

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=CH0012138530

Access Investor Kit for Credit Suisse Group AG

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US2254011081

Subscribe to WSJ: http://online.wsj.com?mod=djnwires