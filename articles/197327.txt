If you're following Tesla Motors (NASDAQ:TSLA), you've probably already read the company's earnings release, as well as perhaps an article summarizing the electric-vehicle maker's first-quarter 2014 results, which were released after the market close on Wednesday. Briefly, Tesla exceeded both revenue and earnings estimates and nudged past its vehicle deliveries guidance (6,457 versus 6,400), though its Q2 earnings guidance fell short of what analysts had been expecting, which was surely a key reason the stock sold off more than 7% in after-hours trading.

My purpose here isn't to rehash the earnings' results, but to supplement the information you've probably read with some additional color from Tesla's conference call with analysts. There's a wealth of information an investor can glean from these calls. There were too many valuable nuggets shared to cover them all, but here are four you should know about.

1. Tesla expects to break ground on the first Gigafactory site next month

CEO Elon Musk stated on the call that Tesla plans to break ground on the first site for the company's massive $5 billion lithium-ion battery factory "probably next month." No news was provided as to where that site will be, as the four states that Tesla originally named to be finalists to house the "Gigafactory" -- Nevada, Arizona, New Mexico, and Texas -- are apparently still in the running. Additionally, Musk stated that "California was potentially back in the running," though based on his comments, investors should consider the Golden State a long shot for now. Nevada is widely viewed as the favorite, given its proximity to Tesla's auto manufacturing plant in Fremont, Calif. Further, there's rail transportation available, a large lithium factory in the state, and additional lithium reserves relatively nearby in Wyoming.

Musk added that Tesla plans to break ground on the second Gigafactory site a month or two after it breaks ground on the first -- so, a July-to-August time frame. As some may know, Musk recently announced that Tesla plans to break ground on "multiple sites" to minimize the risk of delays arising after groundbreaking. This costly precaution is needed, as the company must ensure that it has an adequate battery supply when it starts producing the more affordable "Generation III" sedan. This mass-market vehicle is expected to be available in 2017.

2. Panasonic has signed a letter of intent to be a supplier for the Gigafactory

There's been a lot of uncertainty as to whether Panasonic -- Tesla's current supplier of battery cells -- would commit to partnering with Tesla on the Gigafactory, given what many view to be the higher risk level of this massive project. Much of that uncertainty has been dispelled, as Musk stated on the call that Tesla has signed a letter of intent with Panasonic to be a supplier for the project. A final agreement is expected to be signed by the end of the year.

Musk expounded on the planned partnership to say that Panasonic would be the "only" company producing battery cells at the Gigafactory, but that Tesla would be buying cells above and beyond what Panasonic produces at the facility. He expects that most of these additional cells would come from Panasonic, but some would probably be purchased from other suppliers as well.

3. Battery cost savings from the Gigafactory might be higher than expected

Musk said on the call that he's "quite confident" that the Gigafactory will enable Tesla to meet its goal of reducing battery costs by 30% per kilowatt hour. More telling, Musk went further to say that he's "cautiously optimistic" that the company will be able to actually exceed this cost savings level.

While Musk didn't use any definitives, it seems he believes that Tesla will be able to generate some greater-than-anticipated cost savings from nickel and cobalt suppliers. The company is in talks with mining companies, notably large ones in Canada.

4. North American demand for the Model S grew 10% sequentially

I've concentrated my key takeaways on the Gigactory in this article because the ability of Tesla to be a long-term business and stock market success story hinges on its ability to produce a profitable Gen III vehicle, and that, in turn, depends upon the success of the Gigafactory.

However, I think it's important to highlight one piece of information pertaining to demand that Tesla included in its first-quarter letter to shareholders: North American demand for the Model S grew 10% sequentially in the quarter. There's been a fair amount of chatter that demand for the S has been drying up in North America, which this fact should help dispel. It's important to note that we're talking about demand here, not deliveries. Deliveries to North America were down in the quarter, but that's only because Tesla needed to meet European demand, as many Europeans have been kept waiting for their vehicles for quite some time.

Musk emphasized several times during the call -- and these are key points -- that demand and deliveries (talking in general, not just about North America) are two different things, and demand continues to outstrip production capacity. This is a good "problem" for any company to have, assuming it can continue to grow capacity to meet that demand, and Tesla remains on track on this front.