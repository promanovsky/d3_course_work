When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up forfor latest news plus tips to save money and the environment

Daily Express: Predator Rolf Harris faces jail after being found guilty of 12 sex attacks.

Daily Star: Rooneys' fury after Wayne and Coleen robbed on holiday.

The Daily Telegraph: Rolf Harris could die in jail after he was convicted of being a serial sex attacker.

The Times: Dramatic poll shows slump in supporters for Alex Salmond's campaign for Scottish independence.

The Independent: Rolf Harris found guilty on all charges as picture of decades of sexual abuse begins to emerge.

The Daily Mail: Rolf Harris was a predator who duped us all after sex attack convictions.

The Guardian: From national treasure to paedophile, Rolf Harris now faces jail for sex assaults.

The Sun: Rolf Harris and Jimmy Savile stalked Broadmoor together.

Daily Mirror: Rolf Harris unmasked as paedophile and facing years in prison.