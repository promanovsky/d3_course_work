Hunger Games: Catching Fire walked away with the most awards at last night's (April 13) MTV Movie Awards.

The second instalment of the science fiction adventure series bagged the main Movie of the Year prize, as well as trophies for Jennifer Lawrence as Best Female Performance and co-star Josh Hutcherson with Best Male Performance.



Hutcherson dedicated his award to the late Philip Seymour Hoffman, saying: "I know that if Philip were here, he would really think this is really cool.

"To have him in our movies was one of the coolest things in the world. He's one of the actors I've looked up to my entire life. We think about him every day on set. Wherever he is, this definitely goes out to him as well."

We're the Millers actor Will Poulter also did well at the event which was hosted in Los Angeles, securing the award for Breakthrough Performance and Best Kiss with Emma Roberts and Jennifer Aniston.

Meanwhile, The Wolf of Wall Street took home two awards for Best Comedic Performance by Jonah Hill and #WTF moment for Leonardo DiCaprio's scene where Jordan Belfort takes his Lamborghini for a spin.

Getty Images for MTV

WireImage



Zac Efron caused a bit of steam on collecting his award for Best Shirtless Performance in That Awkward Moment. The 26-year-old was presented the award by Rita Ora, who ripped the actor's shirt off.

Elsewhere, The Hobbit: The Desolation of Smaug was awarded Best Fight between Orlando Bloom and Evangeline Lilly and Orcs, while Mila Kunis was named Best Villain for her portrayal of witch Theodora in Oz The Great and Powerful.

Vin Diesel and Paul Walker were named Best On-Screen Duo for their collaboration in Fast & Furious 6. Co-star Jordana Brewster led a tribute to Walker, saying: "He will always be in our hearts".

Mark Wahlberg turned the air blue with his expletive-filled acceptance speech for the MTV Generation Award, saying: "This is the 'you're-too-f**king-old-to-come-back' award. This is not the 'Channing Tatum you're-dancing-around-chicks-want-you' award. This is the you're-f**king-done award."

Other winners included Brad Pitt, Jared Leto, Henry Cavill, and the Backstreet Boys, Jay Baruchel, Seth Rogen and Craig Robinson in This Is the End.

UK viewers can catch the MTV Movie Awards on MTV tonight at 9pm.

MTV Movie Awards 2014 - red carpet:

MTV Movie Awards 2014 - red carpet

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io