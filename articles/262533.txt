For the study, published Tuesday in the Journal of the American Medical Association , researchers recruited sedentary people ages 70 to 89 years who had trouble walking more than a quarter-mile. Half of them were randomly assigned to participate in a daily exercise program, and after nearly three years, they had an 18 percent lower risk of losing their walking abilities compared with the others, who were instructed to take health education classes.

While exercise has long been promoted as the elixir of youth, a large clinical trial conducted partly at Tufts University finally provides strong proof for the claim: It found that elderly people who walked and did basic strengthening exercises on a daily basis were less likely to become physically disabled compared to those who did not exercise regularly.

Advertisement

“These exercises were simple and applicable and can be implemented anywhere in any environment to have an important public health impact,” said study leader, Dr. Marco Pahor, director of the Institute on Aging at the University of Florida, Gainesville.

The people in the exercise group participated in fitness classes and home workouts. Still, despite working up to a 30-minute brisk walk over the course of the study, they were not able to sidestep disabilities completely. During the study, 30 percent of them had trouble walking and performing routine tasks, at least temporarily, compared with nearly 36 percent of those in the control group, the ones given health education. The disabilities persisted for nearly 15 percent of the exercise group, compared with nearly 20 percent of the control group.

One strength of the study, the authors noted, was that it enrolled typical seniors, with an assortment of chronic illnesses.

“These were people who began the study with health problems like high blood pressure, diabetes, and previous heart attacks and strokes,” said coauthor Roger Fielding, a senior scientist at the Jean Mayer USDA Human Nutrition Research Center on Aging at Tufts, “far from the healthier populations typically enrolled in clinical trials.”

Advertisement

Tufts recruited 203 of the 1,635 participants in the trial, which was conducted at eight centers around the country.

A growing number of seniors in the Boston area have been taking similar classes outside the clinical trial in a “Fit-4-Life” exercise program developed by the Tufts researchers. Daily classes offered at one of the sites, the Holland Street Center in Somerville, are filled, with about 80 people on a waiting list.

“Everyone exercises at their own pace, and the average age is about 71” said Chris Kowaleski, health and wellness coordinator at the Somerville Council on Aging, which coordinates the program. “We have some pretty fit people, but we also have others in their 90s who use walkers.”

The $10 monthly charge is reimbursed by Medicare, which covers up to $150 per year for fitness classes.

Before Mary Fuller, 66, signed up for the twice-weekly class last September, she had not done any exercise beyond walking around the building where she worked. Now retired, Fuller said she loves the “social atmosphere” of the classes, which inspired her to start walking with friends and to join a local volleyball team.

“The classes encouraged me to be more active on my own time,” she said. “My mood is better, I have more energy, and I sleep better at night.”

While Joe Beckmann, a 70-year-old from Somerville, said he enjoys the classes, they have not gotten him to exercise as much as he knows he should. He also had one other quibble: “There aren’t many men, which is a little sad.”

Advertisement

The researchers said the benefits of exercise in the study might have been muted by the fact that people in the control group also increased their activity levels somewhat, probably inspired by the health education classes.

The exercise group had a slightly higher rate of hospitalizations for health problems such as infections, heart palpitations, and chest pain, but the differences were small and could have been due to chance.

Dr. Evan Hadley, director of the division of geriatrics and clinical gerontology at the National Institute on Aging, called the study “very promising” given that 5 million people over the age of 75, or 25 percent of this population, have trouble walking a quarter mile.

The institute, which is part of the federal government, provided funding for the research, which was presented Tuesday at the American College of Sports Medicine annual meeting in Orlando.

While previous population studies indicated that elderly people who maintained their physical fitness were more likely to remain living independently through the decades, this is the first large clinical trial to provide firm evidence that when older Americans, who are typically sedentary and have numerous health conditions, moderately increase activity levels, they might be able to delay their decline into disability.

“It’s good news all around to see that a structured exercise program can translate into a very relevant clinical outcome,” said Dr. Eric Shiroma, a research fellow at Brigham and Women’s Hospital who was not involved in the study. “It would be interesting to see if those who engage in more physical activity will experience even greater health benefits over time.”

Advertisement

The researchers also collected data on changes in blood pressure, blood sugar, and cholesterol levels, which they plan to publish in the next several months, Fielding said.

But the evidence is strong enough now, he added, for public health officials to “identify strategies to make exercise a part of older people’s daily lives.”

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.