IRS employees who had been disciplined for tax and conduct issues were nonetheless rewarded with monetary awards or time off, according to a watchdog report released Tuesday.

The report by the Treasury Inspector General for Tax Administration found that while for the most part the reward program for IRS workers complied with federal regulations, employees who had themselves failed to pay their federal taxes and had discipline problems were also rewarded.

"While not prohibited, providing awards to employees who have been disciplined for failing to pay federal taxes appears to create a conflict with the IRS’s charge of ensuring the integrity of the system of tax administration,” Treasury Inspector General for Tax Administration J. Russell George said.

The watchdog found that in the period from October 1, 2010 to December 31, 2012 over 2,800 employees who had been disciplined for conduct problems, including issues with federal tax compliance, had received over $2.8 million in monetary awards and over 27,000 hours in time-off awards.

The watchdog found that the more than 1,100 employees who had issues with tax compliance received more than $1 million in monetary awards and more than 10,000 hours in time-off awards.

The IRS said in a statement it has already developed a new policy linking conduct and performance bonuses for executives and senior level employees.

"Even without a formal policy in place over the past four years, the IRS has not issued awards to any executives that were subject to a disciplinary action," the IRS said in a statement. "We are also considering a similar policy for the entire IRS workforce, which would be subject to negotiations with the National Treasury Employees Union."

The IRS had about 100,000 workers during the period under review.

In the 2011 budget year, more than 70,000 IRS workers got cash bonuses totaling $92 million, the report said. In the 2012 budget year, nearly 68,000 workers got cash bonuses totaling $86 million.

The report said the IRS considers prior conduct before awarding permanent pay increases. "However, IRS officials stated that the IRS generally does not consider conduct issues when administering other types of awards," the report said.

There are no government-wide policies on providing bonuses to employees with conduct issues, the report said. However, a 1998 law calls for removing IRS employees who are found to have intentionally committed certain acts of misconduct, including willful failure to pay federal taxes.

Tax compliance at the IRS is generally better than at other federal agencies.

In 2011, 3.2 percent of federal workers owed back taxes, according to IRS statistics. The Treasury Department, which includes the IRS, had the lowest delinquency rate, at 1.1 percent.

The delinquency rate for the general public was 8.2 percent.

The Associated Press contributed to this report.