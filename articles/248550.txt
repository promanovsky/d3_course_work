Suzuki Motor Corp. has recalled over 184,000 small cars in the U.S. because the headlight switch can catch on fire.

Forenza models from 2004 to 2008, as well as Reno models from 2005 to 2008, that were built from Sept. 1, 2002 through July 30, 2008 are covered in the recall, according to USA Today.

The malfunctioning headlight switch was provided by Korean supplier Woochang.

The recall is related to General Motors' recall of almost 218,000 of its 2004 to 2008 Chevrolet Aveos, which had the same issue, The New York Times reported.

The Suzuki and Chevrolet compact vehicles were both manufactured in Korea by Daewoo, currently known as GM Korea.

Suzuki said the headlamp switch, as well as the daytime running light modules, could overheat, melt and catch on fire on the left side of the steering column, ABC News reported.

The Japanese automaker will contact customers about the issue, and the problem will be fixed free of charge. Repairs for the vehicles have yet to be scheduled.

While Suzuki officials have been unable to comment on the recall, GM said previously that it knew about some fires. The company added that it has not been informed of any injuries or deaths related to the problem, Automotive News reported.

After American Suzuki Motor Corp. filed for Chapter 11 bankruptcy in November 2012, Suzuki left the light vehicle market in the U.S. last year. However, the company still sells ATVs, motorcycles and boat engines in the U.S. In March 2013, the bankruptcy plan was approved in court.

GM's latest recall was the 29th the company had this year. It brings its total number of recalls in the U.S. to almost 13.8 million, ABC News reported.

The National Highway Traffic Safety Administration posted documents on Friday saying that Suzuki will provide a plan for fixing the headlight switches. A schedule for contacting dealers and owners about repairs will also be given.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.