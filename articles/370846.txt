Police investigating the theft of medical records on Michael Schumacher have announced a major breakthrough.

Officers in France probing the incident in which the stolen records were offered for sale for around £40,000, confirmed they have tracked down the computer used to share the documents.

The documents appeared for sale after Schumacher was transferred to Lausanne hospital in Switzerland from Grenoble Hospital in France.

He was transported by helicopter and the operator of the craft has denied any involvement in the leak of the medical records.

BBC reported a spokesman for the firm said it had no knowledge of an investigation and had not been contacted about it.

The spokesman added the firm believed patient confidentiality had been reserved in Schumacher's case.

More to follow.