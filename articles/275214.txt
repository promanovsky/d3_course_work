Emily Blunt stars alongside Tom Cruise in ‘Edge of Tomorrow’, which has been the surprise critical hit of the year so far. But there was a time when Blunt wasn’t exactly itching to work with her co-star, as a helpful journalist decided to remind her in the middle of an interview.

Emily Blunt and Tom Cruise star in 'Edge of Tomorrow'

“A few years ago Emily Blunt said that she would rather do badly paid theatre for the rest of her life than ever accept a role ‘as a spear carrier in a Tom Cruise movie,’” Helena de Bertodano wrote, kicking off her interview piece with Blunt in The Telegraph.

More: Official: 'Maleficent' Takes $69.4 Million - But Can It Beat 'Edge of Tomorrow'?

“I never said that! What an awful thing to say,” replied Blunt, before de Bertodano wielded physical proof. “I pull out the newspaper clipping (from an interview in The Telegraph in 2005) and she roars with laughter,” the reporter wrote. “That is so funny,” said Blunt, “Well, at least I’m not a spear carrier.”

Perhaps slightly embrassed, Blunt went on to wax lyrical about her co-star who, if anything, is carrying the spear for Blunt in the sci-fi blockbuster. “That’s the wonderful thing about Tom: he doesn’t take himself that seriously and he is very self-effacing,” the 31-year-old said. “He couldn’t care less what people think about him – that’s quite refreshing.”

More: click for the 'Edge of Tomorrow' trailer

‘Edge of Tomorrow’ has been a smash with the critics so far, and is expected to do big things when it’s unleashed on the public on Friday (June 6) because of that unexpected acclaim. It sees Blunt hone Cruise’s skill as a solider that has to face the unenviable task of dying repeatedly in order to quell a disastrous alien invasion.