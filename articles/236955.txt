Benedict Cumberbatch and Parks and Recreation's Adam Scott are joining the cast of Johnny Depp's upcoming drama Black Mass.

Depp is set to play infamous mobster Whitey Bulger in a dramatisation of the Irish mafia's relationship with the FBI in Boston.

Clive Mason



Cumberbatch has been tapped to replace Guy Pearce as Whitey Bulger's brother Billy Bulger.

Scott has been cast as an FBI agent in director Scott Cooper's adaptation of the book Black Mass: The True Story of an Unholy Alliance Between the FBI and the Irish Mob, according to The Hollywood Reporter.

The actor currently plays Ben Wyatt in Parks and Recreation, and will also appear in the upcoming Hot Tub Time Machine 2.

Joel Edgerton, Dakota Johnson and Juno Temple have also boarded the movie, while Breaking Bad's Jesse Plemons has been reported to be negotiating for a Black Mass role.

Production on the movie is currently under way in Boston, with a release expected for next year.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io