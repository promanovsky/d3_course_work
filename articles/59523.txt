The average age of an ultramarathoner is 36, according to a new Stanford University study. – AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

BOSTON, MARCH 28 — New research to be presented at the upcoming American College of Cardiology’s 63rd Annual Scientific Session suggests training for a marathon may lower risk of heart disease.

Researchers used 45 recreational male runners planning to run the 2013 Boston Marathon for an 18-week training program. Participants were between the ages of 35 and 65 and subjected to a complete medical evaluation before the program began. Half of participants had previously run three or more marathons, with the other half two or less. Additionally, half of participants had “at least one cardiovascular risk factor,” such as high blood pressure and high cholesterol.

Training included a detailed training guide that featured tips about pacing and preparation, as well as group runs, endurance training, cross-training facility access in the Boston area, nutrition advice and regular communication with coaches. Participants ran 12 to 36 miles each week depending on where they were in their training. Researchers used participant-provided running logs to track how well they followed the program.

“We chose charity runners because we wanted to focus on the non-elite type of runner, just the average Joe who decides to get out there and train for a marathon,” said study lead investigator Jodi L. Zilinski, MD of the Massachusetts General Hospital. “They turned out to be a healthier population than we expected with a lot of them already exercising on a pretty regular basis, but they were still nowhere near the levels of elite runners.”

Participants were examined again following the program’s end. It was found participants experienced “significant overall changes” in terms of cardiovascular risks. This included a 5 per cent reduction in low-density lipoprotein, or “bad” cholesterol, a 4 per cent reduction in total cholesterol, and a 15 per cent reduction in triglycerides.

Other changes included a 1 per cent decrease in body mass index and a 4 per cent increase in peak oxygen consumption. The latter is a measurement of cardio-respiratory fitness.

“Overall, participants experienced cardiac remodelling — improvements in the size, shape, structure and function of the heart,” Zilinski said. “Even with a relatively healthy population that was not exercise naïve, our study participants still had overall improvements in key indices of heart health.”

Still, Zilinski notes it’s important for people to talk to a healthcare professional before engaging in a challenging physical fitness program.

A very small 2013 study from Université Laval, published in the Canadian Journal of Cardiology, found that recreational marathon running can damage the cardiac muscle, especially among less-fit runners or those who don’t train properly.

The new study’s researchers say that further research is required to determine the exact cardiovascular benefits of marathon preparation in women and novice runners. — AFP/Relaxnews