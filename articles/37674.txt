Disney has announced that they will purchase YouTube network, Maker Studios, the deal is worth $500 million initially, and could rise to as much as $950 million.

The deal includes a $450 million payment, which will be made if the company meets certain targets, this is the largest acquisition of a YouTube multichannel network to date.

Maker Studios channels have hundreds of millions of subscribers and get over 5.5 billion video views per month, it has previously raised around $66 million in financing.

“Short-form online video is growing at an astonishing pace and with Maker Studios, Disney will now be at the center of this dynamic industry with an unmatched combination of advanced technology and programming expertise and capabilities,” said Disney CEO Robert Iger in the acquisition release.

The company was recently valued at around $300 million in its latest round of financing, so the Disney deal is at a premium on the company’s previous value.

Source TechCrunch

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more