DETROIT (AP) – Nine million parts.

That’s what General Motors needs to repair millions of cars it has recalled since Feb. 7. With ignition switches, power steering motors and other parts slowly arriving at dealers, frustrated drivers face waits of weeks or months, some while driving cars they fear are unsafe.

Any recall can present challenges for automakers and customers. Still, most recalls include less than 50,000 vehicles and are typically completed in two or three months.

But experts say eight simultaneous recalls covering 7 million vehicles is too much for any organization to handle quickly, even one as big as GM. Suppliers have to make the parts — millions aren’t sitting in stock. GM has to notify customers, ship the parts to dealers worldwide and train mechanics how to do repairs.

GM says it will take six months to make and distribute all the parts for the largest recall: 2.6 million small cars with faulty ignition switches that the company links to 13 deaths. The switches, mainly in older Chevrolet Cobalts and Saturn Ions, can slip out of the “run” position into “accessory,” shutting off engines and disabling power-assisted steering and air bags. GM has told dealers to offer concerned owners a loaner car while they wait for parts. Those cars also need to have a second part replaced.

There’s no estimate yet on when the other recalls will be finished.

Owners of all car brands might watch the mail for more notices. GM rival Toyota, which itself recently ordered recalls of millions of vehicles, expects automakers to be more proactive in bringing cars in for repairs.

At least initially, the GM ignition switch recall didn’t go smoothly.

“This is a big ol’ hot mess,” said Blair Parker, a Houston-area attorney who owns a 2005 Chevrolet Cobalt included in the switch recall. Her dealer can’t tell her exactly when parts will arrive.

A few months ago, Parker’s car engine shut off unexpectedly when she hit the keys with her hand, an incident she had chalked up to user error. Now she worries the Cobalt’s switch is defective, and is driving a loaner car.

“We just decided it wasn’t worth the risk,” she said.

After the switch recall, GM conducted a review that turned up 4 million more vehicles with problems, including faulty power steering motors, transmission oil leaks, defective drive shafts and air bag troubles. About 500,000 of them only need a fitting to be tightened and don’t need parts.

All told, the recalls present a Herculean task for GM. Multiple suppliers are involved, and parts need to go to more than 4,300 dealers.

Dave Closs, chairman of the Supply Chain Management Department at Michigan State University, says GM dealers will have frustrated customers on their hands for a while.

Parts makers have to find factory space and workers to ramp up assembly lines. GM said Delphi Automotive PLC has one line working seven days per week to make ignition switches and it’s setting up two more.

Finished parts must then be inspected for quality. After that comes shipping, a costly and slow process, Closs says.

“You’re shipping relatively small shipments all over the world,” he says.

Toyota is in a similar situation. Last month it announced recalls totaling 6.4 million vehicles to fix defective seats and bad air bag wiring.

Bob Carter, Toyota’s U.S. automotive operations chief, says car owners can expect more frequent recalls because the regulatory and competitive environments have changed. Instead of recalling cars for known defects, companies are now “recalling vehicles to change problems that we anticipate might happen,” Carter says.

GM is under fire because it knew about the problem with the ignition switches for 10 years before conducting the recall. Two congressional committees, the Justice Department and federal safety regulators are investigating GM’s slow response, and criminal charges are possible. GM has hired lawyer Kenneth Feinberg to negotiate settlements with surviving families and some injured drivers.

So far, the company says it isn’t going to use its 2009 bankruptcy as a shield from wrongful death and injury claims. However, it is seeking bankruptcy court protection from claims that its small cars lost value.

Wendi Kunkel’s 2010 Chevy Cobalt is part of the switch recall. Her dealer told her to pull everything off her keychain, which GM contends will stop the switches from turning off unexpectedly. But she’s nervous about her 30-minute one-way commute near Dallas.

“I’m on a highway where I’m going 65 mph,” the public relations representative says. “If my car were to switch into accessory or off, the likelihood of me crashing and not having air bags deployed — it’s pretty terrifying to think about.”

Kunkel says her dealer, Lakeside Chevrolet in Rockwall, Texas, didn’t initially offer her a loaner, although she plans to ask for one.

Frank Pecora, Lakeside’s service manager, said the dealership doesn’t offer loaners unless customers express concern for their safety, per instructions from GM.

So far, GM has put 45,000 customers in loaners, equal to 1.7 percent of the cars recalled for the ignition switches.

GM isn’t offering loaners to car owners affected by the other recalls. Duane Paddock, owner of a Chevrolet dealer near Buffalo, New York, says he’s offering loaners on his own dime to keep customers happy.

Recalls are often profitable for dealers such as Paddock because GM provides the parts and pays for installation. Also, owners of older cars travel to dealerships for repairs and see the company’s new vehicles while they wait.

Dealers say small numbers of parts began arriving late in the week of April 7, and the pace has picked up. GM says it has shipped thousands of ignition switches and notified 1.4 million owners to set up repair appointments.

Jerry Seiner, chairman of a Salt Lake City-area dealership group, says his technicians have replaced 23 ignition switches and lock mechanisms for owners who contacted them before April 1. After more publicity about the problem in April, more than 150 people now are waiting for parts. Seiner says parts are arriving three to four weeks after they’re ordered.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: