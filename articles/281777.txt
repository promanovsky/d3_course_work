Gay men who use smartphone apps to hunt for a sex partner nearby run a higher risk of contracting sexually transmitted disease than those meeting online or in bars, researchers said Thursday.

Applications such as Grindr, Scruff and Recon use the smartphone's geolocation facility to help the user hook up with other men in the vicinity who are also looking for sex.

Researchers in Los Angeles, California, carried out a survey among nearly 7,200 homosexual and bisexual men who visited a sexual health centre between 2011 and 2013.

The volunteers were screened for sexually-transmitted infections (STI) and questioned about their use of drugs and social networking in sexual encounters.

Thirty-four percent of the men met sexual partners in person only, while 30 percent used a combination of person-to-person encounters and online dating.

By comparison, 36 percent used smartphone apps, either alone or in conjunction with other methods.

Men who used the smartphone apps were 23 percent likelier to be infected with gonorrhoea and 35 percent likelier to have chlamydia than those who met their partners online or in clubs and bars.

But there was no difference between the three groups in the risk for the AIDS virus or syphilis.

App users tended to be under 40, well-educated, of white or Asian ethnic backgrounds and likelier to use cocaine and ecstasy as "recreational" drugs, the investigation found.

The study, published in the British journal Sexually Transmitted Infections, acknowledged that patterns of sexual behaviour among gay men in southern California may not be the same elsewhere.

But, they said, geolocation apps have given rise to the possibility of instantly available and anonymous sex, and with it a higher risk of infection.

Previous research had found that app users were less likely to use condoms during sex and more likely to have multiple partners, they noted.

"Technology is redefining sex on demand," according to the authors, led by Matthew Beymer of the LA Gay and Lesbian Center.

Health watchdogs "must learn how to effectively exploit the same technology, and keep pace with changing contemporary risk factors for STI and HIV transmission."

Grindr reported in 2013 that it had six million users in 192 countries, with 2.5 million added in 2012.