US CHEMIST Alexander Shulgin, known as the "Godfather of ecstasy" for turning an obscure chemical into a widely-used party drug, has died aged 88, his family says.

"Sasha died (on Monday) ... he was surrounded by family and caretakers and Buddhist meditation music, and his going was graceful, with almost no struggle at all," his widow Ann said on Facebook in a statement posted early on Tuesday.

The octogenarian, who lived in California and studied chemistry at Harvard and Berkeley, suffered from liver cancer. Shulgin developed an interest in the swinging 1960s in so-called psychoactive chemicals, testing hundreds of them including anti-depressants, aphrodisiacs and stimulants on himself and his friends.

Early in his career Shulgin worked at Dow Chemical company, where after successfully synthesising Zectran, the first biodegradable insecticide, he was given free reign to work on chemicals of his choice.

He began experimenting with psychedelics, creating an amphetamine called DOM, which was second only to LSD in potency.

In the 1970s he began working with the amphetamine MDMA, which later became the rave or nightclub drug of choice, Ecstasy, or E.

It allowed clubbers to dance for hours, although there have been cases of sudden death.

The stimulant had already been synthesised at the end of the 19th century and patented in 1912 by pharmaceutical firm Merck, before being abandoned.

Shulgin created a new way of synthesising it, working with a psychologist, Leo Zeff, who used it himself and recommended it to colleagues to treat patients.

The chemist, who organised psychedelic drug sessions with friends, wrote several books on his experiments including TiHkal (Tryptamines I Have Known And Loved) and PiHKAL (Phenethylamines I Have Known And Loved).

Vice magazine's Hamilton Morris spoke to Shulgin, in what would be his last interview, in 2010, visiting the chemist and his wife Ann at their San Francisco home.

"Pretty much every psychedelic drug known came from this house,” he wrote.

"Although Alexander Shulgin is not exactly a household name, he is unquestionably the most important psychedelic chemist who has ever lived.

"Those who do know of him are usually only familiar with his role in the rediscovery and popularization of MDMA. But MDMA is just one of 100-plus unique chemicals that compose Shulgin’s pharmacopeia, which extends so far into the unknown that he often has to invent new terms to describe the effects.

Prof David Nutt, professor of neuropsychopharmacology at Imperial College London, told the BBC there would have been fewer deaths among young people using recreational drugs if people had "listened and learned from Shulgin rather than tried to suppress his knowledge and ideas".