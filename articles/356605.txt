Rescue workers in Honduras said last night they were close to freeing three of the 11 miners trapped by a cave-in in a town in the south of the country yesterday.

Eight of those who were trapped in the abandoned gold mine in El Corpus remain missing and it is unclear whether they are still alive.

“The situation is complex, it is difficult because the tunnels are narrow, they branch off in a number of different directions”, senior emergency services official Moises Alvarado said.

The mine partially collapsed after 22 miners entered on Wednesday to search for gold, however half were able to get out on their own.

Rains in the area are a growing threat to the rescue efforts and may result in the search being suspended, authorities said.

“That area is all limestone, which is sensitive to rain and could cause landslides, threatening the safety of the people involved in the rescue”, a spokesman for the fire services explained.

Gerardo Flores, the Honduras manager of Mayan Gold, a US miner based nearby that was involved in rescue efforts, told local television that without food, water and with little air, the trapped miners could expect to live just three or four days.

The collapse occurred in a drought-striken region of Honduras that had been a gold mining centre for Spanish colonists.

The mine, 100km south of the capital Tegucigalpa, had been ordered to close a few months ago because it was unsafe, officials said.

The mayor of the town said there are more than 50 mines in the area, some of them as much as 200m underground.

Alvardo said many of the mines did not meet the most basic safety requirements and added that there were no maps of the underground structures, making the rescue operation harder.

Hundreds of people including family members and neighbours had gathered near the entrance to the mine, but military troops cleared the area due to concerns of further landslides.

Mining accidents are common in southern Honduras, especially in poverty-striken areas.