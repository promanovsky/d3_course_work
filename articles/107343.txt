Change is afoot at CBS - with David Letterman stepping down as host of The Late Show at 11.30pm, satirist Stephen Colbert is stepping up from 2015.

But is the network planning a broader overhaul of its late-night line-up? That's what the latest rumours suggest, with reports claiming that host Craig Ferguson could be dropped from 12.30am's modestly-rating Late, Late Show.

CBS via Getty Images



His potential replacement? The latest buzz points to Chelsea Handler - soon to depart her E! series Chelsea Lately.

If true, it's admirable that CBS is contemplating adding a female host into the US talk-show sphere - a field that, with the notable exception of Arsenio Hall, has been dominated by white men for time immemorial.

But regardless, axing Ferguson at this crucial juncture would be a mistake on the network's part. It's true, audience numbers aren't spectacular - NBC besting CBS in the ratings war is a rare occurrence, but Ferguson's competitor Seth Meyers has been giving The Late, Late Show a drubbing each and every week.

Sonja Flemming



Retaining Ferguson would be more about retaining credibility for CBS - a network best known for delivering family-friendly produce and reheated procedural formats. Its current programming slate is successful but bland, lacking in edge - and edge is exactly what Ferguson, with his small but passionate fanbase, delivers.

Perhaps it's because the show airs in the early hours, perhaps because it's long felt like the network's unloved second child, but there's a feeling of 'anything goes' on The Late, Late Show - you get the impression that Ferguson is given pretty much free reign and is having an absolute ball doing whatever the hell he likes.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

No talk show reflects its host's personality quite like The Late, Late Show. Since taking over from Craig Kilborn in 2005, Ferguson has used the series as a vehicle to indulge his passions - chief among them a love for British sci-fi series Doctor Who - and he's such a warm, passionate screen presence that his enthusiasm is infectious.

Matt Smith, Karen Gillan and Jenna Coleman have all played the mouth organ or shared an awkward silence with Craig - and surely CBS wouldn't deny us the pleasure of seeing Ferguson reunited with fellow punk rocker Peter Capaldi now that his old friend has landed his dream role?

Lisette M. Azar



There's a wonderfully ramshackle feel to The Late, Late Show - it lacks the slick feel of many US talk shows and is all the better for it. An outsider to Hollywood, the Scottish-American comedian has been quietly but confidently changing the game, showing all other US talk show hosts how it should be done for almost a decade.

If CBS really wants to shake things up, the right thing to do is leave Craig Ferguson precisely where he is.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io