Pacific Organic Produce is voluntarily recalling a limited number of cases of organic Tommy Atkins mangos due to a possible risk from contamination with Listeria monocytogenes bacteria.

Sold under the Purity Organic brand between the dates of April 14, 2014 and May 2, 2014, the affected products bear the PLU numbers 94051 & 94959, and were shipped to retailers and distributors in limited quantities within Arizona, California, Colorado, New Jersey and Texas.

No other mangos or products under the Purity Organic brand are being recalled.

Listeria monocytogenes is an organism that can cause foodborne illness in a person who consumes a food item contaminated with it. Symptoms of infection may include fever, muscle aches and gastrointestinal symptoms such as nausea or diarrhea.

For comments and feedback contact: editorial@rttnews.com