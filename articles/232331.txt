Jolie Lee

USA TODAY Network

Levi Strauss CEO Chip Bergh takes water conservation seriously. Bergh said he hasn't washed his jeans in a year.

"These have yet to see a washing machine," Bergh said at a Fortune Brainstorm Green conference Tuesday, the same day as the company's 141th anniversary.

"I have yet to get a skin disease or anything else," he said in a video of the talk posted by Fortune.

Bergh said half of the water consumption for a pair of jeans occurs when the jeans are made, and the other half from washing the jeans after they're purchased by consumers.

But, according to "real denim aficionados," you're not supposed to wash your blue jeans at all, Bergh said.

In his talk, the CEO discussed the company's sustainability efforts, including working with cotton farmers to develop more sustainable cotton. He also mentioned Levi's Waterless jeans line that use up to 96% less water by using an ozone process for the jeans' finish.

Follow @JolieLeeDC on Twitter