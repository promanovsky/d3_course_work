Lean over the fence to Mr. Feeny, gather Cory, Shawn and the gang in the kitchen, and get a helmet, cause life is tough once again! The first trailer for Girl Meets World, the long-awaited Boy Meets World spinoff is finally here and is chock-full of heart-warming moments sure to induce extreme ’90s nostalgia.

Premiering on the Disney channel this summer, Girl Meets World stars Ben Savage, 33, and Danielle Fishel, 32, as their original lovebird characters of Cory and Topanga, respectively. The childhood sweethearts have grown up from their days of dates at Chubbie’s and serious romance in college, and are now married adults with children of their own.

Cory and Topanga Matthews’ daughter, Riley, the star of the show, is played by 12-year-old actress Rowan Blanchard, who appeared in the 2010 Jennifer Lopez movie The Back-Up Plan.

In the first look at Girl Meets World, Riley and her friend try to sneak out the window of the Matthews’ house, only to be thwarted by Riley’s Dad, Cory. Riley questions how long she will have to live in her father’s world, before Cory tells her, “Until you make it yours.” Topanga enters the clip, promising Riley that she and Cory will always be there for their daughter.

This is Fishel and Savage’s first official onscreen reunion since Boy Meets World wrapped in 2000. Other fan favorites are expected to return to their old roots as well, including resident nerd Minkus (played by Lee Norris), Cory’s parents Amy and Alan Matthews (played by Betsy Randle and William Russ), bad boy BFF Shawn Hunter (played by Rider Strong), and the wise neighbor and teacher Mr. Feeny (played by William Daniels).

Watch the first trailer for Girl Meets World above and keep an eye out for an official premiere date in the coming weeks.