US consumer products giant Procter & Gamble today unveiled plans to eliminate dozens of under perfomring brands as it reported a big increase in quarterly earnings.

P&G, which makes Pantene shampoo, Tide detergent and other mainstays, plans to cull 90-100 lower-selling brands over the next 12-24 months as it aims to further build up its best-sellers.

"Less will be much more," said chief executive A G Lafley.

"We want to be in the businesses we should be in, not the business we are in," Lafley said on a conference call.

P&G did not disclose the brands that are targeted for discontinuation or divestment. The company will keep its 70-80 top brands that account for about 90 per cent of sales and 95 per cent of profit.

Lafley highlighted the recently launched close-shaving Gilette "Flexball" razor and an upcoming "revolutionary" Crest strip product for sensitive teeth as examples of products that can command a premium.

"We're bringing renewed focus to brands," Lafley said, adding that the goal is to build "lasting practice and loyalty" among consumers.

The announcement came as P&G reported USD 2.6 billion in profits for its fiscal fourth quarter ending June 30, a 38 percent increase compared with a year ago. Part of the improvement came from a seven percent cut in expenses to USD 6.3 billion.

P&G chief financial officer Jon Moeller said results were hit by softening currencies in some of its biggest markets, such as Japan, Venezuela and the Ukraine.

P&G's "core earnings" per share, which strips out currency effects and the impact of restructuring charges, were 95 cents for the quarter, four cents above analyst expectations.

Revenues slipped 0.7 per cent to USD 20.16 billion, below the USD 20.48 billion projected by analysts.

For the full year, P&G reported net income of USD 11.6 billion, up three per cent from the prior year. Annual sales were USD 83.06 billion, up 0.6 per cent.

P&G shares were the best performing in the 30-company Dow Jones Industrial Average, rising 4 per cent to USD 80.41 at mid-morning.