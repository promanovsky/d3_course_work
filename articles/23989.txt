Too many patients are getting unnecessary brain scans for headaches, and that use is growing, according to a new study.

Since most headaches are benign, guidelines have recommended against routine MRIs or CT scans. Yet doctors frequently order these expensive scans anyway, the researchers say.

"During headache visits, brain scans are ordered an incredible amount of the time," said lead researcher Dr. Brian Callaghan, an assistant professor of neurology at the University of Michigan Health System, in Ann Arbor.

"There are a lot of MRIs and a lot of CTs and that adds up to a lot of money," he pointed out. "It's about $1 billion a year."

Brain scans for headaches jumped from about 5 percent of patient visits in 1995 to nearly 15 percent in 2010, the researchers found.

While brain scans are good for some patients, they are unnecessary for most, Callaghan noted. "Most people with headaches don't need any testing," he said.

Among the reasons these tests are not recommended is that they can often find some abnormality, which although benign, could lead to further unnecessary tests and treatment, he explained.

"In 1 to 3 percent of people you will find something on the MRI, whether it be a tumor or blood vessel malformation. You don't want to find something you weren't looking for. It can be anxiety provoking," Callaghan said.

Brain scans can cost as much as $4,000. Insurance, including Medicare, only pays from $500 to $2,000, Callaghan said.

For the study, Callaghan's team used data from a national survey on outpatient medical care to look at all adult patients seen for headache from 2007 through 2010. Most patients were younger than 65 and about three-quarters were women.

The study found there were a total of roughly 51 million patient visits for headache, including about 25 million for migraines. MRIs or CTs were performed in more than 12 percent of all visits for headache and in nearly 10 percent of visits for migraine.

The researchers estimated the cost of all these scans in the four-year period at $3.9 billion.

"The number-one reason physicians give is patient reassurance. It's harder to talk a patient out of it than to just get the scan," Callaghan said. "But I would argue that patient reassurance isn't worth $1 billion a year."

In addition, doctors order scans out of fear they might miss a serious condition and be sued for misdiagnosis, he noted.

The profit motive also comes into play, Callaghan added. "Facilities make a lot of money off of these tests," he said.

Patients also have a role, Callaghan said. "Imaging for headaches is on the list of tests patients should question," he said.

The report was published online March 17 as a research letter in JAMA Internal Medicine.

Guidelines from the American Academy of Family Physicians, American Academy of Neurology and the American College of Emergency Physicians recommend against routine brain scans for headache (in the absence of other indications).

The study could not verify whether scans were ordered according to guidelines or not, but showed that they occurred in the context of a headache-related visit to a doctor.

"Though this study shows levels of imaging that are undoubtedly excessive and costs that almost certainly exceed the benefits, it does not measure or discuss the benefits of brain imaging in patients with headache," said Dr. Richard Lipton, vice chairman of neurology and director of the Montefiore Headache Center at the Albert Einstein College of Medicine, in New York City.

These benefits include finding a brain tumor, stroke, brain abscess and bleeding into the brain, according to Lipton, who was not involved with the new study. Reassuring worried patients that they do not have a serious or life-threatening condition is important, he said.

In addition, scans reduce the doctor's concern that they might have missed a serious condition, as well as concerns about the liability that may arise from a failure to diagnose.

"The reality is that most headaches are benign, but distinguishing high-risk and low-risk presentations is not that easy," Lipton said. "I agree that optimizing headache brain imaging practices should be a priority."

More information

For more about headache, visit the U.S. National Library of Medicine.