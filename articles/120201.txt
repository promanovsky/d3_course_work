A woman has spoken of her "miraculous" recovery from the deadly Ebola virus that is sweeping west Africa, and which has so far claimed more than 100 victims.

The 27-year-old, who gave her name as Fanta, told reporters at a treatment centre outside Guinean capital Conakry that she had spent weeks battling the virus.

"To hear that I had a disease that could not be cured, I was so afraid... It is as if I have just been reborn," she told the Daily Mail.

"The way people were looking at me, I knew I had this really dangerous illness but I'm fine now, thank God. I'm cured."

At least seven other patients in Guinea have shaken off the virus, said the medical charity Medecins San Frontieres (MSF), amid an outbreak which has been called "the most challenging ever" by the World Health Organisation.

Overall, two-thirds of those infected have died, with 101 recorded deaths in Guinea, and 10 in Liberia.

There is no known cure or even effective course of treatment for the virus, which first emerged near the Congo's Ebola river in 1974.

The recoveries in Guinea have given medics battling the illness a sliver of hope, with many in the camp calling Fanta's recovery a "miracle".

"She was lucky, she was very strong. We have had patients who were cured after a hard fight for their bodies to recover, even when it didn't look like they were going to make it," said MSF nurse Catherine Jouvince.

The WHO said it was providing specialist training to 70 staff who would fan out across Conakry to track people who had been in close contact with those infected with Ebola. The agency is also setting up a special emergency response centre in Guinea's health ministry and providing specialist training to staff in the country's main hospital and in medical centres.

Ebola is passed on through infected bodily fluids such as blood, sweat and urine, and previous outbreaks have killed 90% of those infected.

It causes weakness, fever, aches, diarrhoea, vomiting and stomach pain. Additional symptoms include rash, red eyes, chest pain, throat soreness, difficulty breathing or swallowing and bleeding (including internal).