Good news for fans of 2014’s surprise hit, possibility of a Flappy Bird return exists, this from developer Dong Nguyen who killed the game over a month ago. The game instantly became a global sensation, despite the fact that it has basic graphics and it happens to be increasingly frustrating to play. Nevertheless, people all around the world were taken to it, and some were actually sad to see the game go. In a feature interview with Rolling Stone, Flappy Bird developer Dong Nguyen says that he is “considering” bringing back the infamous title.

Advertising

When he initially pulled the game, it wasn’t entirely clear why he had done that to a title reportedly netting him $50,000 per day in ad revenues. Some thought he may have been threatened by the possibility of legal action by Nintendo, but Nguyen came out saying that the game’s addictive quality made him pull it. He tells Rolling Stone that if Flappy Bird is to flap its way back onto our devices, a warning will be attached to it telling players to take a break.

He doesn’t go beyond that, so unfortunately there’s no release date to mark our calendars with. Even though he’s considering, that doesn’t mean he’s definitely going to bring it back, but if he does, you’d no longer have to bear with the sheer amount of Flappy Bird clones out there.

Filed in . Read more about Flappy Bird.