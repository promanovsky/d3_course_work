Shutterstock photo

Investing.com -

Investing.com - The euro trimmed earlier gains against the dollar on Thursday after upbeat regional factory data and jobless claims numbers out of the U.S. sparked demand for the greenback.

In U.S. trading, EUR/USD was up 0.12% at 1.3832, up from a session low of 1.3814 and off a high of 1.3864.

The pair was likely to find support at 1.3791, Tuesday's low, and resistance at 1.3905, Friday's high.

The dollar narrowed earlier losses stemming from Federal Reserve Chair Janet Yellen's dovish speech on Wednesday and rose on cheery U.S. data.

The Federal Reserve Bank of Philadelphia reported earlier that its manufacturing index rose to 16.6 in April, the highest level since September, from 9.0 in March. Analysts had expected the index to tick up to 10.

Separately, the Labor Department reported that the number of individuals filing for initial jobless benefits in the week ending April 12 rose by 2,000 to 304,000, better than analysts' forecasts for a rise to 315,000.

On Wednesday, Fed Chair Janet Yellen said that monetary policy will need to remain accommodative for some time, citing slackness in the labor market and low inflation, which weakened the dollar, though Thursday's data gave the greenback some support.

Meanwhile in Europe, Germany's producer price index contracted 0.3% in March from a month earlier and fell 0.9% on year.

Analysts were expecting a 0.1% monthly increase and a 0.7% on-year decline.

The numbers chipped away at the euro's gains over the dollar, as a day earlier, data revealed that the annual inflation rate slowed to 0.5% in March from 0.7% the previous month, soft but in line with expectations

Core inflation, which strips out volatile items like food and energy costs, fell to 0.7% from 1.0% in February, missing expectations for a 0.8% reading.

Euro zone inflation has now been in the European Central Bank's danger zone of below 1% for six straight months, fueling speculation that policymakers will need to implement fresh stimulus measures to shore up the fragile recovery in the euro area.

The euro was up against the pound, with EUR/GBP up 0.04% to 0.8229, and up against the yen, with EUR/JPY up 0.10% at 141.40.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.