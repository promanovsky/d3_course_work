Former President Donald Trump's impeachment team on Friday claimed Democrats are the ones guilty of violent rhetoric, showing clips of prominent politicians — and of Madonna.

Throughout their presentation in his impeachment trial, Trump's defense team played videos and read quotes from Democrats they argued have engaged in violent and inflammatory rhetoric, as Trump faces charges of inciting violence among his supporters. The highlights included President Biden saying he'd "beat the hell" out of Trump if they were in high school and Rep. Ayanna Pressley (D-Mass.) saying there "needs to be unrest in the streets."

But also thrown in were comments from some non-politicians like Madonna, who is seen in one clip saying she's "thought an awful lot about blowing up the White House" following Trump's election. Another comment from Johnny Depp asking "when was the last time an actor assassinated a president" was included, as well.

"I am not showing you this video as some excuse for Mr. Trump's speech," attorney Michael van der Veen said before playing one of the star-studded montages. "This is not whataboutism. I am showing you this to make the point that all political speech must be protected."

Van der Veen argued that Trump "did not engage in any language of incitement" but that "there are numerous officials in Washington who have indeed used profoundly reckless, dangerous, and inflammatory rhetoric in recent years." He also made the case that Trump's speech was protected by the First Amendment.

The clip of Madonna came from a 2017 Women's March, although she immediately followed up her comment about blowing up the White House by adding, "I know that this won't change anything." She later said the remarks were taken out of context and insisted "I do not promote violence." Brendan Morrow