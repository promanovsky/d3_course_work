Netflix, Inc. (NFLX) said it plans to significantly expand in Europe later this year, offering a wide-range of for a low monthly price in Germany, Austria, Switzerland, France, Belgium and Luxembourg.

When launched, broadband users in these countries can subscribe to Netflix and instantly watch a curated selection of Hollywood, local and global TV series and movies, on TVs, tablets, phones, game consoles and computers.

Netflix said that since launching its streaming service in 2007, it has put consumers in charge of when and how they enjoy their entertainment.

Members can play, pause and resume watching series and films across devices, without commercials or commitments. Using the unique Netflix recommendation engine, each member is presented a personalized list of titles to enjoy from Netflix.

The continued European expansion follows the launch of Netflix in the UK and Ireland, Denmark, Finland, Norway, Sweden in 2012 and the Netherlands in 2013.

People interested in becoming Netflix members in France, Germany, Austria, Switzerland, Belgium and Luxembourg can go to www.netflix.com today and sign up to receive an e-mail alert when Netflix is launched.

For comments and feedback contact: editorial@rttnews.com

Business News