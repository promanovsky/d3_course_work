In the Battle for Fast Food Chain Dominance, Taco Bell made a bold swipe at go-to breakfast choice McDonald's by releasing its own breakfast menu this week. And just to make sure that everyone understood — We're Taking On McDonald's — Taco Bell released commercials driving home the message:

Not to be outdone, McDonald's answered with a pretty clever tweet:

Imitation is the sincerest form of flattery. pic.twitter.com/e0dFN1ZCqy — McDonald's (@McDonalds) March 29, 2014

But Taco Bell President Brian Niccol apparently doesn't think promoting a new line of waffle tacos, A.M. Crunchwraps, and other choices while responding to a main competitor is enough on his plate. No, he gave Canada an ultimatum when he took to Reddit on Thursday in an AMA.

"Only in America? When is this coming to Canada?" a poster asked.

"When you take Justin Bieber back," Niccol shot back.

McDonald's, you may have just found a new ally. Sarah Eberspacher