World-renowned Grammy Award winning rapper 50 Cent will release his fifth studio album, Animal Ambition, today, June 3, 2014.

A specialdeluxe version of Animal Ambition featuring a lenticularalbum cover and music video DVD will be available exclusively at Best Buy starting June 3rd as well. On top of the 11 songs featured on the album, the deluxe version will include bonus tracks: "You Know," "Funeral" and "Flip On You" ft. Schoolboy Q.

"I was so ready to share my new album with my fans that I began releasing music from Animal Ambition in March. I am very happy that the full body of work will be available in its entirety for my fans to enjoy on June 3 rd," 50 Cent says.

On March 18 th the first two songs and videos from Animal Ambition were released and each week following an additional song and video have become available continuing through the album's release date.

In February 2014, 50 Cent transitioned to independent status with his music being released worldwide through G Unit Records, Inc. and distributed by Caroline/Capitol/UMG. Animal Ambition is 50 Cent's first album since this transition.

This morning 50 Cent performed on "Good Morning America" as part of their summer concert series to showcase Animal Ambition.

Currently, 50 Cent is gearing up to complete his next studio album, the highly anticipated Street King Immortal, which will be released later this year.

To purchase Animal Ambition please visit: http://smarturl.it/50_iTunes

To purchase the deluxe version of Animal Ambition please visit: http://smarturl.it/50_AA_BBY

For more information on 50 Cent, please visit: www.50cent.com

For more information on 50 Cent's music releases, please visit: www.iTunes.com

About 50 Cent:

50 Cent, born Curtis James Jackson III, is an American rapper, entrepreneur, investor and actor from Queens, New York. He rose to fame with the release of his 2003 albumGet Rich or Die Tryin', which has sold more than 8 million copies. The Grammy Award winner has sold more than 30 million albums worldwide and has been awarded numerous accolades including 13 Billboard Music Awards, six World Music Awards, three American Music Awards and four BET Awards, to name a few. He was ranked as the #1rap artist and most successful Hot 100 Artist of the 2000s by Billboard Magazine, as well as that decade's sixth-best artist and fourth top male artists of that decade.50 Cent's fifth studio album,Animal Ambition, features collaborations withTrey Songz, Yo Gotti, Jadakiss, Styles Pand more.On March 18, 2014, the first two songs and videos from Animal Ambition were released and each week following an additional song and video have become available continuing through the album's release date of June 3,2014. A specialdeluxe version ofAnimal Ambition featuring a lenticularalbum cover will be available exclusively at Best Buy on June 3rdas well. The highly anticipated Street King Immortal album will follow in fall 2014.

What began as music chart dominance for the rap music phenomenon quickly transformed to success in corporate America as a multi-tiered business mogul to be reckoned with. 50 Cent has also carved out a successful and acclaimed career as an actor, appearing in more than 21 films including "Frozen Ground" with Nicolas Cage and John Cusack and "The Tomb" starring Sylvester Stallone and Arnold Schwarzenegger. He recently wrapped filming on "The Prince" with Bruce Willis and John Cusack and has begun filming "Spy" with Melissa McCarthy and Jude Law. He is currently serving as executive producer and has a reoccurring role on the highly anticipated series "Power" set to air on Starz on June 7, 2014. Recognized as one of the most talented and prolific music artists of his time, 50 Cent has managed to leverage his star power into record breaking brand extensions encompassing a broad spectrum of businesses including artist management, film production, footwear and apparel, fragrance, video games, publishing, headphones and health drinks and supplements.

SOURCE 50 Cent