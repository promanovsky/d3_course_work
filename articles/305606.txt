The big market mover today, of course, was the 2PM Federal Open Market Committee press release followed by Janet Yellen's press conference. Prior to that main event, the traded in a narrow range around yesterday's close. As I type this, the Fed's verbiage continues to be dissected by the financial press, but essentially nothing changed. And Yellen's second press conference as Fed Chair avoided any surprises (unlike her first one, which included a remark that rates could start rising "around six months" after the Fed stops buying bonds). The index responded with a rally that took it to new intraday and closing highs. The official gain for the day was 0.77%. The index is up 5.88% so far this year.

Within 30 minutes of the Fed press release, a Business Insider news item summarized today's news accordingly: "It Looks Like The Fed Has No Idea What's About To Happen".

The yield on the 10-year note ended the day at 2.61%, down 5 bps from yesterday's close. It is now 17 bps above its interim closing low of May 28th.

Here is a chart of the last five sessions.

SPX

Here is a daily snapshot of the S&P 500. Today's volume was the strongest of the four-day rally, but it remains below its 50-day moving average.

SPX

For a longer-term perspective, here is a pair of charts based on daily closes starting with the all-time high prior to the Great Recession.

Current market snapshot