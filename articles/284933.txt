Matt Lauer has been on the Today show for 20 years and decided he’d like to stay a little longer. He has re-signed with NBC to keep him on the show for years to come.

Matt Lauer may be one of the most consistent faces at the Today show, and he agreed to keep that face on the air for a little while longer.

The host agreed to a contract extension with NBC to keep him on the Today show, which he’s been on since 1994. The Today show has been floundering in recent years, losing out to Good Morning America on ratings. Lauer’s own popularity suffered two years ago when Ann Curry left the show. But he quickly bounced back, and NBC was said to be happy to have him re-sign, according to the Associated Press.

“Matt really isn’t the problem with the show,” Shelley Ross, a former executive producer for both ABC and CBS, told the AP. “Matt still, I think, is one of the all-time anchor greats. I clearly think the Today show is rattled. It’s not the confident show it once was.”

According to Nielsen, Good Morning America is still on top, but Today is gaining on the ABC morning show. Over the last year, GMA‘s audience grew 8 percent, with an average of 5.7 million people watching each morning. Today grew 9 percent and is now at 5.1 million, and CBS This Morning grew 11 percent and is now at 3.1 million.

NBC wouldn’t say exactly how long Lauer’s contract was extended but did say it was for “multiple years.” Neither would NBC disclose his salary, although it was previously reported at $25 million, according to Reuters.

“We couldn’t be more thrilled with Matt’s decision,” NBC News President Deborah Turness said. “As I’ve said many times before, he’s the best in the business, and there is nobody I would rather have in the Today anchor chair than Matt.”

“I consider this the best job in broadcasting,” Lauer said. “I love the people I work with every day, and I have such respect and gratitude for the people I work for. I couldn’t be happier to be staying.”