Celebrity

The 'Girls Gone Wild' founder allegedly assaulted an employee at his former office and violated restraining order that banned him from coming near the building.

May 19, 2014

AceShowbiz - Joe Francis is facing another legal trouble. After being jailed last year and sentenced to 3 years of probation for being convicted of misdemeanor counts of assault and false imprisonment of women, the "Girls Gone Wild" founder is now taken into custody again for a new assault charge.

The 41-year-old soft-porn mogul was accused of assaulting an employee at his former office. He was reportedly "pushing and shoving" the employee when he was forcing his way into the building on Wilshire Boulevard, an allegation that he vehemently denied.

Besides facing assault charge, Francis could be charged with violation of the restraining order because Federal Bankruptcy Court banned him from coming within 100 feet of the building. He was held in LAPD Valley Jail in Van Nuys on Friday before being released the next day on $20,000 bail.

In his own defense, Francis told TMZ that the company's new owner had a "business vendetta" against him. According to him, he had every right to be in the building because the restraining order had expired. He also claimed the security guard "manhandled" his pregnant wife.

Francis lost his office after filing for Chapter 11 bankruptcy last year due to his multi-million dollar debt. His previous assault charge stemmed from a dispute with three women back in 2011. He was accused of holding them against their will and attacking one of them. Besides the jail and probation, he was ordered to complete anger management course and psychological counseling.