Apple might be planning to improve its iTunes Radio service, as it reportedly close to acquiring the Swell Radio app. Swell is a news and podcast streaming service meant for commuters that creates a customised playlist based on user's podcast listening history.

The Cupertino firm is said to be close to acquiring Swell for $30 million. Swell's technology is expected to improve the iTunes Radio's podcast recommendations and add to Apple's recent introduction of ESPN and NPR radio stations along with 40 other local stations in the iTunes Radio service.

Recode reports that the Swell Radio app will shut down this week and the whole team including CEO Ram Ramkumar will move to Apple. Swell previously sold its SnapTell image-recognition technology to Amazon.

However, the acquisition is not yet confirmed as both the firms have not commented anything on the matter.

Apple appears to be trying to improve its news and podcast recommendations if the reported discussions about a Swell Radio acquisition are correct. The company was recently said to be looking to revamp its iBooks recommendation service as well, with the reported acquisition of BookLamp, an e-book discovery service for around $10-$15 million last week.

Talking about acquisitions, the European Union's antitrust authority cleared Apple's $3 billion deal to buy Beats Electronics on Monday that makes headphones and offers music streaming services.

The takeover of Beats Electronics and Beats Music announced in May for $2.6 billion in cash and $400 million in stock is the most expensive acquisition in Apple's 38-year history.

The move to buy Beats, founded by rapper Dr. Dre and Jimmy Iovine, was widely seen as a bid to counter the increasing threat posed by music streaming services like Pandora and Spotify to Apple Inc.'s iTunes store.