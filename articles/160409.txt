The latest earnings report from Apple has exceed expectations, with the company reporting an increase in revenue for the first three months of the year of $45.6 billion – way ahead of analysts’ predicted $43.5bn.

However, the report also reinforced the fact that Apple relies more than ever on the success of the iPhone for continuing growth.

The smartphone accounts for more than half of the company’s revenues and while sales of the device soared past expectations (43.7 million units sold compared to projections of 37.7m), this success was necessary to offset a slump in interest in the iPod, Macs and the iPad.

Apple’s tablet launched four years ago and although the device has been hailed as the ideal midpoint between a smartphone and a computer, these recent numbers suggest that this position might not be so sustainable, with sales of the iPad falling by 16 per cent to 16.4 million units.

CEO Tim Cook stressed that the device still has great potential from businesses and corporations, but for most consumers the range of apps and the ubiquity of mobile data has meant that smartphones can perform everything that tablets might.

As Quartz’s Christopher Mims notes there are also a whole host of smaller factors that contribute to the iPhone’s dominance, including their shorter lifespan (phones are more likely to get stolen or broken, while mobile networks offer ‘free’ upgrades to keep you getting new ones) and the fact that the main use cases for the tablet (as a convenient TV screen or an email browser in bed) are just as easily as accomplished by a cheaper devices than one of Apple’s £400 machines.

Luckily for Apple, strong iPhone sales also benefit the company via the halo effect, with each new addition to the iPhone family bringing in benefits from connected services like iTunes. Mr Cook stressed the continuing importance of the digital content service to Apple which expanded its sales by 11 per cent to $4.6bn this quarter.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The question is how much longer can Apple keep this up? Market saturation is the main difficulty facing the company, and although there seems to be room for growth in China and Japan (the two fastest growing operating segments) the company still needs to reassure investors - as seen with the promise to return $130 billion in cash to its shareholders at the end of next year.