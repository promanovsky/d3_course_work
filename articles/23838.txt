Flickr/4nitsirk

Dark chocolate has been linked to health benefits ranging from reduced blood pressure to improvements in mood.

Until now, researchers never knew how the chocolate produced some of these benefits in our bodies.

You wouldn't think that America's favorite treat would be good for you, but a recent study now shows that many of the merits may be traced to the influence of cocoa on the good bacteria in our body.

"We knew that there were some very good compounds that were healthy in cocoa," study researcher John Finley, from Louisiana State University, said at the American Chemical Society annual meeting on Tuesday, March 18.

But these healthy compounds "were not there at levels high enough to account for the effects that people had observed," he said.

Finley and undergraduate student Maria Moore decided to look at how the non-digestible fibrous part of cocoa impacts our bodies. This part makes it all the way to the colon relatively intact.

Armed with the idea that "if there's anything that can be used at all, the bacteria in your lower GI tract will figure out how to do it," Finley and Moore used a model digestive system to see what happens once the fiber reaches the colon.

Interestingly, the fiber was fermented into short chain fatty acids — "the favorite food for our colon cells," Finley said. The good bacteria feed on these compounds, causing higher cell turnover and a healthier colon.

"It probably causes more resistance to colon cancer and inflammatory bowel diseases and things like that," Finley said.

While the new study offers an interesting take on why dark chocolate might have health benefits, the results should be considered preliminary until they are published in a journal and confirmed by additional research.

The possible resistance to colon cancer and IBD was not the only benefit they found, though. Cocoa powder is also high in compounds called "polyphenolic polymers" that are too big to be useful during the early stages of digestion. When they hit the colon and our gut bacteria have at them, they begin to break down into anti-inflammatory compounds.

These molecules are small enough to be absorbed by our intestines and could "serve to delay the onset of some forms of cardiovascular disease that are associated with inflammation," Finley said.

"When we eat different foods, different bacteria will rise and fall as the predominant species," he said. "In our study we found that the fiber is fermented and the large polyphenolic polymers are metabolized to smaller molecules, which are more easily absorbed. These smaller polymers exhibit anti-inflammatory activity," he said in the press release.

These compounds — fiber and polyphenols — are found in cocoa powder and in chocolates containing high percentages of cocoa. So, feel free to sprinkle some cocoa on your daily oatmeal, but maybe skip the Kit Kat bar.