Allergan Inc. (AGN), which is famous for its wrinkle treatment drug Botox, is preparing to reject the $46 billion cash and stock offer by Canadian drug maker Valeant Pharmaceuticals International Inc. (VRX,VRX.TO). Allergan could publicly reject the offer as early as Monday, Financial Times reported citing people familiar with the matter.

The company is expected to raise concerns that Valeant would slash research and development as a reason for refusing the offer. Allergan is also likely to hit back at Valeant for using stock to make up over two thirds of its total offer, the report said quoting those people.

A rejection would be a blow to Valeant and Bill Ackman, the activist investor who owns a 9.7 per cent stake in Allergan and teamed up with the drugmaker to launch the takeover attempt. However, both have signalled their intention to pursue Allergan through tougher means if needed, the report indicated.

In April, Valeant announced that it submitted a merger proposal to acquire Allergan for $48.30 in cash and 0.83 Shares of Valeant stock for each Allergan share. The offer valued Allergan at around $46 billion. Following the proposed merger, Allergan shareholders will own 43 percent of the combined company.

For comments and feedback contact: editorial@rttnews.com

Business News