Mali said on Tuesday it was clear of the Ebola epidemic suspected to be behind around 130 deaths this year in Guinea and Liberia.

The health minister told reporters samples taken from patients displaying symptoms compatible with Ebola infection had tested negative for the deadly tropical pathogen.

"All ten samples gave a negative result. This means that, as of today, Mali has no suspect or confirmed cases of Ebola haemorrhagic fever," Ousmane Kone told a news conference in Bamako.

"Nevertheless, we have recommended that our authorities remain vigilant and continue to monitor the situation with the same rigour."

The outbreak in Guinea is one of the most deadly in history, with 168 cases "clinically compatible" with Ebola virus disease reported, including 108 deaths, since the start of the year, according to the World Health Organisation.

Factfile on the deadly Ebola virus, currently affecting West Africa where at least 113 people have died Adrian Leung/John Saeki, AFP/File

The outbreak began in the impoverished country's southern forests, but has spread to Conakry, a sprawling port city on the Atlantic coast and home to two million people, and 71 cases have been confirmed in laboratories as Ebola.

Neighbouring Liberia has reported 20 probable or suspected cases, six lab-confirmed cases and 13 deaths.

But a variety of deadly, highly-contagious tropical bugs, including the Marburg virus and Lassa fever, can lead to similar symptoms -- vomiting, diarrhoea and profuse internal and external bleeding.

Mali said it was keeping a number of patients in hospital, without elaborating on which other viruses may have made them ill.

Bamako announced two weeks ago that it had placed three suspected Ebola patients in isolation while the latest WHO update said there were six possible cases in the west African nation of 15 million people.