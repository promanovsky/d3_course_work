But this version of events is highly unlikely.

A story has recently been making rounds that Ai Hin, a giant panda at China's Chengdu Research Base of Giant Panda Breeding , may have faked a pregnancy as a cunning way to receive better treatment.

Xinhua, China's state news agency, reported that what was supposed to be "the world's first live broadcast of the birth of panda cubs" was called off after it turned out Ai Hin actually had a "phantom pregnancy," a regular occurrence among giant pandas. But the article goes on to suggest that this latest panda pregnancy fake out may have been drawn out after Ai Hin noticed the special treatment that came with the perception that she was expecting.

Advertisement

"After showing prenatal signs, the 'mothers-to-be' are moved into single rooms with air conditioning and around-the-clock care," Wu Kongju, of the Chengdu panda center, told Xinhua. "They also receive more buns, fruits and bamboo, so some clever pandas have used this to their advantage to improve their quality of life."

While it's impossible to know what a panda is thinking, "phantom pregnancies" - which at least occasionally occur in humans, cats, dogs, and other creatures - are very common in giant pandas, and they're not a manipulative trick. The panda's hormones routinely mimic an actual pregnancy when none exists. In other words, a panda may be acting like she is pregnant because she feels like she is.

Advertisement

"Female giant pandas spontaneously ovulate and undergo a phenomenon known as pseudopregnancy if not pregnant, wherein a female's reproductive hormones are similar in concentration and length... as during pregnancy," researchers wrote in the journal PLOS ONE in 2011.

Because of this biologically convincing false pregnancy, the diagnostic tests that can determine pregnancy in many other animals are basically useless in giant pandas. "For decades," the authors write, "researchers all over the world have been searching for a 'magic bullet' that would provide a pregnancy test for giant pandas and other exotic wildlife that undergo pseudopregnancy."

Advertisement

Tian Tian, a female giant panda at the Edinburgh Zoo, is expected to give birth any day now, but Iain Valentine, Director of Giant Pandas for the Royal Zoological Society of Scotland, cautioned that "we will not know 100% if Tian Tian is pregnant until she gives birth."

It seems possible that a panda might pick up on her special treatment and continue a charade of pregnancy, but given the routine nature of pseudopregnancies in the panda world, that is not the likeliest scenario.

Giant pandas can't help the fact that they "act" pregnant when they are not. Australia's Adelaide Zoo has reported that its female giant panda, Funi, experiences a phantom pregnancy once a year. "She becomes lethargic and spends lots of time in her den sleeping," the zoo notes, in a video. "She often shreds bamboo to make nests, loses her appetite, and even cradles objects just like she would with a cub."

Advertisement

Kongju, the main source of the claim about Ai Hin, is identified as an "expert" in the Xinhua report, but his exact role at Chengdu is not clear. One article calls him the "director of Animal Management," another calls him a "senior vet," and he has reportedly been involved in the care of pregnant pandas. But Chengdu's website identifies Kongju as a "research assistant" whose highest level of education is a bachelor's degree. He is listed as part of the conservation biology, not reproductive biology, group.

We've reached out to the Chengdu Research Base of Giant Panda Breeding for clarification on Kongju's role and the claim about Ai Hin, and we'll update the story if we hear back.

Advertisement

But most likely, Ai Hin was convinced that she was pregnant - right along with everyone else.