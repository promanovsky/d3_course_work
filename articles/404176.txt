London - Drew Barrymore has paid tribute to her estranged older half-sister, who was found dead on Tuesday.

The 39-year-old actress offered her sympathies to her extended family after Jessica Barrymore, whom she only met "briefly," was found dead in her car in National City, California from a suspected suicide.

In a statement to PEOPLE.com, the '50 First Dates' star said: "Although I only met her briefly, I wish her and her loved ones as much peace as possible and I'm so incredibly sorry for their loss."

Numerous white pills were reportedly scattered on the passenger seat of Jessica's car, but her cause of death has yet to be determined.

Jessica, 47, was the daughter of the late actor John Drew Barrymore - who was also the father of the 'Charlie's Angels' actress - and his third wife Nina Wayne.