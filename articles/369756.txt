Celebrity

The 'Never Say Never' crooner recently posted a funny video of the 'Cast Away'' actor, who wore a yarmulke and a white scarf, dancing on the stage at the wedding of Justin's manager.

Jul 8, 2014

AceShowbiz - Justin Bieber shared on July 7 a video of Tom Hanks, who danced to 1990s hit song "This Is How We Do It" at the wedding of the "Baby" singer's manager, Scooter Braun. Justin captioned the Instagram video with, "Haha Tom Hanks singing 'This Is How we do it' dressed like a Rabbi lol #thatdancetho."

In the video, Tom wore a yarmulke and a white scarf which was similar to a prayer shawl while dancing to Montell Jordan's 1995 song. He ended his performance by putting his hands up, mimicking a preacher and then leaving the stage as the crowd got wild. The hilarious video of Tom has been liked by more than 430,000 Instagram users.

Justin shared some pictures from the wedding on Instagram. One of them is a picture of him and Ed Sheeran, who also attended the wedding party. Justin captioned the photo with, "@edsheeran is one of the only people on my iPod." The "Beauty and the Beat" singer also took a photo with the bride, Yael Cohen, writing along with the picture, "Congrats Yael ;) u looked like a princess last night. And damn I can't believe Scooter found someone that puts up with his bs ;) love you guys."

Scooter tied the knot with the cancer activist in Canada over the weekend. The Jewish style wedding was also attended by other famous stars, including Tom's wife Rita Wilson, Carly Rae Jepsen and Sophia Bush.