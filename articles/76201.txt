Roughly 1 of 3 U.S. children kids screened for cholesterol are high. UPI/Ron Sachs/Pool | License Photo

WASHINGTON, March 31 (UPI) -- Roughly 1 of 3 U.S. children screened for high cholesterol between the ages of 9 and 11 are borderline or has high cholesterol.

Dr. Thomas Seery, a pediatric cardiologist at Texas Children's Hospital and assistant professor of pediatrics at Baylor College of Medicine, and colleagues examined the medical records of 12,712 children who had screening for cholesterol levels as part of a routine physical exam within the Texas Children's Pediatrics Associates clinics.

Advertisement

Thirty percent had borderline or elevated total cholesterol as defined by the National Cholesterol Education Program.

"The sheer number of kids with abnormal lipid profiles provides further evidence that this is a population that needs attention and could potentially benefit from treatment," Seery said in a statement. "But we can only intervene if we diagnose the proble m."

Cardiovascular disease in children is rare, but the presence of certain risk factors in childhood can increase the chances of developing heart disease as an adult. Previous studies have demonstrated atherosclerosis -- a hardening and narrowing of the arteries -- can begin in childhood.

The researchers also found boys were more likely than girls to have elevated total cholesterol, low-density lipoprotein, or the "bad" cholesterol, and triglycerides, while girls had lower high-density lipoprotein, or the "good" cholesterol. Obese children were more likely to have elevated total cholesterol, LDL and triglycerides, with lower HDL in comparison to non-obese children.

The findings were presented at the American College of Cardiology's 63rd annual scientific session in Washington.

[American College of Cardiology]