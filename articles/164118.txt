Tesla received support from three FTC regulators for its direct to consumer sales model amid a series of court-issued bans in different states.

Tesla Motors, American automaker that manufactures luxury and sports electric cars, has earned some positive response from three top officials of the U.S. Federal Trade Commission. The support from federal regulators is a good sign for the automaker that has constantly been fighting to overturn the ban on its unique direct sales model. Andy Gavil of the policy planning office, Debbie Feinstein of the bureau of competition, and Marty Gaynor of the economics bureau argued that the electric car maker be allowed to sell vehicles directly to customers.

Many states, including New Jersey, Texas, Arizona, Virginia and Maryland have passed laws barring Tesla from initiating direct-online sales of its premium car line without involving an established dealer. But in a post on the FTC's official blog, the three officials in favor of Tesla's direct sale policy said that prohibiting direct online sales for Tesla is a "bad policy" and hurts customers and the chance for new businesses to succeed.

"Such change can sometimes be difficult for established competitors that are used to operating in a particular way, but consumers can benefit from change that also challenges longstanding competitors," FTC regulators said in a press release, Thursday. "Regulators should differentiate between regulations that truly protect consumers and those that protect the regulated. We hope lawmakers will recognize efforts by auto dealers and others to bar new sources of competition for what they are-expressions of a lack of confidence in the competitive process that can only make consumers worse off."

Officials also noted that Tesla sold just 22,000 out of 15 million cars sold in the U.S. last year, which poses hardly a serious threat to established dealers in the country.

State laws protect local dealers from abusive practices by manufacturers and restricting direct car sales was to protect dealers from unfair business competition. Tesla completely relies on online sales of its cars in the U.S. and has no franchised dealers, Bloomberg reports.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.