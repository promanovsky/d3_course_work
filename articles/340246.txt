Samsung's strategy to dig deeper into the number of iPhone 5 sales was made clear when the Korean tech firm revealed it was preparing to inject four more handsets into its Galaxy line of smartphones.

Samsung announced the full-featured, more affordable smartphones that would join the Galaxy S5: the Galaxy Core II, Galaxy Star 2, Galaxy Ace 4 and Galaxy Young 2.

There was no release date or preorder information included in the June 30 announcement of the four new phones, but the device specifications should be intriguing to individuals who have felt the S5 was a tad pricey.

Of the four smartphones announced, the Galaxy Core II and its quad-core, 1.2-GHz processor was the only handset that rivaled the power of the Galaxy S5 and its 2.5-GHZ, quad-core processor.

The Galaxy Core II delivers on affordability by stepping down the RAM -- it has 768 MB of RAM, while the S5 has 2 GB. Also of note was the Galaxy Core II's 5-megapixel, rear-facing camera, though it employed only a VGA camera for selfies and chats, making it equivalent to 0.3 megapixels.

The Galaxy Ace 4 was equipped with a 1.2-GHz, dual-core processor in its LTE form, while the 3G variant of the phone had a 1-GHz, dual-core processor. The Galaxy Ace 4 was on par with the Galaxy Core II's front- and rear-facing cameras, though it beat out its stable-mate by delivering 1 GB of RAM.

Both the Galaxy Young 2 and Galaxy Star 2 will likely serve as the two most affordable entries into the Galaxy line. Both of the phones have been equipped with 512 MB of RAM and they both lack a front-facing camera.

The Galaxy Young 2 has a 3-megapixel rear-facing camera, while the Galaxy Star's camera is 2-megapixels.

All four of Samsung's new smartphones are powered by Android 4.4 (KitKat), have 4 GB of internal storage and accept microSD cards -- the Galaxy Core II and Galaxy Ace 4 could accept microSD cards of up to 64 GB in size, while the Galaxy Star 2 and Galaxy Young 2 restricted cards larger than 32 GB.



While Samsung hasn't mentioned plans to facilitate an upgrade to the inbound Android "L" OS for the newly announced handset, the Galaxy Core II and Galaxy Ace 4 look to have no hardware restrictions that would prevent an upgrade -- both phones are fitted with the 512 RAM required by the KitKat OS and they both have multicore processors.

It's likely the hardware requirements won't change between KitKat and L. But the single core processors of the Galaxy Young 2 and Galaxy Star 2 could hinder the two handsets in the long run.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.