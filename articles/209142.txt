Moto E has reportedly made an appearance on the Motorola Mexico Facebook page of all places.

The image seen above, was first spotted by Android Police, shows the handset's White colour variant pictured alongside a Moto G (left), giving us a glimpse of how the front panel of Moto E would look like.

The smartphone is seen sporting two long and slim grills at top and bottom, with light sensors and Motorola icon placed at the left side of the upper grill. While the exact screen size of the handset is not known, visibly both the Moto G (Review | Pictures) and the Moto E smartphones seem to sport similar screen sizes. Since the image shows no front-facing camera on the Moto E, it's safe to assume it will fall under the Rs. 10,000 price bracket. It's worth pointing out that leaked image has now been removed from Facebook, though not before Android Police snapped up copy seen above.

On Friday, Flipkart, which is the exclusive retailer for Moto G and Moto X (Review | Pictures) smartphones in India, rolled-out a teaser for a Motorola smartphone, in all likelihood the Moto E, on its official website and via Twitter.

Flipkart's tweet said, "Time to upgrade? @Motorola's latest smartphone is coming soon, stay tuned!" along with a sketched image.

The Indian e-commerce website, on its dedicated Moto E page shows the same image that states "Goodbye old phone. Hello new Moto. Coming Soon only on Flipkart.com." The page also states the handset to arrive in next four days that is by May 14.

The image, showing the sketched version of Moto E, looks similar to the image spotted on the Motorola Mexico Facebook page.

Moto E is expected to launch at simultaneous launch events in Delhi and London on Tuesday.

Moto E was earlier spotted at Zauba, an import/export data collection website. The listing, besides mentioning the name Motorola Moto E with model number XT1022, stated the handset to include 4GB of inbuilt storage, as well as per unit price at Rs. 6,330. Note, these import/ export prices are only indicative and usually do not include duties or taxes. However, we are expecting a sub-Rs. 10,000 price for the device, as mentioned above.

Earlier leaks had pointed that Motorola's next smartphone will come in three variants: XT1021 (single SIM), XT1022 (dual-SIM) and XT1025 (dual SIM with digital TV). The alleged Moto E is expected to sport a 4.3-inch display, a 1.2GHz dual-core Snapdragon processor paired with 1GB of RAM, 5-megapixel rear camera, and a 1900mAh battery capacity.

Affiliate links may be automatically generated - see our ethics statement for details.