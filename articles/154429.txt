The 2014 MTV Video Music Awards (VMAs) will take place at the recently-re-opened 'Fabulous' Forum in Inglewood, California.

The show returns to the West Coast on Sunday, August 24, becoming the first major awards ceremony to take place at the venue.



"I am thrilled to welcome the MTV Video Music Awards to the Forum in Inglewood," said city mayor James T Butts.

"Since its reopening in January, the 'Fabulous' Forum has become the destination for music-lovers in Southern California.

"A truly historic location, and home to some of the greatest concerts and sporting events, the Forum is back, better than ever and ready to shine for decades more."

President of MTV Stephen Friedman added: "With its incomparable legacy as a WORLD class music venue, the Forum is one of Southern California's most iconic entertainment landmarks.

"The Forum has hosted some of the most legendary musical artists and performances ever seen and we can't wait to add the 2014 Video Music Awards to this list and create the type of memorable moments that music fans will talk about for years to come."

First opening in 1967, the venue was relaunched in January 2014 after renovation work and has since seen performances from The Eagles, Justin Timberlake, Imagine Dragons, Paul Simon & Sting and Kings of Leon.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io