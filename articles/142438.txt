So there's a bunny, who lays eggs, which kids are sometimes sent out to find, or are given in a basket to eat. But what, many ask, does that have to do with the Christian holiday of Easter? Some of those holiday traditions can be traced back to northern Europe.

Many European words for "Easter" actually find their roots in the Hebrew word "pasah," a clear connection to the Jewish Passover, according to linguistic and religious sources.

But the English word "Easter," and the German version "Ostern," derive from "Ostara" the name of a Germanic goddess, which in pagan traditions represented springtime, the morning, and the East.

A New York Times piece from 1899 notes that for a faith looking to "give Christian significance to all such observances as could not be thoroughly rooted out," it wasn't too far a leap to "prove a connection between joy at the arising of the natural sun... with the rising of Christ from the grave."

Those springtime festivals also featured symbols of fertility, which carried over to Christian traditions surrounding Easter. Hence the bunnies.

"The bunny thing is spring and fertility," said Lizette Larson-Miller, a professor with the Graduate Theological Union in Berkeley. "Bunnies are known for their fertility so I suppose someone long ago put that connection together."

The eggs too are also considered natural fertility symbols, but Christians were also able to find in them something else: Symbols of future life, and, in a way, resurrection.

Eggs don't just have a Christian significance. The New York Times piece also references the fact that Jews use eggs in Passover, "And even the Persians presented each other with colored eggs at their festival of the solar new year in March."

In the Eastern Christian tradition, a popular Easter game involves cracking dyed, hard-boiled eggs together.

"The cracking of the eggs is the cracking open of the tomb," said Larson-Miller. "So the Easter eggs represent in some ways the tomb of Christ in the garden."

The easter egg hunt, seems to have multiple origins. The New York Times 1899 article notes that in "Scotland, it used to be the custom on Easter Sunday for the young people to go out early in the morning to search for the eggs of various wild fowls for breakfast, it being though most lucky to find them."

The German church reformer Martin Luther, Larson-Miller said, also appeared to encourage the tradition.

"We know that Martin Luther had Easter egg hunts where the men hid the eggs for the women and children went and it probably has this connection back to this idea of eggs being the tomb."

And what about the idea of bunnies laying those eggs?

"That might not be biologically correct," Larson-Miller said, laughing. "But somebody made that connection, yeah."