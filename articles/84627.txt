Google Inc. is reportedly launching Android TV soon. This is to attract users to set foot back to their living rooms.

The "Android TV is an entertainment interface, not a computing platform. It's all about finding and enjoying content with the least amount of friction," stated Google in a document obtained by The Verge.

To make it more fun and enjoyable, the Internet giant is trying to make it as simple and as understandable as possible. It even called on to developers to create simple applications for the set-top-box interface, which will display mini movie posters of movies, TV shows, applications, and games. It will also support voice input and notifications.

To choose what to watch, users will just have to scroll up and down -- through different categories of content, -- or sideways -- to choose from Google's suggestions -- in the remote control. The remote control also has Home, Enter, and Back buttons, and game controllers, which are optional.

Google aims to make Android TV "simple and magical." It should never exhaust users from clicking just to enjoy watching a movie or a TV show, or playing games; make spontaneous recommendations of things that users may be interested; and resume the movie they are watching from their smartphones or tablets as soon as they switch on the Android TV things.

"Android TV is Android, optimized for the living room consumption experience on a TV screen," stated the company in the document. Unnecessary features like cameras and touch screen support are removed to keep the developers' focus on creating simple apps. The company has also provided the developers ready-made interfaces so it will be easier for them to just insert movies, TV shows, music, games and photos.

The development of Android TV implies that Google still wants people to spend more time at home and more time with family rather than looking or doing stuff with their gadgets alone in their room.

Google declined to comment on the matter.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.