The family of Justina Pelletier, the 15yo girl at the centre of a legal battle is devastated but defiant after a Massachusetts judge gave permanent custody of the girl to the state's Department of Children and Families. Courtesy Fox News

The family of Justina Pelletier, the 15yo girl at the centre of a legal battle is devastated but defiant after a Massachusetts judge gave permanent custody of the girl to the ...

WHEN Justina Pelletier’s parents noticed their daughter was slurring her words and had trouble walking, they did what any worried couple would do, they got advice from a doctor and called an ambulance.

What happened next is a parent’s worst nightmare, but most people would have trouble even dreaming up the scenario the Pelletier family found themselves in, that has ultimately seen them lose custody of their daughter.

Suffolk County Court Judge Joseph Johnston ruled this week that 15-year-old Justina Pelletier must remain in the hands of the Massachusetts Department of Children and Families until she is 18, unless her parents can prove they are capable of taking care of her, ABC News reported.

Justina has been under the psychiatric care of Boston Children’s Hospital for the past 13 months and her parents Lou and Linda Pelletier have been restricted to once-a-month visits under state supervision.

The story of how she came to be there is bizarre.

Justina had been a patient of Dr. Mark Korson at Tufts Medical Centre, where she was being treated for mitochondrial disease, a rare genetic disorder with physical symptoms that can affect every part of the body.

But one day in February 2013 during a heavy snowstorm, Justina’s condition took a turn for the worse and she was rushed to Boston Children’s Hospital for treatment. Because she arrived by ambulance, she was taken directly into the hospital’s emergency room.

According to her parents, Justina was almost immediately whisked off to a psychologist who diagnosed her with somatoform disorder — a mental condition in which a patient experiences symptoms that are real but have no physical or biological explanation.

The hospital then sought and was granted a court order that allowed it to continue treating the teen for the disorder, during which time she was temporarily placed in state care.

Since then, Justina has been confined to a wheelchair, and Lou Pelletier has alleged her medical symptoms are not being treated and her condition has deteriorated.

Yesterday, Judge Johnston ruled that Justina did suffer from somatoform disorder and not mitochondrial disease as her parents believe.

He handed down a four-page ruling blasting the family for verbally abusing hospital caregivers by calling them “Nazis” and accusing them of “kidnapping” and “killing” their daughter.

The judge also alleged that the social worker assigned to Justina’s case had to be reassigned because Mr Pelletier threatened her. He said the family has stood in the way of every attempt to get Justina treatment.

“Efforts by hospital clinicians to work with the parents were futile and never went anywhere,” Judge Jonston said in the ruling.

“Unfortunately, there has been not any progress by the parents.”

Mr Pelletier, who filed an appeal in the custody case last December, told ABCNews.com that he is now ready to file a writ of habeas corpus in Massachusetts Supreme Court for “wrongful imprisonment” of his daughter.

“Eventually, we are working on federal lawsuits to do with civil rights violations,” he said.

“We are just trying to determine which state to file them in. Obviously, as much as we are devastated, this is the corrupt, crooked court system. This shows the judge is a coward.”

Justina’s family has set up a Facebook page called A Miracle For Justina “to get her home”