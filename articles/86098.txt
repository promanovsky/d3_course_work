Web giant Yahoo is not known for online video, but it's looking to change that.

The company is close to ordering four different original series that would take the form of half-hour comedies, sources familiar with Yahoo's plans told The Wall Street Journal.

The move would pit Yahoo against services such as Amazon and Netflix, which have already made splashes with original series such as "House of Cards" and "Alpha House." According to the WSJ, per-episode budgets for Yahoo's series would range from $700,000 to $1 million. A potential problem for Yahoo will be courting advertisers into its online video space to help finance the shows and make the jump into the crowded field a worthwhile investment.

Yahoo's acquisition of original programming would be the latest in a series of moves by CEO Marissa Mayer to have a stronger focus on video. In November, Mayer hired Katie Couric away from ABC News to be the company's "global anchor." In March, reports swirled that Yahoo was in talks to buy News Distribution Network, an online video service, for upwards of $300 million.