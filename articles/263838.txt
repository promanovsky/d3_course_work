The Eurozone symbol in front of the European Central Bank (ECB) headquarters in Hamburg, Germany. According to the ECB, loans for the private sector in the EU are still contracting. ― Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

FRANKFURT, May 28 ― Loans to the private sector in the euro area ― a gauge of economic health ― are still contracting, but at a slower rate than before, European Central Bank data showed today.

The volume of loans to private businesses and households declined by 1.8 per cent in April, after falling by 2.2 per cent the previous month, the ECB said in a statement.

The data appear to confirm a tentative recovery slowly emerging in credit demand in the 18 countries which share the euro, where the long and deep financial crisis caused credit to dry up almost completely.

At the same time, the overall Eurozone money supply grew by just 0.8 per cent in April, down from growth of 1.0 per cent in March.

This could prove a headache for the ECB, which regards M3 money supply as a barometer for future inflation, since area-wide inflation is currently much lower than the ECB would wish. ― AFP