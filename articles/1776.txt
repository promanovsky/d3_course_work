Jason Cipriani/CNET

On Monday, Apple finally released iOS 7.1. The update brings with it CarPlay support, bug fixes, improvements, and various new features. One such feature is specific to the camera on the iPhone 5S and HDR mode in the Camera app.

When Apple first released HDR mode for iOS devices, the option only existed for it to be turned on or off. The all-or-nothing approach wasn't a horrible idea, but HDR photos aren't always necessary.

With iOS 7.1 on the iPhone 5S, Apple has set HDR mode to automatic by default.

Screenshot by Jason Cipriani/CNET

The first time you launch the Camera app on an iPhone 5S after updating to iOS 7.1, you'll notice an "HDR Auto" along the top of the screen. Tapping on the label will bring up the option for you to manually turn it on or off.

Screenshot by Jason Cipriani/CNET

With HDR set to auto, you can take photos as you normally would without having to worry about remembering to turn HDR on or off. When the iPhone detects a photo that would benefit from HDR, you'll see a giant yellow HDR icon show up directly next to the shutter button on your iPhone's screen. The icon indicates HDR mode will be used for the photo.

Of course, when the photo is saved to your Camera Roll it will have the standard HDR label as it always has. And the same setting exists in Settings > Camera to save the non-HDR and HDR version of a photo when an HDR photo is taken, should you want to have both copies. (I always have this enabled. Sometimes a unplanned motion can ruin an HDR photo, and having the original photo saved is a good fallback.)