A new research shows that 3D mammography could be more effective in identifying invasive cancers.

Researchers explained that such mammography reduces the need for unnecessary recalls.

For the study, researchers analysed data gathered from the University Hospitals Case Medical Center Seidman Cancer Center. The data encompassed nearly half a million mammograms taken from 13 different facilities.

The study findings showed that 3D mammography increased the rate of successfully detecting invasive cancer by 41 percent. It increased the rate of detection of breast cancer in general by 15 percent and reduced the need for unnecessary recalls caused by false alarms by 15 percent.

According to the researchers, this latest form of technology can improve breast cancer diagnoses and treatments.

"This study confirms what we already know: 3D mammography finds more of the invasive, harmful cancers we want found and saves women the anxiety and cost of having additional exams for what turns out to be a false alarm," said the study's co-author Donna Plecha, MD, Director of Breast Imaging at University Hospitals Case Medical Center reported in the press release. "We already knew that breast screening saves lives and this study provides us with firm data that 3D mammography is a better test for detecting breast cancer early when it is treatable."

Plecha explained that breast cancers caught in the initial stages by mammography are more likely to be cured and are less likely to require chemotherapy or as extensive surgery. "This study shows that 3D mammography is a more effective screening tool, and we must make it accessible to all women."

The study was published in the Journal of the American Medical Association.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.