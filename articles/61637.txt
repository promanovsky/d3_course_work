Visitors to the Capitol had the opportunity to inspect the Tesla Motors Model S electric sedan on display in Sacramento, Calif., Tuesday June 18, 2013. Built in Fremont, the Model S has a seating capacity for five adults and can travel up to 300 miles per charge, depending on which battery package purchased. The starting price for the base Model S is $69,900. (AP Photo/Rich Pedroncelli)

The risk of Tesla’s Model S sedan catching fire has been reduced to “virtually zero,” wrote Tesla CEO Elon Musk in a blog post Friday.

Thanks to a titanium shield sandwiched between a "rounded, hollow" aluminum bar and a "shallow angle, solid" aluminum extrusion, Model S sedans built after March 6 are supposedly safer than ever, a fine feat considering the National Highway Traffic Safety Administration named it the safest car it ever tested.

Tesla focused on strengthening the car's underside after it found two Model S fires -- one in Seattle, Wash. and the other in Smyrna, Tenn. -- had arisen from underbody damage from impacts with road debris. The company claims the new shield virtually eliminates road debris from touching the car's battery module.

Tesla even went so far as to design the shield to protect the car from catching fire in the event of a high-speed impact that rips the wheels off the car, like the impact that caused a Model S to catch fire in Mexico last year.

“We have tried every worst case debris impact we can think of," Musk wrote of the new shielding, "including hardened steel structures set in the ideal position for a piking event, essentially equivalent to driving a car at highway speed into a steel spear braced on the tarmac.” Now that's thorough.

And the company released GIFs to show the shield in action. As can be seen, the shield absolutely obliterates a tow hitch, alternator and concrete block.

Images courtesy of Tesla

Despite preventative moves to limit the risk of vehicle fires, Musk has remained adamant that the Model S is the safest car available to consumers, going so far as to use the fires as an example of the Model S sedan's superior safety. He notes that no Tesla owner has ever been injured due to a fire and claims that even if the passengers of Tesla's that caught on fire had remained in the vehicle during the incident, the car's "steel and ceramic firewall between the battery pack and the passenger compartment" would have protected them from significant injury.

While the chances of a Tesla catching on fire are very low –- Musk claimed in an October 2013 blog post that “you are 5 times more likely to experience a fire in a conventional gasoline car than [in] a Tesla” –- the company is allowing owners of older Model S sedans to have the new underbody shield retrofitted to their cars free of charge.