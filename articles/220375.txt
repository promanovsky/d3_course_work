When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up forfor the biggest new releases, reviews and tech hacks

Google are today celebrating the 40th anniversary of Erno's Rubik's cube.

In the spring of 1974, Erno Rubik, a Hungarian professor of design and architect invented the frustrating but ingenious game

Having no idea it would become the sensation it did or the impact it would have on culture and the art world.

It quickly became one of the world's best selling toys - despite him fashioning the first cube himself by hand-carving the 'cubelets'.

It took its creator one month to be able to solve the game for himself.

Today's doodle allows you to play the game from the comfort of your computer.

The cubes six faces made up of nine stickered squares, with each row and column independent of the other apart from the centre square.

The animation allows you to turn and swivel the sides as you would in the normal game.

There is even a number on the bottom left corner to show you how many moves it has taken you.

Play the doodle here