BOSSIER CITY, LA - JANUARY 09: George Strait performs to a sold-out arena at CenturyLink Center on January 9, 2014 in Bossier City, Louisiana. (Photo by Erika Goldring/Getty Images) George Strait performs to a sold-out arena at CenturyLink Center on January 9, 2014 in Bossier City, Louisiana. (Source: Erika Goldring/Getty Images)

MIAMI (CBSMiami) — There’s no question George Strait is the sure bet in the entertainer category of the year at the Academy of Country Music Awards, right?

Not so fast, says Blake Shelton. Sure, the beloved star is on something of a victory lap after winning the same trophy at the rival Country Music Association Awards five months ago. But Sunday’s night’s ACM Awards are held in Las Vegas where there’s no safe bet, after all, and the academy’s once-controversial decision to open its top honor to fan voting makes the category enjoyably unpredictable.

“I think George Strait probably has the biggest overall fan base of any of us, but will they take time to vote?” Shelton said. “That’s what we’ll find out, I guess.”

“It’s an interesting thing,” said Luke Bryan, Shelton’s ACMs co-host. “You never know when those fans go online and vote. They have the complete power. It’s really neat when they do. It’s a pretty crazy validation system when the fans are allowed to vote.”

Bryan and Shelton could be 1989 winner Strait’s top challengers during the awards, which air live at 8 p.m. EDT from the MGM Grand on CBS. They round out the category with two-time winner Taylor Swift and mainstream country darling Miranda Lambert.

Swift is one of pop music’s top stars with a motivated fan base and Lambert is one of the academy’s favorites with 15 trophies so far and is this year’s two top nominee with Tim McGraw.

Bryan and Shelton, however, are as popular as anyone in the genre now. Shelton is a television star thanks to “The Voice” and won his first entertainer trophy at the CMAs. And Bryan provided one of the award show circuit’s true surprises when he won the award at the ACMs last year during his first stint as Shelton’s co-host.

Shelton said he predicted the victory in the seconds before it was announced and provides a measuring stick for Bryan’s rapidly growing popularity.

“When that happened it’s like, oh, my god, it’s not just a matter of selling a bunch of records and having some hits on the radio,” Shelton said. “He’s developed an enormous fan base. To beat out the people that he beat out last year, there’s nothing you can do but just respect that. That’s once in a lifetime and they’re there forever. To see him to get to that place, I love it. I was just as excited — well not as excited because I didn’t cry — but I was happy for him.”

Bryan and Shelton figure in two of the night’s other top categories. They’re both up for male vocalist of the year with Jason Aldean, Lee Brice and Keith Urban, who has six overall nominations.

And Bryan’s “Crash My Party” and Shelton’s “Based on a True Story … ” are up for album of the year with Florida Georgia Line’s “Here’s to the Good Times,” Kacey Musgrave’s Grammy Award album of the year winner “Same Trailer Different Park” and Tim McGraw’s “Two Lanes of Freedom.”

Swift and Lambert are up for female vocalist of the year with Musgraves, Carrie Underwood and Sheryl Crow.

And Lambert’s “Mama’s Broken Heart,” co-written by Musgraves, is up for single record and song of the year.

© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.