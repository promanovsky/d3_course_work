Health is wealth. Google takes the fight for good health further by understanding what goes on inside exactly.

Called Baseline Study, it is a project that aims to collect molecular and genetic information anonymously from people to hopefully create the most accurate picture of good health. In its early stages, the project will start out with samples from 175 individuals. The number of participants will grow as the project moves along. Managed by molecular biologist Andrew Conrad who joined Google [x] in 2013, the project brings together a team of 70 to 100 experts from various fields like molecular biology, imaging, optics, biochemistry and physiology.

"With any complex system, the notion has always been there to proactively address problems. That's not revolutionary. We are just asking the question: If we really wanted to be proactive, what would we need to know? You need to know what the fixed, well-running thing should look like," explained Conrad about the purpose of the project.

Baseline Study is not the first research to tackle genomics but it may be the biggest, collecting data on a broader and larger scale to cover more areas concerning health. As with other studies before, data collection is geared towards helping researchers in detecting diseases sooner so treatment can not only be applied at the soonest time possible but may be avoided altogether as well.

Take high cholesterol, for example. Baseline Study may be the key to discovering a biomarker that will shed light on why some people can efficiently break down fatty food while others cannot. While high cholesterol can be affected by a person's diet, it may also be a condition that is aggravated by genetic conditions. If a biomarker is identified for inefficient cholesterol metabolism, for instance, Baseline Study will make testing possible for the condition, potentially warning affected individuals of their increased risk so they can take action before a heart attack happens.

Most biomarkers that have been identified by previous studies concern late-stage diseases. These have been very helpful but Baseline Study wants to go the other way: spotting conditions before they turn serious. Advances may be slow though, with progress coming in little increments. This is because the Baseline Study team knows the body is complex.

With Google on-board though, analyzing data will be easier. It definitely helps to have access to one of the largest networks of data centers and computers in the world. All data collected for the Baseline Study will be made anonymous and access limited to health and medical purposes.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.