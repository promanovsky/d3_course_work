Algeirs: Algeria reported its first two cases of the deadly Mers virus on Saturday, both among pilgrims returning from Saudi Arabia, where most cases and deaths from the disease have been reported.

The two Algerian men, aged 66 and 59, had gone on a pilgrimage to Muslim holy sites in Saudi Arabia, Algeria’s health ministry said without giving further details about their condition.

Other countries including Egypt, Jordan, Lebanon, Iran, the Netherlands, the UAE and the United States have also recorded cases, mostly in people who had been to Saudi Arabia.

The Middle East Respiratory Syndrome (Mers) is considered a deadlier but less transmissible cousin of the Severe Acute Respiratory Syndrome (Sars) virus that appeared in Asia in 2003 and killed hundreds of people, mostly in China.

Like Sars, it appears to cause a lung infection, with patients suffering coughing, breathing difficulties and a temperature. But Mers differs in that it also causes rapid kidney failure.

Mers has killed 187 people in Saudi Arabia since it first emerged in 2012, and hundreds more have been infected.