Eli Lilly and Co. (LLY) announced that the FDA has approved CYRAMZA (ramucirumab) as a single-agent treatment for patients with advanced or metastatic gastric cancer with disease progression on or after prior chemotherapy. With this approval, CYRAMZA becomes the first FDA-approved treatment for patients in this setting.

Lilly expects to make CYRAMZA available in the coming weeks and is committed to offering patient assistance programs for eligible patients receiving CYRAMZA treatment.

The CYRAMZA (ramucirumab injection 10 mg/mL solution) approval is based on results of REGARD, a multicenter, randomized, placebo-controlled, double-blind trial of patients with locally advanced or metastatic gastric cancer including GEJ adenocarcinoma previously treated with fluoropyrimidine- or platinum-containing chemotherapy.

CYRAMZA is a vascular endothelial growth factor (VEGF) Receptor 2 antagonist that specifically binds VEGF Receptor 2 and blocks binding of VEGF receptor ligands VEGF-A, VEGF-C, and VEGF-D.

For comments and feedback contact: editorial@rttnews.com