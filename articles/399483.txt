Baby loggerhead sea turtles emerge from a nest on the Florida Keys beach and are captured by an live streaming webcam, illuminated by infrared lighting on a Florida Keys beach Friday, July 25, 2014, near Big Pine Key, Fla. (Florida Keys News Bureau)

FLORIDA KEYS (CBSMiami/Florida Keys News Bureau) – Web-watchers were able to see about 100 baby loggerhead sea turtles hatch via a live-streaming, high definition camera.

The webcam has been focused on the nest in the Lower Keys for almost two weeks.

Under dim moonlight Friday evening, the 3-inch-long turtle babies came out of a hole and headed to the Atlantic Ocean.

While the camera has been in place, it has only used infrared light to not disturb the hatchlings or confuse them with artificial light.

“This webcam is high-definition, the first time ever used (to record a turtle hatch), and also an infrared IR-emitting light that is so important because it does not disturb any of the activities of the turtle trying to find the ambient light of the moon,” said Harry Appel, president of the Keys-based Save-a-Turtle organization, that helped to coordinate the webcam in partnership with the Florida Keys tourism council that funded the effort.

Watch the video, click here.

The webcam is part of ongoing efforts in the Keys to raise awareness of sea turtles and the need to protect them.

Loggerhead, green, leatherback, hawksbill and Kemp’s ridley sea turtles nest on beaches in the Keys and other parts of Florida, and inhabit Florida and Keys waters. All five species are considered either threatened or endangered.

The webcam was approved by the U.S. Fish & Wildlife Service and the Florida Fish & Wildlife Conservation Commission.

The Florida Keys News Bureau contributed to this report.

Watch the recorded video from the turtle cam website.

RELATED CONTENT:

[display-posts category=”local” wrapper=”ul” posts_per_page=”5″]