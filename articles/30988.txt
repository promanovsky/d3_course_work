A former Microsoft Corp. employee was charged with stealing the software maker's trade secrets, including code for a program to protect against copyright infringement, and leaking them to a blogger in France.

Alex Kibkalo, a Russian national, was arrested Thursday and ordered held without bail, according to federal court filings in Seattle. He admitted to Microsoft's investigators that he provided the confidential information to the blogger, according to the criminal complaint filed by U.S. prosecutors.

Microsoft was alerted to the theft in 2012 by an individual, who asked that his identity wouldn't be disclosed and who had been contacted by the blogger to help examine code for the Microsoft Activation Server Software Development Kit, a product developed for internal Microsoft use only, according to the complaint.

The company's internal investigation traced the leaked information to Kibkalo, a seven-year employee who was working as a software architect in Lebanon, prosecutors said. He has been working for another U.S. technology company after he returned to Russia, according to the criminal complaint.

'Malicious Insiders'

The Center for Responsible Enterprise & Trade, a Washington-based advocacy organization headed by former Microsoft deputy general counsel Pamela Passman, in February released a study on the economic effects of trade-secret theft. The study identified "malicious insiders" as one of five categories of potential misappropriators of trade secrets. The others were nation states, competitors, transnational organized crime and what the report's authors called "hactavists." As part of the company's internal investigation, Microsoft accessed the blogger's Hotmail account, which contained email messages from Kibkalo with pre-release "hot fixes" for Windows 8 for mobile devises that weren't publicly available, according to the criminal complaint.

"While Microsoft's terms of service make clear our permission for this type of review, this happens only in the most exceptional circumstances," the company said in a statement today. "We apply a rigorous process before reviewing such content." Russell Leonard, a federal public defender representing Kibkalo, didn't immediately respond to a phone message Thursday after regular business hours seeking comment on the case.