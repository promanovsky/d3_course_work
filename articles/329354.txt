Bobby Womack performs at the Rock and Roll Hall of Fame 2009 induction ceremonies in Cleveland, Ohio in this April 4, 2009 file photo. His publicist said Womack died yesterday, age 70. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LOS ANGELES, June 28 — US soul singer and songwriter Bobby Womack has died, his publicist said yesterday. He was 70.

The cause of his death was not yet known, according to his publicist, Sonya Kolowrat at XL Recordings.

Womack, who was inducted into the Rock and Roll Hall of Fame in 2009, started performing gospel music with his brothers in the 1950s. He became a major figure in the rhythm and blues genre in a career that lasted seven decades.

As the lead singer of The Valentinos, the band he formed with his brothers in the 1960s, Womack scored a hit with “Lookin’ For A Love.” After the band broke up, Womack played guitar and worked with popular artists such as Aretha Franklin, Dusty Springfield and Ray Charles.

Womack, a prolific songwriter, composed songs across the rock and soul genres. His hits included “It’s All Over Now,” recorded by the Rolling Stones in 1964, and “Trust Me” for Janis Joplin, which was released after she died.

His solo singing career produced a string of hits, including 1972’s “That’s The Way I Feel About ‘Cha” and “Woman’s Gotta Have It.”

Womack had struggled with health issues in recent years, including diabetes, prostate cancer, heart trouble, colon cancer and pneumonia. Last year, he said he was beginning to show early symptoms of Alzheimer’s disease. — Reuters