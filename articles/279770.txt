Tracy Morgan is on the mend.

The actor, who broke multiple bones in a six-car accident early last Saturday morning in New Jersey, remains hospitalized and still has a long recovery in front of him.

But a rep for the star provided an encouraging update to fans yesterday at least.

"Today was a better day,” Morgan's rep said. “While Tracy remains in critical, but stable, condition, he continues to show signs of improvement.”

His leg will not be amputated, as previous rumors claimed, and FX has made it clear that the sitcom it had planned around Morgan will move ahead whenever the actor is ready.

Adds Morgan's spokesperson:

"His medical team remains optimistic that his recovery is progressing. [His fiancee] Megan remains by his side and is relaying the countless good wishes and prayers that his family, friends and fans have been sending their way."

Jimmy McNair, a 63-year old comedian and good friend of Morgan's, was killed in the crash.

Police have charged a Walmart truck driver named Kevin Roper was vehicular homicide as a result, saying he had been awake for over 24 hours when he fell asleep at the wheel and started the fatal pile-up.

Roper is currently free on bail.