Bangkok: Thailand’s economy probably shrank in the first quarter as exports remain weak and domestic activity has been depressed by months of political unrest, which threatens to tip the economy into recession.

The country has been run since December by a caretaker administration with limited fiscal powers and there is no end in sight to the crisis as protest groups seek to install an unelected government.

According to a Reuters poll of economists, data on Monday is expected to show gross domestic product (GDP) in January-March shrank by 1.6 per cent from the previous quarter on a seasonally adjusted basis.

On an annual basis, growth was expected to be only 0.1 per cent, the worst performance since a contraction in the last quarter of 2011 when the country suffered devastating floods.

The outlook for the April-June quarter and beyond is grim.

“Chances are we are going to see another technical recession in the economy, given that the second-quarter GDP number is likely to be rather poor as well,” said Gundy Cahyadi, an economist with DBS Bank in Singapore.

“The longer the economy is without a functioning government, the more the drag to economic growth,” he added.

The political protests flared up at the start of November.

In the final quarter of 2013, the economy expanded 0.6 per cent both on the quarter and from a year before.

Consumer confidence is at a 12-year low, tourists are staying away from Bangkok and public spending has been delayed.

Many parts of the economy are feeling the pinch, even the property sector, which proved resilient during previous bouts of unrest.

“If the political crisis drags on until the end of this year, the overall sector could see a contraction of as much as 10 per cent,” Rutt Phanijphand, chief executive of home builder Quality Houses, said last week.

The political turmoil is also hurting Thailand’s big auto sector, which accounts for 11 per cent of GDP and is the largest in Southeast Asia. Domestic car sales are falling and some 30,000 industry jobs have been lost his year.

Thai Airways reported a quarterly loss last week and expects more red ink in the second and third quarters as “we have been severely affected by the politics”, Chairman Prajin Juntong said.

Tourism accounts for about 10 per cent of GDP and visitors dropped about 5 per cent in January-April from a year earlier.

This month, the Tourism Authority of Thailand cut its forecast for 2014 tourist arrivals to 26.3 million, the lowest in five years, from 28 million.

Pornthip Hirunkate, vice-president of the Tourism Council of Thailand, said the unrest had probably cost about 100 billion baht ($3 billion) in tourism revenue so far.

“If the unrest continues into the second half, it will hurt the late-year high season and we could see layoffs in a sector that employs more than 3 million people,” she added.

Some think exports will start to pick up on the back of a global recovery. So far, the political unrest has been largely contained to Bangkok and has not disrupted ports and factories.

“There is no problem with orders yet. The unrest has had a minimal impact on shipments and most exporters are still confident about business,” said Nopporn Thepsitthar, chairman of the National Shippers’ Council. He expects exports to grow 2-3 per cent in the second quarter from a year earlier.

Thai Union Frozen Products, the world’s largest canned tuna producer, said its first-quarter net profit rose 40 per cent from a year earlier.

In January-March, exports fell 0.8 per cent from a year earlier and about 0.5 per cent from the previous three months, central bank data showed.

In the Reuters poll, the median forecast for economic growth this year was 2.0 per cent, compared with 2013 expansion of 2.9 per cent.

In October, the central bank forecast 2014 growth of 4.8 per cent. Due to the unrest, the forecast has been cut several times, and a reduction in the current 2.7 per cent is likely next month.

The state planning agency, which compiles GDP data, is likely to chop its 2014 GDP forecast of 3.0-4.0 per cent on Monday.

At its last meeting in April, the Bank of Thailand (BOT) left its benchmark interest rate unchanged at 2 per cent

after cuts in March and November. Most economists expect no change at the next meeting on June 18.

This month, central bank Governor Prasarn Trairatvorakul said current monetary policy settings were appropriate.

“Interest rates are not expensive and there is no liquidity problem. The business sector wants the political situation to ease and customers’ purchasing power to return,” he said.

The central bank’s private consumption index fell a seasonally adjusted 0.5 per cent in January-March from the previous quarter and 1.8 per cent from a year earlier. Private investment fell 1.4 per cent on the quarter and 6.4 per cent on the year.