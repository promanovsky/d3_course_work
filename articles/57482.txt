When Facebook acquired Oculus Rift on Tuesday, the deal validated a technology category that has long been viewed as mostly experimental: virtual reality.

That's why the first question on the minds of those who have followed the VR space for years was: What does Jaron Lanier, one of the early pioneers and most passionate advocates of VR, think of this blockbuster deal?

See also: 5 Free Android Apps for Tethering

"Well, in the abstract, I say congrats to the Oculus team, but beyond that what matters is what they actually do," Lanier said in an email to Mashable. "If they don't make a success of this support, that would be a drag.

"I have seen a lot of cases where big ticket acquisitions seem to actually slow innovative startups down. Hope that doesn't happen in this case. But remember, despite all the publicity, there are a lot of other VR efforts in the world, so this isn't the only game in town. I don't think a failure would curse VR."

Lanier knows something about the challenges of making a virtual reality company a success. He founded VPL Research in the early '80s to work on making virtual reality, well, a reality. The company's developments included networked 3D graphics, a "data glove" allowing the wearer to interact with a virtual environment and a head-mounted image-processing device.

He eventually left the company — which later closed shop, its patents acquired by Sun Microsystems — but his early work and ideas around virtual reality have resonated with his successors in the field.

Now working as an author, and a scientist under the auspices of Microsoft Research, Lanier's reaction to the Oculus deal seems a little tainted by the fact that he was forced to endure so many years of VR's promise being mostly unrealized.

"Of course I'm happy for the Oculus team, and wish them the best," says Lanier. "Whether the combination of Oculus + Facebook will yield more creativity or creepiness will be determined by whether the locus of control stays with individuals or drifts to big remote computers controlled by others.

"VR can be tremendously fun and beautiful. It's been frustrating that more people haven't been able to enjoy it for so many years. I hope lots of people will soon find VR to be as fascinating as I have."