There may now be as many as 180 endangered Florida panthers roaming in the wild, and state wildlife officials are exploring programs designed to encourage private landowners to welcome the big cats on their property.

Florida Fish and Wildlife Conservation Commission biologists updated their population estimate for the panthers during the agency's meeting this week in Fort Myers.

The panther hovered on the brink of extinction in 1994 when just 20 to 30 panthers remained. After years of conservation efforts, including the introduction of a handful of pumas from Texas to southwest Florida, the number of panthers rose was estimated at 100 to 160 adult cats.

The commission documented the births of 21 panther kittens last year.

Panthers once ranged throughout the Southeast, but most are currently found south of the Caloosahatchee River in Florida. Now that 180 adult panthers may be sharing that area, officials say more large tracts of land will be needed to sustain a healthy panther population.

"Due to the expansive habitat needs of the Florida panther, the continued growth of their population presents a unique challenge to the FWC and U.S. Fish and Wildlife Service," said FWC Commissioner Liesa Priddy. "As panther range expands, impacts on private landowners will continue to increase."

State officials say they are working to mitigate conflicts between panthers and people as the number of the cats increases. In the 2012-2013 fiscal year, the commission documented 25 cases of domestic livestock or pets being preyed on by a panther, including 15 calves on commercial cattle ranches.

"We know panthers can prey upon pets and livestock, and we strive to find solutions that work for people who experience these very real losses," said Thomas Eason, director of the commission's Division of Habitat and Species Conservation.

Collisions with vehicles are the primary cause of panther deaths. Fifteen of the 20 panther deaths reported last year occurred while panthers were crossing highways.

___

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Information from: Naples (Fla.) Daily News, http://www.naplesnews.com

Copyright 2014 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.