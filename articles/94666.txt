hidden

Several major U.S. studios filed a copyright infringement lawsuit on Monday against the file-sharing website Megaupload and its ebullient founder, Kim Dotcom.

Megaupload, which U.S. authorities shuttered in 2012, facilitated a "massive copyright infringement of movies and television shows," according to a statement issued by the Motion Picture Association of America on Monday.

The plaintiffs in the lawsuit, all MPAA members, are Twentieth Century Fox Film Corp, Disney Enterprises Inc, Paramount Pictures, Universal Studios Productions, Columbia Pictures and Warner Bros Entertainment Inc.

"Megaupload wasn't a cloud storage service at all, it was an unlawful hub for mass distribution," Steven Fabrizio, an attorney for the MPAA, said in the statement.

U.S. authorities allege Megaupload cost film studios and record companies more than $500 million and generated more than $175 million by encouraging paying users to store and share copyrighted material, such as movies and TV shows.

Dotcom says Megaupload was merely an online warehouse and should not be held accountable if stored content was obtained illegally.

Monday's lawsuit, filed in U.S. District Court for the Eastern District of Virginia, said Dotcom and other defendants "profited handsomely" by providing thousands of copyrighted works over the Internet to millions of Megaupload users without authorization or license.

Movies whose copyrights Megaupload infringed, according to the lawsuit, include Avatar, Forrest Gump and Transformers, according to the lawsuit.

Commenting on Twitter on Monday, Dotcom said U.S. authorities "probably demanded" that the studios file the lawsuit "because they initiated this ... Hollywood science fiction script of a case. Embarrassing."

The lawsuit is seeking unspecified damages as well as attorney's fees. It claims the studios are entitled to Megaupload's profits and up to $150,000 per infringement.

The lawsuit comes as Dotcom is fighting a bid by U.S. authorities to extradite him from New Zealand to face online piracy charges over the now-closed website. He is also known as Kim Schmitz and Kim Tim Jim Vestor, according to the lawsuit.

Dotcom's U.S. attorney Ira Rothken said that Monday's suit was a way for the country's film industry to go after Megaupload if the U.S. Department of Justice fails to extradite Dotcom and his colleagues to the United States from New Zealand. An extradition hearing is scheduled for July.

"The MPAA is suddenly realizing that we're a few months away from the extradition hearing, and once Kim Dotcom and the others prevail in the extradition hearing they'll have more resources and more assets," Rothken told Reuters.

"The MPAA wants to have cover if the Department of Justice fails in the extradition and the criminal case."

He predicted that the judge in the civil suit would likely stay the case pending the extradition hearing, adding that Megaupload will also seek access to evidence stored on its servers housed in Virginia to defend against the suit. Dotcom has been denied access to that evidence for the extradition hearing.

Meanwhile, the legal storm has not stopped Dotcom, a German national with New Zealand residency, from delving into politics, launching a party last month to contest New Zealand's general election in September.

Reuters