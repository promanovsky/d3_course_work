Shutterstock photo

Investing.com -

Investing.com - Upbeat personal spending data released in the U.S. earlier bolstered the dollar against the euro on Friday, though bottom fishing sent the single currency back into positive territory at times.

In U.S. trading, EUR/USD was up 0.04% at 1.3747, up from a session low of 1.3705 and off a high of 1.3772.

The pair was likely to find support at 1.3705, the earlier low, and resistance at 1.3876, Monday's high.

The Commerce Department reported earlier Friday that U.S. personal spending rose 0.3% in February, in line with expectations, Personal spending in January was revised down to a 0.2% gain from a previously estimated 0.4% increase.

A separate report showed that the core U.S. personal consumption expenditures price index remained unchanged at 0.1% last month, in line with expectations.

Elsewhere the revised Thomson Reuters/University of Michigan consumer sentiment index ticked up to 80.0 in March from 79.9 the previous month. Analysts had expected the index to rise to 80.5 this month.

Still the dollar rose, as the largely positive data came a day after economic reports showed that U.S. jobless claims fell to the lowest level since late November last week, while U.S. economic fourth quarter growth was revised higher.

Market sentiment remained firm that the Federal Reserve will continue winding down stimulus programs this year and begin raising benchmark interest rates from their current rock-bottom levels some time in 2015.

Meanwhile in the euro zone, preliminary data revealed that Germany's consumer price index rose 0.3% in March, missing expectations for a 0.4% increase, after a 0.5% gain in February.

Data also showed that French consumer spending rose 0.1% in February, less than the expected 0.8% increase, after a 2.1% decline the previous month.

The euro was down against the pound, with EUR/GBP down 0.10% to 0.8264, and up against the yen, with EUR/JPY up 0.78% at 141.50.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.