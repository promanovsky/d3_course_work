A new study revealed that most parents deny that their kids are overweight and indstead see them as having a healthy size - as long as someone is more overweight than them.

Researchers from Georgia Southern University looked at the survey data of parents with overweight kids. The survey was conducted between 2005 and 2008 with mothers as most of the respondents. The team compared their analysis to a similar study conducted between 1988 and 1994.

The findings showed that parents are viewing their children's weight based on the appearance of their peers, which are not healthy at all. This makes overweight a norm as the population continues to increase in weight. A majority of those from poorer families believed that their children were healthy if someone else is heavier than their children.

"We rarely compare our weight status against an absolute scale or a number recommended by doctors," study author Dr. Jian Zhang told TIME. "Instead, we compare to what our friends, neighbors and coworkers look like. If we look like most of the others, we of course perceive that we are just fine."

According to the U.S. Centers for Disease Control and Prevention, one out of three children and adolescents was obese or overweight in 2012. Childhood obesity has more than doubled in the past 30 years.

As childhood obesity becomes more common, parents think that their child is just normal and healthy, the study says. The researchers warned that this can create a cycle and has to be reversed immediately.

"At a certain point, if no effective strategies are taken now to reverse this trend, all kids are obese, and they continuously carry the extra weight and the risk of various health problems into adulthood," Zhang added.

The researchers recommend pediatricians take the initiative to explain to parents the complications related to obesity, such as heart diseases, diabetes and increased risk of cancer.

The study was published in the Aug. 25 issue of Pediatrics.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.