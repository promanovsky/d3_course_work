Americans living in the most unhealthy counties in the U.S. have double the death rate of those living in the healthiest counties. Not only that, but they also have twice as many kids living in poverty, and double the number of teen births.

The County Health Rankings determine health based on 29 factors, including smoking, unemployment, graduation rates, poverty, and teen births. This year, the report also measures new factors: housing, transit, access to mental health providers, injury-related deaths, food environment, and access to physical activity. The report gives states insight into the disparities occurring within a state, and which counties may need more resources.

Though overall health is influenced by many factors, certain conditions matter more, such as rates of children living in poverty, people who have had some college education, preventable hospital stays, smoking, and physical inactivity.

“The County Health Rankings often provide the spark for businesses, community planners, policy makers, public health, parents, and others to work together for better health,” says County Health Rankings director Bridget Caitlin in a statement.

Healthiest, Least Healthy County by State in 2014

AL Shelby County, Lowndes County

AK Juneau Borough, Wade Hampton Census Area

AZ Santa Cruz County, Gila County

AR Benton County, Phillips County

CA Marin County, Lake County

CO Douglas County, Huerfano County

CT Tolland County, New Haven County

DE New Castle County, Kent County

FL St. Johns County, Union County

GA Forsyth County, Clay County

HI Honolulu County, Hawaii County

ID Madison County, Bear Lake County

IL Woodford County, Alexander County

IN Hamilton County, Scott County

IA Sioux County, Appanoose County

KS Johnson County, Woodson County

KY Oldham County, Perry County

LA St. Tammany Parish, East Carroll Parish

ME Hancock County, Piscataquis County

MD Montgomery County, Baltimore City

MA Middlesex County, Hampden County

MI Ottawa County, Wayne County

MN Carver County, Mahnomen County

MS DeSoto County, Quitman County

MO Nodaway County, Dunklin County

MT Gallatin County, Roosevelt County

NE Polk County, Kimball County

NV Lincoln County, Nye County

NH Rockingham County, Coos County

NJ Hunterdon County, Cumberland County

NM Los Alamos County, Quay County

NY Livingston County, Bronx County

NC Wake County, Columbus County

ND Dickey County, Sioux County

OH Geauga County, Scioto County

OK Kingfisher County, Pushmataha County

OR Benton County, Klamath County

PA Union County, Philadelphia County

RI Bristol County, Providence County

SC Beaufort County, Marion County

SD Hutchinson County, Buffalo County

TN Williamson County, Grundy County

TX Presidio County, San Augustine County

UT Morgan County, Carbon County

VT Chittenden County, Essex County

VA Loudoun County, Petersburg City

WA San Juan County, Pacific County

WV Pleasants County, McDowell County

WI Ozaukee County, Menominee County

WY Teton County, Fremont County

The rankings also reveal nationwide trends that indicate we are heading down the right path:

Teen birth rates have dropped about 25% since 2007.

The rate of preventable hospital stays decreased about 20% between 2003 to 2011.

Smoking rates decreased from 21% in 2005 to 18% in 2012.

Completion of at least some college increased a bit from 59% in 2005 to 64% in 2012.

So while some counties may be falling behind, others are slowly contributing to improvements in major health measures. Still, there’s room for improvement. For instance, the researchers note that one in five homes are overcrowded and lack of adequate facilities to cook and bathe. These issues are most commonly experienced in counties on the coasts, in Alaska, and in parts of the South.

There are also disparities in mental health care, with mental health providers being 1.3 times more available in healthier counties. Healthier counties also have more access to better food and outdoor areas like parks. The data speaks to the effectiveness of these lifestyle factors in making counties healthier and safer.

The County Health Rankings are released in collaboration between the Robert Wood Johnson Foundation (RWJF) and the University of Wisconsin Population Health Institute. You can see the full report here.

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.