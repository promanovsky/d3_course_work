Ryanair is to bid for Cyprus Airways, the troubled island nation's national airline, as it seeks to reinforce its presence across the Mediterranean.

The news was confirmed by Kenny Jacobs, the chief marketing officer for the budget Irish carrier, in an interview with BBC radio this morning. Jacobs said Ryanair would finalise and submit a bid before Friday's deadline.

The announcement had been expected for some time. On Thursday 21 August, chief executive Michael O'Leary flew to Cyprus to hold discussions with its government about a takeover of the loss-making airline, having earlier told journalists: "We are engaged in the process and hope something will come of it. It's a very political process down there."

Jacobs confirmed today that the meeting with the Cypriot authorities was "very positive".

Ryanair is proposing to increase passengers five-fold from 600,000 to 3 million. The Cypriot government is expecting upwards of 15 bids as it seeks to offload an asset which has been a serious drain on public resources.

Ryanair aiming for Isreali

Cyprus Airways is facing investigation from the European Commission over allegations that a rescue package from the government in 2012, which totalled £25m ($41.4m, €31.4m) flouted state aid regulations. The airline has already received almost £80m in state aid and bailouts.

"From Ryanair's point of view there is a very exciting future for Cyprus Airways, where I believe with the help of Ryanair we could put Cyprus Airways and Cyprus tourism back on a path of a very much renewed and rapid growth," O'Leary had previously told reporters.

"We think we can deliver that growth using Cyprus Airways. So it will be Cyprus Airways growing again from 700,000 passengers this year to three million in the next few years," he added.

The move comes in the same week in which O'Leary said that he wanted to launch an "Israeli version" of the low-cost airline.

Speaking to the Irish Independent, O'Leary said: "We're actively talking to the Israeli authorities, but the difficulty is that once you go outside Europe you need to have, in this case, an Israeli air operator's certificate. But it's still very much on the front foot for us. But the Israeli authorities have got much more nervous about protecting El-Al from competition because of the recent events."

The "Israeli Ryanair" would service markets all over Israel to Russia, central Europe and the UK.