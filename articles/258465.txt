We now have another look at the Kim Kardashian wedding dress.

With E! News having unveiled a number of exclusive pictures from the reality star's nuptials this Saturday, the new bride herself could not wait any longer to share the excitement for herself.

She posted the following image to Instagram today and included on her account a very simple caption: "Mr. and Mrs. Kanye West."

That says it all, doesn't it?

Rob Kardashian may have believed the Kimye wedding to be a pile of superficial BS, but it looks rather romantic to us.

Kim donned a Givenchy gown for the occasion, with that designer also coming up with outfits for Kanye and daughter North West.

Various reports also confirm John Legend singing for the couple's first dace (to "At Last") and Kanye giving a toast in which he labeled Kardashian as the ideal celebrity.

That's certainly true as far as online traffic goes!

What do you think of Kim's wedding dress?