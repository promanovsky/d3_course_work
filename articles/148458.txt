When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The Pretty Woman star pulled out of a series of awards season appearances in February (14) after Nancy Motes died following an apparent drug overdose at her Los Angeles home.

The actress has since opened up about the loss to the Wall Street Journal Magazine, in an interview which was conducted less than three weeks after Motes' passing.

In the chat, which was published on Monday (21Apr14), Roberts admits she has been hit hard by the family tragedy.

She says, "It's just heartbreak. It's only been 20 days. There aren't words to explain what any of us have been through in these last 20 days. It's hour by hour some days, but you just keep looking ahead.

"You don't want anything bad to happen to anyone, but there are so many tragic, painful, inexplicable things in the world. But (as with) any situation of challenge and despair, we must find a way, as a family.

"It's so hard to formulate a sentence about it outside the weepy huddle of my family."

However, Roberts has found one way to deal with her grief after finding peace through meditation, something she has introduced to her three young children.

She adds, "Meditation or chanting or any of those things can be so joyous and also very quieting. We share and just say, 'This is a way I comfort myself'."