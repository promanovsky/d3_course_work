While the lines to try virtual reality have been among the longest at the Electronic Entertainment Expo , the buzzed-about technology isn't necessarily a dominating force at the game industry's annual gathering, not when there are dozens of other queues - and immediate financial prospects - for traditional games.

Sony and Oculus VR, the startup that launched the latest VR obsession two years ago and was purchased by Facebook earlier this year for $2 billion, haven't announced plans to bring their respective headsets to market. That hasn't stopped the companies from touting VR with demonstrations in front of and behind closed doors at E3.

There are only a handful of VR creations from smaller developers on display at E3, such as "Words With Friends" co-creator Paul Bettner's cartoony platformer "Lucky's Tale," indie developer Piotr Iwanicki's time-bending shooter "Superhot" and former Microsoft creative director Adam Orth's space odyssey "Adr1ft."

"I've always wanted to work in VR," Orth said. "From the very first moment I had the idea for this game, I said it's gonna be in VR. What surprised me the most were the emotions that I feel from actually being in the world. You're able to have these moments where the player goes, 'Oh, wow!' They're not manufactured at all.'"

Richard Marks, senior director of research and development at Sony Computer Entertainment America, has been showing off the latest demos for Sony's prototype Project Morpheus headset, including a street luge game that simulates careening down a traffic-filled roadway as players lie on a beanbag in the real world.

"I think retailers are still struggling to wrap their minds around it," said Marks, who unveiled Morpheus at the Game Developers Conference in March. "E3 is just a different place. The developers at GDC were all excited to make stuff for it. It's so new. By next year, people will have had more time to think about its commercial viability."

Sony's rivals remain less optimistic about the technology, despite the strides that Oculus and the PlayStation maker have made with sharpness and latency in their headsets.

Phil Spencer, head of Microsoft's Xbox division, and Reggie Fils-Aime, president at Nintendo of America, voiced concerns about the potential of VR following their E3 briefings. Microsoft and Nintendo, which infamously released the failed Virtual Boy headset in 1995, have yet to unveil their own takes on VR.

"For us, it's all about fun gameplay," said Fils-Aime. "That's what we want. We want a fun, compelling experience. Right now, the technology isn't quite there yet, in our view. Certainly, it's something we're looking at. We look at a wide range of technologies. When it's there and enables a fun experience, we'll be there, too."

Publishers like Ubisoft and Electronic Arts have acknowledged that they're tinkering with VR behind the scenes, but their showy E3 presentations and staggering booths inside the Los Angeles Convention Center this year haven't been strictly dedicated to games intended for screens that won't be attached to gamers' noggins.

(Also see: EA at E3 2014: Battlefield Hardline, FIFA 15, Mass Effect 4 and More)



Patrick Soderlund, executive vice president at EA Studios, thinks it's only a matter of time.

"We're deep into VR," he noted. "All you have to do is to put the Oculus or Morpheus headset onto your head and realize that VR is now finally at the point where it's going to be viable. We can now do the things - and more - from 'The Lawnmower Man,' which came out 20 years ago. It has to work. I can't imagine it not working."