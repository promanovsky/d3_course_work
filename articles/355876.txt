WASHINGTON -- How strong is that pina colada? Depending on how it's made, it could contain as much alcohol as two glasses of wine.

The National Institutes of Health is trying to spread the word: Take a look at its online alcohol calculator to see how much you're really drinking with those summer cocktails.

A "standard drink" is the amount of alcohol in a 12-ounce beer, 5 ounces of wine or 1.5 ounces of distilled spirits. It's a useful way to track alcohol consumption. But the multiple ingredients of mixed drinks make for a harder count.

"Most people don't realize how much alcohol is actually in a drink," said Dr. George Koob, director of the NIH's National Institute on Alcohol Abuse and Alcoholism.

"Obviously it depends on the bartender and who's mixing the drinks," Koob adds.

Recipes matter: The calculator's pina colada example, for instance, assumes it contains 3 ounces of rum. Plan on using 2 ounces instead? The calculator adjusts to show it's like 1.3 standard drinks.

What about a margarita? The calculator concludes it's the equivalent of 1.7 standard drinks, if made with 1.5 ounces of tequila, an ounce of orange liqueur and half an ounce of lime juice.

A mojito? 1.3 standard drinks. A martini, extra dry? 1.4 standard drinks.

Other favourites? Type them in: http://rethinkingdrinking.niaaa.nih.gov/ToolsResources/CocktailCalcu lator.asp .

Beyond beverage choice, Koob, who specializes in the neurobiology of alcohol, has some tips:

SUMMER HEAT

Heat increases thirst but alcohol is a diuretic, Koob notes. So in addition to the usual advice to pace yourself -- no more than one standard drink an hour -- Koob says to stay hydrated by alternating some water or club soda with the alcohol.

GENDER DIFFERENCES

Women's bodies react differently to alcohol, and not just because they tend to weigh less than men. They don't metabolize alcohol as quickly, and their bodies contain less water. On average, it takes one less drink for a woman to become intoxicated than a man of the same weight, Koob said. The NIAAA's definition of low-risk drinking for women is no more than seven drinks a week and no more than three drinks on any single day, while for men the limit is no more than 14 drinks a week and no more than four drinks on any single day.

BEYOND DRINKING AND DRIVING

The July Fourth holiday weekend historically is dangerous on U.S. highways: 38 per cent of fatalities involved alcohol-impaired driving in 2011, according to the National Highway Traffic Safety Administration.

But alcohol also doesn't mix with boating, or swimming and diving, Koob warns. According to the Centers for Disease Control and Prevention, alcohol use is involved in up to 70 per cent of adult and adolescent deaths associated with water recreation.

HOLDING YOUR ALCOHOL

What determines why one drink is plenty for one person while another routinely downs two or three? Genes play a big role. So do environmental factors, such as getting used to drinking a certain amount. That tolerance is a balancing act, Koob says. He cites research showing the person who can drink others under the table is at higher risk for alcohol problems later in life than is someone more sensitive to its effects.

WHEN ALCOHOL IS A PROBLEM

Alcohol use disorders affect an estimated 17 million Americans. There are two medications that can help, targeting different steps in the addiction cycle, Koob said. More medications that work in different ways are needed, but changing lifestyle, cognitive therapy and support groups all play a role, he said.

Medications "are never going to cure the disease," Koob said. "What they will do is help you on the way."