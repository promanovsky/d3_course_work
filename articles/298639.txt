The most recent newcomer to the self-destructing messages game is Slingshot, birthed from a hackathon and nursed to life by a small crew at Facebook Creative Labs.

The app will feel familiar to anyone who’s used messaging app like Instagram, Vine and Snapchat to share short movies and photos that can be altered with scribbles and doodles.

Photos and videos shared via Slingshot self destruct a la Snapchat, but the main wrinkle where Slingshot is concerned is that if you share something with someone, they can’t see it until they share something back with you. Whatever you send them looks like a blocky mess until they return the favor. We’ll see how long it takes for that to get annoying: I’m imagining petabytes of shots of people’s feet being sent back just so they can see whatever’s been sent to them.

The app will be available today for iOS and Android. The links haven’t quite gone live as of the time this post got published, but you’ll be able to visit sling.me/download once everything’s ready.

Here’s a quick video that shows off the app:

[TNW]

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.