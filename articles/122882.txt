Car enthusiasts take note. Ford will be releasing 1,964 limited edition Mustangs next year to celebrate 50 years of the iconic muscle car.

The limited edition ponies will be outfitted with chrome highlights around the windows, tail lights and grille, also featuring a faux gas cap badge on the rear, much like the original Mustang.

The edition will only come in two colors, white or blue, and is available in both automatic or manual transmission. The cars will be among the first off the assembly line when Ford begins manufacturing 2015 models later this year.

[AP]

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.