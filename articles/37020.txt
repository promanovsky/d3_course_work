NEW YORK — Two New York entertainment-world fixtures are teaming, as Madison Square Garden has announced that it has taken a 50% stake in Tribeca Enterprises, the company that runs the Tribeca Film Festival.

The move gives Tribeca a financial foundation as well as presumed access to a number of high-profile venues. For MSG, the deal offers a toehold in the film-festival world as it looks to expand its live-event business beyond sports and music.

Specific financial terms were not disclosed, though Tribeca said the deal “values Tribeca Enterprises at $45 million.”

VIDEO: Tribeca Film Festival 2014 trailers

Advertisement

The deal is a coup of sorts for a group that was founded little more than a decade ago as a way of bringing cultural life back to downtown Manhattan in the wake of the Sept. 11 attacks and has relied on sponsorships and ticket sales as its main sources of revenue. Tribeca Enterprises, founded in 2003 by Jane Rosenthal, Robert De Niro and Craig Hatkoff, encompasses the eponymous festival as well as, more recently, the independent-film distribution label Tribeca Film.

The deal would appear to give the springtime festival — whose events are scattered at theaters it largely rents throughout Manhattan — access to some of New York’s premiere venues. MSG owns and operates its namesake arena as well as Radio City Music Hall and the Beacon Theatre in New York and the Forum in Inglewood, among others.

Still, questions remain, from the day-to-day involvement of MSG to the matter of venue usage; after all, most of those spaces in MSG’s portfolio are not primarily equipped as movie theaters and are also larger than most film-festival events require. This year’s opening-night event, which features a performance by the rapper Nas, will be held at the Beacon.

The groups said Tribeca will continue to be run by CEO Jane Rosenthal, the Hollywood producer and executive who is De Niro’s producing partner.

Advertisement

PHOTOS: Tribeca Film Festival 2014 | Scene

“Bob De Niro and I have built a brand that supports community, artists, filmmakers and storytellers, we are thrilled to be a member of MSG’s family of iconic entertainment brands,” Rosenthal said in a statement. “Our partnership will allow us to grow the Festival and enhance the experience for our audience and provide more opportunities for the creative and filmmaking community nationally and internationally.”

MSG president and CEO Tad Smith added that he believed the deal enables the companies to “honor not only movies, but the future of storytelling — while utilizing the tremendous platforms of both brands to drive the value and growth of our respective businesses.”

Smith, who took over as CEO at MSG from Hank Ratner just several weeks ago, has experience in the movie business, overseeing the Hollywood trade paper Variety in a previous turn as CEO of publisher Reed Business Information. MSG executive chairman James Dolan also is close with Harvey Weinstein, and before a spinoff counted the cable network AMC among his holdings.

Advertisement

It is unclear if any effects of the deal besides the opening-night festivities will be felt at this year’s festival, which gets underway next month.

ALSO:

Tribeca 2014: ‘Black Coal,’ Dior doc will play festival



Tribeca: Courtney Cox, Joss Whedon to unveil new works

Advertisement

Tribeca: New York Knicks doc, featuring Phil Jackson, to premiere