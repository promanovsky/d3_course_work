Driverless car licenses will be issued in the state of California by the end of this year.

According to TechCrunch, the California Department of Motor Vehicles will begin issuing licenses to “select driverless cars,” which means that companies experimenting with cars that drive themselves will be able to apply for a license in order to test their new vehicles. While some people believe that robot-driven vehicles are the future, others are a little skeptical about the new technology — and that’s fair. Sharing the road with “real” people is enough of a headache.

Slash Gear reports that the rules set forth by the California DMV in regard to these licenses are very stringent, and that pretty much guarantees that the “average Joe” won’t be granted one.

Driverless car licenses will be issued to a select few. According to the reports, “only designated employees of select autonomous vehicle manufacturers can apply.” Other rules for applying include that the applicant must be able to take control of the robot-driven car at all times. The applicant must also have a clean driving record (anyone with a DUI in the past 10 years needn’t think about applying) and must have insurance “for at least $5,000,000 against personal injury, death, or property damage.”

Naturally, those who will be applying for a license will know the rules — and will likely be working under a company (cough, cough Google) who will be able to purchase the pricey insurance.

Driverless car licenses will have an application period starting in July and the first licenses are slated to be issued sometime in the early fall. It will cost $150 to get the license (totally cheap, right?).

As previously reported by The Inquisitr,autonomous cars are legally being tested in four states: California, Florida, Michigan, and Nevada. There are only a few approved roads for these cars to be tested out but those areas will likely expand as the demand increases. While the number of self-driving cars is relatively low now, IHS Automotive forecasts that there will be 11.8 million driverless car sales in the year 2035. Pretty crazy, right?

With California requiring (and granting) driverless car licenses, other states will undoubtedly follow suit. Do you think that robot-driven cars will be something we will see in this lifetime? Do you think that the future will consist of cars that communicate to one another and that don’t need human’s behind the wheel?

[Photo courtesy of Wikimedia Commons]