The White House released a climate report Tuesday that warns of more drastic weather changes that could occur in the near future because of climate change.

President Obama is focusing on combating climate change in his second term in addition to preparing Americans for climate change effects that are already occurring, such as rising sea levels, higher temperatures and more unpredictable weather.

"Climate change, once considered an issue for a distant future, has moved firmly into the present," the National Climate Assessment says. It also states that evidence of man-made global warming "continues to strengthen" and that "impacts are increasing across the country."

"Americans are noticing changes all around them," the report states. "Summers are longer and hotter. [...] Rain comes in heavier downpours."

The White House called for a response to the report on the same day the climate change report was released, according to CNN.

"The findings in this National Climate Assessment underscore the need for urgent action to combat the threats from climate change, protect American citizens and communities today, and build a sustainable future for our kids and grandkids," the White House said in a statement.

More than 300 climate chance experts helped create the report over a number of years and updated a previous report that was published in 2009.

Obama will speak with meteorologists about the alarming report to garner knowledge for communities across the country as they deal with longer droughts and higher risk for wildfires.

"It begins to take the climate discussion down to a regional level, so it breaks the country apart, anticipates what's going to happen in each region," Obama's counselor John Podesta said. "That kind of information will help communities plan."

The report describes each region of the country and identifies which threats each region will face if climate change continues.

The report states that the Northeast could see flooded rail lines and other infrastructure damage if sea levels continue to rise. The Great Plains could suffer more droughts and heat waves, and the West could be plagued by higher temperatures and wildfires that could threaten communities and agriculture.

The White House will continue its focus on climate chance Wednesday, when experts and politicians will convene to discuss sustainable strategies. Obama will also announce new solar power initiatives later in the week.

Obama also recently introduced new regulations on truck emissions and created "climate hubs" to help businesses prepare for the detrimental effects of climate change.

Obama's administration has not yet come to a decision about the Keystone XL pipeline, which could transport oil from Canada to the Gulf of Mexico. Environmental groups are against the project, claiming it will contribute to climate change because it will not reduce fossil fuels.

Yet, the administration believes that the controversial process of fracking, which is extracting oil and natural gas, could reduce the United States' dependance on fossil fuels.