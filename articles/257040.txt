The LG G3 smartphone has been prematurely revealed by the company's Dutch website.

Specs details and official photographs of the flagship handset have been outed, but pricing and availability is yet to be confirmed.

LG



The LG G3 will apparently feature a 5.5-inch Quad HD display with 2560×1440 screen resolution and 538ppi, a Snapdragon 801 processor and 2GB of RAM.

Other hardware features include a 13-megapixel rear camera with OIS+ and laser autofocus, and a 2.1-megapixel front camera.

The phone also features a lightweight metallic design, a 1-watt speaker with boost amp, wireless charging, a 3,000mAh battery, microSD support and a smart cover called the QuickCircle case.

LG will announce the handset at a press conference later today (May 27).

Watch a video introducing the LG G3's QuickCircle Case below:

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io