Samsung had a major technology breakthrough in synthesizing pure form of graphene on a large scale opening up the possibility of mass manufacturing of the material for use in devices.

Samsung, is on the edge of revolutionizing the tech industry with the use of a pureGraphene in major electronic gadgets. The tech giant along with an affiliated laboratory has managed to synthesize large-scale, impurity free graphene, which is a significant step toward better, thinner and durable smartphones and wearables.

Sungkyunkwan University's School of Advanced Materials Science and Engineering and the Samsung Advanced Institute of Technology (SAIT) succeeded in the extremely challenging task of separating graphene and germanium, and then merging several small sheets in to a large one to form a seamless pure graphene. The breakthrough will help researchers take advantage of graphene's properties such as incredible strength, sleekness, immense flexibility and great conductivity.

Graphene was discovered by physicists at the UK's University of Manchester in 2004. The material is a one-atom thick layer of carbon arranged in a honeycomb network. Millions of these stacked on top of one another to form graphite, popular for its use in pencils. The popularity of graphene grew enormously, mainly for its impressive properties, but researchers were unable to synthesize graphene wafers, until now. The latest breakthrough with graphene appears in Science, a subscription-based online journal, GigaOm reports.

Following the discovery, Samsung appears to have a good lead in the development of future electronics. The thin material will help build slimmer smartphones. Since graphene is more durable than steel and has greater electron mobility than silicon, the devices will be less prone to damages. Samsung is also aiming at graphene's flexibility to invent new devices with bendable displays.

Graphene has long been speculated to replace Silicon as the manufacturing material for tech goods. With the latest breakthrough, the future seems brighter.

"This is one of the most significant breakthroughs in graphene research in history," the laboratory leaders at SAIT's Lab, said in a Samsung press release, Friday. "We expect this discovery to accelerate the commercialization of graphene, which could unlock the next era of consumer electronic technology."

Samsung will not be a sole player in the new space, rival company Nokia has been experimenting with graphene since 2006. European Union's Future and Emerging Technologies program also issued a $2.3 billion grant for the research, Mashable reports. But Samsung is clearly taking the lead with its phenomenal endeavor and is expected to launch grapheme-based technology in the market by 2015.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.