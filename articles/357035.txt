No change in assessment of eco outlook; only ‘gradual’ improvement envisaged.

Measures announced last month will take considerable time to impact economy.

Mr Draghi signals expectation strong take-up of TLTROs will help lift inflation.

ECB has bought significant time, but hopes for further action won’t disappear.

ECB to meet less frequently and publish ‘accounts of monetary discussions’





ECB in no hurry to do more

As was universally expected, the European Central Bank did not change policy today and the general tone of comments from ECB president, Mario Draghi suggested it doesn’t expect to alter policy again for a very long time.

Our feeling is that persistently low inflation and a sluggish economy will keep some focus on the possible need for additional measures. However, the tenor of today’s press conference is likely to give the ECB valuable breathing time to assess how key economic variables develop through the autumn.

Importantly, Mr Draghi sought to play down any suggestion that he had signalled in a newspaper interview that the ECB did not intend to alter policy until the end of 2016 at the earliest. He indicated that he had not made a direct link between the length of the fixed rate full allotment liquidity operations and the capacity to alter policy and noted that policy would continue to be shaped by the ECB’s assessment of the medium term outlook for inflation. However, our judgement is that it would take very disturbing developments to prompt the ECB to contemplate action before the end of that year.

The sense that the ECB sees itself on the sidelines ‘for an extended period’ was conveyed in a number of ways. First of all, Mr Draghi signalled an intention to shift from a monthly to a six weekly calendar of policy meetings in the new year, arguing that the monthly schedule was ‘just too tight’ as well as hinting at an element of irritation with ‘whether we should have each and every month the expectation for action’.

The switch in the frequency of policy meetings shouldn’t be seen simply as reflection of intended inactivity but it does serve to remind us of the ECB’S repeatedly emphasised medium term policy orientation. The reality is that the medium term outlook shouldn’t be expected to change frequently in normal circumstances and, as the US Federal Reserve, has shown even in crisis conditions a six weekly meeting schedule can work well.





Little change in thinking on economy and inflation

ECB thinking on the economy appears to have changed little in the past month. The brief assessment of the current state of the Euro area economy given by Mr Draghi today was very similar to that presented when a range of policy easing measures were announced in early June but, as might be expected there was significantly less focus on the nature or extent of the problems posed by very low inflation or a relatively fragile economy; the pace of recovery was described as ‘very gradual’ this month rather than ‘gradual’ previously, possibly in response to some slightly softer survey data of late. Looking forward, precisely the same range of factors were cited as a month ago implying that risks to the outlook for activity ‘remain to the downside’.

Similarly, the ECB sees the inflation outlook almost identical to that of a month ago with ‘low levels’ expected to persist in coming months before ‘increasing gradually through 2015 and 2016’. So, the slightly weaker than expected outturn for June—when inflation was unchanged from May at +0.5%— against a widely predicted uptick to 0.6% doesn’t merit comment in today’s statement. Subdued inflation is now regarded as the norm.

A further reason for a relatively dull press conference today and expectations of near term inaction is that it will take some significant time to assess the significance of some of the measures announced a month ago. Mr Draghi provided a little more detail on how the proposed Targeted Longer Term Refinancing Operations (TLTROs) will relate to banks’ lending performance.





High hopes on TLTRO impact

In July Mr Draghi indicated that the initial tranche of these TLTROs would be set at a maximum of 7% of the outstanding levels of relevant lending, implying a possibility of additional ECB funding of up to €400bn. Today, Mr Draghi indicated that the final TLTRO figure including subsequent tranches available from March 2015 to June 2016 could be as high as €1000bn. This is the equivalent of 17.5% of the outstanding amount of the relevant lending to non‐financial corporate and households (excluding house purchase loans). It would imply a marked turnaround in credit trends as we noted last month outstanding lending has fallen by about €500bn in the past couple of years. The gap between the maximum figure indicated for the first tranche a month ago and for the scheme as a whole today also suggests that ramping up borrowing is seen as a protracted process. Indeed, it might be seen as lagging recovery and possibly also coming after a subsequent pickup in inflation. Mr Draghi may be hoping for a somewhat quicker impact as he said today that this measure would be ‘very helpful’ to bring inflation back towards target.

Our sense is that the terms on which these funds are being offered—as low as 25bps for four year money—will prompt a significant uptake, but we think the final figure could fall well short of the number indicated by Mr Draghi today. This is because credit demand is constrained by many influences besides the availability of funds while risk appetite will remain an issue for lenders.





Market debate on QE will continue

As a result, we think markets will continue to debate the need for further ECB easing. In light of the debate on the timing of tightening in the US and UK, this divergence will be welcomed by the ECB as a source of possible downward pressure on the Euro on FX markets. However, the limited easing in the exchange rate in the past month means in the context of a weak environment, it remains a source of downward pressure on inflation and activity in the Euro area at present.

P.S. Mr Draghi also indicated today that in the context of a switch to policy meetings every six weeks that the ECB would publish an account of these meetings but until we see how detailed and ‘open’ these are, it is impossible to say whether they will materially improve markets understanding of how the policymaking debate is shaped.