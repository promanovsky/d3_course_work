Researchers at Ohio State University have programmed a computer to recognize up to 21 different emotions and their corresponding facial expressions -- from the most basic to the oddly conflicted and hybridized, such as "happily disgusted" and "sadly angry."

Previous studies into common facial expressions have focused at the basic six: happy, sad, fearful, angry, surprised and disgusted.

Advertisement

"We’ve gone beyond facial expressions for simple emotions like 'happy' or 'sad,'" explained Alex Martinez, cognitive scientist and professor of computer engineering at Ohio State. "We found a strong consistency in how people move their facial muscles to express 21 categories of emotions."

"That is simply stunning," he added. "That tells us that these 21 emotions are expressed in the same way by nearly everyone, at least in our culture."

Martinez and his colleagues modeled the computer program on analysis of hundreds of photos. Researchers photographed 230 volunteers, 130 female and 100 male, each making a range of faces in response to verbal cues. After plotting various facial points and positions, the researchers analyzed the photographs for similarities -- ultimately finding 21 distinct emotional responses.

The computer model is meant to be used as a basic tool in cognition research.

“In cognitive science, we have this basic assumption that the brain is a computer," Martinez said. "So we want to find the algorithm implemented in our brain that allows us to recognize emotion in facial expressions."

Martinez is hopeful that the addition of new compound-emotions, the precision and complexity of cognitive research can improve. "Hopefully with the addition of more categories, we’ll now have a better way of decoding and analyzing the algorithm in the brain."

The research was recently published in the Proceedings of the National Academy of Sciences.

[Ohio State University]