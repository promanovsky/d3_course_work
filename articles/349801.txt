As much as 88 per cent of the open ocean's surface contains plastic debris, raising concern about the effect on marine life and the food chain.

Washington: As much as 88 per cent of the open ocean's surface contains plastic debris, raising concern about the effect on marine life and the food chain, scientists have said.

Mass-produced plastics from toys, bags, food containers and utensils make their way into the oceans through storm water runoff, a problem that is only expected to get worse in the coming decades.

The findings in the Proceedings of the National Academy of Sciences are based on more than 3,000 ocean samples collected around the world by a Spanish science expedition in 2010.

"Ocean currents carry plastic objects which split into smaller and smaller fragments due to solar radiation," said lead researcher Andres Cozar from the University of Cadiz on 30 June. "Those little pieces of plastic, known as microplastics, can last hundreds of years and were detected in 88 per cent of the ocean surface sampled during the Malaspina Expedition 2010."

The study projected that the total amount of plastics in the world's oceans, between 10,000 and 40,000 tons, is actually lower than prior estimates.

However, it raised new concerns about the fate of so much plastic, particularly the smallest pieces. The study found that plastic fragments "between a few microns and a few millimetres in size are underrepresented in the ocean surface samples."

More research is needed to find out where these are going and what effects they are having on the world's marine life. "These micro plastics have an influence on the behaviour and the food chain of marine organisms," said Cozar.

"But probably, most of the impacts taking place due to plastic pollution in the oceans are not yet known."

PTI