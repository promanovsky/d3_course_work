No more dreaded pelvic exam? New guidelines say most healthy women can skip the yearly ritual.

Routine pelvic exams don't benefit women who have no symptoms of disease and who aren't pregnant, and they can cause harm, the American College of Physicians said Monday as it recommended that doctors quit using them as a screening tool.

It's part of a growing movement to evaluate whether many longtime medical practices are done more out of habit than necessity, and the guideline is sure to be controversial.

Scientific evidence "just doesn't support the benefit of having a pelvic exam every year," said guideline coauthor Dr. Linda Humphrey of the Portland Veterans Affairs Medical Center and Oregon Health & Science University.

"There will be women who are relieved, and there are women who really want to go in and talk with their doctor about it and will choose to continue this," she added.

The recommendations aren't binding to doctors - or insurers.

Indeed, a different doctors' group, the American College of Obstetricians and Gynecologists, still recommends yearly pelvic exams, even as it acknowledges a lack of evidence supporting, or refuting, them.

Pelvic exams have long been considered part of a "well-woman visit," and some 62 million were performed in the United States in 2010, the latest available data.

Here's what put the test under the microscope: Pap smears that check for cervical cancer used to be done yearly but now are recommended only every three to five years. So if women weren't going through that test every year, did they still need the pelvic exam that traditionally accompanied it?

During a pelvic exam, a doctor feels for abnormalities in the ovaries, uterus and other pelvic organs. But two years ago, scientists at the Centers for Disease Control and Prevention reported that the internal exams weren't a good screening tool for ovarian cancer and shouldn't be required before a woman was prescribed birth control pills.

The American College of Physicians, specialists in internal medicine, took a broader look.

Pelvic exams are appropriate for women with symptoms such as vaginal discharge, abnormal bleeding, pain, urinary problems or sexual dysfunction, the ACP said. And women should get their Pap smears on schedule - but a Pap doesn't require the extra step of a manual pelvic exam, it said.

For symptom-free women, years of medical studies show routine pelvic exams aren't useful to screen for ovarian or other gynecologic cancers, they don't reduce deaths, and there are other ways, such as urine tests, to detect such problems as sexually transmitted infections, the doctors' group reported in the journal Annals of Internal Medicine.

Moreover, pelvic exams can cause harm - from unnecessary and expensive extra testing when the exam sparks a false alarm, to the anxiety, embarrassment and discomfort that many women report, especially survivors of sexual abuse, the guidelines said.

No one knows how many women postpone a doctor's visit for fear of a pelvic exam, Humphrey said.

Dr. Ranit Mishori, a family physician and associate professor at Georgetown University School of Medicine, said the new guideline "gets rid of an unnecessary practice" that takes up valuable time that could be put to better use.

"Many women will be happy to hear that, and I think also, frankly, many physicians will be happy to hear it. Many of us have stopped doing them for a long time," said Mishori, who wasn't involved with the recommendations.

Despite its continued recommendation for annual pelvic exams, the American College of Obstetricians and Gynecologists said in 2012 that patients should decide together with their providers whether to have them.

Sometimes that exam lets the doctor spot, say, problems around the uterus that might lead to questions about incontinence that the supposedly asymptomatic patient was too embarrassed to bring up, said ACOG vice president Dr. Barbara Levy.

"Women have an expectation that they're going to have an exam" if they choose a gynecologist, Levy said.

An editorial published alongside the guidelines Monday cautioned that pelvic exams also look for noncancerous uterine and ovarian growths, and the scientific review didn't address whether that's beneficial.

Still, editorial coauthors Drs. George Sawaya and Vanessa Jacoby of the University of California, San Francisco, said that whether the new guideline changes doctors' practice or not, it could lead to better evaluation of what "has become more of a ritual than an evidence-based practice."

"Clinicians who continue to offer the examination should at least be cognizant of the uncertainty of benefit and the potential to cause harm through a positive test result and the cascade of events that follow," they wrote.