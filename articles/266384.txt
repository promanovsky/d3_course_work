Scientists warn WHO not to stub out e-cigarettes

Share this article: Share Tweet Share Share Share Email Share

London - A group of 53 leading scientists has warned the World Health Organisation not to classify e-cigarettes as tobacco products, arguing that doing so would jeopardise a major opportunity to slash disease and deaths caused by smoking. The UN agency, which is currently assessing its position on the matter, has previously indicated it would favour applying similar restrictions to all nicotine-containing products. In an open letter to WHO Director General Margaret Chan, the scientists from Europe, North America, Asia and Australia argued that low-risk products like e-cigarettes were “part of the solution” in the fight against smoking, not part of the problem. “These products could be among the most significant health innovations of the 21st century - perhaps saving hundreds of millions of lives. The urge to control and suppress them as tobacco products should be resisted,” the experts wrote. Leaked documents from a meeting last November suggest the WHO views e-cigarettes as a “threat” and wants them classified the same way as regular tobacco products under the Framework Convention on Tobacco Control (FCTC). (http://link.reuters.com/muq69v)

That has set alarm bells ringing among a number of medical experts - and in the booming e-cigarette industry. A total of 178 countries are parties to the international convention and are obliged to implement its measures, with the United States the one notable non-signatory.

A move to classify e-cigarettes alongside regular cigarettes would push countries into taking similar tough measures to restrict demand, including raising taxes, banning advertising, introducing health warnings and curbing use in public places.

Uptake of electronic cigarettes, which use battery-powered cartridges to produce a nicotine-laced inhalable vapour, has rocketed in the last two years and analysts estimate the industry had worldwide sales of some $3-billion in 2013.

But the devices are controversial. Because they are so new there is a lack of long-term scientific evidence to support their safety and some fear they could be “gateway” products to nicotine addiction and tobacco smoking - though the scientists said they were “unaware of any credible evidence that supports this conjecture”.

For tobacco companies seeking to offset the decline in traditional smoking, investment in e-cigarettes was an obvious choice and all the major players now have a presence, prompting Big Tobacco to line up behind scientists on this occasion.

Kingsley Wheaton, director of corporate and regulatory affairs at British American Tobacco, said classifying e-cigarettes as tobacco products would mean smokers find it harder to access a less risky alternative.

The Geneva-based WHO said its position on e-cigarettes was still in flux ahead of a key meeting on the FCTC scheduled for October 13-18 in Moscow, where proposed regulations will be discussed.

“At this point the only thing I can say is that we are elaborating these regulations and they will soon be available to you,” Armando Peruga, programme manager for the WHO's Tobacco Free Initiative told reporters this week.

Gerry Stimson, emeritus professor at Imperial College London and one of the organisers of the letter to Chan, told Reuters that the WHO's position was “bizarre” and its stance on e-cigarettes was harsher than that of regulators in Europe and the United States.

“We want to make sufficient noise now before things get too set in stone,” he said. - Reuters