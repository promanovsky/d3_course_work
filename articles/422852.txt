Ebola hits West Africa’s economies

Share this article: Share Tweet Share Share Share Email Share

Freetown - The worst ever Ebola outbreak is causing enormous damage to West African economies as foreign businessmen quit the region, the African Development Bank said, while a leading medical charity branded the international response “entirely inadequate”. As transport companies suspend services, cutting off the region, governments and economists have warned that the epidemic could crush the fragile economic gains made in Sierra Leone and Liberia following a decade of civil war in the 1990s. At least 1 427 people have died of the deadly haemorrhagic virus since it was first detected in the remote jungles of south-east Guinea in March and spread quickly to neighbouring Liberia and Sierra Leone. Five people have also died in Nigeria. Air France, the French network of Air France-KLM said on Wednesday it had suspended flights to Sierra Leone after advice from the French government. African Development Bank (AfDB) chief Donald Kaberuka said on a visit to Sierra Leone he had seen estimates of a reduction of up to 4 percent in gross domestic product due to Ebola.

“Revenues are down, foreign exchange levels are down, markets are not functioning, airlines are not coming in, projects are being cancelled, business people have left - that is very, very damaging,” he told Reuters late on Tuesday.

Liberia has already said it would have to lower its 2014 growth forecast, without giving a new one.

Sierra Leone Deputy Minister of Mineral Resources Abdul Ignosis Koroma said the government would miss its target of exporting $200 million in diamonds this year because of the Ebola outbreak. It exported $186 million of diamonds in 2013.

He said miners were too afraid to go to alluvial diamonds pits in the Ebola-stricken east and tough border controls to curb the spread of the virus were also hurting the trade.

Several international companies in the region have pulled out expatriate staff in recent weeks. Iron ore miner London Mining, whose only operating mine is in Sierra Leone, said recently that Ebola could hurt its production this year.

The AfDB has announced $60 million to help train medical workers and purchase supplies to fight the outbreak. Some $15 million will be disbursed in September, Kaberuka said, voicing hope the donation would stop money being diverted away from other programmes such as education and agriculture.

Medical charity Medecins Sans Frontier (MSF), which has been spearheading the healthcare response, said international efforts had been chaotic and entirely inadequate to the scale of the crisis. It said its new centre in the Liberian capital Monrovia had filled with Ebola patients shortly after opening.

“It is simply unacceptable that... serious discussions are only starting now about international leadership and coordination,” said Brice de la Vigne, MSF operations director. “Self-protection is occupying the entire focus of states that have the expertise and resources to make a dramatic difference.”

Kaberuka, echoing comments from governments of the Ebola-affected countries, said travel and trade restrictions imposed by airlines, shipping firms and neighbouring economies were increasing the economic hardship.

“I understand the countries which are posing restrictions... but let us only do so based on medical evidence and not on political imperatives,” said Kaberuka. The World Health Organisation (WHO) has repeatedly advised against such bans, warning they could cause food and supply shortages.

Brussels Airlines said on Wednesday it resumed flights to Liberia, Sierra Leone and Guinea after it was forced to suspend the routes at the weekend after Senegal refused to allow it to change its flight crews there.

The airline, in which Germany's Lufthansa owns a 45 percent stake, said there were passenger waiting lists in all three countries and around 50 tons worth of emergency medical supplies waiting at Brussels airport to be transported.

“There is indeed a strong need for airline services to the countries,” spokesman Geert Sciot told Reuters. “We're trying under extremely difficult circumstances to fill that role.”

In Nigeria, where five people have died after an infected US citizen flew in from Liberia, the outbreak has been contained so far.

“Nigeria is still at risk of Ebola because we still have one case and from this one case the risk of spread is there,” Health Minister Onyebuchi Chukwu said, adding that the start of the school year, planned for Monday, would be delayed until October 13 as a preventive measure.

Democratic Republic of Congo announced on Sunday a separate outbreak of Ebola in its remote northwestern province of Equateur and said it had killed at least 13 people. It was Congo's seventh outbreak since the disease - believed to be carried by bush animals - was first detected there in 1976.

“At this time, it is believed that the outbreak in DRC is unrelated to the ongoing outbreak in west Africa,” the UN agency said in a statement, adding that samples had been sent to laboratories to determine the specific strain of the virus. - Reuters