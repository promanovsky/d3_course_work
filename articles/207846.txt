The euro hovered within 0.1pc of its weakest level in a month against the dollar yesterday as Citigroup and UBS forecast further declines amid signs the European Central Bank will add further stimulus.

A weaker currency helps boost exports but can drive up the real cost of goods and services bought in from outside the eurozone.

"We are bearish and think that finally the ECB is ready to back words with action," said Geoffrey Yu, a foreign-exchange analyst at UBS in London. "The market is pricing in some degree of accommodation ahead. The level of the euro is going to jeopardise the ECB's inflation forecasts."

The euro was at $1.3759 at 7:38am in New York after dropping to $1.3745 on May 9, its lowest level since April 8.

The single currency advanced 0.1pc to 140.25 yen after sliding 1.2pc last week. Japan's currency fell 0.1pc to 101.93 per dollar. The euro may extend losses to around $1.33 into the third quarter, UBS's Yu said.

The common currency's decline from last week's high of $1.3993, the strongest since October 2011, has formed a bearish reversal pattern that should lead to further losses, according to Citigroup analysts including Shyam Devani in London.

"Not only did we see a daily reversal at the trend highs but also a bearish weekly reversal," the analysts wrote. The euro "now looks vulnerable and is in danger of trending down over the weeks ahead," they said. Citigroup is the world's biggest currency trader, according to a Euromoney Institutional Investor Plc survey released last week. UBS is the fourth.

The euro has gained 5pc in the past 12 months, the third-best performer of 10 developed-nation currencies tracked by Bloomberg Correlation-Weighted Indexes. The dollar weakened 1.5pc and the yen fell 1.9pc. (Bloomberg)

Irish Independent