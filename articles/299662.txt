Holy superheroes, Batman! Former ‘Game of Thrones’ actor Jason Momoa is set to play Aquaman in the highly-anticipated film ‘Batman Vs. Superman: Dawn of Justice.’ Khal Drogo would be so pleased to hear of this.

Batman vs. Superman: Dawn of Justice is just looking better and better. Jason Momoa, better known as Khal Drogo from Game of Thrones, is set to don the suit of Justice League’s Aquaman!

‘Justice League’: Jason Momoa To Play Aquaman

In the best news you’ll hear all day, Jason Momoa is going to play Aquaman in 2015’s Batman vs. Superman: Dawn of Justice, HitFix reports.

Jason will join the ranks of Batman (Ben Affleck), Superman (Henry Cavill) and Wonder Woman (Gal Gadot) in the Superman sequel.

This casting news just fuels the buzz surrounding another upcoming superhero film: Justice League. Aquaman is a crucial part of the League, along with Green Lantern and The Flash.

Aquaman’s powers include controlling the high seas as well as super-strength and super-speed, among others.

Jason is best known for his recent role on Game of Thrones as Khal Drogo. The fan-favorite character co-starred opposite Emilia Clarke (more formerly known as Khaleesi) until he was killed off in season one.

Jason will co-star alongside Laurence Fishburne, Jesse Eisenburg, Holly Hunter and Amy Adams in the Superman sequel. Batman vs. Superman: Dawn of Justice is set to hit theaters May 6, 2016.

HollywoodLifers, what do you think about Jason as Aquaman? Are you excited for Batman vs. Superman: Dawn of Justice? (We are.) Are you missing Khal on Game of Thrones? (We are.) Drop us a comment below!

— Avery Thompson

Follow @avery__thompson

More Movie News: