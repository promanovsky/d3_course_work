You do not want to go to war with Amazon.com. The retail giant will crush you like a tiny, tiny microbe and make you rue the day you were born.

The latest evidence: Amazon has escalated its battle against book publisher Hachette. Now Amazon won't allow you to pre-order any Hachette books, the publisher confirmed to The Huffington Post on Friday. That means you cannot buy the paperback version of Brad Stone's Amazon exposé "The Everything Store: Jeff Bezos and the Age of Amazon."

When you go to buy Stone's book, this is what you see:

If you want to pre-order that book, which lays out many other instances of the retail giant's down-and-dirty business tactics you'll have to go to Barnes and Noble.

"We are doing everything in our power to find a solution to this difficult situation," Sophie Cottrell, a spokesperson for Hachette, told HuffPost.

This is just the latest move in Amazon's fight against the publisher. Due to what is thought to be a problem in contract negotiations, Amazon has been aggressively discouraging customers from buying Hachette's books. One way it's been doing that is by telling customers that the publisher's books will take weeks to ship.

This is what you see if you try to buy Stephen Colbert's book, which is published by Hachette:

Amazon says that this book will take "2 to 5 weeks" to ship, when other books generally ship within days. Barnes and Noble's site says the book "usually ships within 24 hours" and offers same day delivery in Manhattan.

The problem is that Amazon is responsible for such a huge proportion of book sales that it really could make Hachette suffer. In fact, Amazon reportedly controls about one third of book sales. This isn't Amazon's first time strong-arming a publisher. In 2010, the company temporarily pulled books published by MacMillan because of a fight over e-book pricing.

Amazon has a history of aggressively going after its competition. In 2009 the company bought the shoe site Zappos for more than $900 million after launching an Amazon shoe site specifically to undercut Zappos. It deployed the same tactic on Diapers.com, aggressively selling cheap diapers until the website had no choice but to sell.