New research claims people are less stressed on the job and more stressed in the home environment, and men are happier at home than at work compared to women.

The study, by Penn State researchers, claims even parents, mothers and fathers, had lower stress at work despite the common belief that home is where we all relax.

"Our findings suggest that telling people to quit or cut back on work in order to resolve their work-family conflicts may not be the best long-run advice," writes Sarah Damaske, assistant professor of Labor & Employment Relations, Sociology, and Women's Studies and Research Associate Population Research Institute, The Pennsylvania State University School of Labor.

The findings are the opposite of major national surveys and other studies that indicate the major source of stress for people is work and Damaske notes some of those findings in her column on the study: A 2005 Work and Families Institute study found that almost 90 percent of workers felt they either never had enough time in the day to do their job or that their job required them to work very hard. A Pew Report from 2013 found that more than half of all working moms and working dads experience work-family conflict. One-third of working moms and dads feel rushed on work-days, and almost 50 percent of working dads (and 25 percent of working moms) say they don't have enough time with their children.

The Penn State study measured cortisol levels of working people. Cortisol is a major biological marker of stress.

"These low levels of cortisol may help explain a long-standing finding that has always been hard to reconcile with the idea that work is a major source of stress: People who work have better mental and physical health than their non-working peers, according to research," writes Damaske. She states mothers who work full time and steadily across their twenties and thirties report better mental and physical health at age 45 than mothers who work part-time, who stay at home, or who experience repeated bouts of unemployment.

"We found that women as well as men have lower levels of stress at work than at home. In fact, women may get more renewal from work than men, because unlike men, they report themselves happier at work than at home. It is men, not women, who report being happier at home than at work," she said.

So where does that leave today's telecommuterwho works at home? Damaske said telecommuting is actually a viable solution to relieving the home stress issue.

"Telecommuting, paid sick days, paternity and maternity leaves, are all policies that make it easier for workers to retain the health benefits of employment and for companies to retain the financial benefits of having loyal employees rather than having to deal with constant job turnover."

TAG Stress, work, Life, Home, Study, Telecommuting

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.