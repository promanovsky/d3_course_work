US drug major Pfizer today shelved its USD 117-billion takeover bid of AstraZenaca on the day of expiry of the offer as the British pharmaceutical company remained firm in opposing the deal.

"Following the AstraZeneca board's rejection of the proposal, Pfizer announces that it does not intend to make an offer for AstraZeneca," the company said in a statement.

Commenting on the development, Pfizer Chairman and CEO Ian Read said: "We continue to believe that our final proposal was compelling and represented full value for AstraZeneca based on the information that was available to us."



He further said the pursuit of this transaction was a potential enhancement to the company's existing strategy.

On May 18, 2014, Pfizer had announced that it had made a final proposal to AstraZeneca to make an offer to combine the two companies.

The US firm had urged "supportive AstraZeneca shareholders to urge the AstraZeneca board to begin substantive engagement with Pfizer and extend the period for such talks prior to the May 26 deadline for making an offer".

Pfizer Inc, the New York-based maker of blockbuster drugs such as Lipitor and Viagra, had made a final offer of 55 pounds a share to Astrazeneca on May 18, which was 15 per cent higher than its last bid made on May 2.

Reacting to the final proposal, AstraZeneca had said it undervalued the company and "its attractive prospects and has been rejected by the board of AstraZeneca".

Pfizer had earlier approached AstraZeneca on January 5 and April 26 with a proposal to acquire the company for USD 100 billion. On May 2, the company had raised the offer to USD 106 billion.

Under the final proposal, Pfizer and AstraZeneca shareholders would have owned approximately 74 per cent and 26 per cent, respectively, of the combined company.

AstraZeneca's products include drugs to treat cancer, gastrointestinal and cardiovascular and metabolic diseases.