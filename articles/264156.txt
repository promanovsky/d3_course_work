It took Apple Inc. (NASDAQ:AAPL) longer than a day to acknowledge reports that Australian iPhones, iPads and Mac computers had been remotely locked by a hacker going by “Oleg Pliss” and held for ransom. The statement denied that iCloud was breached, and instead implied that users put themselves at risk by having a username and password from another website as their Apple ID.

“Apple takes security very seriously and iCloud was not compromised during this incident,” Apple said in a statement. “Impacted users should change their Apple ID password as soon as possible and avoid using the same user name and password for multiple services. Any users who need additional help can contact AppleCare or visit their local Apple Retail Store.”

Apple didn’t give any information about who or what caused the attack, but the statement supports the suspicion of many security professionals that hackers simply used account information leaked from the data breach of another company. Many people use the same username and password for several online accounts, and “Oleg Pliss” seems to have just taken that data, used it to access iCloud, and used the “Find My iPhone” feature to lock the phone and send the ransom note.

One blogger demonstrated exactly how to do this on his personal blog.

What's puzzling is that outside of a few cases in Canada, the U.S. and New Zealand, nearly all reported cases of this attack came from Australia. A thread on the Apple Support forum has morphed into an investigation looking for commonalities between the victims, but as yet a consensus has not been reached. Several hacked users denied that their Apple ID information was used on other sites.

Some have speculated that the information came from the eBay or Adobe data breaches, but that wouldn’t really explain why the victims are predominantly from Australia.

If hackers have access to iCloud accounts, they also have access to any photos, videos, documents or messages that a victim may have stored there.

The attack is preventable by using a security code to lock every device and computer, and users can be even more protected with two-factor identity verification. The “Oleg Pliss” attack is just the latest reminder for users to not use the same password for multiple online accounts.