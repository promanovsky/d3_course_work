Nest wants your home devices to work together and check up on each other – even when you might not know it.

The Google-owned maker of digital thermostats and Nest Protect smoke alarms announced Tuesday the Nest Developer Program, designed to give developers the tools to make products that can interact with Nest's smart household appliances.

Nest has long planned on expanding its array of products into an interconnected home. Now, for example, your car can communicate with your thermostat while you're driving home to turn on heating before you get home. "What we’re doing is making it possible for your Nest devices to securely interact with the things you already use every day. Things like lights, appliances, fitness bands and even cars," writes Nest co-founder Matt Rogers in a blog post.

Companies that have released "Works with Nest" features include Mercedez-Benz, Jawbone, Whirlpool, Logitech, and, of course, Google. People will be able to give a simple "OK Google" command to verbally adjust their Nest thermostats with certain devices. In another example, by working with the Nest thermostat once it goes into "away mode," LIFX lightbulbs can periodically turn on and off to make it look as though someone is home when you're on vacation.

The Nest Developer Program will also allow developers to work across platforms, making features that work with iOS, Android, and the Web.

This announcement comes in the wake of Apple's recently launched HomeKit, which aims to let users control smart home devices through their iPhone. Mr. Rogers sees this as compatible with the Nest Developer Program.

"This is less about 'how do you get devices on your Wi-Fi network,' " Rogers told Ars Technica. "This is 'after you get them on the Internet, what do you do with them after that?' "

Google bought Nest in January for $3.2 billion. That acquisition raised eyebrows from privacy advocates as Nest products collect data about users' homes. For its part, Nest has previously stated that it shares no user information with parent-company Google. And it is now adding that it does not send personal information to developers who are working with Nest and does not allow them to maintain users' data for more than ten days, according to a company release.

Still, Google is well known for its targeted advertising and does not think using household appliances to target users with ads is out of the question. In a December letter to the Securities and Exchange Commission, Google outlined possible plans to place ads in the home.

"A few years from now, we and other companies could be serving ads and other content on refrigerators, car dashboards, thermostats, glasses, and watches, to name just a few possibilities," the letter states. This reflects the trend in the so-called Internet of Things – everyday objects and appliances connected to the Internet – which is expected to grow as an industry in coming years.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

It was confirmed last week that Nest was purchasing home-security start-up Dropcam for $555 million.

Nest's announcement also prefaces the beginning of Google's annual I/O Conference to be held in San Francisco this week. It has been anticipated that Google could mention updates to its plans for home integration. Although Nest has operated more or less on its own since joining Google, the developers conference could serve as a major stage for consumers to learn more about the new developer program.