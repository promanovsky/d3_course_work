Messaging startup Snapchat has settled charges with US regulators accusing it of deceiving consumers by promising that photos sent on its service disappeared forever after a certain period of time.

Under the terms of the settlement, Snapchat's privacy practices will be monitored by an independent professional for the next 20 years.

"If a company markets privacy and security as key selling points in pitching its service to consumers, it is critical that it keep those promises," Federal Trade Commission Chairwoman Edith Ramirez said in a statement.

According to the FTC, photos sent on Snapchat could, in fact, be saved by recipients using several methods.

The FTC also said Snapchat collected certain personal information about its users without their consent and failed to take proper security measures, resulting in a breach in which hackers compiled a database of 4.6 million Snapchat user names and phone numbers.

In a post on its official blog on Thursday, Snapchat acknowledged that it had made mistakes, which it attributed to being a young company focused on developing its product.

"While we were focused on building, some things didn't get the attention they could have. One of those was being more precise with how we communicated with the Snapchat community," the company said.

Snapchat is among a new crop of popular mobile apps that competes with established Internet services such as Twitter Inc and Facebook Inc. According to media reports last year, Snapchat turned down a $3 billion acquisition offer from Facebook.

The settlement with the FTC is subject to a 30-day public comment period, after which the commission will decided whether to make the consent decree final.

Copyright: Thomson Reuters 2014