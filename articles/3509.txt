Rude, cocky and belligerent: Justin Bieber argues with lawyer, disses Usher and pretends to fall asleep during deposition



Anyone who has been feeling sympathetic towards tearaway Justin Bieber will surely rethink their loyalties after watching the video of his deposition.

The 20-year-old singer was called to Miami on Thursday for a formal hearing regarding a lawsuit which claims he instructed his bodyguards to confront a photographer who was then assaulted.

Rather than take the allegations seriously, Justin was rude and arrogant towards the paparazzo's lawyer, winked at the camera recording the deposition and at one stage, pretended to fall asleep, in videos obtained by TMZ.

Scroll down for video



Rude: Justin Bieber was called to Miami for a deposition regarding a lawsuit that has been put against him by a photographer who claims he was assaulted by his bodyguards but was rude and cocky towards the opposition lawyer



The Believe hitmaker is being sued by Jeffrey Binion, who claims one of the star's minders choked him and stole his camera equipment in June 2013.

It has been claimed Justin ordered his bodyguard to seize Jeffrey's memory card after he tried to snap a candid picture of the teen heartthrob leaving a recording studio.

Letting rip: Justin lost his cool when asked about Selena Gomez and said 'Do not ask me about her again'

Not happy: Justin gave a series of barbed responses to the photographer's lawyer during the deposition

The star rocked from side-to-side on his chair in a non-plussed fashion, while telling the photographer's lawyer, 'I don't have to listen to anything you have to say.'

He was then asked: 'Do you remember being in Australia ever?' to which he shrugs and replies: 'I don't know if I've been to Australia. Have I been to Australia?'

Cocky: At one stage Bieber pretended to fall asleep because he didn't want to answer a question



Smirk: Pleased with his performance, Justin couldn't resist a smirk at his deposition



Justin also fumed after being asked about his beau Selena Gomez. The lawyer asked: 'Have you ever talked to Selena Gomez and discussed your feelings about paparazzi?'

To which the singer put his head in his hands, sighed, and said: 'Don't ask me about her again. Don't ask me about her again,' repeatedly.

Bieber's lawyers permitted him to take a break at this point and he eventually ripped his microphone off before he and his lawyer storm out.



Bieber and Gomez have since reunited and on Sunday he called her 'my baby' and dedicated his song As Long As You Love Me to her at SXSW, according to GossipCop.



Under fire: The Believe hitmaker is being sued by Jeffrey Binion, who claims one of the star's minders choked him and stole his camera equipment in June 2013

Unrepentant: Bieber told the photographer's lawyer, 'I don't have to listen to anything you have to say'

The pair later returned to complete the deposition, but in a series of barbed responses Bieber allegedly said: 'What is this, 60 Minutes?' and later: 'I don't know Katie Couric, you tell me?'

Later, the lawyer said, 'Do you know an individual by the name of Usher?' Justin replied: 'Yes, Usher, that sounds familiar,' sarcastically.

When asked to watch footage of the paparazzi, which the lawyer referred to as a 'film', childish Bieber quipped ‘Is this a film?’ so much that they have to stop the tape.







Childish: When asked to watch footage of the paparazzi, which the lawyer refers to as a 'film', childish Bieber quipped, 'Is this a film?'

He also looked directly into the camera and winked, undermining the seriousness of the meeting.

When asked 'Do you have a prescription for Xanax?', Bieber replied with a 'No sir' so quickly that his lawyer has to caution him to allow him the chance to object.

When Bieber didn't want to answer a question he closed his eyes as if he was sleeping, while the lawyer spoke in vain.

However, the funniest part of the video came when Bieber, mixing up the words 'detrimental' and instrumental, said: 'I was found on Youtube. I think that I was detrimental to my own career.'

This came in response to a question about Usher’s influence on his career with Bieber at first unwilling to acknowledge Usher's role in his fame before later admitting Usher was instrumental in his fame.



When asked, 'Is Usher a close friend?' he said 'Yes' and replied 'I guess yes', when asked if he is a confidante.







Brat: Anyone who has been feeling sympathetic towards tearaway Justin will surely rethink their loyalties after watching the video of his deposition

Detrimental: The funniest part of the video came when Bieber, mixing up the words 'detrimental' and instrumental, said: 'I was found on Youtube. I think that I was detrimental to my own career'













