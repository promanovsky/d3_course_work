Hey, Justin Bieber: It's never too late to go back to school.

The Canadian pop star's performance in a deposition last week suggests Bieber might want to reconsider his decision to cease his education following his graduation from high school in 2012 (he completed his final two years via mail-order courses).

In video obtained by TMZ, Bieber is surly and sulky and at times seemingly ill-educated in the deposition regarding his involvement in a lawsuit claiming that the singer's bodyguard was instructed to beat up a photographer.

In the highlight moment, the prosecutor asks Bieber if the pop star Usher was "instrumental in starting your career."

The Biebs' curt response: "I was found on YouTube. I think I was detrimental to my own career."

Which may have been the biggest Freudian slip in Bieber's career to date.

After the singer's legal counsel point out the grammatical error, Bieber repeats the word "instrumental."

And Bieber may be having memory problems these days because he conveniently forgets in the deposition that Usher more or less launched his career several years back.

At one point in the proceedings, a lawyer asks Bieber, "Do you know Raymond Usher IV?" To which Bieber replies, "No."

In fact, Bieber only acknowledges Usher after continued prodding and asked specifically if he knows "an individual entertainer by the name of Usher."

Bieber's response: "That sounds familiar."

Eventually, Bieber conceded that Usher was a friend who was "helpful" in the early stages of his pop career. But the low point of Bieber's deposition was still to come.

At one point, a visibly perturbed Bieber responds "yeah" so quietly that the off-camera court reporter has to ask him to speak up because she can't hear him.

When the woman asks him if he said "yes or no," Bieber snaps at her, "Yes and no are f–-ng pretty different."

At which point Bieber's lawyer wisely hustles his client out of the room.

Time to start hitting the books again, Justin. And a refresher course in Canadian common courtesy might be in order.