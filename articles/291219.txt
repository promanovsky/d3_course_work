Former Apple CEO Steve Jobs was apparently opposed to the launch of the iPad Mini, believing that there was no market for the downsized tablet.

His successor Tim Cook was the driving force behind the device, advising his colleges to proceed despite Jobs's reservations, the New York Times reports.



Apple board member and Disney chief Robert A Iger told the newspaper that Cook "thought the world would love a smaller and less expensive tablet", while his predecessor was against the project.

Meanwhile, Apple design chief Jony Ive claims that nothing has changed at the company since Cook succeeded Jobs as CEO.

"Honestly, I don't think anything's changed. People felt exactly the same way when we were working on the iPhone," he said.

Apple is expected to launch its next iteration of the iPhone this autumn alongside the much-rumoured iWatch.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io