There was yet another revelation regarding the lengths the NSA will go to spy on its perceived enemies on Friday. According to an article published by Bloomberg, the NSA has known about the recently publicized Heartbleed encryption bug for more than two years. The sources for the article claim the NSA discovered the Heartbleed flaw soon after the encryption algorithm was created, but chose not to reveal the vulnerability so they could exploit it for intelligence reasons.

Statement from NSA

National Security Council spokeswoman Caitlin Hayden spoke to the media last Friday and categorically denied the claims in the Bloomberg article. “Reports that NSA or any other part of the government were aware of the so-called Heartbleed vulnerability before April 2014 are wrong.”

Michael Zimmerman’s Prentice Capital had an excellent year Prentice Capital's Long/ Short Equity Fund was up 26% net for the fourth quarter, bringing its full-year return to 53.6% for 2020. In his fourth-quarter letter to investors, which was reviewed by ValueWalk, Michael Zimmerman said the development of COVID-19 vaccines, continued easy money and clarity in the election drove a risk-on environment. Q4 2020 Read More

Hayden continued, “When Federal agencies discover a new vulnerability in commercial and open source software — a so-called “zero day” vulnerability because the developers of the vulnerable software have had zero days to fix it — it is in the national interest to responsibly disclose the vulnerability rather than to hold it for an investigative or intelligence purpose.”

Hayden closed her statement with vague reassurances, citing proven toothless watchdog policies. “In response to the recommendations of the President’s Review Group on Intelligence and Communications Technologies, the White House has reviewed its policies in this area and reinvigorated an interagency process for deciding when to share vulnerabilities. Unless there is a clear national security or law enforcement need, this process is biased toward responsibly disclosing such vulnerabilities.”

Zero day vulnerabilities

The Bloomberg article claims the NSA maintains a list of software bugs and holes it has discovered, the “zero day” list, which can be exploited to gain access to supposedly secure networks. The sources claim the NSA only releases zero day information begrudgingly and after some time has passed, and most importantly only when they are sure they have other means of breaking into secure networks. The agency justifies this egregious violation of law and the U.S. Constitution as a necessary part of national security efforts.

According to the Wall Street Journal, during recent discussions at the White House among senior officials regarding the issue, “People familiar with the discussions said the push to disclose and address zero day vulnerabilities was a major point of contention for the intelligence agencies, which fought the recommendation.”