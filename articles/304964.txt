Melanie Eversley

USA TODAY

A Canadian woman is being hailed for taking her own health into her hands by using her smartphone to record herself having what doctors believe was a stroke.

Back in April, doctors told 49-year-old Stacey Yepes of Thornhill, Ontario, that the numbness and slurring of words that suddenly came over her while she was watching TV were due to stress. Tests showed no signs of anything physically wrong that could have caused the symptoms. The legal secretary tried to follow doctors' advice to relax, but deep down she was convinced something more was wrong.

"It's true that I hadn't slept well the last few days and that I have a stressful job," Yepes said. "But I was pretty sure that the symptoms that I had experienced were due to a stroke."

So when the symptoms returned while Yepes was behind the wheel of her car, she pulled out her phone and began recording. The video has gone viral and had received almost 67,000 hits on YouTube alone by 9:30 p.m. ET.

"I think it was just to show somebody because I knew it was not stroke related," Yepes told the CBC. "I thought if I could show somebody what was happening, they would have a better understanding."

In the nearly 90-second video that Yepes created, the left side of her face is visibly droopy and she demonstrates that she is unable to perform even a simple task like touching her nose, her hand wavering several inches from her face. Her words are slurred.

"My tongue feels very numb," she says in the video. "I don't know why this is happening to me."

Doctors later were able to use the video to diagnose what they believe was a transient ischemic attack, also known as a TIA or mini stroke.

"In all my years treating stroke patients, we've never seen anyone tape themselves before," Dr. Cheryl Jaigobin, stroke neurologist at the Krembil Neuroscience Centre of Toronto Western Hospital, told the CBC. "We were all touched by it."

As with other strokes, a TIA is caused by a blood clot, the only difference being that the blockage in a TIA is temporary, according to the American Stroke Association. For that reason, it is considered a "warning stroke" that more trouble might be on the horizon, the association says.

"Strokes can affect people of any age even if they have few risk factors, so it's very important to be aware and know the signs of a stroke," Jaigobin said.

The five signs of stroke are sudden weakness or numbness, trouble speaking, vision problems, headache and dizziness, according to the Heart and Stroke Foundation, a Canadian organization.