Dubai: Emirates airline pointed to lower fuel prices and new partnerships on Thursday when it reported a 42.5 per cent increase in annual profit to Dh3.3 billion ($887 million).

Reported revenue at the Middle East’s largest carrier was a record high of Dh82.6 billion for the year ending March 31, surpassing Dh80 billion for the first time in the airline’s history.

Shaikh Ahmad Bin Saeed Al Maktoum, president of Dubai Civil Aviation and chairman and chief executive of Emirates airline and Emirates Group, said lower fuel prices and the year-old Qantas partnership had contributed to part of the airline’s growth.

While the fuel bill increased by 10 per cent to Dh30.7 billion, the airline said the average jet fuel prices were slightly lower than the previous year. Emirates’ fleet size increased by almost 10 per cent with the addition of 24 new aircraft this past year. It added 16 Airbus A380s and six Boeing 777-800ERs and two Boeing 777 Freighters.

Qantas partnership

Emirates’ latest financial report card covers the first year of its partnership with Australian flagship carrier Qantas, which was launched in April last year.

“The partnership with Qantas is doing well not only from the airline side but also for having a new customer at Dubai International,” Shaikh Ahmad, who is also Chairman of Dubai Airports, said.

Regional revenues

East Asia and Australasia contributed the highest revenue by region delivering Dh23.8 billion, a 14 per cent increase. Australasia became one of the biggest growth markets at Dubai International in 2013, increasing 33.4 per cent in 2013.

Gulf and Middle East revenue increased 17 per cent to Dh8.3 billion. Europe contributed Dh23.4 billion in revenue, up 16 per cent, while Africa was up 15 per cent to Dh7.7 billion. The America’s increased 11 per cent to Dh9.2 billion and West Asia and the Indian Ocean grew 3 per cent to contribute Dh8.3 billion in revenue.

Part of the Emirates Group’s record Dh22 billion in investments this past fiscal year went to the airline’s record $90 billion fleet order at the Dubai Airshow for 150 Boeing 777 and 50 A380s, Shaikh Ahmad said.

More passengers carried

Carrying 44.5 million passengers, an increase of 13.1 per cent, passenger load factor was slightly at 79.4 per cent, while seat capacity by Available Seat Kilometres (ASK) rose by 15 per cent.

Shaikh Ahmad attributed the drop in load factor to increases in seat capacity and tough global economic conditions.

Emirates airline raised Dh12 billion in capital that was primarily utilised for its fleet expansion. Eight of the aircraft delivered in the 2013-2014 fiscal year were funded through two corporate bonds that raised Dh6.4 billion.

Cash flow dropped 13.1 per cent to Dh16.6 billion, however, Shaikh Ahmad said the airline remains in a comfortable position to fund its expansion plans.

Responding to a question on the airline’s further capital needs in 2014, Shaikh Ahmad said it would look to a sukuk or bond if the market was good. However, he added that “maybe we don’t need any [this year].”