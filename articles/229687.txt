The world's first drinkable SPF has arrived.

Harmonised H20 UV provides users with up to 30 percent protection, helping sunbathers to soak up the rays for longer without fear of getting burned, UK MailOnline reported.

According to U.S. company Osmosis Skincare, the product's liquid molecules cancel out 97 percent of UVA and UVB rays after vibrating on the skin upon being ingested.

Priced at $29 for a 100 ml bottle, the liquid sunscreen boasts of being the world's first one. It is currently on sale.

There are two varieties available - "tanning" and "non-tanning," the former allows users to achieve a tan while being protected from harmful sun rays.

On its website, the medical skincare brand advises, "Take 2ml every 4 hours while in the sun (preferably with 2+ oz. of water)."

"Wait 1 hour before exposure to the sun. Monitor sun exposure carefully. Take second dose if still in sun 3 hours after first dose."

"For extended intense exercise outdoors or if taking sun-sensitizing medications, use alternate protection after 30-40 minutes."

Dr. Ben Johnson, who founded the company, said in his blog, "If 2 mls are ingested an hour before sun exposure, the frequencies that have been imprinted on water will vibrate on your skin in such a way as to cancel approximately 97 percent of the UVA and UVB rays before they even hit your skin."

"This results in coverage for approximately three hours," he said. "This is similar to the amount of UV reflection created by SPF 30 titanium/zinc sunblocks but distinctly better than UVB chemical sunscreens which prevent certain damage that leads to the visible/painful/inflammation reaction we identify as sun damage."

Although none of the dermatologist bodies have endorsed Harmonised H20 yet, testimonials for the product are listed on the company's website.

"I tested the UV Protection Harmonized Water (my skin burns in 15 minutes w/o sunscreen so i was nervous) SUCCESS!! I was outside for 2 hours with NO sunscreen during peak hours and wasn't even pink!" one, submitted via Facebook, said.

"My year and a half year old drinks it as well and hasn't burned once this summer and is outside everyday! Thank You, Thank You for this product!"

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.