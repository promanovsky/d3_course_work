A man suffering from the deadly Middle East Respiratory Syndrome (MERS) is doing better today, authorities said.

The victim is hospitalized at the Community Hospital in Munster, Ind., which is the first U.S. facility to admit a patient with the deadly virus.

The patient, whose name is being kept confidential, remains hospitalized in good condition and is improving each day, according to a statement from the Indiana State Department of Health (ISDH).

As of Saturday, no other cases of MERS have been identified, according to ISDH.

State and federal health officials confirmed the first U.S. case of the virus on Friday. The patient, a male healthcare worker, had traveled to Riyadh, Saudi Arabia, and began exhibiting symptoms upon his return to the United States, they said.

"We are very pleased the patient is improving and no other cases have been identified at this time," said State Health Commissioner William VanNess II, M.D. in the statement.

"The individual has received excellent care while at Community Hospital in Munster. The swift diagnosis and precautionary measures taken have undoubtedly greatly helped reduce the risk of this potentially serious virus spreading," he said in the statement.

The Indiana State Department of Health is working with the hospital, the Centers for Disease Control and Prevention (CDC) and others to monitor the situation and prevent the spread of the virus.

Since symptoms of MERS may take up to 14 days to occur, staff members at the hospital who had direct contact with the patient prior to the patient being placed in full isolation have been taken off duty and placed in temporary home isolation, the statement said.

Those individuals are being closely monitored for any signs or symptoms of the virus and will be allowed to return to work once the incubation period is over and they have confirmed negative laboratory results. There have been no reported cases of people without symptoms transmitting this virus.

"The patient is in full isolation and presents no risk to patients, staff or the general community," said Don Fesko, CEO of Community Hospital in the statement. "We are thoroughly prepared to handle respiratory infections. We continue to work closely with the CDC and State Health Department and are following every recommendation. Safety is our top priority."

For questions about MERS, please call the Indiana State Department of Health hotline at 1-877-826-0011, which is being answered daily, 8 a.m. to 4:30 p.m. EDT. A voicemail system is available for callers to leave a message during off hours. All calls will be returned first thing the following morning.

To keep you and your family healthy by reducing your risk of disease, follow CDC's tips:

Wash your hands often with soap and water for 20 seconds, and help young children do the same. If soap and water are not available, use an alcohol-based hand sanitizer.

Cover your nose and mouth with a tissue when you cough or sneeze, then throw the tissue in the trash.

Avoid touching your eyes, nose, and mouth with unwashed hands.

Avoid close contact, such as kissing, sharing cups, or sharing eating utensils, with sick people.

Clean and disinfect frequently touched surfaces, such as toys and doorknobs.

chicagobreaking@tribune.com | Twitter: @ChicagoBreaking