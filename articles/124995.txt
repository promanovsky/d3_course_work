NEW YORK (TheStreet) -- Yahoo! (YHOO) is soaring on Wednesday after reporting better-than-expected earnings and on positive growth in Alibaba Group Holding, a company in which it holds 24% interest.

Just after market open, shares had spiked 6.9% to $36.56.

Over the three months to March, the company reported first-quarter earnings of 38 cents a share, a penny higher than analysts surveyed by Thomson Reuters had expected. Revenue of $1.08 billion was 0.9% higher year over year and beat analysts' estimates by $10 million.

In a post-earnings conference call after the bell Tuesday, Yahoo! said Chinese e-commerce company Alibaba had seen 66% revenue growth in its fourth quarter to $3.06 billion. A quarter earlier, revenue growth was 51%.

Alibaba is expected to file for an initial public offering on U.S. markets later this year.

Must Read: Warren Buffett's 10 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates YAHOO INC as a Buy with a ratings score of B+. TheStreet Ratings Team has this to say about their recommendation:

"We rate YAHOO INC (YHOO) a BUY. This is driven by a few notable strengths, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its solid stock price performance, largely solid financial position with reasonable debt levels by most measures, reasonable valuation levels, expanding profit margins and good cash flow from operations. We feel these strengths outweigh the fact that the company has had somewhat disappointing return on equity."

You can view the full analysis from the report here: YHOO Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.