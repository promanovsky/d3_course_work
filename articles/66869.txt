Since reawakening from hibernation on January 20, a fridge-sized robot designed to make the first-ever spacecraft landing on a comet has sent back the first image of its destination, according to NASA.

The space agency's comet-hunter Rosetta spacecraft used the Optical, Spectroscopic and Infrared Remote Imaging System wide-angle camera and narrow-angle camera to capture the amazing photo on March 20 and 21, the State-Colum reported.

Captured from approximately three million miles away, the photos, taken from a sequence of exposures of 60 to 300 seconds, were snapped by the comet hunter of the comet 67P/Churyumov-Gerasimenko.

"According to the space agency's news release, the photos are part of multiple weeks of activities designed to ready Rosetta's instrument for in-depth examination of 67P/Churyumov-Gerasimenko," the State-Column reported. "So far, the spacecraft has journeyed through space towards its destination comet for a decade."

In August 2014, the robot is finally expected to land at the comet.

The comet's "catch" is the primary mission of Rosetta's spacecraft in 2014, which will then accompany it into the interior system.

"During this time, the spacecraft will observe 67P/Churyumov-Gerasimenko's nucleus and coma from close range, determine the increase in cometary activity during perihelion and deploy Philae to make the very first controlled landing on a comet," the State-Column reported.

After more than three years of deep space hibernation, in a key phase of a billion-dollar mission launched over a decade ago, the 220-pound lander was revived, Agence France-Presse reported.

The spacecraft had re-established contact with Earth, and an "initial signal was received at 3.00 pm (1400 GMT) today at mission control in Cologne, Germany," France's National Centre for Space Studies (CNES), in Paris, said.

"My controllers say that I am in quite good condition after 39 months of hibernation," tweeted a Twitter account set up for the robotic lander.

"My new software has uploaded perfectly. I'll be taking a little rest now! Talk to you soon."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.