Alex Trebek broke a world record on an episode of Jeopardy! which aired on Friday. The 73-year-old was awarded with a certificate for Most Game Show Episodes Hosted by the Same Presenter in honour of his achievement.



Alex Trebek is a Guinness World Record breaker!

Trebek has been on the popular game show since 1984, hosting 6795 episodes to date. The show is currently in its 30th season and its 31st will begin in September. Trebek will of course be continuing as host, simply increasing his record. Incredibly for such a long running show, Trebek is Jeopardy!'s second host. The first was Art Flemming who hosted from 1964-1979 before the show was cancelled. Five years later it was Trebek who stepped in when the show was re-launched.

Read More: Pantomime Villain Arthur Chu Finally Loses on Jeopardy! [Video].

Trebek received his official Guinness World Record certificate on the episode and thanked Guinness for the award, adding his family were in the audience watching. He said, "Thank you very, very much. It's a tribute to the fact that I've lived this long and secondly to the fact that I've had the good fortune to be associated with a very popular programme, Jeopardy!. It's been more significant that I have my son and daughter here today to witness it."

Happy though he was to accept the award, Trebek was aware that it could easily be broken by another chat show host and even referenced Pat Sajak, the 67-year-old host of Wheel of Fortune. Speaking on the show, Trebek said, "This is the kind of record that can, and could, and probably will be broken by someone else. I'm thinking Pat Sajak is not that far behind and he's a lot younger than I am."



Trebek thinks Pat Sajak could beat his record with his hosting of Wheel of Fortune.

Trebek's contract, as the LA Times reports, with Jeopardy! ends in 2016 which has prompted rumours about his imminent retirement. Yet Trebek is merely "thinking about" retirement and has no immediate plans to leave the show. He did state, in an interview with the same newspaper last year, that when he does retire it will be a quiet affair. He said, "There won't be any fanfare. It'll be like the time I shaved my moustache on a whim. I'll just ask the director to leave me 20 or 30 seconds at the end of the program to say a few words and I'll say a few words and thank people and be on my way."

Trebek's Guinness World Record will expand of his already impressive collection of awards including five Emmy Awards for Outstanding Game Show Host and a Daytime Emmy Lifetime of Achievement Award in 2011.

Read More: Trebek's 'Magic Johnson' Reaction on Jeopardy! Is Priceless [Video].





Alex Trebek at the Daytime Emmy Awards in 2013.