Diabetes-related health complications, including heart attacks and stroke have substantially declined, according to a new report from the Centers for Disease Control.

The study, published in the New England Journal of Medicine, found over the last two decades, incidences of heart attacks and stroke among diabetics fell by more than 60 percent.

Additionally, the researchers confirm earlier reports of drastic declines in amputations and end-stage kidney failure. Amputations of the upper and lower legs, ankles, feet and toes declined by about half, while rates of end-stage renal failure plummeted approximately 30 percent.

The researchers say the drop is mainly attributed to better screening, medicines and care. The improvements came even as the number of U.S. adults with diabetes more than tripled in those 20 years.

"This is good news," Dr. Joel Zonszein, professor of clinical medicine at Albert Einstein College of Medicine and director of the Clinical Diabetes Center at Montefiore Medical Center, told CBS News. "It shows that in the last 20 years we are treating better and we can prevent some of the complications. But obviously the aim isn't to treat diabetes but to prevent diabetes." Zonszein was not involved with the study.

Diabetes is a disease in which sugar builds up in the blood. The most common form is tied to obesity, and the number of diabetics has ballooned with the rise in obesity. Today, roughly 1 in 10 U.S. adults has the disease, and it is the nation's seventh leading cause of death, according to the CDC.

The obese are already at higher risk for heart attacks and strokes. But diabetics seem to have more narrowing of their blood vessels -- a condition that can further foster those problems.

For the study, the CDC tallied complication rates from 1990 to 2010 for diabetics ages 20 or older.

During that time, the heart attack rate fell 68 percent, from 141 to 45.5 per 10,000 diabetics, even though the number of diabetes patients increased. The actual number hospitalized with heart attacks dropped from more than 140,000 to about 136,000. Stroke rate fell less dramatically -- but still declined by more than half, finishing at 53 per 10,000.

The study also found kidney failure rates have declined by 28 percent. But that wasn't true for all ages -- the number increased in patients 65 and older. This may suggest diabetics are living longer, or long enough to develop kidney disease.

The researchers also looked at deaths caused by dangerous levels of blood sugar, which they found dropped by 64 percent. In 2010, those deaths totaled 2,361.

"The prognosis for folks with diabetes has improved dramatically over the last two decades, at least for those with good access to care," said Dr. John Buse, a University of North Carolina diabetes specialist, said in an email. He was not involved in the study.

Zonszein added that today there are also more drug options for diabetes patients. He said 25 years ago, diabetics only had access to two medications and insulin. But today, there are 11 drugs on the market. Additionally, patients also have better access to education and public health services, such as smoking cessation and nutrition programs.

Insurance programs have also expanded coverage of blood sugar monitors and diabetes treatment. And in general, diabetics have a better chance of being diagnosed earlier and with milder disease than they did even a decade ago.

"When we treat Type 2 diabetes, it's not treating only high sugars. It's treating obesity and now we have medications that can lower sugar with weight loss. We treat high cholesterol with statins," said Zonszein. "It's very global and it's very complex, it's not just lowering sugar."