NEW YORK (Real Money) -- How big a deal is the Apple (AAPL) - Get Report-IBM (IBM) - Get Report deal? Wrong question. How smart is the Apple-IBM deal for Apple? Very.

I know IBM is rallying harder than Apple on its newfound affiliation with the best ecosystem in the world. That's all well and good, and perhaps Ginni Rometty can spin the earnings that are reported tomorrow with some Apple-infused excitement.

But, to me, this is all about IT -- meaning that the objections of the IT departments are finally being met from the ground up.

Intel Soars: What Wall Street Thinks

IBM, Apple Just Made an Historic Announcement

We all know what's been going on at companies in this country for years and years. Ingrained IT people have been reluctant to support Apple because Apple is a consumer company. They always want us to carry around many devices. They always plead that it is security that keeps the WINTEL system in place with its BlackBerry adjunct.

We are all sick of hearing it. We -- or if you are older, like me, our kids -- would not want to buy a WINTEL device unless someone put a gun to our heads, which is exactly what the Information Technology departments in almost all large companies have been doing for years and years.

IBM rebuts the objection. IBM is one of those shot-calling companies that shouldn't be calling the shots but just does. All of us have experienced, at one time or another, the brick wall that is IBM, which stands for: "You can't use Apple in this barren ecosystem."

Now IBM will stand for: "You have to retrain all of your people to support Apple, and we are no longer blessing the security issue against Apple."

Now, of course, the Hewlett-Packard (HPQ) - Get Report, Dell (DELL) - Get Report, Blackberry (BBRY) nexus has always provided a healthy discount to anything Apple. The cost of a PC and a phone from any of those companies is pretty much nil. They just want in to be able to sell you value-added products.

It will be interesting to see whether companies are willing to pay to buy all Apple systems for the enterprise. Would they rather give you a cheap HP or Dell desktop and pay for your BlackBerry? Or will they go all-in Apple and bet that it might actually be cheaper to buy you Apple hardware for the office and sync it with your iPhone?

It amounts to what Verizon (VZ) - Get Report and AT&T (T) - Get Report have to do. They would much rather sell you a cheap Samsung phone than have to pay full freight and subsidy for an iPhone.

Will the troops be able to clamor for the Apple ecosystem, or will they have to settle for an iPhone or a tablet or a personal computer that has to sync with the cheap stuff from Dell or HP?

That, to me, is the real question. Apple is not a Trojan horse company. It is not willing to give away anything to get an account -- not a PC, and not a phone. But the IT people will be besieged with requests to allow the Apple consumer be part of the overall system, and the companies may just have to buy hardware if IBM suggests they do.

So, to me, watch the wins. Watch the rip-outs. Watch the ground-up groundswell. That's what will tell you if we can really raise numbers for Apple.

Raise numbers for IBM off this? I just don't see it.

But it is hard to believe that numbers can stay the same for Apple.

And the numbers are all that matters, especially when you are dealing with a stock that has a price-to-earnings ratio of 15.

Action Alerts PLUS, which Cramer co-manages as a charitable trust, is long AAPL.

This article was originally published on Real Money at 7:20 a.m. on July 16.