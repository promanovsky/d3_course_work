The euro strengthened against its most major counterparts in European deals on Monday, as data showed that Eurozone inflation held steady in June, putting less pressure on the European Central Bank to act again immediately.

The flash estimate from Eurostat showed that Eurozone inflation remained at 0.5 percent in June. It forecast to rise slightly to 0.6 percent.

Inflation has been below the European Central Bank's target of 'below, but close to 2 percent' for the seventeenth consecutive month.

Core inflation that excludes energy, food, alcohol and tobacco, rose marginally to 0.8 percent in June from 0.7 percent in May.

After introducing a package of stimulus measures last month, the European Central Bank's policy makers meet later this week in Frankfurt. However, most economists expect any significant policy change from the bank at the meeting.

German retail sales fell unexpectedly in May from the prior month, official data revealed.

Retail sales were down 0.6 percent month-on-month in May, but slower than the 1.5 percent drop seen in April, provisional results from Destatis showed. This was the second consecutive fall in sales.

The euro hit a 4-day high of 138.51 against the yen, recovering from an early low of 138.09. If the euro extends its ascending trend, 139.00 is seen as its next possible resistance level. The euro-yen pair closed deals at 138.41 on Friday.

The data from the Ministry of Economy, Trade and Industry showed that Japan industrial output rose a seasonally adjusted 0.5 percent on month in May. That was shy of forecasts for an increase of 0.9 percent following the 2.8 percent contraction in April.

The euro extended rise to 1.3662 against the greenback, its strongest since June 9. The next possible resistance for the euro-greenback pair lies around the 1.375 mark. At last week's close, the pair was worth 1.3647.

Bouncing off from an early low of 0.8003 against the pound, the euro hit a 4-day high of 0.8026. The pair was worth 0.8005 when it closed deals on Friday. Next key upside target for the euro is seen around the 0.81 area.

U.K. mortgage approvals declined for the fourth consecutive month in May, data from the Bank of England showed.

The number of loans approved for house purchases fell to 61,707 in May, the lowest since June 2013, from 62,806 in April. Approvals were forecast to fall to 61,600.

The euro advanced to a 5-day high of 1.4534 against the aussie and a 4-day high of 1.4593 against the loonie, off early lows of 1.4471 and 1.4544, respectively. Further gains may lead the euro to resistance levels of around 1.475 against the aussie and 1.47 against the loonie.

The euro held steady against the NZ dollar with pair trading at 1.5622, after climbing to a 5-day high of 1.5635 earlier. The euro is poised to test resistance around the 1.58 mark.

On the flip side, the euro reversed from its early high of 1.2160 against the franc, falling to near a 3-month low of 1.2148. The euro may possibly find support around the 1.21 mark. The pair finished last week's deals at 1.2153.

Looking ahead, Canada GDP data for April and U.S. pending home sales for May are set for release in the New York session.

For comments and feedback contact: editorial@rttnews.com

Forex News