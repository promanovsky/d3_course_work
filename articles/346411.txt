Jennifer Love Hewitt is joining the cast of Criminal Minds.

Hewitt will join the series in the autumn and play Kate Callahan, an undercover agent whose hard work at the FBI has landed her a spot with the Behavioural Analysis Unit.

Dan Hallman/AP



Showrunner Erica Messer said: "We're thrilled to introduce a new special agent by adding the very talented Jennifer Love Hewitt to our amazing cast.

"Many of us on the show have enjoyed working with her over the years and look forward to again. We have exciting storylines planned for her character and the entire BAU team as we head into our 10th season."

The actress previously starred in CBS' The Ghost Whisperer, which ran for five seasons until 2010.

Hewitt also starred in the Lifetime series The Client List for two seasons, which was cancelled at the end of last year.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io