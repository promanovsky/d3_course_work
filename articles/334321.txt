Argentina is poised to miss a bond payment today, putting the country on the brink of its second default in 13 years, after a U.S. court blocked the cash from being distributed until the government settles with creditors from the previous debt debacle.

The nation has a 30-day grace period after missing the $539 million debt payment to seek an accord with a group of defaulted bondholders led by billionaire Paul Singer’s NML Capital Ltd. and prevent a default on its $28.7 billion of performing global dollar bonds. Both Argentina and NML have said that they’re open to talks.

A decade-long battle between Argentina and holdout creditors from the country’s $95 billion default in 2001 is coming to a head. The U.S. Supreme Court on June 16 left intact a ruling requiring the country to pay about $1.5 billion to holders of defaulted debt at the same time it makes payments on restructured bonds. Argentina last week transferred funds to its bond trustee to pay the restructured notes, only to have U.S. District Court Judge Thomas Griesa order the payment sent back while the parties negotiate.

The judge’s decision “closes Argentina’s options to finally force it to negotiate,” said Jorge Mariscal, the chief investment officer for emerging markets at UBS Wealth Management, which oversees $1 trillion. “Argentina should now stop using these delay tactics and get serious.”

Argentina took out a full-page advertisement in yesterday’s New York Times saying that Griesa favored the holdout creditors and was trying to push Argentina into default.

The ruling “is merely a sophisticated way of trying to bring us down to our knees before global usurers,” Argentina said. “But he will not achieve his goal for quite a simple reason: The Argentine Republic will meet its obligations, pay off its debts and honor its commitments.”

Wasting Time

Economy Minister Axel Kicillof has said the nation was complying with its obligations to bondholders when it sent the money to bond trustee Bank of New York Mellon Corp.

Argentina probably knew the payment was going to be blocked, according to Carlos Abadi, the chief executive officer of New York-based investment bank ACGM Inc.

“What is not so obvious is why they would put up such a show and waste precious time,” Abadi said. “It’s going to be incredibly complex to reach a comprehensive deal by July 30th, and this sideshow makes the goal even harder.”

After the Supreme Court’s decision, President Cristina Fernandez de Kirchner’s government had initially said that the country would skirt the ruling by swapping its bonds, which were issued under New York law, into new securities outside of U.S. jurisdiction. Officials later said they’d seek a negotiated settlement with the holdouts.

Not Welcome

In an e-mailed statement, the Economy Ministry said Griesa is abusing his power and acting outside of his jurisdiction as the restructured bonds are not part of the holdouts’ case.

“A judge is trying to impede a debtor from carrying out its obligations and creditors from getting paid,” the ministry said.

The price on Argentine’s 2033 bonds fell 1.55 cent to 83.69 cents on the dollar June 27, when the judge blocked the bond payment, according to prices compiled by Bloomberg. It traded 0.36 cent lower at 83.33 as of 8:03 a.m. in Buenos Aires.

Argentina’s next international bond payment is scheduled for Sept. 30, for securities due in 2038.

Carmine Boccuzzi, a lawyer for Argentina, told Griesa at the June 27 hearing that the country still hopes for a negotiated settlement.

Jay Newman, a money manager at Elliott’s NML Capital, said in an interview, “We are hoping to have the opportunity to negotiate with Argentina.”