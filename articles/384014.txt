Samsung is reportedly planning for a new variant of Galaxy smartphone dubbed Galaxy S5 F or Galaxy S5 Alpha.

PhoneArena quoting Korean Herald, which received information from certain industry sources, reports the Galaxy S5 F aka Galaxy S5 Alpha will sport a premium metal body. The smartphone will have a 4.7in Super AMOLED display.

Additionally, the aforementioned mysterious Galaxy smartphone will be powered by Exynos 5 octa-core processor and have 6mm thickness. The smartphone is referred as card-phone due to its slenderness. The device had received its Bluetooth certification this month.

"The new smartphone, known as the 'card-phone' for its slenderness, will soon be out on the Korean market and some foreign markets as well," added some industry sources.

The disclosure about Galaxy S5 Alpha surfaces after Samsung's recent launch of the LTE-A variant of the Galaxy S5, which is the first to have download speeds up to 225 Mbps and use Snapdragon 805 chip with a Krait 450 processing unit clocked at 2.5GHz. The LTE-A model sports a QHD screen with resolution measuring 2560 x 1440 pixels.

Related Samsung Galaxy Alpha with Metal Build to be Unveiled on 13 August

A couple of days ago, a report had surfaced suggesting Samsung Galaxy Alpha series smartphone, featuring a metal build, will be introduced on 13 August.

Samsung's new Alpha smartphone, which is expected to be released in September, will compete with Apple's upcoming iPhone 6, which is likely to arrive around the same time.