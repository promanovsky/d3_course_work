FILE - In this Thursday, May 19, 2011, file photo, actor Mickey Rooney poses during a portrait session in Los Angeles. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/Matt Sayles, File)

FILE - In this March 19, 1957, file photo, actor, singer and dancer Mickey Rooney, wearing spats and a pinstriped suit, performs a dance routine during rehearsal for the television show "George M. Cohan Story" in Hollywood, Calif. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/File)

FILE - Child star Mickey Rooney poses for a promotional photo at age 5 in this photo dated about 1925. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/File)

FILE - In this Sunday, March 7, 2010, file photo, Mickey Rooney, right, and Jane Rooney arrive during the 82nd Academy Awards, in the Hollywood section of Los Angeles. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/Chris Pizzello, File)

FILE - In this Jan. 5, 1942, file photo, Mickey Rooney, 21, Movieland's No. 1 box office star, and Ava Gardner, 19, of Wilson, N.C., pose together in Santa Barbara, Calif., shortly after the couple applied for a marriage license. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/File)

FILE - Entertainer Mickey Rooney is shown in this May 1987 file photo. Rooney, a Hollywood legend whose career spanned more than 80 years, has died. He was 93. Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died Sunday, April 6, 2014, at his North Hollywood home. (AP Photo/File)

Mickey Rooney, the pint-size, precocious actor and all-around talent whose career spanned more than 80 years, has died at the age of 93.

He starred in silent comedies, Shakespeare, Judy Garland musicals, television and the Broadway theatre.

Los Angeles Police Commander Andrew Smith said that Rooney was with his family when he died at his North Hollywood home.

He said police took a death report but indicated that there was nothing suspicious and it was not a police case. He said he had no additional details on the circumstances of his passing.

Rooney started his career in his parents' vaudeville act while still a toddler, and broke into movies before the age of 10.

He was still racking up film and TV credits more than 80 years later - a tenure probably unmatched in the history of show business.

"I always say, 'Don't retire - inspire,'" he told The Associated Press in March 2008. "There's a lot to be done."

Among his roles in recent years was a part as a guard in the smash 2006 comedy A Night At The Museum.

Rooney won two special Academy Awards for his film achievements, and reigned from 1939 to 1942 as the No 1 moneymaking star in films, his run only broken when he joined the Army.

At his peak, he was the incarnation of the show biz lifer, a shameless ham and hoofer, his blond hair, big grin and constant motion a draw for millions. He later won an Emmy and was nominated for a Tony.

"Mickey Rooney, to me, is the closest thing to a genius I ever worked with," Clarence Brown, who directed his Oscar-nominated performance in The Human Comedy, once said.

Rooney's personal life matched his film roles for colour. His first wife was the glamorous - and taller - Ava Gardner, and he married seven more times, fathering seven sons and four daughters.

Through divorces, money problems and career droughts, he kept returning with customary vigour.

"I've been coming back like a rubber ball for years," he commented in 1979, the year he returned with a character role in The Black Stallion, drawing an Oscar nomination as supporting actor, one of four nominations he earned over the years.

That same year he starred with Ann Miller in a revue called Sugar Babies, a mixture of vaudeville and burlesque.

It opened in New York in October 1979, and immediately became Broadway's hottest ticket. Rooney received a Tony nomination (as did Miller) and earned millions during his years with the show.

Rooney was among the last survivors of Hollywood's studio era, which his career predated.

Rooney signed a contract with MGM in 1934 and landed his first big role as Clark Gable as a boy in Manhattan Melodrama.

A loan to Warner Brothers brought him praise as an exuberant Puck in Max Reinhardt's 1935 production of A Midsummer Night's Dream, which also featured James Cagney and a young Olivia de Havilland.

Rooney was soon earning 300 dollars a week with featured roles in such films as Riff Raff, Little Lord Fauntleroy, 'Captains Courageous and most notably, as a brat humbled by Spencer Tracy's Father Flanagan in Boys Town.

The big break came with the wildly popular Andy Hardy series, beginning with A Family Affair.

But Rooney became a cautionary tale for early fame. He earned a reputation for drunken escapades and quickie romances and was unlucky in both money and love. In 1942 he married for the first time, to Gardner, the statuesque MGM beauty. He was 21, she was 19.

The marriage ended in a year, and Rooney joined the Army in 1943, spending most of his Second World War service entertaining troops.

Rooney returned to Hollywood and disillusionment. His savings had been stolen by a manager and his career was in a nose dive. He made two films at MGM, then his contract was dropped.

His film career never regained its prewar eminence. The Bold And The Brave, a 1956 Second World War drama, brought him an Oscar nomination as best supporting actor.

But mostly, he played second leads in such films as Requiem For A Heavyweight with Anthony Quinn. In the early 1960s, he had a wild turn in Breakfast At Tiffany's as Audrey Hepburn's bucktoothed Japanese neighbour.

His later career proved his resilience: The Oscar nomination for Black Stallion. The Sugar Babies hit that captivated New York, London, Las Vegas and major US cities. Voicing animated features like The Fox And The Hound, 'The Care Bears Movie and Little Nemo. An Emmy for his portrayal of a disturbed man in the 1981 TV movie Bill.

In 1983, the Motion Picture Academy presented Rooney with an honorary Oscar for his "60 years of versatility in a variety of memorable film performances".

Born Joe Yule Jr. in 1920, he was the star of his parents' act by the age of two, singing Sweet Rosie O'Grady in a tiny tuxedo.

His father was a baggy-pants comic, Joe Yule, his mother a dancer, Nell Carter. Yule was a boozing Scotsman with a wandering eye, and the couple soon parted.

While his mother danced in the chorus, young Joe was wowing audiences with his heartfelt rendition of Pal o' My Cradle Days. During a tour to California, the boy made his film debut as a midget in a 1926 Fox short, Not To Be Trusted.

The boy was also playing kid parts in features, and his name seemed inappropriate. His mother suggested Rooney, after the vaudeville dancer, Pat Rooney.

In 1978, Rooney, 57, married for the eighth - and apparently last - time. His bride was singer Janice Darlene Chamberlain, 39. Their marriage lasted longer than the first seven combined.

After a lifetime of carrying on, he became a devoted Christian and member of the Church of Religious Science.

In 2011, Rooney was in the news again when he testified before Congress about abuse of the elderly, alleging that he was left powerless by a family member who took and misused his money.

"I felt trapped, scared, used and frustrated," Rooney told a special Senate committee considering legislation to curb abuses of senior citizens. "But above all, when a man feels helpless, it's terrible."

PA Media