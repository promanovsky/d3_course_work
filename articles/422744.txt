ROANOKE, Va. — The Catholic Diocese of Richmond is discouraging parishes and schools that participate in the ice bucket challenge from sending money to the ALS Association.

WDBJ-TV reports that the diocese has sent a letter to 29 Catholic schools advising them to send donations to another organization that combats ALS. The diocese planned to send the letter Tuesday to 146 parishes.

In the letter, the diocese says the ALS Association uses embryonic stem cells to conduct research on ALS.

The Catholic Church relates the use of embryonic stem cells in research to abortion and says it violates the sanctity of human life.

The Richmond diocese says it supports adult stem cell research.

ALS is a neurodegenerative disease that causes paralysis and almost certain death. It’s also known as Lou Gehrig’s disease.