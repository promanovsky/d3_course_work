LONDON -- Air pollution kills about 7 million people worldwide every year through stroke, heart disease and cancer, with more than half of the fatalities due to fumes from indoor stoves, according to a new report from the World Health Organization (WHO) published Tuesday.

The agency said air pollution is the cause of about one in eight deaths and has now become the single biggest environmental health risk.

That's in addition to air pollution being a major driver of respiratory diseases, including acute respiratory infections and chronic obstructive pulmonary diseases (COPD), the report found.

WHO's report noted women had higher levels of exposure than men in developing countries.

"Poor women and children pay a heavy price from indoor air pollution since they spend more time at home breathing in smoke and soot from leaky coal and wood cook stoves," Flavia Bustreo, WHO Assistant Director-General for family, women and children's health, said in a statement.

One of the main risks of pollution is that tiny particles can get deep into the lungs, causing irritation. Scientists also suspect air pollution may be to blame for inflammation in the heart, leading to chronic problems or a heart attack.

A July 2013 study in The Lancet found air pollution exposure could raise risk for lung cancer and heart failure, specifically.

WHO estimated that there were about 4.3 million deaths in 2012 caused by indoor air pollution, mostly people cooking inside using wood and coal stoves in Asia. WHO said there were about 3.7 million deaths from outdoor air pollution in 2012, of which nearly 90 percent were in developing countries.

But WHO noted that many people are exposed to both indoor and outdoor air pollution. Due to this overlap, mortality attributed to the two sources cannot simply added together, hence WHO said it lowered the total estimate from around 8 million to 7 million deaths in 2012.

"The risks from air pollution are now far greater than previously thought or understood, particularly for heart disease and strokes," says Dr Maria Neira, Director of WHO's Department for Public Health, Environmental and Social Determinants of Health. "Few risks have a greater impact on global health today than air pollution; the evidence signals the need for concerted action to clean up the air we all breathe."

The new estimates are more than double previous figures and based mostly on modeling. The increase is partly due to better information about the health effects of pollution and improved detection methods. Last year, WHO's cancer agency classified air pollution as a carcinogen, linking dirty air to lung and bladder cancer.

"We all have to breathe, which makes pollution very hard to avoid," said Frank Kelly, director of the environmental research group at King's College London, who was not part of the WHO report.

Other experts said more research was needed to identify the deadliest components of pollution in order to target control measures more effectively.

"We don't know if dust from the Sahara is as bad as diesel fuel or burning coal," said Majid Ezzati, chair in global environmental health at Imperial College London.

Kelly said it was mostly up to governments to curb pollution levels, through measures like legislation, moving power stations away from big cities and providing cheap alternatives to indoor wood and coal stoves.

He said people could also reduce their individual exposure to choking fumes by avoiding traveling at rush hour or by taking smaller roads. Despite the increasing use of face masks in heavily polluted cities such as Beijing and Tokyo, Kelly said there was little evidence that they work.

"The real problem is that wearing masks sends out the message we can live with polluted air," he said. "We need to change our way of life entirely to reduce