“He faced relentless criticism, personal threats, and the certainty that history would judge him harshly if he were wrong,” she said. “But he stood up for what he believed was right and necessary. Ben Bernanke’s intelligence and knowledge served him well as chairman.”

Bernanke’s actions were courageous and “unprecedented in ambition and scope,” Yellen told a swirl of thousands of purple-robed graduates and their families who were gathered in the stands of Yankee Stadium for the ceremony.

NEW YORK — Federal Reserve chairwoman Janet L. Yellen, in a speech to graduates of New York University on Wednesday, heaped praise on her predecessor, Ben S. Bernanke, saying his grit and courage helped him manage the financial crisis in difficult times.

Advertisement

Yellen told the graduates that finding the right path in life involved missteps, something she and her colleagues experienced as they struggled to address a financial and economic crisis that threatened the global economy.

“We brainstormed and designed a host of programs to unclog the plumbing of the financial system and to keep credit flowing,” said Yellen, who served as vice chairwoman of the Fed’s Board of Governors from 2010 until this year, when she took over as the leader of the central bank. She was president of the Federal Reserve Bank in San Francisco during the crisis. “Not everything worked, but we kept at it, and we remained focused on the task at hand. I learned the lesson during this period that one’s response to the inevitable setbacks matters as much as the balance of victories and defeats.”

For Yellen, the speech was a departure from her typical days, which she normally spends surrounded by economists in the Fed’s cloistered headquarters and meeting with business executives, politicians, and officials.

On Wednesday she was behind a lectern on second base, sitting near a baseball legend, Mariano Rivera, a former Yankees player who was a top relief pitcher and played on five winning World Series teams; a Supreme Court justice, Elena Kagan; and the Queen of Soul, Aretha Franklin. All received honorary degrees.

Advertisement

Yellen did not delve into her views on monetary policy. Instead she talked of sea slugs. She told graduates the story of how focusing on the simple California sea slug, even amid ridicule, led to a Nobel Prize for Eric R. Kandel, an NYU alumnus.

It is a lesson in being persistent about curiosity, she said.

Listen carefully, she also told the graduates, even to detractors. And show determination.

“There is an unfortunate myth,’’ she told the graduates, ‘‘that success is mainly determined by something called ‘ability.’’’

In fact, she said, research shows that measures of ability are unreliable predictors of performance in academics or employment. Instead, she said, what’s more important is a quality that psychologist Angela Lee Duckworth calls grit — ‘‘an abiding commitment to work hard toward long-range goals and to persevere through the setbacks that come along the way.’’

The ceremony represented a homecoming of sorts for Yellen, who spent her childhood in Brooklyn and attended Fort Hamilton High School there.

Material from the Associated Press was used in this report.