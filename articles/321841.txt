Hologic, Inc. ( HOLX ) reported positive results from a study validating the company's 3D Mammography (breast tomosynthesis) screening technology. Per the study, Hologic's 3D mammography leads to more efficient and accurate detection of invasive breast cancers, compared to a traditional mammogram.

The study, "Breast Cancer Screening Using Tomosynthesis in Combination with Digital Mammography", was published in the June 25, 2014 issue of the Journal of the American Medical Association (JAMA).

A Brief Note about the Study

Mammography plays a key role in detecting early-stage breast cancer in women. Currently, Hologic 3D Mammography is the first and the only U.S. Food and Drug Administration (FDA) approved 3D mammography system available in the U.S.

The purpose of the JAMA 3D study was to verify whether mammography combined with tomosynthesis can improve the performance of breast screening programs in the U.S. This study focused on the two prior concerns related to breast cancer screening. Firstly, a number of cancers are being detected in women which do not need any treatment and secondly, too many women are recalled for unnecessary additional testing.

A total of 454,850 screening mammograms were evaluated at 13 institutional sites by 139 radiologists and were used for primary analysis. This multi center analysis compared performance of breast cancer screening before and after the introduction of tomosynthesis over 2 periods.

Of the 454,850 candidates, only digital mammography was performed on 281,187 (61.8%) in the first period, while mammography and tomosynthesis were jointly performed on 173,663 (38.2%) in the second period.

The mean age of patients undergoing imaging with digital mammography alone was 57.0 years, and for those with digital mammography and tomosynthesis, was 56.2 years.

Study Results

The study underscored a 41% increase in the detection of invasive breast cancers and a 29% increase in the detection of all breast cancers when screened with digital mammography and tomosynthesis.

Cancer was detected in 1207 women with digital mammography and in 950 women with digital mammography and tomosynthesis. The cancer detection rate per 1000 examinations for the cohort screened with digital mammography was 4.2 compared with 5.4 when screened with digital mammography and tomosynthesis.

The results indicated an overall increase in cancer detection rate of 1.2 with digital mammography and tomosynthesis compared with digital mammography alone, per 1000 women.

After implementation of tomosynthesis, the invasive cancer detection rate increased from 2.9 to 4.1 per 1000, representing a relative increase of 41%, which proved the efficacy of Hologic's 3D mammography combined with tomosynthesis in breast cancer screening.

Moreover, researchers found that 3D mammography reduces the number of women recalled for unnecessary testing due to false alarms by 15%. This can help reduce unwanted anxiety in patients and curb unnecessary health care costs.

Zacks Rank

Currently, Hologic carries a Zacks Rank #3 (Hold). Some better-ranked medical instrument stocks that warrant a look are Alphatec Holdings, Inc. ( ATEC ), Luminex Corporation ( LMNX ), and Accuray Incorporated ( ARAY ). While Alphatec and Luminex sport a Zacks Rank #1 (Strong Buy), Accuray holds a Zacks Rank #2 (Buy).

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

HOLOGIC INC (HOLX): Free Stock Analysis Report

LUMINEX CORP (LMNX): Free Stock Analysis Report

ACCURAY INC (ARAY): Free Stock Analysis Report

ALPHATEC HLDGS (ATEC): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.