CUPERTINO, Calif., March 10 (UPI) -- Apple released iOS 7.1, the first major update for the latest version of its mobile software, which includes CarPlay, Siri improvements, and visual tweaks.

The update comes six months after iOS 7's launch last September, which was much needed after initial adopters complained of bugs, and the update will available to iPhone 4 and later, iPad 2 and later, and iPod touch users. For those who cannot download the update over the air, they can use iTunes to update their devices.

Advertisement

The major change with iOS 7.1 is the introduction of CarPlay, a new car interactive system running on iOS that has already been showcased on Volvo, Ferrari and Mercedes-Benz cars. CarPlay all allow users to make calls, dictate text messages, listen to music and other features all using voice-enabled commands.

Siri will also no longer wait for a user's voice commands indefinitely. Siri will now listen to users only when they hold down the home button and on releasing it she will stop listening to you. The update also comes with more human-like, natural voices in Mandarin Chinese, UK English, Australian English and Japanese.

RELATED European soccer team Bayern Munich boss involved in tax fraud

A few visual changes have been made with the onscreen keyboard, the in-call buttons, lock screen icons and more. A recurring bug that caused the home screen to crash has also been fixed in this version.

For the first time, Apple has released a document listing all the new features included in this update. According to Apple, as of late February, 82 percent of all compatible iPhones, iPads, and iPod touch devices were running iOS 7.

[Apple] [ZDNet]