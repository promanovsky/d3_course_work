Weird Al “Tacky” music video taps Pharrell’s “Happy”

This week we’re ramping up toward the release of the next Weird Al Album “Mandatory Fun” with the first of a series of music videos. This first video parodies Pharrell’s “Happy” with a lovely tweak called “Tacky.”

Like the original series of Happy videos released by Pharrell for his mega-hit of the summer, this amalgamation of oddities features fun-loving individuals walking towards a camera. Instead of well-dressed dancers and especially photogenic do-gooders, this video takes well-known comedians to work, putting them in just about the ugliest combinations of clothes on the planet.

Inside this majestic beast of a video you’ll find Aisha Tyler and Margaret Cho followed by Eric Stonestreet, Kristen Schaal, and Jack Black – comedians all. Masters of their art and very, very tacky dancers to boot.

This is also part of an 8 days, 8 videos initiative held by Al, this first video hosted by Nerdist. You’ll find an #8videos8days hashtag backing this week+ up, and SlashGear is pushing the whole collection day by day as well. We’ll be following along the whole week and bringing the heat to our relatively new Weird Al tag portal – stick around and stay weird!