The death of Eric Hill was revealed on The Bachelorette on Monday, leaving Andi Dorfman in tears and fellow contestants shocked.

Monday’s episode featured hometown dates for the four remaining contestants — Chris, Marcus, Nick, and Josh. But at one point, host Chris Harrison brought all four remaining men to his home and then invited Dorfman, breaking the news that former contestant Eric Hill had been killed in a paragliding accident.

“Y’all know, Eric Hill. He, a couple of days ago, was in Utah paragliding and was in a serious accident. We knew he was hurt bad we just didn’t know how bad. We learned this morning that he passed away,” Harrison revealed.

Upon hearing the news, Andi Dorfman and Nick both broke down and began to cry, while the others looked shocked. Marcus also stood up to leave the room for a moment.

“It just puts it in perspective, there’s so much more to all this — there’s life. People have lives here,” Dorfman said.

The death of Eric Hill also caused the show to drop its fourth wall, showing crew members joining in the mourning. Andi was heard talking to a crew member about her last conversation with him before he left; an argument in which she told Hill that he had a poker face, prompting him to leave the show and go home.

Dorfman later admitted that she felt guilty about Eric Hill’s death, but vowed to compose herself for the sake of the remaining contestants.

Hill died in April following the accident. He had been paragliding in Utah when he experienced turbulence strong enough to push him into a mountainside. The accident left him with significant trauma, and after being airlifted to a hospital, he fell into a coma.

Days later, Eric Hill died of his injuries.

“Eric shared his final journey with us this morning as all his immediate family were able to be at his side when he passed away. Thank you to all of your love and support and prayers and fasting. It was amazing to be with so many of his friends and family yesterday in the hospital who came to express their love for Eric,” Hill’s sister Karen Tracy wrote on her Facebook page.

The death of Eric Hill is the second tragedy to strike The Bachelorette series in the last year. In summer 2013, former Bachelor contestant Gia Allemond died, one day after a suicide attempt left her in a coma.

[Images Via Bing]