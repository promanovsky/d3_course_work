May 14 (Reuters) - A new study of 80,000 women with early breast cancer found that obesity was associated with a 34 percent higher risk of death for pre-menopausal women whose disease is susceptible to higher levels of the hormone estrogen.

The study, featured ahead of the annual meeting of the American Society of Clinical Oncology in Chicago beginning on May 30, adds to the accumulation of evidence that obesity is a risk factor for cancer outcomes as well as development of the disease.

"No matter how we look at it, obesity is slated to replace tobacco as the leading modifiable risk for cancer," Dr Clifford Hudis, ASCO president and chief of Memorial Sloan Kettering Cancer Center's Breast Cancer Medicine Service in New York, told a news conference.

ASCO earlier this week issued new guidelines for doctors and patients on how to manage obesity-related issues after a cancer diagnosis.

Obesity has been linked with an increased risk for many of the most common cancers, including breast, colon, and high-grade prostate cancers. Obesity is also associated with increased levels of estrogen.

But public awareness about the connection remains low. ASCO cited a recent survey suggesting that fewer than one in 10 Americans even realize that obesity is a risk factor for cancer.

The latest study, conducted at Britain's Oxford University, also found that obesity had little effect in women with hormone-positive breast cancer once they had passed menopause.

Lead researcher Hongchao Pan said more research was needed to determine the biological mechanisms behind the findings.

She noted that, as expected, the study found no link between body weight and outcomes for patients whose breast cancer was estrogen receptor negative, meaning the tumors do not rely on the sex hormone to grow.

The U.S. Centers for Disease Control and Prevention estimates that about 35 percent of U.S. adults are obese and around 70 percent are either obese or overweight.