Novartis to license Google “smart lens” technology

● Innovative technology offers potential to transform eye care and further enhance Alcon’s leadership in contact lenses and intraocular lenses

● Agreement is a first step for Novartis to evolve technology to manage human diseases and conditions

Basel, Switzerland, July 15, 2014 – Novartis announced that its eye care division Alcon has entered into an agreement with a division of Google Inc. to in-license its “smart lens” technology for all ocular medical uses. The agreement with Google[x], a team within Google that is devoted to finding new solutions to big global problems, provides Alcon with the opportunity to develop and commercialize Google’s “smart lens” technology with the potential to transform eye care and further enhance Alcon’s pipeline and global leadership in contact lenses and intraocular lenses. The transaction remains subject to anti-trust approvals.

The agreement between Google and Alcon represents an important step for Novartis, across all of its divisions, to leverage technology to manage human diseases and conditions. Google’s key advances in the miniaturization of electronics complement Novartis’s deep pharmaceuticals and medical device expertise. Novartis aims to enhance the ways in which diseases are mapped within the body and ultimately prevented.

“We are looking forward to working with Google to bring together their advanced technology and our extensive knowledge of biology to meet unmet medical needs,” said Novartis CEO Joseph Jimenez. “This is a key step for us to go beyond the confines of traditional disease management, starting with the eye.”

“Our dream is to use the latest technology in the miniaturization of electronics to help improve the quality of life for millions of people,” said Sergey Brin, Co-Founder, Google. “We are very excited to work with Novartis to make this dream come true.”

Under the agreement, Google[x] and Alcon will collaborate to develop a “smart lens” that has the potential to address ocular conditions. The smart lens technology involves non-invasive sensors, microchips and other miniaturized electronics which are embedded within contact lenses. Novartis’ interest in this technology is currently focused in two areas:

● Helping diabetic patients manage their disease by providing a continuous, minimally invasive measurement of the body’s glucose levels via a “smart contact lens” which is designed to measure tear fluid in the eye and connects wirelessly with a mobile device;

● For people living with presbyopia who can no longer read without glasses, the “smart lens” has the potential to provide accommodative vision correction to help restore the eye’s natural autofocus on near objects in the form of an accommodative contact lens or intraocular lens as part of the refractive cataract treatment.

The agreement marries Google’s expertise in miniaturized electronics, low power chip design and microfabrication with Alcon’s expertise in physiology and visual performance of the eye, clinical development and evaluation, as well as commercialization of contact and intraocular lenses. Through the collaboration, Alcon seeks to accelerate product innovation based on Google’s “smart lens” technology.

“Alcon and Google have a deep and common passion for innovation,” said Jeff George, Division Head of Alcon. “By combining Alcon’s leadership in eye care and expertise in contact lenses and intraocular lenses with Google’s innovative “smart lens” technology and groundbreaking speed in research, we aim to unlock a new frontier to jointly address the unmet medical needs of millions of eye care patients around the world.”