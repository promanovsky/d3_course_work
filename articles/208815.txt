It's a good question, really.... Why does Apple (AAPL) - Get Report want to buy Beats?

Apple changed the entire music world with iTunes and iPods. Is Apple on the brink of changing the entire music world again with its potential acquisition of Beats?

Far be it from me to challenge Apple's decision to want to buy Beats. It's an interesting choice for many reasons. For me, the challenge is to take it all in and then decide what the impact will be on Apple's stock price and the shares of its competitors.

To accomplish that task, I've taken a look at iTunes' download music competitors, as well as iTunes Radio streaming music competitors, Beats Music being one of them.

It's hard to remember a time when iTunes didn't exist. In fact, I can't remember when iTunes didn't exist because it was started when I was four years old (January 2001). It's hard to believe that people listened to things called 8-Track tapes and cassettes and albums, and crazy to understand that music listening sitdowns lasted longer than three minutes. People used to listen to music, so I'm told, for hours at a time, or at least the length of time of an album, about an hour.

Currently, sales of downloaded music is declining. Apple's competitors in the music download market are Amazon mp3 (AMZN) - Get Report, Google Music (GOOG) - Get Report, and eMusic, a private company. There are a few others, but the bigger focus is now on music streaming companies --- and that is what may be at the heart of the Apple purchase of Beats.

SOURCE: 13 Streaming Music Services Compared by Price, Quality, Catalog Size and More,

by Matt Peckham @mattpeckham March 19, 2014

http://time.com/30081/13-streaming-music-services-compared-by-price-quality-catalog-size-and-more/

After researching a lot about Beats and the pros and cons of Apple buying Beats, I came to one great advantage and one great disadvantage for Apple. Let's start with the bad news....

The great disadvantage for Apple in buying Beats is quality, at least according to Mark Owsinski in a May 10, 2014 Forbes piece, Why The Apple-Beats Deal Makes Sense, And Why It Doesn't. Owsinski states, "Unfortunately Beats headphones are more of a fashion statement and are nowhere near the overall quality one would normally associate with Apple, so you have to wonder how the electronics portion of Beats even fits into the equation of the deal.

The great advantage for Apple in buying Beats is the cool factor. According to Frank Aquila, M&A Partner at Sullivan & Cromwell, in a May 9, 2014 Fox Business news article, "Right now iTunes is not the cool music service.... Acquiring Beats gives them the 'Dr. Dre cool' factor."

All that said, I turn my attention to what may happen to the stock price of these companies - Apple, Amazon, Pandora (P) -- if Apple (AAPL) goes through with the deal and adds on some extra cool to the Apple brand?

If the sale goes through, I'm long on Apple long-term. Apple in the short-term, (inhaling sound.....hshsssss) .... I just don't know. Apple is too in the news for me to want to play it either way, and I am not feeling the short-term direction clearly enough to put my money into it.

Pandora should be hurt by the deal and is coincidentally in a bearish trend, so shorting Pandora may be a good play, short-term and short-medium-term. Amazon is looking the bear as well, because it is oversold with an RSI of 36, leading me to believe it can and will go lower. Amazon shouldn't be too pleased with the Apple/Beats deal, as it is just entering the music streaming business and has been a music download player for some time now.

I do not have positions in Apple, Amazon, or Pandora at this time. I do not plan on buying or selling those stocks within the next 72 hours, although I might. I do not recommend you follow any stock advice, but do your own research and choose what you believe will happen to the prices of these stocks.

Whatever stock and price direction you choose, I wish you

Many Happy Returns,

Rachel

For more from Rachel Fox, visit

www.foxonstocks.com