Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Nicki Minaj made us all cringe at the MTV VMAs when she appeared on stage with Ariana Grande and Jessie J to sing the threesome's new track Bang Bang - because her dress was gaping open.

Oops.

But it seems the unzipped 'accident' was actually done on purpose , TMZ reports.

Eyewitnesses tell the site the rapper was seen with the same problem in rehearsals too, and appeared to move her arms around to see how much she could safely expose.

VMAs stage manager Gary Hood phoned in to KIIS-FM radio on Monday too, accoridng to E! News, and said: "Like I said, when we rehearsed in the dress that afternoon, same thing."

(Image: Splash)

Another source told TMZ the star had a makeshift dressing room just off stage - giving her enough time to change.

Earlier on in the evening the star posed for pictures in the press room in the same black dress, but looked just as uncomfortable as she tried to shield her cleavage.

Given her later raunchy performance of Anaconda we're not quite sure why she came over quite so shy.

Last year Miley Cyrus scooped the award for most controversial performance and this year it belonged to the rapper.

Nicki delivered one of the most shocking shows we've seen in a while as she belted out new hit Anaconda at the Forum in Inglewood shortly after she hit the stage with Jessie and Ariana.

Dressed in a pair of high-waisted sequin shorts and a matching green crop top, the 31-year-old singer strutted, twerked and writhed around stage.

But the most shocking part was when she dropped to the floor, grabbed her crotch and had one of her backing dancers lie on top of her - and you can imagine the rest.

For more pictures of the star awkwardly adjusting her dress, scroll through the gallery below...