Samsung’s Galaxy apps generally ignored says research

Samsung bloatware or bonus features: however generously you want to refer to them, it turns out Galaxy smartphone owners aren’t actually using them, according to new research. Total usage time for a sample of Galaxy S4 and Galaxy S5 users of Samsung’s key homegrown apps amounted to less than seven minutes in March 2014, whereas they used three of Google’s apps for 149 minutes in all.

Strategy Analytics looked at usage of Samsung-exclusive features like S Memo, Siri-rivaling S Voice, IM client ChatON, the Samsung Apps store, Group Play, Samsung Link, and Samsung Hub. That was contrasted with use of three typical Google apps: YouTube, the Google Play Store, and Google Search.

Of Samsung’s apps, the most popular were S Memo and S Voice, though they still only amounted to less than four minutes use a month apiece. Contrasting the download store use is particularly telling, with Galaxy S4 and S5 users still sticking to Google Play in clear preference to Samsung’s own curated download store.

Compared to phones from other manufacturers, those with a Galaxy S4 have 21 more apps on average, the research discovered. Some of those have been downloaded intentionally, but some are preinstalled by Samsung itself.

Now, the research only looked at 250 users of the two older flagships, but it seems Samsung was already waking up to the idea that it might need to refine its custom software strategy back when it was planning the latest Galaxy S5.

That launched with far fewer gimmicks and special features, with Samsung instead focusing on a few key themes with TouchWiz like its integrated fitness system. Nonetheless, there are still plenty of changes that Samsung has wrought, and there may be more in the pipeline if the company’s efforts to distance itself from Android pan out further.

SOURCE Strategy Analytics