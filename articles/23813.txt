No, it is not suddenly good to eat more saturated fat -- and the new study grabbing headlines showed no such thing.

The new study, a meta-analysis (meaning a pooling of previously published studies, not new research) in the Annals of Internal Medicine, shows the following two things in particular: (1) you cannot get a good answer to a bad question; and (2) there is more than one way to eat badly.

We'll come back to those shortly, but first here's an overview of the study objectives, methods, and findings. The investigators, an international team, started out questionably by asserting that dietary guidelines emphasize "changes in fatty acid composition to promote cardiovascular health." There is some truth to that, but there are some fundamental problems with the assertion as well. Dietary guidelines make recommendations about any given nutrient in the context of the overall diet. So, for instance, advice to eat less saturated fat also comes along with advice about eating less sugar. It is not in the form of "eat less saturated fat and do whatever else you want." More importantly, good dietary guidance is increasingly about foods rather than isolated nutrients, although that is admittedly still a work in progress.

So, having framed the issue questionably, the researchers set off in the appropriately questionable direction to address it. They looked at variation in the intake of specific fatty acid categories (e.g., saturated, monounsaturated, polyunsaturated, trans), sub-categories (e.g., omega-3 and omega-6 polyunsaturates), and specific fatty acids within those sub-categories and corresponding variation in coronary heart disease.

The methods, as noted, were meta-analytic, the pooling of data from multiple prior studies to reach a summary conclusion. Some experts in meta-analysis have raised concerns about the methodologic details, with at least one expert at Harvard suggesting there may be a major mistake. But that debate can be left to play out. We will give the analytic methods the benefit of the doubt, and assume they correctly answer the questions asked.

Those questions were: What are the differences in rates of coronary heart disease (defined rather vaguely in the paper) in observational studies when the top third of intake is compared to the bottom third of intake for some particular class of fatty acid?; and what are the differences in coronary disease rates in intervention trials between groups when one group is given some kind of fatty acid supplement?

For the observational studies, representing more than 500,000 people, the investigators found more coronary heart disease in the highest intake of trans fat compared to the lowest; and less heart disease in the highest intake of omega-3 fat compared to the lowest. There was a hint of benefit for higher monounsaturated fat intake. And then the part spawning the predictably exaggerated headlines: there was no appreciable difference in coronary heart disease rates seen comparing the top to bottom third of saturated fat intake, although there was in fact a suggestion of more heart disease with more saturated fat consumption. There was no difference when comparing the top to bottom third of omega-6 fat intake either.

For the most part, the intervention studies, which included more than 100,000 people, administered some omega-3 supplement. A smaller batch of studies administered some other kind of polyunsaturated fat. You may recall we had already heard the news that omega-3 supplements, all other things being equal, did not appreciably reduce rates of coronary disease, so it should come as no surprise that this study found the same. In fact, while not statistically significant, there was a trend toward benefit seen with all of the polyunsaturated fat supplements, including omega-6 fat. For long-chain omega-3 fat, or so-called "fish oil," the apparent benefit was very close to statistically significant.

Now, consider for a moment some of the leading arguments about diet and health swirling around us. Are they all about dietary fat? Not remotely. Much of our collective attention over recent years has been focused on sugar, starches, carbohydrates in general, meat in general, processed meats and grains.

I searched the new paper for the word "sugar," and could find no mention of it. None.

People eating less saturated fat don't simply stop eating a nutrient and leave a big hole in their diets. They eat less of A, and make up for it by eating more of B. The most obvious of questions, yet one to which this study was totally inattentive, is: what is B?

We know those trends at the level of the general population. When we started cutting back on saturated fat, we started eating more refined starch and added sugar. We also know that excess intake of sugar, starch, and calories is associated with obesity, diabetes, and coronary disease. So if eating less saturated fat means eating more sugar, it would at best be a lateral move in terms of health -- and probably worse than that. The study simply ignored this consideration.

Does this show, as the titillating headlines suggest, that saturated fat is unrelated to coronary disease? No, however we might wish it to be so. It merely shows there is more than one way to eat badly -- and from my perspective, our culture seems committed to exploring them all.

Basically, this study showed that if you vary your intake of saturated fat or omega-6 fat without altering the overall quality of your diet, you are not likely to alter your health much either. That's not much of a revelation -- and unlikely to make any headlines expressed as such. But the headlines we are getting, while much more exciting, are entirely misleading. There was no suggestion at all here of any health benefits of saturated fat, and some hint of harmful effects despite the important study limitations. There were suggestions of favorable effects of the usual suspects, omega-3 fat and monounsaturated fat.

But moving on from such one-nutrient-at-a-time preoccupations, there is a bigger fish to fry here than just fish oil, or olive oil or lard. Dietary guidance must be about the whole diet, and should be directed at foods rather than nutrients. If we get the foods right, the nutrients take care of themselves.

Are such diets low in saturated fat? Yes, but as a byproduct of the foods that are eaten. A diet that is made up mostly of vegetables, fruits, beans, lentils, nuts, seeds, and whole grains, with or without fish, seafood, lean meats, eggs and dairy simply has less room for saturated fat, let alone trans fat. Such a diet is natively high in omega-3 and monounsaturated fat, and balanced in terms of polyunsaturates. Just as important, such a diet is relatively low in refined starch and added sugar, and natively rich in fiber, vitamins, minerals and antioxidants. Attend to the forest, in other words, and the trees thrive. Bark up any given tree, and you may fail to notice that the forest has burned to the ground.

Our one-nutrient-at-a-time approach to diet and health has been a decades long public health boondoggle. Our penchant to talk about nutrients rather than food is antiquated and substantially misguided. There is saturated fat in salmon and salami. There is carbohydrate in lentils and lollipops. Lumping foods together across such a spectrum is the garbage in that invites studies that will inevitably spit garbage out. Looking at variation in saturated fat while ignoring sugar is an exercise in futility.

Our inclination to play Ping-Pong with scientific findings comes at a cost in human potential. Our proclivity for hyperbolic headlines is a public health menace.

This new study shows we can vary our intake of any given fatty acid and not alter the quality of our diet or health. Well, duh. There is more than one way to eat badly.

And there are no good answers to misguided questions.

My advice is as it ever was. Chew carefully on headlines before choosing to swallow the hyperbole, and eat a diet of wholesome foods reliably associated with good health across a vast and stunningly consistent literature. Do that, and let the fatty acids and other nutrients sort it out for themselves.

-fin