NEW YORK (CBSNewYork) — A new study finds a child’s sleep could be connected to his or her weight later in life.

As CBS 2’s Carolyn Gusoff reported, the study by Massachusetts General Hospital found children who got the least shut-eye had the highest rates of obesity and body fat.

Doctors have long known disrupted sleep affects health. But the findings suggest even children’s sleep can affect their metabolism and body clocks for life.

“As we evolve and we get more or less sleep, our body … may adapt or be changed,” said Dr. Ron Feinstein, who runs the Weight Management Program at Cohen Children’s Medical Center.

That puts pressures on parents, such as Nazy Aghalarian, a Great Neck resident and mother of six.

“I have friends that their kids sleep from 7 to 7,” Aghalarian said. “I’m not that fortunate.”

The recommended amount of sleep for children 2 and younger is more than 12 hours a day. Three- to 4-year-olds should get at least 11 hours of sleep. And kids 5 to 7 should rest at least 10 hours.

Parents said it’s a constant struggle getting their little ones to go to bed.

“We walk around the kitchen and living room. We dance,” described Melonie Weingarten, the mother of 9-week-old Maya.

“Sometimes it’s just they’re wound up,” said Adam Glassman, owner of Dreamnastics in Great Neck. “Just they want more books. They like their routine.”

It’s even harder with older children, who cram after-school activities into limited waking hours.

“Karate, baseball, art — a lot of demands, a lot of stuff — and it’s definitely hard, especially with homework,” said mother Jarah Wilk.

But Dr. Eric Gould, a pediatrician, said parents shouldn’t stress.

“I don’t worry about the kids being sleep deprived,” he said. “I worry about the parents being sleep deprived.”

The study, which tracked children from 6 months to 7 years old, bolsters the old adage “never wake a sleeping baby.” Doctors say you should let your children sleep as long as you can.

You May Also Be Interested In These Stories

[display-posts category=”news” posts_per_page=”4″]