Bank of America (BofA) has said it would ditch its $4bn stock buyback programme and a planned increase in its quarterly dividend because it miscalculated the level of its capital after its 2009 acquisition of Merrill Lynch.

The restatement indicates that the second-largest US bank by assets holds less capital than previously projected.

The US Federal Reserve has ordered BofA to suspend and resubmit its capital plans for 2014.

Bank of America said the slip-up was related to the treatment of structured notes after the acquisition of Merrill Lynch at the height of the financial crisis.

BofA discovered the miscalculation after it put out its first-quarter results on 16 April. The lender is required to resubmit its capital plan within 30 days.

The reduction in regulatory capital and ratios will have no impact on the company's historical consolidated financial statements or shareholders' equity, BofA added, reported Reuters.

The Fed said in a statement: "The Federal Reserve Board [on 28 April] announced it is requiring Bank of America Corporation to resubmit its capital plan and to suspend planned increases in capital distributions. The decision relates to the disclosure by Bank of America that the banking organization incorrectly reported data used in the calculation of regulatory capital ratios and submitted as inputs for the most recent stress tests conducted by the Federal Reserve."

$9bn FHFA Settlement

Late last month, BofA reached a deal with authorities to settle claims that it sold faulty mortgage-backed securities to state-owned Fannie Mae and Freddie Mac in the run-up to the 2008 financial crisis.

The bank agreed to pay $9.3bn to the mortgage firms, resolving disputes with the Federal Housing Finance Agency (FHFA) that has been controlling them since 2008.

Fannie and Freddie play a huge role in the US housing market, as they own or guarantee about half of all US mortgages. The state-owned firms buy mortgages from lenders, securitise them and sell them to investors.