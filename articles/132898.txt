It's that time of year again: Car enthusiasts and the general public alike converge on the west side of Manhattan every April for the New York Auto Show at the Jacob Javits Center.

The biggest themes in the auto industry continue to center around alternative power (be it hybrids, diesels, or fully electric) and around connected car tech, such as self-driving vehicles, enhanced adaptive safety and collision avoidance, and onboard 4G LTE connectivity, to name just a few.

As we saw in Detroit back in January, there's a ton of exciting stuff happening, and we're entering what appears to be a golden age of performance and styling innovation—even in the face of rising fuel prices and increased concern for the environment.

So with that, just like last year, we hit the Javits Center before it opened to the public to bring you the coolest cars on the show floor, listed in alphabetical order. Many of these cars are actual debuts at the show this week; some of them came out over the past several months, but the buzz hasn't worn off yet. As always, we don't want to focus entirely on brand new model introductions, because that would exclude some of the most exciting vehicles.

Finally, while some of these cars are downright affordable, others can push the boundaries of even the richest among us. One of 'em costs almost a million dollars. Consider yourself warned.

The Auto Show officially runs from April 18-27. Be sure to keep up with all of PCMag's car coverage via NextCar.

Advertisement

1. Acura TLX Acura finally fused the TSX and TL sedans into a single model that should help streamline the brand's entry-level luxury lineup.

2. Alfa Romeo 4C Alfa Romeo will finally return to U.S. shores after leaving in 1995; post-Fiat acquisition of Chrysler, arrival dates had come and gone. Meanwhile, the 4C looks ready to party.

3. Aston Martin DB9 Volante Photos can barely capture this beautiful Aston Martin DB9 Volante.

4. Bentley Continental GT Speed Bentley is showing off its Continental GT Speed roadster in a resplendent orange. In all honesty, photos can't capture this one either.

5. BMW 2 Series The BMW 2 Series looks better than the 1 Series while still sticking to the basics for E30 and 2002 fans. I'll take mine with a stick, thank you.

6. BMW i8 Concept BMW's i8 plug-in electric supercar costs $135,000. We're still waiting for a production date on this thing.

7. Bugatti Veyron EB 16.4 Who can ever get tired of the Bugatti Veyron? And for that matter, what else can touch the Super Sport version's 267mph top speed?

8. Cadillac Elmiraj Concept This super-long, flowing GT coupe was introduced last year. Still no word on a production date, but a dramatic statement like this could do wonders for the Cadillac brand.

9. Cadillac Escalade The new 2015 'Slade is as grand and imposing as ever. It starts at $65,000 and goes way up, depending on options.

10. Chevrolet Camaro ZL1 Convertible Chevrolet's venerable Camaro gets another superfast ZL1 model, this time with a drop top.

11. Chevrolet Corvette Stingray Z06 Convertible In case it's not patently obvious, 625hp can do a lot for your hair with the top down.

12. Chevrolet Trax It looks like a normal subcompact SUV, but the new '15 Chevy Trax features OnStar 4G LTE, Siri Eyes Free, and other connected car tech.

13. Dodge Charger SRT Dodge brings back the Super Bee for this 2014 Dodge Charger SRT rear-drive sedan. Enjoy those burnouts.

14. Fiat 500 Abarth Fiat 500 Abarth with a rolled back cloth top. Perfect for hearing its killer exhaust note—and soaking in some sun.

15. 2015 Ford Mustang 50th Anniversary Edition To honor the Mustang's 50th anniversary, Ford has debuted a 50th anniversary trim level with many period details from the original 1964 1/2 model. (Photo credit: Ford)

16. Ford Mustang GT The regular 2015 Ford Mustang GT is quite a looker in red as well. Or in any color, really.

17. Ford Transit Skyliner Concept Inside you'll find a projection movie screen, a wine bar, a fridge, and a custom surround-sound system.

18. Ford Transit Skyliner Concept Interior Here's the 58-inch projection screen. There are four soft cream-colored leather chairs, plus three iPads for controlling the various features of the van.

19. GMC Yukon XL This Yukon XL—new for 2015—is large enough to have its own ZIP code. If you need an SUV with the most space possible, and a Chevy Suburban doesn't do it for you, have a look at this model.

20. Honda Fit The 2015 Honda Fit looks a lot cooler in gray than it did in that phone book yellow the car debuted with.

21. Hyundai Genesis This car first debuted in Detroit; it starts under $40k and looks to show up the Germans, much in the same way Lexus did with its original LS400 luxury sedan in 1989.

22. Hyundai Genesis (Cutaway) Ever wonder what a luxury car looks like after someone gets to it with a giant chop saw? Hyundai shows us the answer with its exploded view of the '15 Genesis.

23. Hyundai Sonata The Genesis in the prior two slides isn't Hyundai's mass-market play; for that, here's the new 2015 Hyundai Sonata. Hyundai is going after the Accord and Camry once again.

24. Infiniti Q70 Nissan's luxury division finally gives its flagship sedan some much-needed love; the Q70 is much more sinuous and attractive than the last decade's oddly proportioned Q.

25. Jaguar F-Type S Jaguars can be a lot of things, and thankfully, they're generally more reliable than they ever were. But one thing they always are is gorgeous—as this F-Type S convertible surely demonstrates.

26. Jaguar F-Type R Coupe Another striking Jaguar, this time in supercharged performance coupe form.

27. Kia GT4 Stinger Concept We first saw Kia's GT4 Stinger concept in Detroit; here it is again, now properly lit up. It's a striking design.

28. Kia GT4 Stinger Interior It was tough to get an interior Kia GT4 Stinger shot at the Javits Center, but even with the reflections in the window you can still make out the crazy dash layout.

29. Koenigsegg Agera R The 1,140-horsepower (!) Agera R hits 200 miles per hour in 17 seconds and has a seven-speed automated manual with paddle shifters. Those are some serious vertically aligned doors.

30. Lamborghini Aventador LP700-4 The eternal poster child for the teen car enthusiast set, Lamborghinis always look dramatic and possibly ridiculous at the same time. Let it ever be thus.

31. Lincoln Executive Limousine This beautiful, luxurious '67 sedan is still a head turner even today.

32. Mazda Miata Anniversary Need a Miata? Mazda has lots of 'em. Celebrating 25 years of their MGB copy--reliable, and without a leaky top. A new one is due very soon.

33. Mercedes-Benz S63 AMG Coupe Mercedes hit one out of the park with its stunning S Class coupe; this AMG version packs even more horsepower while retaining the regular car's beautiful lines.

34. Mini Cooper S The 2015 Mini Cooper S hardtop, in what appears to be its Integrated Circuit Edition or something.

35. Nissan GT-R Nismo 2015 Nissan GT-R, done up Nismo style with many performance parts, a matte paint job, and a carbon fiber wing. Because the regular one wasn't fast enough.

36. Nissan Murano Unveiled this week in New York: the all-new 2015 Nissan Murano. It's the third generation for this popular crossover SUV.

37. Nissan Sports Sedan Concept The Nissan Maxima has had a bit of an identity crisis ever since the Altima moved upmarket; this concept car looks to be a solid direction for the nameplate.

38. Packard Light Eight Some cars will always remain a classic. This gorgeous 1932 luxury car, fresh off the Roaring Twenties, is quite large and imposing—in a really good way.

39. Porsche 918 Spyder What hasn't been said already about this car? It's the Porsche 918 Spyder. 887hp, and yet it has more EV range than a plug-in Prius hybrid. Yours for just $845,000.

40. Scion FR-S Release Series 1.0 Scion unveiled an F-RS done up TRD (Toyota Racing Development) style. Could a power increase for the regular model be far behind?

41. Soleil Anadi This coachbuilt supercar, complete with a matte red finish and tons of real wood inside, is based on a Corvette and features a supercharged V8 outputting 651 horsepower.

42. 2015 Toyota Camry Instead of the usual, mid-cycle refresh you'd expect of a 2012 design, Toyota practically redid the car from the ground up to address complaints that it's not exciting enough to look at or drive. (Photo credit: Toyota)

43. Toyota i-Road This nifty three-wheeled electric concept car from Toyota looks to turn personal transportation on its head.

44. Toyota FV2 Concept This thing has no steering wheel; instead, the driver shifts his or her body, and the car moves to compensate like an overgrown, super-advanced Segway-style bobsled, as a reader pointed out to us. Insane.

45. Volkswagen e-Golf An all-electric Golf. It's interesting to see VW push hybrids and EVs after so many diesel-powered TDI models.

46. Volkswagen Golf R A 2015 Volkswagen Golf R looking very blue. Cool blue. VW aficionados know this car is much faster than it looks.

47. Volvo Connected Car Concept I wonder if Volvo is showing off any connected car tech at the auto show this week.

Further Reading

Cars & Auto Reviews

Cars & Auto Best Picks