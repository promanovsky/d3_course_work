NEW YORK, July 15 (UPI) -- Mosquitoes collected from Queens and Staten Island tested positive The West Nile Virus, marking the first 2014 citing of the potentially deadly virus in America's largest city.

According to the New York City Department of Heath and Mental Hygiene, the infected mosquitoes were collected from Douglaston and College Point in Queens and Old Town in Staten Island.

Advertisement

"Now that West Nile virus has returned to New York City, it is important to take simple precautions to protect you and your family," Health Commissioner Dr. Mary Bassett cautioned in an official statement.

"During warm weather, mosquitoes can breed in any still water that stands for more than four days, so the most effective way to control mosquitoes is to eliminate standing water. New Yorkers are also encouraged to mosquito-proof their homes, wear mosquito repellent and cover their arms and legs if they're outside at dawn or dusk. New Yorkers over 50 should be especially cautious, as they are more likely to develop serious illness if they contract the virus."

The Department of Health goes on to warn that although not all West Nile cases are fatal, "West Nile virus can cause serious complications, including neurological diseases, and can also cause a milder flu-like illness with headache, fever and fatigue, weakness and sometimes rash."

In 2013, 10 New Yorkers contracted the virus but none died.

Thus far in 2014 the Center for Disease Control reports 18 known cases of West Nile in humans in the United States.