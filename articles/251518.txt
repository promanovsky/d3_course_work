Mariah Carey fans are definitely a dedicated lot. If they have to spend over a thousand dollars to acquire her latest album, then they’re more than willing to do it.

The singer recently released Me. I am Mariah… The Elusive Chanteuse, her latest — and long-delayed — studio effort. Judging from the early reviews, people who weren’t too impressed with her recent endeavors should definitely give it a fair shot. Apparently, it’s one of the best records she’s recorded in a very long time.

Unfortunately for hardcore Mariah Carey fan Milo Yiannopoulos, the album didn’t arrive on London retail shelves in a timely manner. Instead of downloading the songs illegally or waiting for the official release, the Business Insider columnist decided to purchase a copy of the record in Germany. The cost of his dedication: $1,200.

After referring to the record label executives as “a**holes,” Milo explained why he decided to make the costly trip to Germany for a Mariah Carey CD.

Yiannopoulos wrote:

“In the age of torrenting, staggered international releases are ridiculous. Thanks to differing time zones, a Friday release in Australia actually means that the album was in the wild as early as 3 p.m. London time the previous day—i.e., before I even arrived at the airport. Does Mariah’s record label really expect fans not to download a major new release at the earliest possible second?”

Milo believes that it’s much harder these days to prove that you’re a “nut-job groupie,” especially since you can acquire Mariah Carey’s new album for free on a number of different websites. However, it would appear that the writer also wanted to prove to his idol that he’s one of her biggest fans on the entire planet.

“I’m hoping no one bested me and flew to Australia, by the way, because that would murder my standing in the forums,” he wrote.

Proving that you’re the biggest fan by purchasing a $1,200 plane ticket to Germany is a little ridiculous, regardless of how much you love Mariah’s music.. There are plenty of hardcore fans out there who simply don’t have the extra money to spend on such a frivolous excursion. Just because they can’t toss around wads of cash doesn’t make them any less supportive of Carey’s career. Dedication is certainly not measured by dollar signs.

Alas, Milo’s attempt to draw attention to himself seems to have worked out in spades. Other websites are picking up this story, and chances are folks are discussing it at-length on social media. Whether or not Mariah Carey will care is another matter altogether.

[Image via Tutgen]