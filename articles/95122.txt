Groundbreaking London research is looking at the role bacteria may play in breast cancer.

A first-of-its-kind study has identified bacteria in breast tissue that was associated with cancer.

At a Lawson Health Research Institute lab, microbiologists Camilla Urbaniak and Dr. Gregor Reid could be changing our understanding of cancer by looking at bacteria in the breast.

"There were what we call, some beneficial bacteria, but there was also E. coli, pseudomonas and bacillus that are bacteria that tend to not be ones that you necessarily think of as beneficial," Reid says.

The study looked at samples from 81 women - including 10 cancer-free tissue samples and 71 samples from cancerous tumours.

There were noticeable differences in the bacterial composition of the two groups.

Urbaniak says "There's a different colour profile between women who are healthy...and women who had cancer...Some of the bacteria that we saw that were associated with cancer were bacillus which you can see in the orange, E. coli which you can see in the pink, staphylococcus which you can see in the purple."

Previous research has focused on things like genetics, such as the role of the BRCA gene in breast cancer, and environmental factors such as obesity.

Now, bacteria can be added to the list, Urbaniak explains "It could be an interaction between the genetic component and these bacteria and other environmental components."

And since we know that priobiotics can positively affect gut health, might the same beneficial substances influence breast health? Related bacterial research offers tantalizing possibilities.

"It shows you how closely associated microbes are with our body and our health," Reid says. "And therefore when you try and modulate them through probiotics chances are you could have an effect that's beneficial."