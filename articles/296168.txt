Miss USA Nia Sanchez slammed allegations that she's not from Nevada, the state she represented in the 2014 beauty pageant.

Along with fame comes a lot of controversies. Recently crowned Miss USA, Nia Sanchez is already facing the heat generated from her new found stardom. According to FOX411, a "well-placed" source accused Sanchez of not being from Nevada, the state she represented in the 2014 beauty pageant.

It is a well known fact that Sanchez lived in California for a while and worked in Disney there before she moved to Nevada. However, the source revealed that the pageant winner competed for the Miss California crown in 2010, 2011 and 2012, failing to win each time. She just managed to meet the minimum residency requirements in order to compete for the Miss Nevada crown, and didn't actually live there.

"She never actually moved to Nevada, but continued to work at Disney and live in California, setting up some minimum paper trail to appear like she was in Nevada and allow her to compete," the insider told FOX411. "Shanna Moakler is the Nevada director, and she personally coached Nia on what she needed to do to qualify in Nevada and skirt around the need to be actually from there."

Of course, Sanchez slammed the allegations saying there's no truth to the allegations. She also confirmed that she had lived in the state for more than 18 months before winning the title, which is much more than the minimum 6 months required to represent the state.

"[Las Vegas] is my home," she fold FOX411 in a recent one-on-one interview. "I have a house there with a friend. I actually had an agent that was working me in Nevada a lot so I figured why not work in that state, and then I looked into the pageant program because I had done pageants before. So I figured why not look into the one in Nevada? I really liked the way that it was run, the director that ran it, it seemed like a really healthy, well-run state program. So I decided to go there since I was living there anyway."

This is not the first time a beauty pageant winner has faced scrutiny for her ethnicity. In September last year, when Nina Davuluri was crowned Miss America 2014, she faced a terrible backlash from several Americans who were upset with the fact that the crown went to someone with an Indian heritage. The 24-year-old then went on record to explain that no matter what her ethnicity, she was first an American.

"I have to rise above that," she said, according to a Washington Post report. "I always viewed myself as first and foremost American."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.