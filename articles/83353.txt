The legitimacy of a new technology, which claimed it could create embryonic-like cells from mature mouse cells, has been questioned after its lead researcher was found guilty of “scientific misconduct” for manipulating research data.

The technology, called stimulus-triggered activation of pluripotency, or STAP, was presented in two research papers in Nature in January. Researchers at the RIKEN Center for Developmental Biology, or CDB, in Kobe, Japan, had claimed that the method could turn ordinary mature mouse cells into cells that share the ability of embryonic stem cells to replicate all of a body’s cells.

However, as many as six problems were identified in the study’s methodology after the papers were published, and while a six-person committee dismissed four of the problems as innocent errors, in the remaining two cases, CDB’s Haruko Obokata, the lead researcher, was found manipulating data “in an intentionally misleading fashion,” Nature reported Tuesday.

According to the publication, one of the problems concerned a diagram used in the paper while the other issue was related to the use of an image from Obokata’s doctoral thesis. According to Obokata, the use of the image was a genuine mistake but the investigating committee ruled that it was a deliberate act because the captions on the image had been changed.

The committee, however, did not comment on whether the new technology works, or whether STAP cells actually exist.

“That is beyond the scope of our investigation,” Shunsuke Ishii, a molecular biologist at RIKEN in Tsukuba, Japan, and the committee’s chair, reportedly said.

While Obokata said that the committee’s judgment of her conduct is “unacceptable,” a spokesperson for Nature said the journal will conduct its own evaluation of the matter.

“Nature does not comment on corrections or retractions that may or may not be under consideration. Nature takes all issues related to these papers very seriously, is conducting its own evaluation and considering the results of the RIKEN investigation. We cannot comment further at this stage,” the spokesperson said.