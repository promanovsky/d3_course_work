Hydrogen-powered Toyota costs a bomb

Share this article: Share Tweet Share Share Share Email Share

By: IOL Motoring Staff Tokyo, Japan - These are the first official pictures of Toyota's production-spec hydrogen-powered Fuel Cell Sedan, following the car's appearance in concept form at the 2013 Tokyo motor show. In typical Japanese 'leak' format, they show only the exterior, although the inside is likely to follow much the same layout as the concept. The four-door FCV Sedan will be introduced first in Japan, in the first quarter of 2015, ahead of launches in the US and European markets in mid-year. It'll retail in the domestic market at about ¥7 million (R738 000) and sales will initially be limited to those parts of the country where a hydrogen refuelling infrastructure is under development.

Prices for Europe and the US have not yet been decided.

Toyota has been developing fuel cell vehicles in-house for more than 20 years, including its own fuel-cell stack that generates electricity from the chemical reaction between 'canned' hydrogen, stored in a high-pressure tank, and the oxygen in the air.

And, as with all hydrogen-powered vehicles, you can drink what comes out of the tailpipe - it's pure water vapour.

Toyota's first one-the-road application was in its Fuel Cell Hybrid Vehicle, an SUV that was leased to customers in Japan and the US on a limited basis from 2002.

Since then, the company has improved its fuel-cell system to the point where, it claims, the new Fuel Cell Sedan delivers performance and range similar to that of a petrol-engined vehicle, while refuelling takes about three minutes.

STEP BY STEP

Advocates of fuel-cell vehicles say that can combine emissions-free driving with the convenience of petrol-powered engines, although the definition of 'emissions-free' can depend on how the hydrogen is produced in the first place.

Toyota Europe vice-president Karl Schlicht commented: “We're very excited about the arrival of fuel-cell technology, although there are many challenges ahead, such as the availability of fuelling infrastructure and customer awareness.

“But our history with hybrids gives us experience in bringing a new technology to market. In Europe we will be taking it step by step, gradually introducing the car in selected markets.”

Toyota is also designing fuel-cell forklifts and buses, and developing and testing fuel cells for use in homes.