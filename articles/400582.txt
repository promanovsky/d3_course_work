It certainly seems like a harmless enough activity -- you grab a drink, plop down in a lawn chair on a nice summer day and nod off. However, the U.S. Surgeon General feels very strongly otherwise, and is speaking up about the dangers of tanning, both indoor and out.

Calling the potential of developing skin cancer from too much exposure to the sun a "major public health problem," Acting Surgeon General Boris Lushniak has released a report that essentially warns the public of the dangers of both indoor and outdoor tanning.

"Right now we're seeing kind of a bad trend developing when it comes to skin cancers. Skin cancers -- melanoma and nonmelanoma skin cancer -- are increasing. It got to the point for us, right now, to be able to say, we need to have this call to action," he explained.

As for the sudden urgency, Lushniak added, "Certainly, this time of year, we're in the midst of this vacation season. Knowing that as August comes around, it's the last big spurt of people heading to the beach, people spending time having that potential exposure to ultraviolet radiation. We thought now was a really good time to get the message out."

Lushniak also pointed out that his office has seen melanoma cases triple over the course of the last 30 years. While it can't pinpoint the exact reasons for the dramatic increase, he felt the time was now to address the situation. More information on the report is located here.

In an earlier report on the subject of the dangers of tanning by Tech Times, it was revealed that a study conducted by researchers at the Norris Cotton Cancer Center and director of the Children's Environmental Health and Disease Prevention Research Center at Dartmouth, found the ultraviolet (UV) radiation lamps used for indoor tanning put adolescents and young adults at risk for basal cell carcinomas, the most common form of skin cancer.

The study's research also concluded that the devices used in indoor tanning facilities produced 10 to 15 times as much UV rays as the midday sun.

In yet another recent study conducted by JAMA Dermatology, these findings were also released:

The number of skin cancer cases due to tanning is higher than the number of lung cancer cases due to smoking; and

In the U.S. alone, 419,254 cases of skin cancer can be attributed to indoor tanning. Out of this number, 6,199 are melanoma cases.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.