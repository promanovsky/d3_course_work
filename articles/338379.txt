A panel of mental health experts has concluded that Oscar Pistorius was not suffering from a mental illness when he killed girlfriend Reeva Steenkamp in his home last year, the chief prosecutor at the athlete's murder trial said Monday.

Pistorius' trial resumed after a break of one month during which a psychologist and three psychiatrists also assessed whether the double-amputee runner was capable of understanding the wrongfulness of his act when he shot Steenkamp through a closed toilet door.

The panel's reports were submitted to Judge Thokozile Masipa, and prosecutor Gerrie Nel referred to key parts of the conclusions, noting that the experts believed Pistorius was "capable of appreciating the wrongfulness of his act" when he killed Steenkamp, a 29-year-old model.

The evaluation came after a psychiatrist, Dr. Merryll Vorster, testified for the defence that Pistorius, who has said he feels vulnerable because of his disability and long-held worry about crime, had an anxiety disorder that could have contributed to the killing in the early hours of Feb. 14, 2013. He testified that he opened fire after mistakenly thinking there was a dangerous intruder in the toilet.

Prosecutor Gerrie Nel has alleged that Pistorius, 27, killed Steenkamp after a Valentine's Day argument, and has portrayed the Olympic athlete as a hothead with a love of guns and an inflated sense of entitlement. But he requested an independent inquiry into Pistorius' state of mind, based on concern the defence would argue Pistorius was not guilty because of mental illness.

Pistorius faces 25 years to life in prison if found guilty of premeditated murder, and could also face years in prison if convicted of murder without premeditation or negligent killing. He is free on bail.

Pistorius was evaluated as an outpatient at Weskoppies Psychiatric Hospital in Pretoria, the South African capital. He has been staying at the upscale home of his uncle.

Childhood surgeon testifies

Later Monday, defence lawyer Barry Roux called surgeon Gerald Versfeld, who amputated Pistorius' lower legs when he was 11 months old, to testify about the runner's disability and the difficulty and pain he endured while walking or standing on his stumps without support. Pistorius was born without fibulas, the slender bones that run from below the knee to the ankle.

At the beginning of image gallery At the end of the image stream

At Roux's invitation, Judge Masipa and her two legal assessors left the dais to closely inspect the stumps of Pistorius as he sat on a bench.

The athlete was on his stumps when he killed Steenkamp, and his defence team has argued that he was more likely to try to confront a perceived danger rather than flee because of his limited ability to move without prostheses. Versfeld noted that Pistorius' disability made him "vulnerable in a dangerous situation."

During cross examination, Nel questioned Versfeld's objectivity and raised the possibility that Pistorius could have run away from a perceived intruder on the night of the shooting. He also said Pistorius rushed back to his bedroom after the shooting and made other movements that indicated he was not as hampered as Versfeld was suggesting.

Defence calls acoustics expert to testify

Roux, the chief defence lawyer, also called acoustics expert Ivan Lin to testify about the challenges of accurate hearing from a long distance.



Neighbours have said in court that they heard a woman screaming on the night that Pistorius shot Steenkamp, which could bolster the prosecution's argument that the couple was arguing before Pistorius opened fire.

The defence, however, has suggested they were actually hearing the high-pitched screams of a distraught Pistorius after realizing he had shot Steenkamp through the toilet door.



At times during the trial, Pistorius has sobbed and retched violently in apparent distress, forcing Judge Thokozile Masipa to call adjournments. On Monday, Pistorius was calm and took notes during testimony.