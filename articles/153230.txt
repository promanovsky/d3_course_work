Asteroids cause dozens of nuclear sized explosions in Earth's atmosphere

The Nuclear Test Ban Treaty Organization is saying between 2000 and 2013 its network of sensors observed 26 nuclear sized explosions from asteroids. Thankfully, most of those were in the upper atmosphere.