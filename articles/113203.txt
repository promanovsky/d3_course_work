A new UN report tackling greenhouse-gas emissions should serve as a "wakeup call" for entrepreneurs, especially in the energy sector, US Secretary of State John Kerry said today.

"We've already had wakeup call after wakeup call about climate science. This report is a wakeup call about global economic opportunity we can seize today," he said of the Intergovernmental Panel on Climate Change (IPCC) report issued in Berlin.

"The global energy market represents a USD 6 trillion (4.34-trillion-euro) opportunity, with six billion users around the world. By 2035, investment in the energy sector is expected to reach nearly USD 17 trillion."



"We already know that climate science is unambiguous and that every year the world defers action, the costs only grow," Kerry said in a communique issued by the State Department.

"But focusing only on grim realities misses promising realities staring us right in the face. This report makes very clear we face an issue of global willpower, not capacity."



The report by the Nobel-winning expert panel said the world had a likely chance of meeting the UN's warming limit of two degrees Celsius if it cuts annual greenhouse-gas emissions 40 to 70 per cent by 2050, especially from energy.