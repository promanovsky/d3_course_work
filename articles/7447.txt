A promotional video for Wren Studio, a Los Angeles-based women’s clothing company, has gone viral within 24 hours of being published on YouTube Monday.

The three-minute video, titled “First Kiss,” which is shot by writer-director Tatia Pilieva, of 20 strangers kissing each other, had attracted more than 19 million views by Tuesday night.

“Here's my FIRST KISS film: I asked 20 strangers to kiss for the first time w/ @WRENSTUDIO and many dear hearts,” Pilieva wrote on her Twitter account as she posted the video, which features the first kiss between young, old, straight and gay pairs.

“The key trick I used was not to say ‘action,’” the filmmaker reportedly told WHAS11 news based in Louisville, Ky. “I’ve done lovemaking scenes before, but there was something so tender about this that we all kind of melted behind the five cameras we were using.”

Pilieva told WHAS11 that some of the actors in the video responded so well that their interactions did not end even when she reluctantly called out the end of the scene.

“We kind of grouped them together just by instinct,” Pilieva reportedly said. “I was really nervous at first, but you can see how some of the people responded.”

According to reports, the cast of the video included models Natalia Bonifacci, Ingrid Schram and Langley Fox, musicians Z Berg of “The Like,” Damian Kulash of “OK Go,” Justin Kennedy of “Army Navy,” singer Nicole Simone and singer-actress Soko.

It also included actor Karim Saleh, Matthew Carey, Corby Griesenbeck and Luke Cook and actress Elisabetta Tedla, Jill Larson and Marianna Palka.

The filmmaker said that none of the people were paid for the advertisement.

Check out the viral video here: