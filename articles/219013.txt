Joe Francis, the founder and creator of "Girls Gone Wild," was arrested Friday evening in Los Angeles on suspicion of assault after allegedly being involved in a "pushing and shoving" incident.

According to The Los Angeles Times, Los Angeles police officers detained and arrested the 41-year-old entrepreneur at an office building on the 10900 block of Wilshire Boulevard.

The Los Angeles County Sheriff's Department released Francis early Saturday morning on a $20,000 bail, according to the department's website.

Los Angeles Police Sgt. John Juarez said a federal Bankruptcy Court issued a "stay away" order and authorities are currently investigating whether or not Francis violated the order by being at the building Friday night.

In February 2013, Francis filed for Chapter 11 bankruptcy protection, according to the Times.

Since 2008, Francis has been tied up in legal battles with casino mogul Steve Wynn, who was also one of Francis' creditors.

Wynn filed a civil defamation case against Francis, which resulted in the Nevada Supreme Court upholding a $7.5 million judgment granted in Wynn's favor earlier this month.

In 2012, Francis' attorneys withdrew from the case, leaving the soft-porn video empire founder to defend himself. As a result, he requested that Clark County judge's default judgment be set aside but a three-justice panel rejected his motion.

The justices told Francis that he was just trying to stall proceedings as a last-minute attempt to challenge the default judgment.

Wynn also won a $21-million judgment in a separate slander trail that took place in Los Angeles in 2012. And in 2008, Wynn filed a lawsuit against Francis over a $2-million gambling debt Francis had accrued at the Wynn Las Vegas resort.

The Times also reported that last August Francis was sentenced to 270 days in jail and three years of probation after he was convicted of five charges including assault and the false imprisonment of three women.

Included in his sentence, Francis is supposed to complete a year of psychological counseling and anger management.