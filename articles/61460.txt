From acquiring WhatsApp , to now announcing the acquisition of Oculus VR, things are moving at a frantic pace in 2014 at the headquarters of the social-networking giant, Facebook, in Menlo Park.

Amongst the latest developments is the growth of Instagram, and slight changes to the service. Bought by Facebook two years ago for a cool $1 billion, Instagram announced it recently crossed 200 million users. In addition, it has started testing integration with Facebook Places for location tagging in addition to Foursquare.

Making the user base announcement on the Instagram blog on Tuesday, the company said, 'Today we're excited to share that the Instagram community has grown to more than 200 million Instagrammers.'

The blog goes on to mention that till date in excess of 20 billion photos have been shared on Instagram and that there has been a growth of 50 million users in the last six months.

In addition, Instagram has also started testing integration with Facebook Places, with some users reporting seeing the location tagging option along with Foursquare. According to some reports the idea behind the new testing might be to replace Foursquare, though this seems unlikely, given the long partnership with the service.



The free API and expansive database provided by Foursquare has been used not only by Instagram for location sharing but also by the likes of Pintrest, Path, Vine and Flickr. Instagram uses the API data to attach the pictures, clicked and edited via its photo editing filters, to the places where they were clicked.



Engadget received a statement from the company, commenting on the possible removal of Foursquare, that said, "Foursquare is a great partner, and people will continue to be able to share their check-ins to Foursquare from Instagram. We are constantly testing experiences throughout the app to provide the best possible user experience as part of future planning."

It definitely does look like Instagram is trying out new avenues to enhance the user experiences. While it might still be too early to comment whether it will eventually ditch Foursquare for Facebook Places, it would not affect Foursquare a lot given that it still have other big players to cater to.

If the Facebook Places integration does make it to Instagram, it would mark the first major integration of Facebook services since the social network acquired the photo-sharing services