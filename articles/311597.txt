Google Glass is now available in the United Kingdom for about £1,000. Unfortunately, this device is targeted to developers, not consumers.

Google Glass now available in Europe

The Explorer Edition is currently available through the Google Play. It is available in Cotton, Charcoal, Shale, Sky, and Tangerine. The Titanium frames are also available add-on. Every single Explorer Edition comes with an extra shade or frame for free. Frames are separately sold for £175 or shades £120. And that is not all, Google is also selling new accessories that go with the Explorer.

There is a pair of Stereo Earbuds starting at £65 and a mono iteration for £40. Both sets come with a V shape Glass branding on the outside. Padded pouches and storage cases also start at £40. A clear shield visor retails for £60. All the items ship within the first one or business days.

Gates Capital: Why (Free) Cash (Flow) Is King Gates Capital Management's ECF value fund has earned strong returns for investors since its inception in June 1996. The ECF, or Excess Cash Flow Fund, had returned 12.9% annualized since inception to the end of September 2020. That was compared to 8.9% for the S&P 500 total return index and 7.4% for the HFRI Event-Driven Read More

Customers who live in or near London will get the opportunity to try out Glass before buying. Interested fans can RSVP to Google Inc (NASDAQ:GOOG) (NASDAQ:GOOGL)’s demo days from July 27th to 28th.

Guardian created a Glass application

The UK’s premiere newspaper Guardian released its own application called “the Guardian for Glass”. This new apps delivers breaking news notifications and special reports with the option of having it all delivered as audio files. The editor-in-chief for Guardian News and Media Alan Rusbridger explained the thing that interests his company most about Google Glass. He said Google Glass inspires journalistic opportunities and allow the news source to collaborate with readers around the world while still respecting privacy.

Google kicked off the launch of Google Glass with a promotional video that covers the city including soccer, cycling, and black cabs. The fact Google launched Glass in the United Kingdom is rather exciting considering Google Inc (NASDAQ:GOOG) (NASDAQ:GOOGL) kept the Explorer program exclusive to the United States.