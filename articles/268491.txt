Apple unveiled the brand new iOS 8 mobile operating system Monday at its keynote speech kicking off the company's annual Worldwide Developers Conference (WWDC) in San Francisco. The early verdict? iOS 8 looks like an exciting update.

There are several key new features in iOS 8 but one thing is certain: Apple is looking to expand its usefulness. For starters there's the new HealthKit, a software that compiles user health data into one place, drawing from other apps such as Nike's FuelBand. HealthKit is also an obvious step in the direction of wearables that can track user health information in real time.

Apple also confirmed rumors that iOS 8 would bring home automation. iOS 8 users will be able to control things like garage doors, thermostats, lights, and more. Of course, the necessary hardware isn't included, so you'll still have to pony up for the right appliances.

Photo junkies will be happy to know that all photos will be stored in iCloud, allowing for easy access from different devices.

Interactive notifications will also hit iOS 8, allowing for third-party apps as well to give users actions they can perform. It's similar to Android, but hey, better late than never.

Siri's also gotten a slight upgrade in iOS 8. Users will be able to activate Siri with a simple phrase -- useful in on-the-road situations. Apple also included Shazam, the song recognition app, in Siri's new repertoire, meaning she'll now be able to tell you what that song coming out of the radio is, all hands-free. Siri also has access to better maps and global information such as the lunar calendar.

Apple is also going to allow third-party keyboards systemwide on top of a revamped first-party keyboard as well as third-party filters to be applied to photographs right in the camera app. As mentioned in Latin Post's look at upcoming Apple's desktop operating system, OS X Yosemite, there's also more connectivity between iOS and Macs. Users will be able to airdrop between OS X and iOS 8, as well as pick up working on a task started in iOS on a desktop.

On the app side of things, Apple is revamping the App Store to have continuously scrolling searches as well as a TestFlight beta testing area where developers can ask the public to beta test an app. Developers will also be able to sell app bundles in iOS 8 and create short app preview videos and families can share apps if they are purchased with the same card.

Apple is also giving developers a new SDK with over 4,000 new APIs, including new camera APIs. A new graphics API named Metal promises to drastically reduce the overhead between a game and the processor.

All in all, iOS 8 looks exciting. Those with multiple Apple products will find a more seamless digital environment than ever before, while developers now have access to tools that should all make for some exciting apps in the coming years.

Although no specific release date was given, iOS 8 will land on the iPhone 5s, iPhone 5c, iPhone 5, iPhone 4s, iPod touch 5th generation, iPad 2, iPad with Retina display, iPad Air, iPad mini, and iPad mini with Retina display. For those at WWDC, however, beta testing began Monday after the morning keynote.

For more stories like this, follow us on Twitter!