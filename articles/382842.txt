The U.S. Centers for Disease Control and Prevention says it has closed two laboratories at its Atlanta headquarters following a series of incidents involving the mishandling of dangerous pathogens.

Laboratories at the agency dealing with flu and anthrax have been closed and shipments of any infectious agents originating in its highest-security labs have been halted, federal officials say.

An investigation of a possible anthrax exposure in June turned up evidence of at least five incidents of improper handling of pathogens in the last 10 years, they say.

"These events should never have happened," CDC Director Tom Frieden said in a news conference.

"I'm disappointed, and frankly I'm angry about it," he said, adding he was "astonished that this could have happened here."

In the June incident, CDC researchers working in a high-security lab did not follow procedures meant to deactivate the anthrax bacteria and the live pathogen was then transferred to a lower-security facility in the agency's Atlanta complex.

In a second incident described in the report, another lab at the CDC accidentally allowed a dangerous bird flu strain responsible for the deaths of almost 400 people since 2003 to contaminate a relatively benign flu sample.

The flu and anthrax labs at the CDC will remain closed while new handling procedures are decided on by a panel of experts convened to recommend revised procedures, Frieden said.

Current biosecurity procedures are formulated with the goal of keeping "bad guys out of the lab," Michael Osterholm of the University of Minnesota says.

"One of the critical issues we need to focus on is the good guys who just forget to do it safely," says Osterholm, who as a member of the National Science Advisory Board for Biosecurity advises the federal government on safety protocols.

Frieden said any CDC staff members who are found to have knowingly violated research protocols or who failed to report a laboratory breach would be disciplined.

The latest disclosures come days after it was reported that unaccounted-for vials containing smallpox virus dating to the 1950s were discovered in a laboratory storage area at the National Institutes of Health complex in Bethesda, Maryland.

In two out of the six samples, the virus was found to still be alive, Frieden said. After testing, the virus would be destroyed, he said, noting no infections were reported in the incident.

One expert called the CDC's announcements "disturbing" but praised the agency for its openness about the incidents and its commitment to preventing such occurrences in the future.

"They deal with the most sensitive infectious agents and so we expect they will adhere to very high standards," said Vanderbilt University infectious disease expert Dr. William Schaffner.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.