The Amazing Spider-Man 2 releases in the UK on 16 April, a couple of weeks earlier than the US release date on 2 May. However, early reviews of the movie have started pouring in and looks like the film has received a mixed response from critics.

The Spidey reboot sequel, directed by Marc Webb stars Andrew Garfield as the web-spinning superhero along with Emma Stone as Gwen Stacy, Dane DeHaan (Chronicles) as Harry Osborn/ Green Goblin and Jamie Foxx as Max Dillon/ super villain Electro.

Here's what critics have to say about the film:

"Shortly after wrapping Batman & Robin its star, George Clooney, turned to director Joel Schumacher and quipped, 'I think we might have killed the franchise'. I wonder if Andrew Garfield said the same thing after watching the finished print of this squished spider of a superhero sequel," says a disappointed David Edwards of Mirror.

"The stars lack sparkle, the story is rote and workmanlike and the action unexciting," he states in his review.

Simon Brew, in his Den of Geek review, calls the film "a step forward for the franchise."

"It might still leave a good deal of room for improvement, it might ultimately be less successful than Captain America: The Winter Soldier, and we're still some way away from the peak of Sam Raimi's trilogy here," says Brew.

"But it does feel as though Marc Webb has taken on an overladen screenplay, and put a more successful stamp on it this time around. It might not be amazing still, and it remains frustrating, but at least this time the new Spider-Man movie is quite good as well."

Praising DeHaan and Garfield for their performance in the film, Simon Reynolds of Digital Spy says:

"DeHaan fares much better, playing a Harry Osborn who's more jittery and complex than playboy smarmy like his predecessor James Franco. The Oscorp heir and Peter manage to rekindle their childhood friendship, but the bromance turns sour as Harry becomes obsessed with vanquishing the genetic disorder that took his father and has taken hold of him."

"Despite the uneven nature of the whole endeavour, praise needs to go to Garfield for holding it all together. He's more at ease in the role than Tobey Maguire ever was, and manages to nail the big moments when called upon. He's an absolute revelation as this character," states Reynolds.

The Telegraph review gives the film a 2/5 rating and credits the sparks between Andrew Garfield and Emma Stone to the saving grace of the film.