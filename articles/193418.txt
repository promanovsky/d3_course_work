Nintendo has finally announced that it will launch its smartphone service alongside Mario Kart 8 at the end of May this year. Dubbed 'Mario Kart TV,' according to The Verge, it will serve as a trusty companion to fans of the game, allowing them to watch both official and user-generated videos, along with other functions.

Apart from watching videos, users will also be able to view and follow rankings via desktop, phone and tablet using a cross platform web-interface.

"This "Mario Kart TV" (temp.) web service is available even for players without NNIDs", said Nintendo in a post.

"But by logging into the service with an NNID, they will be able to easily find their rankings, videos that their friends have shared, videos of tournaments in which they have participated, and it will be convenient to access this service through smart devices even when they are not at home."

Nintendo Figurine Platform

Remember that NFC reader on the Wii U GamePad? Nintendo will be launching the Nintendo Figurine Platform, or NFP, which will let users interact with toy figures a la Disney Infinity. However, Nintendo is going the extra mile by letting users carry these figurines across different Nintendo games.

More details on the same will be served up during E3 this June, while the figures will hit store shelves during the holiday season this year. Also worth noting is that the company intends to release an NFC adapter for the Nintendo 3DS handheld.

Wii U Update

Users of the Wii U should expect an update that will reduce system start-up time by a full 20 seconds.