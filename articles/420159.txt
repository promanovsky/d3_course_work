For the second year in a row, Netflix racked up nominations and buzz before the Emmy Awards but then went home nearly empty-handed.

While the online streaming behemoth got all the chatter leading up to Monday night's ceremony, thanks to the political thriller House of Cards and the first year of Orange is the New Black eligibility, Netflix was completely shut out of the big awards. Granted, it got seven at last week's Creative Arts Emmys (including a guest actress in comedy prize for OITNB star Uzo Aduba), but no one watches that.

Nominations but no win: House of Cards.

Even more telling, seven wins from 31 nominations this year is a similar winning percentage to what Netflix had at last year's Emmy Awards. In 2013, Netflix dominated the conversation leading up to the awards, with 14 nods going to House of Cards and Arrested Development. The questions were constant: Would Kevin Spacey beat Bryan Cranston of Breaking Bad? Robin Wright could definitely take down Claire Danes of Homeland, right? Wrong. The streaming service won only three awards, two of them technical. That included House of Cards producer David Fincher for directing in a drama, though he didn't even show up to collect the trophy.

The perception-versus-reality contrast is fascinating at the Emmy Awards, and really, in the ever-evolving television industry in general. Even host Seth Meyers had everyone primed to believe that Netflix was about to have a big night.