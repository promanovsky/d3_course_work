Top Tech Stocks

MSFT -0.10%

AAPL +1.81%

IBM -0.66%

CSCO +0.82%

GOOG +1.94%

Technology stocks were ending higher this afternoon with shares of technology companies in the S&P 500 rising nearly 0.8%.

In company news, Intel Corp. ( INTC ) was up slightly more than 1% in late trading Tuesday afternoon after the chip-maker announced a new partnership with Chinese semiconductor firm Rockchip to work on new technologies for mobile devices.

The two companies are expecting to deliver an INTC-branded mobile system-on-a-chip platform based on the company's technology, according to a statement. The chips are slated for value-priced tablets running the Android operating system and should become available during the first half of 2015.

INTC was up 1.3% at $26.64 a share in late trade, just 8 cents off its session high. The stock has a 52-week range of $21.89 to $27.24 a share, adding 15.4% over the past 12 months.

In other sector news,

(+) SPEX, U.S. Sen. Patrick Leahy (D-Vt.), Judiciary Committee chairman, removes patent reform bill from the committee calendar.

(-) NTLS, Reduced to Hold from Buy at Jefferies, which also cut its price target by $10 to $15 a share.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.