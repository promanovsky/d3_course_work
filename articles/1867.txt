The average price of gas in Massachusetts has risen 16 cents a gallon in the last month, AAA Southern New England said Monday.

A gallon of regular unleaded currently costs about $3.52, up four cents from last week, according to a weekly survey by the auto group. Despite the increase, that price is still 17 cents cheaper than the cost of a gallon of gas at this time last year.