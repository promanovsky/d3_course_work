Arnold Schwarzenegger will play an ageing T-800 in Terminator: Genesis.

The role catapulted Schwarzenegger into international stardom in 1984, when he starred in James Cameron's The Terminator.

Tristar Pictures



It was announced last year that the actor would reprise the role in the upcoming fifth installment of the series.

Emilia Clarke, Jai Courtney and J.K. Simmons are among the actors who've also signed on to star in the film.

In an interview with MTV, the 66-year-old actor revealed that there will be a younger T-800 model, and that these Terminators will be able to age in the franchise.

He said: "The way that the character is written, it's a machine underneath. It's this metal skeleton. But above that is human flesh. And the Terminator's flesh ages, just like any other human being's flesh. Maybe not as fast. But it definitely ages."

The Alan Taylor-directed sequel will begin production this April, and is scheduled for release in July 2015.

Arnold Schwarzenegger's 20 Best One-Liners



This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io