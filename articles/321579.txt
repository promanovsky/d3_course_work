Women who give birth after the age of 33 live longer: Remaining fertile later in life is linked to a slower ageing process



Study found genetic variants mean women remain fertile for longer

Having a natural ability to give birth later in life indicates a woman's reproductive system is ageing slowly, therefore so is the rest of her body

Scientists say genes could be key to slowing the ageing process



A new study has found women who give birth naturally later in life are more likely to live longer

Women who give birth later in life live longer and as a result could provide an insight into slowing the ageing process, scientists have said.



Genetic variants that mean women remain fertile for longer may be the key, researchers said today.



The study found women who had their last child after the age of 33 were twice as likely to celebrate their 95th birthday, compared to those who gave birth for the last time by 29.

Lead researcher Dr Thomas Perls, from Boston University Medical Center in the US, said the findings do not mean women should delay having children.

He said: 'Of course this does not mean women should wait to have children at older ages in order to improve their own chances of living longer.

'The age at last childbirth can be a rate of ageing indicator.



'The natural ability to have a child at an older age likely indicates that a woman’s reproductive system is ageing slowly, and therefore so is the rest of her body.'

Researchers analysed data from the Long Life Family Study (LLFS), a social and genetic investigation of 551 families containing many exceptionally long-living members.

The scientists determined the ages at which 462 women had their last child, and how long those women lived to be.

The findings, published in Menopause: The Journal of the North American Menopause Society, indicate that women may be the driving force behind the evolution of genetic variants that slow ageing.

'If a woman has those variants, she is able to reproduce and bear children for a longer period of time, increasing her chances of passing down those genes to the next generation,' said Prof Perls.

'This possibility may be a clue as to why 85 per cent of women live to 100 or more years while only 15 per cent of men do.'