Grand Theft Auto V's Lacey Jonas is a blond celebrity who lives at the Chateau Marment on West Hollywood and is trying to escape the paparazzi.

ADVERTISEMENT

Lindsay Lohan , 28, has filed a lawsuit against the game makers, Take-Two Interactive Software and Rockstar Games, claiming that they used her likeness to create Jonas.

"The portraits of the Plaintiff [Lohan] incorporated her image, likeness, clothing, outfits, Plaintiff's clothing line products, ensemble in the form of hats, hair style, sunglasses, jean shorts worn by the Plaintiff that were for sale to the public at least two years," the suit reads.

Lohan's suit adds that Take-Two, like other companies, should pay her for use of her image.

TMZ reported last December that Lohan's lawyers were planning a suit, noting that the 28-year-old actress wanted "serious money" as compensation.

In Jonas' portion of the GTA 5 story, players help her escape from the paparazzi.

"I'm really famous. I didn't do anything!" she says at one point.