BILOXI, MS - APRIL 16: A woman sunbathes on an uncrowded beach April 16, 2011 in Biloxi, Mississippi. BP says it has made tourism payments of $18 million to Mississippi in an attempt to help draw tourists back to its beaches following the oil spill. April 20th marks the one-year anniversary of the spill, the worst environmental disaster in U.S. history. Endangered Sea Turtles and dolphins are still dying in high numbers in Mississippi, which continues to be impacted by tar balls and weathered oil. (Photo by Mario Tama/Getty Images) A woman sunbathes on a beach April 16, 2011 in Biloxi, Mississippi. (Photo by Mario Tama/Getty Images)

LANHAM, Md. (WNEW) — As rates of other cancers have declined, rates of the deadliest form of skin cancer, melanoma, have increased more than 200 percent from 1973 to 2011, according to a new “Call to Action” report from the Surgeon General’s Office.

The report highlights how preventable skin cancer is, and proposes prevention strategies such as providing more shaded areas at outdoor recreation spots, enforcing and expanding indoor tanning laws and teaching skin cancer prevention as part of the curriculum in public schools.

Assistant Secretary for Health Howard K. Koh, a skin oncologist, says almost all skin cancers are caused by unnecessary ultraviolet radiation exposure, usually from excessive time in the sun or from the use of indoor tanning devices.

Melanoma, which causes nearly 9,000 deaths each year, is also one of the most common types of cancer among U.S. teens and young adults, the report says.

While other populations also use indoor tanning devices, teens and young adult women are most likely to do so. Having a mother who tans is a strong predictor of adolescent girls’ decision to start tanning.

“Each day, thousands of teens are exposing themselves, unprotected, to harmful UV radiation from tanning beds, but only 10 states currently have laws in place to prevent this practice for youth younger than age 18 years,” acting Surgeon General Boris D. Lushniak writes.

“Together, we must communicate the risks in a clear and effective way to family, friends, and others to help them understand their role in preventing skin cancer.”

“Tanned skin is damaged skin, and we need to shatter the myth that tanned skin is a sign of health,” Lushniak says. “When people tan or get sunburned, they increase their risk of getting skin cancer later in life.”

To read the full Surgeon General’s report, click here.