Fred Meier

USA TODAY

As General Motors moves to overhaul its safety processes in the wake of its ignition switch recall, the company announced on Tuesday a global safety chief, a new executive whose job will be to "quickly identify and resolve product safety issues."

CEO Mary Barra has named veteran GM engineer Jeff Boyer the company's first vice president, global vehicle safety.

Barra, in a meeting with journalists on Tuesday, emphasized that the new job "is a permanent position" with access to the top in the GM organization and not a temporary job to handle the current recall.

In the same meeting, Mark Reuss, GM's global product chief, described Boyer as a "passionate safety zealot."

GM's appointment comes as the company has recalled 1.62 million vehicles worldwide for an ignition switch defect that company engineers knew about as early as 2001, but was not recalled for a fix until last month. GM knows of 12 deaths and 31 crashes linked to the defect.

As the company conducts its own investigation of the defect and recall process, both houses of Congress, the Justice Department and federal safety regulators have begun probes of the years-long lag before the cars were recalled for the defect that can shut off the engine and safety systems.

Boyer will be responsible for safety performance before and after vehicles are on sale —including recalls — and will have the ear of GM's top brass and board, the company says.

Boyer, at GM since 1974, will report to John Calabrese, VP of global vehicle engineering, and be part of the global product development team under product chief Mark Reuss. As part of the team, he will meet with Reuss weekly and with Barra monthly and also will keep the board of directors informed.

"Jeff's appointment provides direct and ongoing access to GM leadership and the board of directors on critical customer safety issues," said Barra in a statement. "This new role elevates and integrates our safety process under a single leader, so we can set a new standard for customer safety with more rigorous accountability. If there are any obstacles in his way, Jeff has the authority to clear them. If he needs any additional resources, he will get them."

Jack Nerad, executive market analyst at Kelley Blue Book, sees the new position as a tangible sign that GM is serious about change. "The appointment ... is another indication of how seriously General Motors and CEO Mary Barra are in dealing with the safety recalls that have recently engulfed the company. There is no doubt that GM wants to send a strong message that this is not business as usual. Instead, it signals the company is changing its ways in identifying and dealing with potential safety issues."

Like Barra, Boyer started at GM as a co-op student and got an engineering degree from General Motors Institute (now Kettering University). He has an MBA from Michigan State and most recently was head of engineering operations and systems development. Until 2011, he was the executive responsible for performance and certification of GM vehicle safety and crashworthiness.

"Nothing is more important than the safety of our customers in the vehicles they drive," said Boyer in a statement. "Today's GM is committed to this."