Top Healthcare stocks:

JNJ: +0.56%

PFE: flat

ABT: flat

MRK: flat

AMGN: +2.27%

Healthcare shares were fairly flat in pre-market trade on Monday.

In healthcare stocks news, Johnson & Johnson ( JNJ ) was up about 1% after it said it accepted the binding offer from The Carlyle Group to acquire its Ortho-Clinical Diagnostics business for about $4 billion, subject to customary adjustments. The transaction is expected to close toward the middle of the year, upon satisfaction of customary closing conditions.

And, Prana Biotechnology ( PRAN ) shares were down 67% at $3.27 in pre-market trade Monday after the development stage company focusing on brain and eye disease reported that results of the 12-month Phase II Imaging trial in Alzheimer's Disease did not meet its primary endpoint.

PRAN trades in a 52-week range of $2.12 to $13.29. Shares touched a low of $2.35 earlier Monday morning.

Finally, Novartis ( NVS ) shares were up 3.6% in pre-market trade Monday after the healthcare company wrapped up its late-stage clinical trial of a chronic heart failure drug early.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.