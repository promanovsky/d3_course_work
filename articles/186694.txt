Google and Intel are releasing a new lineup of Chromebook PCs based on Intel's brand-new low-power chip, executives from the two companies announced in a press conference in San Francisco on Tuesday.

New devices based on the new chip, called Bay Trail, are coming from Acer, Asus, Lenovo and Toshiba. The companies say that these PCs will have up to 11 hours of battery life.

Advertisement

There's also what Intel calls "a new class" of Chromebooks coming from Acer and Dell. These are supposed to be more powerful, and able to handle power-hungry applications like Google Hangouts with multiple parties. They cost $349.Meanwhile, Microsoft is trying to steal Google's thunder. It is offering a one-day sale on an Asus Windows 8 PC today, for $199.

Developing: The press conference is going on now. We're sure these executives will offer more details and we'll update this story as we get those details.