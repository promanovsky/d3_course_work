Lawyers for Apple and Samsung exchanged barbs as a major new patent trial opened Tuesday, debating the role of a company not even part of the case -- Google.

Apple's legal team vowed to prove that Samsung flagrantly copied iPhone features and should pay more than $2 billion in damages, as the two smartphone giants squared off anew in a California courtroom.

Apple attorney Harold McElhinny opened his presentation with a video showing legendary Apple co-founder Steve Jobs introducing the first iPhone in 2007.

By putting computing power in smartphones powered by fun software and easy-to-use touch-screens, Apple transformed the market, sending Samsung onto its heels, according to McElhinny.

The attorney told jurors in his opening statement that they would see internal Samsung documents and messages showing that the company felt it was suffering "a crisis of design" with the difference between its devices and the smartphone "a difference between Heaven and Earth."

Apple said evidence will show that the South Korean electronics giant sold more than 37 million infringing smartphones and tablets in the United States.

California-based Apple would have demanded royalties of about $40 per device to license the patented technology to Samsung, according to McElhinny.

The overall amount being sought by Apple in damages from Samsung will top $2 billion, the lawyer explained.

"This case is not about Google," McElhinny told jurors.

"It is Samsung, not Google, that chose to put these features into its phones."

But Samsung's lawyer told the jurors in the San Jose, California court that the case was indeed about Google, and Apple's struggle against the maker of the Android operating system which is now winning in the global marketplace.

'Attack on Android'

"It's an attack on Android, that is what this case is," attorney John Quinn said.

"Apple is trying to limit consumer choice and gain an unfair advantage over Google's Android."

Quinn contended that four of the five patents at issue in the trial are not used in Apple mobile devices, but because of features built into Android software by Google engineers litigation was pursued.

He promised jurors that Google engineers would be called to testify to how they independently designed Android software and did not copy Apple.

Samsung is the world's leading maker of smartphones and tablets built using Google's free Android mobile operating system.

Android smartphones dominate the global market, particularly in devices offered for lower prices than iPhones.

"Apple is an amazingly innovative company, but in some respects, Google's Android has passed them," Quinn said.

"Apple is trying to gain from you in this courtroom what it has lost in the marketplace."

In August 2012, a separate jury in the same court decided that Samsung should pay Apple $1.049 billion in damages for illegally copying iPhone and iPad features, in one of the biggest patent cases in decades.

The damage award was later trimmed to $929 million and is being appealed.

If this new trial goes in Apple's favor, it could result in an even bigger award since it involves better-selling Samsung devices, such as the Galaxy S3 smartphone.

Quinn attacked expert witnesses Apple planned to call to back its case in the sky-high damages claim. The attorney contended the lawsuit extended from a war that Jobs declared on Google because of Android in 2010.

"A holy war on Android, that was Apple's strategy," Quinn said. "This lawsuit is part of a strategy to catch up with Google."

Dark side

Apple lawyers accused Samsung of going far beyond competitive intelligence to the "dark side" of intentional copying.

Jurors will also consider Samsung's claims that Apple infringed on patents related to transmitting digital video and storing digital images.

McElhinny called Apple vice president of worldwide marketing Philip Schiller as the first witness in the case.

Under questioning, Schiller described how Apple took a big risk betting on the iPhone, which was created during a top-secret project over the course of three years.

The launch of the iPhone in 2007 was an "historic" moment for Apple, with customer surveys showing that ease of use was a major appeal for customers, according to Schiller.

He said he reacted with "shock" when Samsung introduced its Android-powered Galaxy smartphone because it appeared to him that Samsung was copying the iPhone.

Copyright: Thomson Reuters 2014