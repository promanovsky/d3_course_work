Tesla and Panasonic to kickstart Gigafactory plans

Every journey begins with a single step. For Elon Musk and his 200 Gigafactory dream, that single step is this agreement with Japanese electronics company Panasonic. The two will be working together to build and manage Tesla‘s first gigafactory, a large-scale manufacturing plant that will produce batteries for its EV cars.

For Tesla, building its own battery factory isn’t simply a practicality. It’s a necessity. The number of electric vehicles that it can put out at a cheaper price than its current models largely depends on how much it can save on batteries. To do that it needs these battery-making gigafactories. Plural. The first step, however, is to actually build the first one. And for that, it has enlisted the industry expertise and equipment of known battery maker Panasonic.

Exact details of the factory haven’t been disclosed, but the division of labor has already been set. Tesla will be in charge of providing the land, the buildings,and managing the factory complex. Panasonic, on the other hand, will take care of the equipment, the machinery, and the actual manufacturing process itself. Tesla will also take charge of assembling the battery modules and packs. There will also be some suppliers onboard to provide precursor materials, but they will mostly be staying on Tesla’s half of the complex.

Tesla envisions that this Gigafactory, and more like it, will help pull the price tag of the next Tesla EV, the Model III (or Model 3), considerably from the Tesla S’ exorbitant $70,000 price. The plant is expected to reach full production capacity by 2020, producing 35 GWh of battery cells and 50 GWh of battery packs per year by that time. All that’s left now is settling on the actual location of this sprawling industrial context, with names such as New Mexico, Texas, and Nevada thrown into jar.

SOURCE: Tesla