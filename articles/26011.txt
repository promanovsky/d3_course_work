SAN FRANCISCO — I received an ominous warning before strapping on Project Morpheus, Sony’s new virtual-reality headset for the PlayStation 4 home console.

“If you think you might get motion sickness using Project Morpheus, let us know now,” a Sony rep said.

I do get motion sick pretty easily, but I didn’t want to tell that attendant that. Before I knew it, I was donning Morpheus, which Sony announced yesterday at the 2014 Game Developer Conference.

From the start, it’s clear that Morpheus is a work-in-progress. Compared to its nearest competitor — the Oculus Rift — it looks rougher and feels less comfortable. The screen is also blurrier, making it akin to gazing out through a dirty window.

But then the shark arrived, and any complaints I had about the screen quality vanished. Standing in a steel cage, I watched apprehensively as the massive great white flitted in and out of the shadows before charging the cage, giving me a close-up view of teeth that looked like steak knives. Looking into that maw, I totally forgot that I was wearing an uncomfortable headset and that the cage wasn’t really shaking all around me. In that moment, I finally “got” VR in a way that has eluded me until now.

While I didn’t truly feel what I would call “fear” — I was very much aware that I was in a simulation — I felt a level of immersion that I’ve never experienced in a game before. Until now, the distance between myself and the screen has always created a natural sense of removal from the action. With Morpheus, that sense of removal is mostly gone. I knew it wasn’t real, but the lizard portion of my brain was still screaming, “That’s a shark!” It was definitely a unique experience.

I’ll still admit to being a VR skeptic even after playing with Morpheus and the Rift. Despite the novelty of coming face-to-face with a Great White and the immersion inherent to the experience , I have a hard time imagining anyone outside of hardcore gamers donning one of those unwieldy headsets on a regular basis.

Yes, I’ll admit to being a VR skeptic even after playing with Morpheus and the Rift. Even now, I have a hard time imagining anyone outside of hardcore gamers donning one of those unwieldy headsets on a regular basis.

Nevertheless, I think Sony’s VR headset has a few crucial things going for it, a big one being that the DualShock 4 and PlayStation Move controllers for the PS4 are both really well suited for VR. Once the helmet was on my head, both of them felt like a natural extension of my arm, which was particularly surprising for the DualShock 4. I was kind of expecting a controller designed for two hands to feel kind of awkward being held out in one, but that was not the case.

The other thing that Morpheus has going for it is the simple fact that it’s on a console. With the right price and the right software, it’s easy to imagine it catching on as a PlayStation 4 peripheral. It feels more substantial than either 3D television or the Move, Sony’s original foray into motion control. This is a thing that game developers can really embrace as a new medium, provided of course that they are willing to take a few risks and allocate their resources accordingly.

I ended up seeing two demos at the Morpheus booth. The second one was a relatively simple demonstration called “The Castle” in which I put on a pair of disembodied gloves and swung a sword at a dummy. I’ll admit that it was pretty cool to point and aim a crossbow in the way that I had once imagined myself doing with the Wii, Nintendo’s first motion-controlled console, but the shark is definitely what stuck with me after the demo.

Unfortunately, it was also the first moment that I felt that familiar twinge of nausea and thought, “It doesn’t matter how immersive VR turns out to be. If I end up getting sick any time I use it, it really might not be for me.” To be fair, I think some of my motion sickness is down to Morpheus’ slightly slower head-tracking.

It’s the early days yet for Morpheus — Sony hasn’t even finalized its specs — and I expect the company will sand down a lot of the rough edges in the future. At least I hope so. If people like me end up upchucking every time we don that headset for longer than 15 minutes, Sony will have a problem on its hands.

Otherwise, Sony is off to a strong start with virtual reality. Even at this early stage, Morpheus easily bests what the original Rift VR headset had to offer and comes close to matching the updated Oculus development kit as well.

If Sony’s shark is a sign of things to come, I expect big things in Morpheus’s future.