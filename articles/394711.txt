A Chinese regulator has said it was conducting an anti-monopoly investigation into Microsoft because the firm has not fully disclosed information about its Windows and Microsoft Office software.

China's State Administration for Industry & Commerce said it was investigating senior Microsoft staff, and has made copies of the firm's financial statements and contracts.

Microsoft is one of the biggest US companies to fall under the eye of Chinese regulators as they ramp up their oversight in an apparent attempt to protect local companies and customers.

The SAIC said it had obtained documents, e-mails and other data from Microsoft, adding that it could not complete the investigation as the company had said some of its key personnel were not in China.

Microsoft has been suspected of violating China's anti-monopoly law since June last year in relation to problems with compatibility, bundling and document authentication, the statement said.

The announcement from SAIC, one of three anti-trust regulators in China, came a day after officials from the agency raided Microsoft offices in Beijing, Shanghai, Guangzhou and Chengdu.

Microsoft said yesterday it had been visited by officials and the company was "happy to answer the government's questions", a statement it repeated after the announcement from the regulator on Tuesday.

The SAIC statement said the raids were prompted by reports from other companies, without naming the companies.

Microsoft has already engaged a Chinese law firm to help with the anti-monopoly case, said the law firm, declining to be identified or to comment any further.

But mystery surrounds the probe, with industry experts and lawyers questioning what, if any, violations Microsoft can have made in China, where the size of its business is negligible.

Microsoft does have a wide range of operations in China, including research and development and teams for products such as its Windows operating system, Microsoft Office, servers, entertainment and hardware.

But its revenues for China, which Microsoft does not breakout in its earnings statements, are very low.

The disappointing sales in a market of 1.4 billion people, where piracy is rampant, have led to strained relations with Microsoft's headquarters in Redmond, Washington, said a former employee.

Former CEO Steve Ballmer reportedly told employees in 2011 that, because of piracy, Microsoft earned less revenue in China than in the Netherlands, even though computer sales matched those of the United States.

The US Chamber of Commerce earlier this year urged Washington to get tough with Beijing on its increasing use of its six-year-old anti-competition rules, noting that "concerns among US companies are intensifying".

US-China business relations have also been severely strained recently by wrangles over data privacy.

State media have called for "severe punishment" against tech firms for helping the US government to steal secrets and monitor China, in the wake of revelations by former US National Security Agency contractor Edward Snowden.

Tensions increased in May when the US Justice Department charged five members of the Chinese military with hacking the systems of US companies to steal trade secrets.

The latest move by China's authorities caps a rocky period for Microsoft in the country. Earlier this month, activists said Microsoft's OneDrive cloud storage service was being disrupted in China.

In May, central government offices were banned from installing Windows 8, Microsoft's latest operating system, on new computers. This ban appears not to have been lifted, as multiple procurement notices since then have not allowed Windows8.

Nevertheless, the company has pushed forward with plans to release its Xbox One gaming console in China in September, forming distribution ties with wireless carrier China Telecom Corp and e-commerce company JD.com.