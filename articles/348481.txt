Drilling for oil in the Bakken shale formation outside Watford City, N.D.

Crude oil on both sides of the Atlantic dropped on Thursday as supply fears began to ease after Libya declared an end to an oil crisis that has slashed exports from the OPEC member.



Libya's government said it had reached a deal with a rebel leader controlling oil ports involving the handover of the last two terminals, potentially making an extra 500,000 barrels per day (bpd) of crude available for export.

Brent fell to a three-week low, dropping 50 cents to under $111 a barrel. U.S. crude declined 50 cents to under $104, also sliding to a three-week trough.

The crisis in Iraq is still providing a floor for prices, however, with industry officials and analysts saying the world's spare production capacity would struggle to cover for another big oil outage.

Iraqi Prime Minister Nuri al-Maliki said he hoped parliament could form a new government in its next session after the first collapsed in discord. Baghdad can ill afford a long delay as large swathes of the north and west fall under the control of an al Qaeda splinter group.

The fighting has had little impact on Iraqi exports to date. Production fell by about 170,000 bpd in June, according to a Reuters survey, with southern exports affected by technical issues.