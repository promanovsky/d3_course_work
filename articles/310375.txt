Consumers are more confident than they've been in over six years, according to The Conference Board's June Consumer Confidence Index released today.

After May's index clocked in at a solid revised 82.2, the index managed to add on an additional three points to reach 85.2 for June. Analysts had expected more confident consumers, but their 83.7 estimate proved overly conservative. With four straight months of 80.0+ reports, June's index marks the strongest reading since January 2008. The index uses 1985 as its 100-point benchmark.

"June's increase was driven primarily by improving current conditions, particularly consumers' assessment of business conditions," said Conference Board Director of Economic Indicators Lynn Franco in a statement today. "Expectations regarding the short-term outlook for the economy and jobs were moderately more favorable, while income expectations were a bit mixed. Still, the momentum going forward remains quite positive."

The index is comprised of responses from a random sample of consumers. In this latest report, more respondents consider business conditions to be "good" (23%) than bad (22.8%). And while 31.8% of those surveyed still believe jobs are hard to get, compared to just 14.7% who consider jobs to be plentiful, those percentages are down 0.4 points and up 0.5 points, respectively.

Looking ahead, 18.8% of those surveyed expect business conditions to improve over the next six months, compared to 11.4% anticipating tougher times.