Researchers have found evidence that children who consistently received less than the recommended hours of sleep during infancy and early childhood had increases in both obesity and in adiposity or overall body fat at age 7.

Lead author Elsie Taveras, MD, MPH, chief of General Pediatrics at MGHfC, said their study found convincing evidence that getting less than recommended amounts of sleep across early childhood is an independent and strong risk factor for obesity and adiposity.

She said contrary to some published studies, we did not find a particular 'critical period' for the influence of sleep duration on weight gain. Instead, insufficient sleep at any time in early childhood had adverse effects.

The study analyzed data from Project Viva, a long-term investigation of the impacts of several factors during pregnancy and after birth. Information used in this study was gathered from mothers at in-person interviews when their children were around 6 months, 3 years and 7 years old, and from questionnaires completed when the children were ages 1, 2, 4, 5 and 6.

Among other questions, the mothers were asked how much time their children slept, both at night and during daytime naps, during an average day. Measurements taken at the seven-year visit included not only height and weight but also total body fat, abdominal fat, lean body mass, and waist and hip circumferences - measurements that may more accurately reflect cardio-metabolic risks than BMI alone. Curtailed sleep was defined as less than 12 hours per day from ages 6 months to 2 years, less than 10 hours per day for ages 3 and 4, and less than 9 hours per day from age 5 to 7. Based on the mothers' reports at each age, individual children were assigned a sleep score covering the entire study period - from 0, which represented the highest level of sleep curtailment, to 13, indicating no reports of insufficient sleep.

Overall, children with the lowest sleep scores had the highest levels of all body measurements reflecting obesity and adiposity, including abdominal fat which is considered to be particularly hazardous.

The study has been published in the journal Pediatrics.