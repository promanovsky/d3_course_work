Music fans are headbanging as the Brazilian metal band Soulfly performs on stage during the Greenfield Open Air festival in Interlaken, Switzerland, on Saturday, June 13, 2009. (AP Photo/ Keystone, Peter Klaunzer)

Bang your heads, metal fans -- just be aware that there's a very small risk that it could lead to one of the most hardcore rock injuries of all time: bleeding in the brain.

In a case study published in The Lancet, a 50-year-old German man reported a headache that wouldn't go away for two weeks. A scan revealed a chronic subdural hematoma, aka bleeding in the brain.

The cause? The man had been banging his head at a Motörhead concert.

Doctors had to drill a hole into the man's skull, remove a clot and then drain the brain for six days. He recovered completely, and now almost certainly has the ultimate bragging rights at any metalhead gathering.

Despite the incident, experts say headbangers shouldn't worry too much about brain bleeds.

"There are probably other higher risk events going on at rock concerts than headbanging," Dr. Colin Shieff, a neurosurgeon and trustee of the British brain injury advocacy group Headway told the Associated Press. "Most people who go to music festivals and jump up and down while shaking their heads don't end up in the hands of a neurosurgeon."

The study's authors agree.

"We are not smartasses who advise against headbanging," lead author Dr. Ariyan Pirayesh Islamian of the Hannover Medical School in Germany told the CBC.

"Our purpose was not only to entertain the readership with a quite comical case report on complications of headbanging that confirms the reputation of Motörhead as undoubtedly one of the hardest rock 'n' roll bands on Earth, but to sensitize the medical world for a certain subgroup of fans that may be endangered when indulging themselves in excessive headbanging," he said.

Yes, that's right. Science officially confirms that Motörhead is "one of the hardest rock 'n' roll bands on Earth."

Not bad when you consider that Motörhead singer-bassist Lemmy Kilmister is 68 years old and rocking with a defibrillator.