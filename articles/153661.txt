Larger iPhone 6 could cannibalize iPad mini

When the first real glimpses of the iPad mini came about, a wave of analyst suggestions made clear the possibility that a new iPad could eat into sales of an old one. More importantly, an iPad with a smaller display and a smaller price had real potential for eating into sales of the larger, more expensive model. As it turned out, there was a possibility that the iPad mini would cannibalize AND drive iPad sales as it hit the market.

Tim Cook went on to speak on the matter in February of 2013, suggesting that “if we don’t cannibalize, someone else will.” He also made clear that Apple intended on cutting in to the Windows PC market more than they did the Mac or iPad market.

The same may very well be true of a larger iPhone. The iPhone 6 is tipped to be coming in a couple of sizes later this year. While they may have different names, this next generation of iPhone devices is said to bring on the first Apple smartphone with a display larger than 4-inches.

Piper Jaffray analyst suggested this week that a larger iPhone 6 would potentially lead to users purchasing the device and an iPad Air rather than sticking with an older iPhone and purchasing an iPad mini. This is, of course, assuming they want an iPad at all. It also assumes that users are interested in a larger display in the first place.

VIA: CNET