The European Commission has accepted commitments by Samsung to allow competitors access to key basic patents, but rapped Google-owned Motorola for pursuing a case against Apple.

Samsung's commitment not to seek injunctions in Europe against companies using its Standard Essential Patents for smartphones and tablet computers will be made legally binding, the Commission said.

The Commission has been investigating Samsung for abusing its dominant market position in certain technologies after it took out injunctions against fierce rival Apple over use of its SEPs.

If there are any future disputes about the use of such patents, they will be settled under the industry standard of Fair, Reasonable and non-Discriminatory (FRAND) practice.

The Commission said that as a result, any company signing a licence accord with Samsung will now be protected against SEPs injunctions.

Protecting intellectual property is essential, EU Competition Commissioner Joaquin Almunia said, but it should not be "misused to the detriment of healthy competition and, ultimately, of consumers."

Samsung's commitment on SEPs "provides clarity to the industry on an appropriate framework to settle disputes over 'FRAND' terms in line with EU antitrust rules," Almunia said in a statement.

"I would also encourage other industry players to consider establishing similar dispute resolution mechanisms," he added.

In a related case, the Commission said it had found Motorola Mobility at fault for abusing its dominant market position by pursuing a SEP injunction against Apple in a German court.

Motorola Mobility will now have "to eliminate the negative effects resulting" from the case, it said, without, however, imposing a fine.

Patent holders should get a fair return but companies seeking to use them should also be able to do so on FRAND terms, Mr Almunia said.