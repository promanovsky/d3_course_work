Shutterstock photo

Investing.com -

Investing.com - The dollar slipped lower against the yen in thin trade on Monday after the latest U.S. nonfarm payrolls report fell slightly short of expectations, but losses were held in check ahead of the Bank of Japan's upcoming monetary policy decision.

USD/JPY touched session lows of 103.00, the weakest since March 31 and was last down 0.17% to 103.08.

The pair was likely to find support at 102.78, the low of March 31 and resistance at 103.50.

The dollar remained on the bank foot after data on Friday showed that the U.S. economy added 192,000 jobs in March, below expectations for jobs growth of 200,000. The U.S. unemployment rate remained unchanged at 6.7%, compared to expectations for a downtick to 6.6%.

The data disappointed some market expectations for a more robust reading but indicated that the Federal Reserve is likely to stick to the current pace of reductions to its asset purchase program.

Investors remained cautious ahead of Tuesday's conclusion to the BoJ's policy meeting, after Japan's sales tax increase came into effect earlier this month. The increase is expected to present a challenge to the central bank's attempts to bolster economic growth and stave off deflation.

Trade volumes remained thin, with markets in China closed for a holiday.

The euro was also slightly lower against the yen, with EUR/JPY slipping 0.12% to 141.35.

The shared currency remained under pressure after the European Central Bank indicated last week that unconventional monetary policy instruments may be necessary to avert the risk of ongoing low inflation in the euro zone.

Elsewhere, the euro was steady against the dollar, with EUR/USD inching up 0.02% to 1.3705, not far from the five-week trough of 1.3671 reached on Friday.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.