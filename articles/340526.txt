From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Mike Gupta is no longer Twitter’s chief financial officer.

Twitter announced in a public filing today that former Goldman Sachs executive and NFL CFO Anthony Noto will take over Gupta’s responsibilities. While at Goldman Sachs, Noto played a key role in helping Twitter through its initial public offering.

I could not be more excited about joining the @Twitter team & helping them reach every person in the world. #indispensable — Anthony Noto (@anthonynoto) July 1, 2014

Gupta will remain at Twitter, Re/Code reports, serving as the company’s SVP of strategic investments. The change was reportedly prompted by internal dissatisfaction with Gupta’s progress in monetizing the social network. In addition, Gupta’s move to unload more than $1 million in stock was reportedly not taken well within the company.

Investors appear happy with the change. Twitter is up 4 percent today following the announcement.