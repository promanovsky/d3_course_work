Denzel Washington was seen as a can’t-miss coming into Tuesday’s Tony Awards nominations. After all, the veteran star had won a Tony for August Wilson’s “Fences” in 2010 and was garnering strong reviews for his turn as Walter Lee Younger in the revival of Lorraine Hansberry’s “A Raisin in the Sun.”

Yet when Tony nominations for leading actor in a play were announced, Washington’s name was nowhere to be found. True, he was competing in a crowded category. But nominators clearly liked the show, giving it three other acting nominations (to the three principal actresses, including eleventh-hour replacement LaTanya Richardson Jackson).

Washington was one of several snubs and surprises that emerged from the nominations announcement for the Tony Awards, which will be handed out June 8 at Radio City Music Hall. Here are five more.

PHOTOS: Tony Awards 2014 - Top nominees

Advertisement

Radcliffe ripped. Daniel Radcliffe has been overlooked so many times by this point it would almost be a bigger surprise if he was nominated. He struck out with “Equus” in 2009 and missed the cut two years later for “How to Succeed in Business Without Really Trying” — a role that before nominations had many thinking he could win the thing. He wasn’t considered a front-runner for his role as orphan Billy Claven in Martin McDonagh’s revival of “The Cripple of Inishmaan,” and the field was competitive. Still, Radcliffe would be forgiven for thinking Hollywood is where he might want to spend the next year or two.

Meanwhile, the man who wrote the play is trying to right a snub of his own. After striking out on the big night with four previous nominations for best play, McDonagh will try to win one this time, in the category of best revival of a play.

Waiting for Tony. Patrick Stewart and Ian McKellen were titans of the theater. Their “Waiting for Godot,” part of a double bill, was seen as a solid production, and the two were tipped as strong contenders. In fact, it seemed likely they’d be facing off against each other. Instead neither will be taking the winners’ podium at Radio City come June, as less-anticipated contenders Chris O’Dowd from “Of Mice and Men” and Samuel Barnett in “Twelfth Night” took spots that many thought could have gone to Stewart or McKellen. “Twelfth Night” was clearly a favorite of nominators -- it also landed three of the five spots in featured actor in a play.

Don’t tell Mama. When “Cabaret” last opened on Broadway, in 1998, it was a Tonys force, landing 10 nominations and four wins. A nearly identical production, also directed by Rob Marshall and Sam Mendes, found its way back to Broadway this year. But the good times weren’t swinging at the Kit Kat Club. Critics raised their eyebrows at Michelle Williams’ turn as Sally Bowles and the show in general. On Tuesday, the Tonys did too. Nominators shrunk the revival of a musical category to just three shows (an option given the paucity of eligible productions) and left “Cabaret” out. The show landed just two nominations, for Danny Burstein and Linda Emond in featured-acting categories.

Advertisement

“Bullets” slain? Before it opened, Woody Allen’s adaptation of his own musical-comedy film “Bullets Over Broadway” was seen as the show to beat. Tuesday morning? Not so much. The show received six nominations but none of them major — though it did land Allen a best book nod. The show may be in trouble at the box office after its snub in the best musical category, an award that can redeem a middling performer. One of the few bright spots is Nick Cordero. The relative unknown, playing the surprisingly lyrical gangster that Chazz Palminteri incarnated in the 1994 film, has been earning strong reviews and picked up a featured actor in a musical nomination.

Unrealistic. Will Eno has been a favorite of drama lovers for a while, with Off-Broadway shows like “Thom Pain (based on nothing)” and “Middletown” earning awards and acclaim. He made his debut this season with “The Realistic Joneses,” a starry affair that saw him tackling American family and suburbia in sketch-like form. Was the play too edgy for voters? Tipped as a favorite to land a spot in best play, the show was shut out of all noms, with the play slot instead going to John Patrick Shanley’s darker horse “Outside Mullingar.”

ALSO:

Actors’ Gang takes Shakespeare to China

Advertisement

Tony Awards 2014: The complete list of nominees

Review: Bandaging life’s battle scars in ‘Water by the Spoonful’