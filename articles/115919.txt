Jenny McCarthy insisted this week that she isn't anti-vaccine in an op-ed article she wrote for the Chicago Sun-Times, after years of publicly speaking out against vaccinations.

The actress and television presenter penned the April 12 piece to clear up being "wrongly branded as 'anti-vaccine'" for years.

"This is not a change in my stance, nor is it a new position that I have recently adopted," 41-year-old McCarthy wrote in her piece on Saturday.

McCarthy has been active in her involvement with an anti-vaccine movement she first launched in 2007. In the past, she's maintained that her 11-year-old son Evan's autism was caused by a measles, mumps and rubella vaccine he was given as a child.

"Blatantly inaccurate blog posts about my position have been accepted as truth by the public at large, as well as media outlets (legitimate and otherwise) who have taken those false stories and repeatedly turned them into headlines," she wrote.

McCarthy said she aimed to create a safe space for parents to discuss different ways to keep their children safe and healthy. In the article, the mother of one wrote a lack of "critical thinking" presented one of the biggest probems for understanding the anti-vaccine movement.

"For my child, I asked for a schedule that would allow one shot per visit instead of the multiple shots they were and still are giving infants," she wrote. "I believe in the importance of a vaccine program and I believe parents have the right to choose one poke per visit. I've never told anyone to not vaccinate."

TIME science writer Jeffrey Kluger wrote a response to McCarthy's op-ed in which he pointed out that the actress omitted a portion of a quote she'd given him in a 2009 interview.

"'People have the misconception that we want to eliminate vaccines,' I told TIME magazine science editor Jeffrey Kluger in 2009," McCarthy wrote this past weekend. "'Please understand that we are not an anti-vaccine group. We are demanding safe vaccines. We want to schedule and reduce the toxins.'" In the original quote, she adds: "If you ask a parent of an autistic child if they want the measles or the autism, we will stand in line for the f-----g measles."

"It's just too late to play cute with the things you've said," Kluger fired back in his piece this week. "You are either floridly, loudly, uninformedly anti-vaccine or you are the most grievously misunderstood celebrity of the modern era."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.