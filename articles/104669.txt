Al Ain: One Filipino paramedic died and five other medics from the Al Ain Rescue and Ambulance Section have been quarantined after being infected with the Middle East Respiratory Syndrome Coronavirus (Mers-CoV), said official from the UAE Ministry of Interior.

In a statement on Friday, the Ministry of Interior said the six cases “were diagnosed during the periodic medical examinations”.

The ministry said it “has taken all necessary preventive health measures by placing the patients under quarantine. The ministry has also been in touch with community members, who have been transferred lately to hospitals to check on their health condition as a precaution.”

The Mers death and quarantine in the UAE comes three days after the emergency unit at King Fahad Hospital in the Saudi Red Sea city of Jeddah was closed as a precautionary measure amid reports that several people fell ill with Mers coronavirus.

Earlier this week, Saudi health authorities had reported the deaths of another two men and four new cases of Mers, bringing the death toll from the respiratory disease in the country to 66.

At least 11 medical staff have been reported to have contracted the virus, a spokesperson at the King Fahad hospital said.



