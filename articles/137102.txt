Food giant General Mills (GIS) instituted a program that restricts legal action some of its most loyal customers can take should they have a problem with the company's products. Consumers who accept any of a list of online benefits agree to binding arbitration and cannot join class action suits in disputes, including those over products purchased at a store.

General Mills owns such brands as Pillsbury, Betty Crocker, Nature Valley, Yoplait, Wheaties, and Cheerios. The list of online benefits includes entering contests, joining the company's online communities, and receiving electronic newsletters. An update to its legal terms, prominently placed at the top of the website, directs consumers to the information.

People who participate in such so-called loyalty programs are generally considered among a company's best customers. The actions do not affect the many consumers who casually buy General Mills products.

A New York Times story proclaimed that liking a brand could void a right to sue completely. A statement that General Mills sent CBS MoneyWatch called the claim a "mischaracterization" and said that "[n]o one is precluded from suing us merely by purchasing our products at the store or liking one of our brand Facebook pages."

Omri Ben-Shahar, a professor of law at the University of Chicago and expert in consumer legal issues, called the story "grossly misleading" and said that the approach is widely practiced on websites. Furthermore, under U.S. law, there can be no passive restriction on rights when it comes to product defect and liability. Consumers will have to actively accept the agreement when trying to download a coupon or join an email list. However, few bother to read the terms when they click through online, so they may not realize the implications.

What is unusual is the extension to physical products purchased online or in stores. "If you're a customer of Facebook or Google, the only way you can do business with them is online," he said. "Wal-Mart.com has legal terms but they don't try to extend them to brick-and-mortar stores."

Ben-Shahar said that the "practical importance of this kind of agreement is negligible." The company's biggest concern would likely be to limit class action lawsuits, but given the nature of General Mills sales, lawyers could easily find other plaintiffs.

As the New York Times reported, General Mills paid $8.5 million in 2013 to settle lawsuits challenging health claims made on Yoplait Yoplus yogurt packaging. The change in legal terms this year came after a court refused to dismiss a lawsuit about claims that Nature Valley products are "natural" when the ingredients could be genetically modified or processed.

The restriction to using arbitration does not prevent a lawsuit. Instead, it forces the consumer to go through a process outside of the traditional court system.



