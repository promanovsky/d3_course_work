Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

North Korean officials have reacted rather badly to the news that spoof film The Interview features an assassination attempt on their Supreme Leader Kim Jong-un.

Starring Seth Rogan and James Franco, the storyline sees the pair recruited by the CIA to kill the dictator during a visit to the People's Republic.

It hasn't gone down very well, with a foreign ministry spokesperson describing the film as "blatant act of terrorism and war".

If there's one thing you can't accuse the North Korean leader of, it's having a sense of humour. The official also promised a "merciless retaliation" if the film isn't scrapped. Yikes.

Here are eight more people who didn't see the funny side...

The government of Kazakhstan

(Image: Moviestore Collection/REX (1554180a))

Sacha Baron Cohen's satirical film Borat went down well in Western box offices but less so in Kazakhstan, where its fictional lead character was said to be from. The country's Foreign Ministry threatened to sue the star, and also launched a multi-million dollar 'Heart of Eurasia' campaign to counter what they saw as the negative impact on the country's reputation. They did, however, later thank him for helping to boost tourism.

Samuel Preston aka Preston

(Image: REX)

The Ordinary Boys singer saw red when Never Mind The Buzzcocks host Simon Amstell poked fun at his wife, reality star Chantelle Houghton. After Amstell started reading extracts from her autobiography, Preston stormed off the show and out of the building.

Tim Burton

The film director took issue with comic book writer Kevin Smith when he joked that Planet of the Apes had been plagiarised from one of his stories. Burton scathingly retorted that he doesn’t read comic books, especially ones by Kevin Smith. Undeterred, The Jay and Silent Bob creator responded that his admission “explains f ** Batman". Touché.

Donald Trump

(Image: Getty)

The American billionaire was one of the first in line to demand that President Barack Obama reveal his birth certificate in the midst of a conspiracy that he was born outside the USA. Obama duly did and confirmed that he was indeed born in Hawaii. But when Obama joked about this at a White House dinner, Trump sat stony-faced throughout, later replying “no, not really” when asked if he enjoyed the jokes.

Huey Morgan

Another Buzzcocks walk-out, only the Fun Lovin' Criminals singer was not content with simply storming out, he also smashed his mug on the way out. The normally chilled out radio presenter was wound up when hosts Rizzle Kicks persisted in bombarding him with his own hits during the 'Next Lines' round.

North Korea (...again)

(Image: M&M hair academy)

Another day, another sense of humour failure. This time it was a barber's shop in West London that got into trouble for putting up a poster showing a picture of Kim Jong-un with the caption “Bad Hair Day?” Staff claim they were visited by two angry embassy officials who demanded they take it down.

Charlie Sheen

(Image: Getty)

The actor hit the roof when an American TV producer, talking about his healthy lifestyle, joked that “if Charlie Sheen outlives me, I’m gonna be really p * .” Among the more publishable names he was called by Sheen in response were: “clown,” “stupid, stupid little man,” and “pussy punk”. Nicely done, Charlie.

Taylor Swift

(Image: Getty)

Comedy writer Tina Fey is well-known for her satire so when she joked about Taylor Swift's taste in men and her confessional songs it ought to have been clear she was being light-hearted. Unfortunately Tay Tay didn't take it that way, fuming with these sage words she'd once heard: “There's a special place in hell for women who don't help other women.” Ouch.