If the storm over the Sultan of Brunei’s ownership of the Beverly Hills Hotel continues, it could cost the historic L.A. landmark millions annually, hospitality industry analysts warned Monday.

The hotel opened in 1912, and for decades has been a hangout for Hollywood’s elite and home to the famed Polo Lounge. It became the focus of protests last week after Hassanal Bolkiah, who is also the prime minister of the Southeast Asian country, imposed a new Islamic Shariah criminal law that calls for punishing adultery, abortions and same-sex relationships with flogging and stoning.

Cancellations of events over the past few days will hurt, but if the protests continue or spread it could cut into the hotels’ occupancy, and that would run into millions of dollars, said Gary Vallen, who heads his own hospitality consultancy agency.

Also read: Beverly Hills Hotel Protest: Jay Leno Compares Sultan of Brunei to LA Clippers Owner Donald Sterling (Video)

“Anytime that you have controversy you have the potential to lose occupancy,” Vallen told TheWrap. “The longer it goes, the deeper the cut in revenue will be. If they don’t take care of this problem, groups will find they have a lot of other choices.”

If the public relations fallout were to cause occupancy to fall off by 10 percent, it could push the annual loss to the 208-room hotel more than $6 million, Vallen estimated; if that percentage goes up, the financial toll would as well. He based his hypothetical impact analysis on total occupancy at the 208-room hotel, with an average cost of $500 per night. Rooms typically account for around 60 percent of most high-end hotels’ revenues, with bars, spas and banquets providing the rest.

Also read: Grilled: Creative Coalition’s Bronk Defends Glaxo Funding

The problems doesn’t appear to be going away soon, as the Sultan has shown no signs of budging on the new laws, which he last week called a “great achievement.”

Hollywood is in the forefront of the campaign to put pressure on Bolkiah. Sharon Osbourne and Ellen DeGegeneres tweeted out the call to boycott early on. Jay Leno and actress Francis Fisher led a rally adjacent to the hotel’s 12-acre Sunset Boulevard site on Monday, hours after the Motion Picture & Television Fund said that it would not hold its annual Night Before the Oscar party at the hotel as it has for many years.

Richard Branson tweeted over the weekend that his Virgin Group would not be doing business with any firms associated with the Dorchester Collection, which was formed in 1996 to manage the hotel interests of the Sultan’s Brunei Investment Agency. The Feminist Majority Foundation moved its annual Global Women’s Rights Awards on Monday from the Beverly Hills Hotel to the nearby Hammer Museum.

Also read: How Idealist.org Wants to Crowdsource Activism

The fact that Hollywood types are leading the fight makes the hotel particularly vulnerable, and not just because the famous faces draw attention from the mainstream.

“The type of groups these hotel attract as their clientele. showbiz people, political organizations, the fashion industry, they all tend to have higher levels of sensitivity and solidarity,” Vallen said. “As more of them become aware, it could make the effects of the protests last a long time.”

And a drawn-out battle appears to be a real possibility.

“Even if the Sultan were to decide he wanted to sell tomorrow, it would still probably take a year to get a reasonable price,” said Kenneth Free, president of Straightline Advisors, which specializes in hotel real estate. “The situation is tricky because the hotel is a unique, historical asset to the community,” he added.

Also read: Hillary Clinton Memoir to Cover Arab Spring, Bin Laden Death

Christopher Cowdray, chief executive of the Sultan’s Dorchester Collection, said in a statement Monday that a boycott should not be directed at its hotels or its employees.

“The economic impact of this not only affects our loyal team members but extends to the local community, our valued partners and suppliers.

“Today’s global economy needs to be placed in a broader perspective. Most of us are not aware of the investors behind the brands that have become an integral part of our everyday life, from the gas we put in our cars, to the clothes we wear, to the way we use social media, and to the hotels we frequent. American companies across the board are funded by foreign investment,” he said.

Also read: Mark Cuban: YouTube Can Change the World, But It Can’t Make Money Streaming

The fact that the campaign could take a toll on the local economy and the hotel employees isn’t lost on the leadership at the Feminist Majority Foundation, which organized Monday’s protest.

“It’s a valid concern and something we’ve thought about,” said spokeswoman Stephanie Hallett. “We don’t want to hurt the workers, and we’re not actually urging a boycott, which could have legal ramifications,” she said.

“What we are doing is trying to make people to understand that patronizing these hotels encourages this sort of radical lawmaking, and we are urging them to act with their conscience.”