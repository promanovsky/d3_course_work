Shaken by the advances of newer, sportier rivals, the Toyota Camry is trying to shed its vanilla reputation.

The redesigned 2015 Camry, unveiled Wednesday at the New York International Auto Show, is longer and wider, with a large, aggressive grille and chiseled sides. Toyota says it changed every exterior piece but the roof.

The Camry has been the best-selling car in the U.S. for the last 12 years, supported by loyal buyers wedded to a dependable family car. But Toyota acknowledges that tastes have changed, and buyers of midsize cars want more style, comfort and performance to go with the reliability.

U.S. Toyota division chief Bill Fay said the company started redesigning the Camry almost immediately after a new version went on sale in 2011. Fay said the company knew it needed a more daring style after competitors like Hyundai and Ford offered newer, more striking designs.

"Everyone was raising the stakes a bit. We had to make sure we could keep this competitive," Fay said Wednesday at the show.

Inside the updated Camry, there are softer materials and a wireless charging system. The body is stiffer and the suspension and steering were retuned for more responsive driving. Even the carpet and side mirrors were redesigned to make the car quieter.

The new Camry, which goes on sale this fall, will help the Camry defend its turf, which has been increasingly challenged by rivals.

The Honda Accord, redesigned for the 2013 model year, narrowed Camry's full-year sales lead to 41,000 cars last year from 73,000 in 2012. The Nissan Altima and the Ford Fusion each had bigger percentage sales gains last year than the Camry.

What's more, the new Mazda6 breezed past the Camry's fuel economy numbers. And even luxury makers like Mercedes-Benz have introduced new cars that sell for under $30,000 — right in Camry buyers' price range.

It didn't help that Toyota's reputation was hurt by a series of recalls in 2010. The Camry has never regained the 15 percent share of the midsize car market it held before the recalls. It controlled 13 percent of that market in 2013, with total sales of 408,484, according to Ward's AutoInfoBank.

The midsize rivals are competing in a shrinking market. Young families and aging Baby Boomers are flocking to small SUVs like the Toyota RAV4 and Honda CR-V, which offer more space and competitive fuel economy. Midsize car sales have fallen 8 percent so far this year, while small SUVs are up 20 percent, according to Kelley Blue Book.

In that kind of market, no one can stand still. Hyundai, which brought the midsize segment out of the doldrums with the racy 2011 Sonata, which went on sale in 2009, introduced a new Sonata in New York Wednesday.

The 2015 Sonata has ditched the sharp creases on the sides that polarized buyers in favor of a taut, refined look to match the upscale Genesis sedan. The new Sonata, which goes on sale this summer, has many new features, including Apple's CarPlay system that lets drivers control their Apple devices through the car.

The more refined styling may disappoint some fans. Mike Cimino, a Hyundai dealer and vice president of Phil Long Dealerships in Colorado Springs, Colo., was hoping for a revolutionary change along the lines of the 2011 model.

"I'm wondering what they have that's going to take them out of the box again," he said.

But Dave Zuchowski, the CEO of Hyundai in the U.S., said Hyundai no longer needs to grab buyers' attention like it did five years ago.

"We don't have to stand on the table and shout," he said.

Cimino, who also sells Toyotas, said the Camry doesn't need to make revolutionary changes. Toyota buyers are extremely loyal, he said, and are satisfied with small upgrades in design and technology.

Aaron Bragman, the Detroit bureau chief for the car buying site Cars.com, says Toyota surprised him with the extent of the changes on the 2015 Camry.

"They had to step up their game," he said.

In addition to those loyal buyers, Toyota has another advantage: the weak yen. Adam Jonas, an auto analyst with Morgan Stanley, says the depreciation of the yen has translated into a $2,500 to $3,000 profit per vehicle for Japanese automakers.

That's helped Toyota maintain its lead, since it can make a profit even if it offers big discounts. The average Camry currently sells for $23,965, or around $900 less than the average midsize car, according to KBB. Only the Dodge Avenger sells for less.

Those profits can also be reinvested in better products, like the new Camry.

"They will do what they need to do to stay on top," said Stephanie Brinley, an analyst with IHS Automotive.