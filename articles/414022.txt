The ball boys involved in this year's U.S. Open will be testing Ralph Lauren's new smart T-shirts that will monitor their biometric data.

The black nylon shirts are the result of a venture between the clothing designer and OMsignal, and feature sensors and conductive thread coated in silver, according to Gizmodo. The goal for the shirts is to keep track of the ball boys' vital signs like heart rate, stress levels and breathing.

"Everyone is exploring wearable tech watches and headbands and looking at cool sneakers," explained David Lauren, director of marketing and corporate communications for Ralph Lauren and son of the designer. "We skipped to what we thought was new, which is apparel. We live in our clothes."

The U.S. Open begins today at Arthur Ashe Stadium, and will feature champions such as Roger Federer, Novak Djokovic, and Serena Williams, The Verge reported.

A "black box" attached to the shirt will collect the vital information from the ball boys, and a smartphone app can be used for monitoring the data.

The aim for the venture is to design shirts that find a balance between being fashionable and including technology that makes them highly functional, The New York Times reported.

"No one has produced a wearable that is 100 percent bang-on in terms of what mass consumers will want," said Rachel Arthur, global senior editor at WGSN.

"We all know this is the future, but in terms of beautiful design, it's not there yet. It has to be something we want to use."

The ball boys won't be the only ones wearing the new high-tech shirts at the two-week tournament, according to The Verge. Ralph Lauren said that the shirts will be worn not only by ball boys at select matches, but also by top-ranked collegiate tennis player Marcos Giron, who will be participating in his first Grand Slam.

The company is also looking to introduce a line of clothing in the upcoming months featuring the technology. Prices for the apparel have yet to be revealed.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.