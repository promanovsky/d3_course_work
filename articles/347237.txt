Melissa McCarthy's next movie "Tammy" hits theaters this week.

The movie stars the comedian as a down-on-her-luck, broke young woman who decides to go on a road trip with her grandma after getting fired from her fast-food job and catching her husband with another woman.

After box-office hits like "The Heat" and "Identity Thief," trailers for "Tammy" would make audiences believe McCarthy is aiming for another high comedy note at theaters; however, there's just one problem.

Critics say the film isn't actually funny.

Instead, "Tammy" is more of a drama about a broken woman disguised as a comedy in clever ads.

The film, currently sitting at 21% on Rotten Tomatoes, is expected to open to around $25 million this weekend, which would be less than the debut of McCarthy's breakout role in 2011's "Bridesmaids" ($26.2 million).

Film.com:

“'Tammy' is far more serious than the gangsta-rap dancin’, jet skiing ads let on ... If it weren’t for the twinkle in Sarandon’s eye or McCarthy’s uncanny ability to squeeze a laugh from a throwaway remark you’d likely drop dead of boredom right there in the theater."

The Hollywood Reporter:

"What’s been funny for her [McCarthy] before no longer is. Nor is it amusing when she explodes upon returning home to find her husband (Nat Faxon) having dinner with another woman (Toni Collette), nor when she erupts at her mother for not giving her a car and then drives off with her grandmother, Pearl (Susan Sarandon) ... the film progresses from merely unfunny to unconvincing to dull."

NJ.com:

"The real problem with the movie is simple — it's just not funny enough, and too worried about making McCarthy's usual hilarious supporting character into a woman sympathetic enough (that is, bland enough) to be the heroine."

Variety:

"Falcone’s attempts to spin this generally flat, formulaic comedy into an affecting character drama are frustrated by filmmaking choices that work against a sense of persuasive reality."

The Wrap:

"I certainly wanted to like Tammy, and 'Tammy,' since I've been a fan of McCarthy going all the way back to her work on 'Gilmore Girls,' and at Los Angeles’ Groundlings improv theater prior to her dynamic, Oscar-nominated turn in “Bridesmaids.” But she and her husband Falcone (who also directed) have created a character comedy that's missing both comedy and character."

Film School Rejects:

"Tammy is not a great comedy. It’s not even a good one really, and if I had to toss an adjective its way to describe the quality of comedy on display I’d go with 'okay.'"

The New York Times calls it McCarthy's "least funny comedy":



"The jokes about Tammy’s eating habits, her appetite for burgers, pies and doughnuts, aren’t especially funny and, after a while, register as both tedious and borderline desperate."

The Washington Post actually gave the film zero stars:

"Most of 'Tammy' consists of the two women getting entangled in some kind of un-funny, ill-advised encounter, which ends with a string of expletives and one or both of them knocking something over, whether it’s a suntan lotion display on a personal watercraft vendor’s counter or the salt and pepper shakers on the table of two men who spurn Tammy’s boorish flirtations."

