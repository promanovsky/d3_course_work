Ditching handshakes in favor of more informal fist bumps could help cut down on the spread of bacteria and illnesses, according to a study released on Monday.

The study in the American Journal of Infection Control found that fist bumps, where two people briefly press the top of their closed fists together, transferred about 90 percent less bacteria than handshakes.

"People rarely think about the health implications of shaking hands," Dave Whitworth, a biologist at Aberystwyth University in the United Kingdom who co-authored the study, said in a statement.

"If the general public could be encouraged to fist bump, there is genuine potential to reduce the spread of infectious diseases," he said.

The fist bump appears to enjoy the support of both U.S. President Barack Obama and the Dalai Lama, both of whom have been seen enthusiastically using the greeting, the study notes.

The study used participants who wore gloves that had been thoroughly coated in a film of non-pathogenic E. coli bacteria. They then variously shook hands, high-fived and fist-bumped fellow participants in sterile gloves and the amount of transferred bacteria was examined.

High-five slaps transferred about half the amount of bacteria as shaking hands.

Handshakes relay more germs because they result in a larger area of contact between hands, but the strength and length of handshakes also play a role, the study found.

"Transmission is greater with increased duration and grip," it said.

The research was prompted by an apparent increase in workplace cleanliness measures, including the growing use of hand sanitizers and keyboard disinfectants, the university said in a statemen