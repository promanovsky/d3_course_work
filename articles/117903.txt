DALLAS (CBSDFW.COM) – If you did not wake up early Tuesday morning to check out the first lunar eclipse of 2014, then you missed quite a show. In a rare spectacle, the moon turned a deep shade of red, a phenomenon known as a ‘blood moon’ because of the unique color.

The copper hue is created by the Earth’s shadow.

The eclipse began at 12:20 a.m. on Tuesday, but the red coloring could be seen between 2:00 a.m. and 3:30 a.m. across all of North Texas.

It is likely that nobody will remember the night more than Brandon Gomez, who surprised a special someone under that rare red moon. “Finally got to ask the person I love to marry me,” Gomez said. He and his girlfriend, Bonnie, were at the watch party at the Perot Museum of Nature and Science in Dallas. And, she said yes.

This was the first in a sequence of four lunar eclipses taking place over the next two years. The next lunar eclipse happens on October 8.

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: