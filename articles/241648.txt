DETROIT, May 23 (UPI) -- Suzuki Motors is recalling 184,244 cars made for it by General Motors because the steering columns could catch fire.

The Suzuki Forenza is a small car that was also involved in a recall in 2012 for a poor electrical connection that could case the headlights to fail. Forenza models from 2004 to 2008 and Reno models from Sept. 1, 2003 through July 30, 2008 are involved in the recall.

Advertisement

Suzuki said that the problem is similar to the recall initiated by GM earlier this week, which involved the overheating of the headlamp and running light modules, which could melt and catch fire on the left side of the steering column. GM said that it currently doesn't have a fix for the problem.

General Motors recalled 218,000 Chevrolet Aveos and Optras Wednesday, just a day after it announced the recall of 2.42 million GM cars, pickup trucks and SUVs.