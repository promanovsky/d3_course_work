CHICAGO (CBS) — Jet Magazine, a staple in African-American homes for 63 years, is pulling the plug on print. The Chicago based publication is shifting to online-only.

CBS 2’s Dorothy Tucker takes a look at the transition from paper to computer.

The president of the DuSable Museum gets a bit nostalgic talking about the iconic magazine launched in 1951 for just 15 cents, it was the face, the voice, the weekly vehicle for news about Black America.

“And if you were in Jet magazine your friends all over the country would say, ‘girl I saw you in Jet,’” said Carol Adams.

But being featured on the pages of Jet is coming to an end. Next Month Jet will be mostly digital.

CEO Linda Johnson-Rice inherited the publishing company from her father, John Johnson. He created Jet and the renowned Ebony Magazine.

“I think he would be very proud. We’re going back to his premise from the beginning and that was to disseminate news on a timely basis,” said Linda Johnson-Rice.

Jet, like many publications transitioned from print to online-only after suffering declining circulation and for Jet, a 25 percent drop in ad sales.

“You need to have a balance between subscribers and ad revenues and when that gets out of sync it does make it challenging,” said Johnson-Rice.

It also creates opportunities. Johnson-Rice sees a chance to attract a younger audience with more interactive options, yet keep the older ones with a twist on the magazine favorite.

“Now we’ll be able to see more about the beauty of the week, we can see her walking, talking,” said Johnson Rice.

Jet will introduce its digital App the end of June. It will be a paid subscription, $20 a year. There are plans to produce a special print edition once a year.