Ovarian cancer risk increases with excessive body fat, according to a new study by the Rutgers Cancer Institute of New Jersey.

The study discovered that a 6 percent increase in the risk of getting ovarian cancer was observed with a 5 point increase in the Body Mass Index (BMI) of a woman.

This groundbreaking study was one of the first to consider obesity as a possible cause for the occurrence of ovarian cancer.

However, the researchers emphasize that as far as the data shows, there is only a correlation between the two variables. No causal relationship has been established between obesity and ovarian cancer.

The researchers also found out that although a BMI of 25 is already considered overweight, women with BMI of 30 and above had the highest risk of developing ovarian cancer.

Moreover, overweight women who are also taller than average are seen to have a higher chance of developing ovarian cancer. It is unclear to the researchers how tallness plays a role in the risk of ovarian cancer.

According to Health Magazine, the researchers believe that the study sends one single important message: live healthier.

Elisa Bandera, associate professor of epidemiology at Rutgers say that efforts to prevent cancer should start as early in life as possible, not just during adulthood. Families, and not just individuals, should take a collaborative effort to prevent cancer risk factors and encourage healthy lifestyle.

A healthy weight may be able to prevent some form of cancers, says Bandera.

Samantha Heller, a dietitian who was not involved in the study, says she was not surprised by the recent findings.

According to her, an excess of fat cells “knocks off the body’s delicate balance of health” which can lead to imbalances in the body that increase the risk of growing cancer cells.

Heller, a clinical nutrition coordinator at the Center for Cancer Care in Connecticut, adds that as much as 95 percent of cancers can be prevented by healthy lifestyle and nutritious diet.

Ovarian cancer is the fifth leading cause of death in the United States. It is also one of the most difficult cancers to diagnose.

A June article by the Inquisitr reported that talcum powder, commonly found in baby powder and makeup, is also linked to ovarian cancer risk.

The American Cancer Society estimates that 21,980 women will be diagnosed with ovarian cancer in 2014.

They also estimate that about 14,290 women will die from ovarian cancer this year.

[Image from Tony Alter via Flickr]