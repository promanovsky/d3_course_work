Khloe Kardashian and Scott Disick clap their tennis rackets together while playing around at a tennis club on Thursday afternoon (June 12) in the Hamptons, N.Y.

The 29-year-old reality star and Scott, 31, filmed some scenes for their show Keeping Up with the Kardashians sans his longtime girlfriend Kourtney Kardashian, who is currently pregnant with the couple’s third child.

This is the second day in a row that Khloe and Scott were seen hanging out without Kourtney, who was spotted this week heading to an orthodontist office by herself.

If you watched the season premiere of KUWTK, then you definitely know about the special (and friendly) bond that Khloe and Scott have with each other.

30+ pictures inside of Khloe Kardashian and Scott Disick hanging out together…