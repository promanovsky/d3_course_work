Miley Cyrus is not in the mood to Twerk, pose topless or spit on any audience members.

Her dog has passed away.

The 21-year old announced the sad news via Twitter last night, telling followers:

"I don’t wanna say it because I don’t want it to be real… But my precious baby Floyd has passed away.. I am broken."

Cyrus later added another photo (above, right) and another Tweet, making it clear just how difficult this event is to deal with.

"I know I don’t mean it but I wish he would’ve taken me with him," she wrote. "this is unbearable. What am I gonna do without him?"

Cyrus owns multiple dogs and has always been open about how much they mean to her.

On her 21st birthday, Miley did not celebrate with a photo of herself downing champagne or blowing out candles on a cake. She posted a picture of herself and her pooch and wrote:

"I love him soooo f--king much I can't stand it."

We send our thoughts and prayers to Miley during this tough time.