Aereo appears before the Supreme Court on Tuesday in a case that has far-reaching implications for everyone from the average consumer, to the largest media companies in the world.

In one corner, there's streaming TV startup Aereo and its fields of tiny antennas. In the other, it's media institutions that have remained unchallenged for decades.

What's at stake? Billions of dollars, entire business models and the future of digital media.

See also: A Simple Guide to the Aereo Supreme Court Case

What the case means for Aereo

It’s pretty simple for Aereo. Win and the company has a major head start in a business that has a high barrier to entry. Lose and even its main backer, Barry Diller, has said the company is done.

Assuming it wins, Aereo will still be competing in an industry with giants like Apple, Google, Netflix and the broadcasters.

For broadcasters

One word: money. Broadcasters bring in money from cable and satellite television providers that pay retransmission fees. In 2013, financial services firm SNL Kagan projected those fees to hit $7.15 billion by 2018, up by more than $1 billion from its previous year’s estimates. With advertising spending either stagnant or in decline, that growth is important to the industry.

If the broadcasters lose, the court has essentially said those fees are unnecessary. Little would stop cable providers from employing similar strategies to access broadcast television.

Broadcasters have some options. CBS CEO Les Moonves indicated his channel could explore its own streaming service. Fox has said it would consider the nuclear option — ceasing public broadcast and going entirely to cable.

If the broadcasters win, they have essentially locked up their lucrative retransmission fees for the foreseeable future.

For consumers

All those retransmission fees come from one place: consumers with cable bills. Yes, 2013 saw a slight decrease in total cable subscribers, but there’s still millions of them.

For those that began to abandon cable, Aereo represents a legal, reasonably priced way to gain access to free-to-air TV. If Aereo doesn't win, consumers lose that access (or at least have to go buy their own antenna).

Not everybody has access to Aereo, and only a percentage that does has taken advantage of it. That does not stop its presence from impacting the average consumer.

If Aereo is found legal, broadcasters have less leverage to negotiate higher retransmission fees. Flat or even falling retransmission fees would hopefully make it to consumers who would have flat or falling cable bills. That might be a bit ideal (Comcast, for instance, could just pay lower retransmission fees and pocket the difference), but it at least eases upward price pressure on cable TV subscribers.

For cloud technology

At first blush, it might not appear that the Aereo lawsuit could have any impact on industries unrelated to broadcasting, but that's not necessarily true.

There are broader implications for other types of technology companies — particularly cloud-computing and cloud-storage companies, attached to the Aereo case.

That's because a key aspect of Aereo's defense is the court case Cartoon Network LP, LLLP v. CSC Holdings, Inc., better known as Cablevision. The Cablevision case involves the cable provider's network DVR product. The idea was that, rather than storing copies of recordings on a physical hard drive connected to a cable box, customers could store their copy on a hard drive in Cablevision's data centers. Cable networks argued that the digital copies stored by the cable company on its networks (or "cloud") violated reproduction and public performance statutes.

The second circuit of appeals disagreed and ruled that because Cablevision's network DVR didn't violate copyright or public performance laws. Networks attempted to bring Cablevision to the Supreme Court in 2009, but the court declined to hear the case.

Many scholars and legal analysts have accused broadcasters of attempting to use the Aereo case as a way to hold a backdoor-hearing on Cablevision. In fact, in the broadcaster's original writ to the Supreme Court, the Cablevision case is referenced 429 times and the appellate court is called into question.

So why does this matter to companies dealing with cloud computing or remote storage? Because the higher court refused to hear the case, the Cablevision ruling has been used as a way of establishing copyright liability protection for cloud computing companies (such as Amazon, Rackspace, Microsoft, Dropbox, Google, etc.). In other words, just because Dropbox makes a copy of a file on your computer that is protected by copyright doesn't mean Dropbox itself is guilty of copyright infringement.

If Cablevision were to be challenged — or if the Supreme Court were to partially invalidate some of the second-court rulings related to Cablevision in the Aereo case — it could have widespread implications for other cloud providers, particularly in the video space.

On the other hand, if the court rules in Aereo's favor — or opts not to address the Cablevision issue, especially as it pertains to digital copies — that ruling standing as case law, especially in relation to cloud services, would be strengthened.

A picture of the antenna array used by Aereo to capture broadcast signals. Image: Aereo

For the OTT/streaming industry

Almost no one questions that the future of broadcast is over-the-top (OTT), or online. In other words, rather than trying to tune into a digital over-the-air (OTA) signal using an antenna or cable, content will be delivered over the Internet.

Aereo is not the first over-the-top (OTT) service to attempt to offer live or near-live access to free-to-air (FTA) broadcast content. Other services in the United States have tried this tactic, and have largely failed — falling victim to lawsuits or running out of money before a legal resolution could be reached.

The Aereo decision will help offer better guidance for other OTT services — and the streaming industry as a whole — about how content can be distributed.

Aereo’s argument is that the physical location of an antenna isn’t relevant — as long as the antenna is used by only one person/device at a time. If that argument holds up in court, expect to other services pop-up that transmit content OTT.

The Digital Transition in 2009 introduced a variety of new digital terrestrial television stations (DTT), often accessible on digital subchannels. OTT distribution for those channels could help those stations reach more households — without having to negotiate for spectrum in a particular location. It might also allow those stations to to form deals for OTT access on streaming devices such as the Apple TV, Roku and Fire TV.

The potential of broadcasting OTT is huge, especially for existing OTT services. A ruling that would pave the way for OTT distribution could mean a future where broadcast content sits side-by-side pure OTT services such as Netflix, Hulu Plus and Amazon Prime. Services such as Aereo could be bundled alongside other digital streaming offerings with a more unified interface.

A ruling in Aereo’s favor could also help convince broadcasters to offer their own OTT services to users as a way to compete or undercut Aereo. To date, ABC is the only major broadcast network that offers live OTT streams of its network content — and even then, only for cable subscribers and only for users in markets served by an ABC-owned station (in other words, not a local ABC affiliate). A win for Aereo could also result in broadcasters speeding up their own plans to transmit digitally. For example, CBS has said it would react to an Aereo win by creating its own OTT service.

If the broadcasters prevail, it doesn’t necessarily mean that OTT content will go away — it just means that access to OTT content will likely be reliant on cable authentication — commonly known as TV Everywhere. TV Everywhere systems allow access to OTT streams of some broadcast and cable programming, provided the user authenticates with a login from cable or satellite company.

For copyright holders

The Aereo case represents particular areas of copyright law that do not apply very broadly. Free-to-air television is subject to different copyrights than other media. For instance, to stream a movie from a provider like Netflix, that company must have the rights to do so. If Aereo is found to be within current law, it would not suddenly be able to stream anything it cannot access over free-to-air TV.

Thus a victory for Aereo would not mean a dramatically different environment for content holders but could have a chilling effect on anyone who broadcasts content.

“I would not say that copyright would be weaker, but content holders may have serious reservations about broadcasting their content,” said Anderson Duff, a lawyer for intellectual property firm Wolf Greenfield. “Content creators need to make money from their content to continue producing more material. It is important that those revenue streams are protected.”

Duff added that Aereo is far from the only company putting pressure on content creators and distributors to create cheaper and more efficient systems.

“I think it is important to recognize, however, that if the Supreme Court reverses the Second Circuit, the media landscape will still change,” Duff said. “Consumers are more and more expecting greater flexibility when accessing content, and content holders are working to make that happen.”

For local television

Local television channels could be among the biggest losers if the Supreme Court gives Aereo the OK. Local stations make money from retransmission fees paid by companies like Comcast or DirecTV. Customers of those companies move to Aereo, that means less subscribers and less revenue from retransmission fees.

Transmission fees are rarely swayed much by economic conditions or poor ratings, Brad Adgate, research director at Horizon Media, told Mashable. If Aereo begins to cut into this cashflow, the value of local stations could decline.

“Owning a TV station may become less desirable (there has been a flurry of acquisitions of local TV stations in recent years) and the value of owning a TV station will drop to some degree,” Adgate wrote in an email.

For the U.S. government

Politicians, particularly those with ties in the media industry, will be keeping a close eye on the case. Some of the companies and interest group involved represent major donors with millions of dollars in lobbying power.

For the Supreme Court, the case represents what will be one of its landmark rulings in the world media and technology.

If Aereo wins, Congress can expect some calls (and probably some campaign contributions) from broadcasters and other companies with an interest in stopping the streaming startup.

The Supreme Court may have ruled its antenna and cloud system legal, but that does not mean the government cannot make a new law rendering it on the wrong side of the law.

“If Aereo wins," Adgate wrote, "I would expect the broadcasters to pressure Congress to re-write the laws regarding retransmission consent fees to include streaming content."