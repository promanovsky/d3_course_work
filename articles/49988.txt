Nine-year-old Kamryn Renfro made headlines earlier this week when news that the third-grader was suspended from school spread like wildfire. Not for misbehavior - Kamryn was instead suspended for shaving her head, apparently in violation of the school's strict dress code. Kamryn's actions were a show of solidarity with her friend Delaney Clements, an 11-year-old battling neuroblastoma from the age of seven who has recently undergone chemotherapy, causing her to lose her hair. Kamryn and Delaney's school has now overturned its original decision, allowing Kamryn to return with her proudly bald head.

The board of directors of Colorado's Caprock Academy voted 3-1 on Tuesday to let Kamryn return to the charter school, and waive her violation of the dress code. A special meeting was called, with two policemen from the local Grand Junction precinct on hand to observe the meeting. While no real security threat was present, the meeting had drawn substantial outside attention, prompting Grand Junction police chief John Camper to volunteer the services of uniformed offices.

The only board member to vote against waiving Kamryn's dress code violation was Bill Newcomer, stating that the 'emotionally charged' decision would establish an unwanted precedent for future policy violations.

Kamryn had the full support of Delaney, as well as her parents. "I was really excited I would have somebody to support me and I wouldn't be alone with people always laughing at me. I would at least have somebody to go through it all," said Delaney. Kamryn's mother, Jamie Olson Renfro, wrote the following statement on her Facebook page about the special meeting: "She got up, got ready, and held her head high as she walked into her classroom this morning. To say her dad and I are proud, is a total understatement."

Olson Renfro also noted on Wednesday that Kamryn would take an additional day off school, returning Thursday, as she would accompany Delaney to Denver for her next round of chemotherapy.

"It feels good to have my friends be there for me and to know I am not alone," Delaney said to the Denver Post.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.