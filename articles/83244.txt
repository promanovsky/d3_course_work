In 2009, MP Cheryl Gillan put forward a bill in parliament. The idea behind it was to ensure more support was available for adults with autistic conditions. Up to this point, children and their families were being given help, but children grow up. Even autistic children. What then?

That same year the Autism Act became a reality, and I was diagnosed as being on the autistic spectrum. I was 38 years old. As a child of the 70s, autism was practically unheard of. Any withdrawn or "difficult" behaviour on my part was generally seen as naughtiness. My lack of people skills was put down to me being antisocial, mean, or aloof.

There must be a multitude of adults out there who have some form of autism but remain undiagnosed. Confused, isolated and quite often suicidal. I know, because for much of my life that is how I felt.

Since the mid-90s, awareness of autistic conditions such as Asperger's syndrome, high-functioning autism and pervasive developmental disorder has grown, but not nearly enough to say they are fully understood. It has been disturbing for me, since being diagnosed, to discover how little people know.

Adults seeking diagnosis are often faced with a struggle. Not being taken seriously seems to be a common occurrence. I experienced more than just a few quizzical looks and furrowed brows when I asked to be assessed. Many people simply give up at this point. I was even asked outright, by a consultant psychologist, why I was seeking diagnosis when I was an adult. Apparently, this happens a lot.

It didn't help that much of the assessment process seemed designed specifically for children. This became even more exasperating when my mother's faded memories of my childhood were bought into the mix, creating a very confused picture. The team assessing me were concentrating solely on my childhood, the very distant past, rather than me in the here and now. Surely there is a better way to accommodate adults?

It took well over a year to get diagnosed, partly because only one place in the southeast had the facilities to do it. But I am glad I persevered. It means that my GP is aware of my condition. It meant I was able to get cognitive behavioural therapy to cope with the challenges I face every day. It has enabled me to understand who I am. But more than anything, diagnosis was a massive relief.

As far as support goes, where I live in south London, things are better compared with five years ago when I was first diagnosed. But at the same time I have never been contacted or approached by any of the services in my area – I had to make myself known. This can be a problem when it comes to people with autism. If you leave it to us, it might not happen.

Since 2009, most local authorities have set up schemes for adults seeking a formal diagnosis. Which is fine, but then what? Will a freshly diagnosed adult get the support they need? And what about autistic children who have grown up? Has the Autism Act done anything to improve these people's lives?

Here are some depressing statistics. Of all adults with autism, 70% feel they are not receiving the help they need; 36% said they need help to wash and dress, but only 7% get this support from social services; and 53% say they want help to find work, but only 10% get the support to do so.

Last year the National Autistic Society started a campaign, Push for Action, to improve support for adults with autism. In October, I joined other campaigners in delivering a petition to 10 Downing Street demanding more action from the government, including money for new services, better training for staff such as GPs and care assessors, and more to be done to raise public awareness of autism.

Things are slowly getting better, especially with regard to awareness, but solid support is still lacking. I hope that the government's revised autism strategy, which will be published today – World Autism Awareness Day – will tackle this and actually improve the lives of adults with autistic conditions as well as those of their families.

In this day and age, I hate to think that anybody else would have to go through the time-consuming and frustrating process that I experienced. Autism is a real and serious condition, and adults have it too.