Responding to a sharp public backlash, Federal Communications Commission Chairman Tom Wheeler revised his proposed Internet traffic rules as he tries to secure support for the agency to start formally considering them this week.

The new net neutrality plan, circulated to the agency’s four other commissioners Monday, largely sticks to the same outline Wheeler sketched last month to try to ensure all Internet traffic is treated equally. A federal court tossed out the FCC’s net neutrality rules in January.

In a nod to criticism from many Democrats, Internet companies and public interest groups, Wheeler tried to soften the most controversial part of his proposal by allowing broadband providers to charge higher fees for faster delivery of content as long as consumers and competition are not harmed, an agency official said.

The revised plan seeks public comment on whether such pay-for-priority arrangements should be banned, said the official, who spoke on condition of anonymity because the plan has not been made public yet.

Advertisement

The plan also asks more questions about whether the FCC should subject Internet service to tougher utility-like regulation, similar to telephone service.

The move would give the FCC more authority to police online traffic, but Internet providers argue it would stifle innovation.

In addition, the proposal asks the public whether the agency should create an ombudsperson to serve as watchdog to protect consumers and Internet start-ups, the official said.

Wheeler made the revisions after his two fellow Democrats on the FCC, Mignon Clyburn and Jessica Rosenworcel, raised concerns last week about his initial plan.

Advertisement

Rosenworcel called for a delay in Thursday’s scheduled vote for at least a month because she had “real concerns” and wanted to get more public input before moving forward.

With both Republican commissioners strongly opposed to net neutrality rules, Wheeler needs the support of Clyburn and Rosenworcel to start the rule-making process. That procedure would involve months of collecting comments from the public and companies as commissioners consider the rules.

Clyburn had no comment Monday, a spokeswoman said. Rosenworcel did not respond to a request for comment.

Some net neutrality advocates said Wheeler’s revisions were a step in the right direction, but didn’t go far enough.

Advertisement

Michael Weinberg, vice president at Public Knowledge, a digital rights group, said the FCC should not allow providers to charge for faster delivery.

“Going forward, we will continue to push to make sure that the FCC understands that Internet fast lanes go against the core values of net neutrality, and that the public demands protection for a truly free and open Internet,” Weinberg said.

Craig Aaron, president of Free Press, a public interest group, said Wheeler should push to reclassify broadband service for tougher regulation.

“If preventing fast and slow lanes on the Internet is the goal, reclassification is the way forward,” Aaron said.

Advertisement

jim.puzzanghera@latimes.com

salvador.rodriguez@latimes.com