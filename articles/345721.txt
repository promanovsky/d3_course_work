A newly-published genetic analysis of hair samples suspected as being from a cryptic primate known by various names such as "bigfoot" or "yeti", has revealed they actually originated from dogs, horses, bears or other well known mammals. The analysis was conducted by an international group of scientists on a small fragment of mitochondrial DNA isolated from "bigfoot" hair samples collected during the previous 50 years by hikers, naturalists and hunters. However, two samples were found to be most similar to the Palaeolithic polar bear, Ursus maritimus, found on Svalbard.

When mountaineer Eric Shipton returned from his 1951 expedition to climb Mt Everest, he returned with more than cold fingers and toes. He brought back a photograph of enormous footprints in the snow. The photograph, which included a print of a human-like bare foot alongside an ice ax for scale, were later attributed to a large unknown species of primate, locally known as the Yeti.

Of course, the news traveled the world with astonishing speed in those pre-twitter days, and captured the public imagination: a really big unknown primate species living hidden in the Himalayas? Incredible!

Soon, more reports popped up describing similar creatures in other remote wilderness areas, including the mountains in the Pacific north-western region of North America (where it was known as "bigfoot", "sasquatch" or the "abominable snowman"), Europe's Caucasus mountains ("almasty") and even the rain forests of Sumatra ("orang pendek" or "short person"; a mythical creature that is the opposite of everything that Westerners associate with Bigfoot).

At least some of these reports included photographs or casts of outsized footprints, hair, blood, feces or – occasionally – photographs (grainy) or film (also grainy) of huge hairy ape-men. Even Hollywood got in on the act.

Reading on a digital device? Here's the video link.

Many of these reports were later found to be fraudulent or the work of pranksters. The complete absence of bodies or body parts, tools and other cultural artifacts, photographs from camera traps, or fossils, added suspicion to already dubious claims. This lack of scientific rigour regarding yeti and other "anomalous primate species" caused this occupation -- dubbed "cryptozoology" -- to be viewed as pseudoscience by skeptics.

But – rarely – large and seemingly mythical beasts (sometimes known as cryptids, other times known as monsters) have been identified and formally described relatively recently; notably the platypus, the okapi and the giant squid.

As with all cryptids, there are many ideas about what these "ape-men" may be – surviving remnants of prehistoric hominids, such as Homo neanderthalensis, Homo floresiensis or Denisovans, extinct apes such as Gigantopithecus, or even exotic hybrids between humans and other mammals.

In an effort to finally shed some light on the identities of these mysterious creatures, Bryan Sykes, a professor of human genetics at the University of Oxford and a Fellow of Wolfson College, Oxford, launched a collaborative project in 2012 with Michel Sartori, Director of the Lausanne Museum of Zoology. This international investigative team, which also included members from the USA and France, was known as the Oxford-Lausanne Collateral Hominid Project. The team proposed to collect and use the latest technology to analyse DNA contained within donated "bigfoot" hair samples, and they constructed a website to solicit specimens for genetic testing.

The scientific team received a total of 57 hair samples. Visual, microscopic and infrared fluorescence examinations eliminated two samples as "obvious non-hairs" (one was plant material, the other was glass fibre). Of the remaining screened samples, 36 were selected for genetic analysis based either on their origin or historic interest.

The samples were cleaned, the DNA was extracted and a short segment of mitochondrial 12S ribosomal DNA was amplified and sequenced. This highly-conserved DNA fragment is suitable for identifying species to genus but was not sufficient to distinguish between closely related species. Thus, this amplified fragment could identify the sample as originating from a canid, but it was not sufficient to differentiate between, say, a wolf, Canis lupus, a coyote, Canis latrans, and a domestic dog, Canis domesticus.

Recovered sequences were compared to known DNA sequences catalogued in GenBank to ascertain their identities. (GenBank is an open-access annotated database containing millions of DNA and RNA sequences along with their protein translations.)

DNA was recovered from 30 of the specially selected hair samples. DNA analysis revealed they originated from a variety of well-known animals, including American black bear (6 samples), canids (4 samples), cows (4 samples), horses (4 samples), brown bear (2 samples), deer (1 sample), North American porcupine (1 sample), sheep (1 sample), Malaysian tapir (1 sample), serow (1 sample), human (1 sample), and even raccoons (2 samples) – remarkable since one sample identified as a raccoon was collected in Russia, which is far removed from the raccoon's natural range.

These findings deal a fairly decisive blow to cryptozoologists' claims.

"Sykes' meticulous work shows that none of the 'evidence' sent him of possible Bigfoot/Yeti DNA is anything other than mundane known animals", said Brian Regal, who was not involved in this study. Professor Regal, a historian of science at Kean University in the United States and author of the popular science book, "Searching for Sasquatch", is an outspoken aristarch of cryptozoologists, whom he refers to as "monster people".

"For decades, monster people have accused mainstream science of ignoring them, refusing to look at their evidence, and making reference to non-existent conspiracies about the 'establishment' covering up the existence of anomalous primates", said Professor Regal in email.

However, Professor Sykes and his colleagues demonstrate that mainstream scientists are in fact willing to examine samples using cutting-edge technologies – if viable materials are provided.

"This is not as momentous a day as some cryptozoologists were hoping", said Professor Regal in email.

Although this study didn't reveal anything new to those of us who stay informed about cryptozoology, two samples returned strange matches. Both samples (25025 and 25191) were 100 percent matches to DNA recovered from a Pleistocene polar bear, Ursus maritimus, that lived more than 40 000 years ago on Svalbard. Weirdly, the authors report that neither sample gave a 100 percent match to modern polar bear DNA sequences, and neither specimen originated from within the polar bear's modern day range.

Professor Sykes and his colleagues elaborate (somewhat) in their paper about these two peculiar samples: "Hair sample no. 25025 came from an animal shot by an experienced hunter in Ladakh, India ca 40 years ago who reported that its behaviour was very different from a brown bear Ursus arctos with which he was very familiar."

But if this "experienced hunter" did not think this sample came from a brown bear, how did he ever conclude that it came from a Yeti?

Regarding the second sample, the authors write: "Hair sample no. 25191 was recovered from a high altitude (ca 3500 m) bamboo forest in Bhutan and was identified as a nest of a migyhur, the Bhutanese equivalent of the yeti."

A 100 percent match between two geographically distant hair samples to a Pleistocene polar bear is … spectacularly bizarre, in my opinion. So I contacted one of the world's foremost authorities of polar bear evolutionary history, Frank Hailer, a postdoctoral researcher at the Biodiversity and Climate Research Centre (Biodiversität und Klima Forschungszentrum) in Germany. Dr Hailer compared the authors' two reported DNA sequences to previously published data from other polar and brown bears but was unable to confirm the authors' reported results.

Dr Hailer instead found that the two sequences were 100 percent identical to a polar bear that was sampled somewhere between Siberia and Alaska approximately 10 years ago. He found that the Pleistocene polar bear sequence differed at one position from the sequence data reported from the authors' Himalayan "Yeti" samples. So, unless the database sequence submitted by the authors is incorrect, their hair samples actually do carry a DNA sequence that is present in modern polar bears.

But why might polar bear DNA be found in brown bears? A few years ago, Dr Hailer and his colleagues showed that polar bears hybridised with brown bears long ago in the late Pleistocene (doi:10.1126/science.1216424), so that may be the reason for Sykes and colleagues' genetic findings. Additionally, although several bear species occur in and around the Himalayas, none have so far been identified as carrying mitochondrial DNA from polar bears.

"If true, this would raise some interesting questions about the movement of polar bears, or at least their genes, outside their current arctic distribution", writes Dr Hailer in email.

"Brown bears might transport introgressed polar bear alleles far beyond the polar bear range. Of course, this assumes that the reported geographic origin of the hair samples is correct."

Did these two anomalous hair samples actually originate from the Himalayas? If so, the authors may have stumbled across an unreported population of brown bears in central Asia that carry mitochondrial DNA from polar bears.

Of course, it is important to point out that this study is based entirely on analysis of a short fragment of mitochondrial DNA sequence, which comprises a tiny portion of the entire genome, and is inherited solely from the mother.

"[W]e recently found that the Y chromosomes of polar and brown bears are clearly distinct", writes Dr Hailer in email. Thus, analysis of a small portion of the Y chromosome would "add the paternal view to the current result."

This point then raises a second aspect of this paper that surprised me: how did it get published by such a prestigious journal (Proceedings of the Royal Society B)? I cannot recall the last time I read a paper published in this particular journal that reports an expected, albeit negative, result (no genetic support for a previously unidentified hominid). Further, I have not read a paper in this journal that has such a brief and opaque methods section, especially when contrasted to such a startling finding (Pleistocene polar bear DNA). What's the dealio with that?

Despite my reservations, Professor Sykes' and his colleagues' study reveals that hair samples attributed to Yeti/Bigfoot/Sasquatch/the Abominable Snowman actually originated from a variety of common mammals, most of them domesticated. Thus, these samples do not support the notion that relict populations of unknown primates are alive somewhere in the world.

"While it is important to bear in mind that absence of evidence is not evidence of absence and this survey cannot refute the existence of anomalous primates, neither has it found any evidence in support", the authors write in their paper (doi:10.1098/rspb.2014.0161).

"Does this evidence disprove the legends of the Yeti, Migyhur, Almasty, Sasquatch/Bigfoot? It does not. Scientific hypothesis testing of this sort is not designed to, and cannot, prove hypotheses alternative to the null hypothesis", writes Norman MacLeod, the Keeper of Palaeontology at London's Natural History Museum, in a separate commentary, which was also published in the same issue of the Proceedings (doi:10.1098/rspb.2014.0843).

"Rather than persisting in the view that they have been 'rejected by science', advocates in the cryptozoology community have more work to do in order to produce convincing evidence for anomalous primates and now have the means to do so", the authors write in their paper.

"The techniques described here put an end to decades of ambiguity about species identification of anomalous primate samples and set a rigorous standard against which to judge any future claims."

Sources:

Sykes B.C., Mullis R.A., Hagenmuller C., Melton T.W. & Sartori M. (2014). Genetic analysis of hair samples attributed to yeti, bigfoot and other anomalous primates, Proceedings of the Royal Society B, doi:10.1098/rspb.2014.0161 (open access).

Frank Hailer, a postdoctoral researcher at the Biodiversity and Climate Research Centre (Biodiversität und Klima Forschungszentrum, or BiK-F) [emails; 30 June, 1 & 2 July 2014]

Brian Regal, historian of science at Kean University and author of the popular science book, "Searching for Sasquatch" [emails; 1 July 2014]

Also cited:

MacLeod N. (2014). Molecular analysis of 'anomalous primate' hair samples, (commentary) Proceedings of the Royal Society B, doi:10.1098/rspb.2014.0843 [$]

Hailer F., B. M. Hallstrom, D. Klassert, S. R. Fain, J. A. Leonard, U. Arnason & A. Janke (2012). Nuclear Genomic Sequences Reveal that Polar Bears Are an Old and Distinct Bear Lineage, Science, 336 (6079) 344-347. doi:10.1126/science.1216424 [OA]

.. .. .. .. .. .. .. .. .. .. ..

GrrlScientist can also be found here: Maniraptora. She's very active on twitter @GrrlScientist and sometimes lurks on social media: facebook, G+, LinkedIn, and Pinterest.