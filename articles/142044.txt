Irish actor Liam Neeson was today targeted by animal rights activists, protesting his support of New York's horse drawn carriages.

The Ballymena native is currently residing in the Big Apple and is a vocal supporter of the horse drawn carriage system in the city, which is a huge draw for tourists.

But People for the Ethical Treatment of Animals (PETA) picketed outside his plush apartment with signs saying, 'Liam Neeson: Stop Supporting Cruelty'.

Expand Close NEW YORK, NY - APRIL 19: PETA and NYCLASS stage a protest outside of Liam Neeson's Upper West Side apartment building in support of a ban on carriage horse rides through Central Park on April 19, 2014 in New York City. (Photo by Taylor Hill/Getty Images) / Facebook

Twitter

Email

Whatsapp NEW YORK, NY - APRIL 19: PETA and NYCLASS stage a protest outside of Liam Neeson's Upper West Side apartment building in support of a ban on carriage horse rides through Central Park on April 19, 2014 in New York City. (Photo by Taylor Hill/Getty Images)

He did comment on the protest, or leave his building to meet the activists.

He recently wrote a column for the New York Times in support of the carriages, with the headline: 'Carriages belong in Central Park'.

"New York’s horse carriages have made an estimated six million trips in traffic over the last 30 years. In that time, just four horses have been killed as a result of collisions with motor vehicles, with no human fatalities," he wrote for the paper.

Expand Close NEW YORK, NY - APRIL 19: PETA and NYCLASS stage a protest outside of Liam Neeson's Upper West Side apartment building in support of a ban on carriage horse rides through Central Park on April 19, 2014 in New York City. (Photo by Taylor Hill/Getty Images) / Facebook

Twitter

Email

Whatsapp NEW YORK, NY - APRIL 19: PETA and NYCLASS stage a protest outside of Liam Neeson's Upper West Side apartment building in support of a ban on carriage horse rides through Central Park on April 19, 2014 in New York City. (Photo by Taylor Hill/Getty Images)

"A majority of carriage drivers and stable hands are recent immigrants, often raised on farms in their home countries. They love their jobs and their horses, and they take pride in being ambassadors for this great city."

New York mayor Bill de Blasio recently pledged to ban the practice and instead replace them with same-look electrical versions after complaints by animal rights organisations.