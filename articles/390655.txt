Could pacemakers become a thing of the past? — ©Picsfive/shutterstock.com

Subscribe to our Telegram channel for the latest updates on news you need to know.

WASHINGTON, July 18 — Injecting a certain gene into cardiac muscle has been shown in animal studies to help a weakened heart beat more strongly, scientists said Wednesday.

If shown to be safe and effective in people, experts said the procedure might one day replace the need for electronic pacemakers, though that knowledge is years away.

“This development heralds a new era of gene therapy, where genes are used not only to correct a deficiency disorder, but actually to convert one type of cell into another to treat disease," said lead author Eduardo Marban, director of the Cedars-Sinai Heart Institute.

It is the first time that a heart cell has been preprogrammed in a living animal in order to cure a disease, said Marban.

Gene therapy has long been viewed as a promising but risky field, particularly after early attempts to use it in people in the 1990s showed it could be dangerous and even fatal.

Marban said the use of a mild virus as a delivery vector for the gene should reduce concerns that typically arise in gene therapy, such as the potential for a deadly immune reaction or the possibility that the process could lead to the formation of a tumour, but acknowledged that more research is needed.

The study in the journal Science Translation Medicine details a therapy in which a gene known as TBX18 is injected into an area the size of a peppercorn in the pumping chambers of the heart.

The gene converts some of the normal heart cells into another type, called sinoatrial cells, which take over the heart's pumping duties.

“In essence, we create a new sinoatrial node in the part of the heart that ordinarily spreads the impulse but does not originate it,” he told reporters on a conference call to discuss the research.

“The newly created node then takes over as the functional pacemaker.”

The minimally invasive approach was tried in pigs with a condition known as complete heart block, a severe condition in which the heart's electrical system is impaired and produces an irregular heartbeat.

The gene was injected with a catheter, and open heart surgery was not required.

The day after the gene was injected, pigs who received it showed significantly faster heartbeats than those who did not undergo the treatment.

It worked in the animals for the duration of the two-week study. The research team is continuing its work to see how long the effect may last.

'Sci-fi to reality'

David Friedman, chief of heart failure services at North Shore-LIJ's Franklin Hospital in New York, who was not involved in the study, said future work in animals should reveal more safety and efficacy data, “such as lack of inflammation or systemic infection from the gene delivery process.”

Another outside expert, Tara Narula, associate director of the cardiac care unit at Lenox Hill Hospital in New York City, described the work as taking “science fiction into the realm of reality.”

“Genetically re-engineering heart tissue so it becomes capable of generating electrical cardiac impulses is truly a remarkable step in the direction of one day freeing patients from the need for pacemaker implantation,” she said.

Some 300,000 electronic pacemakers are implanted each year in the United States, costing the healthcare system some US$8 billion (RM25.5b) per year, researchers said.

Those who might initially be aided by this kind of new therapy include the two per cent of pacemaker patients who develop infections.

Other include fetuses with a condition called congenital heart block, which affects one in 22,000 and causes an irregular heart beat in the womb.

Since it is impossible to implant fetuses with pacemakers, there is little that can be done to treat congenital heart block and it often results in stillbirth.

The approach developed by Marban and colleagues is still at least two to three years away from human trials, he said.

“It offers the potential, at least, for a single-shot curative therapy without the need for follow up interventions, repeated procedures or dependence on artificial hardware that has been implanted in the body," said Marban. — AFP-Relaxnews