From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Think finding a couple of quarters in your couch is cool? Try $116 million worth of Bitcoins.

The ongoing saga of Bitcoin exchange Mt. Gox took another strange turn this morning when the firm announced (PDF) that it has found an old wallet full of 200,000 Bitcoins, worth around $116 million. The news brings the total Bitcoins lost by Mt. Gox down from 850,000 (worth around $500 million today) to 650,000.

Mt. Gox, once the largest Bitcoin exchange on the web, filed for bankruptcy protection in late February following a series of attacks on its servers. The company closed its trading platform, which left anyone with Bitcoins on its system out of luck.

Mt. Gox says it discovered the lost Bitcoins in older wallets that were thought to be empty. The company moved those Bitcoins to other online wallets on March 7 and then to offline wallets in the middle of the month for security purposes. Offline wallets can’t be hacked because they’re not connected to the Internet, though they can be misplaced like cash.