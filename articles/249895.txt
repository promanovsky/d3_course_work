Minors with epilepsy may soon be allowed to legally use medical marijuana in Illinois. On Wednesday, the Illinois House passed the bill.

The bill went through the House with a vote of 98 to 18, the Chicago Sun-Times reports. Although the bill allows for minors to use medical marijuana to alleviate their epilepsy, it does not allow them to smoke it.

"These people are not interested in getting high," said Rep. Lou Lang, D-Skokie, who supported the bill. "These are folks that are interested in alleviating their seizures."

Some parents are strong believers that cannabis can help children with epilepsy.

"In a recent survey of 19 parents who gave CBD-enriched cannabis to their children to treat severe epilepsy, more than half said their child's seizure frequency dropped by at least 80 percent after starting the drug," LiveScience reported this week.

The bill passed through the House with the support of some Republicans who were originally against it. This included Minority Leader Jim Durkin, R-Western Springs, who said he thinks passing the bill "is the right thing to do," and Ron Sandack, R-Downers Grove, who cited "compelling reasons" for his support.

"I don't know how you can ignore these fact situations, these anecdotal situations, of the terrible tragedy that these families are going through, particularly these young children," Durkin said. "The fact is: parents have no other way to go. The medicines these children are taking are so strong that they're equally as debilitating as the actual infliction of the seizure that these children have."

Now, Senate Bill 2636 must go back to the Senate. Members must decide whether or not to pass alterations to the bill that allow for the Department of Public Health to grant minors with other "debilitating medical conditions" permission to use medical marijuana as well.

"It now makes no sense to limit the ability for minors, juveniles with epilepsy, the ability to get a byproduct -- frankly, a very limiting non-gateway-esque component of an oil -- that could give them relief," Sandack said.

"We're giving them a lot of leeway to decide ... what conditions this product could be used for, setting it up with the rulemakers and not just specifying that it's only for epilepsy, which we know it helps epilepsy," Rep. Jeanne Ives, R-Wheaton, who doesn't think the bill should apply to minors without epilepsy, said. "They're going to have a lot of leeway here."

---

Follow Scharon Harding on Twitter: @ScharHar.