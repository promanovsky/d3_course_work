Samsung Electronics Co. (SSNLF.PK) chairman is now recuperating in a stable condition after undergoing an emergency operation on Sunday, hospital officials said.

Lee Kun-Hee, 72, was admitted to a hospital near his home in Seoul on Saturday night experiencing breathing difficulties, and received cardiopulmonary resuscitation for symptoms of cardiac arrest.

Lee was then moved to Samsung Medical Center, where he had a cardiac procedure for acute myocardial infarction.

Lee is known to have had problems with his respiratory system since surgery for lung cancer in 1999 and has been suffering from respiratory complications since then.

Lee is South Korea's richest man with a net worth estimated by Forbes magazine at around $10.8 billion and has a son and two daughters. His son, Jay Y. Lee, serves as vice chairman of Samsung Electronics.

For comments and feedback contact: editorial@rttnews.com

Business News