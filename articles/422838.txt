Air France has suspended flights to Sierra Leone because of the Ebola outbreak there and at the request of the French government.

The decision comes a week after the airline's staff signed a petition calling on their employer to avoid Ebola-hit countries for their own safety.

Unions representing employees called on the airline to halt its flights to and from the West African countries that are grappling with the worst Ebola outbreak ever.

Air France has temporarily suspended flights to Sierra Leone at the request of the French government

The disease has now killed more than 1,400 people in West Africa.

Air France announced that it temporarily halted its three flights a week to Freetown, Sierra Leone, but said it is maintaining its flights to Conakry, Guinea, and to Lagos, Nigeria, cities it flies to once a day.

The French government said the increasing spread of Ebola — notably in Liberia and Sierra Leone — prompted its request for suspension of Air France flights to Sierra Leone and its recommendation that French citizens leave Sierra Leone and Liberia.

Referring to Ebola, the airline said: 'Measures in place at airports there "guarantee ... that no passengers presenting symptoms ... can board.'

A group of young volunteers wear special uniforms for the burial of people in Sierra Leone in a bid to combat the spread of the Ebola virus

The decision came as British Airways announced it was extending its flight suspension to Liberia and Sierra Leone to December.

The British carrier initially said it was halting its service between Heathrow Airport and Liberia and Sierra Leone until the end of August due to ‘the deteriorating public health situation in both countries’, but it confirmed that it is extending the suspension until 31 December.

The decision was announced by the Foreign and Commonwealth Office in an updated travel advisory for Britons.

As many as 1,400 people have died in West Africa in the worst Ebola outbreak in history

In a statement provided to MailOnline Travel, British Airways said: ‘The safety of our customers, crew and ground teams is always our top priority and we will regularly reassess the routes in the coming months.

‘Customers with tickets on those routes are being offered a full refund or a range of rebooking options.’

BA normally operates four flights a week between Heathrow Airport and Monrovia, Liberia, with a stopover in Freetown, Sierra Leone.

A week ago, Air France employees protested at the airline's decision to continue operating in West Africa.

Sophie Gorins, the secretary-general of the SNPNC union, which represents cabin crew, told AFP: ‘We know that our jobs put us at risk, but they are measured risks. This is completely out of control and the information is not the same from one day to the next.’

The Centers for Disease Control and Prevention (CDC) has released an image of how the Ebola virus looks

Some workers were also refusing to board planes bound for the affected countries, a spokesman for the airline said.

Many regional and major airlines have suspended services to the three countries most affected by the Ebola outbreak: Liberia, Sierra Leone and Guinea. The United Nations has said the lack of flights is making it increasingly difficult to bring supplies to those countries. U.N. flights to bring humanitarian workers to Guinea, Liberia and Sierra Leone, also have faced restrictions.

The World Health Organisation said the risk of transmission of Ebola virus disease during air travel is low as it is not spread by breathing air from an infected person.

The organisation said: ‘Transmission requires direct contact with blood, secretions, organs or other body fluids of infected living or dead persons or animals, all unlikely exposures for the average traveller.

‘The risk of getting infected on an aircraft is also small as sick persons usually feel so unwell that they cannot travel and infection requires direct contact with the body fluids of the infected person.’