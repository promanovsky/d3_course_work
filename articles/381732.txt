Weird Al parodies “Blurred Lines” with “Word Crimes” music video

This morning we’re getting treated to the second of 8 videos in 8 days from the master of music parody: Weird Al Yankovic. Like yesterday’s “Tacky”, this second installment once again parodies a Pharrell Williams-produced song. This time it’s all about the “Blurred Lines” – and a parody of the word-centric music video rather than the slightly more promiscuous release.

It’s difficult not to enjoy a song that starts with the phrase “everybody shut up.” This music video commands the words and the moving graphics the way several high-powered singles have over the past years – F*** You included.

NOTE: If you’re unable to see the video in your area, head to WeirdAl.com – we’re working on more alternatives!

This video may very well be used in classrooms at some point given its extremely helpful lesson planning for the english language. One example is the following set of lyrics: “Say you’ve got an ‘i t’ followed by apostrophe, then an ’s’, now what does that mean? You would not use this in this case. As a possessive, it’s a contraction. What’s a contraction? Well, it’s a shortening of a word or group of words by omission of a sound or letter.”

Make special note of the fact that Al gives an out to Prince for his use of the letters B, C, R, and U, and numbers instead of words. If you’re Prince, you can do whatever you want.

This is just one of a collection of music videos from Weird Al being released this week. Have a peek at the #8videos8days hashtag and stick around the SlashGear Weird Al tag portal. We’ll be rolling with the full collection before the week is done!