Celebrity

In an interview with the magazine, Fox also shares tips about love life, saying, 'Allow yourself to be strong and powerful and men will be crawling on their hands and knees.'

Jul 2, 2014

AceShowbiz - Megan Fox is featured on the front page of Cosmopolitan's August issue. The "Teenage Mutant Ninja Turtles" star shows off her hot body in a cleavage-baring pink bustier and printed skirt on the magazine cover. The mother of two touches her hair as she makes seductive stare for the camera.

In an interview for the spread, the 28-year-old actress talks about love life and explains why she thinks Ellen DeGeneres is "sexy." She shares her tips in love life, saying, "Women don't have to be desperate and try so hard. Allow yourself to be strong and powerful and men will be crawling on their hands and knees."

Speaking of "The Ellen DeGeneres Show" host, Fox says that she likes her because she is funny. "I think Ellen [DeGeneres] is sexy. Maybe it's the way she gives off the impression that she's anti-'the business' even though she's engaging in it. And humor is always sexy," she explains.

In other news, Fox apparently experienced some uncomfortable incidents during the filming of "Teenage Mutant Ninja Turtles" in Times Square. As reported by Page Six, Fox said she was cursed by some pedestrians for "hindering their commute."

"It's like, we're working, too," she commented on the incident. "We're making a movie right now. You're in it. Go see yourself in August. You're a pleasure."