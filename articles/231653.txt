Microsoft's (NASDAQ:MSFT) Halo TV series might not be an Xbox exclusive, according to a report from Variety. Rather, Microsoft is said to be working on a deal with CBS (NASDAQ:VIAC)that would bring the Steven Spielberg-produced series to the premium cable network Showtime.

The report is not altogether unbelievable, given that former CBS executive Nancy Tellem is in charge of Microsoft's original programming. Assuming it's true, is this a mistake? Microsoft could be giving up a key Xbox selling point.

Microsoft's TV ambitions have foundered

When Microsoft introduced the Xbox One last year, it was clear to all that the device was much more than a video game console. With the capacity to interface with cable boxes, and a bevy of voice commands, Microsoft had positioned the Xbox One as an all-in-one entertainment solution, capable of providing immersive video games while simultaneously improving on the traditional television experience.

At that rollout event, Microsoft announced the Halo TV series, and touted Spielberg's involvement. In addition to offering next-generation technology, the Xbox would also offer the highest quality original programming.

The problem, however, is that the Xbox One has foundered. After an impressive debut, sales slowed to a crawl, and Sony's competing PlayStation 4 surpassed it globally. To reinvigorate its console, Microsoft plans next month to release a cheaper version that excludes the somewhat controversial Kinect motion sensor system.

The loss of Kinect severely weakens the Xbox One's TV abilities. Without Kinect, the Xbox One can still interface with and control a cable box, but it cannot offer the voice commands ("Xbox, watch ESPN!") and gesture controls Microsoft has made central to its entertainment push. Microsoft will still sell the Kinect; but as an optional (and somewhat costly) add-on, it may see little adoption.

Exclusive content as a selling point

It's possible that the Halo TV series could have helped to jump-start Xbox One sales. Although I can't imagine many people shelling out $399 for an Xbox to watch one show, a well-received series might have swayed a prospective console buyer to Microsoft's system.

Giving the show to CBS robs Microsoft of this potential selling point -- gamers who wants to watch the show can still buy a PlayStation 4, so long as they also buy a subscription to CBS' Showtime (with more than 25 million U.S. subscribers, many gamers may already have one). Microsoft is said to be planning "interactive features" that would be exclusive to the episodes watched through the Xbox One, but unless they're truly revolutionary, it doesn't seem likely that Showtime subscribers will run out to buy an Xbox console.

An alternative advertising campaign

However, airing the Halo series on CBS' premium network could benefit Microsoft indirectly: If Halo is a success, it could spark general interest in Microsoft's Xbox brand, attracting buyers who would otherwise never have considered the console.

To put it another way, given that Microsoft's Halo franchise is its gaming division's most successful intellectual property, and completely exclusive to Microsoft's consoles, most die-hard Halo fans likely already own, or will eventually purchase, the Xbox One. Airing a Halo-themed series on Showtime, however, could broaden the appeal of the brand, making Halo relevant to nongamers. Nintendo used this strategy to great success in the 1990s with the series of cartoons and movies based on its most popular video game franchises.

There are still many unknowns: In particular, will the show actually be worth watching? Regardless, although it may seem like a negative, giving its exclusive content to CBS could be great for Microsoft's Xbox.