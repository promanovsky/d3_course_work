Apple's iPhone 6 may have been glimpsed for the first time in what appears to be a trio of leaked photographs.

The images appeared on Chinese social network Weibo, and are said to have originated from an employee at Apple manufacturing partner Foxconn.

Leaked pictures of iPhone 6 from WeiboThe photos show a silver device with an ultra-slim form factor, its camera lens appearing slightly raised from its body.

It is unclear from the pictures whether rumours that the iPhone 6 will feature a larger display are accurate.

Previous reports suggested that the handset will include a Lytro-style camera that lets users manually refocus images after they have been shot.

There have also been suggestions that the phone will sport a curved display made of near-unbreakable sapphire glass.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io