The UN's Intergovernmental Panel on Climate Change (IPCC) new and scary release includes a map showing how many things have been direly affected around the world in the last few decades.

"One of the most important findings is that we're not in era where climate change is a future hypothetical," Christopher Field, the co-chair of the IPCC's working group II paper said at a news conference from Yokohama, Japan, on Monday. 'There's no question that we live in a world already affected by climate change."

Each continent has a box showing how much of an impact climate change is having on physical systems (like glaciers, coastal erosion, and water resources), biological systems (like forests and reefs), and human systems (like crops and livelihoods).

Icons that are only outlined show when climate change is only a minor contributor. The stack of squares next to the colored icons indicates how much of these impacts is caused by the climate, from zero to five bars. When part of the bars are lighter in color, that means there's some doubt to how high that cause is.

For example: In North America we will have severe impacts (four bars) on our physical systems — including water issues along the California coast. There are also severe impacts (three bars) on ecosystems, including fisheries in both the Atlantic and Pacific oceans.

Across the globe there are severe impacts on human health, livelihood and economics (the red family).

IPCC

Here are the specific impacts that the changing climate has had in North America:

IPCC

Read more about the serious impacts of climate change on our food sources around the world.