France’s far right, anti-immigration National Front party nabbed a historic win last night in the European elections, with hopes now set on dissolving the National Assembly and creating an alliance with Eurosceptics. France's ruling Socialist Party is calling it a “political earthquake.”

Shortly after exit poll results were announced, National Front (FN) party leader Marine Le Pen was already claiming victory. With preliminary estimates giving the FN a steady lead of 25 percent over the center-right UMP’s 20.6 percent and the Socialist Party’s 14.2 percent, Ms. Le Pen said the French people had spoken loud and clear.

“[Our people] want politics by the French, for the French, and with the French. They don’t want to be led anymore from outside, to submit to laws,” Le Pen said from her party’s headquarters in Paris’s western suburb of Nanterre.

“We must build another Europe, a free Europe of sovereign nations and one in which cooperation is freely decided. Tonight is a massive rejection of the European Union,” she said, calling on President François Hollande to dissolve the National Assembly.

“What else can the president do after such a rejection?” she said.

The National Front’s win has caused a hailstorm of commentary and shock from opposing parties. While opinion polls from earlier this month showed the FN taking the lead in the European Elections, some observers were surprised to see it actually happen.

France’s Prime Minister Manuel Valls called the election results “a grave moment for France.

“The rejection of others is not the French way, and it’s not the image we want of France,” Mr. Valls said, in reference to the FN’s anti-immigration stance.

The FN registered its best results in any national election – topping those from the 2002 presidential election, when Le Pen’s father and former party leader Jean-Marie famously made it into the second round with nearly 18 percent of the vote.

The election results have the potential to offer the FN a significant voice in the European Parliament, giving them between 21 and 24 of France’s 74 seats – a sizeable increase from the three they won in 2009. Meanwhile, the UMP is expected to gain between 18 and 21 seats, with the Socialists getting 13 to 15.

The blame game

Despite Valls’s numerous trips to neighboring countries – including Germany and Spain – in a bid to show France’s commitment to Europe, his Socialist Party registered its worst results in the history of the elections. Their 2009 results of 16 percent were already considered a “catastrophe.” Some observers wonder if the estimated 57 percent abstention rate could have contributed to the Socialist Party’s poor score.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Meanwhile, UMP party leader Jean-Francois Copé received the brunt of the blame for his party’s Sunday loss. Bordeaux’s UMP mayor Alain Juppé called for a “more collective” leadership and Mr. Copé’s rival, former Prime Minister Francois Fillon, said the party must now look inward for answers. Copé said the results were evidence of “strong exasperation” by the French people, blaming the results on President Hollande’s leadership.

While France’s two major parties try to pinpoint what went wrong, the FN will now look to create a pan-European alliance with fellow far-right parties. Le Pen has initial approval from the Austrian Freedom Party and the Dutch Freedom Party, but will need to convince parliamentarians from at least six other countries to create the group.