Today, US tobacco companies will make their annual payment to 46 states for the cost of providing health care for people with smoking-related illnesses.

Could South Korea be headed down a similar path?

In a landmark case, the state health insurer there has filed a lawsuit against a number of domestic and foreign tobacco companies seeking to recoup the cost of treating people with diseases linked to smoking.

The BBC's Seoul Correspondent Lucy Williamson, told Rico Hizon that as this suit is coming from a state agency it has access to a lot more information than there has been in the past.

Watch more reports on Asia Business Report's website.