Southwest Airlines Co. (NYSE:LUV), the Dallas-based carrier that started 43 years ago, has long touted itself as “America’s largest domestic airline.” But it will have to change its slogan now that it has launched its first international operations.

On Tuesday Southwest began offering flights to the Bahamas, Aruba and Jamaica. In August, the airline will add flights to Cancun and Los Cabos, Mexico, with more routes to Mexico and the Dominican Republic to come in November.

The international routes are inherited from AirTran Airways, which Southwest acquired in 2011.

Southwest executive Teresa Laraba lauded the change as a big step for the airline, which operates nearly 700 aircraft and employs more than 45,000 people.

“Obviously we've been waiting for 43 years to get to this point. It's a pretty big deal,” she told the Dallas Morning News last week. “It's such an opportunity for us to plant our name and flag in international waters, and we've never done it. We are pretty excited around this place.”

For now, the international routes will still comprise a tiny part — just 1 percent — of the carrier’s capacity in 2014. Southwest CEO Gary Kelly doesn’t expect that to change soon. “I think it’s going to continue to be a relatively modest component of the Southwest route system for the near future, for over the next several years,” Kelly said in a call to analysts and reporters in April.