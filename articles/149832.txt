Thomas Piketty's new book, Capital for the 21st Century, has done a remarkable job of focusing public attention on the growth of inequality in the last three decades and the risk that it will grow further in the decades ahead. Piketty's basic point on this issue is almost too simple for economists to understand: If the rate of return on wealth (r) is greater than the rate of growth (g), then wealth is likely to become ever more concentrated.

This raises the obvious question of what can be done to offset this tendency toward rising inequality? Piketty's answer is that we need a global wealth tax (GWT) to redistribute from the rich to everyone else. That is a reasonable solution if we're just working out the arithmetic in this story, but don't expect many politicians to be running on the GWT platform any time soon.

If we want to counter the rise in inequality that we have seen in recent decades we are going to have to find other mechanisms for reversing this upward redistribution. Specifically, we will have to look to ways to reduce the rents earned by the wealthy. These rents stem from government interventions in the economy that have the effect of redistributing income upward. In Piketty's terminology cutting back these rents means reducing r, the rate of return on wealth.

Fortunately, we have a full bag of policy tools to accomplish precisely this task. The best place to start is the financial industry, primarily since this sector is so obviously a ward of the state and in many ways a drain on the productive economy.

A new I.M.F. analysis found the value of the implicit government insurance provided to too big to fail banks was $50 billion a year in the United States and $300 billion a year in the euro zone. The euro zone figure is more than 20 percent of after-tax corporate profits in the area. Much of this subsidy ends up as corporate profits or income to top banking executives.



In addition to this subsidy we also have the fact that finance is hugely under-taxed, a view shared by the I.M.F. It recommends a modest value-added tax of 0.2 percent of GDP (at $35 billion a year). We could also do a more robust financial transactions tax like Japan had in place in its boom years which raised more than 1.0 percent of GDP ($170 billion a year).

In this vein, serious progressives should be trying to stop plans to privatize Fannie and Freddie and replace them with a government subsidized private system. Undoubtedly we will see many Washington types praising Piketty as they watch Congress pass this giant new handout to the one percent.

The pharmaceutical industry also benefits from enormous rents through government granted patent monopolies. We spend more than $380 billion (2.2 percent of GDP) a year on drugs. We would spend 10 to 20 percent of this amount in a free market. We would not only have cheaper drugs, but likely better medicine if we funded research upfront instead of through patent monopolies since it would eliminate incentives to lie about research findings and conceal them from other researchers.

There are also substantial rents resulting from monopoly power in major sectors like telecommunications and air travel. We also give away public resources in areas like broadcast frequencies and airport landing slots. And we don't charge the fossil fuel industry for destroying the environment. A carbon tax that roughly compensated for the damages could raise between $80 to $170 billion a year (0.5 to 1.0 percent of GDP).

This short list gives us plenty of places where we could pursue policies that would lower profits to the benefit of the vast majority of the population. And, these are all ways in which a lower return to capital should be associated with increased economic efficiency. This means that, unlike pure redistributionist measures like taxing the rich, we would have a larger pie that would even allow for some buying off of the losers.

These are the sorts of measures that economists usually try to seek out when the pain is inflicted on ordinary workers. Economists are big fans of trade agreements that arguably boost growth but lead to a loss of jobs and wages for manufacturing workers. For some reason economists don't have the same interest in economic efficiency when the losers are the rich, but that is no reason the rest of us should not use good economic reasoning in designing an agenda.

In addition to the rent reducing measures listed above, there are redistributionist measures that we should support, such as higher minimum wages, mandated sick days and family leave, and more balanced labor laws that again allow workers the right to organize. Such measures should help to raise wages at the expense of a lower rate of return to wealth.

If this post-Piketty agenda sounds a great deal like the pre-Piketty agenda, it's because the book probably did not change the way most progressives think about the world. The basic story is that income and wealth are being redistributed upward.

Piketty has produced an enormous amount of data to support what we already pretty much knew. This is very helpful. However the real question is how are we going to reverse this upward redistribution. For better or worse, Piketty pretty much leaves us back with our usual bag of tricks. We just might feel a greater urgency to use them.