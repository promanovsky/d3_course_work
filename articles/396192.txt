The first trailer for 'Fifty Shades of Grey' has become the most watched movie preview of the year in less than a week.

Set to a new version of Beyonce's hit "Crazy in Love", the trailer has notched up more than 36.4 million views, according to the Hollywood Reporter, citing figures compiled by tech firm Zefr, since it was released with much fanfare on 24 July.

In its first week, "Fifty Shades" easily beat the trailer for the upcoming "Teenage Mutant Ninja Turtles", which racked up 31 million views at the end of March, and surpassed both Godzilla and Michael Bay's Transformers: Age of Extinction, with more than 26 million views.

The trailer gave fans of the erotic trilogy a first look at Calvin Klein model turned actor Jamie Dornan in the role of Christian Grey, a billionaire with an appetite for S&M, and Dakota Johnson as the shy college student Anastasia Steele.

The American Parents Television Council (PTC) also spoke out against NBC's decision to show an edited down clip of the trailer on morning television.

Directed by Sam Taylor-Johnson and co-produced by EL James, the author of the series, the film is due for release in UK cinemas on Valentine's Day 2015. The S&M trilogy has sold over 100 million copies, making it one of the biggest and fastest-selling book series of all time.