Families of the passengers and crew members on board missing flight MH370 have begun offering counselling to the relatives of those who died last week on downed flight MH17.

The remains of MH17 victims are not expected to arrive back in Malaysia for many more weeks. "No one deserves to go through what they're going through," said Jacquita Gonzalez, wife of MH370 in-flight supervisor Patrick Francis Gomez.

"Right now they [the MH17 bereaved] are like we were in the beginning: quiet and wanting their space. But we are here for them, we actually know what they're going through, we know this is so painful, so hard."

The offer came at a crucial time just one week after Malaysia Airlines flight MH17 crashed in the early hours of Friday 18 July in the Donetsk region on the Ukraine-Russia border, where it is believed pro-Russia separatists fired a surface-to-air missile at the aircraft, killing all 298 people on board.

Malaysia's prime minister, Najib Razak – who was lauded last week after negotiating directly with pro-Russia separatists for the return of the black boxes and the passengers' remains – had previously stated that the Malaysian remains were expected by the end of Ramadan. But he confirmed last week that their return could take much longer owing to forensic testing in the Netherlands, where the remains of 282 passengers – including 43 Malaysians, among them 15 crew – arrived on Wednesday.

"There are technicalities and legal requirements that cannot be avoided," he said. "It is highly unlikely for the remains to be brought back soon."

Dutch experts have now begun the difficult process of verifying and identifying the remains using DNA samples collected from next-of-kin. A special Malaysian crisis team, as well as a group of psychologists, chemists, forensics experts and police, are currently in Kharkiv to help with the investigation, while six other hospital teams in Malaysia are awaiting the remains once they arrive back from Holland via C-130 military plane, local media reported.

Malaysia Airlines and Malaysia's department of civil aviation are also working on removing all evidence from the crash site for further investigation – a complicated endeavour given that the site is on the frontline of a war zone.

"[T]he bodies may have to remain [in Holland] longer for a post-mortem to determine the elements of criminality," the health minister, Subramaniam Sathasivam told the New Straits Times.

"There is strong suspicion that the plane was shot down. There is a possibility that countries affected may want to seek justice for their citizens."

He added that the amount of evidence required to build a criminal case would take time and potentially delay the eventual repatriation of the bodies.

The victims' friends and families have been left saddened that they will not be able to receive the bodies as soon as they had hoped.

Dutch experts have now begun the difficult process of verifying and identifying the remains using DNA samples collected from next-of-kin.

Murphy Govind, the brother of MH17 stewardess Angeline Premila Rajandran, said: "It is sad that the bodies will not be home before [Eid, the end of the fasting period] but there's nothing we can do. We can just hope for the best.

"As long as the Dutch people are doing their job identifying the bodies, we just hope that they can do it as soon as possible."

The tragedy has added further pain to a nation reeling after Malaysia Airlines flight MH370 disappeared on 8 March, and is believed to have crashed in the southern Indian Ocean. For the family and friends of those on board MH370, the crash of MH17 is a stark reminder of how little closure there is for understanding what happened to the 239 people on board.

"I'm glad that MH17 is being settled and at least they have the remains coming back, they know where the plane is – now it's about who's at fault an who did that," said Gonzalez, who met her husband when she was 12, married him at 22, and was grieving on his 51st birthday last Thursday.

"But we are still in limbo, we don't know anything because we haven't heard anything about MH370 … We also want closure, we want to know what happened."

Instead of drawing the nation together, in some ways, the double tragedy has further amplified religious tension among Malaysians, who comprise Chinese, Indians and ethnic Malay Muslims.

It is often argued that Malay Muslims receive special benefits not available to other Malaysians, from government positions to scholarships.

A government announcement that "special arrangements" had been made for the remains of the 21 Muslim passengers on board MH17, with the Islamic religious and development departments providing logistics and a special burial site, was questioned over the lack of clarity on how the remaining Malaysian bodies would be handled.

"The fact that the Malaysian government is announcing special arrangements for only less than half of the total number of Malaysians killed in this tragedy seems a little awkward," one news report noted.

Adding to controversy, an MP caused an uproar after by telling parliament alcohol and revealing uniforms should be banned from all Malaysian flights to avoid "Allah's wrath". "If smoking is prohibited on flights, what more alcohol? This must not be allowed on our flights," said Siti Zailah Yusof, speaking in parliament earlier this week.

"Another thing the government should pay attention to is the dress code of female flight attendants, especially Muslim flight attendants.

"No one should die in sin … This must be taken into consideration: we cannot stop Allah's wrath."

Siti's comments were met with derision and disbelief by citizens and NGOs alike, who called her comments "sexist, discriminatory and condescending".

"Such a statement is insensitive and irrelevant, especially at a time when the grieving nation is still recovering," said human rights NGO Empower in a statement. "The MP failed to understand that the victims died because MH17 was shot down by perpetrators who have still not been brought to justice."