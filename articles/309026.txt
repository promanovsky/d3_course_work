CNET/Sarah Tew

CHICAGO -- Sprint wants to be the go-to carrier for fitness buffs.

At an event here Monday the company announced starting July 25 that it will offer the new Samsung Galaxy S5 Sport , a version of the popular Samsung smartphone that is water resistant and dust proof. It's also teaming up with athletic clothing maker Under Armour, which owns the fitness app MapMyFitness, and streaming music provider Spotify to provide a comprehensive fitness experience for GS5 Sport users as part of its new Sprint Fit Live experience.

Sprint appears to be trying to make sense of a market that offers dozens of apps and gadgets that promise to make consumers more fit and healthy. From smartphone apps, like MapMyFitness that promise to track users' activity and motivate them to continue, to wearable technology like Samsung's Gear Fit and Gear 2 devices, fitness is hot. Sprint CEO Dan Hesse said there's a need to help consumers make sense of this information and consolidate functionality.

"The fitness app market is just emerging, and it's really complex," Hesse said in an interview with CNET following the announcement. "Sprint Fit Live brings all the apps to one place and it provides a much richer experience on these devices, which are getting better and better."

The Galaxy S5 Sport is almost identical to the flagship GS5, which was introduced earlier this year and will be available on all four major wireless carriers. It includes the 5.1-inch 1920x1080 resolution AMOLED display, Qualcomm Snapdragon 801 processor, 2GB of RAM, 2800mAh battery, and Android 4.4 KitKat. In addition to these hardware specifications, it is also water and dust resistant.

The device will be available in two colors from Sprint: Electric Blue and Cherry Red. Sprint is taking preorders for it now, and it will be available on July 25. Sprint says it will offer the phone for zero money down and $27.09 per month, which is about $199 for customers with a monthly contract.

Sprint also launched a new feature for GS5 customers called Sprint Fit Live. It will consolidate all the fitness information and apps that GS5 users are accessing on their device and present it with a customized look and feel on the wallpaper or full screen of the device.

Sprint is offering a couple of free subscriptions to sweeten the deal for new Sprint customers.

CNET/Sarah Tew

New Sprint GS5 Sport customers will get 12 months of MapMyFitness MVP, which will allow them to track, monitor, and share workout activity. The free subscription will also include premium content, such as customized audio coaching, training plans, and live tracking. GS5 Sport Sprint customers on Sprint's Framily plan will also get a free six-month subscription to Spotify to access the streaming service's workout playlists. Customers not on Framily will get three months of premium Spotify service.

Sprint Fit Live will launch on the GS5 Sport, but Sprint plans to make it available to all Android phones in 2014.

The GS5 Sport on Sprint also comes with Samsungs S Health app pre-installed. This feature lets users access health information and map out workouts. It monitors a user's heart rate before and after workouts. For a limited time, GS5 Sport customers will also get $50 off a Samsung Gear Fit, one of Samsung's new wearable fitness devices.