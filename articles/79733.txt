The number of women opting for breast lifts has increased by 70 percent since 2000. Stats reveal that for every breast implant procedure there were two lift procedures conducted.

New plastic surgery statistics released by the American Society of Plastic Surgeons (ASPS) revealed that though breast implants remain the most performed cosmetic surgery in women, breast lifts have become increasingly popular. Since 2000, for every breast implant procedure conducted, there are two lifts. In 2013, more than 90,000 breast lift procedures were performed by ASPS member surgeons, according to a press release. Seventy percent of these were conducted on women aged between 30 and 54 years.

"The breast lift procedure is way up in my practice," said Anne Taylor, MD, an ASPS-member plastic surgeon in Columbus, Ohio. "More women are coming to us who've had children, whose breast volume has decreased and who are experiencing considerable sagging," she said. "For many of them, we are able to get rid of excess skin and lift the breasts back up where they're supposed to be."

These stats provide proof that breast lift procedures are growing at twice the rate of breast implant surgeries. In 2000, less than 53,000 breast lifts were conducted compared to 90,006 last year. 290,224 breast augmentation surgeries were performed by ASPS member surgeons in 2013, which is a 23 percent increase since 2000.

The popularity of breast augmentation surgeries has been a growing concern for health experts worldwide. Some women that can't afford this expensive cosmetic surgery take to alternate means to enhance their breast appearance that could have dire consequences.

Recently, a 39-year-old Argentine woman died after she tried injecting vaseline into her breast as a means of performing a breast augmentation procedure on herself. She was admitted into a hospital and died a few weeks later. Doctors revealed she suffered pulmonary embolism after injecting herself with petroleum jelly.

In 2013 alone, over 15.1 million cosmetic surgery procedures, including both minimally-invasive and surgical, were performed. This number is a 3 percent rise from 2013. Additionally, 5.7 million reconstructive surgery procedures were performed last year, up 2 percent from the previous year, according to Surgery.org.

Breast lifts are not the only cosmetic procedures that have risen in popularity over the last decade. In February, reports revealed that the Brazilian butt-lift surgical procedure experienced a 16 percent growth in 2013. Researchers also found that in 2013, 30 percent of plastic surgeons stated that they were doing butt augmentations, which was only 19 percent in 2012.

"It's just amazing, the numbers," said Dr. Douglas Taranow, a board-certified plastic surgeon in New York City's Upper East, via NBC News. "It's with J. Lo, and Beyoncé, and everyone else having a great derriere. ... I think people see that and they want to mirror image it."

Experts speculate that new products and advances in technology may be the factors contributing to this boost. In 2013 alone, the Food and Drug Administration (FDA) approved several new plastic surgery devices and products, including two form-stable silicone gel breast implants and a hyaluronic acid facial filler designed to treat mid-face volume loss.

Despite several reports highlighting the risks and complications of cosmetic surgeries, the number of people opting for them keeps rising. These risks include:

Complications related to anesthesia including pneumonia, blood clots and rarely, death

Infection at the incision site, which may worsen scarring and require additional surgery

Fluid buildup under the skin

Mild bleeding, which may require another surgical procedure, or bleeding significant enough to require a transfusion

Obvious scarring or skin breakdown, which occurs when healing skin separates from healthy skin and must be removed surgically

Numbness and tingling from nerve damage, which may be permanent.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.