Also helped by subdued stock markets, with which it has an inverse correlation, the yen had been inching up before the BOJ policy decision. It extended those gains after the BOJ kept policy unchanged, and strengthened further after Kuroda sounded upbeat about the economy and said both and inflation growth would pick up in coming months despite a sales tax hike in April.

The rose on Tuesday, as some investors trimmed bets against it after the Bank of Japan held off from additional easing and Governor Haruhiko Kuroda offered little indication that more stimulus was likely in the short term.

Many investors have been selling the yen in anticipation that the sales tax hike will hurt consumption, and that the BoJ may have to ease policy in coming months to soften the blow.

The greenback has also struggled against the yen in recent sessions after U.S. jobs data on Friday disappointed some investors who have been betting on a faster recovery in the world's largest economy. It has pulled back from a 10-week high of 104.13 yen set on Friday.

The dollar's drop offered a good opportunity to investors to initiate fresh bets in favor of the greenback and against the yen, analysts said. That is based on the view that monetary policy between the Federal Reserve and the BoJ is increasingly diverging. The Fed is tapering its bond-buying program and many are also looking for it to hike rates sometime in the middle of 2015.

Low Volatility

Besides monetary policy divergence between the BoJ and the Fed, implied volatilities - a gauge of how choppy a currency will be - have fallen too in the past few weeks.

Low volatility means a favorable environment for carry trades, where investors borrow in a low-yielding currency to invest in riskier assets. That hurts the yen which is traditionally favored in times of economic uncertainty and financial market turmoil.

That is likely to underpin higher-yielding, growth-linked currencies such as the Australian dollar and New Zealand dollar against the yen and keep the yen's gains in check against the U.S. dollar, Henderson added.

The euro inched up against the dollar, to trade around $1.38. It gained after the European Central Bank policymakers again played down the need for any immediate action, although many were wary of pushing it much higher.

--By Reuters