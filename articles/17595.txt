US President Barack Obama talks with a student holding an iPad during a visit to Buck Lodge Middle School in Adelphi, Maryland February 4, 2014. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SAN FRANCISCO, March 18 — Apple’s reintroduction of the last-generation tablet replaces the now-discontinued iPad 2 at the same price point.

It means that consumers can snap up the 9.7-inch retina display tablet for just US$399 (RM1,306) in the US and €379 (RM1,726) in Europe, which is a bit of a bargain considering that less than six months ago it was the company’s flagship iPad and cost US$100 more. The model had been discontinued when the company’s new flagship tablet, the iPad Air, was introduced.

For the price, consumers get a choice of color — black or white — but are confined to 16GB of storage. However, it does pack Apple’s A6X processor, a 5-megapixel rear-facing camera, and supports the company’s new Lightning connector. The old iPad 2 still used the 30-pin connector that the company started phasing out with the launch of the iPhone 5.

The reintroduction also means that the company’s tablet lineup remains unchanged with four devices: a flagship and entry-level 9.7-inch iPad and premium and basic 7.9-inch iPad Mini on offer. — AFP/Relaxnews