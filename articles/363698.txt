Los Angeles: Singer-fashion designer Jessica Simpson, who is set to tie the knot with fiance Eric Johnson, reportedly got emotional at her rehearsal dinner.

The 33-year-old star and Johnson, 34 were joined by approximately 70 family members and friends at San Ysidro Ranch in Montecito, California on July 3 for their first pre-wedding celebration ahead of their marriage, reportedly.

The singer, who wore a beaded silver mini dress, got teary-eyed several times when friends stood up and gave heart-warming speeches to toast the couple," a source said, adding it was very sweet.

The couple`s children were the centre of attention at the bash, and both were dressed in formal attire, according to the source.

Simpson, who announced her engagement to Johnson in November 2010, was previously married to Nick Lachey, while the Yale graduate was married to Keri D`Angelo for five years before their divorce was finalised in October 2010.