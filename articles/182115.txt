Washington: Researchers have said that the if a small area of ice on East Antarctica shore melts it could lead to 10 foot rise in sea-levels.

According to researcher, an ice rim currently holds the largest region of marine ice on rocky ground in East Antarctica and warming oceans could lead to ice loss on the coast, while the air over Antarctica stays cold.

According to the researchers, if the rim is lost it can lead to sea-level rise of about 10-13 feet, Discovery News reported.

Co-author Anders Levermann said that if the ice-cork region loses 50 per cent of the ice then the discharge will begin, adding that the scientists have overestimated the stability of East Antarctica.

Computer simulations of the region has shown that it`s going to take about 5,000-10,000 years for the basin to discharge completely and once started, the basin would empty, even if global warming is halted.

The study has been published in the journal Nature Climate Change .