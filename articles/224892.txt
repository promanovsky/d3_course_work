A £10m prize has been launched to solve one of the greatest scientific problems facing the world today.

The competition idea is based on the 1714 Longitude Prize, which was won by John Harrison. His clocks enabled sailors to pinpoint their position at sea for the first time.

In an updated version, the public will be asked to choose a new challenge.

Rebecca Morelle reports.