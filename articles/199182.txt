6 and older

Legends of Oz: Dorothy’s Return (PG). The animated “Legends of Oz: Dorothy’s Return” is a tacky, cloying confection with forgettable songs. Toto is cute, though, and the film is perfectly safe fare for those 6 and older, and some younger kids. Based on newer Oz stories by Roger S. Baum, great-grandson of “Wizard of Oz” creator L. Frank Baum, the cartoon never achieves the magic of the 1939 live-action film. Dorothy awakens from a dream about her time in Oz to find a shifty “official” telling her aunt and uncle that their tornado-damaged farm is condemned. Back in Oz, a new villain, the Jester, has taken over. Scarecrow, Tin Man and the Cowardly Lion use the Wizard’s old machine to bring Dorothy back so she can help.

THE BOTTOM LINE: The flying monkeys work for the evil Jester now and are just as creepy and malevolent looking as they were 75 years ago. The Jester looks more like a shape-shifting “Batman” villain, so he could unsettle viewers younger than 6, as could images of a huge tornado.

8 and older

Moms’ Night Out (PG). Kids 8 and older of any religious background may be carried along on the adventures of a stressed-out mom in this faith-infused Christian comedy, saved from blandness by a solid cast, some decent laughs and low-key talk about faith. Allyson is a hyperactive neat-freak mom, overwhelmed by her kids. She admits to feeling “distracted” and “stressed” and is often about to blow like the Incredible Hulk. Her understanding husband urges her to go out for a night with her friends. She and her bestie Izzy and the kindhearted but prim preacher's wife Sondra (Patricia Heaton of “Everybody Loves Raymond”) head to a trendy eatery, leaving the kids in the care of the dads. The result: a baby belonging to Allyson’s sister-in-law goes missing at a tattoo parlor; the dads and the kids wind up at the ER; and Allyson and her friends land in a bowling alley, then jail. After a heart-to-heart with a tattoo artist/biker, Allyson accepts her shortcomings and renews her faith in God’s and her family’s love.

THE BOTTOM LINE: The preacher’s wife is accidentally tasered by a cop and is temporarily paralyzed and has slurred speech. A man gets hit by a car but isn’t hurt. A secondary character seems drunk or stoned. A pet bird is accidentally squished, but off-camera. Some children under 6 or 7 may find it unsettling to see a mother so frantic and to see adults misplace a baby.

13 and older

Belle (PG). Here is a chapter in the history of the British slave trade of which few Americans are aware, and it is told in the mildest of styles — like a lushly designed cross between Masterpiece Theatre and an after-school special. Teens (and even some tweens) can certainly see it. The young woman of the title, Dido Elizabeth Belle, was the illegitimate child of a Royal Navy admiral and, it is believed, a woman enslaved in the West Indies in the late 18th century. When the admiral learns that the woman has died, he brings the girl to live at the English estate of his uncle and demands that the family treat Belle as one of their own, which they do. When she becomes a young woman and inherits a fortune, her guardians feel that she can never marry because she would be exploited. But Belle finds kinship and romance with a young English lawyer who wants to abolish the British slave trade. Her guardian, the lord chief justice, will soon rule on a case that affects the lawyer’s cause, and Belle risks all to get involved.

THE BOTTOM LINE: The horrors of the slave trade and slavery itself are not depicted in the film, and they are discussed in nongraphic terms.

PG-13

The Amazing Spider-Man 2. Teens may fall happily back into the world of Spider-Man in this sequel, but the overlong, uneven film swings from pillar to post as much as the superhero himself. Charming, athletic Peter Parker agonizes too much over how to balance his public heroics as Spider-Man with his private life and his yearning to solve the mystery of his parents’ disappearance. In love with Gwen, Peter worries about compromising her safety. His creepy childhood pal, Harry, sick with a genetic disease, injects himself with the dread spider venom from their dads’ research and morphs into the Green Goblin. We’re never quite sure whether Harry is wholly evil, which makes things interesting. The same goes for Jamie Foxx as Max, the nerdy electrical engineer at Harry’s company, Oscorp. Max accidentally electrifies himself, emerging as the power-eating Electro. Spider-Man must take on both of these villains, and the mood turns surprisingly dark at the end.

THE BOTTOM LINE: The violence is all high-flying fights and special effects, with very little blood. However, there is a lot of veiny sci-fi ookiness when humans morph into monsters. Characters use rare mild profanity.

R

Neighbors. A knock-down, drag-out, adults-only farce, “Neighbors” aims its lewd gags at the 17-and-older crowd. While often funny, the gags are too profane, sexually explicit and drug related to be appropriate for viewers younger than 17, and they’re especially not for middle-schoolers. Seth Rogen and Rose Byrne play a married couple, Mac and Kelly, who have an adorable baby. They’re still struggling with the idea of being adults themselves when a hard-partying fraternity buys the house next door. Mac and Kelly try to make nice with Zac Efron's Teddy, the feckless leader of the frat house. The 30-somethings drink and do drugs with the college kids to show they’re neighborly, but as soon as Mac and Kelly try to get them to turn down the music, Teddy declares war.

THE BOTTOM LINE: The major R-ish elements include very strong profanity and graphic sexual slang; explicit sexual situations and frequent semi-nudity; and use of marijuana, mushrooms, cocaine and booze at the frat parties. The slapstick fistfights, fireworks and gunfire are more PG-13-ish.

Locke. Actor Tom Hardy’s nuanced tour-de-force performance in “Locke” will attract high school- and college-age cinema and acting buffs, both for the experimental nature of Steven Knight’s film (terrific cinematography tracking a nighttime car trip) and for Hardy’s ability to hold our interest as the lone human on camera for 85 minutes. Hardy’s Ivan Locke is a construction site manager. We meet him as he gets into his car at dusk and heads on a three-hour drive to London to be at someone’s side. A stable family man, Locke feels obligated for complex reasons to do what he feels is his duty. His reasons come out in a series of fraught phone calls as he tries to balance all of his obligations.

The bottom line: The very strong profanity earns the R. The story also has themes dealing with marital infidelity.

Horwitz is a freelance writer.