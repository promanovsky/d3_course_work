We’ve hearing lots of news about the launch of the Office for iPad since yesterday, which includes Word, Powerpoint and Excel, but it’s not just the iPad.

Office is also available for iPhone and Android in the respective App Stores, and best of all they are available as a free download.

The much awaited Office for iOS and Android lets users create word documents, view them, edit spreadsheets, create presentation, ability to connect your cloud storage account and save the documents/spreadsheets online for quick access anywhere and much more.

However, there’s small catch here: the free version comes with a very limited functionality as it only lets users view and save the documents. If you want to create new ones or edit the ones save in your device, you must have an Office 365 subscription which costs $9.95 per month. Microsoft also announced a personal subscription that costs $6.99 so if you have it, you’re good to go, otherwise you way find it to be a little useless for editing documents.

Anyway, head over to the Google Play store or iOS App Store to grab Office (Word, Excel and Powerpoint). What do you think about the apps? Were they worth the wait?

Source: Office Blog

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more