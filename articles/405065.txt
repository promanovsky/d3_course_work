New findings could lead to the development of a blood test for determining suicide risk.

Researchers have detected a chemical alteration in a single human gene linked to stress reactions, Johns Hopkins Medicine reported. The study, published in The American Journal of Psychiatry, suggests changes in a gene linked to the brains response to a stress hormone could cause an everyday stressor to trigger suicidal thoughts.

"Suicide is a major preventable public health problem, but we have been stymied in our prevention efforts because we have no consistent way to predict those who are at increased risk of killing themselves," said study leader Zachary Kaminsky, Ph.D., an assistant professor of psychiatry and behavioral sciences at the Johns Hopkins University School of Medicine. "With a test like ours, we may be able to stem suicide rates by identifying those people and intervening early enough to head off a catastrophe."

The SKA2 gene is expressed in the prefrontal cortex of the brain, which is linked to inhibiting negative thoughts and impulses. The gene helps move stress hormones into the cell receptor's nuclei so it can function. If there is not enough SKA2 the stress hormone receptor cannot release cortisol through the brain.

To make their findings the researchers pinpointed a genetic mutation in a gene known as SKA2. They looked brain samples of mentally ill individuals as well as healthy people and noticed levels of SKA2 were greatly reduced in those who had died from suicide. Within this common mutation they found an epigenetic modification in some samples that changes the genes DNA sequence; this led to higher levels of the chemical methylation.

In another study the researchers looked at blood samples from 325 participants. They found higher levels of methylation at SKA2 in individuals who reported having suicidal thoughts and tendencies.

In the future the findings could help create a blood test predicting suicide risk. It could be used in the military for determining which members are most vulnerable.

"We have found a gene that we think could be really important for consistently identifying a range of behaviors from suicidal thoughts to attempts to completions," Kaminsky said. "We need to study this in a larger sample but we believe that we might be able to monitor the blood to identify those at risk of suicide."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.