A Russian spacecraft carrying a three-man crew docked successfully at the International Space Station on Thursday following a flawless launch.

The Soyuz craft, carrying NASA's Reid Wiseman, Russian cosmonaut Max Surayev and German Alexander Gerst of the European Space Agency arrived at the station at 5:44 a.m. (9:44 p.m. EDT). They had lifted off less than six hours earlier from the Baikonur cosmodrome in Kazakhstan.

The Mission Control in Moscow congratulated the trio on a successful docking.

They are joining two Russians and an American who have been at the station since March.

The Russian and U.S. space agencies have continued to cooperate despite friction between the two countries over Ukraine. NASA depends on the Russian spacecraft to ferry crews to the space station and pays Russia nearly $71 million per seat.

Until last year, Russian spacecraft used to travel two days to reach the station, and this will be only the fifth time that a crew has taken the six-hour "fast-track" route. After the previous launch, in March, the crew ended up taking the longer route because of a software glitch.