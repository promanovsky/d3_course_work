New ray of hope for patients battling high blood pressure [GETTY/PIC POSED BY MODEL]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up forfor latest news plus tips to save money and the environment

Boosting vitamin D may offer an alternative to drugs by cutting the risk of heart attacks and stroke, say scientists.

Eight million Britons take pills to combat high blood pressure, which affects one in three adults.

Researchers found that every 10 per cent rise in vitamin D in the system was linked to lower blood pressure and an 8.1 per cent lower chance of suffering from hypertension.

The vitamin is absorbed by the body from sunlight but is also found in small amounts in eggs, meat and oily fish.

High blood pressure is the main risk factor for stroke and plays a major part in heart failure and kidney disease.

A third of those with high blood pressure do not know it, which is why it is called the “silent killer”. The new study, published in the Lancet, is the first to find strong evidence that increasing vitamin D levels could be an alternative to hypertension drugs such as beta-blockers.

Lead researcher Professor Elina Hypponen from the University of South Australia said: “In view of the costs and side effects associated with anti-hypertensive drugs, the potential to prevent or reduce blood pressure and therefore the risk of hypertension with vitamin D is very attractive.”