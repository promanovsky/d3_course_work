Chinese government officials have made sudden visits to Microsoft Corp. offices in China, a spokeswoman for the company said on Monday, but declined to give any reason for the inspections.

China's State Administration for Industry & Commerce, which Chinese media reported had made the visits to Microsoft offices in Beijing, Shanghai, Guangzhou and Chengdu, declined to give comment outside of working hours.

Microsoft has been a focus of anti-U.S. technology sentiment in China since U.S. National Security Agency (NSA) contractor Edward Snowden revealed widespread spying programs, including PRISM, which used U.S. company's technology for cyber espionage.

In an e-mailed statement, the Microsoft spokeswoman said: "We aim to build products that deliver the features, security and reliability customers expect and we're happy to answer the government's questions," but declined to give any further information.

The world's largest software company has had a rocky time in China, including a call by state media for "severe punishment" against American tech firms for helping the U.S government to steal secrets and monitor China.

Earlier this month, activists said Microsoft's OneDrive cloud storage service was being disrupted in China.

In May, central government offices were banned from installing Windows 8, Microsoft's latest operating system, on new computers. This ban appears to not have been lifted, as multiple procurement notices since then have not allowed Windows 8.

Nevertheless, the company has pushed forward with plans to release its Xbox One gaming console in China in September, forming distribution ties with wireless carrier China Telecom Corp. and e-commerce company JD.com Inc.