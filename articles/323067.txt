About 250 residents of the New York City Rescue Mission were served sesame-seed-encrusted tuna, beef filet and berries with crème fraiche, thanks to recycling magnate Chen Guangbiao, who serenaded the crowd with a karaoke rendition of “We Are The World” and performed magic tricks.

Chinese Tycoon Treats 250 Homeless New Yorkers To Lunch In Central Park

During the luncheon, Chen promised to give each guest $300 in cash, but Mission director Craig Mayes said that was never the arrangement and those attending were told, 1010 WINS’ Juliet Papa reported.

“No cash, everybody understand? No cash. Everybody understood?” Mayes told guests.

The agreement apparently involved a donation to the mission supplying goods and services to its members. The rescue mission feared if cash were given directly to members it would be used for alcohol or drugs, WCBS 880’s Marla Diamond reported.

Chen had several people on stage with him holding the money before it was apparently given back and some of the guests stormed out of the restaurant when they found out they would not receive cash in hand, Papa reported.

“The four people who went on stage, it was a masquerade, this whole thing is a sham,” one guest said.

Lorna Richardson told CBS 2’s Vanessa Murdock she traveled from Hempstead on Long Island by bus and then by train to Central Park with her teenage son hoping to get some money.

“I needed that money. I don’t drink, smoke or do drugs. I have bad credit. I would like to get an apartment and pay off some debt,” Richardson said.

Through an interpreter, Chen said he was going to the mission to sort everything out, Papa reported.

Chen had placed ads in the Times and The Wall Street Journal to announce the event. Saying he wanted to invite 1,000 “poor and destitute Americans,” his photo was printed next to an image of Lei Feng, a soldier in Mao Zedong’s People’s Liberation Army who is characterized as selfless. The caption read “China’s Lei Feng for a new era.” Some of the waiters’ outfits on Wednesday were replicas of Lei’s uniform.

In an interview on “CBS This Morning,” Chen said he wanted to disprove the cliche image of rich Chinese spending money mostly on luxuries.

“I was not born into a rich family or a family of government officials. When I was 4 years old my brother and sister died of hunger, so I achieved my success through confidence, self-motivation and my hard work,” Chen said.

But Chen’s American ambitions surpass philanthropy.

Earlier this year, the 46-year-old businessman wanted to buy The New York Times. Times chairman Arthur Sulzberger, Jr., said the newspaper was not for sale.

Chen, whose worth is estimated at $750 million, has been deemed eccentric from his theatrical antics.

To protest air pollution in Beijing, he stood on a street corner handing out containers marked “Fresh Air.” Chen also rushed to the scene of a massive earthquake in Sichuan and handed out cash to victims.

“When I see that there is a disaster in China, I see a lot of U.S. companies or the U.S. government going to help China. But when there is a disaster in the U.S., I do not see any Chinese people donating money to the U.S.,” Chen told Murdock.

On Tuesday in New York City, he also was on the street handing out $100 bills to anyone who looked like they needed money. His English language business card reads: “MOST CHARASMATIC PHILANTHROPIST OF CHINA.”

“These are not titles I give to myself. Those are titles given by the people in the country,” Chen told Murdock.

You May Also Be Interested In These Stories:

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)