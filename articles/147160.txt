Today AT&T (NYSE:T) announced it would begin conversations with civic leaders in consideration of expanding its fiber network to up to 100 new municipalities across the United States.

The areas include 21 metropolitan statistical areas (MSAs) across the United States, including Atlanta, Chicago, Los Angeles, Miami, and San Francisco. Previously AT&T announced four places it would explore installing the service. Called GigaPower, the service has a 1-gigabit-per-second speed that is about 100 times what U.S. consumers typically get with broadband. AT&T currently has such speeds in Austin, Texas, and has committed to offer the service in Dallas. The company is also in advanced talks to bring GigaPower to two additional markets, Raleigh-Durham, and Winston-Salem, N.C.

"We're delivering advanced services that offer consumers and small businesses the ability to do more, faster, help communities create a new wave of innovation, and encourage economic development," noted AT&T Home Solutions Senior Executive Vice President Lori Lee in today's announcement. "We're interested in working with communities that appreciate the value of the most advanced technologies and are willing to encourage investment by offering solid investment cases and policies."

The press release highlighted the reason behind this was to allow both consumers and businesses the ability to use the capabilities of the AT&T U-verse with GigaPower fiber technology to outpace current Internet speeds provided by cable and other solutions. As shown in the chart to the right, the GigaPower service can deliver speeds up to 1 gigabit per second, which could allow for up to 25 songs to be downloaded in a second.

AT&T noted its expansion to new cities would be based on whether the areas had appropriate network facilities, receptive policies, and are those that "show the strongest investment cases based on anticipated demand." The company said it may start building some of the new networks by the end of the year. None of the new markets are in the Northeast because AT&T doesn't have landline operations there.

The release also highlighted the expansion of its fiber network is not anticipated to have an impact on AT&T's capital investment allocation for 2014, but it was a part of the Project Velocity IP, which is its efforts to both enhance and expand its high-speed networks in response to increased consumer demand.

-- Material from The Associated Press was used in this report.

link