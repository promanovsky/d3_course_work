New York: French publishing giant Hachette said Wednesday it wants to make peace with online retail titan Amazon, but on terms that "value" the role of authors and their publishers.

In the latest response in the weeks-old dispute between the two, Hachette said it would keep up negotiating efforts with Amazon.

"We will spare no effort to resume normal business relations with Amazon -- which has been a great partner for years -- but under terms that value appropriately for the years ahead the author`s unique role in creating books, and the publisher`s role in editing, marketing, and distributing them," Hachette Book Group said in an email to AFP.

"Once we have reached such an agreement, we will be happy to discuss with Amazon its ideas about compensating authors for the damage its demand for improved terms may have done them, and to pass along any payments it considers appropriate."

Hachette also criticized Amazon for treating literary works as commodities.

"By preventing its customers from connecting with these authors` books, Amazon indicates that it considers books to be like any other consumer good. They are not," the publisher said.

A day earlier, Amazon said it saw little reason for optimism in resolving the dispute, which is believed to be about pricing, and offered to contribute to a fund to compensate authors for lost revenue.

Amazon acknowledged it was maintaining less inventory from Hachette and was no longer taking pre-orders from the publisher, and said the talks were part of the normal discussions with suppliers.

"Negotiating with suppliers for equitable terms and making stocking and assortment decisions based on those terms is one of a bookseller`s, or any retailer`s, most important jobs," Amazon said.

"When we negotiate with suppliers, we are doing so on behalf of customers. Negotiating for acceptable terms is an essential business practice that is critical to keeping service and value high for customers in the medium and long term."

Noticed tactics include delaying delivery of Hachette books and not discounting works handled by the publisher, and suggesting that readers looking for some Hachette titles might enjoy a book from another author instead.

Amazon maintained that the discord with Hachette affects a small percentage, along the lines of a dozen out of every thousand, of the Kindle-maker`s book sales.

A recent New York Times report maintained that the "scorched-earth tactics" arose as Hachette balked at Amazon`s demands for better terms in contract negotiations.

Early this month, Hachette confirmed that many of its older titles and a few new releases sold by Amazon were being hit with shipping delays that it attributed to the negotiations with the e-retailer.

Amazon has a reputation for negotiating hard to push down prices for the goods it sells online.

In a turn that may have emboldened Amazon, the US Justice Department triumphed in an e-book anti-trust lawsuit against Apple.

Last year, the judge who found Apple guilty of illegal price-fixing for e-books ordered the tech giant to steer clear of new contracts with publishers that could violate antitrust law.