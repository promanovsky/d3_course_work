No wonder he's single! Bachelor star Juan Pablo Galavis blames poor English for mocking the mentally disabled

In January, he enraged Bachelor fans for saying that gay contestants should be banned because they're more 'pervert in a sense.'



Now Juan Pablo Galavis offended even more people on Thursday for retweeting an insensitive joke mocking the mentally disabled and using the 'R' word.



The 32-year-old reality star shared: 'Not every flower can save love, but a rose can. Not every plant survives thirst, but a cactus can. Not every retard can read, but look at you go, little buddy!



Foot-in-mouth syndrome: Juan Pablo Galavis offended fans of The Bachelor on Thursday for retweeting an insensitive joke mocking the mentally disabled and using the 'R' word

'Today you should take a moment and send an encouraging message to a f***ed up friend, just as I’ve done. I don’t care if you lick windows, or interfere with farm animals.



'You hang in there cup cake, you’re f***ing special to me, you’re my friend, look at you smiling at your phone! You crayon eating bastard you!'



The former footballer has since deleted his retweet, which included uproarious laughter: 'JAJAJAJAJA LOVED IT!'



Impressionable: It makes one wonder whether the 32-year-old reality star is teaching this 'culture' to his four-year-old daughter Camila with ex-girlfriend, Carla Rodriguez

Juan - a New York-born Venezuelan - then defended his actions by informing his 278,000 followers that it's no big deal in his country.



'In Venezuela the R word is USED commonly and by NO means is to OFFEND anyone... #Relax #DifferentCulture #Respect,' Galavis wrote.



'People, try to be HAPPY and enjoy JOKES and SARCASM.'



It makes one wonder whether the outspoken athlete is teaching this 'culture' to his four-year-old daughter Camila with ex-girlfriend, Carla Rodriguez.







Homophobic hunk: In January, the outspoken athlete enraged Bachelor fans for saying that gay contestants should be banned because they're more 'pervert in a sense'

The single father-of-one admitted his written English was 'horrible' and then scolded the Twitterverse for their lack of worldly culture.



'I'm BLESSED to speak SPANISH, an OK english and a LITTLE bit of portuguese...For those who DON'T I recommend to LEARN... #Culture,' he tweeted.



'Some people need to TRAVEL and get around the WORLD...The US is a GREAT country but there is a lot out there... #openyourmind #learnculture.'



The finale for the 17th season of The Bachelor airs Monday on ABC.