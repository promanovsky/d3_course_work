The U.S. Surgeon General Boris D. Lushniak called recreational tanning a "major public health problem that requires immediate action."

A report released Tuesday revealed there are 63,000 new cases of melanoma diagnosed every year, the Washington Post reported. This type of cancer results in about 9,000 deaths every year, some of which involve teens and young adults.

"Skin cancer greatly affects quality of life, and it can be disfiguring or even deadly. Medical treatment for skin cancer creates substantial health care costs for individuals, families, and the nation. The number of Americans who have had skin cancer at some point in the last three decades is estimated to be higher than the number for all other cancers combined, and skin cancer incidence rates have continued to increase in recent years," the report's executive summary stated.

The report, titled The Surgeon General's Call to Action to Prevent Skin Cancer, estimates 90 percent of all melanomas are a direct result of exposure to UV light; this is also the most preventable cancer trigger. For most people in the U.S. sunlight is their primary source of UV light exposure. Indoor tanning beds are also a huge source of exposure to this type of light; tanning bed exposure is "completely avoidable."

To combat rising melanoma rates the Surgeon General suggests "wearing tightly woven protective clothing, wearing a hat that provides adequate shade to the whole head, seeking shade whenever possible, avoiding outdoor activities during periods of peak sunlight (such as midday), and using sunscreen (in conjunction with other sun protection behaviors).

The report outlines five strategic goals to help reduce skin cancer rates in the U.S.: increasing outdoor Sun protection opportunities such as shaded areas; providing individuals with information encouraging healthy Sun protection habits; promoting policies that benefit national skin cancer prevention goals; reducing indoor tanning; strengthening "research, surveillance, monitoring, and evaluation" on skin cancer prevention.

"With this Call to Action, the U.S. Surgeon General emphasizes the need to act now to solve the major public health problem of skin cancer. To reduce skin cancers in the population, people must get the information they need to make informed choices about sun protection, policies must support these efforts, youth must be protected from harms of indoor tanning, and adequate investments need to be made in skin cancer research and surveillance," the summary stated.

SEE FULL STATEMENT

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.