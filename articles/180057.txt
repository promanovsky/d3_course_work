Researchers have found that the blood of young mice has the ability to restore mental capabilities in old mice. Reuters

Researchers have found that the blood of young mice has the ability to restore mental capabilities in old mice.

If the same holds true for humans, it could pave way for new therapeutic approaches to treating dementias such as Alzheimer's disease, said researchers from Stanford University School of Medicine.

In the study, published in the journal Nature Medicine, the researchers used sophisticated techniques to pin down numerous important molecular, neuroanatomical and neurophysiological changes in the brains of old mice that shared the blood of young mice.

The scientists also compared older mice's performance on standard laboratory tests of spatial memory after these mice had received infusions of plasma (the cell-free part of blood) from young versus old mice, or no plasma at all.

"We've shown that at least some age-related impairments in brain function are reversible. They're not final," said Saul Villeda, study's lead author and a graduate student at Stanford, who is now a faculty fellow in anatomy at the University of California-San Francisco.

Previous experiments by Tony Wyss-Coray, the senior author of the study and a Stanford professor of neurology and neurological science, Villeda and colleagues had found that key regions in the brains of old mice exposed to blood from young mice produced more new nerve cells than did the brains of old mice similarly exposed to blood from old mice.

This time, the researchers checked both for changes within nerve circuits and individual nerve cells and for demonstrable improvements in learning and memory.

They examined pairs of mice whose circulatory systems had been surgically conjoined. Members of such pairs, known as parabiotic mice, share a pooled blood supply.

Researchers found that the hippocampi of older mice that had been conjoined to younger mice more closely resembled those of younger mice than did the hippocampi of older mice similarly paired with old mice.

The old mice paired with young mice made greater amounts of certain substances that hippocampal cells are known to produce when learning is taking place, for example.

Hippocampal nerve cells from older members of old-young parabiotic pairs also showed an enhanced ability to strengthen the connections between one nerve cell and another - essential to learning and memory.

"It was as if these old brains were recharged by young blood," Wyss-Coray said.

In experiments, the improvement vanished if the plasma provided to the old mice had first been subjected to high temperatures.

Heat treatment can denature proteins, so this hints that a blood-borne protein, or group of them, may be responsible for the cognitive improvements seen in old mice given young mouse plasma, researchers said.