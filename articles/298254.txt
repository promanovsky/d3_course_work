Amazon's smartphone is expected to debut as early as Wednesday, and according to reports it will only be available on the AT&T network.

The Wall Street Journal reported Tuesday that AT&T will be the exclusive carrier of Amazon's rumored smartphone. The Journal cited unnamed people familiar with the plans.

An AT&T spokesman declined to comment on the report.

This isn't the first big phone release to be an AT&T exclusive. Back in 2007, when the iPhone first launched, it was an AT&T-only smartphone, leaving those on other carriers out of luck or forced to switch. Finally, in 2011, Verizon broke that exclusivity deal and began offering the iPhone.

Amazon smartphones have been widely rumored for years, but the company focused on their website, e-readers and tablets. In 2013, the company hired former Windows Phone executive Charlie Kindel to work on "something secret," according to Kindel's LinkedIn profile at the time.

Another Journal report suggested the new Amazon smartphone may be 3D-capable. Users won't need 3D glasses to see the images because there will be a total of four built-in camera with "retina-tracking" that will make images appear in a 3D-like hologram.

The phone, expected to come out this week, possibly as early as Wednesday, will have about 240,000 apps and games compatible with it through Amazon's Appstore.

An event will be hosted by Amazon Wednesday in Seattle. This is likely the official announcement and preview of the device.

A video advertising the "launch event" by Amazon shows customers testing an unknown product. They are amazed with the movement of the device. It's assumed to be the new smartphone but the camera doesn't show what they are holding.

Will you buy a new Amazon smartphone? Leave a comment below in the comments section and let us know!