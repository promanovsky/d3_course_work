Facebook is letting its Poke and Camera apps go the way of the Dodo, quietly and hopefully without a hitch, reports suggest. The move comes as Facebook continues to develop new apps for users and the Poke and Camera apps have been largely unsuccessful with users in the year and a half since Poke was launched.

Poke was seen as a competitor to SnapChat, but the Social Network did little to promote the idea and the app, leaving it on the backburner. Finally, they have decided to let it go.

Camera, which was also released in 2012 along with Poke, is also disappearing, as a result of the company's takeover of Instagram. The app was originally released only a few weeks after that acquisition, which had many experts questioning the role of the app and the Instagram future.

In the intervening two years since the apps went live, Instagram has continued to show massive success and Facebook is discontinuing the Camera app as a result, giving more time to focus on Instagram's widespread popularity.

It also comes as Facebook continues to move users from the multi-app strategy of a few years' ago into a main app dedicated solely to messaging.

The tech media world has been harping on Facebook CEO Mark Zuckerberg - who voices the Poke updates himself - over the failure of the app to capture the attention and appeal that the company had expected. In the end, not everything Facebook does turns to gold, is the observation of many as the apps come to a soft death.

Facebook is moving forward with its Creative Labs project aimed at developing new and innovative apps for users without needing a multiple selection of apps for daily purposes. The project has become the center for Facebook's app development and while Poke might be dead, new apps are already in the works.

"Great products don't start with a billion users, they start with just a few," says Michael Reckhow, product manager on Paper, an app that allows users to change fonts and readjust news and text with the app in order to create a better and more personal news feed.

The hope by ending the two largely unpopular apps is to put more time and resources behind Creative Labs and their continued expansion of apps to fit the growing media world that many users online want in their social media.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.