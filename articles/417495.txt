Orbitz shares were down as much as 5% on Tuesday after an announcement that American Airlines has pulled its fares from Orbitz websites, with US Airways set to do the same effective September 1.

Here's the full announcement from American:

Advertisement

FORT WORTH, Texas, Aug. 26, 2014 /PRNewswire/ -- American Airlines has withdrawn its fares from consumer websites powered by Orbitz, effective immediately. American Airlines Group has notified Orbitz it also will withdraw US Airways fares on Sept. 1, 2014. Corporate clients that use Orbitz for Business to book travel are not affected by this change."We have worked tirelessly with Orbitz to reach a deal with the economics that allow us to keep costs low and compete with low-cost carriers," said Scott Kirby, President - American Airlines. "While our fares are no longer on Orbitz, there are a multitude of other options available for our customers, including brick and mortar agencies, online travel agencies, and our own websites."

American expects these changes will have minimal disruptions for its customers. Customers can continue to purchase tickets and all options for travel on American and US Airways through aa.com and usairways.com. American and US Airways fares are also available through reservations agents and other travel agencies.

Advertisement

Tickets already purchased through Orbitz websites remain valid for travel, but changes to reservations must be made through each airline's reservations department.

This chart shows the drop in shares of Orbitz following the headlines.

Advertisement

More to come ...