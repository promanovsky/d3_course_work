Lady Gaga turned 28 on 28 March.

Before the world fame, Lady Gaga was simply Stefani Germanotta, an Italian-American girl from New York.

The Born This Way singer shot to fame with the release of her debut studio album, The Fame, in 2008. It sold over 15 million copies worldwide and gained her six Grammy nominations. Previously, she revealed that she has been influenced by Madonna, Judy Garland, Britney Spears and Michael Jackson.

Well known for her outlandish dress sense and out-there stage shows, she has worn a huge fanbase all over the world.

Lady Gaga is an avid Twitter user and often uses the popular social networking service to keep her fans updated on the latest in her life and career.

She became the first Twitter user to have 20 million followers and was listed by Forbes as one of the World's Most Powerful Celebrities and is seventh in its list of the 100 Most Powerful Women.

"#HappyBirthdayLadyGaga" and "#HappyBirthdayMotherMonster" has been trending on Twitter, with fans sending their birthday wishes.

Lady Gaga took to Twitter to thank her fans.

I love my fans best birthday ever http://t.co/QjlYLlu2v7 — Lady Gaga (@ladygaga) March 28, 2014

Meanwhile, Lady Gaga who is in long-term relationship with boyfriend, Zero Dark Thirty movie actor Taylor Kinney, recently revealed she likes to be submissive with him.

The Applause singer admitted during a radio interview on Sirius XM's The Morning Mash Up, saying: "He's totally in charge. I mean, when I am home, I am like, shoes are off, I'm making him dinner. He has a job, too, and he is really busy!"

"I'm in charge all day long, the last thing I want to do is tell him what to do," she explained. "It's not good for relationships to tell men what to do."