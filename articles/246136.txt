Update: Windows 9 is now known as Windows 10. Want to know more about when you can get your hands on it? Check out our in-depth Windows 10 release date page

Windows Phone 8.1 has only just joined the party, but according to a leaked document Microsoft is already well into development of Windows Phone 9 and Windows 9.

The document, which was revealed by Myce.com, mentions dates of between Q2 and Q3 2015 for the release of both preview builds.

In other words they could arrive any time between April and September 2015, so that's a big window, but if it hits the earlier end then we could be seeing Windows Phone 9 in less than a year.

Change is in the air

Despite Windows Phone 8.1 only being a small number bump it included some major new features, so we'd expect the upgrade to Windows Phone 9 to bring some massive changes.

We don't know what those will be yet, but earlier rumours suggest that Microsoft may do away with the tile based interface.

There's also been talk in the past that Microsoft might ditch Windows RT and use Windows Phone 9 on both phones and tablets.

It remains to be seen what the truth will be, but in the meantime at least we have Windows Phone 8.1 to tide us over.

Is the new OS any good? Read our hands on Windows 10 review to find out

Via PhoneArena