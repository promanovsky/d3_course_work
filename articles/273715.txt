Apple on Monday at WWDC 2014 unveiled OS X Yosemite, its latest Mac operating system, which brings a variety of visual changes, but also many new interesting features that will further improve the overall user experience of those customers who own multiple devices from the company. Yosemite will be released free of charge to consumers in fall 2014, with the open beta program to be available to 1 million Mac users later this summer, so there’s plenty of time to get your affairs in order and make sure your current Mac setup is compatible with the latest OS X version.

The good news is that any Mac that can run OS X 10.8 Mountain Lion or OS X 10.9 Mavericks will be able to also run Yosemite. That doesn’t mean all the new features demoed on stage during WWDC’s opening keynote will be available to all those models, but Apple is yet to explain what features, if any, will be exclusive to newer models.

To run Yosemite, users will need Macs with 2GB of RAM, at least 8GB of available storage and at least OS X 10.6.8 already installed on the device, in order to be able to download Yosemite from the Mac App Store.

The full list of Yosemite-compatible Mac models, as listed by ArsTechnica follows below:

iMac (Mid-2007 or later)

MacBook (13-inch Aluminum, Late 2008), (13-inch, Early 2009 or later)

MacBook Pro (13-inch, Mid-2009 or later), (15-inch, Mid/Late 2007 or later), (17-inch, Late 2007 or later)

MacBook Air (Late 2008 or later)

Mac Mini (Early 2009 or later)

Mac Pro (Early 2008 or later)

Xserve (Early 2009)

To see what devices are compatible with iOS 8, follow this link.