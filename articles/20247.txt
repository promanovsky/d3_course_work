NEW York City has been put on high alert over the past week. Not against a terrorist threat, but from an enemy within that is altogether much more preventable and curable.

Officials in the Big Apple are calling on all residents to vaccinate themselves as an outbreak of measles has caused a grand total of nine people to be hospitalised.

Nine? In a city of more than eight million? And they call that an epidemic?

Sure, in 2011 in Ireland we had nearly 200 cases, 75pc of which had not been vaccinated.

And last year we clocked up the highest rate of measles in all of Europe.

Yet there is little discussion here about whether so-called anti-vaxxers are responsible for the reappearance of the illness. Nor is the fact that measles can kill or leave children seriously disabled being talked about as much as it should be.

Every year, 1.5 million deaths worldwide are due to vaccine-preventable diseases, but yet educated, sensible Irish parents are choosing not to have their children vaccinated.

Why is this? Surely the 1996 Lancet piece by Andrew Wakefield – when he asserted a link between the MMR (Measles, Mumps and Rubella) vaccine and autism – has been well and truly trashed at this stage.

Since then, hundreds of peer-reviewed studies have shown that there is absolutely no connection between autism and the MMR jab.

Wakefield's irresponsible paper has been retracted and he has been stripped of his medical licence, yet still we hear of people who refuse to have their children immunised against disease because they fear "the autism link".

"Just look at all the evidence on the internet that says so," these people insist.

It has all been proven to be bulls***, if you'll excuse my French, and yet people still choose to believe it.

So just why do some people prefer to believe pseudo-scientific mumbo-jumbo over empirically proven facts?

And why do some media also fall into this trap – constantly failing to explain the crucial difference between correlation and causation?

This is also happening in the ridiculous fluoride debate, where there is absolutely no evidence that our fluoride intake causes any of the diseases being attributed to it.

Seth Mnookin, author of The Panic Virus: A True Story of Medicine, Science and Fear, makes the point that, in the 21st century, "feelings" seem to have as much validity – and sometimes even more – than proven empirical facts.

Because there is, with some justification, such widespread mistrust of governments, pharmaceutical companies and other agencies that many people, with the help of Google, prefer to depend on their gut feelings about what is true, regardless of facts.

The problem is that much of what they are finding as "evidence"on the internet is not only bulls*** but is also downright dangerous.

ANECDOTES

Writing about all of this, philosopher Stephen Law has explained how phenomena such as "piling up the anecdotes" (my sister's friend's brother's child got vaccinated and is now autistic), "playing the mystery card" (science doesn't have the answer to everything, you know) and asserting that "all the evidence fits" (the rise in cancer rates coincides with the introduction of fluoride in water) can all combine to drag us into something akin to a psychological fly trap.

We see this kind of nonsense peddled in Ireland every winter when numerous people die as a result of influenza.

And yet people at risk still refuse to get the flu vaccine because they have been fed the misinformation that it is somehow dangerous.

This sort of misleading conspiracy theory nonsense is killing us.

So I propose that concerned Irish citizens against dangerous nonsense should start a campaign of their own. I'm even prepared to lead it, if you like.

I could call myself The Girl Against Bulls***. That has a nice ring to it, doesn't it?

Now, I just need a fancy website and a few followers and I'm sorted.