(Adds detail, quotes)

April 17 (Reuters) - The U.S. Food and Drug Administration warned that a common surgical procedure used to remove uterine fibroids could spread undetected uterine cancer.

Data showed that the procedure, laparoscopic power morcellation, could significantly worsen a patient's chance of long-term survival, the regulator said. (http://r.reuters.com/dug68v)

Laparoscopic, or minimally invasive, power morcellation involves the use of an electric device that minces the fibroid into small pieces that can then be pulled out through a tiny incision in the abdomen.

Uterine fibroids are benign tumors that usually pose no risk. However, certain women suffer from symptoms that include heavy or prolonged menstrual bleeding, pelvic pressure and frequent urination, which could warrant medical or surgical intervention.

An estimated 50,000 procedures are performed using laparoscopic power morcellation devices in the United States every year, said William Maisel, chief scientist for the FDA's Center for Devices and Radiological Health.

The procedure reduces risk of infection, lowers post-surgical pain and enables quicker recovery compared with traditional surgeries.

About 1 in 350 women who are undergoing certain surgeries for fibroids could suffer from a type of uterine cancer, the agency said, but added there exists no reliable tests to determine if they are cancerous prior to the removal.

Some risk of cancer spreading as a result of the procedures has been known for many years to surgeons, Maisel said.

"(But) the magnitude of the risk is higher than had been appreciated by the medical community," he said on a conference call with journalists on Thursday.