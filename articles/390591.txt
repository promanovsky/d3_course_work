Could a SMELL test diagnose Alzheimer's? Inability to detect odours indicates damaged brain cells



A decreased ability to identify odours is linked to loss of brain cell function

A simple smell test could diagnose the early stages of Alzheimer's disease, scientists say.

Two new studies have found that the decreased ability to identify odours was significantly associated with loss of brain cell function and progression to Alzheimer's disease.

The research trials were presented at the Alzheimer's Association International Conference 2014 in Copenhagen.

One study, by Harvard Medical School, investigated the associations between sense of smell, memory performance, and amyloid deposition in 215 elderly people.

Beta-amyloid protein forms clumps in the brains of Alzheimer's patients and is a key hallmark of the disease.



The patients carried out smell tests and the size of their entorhinal cortex and the hippocampus - which are important for memory - and amyloid deposits in the brain were measured.

A smaller hippocampus and a thinner entorhinal cortex were associated with a worse sense of smell



Researcher Dr Matthew Growdon said: 'Our research suggests there may be a role for smell identification testing in clinically normal, older individuals who are at risk for Alzheimer's disease.

'For example, it may prove useful to identify proper candidates for more expensive or invasive tests. Our findings are promising but must be interpreted with caution.

'These results reflect a snapshot in time; research conducted over time will give us a better idea of the utility of olfactory [smell] testing for early detection of Alzheimer's.'

The second study, by Columbia University Medical Centre, also found people who struggled to identify odours in tets were more likely to suffer from dementia and Alzheimer's disease.

Commenting on the study, Dr Doug Brown, Director of Research and Development at Alzheimer's Society said: 'These studies do add to the growing evidence that changes in the sense of smell could be an early indicator of Alzheimer's disease, but we need larger studies to test how reliable smell tests could be in the clinic.

Brain scan showing Alzheimer's disease. In the study, patients carried out smell tests and the size of their entorhinal cortex and the hippocampus - which are important for memory - and amyloid deposits in the brain were measured

'Most people experience some sensory loss as they age, so people with a poor sense of smell shouldn't be immediately worried about dementia.

'There are more than sixty medical conditions that can affect your ability to smell odours including some medications. '





