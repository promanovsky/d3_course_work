Following a wave of calamities at sea, Carnival Corp. reported strong profit and revenue in its fiscal second quarter, with bookings for the rest of the year on pace to surpass last year’s numbers.

For the three months ended May 31, Carnival reported net income of $106 million, or 14 cents a share, compared with $41 million, or 5 cents, a year earlier.

Miami-based Carnival, the world’s largest cruise line, reported a slight drop in bookings for the period but at higher prices. The cruise line also benefited from lower fuel costs and lower fuel consumption.

Revenue was $3.6 billion, compared with $3.5 billion the previous year.

Advertisement

“We believe we have reached a positive inflection point for our company as we return to earnings growth in 2014 and work hard to ensure that growth accelerates in the years to come,” said Chief Executive Arnold Donald.

Advance bookings for the second half of 2014 are slightly ahead of 2013, at higher prices, the company reported.

Carnival increased its advertising campaign last fall by 40% and invested $300 million to upgrade every ship to rebuild its image following a series of mishaps at sea.

A fire aboard the Carnival Splendor in November 2010 killed power to the engines with 4,500 passengers and crew on board in the Pacific Ocean. The ship was towed to San Diego three days later.

Advertisement

The Costa Concordia, which is owned by a Carnival subsidiary, ran aground on an island off the coast of Tuscany in January 2012, killing 32 of 4,200 people on board.

In February 2013, a fire on the Carnival Triumph left the ship without power in the Gulf of Mexico. Passengers were stranded aboard the stalled ship for four days until it was pulled into port by several tugboats.

To read more about travel, tourism and the airline industry, follow me on Twitter at @hugomartin.