On April 10, BP (NYSE:BP) held its 105th annual meeting in London. Among the company's topics presented, its strategies to generate growth were the most pressing issues. BP is obviously a much smaller company in the years since the Gulf of Mexico oil spill. While management acknowledges the intense challenges that continue to haunt BP, it also feels it now has an opportunity in front of it.

BP believes its new status as a slimmed down oil and gas company will allow for greater focus on pursuing only the best, most profitable opportunities in the years ahead. While it may come as a surprise, one of its biggest opportunities will still be in the Gulf of Mexico.

A strategy that delivers value over volume

That's how management describes its over-arching goals over the next few years. BP is now a much different company than it was before the oil spill in the Gulf of Mexico. In the years since, BP has had to slim down, primarily to finance the massive legal fees and other associated penalties it's been forced to pay. Those fees have totaled more than $40 billion so far, a staggering sum that has, not surprisingly, required BP to raise cash.

But, in addition to setting money aside for those expenses, BP has its future as an oil and gas company to worry about. The company still wants to grow, and to accomplish all these goals, it's pursuing a strategy of value over volume. Over the next few years, BP will keep selling assets. Through next year, it plans to unload approximately $10 billion.

In addition, it's going to limit capital expenditures to roughly $24 billion this year, and won't spend more than $26 billion per year through 2018. This represents a much lower level of capital spending than BP's rivals, and shows how much smaller of a company BP has become in the years since the oil spill. For example, both ExxonMobil (NYSE:XOM) and Chevron (NYSE:CVX) plan to allocate $39.8 billion to capital expenditures this year, down roughly $2 billion for both companies.

BP's two rivals are trimming spending for a slightly different reason, which is that last year represented a peak year of investment. ExxonMobil and Chevron are now hoping the huge amounts of spending over the past couple of years will pay off with projects coming on-line.

Despite its capital restrictions, BP maintains its growth forecasts, which are to increase operating cash flow to $30 billion by the end of this year. This would represent a more than 50% increase from 2011. The company plans to achieve this with its smaller but more focused asset base, with fewer assets that are more streamlined. One specific area of focus will be deep-water exploration and production, where BP has several projects it expects to contribute in short order.

Among its global deep-water operations, BP is actually back to business in the Gulf of Mexico. It may be a surprise, but it's indeed true, thanks to a recent agreement reached with U.S. government. BP has reached an agreement with the Environmental Protection Agency that effectively resolves BP's suspension from new Gulf of Mexico contracts.

As part of the agreement, BP is once again able to enter into new contracts and bid for new deep-water leases in the Gulf of Mexico. In all, BP operates 10 drilling rigs in the deep-water Gulf of Mexico, and plans to increase future activity and investment there, which will be possible thanks to the agreement.

BP's focus will shape its future

BP has had to sell off tens of billions in assets and cut capital expenditures to strengthen its balance sheet and fund the more than $40 billion it's paid in financial penalties since the oil spill in the Gulf of Mexico. That's not deterring the company from making ambitious plans for the future. Rather, management is entirely confident it will achieve strong growth in operating cash flow and continue to return cash to shareholders.

This sense of confidence is due to management's focus on only the highest-performing assets in the best oil and gas fields across the world. This includes, but is not limited to, the Gulf of Mexico, where BP is once again a major player.