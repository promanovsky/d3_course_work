Intel’s 3D-printed robots

Share this article: Share Tweet Share Share Share Email Share

Rancho Palos Verde, California - Intel introduced a walking, talking robot on Wednesday made from 3D-printed parts that will be available to consumers later this year, if they are willing to assemble it with a kit that costs around $1 600 (about R16 000). The company's Chief Executive Brian Krzanich was accompanied by “Jimmy” on stage at the Code Conference in Rancho Palos Verdes, California. The white 2-foot tall robot shuffled onto the stage, introduced itself and then waved its arms. Intel describes Jimmy as a research robot, but the company intends to make 3D-printable plans available without charge for a slightly less advanced version, and partners will sell components that cannot be 3D-printed, such as motors and an Intel Edison processor, in kits. Jimmy can be programmed to sing, translate languages, send tweets and even serve a cold beer.

Under Krzanich, who took over a year ago, the chipmaker is trying to be an early player in emerging technologies like smart clothing, after coming late to the mobile revolution and making little progress in smartphones and tablets.

Its strategy includes engaging tech-savvy do-it-yourselfers and weekend hobbyists working on everything from Internet-connected baby blankets to robots and drones.

Owners of the robots will be able to program them to perform unique tasks. They can then share the programs with other owners as downloadable apps.

Intel, based in Santa Clara, California, hopes the price for the robot kits will fall below $1 000 within five years.

Separately on Wednesday, entrepreneur Bill Gross announced plans for a 3D printer that would sell for $149, far less than devices that now typically sell for $1 000 or more. - Reuters