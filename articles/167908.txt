Microsoft’s Internet Explorer may not be best for exploring the Internet for the time being, as the company works to fix a security flaw that has left it vulnerable to hackers.

The U.S. Department of Homeland Security is recommending that Internet users “consider employing an alternative web browser” until the flaw is fixed, and Microsoft says it may not have a full security patch until May 13.

Computers that run on Windows XP are particularly susceptible to the IE security flaw because Microsoft has stopped offering security patches and software updates for the operating system.

The easiest solution is to use an alternative browser like Mozilla Firefox or Google Chrome.

For those who can’t avoid using IE, you can download Microsoft’s Enhanced Mitigation Experience Toolkit 4.1 to lessen the potential damage from malware, and also set the browser's security level to "high" under “Internet options.”

Microsoft offers more advice on boosting your computer's defences here, and the latest security advisory on the IE flaw is here.

In a statement posted Monday, the Department’s Computer Emergency Readiness Team (CERT) says that the flaw affects versions six through 11 of IE “and could lead to the complete compromise of an affected system.”

The company is blaming the problem on a coding flaw, and says it is actively working on a fix. Both CERT and Microsoft said they were aware of attacks targeting the flaw.

"The vulnerability may corrupt memory in a way that could allow an attacker to execute arbitrary code in the context of the current user within Internet Explorer," Microsoft said in an online statement on Saturday.

"An attacker could host a specially crafted website that is designed to exploit this vulnerability through Internet Explorer and then convince a user to view the website."