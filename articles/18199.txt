Topping tech headlines over the weekend, Microsoft announced the new Office 365 Personal, available this spring for $6.99 per month, or $69.99 a year.

The option is aimed at individual users, allowing one PC or Mac and one tablet to be connected to the service. The existing Office 365 Home Premium subscription, which lets you connect five computers, will still be available for $99.99 a year, but will drop the "Premium" title.

In other news, one of Northern California's most wanted is now in custody, thanks to Google. San Leandro resident Christopher Viatafa was reportedly Googling his own name when he stumbled upon the police site, where he was listed among the area's most wanted, in connection with a local shooting. According to local authorities, 27-year-old Viatafa shot a few rounds of his handgun at a private party in August; no one was hurt, but he has been detained on suspicion of assault with a deadly weapon.

Meanwhile, Sony's PlayStation 4 took the "best-selling game console" crown in the U.S. in February, only narrowly beating the Xbox One. According to The NPD Group, Microsoft's new console sold "over 90 percent of what the PS4 sold in terms of unit sales," but thanks to its higher price point, Redmond actually made more money. Overall sales of new hardware, software, and accessories totaled $887 million last month.

Be sure to check out PCMag Live from Friday in the video, as well as a few other stories making headlines in the links below.

Further Reading