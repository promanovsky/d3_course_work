The International Monetary Fund (IMF) has pledged a financial support of $14-$18 billion for Ukraine's economic reform program.

Nikolay Gueorguiev, IMF Mission Chief for Ukraine, said in a statement on Thursday that "The mission has reached a staff-level agreement with the authorities of Ukraine on an economic reform program that can be supported by a two-year Stand-By Arrangement (SBA) with the IMF."

The financial support from the broader international community that the program will unlock amounts to $27 billion over the next two years. Of this, assistance from the IMF will range between US$14-18 billion, with the precise amount to be determined once all bilateral and multilateral support is accounted for, he added.

Gueorguiev made it clear that the agreement is subject to approval by IMF Management and the Executive Board, which is expected to consider it in April, "following the authorities' adoption of a strong and comprehensive package of prior actions aiming to stabilize the and create conditions for sustained growth."

For comments and feedback contact: editorial@rttnews.com

Market Analysis