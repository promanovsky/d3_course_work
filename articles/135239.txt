Lindsay Lohan stopped by Bravo’s Watch What Happens Live on Thursday, April 18, to chat with Andy Cohen, and it was definitely worth watching. The actress, who currently appears in the docu-series Lindsay on the Oprah Winfrey Network, shot down rumors that she relapsed at Coachella, discussed her representation on the show, and commented on her reputation in the entertainment industry.

Things got even juicier when Lohan, 27, played a round of the show’s segment Plead the Fifth, where guests are asked three questions and given the option to skip one of them. She first answered Cohen’s question as to whether her 2008 relationship with female DJ Samantha Ronson was her first time “swimming in the lady pond.” Lohan paused and laughed before answering simply, “yes.”

The Mean Girls actress chose not to answer the second question, “pleading the fifth” when Cohen asked which actress that Lohan’s worked with is the meanest on set. “I will not talk negatively about any other actresses,” she said.

Cohen then turned to the recovering actress’ famed sex list, which made the rounds earlier this year. Written on a Scattegories sheet, the 36-item list included high-profile stars such as Justin Timberlake, Colin Farrell, Heath Ledger, James Franco, Adam Levine, and Zac Efron. “I can not confirm or deny it,” Lohan said about the famous names listed.

After realizing she had already used her “plead the fifth” out on the second question, Lohan elaborated. “You know what? I’m going to get serious for a second,” she said. “That was actually my fifth step in AA at Betty Ford [Drug and Alcohol Abuse Treatment Center]. Someone, when I was moving during the OWN show, must have taken a photo of it. That’s a really personal thing and it’s really unfortunate.”

“I talk about this on the last episode of the OWN show,” she added. “To be continued…” Watch Lohan chat with Cohen about her alleged partners above.