Brad Pitt was allegedly punched in the face by an Ukrainian reporter at the Maleficent premiere in Los Angeles yesterday (27 May).

The actor was not seriously hurt.

The reporter, named Vitalii Sediuk, has since been arrested on suspicion of battery.

The 25-year-old – who is known for pranking celebrities – was reportedly seen jumping over a barrier, hugging and then hitting the actor.

He was then wrestled to the floor by bodyguards before being handcuffed and led off by police.

Pitt was at the premiere to support his long-term partner, Angelina Jolie, who stars in the title role.

"This was an unfortunate and inappropriate incident that is being investigated by the Los Angeles Police Department," a spokesperson for the film company told the BBC.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The reporter was held in custody on Wednesday night (28 May) pending $20,000 (£12,000), a police spokesperson said.

Vitalii Sediuk following his arrest at the Los Angeles Maleficent premiere

"The officers who were handling him said it looked like [the attack] was intentional, but at this point, it remains to be determined," Sergeant Leonard Calderon told Reuters.

Sediuk has pulled a string of red carpet pranks over recent months; a few weeks ago he dived under the skirt of Ugly Betty actress America Ferrera at the Cannes Film Festival.

Vitalii Sediuk crawls under America Ferrera's dress at Cannes Film Festival on 17 May

Earlier this month, he attempted to gatecrash the Met Ball Gala – wearing a skimpy pink mankini and a gold duty. Security prevented him from making in inside the venue.

In January at the SAG Awards, he hugged Bradley Cooper round the waist, with his face pushed into the actor’s crotch. He tried a similar move on Leonardo DiCaprio at the Santa Barbara Film Festival in February.

Vitalii Sediuk grabs Bradley Cooper's waist at the SAG Awards in January 2014

At the 2013 Grammys, he was arrested after crashing the stage before Adele was about to collect her award from Pitbull and Jennifer Lopez.