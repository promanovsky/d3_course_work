The US economy added 288,000 jobs in June, according to the monthly estimate from the Bureau of Labor Statistics.

The unemployment rate fell from 6.3 percent to 6.1 percent.

BLS also had good news for previous months. The bureau had estimated the nation added 217,000 jobs in May. That number was revised upward to 224,000 in Thursday’s release. And its April estimate of 282,000 was adjusted upward to 304,000.

BLS says the number of long-term unemployed workers shrank by 293,000 in June and now stands at 3.1 million. The labor force participation rate, at 62.8 percent, stayed steady month-over-month. The U-6 unemployment figure, which accounts for those who are underemployed or who stopped looking for a job but would like to work, fell from 12.2 percent to 12.1 percent. However, the number of involuntary part-time workers increased last month from about 7.27 million to more than 7.5 million.

Advertisement

The professional services sector led the way in June, adding 67,000 jobs. Retail added 40,000, and food services added 33,000.

The release comes a day after payroll services company ADP released its monthly private survey, which also showed the country had a very strong June from a hiring perspective, with 281,000 jobs added.

The latest release from Massachusetts, based on May hiring, showed the state’s unemployment rate to be 5.6 percent. State-by-state jobs figures for June will be released later this month.