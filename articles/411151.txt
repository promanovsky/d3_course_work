James Franco is a jack-of-all-trades on his latest project, "Child of God."

The "Of Mice and Men" star wrote, directed and acted in the new drama, adapted from the 1973 novel by "No Country for Old Men" and "The Road" author Cormac McCarthy.

Actor Scott Haze plays protagonist Lester Ballard in this dramatic thriller, set in the 1960's Eastern Tennessee countryside. After being falsely accused of rape, Ballard begins to remove himself from society, eventually becoming a serial killer.

"What we are examining is human behavior, human desire and the extremes that we can go to just based on feelings and these certain needs to connect to another person," Franco recently told CBS News. The filmmaker pointed out that it was important to make Ballard's journey disturbing, yet still watchable.

"Not that we would ever condone what he does but that within this fictional framework we can go on a journey with him," Franco said.

Watch the video above to see Franco speak more about the film with his co-star Haze.

"Child of God" is now playing in theaters.