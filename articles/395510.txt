Freddie Prinze Jr. may accuse Keifer Sutherland of being hard to work with, but there's no denying it: Jack Bauer kicks ass.

Freddie Prinze Jr. may accuse Keifer Sutherland of being hard to work with, but there's no denying it: Jack Bauer kicks ass....

WORKING with Kiefer Sutherland made Freddie Prinze Jr. want to quit acting.

“I did 24, it was terrible. I hated every moment of it,” the 38-year-old actor told ABC News while promoting the animated series Star Wars Rebels at Comic-Con in the US this weekend.

“Kiefer was the most unprofessional dude in the world,” he continued. “That’s not me talking trash, I’d say it to his face, I think everyone that’s worked with him has said that.”

Prinze Jr., who rose to prominence during the late ’90s after roles in She’s All That and I Know What You Did Last Summer, appeared alongside Sutherland on the eighth season of 24.

While the actor, who’s married to Buffy the Vampire Slayer star Sarah Michelle Gellar, wasn’t at his peak in 2010, he was still getting regular work in movies and on TV, but the experience made him rethink his career.

“I just wanted to quit the business after that. So, I just sort of stopped,” he said.

In case it wasn’t clear how much Prinze Jr. hated working with Sutherland, the actor threw in one last dig at the Emmy winner.

“I went and worked for Vince McMahon at the WWE, for Christ’s sake, and it was a crazier job than working with Kiefer,” he said. “But, at least he was cool and tall. I didn’t have to take my shoes off to do scenes with him, which they made me do. Just put the guy on an apple box or don’t hire me next time. You know I’m 6 feet and he’s 5’4.”

The actor’s stint on 24 was during its final season, although the series recently returned to Fox as a limited series.

A rep for Sutherland released a statement about Prinze Jr’s claims, saying, “Kiefer worked with Freddie Prinze, Jr. more than 5 years ago, and this is the first he has heard of Freddie‘s grievances”.

“Kiefer enjoyed working with Freddie and wishes him the best.”

An unnamed source from Fox, the studio behind 24, also told TMZ that he was surprised by the actor’s issues with Kiefer.

“It’s so out of left field, five years later. We wouldn’t have done another 24 if Kiefer were anything like Freddie described.”

This article originally appeared in the New York Post.