Having become something of a succès d’ordures, “Sharknado,” the 2013 TV movie about a tornado full of sharks (to simplify somewhat), has inevitably birthed a sequel. Here it comes now.

Premiering Wednesday on Syfy, it is called “Sharknado 2: The Second One,” to let you know, as does most every (super-intense) expression, (seriously read) line and too-big-for-this cameo appearance, that everyone involved is in on the joke. Even the CGI sharks, who seem strangely incidental this time, seem to be laughing behind their fins.

Watching the movie? Join us Thursday at noon to discuss whether it had teeth.

It’s hard to say whether such knowingness makes for an improvement on the original, which, before the culture decided differently, was just another in a series of Syfy-produced C-pictures made to recall a time before sci-fi came with a big budget or not at all. It depends on how you define “improvement,” really.

Advertisement

To call either film clumsy would be to miss the fact that a degree of terribleness was built into the compact, and to deny that there is something here that caught the imagination of a nation. America tweeted “Sharknado” into a bona fide phenomenon -- the cinematic version of a crowd-funded $50,000 potato salad.

Indeed, better ingredients would likely spoil the soup: An expensively rendered tornado full of live sharks -- a Michael Bay “Sharknado” -- would be worse than one that’s merely pretty good for the price. And though “Sharknado 2" (once again written by Thunder Levin and directed by Anthony C. Ferrante) is to some extent a joke about the success of “Sharknado,” it is not exactly the parody that greater resources would inevitably make it. Set now in New York, instead of Los Angeles, it is just more of the insane same -- insaner, if anything -- in a slightly nicer suit. As with John Heard, the ringer in the first installment, the picture manages to bring everyone in it down to its level. Or close enough.

Anyway, according to my notes, it goes something like this. (Order approximate. Vague spoilers ahead.)

Airplane. Rain. Cameo. Returning star Tara Reid, accompanying surfer-warrior hero Ian Ziering, suffering a sort of PTSD -- Post-Traumatic Shark Disorder. (I may not be the only reviewer to make that joke. But I hope I will be.) Grumpy retrospective exposition. “I got eaten by a shark; how much fun do you think that was?” Cameo. “Twilight Zone” reference. Woman in lavatory marked for death. Sharks on a plane. Actually surprising disfigurement. Impossible piloting.

Advertisement

Times Square. Mark McGrath (the Sugar Ray singer) -- not a cameo, a role. Hospital. Cameo. (Or possibly two cameos: not sure who second person is, but looks “famous.”) “Today Show.” (NBC owns Syfy, so there you go.) Cameo. Cameo. “Southerly shark wind.” Prediction of unseasonal snowstorm to account for winter weather in a story set in the summer. Ferry. Statue of Liberty. Minor characters marked for death. “The shark kept chasing me; it’s like he knew who I was.”

Mets game. Lengthy cameo. Vivica A. Fox, old flame. Shark rain. “If anyone’s going to play me in the movie it’s going to be me.” Shark rain. Panic. Subway. More panic. Sewer (alligator joke). Sharks. Emotion. Shark. Shark. Wrestling a shark. “I hate the subway.” Tiny shark.

Ferry. Cameo. Coincidence. Chainsaw. “Duct tape.” Plan: “It worked in L.A.” Sharks. Subway (sandwich) product placement. Cameo. Statue of Liberty (partial). Driving. Running. Water spouts.

Back to “Today”: “These sharknados are actually a rare anomaly.” New York pizza. “Fuggedaboudit!” Napalm-brand lighter fluid. Bikes. Sharks. Running. Sharks. Flooding. Hope. Regret. Running. Phony science. Gadgets. Sword. Fear. Fiery sharks. Jammed door. Cameo. Pep talk: “I know you’re scared. I’m scared, too. They’re sharks, they’re scary.” Chainsaw. New Yorkers with farm tools. Skyscraper. “This building’s going to fill up like a fishtank.” Ridiculousness. Super-ridiculousness. Mega-ridiculousness. Meta-ridiculousness. Love. Fireworks.

Advertisement

robert.lloyd@latimes.com

Robert Lloyd supports the sharks on Twitter @LATimesTVLloyd