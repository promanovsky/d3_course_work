Game of Thrones served up one of its most jaw-dropping episodes last night.

We've laughed at the internet's reactions. We've all discussed our own reactions. And now, thanks to the guys at worldwideinterweb, we now know how the world of TV and movies reacted to the Purple Wedding.

Watch movie and TV characters react to Game of Thrones (WARNING NSFW and spoilers):



This content is imported from Third party. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

The Ghostbusters, the South Park gang and Nelson from The Simpsons are just a few of the famous characters who have been mashed together with the closing scene from this week's episode 'The Lion and the Rose'.

At the close of the episode King Joffrey (Jack Gleeson) was poisoned on his own wedding day by an as-yet-unknown killer.

Game of Thrones airs on Sunday nights on HBO in the US and on Monday nights in the UK on Sky Atlantic.

How should Game of Thrones spoilers be punished? The cast reveal all:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io