I've been dieting the last six months and lost two stone. I'm now eight stone and 5ft 3in. I'm happy at this weight. The problem is, I still have a pooch belly. I run 10km, three to four times a week, and it takes me one hour.

I then do three sets of 40 sit-ups, front and side planks, five sets each, and then dumbbell exercises.I do feel toned under the fat around my belly but I've had two children, 3 and 10 months, maybe that has stretched things.

Karl says: Firstly well done on your weight loss, that's serious hard work to lose that weight. It can be hard to know without information on how you lost it in terms of food but I still think I can give you some recommendations that will help.

Firstly, sit-ups etc won't have any effect on your pooch belly. They simply strengthen the muscles below, the layer of skin or fat on top isn't affected at all.

You're basically looking to reduce your body fat and to do this you need to ensure that you have a full body exercise routine, working all the muscle groups all around the body.

By doing this you will increase the lean tissue all around the body, increase your metabolic rate and burn more fat all around the body just to keep the lean tissue there.

Secondly you need to ensure that you are eating a clean diet, with all the food groups and reduce your sugar content. Link this with a protein intake of 1.5g per kilo body weight, so around 75 grams of protein per day.

Thirdly you need to shake up your runs and work harder in them.

Aim to run a faster 10k, maybe doing intervals and more races to ensure that you work harder in them.

These recommendations should make a big difference for you.

As you have got quite fit, now it's time to take it up a notch!

Any motivation tips for staying on the paleo diet?

Q: I've been on the paleo diet on and off for a while. I lose weight, feel great but end up putting back on all the weight and more. I was wondering had you any tips on how to stay motivated or ways to stay with the paleo?

Karl says: Any diet that you find hard to sustain will generally mean that you yoyo in terms of weight and inches, I'm afraid.

Especially if it is quite different to what you normally eat as you are forcing yourself to give up your favourite foods etc and eventually you binge on those foods, putting the weight back on and more which can be so frustrating.

So my first piece of advice would be to ask, is paleo the diet type for you? Why are you choosing it? If like so many people you like carbs then why not look at a low glycemic diet? Which in reality isn't all that different, there are just more carbs in there, which can make it easier in the long-run.

If you are determined to stick to paleo then there are two recommendations I would have for you.

The first would be to plan better, and stock up on all the foods that you are meant to be eating. This makes diets a lot easier.

The second is to allocate a treat day each week, where you can have those foods that you are cutting out. This will help create a much more balanced approach and make it easier to maintain in the long term.

Health & Living