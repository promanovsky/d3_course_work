The creator of Flappy Bird, the extremely popular mobile game that was surprisingly and suddenly removed from app stores last month, said he is considering re-releasing the game.

Speaking with Rolling Stone, Vietnamese independent developer Dong Nguyen said he is now considering reinstating Flappy Bird into the Apple App Store and Google Play after removing the game in February.

Flappy Bird was a simple game that users played by tapping their screen. The point was to guide a bird through deadly pipes to attain the highest score possible. Though simple to learn, the game is very difficult to play. It became the No. 1 game on both mobile app stores and was raking in $50,000 daily in ad revenue before Nguyen decided to remove the game.

Nguyen, 28, told the magazine his decision came after receiving countless messages from users telling him the game was negatively affecting their lives, with many breaking their phones and losing their jobs over Flappy Bird.

Advertisement

VIDEO: Unboxing the Samsung Galaxy Note Pro 12.2

Allegations he had ripped off Nintendo and paparazzi-like attention at his parents’ home, where he lived, added to his stress and made it impossible for him to sleep or focus, he told Rolling Stone.

“I couldn’t be too happy,” he said, according to the report. “I don’t know why.”

Since taking down the game, Nguyen said he has continued to make tens of thousands of dollars from the millions of users who downloaded Flappy Bird before it was removed.

Advertisement

Nguyen said he has also been busy developing new games. So far he has three upcoming games and plans to release one this month.

If he decides to re-release Flappy Bird, Nguyen said the game will be the same as before but will include a new warning: “Please take a break.”



ALSO:

Expect better Apple Maps navigation app in iOS 8, report says

Advertisement

IPhone 6 concept video: Is this what we should expect from Apple?

Apple releases iOS 7.1, includes CarPlay, better iPhone 4 performance