By Kim Glovas

PHILADELPHIA (CBS) — The Organ Procurement Transplantation Network has just voted to allow the exception to the Under 12 rule to remain in effect and not expire on July 1.

The temporary exception, enacted last year, allowed 11-year-old Sarah Murnaghan of Newtown Square to have a double lung transplant operation with adult lungs. Before that, lungs were only allowed to be used in adults and not pediatric cases. (See Previous Story)

The Murnaghan family is expected to have a news conference at their home shortly.

Sarah Murnaghan suffers from cystic fibrosis and doctors said in 2013, that she needed a double lung transplant or she would most likely die.

Organ Procurement and Transplantation Network (OPTN) responded by making a temporary, one-year appeals process for children under the age of 12 whose doctors felt that they could benefit from adult lungs.

The Murnaghan family released the following statement regarding the OPTN vote:

“Our family is thrilled that OPTN (Organ Procurement and Transplantation Network) has decided to make the temporary exception that allowed Sarah to receive adult lungs a permanent rule. The proposal was passed by 38 out of 40 OPTN board members.

From the beginning of this process, this fight has always been not just about Sarah – because there was a very good chance it would have been too late for her – but for every family in our situation. Today’s vote is important for two reasons: More children will be fortunate enough to receive life-saving lung transplants, and the medical community has determined this is the right step to take. There was a problem that was affecting a small group of transplant candidates and an action was appropriate.

There was a lot of criticism of us using the legal system to get lungs for Sarah, but that was the absolute last resort after we followed every other process and had been unsuccessful. We believed making lungs from donors 12 and older available to children under 12 who are good candidates to receive them was the right thing to do. We very much appreciate that the medical community agrees with that and has determined a responsible way to expand that access.

For us, Sarah receiving adult lungs means she is now breathing on her own after three years of being tethered to machines. We have learned to let Sarah’s progress dictate her readiness for each step and we continue to take it one day at a time. Sarah is still getting physical rehabilitation and we have work in front of us, but we are blessed there has been NO rejection. Her new lungs look beautiful and we are going looking forward to her living life to the fullest – including returning to school during the 2014-2015 school year.

And we never forget Sarah received this incredible gift of life from her heroes, her organ donors. We thank them and their families for their sacrifice.”

You may also be interested in these stories:

[display-posts category=”news” wrapper=”ul” posts_per_page=”5″]