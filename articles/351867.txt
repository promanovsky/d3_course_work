Katherine Heigl has revealed that she "couldn't say no" to doing romantic comedies.

The Hollywood actress, known for her roles in rom-coms such as 27 Dresses and Knocked Up, has admitted that she "stopped challenging" herself in her choice of movie roles.

Speaking to Marie Claire, she said: "I had an amazing time [doing rom-coms]. I love romantic comedies. But maybe I hit it a little too hard. I couldn't say no.



"I stopped challenging myself. It became a bit by rote and, as a creative person, that can wear you down.



"That was part of why I took that time off, to ask myself, 'What do I want? What am I looking for?' and shut down all the noise."

The actress, who took a break from Hollywood to spend time with husband Josh Kelley and daughters Naleigh and Adelaide on a ranch in Utah, admitted that she considered throwing in the towel and quitting acting.

She said: "Oh yeah, I had a moment where, I don't know, I was thinking I'd maybe open a knitting store, get my money out of retirement accounts and live off that, live off the land. I had my moment where it all seemed so complicated and all I wanted to do was simplify."

Heigl, who is set to star in new NBC show State Of Affairs, said that she's excited about the next step in her career, saying: "There's a part of me that's a Hollywood animal as well. I can't wait to get into the writer's room and see how we do this. I feel like I'm finally rolling into the next phase of my adulthood."