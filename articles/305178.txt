Controversial American Apparel CEO is fired: Founder who faced NINE sexual harassment claims after turning T-shirt business into multi-million dollar empire is sacked for 'misconduct'

Dov Charney, 45, will be replaced as chairman of American Apparel's board and terminated as president and CEO



Charney, a native of Canada, started the company in 1991 as a wholesale T-shirt business and grew it into multimillion-dollar brand

Retailer has become notorious for risque ad campaigns featuring female workers and porn stars



The embattled founder and CEO of American Apparel has been ousted by the board of directors after spending years fighting sexual harassment allegations.

The company's board voted unanimously Wednesday to replace Dov Charney as chairman and 'terminate his employment as president and CEO for cause,' a press release stated.



Charney, 45, a native of Montreal, Canada, who first started American Apparel in 1991 as a wholesale business peddling T-shirts, had faced repeated accusations of sexual misconduct made by female employees, but all the cases have been either dismissed or settled.

Scroll down for video

Ousted millionaire: Dov Charney, 45, has been fired as president and CEO of American Apparel for cause amid allegations of misconduct

Mad genius? Dov Charney, 45, a Canadian national, started American Apparel in 1991 as a humble wholesale T-shirt manufacturer



In 2011, four former American Apparel workers filed a lawsuit against Charney, but at the time the company came to his defense, claiming that the plaintiffs conspired to 'shake down' the fashion tycoon with an estimated net worth of $20million.



In 2012, Mr Charney once again came under fire when Michael Bumblis, a former store manager in Malibu, California, accused the CEO of rubbing dirt in his face and choking him.

Charney also allegedly called the man 'a wannabe Jew' and used a derogatory term for homosexuals. Bumblis was later fired from his job.

American Apparel once again backed Charney, insisting that Bumblis' termination was in line with company policy.



All the claims were either settled or dismissed.



Under the terms of Charney's contract, he will be given 30 days before his termination is finalized. It is unclear what the alleged misconduct was.

In the meantime, board members Allan Mayer and David Danziger have been appointed as co-chairmen.



‘We take no joy in this, but the Board felt it was the right thing to do,’ Mayer said in the statement obtained by Los Angeles Times.



‘Dov Charney created American Apparel, but the Company has grown much larger than any one individual and we are confident that its greatest days are still ahead.’

Big business: American Apparel currently employs 10,000 employees and operates 249 retail stores in 20 countries

Courting controversy: The retailer has repeatedly come under fire for using young models dressed in sexy clothes to promote the brand

The board named chief financial officer John Luttrell as its interim CEO.



Mayer, who has served on the board since the company went public in 2007, said the decision to fire Charney was prompted by an ongoing investigation into alleged misconduct.



A source told Chicago Tribune Wednesday night that Charney was blindsided by the board's decision to dismiss him and will likely 'fight like hell' to try and regain control of his company.

As an edgy fashion brand catering to teens and 20-somethings, American Apparel has drawn sharp criticism by using half-naked young women to promote its products in ads.

To this day, the company’s Tumblr account is filled with highly sexualized images of women frolicking in transparent tops, mixed in with Japanese Manga characters and pictures of pets.







Explosive allegations: In 2011, Irene Morales (left) sued Dov Charney (right) accusing him of treated her as a 'sex slave'



Weakened: Morales's claims were later undermined by a trove of explicit pictures and messages she allegedly sent her boss

Legal victory: Two years ago, a New York judge tossed Morales' $260million lawsuit against Charney

Over the past decade, at least nine women have filed lawsuits against Charney accusing him of sexual harassment.

In March 2012, the millionaire CEO scored a major victory when a judge in Brooklyn tossed a $260million suit filed by 21-year-old Irene Morales, who claimed that Charney held her as a sex slave in his Manhattan apartment and threatened to fire her if she refused to perform sex acts on him.



Morales' claims had been severely undermined by explicit pictures and messages she sent to her boss, which were made public during the court case.



Other cases against Charney followed roughly the same pattern, with some being thrown out in court and others being settled.



Besides Charney's recurring legal problems, the brand has faced other challenges mainly stemming from its controversial approach to advertisement.

The international retailer’s provocative campaigns have been known to feature female employees and even professional porn stars posing suggestively in little more than sexy lingerie.



Charney, notorious for his hands-on management style, had photographed many of the ads himself.



In May 2007, Oscar-winning director Woody Allen tried to sue American Apparel for $10million after the retailer used stills from the filmmaker's 1977 classic Annie Hall on billboards.

The posters were removed eventually and Charney settled the case for $5million.



Master of his domain: Dov Charney, then-chairman and chief executive officer of American Apparel Inc., stands for a portrait in a company retail store in New York in 2010

Unexpected dismissal: Charney reportedly did not see his termination coming and will fight to regain control of American Apparel

American Apparel has built a large following thanks to its commitment to manufacturing its clothing in the U.S. instead of outsourcing the work to other countries.

The company also prides itself on paying employees fair wages and providing health insurance, which is uncommon in the garment industry.



As of May 31, 2014, American Apparel had approximately 10,000 employees and operated 249 retail stores in 20 countries . But the retailer is facing an uncertain future after posting a net loss of $106.3million last year.