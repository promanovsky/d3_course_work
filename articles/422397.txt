"Recording a memory is not like playing a tape recorder, it's a creative process," Susumu Tonegawa, director of the RIKEN-MIT Center for Neural Circuit Genetics at MIT and senior author of the paper, said in a Nature news conference. Psychotherapists already attempt to do this in patients with depression, Tonegawa explained, by trying to manipulate the emotional context of troubling memories. But whether or not there was a sound neurological basis for such techniques remained unclear until now.

The researchers used genetically engineered mice who expressed a light-sensitive protein, allowing the scientists to activate different neurons by targeting them with a laser. They exposed half of them to a positive stimuli (interaction with a female mouse) and half to a negative one (small electric shocks). This activated both the neurons that form the structure of a memory, which are found in the hippocampus, and the neurons that determine the emotional value of a memory, in the amygdala.

AD

AD

Then the mice were placed in a box with two sides that the mice could move freely between. When the mice moved to one particular end of the box, a light would shine down on them — activating the neurons that had been active during their conditioning. So for the mice who'd been shocked, the "target" end of the box meant an activation of the fearful memory. For the mice who'd spent time with a female, the same target end meant and activation of the pleasurable memory. Sure enough, the shocked mice avoided the target, while the others spent more time there than the side without a laser light.

This simply showed the researchers that they were activating the right regions of the brain to remind the mice of their emotional conditioning. The next step was to swap them around. "The assumption here is that memories are formed between neurons that are active at the same time," Roger Redondo, a Howard Hughes Medical Institute postdoc at MIT and co-author of the study, said in the news conference. "So if this is right, we should be forcing the neurons associated with fear to begin with to link up with the new neurons expressing the pleasure of spending time with a female."

Male mice that had first received shocks had the same memory cells activated while they spent time with a female mice, and vice versa. Now, the very same experiment in the box had the opposite result: The mice who'd originally been remembering something pleasant when the light shone down on them now seemed to be experiencing fear, and avoided the target area. "These animals don't look confused," Redondo said. "These animals actively choose to go to one side." In other words, the neurons that had once conjured up fearful memories had been switched to pleasant ones.

AD

AD

It's not entirely clear if or how this could be translated to humans. First, researchers will have to learn how to change the emotional context of a memory without requiring the subject to actively participate in a conditioning exercise. But the basic road map should be the same in humans as it is in mice.

“There may be a lengthy process before this tech can be translated into human application," Redondo told the news media, "but the circuits seem to be very similar between humans and mice when it comes to these memory associations and emotional memories." And eventually, he said, an understanding of this circuitry could lead to new pharmaceutical, therapeutic, and other treatments for things like depression and PTSD. "What we provide here in this paper and this study is a different avenue," Redondo said.