Radio host Rush Limbaugh is deeply concerned about CBS's decision to install Stephen Colbert as David Letterman's replacement.

The way Rush sees it: "CBS has just declared war on the heartland of America."

Say what now?

On yesterday's edition of the syndicated radio program The Rush Limbaugh Show, the famously right-wing commentator offered his views on the news that Colbert – who portrays a dense right-wing personality on The Colbert Report – would take over the Late Show from David Letterman when the host retires next year.

In what has become a recurring theme on his radio program, Limbaugh said the Colbert appointment is yet another example of liberal media turning its back to traditional America. In other words, it's the end of days all over again.

Said Limbaugh: "No longer is comedy going to be a covert assault on traditional American values, conservatism – now it's just going to be wide out in the open. What this hire means is a redefinition of what is funny and a redefinition of what is comedy."

According to Limbaugh, CBS is "blowing up the 11:30 format … they hired a partisan, so-called comedian, to run a comedy show."

As previously announced, Colbert will be retiring his fake conservative persona in his new Late Show duties.

"I won't be doing the new show in character, so we'll all get to find out how much of him was me," said Colbert in the CBS press release.

But clearly Limbaugh is rankled by Colbert's past work and yearns for the good old days. In the same show, he said: "The world is changing. People don't want the kind of comedy that [Johnny] Carson gave us, or even Letterman."

That's correct, Rush. And most viewers who loved Carson are now too old to stay up to 11:30 p.m. Or dead.

And can you imagine Limbaugh's reaction if the Late Show post had gone to a proud gay woman, like, say, Ellen DeGeneres? His head would have exploded.

On yesterday's edition of The Ellen DeGeneres Show, the host congratulated Colbert and said that she wasn't much interested in the Late Show job anyway.

"I heard a rumour that one of the people being considered to take over the Late Show is me," said DeGeneres. "I am flattered, but I really love doing my daytime show. Nighttime is very different, there's a lot of political humour, which I don't do."

But keep in mind, Ellen is a two-time Oscars host.

In her typical game-for-anything way, DeGeneres gave her audience a peek at what her version of hosting the Late Show might have looked like, which began with jazzy intro music and her getting tangled in the curtains.

"Thank you, ladies and gentleman, I haven't had that much trouble coming out since 1996," said the openly gay comedian.

Continued DeGeneres: "There's so much happening in politics right now… Europe, Asia, Um, Hillary Clinton, am I right?"

DeGeneres also rattled off a "Top 11" list that was really just a ranking of her favourite pets.

Following her fake test run, DeGeneres announced, "I'll stick to daytime for now."

Then she added, "I would like to say to David Letterman, you are an incredibly brilliant and clever man. I've been a fan, I've loved watching you. You've made us happy for over 30 years."

And now it's Colbert's turn. Live with it, Rush.