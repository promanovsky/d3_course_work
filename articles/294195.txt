Board of Hillshire Brands Co. (HSH), the maker of Jimmy Dean sausages and Ball Park hot dogs, Monday said it has withdrawn its recommendation of the pending acquisition of Pinnacle Foods Inc. (PF), in the light of Tyson Foods Inc.'s (TSN) hefty offer to buy Hillshire.

"The Hillshire Brands board now recommends a vote against the Pinnacle Foods transaction," the company said in a statement.

Last Monday, Tyson sweetened its bid for Hillshire Brands to $63 per share, valuing the offer at about $8.55 billion, including debt.

In early May, Hillshire agreed to buy Pinnacle Foods, the maker of Birds Eye frozen foods, Duncan Hines baking mixes and frostings, and Vlasic shelf-stable pickles, in a cash and stock transaction with a total enterprise value of $6.6 billion.

However, late May, Pilgrim's Pride (PPC), a unit of Brazilian meat giant JBS SA, made an unsolicited bid to buy Hillshire for $45 a share in cash. Meat producer Tyson Foods soon proposed to acquire Hillshire for $50 per share, and on June 1, Pilgrim's raised its offer to $55 per share. However, after Tyson sweetened its bid to $63 per share Pilgrim's Pride withdrew its offer.

According to the merger deal, "if Pinnacle Foods terminates the merger agreement prior to a vote of Hillshire Brands stockholders, Pinnacle Foods may be entitled to receive a termination fee in the amount of $163 million." However, reports suggest that Pinnacle might try to get more from Hillshire as break-up fee, otherwise will opt for a shareholder vote, which would delay the Hillshire and Tyson deal for months.

"We believe our offer to acquire Hillshire Brands for $63 per share in cash is a superior proposal for Hillshire Brands shareholders," said Donnie Smith, president and CEO of Tyson Foods.

"We hope Pinnacle Foods will promptly accept the termination fee and not delay the ability of Hillshire Brands' shareholders to benefit from Tyson Foods' superior offer," Smith added.

However, if Pinnacle Foods ask for a shareholder vote and Hillshire shareholders reject the deal, then Pinnacle could only get a termination fee of $43 million, and an additional $120 million under certain circumstances.

PF is currently trading at $33.10, up $0.06 or 0.17%, on the NYSE. HSH is currently trading at $61.98, up $0.16 or 0.26%.

For comments and feedback contact: editorial@rttnews.com

Business News