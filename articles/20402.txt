This Aug. 26, 2009 photo shows Dr. Pedro Jose Greer, right, preparing to do a colonoscopy at Mercy Hospital in Miami. Greer has made a career of offering treatment without regard to patients' ability to pay, and it's one reason he received a 2009 Presidential Medal of Freedom. (AP Photo/Lynne Sladky)

By now, many people are aware that symptoms can be difficult to spot In the early stages of colorectal cancer. And because the symptoms don't often become apparent until the disease has progressed beyond the initial stages, regular screenings have always been recommended.

And now it appears that regular screenings are paying off -- in a big way.

New research shows colon cancer incidence rates plunging by 30 percent in the U.S. in the last 10 years among adults 50 and older thanks to the widespread uptake of colonoscopy, with the largest decrease in people over age 65. Colonoscopy use has almost tripled among adults ages 50 to 75, from 19 percent in 2000 to 55 percent in 2010.

Like incidence, mortality rates also have dropped rapidly over the past decade. From 2001 to 2010, rates fell by about 3 percent a year in both men and women, compared with declines of about 2 percent a year during the 1990s.

"These continuing drops in incidence and mortality show the lifesaving potential of colon cancer screening; a potential that an estimated 23 million Americans between ages 50 and 75 are not benefiting from because they are not up to date on screening," said Dr. Richard C. Wender, American Cancer Society chief cancer control officer, in a release. "Sustaining this hopeful trend will require concrete efforts to make sure all patients, particularly those who are economically disenfranchised, have access to screening and to the best care available."

The American Cancer Society is on a mission to use this new data to sway more people to undergo colonoscopies, with an eye towards increasing screening rates to 80 percent by 2018.

Excluding skin cancer, colorectal cancer, commonly called colon cancer, is the third most common cancer and the third leading cause of cancer death in men and women in the United States. Its slow growth from precancerous polyp to invasive cancer provides a rare opportunity to prevent cancer through the detection and removal of precancerous growths. Screening also allows early detection of cancer, when treatment is more successful. As a result, screening reduces colorectal cancer mortality both by decreasing the incidence of disease and by increasing the likelihood of survival.

The new findings come from Colorectal Cancer Statistics, 2014, published in the March/April issue of CA: A Cancer Journal for Clinicians.

A previous study from 2012 also underscored the value of screenings, finding that colonoscopies in which precancerous polyps are removed can slash the risk of dying from colon cancer by 53 percent.