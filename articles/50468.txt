Heads up! We are minutes away from the release of the third and final estimate of fourth-quarter 2013 GDP, due out from the U.S. Bureau of Labor Statistics at 8:30 AM ET.

The previous estimate, published by the BLS a month ago, saw GDP rise 2.4% at an annualized pace and personal consumption expand 2.6% annualized. These numbers marked a slowdown in GDP growth from Q3's 4.1% annualized rate but an acceleration in personal consumption growth from 2.0% annualized.

Advertisement

Given new economic data released since the publication of the last BLS estimate for GDP, market economists predict the final estimate out today will reveal GDP and personal consumption growth of 2.7% annualized in Q4.