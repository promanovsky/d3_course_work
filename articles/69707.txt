(Spoiler alert: Don’t read this if you haven’t seen Sunday’s “Walking Dead” Season 4 finale.)

First things first: These Terminus people are cannibals, right?

I was sure of it last week because of the woman grilling meat. And I’m even more sure after the shots Sunday of the Terminus sharpshooters bullet-herding Rick and friends to the rail car, past butchered human cadavers. This was the most legitimately scary “Walking Dead” in quite a while. (Also: Horribly icky Holocaust overtones with the rail car.)

Also read: ‘Walking Dead’ Season 4 Death Toll: 34 and Counting (Photos)

This season, “The Walking Dead” stopped gently hinting at the notion that the living are becoming just as zombie-like as the dead, and started stabbing it into our heads. First we had Lizzie noting that the only difference between humans and zombies, really, is that zombies are humans unlucky enough to die. This episode, Rick bit another man’s jugular — and I’m sure everyone in the audience was on his side.

And now we see that some people are perfectly willing to eat other people.

Where again is the line between us and them?

Also read: ‘Walking Dead’ Shocker: Did They Have to Do That?

This was the most uneven season of TV’s top-rated drama. I noted midway through that the “Walking Dead” is in “Lost” mode — a ragtag group of survivors wander around getting separated and reunited, and sometimes it feels like the writers are stalling as they try to decide where to go next.

But this was a good season finale. It sadly and successfully compared the optimism of the prison era to the desperation of Terminus. (One sign you’re watching a grim show: The happiest moments occur in a prison.) But more importantly, it was truly scary.

Also read: ‘Walking Dead’ Is in ‘Lost’ Mode

What made the cannibals so frightening was their apparent decency, even kindness. They were as gentle with Rick, Carl, Michonne and Daryl as stockyard workers might be with the cows. No need to spook the cattle: Things here only go in one direction.

Gareth (a wonderfully nonthreatening name for a maneater) seemed more like a dot com CEO than a sociopath: He was effortlessly cool on the surface, and what lurked beneath seemed less like malice than condescension.

We get the sense from their calm and efficiency that Team Terminus is untroubled by its actions: They have a process, they believe in their process, and the only stress is in making it work better. They don’t have moral dilemmas, they have engineering dilemmas.

That’s pure evil. Evil so evil it doesn’t even consider morality any more.

The question for Season 5 isn’t whether the Terminus gang “screwed with the wrong people.” It’s whether there are any of the right people left.