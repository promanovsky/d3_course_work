Drinking too much in middle age can lead to memory loss in later life, a study has found. Picture Posed

Drinking too much in middle age can lead to memory loss in later life, a study has found.

Scientists questioned 6,542 American middle-adults about their past alcohol consumption and assessed their mental abilities over a period of eight years.

They found that a history of problem drinking – described as alcohol use disorders (AUDs) – more than doubled the risk of developing severe memory impairment.

Lead researcher Dr Iain Lang, from the University of Exeter Medical School, said: "We already know there is an association between dementia risk and levels of current alcohol consumption – that understanding is based on asking older people how much they drink and then observing whether they develop problems."

He added: "What we did here is investigate the relatively unknown association between having a drinking problem at any point in life and experiencing problems with memory later in life."

Belfast Telegraph