The board of directors of Air Products & Chemicals, Inc. (NYSE:APD) appointed Seifi Ghasemi to serve as chairman, president and CEO of the company effective July 1. He will replace John E. McGlade who will be retiring from the positions on June 30.

Ghasemi currently serves as a director of the company, and he is also the chairman and CEO of Rockwood Holdings, Inc. (NYSE:ROC).

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

The shares of Air Products & Chemicals, Inc. (NYSE:APD) surged more than 6% to $129.70 per share after the announcement of Ghasemi’s appointment.

Accomplished business leader

Ghasemi joined the board of directors of Air Products & Chemicals, Inc. (NYSE:APD) in September 2013. The board of directors of the company describes him as an accomplished global business leader with a combination of financial and operational expertise and significant experience in overseeing an international portfolio.

Ghasemi is an expert in industrial gases, specialty chemicals and advanced materials industries. He has been the chairman and CEO of Rockwood Holdings, Inc. (NYSE:ROC) since 2001. Rockwood Holdings is a leader in inorganic specialty chemicals and advanced materials. He also assumed several leadership positions in global industrial companies including chairman and CEO of GKN Sinter Metals, and different senior positions at The BOC Group, which is now a subsidiary of Linde AG (ETR:LIN).

In a statement, Evert Henkes, presiding director of the board of Air Products & Chemicals, Inc. (NYSE:APD) said, “Seifi is a highly respected and dynamic leader with a strong track record of delivering superior shareholder returns and financial performance. Air Products has benefited greatly from his insights and guidance on the board, and we are pleased that his significant skills will continue to benefit Air Products’ employees, shareholders, customers and communities as he works to accelerate the momentum in our business.”

Role in Air Products & Chemicals a tremendous privilege

Commenting on his new role as leader of Air Products & Chemicals, Inc. (NYSE:APD), Ghasemi said the opportunity given to him to advance the company and improve returns is a tremendous privilege.

According to him, “Air Products is a unique, global business with leading market positions in key regions and a well-balanced geographic footprint. I am excited by the many significant growth opportunities that lie ahead, driven by energy, environmental and emerging markets. I look forward to working with all stakeholders to take Air Products forward.”