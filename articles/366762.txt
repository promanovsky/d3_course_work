Hookah use is on the rise among high school seniors, according to research conducted at New York University. The study, published in the journal Pediatrics, surveyed 5,540 students, finding that one in five reported using the tobacco pipe.

"What we find most interesting is that students of higher socioeconomic status appear to be more likely to use hookah," said Joseph J. Palamar, the study's co-author.

"Surprisingly, students with more educated parents or higher personal income are at high risk for use. We also found that hookah use is more common in cities, especially big cities. So hookah use is much different from cigarette use, which is more common in non-urban areas."

One teen, speaking with CNN, said he prefers the hookah to cigarettes because the "smoke is pleasant" and because it is "cool."

The FDA is currently attempting to push through new laws which would regulate the manufacturing and marketing of hookah products.

For comments and feedback contact: editorial@rttnews.com

Health News