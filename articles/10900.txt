Kylie Jenner is all about the MIA! Kris and Bruce Jenner‘s youngest daughter, 16, went on an Instagram spree on Wednesday, March 12, posting photos of herself in a black cut-out bikini while staying at the Versace Mansion in Miami, Fla., with the rest of the Kardashian family.

“Good morning,” Jenner wrote alongside one pic of herself, snapped from behind as she stood beside the famed South Beach estate’s mosaic floor. The teen also shared a bikini shot of herself standing beside a nude fresco painting of a man, with the caption, “Oh haaayyy.”

Jenner joins older sisters Kim and Khloe Kardashian in Miami for the grand opening of the new Dash boutique as the family continues filming Keeping Up With the Kardashians.

Like Jenner, Kim and Khloe have also been sharing their Miami experiences via social media, with topics ranging from their stay at the Versace Mansion, to what they’re wearing. Khloe even posted several videos Wednesday afternoon of fans reacting wildly to her in-store appearance at the grand opening of Dash on Collins Avenue.