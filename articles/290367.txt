Vodafone has also entered into an agreement with certain minority shareholders of Cobra's Telematics.

British telecom major Vodafone today said it will acquire Italian security and telematics firm Cobra Automotive Technologies for euro 145 million to expand its machine-to-machine business.

The machine-to-machine (M2M) technology helps people control consumer appliances, vehicles or various other products through their mobile phones or computers from a remote location using SIM card or Internet connectivity.

"The combination of Vodafone and Cobra will create a new global provider of connected car services. We plan to invest in the business to offer our automotive and insurance customers a full range of telematics services," Vodafone's Director of M2M, Erik Brenneis said in a statement.

Vodafone has given offer of euro 1.49 per share in cash to Cobra Automotive Technologies S.p.A valuing the entire fully diluted ordinary share capital of Cobra at euro 145 million. Cobra had reported net debt of euro 48 million as at 31 March 2014.

Vodafone said that it has entered into an agreement with the main shareholders of Cobra, who, together, hold 73.96 per cent of the share capital of Cobra, on a fully diluted basis, to effect certain matters including agreeing to tender their shares into the offer.

"Vodafone has also entered into an agreement with certain minority shareholders of Cobra's Telematics subsidiary to acquire their 20 per cent shareholding for a consideration of Euro 20 million," the statement said.