Revoking all the SSL certificates leaked by the Heartbleed bug will cost millions of dollars, according to Cloudflare, which provides services to website hosts.

SSL, the technology used to secure much of the internet, relies on private keys that must be kept hidden, but the Heartbleed flaw allows an attacker to steal them by pummelling a server with carefully crafted requests.

Cloudflare initially speculated that such an attack was impossible on the type of web server they use, but after opening it up to the public to test, the firm was soon proved wrong. As a result, it has decided to revoke and reissue all SSL certificates for its customers – well over 100,000 of them.

The company also responded to queries as to why it had not done so earlier, as a preventative measure. "The answer," cofounder Matthew Prince writes, "is that the revocation process for SSL certificates is far from perfect and imposes a significant cost on the internet's infrastructure."

In revoking their customers' SSL certificates, CloudFlare caused the size of the file which contains a list of all revoked certificates to grow by more than 200 times, from 22KB to 4.7MB, still held by CloudFlare's certificate authority Globalsign, which issues the certificates. That list has to be served to every single internet user, to ensure that their browsers know to reject stolen certificates.

As a result, Prince writes, "if you assume that the global average price for bandwidth is around $10/Mbps, just supporting the traffic to deliver the CRL would have added $400,000USD to Globalsign's monthly bandwidth bill … The total cost to Globalsign if they were using [Amazon's] infrastructure, would be at least $952,992.40/month"

Cloudflare had an extra reason to hesitate, because the firm was one of the few given forewarning about Heartbleed. The company was told about the flaw a week before researchers from Codenomicon disclosed it publicly – even before it was given a name, which is why CloudFlare's initial release on the matter just refers to "a new vulnerability … in OpenSSL". Since no patch was available, it fixed it by simply turning off the affected functionality altogether.

But even with that early warning, the company still had to act. The Heartbleed flaw has been in OpenSSL for two years, and with no way of telling whether it had been hit, the safest option was to replace all the certificates.



• Tor may be forced to cut back capacity after Heartbleed bug