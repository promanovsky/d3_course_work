The bacterial infection that causes whooping cough is on the rise, and has been classified as an epidemic by the CDC. California has reported 800 cases in the past two weeks and 3,458 during the year.

The CDC recommends that parents, siblings, grandparents, other family members and babysitters get vaccinated for whooping cough because babies cannot be vaccinated until they are six weeks old.

"As an important preventive measure, we recommend that pregnant women receive a pertussis vaccine booster during the third trimester of each pregnancy and that infants be vaccinated as soon as possible," Dr. Ron Chapman, California's public director, said in a statement.

Dr. Sara Cody, Santa Clara County's health officer added, "Even if a woman has had the vaccine before, if she is pregnant, she needs to get it again to protect her infant."

Symptoms include coughing and runny nose, which then leads to worse coughing.

For comments and feedback contact: editorial@rttnews.com

Health News