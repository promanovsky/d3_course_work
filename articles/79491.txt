The Japanese scientist who stunned the world earlier this year with an experiment showing that stem cells can be created from skin by simply exposing it to weak acid solution has been found guilty of scientific misconduct by her own research institute.

An investigation by the Riken Centre for Developmental Biology in Kobe into the study published in January in Nature found that there were two instances of scientific misconduct by the lead author of the study, Haruko Obokata, and that the study may have to be retracted.

The investigating committee said that Dr Obokata, 30, spliced two images together as if they were one and reused data from her doctoral dissertation in the Nature study, which purported to show how relatively easy it is to produce stem cells from skin cells using weak acid solution – a finding that had dramatic implications for regenerative medicine.

“Actions like this completely destroy data credibility. There is no doubt that she was fully aware of this danger. We’ve therefore concluded this was an act of research misconduct involving fabrication,” said Shunsuke Ishii, head of the investigating committee.

A statement from the Riken said that other scientists involved in the study may also be disciplined and that a full retraction of the scientific paper on stimulus-triggered activation of pluripotency (Stap) is likely to be issued following the results of any appeal by Dr Obokata.

“Those who were not found to have been involved in research misconduct still bear a heavy responsibility for the administrative negligence which allowed the research misconduct to occur,” Riken said.

The study, which involved several scientists from a number of research centres in Japan and the United States, turned Dr Obokata into a national celebrity in Japan, but other stem cell researchers failed to replicate her findings and doubts soon began to emerge over the published results.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Riken said that it has established its own internal group of scientists led by a special adviser to see if they can repeat the experiments, but that it will take about a year to reach any conclusion. Other researchers will also be encouraged to replicate the study.

“We will establish a framework so that the Stap cell phenomenon can be tested scientifically by third parties. Riken researchers will attempt to replicate the findings very strictly, and will offer their active cooperation to outside researchers attempt to reproduce them,” said Riken’s president, Ryoji Noyori, a Nobel laureate.

“I would like to take this opportunity to apologize once again for the fact that papers authored by researchers at Riken have harmed the public’s trust in the scientific community,” Professor Noyori said.

Dr Obokata insisted in a statement that she stands by her work and said that she intends to file a complaint against the institute for suggesting she deliberately misled her colleagues and other scientists.

“I am filled with shock and indication. If things stay as they are, misunderstanding could arise that the discovery of Stap cells itself is forgery. That would be unacceptable,” Dr Obakata said.

Professor Charles Vacanti of Brigham and Woman’s Hospital in Boston, a co-author of the research, said that it is too early for the work to be retracted.

“I firmly believe that the most appropriate course of action at this time is to clarify, in a very specific manner, all of the subtleties associated with the creation of Stapcells by posting specific details of our most effective protocol on our laboratory web site,” Professor Vacanti said.