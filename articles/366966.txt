Bey Scores 30 To Lead Pistons Past Celtics 108-102Saddiq Bey scored 30 points and the Detroit Pistons held on to beat the Celtics 108-102 on Friday night.

Halak, Ritchie Lead Bruins Past Rangers 1-0 For 5th StraightJaroslav Halak made 21 saves, Nick Ritchie scored in the second period and the Boston Bruins beat the New York Rangers 1-0 on Friday night for their fifth straight victory.

This One Tom Brady Ranking Among NFL QBs Really Puts Things In PerspectiveWith some of the Super Bowl dust now firmly settled, there was one conclusion drawn by a quartet of editors at NFL.com which really stood out as something that crystallized just how insane Tom Brady's age 43 season was for the Buccaneers.

Texans Release J.J. Watt ... Will Patriots Try To Sign Veteran Star?J.J. Watt's time in Houston is over. Will Bill Belichick make a play for the veteran star?

Red Sox Reportedly Sign Utility Man Marwin Gonzalez To One-Year DealGonzalez can play all around the infield and the corner outfield positions, and could help plug some holes on the Boston roster.