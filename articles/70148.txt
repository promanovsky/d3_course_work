There's now a simple blood test that can determine if someone who's rushed to the emergency room with chest pains is, in fact, suffering a heart attack.

Researchers in Sweden found the newly-developed test, combined with a typical electrocardiogram of the heartbeat, was accurate 99 percent of the time in showing which patients could be sent home, rather than be admitted to the hospital for observation and more diagnostics.

Out of about 9,000 patients determined to be low heart attack risks with the blood test and electrocardiograms, only 15 experienced heart attacks over the following month and none of them died.

"We believe that with this strategy, 20 to 25 percent of admissions to hospitals for chest pain may be avoided," Nadia Bandstein, a researcher with the Karolinska University Hospital in Stockholm who helped lead the study, said in a report by the Associated Press.

The findings were published in the Journal of the American College of Cardiology and presented March 31 at the cardiology school's annual conference in Washington, D.C.

Over 15 million people show up with chest pains at emergency rooms in the United States and Europe yearly, with a majority of the symptoms caused by anxiety, indigestion or other conditions less serious than heart attacks. Still, it turns out about 2 percent of those patients cleared by ER staff and sent home actually are having heart attacks.

The study included about 15,000 people over a two-year period who rushed to the Karolinska University hospital with chest pains.

The patients were an average age of 47 years old, with 4 percent of them having had heart attacks previously. About 21 percent of those studied ended up being admitted.

The research team reviewed the data later to see how using the blood test and electrocardiogram could have predicted the way the patients managed over the following month.

The study's results are "almost too good to be true," said Judd Hollander, an emergency medicine specialist at the University of Pennsylvania, told the AP. He concurred with the study that such a test would be "enormously useful" in the U.S., where he believes approval for its use is being held up by regulators who have judged the technique against unrealistically high standards.

Allan Jaffe, a cardiologist at the Mayo Clinic, said the problem is not what the test rules out, but what it might falsely rule in. It's so sensitive that it can pick up troponin from heart failure and other problems and cause unnecessary tests for that. He told the AP more studies in the U.S. are indeed needed to validate the study's conclusions. However, in the end, he confidently suggested, "I think the strategy long-term will be proven.