The Supreme Court ruled Wednesday that a startup Internet company has to pay broadcasters when it takes television programs from the airwaves and allows subscribers to watch them on smartphones and other portable devices.

The justices said by a 6-3 vote that Aereo Inc. is violating the broadcasters' copyrights by taking the signals for free. The ruling preserves the ability of the television networks to collect huge fees from cable and satellite systems that transmit their programming.

Aereo is available in New York, Boston, Houston and Atlanta among 11 metropolitan areas and uses thousands of dime-size antennas to capture television signals and transmit them to subscribers who pay as little as $8 a month for the service.

Company executives have said their business model would not survive a loss at the Supreme Court.

Some justices worried during arguments in April that a ruling for the broadcasters could also harm the burgeoning world of cloud computing, which gives users access to a vast online computer network that stores and processes information.

But Justice Stephen Breyer in his majority opinion that the court did not intend to call cloud computing into question.

But Breyer also said Aereo should be treated no different from a cable system. "Aereo's system is, for all practical purposes, identical to a cable system," he said.

Justices Antonin Scalia, Samuel Alito and Clarence Thomas dissented. Scalia said he shares the majority's feeling that what Aereo is doing "ought not to be allowed." But he said the court has distorted federal copyright law to forbid it.

Congress should decide whether the law "needs an upgrade," Scalia said.

Broadcasters including ABC, CBS, Fox, NBC and PBS sued Aereo for copyright infringement, saying Aereo should pay for redistributing the programming the same way cable and satellite systems must or risk high-profile blackouts of channels that anger their subscribers.

In each market, Aereo has a data center with thousands of dime-size antennas. When a subscriber wants to watch a show live or record it, the company temporarily assigns the customer an antenna and transmits the program over the Internet to the subscriber's laptop, tablet, smartphone or even a big-screen TV with a Roku or Apple TV streaming device.

The antenna is only used by one subscriber at a time, and Aereo says that's much like the situation at home, where a viewer uses a personal antenna to watch over-the-air broadcasts for free.

The broadcasters and professional sports leagues also feared that nothing in the case would limit Aereo to local service. Major League Baseball and the National Football League have lucrative contracts with the television networks and closely guard the airing of their games. Aereo's model would pose a threat if, say, a consumer in New York could watch NFL games from anywhere through his Aereo subscription.

The federal appeals court in New York ruled that Aereo did not violate the copyrights of broadcasters with its service, but a similar service has been blocked by judges in Los Angeles and Washington, D.C.

The 2nd US Circuit Court of Appeals said its ruling stemmed from a 2008 decision in which it held that Cablevision Systems Corp. could offer a remote digital video recording service without paying additional licensing fees to broadcasters because each playback transmission was made to a single subscriber using a single unique copy produced by that subscriber. The Supreme Court declined to hear the appeal from movie studios, TV networks and cable TV channels.

In the Aereo case, a dissenting judge said his court's decision would eviscerate copyright law.

Judge Denny Chin called Aereo's setup a sham and said the individual antennas are a "Rube Goldberg-like contrivance" — an overly complicated device that accomplishes a simple task in a confusing way — that exists for the sole purpose of evading copyright law.

Smaller cable companies, independent broadcasters and consumer groups backed Aereo, warning the court not to try to predict the future of television.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Indeed, Scalia himself noted that the high court came within a vote of declaring videocassette recorders "contraband" when it ruled for Sony Corp. in a case over recordings of television programs 30 years ago.

The case is ABC v. Aereo, 13-461.