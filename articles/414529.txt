PlayStation Network was down over the weekend, and it wasn’t routine maintenance. Apparently, a group of hackers who call themselves “Lizard Squad” forced a DDoS attack on Sony’s servers and made it impossible for everybody with a PlayStation 3 or PlayStation 4 to go online.

The attack also affected Xbox Live, Battle.net, and other networks. So PSN wasn’t the only one involved, but Sony seems to be the central victim though.

The latest attack was a lot more serious, or so they claimed, but the possibility was enough. Lizard Squad apparently felt that PSN downtime wasn’t enough and issued a bomb threat on Sony Online Entertainment president John Smedley‘s American Airlines flight from Dallas to San Diego.

Taking down servers for video game consoles is just annoying, but a bomb threat on a commercial airplane is considered a terrorist level criminal offense. The FBI is now investigating the hacker group previously responsible for bringing the PlayStation Network down.

This was not simply a blow to the opposition in the Xbox One vs PS4 console war. Both sides have been attacked alongside Blizzard’s network, but now it’s become much more serious. The FBI takes all bomb threats to airplanes seriously, even fake ones. Lizard Squad could face prison time for having Fight 362 diverted over the possibility of a bomb on board.

Ever since the attacks on the World Trade Center on September 11, 2001, anything involving real world threats to air traffic has been close to the top of the priority list for the FBI.

Lizard Squad discovered that the Sony exec was planning to take Flight 362 and planned their apparently fake threat accordingly. It is unknown how they came across this information. The threat was issued on Twitter and ended up with Smedley and over 170 others being diverted to Phoenix as authorities searched the plane for the explosives.

The hackers responsible for bringing PlayStation Network down probably think they’re safe since the bomb wasn’t real and no serious harm was done, aside from the loss of fuel and inconvenience of everyone on board.

My name is Brian Willson, I’m from Las Vegas NV. You think I’m scared of the FBI? — Lizard Squad (@LizardSquad) August 24, 2014

The FBI doesn’t see it that way, unfortunately for them. While no information was known to be stolen in the attacks, the threat of taking out a plane, never mind one with the Sony exec aboard, turned the situation from simply a high-tech prank into a matter of national security.

Yes. My plane was diverted. Not going to discuss more than that. Justice will find these guys. — John Smedley (@j_smedley) August 24, 2014

John Smedley took to Twitter shortly after having his flight diverted and expressed his confidence that the hackers will be brought to justice.

It seems the Lizard Squad should have been happy with simply taking the Xbox Live, Battle.net and PlayStation Network down for the weekend. This is no longer a game.

[image via Next-Gamer]