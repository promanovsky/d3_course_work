So... now much went down on the Game of Thrones Season 4 finale, huh?

Only two major deaths, one major escape and one major new direction for a lead character.

We need to start at King's Landing, don't we?

After Cersei destroyed her father's happy family dreams by admitting the truth about her (naked!) relationship with Jaime, the Queen Regent got it on with her brother.

Perhaps inspired by his lover/sister's strength to finally stand up to their dad, Jaime proceeded to free Tyrion (with the help of Varys), but everyone's favorite little hero couldn't go quietly into the night.

He went into Tywin's bedroom, presumably to kill his dad, only to find Shae there. Talk about a heart-breaking shock.

Tyrion tearfully choked the love of his life to death with the very necklace he gave her, before aptly spotting Joffrey's old crossbow and tracking his dad down... in the bathroom.

Tywin, as cocky and dismissive of Tyrion as ever, never believed his son would fire. But he uttered the W-Word about Shae one too many times and... THAWCK! THWACK! Two arrows into the chest later and the King of the Hand was dead, while Tyrion was off to be smuggled aboard a ship.

Wow.

ELSEWHERE:

After Drogon killed a three-year old girl, Dany realized she needed to control her dragons. So the Breaker of Chains lured the two on the premises to the catacombs and unbroke their chains there. She tied them up and shed tears as they cried for their mother.

Brienne stumbled upon The Hound and Arya, but could not convince the former to give up the latter, or the latter to follow her to safety. An epic sword fight ensued, one Brienne actually one.

With The Hound suffering and on the verge of death, Arya crossed him off her list in the most sadistic say possibe: by ignoring his pleas for a mercy killing and leaving him to rot. The episode ended with Arya using her magic words to board a boat headed for Braavos.

Bran finally arrived at the Three-Eyed Raven (after fighting off some skeleton monsters and losing a friend) was told he would never walk again. But he would fly!

Jon Snow had a tense bargaining session with Mance interrupted by Stannis and company. They have arrived at the Wall and they have taken Mance prisoner. Next stop, King's Landing?

What did you think of Game of Thrones Season 4 Episode 10? As teased ahead of time by producers, was it the best Game of Thrones finale yet?