From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Apple today confirmed weeks of rumors by announcing that it’s acquiring Beats Electronics in a deal worth $3 billion.

The purchase means Apple will now be able to expand its hardware business into premium headphones. It also gives Apple an on-demand streaming music service via Beats Music, which the company has previously been hesitant to do for fear that it would cannibalize digital download sales on iTunes.

The deal itself includes a purchase price of approximately $2.6 billion in cash and approximately $400 million in stock that will vest over time for both Beats Electronics and its Beats Music streaming music service. That’s a tad lower than what sources originally reported ($3.2 billion), but it’s still quite a bit of money. (Also the amount hardly puts a dent in Apple’s mountain of cash — nearly $160 billion, according to the company’s last SEC filing.)

“Music is such an important part of all of our lives and holds a special place within our hearts at Apple,” said Apple CEO Tim Cook in a statement about the deal. “That’s why we have kept investing in music and are bringing together these extraordinary teams so we can continue to create the most innovative music products and services in the world.”

As part of the deal, Beats CEO Jimmy Iovine and iconic rapper and Beats cofounder Dr. Dre will join Apple. This is something that bystanders speculated about in the days after news of the acquisition leaked, with Iovine said to be Apple’s choice to take over its digital media business. Apple, however, did not disclose in the release what roles Iovine and Dre will assume at the company.

“I’ve always known in my heart that Beats belonged with Apple,” said Iovine in a statement. “The idea when we started the company was inspired by Apple’s unmatched ability to marry culture and technology. Apple’s deep commitment to music fans, artists, songwriters and the music industry is something special.”

Apple said it expects the acquisition deal to close in the fiscal fourth quarter, provided that everything passes regulatory approval.

Related: Beats by Apple: Why a deal makes sense — and what it says about today’s Apple

Many people expected Apple to announce the Beats acquisition during the company’s Worldwide Developers Conference next month, but it looks like Apple didn’t want to overshadow any other announcements the company may have planned. And while Apple didn’t mention anything about WWDC, I’d be shocked if the company didn’t expand on its plans for Beats at the event.