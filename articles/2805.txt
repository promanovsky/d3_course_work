Welcome back to the wonderful world of white wine tears, roses, grammatical incorrectness and two-month journeys toward engagement. That's right, "The Bachelor," has returned -- this time with the ever-adorable increasingly detestable former pro soccer player and single dad Juan Pablo at its helm. This season, we'll be recapping the highlights of each episode in haiku.

12 Haikus About Juan Pablo's 'I'm Not Sorry' Tour

Sean and Catherine:

Here to make sexual jokes,

Distract from JP.

'Bachelor' nearly

Ruins 'The Muppets' for us.

[Insert shameless plug.]

The women arrive

To talk sh*t about JP.

Lots more of this, please.

The girls defend Clare,

Address JP's slut-shaming.

Kat says, "good for her."

Sharleen's "intriguing."

She'd kiss Juan, not marry him.

We understand, girl.

House mom Renee's up.

Juan wouldn't kiss her, but now

She's engaged. Thumbs up.

Andi fake slept to

Avoid Juan, but she wants love.

Next Bachelorette?

Juan has no regrets.

"I'm honest." "It's gonna hurt."

Of course, "It's OK."

Kelly calls Juan out

For his homophobia.

"I love gay people."

My dad is gay. It's relevant bc any guy that dates me knows that. If you think my family is more pervert, send me home sooner. #TheBachelor — Kelly Travis (@kellytravisty) March 4, 2014

We reached peak side-eye,

From the women and viewers.

Yup, he's still the worst.

Now the bloopers, to

Try and save some of Juan's charm.

Fail, but "ess oh-kay."

Chris asks the tough Qs:

"Are you Team Nikki or Clare?"

Run away now, girls.

Next Week, During The "Fin-ahhh-le"...

St. Lucia! Gorgeous and romantic! Hugs! Kisses! Falling in love! Every girl dreams of weddings! Fairy tale! Perfect fairy tale! Fairy tale ending! Is he ready? His mom's not sure! His mom says he's rude! Does he know Clare well enough? Nikki doesn't want to lose this! Clare tears! Nikki tears! Juan Pablo walks!

The Best Tweets About This Week's "Bachelor"