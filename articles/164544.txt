Facebook has confirmed that it intends to step up its use of video advertisements on the social network.

The firm introduced auto-play video ads to News Feeds last month, and its COO Sheryl Sandberg has revealed that they will become more common going forward.



"We really want to see auto-play video ads be something that's pretty common in the News Feed experience, based on consumer usage, before we push very hard in the ads business," she said during Facebook's Q1 earnings call.

She added: "Our goal is to make News Feed as engaging as possible. I'm sure your friends love seeing your kids play basketball [in videos on their News Feeds]. I think they'd probably like to see more of those.

"And when and if we deliver a really great ad experience, an ad that you love, something you're interested in, I think they're going to like that just as much."

Facebook's advertising revenue expanded 82% year-over-year during the first quarter of 2014, helping it to a revenue increase of 72%, its biggest growth in almost three years.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io