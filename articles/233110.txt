LOS ANGELES -- "Star Wars: Episode VII" director J.J. Abrams is giving fans a shot at a cameo in the film that's set to be released next year.

Disney, Lucasfilm and Bad Robot have teamed up for a campaign to raise funds for UNICEF. Abrams announced the initiative in a video from the set of the movie in Abu Dhabi.

For every $10 donation at Omaze.com/StarWars, contributors become eligible to appear in the film when it shoots in London. The campaign began just after midnight Wednesday and runs until 11:59 p.m. PDT on July 18.

The cast of "Episode VII" was revealed in April, and Adam Driver is among the newbies. Disney will release the film in December 2015.