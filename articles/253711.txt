Apple is preparing to launch a new platform that will help it to break into the market for smart homes at its World Wide Developers’ Conference (WWDC) next week, according to reports.

The system would make it easier to set up new smart home devices, such as lights that come on when an iPhone enters a house, says a report in the Financial Times. It would be controlled using an iPhone, iPad or Apple TV, which is set to be updated later in the year.

It may also use near-field communication, a technology that allows phones to communicate by touching them, which has already been suggested to be a key part of Apple’s new iPhone 6.

Other manufacturers would be able to join the system, with a scheme similar to Apple’s ‘Made for iPhone’ label, which marks out Apple approved third-party products. Apple already sells smart, phone-controlled products including thermostats, light bulbs and cameras through a ‘connected home’ section on its website.

Many see home automation as one of the growth sectors in technology, alongside wearable hardware and health software, both of which are also expected to feature heavily at WWDC.

Apple is also expected to announce some of its new Mac, iPhone and iPad operating systems at the WWDC keynote on June 2, which opens the week-long conference. Source: Getty Images (Getty Images)

Google has also been attempting to move into smart homes, with its purchase of Nest earlier in the year. Samsung has also announced a range of connected fridges and TVs.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here