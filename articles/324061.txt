Today at Google I/O, Chrome Director of Product Management Avni Shah introduced the new version of Chrome coming in the next Android update dubbed Android L.

As expected, we got a couple of feature updates. But it isn’t just a better version of Chrome. There is a clear and more profound message coming from Google. The company wants to blend native apps with web tabs. And the end of native apps as we know them could be closer than we might think.

In Android L, the redesigned app switcher looks a lot like Safari on iOS 7. It shows you your recent apps in a sort of card drawer. But it also shows native apps as well as active web tabs. Every web tab has the same value as an app. It’s huge.

Similarly, Google is expanding its App Indexing API to all Android apps. Before today, the company worked with selected companies. For example, when you search for a movie in Google, there could be a deep link in the search results that will open the IMDb app on this exact movie page. It’s a seamless transition from the web to a native app.

Knowing all this, imagine for a second what Android L will look like. You turn on your phone, search for something in Google using voice search on your home screen. It launches Chrome. You tap on the first search result, it launches a native app. You switch apps to read this article you found earlier in Chrome.

Back, and forth, and back, and forth between the web and native apps. After a while, you won’t even notice if you’re on the web or in a native app.

Why does this change make sense? Google has always been a web developer first. The company first became successful with its search engine. But even its following hit products were web apps, such as Gmail, Google Calendar or Google Drive (née Google Docs). Arguably, Google is still the king when it comes to developing web apps.

But even more important, Google still makes the vast majority of its revenue from web ads. The company wants people to spend more time on the web to see Google ads. It’s not going to change in the next quarter earnings. At least not yet.

So when it comes to both technology and business, Google’s future is in the web. Still. According to multiple HTML5 and web development advocates I talk with, Android could eventually become a low-level operating system with web apps on top of it. It will effectively be the end of native apps as we know them.

Multiple companies have tried this before — Palm with WebOS, Mozilla with Firefox OS. They all failed due to a combination of flaws — systems on a chip were not powerful enough, web runtimes were not efficient enough, web developers were not talented enough.

It mostly comes down to timing. Now, it would make sense for Google to slowly but surely switch to this new app model. Systems on a chip could potentially run full-fledged laptops now. Google has tirelessly worked on improving JavaScript and HTML rendering engines. And of course, there are thousands of talented web developers working for Google.

Promoting web apps is a long process and it could take years. But today is the first clear step in that direction.