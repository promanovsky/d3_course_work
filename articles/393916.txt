ANY airline would struggle with the devastating impact of losing one jet full of passengers, especially if it had already been bleeding money for years.

But losing another just months later is pushing crisis-hit Malaysia Airlines to the brink of financial collapse, airline experts said, spotlighting whether it can steer its way out of extended turmoil as once-troubled carriers such as Korean Air and Garuda Indonesia did before.

The flag carrier needs an immediate intervention from the Malaysian government investment fund that controls its purse strings, and likely deep restructuring, to survive the twin tragedies of flights MH370 and MH17, analysts added.

The would likely involve the airline changing its name.

Malaysia Airlines (MAS) was already struggling with years of declining bookings and mounting financial losses when MH370’s mysterious disappearance in March with 239 people aboard sent the carrier into free-fall.

The July 17 downing of flight MH17 over Ukraine, which killed all 298 people on board, deeply compounds those woes.

“The harrowing reality for Malaysia Airlines after MH17 is that if the government doesn’t have an immediate game plan, every day that passes will contribute to its self-destruction and eventual demise,” Shukor Yusof, an analyst with Malaysia-based aviation consultancy Endau Analytics, told AFP.

Shukor said MAS was losing “$US1-2 million ($1.06-$2.13 million) a day,” and has “the bandwidth to stay afloat for about six more months based on my estimates of its cash reserves”.

Writing in the UK’s Sunday Telegraph, Hugh Dunleavy, Malaysia Airlines’ commercial director, stated that the carrier “will eventually overcome this tragedy and emerge stronger”.

The Malaysian government had begun to speed up its review of the airline’s future — started after the disappearance of MH370 — following the second tragedy, Dunleavy wrote.

“There are several options on the table but all involve creating an airline fit for purpose in what is a new era for us, and other airlines.” “With the unwavering support we have received from the Malaysian government, we are confident of our recovery, whatever the shape of the airline in future,” he wrote.

Image is everything

While the MH17 disaster was beyond the airline’s control — pro-Russia separatists in Ukraine stand accused of shooting it down with a missile — bookings are expected to take a further significant hit, as they did in MH370’s wake.

“I’m not considering going to Malaysia in the next few years. Not unless Malaysia Airlines acts or does something in the future that will allow people to feel more relaxed about travelling there,” said Zhang Bing, a Chinese national in Beijing.

Jonathan Galaviz, a partner at the US-based travel and tourism consultancy Global Market Advisors, said “perception is key in the airline industry”.

“Unfortunately for Malaysia Airlines, potential international customers are now going to link the brand to tragedy.” The airline already has announced refunds for ticket cancellations following MH17, which Galaviz said would cost millions of dollars.

Speculation is rife that state investment vehicle Khazanah Nasional, which owns 69 per cent of the airline, will delist its shares and take it private, which could set the stage for painful cost-cutting measures and other reforms.

Analysts have long blamed poor management, government interference, a bloated workforce, and powerful, reform-resistant employee unions for preventing the airline from remaining competitive.

MAS lost a combined 4.1 billion ringgit ($1.37 billion) from 2011-13. It bled a further 443 million ringgit in the first quarter of this year, blaming MH370’s “dramatic impact” on bookings.

Khazanah declined to comment on future plans for the airline.

Rising from the ashes

Other airlines have risen from the ashes, lessons that could be instructive for MAS, experts said.

Indonesia’s state-owned Garuda Indonesia was plagued by a series of problems in the 1990s and early 2000s, including heavy debts and the murder of a prominent human rights campaigner midair in 2004.

Safety problems also blighted its image, including a 1997 crash on Sumatra island that killed all 234 aboard and remains Indonesia’s deadliest air disaster.

Former banker Emirsyah Satar was appointed in 2005 to turn around the airline, and he undertook a massive exercise to nurse it back to health. In 2010, it was named the world’s most improved airline by London-based consultancy Skytrax.

Korean Air was in trouble after a period during the 1980s and 1990s when several accidents left more than 700 people dead.

It embarked on a major reform push, bringing in retired Delta Air Lines vice-president David Greenberg in 2000, who subsequently revolutionised its safety and operational practices.

Korean Air is now widely respected worldwide.

Shukor said Malaysia’s government and Khazanah face a “mammoth task” but that the airline could learn much from carriers that have faced similar challenges.

“Its name is now synonymous with disaster, mismanagement, lack of discipline and many negative elements,” he said.