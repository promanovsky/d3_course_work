Children with autism or Attention Deficit Hyperactivity Disorder are seven to eight times more likely have strong desire to be the other gender compared with kids with no neuro-developmental disorder.

For the study, the researchers compared the rate of gender identity issues in children with specific neuro-developmental disorders against normally developing children. The researchers studied the data of the children aged between 6 and 18. Some participants were normal, some had autism, ADHD, epilepsy, and neurofibromatosis.

The study results showed that gender variance was about eight times more likely in participants with autism, and approximately seven times more likely in children with ADHD. However, no differences were found between children with epilepsy or neurofibromatosis, and the control group.

"From previous studies and clinical reports, we knew that children who presented with gender identity issues were more likely to have autism than would be expected just by chance, but we'd never looked at how common gender variance was in different developmental groups," John F. Strang, PsyD, a pediatric neuropsychologist at Children's National Health System, said in a news release. "What we saw was an over-representation of gender variance in kids with autism or ADHD. The next step for us is to understand what that might mean."

According to the researchers, ADHD or autism increases the risk of gender variance because of their primary symptoms. They explained that kids with ADHD may find it more difficult to hide their gender variant impulses. On the other hand, those with autism were more likely to express their underlying gender variance as they have less awareness of social expectations.

The findings also revealed that kid with neurodevelopmental disorders and gender variance showed higher rates of anxiety and depressive symptoms compared to those with no reported gender variance.

"In our autism clinic we now ask questions regarding gender identity because we know it's not such an uncommon issue. We don't overemphasize it because it is a relatively small percentage of children overall, but we do want to make sure we're asking about it and providing children and families support when they need it," concluded Strang.

The study was published in the Archives of Sexual Behavior.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.