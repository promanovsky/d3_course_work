Slimmed-down telecommunications firm Nokia unveiled a new CEO on Tuesday and reported net profit for its network equipment business that beat expectations.

Operating profit at its networks business known as NSN - its core unit now that its handset unit has been sold to Microsoft - saw a 10 percent rise year-on-year in the first quarter. This beat analysts' expectations in a Reuters poll. For the group as a whole, sales in the first quarter came in at 2.7 billion euros ($3.7 billion) and the group earned 0.04 euros share, against the 0.08 euros seen in the fourth quarter of 2013. Nokia added that it would return an extra 1 billion to shareholders from the sale of its phone unit.

With Nokia now focusing on NSN, which accounts for nearly half of Nokia's total revenue, the company has announced Rajeev Suri will take the helm.

Shares in the group jumped 6 percent in opening deals in Europe.

Read MoreNokia sealsMicrosoft deal amid new CEO speculation



In 2011, Suri undertook an aggressive restructuring drive that saw around 17,000 jobs axed at the unit in an attempt to make it more profitable. Suri was largely credited with the turnaround and analysts believed he was a natural choice for the company. In 2013, the networks unit posted operating profits of 1.1 billion euros, up 39 percent from the year before.