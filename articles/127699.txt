Public health officials in Ohio warned the parents of unvaccinated children against sending their kids to school, after news of a mumps outbreak in the Columbus area surfaced.

Health commissioners Dr. Teresa Long and Susan Tilgner released official statements to school authorities to give to parents of children who haven't been vaccinated against mumps, the Columbus Dispatch reported on Wednesday. The letters reportedly instructed parents to keep kids who weren't fully vaccinated at home. Some might even have to stay off school grounds for the next 25 days, in the instance that the mumps shows up on campus.

Columbus Public Health Spokesperson Jose told the Dispatch that this might push some mothers and fathers to get their children vaccinated.

"Hopefully, there will be some [parents] who will reconsider and, hopefully, take action and get their kids immunized," Rodriguez said.

The mumps outbreak in central Ohio has increased significantly in the past few days - the Dispatch reported that local cases grew to 224 in eight counties on Tuesday.

17 cases among schoolchildren have been reported, but none of the instances have occurred in clusters of two or more. In that case, Long said, serious action would be taken by public health authorities.

More than one of the children who got the mumps wasn't vaccinated, and two others had only received on of their two shots. Two others didn't have a medical record readily available.

Mumps often leads to dangerous health issues, including deafness. Seven men in the Columbus area reported having severely swollen testicles. One woman said she had bloated ovaries.

Mumps can sometimes cause infertility, the Dispatch reported.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.