LONDON, ENGLAND - JANUARY 18: A mobile device shows Wikipedia's front page displaying a darkened logo on January 18, 2012 in London, England. The Wikipedia website has shut down it's English language service for 24 hours in protest over the US anti-piracy laws. (Photo by Peter Macdiarmid/Getty Images)

Your high school teacher said it best: Wikipedia is not a reliable source.

The online encyclopedia that can be edited by experts and idiots alike is an easy source of information when trying to learn about a new topic. But a new study confirms what we all (hopefully) already know: Many entries -- especially medical entries -- contain false information, so don't use Wikipedia in place of a doctor.

Dr. Robert Hasty of Campbell University in North Carolina, along with a team of researchers, published the study in this month's issue of the Journal of the American Osteopathic Association. The study calls the information published in 20,000-plus medical-related Wikipedia entries into question.

For the study, researchers identified the "10 costliest conditions in terms of public and private expenditure" -- which included diabetes, back pain, lung cancer and major depressive disorder -- and compared the content of Wikipedia articles about those conditions to peer-reviewed medical literature. Two randomly assigned investigators found that 90 percent of the articles contained false information, which could affect the diagnosis and treatment of diseases.

Now for those of you who are saying that it's not the doctors themselves checking Wikipedia, you'd be wrong. According to a pair of studies from 2009 and 2010, "70% of junior physicians use Wikipedia in a given week, while nearly 50% to 70% of practicing physicians use it as an information source in providing medical care."

Pew research suggests that 72 percent of Internet users have looked up health information online in the last year. False information on Wikipedia accounts -- like a edited information about the side effects of a medication or false information about the benefits of one course of treatment over another -- could encourage some patients to push their doctors toward prescribing a certain drug or treatment.

Moral of the story: Wikipedia can't tell you if those sniffles are a symptom of the common cold or the West Nile Virus, so consult your doctor if you have health concerns. If you're a doctor, we don't know what to tell you, except maybe get off Wikipedia. (Isn't that what med school was for?)