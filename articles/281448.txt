Diets rich in protein appear to reduce a person's risk of stroke, particularly if it's a lean animal protein like fish, a new analysis suggests.

People with the highest amounts of animal protein in their diets were 20 percent less likely to suffer a stroke, compared with those who ate little to no protein, said study author Xinfeng Liu, of Nanjing University School of Medicine in Nanjing, China.

For every additional 20 grams per day of protein that people ate, their risk of stroke decreased by 26 percent, the researchers found.

"If everyone's protein intake were at this level, that would translate to more than 1.4 million fewer deaths from stroke each year worldwide, plus a decreased level of disability from stroke," Liu said in a news release from the American Academy of Neurology.

The researchers concluded that animal protein offers more than twice the protective benefit against stroke as protein from vegetable sources.

Stroke experts cautioned against taking the study's findings too literally, however. Many animal protein sources also come with high levels of saturated fats that can increase risk of stroke.

"I don't think this study means to the public you should run out and start eating burgers and red meat," said Dr. Ralph Sacco, chair of neurology at the University of Miami's Miller School of Medicine. "Focusing on lean protein consumption and/or even vegetable protein is important."

Liu noted that two of the seven studies took place in Japan and a third took place in Sweden, where people tend to eat more fish than red meat. Fish has been previously linked to reduced stroke risk, while red meat consumption has been shown to increase the chances of suffering a stroke.

Doctors aren't sure exactly why protein decreases stroke risk. The nutrient appears to help protect against hardening of the arteries, high blood pressure and diabetes, all of which are risk factors for stroke, Sacco said.

The new review looked at previous research on the relationship between protein in the diet and the risk of stroke. Seven studies involving more than 250,000 participants were included in the analysis.

The results, which are published online June 11 in the journal Neurology, accounted for other factors that could affect the risk of stroke, such as smoking and high cholesterol, the study authors said.

The investigators found in their analysis that animal protein reduced stroke risk by 29 percent, while vegetable protein lowered risk about 12 percent. However, the study only found an association between protein intake and stroke risk because it was not designed to prove a cause-and-effect link.

Protein quality might explain this difference, said Dr. Linda Van Horn, a professor of preventive medicine at Northwestern University Feinberg School of Medicine and a past chair of the American Heart Association's Nutrition Committee.

Animal proteins are considered "complete" because they contain all the amino acids needed by humans, while most sources of vegetable protein are incomplete, she said. Vegetarians often need to include a wide variety of vegetable protein sources in their diet to get all the amino acids they need.

"One could say that having animal protein simply means you're getting a better quality diet because all the amino acids are present," Van Horn said.

But vegetable proteins also come with lower amounts of saturated fat. Dr. Arturo Tamayo, an assistant professor of neurology at the University of Manitoba in Winnipeg, Canada, said that people will do themselves no favor if they pay attention only to protein intake without considering other dietary risk factors for stroke.

"If we exclusively rely on protein, we are making a mistake," said Tamayo, who wrote an editorial that accompanied the new analysis. "This is a complex disease that needs the control of multiple risk factors and lifestyle changes."

Saturated fat, salt and sugar all have been shown to increase a person's risk of stroke, he said. People who smoke or drink also are at increased risk.