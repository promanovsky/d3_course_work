Afrezza has a Boxed Warning advising that acute bronchospasm has been observed in patients with asthma and chronic obstructive pulmonary disease (COPD). Afrezza should not be used in patients with chronic lung disease, such as asthma or COPD because of this risk. The most common adverse reactions associated with Afrezza in clinical trials were hypoglycemia, cough, and throat pain or irritation.

The FDA approved Afrezza with a Risk Evaluation and Mitigation Strategy, which consists of a communication plan to inform health care professionals about the serious risk of acute bronchospasm associated with Afrezza.

The FDA is requiring the following post-marketing studies for Afrezza:

•a clinical trial to evaluate pharmacokinetics, safety and efficacy in pediatric patients;

•a clinical trial to evaluate the potential risk of pulmonary malignancy with Afrezza (this trial will also assess cardiovascular risk and the long-term effect of Afrezza on pulmonary function);

•two pharmacokinetic-pharmacodynamic euglycemic glucose-clamp clinical trials, one to characterize dose-response and one to characterize within-subject variability.