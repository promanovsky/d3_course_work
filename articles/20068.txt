It's a week of new celebrity tattoos.

Ariana Grande has fresh neck ink. Justin Bieber is rocking an unusual, artistic tattoo on his neck.

And now Miley Cyrus is showing off a Sad Kitty marking, posting the following photo on Instagram along with the simple caption:

"#sadkitty #f–kyeahtulsa.

Cyrus has a number of tattoo across her body, but that's nothing compared to the Miley Cyrus fan who has over FIFTEEN tattoos related to the artist on his body.

That's impressive. And a bit scary.

We've included Miley's latest ink among this look at celebrity tattoos: