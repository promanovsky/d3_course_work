A California chicken producer has issued its first recall since being linked to an outbreak of an antibiotic-resistant strain of salmonella that has been making people sick for more than a year, company and federal food officials said Thursday night.

The U.S. Department of Food and Agriculture said it has found evidence directly linking Foster Farms boneless-skinless chicken breast to a case of Salmonella Heidelberg, an antibiotic-resistant strain of the disease that has sickened more than 500 people in the past 16 months and led to pressure from food safety advocates for federal action against the company.

As a result, Foster Farms issued a recall for 170 different chicken products that came from its Fresno facilities in March.

The USDA said its investigators first learned of the salmonella case on June 23, and the recall was issued as soon as the direct link was confirmed. The location of the case and identity of the person were not released.

Foster Farms says the products have "use or freeze by" dates from March 21 to March 29 and have been distributed to California, Hawaii, Washington, Arizona, Nevada, Idaho, Utah, Oregon and Alaska.

The long list of products in the recall include drumsticks, thighs, chicken tenders and livers. Most are sold with the Foster Farms label but some have the labels FoodMaxx, Kroger, Safeway, Savemart, Valbest and Sunland. No fresh products currently in grocery stores are involved.

The USDA said it was working with the company to determine the total amount of chicken affected by the recall.

The company emphasized that the recall was based on a single case and a single product but the broad recall is being issued in an abundance of caution.

"Our first concern is always the health and safety of the people who enjoy our products, and we stand committed to doing our part to enhance the safety of our nation's food supply," Foster Farms said in a statement.

The federal Centers for Disease Control says 574 people from 27 states and Puerto Rico have been sickened since the outbreak began in 2013, leading to increasing pressure from food safety advocates for a recall or even an outright shutdown of Foster Farms facilities.

Bill Marler, a Seattle attorney who specializes in class-action food-safety lawsuits, commended both Foster Farms and the USDA for "doing the right thing for food safety."

"Recalling product is both embarrassing and hard, but is the right thing to do for your customers," Marler said.

The company was linked to previous salmonella illnesses in 2004 and in 2012.

Recalls of poultry contaminated with salmonella are tricky because the law allows raw chicken to have a certain amount of salmonella — a rule that consumer advocates have long lobbied to change. Because salmonella is so prevalent in poultry and is killed if consumers cook it properly, the government has not declared it to be an "adulterant," or illegal, in meat, as is E. coli.

In a letter from USDA to Foster Farms last October, the department said inspectors had documented "fecal material on carcasses" along with "poor sanitary dressing practices, insanitary food contact surfaces, insanitary nonfood contact surfaces and direct product contamination."

Foster Farms said in May that it had put new measures in place, including tighter screening of birds, improved safety on the farms where the birds are raised and better sanitation in its plants.