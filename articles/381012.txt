Its lead producer, Hollywood veteran Eric L. Gold, told Variety last week he was hoping to raise $5 million to keep the show onstage, but his quest to save it was unsuccessful.

“It’s week to week right now,” he said. “I can’t tell you if it’ll be two weeks or two months. It’s an expensive game, and I’m the guy carrying the load financially. I made a rookie mistake by underestimating how much capital was necessary, but I’m tenacious.”

AD

The musical had plenty of elements working against it — there were questions of whether it would be able to draw traditional theatergoers and pique the interest of Tupac fans who aren’t necessarily lining up to see Broadway plays. “Holler” wasn’t able to gain traction via word of mouth, either, a fault most reviewers attributed to the book, written by Todd Kreidler. Kreidler was forced to make up a generic story because the production could not obtain the biographical rights to Shakur’s story. That was bizarre, given that Tupac’s mother Afeni Shakur was one of the show’s producers.

AD

Variety called the story line “vague and unspecific,” while Vulture condemned it for being riddled with clichés. Its musical arrangements and energy were widely praised, but many left wishing the musical had been able to tap Shakur’s life story instead of trying to match up his work with the tale of a down-on-his-luck, impoverished Everyman battling violence and drugs. Even the setting for the musical was vague: The program informed attendees it took place in a “Midwestern industrial city.”