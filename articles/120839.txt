Yahoo! CEO Marissa Mayer doesn’t have to do a whole lot to make Wall Street jump.

The 38-year-old tech chief saw the stock rise as much as 10 percent in after-hours trading after the search giant said revenue in Chinese e-commerce company Alibaba Group Holdings jumped a whopping 66 percent in the latest quarter.

It mattered little that Mayer’s revenue came in below analysts’ estimates.

Yahoo owns 24 percent of the Chinese e-commerce company, which is expected to go public later this year in a deal that could yield big bucks for the company.

After the conference call, Yahoo traded at $36.32, up 6.8 percent over its closing price of $34.21.

“The most important number [for Wall Street] is not related to Yahoo,” said Suntrust analyst Robert Peck. “It’s the growth rate of Alibaba ahead of the IPO.”

Wall Street has been salivating over Alibaba’s anticipated IPO, which is expected to raise a whopping $15 billion on the New York Stock Exchange in an offering that could value the firm at $100 billion.

Yahoo’s own numbers paled in comparison to its Chinese investment, but Wall Street was pleased nonetheless as Yahoo said revenue, minus certain acquisitions costs, grew by 1 percent in the first quarter to $1.87 billion from $1.74 billion the year prior.

Measly as that growth is, it marked Yahoo’s first improvement after four straight quarters of zero revenue growth.

Display ad revenues minus certain acquisition costs also increased by 2 percent to $409 million.

Earnings by almost any other measure were flat to down, including income minus stock-based compensation and other items of 38 cents a share in the first quarter, which is flat from a year earlier.

Mayer said on the conference call it will still take several years before the company starts seeing consistent growth figures. In the meantime, there’s Alibaba to help cover the gaps.