LONDON -- Australian-born TV entertainer Rolf Harris faces new allegations of sex abuse after being convicted of a string of indecent assaults on teenage girls.

Britain's National Society for the Prevention of Cruelty to Children said Tuesday it had passed a number of new allegations to police for investigation. Abuse lawyer Richard Scorer said his firm has received up to a dozen new complaints against the celebrity broadcaster and entertainer, and police said they are looking into the latest claims.

Harris, 84, was found guilty on Monday of 12 counts of indecent assault, dating from the 1960s to the 1980s. Most of his victims, including one of his daughter's close friends, were minors at the time.

The conviction of the once-popular musician and artist follows investigations into historic sex abuse by a number of British celebrities, including BBC TV host Jimmy Savile who was a friend of Harris, and died in 2011.

Savile was found after his death to have molested hundreds of young victims, police said.

Those revelations prompted many victims to come forward and led to charges against other entertainment figures, including Harris, celebrity agent Max Clifford and broadcaster Stuart Hall.

"What we have discovered in the wake of the Jimmy Savile scandal is just how deep child abuse in institutions ran and how some people were seemingly allowed to continue their sickening crimes unchallenged," said Scorer.

Harris is set to be sentenced Friday. He has been told to expect a prison sentence.

He was long a prominent figure on British and Australian children's TV shows and is remembered for the novelty song "Tie Me Kangaroo Down, Sport."