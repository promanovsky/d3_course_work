Country star Garth Brooks is scheduled to play five concerts at Croke Park

All five Garth Brooks concerts due to take place at Croke Park in Dublin have been cancelled, it was confirmed.

In a statement, Aiken Promotions said:

"It is with great regret that Aiken Promotions today announce that the five concert Garth Brooks comeback special event at Croke Park has been cancelled. No concerts will take place," it said.

"The ticket return process will be outlined tomorrow.

"Aiken Promotions have exhausted all avenues regarding the staging of this event.

"We are very disappointed for the 400,000 fans who purchased tickets for The Garth Brooks comeback special event."

Concert promoter Peter Aiken flew to the US this morning in a last-ditch attempt to persuade Garth Brooks to go ahead with the three permitted concerts in Croke Park.

Mr Aiken left Ireland early this morning on a flight to the US, so that he could meet the country and western singer face-to-face.

The chief executive of the Labour Relations Commission Kieran Mulvey confirmed this morning that Mr Aiken decided to be proactive when communications through emails and phone calls were not having a desired outcome.

Mr Mulvey told Newstalk: “There’s a time difference involved in all of this but maybe by this evening, something will have emerged.”

Mr Aiken left Ireland this morning to see if Garth Brooks “would reconsider what he was going to do... and maybe revisit this,” Mr Mulvey said

The mediator said while Garth Brooks’ ultimatum of “five concerts or none at all” was reported in the press, he thought perhaps the statement was made in the heat of the moment.

“Garth Brooks is a country and western singer, he’s not a diplomat. We all say things.”

“There is a logistics issue, there is equipment [for the concert] that needs to be brought over here... a degree of persuasiveness is still to be exerted.”

Further reading

Read More

Belfast Telegraph