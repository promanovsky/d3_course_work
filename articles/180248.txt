The WHO today slapped international travel restrictions on Pakistan, Cameroon and Syria over the countries' failure to control the spread of the deadly polio virus , just a month after India did the same.

According to the World Health Organizations' (WHO) restrictions, it will be mandatory for all residents and long-term visitors (over 4 weeks) from Pakistan, Cameroon and Syria to receive a dose of Oral Polio Vaccine (OPV) or Inactivated Poliovirus Vaccine (IPV) between 4 weeks and 12 months prior to international travel.

Recommendations for the travel restrictions were put forward by a 21-member International Health Regulations Emergency Committee which was formed on the directives of WHO board members on Geneva.

The WHO director-general accepted the committee's assessment and declared the international spread of wild polio virus in 2014 a Public Health Emergency of International Concern (PHEIC), a statement by the global health watchdog said.

"This has primarily been done to stop the spread of the deadly virus. And of the situation does improve when a review comes up, the restrictions will be withdrawn too," WHO national campaign coordinator in Pakistan Zubair Mufti told PTI.

The new WHO regulations call upon these countries to ensure that those undertaking urgent travel within 4 weeks, who have not received a dose of OPV or IPV in the previous four weeks to 12 months, receive a dose of polio vaccine at least by the time of departure as this will still be beneficial, particularly for frequent travellers.

It also calls on them to maintain these measures until at least 6 months have passed without new exportations.

As per the WHO, the countries affected by polio are Afghanistan, Cameroon, Equatorial Guinea, Ethiopia, Israel, Nigeria, Pakistan, Somalia and Syria.

However, it added that Pakistan, Cameroon and Syria pose the greatest risk of further wild poliovirus exportations in 2014 and hence the restrictions.

India has already imposed travel restrictions on those travelling from Pakistan.

Effective from March 15, all Pakistanis visiting India will have to submit along with their visa applications a certificate of vaccination against the polio virus.