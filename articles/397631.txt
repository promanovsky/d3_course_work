Dr. Azaria Marthyman, a Victoria doctor who was part of a medical group that travelled to Liberia earlier this month to treat victims of the Ebola virus has returned home, while two of his U.S. colleagues infected with the deadly virus fight for their lives.

Marthyman was part of a 14-member team sent by Christian relief agency Samaritan's Purse to provide clinical care following a renewed outbreak of Ebola in West Africa.

On Saturday, the charity issued a news release saying that one of Marthyman's colleagues, Dr. Kent Brantly of the U.S., had tested positive for the virus and was being treated in isolation in Liberia.

On Sunday, Serving In Mission (SIM) — another charity that works closely with Samaritan's Purse — said that Nancy Writebol, an American who was part of the joint SIM/Samaritan's Purse team, had also tested positive for Ebola and was being treated in the same isolation centre as Brantly.

Earlier, Samaritan's Purse had told media that Marthyman voluntarily placed himself under quarantine at home as a precaution upon his return.

But spokesman Jeff Adams said Tuesday afternoon that information was incorrect, and that Marthyman is just taking some time off with his family to decompress from his trip.

Marthyman is healthy and has no symptoms, he said.

Dr. Azaria Marthyman wore protective medical clothing as part of his work with Ebola patients. (samaritanspurse.ca)

Both charities announced Tuesday that they have now ordered the evacuation of their non-essential personnel from Liberia after an upsurge in the number of Ebola cases in the country.

A statement from SIM says no symptoms of Ebola are present in any of the evacuees, who are being monitored continually.

According to the World Health Organization, the incubation period for the virus is two to 21 days.

The fast-acting Ebola virus, which first appeared in 1976, produces a violent hemorrhagic fever that leads to internal and external bleeding. The infection is transmitted by direct contact with blood, body fluids and tissues of infected people or animals.

As of July 23, the number of Ebola cases in West Africa reached 1,201, with 672 deaths, according to the World Health Organization.

This Ebola outbreak is the largest in history, with deaths in Sierra Leone, Liberia, Guinea and Nigeria blamed on the virus. There is no vaccine and no specific treatment.

Doctor refuses interview request

CBC News sought to interview Marthyman by email about his experiences in Liberia fighting Ebola, but he declined.

"I regret to inform you that I am declining all requests for media interviews," the email reads. "The situation in Liberia is changing rapidly, and I don't have the most up-to-date information. Therefore, to avoid giving out any information that might not be accurate, I am referring all interview requests to our international headquarters, because staff there are closest to the situation in Liberia."

Marthyman posted many updates on the Samaritan's Purse blog while he was in Liberia. On July 24, he posted that he was headed home.

"I am doing very well physically and emotionally, having worked every day since my arrival in Liberia, and today having to say goodbye to so many people," the post read.

Mostly, though, Marthyman used his blog post to tell the story of a young boy named William, the doctor's first Ebola patient in Liberia, who needed to find a home.

"William now needs a home to go to," Marthyman wrote. "He does not have any family to care for him and will be under the ministry’s care. I pray that he will have a loving home to go to. Meeting William today seems to bring some closure to my stay here in Liberia, as I prepare myself to come home to my wife and family."

At least two other British Columbians have travelled to Liberia this month with Samaritan's Purse to fight Ebola. They include a nurse from Vancouver and a nursing student, who is also an emergency medical responder, from Squamish.