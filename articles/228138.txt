You're not invited (yes, that means you Lindsay Lohan): The A list stars snubbed by Kim and Kanye

Most of the cast of Hollywood celebs who attended Kim's wedding to Kris Humphries in 2011 are not invited to her intimate nuptials to Kanye



Lindsay Lohan, her mom Dina, singers Avril Lavigne, Demi Lovato and Mel B were on the guest list last time

But Kim's longtime best friends Brittny Gastineau and Jonathan Cheban ARE flying out to Paris for her third trip down the aisle this weekend



When Kim Kardashian tied the knot with Kris Humphries in 2011, it was a lavish, made-for-reality TV affair.

But when she says I Do to Kanye West on Saturday, the 33-year-old will try and shrug off all memories of her disastrous 72-day marriage to the basketball player - and this means a big snub to many of the celebs who turned up to her last ceremony.

Keeping it 'intimate' - or as intimate as Kimye can manage - the reality star and the millionaire rapper have failed to invite American Idol host Ryan Seacrest, who produces E! show Keeping Up With the Kardashians, alongside train wreck Lindsay Lohan, who nearly stole the show when she turned up to Kim and Kris's wedding in a plunging white gown.

Scroll down for video



Getting ready for the big day: Kim Kardashian and baby North West at the Givenchy store in Paris today ahead of her weekend wedding to Kanye West

Not invited: Troubled actress Lindsay Lohan leaves Kim's 2011 a little the worse for wear, alongside her mom Dina, former Spice Girl Mel B and husband Stephan Belafonte. All are not believed to be invited this weekend

Singers Demi Lovato and Avril Lavigne also joined in the celebrations as did TV host Mario Lopez and his wife Courtney.

None of these guests however, are set to join Kim and Kanye in Europe this weekend.



Asked about Kim's wedding, set to be held on Saturday at the Forte di Belvedere in Florence, with an extravagant evening bash at the Palais de Versailles outside Paris on Friday night, Seacrest did not seem too perturbed to be snubbed.

'Here's what I would guess. It would be one of the most amazing ceremonies that has ever been had," Seacrest, 39, told Access Hollywood at the FOX Upfronts in NYC on May 12. 'I think that we'll all sort of smile and love that fact that you can see how much he adores her and she adores him.'

Kim previously discussed the ceremony on Seacrest's KIIS FM radio back in February, saying: 'We're having a super, super small, intimate wedding.

They won't be flying to Paris: Ryan Seacrest, right, and host Mario Lopez and his wife Courtney - seen at Kim's 2011 wedding - have not been invited to her nuptials to Kanye West this weekend



We're in: Rachel Roy has been invited to the wedding- and naturally, Kim's best friend Brittny Gastineau will be there



'As we go along, we're realizing we want it to be smaller and more intimate than people are imagining and thinking.'

There are of course some who will have the honour of having been invited to both weddings.

Close friend, tennis champ Serena Williams - who attended the 2011 wedding with her sister Venus - is on the list.

While Kim's faithful best friend Brittny Gastineau will be jetting to Paris this week, to be joined by designer and New York socialite Rachel Roy.

Of course, Kim's longtime sidekick, PR Jonathan Cheban will be there - today tweeting that he was getting ready to fly out.

Made-up: Kardashian family friend and make-up artist Joyce Bonelli poses with Kris Jenner by the Eiffel Tower after flying in for the wedding

Besties: Tennis champ Serena Williams and PR Jonathan Cheban will be at the European extravaganza



Family ties: Make-up artist Rob Scheppy - seen with Kendall Jenner - has flown to Paris already for the wedding

Joyce Bonelli, close family friend and regular family make-up artist has already arrived with Kim's mom, Kris Jenner, and the pair have already posed for pictures in front of the Eiffel Tower.

They are joined by Kim's other make-up artist and hair stylist Rob Scheppy, who has also been busy tweeting pictures of himself in Paris, alongside Kim's younger sister Kendall Jenner.



TV presenter LaLa Anthony is also believed to be on the list - and singer Ciara, a close friend of Kim's would be attending - had she not given birth to a baby boy yesterday , celebrating his arrival with fiancé Future'.

Of course, the two guests whom everyone is waiting to see will show up are Kanye's friends Jay Z and Beyoncé.

The A-list pair were said to be shying away from attending - but MailOnline understands they will now make an appearance after Kim and Kanye banned cameras for the Kardashians reality show at the wedding.

Kim tweeted earlier this month: 'We are not filming our wedding for Keeping Up With The Kardashians. You will see everything leading up til and after! As much as we would love to share these memories on camera, we've decided to keep this close to our heart & share thru photos.'

Just an intimate affair: Kim and Kanye - seen today in Paris - have hired out le Palais de Versailles on Friday night - and will then fly guests to Florence for their wedding on Saturday

Happy day: Kim Kardashian married basketball player Kris Humphries in August 2011 and every aspect of the lavish ceremony was captured for her reality show - the wedding lasted just 72 days



She also disputed reports they had finalized a massive guest list, adding: 'No guest list has been released. Seeing fake ones. Especially not 1600 people invited like I just read. Its VERY small & intimate 4. Seeing fake wedding dress pics of me. No one has seen my dress! Those photos are old or photo shopped. That's it! Unless you hear it from us please don't believe nonsense!'

Page Six reported the wedding guests will each be given personal security and a mobile phone on arrival in Paris in a bid to shroud the event in secrecy.

Vogue writer Andre Leon Talley has asked Valentino to host a brunch for Kim and Kanye at his elegant Château de Wideville on Friday - and will cover the event for Vogue.com.

Guests will include the bridal group, among them sisters Khloé and Kourtney, mom Kris Jenner and stepfather Bruce Jenner.

Following an evening event at le Palais de Versailles, guests will be flown via private jet to Florence, Italy, where the wedding will take place Saturday at Fort Belvedere. Elisa Di Lupo.