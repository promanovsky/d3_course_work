NEW YORK (MarketWatch) — Treasury notes swung to gains Monday, pushing yields lower, after Federal Reserve Chairwoman Janet Yellen appeared to backtrack on earlier comments about how soon an interest-rate hike could come.

Some of the biggest moves came from short- and intermediate-duration Treasury notes.

Federal Reserve’s Janet Yellen. Getty Images

Yellen said Monday that the central bank will maintain its “extraordinary” economic support for “some time to come” at a conference in Chicago. The speech was likely aimed at downplaying previous comments about an approximate six-month gap between the end of the Fed’s bond purchases and the first interest-rate hike, said David Keeble, global head of interest-rate strategy at Crédit Agricole Corporate and Investment Bank. That timing was first suggested by Yellen after her first Fed meeting.

“It was back to the old Yellen,” he said, noting that parts of her speech were very similar to earlier testimony. The result was a roughly three basis-point drop in the two-year yield, which is “quite stupendous for that part of the curve,” he said. Read: Just how dovish was Yellen’s speech?

In recent trade, the two-year Treasury note US:2_YEAR yield was down 3 basis points to 0.418%. One basis point is one one-hundredth of a percentage point. The five-year note US:5_YEAR yield fell 3 basis points to 1.718% and the yield on the seven-year note US:7_YEAR was down 1 basis point to 2.302%.

The Fed has decided to reduce its monthly bond purchases at each meeting since December, putting the bond-buying program on track to end before the close of 2014.

“Tapering is the given for the balance of this year with the hiking timing becoming the market’s immediate big focus,” said David Ader and Ian Lyngen of CRT in a note. That increases the focus on economic data, which this week includes the Institute for Supply Management’s manufacturing index, ADP private-sector employment and the jobs report for March.

The Chicago purchasing-managers index fell to 55.9 in March, marking the lowest level since August, according to data released Monday. Economists polled by MarketWatch had expected a reading of 60.

The yield on the benchmark 10-year Treasury note US:10_YEAR was flat at 2.726%. The 30-year bond US:30_YEAR yield was up 1 basis point to 3.558%.

Read more on MarketWatch:

The company with the ticker IEX is not the firm on ‘60 Minutes’

Play-it-safe stock investors may take some risk on jobs

High-frequency trading hurts regular customers, Michael Lewis tells ’60 Minutes’