The latest acquisition headline swirling throughout the financial media involves Apple (NASDAQ:AAPL) and its potential buyout of Beats Electronics for $3.2 billion. At first glance, this seems like a good bet to silence the financial pundits that have heaped criticism on Apple and Tim Cook to pursue a large acquisition.

It's been well documented that Apple has a nearly unimaginable amount of cash on its balance sheet that's earning very little for shareholders because of extremely low interest rates. One of the things critics wanted Apple to do was to announce a strategic acquisition in order to restore innovation and growth. While Apple makes several acquisitions every year, 24 of them in the last 18 months to be exact, it has long resisted the high-profile acquisition, until now.

But while Apple's possible purchase of Beats Electronics represents a big deal that's likely to make for an exciting topic of discussion in the media, it's reasonable to question whether investors should really be all that excited about it. That's because the deal really isn't big enough to move the needle for a company of Apple's size. Also, the acquisition likely isn't enough to win the streaming music battle with Pandora Media (NYSE:P).

Apple goes shopping

Since Tim Cook took the helm of Apple, his position has long been to resist the urge to pursue a large acquisition just for the sake of doing one. He's fully aware of Apple's mountain of cash, which currently totals $146 billion in cash and marketable securities. All that cash has gotten the attention of large investors and analysts alike, who increasingly put pressure on Apple to do something with it.

Their hunger for a deal may finally be satiated with Apple's $3.2 billion potential acquisition. But from a strategic position, it's debatable whether this will really have much of a tangible effect. After all, Apple is a truly gigantic company. It generated more than $170 billion in sales last year, meaning it registered about $3.2 billion in revenue every week.

It's clear that beyond just Beats Electronics' headphones, Apple is targeting its newly launched streaming much service as a means of supplementing its own efforts in that arena. Apple's iRadio landed with a thud and hasn't latched on with consumers nearly to the extent the company would like. And even iTunes is showing signs of weakness.

Despite holding onto its spot as the world's largest music download service, total music downloads declined last year. By contrast, revenues for streaming services soared 50% last year. It's becoming increasingly clear that music downloads may soon be a technological relic, and that streaming may be the wave of the future. Unfortunately, that's where Apple is still falling short.

Too little, too late?

Pandora is still the king of music streaming. When Apple launched iRadio, it hoped it would be able to unclench Pandora's grip on market share. But that hasn't happened, and it's not likely, even with Beats Electronics in tow. Beats holds only about 200,000 streaming subscribers. By contrast, Pandora is still the leader with more than 75 million active listeners, which increased 8% in the first quarter.

Furthermore, Pandora itself controls 9% of all U.S. radio listening and racked up an impressive 69% revenue growth in the first quarter. Plainly stated, Pandora's dominance in music streaming is clear and accelerating.

The bottom line is that while acquisitions are always exciting, it's not readily clear that Apple's rumored acquisition of Beats Electronics will really help its desire to compete with Pandora in music streaming. Beats Electronics' fledgling streaming service has a very small number of subscribers, especially considering Pandora's ironclad grip on the streaming industry. While $3.2 billion is surely a lot of money, it's a drop in the bucket for a company of Apple's size.

Going forward, investor attention would be better fixed on Apple's upcoming product releases. Those are where the company generates most of its profits, and new versions of the iPhone, a new Apple television, or the much-anticipated iWatch would be much better future growth catalysts than a buyout of Beats Electronics.