New home sales in the U.S. pulled back in the month of February, according to a report released by the Commerce Department on Tuesday.

The report said new home sales fell 3.3 percent to an annual rate of 440,000 in February from the revised January rate of 455,000.

Economists had been expecting new home sales to drop 6 percent to 440,000 from the 468,000 originally reported for the previous month.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.