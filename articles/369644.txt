Columbus Short has stated that ABC "did the right thing" in letting him go from Scandal.

It was announced in April that Short would be departing the series and his role of Harrison Wright, following his arrest for alleged involvement in a bar brawl.

Valerie Macon/Getty Images



"I think, at a certain point, when you're [hearing] a barrage of stories after stories, ABC justifiably would want to distance themselves," Short told Access Hollywood. "They did the right thing."

The actor - who last week was arrested for public intoxication in Dallas - spoke out to defend Scandal creator Shonda Rhimes and the entire cast and crew.



"They did not throw me to the curb like trash - that's my family," he affirmed. "They knew that I was going through a tough season and rallied around me and wrapped their arms around me, from Shonda to the craft service man."

Of his recent legal troubles, Short added: "I have not been perfect, but I have not been all the things that have been in the press."

Scandal will return to ABC for a fourth season this fall. The show's third run - Short's final outing as Harrison - will air on Sky Living in the UK from July 31.

Former Scandal actor Columbus Short pleads not guilty to battery charge

Watch the full interview with Columbus Short below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io