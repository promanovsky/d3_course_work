Shutterstock photo

Investing.com -

Investing.com - U.S. stock futures pointed to a higher open on Wednesday, as upbeat U.S. economic reports released on Tuesday continued to fuel optimism over the country's outlook for growth.

Ahead of the open, the Dow 30futures pointed to a 0.21% rise, S&P 500futures signaled a 0.23% increase, while the Nasdaq 100futures indicated a 0.23% gain.

U.S. equities found support on Tuesday after data showed that U.S. durable goods orders rose unexpectedly in April and another report showed that U.S. consumer confidence improved in line with forecasts this month.

The Commerce Department said orders for long lasting manufactured goods rose 0.8%, compared to expectations of a 0.7% decline.

Separately, the Conference Board reported that its consumer conference index rose to 83 in May, up from a revised 81.7 in April, I line with economists' expectations.

General Electric (NYSE:GE) was expected to be active, after Chief Executive Officer Jeffrey Immelt told French President Francois Hollande that he is working on an improved offer for Alstom (PARIS:ALSO)'s energy business. GE shares edges down 0.08% in after-hour trade.

Amazon.com (NASDAQ:AMZN), up 0.38% in early trading, was also likely to remain in focus as the online retailer said it is "not optimistic" that a dispute with publisher Hachette Book Group will be resolved soon and added that it is acting "on behalf of customers."

The comments came after the company began refusing orders last week for Hachette books, including J.K. Rowling's new novel.

In the tech sector, Google (NASDAQ:GOOGL) gained 0.64% pre-market following reports it has designed its own self-driving vehicles that transport passengers at the push of a button. Speaking at a conference on Tuesday, co-founder Sergey Brin the company plans to have 100 to 200 test vehicles that are fully autonomous with extra safety features.

Across the Atlantic, European stock markets were little changed. The DJ Euro Stoxx 50 dipped 0.01%, France's CAC 40 edged up 0.08%, Germany's DAX inched 0.06% higher, while Britain's FTSE 100 eased up 0.02%.

During the Asian trading session, Hong Kong's Hang Seng climbed 0.59%, while Japan's Nikkei 225 rose 0.24%.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.