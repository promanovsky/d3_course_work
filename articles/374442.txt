NEW YORK (TheStreet) -- Shares of American Airlines Group (AAL) - Get Report are up 4.67% to $42.14 in pre-market trade after the airline slightly moderated its growth forecast for an important revenue measure for the second quarter, which is usually a strong period for carriers, Reuters reports.

Unit revenue, or passenger revenue per available seat mile, is expected to grow between 5.5% and 6.5% in the second quarter from a year earlier, American said.

At the higher end of that range, the forecast is slightly lower than the company's outlook in June, which called for an increase of 5% to 7%, Reuters noted.

Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

AAL data by YCharts

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.