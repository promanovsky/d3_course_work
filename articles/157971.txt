JK Rowling's Harry Potter follow-up novel The Casual Vacancy is set to be turned into a television miniseries thanks to a joint production from the BBC and HBO.

The BBC announced in 2012 that it would be adapting the novel but only now has confirmed that production, alongside US giants HBO, will take place this summer.

Three hour-long episodes will be produced according to The Hollywood Reporter. EastEnders alumni Sarah Phelps is writing the script with Paul Trijbits, Neil Blair, Rick Senat and Rowling herself on board as executive producers.

Jonny Campbell, who directed 2011 TV movie Eric & Ernie and fan-favourite Doctor Who episode Vincent and the Doctor for the BBC, will direct.

Rowling's first post Potter novel received mixed reviews upon release in 2012 but became a bestseller nonetheless thanks to its author's name.

It concerns an English village called Pagford, which appears idyllic but Is really anything but. Beneath the flower arrangements and lavish quaintness there is a populace at war with each other. The book deals with issues of class, drugs, prostitution and rape – which let's be fair makes it perfect for HBO.

The Casual Vacancy miniseries is expected to air on BBC One and HBO later this year.