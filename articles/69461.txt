Authorities say a man identified as reality TV star Benzino has been shot and injured by his nephew while in a funeral procession in Massachusetts.

Benzino, whose real name is Raymond Scott, is a cast member of the VH1 reality show Love & Hip Hop: Atlanta and CEO of Hip-Hop Weekly. The magazine's website confirmed on Saturday night that Benzino had been shot.

The Plymouth County District Attorney's Office says 48-year-old Benzino was on Route 3 south of Boston around noon on Saturday when 36-year-old Gai Scott pulled alongside and fired several shots into an SUV driven by Benzino. He was taken to a hospital with gunshot wounds. Hip-Hop Weekly reported his condition as stable.

Gai Scott, of Randolph, Mass., was taken into custody and charged with assault with intent to murder.