Burger King's proposed deal to buy Tim Horton's may come with a side of consumer backlash.

Dozens and dozens of commenters took to Burger King's Facebook page on Monday to criticize the American fast food giant for its efforts to acquire the Canadian coffee chain. The central complaint, leveled by these commenters, is that Burger King wants to relocate its base to Canada to get out of paying higher U.S. taxes.

"If you attempt to buy Tim Horton's for the purposes of evading US Taxes, I will NEVER step foot in another Burger King again," one person wrote on the company's Facebook page. "Don't do it."

Another commenter echoed the point: "I eat at Burger King about twice a month or so, but if you buy Tim Horton's and move your headquarters to Canada to reduce how much you pay in American corporate taxes, I will reduce how much I spend of my American dollars and find a new burger joint to frequent."

Image: Facebook, Burger King

Some appealed to the company on Twitter not to go through with the plan; others even went so far as to put up an online petition urging against the move.

It appears @BurgerKing is showing its true colors by doing an inversion and letting its customers pick up their tax tab. #StopEatingAtBK — Jimmy Fats (@jimmywhiz) August 25, 2014

@BurgerKing As a loyal customer, I will never enter your store again if you go off shore! A lot of people feel the same! Go Broke! — Charles R. De Armond (@crdearmond) August 25, 2014

I don't know why @BurgerKing wants to be the next company to lose customers bcz it unpatriotically won't pay US taxes. #LegalTaxEvasion — Pat120 (@Pat120) August 25, 2014

I can't say I'm their biggest customer anyway, but I won't go to @BurgerKing ever again if they dodge US taxes through #inversion. — Jon (@Jarathen) August 25, 2014

Walgreens encountered a similar backlash among customers as well as politicians after it considered incorporating its business abroad to alleviate its tax burden. The company scrapped its plans earlier this month, noting that it was "mindful of the ongoing public reaction to a potential inversion."

Burger King declined to comment on the initial consumer reaction to the news, but one source close to the company stressed that the merger was not a "tax play," but rather an effort to better enable both chains to expand their global presences.

"The two companies do fit together from a geographic standpoint," says R.J. Hottovy, a senior restaurant analyst with Morningstar. That said, he believes that the so-called "tax play" is a "key part of the deal."

Hottovy believes the consumer backlash should blow over "as long as there is not a change to the restaurants themselves [and] to the product offering." Not all analysts are so sure, though.

"This makes rational sense from a pure dollars and cents standpoint, but there are many bigger issues that need to be taken into account like the impact on the brand and consumer feedback," Sucharita Mulpuru, a retail analyst with Forrester Research, said in an email. "The execs at Burger King are very young and I would be very surprised if they would know how to deal with the latter."

She added: "It could be a PR disaster in the making."

BONUS: 5 Horrifying Facts About Fast Food