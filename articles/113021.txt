Updated from 2:36 p.m. ET to include Google confirmation of acquisition and comment.

NEW YORK (TheStreet) - Google (GOOG) - Get Report will buy drone-maker Titan Aerospace as the web-search giant continues to work on improving its popular imaging and mapping capabilities. Previously, Titan Aerospace had been rumored to be a target of Facebook (FB) - Get Report until the social network bought Ascenta for $20 million.

Google will use Titan Aerospace to bring internet access to regions of the world that continue to lack access and said that its efforts may also be used to help with global issues such as disaster relief and deforestation.

"Titan Aerospace and Google share a profound optimism about the potential for technology to improve the world. It's still early days, but atmospheric satellites could help bring internet access to millions of people, and help solve other problems, including disaster relief and environmental damage like deforestation," a Google spokesperson said via email to TheStreet.

Google didn't disclose a purchase price for Titan Aerospace, which currently has about 20 workers and will remain headquartered in New Mexico, according to The Wall Street Journal.

Titan Aerospace will work closely with the company's Project Loon. "It's why we're so excited to welcome Titan Aerospace to the Google family," they added in an e-mailed statement.

Titan Aerospace makes solar-powered drone planes that can be flown remotely. The company's technologies and capabilities could also prove a practical tool for advancements in Google's popular mapping application, Google Maps.

According to Titan's website, the company will launch commercial operations of its aircraft in 2015. Titan's drones fly as high as 65,000 feet and can remain in the air for years' at a time.

Shares of Google were higher in Monday trading, gaining 0.36% to $532.51.

-- Written by Antoine Gara in New York

Follow @AntoineGara