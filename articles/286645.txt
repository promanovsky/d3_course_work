Microsoft

Aaron Paul, co-star of the wildly popular "Breaking Bad" television series, is featured in a Microsoft ad that just might take control your Xbox One.

Microsoft released ad earlier this month with Paul showcasing the voice commands available in the Xbox One. At the beginning of the commercial, Paul says "Xbox On," the command console owners can use to interact with the Kinect sensor and turn on the console. While everything initially seemed just fine about that commercial, it turns out some gamers have discovered that Paul's command in the ad will get picked up by their Kinect and turn on the Xbox One in their home.

Although it doesn't appear to be a universal hack on Microsoft's part -- some users are seeing their devices turn on and others not -- it calls into question whether the company intended for it to happen or if it's a simple mistake.

Microsoft's Kinect comes with a surprisingly sensitive microphone that allows gamers to speak commands from across the room. "Xbox On" turns the console on, but there are countless others that will launch games, bring up the built-in Xbox store to buy digital goods, and more. Paul uses other voice commands in the video, but according to affected users, those don't appear to have been registered by the Kinect.

Beyond the possibility of having a celebrity control your gaming console, there's one other interesting thing to mention about the ad: it promotes features gamers can only find with Kinect integration with the Xbox One. At the end of the ad, however, Microsoft puts up a screen that mentions its new Kinect-free bundle now available for $400 instead of the $500 bundle that includes the Kinect. That could prove confusing to customers who don't follow the ins and outs of the gaming industry and wonder why, out of the box, their $400 Xbox One won't deliver the Aaron Paul experience.

CNET has contacted Microsoft for comment. We will update this story when we have more information.