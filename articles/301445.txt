All the news you need to know.

All the news you need to know....

AUSTRALIA has been given a healthy prognosis in a new report, but there are signs the condition of our health care system could be worsening.

The Commonwealth Fund has compared 11 developed nations’ health care systems and Australia has ranked fourth overall behind the UK (1st), Switzerland (2nd) and Sweden (3rd).

While the high ranking is something to be celebrated, the report shows Australia has dropped two spots over the past decade, having been placed second overall in 2004.

When stacked up against the 10 other counties studied in the report — Canada, France, Germany, the Netherlands, New Zealand, Norway, Sweden, Switzerland, the UK and the US — Australia performed strongly in quality of care, but fell down on cost-related issues.

THE BAD NEWS

The one major sore point for Australia in the report is that cost-related issues are making it more difficult for Australians to access health care.

Australia falls down in the categories of access (where we rank eighth) and cost-related access problems (ninth).

Access relates to whether patients can obtain affordable care and receive attention in a timely manner. The survey examined whether patients were able to afford necessary care and whether they had serious problems paying medical bills.

The results show that 25 per cent of doctors in Australia think their patients often have difficulty paying for medications or out-of-pocket expenses.

Australia ranked second-last when it came to out-of-pocket medical expenses, with 25 per cent of Australians paying upwards of $1000 in the past year. Only the US performed worse on this metric.

Sixteen per cent of Aussies did not fill a prescription, skipped a recommended medical test or treatment, or decided against seeing a doctor in the past year because the cost was prohibitive.

In the category of efficiency measures, the survey found that 31 per cent of patients visited an emergency department for a condition that could have been treated by a general practitioner.

Grattan Institute Health Program director Stephen Duckett said the latest results backed up the Commonwealth Fund’s previous findings about the Australian health system.

“That’s been consistent for the last few years,” Professor Duckett told news.com.au.

He said these problems would be exacerbated by the federal government’s proposed $7 GP co-payment, due to begin on July 1, 2015.

“That’s going to make access problems worse,” Prof Duckett said.

He said the new price signal “could mean up to 5 per cent (of Australians) will find it difficult to access health care”.

“People will either not seek care or put off care, or they might go to an emergency department. Either outcome is a bad outcome because hospitals are not good places for primary care,” Prof Duckett said.

University of Melbourne professor of public health Rob Moodie agreed the GP co-payment would make the health system less fair and worsen existing access problems.

“Out-of-pocket expenses are increasing, since 2000 they’ve doubled, and that’s seen by doctors to be deterring people from seeking treatment,” Prof Moodie said.

He said these expenses included fees for specialists and medicines.

“When you’re visiting a specialist they can charge whatever they like.”

Prof Moodie acknowledged that health costs were increasing, but he said there may be better ways to find efficiencies, such the government reviewing the high costs it pays for medicines for the Pharmaceutical Benefits Scheme.

THE GOOD NEWS

Australia ranks highly on the criteria of quality care — second overall behind the UK — which shows our health system is effective, safe, coordinated and patient-centred.

The report singles out Australia — which spends $3800 per capita on health — as being highly effective in the treatment of chronic diseases.

“Australia performs well in delivering recommended services to patients with diabetes, as well as providing written instructions to chronically ill patients,” the report states.

Australian doctors also received a tick for discussing preventive measures with patients, such as healthy eating and exercise.

Importantly, Australia ranks highly on the metric that our health care system prevents deaths and people over the age of 60 have a healthy life expectancy.

Prof Duckett said there was plenty of good news in the report.

“We are the second-cheapest place to get health care after the UK. It’s not like we have runaway costs; there are many, many other countries where it is more expensive,” he said.

“There are signs of good quality care, which reflects well on the whole system. It’s also a sign of openness; where there is a mistake, we learn from it.”

Despite our good performance overall, another recent survey from The Commonwealth Fund found Australians were pessimistic about our health care system.

Fifty-five per cent of people surveyed said the Australian health system needed fundamental changes, which was the highest out of the 11 countries, and one in five said the system needed to be completely rebuilt.

AT LEAST WE’RE NOT IN THE US

The Commonwealth Fund produced the report primarily to compare the US health care system with the world — and five different reports over the past decade have found the US system is the worst among developed nations.

The fund acknowledges that there are difficulties in comparing different countries, but it concludes that the US system is the most expensive in the world and consistently underperforms when stacked up against other developed nations.

The US falls down because it lacks a universal health insurance scheme, like Medicare. (Data on the back of Barack Obama’s Affordable Care Act reforms were not included in the report.)

“The US also ranks behind most countries on many measures of health outcomes, quality and efficiency,” the report states.

“US physicians face particular difficulties receiving timely information, coordinating care and dealing with administrative hassles.”

The Commonwealth Fund reached its finding by collating figures from a number of reports, including its 2011 international survey of sick adults, a 2012 survey of doctors and a 2013 survey of the general population, along with findings of the World Health Organisation and Organisation for Economic Co-operation and Development.