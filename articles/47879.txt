General Motors admits it screwed up by not fixing defective ignition switches in the Cobalt and five other car models as soon as it became aware of the problem. The company has now recalled 1.6 million cars worldwide to replace those faulty switches which can suddenly shut off the engine while the car is being driven and disable the airbags in a crash.

The problem has been blamed for 31 crashes and at least 12 deaths.

Read More

"Something went very wrong in our processes in this instance, and terrible things happened," said GM CEO Mary Barra in a video to employees. She promised the company's system for deciding and managing recalls will change because of this tragic situation.

Consumer advocates also want to see things change at the National Highway Traffic Safety Administration. They believe these dangerous vehicles would have been recalled many years ago had the safety agency done its job.

"NHTSA is simply not protecting the public from safety defects as it's supposed to do," said Clarence Ditlow, executive director of the Center for Auto Safety.

Ditlow blew the whistle on the GM air bag problems several months ago, after his nonprofit organization analyzed the data in NHTSA's own files. He told CNBC that this is not an isolated incident, but a pattern with the agency.

Read MoreFierce headwinds: GM's past dragging on its future

Ditlow said NHTSA is understaffed and underfunded, but he also believes the agency "has gotten too cozy" with the auto industry.

"They now refer to the auto companies as 'their customers.' The American public is their customer. They regulate the auto industry," Ditlow said.

The agency did not respond to CNBC's request for an interview. It provided a statement that said safety is the agency's top priority.

"NHTSA receives and screens more than 40,000 consumer complaints each year and pursues investigations and recalls wherever our data justifies doing so," the statement said. "Regarding the recent recall of certain GM vehicles, the data available to NHTSA at the time did not contain sufficient evidence of a possible safety defect trend that would warrant the agency opening a formal investigation," the statement said.

The agency said it is constantly looking for ways to better identify safety defects, but the statement pointed out that during the past seven years, NHTSA safety defect investigations resulted in 929 recalls involving more than 55 million vehicles and motor vehicle equipment.

Read More Toyota's $1.2 billion fine a warning for GM

Joan Claybrook, who ran NHTSA during the Carter administration, believes that in recent years the agency has looked for ways not to act, rather than protect the public. She calls it a "don't ask, don't tell mentality." NHTSA doesn't ask the auto companies what they know about safety problems and when it does learn about them, it doesn't tell the public, said Claybrook, who is now president emeritus of Public Citizen, a consumer advocacy group founded by Ralph Nader..

"It's not transparent; it's not open and it's very secretive," she said. "It has all sorts of steps that it insists be taken before it can even open an investigation, which is ridiculous."

Claybrook insists NHTSA had enough information in 2007 to demand a recall of GM vehicles with defective ignition switches. The agency had consumer complaints, and reports from its own investigators. It even had service bulletins GM sent to its dealers outlining the problem.

"The agency did not open an investigation in the GM Cobalt case and in many other cases, I believe, because they decided that unless they had everything handed to them on a silver platter in terms of evidence and information, they didn't have to act. And that's just completely incorrect," Claybrook said in an interview.

Sean Kane, a safety advocate and president of Safety Research & Strategies, has sued NHTSA four times in the past few years to get access to documents on incident investigations. He says the agency needs better practices and procedures.

"There are no guidelines for what gets investigated and how the agency's limited resources are allocated," he said.

Kane, who is often an expert witness for the plaintiff in accident cases, believes a "complete systematic shakeup" is needed to make NHTSA work better and smarter. The goal should be to find hidden safety problems, rather than "go from crisis to crisis," he said.

Lives are at stake

There's no way you can tell if your vehicle has a hidden safety defect. We rely on federal regulators to uncover these problems and force the manufacturers to fix them. Critics say the much-delayed GM Cobalt ignition switch recall is just one example that shows the watchdog isn't doing its job.

Read More'Clearly this took too long': GM's Barra on recall

NHTSA has two large databases containing consumer complaints and fatal accident reports from across the country. Those databases had information about GM vehicles with engines that suddenly turned off, as well as more than 300 reports of crashes where the airbags in these vehicles did not deploy—a red flag of a major system failure.

"NHTSA simply hasn't connected the dots using the data it has on file, and people have died as a result of that," said Jack Gillis, author of "The Car Book 2014."

The GM Cobalt case has generated so much publicity and so much outrage that Congress is now looking at how NHTSA operates.

Sen. Edward Markey, D-Mass., wants NHTSA to become more aggressive. In a letter to the agency's acting administrator, Markey called on NHTSA to require auto manufacturers to provide it with detailed information when they first become aware of incidents involving fatalities.

"More details about auto defects could prevent more deaths on the nation's roads, Markey said in a statement. "Americans need NHTSA's Early Warning Reporting system to actually provide early warnings, instead of just a rear view mirror look into what has already gone wrong. If NHTSA won't take action to greatly increase public disclosure of information related to potential safety defects, I will introduce legislation ensuring that it does so."

The Alliance of Automobile Manufacturers and the Association of Global Automakers declined our requests for a comment.