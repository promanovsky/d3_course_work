Rumors that Jay Z cheated on Beyonce have been circulating for months now, with no official response from either half of the power couple.

However, it now seems Queen Bey may have finally addressed the allegations during a recent concert.

That's Bey performing during a stop on her and Jay's On the Run tour. She's singing an as-yet-unreleased song entitled "Resentment" that tells the tale of a woman who's been cheated on.

Observant fans noticed that Bey changed the lyrics at certain points, possibly to more accurately reflect her own situation:

At the 2:27 mark in the above video, a line that originally read, "I'll always remember feeling like I was no good/Like I couldn't do it for you like your mistress could," has been changed to:

"I'll always remember feeling like I was no good/Like I couldn't do it for you like that wack bitch could."

Okay, so artists improvise and change lyrics to songs during live performances all the time. But it's the changes that Bey made at the 3:25 mark that are really raising questions:

The lines: "Been riding with you for six years...I gotta look at her in her eyes and see she's had half of me," have been changed to:

"Been riding with you for 12 years...I gotta look at her in her eyes and see she's had half of me. She ain't half of me. That bitch will never be me."

Yeah, she sounds pretty angry. We think you might have your 100th problem, Jay.