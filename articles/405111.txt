Malaria killed an estimated 627,000 people in 2012, mostly among African children, according to the WHO. ― File pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

PARIS, July 31 ― Drug-resistant malaria parasites are now firmly established in border regions in four Southeast Asian countries, imperilling global efforts to control the disease, experts warned yesterday.

Blood samples taken from 1,241 malaria patients found that parasites which are resistant to the frontline drug artemisinin have spread to border areas in western and northern Cambodia, eastern Myanmar, Thailand and Vietnam, they said.

There also signs of emerging resistance in central Myanmar, southern Laos and northeastern Cambodia, but none in three African states ― Kenya, Nigeria and Democratic Republic of Congo (DRC) ― that were included in the sampling.

The study, published in the New England Journal of Medicine, said doubling the course of antimalarial treatment, from three days to six, could help fight the resistance problem but time was short.

“It may still be possible to prevent the spread of artemisinin-resistant malaria parasites across Asia and then to Africa by eliminating them, but that window of opportunity is closing fast,” said Nicholas White, a professor of tropical medicine at Oxford University, England.

“Conventional malaria control approaches won’t be enough ― we will need to take more radical action and make this a global public health priority, without delay.”

Southeast Asia has been the source of growing worries that artemisinin is losing its edge as the weapon of choice against malaria.

If so, it will be the third time in little more than half a century that a drug will have been blunted by parasites that became resistant to it ― a process that has claimed millions of lives.

From the 1950s to the 1970s, parasites that were resistant to the drug chloroquine spread from Asia to Africa.

SE Asia source of resistance

Chloroquine was then replaced by sulphadoxine-pyrimethamine (SP), resistance to which emerged in western Cambodia and then spread to Africa.

SP was followed by artemisinin, a drug derived by Chinese scientists from a herb called sweet wormwood.

“The artemisinin drugs are arguably the best anti-malarials we have ever had. We need to conserve them in areas where they are still working well,” Elizabeth Ashley, a University of Oxford researcher who led the study, said in a press release issued by Britain’s Wellcome Trust.

The World Health Organisation (WHO) recommends that for uncomplicated malaria involving the Plasmodium falciparum strain of parasite, artemisinin be used in a combination therapy, rather than a monotherapy, to get the best chance of eliminating all the parasites.

When a therapy is stopped prematurely, or fails to kill all the parasites, the surviving parasites are able to mutate, handing on resistant genes.

The study, carried out at 15 sites in Southeast Asia and Africa, entailed measuring blood samples to see the rate at which parasites were killed.

A benchmark for elimination called the “parasite clearance half-life” ranged from 1.8 hours in the DRC to seven hours at the Thailand-Cambodia border, a hotspot for resistance that leapt to notoriety in 2005.

Researchers in Kenya meanwhile have discovered antibodies to produce new vaccines targeting the most virulent form of malaria, in a potentially significant boost.

For the study, published yesterday in Science Translational Medicine magazine, scientists tested a library of proteins from Plasmodium falciparum with antibodies formed by the immune systems of a group of infected children in Kenya.

The tests sought to determine which proteins elicited a response from the children’s immune systems, revealing previously unidentified antigens as possible targets for vaccines.

“Resistance to malaria drugs is an increasing problem so vaccines are desperately needed to battle the Plasmodium falciparum parasite before it has a chance to make people sick,” lead author Faith Osier from the Kenya Medical Research Institute said in a statement.

“This study presents us with a large number of new vaccine candidates that offer real hope for the future.”

Malaria killed an estimated 627,000 people in 2012, mostly among African children, according to the WHO. ― AFP