In 1773, Boston had its Sons of Liberty. In 2014, New York has the Hon. Milton A. Tingling.

The issue before each was constitutional. In the case of the original Boston tea partiers, they argued a parliament without any American representation had no legal right to tax them.

More than two centuries later in New York, the beverage in contention has changed — it’s soda, not tea — but the substance of the issue is the same: the legitimacy of a law, in this case the Board of Health’s limits on large sugary drinks, imposed on citizens without having been approved by a representative legislature.

Back in March 2013, just a day before the soda ban was to go into effect, Judge Tingling stunned the city by ruling the Board of Health did not have the authority to push its plan through.

He was pooh-poohed at the time, with then-Mayor Michael Bloomberg asserting Tingling had ruled “in error” and insisting he would be overturned.

But he wasn’t. In the second sentence of its decision this week, New York’s highest court held that by enacting its soda ban the Board of Health “engaged in law-making and thus infringed upon the legislative jurisdiction of the City Council of New York.”

It doesn’t get much clearer than this.

In some quarters, the ruling has been hailed as an affirmation of every American’s God-given right to enjoy a Big Gulp without government interference. There’s something to this argument.

But the libertarian outcome here is less the substance than the consequence of what Judge Tingling hath wrought.

What the judge grasped, almost alone, is that the issue isn’t whether 16-ounce servings of soda are good or bad for you.

At issue is the defining postulate of a self-governing society: laws are made by the people themselves acting through their elected representatives, not by unelected bureaucrats invoking their appointed authority and the notion they know best.

We know which side of this argument Mike Bloomberg is on.

“I’ve got to defend my children, and yours, and do what’s right to save lives,” he said when ­Tingling’s decision came down.

Judge Tingling’s virtue was to recognize this for what it really is: an argument for government free to do whatever it pleases. It’s the same logic behind the Board of Health’s astounding claim that its mandate gives it law-making powers equal to the City Council.

To accept this interpretation, the judge wrote, would be to accept an “administrative Leviathan” that would leave the Board of Health’s “authority to define, create, mandate and enforce limited only by its own imagination.”

The consequences of such an acquiescence, he suggested, would be “more troubling than sugar-sweetened beverages.”

All in all, it’s an extraordinary ruling.

To begin with, Judge Tingling showed both modesty and courage in his willingness to go up against the great god of health, in whose name government expands into areas it never did before while silencing any questions along the way.

Basically, Judge Tingling is saying that even if Bloomberg is right about obesity and the health consequences of too many Cokes — and he probably is — it still doesn’t give him the right to make an end-run around the democratic process.

Second, the judge didn’t simply substitute his own reading of government for Bloomberg’s. He went back centuries to read the city charters to understand what powers the Board of Health has and why.

Lo and behold, he found that while the board behaves as though its job is to preside over the fitness and health of New Yorkers, the charge given in law is far more limited: to protect us from threats such as the spread of communicable diseases.

The irony here, of course, is that in finding for the legislature’s exclusive right to make law, Judge Tingling is in the same position Joan of Arc found herself in with Charles VII: She was more interested in his claim to the French throne than he was.

So it is today, where it would be hard to find a legislature less interested in its real rights and responsibilities than the New York City Council.

None of this diminishes Judge Tingling’s contribution. Lawmaking is the fundament of a democratic society. The mere fact of having to present a bill, argue its merits and go on record as yea or nay to get a law passed or defeated is the basis of democratic accountability.

As The New York Times has written, Bloomberg’s ban on soda “was hailed by many public health officials as a breakthrough in the effort to combat the effects of high-calorie, sugary drinks on the public’s health.”

In other words, this was not just about another city regulation. This was a plan meant to kick off a revolution in modern, enlightened government.

In the end, Bloomberg did get the revolution. Thanks to Milton Adair Tingling, it’s just not the one he imagined.