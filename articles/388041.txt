Microsoft announced its biggest every jobs cuts on Thursday, but that's not the only big announcement the company made. CEO Satya Nadella also announced that the Nokia X series of Android phones will be making the transition to Windows Phone.

Nadella made the announcement via an internal memo sent to employees, made public on the Microsoft website:

Second, we are working to integrate the Nokia Devices and Services teams into Microsoft. We will realize the synergies to which we committed when we announced the acquisition last September. The first-party phone portfolio will align to Microsoft's strategic direction. To win in the higher price tiers, we will focus on breakthrough innovation that expresses and enlivens Microsoft's digital work and digital life experiences. In addition, we plan to shift select Nokia X product designs to become Lumia products running Windows. This builds on our success in the affordable smartphone space and aligns with our focus on Windows Universal Apps.

Stephen Elop, the erstwhile Nokia CEO and now the Executive Vice President of Microsoft's Devices & Services business unit, reiterated the same in his email:

With our focus, we plan to consolidate the former Smart Devices and Mobile Phones business units into one phone business unit that is responsible for all of our phone efforts. Under the plan, the phone business unit will be led by Jo Harlow with key members from both the Smart Devices and Mobile Phones teams in the management team. This team will be responsible for the success of our Lumia products, the transition of select future Nokia X products to Lumia and for the ongoing operation of the first phone business.

Nokia announced the X series of phones running a heavily customised version of Android at MWC this year.

Affiliate links may be automatically generated - see our ethics statement for details.