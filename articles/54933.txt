Most students try to avoid summer school.

But Skidmore College may see a rise in applications this year, thanks to a course offered by Visiting Assistant Professor of Sociology Carolyn Chernoff.

It's titled "The Sociology Of Miley Cyrus."

As you can see in the syllabus above, the class will not teach Twerking or spitting on audience members.

Instead, it will focus on the “rise of the Disney Princess,” while taking a look at other stars (such as Justin Timberlake, Britney Spears and Christina Aguilera) and also teaching:

Gender stratification and the hyper-commodification of childhood.

Different kinds of transitions to adulthood.

Allies and appropriation.

Uses of culture across race, class, and gender.

Bisexuality, queerness and the female body.

Rutgers University, meanwhile, is bringing back a course titled “Politicizing Beyonce."

Where do you stand on college curriculum being based on the lives of celebrities?

Would you take any of the following courses?