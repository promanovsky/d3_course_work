

In this Oct. 23, 2013, file photo, container ships wait to be off-loaded in a thick fog at the Port of Oakland in California. A decline in exports contributed to the economic slowdown in the first quarter. (Ben Margot/AP)

Well, that was pretty ugly.

It turns out that, at an annual rate, the economy shrank 2.9 percent in the first quarter of 2014, mostly due to weak exports, weak inventories, unexpectedly weak health-care spending and the harsh winter. That's a full three percentage points lower than the initial estimate of 0.1 percent growth, which makes this the largest revision from start to finish in the 30 years that we've been calculating Gross Domestic Product this way. And, as you can see below, that was enough to make this the worst quarter for economic growth since the recovery began in mid-2009.

But you shouldn't worry too much. If you dig beneath the headline numbers, the economy, in all likelihood, is still growing at the same slow and steady 2 percent pace that it has been for the whole recovery.

One way to tell, as Council of Economic Advisers Chair Jason Furman points out, is that total hours worked increased as much as you'd expect if the economy were growing 2 percent, and not contracting 2.9 percent. As you can see below, there's normally a pretty strong relationship between the increase in hours and the increase in GDP, but there wasn't this past quarter. That's probably just a statistical blip.

So why was growth so low? Well, there was a perfect storm of one-off factors — in the case of the polar vortex, literally so — that temporarily knocked the economy so far off its course.

The two biggest negatives were also the ones that tend to be the most volatile: inventories and exports. Companies didn't need to spend as much money on restocking, and they didn't make as much money on overseas sales. These things happen. They just happened more than usual, and more than we initially thought. Indeed, as you can see below, inventories and exports got revised down a combined 1.8 percentage points of GDP between the first and final estimates.

But the last negative was actually something of a positive — at least in the long run. That was how much lower health-care spending was than expected. See, the first couple GDP estimates looked at how many more people were getting health-care insurance from Obamacare, and assumed that health-care spending would go up quite a bit. It didn't.

Health-care spending actually subtracted 0.16 percentage points from GDP. That's because health-care prices and utilization haven't increased much despite the increase in health-care customers — which is good news for our long-term budget, but bad news for our short-term growth.

Still, this is something of the soft bigotry of a slow recovery's expectations. The economy should be able to withstand some bad weather and bad inventories without falling back into negative territory. And it should be growing faster now to make up for that slower growth before, if the first quarter really was a blip.

Catch-up growth has to show up eventually, right, Godot?