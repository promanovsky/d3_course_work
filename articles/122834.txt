2015 VW Jetta makes global debut in New York

Volkswagen has unveiled the new 2015 Jetta at the New York International Auto Show. The 2015 Jetta gets new front, rear, and interior styling that enhances the design the Jetta is famous for. VW is also adding some optional driver assistance systems as well with blind spot detection, rear cross-traffic alert, frontal collision warning system, and park distance control.

VW also has a new engine available for the car with a 2.0T TDI Clean Diesel EA288 engine available that has up to 45mpg on the highway. VW is also offering a 2.0L naturally aspirated engine and a 1.8T turbo engine. In addition to the new fuel-efficient diesel engine, the car also has improved efficiency with better aerodynamics and a reduction in rolling resistance.

The 2015 Jetta gets a new radiator grille design with three cross fins and can be fitted with optional bi-xenon headlights. LED daytime running lights and GLI and Hybrid models get LED taillights. One of the aerodynamic enhancements for the car is a grille shutter than can be closed to shorten the warm up phase for the engine and can close at other times to improve aerodynamics.

The new EA288 TDI 2.0L engine produces 150hp and is rated for 32 mpg in the city. I already mentioned its 45mpg on the highway rating. It also produces 236 lb-ft of torque from just 1750 rpm. This new engine is also the first engine to make it to the US using the new VW modular diesel system known as MDB.

SOURCE: VW