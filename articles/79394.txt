You may have already been told many times since you were a kid of the benefits of eating fruits and vegetables but if you remain unmotivated to eat enough servings of fruits and vegetables regularly, you might want to inspire yourself that eating fruits and veggies may actually help you live a longer life.

A new study suggests that eating enough servings of fruits and vegetables daily can reduce your risks of dying early but while the World Health Organization (WHO) recommends "five-a-day" servings, the researchers at the University College London in U.K. said that eating five portions of fruits and vegetables a day would only reduce death risks by 29 percent. Consuming seven portions of fruits and vegetables daily, however, can significantly reduce the risks by 42 percent.

The 12-year study "Fruit and vegetable consumption and all-cause, cancer and CVD mortality: analysis of Health Survey for England data" was published in the Journal of Epidemiology and Community Health and involved examination of the eating habits of more than 65,000 individuals in the U.K who were over 35-years old.

"Fruit and vegetable consumption was associated with decreased all-cause mortality (adjusted HR for 7+ portions 0.67 (95% CI 0.58 to 0.78), reference category <1 portion)," study researcher Oyinlola Oyebode from the Department of Epidemiology & Public Health, University College London and colleagues reported. "This association was more pronounced when excluding deaths within a year of baseline (0.58 (0.46 to 0.71))."

The researchers also found that frozen and canned fruits may not be beneficial to health because they actually increase a person's risks of dying by 17 percent. The researchers said that this may be due to the amount of sugar found in many canned fruits.

"Most canned fruit contains high sugar levels and cheaper varieties are packed in syrup rather than fruit juice," Oyebode said. "The negative health impacts of the sugar may well outweigh any benefits."

The researchers have likewise noted that vegetables appear to be more effective in staving off early death than fruit as two to three portions of vegetables per day can lower a person's risks of death by 19 percent compared 10 percent risk reduction associated with consuming the same portions of fruits per day.

TAG fruits, Vegetables, WHO

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.