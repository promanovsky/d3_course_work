Samsung's new tablet, the Galaxy Tab S, is promoted as the company's "thinnest and lightest tablet to date." It's no secret that the world leader in the smart phone market is making moves to overtake competitor Apple when it comes to tablets. That being said, here are a few questions to keep in mind regarding the latest device to hit the tablet market.

What features will the new tablet include?

The Galaxy Tab S will include a fingerprint-reading sensor and Super AMOLED screen, the first tablet device to have such a high-resolution screen, according to Samsung. AMOLED screens use more pixels than traditional LCD screens, resulting in a more realistic image and, because they don't require backlights, use less energy than LCD screens. This tablet will be 0.2 inches thick and will come in two sizes, one with a 10.5-inch display and the other with a 8.4-inch display. (For comparison, the iPad Air has a 9.7 inch screen.)

"The tablet is becoming a popular personal viewing device for enjoying content, which makes the quality of the display a critical feature,” says JK Shin, chief executive and president of Samsung's IT & mobile division, in a release. “With the launch of the Galaxy Tab S, Samsung is setting the industry bar higher for the entire mobile industry. It will provide consumers with a visual and entertainment experience that brings colors to life, beautifully packaged in a sleek and ultra-portable mobile device.”

Where is Samsung in the tablet market?

Apple still claims the top spot as far as tablets are concerned. But Samsung is gaining. Samsung's percentage of the global tablet market share climbed to 22.3 percent in the first quarter of 2014, up from 17.9 percent in first quarter 2013, according to market intelligence firm IDC. Apple, meanwhile, has seen a steady decline in the tablet market, decreasing from 39.6 percent to 32.5 percent, according to the same data from IDC.

However, IDC predicts the overall tablet market to slow in 2014, due in part to the rise of "phablets," smart phones with screens of 5.5 inches or larger, and the fact that people are keeping their tablets longer than originally expected. As a result, IDC estimates that tablet shipments will increase this year by 12 percent, rather than the 52 percent bump seen in 2013.

In a May release, Tom Mainelli, program vice president for devices and displays at IDC, elaborated on the effect large-screen smart phones is having on the tablet market, noting that they are "causing many people to second-guess tablet purchases as the larger screens on these phones are often adequate for tasks once reserved for tablets."

Of course, Samsung is no stranger to "phablets." Its popular Galaxy Note smart phone averages between five and six inches in screen size.

What does this all mean for consumers?

Well, it's not likely to cause any major shift in how people approach tablets, according to Tuong Nguyen, an analyst with the technology research firm Gartner.

"Hardware is table stakes at this point," Mr. Nguyen says. "It's about services and applications."

In a market moving increasingly toward connections among products — evident in Apple's emphasis on connectivity between its desktop, tablet, and smart phone — Nguyen says updates to tablet hardware and design will not sway public opinion on products in the same way as new services, such as being able to control household devices with electronics, or the "Internet of things."

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

“Ultimately, consumers like something that looks nice, but they want something that fits their needs and applications,” he says.

The Galaxy Tab S will be available in select markets starting in July.