'More than three million deaths worldwide in 2012 were due to harmful use of alcohol' Picture posed

More than three million deaths worldwide in 2012 were due to harmful use of alcohol – and Europe is the area with the highest consumption of alcohol per capita, according to a report.

Alcohol consumption can not only lead to dependence but also increases the risk of developing more than 200 diseases including liver cirrhosis and some cancers.

A World Health Organisation (WHO) report found harmful use of alcohol led to 3.3m deaths in 2012.

Some countries in Europe have particularly high consumption rates, it added. Consumption level is stable over the last five years in Europe, Africa and the Americas, though increases have been reported in South-east Asia and the western Pacific region.

Dr Oleg Chestnov, WHO assistant director-general for non-communicable diseases and mental health, said: "More needs to be done to protect populations from the negative health consequences of alcohol consumption.

"The report clearly shows that there is no room for complacency when it comes to reducing the harmful use of alcohol," he said.

Belfast Telegraph