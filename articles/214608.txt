Jupiter's Great Red Spot — the most powerful storm in the solar system — is at its smallest observed size yet, and scientists aren't sure why.

Recent Hubble Space Telescope images of the storm show that it is now 10,250 miles (16,496 kilometers) across, which is less than half the size of the storm in the late 1800s. At one point, scientists theorized that three Earths could fit inside the Great Red Spot, but today, only the width of one Earth could fit within the raging tempest. You can watch a Great Red Spot video for more views of the diminishing storm.

As the spot diminishes, its shrinkage rate appears to be accelerating. Amateur observations from 2012 show the storm's "waistline" is reducing by 580 miles (933 km) a year, a little less than the driving distance from New York City to Cincinnati. [Jupiter's Great Red Spot: Photos of the Solar System's Biggest Storm]

Nobody knows for sure why the Great Red Spot is getting smaller.

"One possibility is that some unknown activity in the planet's atmosphere may be draining energy and weakening the storm, causing it to shrink," Hubble officials wrote in a statement.

While the storm has been observed since the 1600s, astronomers didn't discover the "downsizing" until 1930. The spot was estimated at 25,500 miles (41,038 km) across in the late 1800s. A century later, the Voyager 1 and 2 flybys of Jupiter in 1979 revealed the spot's longest axis had shrunk to 14,500 miles (23,336 km).

Hubble has tracked the shrinkage since arriving in Earth's orbit in the 1990s. A 1995 image showed the storm was about 13,020 miles (20,953 km) across, but by 2009, that had diminished to 11,130 miles (17,912 km).

"In our new observations, it is apparent that very small eddies are feeding into the storm," Amy Simon, associate director for strategic science at NASA's Goddard Space Flight Center, said in a statement. "We hypothesized that these may be responsible for the accelerated change, by altering the internal dynamics and energy of the Great Red Spot."

Story continues

A large storm on at least one other planet changed or disappeared in recent decades. The Voyager 2 spacecraft also captured images of a "Great Dark Spot" on Neptune during a flyby in 1989. The storm was not visible to Hubble when the telescope examined the planet in 1994.

Hubble astronomers, including Simon, will take part in a webcast about Jupiter's shrinking Great Red Spot on May 22 at 4 p.m. EDT (2000 GMT). Learn more about the live webcast here: https://www.youtube.com/watch?v=9coSaxpQ8DQ

Follow Elizabeth Howell @howellspace. Follow us @Spacedotcom, Facebook and Google+. Original article on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.