A member of the Garda forensics team at the scene on Fassaugh Avenue, Cabra, north Dublin, where a man was found seriously injured outside the Cabra House pub.

Four cases of infection by the deadly Ebola virus have been confirmed in Conakry, Guinea's Health Minister Remy Lamah said on Thursday, marking the first confirmed spread of the disease from rural areas to West African state's capital.

The minister said the virus appeared to have been transmitted by an old man who showed symptoms of haemorrhagic fever after visiting Dinguiraye in central Guinea, far from the identified outbreaks of Ebola in the remote southeast.

Four of the man's brothers, who attended his funeral in the central town of Dabola, started to show the same symptoms and were tested for Ebola on their return to Conakry.

"The four tested positive," Lamah told Reuters. "They have been placed in an isolation ward in Donka hospital." The old man's family has also been quarantined, the minister said.

The spread of the disease to Conakry, a city of some 2 million people, marked an escalation in the Ebola outbreak in Guinea, which ranks as one of the poorest nations on earth despite rich deposits of bauxite and iron ore. As of Wednesday, 63 deaths had been reported from suspected cases of infections.

Online Editors