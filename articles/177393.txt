Zee Media Bureau

Washington: In a significant finding, scientists have developed a new low-cost, efficient and environment-friendly solar cell that can be used without hazardous material lead.

The breakthrough discovery, which is being seen as a “next big thing” in the photovoltaic industry, was carried out by the researchers at Northwestern University.

The new solar cell uses tin instead of lead as the harvester of light.

“This is a breakthrough in taking the lead out of a very promising type of solar cell, called a perovskite. Tin is a very viable material and we have shown the material does work as an efficient solar cell,” said Mercouri G. Kanatzidis, an inorganic chemist at Illinois-based Northwestern University.

Northwestern University researchers become the first to create the solar cell that uses a structure called a perovskite but with tin instead of lead as the light-absorbing material.

The researchers said while lead perovskite has achieved 15 percent efficiency, tin perovskite should be able to match and possibly surpass that.

The researchers developed, synthesised and analysed the material.

Kanatzidis then turned to Northwestern collaborator and nanoscientist Robert PH Chang to help him engineer a solar cell that worked well.

“Our tin-based perovskite layer acts as an efficient sunlight absorber that is sandwiched between two electric charge transport layers for conducting electricity to the outside world,” said Chang, a professor of materials science and engineering.

Solar energy is free and is the only energy that is sustainable forever.

“If we know how to harvest this energy in an efficient way we can raise our standard of living and help preserve the environment,” added Kanatzidis.

The finding has been published in the journal Nature Photonics.

With Agency Inputs