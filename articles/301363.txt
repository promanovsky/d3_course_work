BOSTON (CBS/AP) – Last fall, WBZ introduced you to the “bionic pancreas”, and now new research published in the New England Journal of Medicine shows it can do what it was designed to do.

Ed Damiano is an Assistant Professor of Biomedical Engineering at Boston University. He has spent years worrying about his 15-year-old son, David, who has Type 1 Diabetes, and sometimes develops low blood sugar at night, which can be deadly. “He sleeps through any particular disturbance you can muster, including low blood sugar,” says Damiano.

So he and a colleague developed the “bionic pancreas”, which uses a sensor, an iPhone app, and a pump to continuously monitor blood sugar levels and deliver one of two drugs…insulin, which lowers blood sugar, or glucagon which raises it. Damiano says the pump works 24/7. “This device is a relentless device. It doesn’t take a vacation. It doesn’t go to sleep at night.”

This latest study out of Mass. General Hospital and Boston University looked at 52 adults and teenagers and found that even when patients didn’t have to restrict their carbs or sugar intake, they maintained good glucose control.

Dr. Steven Russell is a diabetes expert at Mass. General and is collaborating with Damiano on this project. He says the bionic pancreas not only kept blood sugar levels from rising too high, but also from falling too low. “It lowered the blood glucose in all of the subjects to the level that would meet the recommendations to avoid diabetic complications,” says Dr. Russell.

Ariana Koster of Brookline has had Type 1 Diabetes since age eleven and is getting outfitted with the bionic pancreas for the next phase of testing. It’s her birthday and she’s looking forward to extra dessert. “Just not having to worry about diabetes for the night,” she says. “It’s going to be a wonderful feeling.”

Michael McCabe of Lowell was diagnosed with Type 1 Diabetes six years ago. He says a device like this would change his life. “It’s almost like being completely healthy again,” McCabe says.

Phase two of the trial began today and should wrap up in less than a year. Then larger clinical trials will follow. If all goes well, the bionic pancreas could be available to the public within four years.

And if you’re wondering if something like this could help patients with Type 2 Diabetes, the researchers say it’s certainly possible, and they hope to look more closely at that in the future.

THREE PARTS

It has three parts: two cellphone-sized pumps for insulin and sugar-raising glucagon, and an iPhone wired to a continuous glucose monitor. Three small needles go under the skin, usually in the belly, to connect patients to the components, which can be kept in a fanny pack or a pocket.

Patients still have to prick their fingers to test blood sugar twice a day and make sure the monitor is accurate, but the system takes care of giving insulin or glucagon as needed.

Kristina Herndon said her 13-year-old son, Christopher, “loved it” when he tried it for the study, and “felt pretty badly giving it back” when it ended. Christopher has to check his blood sugar eight to 10 times a day and his family has to watch him closely in case it dips too low while he sleeps, which can cause seizures or even death.

“It’s a disease that I think people think is not a big deal but it’s tough. It’s hard on a family,” said Herndon, who lives in Newburyport, Massachusetts.

WHAT’S NEXT?

Next steps: A study starts Monday in 40 adults who will use the device for 11 days. By fall, researchers hope to have a next-generation version combining all three components in one device to be tested in studies next year aimed at winning federal Food and Drug Administration approval.

“My goal is to have this device done by the time my kid, who has Type 1 diabetes, goes to college” in about three years, said Ed Damiano.

Two San Diego-based companies — DexCom and Tandem Diabetes Care Inc. — made components of the version tested in the current study. Boston University and Massachusetts General own or have patents pending on the system, and several researchers may someday get royalties.

Medtronic, Johnson & Johnson and several other companies also are working on artificial pancreas devices.

The Boston group’s work is exciting and the results are compelling, but there still are practical challenges to bringing a device to market, said Aaron Kowalski, who oversees grants by the Juvenile Diabetes Research Foundation on artificial pancreas development.

“Most people with diabetes want less devices in their lives, not more,” so putting the components into a single automated system is key, he said.

WBZ-TV’s Dr. Mallika Marshall and AP Chief Medical Writer Marilynn Marchione contributed to this report.

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)

MORE LOCAL NEWS FROM CBS BOSTON

[display-posts category=”local” wrapper=”ul” posts_per_page=”4?”]