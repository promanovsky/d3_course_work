A BRITISH national living in Sierra Leone who has tested positive for Ebola would probably be treated at a specialist hospital in London if brought back to the UK, an expert has said.

The unnamed man tested positive for the deadly Ebola virus in the first confirmed case for a UK citizen, the British Department of Health has said.

It has been reported that a decision on whether to transport the Briton here may be taken today, but officials from the Department of Health would not confirm this.

Professor Tom Solomon, director of the Institute of Infection and Global Health at the University of Liverpool, said the "strains" on the healthcare system in Sierra Leone may mean a decision is taken to bring the Briton to the UK.

"We do have facilities in the UK for caring for people with haemorrhagic fevers," he said. "There is a high-level isolation unit at the Royal Free Hospital in London which is very well set up for things like this.

"The medical services in Sierra Leone are very strained at the minute so it may well be the case that this person is brought to the UK for treatment."

Prof Solomon said the chances of the infection spreading are minimal, saying the patient would be transported "in what is effectively a bubble".

According to the World Health Organisation (WHO) the Ebola virus has claimed the lives of some 1,427 people since the disease was identified in Guinea in March and spread to Sierra Leone, Liberia and Nigeria.

The current outbreak is the worst in history with around 2,615 people being infected with the disease.

Despite the continued spread of the disease health experts are keen to stress the overall risk to those in the UK "continues to be very low".

Responding to the news that the first Briton with the disease was being treated, Professor John Watson, deputy chief medical officer, said: "Medical experts are currently assessing the situation in Sierra Leone to ensure that appropriate care is provided.

"We have robust, well-developed and well-tested NHS systems for managing unusual infectious diseases when they arise, supported by a wide range of experts."

Further details on the sex, age and health of the patient were not immediately revealed.

It was confirmed yesterday that an Irish engineer who died at home after returning from working in Sierra Leone had not contracted Ebola.

Dessie Quinn, 43, was being treated for malaria after returning two weeks ago from the west African country and was found dead in bed in Co Donegal by a friend in the early hours of Thursday.

In response to the outbreak Ivory Coast has imposed a ban on flights to Sierra Leone as well as Liberia and Guinea.

In addition Sierra Leone has passed a new law imposing possible jail time for anyone caught hiding an Ebola patient — a common practice that the World Health Organization believes has contributed to a major underestimation of the current outbreak.

Ebola is one of the world's deadliest diseases, with up to 90% of cases resulting in death.

The virus has no cure and rigorous quarantine measures are used to stop its spread, as well as high standards of hygiene for anyone who might come into contact with sufferers.

Symptoms of Ebola appear as a sudden onset of fever, intense weakness, muscle pain, headache and sore throat.

According to the WHO, this is followed by vomiting, diarrhoea, rash, impaired kidney and liver function and, in some cases, both internal and external bleeding.

The effects of the disease normally appear between two and 21 days after infection.

It is transmitted to people from wild animals and spreads in the human population through person-to-person transmission. Outbreaks have a case fatality rate of up to 90%.

The WHO says the disease can be passed between people by direct contact - through broken skin or mucous membranes - with the blood, secretions, organs or other bodily fluids of infected people, and indirect contact with environments contaminated with such fluids.

- Rob Williams, Independent.co.uk

Online Editors