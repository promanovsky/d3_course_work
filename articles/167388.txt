A Motorola Mobility logo is seen on a screen at the public unveiling of their global headquarters in Chicago, Illinois on April 22, 2014

The European Commission, the European Union's competition watchdog, ruled on Tuesday that Motorola Mobility broke EU competition rules by seeking an injunction against Apple on the basis of a smartphone patent.



"The so-called smartphone patent wars should not occur at the expense of consumers," EU Competition Commissioner Joaquin Almunia said in a statement.



"While patent holders should be fairly remunerated for the use of their intellectual property, implementers of such standards should also get access to standardised technology on fair, reasonable and non-discriminatory terms," he said.



"It is by preserving this balance that consumers will continue to have access to a wide choice of inter-operable products."





The European Commission ruled that it was abusive for Motorola to seek an injunction against Apple in Germany on the basis of a patent it had committed to license. Apple had agreed to buy a licence and pay royalties.

It did not to impose a fine on Motorola because there is no case-law by the European Union Courts dealing with the legality of such injunctions and because national courts have so far reached different conclusions on this.Copyright @ Thomson Reuters 2014