A mysterious illness has sickened more than 200 people in Washington state who reported becoming ill after swimming at Horseshoe Lake near Mount Saint Helens, KOMO News reported.

Reports of the unknown sickness surfaced over the weekend, and public health officials are working to determine the cause. Symptoms of the disease include intense stomach pain, vomiting and more.

So far, experts suspect a norovirus might be involved, but testing of the lake’s water is still ongoing. Norovirus is a very contagious virus known for causing widespread illnesses, most recently sickening passengers on the Princess Cruise’s Crown Princess in April and Royal Caribbean’s Explorer of the Seas in February. People can get infected from contaminated food or water, which causes their stomach or intestines to become inflamed, leading to abdominal pain, nausea, diarrhea and vomiting.

While tests are pending, officials for the Kitsap Public Health District are sanitizing the bathroom facilities near Horseshoe Lake County Park as a precaution. Park officials have also closed off access to the popular swimming site and have posted warning signs around the shore of the lake.

"It's a bit of a bummer for [the kids]," said Doug Chase, the vice president of a nearby summer camp called Crista Camps. "They'd love to be out on the water [with] toys and having a blast, but we feel like the conservative approach is best."

Click for more from KOMO News.