“Unfortunately UCLA was the sink for this water source,” university chancellor Gene Block said, according to the Associated Press.

AD

AD

After the flooding began, authorities swarmed the campus. Tractors dumped mounds of dirt onto the ground in an effort to redirect water away from buildings. Water reached the height of most cars’ wheelwells. Police evacuated nearby parking garages. And firefighters, some using inflatable boats, rescued at least five people stranded in structures where their vehicles were stuck, according to news reports.

Although many fled, some students calmly abandoned their shoes and socks waded across campus in ankle-deep water with book bags in tow.

A few reached for their boogie boards.

“That is probably one of the most dangerous things you can do,” Los Angeles Fire Department Capt. Jaime Moore told the Times. “For somebody to try and boogie board in this, it’s just going to be an asphalt bath.”

AD

To reach the water main break, workers had to confront Los Angeles’s infamous rush-hour traffic, Jim McDaniel, an L.A. Department of Water and Power senior assistant general manager, told the Times. Then, once they got there, they had to figure out which valves needed to be shut down. Closing the wrong valves, he told the newspaper, would have left many people without water.

The main line, which delivers 75,000 gallons a minute, burst just before 3:30 p.m. It was finally shut off at about 7 p.m.