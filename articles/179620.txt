Parents in search of better-behaved children at the dinner table may need to simply do one thing: cut up their food.

That's the advice of researchers from Cornell University, who observed 12 elementary-level school children during a 4-H summer camp and found when the children aged 6-10 years ate foods they had to bite with their front teeth -- such as drumsticks, whole apples, or corn on the cob -- they seemed more agitated than when the same foods were cut up.

"They were twice as likely to disobey adults and twice as aggressive toward other kids," said Brian Wansink, professor and director of the Cornell Food and Brand Lab.

Findings from the two-day study have been published in the journal Eating Behaviors.

On the first day of the study, half the child subjects were seated at one picnic table and given chicken on the bone that had to be bitten into with their front teeth; the other half of the children were seated at a nearby picnic table and doled out chicken cut into bite-sized pieces. The conditions were reversed the second day.

During each studied eating session, camp counselors directed the children to stay inside a circle with a 9-foot radius.

Both meal sessions were videotaped and evaluated by behavioral specialists able to determine how aggressive or compliant the children appeared to be, and noted if the subjects exhibited any atypical behaviors, such as jumping and standing on the picnic tables.

The observations from both the counselors as well as coders indicated that when children were served chicken on the bone, they acted twice as aggressively and were twice as likely to disobey adults, compared to when they were served bite sized pieces of chicken.

Also, the children served chicken on the bone left the circle without permission more frequently and were more prone to jump and stand on the picnic tables.

The study concluded that when children need to bite into food with their front teeth, they are more likely to misbehave.

So, "if you want a nice quiet, relaxing meal with your kids, cut up their food," Wansink said. Otherwise, "if drumsticks, apples, or corn on the cob is on the menu -- duck!"