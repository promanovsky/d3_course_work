From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

After pulling the trigger a little early, Amazon today formally announced the launch of Kindle Unlimited, an ebook and audiobook subscription service that costs $10 per month.

As we detailed yesterday, this launch pits Amazon against services like Oyster and Scribd, both of which offer a Netflix-esque unlimited service for ebooks.

On launch, Amazon’s catalog of books is understandably large. Amazon says subscribers will have access to over 600,000 books and “thousands of audiobooks”; that’s 100,000 more books than Oyster promotes and 200,000 more than Scribd.

Unfortunately, the service is only available to U.S. residents for now. Amazon advertises that its new service offers “hundreds of thousands of books you won’t find anywhere.”

According to Scribd chief Trip Adler, Amazon’s new subscription service “is exciting for the industry as a whole. It’s validation that we’ve built something great here at Scribd. Successful companies don’t fear competition, but rather embrace it, learn from it, and use it to continue to fuel their own innovation, which is exactly what we intend to continue doing.” VentureBeat has reached out to Oyster for comment on the matter.

Amazon says Kindle Unlimited is available now on Kindle devices, iOS, Mac, Android, and Windows Phone.