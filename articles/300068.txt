Pop star Katy Perry announced the launch of Metamorphosis Music, her new record label launched in conjunction with Capitol Records. Perry also announced the label’s first signed artist, newcomer Ferras.

Ferras’ first self-titled EP was digitally released today. Perry duets with Ferras on one song, the Greg Wells-produced “Legends Never Die”, and also serves as the album’s executive producer.

See video: Katy Perry Super Fan Knows More About Katy Perry Than Katy Perry

“Ferras is a man with a message, and a unique lyrical way of communicating his perspective on life that makes you feel connected to every note when you listen,” Perry states. “‘Speak in Tongues’ is a song with raw emotional power that I wish I wrote. I believe he is going to be an important artist to watch unfold. I am SO ecstatic the world gets to hear his music now both online and on tour with me.”

The two artists have known each other since both signing with Capitol in 2007.

Ferras will also join Perry on the North American leg of her Prismatic World Tour, which kicks off June 22nd. “I think of every song as having its own world,” Ferras says of his work. “I try not to categorize it; it’s a pop record with many different textures, but it’s definitely dark in places.”