Samsung Electronics Co Ltd on Tuesday issued unexpectedly weak quarterly earnings guidance which put it on track for its worst results in two years and cast doubt on the smartphone leader's strategy against cheaper Chinese rivals.

Though the South Korean company said it saw better business conditions in the third quarter, it faces slowing market growth, intensifying price competition at the lower end and the looming threat of Apple's next iPhone.

"The earnings deliver a harsh reality check to Samsung that it is not Apple, but Samsung. Its strategy of selling phones at expensive prices will not work anymore, as Chinese rivals also offer good enough phones at much cheaper prices," Lee Seung-woo, a technology analyst at IBK Securities, said.

"Samsung needs to review its smartphone strategy," he said.

While smartphones drove Samsung to record profits last year, the market is maturing. Research firm IDC predicts global shipments growth will slow to 19.3 percent this year from 39.2 percent in 2013, while average sales prices will also drop.

(Also see: 2014 Smartphone Sales to Top 1.2 Billion Units: IDC)

Some analysts said Samsung may have no choice but to slash prices for mid-to-low tier devices, where growth is stronger, to go after Chinese rivals such as Huawei Technologies Co Ltd and Lenovo Group Ltd. While that would help defend market share it would also hurt margins, curbing its earnings recovery in the short term.

The company said it "cautiously expects" a better third-quarter outlook with the release of a new smartphone lineup, lower marketing costs and a seasonal lift in demand for its memory business. Its flagship Galaxy Note 4 is expected to hit the market in September.

Samsung's second-quarter guidance was well below the mean forecast of 8.3 trillion won from a Thomson Reuters I/B/E/S survey of 38 analysts.

CIMB analyst Lee Do-hoon, one of the few to accurately predict Samsung's second-quarter guidance, said the company appeared to be taking longer to address challenges it is facing in the mid-to-low end of the market. He did not expect a meaningful third-quarter earnings recovery and predicted that analysts would cut their forecasts.

"Samsung has been talking about strengthening its mid-to-low tier lineup but we found that this has not happened yet and will take more time," he said.

Shares in Samsung, down more than 10 percent since the start of June through Monday, rose as much as 2.1 percent after the earnings guidance, as some analysts said the second-quarter results may mark a low point in earnings. The stock later gave back much of that gain.

Sagging profits

Samsung estimated on Tuesday that its April-June operating profit likely fell 24.5 percent from a year earlier to 7.2 trillion won ($7.12 billion), the sharpest percentage drop since the first quarter of 2011 and the weakest level since a 6.5 trillion won profit in the second quarter of 2012.

The figure, which marks the third straight quarter of annual profit decline, was far below market expectations as sales fell for the first time since the company adopted new accounting standards in 2009.

In a separate statement, Samsung said second-quarter earnings would be hit by slower global smartphone market growth, competition in China, inventory buildup in Europe and the strength of the won, which appreciated by around 9 percent on average against the dollar during the second quarter.

But analysts on average expect the streak of on-year profit declines to extend into the third quarter, with Apple widely tipped to launch a successor to the iPhone 5 to compete with Samsung's high-end smartphones. The prior year's record 10.2 trillion won profit could also be difficult to beat.

"Samsung is most competitive in the mid-to-high end products, but market demand is being driven in the lower-tier end where the biggest issue is price," said HMC Investment analyst Greg Roh.

"I think problems for the company's mobile division will continue in the third quarter."

© Thomson Reuters 2014