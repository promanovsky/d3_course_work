Few of the facts in the case were in dispute but the prosecution alleged different levels of involvement by the three accused.

The State said that Anglo’s former Head of Irish Lending Pat Whelan was most active in arranging the Maple deal. He approached the ten and drafted the associated documentation.

According to prosecuting counsel he was “up to his neck in it”, although Mr Whelan’s counsel insisted he was merely following the instructions of his CEO.

During his voluntary garda interviews Mr Whelan said that both he and Anglo were comfortable with the lending to the Maple Ten as it was “in the ordinary course of business” and the bank’s compliance department had raised no objection.

He told gardaí that the Financial Regulator was fully aware of the deal and supported it.

“They seemed to have a hotline with each other,” he told gardaí.

He said that the Maple Ten deal may seem unwise in hindsight but that it was required to “keep the bank alive” during a difficult period.

He said he believed the loans were made in the ordinary course of business and therefore not illegal.

The prosecution said that former Director of Finance William McAteer was slightly less involved in the deal

On July 8, 2008 he told Chief Financial Officer Matt Moran about the Maple Plan and the next morning went through the details of the deal with Mr Moran and Head of Compliance Fiachre O’Neill.

He told investigators that he had limited involvement in the Maple deal but that he didn’t have an issue with the bank lending for the purchase of its own shares. He said he wasn’t sure if he signed off on the loans and that he might have been on holidays at the time.

He said he knew the Maple Ten were receiving loans from Anglo but that he was not aware Anglo was also funding the Quinn family’s purchase of the shares.

The accused said that then Anglo CEO David Drumm decided the candidates and that it would have been the director of lending who decided to loan to them.

The accused insisted that he “certainly wasn’t instrumental” but that he was “certainly involved in carrying out the transaction”. He said he travelled to the Middle East and spoke to Rabobank and the Financial Regulator when trying to find solutions to Mr Quinn’s control of the bank.

He said that he had no involvement in the lending to the Maple Ten and that there were meetings and phone calls that he wasn’t involved in.

When gardaí again asked if he instructed others in the transactions, Mr McAteer replied that he “certainly instructed people to make sure everything was done properly.”

Least involved was former Chairman Sean FitzPatrick, who was acquitted on all charges on Wednesday. He was aware of the Quinn issue from meeting Mr Quinn in September 2007 and from a second meeting in March 2008. As chairman of the bank he was present for all board discussions of the Quinn problem.

He was told over the phone by Mr Drumm about the Maple plan and informed that Anglo would be lending to ten wealthy customers to buy shares. Mr FitzPatrick told gardaí that Mr Drumm never told him the ten’s identities as he “was keeping it very very tight.”

Irish Independent