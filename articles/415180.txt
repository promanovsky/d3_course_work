Richard Attenborough, who was well-known as an actor and a director, died on Sunday at the age of 90.

Attenborough launched his 60-plus-year career in film in 1942 and landed kudos for his performances in "In Which We Serve" and "Brighton Rock." He made his directorial debut in 1969's "Oh! What a Lovely War" and went on to direct "Young Winston," "A Chorus Line," and his most critically acclaimed films "Gandhi," "Cry Freedom" and "Shadowlands."

Steven Spielberg—who directed Attenborough in "Jurassic Park"--said of him, "Dickie Attenborough was passionate about everything in his life — family, friends, country and career. [. . .] He was a dear friend and I am standing in an endless line of those who completely adored him."

Attenborough, who had been in frail since a fall in 2008, is survived by his wife and their son and daughter.

For comments and feedback contact: editorial@rttnews.com

Entertainment News