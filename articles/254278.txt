BLOGS OF THE DAY: Jim Carrey gets an honorary degree



Jim Carrey received a degree from Maharishi University of Management in Iowa

In a commencement speech this weekend, Jim Carrey said that witnessing his father struggle as an accountant inspired him to make the riskier choice of going into comedy.

"My father could have been a great comedian but he didn't believe that that was possible for him, and so he made a conservative choice. He got a safe job as an accountant," Carrey told graduates of the Maharishi University of Management in Iowa.

Carrey received the honorary degree of Doctor of Fine Arts Honoris Caus during the graduation ceremony.



Carrey said his father, Percy Carrey, lost his accounting job when the actor was 12, and the family then "had to do whatever we could to survive".

"I learned many great lessons from my father, not the least of which was that you could fail at what you don't love, so you might as well take a chance on doing what you love," the 52-year-old actor told the graduates. - eonline.com

'X-Men' tops weekend



Fox's X-Men: Days of Future Past had the second-biggest (three-day) opening in the franchise's history this weekend, debuting with an estimated $90.7 million to easily top the domestic box office.

That's $12 million less than 2006's X-Men: The Last Stand's opening, but $45 more than X-Men: First Class bowed with in 2011.



Internationally, the Bryan Singer-directed sequel - which brings together both the original and First Class casts - earned about $171.1 million to brings its global income to $261.8 million. - ign.com

'The Gunman' set for '15

A UK and US release date for Sean Penn's The Gunman has been announced. The military thriller is slated to hit cinemas on February 20, 2015.



Penn is starring as an ex-special forces soldier and military contractor who must chase across Europe in order to clear his name.



The action thriller will be directed by Pierre Morel (Taken and From Paris with Love).

