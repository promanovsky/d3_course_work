Box-Office Preview: 'Maleficent' Set to Curse 'Million Ways to Die in the West'

UPDATED: Angelina Jolie's live-action fairy tale is tipped to win the weekend with $60 million while Seth MacFarlane's R-rated Western is tracking to open in the $25 million range.

Marking the summer's first tentpole for females of all ages, Disney's Maleficent has a strong shot at crossing $60 million in its domestic launch and pushing holdover X-Men: Days of Future Past out of the top perch at the box office. The live-action fairy tale -- a passion project for star Angelina Jolie -- also makes a major push overseas.

PHOTOS: 35 of 2014's Most Anticipated Movies

Seth MacFarlane also enters the fray with A Million Ways to Die in the West. He both directed and stars in the R-rated Western comedy, which is eyeing a debut in the $25 million range, well behind the $54.4 million launch of MacFarlane's Ted on the same weekend two years ago (Ted went on to become the top-grossing original R-rated comedy of all time, earning $549.4 million globally) or the $49 million opening of fellow Universal comedy Neighbors three weeks ago.

Million Ways to Die is expected to come in No. 3 after Maleficent and Fox's Days of Future Past, which is expected to earn $38 million to $40 million in its second outing after opening to a dazzling $110.7 million over Memorial Day weekend.

Both Maleficent and Million Ways to Die have received decidedly mixed reviews (Maleficent is faring better).

Box-office observers believe Maleficent -- featuring Jolie as the infamous sorceress from Sleeping Beauty -- will benefit greatly from targeting girls and moms, as well as families, considering the glut of male-skewing tentpoles in the marketplace (Days of Future Past, Godzilla and The Amazing Spider-Man 2). Elle Fanning plays Princess Aurora in the 3D movie, which comes on the heels of Disney's femme-fueled global Frozen and marks Jolie's first appearance in theaters in four years.

FILM REVIEW: 'Maleficent'

Still, Maleficent won't match the $116 million debut of Alice in Wonderland in March 2010 or the $79.1 million opening of Oz the Great and Powerful in March 2013. Those two Disney movies, along with Maleficent, were produced by veteran Hollywood player Joe Roth.

Overseas, Maleficent opens in every major territory this weekend save for China and Japan. All told, it will be playing in 75 percent of the foreign marketplace.

Maleficent is not a cheap proposition, having cost $175 million to produce after reshoots (there was reportedly tension between Jolie and first-time feature director Robert Stromberg).

Another big-budget tentpole, Tom Cruise's Edge of Tomorrow, likewise begins rolling out overseas this weekend a week ahead of its June 6 domestic launch.

Million Ways to Die cost only $40 million, far less than Maleficent. Universal and Media Rights Capital reteamed to make the movie, which marks MacFarlane's first turn in a leading role. The anachronistic Western also stars Charlize Theron, Liam Neeson, Amanda Seyfried, Giovanni Ribisi, Sarah Silverman and Neil Patrick Harris.

STORY: From 'Maleficent' to 'Hercules,' Summer's 5 Biggest Box-Office Risks

Even though MacFarlane boasts an enthusiastic, heavily male fanbase, Westerns are an inherently tough sell. Universal points out that an opening north of $20 million would be a victory, considering the film's budget. Overseas, the R-rated comedy debuts day-and-date in 22 foreign territories.

Elle Fanning's sister, Dakota Fanning, has her own film opening this weekend, Kelly Reichardt's indie drama Night Moves. The film, debuting in New York and Los Angeles, co-stars Jesse Eisenberg and Peter Sarsgaard.

James McAvoy's Scottish crime dramedy Filth also opens at the specialty box office.