The Ebola epidemic in three West African countries has already infected 1,100 people and killed more than 670, making it the largest on record. Ebola is a virulent, contagious disease for which no cure has been discovered, and it usually has a fatality rate of 90 percent. That the death toll in the current outbreak hasn't reached that terrible extreme attests to the courageous efforts of doctors and other health workers in Guinea, Liberia, and Sierra Leone, who have been racing to get care to the sick and prevent the virus from spreading.

Dr. Kent Brantly is one of two American aid workers who have contracted the virus.

So it is especially poignant that medical workers have been among the victims in this crisis. One of Liberia's highest-profile physicians, Dr. Samuel Brisbane, died of Ebola fever on Saturday; three nurses at an Ebola treatment center in Sierra Leone have died as well. Dr. Sheik Umar Khan had been heading Sierra Leone's response to the outbreak, until he was stricken with Ebola himself. He died Tuesday.

Advertisement

Now two Americans, both working for the Christian humanitarian group Samaritan's Purse, have fallen ill. Dr. Kent Brantly, the medical director of the mission's Ebola center in Monrovia, and Nancy Writebol, a staff member at the Monrovia site, contracted the virus and are receiving intensive care.

All who work to save lives are to be admired. But those who do so despite the risk to their own lives are the most extraordinary and idealistic of heroes.