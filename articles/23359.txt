Last week, GlobalPost told you about an effort to crowd-source the search for MH370 using satellite imagery. DigitalGlobe, a US satellite imagery company based in Colorado, made available an easy-to-use web platform called Tomnod, which allows users to scan the search area, square by square, tagging anything that might be wreckage or an oil slick.

More than three million people have used Tomnod to help search for the plane, DigitalGlobe says.

One of those three million: Courtney Love, rock musician and wife of the late Nirvana frontman Kurt Cobain. And she thinks she may have found it.

Love could have just submitted the tagged map in Tomnod. But instead she marked it up with MS Paint (presumably) and posted it on social media. And so it begins.

The internet response to Love's find has been exactly what you'd expect. Some people have been encouraging.

Most have been trolls and cyber bullies.

Let's be real folks. Courtney Love was looking for her crack pipe and found a plane instead. #sorrynotsorry — Sebby (@whyalwayssebby) March 18, 2014

But really, aren't we all thinking the same thing?

Just imagine if Courtney Love did actually find that plane. — Cath Hurley (@CATHFACTORY) March 17, 2014

Stick with it Courtney. Haters be damned.