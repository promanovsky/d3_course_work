Zee Media Bureau

Washington: Mathias Dopfner, the boss of Europe`s largest newspaper publisher, has accused Google of abusing the monopoly in digital market, discriminating other search engines and building up a `superstate`.

Dopfner sent a letter to Google`s Eric Shmidt, saying that the US company was discriminating against the competitors, and stated Google`s motto as `pay us or be finished`.

Dopfner, chief executive of Axel Springer, admitted that even he was scared, because his company too relied on Google.

He added that Google knew everything about their customers, and all the private and business e-mails were also read and analysed by Gmail when needed.

Mr Dopfner`s letter is published in Wednesday`s edition of the Germany`s Frankfurter Allgemeine Zeitung newspaper, which is not part of the Springer stable, in response to a column by Google`s Executive Chairman, Eric Schmidt.

With Agency Inputs