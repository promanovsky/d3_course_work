Roku announced their new Roku Streaming Stick earlier this month, the device is similar to the Google Chromecast and it connects to your TV via HDMI.

The Roku Streaming Stick has now started shipping out to customers, the device is now also available to buy from a number of retailers for $49.

This new streaming device from Roku connects to your TV via HDMI, this is the companies second streaming stick, and it comes with a number of new features over the previous version.

The Roku Streaming Stick offers support for both HD (720p) and Full HD (1080p) resolutions, it also comes with 5.1 and 7.1 channel audio and features 802.11 WiFi.

On top of that the device comes with a remote control, and it will work without the need for a smartphone, tablet or computer.

It comes with around 1200 internet channels and apps, and offers support from popular streaming services including Netflix, Amazon Instant video and more.

Source Liliputing

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more