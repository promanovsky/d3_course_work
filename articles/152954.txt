Before Samsung’s K Event taking place in Asia a photograph of a new Samsung Galaxy K camera phone has been leaked providing a first look at the new Android powered smartphone camera.

Previously the new camera phone has been referred to as the Galaxy S5 Zoom however it is now being rumoured that Samsung will use the Galaxy K reference for then new smart phone camera when it finally launches.

Unlike the Galaxy S4 zoom camera phone the new Galaxy K seems to be less bulky and more in the style of the smartphone rather than a camera.

The new Galaxy K smartphone camera is rumoured to be equipped with a 20 megapixel sensor and sport a 10x zoom lens and will come equipped with a Samsung Exynos SoC, supported by 2GB of RAM and running Android 4.4.2 KitKat.

Unfortunately no information on pricing or worldwide availability has been leaked or announced as yet by Samsung, but as soon as information comes to light we will keep you updated as always.

Source: Android Central

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more