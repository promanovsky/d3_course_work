NEW YORK, May 8 (UPI) -- Apple is days away from purchasing Beats Electronics from hip hop mogul Dr. Dre for a reported $3.2 billion, the Financial Times reported.

The deal, should it go through, would be Apple's largest ever acquisition.

Advertisement

Beats Electronics is primarily known for its high-end, over-ear headphones, but also includes a music streaming service. The company was founded in 2008 by Dr. Dre and music producer Jimmy Iovine.