On Aug. 25, it'll be 25 years since the Voyager 2 spacecraft flew by Triton, one of the 13 natural moons orbiting the planet Neptune - and only now are we seeing what the encounter probably looked like in living color.

Thanks to the efforts of Paul Schenk, a scientist at the Lunar and Planetary Institute in Houston, the probe's historic footage of the far-off flyby has been used to produce the best-ever global color map of the planet's largest but rather quirky moon, which orbits in an opposite direction of the other moons and Neptune's rotation.

The redrawn map has also been used to develop a video recreation of the space probe buzzing by the moon, an estimated 2.7 billion miles from home, on Aug. 25, 1989, according to a news release issued by the national Aeronautics and Space Administration.

The new Triton map has a resolution of 1,970 feet (600 meters) per pixel, while image colors have been enhanced to bring out contrast, but really nare a close approximation to Triton's natural hues.

The original images captured by Voyager represented Triton in different colors that are generally perceived by human eyes -- orange, green and blue filter images.

In 1989, most of the northern hemisphere of the moon was in darkness and unseen by Voyager, NASA explained; because of the speed of Voyager's visit and Triton's slow rotation, only one hemisphere was able to been seen clearly at the close distance. The rest of the surface was either in darkness or seen as blurry markings back then.

So, the production of the new Triton map and video has spurred greater interest in NASA's New Horizons encounter with Pluto, expected to happen in about a year.

Improvements to the Triton images resulted in greater accuracy of feature locations, sharper feature details and, as already stated, improved colors through better processing.

The new Triton map and movie can be found at: http://www.lpi.usra.edu/icy_moons/.

As far as Voyager milestones go, Aug. 25 also marks the two-year anniversary of Voyager 1 reaching interstellar space.