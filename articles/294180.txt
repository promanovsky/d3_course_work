Oil and gas explorer BG Group Plc (BRGYY.PK,BG.L) said Tuesday that it agreed to sell its equity interest in the Central Area Transmission System or CATS gas pipeline in the UK North Sea and associated infrastructure to Antin Infrastructure Partners for total proceeds up to 562 million pounds, or about $954 million at current exchange rates.

As per the deal, BG Group will sell its 62.78% interest in CATS for a consideration comprising 523 million pounds or approximately $888 million on completion of the transaction and a deferred amount of 39 million pounds or approximately $66 million.

It is anticipated the transaction will close in the second half of 2014.

CATS comprises a fixed-riser platform linked to the Everest oil and gas platform, a 404-kilometre,1 700 million-standard-cubic-feet-of-gas-per-day (mmscfd) capacity subsea pipeline and a two-train onshore gas processing terminal at Teesside which has capacity of 1 200 mmscfd.

The sale of CATS is expected, on completion, to create a post-tax profit on disposal of about $700 million. This may be partly offset by a potential impairment of BG Group's associated UK North Sea assets following the categorisation of the Group's interest in CATS as held for sale in the second quarter, the company said.

For comments and feedback contact: editorial@rttnews.com

Business News