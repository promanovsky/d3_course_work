Defying expectations for a decline, China's exports as well as imports increased in April, vindicating the government's resistance to introduce more stimulus to cushion the recovery.

Exports gained 0.9 percent in April from last year, data published by the General Administration of Customs showed Thursday. Shipments were forecast to fall 2 percent after declining 6.6 percent in March.

Likewise, imports rose 0.8 percent, confounding expectations for a 2.1 percent drop. The increase reversed some of the 11.3 percent fall in March.

The trade surplus more than doubled to $18.46 billion in April from $7.71 billion in March. The figure also exceeded the $17.7 billion surplus forecast by economists.

Looking ahead, healthy demand in external should continue to support exports, Julian Evans-Pritchard, China economist at Capital Economics said.

Moreover, because Chinese officials appear to have cracked down on over-invoicing around this time last year, the distortions that have pulled down headline export growth look set to fade, he noted.

Further, the economist said the will post large trade surpluses this year as import growth is set to remain relatively weak on subdued commodity imports.

The government targets 7.5 percent GDP growth for this year but resisted calls for additional stimulus to achieve short-term goals.

The Organization for Economic Co-operation and Development forecast China's growth to slow with measures to phase out excess industrial capacity amid investment remaining weak in response to tighter credit conditions.

The Paris-based organization estimated 7.4 percent growth for China this year and 7.3 percent in 2015.

After consistently withdrawing funds for three straight months since the end of the Chinese Lunar New Year holidays, the People's Bank of China injected net CNY 91 billion last week to meet cash demands ahead of the 'May Day' holiday. Nevertheless, the central bank drained CNY 60 billion yuan from the financial system this week.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.