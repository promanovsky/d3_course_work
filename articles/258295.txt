The happy couple kiss in front of a wall of white roses at their Italian ceremony[E! NEWS]

FREE now for the biggest moments from morning TV SUBSCRIBE Invalid email Sign up fornow for the biggest moments from morning TV When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

It was the most eagerly anticipated celebrity wedding of the year. Since the moment TV viewers watched Kanye get done on one knee on Keeping up With the Kardashians the public have been entranced by the famous duo. And one of the biggest questions on people's lips was - what dress will the beautiful bride wear? There were rumours that the curvy brunette would have three costume changes for the ceremony held in Forte di Belvedere in Florence, but in the end she opted for just two stunning creations.

The reality TV star stunned in a white, Givenchy Haute Couture lace gown and a long silk veil [E! NEWS]

And these beautiful photos, of the newlyweds kissing in front of a wall of white roses, give us our first full glimpse of Kim's first elaborate wedding gown. The reality TV star stunned in a white Givenchy Haute Couture lace gown and a long silk veil during the ceremony, while Kanye was fitted in a custom Givenchy tuxedo. E! News also obtained a picture from the bride's final dress fitting at the Givenchy atelier in Paris with Riccardo Tisci. The picture shows Kim trying on her huge embellished veil in the tight-fitting backless fishtail dress.

The couple were pictured in matching leather jackets as they posed inside a photo booth [E! NEWS]

The high-necked gown features long lace sleeves and flattering panel-mesh sides which cinched in at her tiny waist. Her now-husband looked dapper beside her in a classic black tuxedo with a white shirt and dark brown bow tie. After the nuptials, the pair and their loved ones partied at the reception, which included a four-course meal and seven-tiered wedding cake. Kim then changed into a short, cream-coloured, custom Balmain dress as she listened to a 20-minute speech from her new husband.

Kim changed into a short, cream-coloured, custom Balmain dress after the ceremony [E! NEWS]

After the couple were serenaded by John Legend, who sang All of Me for their first dance, they were pictured in matching leather jackets as they posed inside a photo booth. But it wasn't just the wedding day where the fashion was expertly choreographed, Kim's pre-wedding wardrobe also did not disappoint. The star has been wowing in a series of figure-flattering outfits all the way from Los Angeles to Italy.

The star wore a $20,000 petal-encrusted Blamain dress for her hen do [INSTAGRAM]

While most brides would be lounging around in a future-Mrs dressing gown the business women took a more high-end approach. The brunette beauty made at least three outfit changes a day in the week leading up to the wedding. From her $20,000 petal-encrusted Balmain dress to the high-maintenance camel suede dress by Italian designer Ermanno Scervino, her pre-wedding wardrobe was excellently managed.

She stunned in a camel suede dress by Italian designer Ermannoo Scervino [GETTY]

The star opted for the arty rose embellished pearl dress for her hen do. Kim and a group of her closest friends celebrated her last night of freedom in style as the feasted on snails at Costes - the finest restaurant in Paris. There wasn't an L plate in sight but Kim managed to stand out in the high-necked figure-hugging mini dress and nude heels. Perhaps in an effort to show she is no blushing bride the star also rocked a buttoned-down black leather ensemble.

The 33-year-old could have stopped traffic in the dominatrix-style leather ensemble which showed off her curvy bottom and impressive cleavage. She paired the racy outfit with a pair of simple gold-chain heels.

Kim showed off her famous curves in a dominatrix-style leather ensemble [GETTY]

Kim stepped out in an all-white ensemble ahead of her wedding in Italy [GETTY]

Kim recycled these again as she practised for the big day in an all-white ensemble. Leaving their residence in Paris ahead of the wedding Kim wore a draped white dress which featured a revealing low-cut neckline. She also practised posing for her wedding shots when she flaunted her curvaceous figure in a see-through backless gown in front of a wall of roses which Kanye gave her for Mother's Day.

Kim poses next to a wall of flowers - a Mother's Day present from Kanye [INSTAGRAM]