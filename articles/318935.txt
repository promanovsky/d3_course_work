But in a 6-to-3 decision, the justices ruled that Aereo violated copyright law because it did not pay the same licensing fees that cable television providers do for the right to retransmit the programs of broadcast networks.

AD

AD

"Aereo performs petitioners’ works publicly," wrote Justice Stephen Breyer in the court's opinion. Whether Aereo provided "public" performances of broadcasters' programs was central to the case. Aereo argued that it provided "private" performances and that its individually assigned tiny antennas and storage files for viewers put consumers in control of what programs they chose to watch. The company equated its technology to an antenna and cloud storage rental service.

But the court disagreed, saying Aereo looks similiar to a cable television provider, which under copyright law must pay licensing fees to broadcast networks to carry their programs to subscribers.

"It does not merely supply equipment that allows others to do so," Breyer wrote.

AD

Aereo CEO Chet Kanojia called the decision a "massive setback for the American consumer" and warned that the high court "sends a chilling message to the technology industry.

AD

He did not discuss the company's plans, but said, "Our work is not done. We will continue to fight for our consumers and fight to create innovative technologies that have a meaningful and positive impact on our world.”

"We worked diligently to create a technology that complies with the law, but today’s decision clearly states that how the technology works does not matter," he said.

For now, it should be business as usual for Aereo customers, who live in places like Baltimore, Atlanta and New York. But Aereo's future is unclear, given that its entire cost structure should change as a result of the Supreme Court ruling.

AD

The decision was a major victory for broadcasting giants, whose stocks soared after the court's decision. Shares of CBS rose 7 percent to $63.75; Walt Disney, which owns ABC, rose nearly 3 percent to $84.32.

AD

Broadcasters waged a two-year battle against Aereo in multiple lower courts as the specter of Aereo or copycats of the company's methods and technology posed a major threat to their business.

"Today's decision is a victory for consumers," Paul Clement, the attorney representing the broadcasters, said in a statement. "The Court has sent a clear message that it will uphold the letter and spirit of the law just as Congress intended."

Walt Disney added, "We're gratified the court upheld important Copyright principles that help ensure that the high-quality creative content consumers expect and demand is protected and incentivized."

AD

The decision did protect cloud Internet services, however, giving relief to storage providers such as DropBox and Google, which Justices were careful to keep out of the case.

AD

"We have said that it does not extend to those who act as owners or possessors of the relevant product," such companies that provide the remote storage of content, the court wrote.

The case drew keen interest from high-tech, media and telecommunications for its potential to disrupt the entire television business, an intricate system of relationships between programmer and distributors.

Cable companies have groused about increasing licensing fees and could have used Aereo as leverage in negotiations with broadcasters. Fights over licensing fees have resulted in blackouts, with consumers stuck in the middle; these disputes are expected to become a bigger source of friction with broadcasters charging more each year for programming.

AD

Retransmission fees reached $3.3 billion last year and are expected to grow to $7.6 billion in five years, according to media research firm SNL Kagan.

AD

And consumers, looking for alternatives to the expensive bundle of cable channels could have seen the combination of Aereo, Hulu and Netflix, for instance, as sufficient services to replace their cable subscription.