Carol Kopp

Minyanville

It had to happen sooner or later. Amazon stock tanked nearly 10% in early trading Friday as investors absorbed the company's outlook for yet another quarter in which current profits will be sacrificed for investment in the future.

The company's guidance for the current quarter includes additional growth in revenue but another loss on the bottom line. It pegs its profit shortfall at $55 million to $455 million, a curiously wide range, which raises more questions than it answers. That compares with a modest $79 million profit in the same quarter a year earlier.

Amazon has consistently traded short-term profits for ambitious long-term investments -- like a warehouse system that stretches from coast to coast and the impossibly low-priced Kindle Fire tablet line, to name just two.

It's a strategy, and it's one that took Amazon from bookstore to mega-store for everything in a few short years. But Wall Street is sending a message that it's fed up with that.

The outlook may have disappointed because it was delivered right after numbers for the first quarter that were pretty good: The company announced a profit of $108 million, up 27% from the first quarter of last year. Revenue came in at $19.7 billion, up 23% over last year.

As CEO Jeff Bezos said in a statement, "2014 is off to a kinetic start."

So, what is Amazon spending all the dough on? Here are three big things.

1. Everything but Game of Thrones

There's really only one way to compete in a tough field for streaming video against the likes of Netflix and Apple, and that's by throwing around a whole lot of money.

That's why Amazon is paying Time Warner's HBO a reported $300 million for video-streaming rights to most, but by no means all, of its most popular productions.

It is a coup of sorts, in that Amazon will be the first and only place to stream HBO shows without an HBO cable subscription.

But it sticks to a tried-and-true formula for digital rights: Rent the archive, hang onto the new stuff.

The new deal gives Amazon access to some genuine classics, including The Sopranos, Big Love, Deadwood, and Six Feet Under. Early episodes of hits including Boardwalk Empire, Veep, and The Newsroom will become available three years after they first aired on HBO.

The deal also includes acclaimed miniseries like Band of Brothers and John Adams, and HBO's comedy specials and documentaries.

But at least two smash hits -- the current Game of Thrones series and the older Sex and the City -- are not on the list. HBO couldn't or wouldn't let loose of the digital rights to those shows at any price.

2. The Amazon Phone, Really

This rumor seems to have been around as long as the one about the Loch Ness Monster, but either the Amazon phone is really coming soon or a hoax of epic proportions has been perpetrated over the Internet.

For better or worse, this is no knockoff. The Amazon phone reportedly has unique navigation controls based on actions such as tilting the phone rather than touching its screen to bring up related information, open an app, or turn the page of an e-book. Some functions and apps also are expected to have 3-D effects. BGR.com has obtained details and images from unnamed sources.

The phone is expected to be announced in June and available by September.

It's worth asking whether the world wants or needs yet another phone brand. But it's only the latest move by Amazon into the hardware business and into direct competition with its own electronics suppliers.

3. Going 'The Last Mile'

This final one is mind-boggling, although we've seen it coming for a while now: Amazon is considering getting into the delivery business. Bright-green Amazon trucks, driven by Amazon-contracted drivers, hauling all of those Amazon boxes around to drop them on customers' doorsteps. (We've seen a preview with Amazon Fresh.)

This is the final "last mile" of its elaborate Amazon Logistics system, and it's an increasing expensive mile. The company has set a goal of getting purchases to customers as fast as possible. That makes it dependent on the major delivery services, and makes it harder to control costs.

So, like all of Amazon's ambitious schemes, it's logical and way "out there" at the same time. We'll see how it goes. Look for the bright-green trucks in test cities, including San Francisco, Los Angeles, and New York.