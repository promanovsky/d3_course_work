Apple and Motorola Mobility have called a patent truce.

In court documents filed on Friday, Apple and Motorola said all parties have agreed that it would be "appropriate" to dismiss all litigation and stop squabbling over mobile patents.

"Apple and Google have agreed to dismiss all the current lawsuits that exist directly between the two companies," the companies said in a joint statement. "Apple and Google have also agreed to work together in some areas of patent reform. The agreement does not include a cross license."

Though Google has agreed to sell Motorola Mobility to Lenovo, the deal is not yet complete.

The fight dates back to November 2010, when Motorola filed its initial complaint. Apple countersued, and over time, the case has evolved and expanded to various courts. More recently, the U.S. Court of Appeals for the Federal Circuit in January upheld an April 2013 decision from the International Trade Commission (ITC) in favor of Cupertino.

Last month, meanwhile, the EU announced that Motorola had abused its power in seeking patent-related injunctions against Apple in Germany, but stopped short of imposing a fine on the phone maker. Instead, the commission said that companies like Motorola - which hold standard essential patents (SEP) - must agree to licensing deals if the opposing party is willing, rather than seeking product bans.

Reuters first reported the story.

Apple told the Wall Street Journal that this deal does not affect its ongoing patent litigation with Samsung. Recently, Apple won $120 million from Samsung, though that was far less than the $2 billion it wanted.

Further Reading

Mobile Phone Reviews