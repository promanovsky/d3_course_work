Shutterstock photo

Investing.com -

Investing.com - Japan's economy can weather a sales tax hike that started in April without immediate further monetary measures to offset it, Bank of Japan Governor Haruhiko Kuroda said on Tuesday at a press conference.

He also told reporters after the bank's two-day policy meeting that kept its current monetary easing targets as expected, that growth and inflation risks are under control and employment is recovering more than expected last year.

The output gap continues to shrink and is estimated to be close to zero," he said, and later added that an increase in the sales tax to 8% from 5% on April 1 has not prompted policy changes.

"We are not considering conducting additional easing at this point but we are ready to take necessary actions."

"The pullback (in consumption) after the tax hike will decrease in the summer onward," he said, adding that aggressive monetary easing launched by the BoJ in April last year is working.

Japan aims to achieve sustained 2% inflation sometime between late fiscal 2014 and during the course of fiscal 2015.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.