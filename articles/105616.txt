AUSTIN: World champion Marc Marquez set a qualifying lap record on his way to securing pole position for the second MotoGP race in succession on Saturday at the Grand Prix of the Americas.

The Spaniard, who took pole in the season-opener in Qatar and then went on to win the race under the desert floodlights, clocked a record 2min 02.886sec on the factory Honda at the circuit where he celebrated his maiden MotoGP win last season.

On pole: Marc Marquez of Spain leans into a turn Credit:AP

Marquez's teammate Dani Pedrosa was second fastest while Stefan Bradl ensured a Honda lockout of the front row.

For Marquez, who broke his leg in February in a dirt-bike accident in his native Spain, it was an 11th premier class pole position and it was achieved with a 0.289sec advantage over Pedrosa.

German rider Bradl was 0.423sec off the top.

It will be an all-Yamaha second row for Sunday's race, with Aleix Espargaro edging the superstar factory pair of Jorge Lorenzo and Valentino Rossi.

AFP