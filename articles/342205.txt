Anyone who followed this past weekend’s exploits of Shia LaBeouf knew this was coming. The former child star and once Hollywood A-Lister has checked into rehab. The move comes after a weekend of bizarre behavior and an arrest in New York City.

From X17 Online:

We followed the actor from his Hollywood Hills home, just an hour ago, to a private facility where other stars have sought treatment — driven by someone believed to be from the rehab facility. When the car arrived at the gated drive to the center, a nurse and security guard were there to great [sic] Shia. In fact, earlier in the day when we spotted Shia in his driveway, we noticed his was carrying the Alcoholics Anonymous Big Blue Book.

LaBeouf starred in some of Hollywood’s biggest blockbusters, including “Transformers’’ and “Indiana Jones and the Kingdom of the Crystal Skull.’’ But more recently he’s made headlines for plagiarism and overall odd behavior.

(h/t Gawker)