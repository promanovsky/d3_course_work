Tests on a man in a Saskatchewan hospital who was suspected of possibly having contracted Ebola during a visit to West Africa have determined he does not have the virus, federal health officials said Tuesday.

Canada's deputy chief public health officer said in a release that tests at the Public Health Agency of Canada's National Microbiology Laboratory confirmed the ill man does not have Ebola or any of three other hemorrhagic fevers -- Lassa, Marburg or Crimean Congo virus.

Dr. Gregory Taylor's statement said ruling out those four hemorrhagic viruses "significantly reduces the risk to the people who have been in close contact with the patient while the patient has exhibited symptoms."

Taylor added there has never been a confirmed case of a hemorrhagic virus in Canada, and that testing continues to determine the man's illness.

George Sharpe, chief geologist at Global Geological Services Ltd. in Saskatoon, identified the sick man as his business partner, Rod Ogilvie.

"He got this assignment in Africa... He wanted his own project and it was just him that was required," Sharpe told Saskatoon radio station CKOM.

"It was pretty routine. He just went over to supervise an iron ore project. He has done it many times before, he has been in Africa before, so it is nothing new to him."

Sharpe said Ogilvie went to the west African country of Liberia in February and came back in the middle of March.

"He was aware, as we all are, that anybody can catch these kinds of illnesses. There is no perfect way to prevent them... He was always advising me 'when you go to the tropics, Ghana or whatever, absolutely you have to take your malaria pills religiously, take your insect repellent, watch what you eat and he was always very careful about that,"' Shape said.

"What he got was just something by pure chance more than likely. Certainly he wasn't the type of person who would ever take a chance on catching anything like that."

Saskatchewan health officials had said Monday that one of the possible diagnoses being considered for the seriously ill man was Ebola hemorrhagic fever.

There is fear an outbreak of the Ebola virus has spread to Liberia, where the man was travelling.

"Viral hemorrhagic fever is a generic name for a number of rather exotic diseases that are found in Africa," Dr. Denise Werker, deputy chief medical health officer, said Monday.

"In that class of diseases there is Ebola hemorrhagic fever, Lassa fever, Crimean-Congo hemorrhagic fever and yellow fever that you would normally include in the viral hemorrhagic fevers."

Marburg hemorrhagic fever is also included in that group.

The World Health Organization says Ebola is one of the most virulent viral diseases. There is no cure.

Werker said Monday it is not easily spread.

"People need to be in close contact with blood and bodily fluids and so that would be close household contacts of people who are taking care of these individuals," she said.

"There is no risk to the general public at all about this."

African health officials have said an outbreak of Ebola is believed to have killed at least 59 people in Guinea and may already have spread to neighbouring Liberia, where health officials said they are investigating five deaths after several people crossed the border from Guinea in search of medical treatment.

--- with files from CKOM