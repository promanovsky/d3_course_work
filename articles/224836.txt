BUENOS AIRES, Argentina -- Scientists have uncovered huge bones in Argentina that could be from the largest dinosaur yet found, a kind of titanosaur that munched tree tops more than 90 million years ago.

Outside experts agree they're from a previously undiscovered species of sauropod at least as big as the Argentinosaurus, whose towering legs and long neck and tail arguably set the record for size.

But what's known about Argentinosaurus anatomy is mostly based on an incomplete thigh-bone found in 1991, also in Argentina's fossil-rich Patagonia region. This time, paleontologists think they'll be able to unearth a much more complete fossil.

Already, the complete thigh bone they've uncovered is at 7.9 feet (2.4 metres) the longest of any vertebrate yet found, said paleontologist Jose Luis Carballido, who is leading the research team. He estimates that the beast reached 66 feet (20 metres) tall, 132 feet (40 metres) long and weighed the equivalent of 14 or 15 adult African elephants.

Carballido said a volumetric estimate based on the femur and humerus they've uncovered suggests the animal weighed around 176,000 pounds.

"Based on what is known of the animal, it was certainly very, very large," said paleontologist John Whitlock of Mount Aloysius College in Pennsylvania.

"Just how large may have to wait for more fossils and will probably depend on the method used to estimate its total size -- we've seen how much estimates of mass can change for fragmentary animals like Argentinosaurus -- but right now it would certainly seem to be a strong contender for largest known sauropod," Whitlock said.

Paleobiologist Paul Upchurch of University College London believes size estimates are more reliable when extrapolated from the circumference of bones, said and this femur is a whopping 43.3 inches around, about the same as the Argentinosaurus' thigh bone.

"Whether or not the new animal really will be the largest sauropod we know, remains to be seen," said Upchurch, who wasn't involved in this discovery but has seen the bones firsthand.

"Certainly the new animal appears to be at least as large as Argentinosaurus, and is a new species," Upchurch added. "Its real scientific value comes from the fact that it looks like this new form will be more complete than Argentinosaurus, so we'll get a better look at the anatomy of one of these super-giants."

The bones were spotted years ago by a worker at a ranch outside the town of Las Plumas, in Patagonia's Rio Chubut valley, and Carballido's team from the Egidio Feruglio dinosaur museum in Trelew began digging in January 2013.

"What they discovered is a cemetery of dinosaurs the likes of which we had never seen in the history of Argentine paleontology," said the museum's director, Ruben Cuneo. Along with this titanosaur, they have already found the fossils of seven other dinosaurs, and have only uncovered an estimated 20 per cent of the find. For now, the rest have been covered with plaster to protect them from the southern winter, and excavations will resume in the spring.

"Given the length and magnitude this animal will bring along when it's reconstructed, there won't be a building that can contain it. I think we're going to need a new home," Cuneo said.