The competition on Dancing With the Stars is getting fierce! So fierce, in fact, that even head judge Len Goodman has "no clue" who's going home and who's going to win. Week eight of the show's 18th season saw yet another favorite couple eliminated from the race to the Mirrorball trophy—not to mention some tense moments between the Chmerkovskiy brothers and guest judge Abby Lee Miller, of the Lifetime reality series Dance Moms. Let's recap!

Charlie White and Sharna Burgess, safe

After last week's disappointing paso doble, Charlie was more determined than ever to get a perfect score—and he succeeded! His quickstep this week with Sharna was universally praised by the judges, save some nitpicking from Abby Lee on his footwork. "Tonight you broke stride," Carrie Ann Inaba gushed. "You've gone a step beyond." Added Bruno Tonioli: "[You were] off the starting block like a thoroughbred at the Kentucky Derby!"

Score: 40 out of 40 (10 from everyone)

Danica McKellar and Val Chmerkovskiy, in jeopardy

Danica, still nursing a broken rib, didn't let her injury get in the way of a great performance this week. Abby Lee praised her tango with Val as "beautiful" and said she "could have been a professional dancer," while Bruno raved that it was her "most majestic and powerful performance" to date. Carrie Ann gushed, too, that "it was like watching the Black Swan recreate the most beautiful ballet in the world."

Score: 38 out of 40 (10 from Carrie Ann and Bruno, 9 from Abby Lee and Len)

Meryl Davis and Maks Chmerkovskiy, safe

Meryl and Maks were really feeling the love for each other this week. During their intro package, Meryl said that despite his "scary" image, Maks is really just a "big teddy bear." And with her, he kind of is. "I think it's clear that we're both going to try to maintain whatever it is we have way past the show," he confessed to the cameras, prompting a chorus of "oohs" from the audience.

Their chemistry was evident in their rumba, which Bruno praised as a "tempestuous melodrama" in the style of Tennessee Williams. "It was like watching a play without words," he said. Added Len, who normally isn't a fan of "overdramatic" routines: "The mood and the feeling of this was fantastic." Only Abby Lee had some "constructive criticism" on Meryl's footwork—criticism that Maks dismissed offhand. "I really don't care for anything she has to say," he scoffed of the reality star.

Score: 36 out of 40 (10 from Bruno, 9 from Carrie Ann and Len, 8 from Abby Lee)

Candace Cameron Bure and Mark Ballas, in jeopardy

Candace was visibly discouraged to be in the bottom three once again, but she put it out of her mind to perform her foxtrot with Mark. "You have made a breakthrough in your mind and spirit," Carrie Ann told the actress, who had to see a sports psychologist for her nerves. Added Len: "It was bright, it was fun, it was frivolous, it was joyful. Loved it."

Score: 36 out of 40 (9 from everyone)

James Maslow and Peta Murgatroyd, in jeopardy

James and Peta had to deal with more questions about their romantic involvement when his Big Time Rush bandmates visited rehearsals. "You guys have chemistry like a married couple," one of them said. "Are you guys dating?"

It certainly seemed that way after their Viennese waltz this week. As Bruno said, "Love was in the air, and I could feel it." All four judges noted, however, that James has some problems with his technique.

Score: 36 out of 40 (10 from Abby Lee, 9 from Len and Bruno, 8 from Carrie Ann)

Amy Purdy and Derek Hough, safe

After injuring herself during last week's live show, Amy struggled to rehearse her Argentine tango with Derek. To give her a bit of a break, he brought in another dancer to demonstrate the moves. It worked like a charm—by showtime, Amy was more than performance-ready. "That was totally spellbinding," Bruno said. "I feel like I'm in a trance." Len shared a similar sentiment, telling the pair, "If I'm dreaming, don't wake me up. That was incredible."

Score: 40 out of 40 (10 from everyone)

Dance Duel No. 1: Meryl and Maks, Danica and Val

There was a bit of tension between Danica and Maks during rehearsals for their dual samba with Val and Meryl, but all seemed well when it came time for the couples to perform for the judges. Unfortunately, even though "the content was there," the synchronicity was not. All four judges said the routine was "very good," but Carrie Ann noted that "some of the timing was off." Abby Lee had some criticism on Danica's technique, too, prompting Val to rush to his partner's defense.

Score: 34 out of 40 (9 from Len and Bruno, 8 from Carrie Ann and Abby Lee)

Dance Duel No. 2: Candace and Mark, Charlie and Sharna

Candace and Charlie's contemporary routine with Mark and Sharna was very well-received by the judges, who acknowledged the "flawless" lifts. "Charlie. Candace. Made for each other," Bruno said. Added Abby: "Candace…you look like an Abby Lee Dance Company member!"

Score: 38 out of 40 (10 from Abby Lee and Bruno, 9 from Carrie Ann and Len)

Dance Duel No. 3: James and Peta, Amy and Derek

Despite some rough rehearsals—James and Amy took a pretty nasty fall while practicing without Derek and Peta—the foursome pulled together a near-perfect jive. "Extraordinary!" Bruno raved of their performance. "Amy and Peta, you looked like sisters, and sisters doin' it for themselves. And James, stepping into Derek's shoes takes some doing. And you did it beautifully." Added Len: "[That was] everything I could have wished to see and more."

Score: 39 out of 40 (9 from Carrie Ann, 10 from everyone else)

In the end, Danica and Val were sent home. "This has been an amazing experience," she said of DWTS. "I've learned so much. I'm so grateful for all of it."?