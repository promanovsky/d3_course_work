SYDNEY, June 30 (UPI) -- Astronomers at the University of New South Wales in Australia are calling the newly discovered exoplanet Gliese 832c a "super-Earth." In fact, scientists say it's one of the most Earth-like of all the exoplanets so far discovered.

Technically -- according to the very official-sounding Earth Similarity Index -- Gliese 832c is the third most Earth-like exoplanet.

Advertisement

"With an outer giant planet and an interior potentially rocky planet, this planetary system can be thought of as a miniature version of our Solar System," explained Chris Tinney, co-author of the new study on the discovery, published this week in the Astrophysical Journal.

The search of exoplanets is motivated by the prospects of finding cosmic bodies capable of hosting life. One would think an exceptionally Earth-like exoplanet would be just that. But even the scientists responsible for the new discovery admit: Gliese 832c may be more like a super-Venus.

RELATED Defense Department honors Bechtel for security practices

Gliese 832c orbits Gliese 832, a red dwarf some 16 light-years away from Earth. The dwarf star has roughly half the mass and radius as the sun. Because red dwarfs like this give off less energy, a planet capable of sustaining life would have orbit much closer to its host star.

Gliese 832c does just that. It orbits Gliese 832 in just 36 days. But being so close might actually make the planet too hot. If its atmosphere is big, thick and heat-trapping (as the atmospheres of big planets usually are), the planet would likely stifle any possibility of life.

"If the planet has a similar atmosphere to Earth it may be possible for life to survive, although seasonal shifts would be extreme," Tinney acknowledged.

RELATED Lightning G Lindback to be free agent

If life does exist on the steam bath that likely is Gliese 832c, it probably looks a lot different than life here on Earth.