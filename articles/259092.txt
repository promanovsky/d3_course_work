The $30 million, five-year project being announced Tuesday is one of the first pieces of President Obama’s BRAIN initiative , unveiled last year in an attempt to foster a revolution in neuroscience similar to the one that occurred when the human genome was sequenced a little more than a decade ago. A second team at the University of California San Francisco will receive $26 million, also for work on brain implants.

Boston researchers will build a generation of brain implants under an ambitious Defense Department program aimed at pioneering more precise ways to treat mental illnesses suffered by combat veterans, including post-traumatic stress disorder, traumatic brain injury, and depression.

Advertisement

The research is inspired by a technology already used in tens of thousands of patients: deep brain stimulators that trigger electrical impulses in part of the brain to quell tremors in Parkinson’s disease. But unlike those simple technologies, the new implants will be responsive, including a network of sensors that can read what is happening in the brain and stimulate multiple brain areas if abnormal activity is detected.

“Which areas are either not working properly, are overactive, underactive, having an abnormal rhythm, or abnormal connectivity?” said Dr. Emad Eskandar, a Massachusetts General Hospital neurosurgeon who will co-lead the Boston research team. “By using an approach like this, you can actually focus in on the actual problematic area.”

Beyond its potential use for the military, the project could help transform understanding of mental illnesses. Despite decades of research, scientists still have little idea what goes wrong in the brain to cause psychiatric diseases, which consequently continue to be categorized by overlapping clusters of symptoms. The laboratory animals that are typically used to study and develop therapies also fall short as models of complicated neuropsychiatric diseases.

Dr. Steven Hyman, director of the Stanley Center for Psychiatric Research at the Broad Institute, a Cambridge genomics research center, said that the initiative is an important one, especially given how little progress has been made over the last half-century. But Hyman, who sits on a panel that advises the Defense Advanced Research Projects Agency on the ethical implications of neuroscience research, added that it was also necessary to be cautious and transparent.

Advertisement

“There’s always the risk that we have to remember, of psychiatric treatments that were perhaps well intended that went off the rails,” Hyman said. The agency, which is funding the implant work, “is being extremely open” with the advisory panel and the public, he said, and given the “pressure to come up with treatments, I see little alternative other than careful human investigation.”

The California and Massachusetts teams aim to build an implantable device that is ready to be used in a clinical trial by the end of the five years, though which disease would be initially targeted remains undecided. They will be held to a strict set of milestones along the way, and receiving the full funding is contingent on meeting those interim goals.

At Mass. General, researchers plan to use what they are calling a “trans-diagnostic” approach, drawing on a suite of tools to identify abnormal brain activity patterns that are hallmarks of seven psychiatric illnesses. They will knit together several approaches that are now used separately, taking advantage of the cutting-edge brain imaging facilities at the hospital’s Martinos Center for Biomedical Imaging in Charlestown, as well as techniques used in treating the most serious cases of epilepsy.

Advertisement

In these patients, surgeons make precise recordings of electrical activity in the brain in order to place electrodes that stop seizures.

The Mass. General team will draw on the engineering expertise of Draper Laboratory in Cambridge to build a tiny, programmable device that can stimulate and sense brain activity in multiple areas. Collaborators at MIT will design computational tools to make sense of the data.

“What we can do is record neuronal activity in one area, and when it reaches a certain threshold that corresponds with symptoms, it [the implant] would stimulate an area to mitigate the symptoms,” said Dr. Darin Dougherty, director of the Neurotherapeutics Division at Mass. General and a coleader of the project. “It’s not constantly turned on; it’s a responsive turn on, and it could change over time — a big leap forward.”

Justin Sanchez, the program manager for the research projects agency, said competition for the contract was extremely tough, with proposals from just about every major medical center and industry player that does work in the area.

Mass. General’s proposal, he said, integrated multifaceted approaches and will provide a level of precision not available with drugs that flow all over the entire brain or external devices that cannot be targeted as specifically.

California researchers will take a slightly different approach to the same problem, starting their work by taking detailed brain recordings from patients with Parkinson’s and epilepsy who are already having such measurements taken. They will use that information to try to understand signatures of brain activity and also plan to take advantage of the brain’s malleability — the fact that it can form new connections between brain cells — in an attempt to use the implants to alter and strengthen circuits.

Advertisement

“We’re entering in neuroscience a perfect storm of opportunity, because the technologies are really advancing very, very rapidly,” said Dr. Arthur Toga, a neuroscientist at the University of Southern California not involved in either project. “Having this kind of information, were it to be obtained, might open up windows for therapies, because it fills in gaps in knowledge.”

Carolyn Y. Johnson can be reached at cjohnson@globe.com. Follow her on Twitter @carolynyjohnson.