Are you planning on getting your hands on the upcoming CyanogenMod-loaded OnePlus One handset? Well if you are, it seems that the company has recently launched some kind of invite system where you basically will need to be invited in order to be able to purchase the device. Sure the device is pretty hyped given the popularity of CyanogenMod and the fact the phone packs some pretty impressive hardware, but is that it popular to be able to pull off such exclusivity?

Advertising

Maybe, maybe not, but according to the company’s explanation, this has to do more with demand and supply and it is a good way to ensure that the company has enough phones for everyone, as opposed to running out of stock and disappointing customers. The invites can be had via forums, contests, as well as invites from other OnePlus One buyers, meaning that if you wanted to get your hands on the handset, you’d have to start looking around.

It’s an odd choice since we’re sure limiting pre-orders via a standard online store would be a perfectly sound way to go about it. Companies such as Apple have allowed customers to place pre-orders and it is usually on a first-come-first-serve basis. Apple has allowed shipping times to slip which no doubt frustrate customers, so perhaps this is a scenario that OnePlus One would like to avoid.

The handset is expected to see a launch on the 23rd of April and according to the company, that is when the invites will start going out to customers. If you’d like to learn more about this invite system, you can head on over to the OnePlus One forum for the details. In the meantime what do you guys think of this move?

Filed in . Read more about CyanogenMod and OnePlus One.