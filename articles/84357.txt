European stocks rallied for a ninth straight daily advance on Friday, as a decent U.S. jobs report kept investors in a positive frame of mind.

Strong manufacturing data from Germany and talk of a big merger between cement makers Holcim Ltd. and Lafarge SA bolstered the major averages.

The Euro Stoxx 50 index of eurozone bluechip stocks rose 0.64 percent. The nine-day win streak is the longest since October.

The German DAX gained 0.7 percent and the French CAC 40 advanced 0.8 percent. The UK's FTSE 100 rose 0.7 percent while Switzerland's SMI lost 0.2 percent despite a strong showing from Holcim.

Holcim Ltd. and Lafarge SA are in talks to explore a merger to form a company that will have a combined market value of more than $50 billion, Bloomberg reported Friday, citing people familiar with the matter.

Holcim shares jumped 6.8 percent, while LaFarge added 8.9 percent.

Fresenius Medical Care, the dialysis care unit of healthcare company Fresenius SE, expects revenues of $28 billion in 2020 and earnings growth in the high single digits. Shares were down 1 percent.

In London, easyJet gained 2 percent after reporting growth in passengers and load factor for the month of March.

Royal Bank of Scotland has appointed Credit Suisse Group AG's Ewen Stevenson as an executive director and its chief finance officer with effect from May 19. The stock was up 0.4 percent.

Tandem Group, which reported higher revenues for the first quarter, surged 43 percent.

TNT Express said its Finance Chief Bernard Bot has decided to step down. The stock lost 3 percent in Amsterdam.

In economic news, the Labor Department said U.S. non-farm payroll employment rose by 192,000 jobs in March compared to economist estimates for an increase of about 200,000 jobs. The unemployment rate stayed at 6.7 percent.

Germany's factory new order growth exceeded expectations on robust domestic demand in February, official data showed Friday.

Factory orders advanced 0.6 percent from the previous month, Destatis said.



An excellent growth performance of the German in the first quarter is clearly in the making, Carsten Brzeski, an economist at ING Bank NV said. Looking beyond the first quarter, the economy should slow again and return to more normal growth rates, he added.

For comments and feedback contact: editorial@rttnews.com