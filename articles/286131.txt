Stocks were holding on to slim gains Friday despite a measure of consumer sentiment declining and energy prices holding near eight-month highs. After two days of profit-taking, equities reversed course at the open, turning positive with a break in the oil market's recent rally and an unexpected drop in May producer prices.

Wholesale prices declined 0.2% last month with declines in nearly every sector except transportation and warehousing. Excluding food and fuel prices, the producer price index dropped 0.1%. Both the nominal and core figures were predicted to increase 0.1% following robust gains in April of 0.6% and 0.5%, respectively.

Wage-related worries continue to plague U.S. consumers, with the Reuters/University of Michigan consumer sentiment index slipping to a preliminary 81.2 reading this month from May's final reading of 81.9. The market consensus was expecting improvement to a 83.0 score.

Bank of England President Mark Carney sent UK equities reeling after he said at his annual Mansion House speech that UK rates could rise sooner than expected. The shift in interest rate expectations sent the FTSE nearly 1% lower although its German counterpart managed to hold onto a very small gain.

Crude oil was up $0.11 to $106.63 per barrel. Natural gas was down $0.03 to $4.74 per 1 million BTU. Gold was down $0.20 to $1,273.80 an ounce, while silver was up $0.09 to $19.62 an ounce. Copper was up $0.03 to $3.04 per pound.

Among energy ETFs, the United States Oil Fund was up 0.18% to $39.15 with the United States Natural Gas Fund was up 0.08% to $26.25. Amongst precious-metal funds, the Market Vectors Gold Miners ETF was down 0.98% to 23.74 while SPDR Gold Shares were up 0.02% to $122.67. The iShares Silver Trust were up 0.56% to $18.87.

Here's where the markets stand at mid-day:

NYSE Composite Index up 21.72 (+0.20%) to 10,846.74

Dow Jones Industrial Average up 41.62 (+0.25%) to 16,775.81

S&P 500 up 5.63 (+0.29%) to 1,935.74

Nasdaq Composite Index up 13.31 (+0.31%) to 4,310.94

GLOBAL SENTIMENT

Nikkei 225 Index up 0.83%

Hang Seng Index up 0.62%

Shanghai China Composite Index up 0.93%

FTSE 100 Index down 0.95%

CAC 40 down 0.24%

DAX down 0.26%

NYSE SECTOR INDICES

NYSE Energy Sector Index up 0.69%

NYSE Financial Sector Index down 0.08%

NYSE Healthcare Sector Index down 0.03%

UPSIDE MOVERS

(+) OPEN (46.97%) Aquired by Priceline ( PCLN ) for $103 per share.

(+) ICLD (+15.53%) Becomes VMware partner, opens New England sales office.

(+) INTC (+7.21%) Raised revenue and earnings guidance.

DOWNSIDE MOVERS

(-) OMED (-13.01%) Halts enrollment, dosing in Vantictumab, Fzd8-Fc trials amid adverse events.

(-) GNC (-3.62%) CFO and Executive VP Michael Nuzzo resigns.

(-) KOOL (-17.51%) Prices stock at 15% discount to Thursday's close.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.