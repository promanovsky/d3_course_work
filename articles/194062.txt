You would have been forgiven for assuming The Amazing Spider-Man 2 had been a monumental success. It's everywhere. Posters, trailers, merchandise. Sony have gone big with the sequel to Marc Webb's 2012 movie and with Andrew Garfield, Emma Stone, Jamie Foxx and Paul Giamatti amongst the cast, you can bet your life this one was big, big, budget.

Andrew Garfield in 'The Amazing Spider-Man 2'

But then there's the reviews. The Amazing Spider-Man 2 ranks way, way lower than previous Spidey movies and with 55% on Rotten Tomatoes, is still 8% lower than the second lowest ranked movie - Sam Raimi's 2007 movie Spider-Man 3. Next comes The Amazing Spider-Man, which introduced Garfield as the webbed hero and nabbed 73% from critics. Raimi's original movie Spider-Man scored 89%, though the superb sequel landed a stunning 93% in 2004 with the unrivalled Alfred Molina as villain Dr Octavius.

More: Whatever happened to Shailene Woodley's scenes in 'The Amazing Spider-Man'

So what went wrong with the latest sequel?

"The successes of The Amazing Spider-Man 2 are human, the failures are typical of superhero CGI adaptations. In a world where the incredible is routine, the "amazing" is mundane," said Liam Lacey of Globe and Mail.

"How bad is this one ...? Amazingly so. Villainy abounds, but the villains are strident contrivances. Spider-Man flies, but does so dutifully, without joy," said Joe Morgenstern of the Wall Street Journal.

"The only parts of The Amazing Spider-Man 2 that work are those scenes with Garfield and Stone, who bring energy and life to characters that otherwise might have felt somewhat lifeless," said Will Leitch of Deadspin.

A mid-credits scene in the latest movie suggested Sony are looking well-ahead into the future for the ailing Spider-Man brand and The Amazing Spider-Man 3 will arrive on June 10th 2016, with The Amazing Spider-Man 4 set for May 4, 2018. With the sequel derided by critics, the studio is going to have to come up with something incredibly innovative to rescue this once thriving franchise.

More: The Amazing Spider-Man 2 pulls in $92 million at the box office

The Amazing Spider-Man 2 trailer:

