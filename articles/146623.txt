Ford Motor Co. will soon name chief operating officer Mark Fields as successor to the automaker's CEO Alan Mulally, according to a report.

The report from Bloomberg, citing two people familiar with a pending announcement, said Fields will replace Mulally before the end of the year.

Fields, 53, has been a 25-year veteran of Ford.

The sources, who spoke to Bloomberg on condition of anonymity, said Mulally's retirement date could be announced as early as May 1.

A Ford spokesperson declined to comment about who will succeed Mulally, 68, or when Mulally is set to retire.

"We take succession planning very seriously, and we have succession plans in place for each of our key leadership positions," Ford spokesperson Susan Krusel told Bloomberg. "For competitive reasons, we don’t discuss our succession plans externally."

Mulally arrived at Ford from Boeing Co. in 2006 and has been credited with lifting the automaker's fortunes from the verge of bankruptcy and fostering a more collaborative environment.

Fields first emerged as Mulally's prospective heir apparent when he was promoted in December 2012 to become COO.