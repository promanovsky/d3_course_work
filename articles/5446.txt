(Reuters) - Japan's SoftBank Corp is still trying to buy T-Mobile US Inc and merge it with its U.S. wireless carrier Sprint Corp, SoftBank CEO Masayoshi Son said, even though U.S. regulators appear set against a deal.

Son told U.S. TV show host Charlie Rose in an interview that if the deal goes through, he would launch a price war to break what he called a duopoly by dominant U.S. carriers AT&T Inc and Verizon Communications Inc.

"We would like to make the deal happen, but there are steps and details that we have to work out," said Son, who had previously declined to comment on whether Softbank was in talks to buy T-Mobile. "We have to give it a shot."

Excerpts of the interview with the PBS network show were posted on YouTube. The show is scheduled to be broadcast late on Monday in the United States.

The possibility of a merger between T-Mobile and Sprint, which was acquired by Softbank last year, was given the cold shoulder by regulators, with Federal Communications Commission Chairman Tom Wheeler expressing scepticism in meetings with Son and Sprint Chief Executive Dan Hesse on February 3, according to an FCC official briefed on the matter.

Regulators are concerned that reducing the number of major wireless carriers to three from four would hinder competition, but Son has argued that competition would be more robust if a third strong player is created through the merger of smaller firms Sprint and T-Mobile.

Softbank is now focused on convincing the parties involved with the merits of a merger, a senior company executive said, adding that any moves towards pursuing a deal were now on hold. The official declined to be named because he was not authorised to speak about the matter publicly.

Son plans to present his vision for the U.S. wireless communications industry in a speech in Washington, D.C., on Tuesday, although a person familiar with the matter said he would not talk about a bid for T-Mobile.

(Reporting by Sophie Knight and Yoshiyasu Shida in Tokyo; Writing by Edmund Klamann; Editing by Miral Fahmy)