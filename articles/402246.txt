A Chinese regulator said it is conducting an anti-monopoly investigation into Microsoft over its Windows operating system

A Chinese regulator said it is conducting an anti-monopoly investigation into Microsoft over its Windows operating system, in the latest in a growing number of competition probes that have unnerved Western firms in China.

China's State Administration for Industry and Commerce (SAIC) is also investigating a Microsoft vice president and senior managers, and had made copies of the firm's financial statements and contracts, the agency said on its website.

It said Microsoft, which has struggled to make inroads in China due to rampant piracy, had not fully disclosed information about Windows and its Office software suite.

Microsoft is one of the biggest US companies to fall under the eye of Chinese regulators as they ramp up their oversight in an apparent attempt to protect local companies and customers.

The investigation comes as US-China business relations have been severely strained by wrangles over data privacy.

Investigators raided Microsoft offices in Beijing, Shanghai, Guangzhou and Chengdu on Monday.

The SAIC said it had obtained documents, e-mails and other data from Microsoft's computers and servers, adding that it could not complete the investigation as Microsoft had said some of its key personnel were not in China.

Microsoft has been suspected of violating China's anti-monopoly law since June last year in relation to problems with compatibility, bundling and document authentication, the statement said.

Microsoft said it had been visited by officials and that the company was "happy to answer the government's questions".

Microsoft has already engaged a Chinese law firm to help with the anti-monopoly case. But mystery surrounds the probe, with industry experts and lawyers questioning what, if any, violations Microsoft can have made in China, where the size of its business is negligible. (Reuters)

Irish Independent