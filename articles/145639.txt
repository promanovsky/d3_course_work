'It's just heartbreak': Julia Roberts opens up about the tragic death of her half-sister Nancy in new interview

Julia Roberts has kept silent since the passing of her half-sister Nancy Motes at age 37 of an apparent drug overdose - until now.

The 46-year-old Pretty Woman star fought tears as she described the pain of losing her sibling in the May issue of WSJ Magazine.

'It's just heartbreak,' Julia said of Nancy, who worked as a production assistant on Glee, in the interview that took place less than a month after the tragedy. 'It's only been 20 days. There aren't words to explain what any of us have been through in these last 20 days. It's hour by hour some days, but you just keep looking ahead.

'There aren't words to explain': Julia Roberts opened up her heart to talk about her half-sister Nancy's tragic passing in the May issue of WSJ Magazine

'You don't want anything bad to happen to anyone, but there are so many tragic, painful, inexplicable things in the world.

'But [as with] any situation of challenge and despair, we must find a way, as a family. It's so hard to formulate a sentence about it outside the weepy huddle of my family.'

Julia has found solace surrounding herself with her own loved ones that include husband Danny Moder and their three children, nine-year-old twins Hazel and Finn, and six-year-old Henry.

Heartbreak: The 46-year-old actress revealed that some days it's 'hour by hour... but you just keep looking ahead'; Nancy Motes, right, pictured last year, died at age 37 of an apparent drug overdose in early February Timeless appeal: Julia - who said she finds peace through meditation and chanting - shows off her still youthful beauty, blonder locks and a cosy cardigan on the cover of WSJ

Living in Malibu away from the Hollywood glitz has served as an anchor to the A-lister, while meditating and chanting have helped her find a sense of peace and calm.

'Meditation or chanting or any of those things can be so joyous and also very quieting,' Julia told WSJ, adding that she even practices with her brood.

'We share and just say, "This is a way I comfort myself."'

The photo shoot shows Julia looking more relaxed than ever while clad in a cosy $4,750 cardigan by Celine, as well as a woolly turtleneck and jacket ensemble, a black dress and coat, and then a summery white blouse and trouser set.



Natural beauty: Julia was photographed wearing a light and summery blouse and trouser set

No star trip here: The Oscar-winning Erin Brockovich star, pictured in a fetching dark dress and coat, told the magazine that she keeps her Academy Award at the NY home of her sister Lisa

Home is an eco-friendly, $9.5 million mansion situation on a cliff overlooking the Pacific Ocean.

'I don't consider myself a celebrity, [at least not] how it is fostered in our culture today,' Julia insisted. 'I don't know if I'm old and slow, but there seems to be a frenzy to it.'

Even her children didn't know what a big star their mother was until recently.

'For a long time, they weren't even aware I had a job because I was home so much. Now they get it,' Julia added.

Solace: Julia shared that she meditates and chants daily and she's often joined in the ritual by her three children Next role: Julia will next be seen the drama The Normal Heart, which premieres May 25 on HBO

Julia won an Oscar for her role as a sassy legal assistant in 2000's Erin Brockovich, but her children have yet to see it.

The golden statuette has found a home at the New York apartment belonging to Julia's other sister Lisa which suits her just fine.

'They were doing this photo album where everyone who visited the apartment would pose with it,' the actress revealed.

Family matters: The Pretty Woman has been wed to cameraman Danny Moder since 2003 and they share twins Hazel and Finn, nine, and son Henry, six; the couple were pictured on January 11

Meanwhile, Julia's latest effort The Normal Heart premieres on HBO on May 25, and in it she plays an AIDS doctor who is confined to a wheelchair.

She described what it was like to work again with her husband Danny, who served as cinematographer on the film.

'I find it nerve-wracking in the best schoolgirl kind of way, and he knows that and is a good sport,' Julia said.

As it's the sixth project they've worked on together, Julia added: 'I am usually hoping he's not looking into the camera and thinking, "What is she doing?" We have worked together a lot and whenever we get there, I think, "Why are we doing this again?" But it's great, and it allows us to travel together.'

