By Reuters - (Reuters) - U.S. drugmaker Pfizer Inc approached Britain's AstraZeneca Plc two days ago to reignite a potential $100 billion takeover and was rebuffed, raising investor expectations it will have to increase its offer to close the deal.

Pfizer said on Monday it proposed a takeover to AstraZeneca in January worth 58.8 billion pounds ($98.9 billion), or nearly 47 pounds per share. It had contacted its British rival again on Saturday, seeking to discuss further a takeover.

The chase was welcomed by investors in both companies, as deal-making grips the healthcare industry. Astrazeneca Plc ( ) shares were up 11.7 percent at $76.69 in New York on news of the latest offer, which would be the biggest foreign acquisition of a British company and one of the largest pharmaceutical deals. Pfizer Inc ( ) shares were up 3.8 percent at $31.92 on the New York Stock Exchange on Monday afternoon.

AstraZeneca said Pfizer's suggested offer undervalued the company "very significantly," adding that Pfizer wanted to pay 70 percent in shares and only 30 percent in cash. AstraZeneca urged its shareholders to take no action and said it remained confident of its independent strategy.

"I feel pretty confident of a higher bid coming," said Neil Veitch, global and UK investment director at SVM Asset Management, which owns AstraZeneca shares. "I think it's more likely than not that we'll see an agreed deal somewhere in that 52 to 53 pound range."

Buying AstraZeneca would boost Pfizer's pipeline of cancer drugs and create significant cost and tax savings. Under British takeover rules, Pfizer has until May 26 to announce a firm intention to make an offer or back away.

The renewed approach comes amid a wave of mergers and acquisitions in the sector, pushing the value of deals to $153 billion so far this year, as the industry restructures amid healthcare spending cuts and competition from cheap generics.

"Society wants products faster, they want more products and they want value," Pfizer Chief Executive Ian Read told reporters. "Industry is responding to society's request for increased efficiencies and productivity."

Read said AstraZeneca had declined to engage in talks and the U.S. group was now considering how to proceed, but he remained convinced that combining the two companies made strategic sense and would benefit AstraZeneca investors.

NEW DRUG PIPELINES

Pfizer's original proposal, made to the board of AstraZeneca on January 5, would have valued AstraZeneca shares at 46.61 pounds each - a premium of around 30 percent at the time.

AstraZeneca said the proposal comprised 13.98 pounds in cash and 1.758 Pfizer shares for each AstraZeneca share.

"My guess is it will go for somewhere between 50 and 55 (pounds a share)," said Dan Mahony, a fund manager at Polar Capital, who raised his stake in AstraZeneca in February last year. "I doubt Pfizer will want to go completely hostile."

Most of Pfizer's past deals have been conducted on a friendly basis, including its 2009 purchase of Wyeth for $68 billion. But it has been willing to play hardball if needed, as it did in 2000 with its $90 billion purchase of U.S. rival Warner-Lambert, with which it won full ownership of cholesterol fighter Lipitor, the best-selling drug of all time.

Read told U.S. analysts that since the initial approach to AstraZeneca, both companies had seen experimental drugs fare well in trials. At the same time, Pfizer concluded it was too difficult to pursue big deals domestically while being on the hook for higher U.S. tax rates.

"We're coming from a position of strength, on our near-term pipeline" of experimental drugs, Read said.

Recent favorable data includes results for Pfizer's experimental breast cancer medicine palbociclib.

INDEPENDENT FUTURE

Pfizer's declaration turns up the heat under AstraZeneca Chief Executive Pascal Soriot, who has been in the job since October 2012 and who made clear last week he saw an independent future for the group, flagging spin-offs of two noncore units as one option to create more value.

Soriot has been credited with reviving AstraZeneca's previously thin pipeline of new drugs, badly needed to offset a wave of patent expiries on older drugs, and shares in the group have now risen more than 60 percent under his tenure.

However, his overhaul - including an ambitious plan to move the company's research and corporate headquarters to Cambridge, England - is still a work in progress and he has also come under fire from some shareholders over executive pay.

Read said it was premature to say who would lead a combined company.

Buying AstraZeneca would give Pfizer a number of promising - though still risky - experimental cancer medicines known as immunotherapies that boost the body's immune system to fight tumors. It could also generate significant cost savings for the U.S. group.

Acquiring a foreign company also makes sense for Pfizer as it has tens of billions of dollars accumulated through foreign subsidiaries, which, if repatriated, would be heavily taxed.

The drugmaker has more recently been divesting certain operations, while mega-mergers had fallen out of fashion in the pharmaceuticals industry following skepticism about how well some of them have worked.

HEFTY RETURN

But CEO Read said large deals could also make good sense and buying AstraZeneca would "maintain the flexibility for the potential future separation of our businesses."

The transaction would complement both Pfizer's innovative drug businesses and also its established products business - comprising older and off-patent medicines - which many analysts expect to be eventually spun off.

In addition to using offshore cash, buying AstraZeneca would be tax-efficient since Pfizer could redomicile to Britain and enjoy lower tax rates, thanks to attractive incentives to companies that manufacture and hold patents in the country.

Pfizer envisages combining the two drugmakers under a new UK-incorporated holding company, although the head office and stock market listing would remain in New York. But the suggested deal has triggered worries about jobs in Britain's drug sector, viewed as a key industry by the government.

AstraZeneca has already laid off thousands of staff as it shrinks its cost base to cope with a fall in sales due to patent losses on blockbuster medicines, while Pfizer has shuttered a research site in Sandwich, southern England.

Read said Pfizer had contacted the British government about its plans on Monday, after Finance Minister George Osborne said on Friday that any deal was "a commercial matter between the companies".

A spokesman for Prime Minister David Cameron told reporters on Monday that any Pfizer takeover bid would be assessed by "independent procedures and frameworks."

Bank of America Merrill Lynch, JP Morgan and Guggenheim Securities are advising Pfizer, while Goldman Sachs, Morgan Stanley and Evercore Partners are working for AstraZeneca.