LG's G3 smartphone will release in the next few months, according to quarterly financial results announced by the company.

The flagship handset was confirmed for release "in the second quarter", meaning its launch will take place before the end of June.

GSMArena



The G3 was recently glimpsed in what appeared to be a leaked photograph of the device.

It will apparently include an overhauled volume rocker on the rear, with a flatter design than its counterpart on the LG G2.

The image depicted the phone alongside a notepad thought to be referencing its hardware specs.

Optical image stabilisation, and 16GB and 32GB storage options were mentioned, while the amount of on-board RAM may not have been finalised, as both 2GB and 3GB were cited.

Previous reports suggest that the LG G3 will feature a 1440 x 2560 2K display, an octo-core Odin processor and a 13-megapixel camera.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io