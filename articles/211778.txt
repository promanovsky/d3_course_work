The chairman of Samsung Electronics Co, the flagship of South Korea's biggest conglomerate, underwent a cardiac procedure on Sunday and is now recovering, the Samsung Medical Center said in a statement.

Lee Kun-hee, 72, was admitted to a hospital near his home late on Saturday night, suffering breathing difficulties, and received cardiopulmonary resuscitation (CPR) for symptoms of cardiac arrest.

Lee was then moved to Samsung Medical Center, where he had a cardiac procedure for acute myocardial infarction, and is now in stable condition, the Center added.

Lee is known to have had problems with his respiratory system since surgery for lung cancer in 1999. He was hospitalised with symptoms of light pneumonia last August.

Samsung Group [SAGR.UL], which has assets of around 435 trillion won, spanning handset-making to ship building and medical services, has been restructuring ahead of a widely expected transfer of ownership to the third generation of the founding family.

On Thursday, its IT solutions affiliate, Samsung SDS, announced plans to list shares on the local stock market this year, signalling further restructuring of family ownership in the conglomerate.

Lee has also promoted his children to top positions and shuffled affiliates in recent years.

Samsung Electronics Vice Chairman Jay Y. Lee, Lee's son and heir apparent, is expected to eventually succeed him as chairman of the flagship unit.