Gentiva Health Services, Inc. (GTIV) announced that its Board of Directors unanimously determined to reject the unsolicited, highly conditional tender offer from Kindred Healthcare, Inc. (KND) to acquire all of the outstanding shares of Gentiva, together with the associated preferred share purchase rights, for a price of $14.50 per share in cash.

The company said its Board determined that the Offer is not in the best interests of Gentiva or its stockholders as it significantly undervalues the company and, as such, the Board recommends that Gentiva stockholders reject the Offer and not tender their shares into the Offer.

The Board also noted that the consideration offered to stockholders pursuant to the Offer is not significantly different from Kindred's previous unsolicited proposals made on April 14 and May 5, 2014, both of which Gentiva's Board unanimously rejected after careful consideration.

In reaching the conclusions and in making the recommendation, the Board considered numerous factors such as the Offer is opportunistic in exploiting a temporary decrease in Gentiva's historical stock price and the Offer significantly undervalues Gentiva.

Also, the Offer attempts to improve Kindred's operations in home and hospice at the expense of Gentiva's stockholders and Gentiva has received oral inadequacy opinions and advice from both of its financial advisors.

For comments and feedback contact: editorial@rttnews.com

Business News