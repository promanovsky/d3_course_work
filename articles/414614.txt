Long before Amazon, New Yorkers purchased their deeply discounted electronics from personalities like Crazy Eddie, who would certainly have enjoyed shouting over television commercials about Microsoft's insane new pricing on last-generation tablets.

Microsoft put on its bargain bin hat this weekend as US-based online and retail stores rolled out deep discounts on Surface 2 models for the next month, with prices starting as low as $349 (about £211, AU$376) for the 32GB model.

Beginning August 24 and ending September 27, or "while supplies last," Microsoft Store shoppers can grab up to five Surface 2 tablets for $100 (about £60, AU$108) off last week's price, including a 64GB model with AT&T 4G LTE for only $579 (about £349, AU$623).

As a refresher, Surface 2 models are powered by Nvidia Tegra 4 processors and run Windows RT as opposed to full Windows. The tablet also features 2GB RAM, a 10.6-inch 1080p display and the requisite front and rear cameras.

Our prices are insane!

The timing of the sale is probably no accident given students have already started heading back to school over the last few weeks; Microsoft offered a similar $100 discount on the more expensive Surface Pro in early August of last year.

Thus far, Redmond has yet to produce a third-generatoin RT-powered tablet, instead focusing its energy on the MacBook Air-busting power of its latest Surface Pro 3.

With rumors of Windows 9 making its debut September 30, the sale could also be Microsoft's effort to clear warehouse shelves of unsold inventory ahead of a new RT model announced on the same day.

As Microsoft works to unify its desktop, tablet and smartphone worlds, the latest deal smells like a fire sale as Redmond prepares for something (or somethings) else.

Game on with our review of Microsoft's Xbox One!

Via PCWorld