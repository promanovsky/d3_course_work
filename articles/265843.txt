WINSTON-SALEM, NC - MAY 20: Poet Dr. Maya Angelou celebrates her 82nd birthday with friends and family at her home on May 20, 2010 in Winston-Salem, North Carolina. (Photo by Steve Exum/Getty Images)

American poet and singer Maya Angelou wears a red dress while dancing next to a fire in a promotional portrait taken for the cover of her album, 'Miss Calypso,' 1957. (Photo by Gene Lester/Getty Images)

The death of American author and civil rights activist Maya Angelou has prompted a mass outpouring of grief throughout the world.

The celebrated African American author and poet, who passed away earlier this week at the age of 86, was one of the first black women to enjoy mainstream success as a writer.

An actress, singer and dancer in the 1950s and 1960s, she broke through as an author in 1969 with 'I Know Why the Caged Bird Sings', a best-seller that became standard (and occasionally censored) reading, and was the first of a multipart autobiography that continued through the decades. In 1993, she was a sensation reading her cautiously hopeful 'On the Pulse of the Morning' at former President Bill Clinton's first inauguration.

Speaking of her passing yesterday, President Barack Obama said Angelou was "one of the brightest lights of our time – a brilliant writer, a fierce friend, and a truly phenomenal woman".

Although the wisdom of Angelou dies with her, her profound words will continue to inspire. Below are some of her most famous quotes.

Expand Close Portrait of author Maya Angelou, London, 4th June 1999 (Photo by Martin Godwin/Getty Images) / Facebook

Twitter

Email

Whatsapp Portrait of author Maya Angelou, London, 4th June 1999 (Photo by Martin Godwin/Getty Images)

"Never make someone a priority when all you are to them is an option."

"Try to be a rainbow in someone’s cloud."

"If you don't like something, change it. If you can't change it, change your attitude. Don't complain."

"There is no greater agony than bearing an untold story inside you."

"I do not trust people who don't love themselves and yet tell me, 'I love you.' There is an African saying which is: Be careful when a naked person offers you a shirt."

"We delight in the beauty of the butterfly, but rarely admit the changes it has gone through to achieve that beauty."

Expand Close UNSPECIFIED - CIRCA 1970: Photo of Maya Angelou Photo by Michael Ochs Archives/Getty Images / Facebook

Twitter

Email

Whatsapp UNSPECIFIED - CIRCA 1970: Photo of Maya Angelou Photo by Michael Ochs Archives/Getty Images

"You may not control all the events that happen to you, but you can decide not to be reduced by them."

"My mission in life is not merely to survive, but to thrive; and to do so with some passion, some compassion, some humor, and some style."

"I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel."

Expand Close UNKNOWN LOCATION, - JUNE 3: Poet Maya Angelou on June 3, 1974. (Photo by Craig Herndon/The Washington Post via Getty Images) / Facebook

Twitter

Email

Whatsapp UNKNOWN LOCATION, - JUNE 3: Poet Maya Angelou on June 3, 1974. (Photo by Craig Herndon/The Washington Post via Getty Images)

"I love to see a young girl go out and grab the world by the lapels. Life's a bitch. You've got to go out and kick ass."

"Courage is the most important of all the virtues because without courage, you can't practice any other virtue consistently."

Expand Close WINSTON-SALEM, NC - MAY 20: Poet Dr. Maya Angelou celebrates her 82nd birthday with friends and family at her home on May 20, 2010 in Winston-Salem, North Carolina. (Photo by Steve Exum/Getty Images) / Facebook

Twitter

Email

Whatsapp WINSTON-SALEM, NC - MAY 20: Poet Dr. Maya Angelou celebrates her 82nd birthday with friends and family at her home on May 20, 2010 in Winston-Salem, North Carolina. (Photo by Steve Exum/Getty Images)

"It's one of the greatest gifts you can give yourself, to forgive. Forgive everybody."

"I've learned that you can tell a lot about a person by the way (s)he handles these three things: a rainy day, lost luggage, and tangled Christmas tree lights."

Expand Close Maya Angelou with Hillary Clinton in 2008. / Facebook

Twitter

Email

Whatsapp Maya Angelou with Hillary Clinton in 2008.

“I’ve learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.”

“If someone shows you who they really are, believe them.”

Online Editors