NEW YORK (TheStreet) -- In typical Google (GOOG) - Get Report fashion, the tech giant has found a new industry to disrupt. This time, it's the auto industry.

Google CEO Sergey Brin announced on Tuesday at the Re/code Code Conference in Palos Verdes, Calif., that his company is building a fleet of electric-powered, self-driving cars.

"What I'm excited about is how we could change transportation today," Brin said. "If you look at people who are too old, too young, or disabled, and can't get around, that's a big challenge for them."

Google's official twitter page sent out a link on Wednesday, discussing specifications of the car.

The blog post says that the Google car will not have a steering wheel, brakes or gas pedal, but will come equipped with sensors and software designed to help it steer clear of accidents. The driver will have a button he can push to stop the car in case of an emergency.



WATCH: More tech videos on TheStreet TV | More videos from Brittany Umar

Google said it's planning to build only "about a hundred prototype vehicles," which will be released later this summer and have its speed capped at 25 miles per hour.

"The vehicles will be very basic -- we want to learn from them and adapt them as quickly as possible -- but they will take you where you want to go at the push of a button, and that's an important step toward improving road safety and transforming mobility for millions of people." a Google blog posting said.

Brin hopes to run a small pilot program in California during the next couple of years. His biggest challenge could come from California's Department of Motor Vehicles.

The DMV must write regulations for use of driverless cars by the end of the year. The DMV had believed that driverless cars were several years away.

"That clock just sped up," said Bernard Soriano, head of the DMV's driverless car program, in a statement. "Because of what is potentially out there soon, we need to make sure that the regulations are in place that would keep the public safe but would not impede progress."

Ultimately, Google's driverless car could save humans from their own driving mistakes.

"They (autonomous cars) free us up to check e-mails, play on our phones, take video calls, or just take a nap while getting to work; things that many people try (and fail) to do today," Alec Gutierrez, senior analyst at Kelley Blue Book, said in a memo.

Here is a video from Google showing the car:

At the time of publication, the author had no position in the stock mentioned.

Follow @macroinsights

This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff.



