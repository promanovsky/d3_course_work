The blockbuster telecom and cable mergers of late would also unite some of the most frustrated customers in the nation.

AT&T bid for DirecTV on Sunday. That followed Comcast’s effort to swallow Time Warner Cable. Spurred by these mega-deals, rumors are spreading of other corporate deals among Internet service providers and cable firms. But virtually all of the companies involved in these marriages rank at the bottom in a new customer satisfaction survey, behind even healthcare providers and airlines.

Consumers were happier with how their hospitals and local utilities treated them than the cable repair guy. The IRS ranked lower overall but even then, people preferred filing taxes online to waiting on hold with an Internet service customer representative, according to a poll released Tuesday by the American Customer Satisfaction Index.

The rankings reinforce the industries’ reputations for poor service, which has been a punchline of comedians and fuel for outrage expressed on social media. Who could forget then-septuagenarian Mona “The Hammer” Shaw, who in 2007 went to a Comcast customer service office in Manassas and bashed a computer keyboard with a hammer to demand attention?

But given how much control those companies will exert over how consumers experience the Internet, telecommunications and entertainment, advocates say federal regulators should exercise their authority over the merger process to improve prices and relieve customers of long waits for repairs.

“This is an industry that has a notoriously bad track record with consumers, and these low customer satisfaction scores should give regulators ample reason to be skeptical of a merger,” said Delara Derakhshani, policy counsel for Consumers Union, a nonprofit advocacy group. Her group, which publishes Consumer Reports, found similarly low rankings for cable and telecom providers in its own annual survey.

Within their industries, Comcast and Time Warner Cable consistently ranked last or next to last for phone, Internet service and cable television, according to the ACSI poll. The two firms have proposed a $45 billion merger.

AT&T is willing to pay $49 billion to bundle its wireless, phone, and high-speed Internet with DirecTV’s satellite television business. Both companies rank far below gas stations, the postal service, and hotels.

The poor results in satisfaction surveys are compounded by lawmakers' concerns about ever rising prices for consumers, especially for high-speed Internet, which some say has become equivalent to a utility.

According to a report by the Federal Communications Commission, cable television prices rose 5.1 percent in 2012, three times the rate of inflation.

“We’re witnessing a major transformation of the telecom industry—and it’s going in exactly the wrong direction,” Sen. Al Franken (D-Minn.) said in a statement. “We’re moving toward an industry with fewer competitors—where corporations are getting bigger and bigger and gaining more and more control over the distribution of information. This hurts innovation, and it’s bad for consumers, who have been getting squeezed by higher bills.”

Comcast has publicly acknowledged its problems with service complaints but says it’s working harder to improve. The company said it has invested new technology and training for call service representatives and is offering consumers a shorter window for an agent to show up at their home, rather than having them wait all day.

The cable giant has also created tablet and smartphone apps for consumers to troubleshoot problems directly from home, which has reduced call volume by 21 million in three years. The company gets about 320 million calls every year.

“It bothers us that we have so much trouble delivering high-quality service to customers on a regular basis,” Comcast Executive Vice President David Cohen said in a Senate hearing on the proposed merger last month. “We’re deeply disappointed with where we are. But that will spur us to be even better.”

AT&T and DirecTV did not immediately respond to requests for comment.

The efforts to improve customers service didn’t help Jim Caskey, who said he endured eight cancelled appointments by Comcast’s repair crew in February. The trouble began with a wrong customer service number on his most recent bill, which led him up a thicket of transfers, promises to speak to manager, and long waits on the phone, the Rockville resident said.

Days into his ordeal, a customer service representative said someone had already fixed his downed cable line even though no work had been done. He finally decided to drop Comcast’s service for Verizon. But in many areas around the country, options are limited and consumers cannot switch.

“I’m against the merger with Time-Warner, and anything that can be done to improve cable competition and service in Montgomery County and hold them truly accountable to customers and the community would be appreciated,” Caskey said. “I cannot fathom their arrogant and indifferent approach to customer service.”

So far, customer service has been a side issue at the FCC, which has broad authority to approve or turn down merger requests based on whether they “on balance. . . serves the public interest, convenience and necessity.”

But experts say it is rare for customer service to be the focus of a merger review. This time, given the poor record of the companies, some advocates are urging the FCC to take the consumer experience more seriously.

“Customer service performance is fair game in a merger review,” said Matt Wood, a policy director for Free Press. The FCC in particular has a “broad standard, with more of a comprehensive and forward-looking mandate to protect consumers and competition alike.”