Popular mobile tipster @evleaks published a new 'official-looking' image of the unannounced HTC One Mini 2, featuring similar looks and color options as the HTC One (M8).

HTC, the famous premium handset maker, launched the latest flagship One (M8) in March and has stayed tight-lipped about the miniature version of its flagship. But rumors and alleged leaks in the tech world have been abuzz with the latest one showing an almost-official press image of the HTC One Mini 2.

While the Taiwanese smartphone maker hasn't confirmed the name for the mini version, mobile tipster @evleaks revealed in March that the company is dropping the M8 moniker.

The mobile tipster is at it again, giving a high-quality shot of the HTC One Mini 2 in company's three signature colors that debuted with HTC One (M8). The leaked image shows the design of the new mini handset, including the back panel, which confirms the lack of a Dual Camera setup.

In a separate report from Phone Arena, the absence of a Dual Camera setup in the HTC One Mini 2 was confirmed by one of the sources. The additional camera lens as seen in the HTC One (M8) helps users change focus on a picture by blurring the background or foreground based on the necessity. The end-result is a high quality picture that looks like an image taken from a profession DSLR camera.

The report also added that the unannounced mini version will not be equipped with the rear-facing UltraPixel camera and will instead be replaced by a 13-megapixels shooter.

A similar leaked report last month showed the HTC One Mini 2 with a protective cover. But the most recent image from @evleaks seems official. The leaked technical specifications suggest the HTC One Mini successor could carry a 4.5-inch display with 720p resolution, a 1.4GHz quad-core Snapdragon 400 processor, 1GB RAM, 16GB internal storage and Android 4.4 KitKat with Sense 6.0 UI.

Details on the availability of the handset are scarce but media sources are speculating the launch sometime later this month or next month. However, @evleaks has confirmed in the tweet that the handset will arrive this year.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.