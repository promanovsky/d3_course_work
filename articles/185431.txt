AstraZeneca's chief executive has launched a staunch attack on Pfizer’s £63bn takeover plan, saying it would jeopardise its “fragile” but ambitious drug discovery programme.

Pascal Soriot declared the company would grow annual sales to more than $45bn by 2023 from the current $25.7bn thanks largely to its pipeline of new medicines being developed. It was just that process of creating new drugs that would be jeopardised by a takeover, he said.

“I have been through enough mergers to known a company is not a machine. It is a group of people. If you disrupt their work you can really have a negative impact very quickly.”

AstraZeneca has gone from a position of having 57 drugs in 2012 to 84 now, as it has moved quickly to boost its development programme. Analysts say that, as well as the UK tax status, is what attracted Pfizer to make its bid.

Mr Soriot said: “Our people are very experienced, motivated and excited about what we are doing. We believe we can make an impact on medicine at this time. We have recruited very talented people. When you do this you really want to remain focused on the job at hand. "

He added: "Any distraction, any disruption, could have a negative impact on our ability to develop these products. A pipeline is something that is very fragile. That is why we believe we are better off focused on what we are doing.”

His view chimes with those of many scientists who have spoken out against the huge disruption a takeover of one of Britain’s two key pharmaceuticals champions could have.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Dr Scott Gottlieb, an adviser to pharmaceuticals companies and resident fellow at the American Enterprise Institute, said: “You get really fundamental product breakthroughs when you have small groups of scientists working together on new areas of science over a period of many years. When companies do these mergers, it can break up these teams and break up the continuity.”

Mr Soriot said AstraZeneca’s shareholders had been supportive so far, adding: "We are in a race with many of our competitors to bring our products to the marketplace as quickly as possible. Anything that creates disruption has the potential to destroy value."

He was speaking just hours after the chief executive of top 10 AstraZeneca shareholder Aberdeen Asset Management backed the Labour leader Ed Miliband’s calls for a public interest investigation into the deal.

Aberdeen chief executive Martin Gilbert told The Independent: “AstraZeneca’s management has done a good job refocusing the business and as far as we’re concerned remaining independent remains a viable outcome.

“We do have to look at this in UK terms because it is so important for our research and development. It’s got to be about more than tax benefits.”

Earlier, he told the BBC: “Pfizer unfortunately has this reputation of being ruthless cost-cutters.”

Mr Soriot added that speed of decisionmaking was critical to bringing new drugs to market – a fleetness of foot that could be disrupted by a massive merger.