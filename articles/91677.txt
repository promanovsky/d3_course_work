A U.S. jury ordered Japanese drugmaker Takeda Pharmaceutical Co. and its U.S. counterpart, Eli Lilly and Co., to pay $9 billion in punitive damages over a diabetes medicine linked to cancer. The drug companies said Tuesday they will "vigorously challenge" the decision.



The U.S District Court in western Louisiana ordered a $6 billion penalty for Takeda and $3 billion for its business partner and co-defendant Eli Lilly. It also ordered $1.5 million in compensatory damages in favor of the plaintiff.



The legal fight turned on whether Actos, which is a drug used to treat type-two diabetes, caused a patient's bladder cancer and by implication was responsible for other cases of the cancer.



Takeda and Eli Lilly are facing numerous other lawsuits over the drug, which allege failure to warn about side effects and concealment of its health risks.



Kenneth Greisman, a general counsel at Takeda, said in a statement that the evidence does not support a link between Actos and bladder cancer.



"Takeda respectfully disagrees with the verdict, and we intend to vigorously challenge this outcome through all available legal means, including possible post-trial motions and an appeal," he said.



Lilly spokeswoman Candace Johnson said the drugmaker intends to "vigorously challenge this outcome through all available legal means," including an appeal.



"While we have empathy for the plaintiff, we believe the evidence did not support claims that Actos caused his bladder cancer," Johnson said in a Tuesday morning email.



Punitive damages are often reduced on appeal.



In June 2011, the U.S. Food and Drug Administration published a drug safety update on Actos, which said there was a 40 percent increase in bladder cancer risk in people who used the drug for longer than a year. It required that the cancer risk be added to the medication's warning label. Actos has been sold since 1999.



Actos comes with warnings about various serious side effects, including liver problems and higher risk of broken bones.



A former Takeda medical reviewer, Helen Ge, alleged in a U.S. District Court filing in 2012 that the Japanese drugmaker understated the number of bladder cancer cases possibly linked to Actos in its disclosures to the FDA.



Shares of Lilly were unchanged at $58.62 in premarket trading Tuesday about 45 minutes ahead of the U.S. market open. They had fallen as low as $57.70 earlier in premarket dealings. Shares of Osaka-based Takeda tanked on the Tokyo Stock Exchange, closing down 5.2 percent.

© Copyright 2021 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.