Apple is gearing up to give Microsoft a new challenge as it was announced Tuesday with IBM that the two companies will work together to sell iPads and iPhones specifically designed for businesses.

This move will allow Apple a chance to invade the Windows PC-dominated market, according to PCWorld.

The two have been working on the project for several months, including 100 business-specific software applications that will run on Apple's iOS, but have not disclosed any financial information about it, according to the New York Times.

Timothy Cook, Apple's CEO, told the Times that the applications to iPhone and iPad users in a way that IBM can, and Apple currently cannot.

IBM benefits from the venture by becoming stronger in the business software market.

The news of the joint venture is somewhat surprising since the two companies were head-to-head at the start of the personal computer age in the 1980s, according to the Wall Street Journal.

An Apple commercial titled "1984" portrayed IBM to the likeness of George Orwell's Big Brother and introduced Macintosh as a game changer.

"In '84, we were competitors. In 2014, I don't think you can find two more complementary companies," Cook, who previously worked at IBM, told the New York Times.

But the joint project is also surprising because Apple has had a long-standing reputation of being a strong independent competitor.

Under the agreement, 100,000 IBM employees will be on-site to support the service of the Apple products inside the companies, according to WSJ.

Virginia Rometty, CEO of IBM, told the New York Times that the idea is to "reimagine how work is done" and to "unlock value, remake professions and transform companies."

She said this would be accomplished by combining data analysis with cloud and mobile technology, transforming the devices into business tools.

WSJ cited statistics by Forrester Research to provide a possible reason for the venture. Forrester Research showed global business and governments spent about $11 billion on iPads in 2013, which is about one-third of Apple's total iPad sales. And the estimates are only supposed to grow.