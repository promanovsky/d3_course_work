OCTOBER 14TH, 2011: The Fall Farmers Market in Schaumburg gathers in the parking lot at American Indian Center, 190 S. Roselle Road. Market hours are 7 a.m.-1 p.m. on Fridays through Oct. 28. Contact (847) 923-3855 for more information. (credit: Steve Hardy/CBS Local)

By Heather Sadusky

CHICAGO (CBS) — A new study has found a link between a mother’s exposure to pesticides and her child’s risk of autism.

Conducted in California, the findings have potential global implications for agricultural production and the inevitable use of pesticides.

In Illinois, 80 percent of the land area is used for farming. The 76,000 farms in Illinois cover 28 million acres and mainly produce corn and soy.

All of this generates $9 billion annually for the economy, 40 percent of which is from corn alone, in addition to the sectors surrounding agriculture such as machinery production, food processing, and real estate.

Illinois has extremely fertile land, called loess, which is the nutrient-rich soil that was deposited by retreating glaciers.

Chicago also became the rail center of the country, not to mention the thousands of miles of road and easy access to waterways, making shipping as easy as can be.

The concern lays in modern-day control of agriculture with the use of pesticides, herbicides, and insecticides; chemicals that are sprayed on crops.

Illinois established some of the earliest regulations for these compounds, and the Illinois Pesticide Act in 1979 combined and strengthened all regulations. However, it is clear that if you experience exposure from drift, medical attention should be sought immediately.

Autism spectrum disorders (ASDs) have been increasing all over the world. Just in the United States, “autism spectrum disorders (ASD) are now estimated to affect 1 in 88 eight-year olds, with much higher prevalence in boys (1 in 54) than girls (91 in 252),” according to the study, published by Environmental Health Perspectives..

Genetic occurrences stem from mutated genes, deletions, and duplications of DNA. However, it has been found that environmental factors pose a larger risk for autism than genetics alone.

It is difficult to gauge the amount of pesticides a pregnant woman is exposed to, but the possibilities are widespread.

From residues on foods, to inhaling chemicals in the air, to directly applying pesticides on a lawn, humans are frequently in contact with the compounds made for killing bugs, often in unsuspecting ways.

According to the 2003–2004 National Health and Nutrition Examination Survey (NHANES), evidence of pesticides were detected in 100 percent of pregnant women.

Animal studies suggest there is a critical period of exposure, something that needs to be further studied and understood in humans in order to guard against pesticide vulnerability during these windows.

Additionally, pesticide exposure is not isolated. The majority occurs alongside other compounds such as “flame retardants, plasticizers, and other pesticides,” that need to be researched further in combination to understand interactions, the study found.

Of course, this study doesn’t definitively conclude that there is a connection between pesticide exposure and autism, it is always a good idea to wash your fruit and veggies.