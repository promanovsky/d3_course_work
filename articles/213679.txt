Two of the world's largest technology giants have reached an agreement to settle all of their lawsuits against each other regarding smartphone patents.

Apple and Google announced that they are dropping nearly two dozen lawsuits in US and European courts against each other.

The disputes revolved around the operating systems Apple uses for its iPhone and Google's Android software as well patent infringement accusations from Motorola Mobility, which Google acquired two years ago.

The deal has no bearing on a separate patent issue: Apple's lawsuits against Samsung Electronics Co, also regarding smartphone technology.

Apple and Google said in a joint statement that they would work together on patent reform and that the agreement does not include the cross licensing of technology.