Joanne Milne, 40, Was Born Deaf; Watch Her Tear-Jerking Reaction to Hearing for the First Time Joanne Milne, 40, Was Born Deaf; Watch Her Tear-Jerking Reaction to Hearing for the First Time

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

After 40 years of being deaf and hearing nothing but white noise, Joanne Milne, 40, of Gateshead, England heard music and the sound of her own voice for the first time after an operation last month and she trembled and cried because it was all so amazing.

Milne, who was born with a rare genetic disorder called the Usher Syndrome, also began losing her sight to the disease during her 20s, according to The Journal and is also legally blind.

"Being deaf was just who I was. Unfortunately, when I became registered blind things changed dramatically and for the first time being deaf became increasingly difficult," said Milne.

After the life-changing operation last month, however, Milne's life is now beautifully overwhelming as her brain adjusts to life with sound.

"Hearing things for the first time is so emotional from the ping of a light switch to running water. I can't stop crying and I can already foresee how it's going to be life-changing," she said.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"I'm so happy. Over the last 48 hours hearing someone laughing behind me, the birds twittering and just being with friends … they didn't have to tap my arm to get my attention which [is] a massive leap," she continued.

A YouTube video of Milne discovering she could hear after the operation for the first time was posted Thursday and since then it has been viewed nearly one million times.

Many viewers thanked God and celebrated the modern technology that made her hearing possible.

"Thank you Lord, amazing, the new technology this time, for the first time in her life she hears sounds. You play a nice music for her, the sounds of the birds, what a wonderful grace. Thank you Lord. Amen," wrote Rose Dawn.

"This is so nice. Actual tears here. Bless her. I'm hard of hearing too and couldn't even fathom being completely deaf. Technology is awesome. To think some nations have issues with the 'West' and advances like this," wrote Jason Rutherford.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit