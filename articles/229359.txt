Angelina Jolie may be Hollywood royalty. But she’s no princess, and has never been a fan of the Disney variety.

“I found them quite dull,” the actress says.

Evil Maleficent, whom Jolie portrays in Disney’s new live-action spin on its animated classic Sleeping Beauty, conversely “seemed to be having a great time,” Jolie said.

“[Maleficent] is kind of an anti-hero for the odd man out,” Jolie continued. “And, when she feels abused, or when somebody hurts her, she then says, ‘Oh, all right. You’re going to mess with me? Watch me.’”

In a recent interview to promote Maleficent, which opens in the US on May 30, the conversation veered to celebrities at the Cannes Film Festival campaigning for the return of the Nigerian schoolgirls kidnapped by Islamic extremist group Boko Haram.

“We need to go after [Boko Haram], arrest them, and they need to face justice,” said Jolie, who is known for her worldwide humanitarian efforts. She added, however, that the media shouldn’t be myopic, but instead continue to report on the array of world horrors, including the continuing plight of millions of refugees suffering in Syria.

Last week marked the one-year anniversary of Jolie’s disclosure that she was having a double mastectomy because she carries a genetic mutation that greatly increases her risk of potentially fatal breast cancer. “[I’m] very happy that other women now know that they have that choice,” she said.

Jolie’s next film is her big-budget directorial debut, Unbroken. She previously directed the modest 2011 Bosnian war drama In the Land of Blood and Honey and the 2007 documentary A Place in Time, which chronicled 49 simultaneous moments around the world.

Arriving this Christmas, Unbroken chronicles the life of Louis Zamperini, an Olympic runner who was taken prisoner by Japanese forces during World War II.

“It looks beautiful,” Jolie said. “It’s such a ... it’s big,” she added, with an exasperated laugh. “So, we’re just trying to kind of wrestle it in to the best version of itself.”