At the closing arguments of the Samsung-Apple patent infringement trial, Samsung argued that it did not copy Apple's patents because the technology was first created by Google and Apple never used the specified features in the iPhone.

Samsung attorney Bill Price told the jury that while it is true that if you don't practice a patent, that doesn't mean you can't collect damages for it, but one can't copy something from the iPhone if it's not in the iPhone.

He added that Apple's patents are narrow and cover specific ways of performing tasks, not the entire tasks-such as universal search or word suggestion - themselves, CNET reports.

Price said that Samsung simply used Google technology given to every other Android maker and that it became more successful than its Android rivals because of its hardware, the report adds.

Apple had filed a patent infringement lawsuit against Samsung, seeking 2.2 billion dollars in damages.