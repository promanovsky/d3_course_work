Earth’s clear and present danger

Share this article: Share Tweet Share Share Share Email Share

Cape Canaveral - The chance of a city-killing asteroid striking Earth is higher than scientists previously believed, a non-profit group building an asteroid-hunting telescope said on Tuesday. A global network that listens for nuclear weapons detonations detected 26 asteroids that exploded in Earth's atmosphere from 2000 to 2013, data collected by the Comprehensive Nuclear Test Ban Treaty Organisation shows. The explosions include the Feb. 15, 2013, impact over Chelyabinsk, Russia, which left more than 1 000 people injured by flying glass and debris. “There is a popular misconception that asteroid impacts are extraordinarily rare ... that's incorrect,” said former astronaut Ed Lu, who now heads the California-based B612 Foundation.

The foundation on Tuesday released a video visualization of the asteroid strikes in an attempt to raise public awareness of the threat.

Asteroids as small as about 131 feet (40 meters) - less than half the size of an American football field - have the potential to level a city, Lu told reporters on a conference call

“Picture a large apartment building - moving at Mach 50,” Lu said.

Mach 50 is 50 times the speed of sound, or roughly 38 000 mph (61 250kph).

Nasa already has a program in place that tracks asteroids larger than 0.65 mile (1km). An object of this size, roughly equivalent to a small mountain, would have global consequences if it struck Earth.

An asteroid about 6 miles (10km) in diameter hit Earth some 65 million years ago, triggering climate changes that are believed to have caused the dinosaurs - and most other life on Earth at the time - to die off.

“Chelyabinsk taught us that asteroids of even 20-meter size can have substantial effect,” Lu said.

City-killer asteroids are forecast to strike about once every 100 years, but the prediction is not based on hard evidence.

B612 intends to address that issue with a privately funded, infrared space telescope called Sentinel that will be tasked to find potentially dangerous asteroids near Earth. The telescope, which will cost about $250-million, is targeted for launch in 2018. B612 takes its name from the fictional planet in the book The Little Prince, by French author and aviator Antoine de Saint-Exupery. The video can be seen on the B612 Foundation website https://b612foundation.org/ - Reuters