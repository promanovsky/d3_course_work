Disneyland raises prices to $96 per ticket just days before Memorial Day weekend - the second increase this year



With Memorial Day weekend kicking off summer for millions of Americans this weekend, Disneyland is squeezing families with a price increase.



New prices posted quietly to the park's Web site without fanfare now quote a price of $96 for a one-day, one-park ticket to Disneyland or California Adventures. That's a $4 increase.



A one-day park hopper pass giving access to both Disneyland and California Adventures has been raised $13 to $150.



Just days before families flood Disneyland for Memorial Day weekend, the park has raised ticket prices and will now charge $96 for one day pass

Along with raising the price on its annual passes, KTLA reports the park also suspended sales of its

DISNELAND TOPS PRICES FOR ONE DAY'S ADMISSION AMONG THE MOST POPULAR THEME PARKS

Disneyland - $96 Six Flags - $41.99

Sea World - $80

Universal Studios - $87

Epcot - $73.50

Hershey Park - $59.95

Southern California annual pass, a popular option that gets holders into both parks on Sundays.

People who've already purchased the pass will be able to renew it at an increased cost of $379.



The Southern California Select annual pass is still available for purchase at $289 but cannot be used on weekends.



A Deluxe annual pass which can be used on Saturdays and Sundays will now run $519.



The most expensive annual pass is the Premium at $699, which includes cost of parking and has no blockout days.



Parking will also cost more, with a single day running $17 for cars and motorcycles.



Unlike prior cost increases, this week's was not accompanied by an advance warning from Disneyland.

Disappointed parents said they they were upset one of the few family friends options for the weekend was charging customers even more

The cost increase comes only five days before the park's 'Rocky Your Disney Side' 24-hour event.



The park will open at 6 a.m. Friday and close on 6 a.m. Saturday morning, according to the Disneyland site.



'It's really sad because … with families, where can you go?' said disappointed parent Rita Webb, outside of the Anaheim theme park. 'This is supposed to be a trip for kids and families.'