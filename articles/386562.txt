Although the exact reason why Alzheimer's disease develops still remains elusive, scientists report that they've found a new protein that may play an important role in the devastating memory illness.

What they don't yet know is whether or not this new protein -- called TDP-43 -- is a cause of Alzheimer's disease, or if it's something that develops due to Alzheimer's disease.

It's too early to know if this finding could have any effect on the diagnosis, treatment or prevention of Alzheimer's disease. For now, "we really need to understand what this protein is doing and its relationship to other proteins," said the study's lead author Dr. Keith Josephs, a professor of neurology at the Mayo Clinic in Rochester, Minn.

Scientists studying Alzheimer's disease have long been interested in two types of proteins in the brain known as beta-amyloid and tau. In Alzheimer's patients, the proteins create a kind of gunk in the brain made up of "plaques" from beta-amyloid and "tangles" from tau.

Experts believe the plaques and tangles may cause Alzheimer's, Josephs said, but they don't know for sure.

The new study looked at a third protein called TDP-43, which has been linked to amyotrophic lateral sclerosis (ALS, also known as Lou Gehrig's disease) and senility caused by damage to the frontal lobe of the brain.

The researchers examined the brains of 342 people who'd been diagnosed with Alzheimer's disease through autopsies. (Alzheimer's cannot be definitively confirmed when patients are alive.) Almost 200 of them had TDP-43, according to the study.

The study is unlike some others because it looked at two types of patients who were diagnosed with Alzheimer's after death -- those who showed symptoms in life and those who didn't. Abnormal levels of TDP-43 were found in those who had the disease and were significantly affected by it.

After adjusting their statistics so they wouldn't be thrown off by various factors, the investigators found that those with abnormal levels of TDP-43 were 10 times more likely to have thinking problems such as memory loss at death than the other patients.

How could people have signs of Alzheimer's, but not have symptoms? That's not clear, Josephs said. But, maybe people who have plaques and tangles don't develop symptoms unless they also have TDP-43, the researchers hypothesized.

How the new protein might be connected to Alzheimer's is also a mystery. "It's possible that it takes a while to get this protein," he said. "Maybe you have to have Alzheimer's developing for 25 years before this protein comes into play," Josephs suggested.

Dr. Anton Porsteinsson, director of the Alzheimer's Disease Care, Research and Education Program at the University of Rochester School of Medicine in Rochester, N.Y., noted that the protein in question has been linked to other brain diseases.

"Does this maybe mean that neurodegenerative disorders are part of a broader continuum?" asked Porsteinsson, who's reviewed the findings of the new study. He added that the diseases may share a similar cause or similarities in their later stages.

Josephs said he plans on continuing research into the role this protein plays in Alzheimer's.

The study findings are scheduled to be presented Wednesday at the Alzheimer's Association International Conference in Copenhagen, Denmark. Until published in a peer-reviewed journal, findings presented at meetings should be viewed as preliminary.