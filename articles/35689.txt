A morning rally wilted Friday afternoon, dragging the S&P 500 off a new intra-day high as investors took a portion of their winnings off the table late in the session and squared positions ahead of the weekend. Declines also picked up speed after mid-day comments from St. Louis Fed President James Bullard defended hawkish comments by Federal Reserve Chair Janet Yellen earlier this week following the central bank's last policy-setting meeting.

Healthcare stocks led the afternoon selloff, followed closely by shares of technology and consumer discretionary companies although most sectors finished well off their earlier highs. The S&P rose to a new all-time high of 1,883.97 about 10 minutes into today's session, again climbing within a point of that high-water mark about 90 minutes later before starting an extended slide lower through the rest of the session.

Treasury yields jumped and stocks sagged earlier this week after Yellen said in her first press conference as Fed chair that rates could rise "around six months" after the central bank completes tapering of its bond-buying stimulus efforts later this year.

Bullard, speaking in Washington today, said Wall Street should not have been surprised by Yellen's remarks, noting "the surveys that I had seen from the private sector had that kind of number penciled in as far as I knew."

Commodities were mixed. Crude oil for April delivery was up 56 cents to settle at $99.46 per barrel while April natural gas was down 6 cents at $4.31 per 1 million BTU. April gold rose $5.60 to finish at $1335.80 per ounce while May silver fell 12 cents to $20.31 per ounce. May copper rose 2 cents to $2.95 per pound.

Here's where the markets stood at the end-of-the-day:

Dow Jones Industrial Average down 25.91 (-0.16%) to 16,305.14

S&P 500 down 5.58 (-0.30%) to 1,866.43

Nasdaq Composite Index down 42.50 (-0.98%) to 4,276.79

GLOBAL SENTIMENT

Hang Seng Index up 1.20%

Shanghai China Composite Index up 2.72%

FTSE 100 Index up 0.38%

UPSIDE MOVERS

(+) ECYT, Committee for Medicinal Products for Human Use of the European Medicines Agency issued positive opinions for joint product development with Merck ( MRK ). Also reports positive results for prospective lung-cancer drug in early-stage testing.

(+) LIN, Accepts $1.6 bln buyout from Media General ( MEG ), which will pay $763 million in cash and issue another 49.5 million shares of its stock for the television broadcaster. Other TV operators also rose after the deal was announced.

(+) UPIP, Sells patent portfolio consisting of 21 patent families - including patents for 3G and LTE mobile technologies - to Chinese computer-maker Lenovo for $100 mln in cash. Also inks term-based licensing pact.

DOWNSIDE MOVERS

(-) AIR, Reported worse-than-expected financial results for Q3 late Thursday and cut its guidance for fiscal 2014 below Street expectations.

(-) SYMC, Stock hit with downgrades after CEO abruptly terminated.

(-) DRAM, Will sell $659,250 of its common stock to institutional investors at a 36% discount.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.