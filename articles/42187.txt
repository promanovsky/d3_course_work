HTC One M8 vs Galaxy S5 in-store battle begins

It’s a fight that’ll be fought on carrier displays – the HTC One M8 vs the Samsung Galaxy S5. Both devices have hit carriers this afternoon – the Samsung Galaxy S5 as early as this past Friday – with intent on taking the cake in Android-based smartphone supremacy. Of course they’re both aimed at taking the whole smartphone pie, but who is competing with who?

This battle isn’t just one fought on a level of device specifications. The Samsung Galaxy S5 and the HTC One M8 are more similar to one another than any previous device generation, both with 1080p displays, the Samsung device with just a 0.1-inch difference in its screen size. Both machines even work with the same processor – Qualcomm Snapdragon 801.

The differences, therefor, are in the software and the implementation of camera equipment. What we’re speaking about here, though, is how they’ll be marketed. For those of you looking to pick up either device starting this week, you’ll need to decide on a carrier. While the Galaxy S5 starts its fight with AT&T – in stores now – you’ll be heading to Verizon for the HTC One M8.

You can find the HTC One M8 in Verizon stores – not all, but apparently most – starting today at 1PM EST. That’ll be before this article is even published. That’s a mere 2 hours after the device was first officially introduced to the world.

At Verizon, the HTC One M8 is being offered in a 2-for-1, buy one get one offer right off the bat. Just so long as you sign up for a 2-year contract with both, you can get two HTC One M8 devices for $199.99 USD. Off-contract the HTC One M8 will set you back a cool $649 USD – that’ll be the same for the Developer Edition and the Google Play Edition as well. Outside the USA it’s a different story.

The Samsung Galaxy S5, meanwhile, is up in AT&T alone for now – both devices will be headed to each of the other top carriers in the United States, but it’s likely the HTC One M8 will arrive first. At AT&T, the Galaxy S5 is available for $199.99 with a 2-year contract or $649.99 off-contract. There are also a variety of “AT&T Next” plans that allow you to spread your payments over the course of 12 or 18 months, as well.

So for the same price on (eventually) the same carriers, it’ll be down to the wire with the HTC One M8 and Samsung Galaxy S5. Will advertising decide this battle, or will it be a war of social media supremacy? Surely we wouldn’t see the device with the more pleasing set of specifications win, would we?