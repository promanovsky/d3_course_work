Social media site Weibo, known also as "the Twitter of China," is expected to start trading on the Nasdaq this week. The social microblogging site's initial public offering of 20 million American depository shares to be priced between $17 and $19, value the company at about $3.9 billion.

On CNBC's "Fast Money," PrivCo CEO Sam Hamadeh said that the IPO price range is either very undervalued, or Twitter is one of the most overvalued stocks in existence.

"Weibo should be valued at $30 a share if you apply any of the Twitter metrics to it," he said. "This is one of the most underpriced IPOs we've seen in years. You've got social, mobile, China all at once. It's beautiful."



Sina-owned Weibo, which goes public on Thursday under the ticker symbol "WB," is growing faster than Twitter, and that will be reflected in the stock's price within the first month of trading, Hamadeh said.

"We expect the pop probably in the mid-twenties at least on the first day and at some point in the next couple of weeks it will trade closer to $30 which is still a good value."

Read More Sina Weibo files for $500 million US IPO

