The rapper and producer has made headphones cool again. Now selling his stake in Beats Electronics will give Dre a personal fortune of $800m – nudging him ahead of P Diddy

Name: Dr Dre.

Age: 49.

Appearance: Distinguished, very well-off.

What is he a doctor of? He's a rapper and record producer, real name Andre Young. The doctorate is sort of an honorary thing.

I was going to say that he sounds a bit overqualified. He's doing all right: Dr Dre is about to become rap's richest man.

Is he not already? The title currently belongs to P Diddy, who according to Forbes is worth $700m. But Dr Dre, former NWA member, Grammy-winning producer and Eminem and 50 Cent collaborator, now looks set to surpass him.

Wow. I had no idea there was so much money in misogynist rhyming. There isn't quite. Dr Dre's big payday would come from the sale of his Beats headphones empire to Apple.

Wow. I had no idea there was so much money in headphones. If there is it's due largely to Dr Dre and his partner, Jimmy Iovine, who started Beats Electronics in 2008. They made headphones fashionable, imbuing them with a streetwise, overpriced cool that only Apple's money can buy.

Roughly how much money are we talking about? With the sales of Beats Electronics and its fledging music streaming service, Beats Music, about $3.2bn

Come again? It's a big number. If it goes ahead it will be Apple's largest ever acquisition.

And Dr Dre will be hip-hop's first billionaire! That's what the headlines say. Pity it isn't true.

Of course it is! Do the maths! If you insist: the good doctor is presently worth $550m. He co-founded Beats, but last year Carlyle Group acquired a minority shareholding for $500m. His present stake is thought to be somewhere between 20 and 25%.

Which means? That after the sale – and after tax – Dre will be worth $800m; ahead of Diddy and Jay Z, but some way shy of his first billion.

Well he can always play a few concerts if he needs some cash. Or he could release his long-awaited but yet-to-arrive album Detox. Some say it was shelved to prevent a lukewarm reception damaging the Beats brand.

Do say: "He reached millions with his music, and made millions with his headphones."

Don't say: "Doctor, I'm having a slight problem with my hearing."