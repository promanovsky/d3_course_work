SXSW: Neil Young Introduces Ponos to the World -- While Its CEO Raises Some Eyebrows

Rock Hall of Famer vows to improve sound quality in the post-MP3 era, as company's business model is questioned.

The location was Ballroom D of the Austin Convention Center, where the legendary Neil Young presented his final thesis -- oddly altruistic sales pitch may be more accurate -- on a long-simmering project: the Ponos music player.

Young's presentation centered on the loss of the recording industry after the success of MP3s, attributing the collapse of the studio system and its attendant businesses -- everything from engineers to delivery persons shuttling equipment around Los Angeles -- to the sonically inferior format.?

"When all the artists and engineers, all the arrangers and musicians that played on giant tracks by people like Phil Spector, with 12 tambourines and two pianos -- those people were still in the studios in Los Angeles, playing. But all of [that] started to die -- it was the most amazing thing, this vibrant creative culture started to go away. And it was because of the MP3 and the cheapening of the quality to the point where it was practically unrecognizable."

"We were selling shit," Young said, referring to MP3s. "They were buying wallpaper."

PHOTOS: The Scene at SXSW 2014

?Young made much of "192 kHz," which is the kHz sampling rate that recordings can be created at in the studio and the upper limit of Pono's output. By way of comparison, 44.1 kHz/16 bit is the standard sampling rate that is used for CDs.

Young also addressed the effect that general MP3 quality recordings have had on the music creation process. "It became beat-heavy, it became right for what the media was for, what was selling it. Clever, tricky, a lot of impressive things. For people like me, it was like 'Whoa … I don't wanna do that.' "

Young then presented a video of a long list of musicians raving after being played a demo of the player. Afterward, Pono CEO John Hamm took the stage for a Q&A session with Young and USA Today technology reporter Mike Snider.

And then things got awkward.

After addressing questions about Pono's ability to play existing digital music libraries (it can), its file system (it uses FLAC) and its somewhat awkward triangle design ("We wanted something iconic," Hamm said), the last question from an audience member shifted the tone of the entire presentation.

Taking the microphone, a young man asked: "What's your cut?" Referring, of course, to Apple's now-famous 30 percent cut of sales on the iTunes Store.

STORIES: SXSW: Jay Z and Kanye West Reunite for Samsung Concert

Hamm, after a flustered moment, responded, "It surprises most people that everyone who buys music from the record labels pays exactly the same amount." To which several audience members shouted, "What?!"

"That's a delicate question, isn't it?" asked Young.

After which, Hamm turned to the moderator, slightly flushed at this point, and said, "We can end it."

"You can answer the question if you like," Snider said.

Hamm shook his head slightly before Snider closed the discussion.

?It was an unfortunate end for Hamm, Young and Ponos to what was on the whole a fairly benign talk, one in which Young had expressed little worry about the company failing because, he said, even if they were put down by a large tech company accomplishing the same thing, music lovers would still win in the end by hearing audio as it was intended.

The fact that a very simple question derailed it so thoroughly is, at the very least, proof positive that the record business is not one for the faint of heart -- and perhaps that even the most seemingly benevolent entrepreneurs may have something to hide.