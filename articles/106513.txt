LOS ANGELES -- Jerry Lewis says women are funny, but not as crude standup comics.

The 88-year-old entertainer was criticized for expressing his distaste for female comedians a few years ago, but in clarifying his comments, he called Lucille Ball "brilliant" and said Carol Burnett is "the greatest female entrepreneur of comedy."

But women who "project aggression" onstage rub him the wrong way because women bear children, "which is a miracle," he said.

Lewis was accompanied by his wife and daughter Saturday as he left his hand and footprints in cement outside Hollywood's Chinese Theatre. Quentin Tarantino introduced Lewis, who planned to attend a 50th anniversary screening of "The Nutty Professor" later that evening as part of the TCM Classic Film Festival.