The Ebola virus, which has caused a global health scare in recent days after spreading rapidly through parts of West Africa, is not a threat to the United States, according to the Centers for Disease Control and Prevention, or CDC.

In the latest outbreak of the Ebola virus, 729 people have been killed and 1,323 people have been infected across Liberia, Guinea and Sierra Leone. The World Health Organization, or WHO, on Thursday sanctioned a $100 million plan to widen efforts to contain the virus' spread while the CDC announced it would send 50 people to assist in containment efforts, and issued a travel alert for West Africa, suggesting that nonessential people avoid travel to the region.

"This is a tragic, painful, dreadful, merciless virus. It is the largest, most complex outbreak that we know of in history," Thomas Frieden, director of CDC, said on Thursday, according to an NBC report. However, “Ebola poses little risk to the U.S. general population,” and that its spread across the U.S. "is not in the cards," Frieden said.

Atlanta’s Emory University Hospital said on Thursday that it is preparing a special isolation unit to house a patient with Ebola “within the next several days” but did not say if the patient would be one of two infected Americans -- Kent Brantly and Nancy Writebol -- who reportedly contracted the virus while working in a crowded medical facility in Monrovia, Liberia.

"There is nothing particularly special about the isolation of an Ebola patient, other than it is really important to do it right," Frieden said, noting that hospitals in West Africa had turned into “amplification centers” for Ebola, which will not happen in a country with modern facilities.

To contain the spread of the virus, which occurs with exposure to blood and bodily fluids, it is necessary to quickly isolate patients, as well as track everyone who has come in close contact with the patient and observe them to ensure they have not been infected, Amesh Adalja, an infectious disease specialist at the University of Pittsburgh Medical Center, told NBC.

“That is what has stopped every Ebola outbreak that has ever happened before and that is what is going to stop this Ebola outbreak,” Frieden said.