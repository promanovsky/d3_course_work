Apple’s iOS 8 comes with a number of new features, one of those that we haven’t heard about up until now is how AirPlay will work in iOS 8.

At the moment if you want to stream something to your Apple TV from your iOS device, or play music on an AirPlay speaker, your iPhone or iPad has to be connected to a network.

When iOS 8 is released, you will be able to stream content using Airplay without having to connect your device to a network, and the content can be streamed direct to an Apple TV without you being connected to WiFi.

This feature has been available on other devices with things like DLNA, which will let you stream content direct to devices and TVs via a direct connection on DLNA compatible devices, this has been widely supported by Android devices for quite some time.

Apple announced at WWDC 2014 that they would be releasing iOS 8 some time this fall, we are expecting it to be available in September, along with the new iPhone 6 and the iWatch.

It comes with a wide range of new features, and deeper integration into Apple’s desktop OS, which will also be getting an update this fall.

As soon as we get some more information on exactly when Apple will launch Apple iOS 8, and also the new iPhone 6, we will let you guys know.

Source Engadget

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more