Nearly two years after its launch, Jessica Alba's startup The Honest Co. is now valued at nearly $1 billion.

Co-founded by Christopher Gavigan, Sean Kane and Brian Lee, the company sells natural, non-toxic items, ranging from household cleaning supplies to baby products. The products are available for purchase either online or at select retail stores in the United States and Canada.

The business raised $70 million from Wellington Management Company so far, with the help from existing investors, including ICONIQ Capital, General Catalyst Partners, Institutional Venture Partners and Lightspeed Venture Partners. According to industry experts, it is worth nearly $1 billion as it moves toward an initial public offering.

The Wall Street Journal reported that the company's annual revenue, which approximately 80 percent of it comes from the monthly subscription service, is expected to be more than $150 million in 2014.

Jeremy Liew, a partner with Lightspeed Venture Partners, said that the "Sin City" actress being "an international star" has certainly benefitted the company.

"We're starting to see a lot of demand from her fans around the world," he said. "This idea for non-toxic, chemical free products has resonated around the world in developed and developing countries."

"We value the support of our partners and customers in helping us make a meaningful impact in the marketplace," said Alba. "We dream big at The Honest Company and continuously strive to make the world healthier and more sustainable - today and in the future."

Jessica Alba's co-founder Lee added in a statement released on Tuesday: "The Honest Company provides families everywhere with safe, convenient and affordable family essentials. This funding will enable us to expand our current category offerings and availability, and develop products across many new and exciting categories."