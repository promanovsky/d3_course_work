Blackberry CEO John Chen has said he is "very pleased" with the progress the firm has made, as the beleaguered smartphone manufacturer reports a net loss of $423 million (£254m) in the three months to the beginning of March.

The loss amounts to roughly 8-cents a share when restructuring charges and other one-time items are taken into account.

This is considerably less than the loss of 55-cents a share that analysts polled by Thomson Reuters had predicted for the Canadian company.

"I am very pleased with our progress and execution in the fourth quarter against the strategy laid out three months ago," John Chen, chief executive officer of Blackberry, told the Toronto Star newspaper.

"We have significantly streamlined operations, allowing us to reach our expense-reduction target one quarter ahead of schedule."

Such streamlining has included a plan to eliminate 40% of the company's workforce though significant restructuring, as well as selling many of its Canadian real estate holdings.

Quarterly figures released by Blackberry revealed smartphone sales fell from 1.9 million to 1.3 million from the previous quarter, the majority of which were Blackberry 7 devices rather than its new line of Blackberry 10 devices.

Despite this, Chen claimed: "Blackberry is on sounder financial footing today with a path to returning to growth and profitability."

The outlook from the firm stated that it is aiming to break-even by the end of its 2015 financial year, with the focus expected to be on providing secure mobile device services on the internal networks of large clients.

The full losses over the past financial year amounted to $5.87 billion (£3.5bn), which compares to a loss of $646m (£388m) from the previous year.