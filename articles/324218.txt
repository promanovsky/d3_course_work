During the keynote speech at the Google I/O, you may have noticed the announcement of Android Auto, which promises to bring the power of Android version 'L' to your car. Google have partnered up with over 40 carmakers in the form of the Open Automotive Alliance (OAA) to bring Android to your next car. This leads us neatly to today's news that a new member of the OAA, Volvo, will be showing off its latest XC90 Sports-Utility-Vehicle (SUV) at the Paris Motor Show in October with Android Auto fully integrated.

H¥kan Samuelsson, the President and CEO of Volvo Cars, stated that "Google's approach to user-centricity and application of technology to improve peoples' everyday lives makes Android Auto a perfect addition to the Volvo experience." He went on to say that Android users will feel "at home in the new Volvo."

Advertisement

Android Auto will allow the driver to access Google Search, Google Maps, Google Play Music and other specially adapted apps such as Spotify and Pandora either by voice control, steering wheel controls or via the use of Volvo's large touch-screen display in the centre console. Volvo says the vertical portrait-oriented touch-screen display will allow users to view both content from Android Auto as well as Volvo's content without the need to switch between car and Android phone screens.

Volvo are staying neutral in Apple and Google's battle to have their software present in your next car, and as such have committed to supporting Apple's CarPlay system as well. This is made possible by the use of Volvo's new Scalable Product Architecture in all of its new models. It makes sense to support the two most widely used mobile operating systems, because why would any car manufacturer want to limit sales to just one demographic?

As we found out yesterday at the Google I/O, Android is going places. Whether its to your wrist in the form of an Android Wear smart watch, wearing a Google Glass device or having an Android TV box in your livingroom, or Android Auto in your next car, Google's vision is clearly to bring a seamless experience to its fans.

Sponsored Video

Is Android Auto a feature you would consider when buying your next car? If the car you were interested in purchasing didn't support Android Auto, would you still purchase it? Let us know in the comments or at our Google Plus page.