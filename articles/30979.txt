Bill Gates, Microsoft CEO Satya Nadella, and former Microsoft CEO Steve Ballmer Microsoft

On Thursday, we learned that Microsoft accessed a blogger's Hotmail account to find evidence that the blogger had been leaked information about Windows 8 before the software was released in 2012.

The news comes from an indictment of a former Microsoft employee who was arrested this week and charged with leaking the information to the blogger. As part of its investigation, Microsoft peeked into the blogger's Hotmail account without a court order to verify the blogger did in fact receive the leaked Windows 8 information.

As you can imagine, it caused quite an uproar. Microsoft was essentially searching a blogger's email simply because the blogger had a super-deep source inside Microsoft.

In response to the backlash, Microsoft defended its actions, saying it has the right to peek into email accounts it hosts (like Hotmail and Outlook) if its legal team believes the user has stolen intellectual property (IP) stored there. And it doesn't need a court order to do so.

Here's how Microsoft explained what happened:

During an investigation of an employee we discovered evidence that the employee was providing stolen IP, including code relating to our activation process, to a third party. In order to protect our customers and the security and integrity of our products, we conducted an investigation over many months with law enforcement agencies in multiple countries. This included the issuance of a court order for the search of a home relating to evidence of the criminal acts involved. The investigation repeatedly identified clear evidence that the party involved intended to sell Microsoft IP and had done so in the past.

As part of the investigation, we took the step of a limited review of this third party’s Microsoft operated accounts. While Microsoft’s terms of service make clear our permission for this type of review, this happens only in the most exceptional circumstances. We apply a rigorous process before reviewing such content. In this case, there was a thorough review by a legal team separate from the investigating team and strong evidence of a criminal act that met a standard comparable to that required to obtain a legal order to search other sites. In fact, as noted above, such a court order was issued in other aspects of the investigation.

Later in the evening, Microsoft's legal team further defended its actions, but said it was going to take an extra step when determining whether or not it should snoop into someone's private email without a court order.

Specifically, these cases will go under the same review process they do now with Microsoft's legal team. After that, Microsoft will take a new step and send the case to an outside legal counsel who is a former federal judge to determine whether or not it's justified in searching someone's email. If the outside legal counsel determines a court would've approved such a search anyway, then Microsoft will go ahead and snoop into that person's email.

In this particular case, Microsoft says it believed there was a good chance the blogger planned to sell some of the information he had received, which gave the company even more of a cause to search the blogger's email.

Again, this would be without a court order. Microsoft's view is that it doesn't need a court order to search itself and can't even get one if it wanted to.

Here's the full statement from Microsoft's legal counsel:

We believe that Outlook and Hotmail email are and should be private. Over the past 24 hours there has been coverage about a particular case, so we want to provide additional context and describe how we are strengthening our policies.

In this case, we took extraordinary actions based on the specific circumstances. We received information that indicated an employee was providing stolen intellectual property, including code relating to our activation process, to a third party who, in turn, had a history of trafficking for profit in this type of material. In order to protect our customers and the security and integrity of our products, we conducted an investigation over many months with law enforcement agencies in multiple countries. This included the issuance of a court order for the search of a home relating to evidence of the criminal acts involved. The investigation repeatedly identified clear evidence that the third party involved intended to sell Microsoft IP and had done so in the past.

Courts do not, however, issue orders authorizing someone to search themselves, since obviously no such order is needed. So even when we believe we have probable cause, there’s not an applicable court process for an investigation such as this one relating to the information stored on servers located on our own premises.

As part of the investigation, we undertook a limited review of this third party’s Microsoft operated accounts. While Microsoft’s terms of service make clear our permission for this type of review, this happens only in the most exceptional circumstances. We applied a rigorous process before reviewing such content. In this case, there was a thorough review by a legal team separate from the investigating team and strong evidence of a criminal act that met a standard comparable to that required to obtain a legal order to search other sites. In fact, as noted above, such a court order was issued in other aspects of the investigation.

While our actions were within our policies and applicable law in this previous case, we understand the concerns that people have. Therefore, we are announcing steps that will add to and continue to strengthen further our policies in any future situations involving our customers. Specifically:

We will not conduct a search of customer email and other services unless the circumstances would justify a court order, if one were available.

To ensure we comply with the standards applicable to obtaining a court order, we will rely in the first instance on a legal team separate from the internal investigating team to assess the evidence. We will move forward only if that team concludes there is evidence of a crime that would be sufficient to justify a court order, if one were applicable. As a new and additional step, we will then submit this evidence to an outside attorney who is a former federal judge. We will conduct such a search only if this former judge similarly concludes that there is evidence sufficient for a court order.

Even when such a search takes place, it is important that it be confined to the matter under investigation and not search for other information. We therefore will continue to ensure that the search itself is conducted in a proper manner, with supervision by counsel for this purpose.

Finally, we believe it is appropriate to ensure transparency of these types of searches, just as it is for searches that are conducted in response to governmental or court orders. We therefore will publish as part of our bi-annual transparency report the data on the number of these searches that have been conducted and the number of customer accounts that have been affected.

The privacy of our customers is incredibly important to us. That is why we are building on our current practices and adding to them to further strengthen our processes and increase transparency.