The 2014 Annual Meeting of the American Society of Clinical Oncology (ASCO) just wrapped up in Chicago, Illinois with meetings and presentations from May 30 to June 3.

At this year's 50th Annual Meeting, more than 25,000 cancer specialists gathered to discuss research on the theme of "Science and Society."

On Friday, new research on breast, head/neck and advanced cancer was presented.

Preserving Fertility

AstraZeneca's (NYSE: AZN) phase III clinical trial of women taking goserelin (Zoladex) with chemotherapy for early-stage, hormone receptor-negative breast cancer, found they were 64 percent less likely to develop early menopause. By shutting down ovarian function, goserlin has helped to preserve fertility in the study.

Two years after starting chemotherapy, eight percent of the women who took the drug experienced ovarian failure versus the 22 percent of women who only received chemotherapy.

Related: Survey: Is Carl Icahn In Trouble Following The FBI And SEC Probe?

In addition to preserving fertility, overall survival rates also improved. Women taking goserlin were 50 percent more likely to be alive four years after starting chemotherapy than those who only received chemotherapy.

Reducing Side Effects

Researchers also discussed the dosing frequency of bone-modifying drugs, such as Novartis' (NYSE: NVS) zoledronic acid (Zometa), to improving the care of patients with bone metastases.

Following a phase III clinical trial of women taking zoledronic acid to treat breast cancer that spread to the bone, researchers found that women can scale back from monthly treatments to quarterly ones. By receiving the treatment to four times a year, women have a decreased risk of serious side effects and see a reduced cost.

In aiming to reduce the long-term side effects of cancer drugs, early research on lowering the dose of radiation therapy for some individuals with oropharyngeal cancer shows it is an "effective" option. The research suggests a better quality of life with a lower risk on serious long-term side effects.

Other new research showed that individuals who have less than a year to live can stop taking statins without shortening their time stamp even more. By discontinuing statins, or cholesterol-lowering drugs, patients can have less symptoms and an overall better quality of life.