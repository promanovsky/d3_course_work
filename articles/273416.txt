Koalas hug trees to lower their body temperature during hot weather, a study has revealed.

The paper, published in the Royal Society journal Biology Letters, said that koalas move to the lower part of trees and press their bodies on the trunk to cool down.

The researchers, led by a team from the University of Melbourne, noticed that in the winter the animals would stay high in the trees. During summer, however, they would move to the lower part.

Dr Michael Kearney explained: "They're just stuck out on the tree all the time so when hot weather comes they're completely exposed to it.

"The fur on their tummy is quite a lot thinner than the fur on their backs, so they're pushing that fur and that part of their body as much against the tree as possible."

According to the study, koalas do not seek shelter in dens and can suffer high mortality during extreme heat events.

"Any way that they can lose heat that doesn't involve losing water is going to be a huge advantage to them," Kearney said.

"Dumping heat into the tree is one of those methods."

The team used thermal cameras to analyse the koalas' behaviour during hot weather.

"When we got the images back it was so obvious what the koala was doing," explained Kearney.

"You could see the koala sitting on the coolest part of the tree trunk with its bottom wedged right into the coolest spot.

"If we had thermal vision, it would have been an obvious thing."

Dr Justin Welbergen from James Cook University told the BBC that thermal images show exactly how animals can exploit these cooler areas in trees.

"This helps them to maximise their chances of survival during extreme heat events."

The study is part of a wider research project investigating the effect of climate on land-dwelling animals in Australia.

The researchers said that this tree-hugging behaviour will become increasingly common as the continent experiences longer and hotter heat waves.