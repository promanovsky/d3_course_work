Leading science journal Nature has withdrawn a flawed stem-cell study which was hailed as a "game-changer" in the quest to grow transplant tissue when it was published in January.

The decision was taken after mistakes were discovered in some data published in two papers, photograph captions were found to be misleading, and the work itself could not be repeated by other scientists, it said.

"All co-authors of both papers have finally concluded that they can not stand behind the papers, and have decided to retract them," the journal said in an editorial.

The controversy is the biggest in scientific publishing in a decade. Nature said it would tighten procedures to vet future studies submitted for publication.

On 4 June, Japan's Riken research institute said lead scientist Haruko Obokata had agreed to retract the papers after an investigation.

Riken was "still discussing" a retraction with co-author Charles Vacanti of Harvard University, a spokeswoman said at the time.

Ms Obokata was feted after unveiling findings that appeared to show a straight-forward way to reprogramme adult cells to become stem cells - precursors that are capable of developing into any other cell in the human body.

Identifying a readily-manufacturable supply of stem cells could one day help meet a need for transplant tissues, or even whole organs for transplant.

The researchers claimed to have created so-called Stimulus-Triggered Acquisition of Pluripotency (STAP) stem cells.

They said white blood cells in newborn mice were returned to a versatile state by incubating them in a solution with high acidity for 25 minutes, followed by a five-minute spin in a centrifuge and a seven-day spell of immersion in a growth culture.

Until then, mammal cells had never been successfully reprogrammed to a youthful state through simple physical manipulation instead of genetic intervention.

Nature said the Riken probe had uncovered "inadequacies in data management, record-keeping and oversight" and the institute had disciplined Ms Obokata.

In the scientific world, claims gain credibility when they are published in a peer-reviewed journal, where a panel of independent experts vets the submitted paper for inaccuracies or inconsistencies.

The British-based Nature and US-published Science journals are widely considered to be the gold standard for the peer-reviewed publishing process.

Publication in these journals is an enormous boost for scientists and the institutes that back them.

In its defence, Nature said on Wednesday it had enquired as to whether the results of the experiments "had been independently replicated in the laboratories of the co-authors", and was given assurances in this regard.

However, it said, the reviewers' "rigorous reports quite rightly took on trust what was presented in the papers."

"Although editors and referees could not have detected the fatal faults in this work, the episode has further highlighted flaws in Nature's procedures and in the procedures of institutions that publish with us," Nature said.

It said it had introduced improvements, including an extension to annexes in papers that detail experimental methods used in research, and using statistical advisors to check data.

The controversy is the biggest since that of South Korea's Hwang Woo-suk, who published two papers in Science a decade ago, claiming to have created human embryonic stem cells by cloning.

Initially hailed as a national hero, he fell into disgrace when much of his data was found to have been faked and he had breached ethics standards.