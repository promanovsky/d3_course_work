Edward C. Baig, and Jon Swartz

USATODAY

SEOUL — In the South Korean industrial town of Gumi, about a 45-minute helicopter ride southeast of here, Samsung Electronics factory workers, nearly all of them young women, are methodically applying the finishing touches on Galaxy S5s.

The launch of Samsung's newest flagship smartphone on April 11 was a little over a week away, and though most of the Galaxy S5 manufacturing process in the factory is automated, workers on this day were putting the backs of AT&T Galaxy S5 phones on or manually removing the stickers on certain tiny components.

Not far away on the Gumi campus, Samsung's very first mobile phone, the SH-100, is on display. A monstrously large and heavy (1.54 pounds) contraption compared to today's models, it launched during the 1988 Seoul Olympics. Samsung pays homage to its remarkable mobile past at Gumi, where walls of about 1,800 phones introduced by the Korean electronics giant are encased behind glass and displayed by year.

Could 2014 be the year Samsung KOs rival Apple in its dogged pursuit of the mantle as top consumer-tech brand? Silicon Valley's marquee heavyweight bout is all the buzz in tech.

In this escalating slugfest, Samsung has become tech's Joe Frazier to Apple's Muhammad Ali, less flashy but tenacious in battering its opponent with a flurry of new products. Apple's product arsenal remains select — by design.

They compete on the airwaves, with clever take-downs of each other's products. They trade legal barbs in courtrooms, over complicated legal patent disputes. They were principal combatants after a record-breaking selfie from Ellen DeGeneres. But they mostly compete in showrooms, where nothing less than the $300 billion worldwide smartphone industry is at stake. Samsung sold 314 million smartphones last year — twice that of Apple. But iPhone and iPad remain dominant in the U.S., cultural icons that many consumers are reluctant to abandon.

It won't be easy. Apple still commands the loyalty of its customers who re-buy its highly regarded products at an industry-high rate, and it still can make or break a market with one product. (See iPhone, iPad and — perhaps? — iWatch.) Innovation remains a hotly contested battleground, in markets and in court, between Apple and Samsung.

"Apple's approach is much more sustainable," says Ben Bajarin, an analyst at Creative Strategies. "IPhone is still dominant in the U.S.; Apple's hardware and software are highly differentiated from what else is in the market; and it has incredible brand loyalty."

What the Korean electronics giant is attempting to do is nothing short of a sea change in tech: upend the competitive and innovative landscape, and displace Apple as the go-to choice for consumer-electronics devices.

Apple may be the main event, but it's also worth watching to see how Samsung contends with other tech heavyweights, notably longtime Android partner Google. On its newest smartwatch, Samsung dumped Android in favor of the software it has its own vested interest in called Tizen. But Samsung has also pledged support for Android Wear, Google's own emerging new wearable computing initiative.

The launch of the S5 and three new wearable devices underscores Samsung's relentless pursuit of not just consumers and big business but the most coveted of commodities in Silicon Valley: attention.

It has spent the better part of two years flooding the market with cereal spoon-dropping gadgets and blitzing the airwaves with ads in an audacious bid to cajole consumers weaned in the age of iPhone and iPad. Samsung has stolen market share from Apple in smartphone and tablet sales, both worldwide and in the U.S.

At the same time, it is climbing Fortune's annual Most Admired Companies list, and is now No. 21, still far off from Apple, which has topped Fortune's list for seven straight years.

Going where Apple is absent

Samsung's stated goal is to hit revenues of $400 billion by 2020, up from $217 billion, and be a top-five global brand by then. By any measure, Samsung is already an international powerhouse, a status Chairman Lee Kun-Hee set in motion as far back as 1993, when he declared that Samsung would have to "change everything except your wife and children."

This month, the company offered USA TODAY unfettered, behind-the-scenes access to its testing and design labs here. It outlined its product plans and a glimpse into its highly regimented business culture. (USA TODAY paid for airfare and hotel.)

It's nearly lunchtime at Samsung's sprawling Digital City headquarters in Suwon, outside Seoul. The spacious Delacourt (Delicious & Delightful Court) cafeteria is teeming with employees. A sign on the wall, in English, reads "Eat Love Work." The atmosphere is collegial.

Some employees skip the food: They're swimming in a gigantic indoor pool, jogging along an indoor track or exercising in a health club facility. Someone explains the hours are restricted during the day. Folks are expected back at work.

Samsung has out-flanked Apple by venturing to parts of the market where the Silicon Valley company is absent — with larger phones, larger tablet displays, smartwatches and aggressive pricing — says Charles King, principal analyst at Pund-IT.

The strategy has worked swimmingly. Between 50% and 60% of all Android phones branded worldwide are Samsung. "There was a jump ball (a few years ago) for which hardware manufacturer would compete with Apple: Nokia, BlackBerry, Motorola and HTC," says Gene Munster, senior analyst at investment bank Piper Jaffray. "Samsung was not part of the discussion then. Now, it is a ball hog."

"When we introduced the Galaxy Note in 2011, we raised some eyebrows, but we knew the larger screen would enhance the consumer's experience," says Gregory Lee, CEO of Samsung North America. "We won consumers over with the large-screen smartphone and ultimately started the trend for other manufacturers to follow."

The Next Big Thing (and bet)

Samsung's Sisyphean quest to unseat Apple rests, in part, on the success or failure of its latest flagship, the Galaxy S5. That will be among the biggest story lines of the intensifying rivalry this year. So will Samsung's deeper push into wearable technology, with the Gear Fit, Gear 2 and Gear 2 Neo smartwatches that arrived just as the new phone did, and ahead of whatever Apple has up its sleeve in the wearable computing category. Samsung's first smartwatch was a dud.

Samsung competes in other areas where Apple is a mere bystander, notably household appliances such as refrigerators and washer-dryers. It envisions health and medical equipment as an area of future growth.

But the fuller narrative is about how the Korean electronics giant cultivates its relationship with consumers worldwide. The products are there, but is a loyal, long-standing audience, asks Munster. He points to 90% repeat buy-rates for Apple products.

Samsung invests heavily in R&D, to the tune of $13.6 billion or about 6.3% of annual sales last year. Indeed, more than 69,000 Samsung employees globally, a fourth of its workforce, are dedicated to R&D or focused on future products.

"There is a philosophy in Samsung that says we start from the consumer and incorporate the future in (them)," Samsung Executive Vice President Donghoon Chang said in an interview in Seoul.

Shifting its center of gravity

What Samsung also intends to offer consumers is a new business wrinkle. The company is making a "lifestyle play," as it did when Academy Awards host DeGeneres crashed Twitter with a selfie that was retweeted a record 3.4 million times. (To be fair, DeGeneres tweeted from an iPhone backstage.)

So-called "organic moments" as those not only highlight Samsung products — a Note 3 phone in Ellen's case — but resonate with consumers, says Todd Pendleton, chief marketing officer of Samsung Telecommunications America.

"The Ellen selfie was one of those moments you can't plan or orchestrate," Pendleton says. "It was a true moment of capturing joy among some of the most famous people in the world."

Apple under Steve Jobs famously chose the products it thought best for the customer. Samsung takes a near polar opposite approach, relying heavily on customer input and market research.

Apple, which has been noticeably quiet this year, declined to comment for this report.

Analysts say Apple's silence merely masks an ambitious new product lineup in the works, led by the long-rumored iWatch, larger-screen iPhone 6, iPad Air 2 and Apple TV.

Apple's possible product road map highlights the impact Samsung has had on it and the rest of the tech industry.

The technological showdown comes against a backdrop of legal jousting. While Samsung says it received 4,676 patents in the U.S. in 2013 — second only to IBM — Apple and Samsung resumed their patent battle in federal court in San Jose, Calif., this month, with claims and counter-claims of ripping off smartphone and tablet designs and features.

The court contretemps adds to a battle that could linger for years, given the size and market prowess of each company.

On a cool April afternoon, a Samsung helicopter lands in Gumi on Samsung's Smart City campus. Inside an assembly plant, the room is well lit. A wall adjacent to the factory floor shows pictures of workers who found time to volunteer their time — it is labeled the Corridor of Volunteer Activity. But it's all business now. The women are laser-focused on the delicate tasks at hand.

In the slugfest against Apple and others, there is apparently still much to be done at Samsung.

Baig reported from Seoul, Swartz from San Francisco.