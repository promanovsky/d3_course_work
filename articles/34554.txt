BALTIMORE (WJZ)—It’s the perfect match. Today dozens of University of Maryland medical students learn the next step in their careers as they’re paired with residences.

Gigi Barnett reports for some of them, match day is a long time coming.

Four years of study at the University of Maryland School of Medicine comes down to one envelope.

Inside is the answer to where each student will start his or her residency.

“Looks like I’m heading to Ann Arbor to do internal medicine at the University of Michigan,” one student said.

On the same day every year, medical students around the nation are matched with their residencies.

David Knipp is the first-ever, fifth-generation Maryland medical school graduate. He wants to practice radiology.

His father and grandfathers weren’t matched like this.

“There wasn’t all of the fanfare that it is today. I think they just got handed an envelope in private and that was it,” Knipp said.

While students select their first, second and third choices, it’s a computer that has the final say.

“If the student ranks a program and the program has them on a list in a position that is favorable, then the student gets the spot at that program,” said Dr. Donna Parker, associate dean of Student Affairs.

This is the first time the Maryland medical school hosted match day at the Hippodrome. Every year more and more family members want to hear where these future doctors are headed.

The bulk of Maryland medical students are pursuing residencies in internal medicine, pediatrics and family medicine.

All 167 Maryland medical school students were matched with residences; 31 percent of them will stay in Maryland for training.

Other Local News:

[display-posts category=”local” wrapper=”ul” posts_per_page=”5″]