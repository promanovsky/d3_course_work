The Amazon AppStore has been around for a couple of years now, and it's been growing pretty rapidly. Now it's still one of the smaller app stores around, behind the Apple AppStore and the Google Play Store, but 240,000 apps is nothing to sneeze at either. Although, according to a report from IDC, Amazon Kindle Fire app developers are seeing higher revenue, about 65% higher, compared to other platforms. As well as a better average revenue per user, around 75% better. Now it's important to remember that this report from the IDC was also commissioned by Amazon. It's still probably only for Kindle Fire users here, but the Amazon App Store is available for every Android device, even though it can get confusing with the Google Play Store and the Amazon App Store. Amazon's full press release is down below for those interested.

SEATTLE–(BUSINESS WIRE)–Jun. 16, 2014– (NASDAQ:AMZN)–Amazon today announced that Amazon Appstore selection has nearly tripled in the past year and developers continue to report strong monetization from the apps they offer in the store. The Amazon Appstore now has over 240,000 apps and games, is available in nearly 200 countries and on a multitude of devices. Additionally, Amazon Coins have become widely popular–customers have spent hundreds of millions of Amazon Coins on apps, games and in-app items. The broad growth in the Amazon Appstore is enticing developers–the number of new developers joining the Amazon Appstore per month close to doubled in the last year. To learn more about developing apps and games for Amazon, visit https://developer.amazon.com/appsandservices/.

Advertisement

According to an IDC survey commissioned by Amazon, developers building apps and games for Kindle Fire are making at least as much money (often more) on the Kindle Fire platform as on any other mobile platform. IDC conducted a survey of 360 smartphone and tablet application developers. The survey examined developers' experiences selling apps on the Kindle Fire platform. Here's what the study says:

65% of developers said that Total Revenue on Kindle Fire is the same or better than developers' experience with other platforms.

74% of the same developers said that Average Revenue per App/User is the same or better on Kindle Fire than other platforms.

76% of developers indicated that the Kindle Fire platform helps them connect with new market segments–an important indicator that the Kindle Fire platform can be a significant source of net-new business and "reach" for developers at a time when new market segments may be difficult to find on competing platforms, the study noted.

"Developers tell us that they experience improved reach, greater monetization, and, oftentimes, higher revenue when they have their apps and games in the Amazon Appstore," saidMike George, Vice President of Amazon Appstore and Games. "But this is just the beginning–we're building more services and capabilities for developers and more Android-based APIs based on their feedback. Most Android apps just work on Kindle Fire, and with an Appstore made for Android devices, Amazon's Appstore can help developers distribute their apps on Android devices all over the world. It's a great time for developers to bring their apps to the Amazon Appstore."

Here's what developers are saying:

Sponsored Video

"Amazon users are very high quality users. User engagement with edjing on Amazon is higher than on any other platform. In proportion, Amazon users spend more time in edjingthan any other platform. In addition, the Average Revenue Per Download (ARPD) on Amazon is actually higher than on Android." –Jean-Baptiste, CEO, DJiT

"Getting our app on Kindle Fire was very easy and it gives developers a great opportunity to distribute and monetize their apps. Monetization in the Amazon Appstore jumped 100% using Amazon's In-App Purchasing API and Mobile Ads API in the first month since implementation." –Daniele Calabrese, CEO, Soundtracker

"We already develop for Android, so getting our games on the Amazon Appstore was a real breeze." –Paul Case, TribePlay, developer of Dr. Panda.

Advertisement

Amazon Appstore includes an array of services that make Amazon the most complete end-to-end ecosystem for developers building, monetizing and marketing their apps and games for customers. These capabilities include:

The ability for app developers to use Amazon Web Services' (AWS) technology platform for their infrastructure needs. Building blocks such as Amazon Elastic Compute Cloud (EC2), Amazon Simple Storage Service (Amazon S3), and Amazon DynamoDB allow developers to focus on what differentiates their app rather than the undifferentiated heavy lifting of infrastructure.

(AWS) technology platform for their infrastructure needs. Building blocks such as Amazon Elastic Compute Cloud (EC2), Amazon Simple Storage Service (Amazon S3), and Amazon DynamoDB allow developers to focus on what differentiates their app rather than the undifferentiated heavy lifting of infrastructure. Amazon SNS Mobile Push enables developers to easily send push notifications to Apple, Google and Kindle Fire devices using one simple API, and easily scale from a few notifications a day to hundreds of millions of notifications per day or higher.

enables developers to easily send push notifications to Apple, Google and Kindle Fire devices using one simple API, and easily scale from a few notifications a day to hundreds of millions of notifications per day or higher. Login with Amazon, which allows developers to easily reduce sign-in friction for their customers by conveniently letting them login in with their Amazon, Facebook or Googleidentities, leading to higher engagement and order conversion.

which allows developers to easily reduce sign-in friction for their customers by conveniently letting them login in with their Amazon, Facebook or Googleidentities, leading to higher engagement and order conversion. In-App Purchasing on Kindle Fire, Android, Mac, PC and web-based games. This enables developers to offer their digital items in apps and games while allowing end users to simply use their Amazon accounts to make the purchase.

on Kindle Fire, Android, Mac, PC and web-based games. This enables developers to offer their digital items in apps and games while allowing end users to simply use their Amazon accounts to make the purchase. Mobile Associates API for Kindle Fire and other Android devices. This enables developers to merchandise millions of physical and digital items from Amazon.com within their apps and games.

for Kindle Fire and other Android devices. This enables developers to merchandise millions of physical and digital items from Amazon.com within their apps and games. Mobile Ads , which helps developers monetize their apps by displaying high quality, relevant display ads from brand advertisers, including Amazon.

, which helps developers monetize their apps by displaying high quality, relevant display ads from brand advertisers, including Amazon. GameCircle , which includes capabilities like Achievements, Leaderboards, Friends and Whispersync for syncing games across Android and Fire OS, and leads to better engagement with games.

, which includes capabilities like Achievements, Leaderboards, Friends and Whispersync for syncing games across Android and Fire OS, and leads to better engagement with games. A/B Testing and Analytics, which helps developers across iOS, Android and FireOS track user engagement metrics or test different in-app experiences across iOS, Android, and Fire OS, giving developers the tools they need to increase user engagement and monetization.

About Amazon.com

Amazon opened on the World Wide Web in July 1995. The company is guided by three principles: customer obsession rather than competitor focus, passion for invention, and long-term thinking. Customer reviews, 1-Click shopping, personalized recommendations, Prime, Fulfillment by Amazon, AWS, Kindle Direct Publishing, Kindle, Fire, and Fire TV are some of the products and services pioneered by Amazon.