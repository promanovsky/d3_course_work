The disappearance of a routine flight from Kuala Lumpur to Beijing with 227 passengers and 12 crew aboard remains shrouded in uncertainty. With wildly differing theories about the loss of the Malaysia Airlines jet, the MH370 mystery is becoming to the early 21st century what the JFK assassination was to the 20th. We look at the key questions – and offer some cautious answers.

Has Malaysia been keeping information secret?

Information has certainly been ineptly managed since the Boeing 777 went missing on 8 March, but I have not seen convincing evidence that it has been deliberately withheld by Malaysia Airlines or the Malaysian government. They have both stressed the need to communicate openly with the relatives of the 239 people on board, but also to verify information before it has been released.

The key accusation that the Malaysians must answer, once the search is completed, is why it failed to act immediately on the key data supplied by Inmarsat about the jet’s last hours? The London-based firm told investigators of the vital information three days after the disappearance of the Beijing-bound flight. But it took a further four days before the futile search in the South China Sea was abandoned, and prolonged the unimaginable anguish of the relatives.

How significant is the lack of a distress signal from the pilots, and of evidence of passengers using their mobile phones?

Very significant. Starting on the flight deck: the order of priorities of any pilot is clear: aviate (ie keep the aircraft flying), navigate (achieve the desired course and altitude), communicate. If the crew were dealing with a complex and unforeseen problem, they would quite properly focus on the issue rather than contacting the ground or other aircraft. But it appears that a number of manoeuvres were performed that would seem to allow enough time for one or other pilot to broadcast a warning.

If passengers became aware of an unusual or sinister development, it is highly likely that at least some of them would switch on their mobile phones – a few of which would be expected to register on networks while briefly flying over the Malayan peninsula. But the Malaysian Communications and Multimedia Commission reports no such contact. It is possible that passengers were unaware of the change in course, or that they were incapacitated, for example by depressurisation of the cabin.

INDY/ GO Weekly Newsletter TIME TO TRAVEL! Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/GO newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ GO Weekly Newsletter TIME TO TRAVEL! Thanks for signing up to the INDY/GO newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Once the aircraft was some distance from land, any contact with ground stations would be impossible.

Were the pilots involved?

“Probably a suicidal pilot,” says the aviation expert Chris Yates. “I see no other conclusion to draw, other than a vague possibility that a passenger or passengers took control.” There has been much speculation about the backgrounds and states of mind of the captain and first officer. However, many other theories have been put forward. Could something as trivial as a spilt cup of coffee on the flight deck cause an electrical short circuit, which smouldered and produced enough smoke to overcame the pilots before they could broadcast a Mayday? Or could some other event have disabled the communications systems and incapacitated the flight crew?

Has hijacking/terrorism now been discounted?

The suicidal terrorism demonstrated to such deadly effect on 9/11 was aimed at mass murder and targeting iconic structures. Had these been the motives with MH370, then the obvious targets would be the Petronas Towers in Kuala Lumpur - until a decade ago the tallest buildings in the world. Traditional hijacking involves the perpetrator demanding to be flown to a different destination, and does not usually result in the destruction of the aircraft. So unless a macabre new strain of evil has been devised, it is difficult to identify a motive. In addition, no credible claims have been made by any terrorist group for the loss of the jet.

Will they ever give up looking for the aircraft?

Compared with the search for traces of Air France flight 447, which crashed in the Atlantic between Rio and Paris five years ago, looking for MH370 is an order of magnitude more difficult. The distance from the shore is much greater, the sea conditions much heavier and the clues about possible locations much less certain. It is an extraordinarily expensive business. But so much is riding on finding out what happens, that the search is likely to continue indefinitely. The need for some sort of closure for the relatives is clear. But the aviation community is also desperate to learn more – in case there is some previously unidentified issue with the aircraft, and to establish responsibility before the legal process begins.

If they find it, will they raise it?

Search teams will want to recover as much as possible of the debris, to get clues about the aircraft’s final moments – and the sequence of events that led to the loss. They will home in on the flight data recorder, which keeps details of commands from the flight deck, and the cockpit voice recorder. The latter, though, may not yield many clues since it has only a two-hour capacity, and therefore cannot reveal what happened over the Gulf of Thailand.

Has bad news about a crash been given by text message previously?