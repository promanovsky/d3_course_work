A Georgia grand jury has indicted film producers Randall Miller, Jody Savin and Jay Sedrish on charges of involuntary manslaughter and criminal trespass in connection with the February death of camera assistant Sarah Jones.

Jackie L. Johnson, district attorney for Georgia’s Brunswick Judicial Circuit, which includes Wayne County where the accident occurred, announced the indictments on Thursday.

Involuntary manslaughter carries a potential prison sentence of 10 years, according to Georgia law. Criminal trespass is a misdemeanor and carries a year’s jail sentence.

The death of Jones, 27, galvanized film crew members throughout the entertainment industry, highlighting longstanding concerns about worker safety. Last year, in a separate accident, three people died during a shoot for a Discovery Channel show when a helicopter crashed in Acton, Calif.

Advertisement

On Feb. 20, crew members were working on the film “Midnight Rider” about the life of rock singer Gregg Allman. They were instructed to walk out on an old railroad trestle high above Georgia’s Altamaha River, where they placed a metal-frame bed on the tracks for an action scene.

William Hurt was scheduled to play Allman in the film.

As the crew prepared for a dream-sequence scene, a train came barreling toward them.

Crew members tried to pull the bed off the train tracks but were unable to. The train hit the bed frame and Jones, killing her.

Advertisement

In Thursday’s announcement the district attorney’s office said that Miller and Savin were the owners of Unclaimed Freight Productions Inc., which was filming “Midnight Rider.” Miller was also director of the film.

Sedrish was an executive producer.

Several other crew members were injured in the accident on the railroad tracks and trestle, located in Doctortown Landing.

The film’s production was suspended after the accident. Hurt withdrew from the production after Jones’ death.

Advertisement

The film’s producers did not have permission to film on the railway trestle itself, which is owned by the railroad company CSX.

The Wayne County sheriff’s office investigated the case. The district attorney presented the case to the grand jury on Wednesday.

A spokeswoman for the district attorney’s office said there would be no further comment because the case is pending.

Staff writer Richard Verrier contributed to this report.