FERNDALE (WWJ) – For Taco Bell lovers, the run for the border now begins with breakfast.

When the fast food joint launched its new breakfast menu on Thursday, locals gathered at the Ferndale location on 9 Mile Road to test it out.

The menu puts a new spin on breakfast with items like the Waffle Taco — featuring eggs with bacon or sausage wrapped in a waffle — and an A.M. Grilled Taco that comes with egg, cheese and bacon or sausage.

Steve Wilson told WWJ’s Laura Bonnell he just had to stop by to give it a try.

“I thought when I heard they had a breakfast now, I hurried right on over this morning,” Wilson said. “I love sausage, I love egg too and all together there, it tastes great.”

Wilson said he planned to have will have lighter meals throughout the day rest of the day since he splurged on breakfast, but he’ll be back another morning.

Ryan Smith, a 19-year-old college student, said when he heard about the Waffle Taco, he was instantly intrigued.

“Normally I wouldn’t eat a breakfast like this in the morning,” said Smith, “but just for today I’m going to try it. If I like it, maybe once a week or something like that.”

According to the Taco Bell website, the Waffle Taco is about 460 calories with 30 grams of fat.

Other more traditional fast-food breakfast items are included such as a breakfast burrito with steak, bacon or sausage, and Cinnabon Delights, to name a few.

To view the entire Taco Bell breakfast menu, click here.

MORE: Viral Video: Ronald McDonald Loves Taco Bell