The automated music recommendation service Pandora Media Inc. increased its ad-free service fee by $1 per month to cover the increasing cost of licensing tunes and royalties paid to the artists.

On Tuesday, Pandora One announced that the cost of streaming ad-free music increases as the royalty rates paid to musicians through SoundExchange have increased by 53 percent. It is also eyeing another nine percent increase by 2015.

Because of that, the company was prompted to implement some changes with its subscriptions.

Starting this week, annual subscriptions that will end soon will be notified that the update is applied to their subscription. That is for them to decide if they will still continue using the service. Monthly pricing of $4.99, on the other hand, will take effect on May. That is to allow new subscribers to take advantage of discounted loyalty pricing if they subscribe before the changes are applied.

However, changes will hardly be felt by active subscribers as existing Pandora monthly subscribers will continue paying $3.99 monthly.

Furthermore, Pandora One, which has more than 250 million subscribers, is taking away the annual subscription option for new users.

Reuters reported that the mark up and other changes are expected to affect 3.3 million of its users who pay for Pandora's ad-free music streaming service.

In its blog post, Pandora One, which debuted in 2009, expressed need of understanding from its users saying, "We hope that you understand why we have taken these steps. Our goal is to continue to be your go-to internet radio destination and enable you to hear the music you love wherever and whenever you want."

The company's active listeners, though it reportedly fell by 2.8 million from 76.2 million in December 2013 to 73.4 million in January 2014, increased by 12 percent compared to the same time period in the previous year.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.