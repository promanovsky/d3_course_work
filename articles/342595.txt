Pippa Middleton talks about adjusting to her overnight fame and says she has at times felt bullied by the media. Courtesy Sunrise.

Pippa Middleton talks about adjusting to her overnight fame and says she has at times felt bullied by the media. Courtesy Sunrise....

THE Duchess of Cambridge’s sister Pippa Middleton has described how she has felt “publicly bullied a little bit” since being propelled into the spotlight following the royal wedding.

Pippa, who gave her first ever television interview to American journalist Matt Lauer for the US Today show, spoke of how it was difficult to read things about herself which were untrue.

Middleton, who is a columnist for Waitrose’s monthly magazine, gained notoriety for her figure-hugging bridesmaid’s dress when Kate married the Duke of Cambridge in 2011.

Her party book, Celebrate, was marketed in 2012 as a one-stop guide to entertaining throughout the year.

But the publication was mocked by some for its over-simplistic advice and even sparked a parody Twitter account, @PippaTips.

“I’ve had amazing opportunities and I feel very fortunate to have opportunities and to have access to things that maybe I wouldn’t necessarily,” party planner Middleton said.

But quizzed on whether she had faced criticism on social media sites and online, she said: “It’s hard sometimes because I have felt publicly bullied a little bit just by, you know, when I read things that clearly aren’t true or that, whichever way someone looks at it, it’s a negative side.

“It is quite difficult. Because effectively I’m just paving my way and trying to live a life like any 30-year-old.

“I think people feel they can say something about you online or on a web page when they would never say it to your face but they think that’s okay. It’s been difficult.”

Middleton discussed her nephew Prince George, describing him as “amazing”, adding: “He’s a very dear boy and he’s brought a lot of pleasure and fun for all of us, the whole family.”

She revealed that her famous bridesmaid’s dress was meant to blend in with her sister’s train, describing the interest in it as “embarrassing,” adding: “The dress was almost meant to be insignificant.”

The outfit is still in her wardrobe at home. “I think I’ll just keep it there,” she said. “I think it’s the sort of thing that I’m sure I’ll bring out if someone wanted to see it or my children one day want to see it. Then I’ll show them but yes it’s tucked away.”

She described her relationship with Kate as “very close” and said they spent a lot of time together.

Middleton said she did not initially grasp the scale of the royal wedding. “It sounds funny to say, but we saw it, as just a family wedding. And actually, I didn’t realise, perhaps, the scale of it until afterwards,” she revealed.

Pippa, who recently took part in a 4828km charity cycle ride across the US, said she had a few plans in the pipeline, including to carry on writing.