The Shanghai Free-Trade Zone, the first of its kind in China, is getting ready to welcome the Xbox One, the first foreign gaming console to reach mainland China since a 2000 ban.

The news comes roughly a year after Microsoft and BesTV reached an agreement to market the 360's successor. The Xbox One hits the world's third-largest gaming market on Sept. 23.

Selling for a price roughly $200 higher than the "Kinected" Xbox One in the U.S., Microsoft's latest gaming package has been priced at $700 for the Chinese market. The unKinected console will sell for roughly $600.

Microsoft's partnership with China's BesTV came in anticipation of the lift of a 14-year-old ban that restricted foreign gaming consoles from entering China.

"We're honored that Xbox One is the first console approved for sale in China through the Shanghai Free Trade Zone," said Yusuf Mehdi, Microsoft's corporate vice president of marketing, devices and studios. "We're dedicated to earning millions of fans in China by working with BesTV to deliver an all-in-one games and entertainment experience starting Sept. 23."

While the Xbox One won't have to face off against Sony and Nintendo consoles in China, at least not yet, China's lucrative gaming market, which reported $14 billion in revenue in 2013, has been dominated by PC and mobile gaming -- China has firmly established itself as the world's largest mobile market.

To combat the rampant PC piracy and passionate mobile play, the Xbox has a solid lineup of approved games to deliver to the People's Republic of China -- the lift of the console ban won't change the status of the at least 40 games that have been outlawed in China, including the first-person shooter that has "evolved" into a "national security threat."

Some of the Xbox One exclusives available for the Sept 23. launch in China include Forza Motorsport 5: Racing Game of the Year, Kinect Sports Rivals, Powerstar Golf, Zoo Tycoon, and Max: The Curse of Brotherhood. Halo: The Master Chief Collection and vibrant post-apocalyptic shooter Sunset Overdrive were scheduled for release shortly after launch.

Robert Xiao, CEO of Beijing Perfect World, said his company was selected among the first batch of publishers to deliver Xbox One games to China. Perfect World has already been preparing games for the console.

"We have three studios working on Xbox games including Neverwinter Online, Celestial Sword and Project X and we can't wait for Xbox fans in China to get their hands on them," said Xiao. "For the six months following Xbox One's launch, Neverwinter Online will be available only on Xbox One. Offering unlimited possibilities to developers, the launch of Xbox One in China will help take the game development industry to a whole new level."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.