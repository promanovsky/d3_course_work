Total consumer credit outstanding expanded at an annual rate of 6.4% for February to hit $3.13 trillion, according to a Federal Reserve Consumer Credit report (link opens a PDF) released today.

After increasing at a seasonally adjusted annual rate of 5.3% for January, this month puts credit back on a path to a higher growth rate similar to December's 7%. In absolute terms, analysts had expected a smaller $14 billion expansion, but outstanding credit actually grew by $16.5 billion.

While overall numbers expanded, gains were far from equal across credit types. Revolving credit (no fixed number of payments, e.g., credit cards) shrank at an annual rate of 3.4% for February, the second month in the red following a -0.3% rate for January. This hints at continued hesitation on the part of American consumers to use credit cards.

Non-revolving credit (fixed installments, e.g., car payments) showed strong growth again, clocking in at a 10.1% rate after hitting 7.5% the month before. Non-revolving credit has expanded in part because of more car loans and student loan activity -- both helping to outweigh dips in revolving credit outstanding.

link