Forget bands and other gadgets. If you want to track your body's vitals while working out, just slip into one of these shirts.

These workout shirts, with some specially woven threads, are able to measure health stats like your heart rate, breathing rate and calories burned, reported wired.com.

The shirts have been developed by a start-up company called OMsignal.

"We need something that complements life, without getting in the way of life," OMsignal CEO Stephane Marceau was quoted as saying. "Clothing was, naturally, the most friction-less fit."

OMsignal's shirts, which have the style and texture of a high-quality, form-fitting workout shirt, feature a band around the chest woven with conductive silver-based thread.

This band can transfer electrical signals to a piece of hardware called a 'little black box' that snaps onto the shirt.

The box connects to your handset via Bluetooth and houses an accelerometer, gyrometer, and magnetometer for tracking your movements in space.

Because that band wraps fully around your chest, the device can also track your heart rate and the way your chest expands and contracts as you breathe.

This lets OMsignal measure additional stats that are impossible for other wearables.

Using the biological signals the shirt measures, it can even tell you how much energy you have left at the end of the day, which is useful for planning the next day's workout.