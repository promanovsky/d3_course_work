Scientists claim to have successfully achieved therapeutic cloning, which could help in the treatment of diseases and which could also be the first step for human cloning.

Cloning has been a topic of debate ever since the first mammal Dolly, a sheep, was cloned in 1996. Scientists have taken cloning a step further and for the first time they have replicated stem cells from the skin of an adult man. Even though cloning has been surrounded by ethical concerns, some scientists are of the opinion that therapeutic cloning can be helpful for the treatment of various diseases.

Robert Lanza, chief scientific officer at Advanced Cell Technology and co-author of a study that was published in Cell Stem Cell, explains that symptoms of many human diseases, such as Parkinson's and diabetes, increase as an affected patient gets older. The extraction of cells from the DNA of older people will enable scientists to find remedies for such diseases.

"The incidence of many diseases that could be treated with pluripotent cell derivatives increases with age, so applications based on autologous cells would most likely involve aged nuclear donors" states the study.

Lanza says that the researchers extracted nuclear DNA from the skin cells of a 35-year-old man, as well as from a 75-year-old man. The scientists then injected the extracted cells into human eggs, which were obtained from four women donors. The nuclear DNA of the women was removed from the eggs prior to the injection of the men's DNA.

The scientists inserted the male DNA into egg cells of 77 samples and created just two worthy cells that now had the DNA of one of the men. Lanza says that both the cells have the ability to divide indefinitely and also grow as many cells as they want.

The newly created cells, also called embryonic stem cells, look like the cells found in the human embryo and, if required, can be turned into any type of human cells, such as heart, liver and more, making them useful for the treatment of various diseases.

Lanza says the researchers will want to check the genes of millions of adults and then create a library of selected DNA donors. The process will enable the scientists to produce cell lines that are expected to be a close match for a major chunk of potential cell receivers.

The recent advancements in therapeutic cloning may be a big step in the medical area. However, the latest advancement also takes scientists a step closer to being able to clone an entire human being.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.