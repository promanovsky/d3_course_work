Combining advanced insulin pump technology with smartphone apps has produced what some refer to as a wearable "bionic pancreas," according to studies reported in the New England Journal of Medicine.

The results of the two clinical studies were also presented at a meeting of the American Diabetes Association in San Francisco. According to reports the smartphone apps use an algorithm to regulate the work of the insulin pump. This removes the guesswork and computations from the patient's responsibility, resulting in safer and improved control of blood sugar levels. The software was installed on an iPhone that used a Bluetooth connection to sync with insulin pumps and glucose monitors.

Edward Damiano, an associate professor of biomedical engineering at Boston University, along with his colleague, Dr. Steven Russell of Harvard Medical School, presented the findings. Damiano began work on the artificial pancreas after his son, David, was diagnosed with Type 1 Diabetes at the age of 11 months.

The bionic pancreas, say the two developers, is better at controlling blood sugar levels than current insulin pumps. The new bionic pancreas is capable of not just injecting or withholding insulin, it can also inject glucagon, which raises blood sugar. The software associated with the pump can track blood sugar levels on a steady basis and direct the pump to inject either substance instantly.

The bionic pancreas measures blood sugar every five minutes using a monitor attached to the body. The iPhone receives the results wirelessly and calculates the amount of insulin or glucagon to be injected by the bionic pancreas.

The ability of the bionic pancreas to provide the body with a sugar product to raise dangerously low blood sugar levels could be a key factor in preventing so-called "dead in bed" syndrome. People with Type 1 diabetes are prone to experiencing precipitously low blood sugar levels during sleep, with fatal effects occurring in some cases.

The researchers are kicking off a new study with 40 adults who will be testing the system for 11 days.

The widespread and approved use of the bionic pancreas - a system that involves pumps, monitors, embedded needles and a smartphone - is still a few years away. In addition to safety and efficacy trials, researchers need to condense some of the hardware into one manageable unit. It is hoped that a version that combines all three hardware components will be available in the fall.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.