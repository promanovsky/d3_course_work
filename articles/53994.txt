From the mouths of babes often come movie lines they parrot without understanding the adult-themed origins, such as Arnold Schwarzenegger’s catch phrase "I’ll be back" from "The Terminator," which is one of the most quoted lines in film history.

The former California governor and action hero, 66, told the media this week that he’s surprised to learn “I’ll be back” is one of the most quoted film lines in history, according to Metro.

Many parents like me already know it as one of many misunderstood and inappropriately quoted lines that come out of the mouths of kids who have never seen the films, but absorbed the quotable lines via social osmosis.

“I’ll be back,” was the line uttered menacingly by the virtually indestructible, emotionless, homicidal android in “The Terminator” movie, sent back in time to murder a woman.

The Terminator, played by Schwarzenegger, says the line to a police front desk clerk before breaking into the station.

Today my son Quin, 10, who has never seen “The Terminator” growls the line, a la Terminator, to his best pals after a fun day because he thinks it’s a “Mr. Cool” way to say, “See you later for a play date.”

Because Mr. Schwarzenegger is out and about promoting his new film, “Sabotage,” the line is all over the media again and Quin has now donned sunglasses to add to the effect after seeing a clip of the film.

Schwarzenegger appeared on “The Tonight Show with Jimmy Fallon” and “WWE Raw” on Monday, reminding us that the movie star and his verbal legacy will be back again and again for as long as it takes to promote his latest film.

The shock value of hearing a young child utter something very adult creates an instant laugh.

It reminds me of how a cussing toddler in the news months ago illustrated the viral popularity, and sometimes problems, caused by kids parroting what they hear from adults.

As a teen growing up in New Jersey – where those who want to set foot on a beach must wear a badge proving they have paid a local fee for the privilege – I recall telling the beach police, in the worst possible Mexican accent, “Badges? We don’t have no badges. We don’t need no badges. I don’t have to show you any stinking badges!”

To this day I have never seen “The Treasure of Sierra Madre” from whence that line comes. I don’t even know how I know the line uttered by Gold Hat as he reacts to Dobbs prompting him to prove they are policemen.

I had to look up the reference on YouTube to write this piece.

Movie phrase misuse runs in the family, because over the past 20 years parenting four sons I have a vast collection of misspoken, misunderstood catch phrase memorabilia of which the “Terminator” is only a small sampling.

Cooking breakfast one morning, when my son Ian, now 18, was just about three-years-old, he toddled up to the griddle, inhaled deeply and announced, “I love the smell of Napalm in the morning!”

My friend who was sharing breakfast nearly died laughing at the reference to “Apocalypse Now” and the idea that perhaps my toddler thought my cooking was toxic.

Ian had no Idea what Napalm was. He’d heard it said by one of the workers in the boatyard where we lived aboard our trimaran, a 37-foot multihulled sailboat, in Goodland, Fl.

Then there was the time my eldest son Zoltan, now 20, was age three and discovered the joys of being a boy.

He strutted out of the tub on the boat deck, same boatyard in Florida, shouting, “Say ‘hello’ to my little friend!”

That was when I had to ask the boatyard workers to please stop spewing movie lines in front of my boys because my little “Scarface” was running around naked slaying the world with laughter.

The problem is that the more adults laugh at what kids say, the more they say it.

Now that we live in a house on land in Norfolk, Va., with TV and internet access, the catch phrases fly thick and fast.

I am not the only one who has fallen victim to a movie catch phrase attack by my kids.

In church when Avery, now 15, was about five he met the priest and while shaking his hand soberly quoted “The Blues Brothers” character Elwood Blues saying, “We’re on a mission from God.”

At the free chess program I run at a local community center, I have a front row seat for movie line madness as kids – mostly under age 10 – say the darndest things.

“THIS IS SPARTA!” an adorable little girl, 6, in a pink frilly dress crowed as she took her opponent’s queen.

Her father smiled guiltily while trying not to burst with laughter.

I tried not to laugh too, while explaining that, while chess may have originated in Persia in 500 A.D. I would prefer if we don’t quote the film “300” and reference King Leonidas saying that line to a Persian messenger while kicking him into the “Pit of Death.”

While writing this story, I realized I have my own phrase that my sons now use upon exiting a location, “It’s time to get out of Dodge.”

While I typically use it to mean it’s time to go, I just looked up the phrase and found it comes from the TV show “Gunsmoke.” I never watched “Gunsmoke" in my life. This one again proves there is verbal osmosis directly from pop culture to children.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

In the show, the marshal looks steely-eyed into the eyes of the leader of a pack of murderous cowboys and says, “You take the rest of your men and you get out of Dodge.”

It seems that parents might want to take a moment themselves to research and review the phrases we and our children habitually use, and perhaps kick a few of them out of Dodge in the process.