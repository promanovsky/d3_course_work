A study of white married couples in the U.S. -- the majority of who were born in the 1930s -- concluded that spouses are more genetically similar to each other than they are to random individuals.

In a paper published Monday in the journal PNAS, a team of social and behavior scientists investigated the statistical likelihood that people will marry someone with a similar genotype.

“It is well established that individuals are more similar to their spouses than other individuals on important traits, such as education level,” wrote lead study author Benjamin Domingue, a behavioral science researcher at the University of Colorado, Boulder, and his colleagues.

“The genetic similarity, or lack thereof, between spouses is less well understood,” authors wrote.

Advertisement

Study authors based their conclusion on data from 9,429 non-Hispanic white individuals in the ongoing Health and Retirement Study, which is sponsored by the National Institute on Aging.

The sample included 825 spousal pairs who were all born between 1920 and 1970. Fifty-nine percent were born during the 1930s, authors wrote.

Research also included the comparison of 1.7 million single nucleotide polymorphisms -- the point at which a sequence of DNA differs between individuals.

Study authors said that while they did find that married couples were more genetically similar than randomly generated pairs of people in the same population, this similarity was just one-third the magnitude of educational similarity between spouses.

Advertisement

Study authors noted that their research was limited by the fact it focused on opposite-sex, non-Hispanic white couples within the United States, and said they encouraged further research.

“The results represented here only represent a first step in understanding the ways in which humans may assortively mate with respect to their genome,” authors wrote.