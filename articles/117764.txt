Parenting website Mumsnet has fallen victim to hackers exploiting the Heartbleed bug, making it the second high-profile organisation to be compromised by the OpenSSL security flaw.

With over 1.5 million registered members, Mumsnet has warned that potentially all accounts have been affected.

In an email to its members, the London-based website wrote: "We have no way of knowing which Mumsnetters were affected by this. The worst case scenario is that the data of every Mumsnet user account was accessed."

The breach was discovered by the founder of Mumsnet, Justine Roberts, who noticed that someone was posting on the website using her account credentials.

"We worked it out when the hacker, we guess, posted on Mumsnet using my username," said Roberts.

"Subsequently [the hacker] posted that they had accessed Mumsnet data via the Heartbleed bug.

"I don't think this has necessarily been done maliciously, I think it's an attempt to warn us that there was a breach in our security and that we needed to react quite quickly, which we've done."

Theoretical threat now 'very real'

On Monday it was confirmed by the Canadian Revenue Agency (CRA) that the social insurance numbers of 900 Canadian taxpayers had been stolen as part of a cyber attack stemming from the Heartbleed bug.

Security experts have warned that a system infiltrated using the Heartbleed bug is often difficult to detect and suggest that more Heartbleed cases could be on their way.

"With the latest news, the Heartbleed vulnerability went from being theoretical to very real," said Fred Kost, vice president of security solutions at Ixia.

"In order to protect themselves from becoming the next victim, enterprises should first deploy the patch and then begin changing their private key to help protect against man-in-the middle attacks that might use the stolen private key.

"Although this can be a complex process and will take organisations a while to complete, not as simple as just applying the patch, organisations can move in the right direction by taking action now."