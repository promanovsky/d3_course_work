tech2 News Staff

Amazon just unveiled Fire, their first ever smartphone which will be sold exclusively via AT&T with a two-year contract. Pre-orders begin today and customers can either pay $199 for the 32GB version or $299 for the 64GB version. The phone ships on July 25. Amazon is also throwing in 12 months of Amazon Prime service free with Fire.

The Amazon Fire sets its self apart with a couple of defining features which Dynamic Perspective and Firefly. Dynamic Perspective uses a new sensor system to respond to the way you hold, view, and move Fire, enabling experiences not possible on other smartphone. The fours sensors on each corner of the phone are also accompanied by infrared LEDs for the depth perception to work even in pitch darkness. Firefly quickly recognizes things in the real world—web and email addresses, phone numbers, QR and bar codes, movies, music, and millions of products, and lets you take action in seconds with the dedicated Firefly button.

Fire features aluminium buttons and injected moulded steel connectors for a premium look and feel. You also get a 4.7-inch HD IPS display with Corning Gorilla Glass 3 and a rubber frame for protection. The display boasts of 590nots of brightness, making it readable even in hard lighting. You also get global LTE coverage, Wi-Fi ‘ac’ with channel bonding, NFC and Bluetooth.

It’s powered by a Qualcomm Snapdragon 800 chipset running at 2.2 GHz. There’s 2GB of RAM and internal storage options of 32GB or 64GB. There’s no mention of a microSD card slot however. The Fire also gets a 13MP rear camera with a f/2.0 lens and Optical Image Stabilisation. There’s a dedicated camera button too and pictures captured can be saved to Amazon’s Cloud Drive, which offers unlimited storage.

Multimedia is taken care of by dual stereo speakers placed on either side (horizontally) along with the Dolby Digital enhancements. The Fire also comes bundled with flat-cable earphones with magnetic earbuds for easy storage.

Fire deeply integrates Amazon exclusive services like X-Ray and Second Screen. ASAP (Advanced Streaming and Prediction) predicts which movies and TV episodes you’ll want to watch and prepares them for instant playback before you even hit play.

Mayday is now available over 3G and 4G, in addition to Wi-Fi. Simply hit the Mayday button in quick actions and an Amazon expert will appear via live video to co-pilot you through any feature on the device. Amazon experts are able to draw on the screen, talk you through how to do a task, or do it for you. This feature is available 24x7, 365 days a year, and it’s free.

Amazon will also release the SDK for Dynamic Perspective and Firefly so apps like MyFitnessPal can give you nutrition values when you point the camera to any food objects.