BILL Gates still rules technology’s rich list but the table is propped up by some that are young enough to be his son and should still be in school.

Here are some baby-faced multi-millionaires — or billionaires in the case of Facebook CEO Mark Zuckerberg:

Palmer Luckey, 21, is the newest multi-millionaire on the tech scene after his startup, Oculus VR, which develops the virtual reality headset Oculus Rift, was bought out by Facebook for $2 billion. Like another famous face on the rich list he also strides about in thongs and dropped out of school before he could get a degree. While he was working on Oculus he was living in a trailer parked on his parents’ driveway. Now his goggles have transported him to a reality that he could only once dream of.

David Karp is the 27-year-old founder of blogging platform Tumblr, which Yahoo! bought for $1.1 billion (A$1.18 billion) last year. Karp, who lives in New York City and was born on the Upper West Side, is worth an estimated $200 million (A$215 million), according to Forbes. Like Luckey, Karp was home-schooled and never graduated college.

Andrew Mason, the 33-year-old founder and former CEO of Groupon, saw his net worth soar to over $1 billion (A$1.07 billion) when his digital coupon company went public in 2011. He was just 30 years old. Groupon’s stock has since lost more than two-thirds of its value and Mason stepped down as CEO last year. Still, he’s worth an estimated $200 million (A$215 million). Mason graduated college, but he dropped out of the prestigious University of Chicago one month into his graduate program.

Mark Zuckerberg is the 29-year-old Facebook founder and CEO — and the 21st richest person in the world with an estimated net worth of $28.5 billion (A$30.6 billion). He dropped out of Harvard to work on Facebook, which was valued at more than $100 billion (A$107 million) when it went public last year. Zuckerberg’s early days with Facebook have already been well chronicled in the award-winning movie “The Social Network.” Of course, it wasn’t all roses. The hoodie-wearing CEO had a bitter legal battle over claims he stole the idea for Facebook from some of his Harvard colleagues.

Blake Ross, the 28-year-old co-founder of web browser Firefox, is worth $150 million (A$160 million), according to Forbes. Born in Miami, Florida, Ross started working for Netscape Communications, one of the earliest web browsers, when he was just 14 years old. In 2007, Ross sold Parakey, a computer-based web interface, to Facebook for an undisclosed sum. Stanford University now considers Ross one of its prestigious alumnus, but he was in and out of college for over a decade.

This article originally appeared on the NY Post.