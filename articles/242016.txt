Microsoft this week revealed that it successfully pushed back on an FBI National Security Letter (NSL) that requested information about one of its enterprise customers.

The incident occurred last year, but Microsoft was not allowed to discuss it until Thursday, when a Seattle court unsealed documents related to the case. "Given the strong ongoing worldwide interest in these issues, we wanted to provide some additional context on the matter," Microsoft general counsel Brad Smith said in a blog post.

At issue was an FBI letter that "sought information about an account belonging to one of our enterprise customers." Microsoft did not reveal which customer was covered in the letter, but said it "sought only basic subscriber information."

The nature of an FBI NSL is such that companies who receive them are not at liberty to discuss them. In this case, Smith said, "we concluded that the nondisclosure provision was unlawful and violated our Constitutional right to free expression. It did so by hindering our practice of notifying enterprise customers when we receive legal orders related to their data."

As a result, Microsoft challenged it in court, prompting the FBI to withdraw its letter.

The move comes about six months after Redmond pledged to notify businesses or government customers if the feds were asking for their data, and challenge any non-disclosure agreements in court.

"We've done this successfully in the past, and we will continue to do so in the future to preserve our ability to alert customers when governments seek to obtain their data," Smith said this week.

"Fortunately, government requests for customer data belonging to enterprise customers are extremely rare," according to Smith. Ideally, the FBI will send its requests to the Microsoft customer directly or Microsoft will secure permission from the firm to release the information.

FBI NSLs made headlines in March 2013 when Google secured permission to break out vague data about them in its transparency reports. That was before the Edward Snowden NSA documents were released, however, an incident that prompted top tech firms like Google and Microsoft to request permission to be more open about the data they hand over to the feds. After much back and forth, the Department of Justice announced a deal in January that provides tech firms with more transparency options.

Further Reading

Security Reviews