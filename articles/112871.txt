Reuters

For one day only, Google will put its futurewear optics on sale to the general public. On Tuesday, April 15 at 9am ET , Google will be "opening up some spots in the Glass Explorer Program,” allowing any U.S. resident to buy the before-its-time wearable computer known as Google Glass

At $1500, the device’s early adopter tax remains as steep as ever, although this time around, Google will toss in a pair of its handsome new prescription-compatible frames or the original shades that shipped with the very first Glass kits a year ago (much to the chagrin of previous Glass buyers who only scored the awkward shades).

After sating the collective appetite of many early adopters, it’s not clear who out there still hasn’t had a shot at buying Glass. But for anyone still “waitlisted” the opportunity sounds like a direct ticket to Glasstown. Here’s Google’s logic behind the one-day sale, as explained on its Glass Google+ account:

Our Explorers are moms, bakers, surgeons, rockers, and each new Explorer has brought a new perspective that is making Glass better. But every day we get requests from those of you who haven’t found a way into the program yet, and we want your feedback too. So in typical Explorer Program fashion, we’re trying something new.

Unfortunately, beyond the sky-high price, one big caveat remains: Glass is open to U.S. residents with a U.S. shipping address only.

Who Should Buy Google Glass

Public perception of Glass vacillates between intense curiosity and something resembling a cartoonish eye-roll, but I stand by the fact that Glass is a remarkably cool technology. The device remains cost prohibitive to a large swath of the population, but if you, your organization or your company can stomach the price, there are still valid reasons to consider signing up come Tuesday.

1. Developers

Ostensibly, the majority of folks who’ve signed up to date are building something cool with Glass. Due to limitations of the device, its adopters or perhaps our collective imaginations, there’s still plenty of room for innovation. For every mind-blowing Glass app out there, there are twenty that do something incredibly boring.

Like virtual reality, augmented reality directly alters the way we interact with the world around us—and that’s really, really cool. If Google can lower the barriers to entry, getting Glass into the hands of a more diverse sample population geographically and socioeconomically speaking, I suspect that innovation would soar. In the meantime, Glass needs more people who can build things outside the box.

2. Scientists

There are so many incredible applications for the sciences that I don’t even know where to start. Medicine, astrophysics, geology, botany, science education—there’s a lot of possibility here.

Even on a more meta level, we don’t yet know how augmented reality changes the way we think, both from a social perspective and a neural imaging one. Researchers, we need you!

3. Photographers/Artists

Silicon Valley can only take us so far. Its portability, low profile (well, in some situations anyway) and hands-free operation is transforming for experimental photographers. You really can interact with people and environments in a totally different way by capturing moments with Glass. And while photographers are already a thriving Glass-wearing tribe, interactive, experimental artists should consider turning the wearable into a sociotechnological medium.

4. Real Explorers

Google throws this term around about all Glass owners, but we need more real explorers. You know, the people who trek to the world’s most fascinating, extreme and remote locales. Hey, even I went on a hike or two. Explorers of the world: Get a Patagonia sponsorship and go forth!

Google Glass Remains An Expensive Habit

Google first made Glass available only to attendees of its 2012 Google I/O developers conference (including press, like us), where the device was first revealed. After a long wait, Google then opened Glass sales to successive waves of so-called “Explorers” through a hashtag contest known as #ifihadglass before allowing each existing Explorer to dole out three “invites” each. Roughly a month later, Google quietly slipped a Glass waitlist onto its website, though it’s unclear how many of those orders have been filled.

After all of those rounds of availability, it’s hard to imagine who out there still hasn’t had a chance to buy Glass. Still, encounters with Google’s augmented reality visor remain rare outside of San Francisco, the device’s indigenous habitat.

We look forward to seeing what happens when (and if) Glass takes its Silicon Valley blinders off.