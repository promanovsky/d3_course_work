Mt Gox says it found 200,000 "forgotten" bitcoins on March 7, a week after the Tokyo-based digital currency exchange filed for bankruptcy protection, saying it lost nearly all the 850,000 bitcoins it held, worth some $US500 million at today's prices.

Mt Gox made the announcement on its website. Online sleuths had noticed around 200,000 bitcoins moving through the crypto-currency exchange after the bankruptcy filing.

"Forgotten": 200,000 bitcoins have been found.

The exchange, headed by 28-year-old Frenchman Mark Karpeles, said the bitcoins were found in an old-format online wallet which it had thought no longer held any bitcoins, but which it checked again after its bankruptcy filing.

"On March 7, 2014, Mt Gox Co, Ltd confirmed that an old format wallet which was used prior to June 2011 held a balance of approximately 200,000 BTC," the statement said.