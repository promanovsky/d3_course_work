If Facebook had a “dislike” button, it would be getting a lot of clicks right now over its $2 billion deal to buy virtual-reality gaming start-up Oculus VR.

And Markus Persson, the owner of Mojang, which produces Minecraft, the popular building game, might have broken his mouse clicking to voice his displeasure with the deal.

“Facebook creeps me out,” the Swedish developer tweeted. Persson said he had been working on bringing a version of Minecraft to Oculus — but no longer.

“I just cancelled that deal,” he said.

Other Oculus fans also blasted the deal, saying they don’t want games with ads, which is how Facebook makes its money.

Palmer Luckey, Oculus’ 21-year-old founder, sought to calm dissent over the deal in a series of Web posts Wednesday.

“This deal will definitely make things better,” he said. “This deal specifically lets us greatly lower the price of the Rift [virtual reality goggles],” he said. It also will let Oculus “afford to hire everyone we need.”

“Facebook has a good track record for letting companies operate independently post-acquisition and they are going to do the same for us,” he said in the posts. “Trust me on this, I would not have done the deal otherwise.”

Geeky gamers weren’t the only ones looking down on the merger. Facebook’s investors sent the social networking company’s stock down 6.9 percent, to $60.38, on the news amid questions about whether Oculus was worth the money.

Oculus’ gaming goggles are still in the development stage. Luckey has said that there’s still room to go before virtual reality replicates real life. Until then, wearing gaming goggles can make some people nauseous when the picture doesn’t catch up as quickly as their eye movements.

Mark Zuckerberg is betting that virtual reality is the next big computing platform and that Oculus is the clear leader in the space.

The Facebook CEO said he wants Oculus not for the gaming, but for its potential beyond games, including virtual shopping and virtual basketball games.