TOKYO -- Asia's robust economy and strong demand for drugs drove global production and trafficking of methamphetamine to record levels last year, with Japan one of the most lucrative markets, the United Nations said Tuesday.

Asia is the world's largest market for stimulants and seizures of methamphetamine pills and crystal meth have tripled to at least 36 tons over the last five years, the UN Office on Drugs and Crime said in a report released in Tokyo.

The trends reflect Asia's growing economic power and demand for new synthetic drugs. China and India, which have large chemical industries, are the region's major production hubs of methamphetamine, the addictive stimulant that affects the central nervous system. The report said organized crime groups, including those in Japan, help traffic meth from Mexico, the Middle East and the rest of Asia.

The regional integration of economies, trade and transportation help spread the drug faster, said Jeremy Douglas, an UNODC regional representative for Southeast Asia and the Pacific, told a news conference. Methamphetamine, originally a drug taken by poor people and workers, migrated into youth culture over a decade ago and more recently into the more prosperous part of the society. "So you have a natural growth through the market," he said.

Japan is seen as as one of the most lucrative markets. The amount of amphetamines seized in the country last year tripled from a year earlier, mostly due to large-volume smuggling from abroad.

Of about 12,000 drug-related arrest in 2013, about half involved yakuza groups, according to police statistics.

"Japan's methamphetamine problem can be attributed to extremely high street price. We are concerned that this could motivate criminal organizations to smuggle more drugs into Japan," said Yoshiya Takesako, a National Police Agency director of international organized drugs and firearms investigation.

An average street price per gram would be $700 here, nearly twice that in the U.S., Takesako said, citing the UN report.

Traffickers know where personal incomes are rising and quickly target the demographic, Douglas said. Dumping crystal meth into the market at artificially cheap prices allowed traffickers to create demand before they moved in large volumes at higher prices.

"Asia, because of its rising incomes, has been targeted by other regions of the world. Because meth is more expensive here than it is in the U.S., so you see people trafficking it here to make more money," he said.