Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

High street fashion chain Zara has hastily pulled tots’ pyjama tops from UK shelves following a backlash saying they looked like the uniform Jews were forced to wear in Nazi concentration camps.

Zara apologised following a storm of protest that children’s nightwear with a yellow star on a striped background was hauntingly similar to Holocaust clothing worn by the Jews with a yellow Star of David on the left.

Today Twitter was awash with furious posts slamming the Spanish owned retailer for its “insensitivity” over the design aimed at babies aged three months to toddlers aged three.

Seething Twitter user @birMisafir wrote: “Hey @ZARA what’s wrong with you? You have no idea about history, right?!”

And @n_rothschild said: “What were the designers thinking?” while Shoshana Cohen posted: “Really @zara? Is that the new holocaust chic?”

The pyjama top with a six point yellow star on a navy and white striped background bears an uncanny resemblance to the concentration camp tops worn by Jews in the Second World war.

(Image: Zara)

While the word “Sheriff” runs across the badge, the faint lettering is difficult to pick out and the design sparked outrage across the world.

Writer @EylonALevy said: “What designer at #Zara thought this screams “sheriff” rather than “Auschwitz inmate?”

And Israeli newspaper Haaretz called it “hauntingly reminiscent of a darker era”.

On its website, it wrote: “The shirt bears a large six-pointed star on the upper-left section, in the exact place where Nazis forced Jews to wear the Star of David.”

Zara, owned by Spain’s Inditex, said the design had been inspired by Western movies.

In a statement it said: “The item in question has now been removed from all Zara stores and Zara.com . The garment was inspired by the classic Western films, but we now recognize that the design could be seen as insensitive and apologise sincerely for any offence caused to our customers.”

The tops were pulled from the UK website as well as international outlets including stores in Israel, France, Denmark, Albania and Sweden.

It’s not the first time Zara has fallen foul of good taste for in 2007 it was forced to withdrew a handbag from stores after complaints the design featured swastikas.