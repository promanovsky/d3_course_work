Celebrity

Walter, who wrote more than 100 books for children and young adults, died on Tuesday in his house in New Jersey.

Jul 3, 2014

AceShowbiz - Walter Dean Myers, an author who produced more than 100 books for children and teenagers during his 45-year career, died on July 1, following a brief illness. He was 76. His publisher announced his death on Wednesday, July 2.

President and Publisher of HarperCollins Children's Books, Susan Katz, said in a statement, "We are deeply saddened by the passing of erudite and beloved author Walter Dean Myers. Walter's many award-winning books do not shy away from the sometimes gritty truth of growing up."

"He wrote books for the reader he once was, books he wanted to read when he was a teen," she continued, "He wrote with heart and he spoke to teens in a language they understood. For these reasons, and more, his work will live on for a long, long time."

Another publisher, Richard Robinson of Scholastic, said that Walter "changed the face of children's literature by representing the diversity of the children of our nation in his award-winning books. He was a deeply authentic person and writer who urged other authors, editors and publishers not only to make sure every child could find him or herself in a book, but also to tell compelling and challenging stories."

Richard added that he would always remember Walter's appearance at a convention when he spoke about his book "Malcolm X: By Any Means Necessary". The publisher recalled, "As we waited for the booksellers to arrive, more than 100 hotel staff crowded into the dining room, drawn to this tall, dignified author they deeply admired."

Author of the best-selling novel "The Fault in Our Stars", John Green, posted on Wednesday a series of tweets regarding Walter's death. "Just devastated to hear that Walter Dean Myers has died. Don't even know what to say. He was so kind to me when I was first starting out," he wrote.

"Myers inspired generations of readers, including a 12-year-old me when I read FALLEN ANGELS, and then a 22-year-old me when I read MONSTER," he added, "Walter Dean Myers was the first winner of the Printz Award, and won the CSK five times. It's hard to imagine YA literature without him."

Walter became the National Ambassador for Young People's Literature from 2012-2013. He promoted the slogan "Reading is not optional" around the United States. He continued to spread the message, which said that reading proficiency and widespread literacy was the key for a brighter future, even after his years of becoming the ambassador ended. Through his books, Walter taught children and teenagers never to give up on life.

Walter published his first book "Where Does the Day Go?" in 1968. The book won an award from the Council on Interracial Books for Children. In 1994, the author, who was born Walter Milton Myers on August 12, 1937, in West Virginia, had his four books win ALA Margaret A. Edwards Awards, including 1983's "Hoops", 1985's "Motown and Didi", 1988's "Fallen Angels" and "Scorpions".

Walter is survived by his wife and two sons, including author and illustrator Christopher Myers. Walter's forthcoming books are going to be released soon. One of his books, "Juba!", will be released in April 2015. It is a teen novel which is written based on a young African-American dancer's life.