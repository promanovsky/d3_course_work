Google's digital assistant, Google Now, will soon be available in Chromebooks.

Plus, Google will soon be delivering an app that will allow you to watch movies and TV offline on your Chromebook, via Google Play.

Advertisement

Google says offline movies will be available in the next few weeks.One of the biggest complaints about Google's low-cost laptops has been that most of the apps require that you are connected to the Internet. Google is working on changing that. It is already possible to work on documents offline. Once you can watch movies offline, the device becomes a real alternative to a Windows PC or tablet, especially when traveling.

If you get on an airplane you can "choose to be productive with Google Docs or be unproductive with your TV shows," joked Google Chrome OS vice president Ceaser Sengupta at a press conference on Tuesday.

Advertisement

Between these two new features, and a new crop of touch devices coming this summer, Chromebooks will soon be taking on both Windows 8 PCs and the iPad.