How will Stephen Colbert stack up against the man he’s succeeding at the “Late Show,” David Letterman?

Turns out, that’s not the burning question these days, at least among taxpayers. What New Yorkers want to know is this: Will Colbert and the “Late Show” get the same tax goodies to stay in New York that went to “The Tonight Show with Jimmy Fallon”?

CBS chief Leslie Moonves sure seems to be fishing, coyly noting CBS is “still determining the locale” and that he’s hearing from mayors “in a lot of different places.”

Those pitching for a move include the mayor of Los Angeles, Eric Garcetti, and two state lawmakers who suggest they’re willing to amend California law to let an LA-based “Late Show” get a piece of the tax-subsidy pie. Meanwhile, both Gov. Cuomo and Mayor de Blasio have said they want the show to stay in New York.

We’d like to see the “Late Show” stay in Gotham, too. Then again, we’d like to see the return of all those businesses driven out of this state by what the Tax Foundation says is the worst business-tax climate in the nation. And we’d all be better off if New York pols fixed this instead of looking to carve out yet more corporate welfare for a rich and glamorous industry.