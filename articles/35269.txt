If imitation is the sincerest form of flattery, the U.S. Navy’s fleet of 10 aircraft carriers should be feeling pretty smug right about now. Why?

First, the U.S. Marines are busy acquiring a three-flattop fleet known as the America class. The first of this new class of amphibious assault ships, the $3.4 billion USS America, is slated to join the fleet later this year.

Second, the Chinese painted a carrier-sized target deep in the Gobi desert about a year ago before attacking it with their new ship-killing missiles.

Finally, the Iranians are busy constructing a crude barge two-thirds the size of the 1,092-foot long USS Nimitz, the world’s biggest warship. According to the Navy, it consists of little more than “pontoons with steel construction to replicate hull, flight deck and superstructure.” Think of it as a parade float, of absolutely no military utility.

What’s going on here?

The first ship of the Marines' new class of mini-carriers, the USS America, in sea trials in November. Navy photo

Well, there’s no better way to grab the attention of U.S. intelligence than pretending to build or destroy aircraft carriers, which have served as the heart of the U.S. Navy’s fleet for 70 years.

The U.S. Marines have always been leery of relying on the U.S. Navy for air support. That’s why they have their own Gator navy to ferry their airplanes to battle (these ships are owned and operated by the Navy, but dedicated to the corps’ use).

The Marines apparently are a little late to the Defense Department’s push for joint operations, where each service brings its own skills to the fight. But it makes sense, in a perverse Pentagon kind of way: seeing as the Marines are basically a second land army, why shouldn’t they have their own naval fleet and air force, as well?

China has tested a ship-killing missile against a carrier-sized target in the Gobi desert. WCT

China, following in the footsteps of its regional rival, Japan, has never cared for U.S. aircraft carriers patrolling off its coast. That explains its effort to develop weapons that can put such American warships at risk, without having to build a costly blue-water navy of its own.

So those two carriers make some kind of sense. But the Iranian example, first reported in the New York Times, doesn’t. The mock carrier is under construction in Bandar Abbas, an Iranian port city on the Strait of Hormuz. “We do not assess that Iran has the capability to build an aircraft carrier,” says Navy Commander Jason Salata of the U.S. Navy’s 5th Fleet in Bahrain. “We do not assess that this particular barge would impact our operations here.”

Iran has pulled such stunts before. Last year, the aviation world hooted and hollered when it rolled out the F-313, Iran’s first homemade “stealth” fighter.

What’s the carrier actually for? Iranian media reported Sunday that the fake carrier will star in a movie about the 1988 shootdown of an Iranian Airbus airliner by the U.S. Navy, which killed all 290 aboard.

The official Navy probe into the tragedy didn’t mention any involvement by a carrier. In fact, the warship that fired the missile that downed the airliner was the USS Vincennes, a cruiser. So constructing a mock carrier for such a movie doesn’t make sense.

But, given that one of the actors involved in the production of Airbus is Sean Ali Stone, son of U.S. filmmaker Oliver Stone, perhaps it does.

Get our Politics Newsletter. The headlines out of Washington never seem to slow. Subscribe to The D.C. Brief to make sense of what matters most. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.