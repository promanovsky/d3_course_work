After Paul Walker’s death, the Fast and Furious franchise put the production of their 7th film on hold until they can figure out how to finish the film without Paul Walker. Today, Fast and Furious 7 is back in the headlines because the studio has announced production will resume on March 31st. The film was originally set to be released July 11, 2014, but of course its being pushed back to April 10th 2015, due to the tragic accident that took the life of one of the main characters.

E! News confirmed that shooting will begin March 31st in Atlanta, which is where they first started shooting last September. Director James Wan still hasn’t announced how they plan to retire Brian O’ Connor from the Fast and Furious franchise, but back in January, the The Hollywood Reporter revealed that they will still use the existing footage that they have of Paul Walker.

Regarding the announcement that production will resume, Vin Diesel released a statement on social media:

“The Transition into that Dom state of mind has always been an interesting one….only this time there is added purpose, a collective goal to make this the best one in the series, P.s The long awaited completion of Seven, begins…..”

Michelle Rodriguez in a Q&A with fans had this to say:

“The chemistry of the cast built the loyal fan base, and the fact that these characters live by their own code and answer one but each other creates a hero like admiration for a multicultural audience that usually has no heroes in Hollywood to relate to. The fast franchise is an underdog Giant in Hollywood and I hope it paves a road for many Multicultural big budget quality productions in the future.”

The Fast and Furious movies are loved by fans because of its action packed and intensely thrilling scenes. Unfortunately, the franchise suffered a huge loss without Paul Walker.

Paul Walker died at the age of 40 in a fatal car crash in Santa Clarita, California on November 30, 2013. Making matters even more tragic was the fact that Walker wasn’t alone in the vehicle; his best friend and business partner also died in the accident. Walker was reportedly driving his new red Porsche at 100 miles per hour when he lost control and crashed.

It will be interesting to see how the Fast and Furious franchise survives without the charm and grace of Paul Walker, but his final performance as Brian O’ Connor will definitely motivate people to go see the final chapter of Fast and Furious.

[Image Via Universal]