The deadly Ebola outbreak in West Africa continues to be a challenge for medical experts as they attempt to contain the killer virus which causes internal and external hemorrhage.

Even though the death toll reached 467 as of June 30, health experts are optimistic that the epidemic can be contained in Africa and will not rear its ugly head in other nations.

"The scale of the current Ebola epidemic is unprecedented in terms of geographical distribution, people infected and deaths," says Doctors without Borders, the organization which is treating those infected in West Africa.

Per the World Health Organization (WHO) this is the first time since 1976 when Ebola has affected urban areas as well. The down side to this is that an infected individual may travel to other parts of the globe.

Doctors without Borders have previously warned that Ebola may spread beyond the current 60 locations where patients have been diagnosed with the virus. The outbreak was identified in March this year and people in Liberia, Guinea and Sierra Leone have been affected.

However, per microbiologist Tom Geisbert, the chances of an individual who hails from an affected area and coming to Europe and North America and causing a major Ebola outbreak are slim. Geisbert is quite an authority on Ebola and has written, as well as co-authored nearly 100 studies on the epidemic and its related viruses.

Canadian Dr. Tim Jagatic echoes the sentiment and gives a pragmatic view that in the event an infected individual travels to Canada and upon arrival becomes ill and then contagious, then it is likely that the risk would be contained speedily. The health infrastructure in Canada he feels is equipped to handle the situation and prevent contamination.

In 2009, a study conducted by the European Centre for Disease Prevention and Control did not find any proof of Ebola transmission on an aircraft. The study cites the case of a passenger who was traveling from Gabon for treatment to South Africa and was diagnosed with Ebola later on. While the passenger in question had Ebola symptoms, there is no proof that the infectious disease spread to any other passenger who was aboard the same flight or in South Africa either.

Per health experts, a major hurdle in combating Ebola is traditional practices of mourning (like touching the body of the deceased at the funeral) or burial rites as these can lead to the spread of the virus.

The UN anticipates that the Ebola outbreak, being dubbed the worst in history, will continue in West Africa for several months.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.