Zee Media Bureau

Moscow: As the US and the EU slapped more sanctions on Russia over the escalating crisis in eastern Ukraine, Russian Defence Minister reiterated Moscow`s denial about presence of Russian armed groups in Ukraine.

A statement released by Russian Defence Ministry said that in a phone call with his US counterpart Chuck Hagel, Russian Defence Minister Sergei Shoigu talked about the recent Ukraine crisis and denied that Russian sabotage and reconnaissance groups were fuelling destabilising the situation in south-eastern regions of Ukraine.

Responding to Hagel`s comments on continued build-up of Russian troops along the border with Ukraine and "destabilizing exercises", Mr Shoigu told the US to “tone down the rhetoric”, added the statement.

In the same phone call, Russian Defence Minister also agreed to continue efforts to de-escalate the crisis, in favor of holding constructive dialogue and looking for compromises in line with the April 17 Geneva agreements.

Russian growing unease over increased US troops` activities in Poland and Vbaltic states became evident when Russian Defence Minister mentioned “the problematic situations stemming both the American Armed Forces` activities in the vicinity of the Russian borders and the overall increase in NATO`s military activity in Eastern Europe”.

The statement by Russia comes at a time when the European Union has released the names of 15 new targets of sanctions because of their roles in the Ukraine crisis.

The list released Tuesday includes Gen. Valery Gerasimov, chief of the Russian General Staff, and Lt. Gen. Igor Sergun, identified as head of GRU, the Russian military intelligence agency.

Russian Deputy Prime Minister Dmitry Kozak and pro-Russian separatist leaders in Crimea and the eastern Ukrainian cities of Lugansk and Donetsk were also on the list.

The EU move comes after the US decided to broaden its own sanctions to include seven Russian government officials and 17 companies with links to President Vladimir Putin.

With Agency Inputs