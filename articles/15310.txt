Is Wayne Knight dead? Seinfield‘s Newman has suffered from a not-so-funny death hoax that claimed Wayne Knight’s death was caused by a tractor trailer accident.

In a related report by The Inquisitr, a Chumlee death hoax claimed the Pawn Stars star was dead. Celebrity death hoaxes have been quite vicious these past couple years and it’s gotten to the point that some don’t even believe initial reports of a death. For example, when WWE wrestler Mae Young died some believed it was a death hoax at first (and for good reason… her passing was announced prematurely earlier that week). Some deaths are outrageous, like with the Wayne Knight death hoax, but others are more believable and based upon known facts. For example, Lil Wayne’s death hoax was one of the biggest for 2013 but the rumor based itself on a real life health condition the rapper suffers from.

Part of the reason people believed Newman was dead was because the hoax started on a website called TMZ.today that mimicked the appearance of the real TMZ. There’s also similar Twitter accounts that pass around fake news while using a name similar to TMZ. This is how they described Wayne Knight’s death:

“Actor Wayne Knight, better known to most for playing one of the most indelible roles on NBC’s ‘Seinfeld’ as the character ‘Newman’ is dead and two other passengers are critically injured after their vehicle slammed into a disabled semi-tractor-trailer late Saturday night along Route 446 near the Pennsylvania-New York state border in Eldred Township. Kane-based state police have identified the deceased as the lovable ‘Newman.’ McKean County Coroner Jeff Levin said Knight died of blunt force trauma at the scene…. Police said the crash occurred around 11:15 p.m., when for an unknown reason, Knight lost control of his 2013 Mercedes s550, slamming into the rear of a disabled 5-axle tractor trailer. The sedan sustained considerable damage.”

But if you need more proof then you can hop onto Twitter and see that Wayne has been joking about the whole incident:

Some of you will be glad to hear this, others strangely disappointed, but….I am alive and well! — Wayne Knight (@iWayneKnight) March 16, 2014

Does someone have to DIE to trend? Geez! Thanks for all the love everybody. I didn’t know you cared. Glad to be breathing! — Wayne Knight (@iWayneKnight) March 16, 2014

Did you really think Wayne Knight dead after first hearing of the death hoax?