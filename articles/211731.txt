Samsung Electronics chairman Lee Kun-Hee was in stable condition in hospital Monday after suffering an apparent heart attack, the company said, denying any disruption to its management affairs.

Lee, 72, is largely credited with turning Samsung - founded by his father in 1938 - into a global brand which is now the world's top chip and smartphone maker.

He is also the de facto head of the entire business empire, which employs some 200,000 staff in dozens of affiliates and dominates many aspects of life in South Korea.

Recent health problems have led Lee to become less involved in the day-to-day running of the giant conglomerate than before - although he still has a major say in important strategic decisions.

He was hospitalised late Saturday and underwent a surgical procedure to widen his arteries the next day.

Samsung officials said he was breathing without assistance on Monday and was in a "stable" condition.

"There has been no disruption in the group's day-to-day operation," a group official told AFP.

Samsung Electronics shares closed up 3.97 percent Monday.

Samsung, whose name means Three Stars, began life as a trading company founded by Lee's father Lee Byung-Chull.

Lee took over after his father's death in 1987 and his family controls Samsung through a complex web of share cross-holdings in group subsidiaries.

In recent years Samsung has reorganised to pave the way for Lee's son, Jay Y. Lee, to take over the top spot. He is currently the vice chairman of Samsung Electronics.

Samsung is best known internationally for its smartphones, which enjoy a leading global market share over arch-rival Apple.