Mumbai: Governor Raghuram Rajan on Tuesday said after the current phase of new bank licences, the Reserve Bank will go ahead with the differentiated licencing process starting with payment banks and then proceed to have universal "on-tap" licencing.

"Quiet soon we hope to open the window, it may be that we may open the window first for differentiated banks licences, for example payment banks and then move to opening the on-tap for universal (licence) down the line," Rajan said at the customary post-policy press conference here.

Rajan said ground work with regard to this is ready in the form of the discussion paper on 'Banking structure -the way forward' and RBI will have to build up on that.

Additionally, the learnings from the ongoing process of awarding bank licenses will also help in the diligence and vetting, he said.

"The point is, we should not be giving licence every 10 years...The hope is to make this an ongoing process," Rajan said, stressing the need to go beyond current practice.

Rajan said this structure of having a specialised bank devoted to payments or lending, will also help aspirants of a universal banking license to develop the capabilities to grow.

"This would allow people to develop banking capabilities even with a relative small size of operations, which will then allow them to apply for a full banking licence down the line," he said, choosing not to give a timeline by when he expects action on the front.

Since there are so many applicants and all of them cannot be given licence at a go, Rajan said, "The idea is that once we give this set of licences, there might be some applicants who might be suited for a different kind of licence. We want to tell them that the possibility is opening up and we will get more people there."