Kanye West has reportedly decided on a very unusual wedding gift for Kim Kardashian. According to the Daily Star, the rapper wants to buy the reality star a Burger King location... 10 of them. A source told the British newspaper that West is planning on purchasing 10 European BK franchises for Kardashian because he thinks they will be a "perfect fit" for her.

Reportedly the locations he's eyeing is spread out across Italy, France and the U.K.

"That's where he sees her future career, away from reality TV," the source said.

"She owns all the jewels anyone could ever want, so he is taking the practical route by investing in business for her instead," the source added. "He knows the fast food industry is reliable and lucrative, plus BK is international so he thinks it's the perfect fit for Kim."

West, 36, already knows a thing or two about owning a burger joint since he owns several Chicago Fatburger locations. According to NME, BK said in a statement that they were not aware of either West or Kardashian buying any stores but would love to cater their summer wedding. The couple will be getting married on May 24 in Paris at the Chateau d'Usse.

"Burger King Corp. is familiar with the recent news stories regarding Kanye West and Kim Kardashian," the statement read. "We are unaware of any purchase of Burger King restaurants made by either Mr. West or Ms. Kardashian, but we're available to cater the wedding!"

Hopefully, there will still be a wedding to cater. According to a recent report by Radar Online the "Yeezus" rapper threatened to call off the ceremony after he found that his fiancée and R&B singer Chris Brown reportedly have a past.

A source said West "blew a gasket" when he found out that Brown and Kardashian were rumored to have dated. According to the source he told the reality TV star that if she ever talks to the singer again, the wedding would be off.

"Even though the encounter between Kim and Chris happened long before Kanye came on the scene, being Kanye, he's still intensely jealous," the source said.

Reportedly the two had met up years ago, back in 2010, to discuss business deals but a source claims there was definitely chemistry between them. Kardashians denies she ever did anything with the "Love More" singer.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.