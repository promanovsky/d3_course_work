Blake Lively has been turning it on the red carpet lately and once again brought her A-game while attending the 2014 CFDA Fashion Awards held at Alice Tully Hall at Lincoln Center on Monday (June 2) in New York City.

The 26-year-old actress accompanied designer Michael Kors to the event that evening.

PHOTOS: Check out the latest pics of Blake Lively

Blake was one of the red carpet highlights throughout the Cannes Film Festival earlier this month and we love seeing her stepping out for events more often these days!

FYI: Blake is wearing a Michael Kors dress and Casadei shoes.