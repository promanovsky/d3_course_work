Microsoft

In the age of the selfie, the point-and-shoot predilections of a smartphone society create a new quandary for people: Where do you put all that stuff?

Given the growing volume of image, video, and data files, that question can easily become a headache. Just as quickly, though, myriad technology companies are seeking to fill that need with a variety of free and fee-based cloud services.

And in the latest installment of what's fast becoming a hard-knocks competition, Microsoft said Monday that it plans to more than double the free storage it offers on its OneDrive cloud storage. Microsoft also is raising the storage limits that come with its various Office 365 subscriptions. The changes will go into effect by July.

The highlights:

Free storage climbs to 15 gigabytes from 7GB.

All Office 365 users now get 1 terabyte of storage, up from the previous 20GB allocation.

Office 365 Personal subscribers get 1TB of storage for $6.99/month (in the UK, £5.99/month).

Office 365 Home subscribers get individual storage of 1TB for up to five people for $9.99/month (in the UK, £7.99/month).

Microsoft also is reducing monthly storage subscription fees for users who don't want Office to $1.99 for 100GB (in the UK, to £1.99/month) from $7.49/month. It's dropping the price on 200GB to $3.99 from $11.49.

The pricing updates will take effect in the next month, and current subscribers will automatically be moved to the new prices, Microsoft said.

On the surface, this sounds like the opening stages of another race to the bottom as storage turns into yet another technology commodity. Microsoft is only playing catchup with Google Drive, which also offers 15GB of free storage. Meanwhile, Box offers 10GB and Dropbox 2GB free. (For more, check out this excellent deep dive into the various storage offerings by my CNET colleague Sarah Mitroff.)

If companies can successfully layer other products and services on top, that's a more compelling business, said Angus Logan, Microsoft's head of product management and marketing for OneDrive.

"Storage isn't super interesting," Logan said. "What we build on top of storage is."

"We're not trying to just be in the storage game where we break even because storage is the only hammer we have to swing," he continued. "But if we can say that we want to deliver productivity across all your devices -- and it also comes with cloud storage -- then it really changes the game from being focused only on storage."

For Microsoft, there's also an opportunity to convince people using its free cloud to upload and sync files to consider Office, which is one of the company's biggest franchises. Execs believe that the fact that its storage works so closely with Microsoft Office apps gives the company an edge over vendors that only sell storage.

The OneDrive service started out 26 months ago as SkyDrive. Microsoft was forced to change the name after a court ruled that the software company had infringed on a trademark owned by the British Sky Broadcasting Group.