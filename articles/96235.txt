Zynga Inc (NASDAQ:ZNGA) announced the appointment of David Lee as chief financial officer and chief accounting officer, which highlights the efforts of CEO Don Mattrick to restructure the company. Prior to Zynga, Lee worked with Best Buy Co Inc (NYSE:BBY), serving as the senior vice president of finance.

One month transition period

Lee will replace Mark Vranesh, who will resign after working alongside Lee during the one month transition period. Don Mattrick stated that he along with Vranesh has been making efforts to find a successor since late last year.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Zynga Inc (NASDAQ:ZNGA) stated that Mr. Lee is an expert in strategy and turnaround situations and joined Best Buy in the year 2012.

“David has a deep understanding of business management and a sharp financial acumen that will be invaluable to Zynga’s long term growth and success,” Mattrick said of Lee’s hire. Don Mattrick said that David has a track record of nurturing cultures of excellence and navigating business transformations through sound counsel and strategic planning.

Management reshuffle at Zynga

For the past couple of years, top level executives have been busy in hiring new faces and aggressively reshuffling management. Former chief financial officer David Wehner departed from Zynga Inc (NASDAQ:ZNGA) to join Facebook Inc (NASDAQ:FB) in 2012. Vranesh replaced Wehner and held the post of chief accounting officer position. Don Mattrick joined Zynga in July 2013 as the chief executive officer.

Mattrick has made a serious effort to turn around the company. The efforts include cutting costs by reducing 15% of the workforce in January along with a $527 million transaction to purchase NaturalMotion, a company designing mobile video games and animation technology. Zynga Inc (NASDAQ:ZNGA) issued its IPO in 2011 and designs mainly social games that were made for the platform such as Facebook. It received initial success on these platforms, but then declined with players becoming more interested in mobile devices.

Zynga Inc (NASDAQ:ZNGA) stated a loss of $25.2 million, or 3 cents per share in the fourth quarter, down from a loss of $48.6 million, or six cents per share in the previous year’s corresponding quarter. Sales of the company declined 43% to $176.4 million from $311.2 million.

Zynga Inc (NASDAQ:ZNGA) will post first quarter financial results on April 23. The social game maker said Lee and Vranesh wouldn’t be available for comment until the conference call to discuss results.