Comcast CEO Brian Roberts told CNBC on Tuesday that the cable giant's proposed merger with Time Warner Cable would help ignite innovation and scale in the cable industry.

Roberts, who leads CNBC's parent company, was responding to backlash over the merger from Netflix's chief executive, Reed Hastings. In its earnings letter last week, the online streaming video company's CEO said the $45 billion deal between Time Warner and Comcast would give too much control over the Internet to a single company.

Read More



"What we're doing is going to allow for more innovation, more scale," Roberts said in an interview with CNBC's "Squawk on the Street." "That's what this is all about for Comcast. ... I don't quite see why he would object, but he's entitled to his opinion."



On Monday, Comcast offered to sell 1.4 million cable subscribers to Charter Communications in exchange for $7.3 billion, a measure aimed at speeding up the regulatory process for the TIme Warner Cable merger.

Disclosure: Comcast is the owner of NBCUniversal, the parent company of CNBC and CNBC.com.

—By CNBC's Jeff Morganteen. Reuters contributed to this report.

