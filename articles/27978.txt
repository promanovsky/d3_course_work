Although colon cancer screening can save lives, many skip it because they're uncomfortable with colonoscopy, a procedure that involves inserting a long tube into the anus, but a new DNA stool test may soon provide an option for those who are intimidated with this traditional colon cancer screening test.

The new non-invasive test called Cologuard developed by the Mayo Clinic used DNA specimens from the stool and does not require diet change or bowel preparations as with colonoscopy. It is also apparently capable of detecting more cancers and precancerous polyps than the existing Fecal Immunochemical Test (FIT), which works by detecting cancer through hidden blood in stools.

In a study that compared the efficiency of the Cologuard and FIT, researchers found that Cologuard detected 92.3 of colon cancers while only 73.8 percent of colon cancers were detected by FIT. The study "Multitarget Stool DNA Testing for Colorectal-Cancer Screening," which was published in the New England Journal of Medicine March 19, involved almost 10,000 individuals who have average risks for colorectal cancer.

The researchers have likewise found that the Cologuard, which has not yet been approved by the Food and Drug Administration (FDA), was 94 percent effective in spotting early stage cancers when they are still more curable.

"For a non-invasive test, that's pretty good," said one of the lead researchers of test Dr. Steven Itzkowitz, a professor of medicine at the Icahn School of Medicine at Mount Sinai in New York.

Unfortunately, Cologuard was found to be more prone to showing false positive results than FIT. "In asymptomatic persons at average risk for colorectal cancer, multitarget stool DNA testing detected significantly more cancers than did FIT but had more false positive results," the researchers concluded.

Dr. Itzkowitz says that a false positive could actually lead a patient to get the recommended colonoscopy. "Everyone is recommended to have a colonoscopy anyway," the Doctor said. "So it would really drive people who should have gotten them in the first place."

Indiana gastroenterologist Dr. Thomas Imperiale, the study's lead researcher, also said that it is common to have more false positive test results in screening tests.

"All screening tests for low-prevalence conditions such as cancers, which include mammography for breast cancer, Pap smears for cervical cancer, and PSA tests for prostate cancer, will have more false-positive test results than true-positive test results," Doctor Imperiale said.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.