Samsung has left its Android rivals in the dust when it comes to smartphone sales. The company is building up its Windows Phone portfolio, and its first Windows Phone 8.1 ATIV Core has leaked.

Windows Phone recently passed BlackBerry as the third largest smartphone platform in the U.S., but is a distant third behind Apple's iOS and Google's Android operating system. Microsoft wants to change that, and some reports claim the company has paid Samsung, Sony, and Huawei over $2 billion to build Windows smartphones in 2014. Some reports claim Samsung received as much as $1.2 billion. If so, the company is putting that money to use by launching the Samsung ATIV SE for Verizon and its upcoming and first Windows Phone 8.1 handset.

The Verizon Samsung ATIV SE is Samsung's re-entry into the Windows Phone world after taking some time away from Microsoft's smartphone OS. SamMobile is reporting that the company plans on expanding and will introduce the ATIV Core, its first Windows Phone 8.1 smartphone.

The site claims that the Samsung ATIV Core will be a mid-range smartphone and will ship with a 4.5-inch 720p display. It will reportedly include 1GB of RAM and likely a quad-core processor. The source says the smartphone will use either an 8- or 13-megapixel rear camera and a 2,100 mAh removable battery.

The Verizon Samsung ATIV SE has a very similar design to the five-inch Samsung Galaxy S4, so the ATIV Core and its smaller display could suggest a Galaxy S4 Mini look.

The site claims that it's unsure of when the Samsung ATIV Core will be released, but believes that since Nokia will release its first Windows Phone 8.1 smartphones in May; Samsung's ATIV Core should be expected to ship around the same time. As with all reports of unannounced smartphones, take this one with a grain of salt, but we'll be sure to keep you posted on any Samsung ATIV Core news.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.