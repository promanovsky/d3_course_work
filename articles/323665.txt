Brent Snavely

Detroit Free Press

DETROIT — The Ram ProMaster City commercial van will be the most fuel efficient and capable van in a growing market segment, Chrysler executives said Thursday at the company's proving grounds in Chelsea, Mich.

"This van represents big capability in a small design," said Mike Cairns, vehicle line executive for Ram trucks.

The Ram ProMaster City was adapted from a popular commercial van sold in Europe as the Fiat Doblo. It also is the second commercial van in Ram's lineup after the ProMaster commercial van, introduced last year, and based on the larger platform of the Fiat Ducato.

The ProMaster City is part of a growing class of delivery vans that have hit the U.S. market in recent years. The van will compete with the Ford Transit Connect, the Nissan NV200 and the Chevrolet City Express. The ProMaster City will go on sale in late 2014 or early 2015.

Despite that competition, Reid Bigland, Ram brand CEO and head of Chrysler's U.S. sales, said he expects the ProMaster City to establish itself quickly and will help boost Ram's commercial sales.

"These types of vehicles have been around for 100 years. So long as there are packages to be delivered, boxes to be delivered, electricians, plumbers and tradesmen who need to get to the job site, there is going to be a market for a van like this," said Bigland.

Chrysler lost the Sprinter commercial van after Daimler sold the company to Cerberus in 2007, leaving a hole in its truck lineup. But Fiat and Chrysler immediately began making plans to develop a commercial van lineup in 2009 after the Italian automaker took control of Chrysler.

The ProMaster City is powered by a 2.4-liter, four-cylinder engine that generates 178 horsepower and 174 pound-feet of torque.

The Ram ProMaster City is designed for commercial customers and will be offered in both a cargo version and a five-seat passenger version.

Bigland estimates that about 65% of buyers will opt for the cargo version. He declined to provide an overall sales estimate.

The ProMaster City can haul a maximum of 1,883 pounds, and both sides are fitted with a sliding door for easy access to the cargo. The roof is reinforced to support load rails or roof racks, with a capacity to carry 154 pounds.

Chrysler said the van will get better fuel economy than any competing van, but it did not release fuel economy figures or pricing Thursday.

Chrysler estimates that about 100,000 small commercial vans are sold annually in the U.S., but that is projected to increase in coming years.

The ProMaster City van will be produced at Fiat's joint venture Tofas plant in Bursa, Turkey. The van will be converted to a cargo van at Chrysler's "transformation center" in Baltimore.