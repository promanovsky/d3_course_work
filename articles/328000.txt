The U.S. Supreme Court ruled Wednesday that online TV service Aereo, backed by media mogul Barry Diller, violates copyright law by using tiny antennas to provide subscribers with broadcast network content via the Internet.

On a 6-3 vote, the court handed a victory to the four major TV broadcasters and cast Aereo's immediate future into doubt.

Click here for the full Supreme Court ruling.

Aereo CEO and founder Chet Kanojia said in a statement that the decision was "a massive setback for the American consumer."

"We've said all along that we worked diligently to create a technology that complies with the law, but today's decision clearly states that how the technology works does not matter. This sends a chilling message to the technology industry," Kanojia said. "We are disappointed in the outcome, but our work is not done. We will continue to fight for our consumers and fight to create innovative technologies that have a meaningful and positive impact on our world."

Networks involved in the case praised the ruling. The National Association of Broadcasters commended the Supreme Court for upholding "the concept of copyright protection that is enshrined in the Constitution."

"Aereo characterized our lawsuit as an attack on innovation; that claim is demonstrably false," NAB President and CEO Gordon Smith said in a statement. "Today's decision sends an unmistakable message that businesses built on the theft of copyrighted material will not be tolerated."

In an interview on CNBC's "Squawk on the Street," former chairman of the Federal Communications Commission, Reed Hundt, said that the Supreme Court "made the right call" and that the decision was "a really, really big win for broadcasters."



"Aereo has very little chance surviving in the business and Barry Diller got his hands caught in the regulatory cookie jar," he said. "You can't use technological tricks to bypass [cable network] rules and regulations. I think that's a very reasonable decision."

