Oh no! Kris Jenner leads a pretty luxurious life, but her luck ran out on the brand new June 29 episode of ‘KUWTK’ — she’s been robbed with Kim Kardashian! Click to WATCH the dramatic sneak peek of Kris’ scary moment!

Keeping Up With The Kardashians is always filled with its ups and downs, but the upcoming episode goes too far — Kris Jenner is burglarized. Tons of her designer goods are stolen right from her own suitcase, while she’s on a trip with Kim Kardashian. The worst part? Her gift from Kanye West is gone!

Kris Jenner Is Robbed On ‘Keeping Up With The Kardashians’

Most people would envy Kris Jenner and Kim Kardashian‘s life, but the downside of fame and fortune is that they’re a target — and they’ve been robbed!

The mother daughter duo are on a trip together, which looks to be in Europe, given that they’re at the Vienna Ball later in the June 29th episode, and Kris’ own belongings are stolen right out of her own suitcase in her luxurious hotel room!

Kris notices that her things are missing and she yells for Kim, in the scene (watch above) when she realizes that her suitcase has been sliced open. How scary!

“I can barely believe what I’m looking at,” Kris says, adding that she feels violated. “Somebody went through my stuff and has actually taken half of it.”

Her favorite sunglasses (actually, two pairs of them), her gold purse that she intended on wearing to an upcoming event (probably the Vienna Ball), her new Chanel bag and matching shoes that she bought on a trip to Paris with Kim were all taken! And a fur throw from Kanye, completely gone!

Poor Kris. You know if that fur was from Yeezus, it was sick.

Kim Kardashian Attacked By Racist Kanye Impersonator

Jeez — these ladies just can’t catch a break. On their trip, which was supposed to be nothing but glam and elegance, not only was Kris robbed, but Kim was accosted by a man in full blackface. How awful.

At the Vienna Ball, which was filmed for the episode (watch a sneak peek here), Kim is approached by a Kanye West impersonator in blackface, and Kim obviously takes offense, calling it a “sick joke.”

Hopefully, Kim and Kris were able to enjoy some of their Eurotrip!

HollywoodLifers, did you notice that Kim didn’t seem upset by Kris being robbed? Should she have been there for her mom or did Kris freak out over nothing? Let us know!

— Elizabeth Wagmeister

Follow @EWagmeister

More ‘Keeping Up With The Kardashians’ News: