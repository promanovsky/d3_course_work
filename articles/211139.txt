Lea Michele paid tribute to the late Corey Monteith on the day he would have been celebrating turning 32.

The 27 year-old actress tweeted a black and white photograph of herself and her former 'Glee' co-star laughing together.

Along with the pic was a heart-warming caption that read, "The biggest heart and most beautiful smile.. In all of our hearts.. We love you so. Happy Birthday."

Michele still feels very strongly about Monteith, whom she was in a relationship with, and along with all her 4.4 million followers she wanted to remember him on his birthday on May 11th.

The actor was found dead at noon on Saturday 13th July 2013, when he was staying at the Fairmont Pacific Rim Hotel in Vancouver. The corner lists Monteith's official cause of death as "mixed drug toxicity, involving intravenous heroin use combined with the ingestion of alcohol."

His passing came only months after he voluntarily checked himself into a rehabilitation facility to seek help for substance abuse.

To pay homage to her late boyfriend, this past February Michele released a song from her debut studio album 'Louder,' titled 'You're Mine.'

The track was dedicated to Monteith because it was one of his favorite's, the 'New Year's Eve' actress revealed, telling her fans at the time: "It's so special to me & it's story is so close to my heart."

MORE: Lea Michele Bares Most On Location For "On My Way" Music Video

Michele also remembered the Canadian actor in a heartfelt interview she gave in April's addition of Seventeen magazine.

MORE: Sia Furler Felt Lea Michele's Pain Over Boyfriend's Death

''Cory made me feel like a Queen every day. From the minute he said, 'I'm your boyfriend,' I loved every day, and I thank him for being the best boyfriend and making me feel so beautiful," she said, later adding, ''I only have happy memories of Cory. He was not his addiction--unfortunately, it won. But that wasn't who he was.''