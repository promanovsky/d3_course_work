Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Forget the Oscars, it's all about the MTV Movie Awards !

OK, so the ceremony might not be quite as prestigious with awards including 'Best Kiss', 'Best Shirtless Performance' and '#WTF moment', but with a star-studded line-up, really who cares?

There are no less than 43 celebrities set to grace the red carpet including heartthrob Channing Tatum , blonde bombshell Cameron Diaz and hottie Johnny Depp donning their finest threads.

Eminem and Rihanna are also set to perform their hit single 'Monster' at the Nokia Theatre in Los Angeles and they will share the stage with our very own Ellie Goulding and a host of acts.

Excited? Read our full guide to the MTV Movie Awards 2014:

When are the MTV Movie Awards 2014?

Our coverage begins on Sunday night taking you into the early hours of Monday 14 April morning - and will bring you all the latest news as and when it happens.

You can then get a second chance to see the glitzy ceremony on Monday evening at 9:00pm on MTV.

Just click here to read more .

Who is hosting the ceremony?

(Image: Getty)

American chat show host Conan O'Brien has been revealed as this year’s MTV Movie Awards host.

Announcing the news on his own show 'Conan', he joked: "After eight years of intense negotiations, I am honoured to announce I am hosting MTV's second most prestigious awards show.

“I'm humbled, I'm honored and, as of late this morning, I'm contractually obligated," O'Brien added.

O'Brien is no stranger to the world of TV, having hosted the Emmys twice, as well as his own talk show for the past 21 years.

Are any performers confirmed?

Topping the bill is superstar rapper Eminem and Ri Ri .

The pair will team up to perform their hit single Monster before hitting the road for their three city tour together, taking in New York, Los Angeles and Detroit in August.

Joining them on the line-up is Burn star Ellie Goulding who will team up with Zedd to perform 'Beating Heart' an 'Find You'.

Twenty One Pilots are also signed-up to sing their new single 'Car Radio'.

Who's actually nominated then?

Movie of the Year

12 Years a Slave

American Hustle

The Hobbit: The Desolation of Smaug

The Hunger Games: Catching Fire

The Wolf of Wall Street

Best Male Performance

Bradley Cooper - American Hustle

Leonardo DiCaprio - The Wolf of Wall Street

Chiwetel Ejiofor - 12 Years a Slave

Josh Hutcherson - The Hunger Games: Catching Fire

Matthew McConaughey - Dallas Buyers Club

Best Female Performance

Amy Adams - American Hustle

Jennifer Aniston - We're the Millers

Sandra Bullock - Gravity

Jennifer Lawrence - The Hunger Games: Catching Fire

Lupita Nyong'o - 12 Years a Slave

Breakthrough Performance

Liam James - The Way Way Back

Michael B. Jordan - Fruitvale Station

Will Poulter - We're the Millers

Margot Robbie - The Wolf of Wall Street

Miles Teller - The Spectacular Now

Best Kiss

Jennifer Lawrence and Amy Adams - American Hustle

Joseph Gordon-Levitt and Scarlett Johansson - Don Jon

James Franco, Ashley Benson and Vanessa Hudgens - Spring Breakers

Shailene Woodley and Miles Teller - The Spectacular Now

Emma Roberts, Jennifer Aniston and Will Poulter - We're the Millers

Best Fight

Anchorman 2: The Legend Continues

Identity Thief

The Hobbit: The Desolation of Smaug

The Hunger Games: Catching Fire-

This is the End

Best Comedic Performance

Kevin Hart - Ride Along

Jonah Hill - The Wolf of Wall Street

Johnny Knoxville - Jackass Presents: Bad Grandpa

Melissa McCarthy - The Heat

Jason Sudeikis - We're the Millers

Best Scared-As-[Expletive] Performance

Rose Byrne - Insidious: Chapter 2

Jessica Chastain - Mama

Vera Farmiga - The Conjuring

Ethan Hawke - The Purge

Brad Pitt - World War Z

Best On-Screen Duo

Amy Adams and Christian Bale - American Hustle

Matthew McConaughey and Jared Leto - Dallas Buyers Club

Vin Diesel and Paul Walker - Fast & Furious 6

Ice Cube and Kevin Hart - Ride Along

Jonah Hill and Leonardo DiCaprio - The Wolf of Wall Street

Best Shirtless Performance

Jennifer Aniston - We're the Millers

Sam Claflin - The Hunger Games: Catching Fire

Leonardo DiCaprio - The Wolf of Wall Street

Zac Efron - That Awkward Moment

Chris Hemsworth - Thor: The Dark World

#WTF moment

The RV Crash - Anchorman 2: The Legend Continues

The Beauty Pageant - Jackass Presents: Bad Grandpa

Car Sex - The Counselor

The Lude Scene - The Wolf of Wall Street

Danny's New Pet - This is the End

Best Villain

Barkhad Abdi - Captain Phillips

Benedict Cumberbatch - Star Trek into Darkness

Michael Fassbender - 12 Years a Slave

Mila Kunis - Oz The Great and Powerful

Donald Sutherland - The Hunger Games: Catching Fire

Best On-Screen Transformation

Christian Bale - American Hustle

Elizabeth Banks - The Hunger Games: Catching Fire

Orlando Bloom - The Hobbit: The Desolation of Smaug

Jared Leto - Dallas Buyers Club

Matthew McConaughey - Dallas Buyers Club

Best Musical Moment

Backstreet Boys, Jay Baruchel, Seth Rogen and Craig Robinson Peform in Heaven - This is the End

Jennifer Lawrence Sings "Live & Let Die' - American Hustle

Leonardo DiCaprio Pops and Locks - The Wolf of Wall Street

Melissa McCarthy Sings "Barracuda" - Identity Thief

Will Poulter Sing "Waterfalls" - We're the Millers

Best Cameo Performance

Robert De Niro - American Hustle

Amy Poehler and Tina Fey - Anchorman 2: The Legend Continues

Kanye West - Anchorman 2: The Legend Continues

Joan Rivers - Iron Man 3

Rihanna - This is the End

Best Hero

Henry Cavill as Clark Kent - Man of Steel

Robert Downey Jr. as Iron Man - Iron Man 3

Martin Freeman as Bilbo Baggins - The Hobbit: The Desolation of Smaug

Chris Hemsworth as Thor - Thor: The Dark World

Channing Tatum as John Cale - White House Down