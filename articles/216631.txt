Mice severely disabled by a condition that was similar to multiple sclerosis (MS) were able to walk less than two weeks following treatment with human neural stem cells.

In striking contrast to active, healthy mice, those with an MS-like condition must be fed by hand because they cannot stand long enough to eat and drink on their own. When scientists transplanted human neural stem cells into the MS mice, they expected no benefit from the treatment. They thought the mice would reject the cells, much like rejection of an organ transplant.

Co-senior author, Tom Lane, Ph.D., a professor of pathology at the University of Utah, said my postdoctoral fellow Dr. Lu Chen came to me and said, 'The mice are walking.' I didn't believe her.

He began the study with co-first author Chen at the University of California, Irvine.

Within a remarkably short period of time, 10 to 14 days, the mice had regained motor skills. Six months later, they showed no signs of slowing down.

The findings have been published online in the journal Stem Cell Reports.