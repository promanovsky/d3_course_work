At least 59 people have died from the deadly and quickly spreading Ebola virus in Guinea, UNICEF said.

The disease was first observed in patients last month, but until now, experts hadn't been able to identify the disease, also known as hemorrhagic fever.

Advertisement

At least 80 people contracted the virus, three of whom were children and 59 of whom died.

"In Guinea, a country with a weak medical infrastructure, an outbreak like this can be devastating," the UNICEF representative in Guinea, Dr. Mohamed Ag Ayoya, said in the statement.

Symptoms of the Ebola virus include fever, bruising, hearing loss, diarrhea, vomiting and bleeding.

[CNN] [Voice of America] [CDC]