A prominent consumer privacy group is filing a complaint with the Federal Trade Commission, claiming that Facebook’s users were deceived by the mood experiments conducted by the social network in 2012.

“We think the Facebook study is a deceptive trade practice, which is subject to investigation by the FTC,” Marc Rotenberg, president and executive director of the nonprofit Electronic Privacy Information Center in Washington, D.C., said.

In addition, the group is asking Facebook to make the News Feed algorithm public so users fully understand why they're being shown certain posts. “There should be no more secret manipulation of Internet uses,” Rotenberg said.

The Menlo Park, California, company is under fire for conducting a mood experiment on 700,000 users in 2012, in which researchers at Facebook manipulated the amount of negative or positive content in News Feeds to determine what, if any, impact it would have on subsequent posts by the user. The experiment came to light late last week when the results were published in an academic journal.

Facebook Inc. (NASDAQ:FB) Chief Operating Officer Sheryl Sandberg apologized Wednesday for the experiment. “This was part of ongoing research companies [advertisers] do to test different products, and that was what it was; it was poorly communicated,” she said while in New Delhi, according to the Wall Street Journal. “And for that communication, we apologize. We never meant to upset you.”

Facebook is already under a 20-year-consent decree as part of a 2011 settlement with the FTC over its privacy practices.

Whether the FTC decides to investigate Facebook will hinge largely on what it disclosed to users in its terms of service. "If they never disclosed they might be conducting this research, either in the privacy policy or user agreement, and is not something users likely would expect to occur, it potentially could be considered a deceptive practice," Dana Rosenfeld, a former FTC attorney who is now a partner and chairwoman of the Privacy and Information Security practice in the Washington office of law firm Kelley Drye & Warren.

"If these are material terms that users would not expect by signing up for Facebook, then they should be disclosed in a clear and prominent manner, using language consumers are likely to understand," she said.

The FTC doesn't comment on prospective or ongoing investigations.

Facebook's current Data Use Policy does disclose that user data can be used for research, but as Forbes pointed out, that "research" caveat was added in May 2012, four months after the study took place.

In responding to EPIC complaint, Facebook said its disclosures where adequate. “When someone signs up for Facebook, we’ve always asked permission to use their information to provide and enhance the services we offer," a representative said. "Companies that want to improve their services use the information their customers provide, whether their privacy policy uses the word ‘research' or not.”

Facebook is already under scrutiny for the experiment by U.K. regulators. The Information Commissioner’s Office will examine if Facebook broke any privacy laws while carrying out the experiment and could fine the company up to 500,000 pounds ($839,500).