SAN FRANCISCO (MarketWatch) — Investors may be focused on all things mobile but they are seeing the downside of that emphasis in the results of companies like Google Inc.

Internet advertising and search giant Google Inc. GOOGL, +0.30% GOOG, +0.39% reported disappointing first quarter results, with a drop in the cost-per-click metric, and lower than expected growth in paid clicks, which rose 26% year-over-year, while Wall Street was looking for growth of nearly 29%.

One issue is that most consumers are not following up on the mobile ads with purchases. Analysts voiced concerns about the disparity of paid clicks for the desktop and ads for mobile devices.

Google executives said they believe the gap between the two will eventually be resolved, because of the large opportunity for customizing mobile ads by location and context.

“I think the way to think about it is that in mobile you have location and you have context of individuals, which you don’t have with the desktop,” said Nikesh Arora, Google’s chief business officer. “And the more you know about the user in the context, the more effective advertising can provide them.”

Another analyst asked Arora to describe some of the building blocks that are needed before the gap between desktop and mobile ads can close. One of the simplest issues is to have a frictionless payment system so consumers can pay easily from their devices, without having to enter a lot of data into a small screen. Another issue is that merchants need to optimize their mobile websites better for selling directly from the mobile interface.

But it isn’t an issue that is going to be resolved in a quarter or two.