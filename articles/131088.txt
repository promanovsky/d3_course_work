Follow-up in the works to 1993 comedy about a divorcee who impersonates a Scottish nanny to be closer to his kids

Robin Williams is set to return in a sequel to Mrs Doubtfire, the 1993 comedy about a divorced dad who impersonates a Scottish nanny to be closer to his children, according to the Hollywood Reporter.

Williams played a struggling actor who dons drag to become the no-nonsense Euphegenia Doubtfire in Chris Columbus's film. The role which won him the Golden Globe for best actor in a comedy or musical two decades ago. The sequel's screenplay is by Elf director David Berenbaum, and the new production would also see Columbus return to the director's role.

Meanwhile, Mara Wilson, who played Doubtfire's charge Nattie Hillard in the original film, has taken to Twitter to voice her opposition to the project.

"I've been in some mediocre movies, but I've never been in a sequel," she wrote. "Sequels generally suck unless they were planned as part of a trilogy or series. I think Doubtfire ended where it needed to end."

"They haven't asked me to be in it, and I think it would be weird if they did! I don't act much and am not a cute little kid anymore."

A second film has been talked about since at least 2001, but neither Williams nor Columbus warmed to previous screenplays. The original Mrs Doubtfire was a sizeable box-office hit in 1993, taking $441m across the globe off the back of strong reviews.