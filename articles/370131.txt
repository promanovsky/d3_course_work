Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

You’re on a speed boat, so obviously you’ve got spray in your face, rubbish hair and watery eyes. Not Kim Kardashian though, obviously.

She’s been on a boat in the Hamptons whilst filming Kourtney And Khloe Take The Hamptons and is obviously aiming to distract herself from missing Kanye West who’s currently getting booed by basically the whole of the United Kingdom for being really annoying at Wireless .

So obviously, the big-bottomed lady took a lovely, windswept selfie. Is there anywhere this woman won’t take a selfie?

The answer is: no. If Kim had been alive in 79AD and iPhones had been available then she’d definitely have got covered in lava whilst taking a volcano-selfie at Pompeii. Ooh, just imagine that for a second. How satisfying.

We can also imagine Kim going into space in an effort to be the first Kardashian with a space selfie (what would happen to her bum in space, though? The mind boggles).

It’s ok though because Kim also did some hand-holding with her nephew, Mason, in order to make up for her utter self-absorption.

Here’s a gallery of Kim being vain on said boat, just in case you don’t believe us.