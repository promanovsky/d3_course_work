The S5 Mini is a solid smartphone with an impressive range of features borrowed from its bigger brother, but it fails to really impress at what is now a highly competitive price point.

The Samsung Galaxy S5 Mini was quietly announced at the beginning of July, and it's since found its way into around the world, so how does the pint-sized smartphone shape up?

Well it's replacing the Galaxy S4 Mini from 2013, but rather than taking cues from that handset it instead looks to its bigger brother, the Samsung Galaxy S5, for inspiration.

As far as design goes there's no mistaking this is a close relative to the Galaxy S5, with the Galaxy S5 Mini sporting the familiar ribbed faux-metal band around its circumference and the dimpled polycarbonate rear linking it directly to its bigger brother.

It's got the HTC One Mini 2, Sony Xperia Z1 Compact, iPhone 5C and the LG G3 Beat in its sights, as these shrunken smartphones look to do battle a couple of tiers below their flagship brethren.

SIM-free you'll need at least £300 (about $470, AU$565) for the Galaxy S5 Mini, while on contact it can be had for free on two year deals starting at £22.50 in the UK.

As I've mentioned when it comes to design it really is a mini version of the Galaxy S5, although there's no annoying flap over the microUSB port at the base of the handset.

Now you may think the omission of this protective flap means the Galaxy S5 Mini has lost the dust and water resistant features of Samsung's flagship, but you'd be wrong.

In fact the Galaxy S5 Mini holds the same IP67 water and dust resistant rating, meaning you can drop your phone in the bath without it dying.

Seeing as this can be done with an open microUSB port it'll be frustrating for any Galaxy S5 owners who are having to manipulate the fiddly flap every time they want to charge the device.

It's not the exposed connection port at the base of the S5 Mini that's got me worried if the phone hits water though - it's the removable rear cover.

The thin piece of dimpled polycarbonate does snap snugly onto the rear of the Galaxy S5 Mini, but it's not exactly difficult to remove and the slender rubber seal that runs around part of the inside doesn't exactly fill me with confidence.

Behind that rear cover you'll find microSIM and microSD slots as well as removable 2100mAh battery which should provide a strong offering, although the Galaxy S5 Mini won't benefit from the power efficiency of the Qualcomm processor its big brother houses as it's stuck with a Samsung own-brand 1.4GHz quad-core chip.

Slightly annoyingly you have to remove the battery to gain access to the microSIM and microSD slots, so you can't easily switch out a card while keeping the phone on.

The Samsung Galaxy S5 Mini measures 131.1 x 64.8 x 9.1mm, meaning it's a little chunkier than the 8.1mm Galaxy S5, but its smaller 4.5-inch display means that in terms of height and width it's easier to manage.

I found the Galaxy S5 Mini sat in the palm nicely, but the metallic effect band and plastic rear offered very little in the way of grip and I did come close to dropping the handset on numerous occasions. You may want to invest in a cover for this device.

At 120g the S5 Mini is a comfortable weight, and it's certainly not overbearing on the wrists allowing you to hold and operate it one handed with relative ease.

The volume rocker on the left, and power switch on the right, are easy to hit during one handed operation, and I was also easily able to reach the physical home key below the screen.

That key is flanked by a back button on its right and a multi-tasking option on the left - again mirroring the setup on the Galaxy S5.

Up top you'll find a 3.5mm headphone jack and a small black square which is hiding the Infra Red (IR) blaster.

This allows you to utilise the Smart Remote app which comes pre-installed on the Galaxy S5 Mini, and it lets you control TVs, set top boxes, DVD players, Hi-Fi and other entertainment equipment from your phone.

It works nicely and the app is easy to set up, so you'll be channel hopping in no time.

The S5 Mini does feel solid and capable of taking a few knocks, but it fails to ooze any premium quality, with the overly noticeable plastic body making it feel pretty cheap in the hand - especially when you compare it to the One Mini 2 and Xperia Z1 Compact.