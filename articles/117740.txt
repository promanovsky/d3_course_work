At least 4m Android smartphones in the US, and tens of millions worldwide, could be exploited by a version of the "Heartbleed" security flaw, data provided to the Guardian shows.

Worldwide, the figure could be 50m devices, based on Google's own announcement that any device running a specific variant of its "Jelly Bean" software – Android 4.1.1, released in July 2012 – is vulnerable.

The figure, calculated using data provided exclusively by the analytics firm Chitika, is the first time an accurate estimate has been put on the number of vulnerable devices. Other estimates have suggested it is hundreds of millions, based on the number of devices running versions of Android 4.1. But most of those run 4.1.2, which is not at risk.

Google has not disclosed how many devices are vulnerable, although it has indicated that the figure is "less than 10%" of devices activated worldwide.



But that could be a huge number, given that Google has activated 900m Android devices worldwide. There are also hundreds of millions of handsets in China running Android without Google services, which would not show up on its systems, and which are also likely to be running vulnerable versions.

The figure on the number of vulnerable devices comes from an analysis for the Guardian by the ad network Chitika of US network traffic. Looking at web traffic for the seven-day period between 7 April and 13 April, "Android 4.1.1 users generated 19% of total North American Android 4.1 Web traffic, with users of version 4.1.2 generating an 81% share. Web traffic from devices running Android 4.1.0 made up less than 0.1% of the Android 4.1 total observed, so we did not include for the purposes of clarity," said Andrew Waber, a Chitika representative.

Based on Comscore data which suggests there are 85m Android smartphones in use in the US, that means that there are at least 4m handsets which are vulnerable.

The devices would be vulnerable to a hack described as "reverse Heartbleed" - where a malicious server would be able to exploit the flaw in OpenSSL to grab data from the phone's browser, which could include information about part sessions and logins.

Although the risk is principally theoretical, it has focused attention on the security risk to Android devices which are running older versions of software but which are in effect abandoned by handset manufacturers and mobile operators, both of which have to process and pass on updates. Manufacturers typically provide updates for Android devices for 18 months after their release, despite efforts by Google in the past to provide a co-ordinated update scheme.

Of the smartphones in use, only Android devices are vulnerable to this form of attack. Apple does not use the vulnerable version of OpenSSL on the iPhone or iPad, while Microsoft said that neither Windows Phone nor Windows is affected.

Although the affected devices lie outside the 18-month window under which Google says devices are "traditionally" updated, the company said that "We have also already pushed a fix to manufacturers and operators." But it's unclear how quickly those will be implemented, if ever.

The security firm Lookout, which provides Android security software, has produced a downloadable Android app which lets people check whether their device is vulnerable.

More than 80% of people running Android 4.1.1 who have shared data with Lookout are affected, Marc Rogers, principal security researcher at the San Francisco-based company, told Bloomberg.

The proportion of at-risk devices in Germany is nearly five times higher than in the US, probably because one of the popular devices there uses the 4.1.1 version of Android, Rogers said. Based on Chitika's numbers, that could mean up to 20% of Android smartphones there being vulnerable, a number that would run to millions.

But Rogers also told Bloomberg that there are no signs yet of hackers are trying to attack Android devices through the vulnerability. It would be complex to set up and have a low success rate because the vulnerable devices would have to be targeted one by one, amid all the non-vulnerable ones. "Given that the server attack affects such a larger number of devices and is so much easier to carry out, we don't expect to see any attacks against devices until after the server attacks have been completely exhausted," Rogers told Bloomberg.

Google released the first version of Android 4.1 in July 2012. It was superseded in July 2012 by 4.1.1, which brought a bug fix for Nexus 7 tablets. A final version, 4.1.2, was released in October 2012.

Only 4.1.1 uses the vulnerable version of OpenSSL. While Google noted in a blogpost about vulnerabilities of its products to Heartbleed, it didn't specify what proportion are running 4.1.1, and the numbers are not split out from its Android platform versions information for developers, which combines the data for all three versions of 4.1 to give a headline figure of 34.4%.

• Developer who wrote code with Heartbleed bug regrets 'oversight'