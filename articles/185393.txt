The government will act with "even-handed neutrality" if US pharmaceutical firm Pfizer bids to takeover its British rival AstraZeneca, Vince Cable has told MPs.

Mr Cable stressed that any deal was ultimately a matter for the shareholders of both companies, but said the government could invoke a public interest test.

"This would be a serious step and not one that would be taken lightly but I'm open-minded about it whilst stressing that we are operating within serious European legal constraints," the Lib Dem business secretary told MPs.

He was summoned to the Commons to explain the government's position after Liberal Democrat MP Julian Huppert was granted an urgent question on 6 May 2014.

AstraZeneca has rejected two multi-billion dollar offers from US firm Pfizer. The board said the latest £63bn offer continued to "significantly undervalue" the company.

'Options open'

Labour has accused the government of acting as a "cheerleader" for a potential deal and called for an independent inquiry into whether it is in the national interest.

Shadow business, innovation and skills secretary Chuka Umunna claimed Pfizer's assurances about its future intentions for AstraZeneca were "not worth the paper they are written on".

Two committees of MPs are to question senior executives from both Pfizer and Astra Zeneca - including the Business, Innovation and Skills Committee.

Its chair, Labour MP Adrian Bailey, said lessons must be learned from the takeover of Cadbury by US-based Kraft, which resulted in British job losses, including that "you can't necessarily take the assurance of the take over company literally".

He asked Mr Cable to undertake to the ensure the government would intervene in any takeover if Pfizer's assurances could not be trusted.

Mr Cable said he would not make assurances at this stage, "I'm merely keeping the options open", but added that the previous Labour government received no assurances from Kraft prior to its takeover of Cadbury.

Conservative MP Richard Harrington asked what message it would send if the UK government found a way to "scupper" a deal despite assurances from the company and against the wishes of shareholders. Mr Cable stressed the government did not want to "compromise our reputation for being open to good foreign investors".

Mr Huppert, whose Cambridge constituency is to host AstraZeneca's new headquarters, said Pfizer's proposal was largely driven by tax incentives and questioned what would happen if the tax regime changed in the US and Pfizer withdrew its investment in Britain.

Mr Cable told MPs: "We see the future of the UK as a knowledge economy, not as a tax haven. Our focus is on what is best for the UK - securing great British science, research and manufacturing jobs and decision-making in the life sciences sector."