A man who suffered horrific facial injuries in a motorbike accident has had pioneering surgery to rebuild his face using 3D printed parts.

Stephen Power from Cardiff in Wales is thought to be one of the first trauma patients in the world to have a procedure in which 3D printing was used at every stage.

The 29-year-old suffered multiple trauma injuries in an accident in 2012. Despite wearing a crash helmet, his top jaw, nose and both cheek bones were broken and he fractured his skull.

Surgeons said that by using 3D printing techniques, much of the guesswork was removed from the reconstruction of the face.

The result is a face that is remarkably similar to Mr Power's before the accident.

The team at Morriston Hospital in Swansea in Wales used CT scans to create and print a symmetrical 3D model of his skull, followed by cutting guides and plates printed to match.

In order to try to restore the symmetry of his face, the surgical team used CT scans to create and print a symmetrical model of Mr Power's scan.

It then cut guides and printed plates to match the CT scan.

The surgical team had to re-fracture Mr Power's cheekbones, following the cutting guides, before remodelling the face.

A titanium implant, printed in Belgium, was then used to hold the bones in place.

Mr Power said the results were "totally life changing".

He wore a hat and glasses to hide his injuries before the operation, but said he felt transformed immediately after the surgery.

"I could see the difference straight away the day I woke up," Mr Power told the BBC.

"I'm hoping I won't have to disguise myself -- I won't have to hide away.

"I'll be able to do day-to-day things, go and see people, walk in the street, even go to any public areas."

Maxillofacial surgeon Adrian Sugar said the results were "incomparable" with anything he had achieved before.

"Without this advanced technology, it's freehand. You have to guess where everything goes," he said in a statement.

"The technology allows us to be far more precise and get a better result for the patient."

The project was a joint venture between the Centre of Applied Reconstructive Technologies in Surgery (Cartis), a collaboration between the Swansea hospital and scientists at Cardiff Metropolitan University.

It is being featured in an exhibition about 3D printing at the Science Museum in London.