The moon appeared brighter and bigger than usual overnight Saturday, due to a phenomena known as a "supermoon." The enhanced visible moon is the first of three supermoons predicted for the summer.

Also known as a "perigee moon," the moon's different appearance is caused when the moon is full as it is also closer in its orbit point to Earth. NASA reported on its website that the moon would "seem extra big and bright" in the early hours on July 12. The enlarged full moon occurred around 7:25 a.m. Eastern Time Saturday.

The Boston Globe reported that the moon appeared "huge" near the horizon, but this was attributed to its low position in the night sky.

Two more nights are forecast for supermoons: one on Aug. 10 and another Sept. 9. In August, the moon will become full at the same time that it is close to Earth, forming what could be an "extra-supermoon," according to NASA.

However, scientists said a perigree moon may be difficult to detect. One supermoon last June was particularly bright and big, appearing 14 percent larger and 30 percent brighter according to NASA.

"A 30 percent difference in brightness can easily be masked by clouds and haze. Also, there are no rulers floating in the sky to measure lunar diameters," an entry on NASA's website said. "Hanging high overhead with no reference points to provide a sense of scale, one full moon looks about the same size as any other."

It is not uncommon to have multiple supermoons in a single year, last year also had three visible supermoons over a span of consecutive three months.

"Generally speaking, full moons occur near perigee every 13 months and 18 days, so it's not all that unusual," Geoff Chester of the U.S. Naval Observatory said.