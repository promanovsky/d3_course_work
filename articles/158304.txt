Eilish O'Regan Health Correspondent

IRELAND'S half-a-million migraine sufferers have been given new hope of relief after trials of pioneering jabs were shown to prevent attacks for the first time.

The experimental drugs, which target a protein-like molecule thought to spark migraines, have halved attacks over a number of weeks of trials. Experts say it is an exciting development for people who have to endure the condition.

The tiny protein molecule is released into the bloodstream during an attack and has a central role in the underlying mechanisms of a migraine attack.

Dr Edward O'Sullivan, medical adviser to the Migraine Association of Ireland, said that the new drug could be a significant breakthrough.

"The recent results are encouraging, and the drugs are now entering phase three late-stage clinical trials to further evaluate the benefits and safety of these new therapies.

"These results are exciting, as to date no drugs have been specifically developed for migraine prevention," he said.

More testing is now needed and it will be a number of years before they are considered by the Irish Medicines Board for use here.

The drugs, one given intravenously and another by injection, are part of a new approach to preventing migraine headaches.

They are "monoclonal antibodies" that target the tiny protein called calcitonin gene-related peptide (CGRP) – which recent research has implicated in triggering migraine pain.

In one study, patients saw a 66pc drop in migraine attacks five to eight weeks after a single dose of the IV drug – known for now as ALD403. That compared with a 52pc reduction in patients who were given a placebo.

Patients in another trial who received the injection drug saw a similar benefit from three months' worth of bi-weekly treatments.

The results, presented to the American Academy of Neurology's annual meeting in Philadelphia, are preliminary, and experts stressed that many questions remain.

Larger, longer-term studies are still needed to confirm the drugs' effectiveness and safety, they said.

Irish Independent