London: A letter signed by more than 50 researchers and public health specialists has urged the World Health Organization (WHO) to "resist the urge to control and suppress e-cigarettes".

The letter says that the devices - which deliver nicotine in a vapour - could be a "significant health innovation", the BBC reported.

But the UK's Faculty of Public Health says it is too early to know whether benefits outweigh potential risks.

The WHO said it was still deciding what recommendations to make to governments.

The open letter has been organised in the run-up to significant international negotiations on tobacco policy this year.

Supporters of e-cigarettes, who argue the products are a low-risk substitute for smoking, fear they might become subject to reduction targets and advertising bans.

There has been a big growth in the market for e-cigarettes, but the Department of Health says they are not risk-free.

Critics said that not enough is known about their long-term health effects. A recent report commissioned by Public Health England said e-cigarettes required "appropriate regulation, careful monitoring and risk management" if their benefits were to be maximised.

The letter has been signed by 53 researchers - including specialists in public health policy and experts such as Prof Robert West, who published research last week suggesting that e-cigarettes are more likely to help people give up smoking than some conventional methods.