Google+

It's been two years since Google unveiled Glass at its annual developers conference in 2012.

What was once a hot, buzzy product, now faces widespread skepticism.

Google has hired someone new to lead the group, and breath fresh life into the product. Ivy Ross, a retail and art industry veteran with a rich background in design, will lead the Google Glass team.

Ross announced in a post from the company's official Google+ page for Glass that she'd be joining the team to help answer the "seemingly simple, but truly audacious questions Glass poses."

In other words, it seems as if her role be to take Google Glass from an early-adopter toy to a mainstream gadget that people actually want to buy.

She'll be taking over the position previously held by Babak Parviz, and she'll work closely with Google X heads Astro Teller and Google co-founder Sergey Brin.

The new hire comes just as Adrian Wong, one of Google's lead electrical engineers for Glass, left to work for Oculus VR—the virtual reality company that Facebook just bought for $2 billion.

Most recently, Ross worked as the Chief Marketing Officer for Art.com, a giant online specialty retailer of high-quality wall art.

In her nearly two years at Art.com, she oversaw the company's marketing, branding, merchandising, user experience and product management functions.

Her experience in the art industry extends beyond working as a marketing executive. Ross has pieces of her own in the permanent collections at the Smithsonian Museum in Washington and the Victoria and Albert Museum in London.

Ross' background includes a diverse array of retail brands such as Gap Inc., The Disney Store, Old Navy, Mattel, Coach, and Calvin Klein—for all of which she held high-end executive positions involving design and development.

She may be best known, however, for her role in developing the Ello Creation System for Mattel in the early 2000s. The Ello Creation System is a building set for girls that was made to successfully engage girls in building and creating things.

In 2001 when Ross worked as the head of design and development for the Girls' Division at Mattel, she was tasked with developing a new hit toy for pre-teen girls. She sought to create something that was innovative and different than the traditional Barbie doll—something that was part craft kit, part construction set, as Fast Company described it in their profile of Ross.

Ross created a special think tank just for the initiative called Project Platypus, a name that Ross chose for a specific reason.

"Other companies have skunk works," she said to Fast Company. "We have a platypus. I looked up the definition, and it said 'an uncommon mix of different species.'"

It's a surprising hire, but it makes sense given Google's strategy so far in advertising Glass. In September, Google placed a 12-page fashion spread in Vogue magazine featuring models wearing its heads-up display. Google also partnered with New York Fashion Week in 2012 to put Glass on the runway.

Wearable tech in general hasn't really taken off with everyday users yet, but Google is clearly trying to change that with Glass. Wearable displays such as Glass, unlike smartwatches, face a more unique challenge in that Google will have to convince people that its product is worth wearing on your face.

This could be more difficult for users who aren't used to wearing glasses, as Chris Jones, VP principal analyst with Canalys Insight, told Business Insider in a previous interview.

"People are doing whatever they can not to wear glasses, whether its contacts or Lasik treatment," Jones said. "And this is kind of going back on that."

Google still hasn't revealed when the consumer-ready version of Glass will go on sale, but anyone in the US is now eligible to buy a beta version for $1,500.