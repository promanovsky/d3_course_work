HBO premiered the preview from the eighth episode of “Game of Thrones” Season 4, titled “The Mountain and The Viper,” on Sunday, after airing the seventh episode “Mockingbird,” where the fate of Tyrion Lannister will be decided.

Warning: Full “Game of Thrones” Season 4 spoilers.

The 36-second promo of the eighth episode opens with Ramsay Snow coaching Theon/Reek as they reach Moat Cailin, a ruined collection of towers located on the Neck, which is a part of the North and under the rule of House Stark. Ramsay orders Reek to pretend that he's Theon and help him bring the Moat to him, so that he can prove himself to his father.

Mance Rayder and his army attack Mole's Town, a village close to Castle Black, while Jon Snow is worried as the Wildlings will hit the Night's Watch next.

Meanwhile, Sansa is seen in a gathering, where people are talking about what happened to Lysa Arryn, who, as seen in “Mockingbird” makes a shocking exit through the Moon Door. And, as Littlefinger’s motives begin to be questioned, Sansa seemingly testifies against him.

The fate of Tyrion Lannister, who has been accused of murdering Joffrey, will be decided as he had asked for a trial by combat in the sixth episode, “The Laws of Gods and Men.”

In the seventh episode, Prince Oberyn Martell who offered himself to be Tyrion's champion for the combat will finally get a chance to fight The Mountain, as Cersei chooses Gregor Clegane as her champion.

Prince Oberyn had mentioned earlier that The Mountain had raped and killed his sister, and this could be his chance to avenge the death of his sister. The fight, which will be the spotlight of the eighth episode, is titled after Clegane and Prince Oberyn's nicknames.

“The Mountain and the Viper” episode of Game of Thrones is written by David Benioff and D. B. Weiss, and directed by Alex Graves. The episode will not air on May 25, as the show is taking a one-week break for Memorial Day, and will air on June 1 instead.

Check out the promo video here: