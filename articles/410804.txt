NEW YORK (TheStreet) -- Shares of The Procter & Gamble Co. (PG) - Get Report are up 3.23% to $79.82 after the company announced it's shedding more than half of its global brands over the next two years, leaving it with 70 to 80 of its top performers to focus on products with better sales, the Associated Press reports.

The company did not say which products it plans to keep but noted that they account for more than 90% of sales, the AP added.

The decision by the world's largest consumer products maker to sell or eliminate 90 to 100 brands comes as the company fights to boost its sluggish sales, reporting a 1% decline in its latest quarter.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.



PG data by YCharts



STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.