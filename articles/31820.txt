What do you get when you cross a velociraptor and a chicken? Anzu Wyliei.

The new dinosaur species was discovered by scientists at an excavation site in North Dakota. That, coupled with the bird-like qualities of the dino, has earned the newly discovered species the nickname “Chicken from Hell.” The results were published in the Plos One Journal on Wednesday. Anzu Wyliei belongs to the Oviraptorosaurian, or dinosaurs with feathers, category with the Archaeopteryx. Like a bird, Anzu Wyliei has a crest, a beak ,and a bird-like skeleton, but rest assured, the “Chicken from Hell” is all dinosaur.

Watch the video above for more.

Get our Space Newsletter. Sign up to receive the week's news in space. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.