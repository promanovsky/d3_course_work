Top Consumer Shares:

WMT: +1.12%

MCD: -0.18%

DIS: +0.20%

CVS: flat

KO: -0.29%

GE: -0.71%

Consumer shares were mixed in pre-market trade on Thursday.

In consumer stocks news, IHS ( IHS ) Thursday reported net income of $55.5 million, or $0.81 per share, for Q2 ended May 31 versus a net income of $42.9 million, or $0.65 per share, a year earlier. On an adjusted basis, net income was $100.9 million, or $1.47 per share, up from last year's adjusted net earnings of $83.5 million, or $1.26 per share, beating the Capital IQ consensus of $1.44.

Revenue rose to $568 million from $418.1 million. The average analyst estimate was for revenues of $550 million, based on the Capital IQ survey.

And, Pier 1 Imports ( PIR ) shares were down after it reported net income of $15.1 million, or $0.16 per share, for Q1 ended May 31, down from a net income of $20.3 million, or $0.19 per share, in the comparable period last year. The average analyst estimate based on the Capital IQ survey was for EPS of $0.20.

The importer of imported decorative home furnishings and gifts generated sales of $419.1 million, up from $394.9 million in the year-ago quarter, versus the Capital IQ consensus of $423 million.

Company comparable sales rose 6.3% due to increases in total brand traffic, conversion and higher average ticket.

Finally, Harley-Davidson ( HOG ) said it is launching an electric motorcycle, promoting it in the U.S. now although it is not for sale yet. HOG is trading with a 52-week range of $49.15 to $74.13.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.