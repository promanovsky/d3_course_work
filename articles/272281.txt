Speaking a second language can slow down the ageing of the brain, a study has said.

According to the paper, published in Annals of Neurology, the positive effects of being bilingual occur even if someone learns a new language during adulthood.

"Our study is the first to examine whether learning a second language impacts cognitive performance later in life while controlling for childhood intelligence," said lead author Dr. Thomas Bak, from the University of Edinburgh.

The study was based on data from the Lothian Birth Cohort. Around 835 English native speakers were given an intelligence test in 1947, at the age of 11, and then again in their early 70s, between 2008 and 2010.

Of the study's respondents, 262 people knew more than one language - 195 of them had learned the second language before 18, while 67 had acquired the skill in adulthood.

After analysing the data from the intelligence tests, the researchers concluded that those who spoke another language had better scores in terms of of intelligence and cognitive abilities.

The benefits of being bilingual were seen both in people who acquired the second language early and those who picked it up later in life.

Bak said the pattern they found was "meaningful" and the improvements in attention, focus and fluency could not be explained by original intelligence.

"These findings are of considerable practical relevance. Millions of people around the world acquire their second language later in life. Our study shows that bilingualism, even when acquired in adulthood, may benefit the aging brain" Buk continued.



"The Lothian Birth Cohort offers a unique opportunity to study the interaction between bilingualism and cognitive aging, taking into account the cognitive abilities predating the acquisition of a second language."

After reviewing the study, Dr. Alvaro Pascual-Leone, an Associate Editor for Annals of Neurology and Professor of Medicine at Harvard Medical School in Boston, said: "The epidemiological study by Dr. Bak and colleagues provides an important first step in understanding the impact of learning a second language and the aging brain.

"This research paves the way for future causal studies of bilingualism and cognitive decline prevention."

A previous study on the positive effects of knowing a second language showed that being bilingual could delay the onset of dementia.