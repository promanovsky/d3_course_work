NBC News’ Brian Williams made an appearance on The Tonight Show Starring Jimmy Fallon on Monday evening (April 21), and was given quite the welcoming present in the form of a rap video.

Host Jimmy Fallon compiled lots of footage of the 54-year-old newsman and edited it enough to where he raps a portion of Snoop Dogg‘s “Gin and Juice.”

“I’ve never said ‘hippity,’” Brian said on the show. “Never said ‘hippity.’”

In case you missed it, Jimmy made Brian rap the song “Rapper’s Delight” earlier in the year – check it out!



Brian Williams Raps ‘Gin & Juice’

Click inside to watch Brian‘s interview with Jimmy after the unveiling…