Unfortunately, you would have had to be living under rock in outer-Mongolia not to have noticed the press coverage surrounding Kim Kardashian and Kanye West’s wedding ‘merger’ this weekend.

Now, the first official pictures have arrived, via an unsurprising exclusive with E! Online (they own the rights to Keeping Up With The Kardashians), and despite the endless amount of farcical pomp linked with the ceremony, the pictures look relatively ‘normal’. At least proportionately so, considering the couple’s predisposition for overexposure, conspicuous consumption and extravagance.

The wedding did, after all, take place in a castle in Florence, surrounded by the Italian military for maxim security, and sound-tracked by operatic musician Andrea Bocelli.

They are pictured kissing in front of a wall of white flowers, following their exchange of vows.

Then they are photographed walking down the aisle, Kardashian in her custom-made Givenchy gown and West in his bespoke tuxedo, also designed by the same label (designer Riccardo Tisci was one of the hundreds of famous guests present).

Kim Kardashian's wedding photo broke a new Instagram record

Their daughter, 11-month-old North West (who was apparently also wearing Givenchy – there’s just no point in skimping on these things really…), is seen precariously balanced in the arms of her grandmother, Kris Jenner.

There is also a full-length picture of Kardashian’s lace gown, which was taken at her final dress fitting in Paris. There are probably few brides who could a boast a dress train to rival that of Diana, Princess of Wales, but we would expect nothing less.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The remaining pictures feature the couple posing in a photo booth installed at the wedding reception – one of them ‘casually’ kissing and the other with them wearing his-n-hers leather jackets, emblazoned with ‘Just Married’ in white paint.

Kim Kardashian and Kanye West let everyone know they are married, just in case we had missed it

Kim Kardashian's wedding photo broke a new Instagram record