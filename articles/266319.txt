Thousands of New York City police officers will soon be carrying emergency antidotes to help deal with the recent spike in heroin overdoses, officials said Tuesday.

New York Attorney General Eric Schneiderman and city Police Commissioner William Bratton announced that the state is providing the New York Police Department with $1.17 million from civil and criminal forfeiture cases to pay for 19,500 kits intended to equip patrol officers, including those assigned to transit and housing bureaus.

"By providing NYPD officers with naloxone, we are making this stunningly effective overdoes antidote available in every corner of the five boroughs," he said. "This program will literally save lives."

Kits contain two syringes and two inhalers of naloxone - also marketed under the brand name Narcan - and instructions. They cost about $60 each with a two-year shelf life.

Some officers in the nation's largest police force were equipped with the drug as part of pilot program on Staten Island. They have saved five overdose victims police officials said.

In less than two months since establishing the reimbursement program, more than 150 law enforcement agencies are getting about 25,000 naloxone kits, Schneiderman said.

In Suffolk County, it was used last year to save 563 lives, according to the attorney general's office. In Quincy, Massachusetts, the first department in the U.S. to require its officers carry the kits, they had used it 221 times by February to successfully reverse 211 overdoses.