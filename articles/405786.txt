US Secretary of State John Kerry is pushing for a trade deal with India as the deadline looms in less than 48 hours. ― Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

NEW DELHI, July 30 — US Secretary of State John Kerry, who arrived in India today for an official visit, has pressed New Delhi to drop its opposition to global trade reforms, saying it was a test of the country’s commitment to advance economic liberalisation.

Kerry made the call in a newspaper article, penned along with US Commerce Secretary Penny Pritzker, published hours ahead of his arrival for talks aimed at revitalising ties that have been mired in disputes over trade, intellectual property rights and climate change.

India has threatened to block a worldwide reform of customs rules, saying it must be accompanied by a parallel agreement allowing developing countries more freedom to subsidise and stockpile food grains.

The deadline for the deal that the World Trade Organisation says could provide a trillion dollar stimulus to the global economy is tomorrow.

Kerry said India stood to gain by creating a level playing field instead of erecting trade barriers.

“In this regard, as we work with our trading partners around the world, India must decide where it fits in the global trading system. Its commitment to a rules-based trading order and its willingness to fulfil its obligations will be a key indication,” he said in the article in The Economic Times.

India and the United States have already clashed at the WTO, with Washington saying Delhi’s 11th hour resistance could kill a deal that could create 21 million jobs.

The row adds to frustration from both sides over the often prickly nature of what US President Barack Obama once called “a defining partnership of the 21st century.”

“It will be hard to make a persuasive case...for a larger economic ambition with India if India decides to part ways with the entire WTO membership on the Bali deal,” said Alyssa Ayres, former US deputy assistant secretary of state for South Asia.

Yet despite growing concern at WTO headquarters in Geneva, a senior Indian trade official said on Wednesday all was not lost.

“I am sure in the course of the next almost 48 hours there will be suggestions coming from others, and suggestions can come from us also. We will make suggestions. We are very positive and constructive,” the official said.

The official said India was committed to trade facilitation, but that its concerns about a lack of progress on food subsidies must be addressed. He also said India was prepared to miss the July 31 deadline if necessary.

Diplomats in Geneva said they were pessimistic that Kerry’s trip could provide a breakthrough.

“Historic transformation”

Kerry arrived in New Delhi this evening for an annual strategic dialogue, the first engagement with the government of Prime Minister Narendra Modi that took power in May promising to put India back on a high growth path.

So far, the new administration has moved slowly on reforms, and its hardening stance at the WTO suggests a more nationalist response on key issues than the Congress party’s centre-left government.

Kerry said India and the United States were on the cusp of a “historic transformation” in their relationship and that Washington stood behind New Delhi’s rise as a political and economic power.

“We will work hand in hand with Prime Minister Narendra Modi and his government to promote open and liberal trade and investment, jobs training and closer strategic ties,” Kerry and Pritzker wrote.

Such rhetoric, including calls to build India as a counterweight to China, have fallen short of concrete progress in the past.

Disputes over protectionism and intellectual property rights have soured the business climate and India has remained cautious about committing to US strategic designs in the region.

The relationship took a dive last year after an Indian diplomat was arrested in New York on charges of mistreating her domestic help, an episode that provoked outrage and resentment in New Delhi.

Modi himself has yet to make clear how closely he plans to work with Washington.

He was banned from visiting the United States after Hindu mobs killed more than 1,000 people, most of them Muslims, in 2002 while he was chief minister of his home state of Gujarat.

The Obama administration sought to turn a new page by quickly inviting Modi to Washington after his election, and he is due to travel in September. — Reuters