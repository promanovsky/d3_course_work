Let’s just all agree that we wish we could be as cool as the characters on Game of Thrones. Season 4 premieres in all its sword-fighting glory.

Photo credit: HBO

As if we had any choice but to love the Season 4 premiere of Game of Thrones, let’s be real — it was everything we wanted and more. We saw from all the big characters except Davos and Bran, but we’re not complaining. The show used every minute of its premiere to the fullest.

ICYMI Season 3 finale…

Daenerys liberates the slave city and is loved by all.

Bran, Jojen, Mira and Hodor are sent beyond the wall by Samwell.

Jon Snow is hit with arrows by Ygritte because he decides to return to Knight’s Watch.

Jamie returned to King’s Landing with Brienne of Tarth.

The Lannisters and Sansa

Let’s start in Casterly Rock where Joffrey and Margery’s big wedding is the hot topic. On the surface, that is. Underneath the supposed merriment, the politics are at play.

When did we go from hating Jaime to loving him? While that key moment may be unclear, we do know it has a lot to do with Brienne, who’s sticking around to make sure Jaime keeps his promise to Catelyn to protect Sansa and Arya. We love her. We love him. We’ll keep them together, thank you very much Game of Thrones. If only the show were so simple, right?

Anyways, if Jaime thinks he’s got it bad, Sansa’s got it worse. Her family is dead (so she thinks), and she’s married to the enemy. But Tyrion is far from the worst thing to happen to her, if we say so. She just hasn’t realized it yet. Though how anyone can not be in love with Tyrion is beyond us.

Oberyn Martell

In the midst of the drama, Oberyn Martell arrives from Dorne to represent his family at the upcoming royal wedding. If you thought Tyrion was bad with the prostitutes, Oberyn is far worse as we discover in this episode. He likes his lovers. Not as much as he likes the idea of revenge, though.

Lannisters beware.

Daenerys

On to Daenerys who is back on the road with her army. She’s learning the hard lesson that her dragons can’t be tamed, especially now that they’re about three times her size. The more dragons we see, the better. They upped the Game of Thrones budget this season, didn’t they?

Someone who can be tamed, on the other hand, is Daario, who is eagerly at the queen’s beck and call. Actor Michiel Huisman is taking over the role this season after a recast. At first, we were skeptical. We had already grown fond of Ed Skrein, who played Daario in Season 3, and weren’t excited to see someone new take his place. But Huisman easily fills the void and after a few scenes with him and Dani together, we get it. The chemistry is there folks.

Jon Snow and The Wildlings

On the other side of the Game of Thrones world, Ygritte and The Wildlings have some company in the form of a new tribe of hairless men. They’re all strong and pretty intimidating looking. Plus, they have a hawk. Sounds like a pretty cool tribe until we discover they’re cannibals. We’d still take them over Ygritte’s arrows any day.

Speaking of her arrows, let’s flash back to the end of last season when she hits Jon Snow with three. The show is quick to point out during the Season 4 premiere, Ygritte is an amazing shot. She wouldn’t have missed killing Jon Snow unless it was intentional, meaning girl left him alive and still has feels for him. Duh. It just leaves us feeling really bad for both of them because while she’s whittling away at her arrows, he’s back at The Wall confessing all the gory details about his time with The Wildlings to the Night’s Watch. No detail is spared, including Ygritte.

Arya and The Hound

Of course, we have to end with Arya, who gets the bada** award tonight after she retrieves her sword Needle, which is long overdue. During her travels with The Hound they stumble on good old Polliver, the Lannister man that killed one of Arya’s friends and took Needle. It’s time for a little revenge Arya and Hound style.

Polliver had teased Arya about the size of her sword when he stole it from her by saying, “Maybe I’ll pick my teeth with it.” As Arya is stabbing Polliver in the throat with the same sword she repeats the line back to him.

What did you think of the Game of Thrones Season 4 premiere?