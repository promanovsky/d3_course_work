Excessive alcohol consumption remains a leading cause of premature death in the United States, responsible for 1 in 10 deaths among working-age adults, according to a new study from the Centers for Disease Control and Prevention (CDC).

Researchers used the CDC's Alcohol-Related Disease Impact (ARDI) online application to estimate total number of deaths that were attributable to alcohol among U.S. adults ages 20 to 64, from 2006 through 2010. They also examined years of potential life lost across the U.S. by gender and age.

Excessive alcohol use led to nearly 88,000 deaths per year over the study period, and shortened the lives of those who died by about 30 years on average, said study researcher Dafna Kanny of the CDC. "In total, there were 2.5 million years of potential life lost each year due to excessive alcohol use," she said.

The number of alcohol-related deaths have increased by around 12,000 since 2004, the year a previous CDC study was conducted, Kanny said. [7 Ways Alcohol Affects Your Health]

The study appears today (June 26) in the CDC's journal Preventing Chronic Disease.

Causes of death

Alcohol is linked with deaths both from acute causes, such as car accidents and falls, and deaths from chronic diseases. Deaths due to alcohol-related car accidents reached nearly 13,000 per year during the study period, according to the report.

Among the chronic diseases that alcohol can cause, the most common cause of death was alcoholic liver disease (14,000 deaths yearly), followed by liver cirrhosis (7,800 deaths yearly) and alcohol dependence syndrome (3,700 deaths yearly), according to the study.

Most of the deaths that were related to excessive alcohol consumption (71 percent) involved males, Kanny said. "Men are more likely than women to drink excessively, especially binge drinking, having five or more alcoholic drinks in one occasion," she said. "Among drivers in fatal motor vehicle crashes, men are almost twice as likely as women to have been intoxicated."

According to the study, New Mexico had the highest number of alcohol-related deaths, with approximately 51 deaths yearly per 100,000 people, while New Jersey had the lowest, with around 19 deaths per 100,000 people.

What can be done

The results of this study are consistent with similar studies conducted on a global level, and reflect the substantial effect that excessive drinking has on life span and loss of productivity, Kanny said. Premature deaths due to alcohol consumption, along with the reduced earnings by heavy drinkers, were responsible for 72 percent of the estimated $223.5 billion in economic costs due to excessive alcohol consumption in the U.S. in 2006, she said.

Kanny offered suggestions on what can be done to decrease the number of fatalities due to excessive drinking. "Health care providers can use alcohol screening and counseling to help people who are drinking too much," she said. "Adults can set a good example for young people by not drinking excessively, and by not providing underage youth with alcohol."

Other strategies, she added, include increasing taxes on alcoholic beverages, reducing the number of retails outlets that sell alcoholic drinks, and holding alcohol retailers liable for injuries and damage following illegal service to intoxicated or underage consumers.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.