Online Dating Site Zoosk Files For $100M IPO, With $178M In Revenues And A $2.6M Net Loss In 2013

Online dating site Zoosk just filed its S-1 registration statement with the SEC, announcing its plans to raise $100 million in an initial public offering. The company, which has offers up a website and apps for daters around the world to find each other, posted a net loss of $2.6 million in 2013 on revenues of $178 million.

Zoosk uses big data and a bit of algorithmic recommendations technology to help its users find partners. With a “proprietary Behavioral Matchmaking engine,” the company learns from clicks, messaging, and other actions of its members to produce matches that are supposed to find better matches. The idea is that the more data it has, the better its recommendations get.

As part of the filing, Zoosk provided details of its global business, which includes 26 million members worldwide and 650,000 paying subscribers across 80 countries at year-end 2013. That’s up from the 18 million registered members and 483,000 paying subscribers a year earlier.

While it started out on the web, the company has been successful in capturing mobile users, as it’s reached the position of #1 grossing dating app and a top 25 grossing app on the Apple App Store.

At the same time its subscriber numbers have grown, revenues increased 63 percent from 2012 to 2013, rising from $109.1 million to $178.2 during that period. Its net loss also decreased significantly, from $20.7 million in 2012 to $2.6 million at the end of 2013.

While subscriptions accounted for $153.8 million of its revenues in 2013, Zoosk also has a rapidly growing virtual currency business, which increased from $5.4 million in 2012 to $24.3 million a year later.

BofA Merrill Lynch, Citigroup, and RBC Capital Markets are acting as joint bookrunning managers for the proposed offering, with Oppenheimer & Co. and William Blair acting as co-managers.

Zoosk had raised $60 million since being founded in 2007. Investors include Crosslink Capital, Keating Capital, Canaan Partners, Bessemer Ventures, ATA Ventures, Jeff Epstein, Plug & Play Ventures, and Amidzad Partners.