Hackers have stolen more than 650,000 passwords from Domino's Pizza customers in France and Belgium, and are now holding the pizza chain franchise at ransom, UK MailOnline reported.

After gaining access to a vulnerable customer database shared by the pizza firm's European headquarters, the group, known as Rex Mundi, revealed the information in a post to dpaste.de.

The group has demanded €30,000 ($40,714) from the company by 8pm CEST (7pm BST) Monday or it will begin publishing the details online.

"It claims to have downloaded more than 592,000 customer records, including passwords from French customers, and over 58,000 records from Belgian ones," according to UK MailOnline reported. "Other stolen details include customers' full names, addresses, phone numbers, email addresses, passwords and delivery instructions."

"The group even admitted to stealing details of the customer's favorite pizza topping."

The alleged hack was publicized by Rex Mundi on Twitter, with customers being advised to speak to their lawyers.

"Earlier this week, we hacked our way into the servers of Domino's Pizza France and Belgium, who happen to share the same vulnerable database," Rex Mundi said in a message.

"And boy, did we find some juicy stuff in there! We downloaded over 592,000 customer records (including passwords) from French customers and over 58,000 records from Belgian ones."

Domino's France issued a statement on Twitter saying that although its data is encrypted, it has fallen victim to "professionals" who were able to "decode the cryptographic system for the passwords."

However, demands for ransom would not be paid and none of the stressed financial data had been compromised, Andre ten Wolde, Domino's Pizza executive, told a Dutch newspaper.

Customers of the pizza company are being advised to change their passwords "as a security measure." The French police have also been reported about the breach.

"The data hacking is isolated to the Domino's franchise in France and Belgium, and no customer credit card or financial information was compromised," a spokesman for Domino's Pizza Group said.

"Domino's customers in the UK and Republic of Ireland are not affected by this incident. The security of customer information is very important to us. We regularly test our UK website for penetration as part of the ongoing rigorous checks and continual routine maintenance of our online operations."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.