Taylor Swift made a surprise visit to hang out with some very sick young fans for an hour at the New York Hospital Sloan-Kettering Cancer Center, but ended up spending over five hours bringing joy to suffering young children and their families. Watch a video from her visit inside.

Taylor Swift took time out of her busy schedule to make a special trip to New York Hospital to visit a handful of fans that are suffering from cancer. One family recorded Taylor’s entire visit so that the world could see what a kind hearted young lady she is.

Taylor Swift Visits Sick Fans

On Saturday, March 22, Taylor Swift was scheduled to stop at the New York Hospital Sloan-Kettering Cancer Center for an hour long visit. Instead, Taylor ended up spending over 5 hours visiting children all over the hospital.

Taylor paid a visit to the pediatric floor, where multiple young children are being treated for a variety of different cancers. We can’t even imagine what it’s like to be on that floor, witnessing so many children with their whole lives ahead of them suffering from such a heinous disease.

Taylor is one incredible lady for not only making time to visit these sick children, but for spending so much time with them and making them so happy.

Shelby Huff In ‘Awe’ After Taylor’s Visit

One fan that got a visit from Taylor is Shelby Huff, a young girl suffering from severe aplastic anemia.

Aplastic anemia results from injury to the blood stem cells, causing a decrease in the number of every type of blood cell. Shelby has already received a bone marrow transplant, and is still receiving different types of blood cell treatment as time goes on.

Shelby’s family recorded Taylor’s entire visit, and it’s like the two have been best friends for a very long time. Taylor gives Shelby advice for her guitar lessons, and Shelby shows off all of the posters of Taylor she was up in her hospital room. Taylor even took time to talk to Shelby’s little sister, Avalon, who Skyped in during the visit.

Tell us, HollywoodLifers — Are you a Taylor Swift fan? What do you think about her visiting sick fans? Let us know your thoughts below!

— Lauren Cox

Follow @Iaurencox

More Taylor Swift News: