FLORIDA, July 31 ― Florida health officials yesterday issued formal warnings to beachgoers about a potentially deadly flesh-eating bacteria that is lurking in the Sunshine State’s warm coastal waters.

The warning came after 11 people contracted infections from the bacteria Vibrio vulnificus in the state. Two of the people have died.

V. vulnificus can infect people either via open wounds or by being ingested. In the case of infections via open wounds, the bacteria causes skin breakdown and ulceration. ― Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

V. vulnificus can infect people either via open wounds or by being ingested. In the case of infections via open wounds, the bacteria causes skin breakdown and ulceration. In the case of ingestion, common symptoms include cause vomiting, diarrhea and abdominal pain.

According to the US Centres for Disease Control and Prevention (CDC), the bacteria is rarely a cause of death and is generally more fatal to people who have compromised immune systems and those who have liver problems such as hepatitis. In people with immunocompromising states, the bacteria could attack the bloodstream and cause redness in the limbs with blood-tinged blisters, fever or chill, low blood pressure, septic shock and even death.

Sometimes aggressive surgical treatment including amputation is needed to prevent death. ― Reuters