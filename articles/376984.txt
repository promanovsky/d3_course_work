The Bank of Japan (BOJ) on Tuesday stuck to its stimulus program and projections that inflation will reach its 2 percent target next year, despite recent downbeat data that reignited concerns over the world's third-biggest economy.

The central bank also cut its growth projection for the current financial year to 1.0 percent, from 1.1 percent, but kept its 2015 and 2016 growth targets intact at 1.9 percent and 2.1 percent, respectively.

The BOJ maintained its pledge to increase base money by 60-70 trillion yen ($592-$691 billion) per year via aggressive asset purchases, mostly in Japanese government bonds.

Read MoreWhat happened to Japan's yen-driven export boom?



The widely expected decision comes even as calls grow louder for BOJ to take action amid weak exports and poor household spending after a sales tax hike took effect in April.

"There was the tax hike at the start of April which really threw out all of the data - all the inflation, confidence and spending figures - so it's a little tricky at the moment to get an exact fix on how the economy is running," Glenn Levine, Senior Economist at Moody's Analytics told CNBC.



"The central bank tends to think that underlying inflation is running at one and a quarter percent. That feels about right but that's well short of their 2 percent inflation target and it's going to be awhile to hit the 2 percent at a sustainable basis," he added.