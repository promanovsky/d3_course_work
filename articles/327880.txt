Asus is reportedly working on its own wearable device, a smartwatch running Android Wear, which will be launching in September.

Techcrunch, citing sources familiar with Asus' plans, has claimed that the Taiwanese giant will launch an Android Wear smartwatch at a price point targeted to be below the LG G Watch (Rs. 14,999) and Samsung Gear Live (Rs. 15,900).

The report suggested that the company is setting a September launch for the unnamed wearable device and will pack an AMOLED display.

Further, the report goes on to claim that Asus might price its Android Wear smartwatch between $99 (roughly Rs. 6,000) and $149 (roughly Rs. 9,000) at launch.

The decision to wait for a later release of the Asus smartwatch was reportedly taken to avoid a "rush" in efforts. The report notes that LG, during the unveiling of the G Watch, had given time constraint reason for not including heart rate monitor in the device, which clearly indicated that the Mountain View giant must have set an Android Wear release window for the company.

As of now, there is no word on specifications of the unnamed Asus' Android Wear smartwatch.

Asus, earlier this year, was said to be working on a smartwatch that would feature a customisable gesture interface and voice-commands. However, this was before Google revealed its Android Wear plans.

Soon after Google's I/O, the LG G Watch, initially unveiled in March, was up for pre-order with 'Add to Cart' option at Rs. 14,999 in India. Meanwhile, one of the biggest news at I/O was Samsung joining forces with Google to launch its first Android Wear-based smartwatch, Gear Live.

The Samsung Gear Live, on the other hand, was listed on Google Play at Rs. 15,900 with a 'coming soon' tag. The other Android Wear smartwatch we know about, the Moto 360, will only be available later in the American Summer.