The vast collusion between technology companies to prevent poaching and hiring among themselves, thereby limiting fair-market wages for the workers in question, has a new wrinkle today: Facebook refused to play ball, and we have a statement to that effect.

A filing widely reported today includes an anecdote from Facebook COO Sheryl Sandberg, indicating that Google once approached her to propose a détente of sorts between the firms. Here’s Sandberg on the how the ask came to be:

“In or about August 2008, I was contacted by Jonathan Rosenberg, who was then at Google. Mr. Rosenberg expressed concern about what he described as the perceived rate at which Facebook could hire employees from Google. Around the same time, I also discussed a similar topic with Omid Kordestani, who was also at Google. I declined at that time to limit Facebook’s recruitment or hiring of Google employees. Nor have I made or authorized any such agreement between Facebook and Google since that time.”

In short, while other companies were being bullied by Apple and others to stop picking up talent from each other, Facebook wasn’t willing to participate.

It’s worth noting that Facebook was hotter than the sun back in 2008, so for it to continue to recruit from other firms could could have been a reflection of its market position; if you are doing the poaching, why would you disarm?

Just how annoyed Google was concerning Facebook stealing its staff? From a separate filing:

So that’s that. Sandberg wasn’t having it. The above appears to corroborate her own telling of the story.

And what happens next is precisely what is supposed to happen when companies don’t collude to defraud their employees of fair-market wages: Google coughed up more money to improve its retention.

Here’s the very next paragraph of the filing screenshotted above:

Well then!

The gist here is simple: The pervasive attempt by large tech companies to suppress wages by diminishing their employees’ rights to choose where they work is not merely selfish in the extreme, but is counter to the very market principles that made these companies so successful to begin with.

You can’t just free market part of the time. Good on Facebook for not being party to the shenanigans.