Researchers have created a dialysis machine that can work on even the smallest babies.

The continuous renal replacement therapy (CRRT) machine, dubbed CARPEDIEM, could help babies suffering from renal conditions, a Morning Post Exchange news release reported.

"Such modifications make adult devices inaccurate when used in infants smaller than 15 [kilograms] and can result in complications with fluid management and treatment delivery," lead author Professor Claudio Ronco from San Bortolo Hospital in Vicenza, Italy, said in the news release. "A major problem is the potential for errors in ultrafiltration volumes-adult dialysis equipment has a tendency to either withdraw too much fluid from a child, leading to dehydration and loss of blood pressure, or too little fluid, leading to high blood pressure and edema."

The researchers developed a device for kidney support that would be effective in infants as small as 2 kg.

In August of 2013 an Italian newborn baby who weighed only 2.9 kg was the first human to be treated with the new device after experiencing the failure of multiple organs following a difficult delivery.

After only 20 days the medical staff was able to discontinue renal support and organ function was restored within 50 days.

About 18 percent of low-birth weight infants experience kidney damage or injury and about 20 percent of children admitted to the hospital have kidney problems.

"We have shown how the technical challenges of providing CRRT can be overcome without relying on the adaptation of technology used in adult settings, and that a CRRT device designed specifically for use in neonates and small children can be used to safely and effectively treat acute kidney injury in small [pediatric] patients. We hope that our success will encourage the development of other medical technologies (eg, catheters, fluids, and monitors) specifically designed for infants and small children," Ronco said.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.