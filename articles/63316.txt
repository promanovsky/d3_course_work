Last week, the Seattle Police Department released two unseen photos of the scene of Kurt Cobain’s suicide. After intense public interest in the images, the department has published 35 additional photos from the scene.

The original two images, though haunting, didn’t reveal much new information about the incident. These new photos, however, might add visual detail for fans interested in Cobain’s last hours.

They contain shots of Cobain’s suicide note in its original location (inside his greenhouse, on top of a planter and held down by a pen). There’s also a close-up shot of Cobain’s wallet where his face is clearly visible on a driver’s license.

PHOTOS: Iconic rock guitars and their owners

Advertisement

Other pictures show investigating officers on the scene and more details of Cobain’s drug paraphernalia. In the photos, glass windows of Cobain’s home are covered up to prevent onlookers from peering in.

The images are a mix of previously undeveloped 35-millimeter shots (whose green tint suggest significant aging) and Polaroids.

The Seattle police released the images after its cold case department decided to re-examine evidence on occasion of the case’s 20th anniversary. The case remains closed and the department has said it is not being reopened.

ALSO:

Advertisement

The VIP treatment at Britney Spears’ Vegas show

Review: Kylie Minogue and Enrique Iglesias sex it up on new albums

Matt Berninger of the National and his brother Tom talk ‘Mistaken for Strangers’