Massachusetts Juvenile Court Judge Joseph Johnston injured every parent’s rights Tuesday when he awarded permanent custody of teenager Justina Pelletier to the state.

Now, the Department of Children and Families (DCF) will decide when and whether she is returned home.

[pullquote]

Justina has been held captive by Boston’s Children’s Hospital and DCF because they believe that her parents have kindled in her a psychiatric disorder that makes her believe she has physical symptoms, including difficulty walking. They insist no physical condition can explain her symptoms.

However, doctors at New England Medical Center in Boston disagree. They evaluated Justina and concluded she suffers with a rare illness called mitochondrial disease.

Full disclosure: I am an assistant professor at Tufts Medical School, which is affiliated with New England Medical Center. But you can be assured I have no bias, because I have gone to the mat debating other issues with New England Medical Center in the past.

There is no way to immediately prove who is right and who is wrong about the diagnosis. But that’s part of the problem with being called mentally ill. Once someone in authority asserts that you are, and insists on framing your behavior according to that theory, it can be tough to prove you are not.

Russia has been expert at doing that with dissidents, who were left to languish in locked psychiatric facilities for their “delusional” ideas.

So, in Massachusetts, if Boston Children’s Hospital wants to say that your child’s mysterious physical symptoms are ones you are causing by coaxing your child to act sick, then you, too, could lose custody of your child to the state. And it won’t matter if academic doctors at another hospital disagree and believe that your child is suffering from a genuine bodily illness.

What’s really interesting here is that Children’s Hospital and the Commonwealth of Massachusetts are doing precisely to Justina, for sure, what they theorize her parents were doing: They have deprived Justina of her rights. They have made of her a permanent patient. They have decided that her theoretical psychiatric disorder should change the course of her entire life. And they have done it, despite other experts telling them they are just plain wrong.

By the way, Children’s Hospital is also home to Norman Spack, the endocrinologist who began his career using hormones to change the gender of tadpoles and went on to use massive doses of hormones to manipulate the gender characteristics of children—sometimes coaxing parents to go forward with the treatment, when they had serious misgivings.

Bottom line: Unless your child has a simple infection, and there’s absolutely no way anyone could ever argue that you injected your kid with dirt to cause it, keep your parental rights intact and keep driving right past Boston’s Children’s Hospital. Until the hospital examines its desire to make the most dramatic decisions possible for parents, rather than with parents, there are other very good options for your child’s medical care.