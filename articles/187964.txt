

Merck announced a $14.2 billion sale of its consumer health unit to focus on other growth areas, including a possible blockbuster cancer drug. (Emile Wamsteker/Bloomberg)

Merck says its $14.2 billion sale of its consumer health unit to Bayer will allow the company to focus investments elsewhere, specifically naming an experimental cancer-fighting drug as an example for future growth for the company.

The Merck deal follows on the sale of other cancer drugs, including Novartis's deal with GlaxoSmithKline, and Pfizer's ongoing pursuit of AstraZeneca, which has a notable stable of cancer drugs. The timing of Merck’s sale, announced Tuesday, also comes as new research shows just how lucrative the area is.

In the last decade, the average cost of a brand-name cancer drug jumped from about $5,000 per month to $10,000 per month in 2013, according to a new IMS Institute for Healthcare Informatics study out this morning. Oncology, by far, remains the largest area of focus in drug research and development, IMS said.

Patients are very likely to feel the squeeze from higher drug costs. Looking at the experience of adjuvant hormonal therapy for breast cancer patients, IMS research shows that patients are more likely to drop treatment as their personal costs increased.



(IMS Institute for Healthcare Informatics)

IMS reports that the global market for cancer drugs reached $91 billion in 2013, while the compound annual growth rate has actually slowed to 5.4 percent during the past five years, compared to 14.2 percent between 2003 and 2008. It partially attributes the slower growth to fewer breakthrough therapies for very large patient populations and expiring patents, among some other reasons.

The study also attributes the rise in spending on cancer medications toward the increasing shift away from small practices toward hospital outpatient facilities. With higher overhead, the outpatient facilities get higher reimbursements for delivering the same drug.

The American Society of Clinical Oncology in the past few weeks has taken a couple of major steps to address the rising cost of cancer drugs. It outlined a plan last month to rate drugs’ cost-effectiveness, and the group said it will urge oncologists to speak with patients about weighing the benefits of a drug against its cost. And on Monday, ASCO proposed a bundled payment reform that would reimburse cancer doctors with a set payment for a certain set of services. The idea is to reward doctors for quality of services, not the volume.