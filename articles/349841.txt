Wilocity

Qualcomm on Wednesday announced it bought startup chipmaker Wilocity to help it provide a speedier, but shorter-distance flavor of wireless technology in more devices.

Wilocity, based in Sunnyvale, Calif., and founded in 2007, develops chips using a newer type of Wi-Fi called WiGig that runs on a relatively high radio frequency, at 60 gigahertz. WiGig, an industrywide standard backed by the Wi-Fi Alliance industry group, offers speeds several times faster than traditional Wi-Fi, but is only effective at short distances, so could work well within a single room.

Because of its speedy connection, WiGig is seen as a potential replacement for physical cables still necessary to connect devices such as tablets or PCs to large displays or televisions. In stating its goal to eliminate cables from all PCs by 2016, Intel pointed to WiGig as one of the potential answers. Qualcomm likewise recognizes its importance.

"We and our partners believe in this technology strongly," Qualcomm executive Cormac Conroy told CNET in an interview Wednesday. "We think it's important to the industry."

The company declined to disclose financial details of the deal.

The capabilities of Wilocity's chips make them useful for heavy-lifting functions that can take place in a small area, such as streaming 4K video from a smartphone to a television with minimal lag, or wirelessly docking a tablet to a large display, keyboard, and mouse.

It wouldn't be useful for transferring the video across a house or large office, like you're able to with a traditional Wi-Fi network, which boasts a longer range, but much lower speeds.

The zippier WiGig technology could create islands of "extreme performance," said Tal Tamir, the chief executive of Wilocity who joined Qualcomm as a product management executive. For instance, Wilocity's chips enable speeds of 4 to 5 gigabits per second, while traditional Wi-Fi speeds are counted in hundreds of megabits per second, Qualcomm's Conroy said.

Conroy added that his company plans to expand the use of Wilocity's technology into mobile, computing, and networking products. He said Qualcomm will combine Wi-Fi and WiGig signals into some of its Snapdragon mobile-device offerings starting in the first half of next year, in hopes of having the chips integrated into new tablets and smartphones when they come out in the second half of 2015.

"I think this acquisition really puts WiGig into overdrive," said Patrick Moorhead, founder and president of the Moor Insights & Strategy analyst firm. While Wilocity as a startup could pursue only a handful of projects, he added, "With Qualcomm's backing, I think we're going to see a huge acceleration here."

Qualcomm, which holds a dominant position in mobile chips, has been investing in and partnered with Wilocity for years. Last year, Qualcomm introduced a wireless module built into Dell Latitude Ultrabooks that pairs Wilocity's WiGig technology with two traditional types of Wi-Fi. The combination allows for longer-range Wi-Fi connections in certain instances and higher-speed but shorter-range WiGig data transfers in others.

"We've had an ongoing partnership for a number of years, and we see the acquisition as the culmination of that," Conroy said.

The acquisition also fits into Qualcomm's push to boost its Wi-Fi offerings in recent years, since it significantly added to that effort when it bought Wi-Fi chipmaker Atheros for about $3.1 billion in early 2011. Qualcomm shares closed up 44 cents to $80.17 in Wednesday trading.

Tamir said he wanted others to see the deal as "more than an acquisition," saying it "shows industry momentum around this technology."

While Wilocity is based in California, most of its employees are in Israel. Conroy said Qualcomm plans to maintain Wilocity's presence there and add its staff to Qualcomm's engineering team already in the country.