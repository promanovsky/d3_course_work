NORTH TEXAS (CBS 11 NEWS) – Gone before you get to enjoy it — it’s not your imagination! Paychecks are disappearing faster these days, with increasing bills sucking them up.

For the fourth month in a row the U.S. Department of Labor reports an increase in the Consumer Price index (CPI) for Dallas-Fort Worth. The data, released on Tuesday, shows that prices rose .4-percent in April and May, and 1.8-percent over the last year.

Energy was the largest index in the equation: up 3.5-percent in April and May, following a 6.3-percent increase in February and March. Higher prices for both electricity and gasoline were the biggest factors for energy CPI, up 5.3-percent and 2.9-percent, respectively.

Consumers are noticing the change.

“I drive to work, and back home. My wife’s a schoolteacher. I figure it takes me $20 to $25 a day in gas to get from work to home,” said Roy Price.

Price is a father of five, whose children range in age from preschool, to college graduate. He says he feels he’s taking more out of his paycheck for everyday spending. “Where it goes, I don’t know.”

Economists like Dr. Bernard Weinstein, the Associate Director for the Maguire Energy Institute at SMU’s Cox School of Business, say spending isn’t slowing among consumers. “When consumers go out and spend, businesses have more pricing power. They can push prices up a little bit.”

Dr. Weinstein says the change to the economy would be significant if we were to maintain the current rate of inflation, because the rate rise is higher than what people receive for rises in wages. But he expects the CPI to level off in the coming months.

He also reminds that North Texas is still considered an affordable place to live, when compared to many other large cities in the country. “Remember the context. We had three to four years where consumer prices hardly changed at all. Now the economy is getting better and we’re seeing some inflation. A little inflation is a good thing. What you don’t want is a high and persistent inflation.”

Some businesses are feeling the effects of rising prices as well. Fuel City, the ranch-like gas station just south of downtown Dallas, is packed most times of day.

Tuesday evening, the price board read $3.47 for unleaded; about $.05 cheaper than the Dallas average at the time. Manager Eric Kotanchik said, “Prices can really fluctuate for gas stations.”

Kotanchik said setting a price that will be both profitable and attractive to customers is a fine balance.

“That can affect our margin,” he said. “Sometimes we’re making hardly any money, and sometimes we’re losing money on gas, because we’re trying to keep the price so low for everyone.”

According to Dr. Weinstein, the price of fuel is a complicated piece of the Consumer Price Index. He says influencers include everything from what season it is, to stability in oil rich countries.

(©2014 CBS Local Media, a division of CBS Radio Inc. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: