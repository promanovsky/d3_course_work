LOS ANGELES -- Results from Microsoft's first quarterly earnings release under new CEO Satya Nadella offered fresh justification for his focus on cloud computing.

Microsoft Corp. on Thursday posted net income and revenue that beat Wall Street expectations.

Factoring out special items from a year ago, revenue in the January-March quarter grew 8 per cent.

Nadella, who replaced longtime CEO Steve Ballmer in February, touted the fact that revenue from cloud-based Office 365 productivity software more than doubled from commercial customers, while revenue from its cloud computing service Azure grew more than 150 per cent.

"This is gold rush time in being able to capitalize on the opportunity," Nadella said.

Redmond, Wash.-based Microsoft's stock rose 76 cents, or 1.9 per cent, to $40.62 in after-hours trading. That followed a 17-cent gain to $39.86 in the regular session.

"It's a good first quarter of earnings for Satya out of the box," said analyst Shannon Cross of Cross Research. "This was enough to offset concerns that investors might have had that growth was slowing or the impact Amazon or other competitors might be having on the cloud strategy."

The company also benefited from the end of support for its 13-year-old operating system, Windows XP, on April 8. Many companies paid to upgrade computers en masse to make sure they'd continue to receive support from Microsoft, including anti-virus updates.

That transition -- which contributed to a 4 per cent increase in Windows revenue-- helped Microsoft buck a 2 to 4 per cent decline in personal computer shipments in the quarter as estimated by market research firms Gartner and IDC.

Chris Suh, Microsoft's general manager of investor relations, said it was hard to tell how much Windows revenue benefited from the end of XP. But he said about 10 per cent of all Windows computers are still running XP, meaning the company would likely continue to benefit going forward as the remaining users pay to upgrade or buy new computers running newer versions of Windows. "It's probably not done. There's a bit of a long tail that will drag out over a long period of time," he said in an interview.

Microsoft also said it would close on its $7.3 billion acquisition of Nokia's handset business on Friday, but declined to account for it in its outlook. Analysts expect the business to hurt Microsoft's profit. Nokia had said in January that its smartphone sales slumped 29 per cent in the October-December quarter.

Overall, Microsoft posted flat revenue and a decline in net income.

Net income came to $5.66 billion, or 68 cents per share, down from $6.06 billion, or 72 cents per share, in the same period a year ago.

Revenue was nearly unchanged at $20.40 billion, compared with $20.49 billion a year ago. Last year's revenue got a temporary boost from upgrade offers revenue and other one-time items.

Analysts polled by FactSet expected Microsoft to post earnings of 63 cents per share on revenue of $20.39 billion.

Revenue from devices and consumer products rose 12 per cent to $8.30 billion. The company sold 1.2 million more Xbox One game consoles, bringing the total to 5.1 million sold. That's behind the 7 million PlayStation 4s sold by rival Sony Corp. Both came out in November.

Commercial revenue grew 7 per cent to $12.23 billion, as revenue grew from its Office 365 productivity software. Microsoft is transitioning customers to pay for an annual subscription, instead of having them update older versions once every few years. That's meant to smooth out and raise revenue over time.

Nadella told investors that Office 365 was "the core engine driving a lot of our cloud." Microsoft is increasingly tailoring Office programs like Excel to integrate better with large pools of data that companies collect on remote servers so they can be accessed over the Internet.