Beards are considered more attractive when they are rare, but as they become more common they will be considered less attractive, new research suggests.

The findings suggest evolutionary forces could be playing a role in the rise and fall of beards and other facial hair patterns on men, according to the study published today in the Royal Society journal Biology Letters.

"In a world full of bearded men, beards are less sexy than they are in a world full of clean-shaven men," says evolutionary biologist Rob Brooks of the University of New South Wales.

Brooks and colleagues set up the experiment to find out whether fashion in men's facial hair might vary in the same way some genes and traits in evolution do.The conclusions come from an online experiment in which men and women rated images of faces with different kinds of facial hair.

Specifically they were interested in looking at what's called 'negative frequency dependence' which is a form of selection pressure on genes.

"When a gene or trait is rare it experiences an advantage," says Brooks. "But if it is too common there is a disadvantage."

Take orchids that mimic female insects, tricking male insects into mating with them, to get pollinated.

If the flowers are too successful at this strategy, sooner or later the male insects wise up to the strategy and stop pollinating the flowers: a case of negative frequency-dependent selection.

In a similar way, says Brooks, when too many people start following a fashion trend, the less valuable it becomes, and what was once considered trendy, starts to decline in popularity.

"There is a currency to rarity and we thought that might apply to facial hair."

Online experiment

Brooks and colleagues got men and women to rate online images of men's faces that were either clean shaven, or adorned by a 5-day growth, 10-day growth, or full beard.

The study participants were first shown 24 faces to 'set the scene'. These 24 faces either had an even spread of all four types of facial hair, or were all clean shaven or all full beards.

Researchers found that both women and men thought heavy stubble and full beards more attractive when these facial hair patterns were rare. (Mike Mozart/Flickr)

Then the participants were shown a standard set of 12 faces that had an even spread of the four facial hair types.

Brooks and colleagues found that both women and men thought heavy stubble and full beards more attractive when these facial patterns were rare during the scene-setting period.

Similarly, clean-shaven faces were judged more attractive when they were rare and less attractive when they were common.

Facial hair patterns were judged as having intermediate attractiveness when they were presented with intermediate frequencies during the scene setting.

Role of evolution

The researchers say the findings suggest negative frequency-dependent preferences towards male facial hair and suggest evolutionary forces may be playing a role in the popularity of beards.

How exactly evolution is working is unclear but he says negative frequency-dependent preferences seem particularly strong for beards, compared to hair colour and body size, which he has tested more recently.

Brooks says while the culture of fashion may be playing a role in facial hair, this does not preclude a role for evolution as well.

"The idea that something like this is either 'evolutionary or cultural' is a hackneyed old way of thinking that should have died in the 20th Century," he says. "What is cultural is deeply evolutionary and a lot of evolutionary changes happen through cultural mechanisms."