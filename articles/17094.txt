Third, we need to revisit the cockpit door issue. Reinforcing the door was a good idea to keep out 9/11-style hijackers, but undermined by the door being opened for toilet needs, refreshment breaks, and crew handovers on long-haul flights. Someone on the cockpit side of the reinforced door can now keep out anyone trying to gain access to the cockpit for the right reasons. This happened as recently as last month, when the co-pilot of an Ethiopian Airlines plane flying from Addis Ababa to Rome locked the pilot out of the cockpit and flew the plane to Switzerland to seek asylum.

Fourth is the mental health of the pilots, which should be regularly reviewed – possibly every six months. I have heard from pilots that flying long distance on autopilot is excruciatingly boring and can cause fixation and stress problems. There appear to be inadequate studies on the effects of frequent long-haul flights on pilots or, for that matter, the health effects on flight crew of regular high-altitude cosmic and solar radiation exposure.

Fifth, no one on an aircraft should be able to disable the ACARS or transponder (as the 9/11 hijackers also did).

Loading

Sixth is the issue of Air Security Officers (ASOs) or flight marshals on international flights. Governments, including the Australian government, have been cutting back funding in this area and it is probable now that not more than 5 per cent of international flights have ASOs on board. An ASO on Flight MH370 could have made a difference – provided, of course, he or she was able to open the cockpit door.