Health officials say the first U.S. patient carrying the potentially deadly MERS respiratory virus is improving rapidly. At a news conference Monday, they said are working on a plan for his discharge from an Indiana hospital, although he may need to continue his isolation at home.

Indiana State Department of Health Commissioner William VanNess II, Governor Mike Pence, and CDC and hospital officials updated the media on the man's condition Monday. They say none of the roughly 50 health care workers or family members who have had contact with the patient have tested positive for the virus. Middle East Respiratory Syndrome, or MERS, has an incubation period of two to 14 days.

The unidentified patient checked into Munster Community Hospital on April 28th after experiencing fever, cough and shortness of breath.

A few days earlier, he had flown to the U.S. from Saudi Arabia, where he lives and is a health care worker at a Riyadh hospital. There was a stopover in London before he landed in Chicago. From Chicago, he took a bus to Indiana to visit family.

Dr. Daniel Feikin, an epidemiologist with the Centers for Disease Control and Prevention, said health officials have contacted about three-fourths of the passengers who were on the man's flights. Ten people were on the bus. No one has reported experiencing similar symptoms.

Saudi Arabia has been at the center of a Middle East outbreak of MERS that began two years ago. The virus has spread among health care workers, most notably at four facilities in that county last spring.

Officials said the patient did not recall having any direct contact with MERS patients in Riyadh, but said the hospital where he worked did have some MERS cases.

MERS belongs to the coronavirus family that includes the common cold and SARS, or severe acute respiratory syndrome, which caused some 800 deaths globally in 2003.

At least 400 cases of MERS have been reported in the past two years, and more than 100 people have died. Officials say it isn't highly contagious, but there is no cure.

The MERS virus has been found in camels, but officials don't know how it is spreading to humans. It can spread from person to person, but officials believe that happens only after close contact. Not all of those exposed to the virus become ill.