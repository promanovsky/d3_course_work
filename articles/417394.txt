Opioid-related overdose deaths are a bleak public health issue in this country.

The percentage of patients who receive opioid prescriptions to treat noncancer pain has almost doubled in the past decade, but the number of overdose-related deaths for women have increased five times as much, according to the Centers for Disease Control and Prevention.

To put the nationwide stats in perspective, more women have died each year from drug overdoses than from motor vehicle-related injuries since 2007. For men in the past decade, the rate of opioid overdose deaths has increased three-fold. According to the CDC, women in particular are more likely to be prescribed opioid pain relievers than men, more likely to use them chronically, and more likely to be prescribed them in higher doses.

Advertisement

But what if medical marijuana, another option for treating chronic pain, could have an impact on these staggering statistics?

Research publised today in JAMA Internal Medicine found that states with medical marijuana laws before 2010 had 24.8 percent lower annual opioid overdose deaths on average when compared to states where medical marijuana was illegal.

Medical cannabis laws were associated in the study with lower overdose mortality rates that generally strengthened over time. In 2010, for instance, researchers noticed there were 1,729 fewer deaths in states where medical marijuana was legal.

The research team, lead by Dr. Marcus A. Bachhuber at the Philadelphia Veterans Affairs Medical Center, examined state medical marijuana laws and opioid overdose deaths using death certificate data from 1999 to 2010.

California, Oregon, and Washington had medical marijuana laws effective before 1999. Alaska, Colorado, Hawaii, Maine, Michigan, Montana, Nevada, New Mexico, Rhode Island, and Vermont had medical marijuana laws effective between 1999 and 2010. Both of these groups of states were included in the research.

Nine states, including Massachusetts, enacted medical marijuana laws after the study concluded in 2010, so data from these states’ recent legislation changes wasn’t analyzed. As of July 2014, 23 states had enacted medical marijuana legislation, primarily to treat chronic or severe pain, but research has not yet been done to analyze whether these states had similar trends.

Advertisement

In Massachusetts, where Governor Deval Patrick has declared a “public health emergency,’’ the number of deaths due to opioid overdoses has increased by 90 percent from 2000 to 2013, according to data from the Massachusetts Department of Public Health. But voters legalized medical marijuana in a November 2012 ballot initiative. Is our state a contradiction?

Researchers warn that more research is necessary before research like theirs inform policy.

“In summary, although we found a lower mean annual rate of opioid analgesic mortality in states with medical cannabis laws, a direct causal link cannot be established,’’ wrote the authors in the study. “If the relationship between medical cannabis laws and opioid analgesic overdose mortality is substantiated in further work, enactment of laws to allow for use of medical cannabis may be advocated as part of a comprehensive package of policies to reduce the population risk of opioid analgesics.’’

In a related review and commentaryof the research, also published in JAMA Internal Medicine, Dr. Marie J. Hayes, Dr. Mark S. Brown write:

“If medical marijuana laws afford a protective effect, it is not clear why. If the decline in opioid analgesic-related overdose deaths is explained, as claimed by the authors, by increased access to medical marijuana as an adjuvant medication for patients taking prescription opioids, does this mean that marijuana provides improved pain control that decreases opioid dosing to safer levels?’’

“The potential protective role of medical marijuana in opioid analgesic-associated mortality and its implication for public policy is a fruitful area for future work,’’ they conclude.