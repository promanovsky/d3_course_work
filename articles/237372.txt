SPRINGFIELD, Mo., May 21 (UPI) -- A worker at a Red Robin restaurant in Springfield, Mo., may have exposed as many as 5,000 to hepatitis A, county officials said. Health officials have set up immunization clinics.

Kevin Gipson, the director of the Springfield-Greene County Health Department, said hepatitis A is a viral infection of the liver that can range from a mild illness for a few weeks to severe symptoms lasting several months, the Springfield (Mo.) News-Leader reported. The disease is easily transmitted from person-to-person in a food service environment. Health officials have set up immunization clinics.

Advertisement

The Health Department recommends those who ate at the Red Robin restaurant at 3720 S. Glenstone Ave., in Springfield from May 8 to May 16 to contact their healthcare provider. The Health Department said it considers this a significant health threat, but many more are commonly vaccinated for hepatitis A, than in the past -- reducing the risk.

For those who have not been vaccinated against heptitis A, the Health Department set up a vaccination clinic at Remington's at 1655 W. Republic Road. Vaccinations will be provided beginning Thursday at 11 a.m. to 6 p.m. and will continue Friday from 7 a.m. to noon, the Health Department said.

However, the Health Department recommends women who are pregnant or those who have a compromised immune system visit their doctor for guidance.