It is not all smooth going for certain companies out there in the market, considering the highly competitive environment that they are in right now. General Motors is one of them having announced a slew of recalls earlier this month. Still, when the going gets tough, the tough get going, right? General Motors has revealed that they do have plans to introduce wireless charging pads within some of the Cadillac models, and this initiative will kick off when they launch the 2015 ATS sport sedan and coupe later this coming fall.

Advertising

Ah, wireless charging. No longer do you need to keep the cigarette lighter port occupied with one of those nasty and large looking USB chargers, and apart from that, you do not need to grapple with them pesky wires and cables which look unsightly, not to mention get all tangled up for one reason or another.

The wireless charging pads that will see action in select Cadillac models will be compatible with Powermat, in addition to supporting Qi (which was demonstrated by Audi before) as well as different in-phone wireless charging technologies. So far, we do know that the Cadillac CTS, Cadillac Escalade, GMC Sierra and Chevy Silverado will feature such capability in due time. [Press Release]

Filed in . Read more about Gm and Wireless Charging.