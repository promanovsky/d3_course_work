Seatle: Amazon.com Inc`s newest mobile device, a smartphone that may sport a 3D screen, is the retailer`s latest attempt to exert its influence over the way consumers shop online.

Chief Executive Officer Jeff Bezos will preside over a mystery launch on Wednesday near its Seattle campus. Industry observers predict the arrival of a long-rumored smartphone after years of development.

It is unclear if a 3D display, as widely reported, is enough to help the new entree stand out in a crowded field dominated by Apple Inc`s seven-year-old iPhone and Samsung Electronics Co Ltd Galaxy devices. Three-quarters of U.S. consumers online already own a smartphone, according to online data tracking service comScore.

But its introduction reflects how smartphones and tablets are fast becoming how many consumers view and buy items online. Mobile commerce grew at almost twice the rate of online retail during the first quarter, according to comScore.

Analysts said Amazon has more opportunities outside the United States. Only 30 percent of the 5.2 billion phones on the market are smartphones, Topeka Capital Markets analyst Victor Anthony said in a June 5 research note.

"As the world shifts toward mobile, how do you make sure Amazon is front and center?" Jefferson Wang, senior partner at IBB Consulting, said in an interview.

Two sources have said the device contains 3D features visible to the eye without special glasses. Analysts expect Amazon`s Prime membership program, which offers features such as movie streaming and two-day delivery, to be tied to the phone.

A phone would represent yet another new area for Amazon, which got its start selling books and has since expanded into everything from original television shows to grocery delivery. It could also boost adoption of an Amazon payments platform.

On Tuesday, the Wall Street Journal reported AT&T Inc would carry the smartphone exclusively, as it did the iPhone for years and a short-lived Facebook phone made by HTC Corp that at one point was discounted to 99 cents.

"Let`s hope @amazon doesn`t fall victim to the @att curse that is the facebook phone," T-Mobile CEO John Legere tweeted.

A phone would be the third new device Amazon has introduced this year after its FireTV streaming video device and Dash grocery-ordering wand. In recent years, the company has moved more aggressively into hardware, selling its devices at cost in hopes of spurring purchases on Amazon`s website.

It has also used its devices to test emerging markets. This year, Amazon began selling the Kindle online in Brazil after years of selling the device in stores. The move gives Amazon a chance to work through some of the country`s logistics problems.