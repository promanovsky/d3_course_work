Lana Del Rey's latest album Ultraviolence has debuted at number one on the UK Albums Chart.

The record becomes Del Rey's second UK number one album following her 2012 debut Born To Die.



Linkin Park's new album The Hunting Party debuts at number two, while Sam Smith's In The Lonely Hour stays at number three.

Coldplay's Ghost Stories is down two places to number four, while last week's number one 48:13 by Kasabian slips down to number five.

Ed Sheeran's 2011 debut album + is at high climber at 9, having risen 41 places. His new album x is released in the UK tomorrow (June 23).



Deadmau5 enters at 14 with his seventh studio album While (1, and Tiesto's A Town Called Paradise lands at at 22.

Klaxons' Love Frequency debuts at number thirty nine, while Jennifer Lopez misses out on a top 40 chart position, with new album AKA entering at 41.

Listen to Lana Del Rey's 'Brooklyn Baby' below:

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

Source: Official Charts Company

View the latest Official Top 100 Album Chart

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io