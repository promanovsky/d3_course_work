Welcome to Wonkbook, Wonkblog’s morning policy news primer by Puneet Kollipara. To subscribe by e-mail, click here. Send comments, criticism or ideas to Wonkbook at Washpost dot com. To read more by the Wonkblog team, click here.

Wonkbook’s Number of the Day: 11.27 million barrels per day. That was daily U.S. petroleum production in April, the highest level in 44 years, thanks in part to the shale boom.

Wonkbook’s Chart of the Day: These charts show why the U.S. still has the most expensive, least effective health-care system.

Wonkbook's Top 5 Stories: (1) The Iraq crisis's broader impacts; (2) what does the tea party do now?; (3) our inefficient health care system; (4) the FCC's new fight; and (5) why are so many migrants flowing across the border?

1. Top story: A guide to how the Iraq conflict's policy and economic impacts on the U.S. and beyond

The conflict is sending up global crude-oil prices... "Brent crude was projected by Wall Street analysts to average as much as $116 a barrel by the end of the year. Now, with violence escalating in Iraq, how far the price will rise has become anyone’s guess. The international benchmark surged above $114 on June 13 for the first time in nine months as militants routed the Iraqi army in the north and advanced toward Baghdad, threatening to ignite a civil war. The Islamic State in Iraq and the Levant, known as ISIL, has halted repairs to the pipeline from the Kirkuk oil field to the Mediterranean port of Ceyhan in Turkey. The conflict threatens output in OPEC’s second-biggest crude producer." Mark Shenk in Bloomberg.

...despite the U.S.'s highest oil output in 44 years. "The turmoil in northern Iraq, affecting some of the country’s principal oil-producing areas, sent the price of Brent crude up 4 per cent last week, its biggest one-week rise since July last year. The price of US benchmark West Texas Intermediate crude has also risen, showing how, in spite of being an increasingly important producer, the US is not immune to the effects of disruption in world markets. The rise in world oil prices is, however, being curbed by US production, which over 2005-13 accounted for almost all of the increase in global supply." Ed Crooks in The Financial Times.

Is the lack of major war holding back the economy? "Counterintuitive though it may sound, the greater peacefulness of the world may make the attainment of higher rates of economic growth less urgent and thus less likely. This view does not claim that fighting wars improves economies, as of course the actual conflict brings death and destruction. The claim is also distinct from the Keynesian argument that preparing for war lifts government spending and puts people to work. Rather, the very possibility of war focuses the attention of governments on getting some basic decisions right — whether investing in science or simply liberalizing the economy. Such focus ends up improving a nation’s longer-run prospects." Tyler Cowen in The New York Times.

Could the conflict help foster Iran cooperation with the U.S.? "Iran could contemplate cooperating with its old adversary the United States on restoring security to Iraq if it saw Washington confronting 'terrorist groups in Iraq and elsewhere,' Iranian President Hassan Rouhani said on Saturday. Rouhani, a pragmatist who has presided over a thaw in Iran's relations with the West, also said Tehran was unlikely to send forces to Iraq but stood ready to provide help within the framework of international law. Baghdad has not requested such assistance, he added." Parisa Hafezi in Reuters.

U.S.-based contractors are having to evacuate. "The crisis in Iraq has prompted U.S. contractors with personnel there to evacuate them from areas near Baghdad that are increasingly in the line of fire as insurgent fighters capture more territory with the apparent end goal of seizing the Iraqi capital....The individuals involved include U.S. citizens who are currently working under contract with Iraq’s central government in support of the Pentagon’s foreign weapons sales program....It could be hundreds, if not thousands, of people." Dan Lamothe in The Washington Post.

Explainer: Understanding the U.S. presence in Iraq. Robert Burns in the Associated Press.

U.S. has already sent billions in military equipment to Iraq. "The US has already provided Iraq with well over $10 billion in military equipment, ranging from ammunition to missiles and helicopters, with another $1 billion currently in the pipeline....Under pressure of militant advance...the Maliki government has also been pressing for a speedup in the delivery of F-16s and Apache helicopters it is purchasing from US defense contractors. Some Iraq experts are recommending caution on the question of sending more arms into Iraq." Howard LaFranchi in The Christian Science Monitor.

Why the decision to send more arms to the Iraqis is a tough one. "The Obama administration faces a tough decision on whether to provide more arms to Iraq’s government. Some arms have already fallen into the hands of the Islamic State of Iraq and Syria (ISIS), an al Qaeda-linked group that has taken over the Iraqi cities Mosul and Tikrit and is advancing on Baghdad. The Pentagon has acknowledged that the group sized some U.S. equipment and vehicles as the Iraqi army folded in front of it....The worry is that more arms could end up with the group if it is able to take over Baghdad." Kristina Wong in The Hill.

Some other good explainers to help understand the latest Iraq turmoil:

With ground troops ruled out, what other military options does the U.S. have in Iraq? Daniel Serwer in The Hill.

11 facts that explain the escalating crisis. Zack Beauchamp in Vox.

CROOK: How U.S. politics undermine security. "As Iraq unravels, a painful truth about U.S. politics and foreign policy is becoming more evident: The U.S. is very good in all-or-nothing situations, but all-or-nothing situations don't often arise. This is a country that can and will meet existential threats with unity of purpose and vast resources. In this regard, even now, it stands alone. Few threats rise to that level. Lesser dangers can still be serious, without commanding or justifying that kind of response. Precisely for that reason, they put greater stress on democratic politics, and U.S. politics seems ever less able to cope. The 'war on terror' is the paradigm example of this syndrome. In Iraq, Syria and Afghanistan, nothing is simple. The goals are complex, the trade-offs excruciating." Clive Crook in Bloomberg View.

Top opinion

KRUGMAN: Obama's big deals. "In terms of policy substance Mr. Obama is having a seriously good year. In fact, there’s a very good chance that 2014 will go down in the record books as one of those years when America took a major turn in the right direction....There were huge missed opportunities early in his administration — inadequate stimulus, the failure to offer significant relief to distressed homeowners. Also, he wasted years in pursuit of a Grand Bargain on the budget that, aside from turning out to be impossible, would have moved America in the wrong direction. But in his second term he is making good on the promise of real change for the better." Paul Krugman in The New York Times.

SAMUELSON: Our enigmatic economy. "The verdict: an invigorated recovery. The stock market’s optimism trumps the bond market’s (possible) doubt. Or does it? Could the economy disappoint again? Post-crisis economic commentary has consistently misjudged the outlook. It has underestimated the crisis’s psychological effects on consumers and business managers. The past is less a guide to the future because the financial crisis and Great Recession are, in living memory, unique events. That’s why the economy remains an enigma." Robert J. Samuelson in The Washington Post.

LESLIE: The true cost of hidden money. "Why do international balance sheets each year show more liabilities than assets, as if the world is in debt to itself?...Money that, say, leaves the United States for an offshore tax shelter is recorded as a liability here, but it is listed nowhere as an asset — its mission, after all, is disappearance. But until now the economists lacked hard numbers to confirm their suspicions. By analyzing data released in recent years by central banks in Switzerland and Luxembourg on foreigners’ bank holdings, then extrapolating to other tax havens, Mr. Zucman has put creditable numbers on tax evasion, showing that it’s rampant — and a major driver of wealth inequality." Jacques Leslie in The New York Times.

CANNON AND PREBLE: The other veterans scandal. "The alternative system we propose combines the universal goal of improving veterans’ benefits with conservative Republicans’ preference for market incentives and antiwar Democrats’ desire to make it harder to wage war. Pre-funding veterans’ benefits could prevent unnecessary wars, or at least end them sooner. We can think of no greater tribute to the men and women serving in our armed forces. " Michael F. Cannon and Christopher Preble in The New York Times.

CHAIT: Today, in non-Obamacare-train-wreck news... "I have made this point repeatedly, but it’s fundamental enough to bear repeating: The information environment surrounding Obamacare is fundamentally asymmetrical. The liberal policy wonks reporting on the program have made a good faith and highly successful effort to depict both the good and the bad news about the program in context. Conservatives, even the most wonkish ones, have engaged in a one-sided propaganda effort. If you get your news about Obamacare from conservative sources, you have heard an endless succession of horror predictions that, when not borne out, have gone uncorrected. The bottom line is that the program is doing what it was designed to do." Jonathan Chait in New York Magazine.

McARDLE: The adoption turnaround. "These days, adoptable infants are so rare that parents wait years and pay tens of thousands of dollars to get one. What explains the change? I know what you’re thinking: birth control; abortion; the end of the stigma against unwed motherhood. And those things undoubtedly played a part: immediately after Roe v. Wade, the number of adoptable white infants dropped sharply. But in fact, the change started well before that. By the end of World War II, when the stigma against unwed motherhood was still very high, demand for adoptable infants was already growing much faster than the supply. What changed? Two things, I’d suggest: First, we became richer....Second, the value of child labor declined." Megan McArdle in Bloomberg View.

Father's Day interlude: The evolution of dad dancing, featuring New Jersey Gov. Chris Christie.

2. What does the tea party do now?

The tea party dethroned Cantor for this? "The only conservative who has jumped into the race for majority leader is Rep. Raúl R. Labrador (Idaho), a long-shot candidate who waited until Friday afternoon to announce a bid. McCarthy’s apparent easy ascent as unrest swirled around him underscores how the tea party, even with its strong pull in congressional primaries and ability to dictate the Republican agenda, remains a limited force in the insular and relationship-driven sphere of House GOP politics. Though sizable in numbers, they lack the organization and preparation, the battle-tested aides and the Machiavellian instincts to take over." Robert Costa in The Washington Post.

The tea party's best hope: Winning the majority whip race. "The last best hope for the tea party to win a House leadership post after the stunning primary defeat of Majority Leader Eric Cantor (Va.) is the race for majority whip, the House’s third-ranking position. But as House Republicans return to Washington on Monday, that contest risks becoming another lost opportunity for the conservatives who have called Cantor’s loss a warning shot to the establishment." Robert Costa in The Washington Post.

But the tea party did get a jolt for the upcoming primaries. "As soon as a little-known conservative toppled House Majority Leader Eric Cantor on Tuesday night, tea party enthusiasts turned their sights to the next big election-year targets: Mississippi and Kansas. The two states are next up on the GOP's primary calendar as Washington insiders, particularly 76-year-old Sen. Thad Cochran of Mississippi, are fighting hard-right upstarts in an environment in which outsiders have suddenly gained currency." Lisa Mascaro in the Los Angeles Times.

All these primaries are doing little to calm the party's internal divisions. "As Republicans struggle to understand the electoral earthquake that cost House Majority Leader Eric Cantor his suburban Richmond seat Tuesday, the party confronts a paradox: It is dominated more by conservatives than at any time in memory and yet riven with divisions, including over issues that barely registered even two years ago. That presents a tricky challenge to those who are seeking the 2016 GOP nomination in a presidential primary where there will be splits over immigration, trade, the government’s role in education, and foreign policy, among other topics." Karen Tumulty and Dan Balz in The Washington Post.

Exhibit A: immigration reform. "The latest round of Republican primaries shows that immigration policy continues to vex a party looking for its next standard-bearer amid a battle between its establishment and tea-party wings. Perhaps no other issue better illustrates the fault line in the GOP between a political class seeking to build a national coalition and grass-roots activists pushing to reinforce conservative ideology. Both sides are trying to leverage the results of Tuesday's primaries to rally support for their cause, while potential presidential contenders struggle to find their footing." Beth Reinhard in The Wall Street Journal.

Big business is panicking after Cantor's loss. "Already uneasy over what they see as an especially hostile strain of anticorporate populism growing within the conservative movement, and threatening the traditional corporate-friendly centers of power inside the Republican Party, many businesses fear the loss of some of their strongest champions on Capitol Hill....Leaders in the financial community still have a formidable force of allied lawmakers and hired lobbyists in Washington. But several major initiatives that business hoped to see through Congress this year are in doubt." Jeremy W. Peters and Shaila Dewan in The New York Times.

Long read: Why big business fears the tea party. Michael Lind in Politico Magazine.

Explainer: What Cantor's departure means for economic policy. Damian Paletta in The Wall Street Journal.

How Cantor's likely successor could be a boon for tech industry priorities. "Earlier this year, the House overwhelmingly passed a surveillance reform bill that tech companies hope will restore public trust in their services after leaks from Edward Snowden showed the National Security Agency was snooping on users with the acquiescence of companies. The House also overwhelmingly passed a patent reform bill last year and has moved forward with tax measures supported by the industry. Much of that success is due to McCarthy and other leaders’ ability to get lawmakers in line, industry lobbyists said, even in a period of hyper-partisanship and deep splits in the House GOP." Julian Hattem in The Hill.

Smartphone interlude: How one teen's app could be a lifesaver.

3. Our inefficient health-care system

Once again, U.S. has most expensive, least effective health care system in survey. "A report released Monday by a respected think tank ranks the United States dead last in the quality of its health-care system when compared with 10 other western, industrialized nations, the same spot it occupied in four previous studies by the same organization. Not only did the U.S. fail to move up between 2004 and 2014 — as other nations did with concerted effort and significant reforms — it also has maintained this dubious distinction while spending far more per capita ($8,508) on health care than Norway ($5,669), which has the second most expensive system." Lenny Bernstein in The Washington Post.

Explainer: 7 factors that make the U.S. system inefficient, expensive. Lillian Thomas in the Pittsburgh Post-Gazette.

More insurers in Obamacare could mean lower premiums. "nsurance companies that sat on the sidelines of Obamacare last year are looking to get in the game in 2015. This could mean more options and lower premiums for Obamacare enrollees next year. A new analysis from The Advisory Board says that in every state where data is available so far, more providers are asking to participate in Obamacare next year. This is good news for consumers, as more competition means more options." Brianna Ehley in The Fiscal Times.

Analysis: Why did so many insurers sit out the first year? "Insurers sat out of the exchanges for different reasons in year one. Some were wary of the start-up risks. Others were openly taking a wait-and-see approach. Still more, it seems, didn’t want any part in the first year's batch of customers, who were expected to be older and sicker....And while the technical problems associated with the exchanges have been legion, plans that participated have reported predictably higher revenue, if unclear profits. One million more consumers signed up than expected…and while they weren't as young and healthy as the insurance companies had hoped for, they were more customers. Now, more plans want their chance to chase those dollars." Dan Diamond in The Advisory Board.

A 'clunky' transition toward electronic records. "Doctors largely supported the Obama administration’s $30 billion incentive program....They understood the potential of using health IT to reduce medical errors, increase efficiency and give patients and caregivers access to complete, portable and up-to-date records. If that vision isn’t motivating enough, there’s also cash on the line....But the transition has proved painful. Paperless records still don’t flow smoothly among doctors, hospitals and patients and they won’t for some time. Nor have measurable savings or widespread improvements been seen yet. And there’s a difference between liking the idea of electronic health records, or EHRs, and liking the particular systems in use." Arthur Allen in Politico.

The government is trying to resolve questions about thousands of people's subsidy eligibility. "The Obama administration is contacting hundreds of thousands of people with subsidized health insurance to resolve questions about their eligibility, as consumer advocates express concern that many will be required to repay some or all of the subsidies....The government is asking consumers for additional documents to verify their income, citizenship, immigration status and Social Security numbers, as well as any health coverage that they may have from employers. People who do not provide the information risk losing their subsidized coverage and may have to repay subsidies next April." Robert Pear in The New York Times.

Other health care reads:

The real reason the CBO won't re-score Obamacare. Adrianna McIntyre in Vox.

HHS vows to fight for orphan drug rule. Ferdous Al-Faruque in The Hill.

World Cup interlude: The Puppy Bowl, World Cup-style.

4. The FCC picks a new fight

The FCC zeroes in on Netflix’s deals with Comcast, Verizon, others. "Netflix's continued dispute with Internet providers underscores that we can't declare a winner in the debate without more information about what Netflix is paying Comcast and Verizon to boost customers' streaming speeds, whether that's causing people's bills to rise, and who actually has the leverage in these relationships. On Friday, the Federal Communications Commission said it's going to look more closely at those deals. FCC Chairman Tom Wheeler said he had received copies of the agreements involving Netflix, Comcast and Verizon and would be asking other content companies, perhaps such as Google, to provide their own information about the paid deals they've struck with ISPs, too." Brian Fung in The Washington Post.

What are 'interconnection deals'? "Since the dawn of the Internet, so-called interconnection deals have remained safely in the universe of things few people cared about. But they suddenly became the subject of passion and confusion in the past several months, as the debate over Internet regulation flared on several fronts. While they are not technically a part of Net neutrality rules—which govern how Internet providers can treat traffic after it has entered their networks—the same themes are in play." Joshua Brustein in Bloomberg Businessweek.

A new day of reckoning for mobile net neutrality? "A surge in mobile Internet usage has U.S. regulators considering whether to apply the same rules to fixed and wireless Internet traffic, and large technology firms are siding with consumer advocates to call for such a change....A growing number of U.S. consumers, many of them low income, non-white and young, rely on such devices as their primary means of Internet access. The lines between fixed and broadband continue to blur as mobile carriers develop fixed broadband businesses of their own and use Wi-Fi to offload wireless data traffic, and cable broadband providers create Wi-Fi hotspots for their customers." Alina Selyukh and Marina Lopes in Reuters.

Other tech reads:

The man who coined the term "net neutrality" is running for lieutenant governor in New York. Timothy B. Lee in Vox.

Skydiving animal interlude: Heroic puppy goes skydiving with the soldier she saved.

5. Why youth migrants are coming across the border in such large numbers

Are the administration's immigration policies at play? "There is growing evidence that a surge of tens of thousands of Central American minors across the Mexican border into Texas is being driven in large part by the perception they will be allowed to stay under the Obama administration’s immigration policies. Administration officials — after initially dismissing such reports — are now attempting to push back on the idea, warning parents not to send their children as officials scramble to accommodate tens of thousands who already have arrived in Texas....The crisis marks another immigration-related political dilemma for President Obama....It could also hamper Obama’s ability to meet the demands of his liberal base by using executive authority." David Nakamura in The Washington Post.

Why thousands of children are fleeing Central America to Texas — alone. "Children are being 'pushed' by violence in Central America....Children are being 'pulled' by a desire or need to be reunited with family....Children are being 'pulled' by lenient US policy — particularly a 2008 law by Congress." Dara Lind in Vox.

Speaking of those executive actions: Obama delaying on immigration despite Cantor loss. "To the frustration of many of his supporters, President Barack Obama is backing away from immigration changes he could make on his own. He is kicking the issue to House Republicans instead, despite mounting evidence they won't address the millions of immigrants living illegally in the United States....It's an approach that's drawing friendly fire from immigration advocates who say Obama has been sitting on his hands long enough. For starters, they want immediate action to slow deportations. But the White House wants to ensure that if and when an overhaul ultimately dies in Congress, Republicans can't claim it was Obama who pulled the plug." Josh Lederman in the Associated Press.

The U.S. is running out of space to house the migrants. "The Department of Homeland Security hasn't disclosed statistics on how many immigrants it has released. But the agency has confirmed that due to a shortage of detention space in Texas, it has shipped hundreds of immigrants recently apprehended in Texas to Arizona for processing, and subsequently dropped some off at bus stations there, allowing them to travel to locations around the country until they can be deported." Ana Campoy in The Wall Street Journal.

Other immigration reads:

Is the Border Patrol finally addressing its excessive-force problem? Dara Lind in Vox.

Science interlude: How dads improve their kids lives, according to science.

Wonkblog roundup

The biggest housing bubble in the world is in … Canada? Matt O'Brien.

David Brat’s victory is part of broader rise of religion in economics. Michelle Boorstein.

There are more museums in the US than there are Starbucks and McDonalds – combined. Christopher Ingraham.

What underage drinkers drink when they binge drink. Roberto A. Ferdman.

Why the private sector is so afraid of student debt. Max Ehrenfreund.

What if your cell phone buzzed every time someone gave your congressman a ton of money? Emily Badger.

Rank-and-file workers have a lot more power over corporations than they think. Lydia DePillis.

Et Cetera

Obama intervenes in Philadelphia transit strike. Scott Moritz and Margaret Talev in Bloomberg.

Changing pot laws create gray areas in child welfare and custody cases. Kristen Wyatt in The Washington Post.

U.S. coal companies ride exports to booming business. Jackie Northam in NPR.

Fight between watchdogs in VA drama could affect federal whistleblowers. Joe Davidson in The Washington Post.

FAA controllers still working exhausting schedules. Joan Lowy in the Associated Press.

OECD sees U.S. growth accelerating through 2015. Howard Schneider in Reuters.

Producer prices fall, but inflation still seen firming. Lucia Mutikani in Reuters.

Got tips, additions, or comments? E-mail us.

Wonkbook is produced with help from Michelle Williams and Ryan McCarthy.