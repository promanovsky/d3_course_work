By Melony Roy

LISTEN to today’s report

Twitter adds new photo-sharing features

Twitter has added new features that allow users to tag multiple people in a photo and attach up to four images to a tweet.

Users can now tag up to 10 friends in a Twitter photo, without taking away from your 140-character count. A second new feature allow users to automatically create a collage by attaching up to four photos to tweet.

Multiple photos can only be uploaded via iPhone, but Twitter says Android and Twitter.com functionality will follow soon.

The changes were announced on the company’s official blog.

————————————————————————————————————————-

Turkey bans another social network

A court in Turkey has ruled the government can not ban Twitter and ordered the country’s telecommunications authority to restore access to the service.

Twitter has said it’s not only fighting the Turkish government’s ban on the service, but it is also fighting a request to remove a specific account that has been tweeting about government corruption, because it is committed to upholding free speech.

Today, YouTube was also blocked in the country. It was removed hours after leaked audio recording of Turkish officials discussing Syria appeared online.

——————————————————————————————————————————

New social trend

A new trend is taking over social media, with the goal of raising awareness of testicular cancer. Men across the Internet are slipping “it” in a sock and taking 98% nude selfies.

A Facebook page and Twitter account has been set up to promote the trend.

————————————————————————————————

Follow KYW social media editor Melony Roy @MelonyRoy.