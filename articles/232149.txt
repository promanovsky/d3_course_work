YouTube is reportedly nearing a deal to buy popular game streaming startup Twitch for one billion dollars.

Variety reported that the deal is said to be an all-cash offer and will close 'imminently'; however, The Wall Street Journal reported that the discussions were "early" and that "a deal isn't imminent."

According to the Verge, the deal, if succeeds, would effectively put one of the web's most highly trafficked sites firmly in Google's hands.

Twitch, launched in 2011, has become a premier destination for video game live-streams, and has effectively turned titles as offbeat as Pokemon into spectator sports. It currently has more than 1 million unique users, the report added.