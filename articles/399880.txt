Nintendo Posts Bigger Quarterly Loss as Wii U, 3DS Sales Slump

The Japanese game giant's operating loss grew to $92.7 million for the three months to March, as sales of its consoles and software fell 8.4 percent compared to the same quarter last year.

TOKYO – Nintendo announced an operating loss of $92.7 million (?9.47 billion) in the April to June quarter, compared to $48.2 million (?4.92 billion) in the same period last year, as sales of its Wii U and 3DS consoles continued to disappoint. However, the company left its prediction of a return to profit for the full year unchanged.

Total sales fell to $731 million (?74.7 billion) from $799 million (?81.6 billion) in the corresponding quarter of 2013. Sales have more than halved since the same period in 2011, when Nintendo posted figures of $1.85 billion (?188.6 billion) on the back of the popularity of the original Wii console.

PHOTOS Titans of Comic-Con: Stars Re-Create Classic Characters

Nintendo sold just 820,000 units of its portable 3DS in the three months to June, along with 8.57 million games. Sales of the 3DS dipped below one million in the previous quarter. Wii U console sales were also weak, registering just 520,000 units and 4.39 million games. Mario Kart 8, released globally in May, accounted for 2.82 million of total game sales.

The company in May said that it expects to sell 3.6 million Wii U consoles in the year ending in March 2015, along with 12 million units of its 3DS. The company said in its earnings release today that there is, "no revision to the consolidated financial forecast for this fiscal year," despite the dismal sales figures.

STORY China's Fosun Pacts With Shanghai Film Group on Investment Fund

The Kyoto-based game and console maker has long had a reputation among analysts for being overly optimistic in its forecasts. However, predicting sales of more than 10 million 3DS units for the second half of the year, having sold less than two million in the first half, appears to be stretching the credibility of Nintendo's predictions to a new level.

A spokesperson from the company confirmed to The Hollywood Reporter that it had no intention of revising its forecasts and expected to hit the stated sales targets.

Rival Sony, whose successful PlayStation 4 the Wii U is losing out to globally, reports its earnings tomorrow.

Twitter: @GavinJBlair