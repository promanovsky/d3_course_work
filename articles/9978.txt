General Motors is offering free loaner cars and $500 toward a new GM vehicle to more than a million owners of compact cars that are being recalled for a deadly ignition switch defect.

But the owners have to ask in order to get the benefits.

The offers, disclosed in a document posted Wednesday on the national highway traffic safety administration’s website, are effective immediately. Owners will be able to use the loaner cars until parts arrive at dealerships to replace the faulty switches. They are expected around 7 April, GM said.

The $500 cash allowance offer runs through 30 April.

GM is recalling 1.6m older small cars worldwide because faulty ignitions can shut off engines unexpectedly. GM says 13 people have died in crashes linked to the problem. If the engines shut off, drivers can lose power steering and power brakes, and the air bags may not inflate if there’s a crash.

GM is facing a department of justice investigation, as well as investigations from two congressional committees and federal safety regulators over its handling of the recall. The loaner/rebate program is part of the company’s damage control effort.

Last week, new GM CEO Mary Barra promised that an internal review would bring improvements and prevent similar problems in the future. She also said current management is intent on “taking great care of our customers and showing that it really is a new day at GM.”

On 13 February, GM announced the recall of more than 780,000 Cobalts and Pontiac G5s (model years 2005-2007). Two weeks later it added 842,000 Saturn Ion compacts (2003-2007), and Chevrolet HHR SUVs and Pontiac Solstice and Saturn Sky sports cars (2006-2007).

In the document disclosed Wednesday, GM tells dealers that in situations where an owner is concerned about driving their car and asks for a loaner, service managers can give them one until repair parts arrive.

Dealers are also instructed to tell owners who ask that GM isn’t offering to buy back their car. But the company will offer the owners cash toward buying or leasing a new Chevrolet, Buick, GMC or Cadillac vehicle.

The allowance “is intended to assist those customers who are unhappy and may want to trade out of their vehicle”, the document said. “This special cash allowance is not a sales tool.”