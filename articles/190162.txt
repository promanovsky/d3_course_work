Inappropriate or much ado about nothing? Actress-singer Willow Smith, Will Smith and Jada Pinkett-Smith's 13-year-old daughter, posed for two controversial social media snaps with actor Moises Arias, 20. In the black-and-white images, which appear on Arias' Tumblr page, the "Whip My Hair" singer lies in bed next to Arias. While Smith wears jeans and a top and stares off into the distance, Arias is shirtless and sits up.

Although the pals don't appear to be touching in the photo, it nevertheless raised more than a few eyebrows via social media. Although the image still appears on his Tumblr page, it was deleted from his Instagram account.

Actor-musician Arias once starred opposite Miley Cyrus as Rico on Hannah Montana, and is pals with Willow and her brother Jaden—plus Jaden's on-and-off girlfriend Kylie Jenner nd her sister, Kendall Indeed, his Tumblr page is chock-full of snaps documenting his friendship with the Jenners and the Smiths, and he appeared to have hung out with the group during Coachella last month in Indio, Calif.

Smith, meanwhile, has proven herself precocious professionally speaking—although she revealed last year that she turned down the title role in the remake of Annie, with Oscar nominee Quvenzhané Wallis taking over.

Her famous father Will recounted the suprising decision. "She looked at me and said, 'Daddy, I have a better idea, how about I just be 12,'" he explained. "I'm really learning through Willow the necessity that we have to snap ourselves back and refocus on the emotional needs of the people that we love," he said last year.