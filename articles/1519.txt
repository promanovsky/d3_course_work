Apparently there is an audience for science in primetime, as Fox’s 10-network roadblock premiere of “Cosmos: A Spacetime Odyssey” attracted a sizable 8.5 million viewers on Sunday night — despite facing tough competition from entertainment series like AMC’s “The Walking Dead” and ABC’s new “Resurrection.”

The Fox network broadcast averaged about 5.8 million viewers in Nielsen’s affiliate-based estimates for the 9 o’clock hour Sunday, as well as a 2.1 rating/5 share in adults 18-49. The under-50 audience was comprised of roughly 60% men.

Viewing on other networks raised these totals to 8.5 million and a 2.9 rating in the demo, according to Nielsen.

In addition to the broadcaster, the premiere of “Cosmos” aired across nine Fox networks: National Geographic Channel, FX, FXX, FXM, FOX Sports 1, FOX Sports 2, Nat Geo Wild, Nat Geo Mundo and FOX Life.

Fox projects that the global total-viewer number for the “Cosmos” premiere will top 40 million.

“Cosmos: A Spacetime Odyssey” is a follow-up to the 1980s series “Cosmos: A Personal Voyage” with Carl Sagan.

The eight-week series is hosted this time around by astrophysicist and science enthusiast Neil deGrasse Tyson. The series is executive produced by Seth MacFarlane and Ann Druyan, the widow of Sagan.