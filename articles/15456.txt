When #JennyAsks, people answer - although not with the responses she had in mind.

The outspoken anti-vaccination activist Jenny McCarthy tried to hold a Twitter chat on Thursday night.

"What is the most important personality trait you look for in a mate? Reply using #JennyAsks," tweeted the co-host of The View.

Rather than answers like "A great personality" or "A sense of humor", many people responded with criticisms of her claims that childhood vaccinations can be linked to autism.

"My ideal mate? Smart, funny, independent and without their own Internet death-toll meter," said one user.

"Someone who puts the interests of the community ahead of preserving their own absurd, unshakable ignorance," tweeted another.

"Somebody who gets that refusing vaccines because of 'toxins' and then shilling for e-cigs makes you a pathetic hypocrite," shot back a third.

The entire hashtag is full of vaccination discussion, arguments and articles instead of...whatever the original question was.

Jenny McCarthy's hosting and acting credits have paled in comparison to her notoriety for claiming vaccinations caused her son Evan's autism.

Recently fellow blond c-lister Kristin Cavallari revealed similar views, saying she's "read too many books" to vaccinate her kids.

So far, Jenny has no response to the hashtag backlash.

VOTE: Vaccines ...