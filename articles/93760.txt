British police are investigating the unexplained death of 25-year-old model, writer and TV personality Peaches Geldof and say they will hand their findings to a coroner.

Local authority Kent Council said her body had been taken to a hospital where a forensic pathologist will perform a post-mortem Wednesday in an attempt to determine the cause of death.

Geldof was pronounced dead Monday by paramedics who were called to her home in Wrotham, southeast of London. Kent Police said officers were investigating the "unexplained sudden death," but did not consider it suspicious.

Peaches Geldof was the daughter of Irish musician and Band Aid founder Bob Geldof and TV presenter Paula Yates, who died of a drug overdose in 2000. She grew up in the glare of Britain's press, which reveled in the late-night antics of her teenage years.

More recently she had married for a second time, to musician Tom Cohen, had two young sons and worked as a broadcaster and fashion writer. She said in 2009 that her drug-taking years were behind her.

Bob Geldof said the family was "beyond pain."

"She was the wildest, funniest, cleverest, wittiest and the most bonkers of all of us. Writing 'was' destroys me afresh. What a beautiful child. How is this possible that we will not see her again? How is that bearable?" he said in a statement. "We loved her and will cherish her forever. How sad that sentence is. Tom and her sons Astala and Phaedra will always belong in our family, fractured so often, but never broken."

Her older sister, Fifi Geldof, paid tribute with a childhood photo posted on Instagram, writing, "My beautiful baby sister... Gone but never forgotten. I love you, Peaches x."

The death came as a shock to Britain's entertainment and fashion circles. She was a frequent attendee at fashion shows in London and New York, and was photographed just last week at a London show for the Tesco brand F&F.

Messages of condolence poured in from celebrities including music impresario Simon Cowell and singer Lily Allen. Irish President Michael D. Higgins, who had been due to meet Bob Geldof on a visit to Britain this week, also sent his condolences to the family on their "immense loss."

"This is such a difficult cross to bear for any family and all of our thoughts are with Peaches' family and friends at this time," Higgins said.

Geldof's death was the lead story in many British newspapers Tuesday, with several using the last photo she posted on Twitter -- of her as a toddler with her mother.

Commentators noted the tragic parallels to the life and death of Yates. In the Guardian, columnist Hadley Freeman said "the shock of Geldof's death comes from the loss of a young woman -- still only 25 -- who many of us had followed since her birth, who seemed so close to finding the stability that had eluded her mother."