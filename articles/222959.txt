Unhealthy diets pose a greater risk to global health than the increasingly regulated sale of tobacco and governments should move fast to tax harmful food products, a United Nations investigator said on Monday.

In a statement issued on the opening of the annual summit of the World Health Organisation (WHO), Belgian professor Olivier de Schutter called for efforts to launch negotiations on a global pact to tackle the obesity epidemic.

"Unhealthy diets are now a greater threat to global health than tobacco. Just as the world came together to regulate the risks of tobacco, a bold framework convention on adequate diets must now be agreed," he said.

De Schutter, who has held his post of special rapporteur on the right to food since 2008 and earlier headed the Paris-based International Federation of Human Rights, reports to the U.N. Human Rights Council in Geneva.

In 2005, a U.N. convention on tobacco control aimed at reducing deaths and health problems caused by the product went into force after long negotiations under the umbrella of the WHO.

In a report to the rights council in 2012, de Schutter said a similar accord on food should include taxing unhealthy products, regulating food high in saturated fats, salt and sugar, and "cracking down on junk food advertising."

That report also called for an overhaul on the system of farm subsidies "that make certain ingredients cheaper than others", and for support for local production "so that consumers have access to healthy, fresh and nutritious foods."

In his Monday statement, issued through the office of the U.N. High Commissioner for Human Rights, de Schutter said any attempts to promote better diets and combat obesity "will only work if the food systems underpinning them are put right.

"Governments have been focusing on increasing calories availability, but they have often been indifferent to what kind of calories are on offer, at what price, to whom they are made available, and how they are marketed."

Such measures, he declared, "are essential to ensure that people are protected from aggressive misinformation campaigns."