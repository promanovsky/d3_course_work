"Come and Get It" singer Selena Gomez seems to be going through it these days and blames a "lack of love" for herself as the reason for her recent "social media detox."

On Monday, Gomez unfollowed "KUWTK" stars Kendall and Kylie Jenner, both of whom were reported to be her best friends. The three gal pals were so close in fact, that reports claimed that Gomez's recent mansion purchase in Calabasas was to be closer to the Jenner sisters. The bestie trio was last spotted together at the Coachella Music Festival.

Gomez's unfollow spree didn't stop there, however.

On Tuesday, she unfriended her former BFF and singer Taylor Swift. Gomez's Instagram account now shows that she follows no one.

Gomez completed the unfollow process by deleting a slew of celebrities from her Instagram feed including Reese Witherspoon, Vanessa Hudgens, Ashley Benson, and even her on-again off-again boyfriend Justin Bieber. (Safe to say they're off again?)

HollywoodLife reports that Gomez has been keeping to herself these days and hasn't been spotted out with any friends at all. Some say she's been looking more down on Instagram - even posting a sad pic of herself captioning it, "I go back to, black," quoting Amy Winehouse.

Following her drastic unfollow spree on Instagram, Gomez posted a Mary J. Blige interview snapshot about her 2007 album "Growing Pains." In the clip, the lyrics to Blige's song "Growing Pains" are included: "I got every material thing I could ever need/ I got love from my fans that I adore/And I'm grateful/But my love for myself is lacking a little bit/I have to admit that I'm working on me."

Blige was also quoted in the article as saying, "It's going to take a long while to recover. I can't understand how anybody could think things are perfect. I'm not saying I haven't made progress, but I have got a lot of work to do if I want to sustain this breakthrough and stay in a positive state of mind."

Fans took to Instagram to express their love and support for their singer through her tough time. Hang in there, Selena!