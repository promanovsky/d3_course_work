A second MERS case has been found in the United States, according to the Centers for Disease Control – CDC. Health experts from the organization are currently investigating whether or not the second case of the deadly virus was related to the first. Patient two is reportedly located in Florida.

On May 2, the CDC announced that the first imported MERS case was found in an Indiana patient. The contracted health care professional had recently been working in Saudi Arabia. The patient was treated and ultimately released from the Community Hospital in Muster, Indiana. The patient reportedly experienced a full recovery from the virus which often results in death. In 2003, approximately 800 deaths occurred around the world after patients presented with the virus.

MERS is a part of the coronavirus group which also includes SARS, which stands for severe acute respiratory syndrome, and the common cold. MERS stands for Middle East Respiratory Syndrome. The virus is reportedly a new affliction in humans. Since the recent outbreak began in Saudi Arabia in 2012, about 500 cases of the deadly virus have been recorded. Approximately one third of the patients diagnosed with the virus during the outbreak in Saudi Arabia have died.

A MERS CDC release reads:

“Most people who have been confirmed to have MERS-CoV infection developed severe acute respiratory illness. They had fever, cough, and shortness of breath. More than 30 percent of these people have died. So far, all the cases have been linked to seven countries in the Arabian Peninsula. This virus has spread from ill people to others through close contact, such as caring for or living with an infected person. However, there is no evidence of sustained spreading in community settings.”

The Centers for Disease Control statement went on to note that MERS cases are closely being monitored on a global scale and the agency is working with its partners to “better understand the risks of the virus” and learning more about both the source of MERS and how to prevent spreading. Some are calling for a quarantine of travelers from the countries where MERS cases have presented before they are allowed to enter the United States.

MERS is believed to spread from person-to-person via close contact. Health care workers have become infected by the patients they are treating, according to a CDC report. Although the source of the deadly virus remains unknown, researchers currently believe that MERS stems from an animal source. Camels in Saudi Arabia, Egypt and Qatar have also been infected.

What do you think should be done to stop the spread of MERS in the United States?

[Image Via: Pat Dollard website]