PepsiCo, Inc. (NYSE: ) is breaking out after a prolonged price consolidation since this time last year.

This mature enterprise fits into the very-slow-growth category, but the company offers a solid dividend. The stock is currently yielding three percent.

Some big investors are after PepsiCo to spin off its snacks business, but management has been reticent due to the fact that growth in beverages has been slow going.

Spin-offs or break-ups are typically wealth-creating for shareholders, but they can be one-time wonders.

Last quarter, PepsiCo’s earnings per diluted share grew a solid 14.5% on the back of flat revenues.

The company certainly could surprise the marketplace with a spin-off plan, but this is not really expected. The stock would soar on the news, however.

The Coca-Cola Company (NYSE: ) is actually yielding the same three percent currently. Over the last two years, PepsiCo has significantly outperformed Coca-Cola on the stock market, but both positions tend to go back and forth.

I don’t consider either company particularly attractive at its current price, but their yields are, and it would be reasonable for investors to expect earnings growth combined with an annual dividend of approximately 10% from either company.

Dr Pepper Snapple Group, Inc. (NYSE: ) is a much smaller company compared to Coca-Cola or PepsiCo. It’s done a lot better on the stock market comparatively over the last five years. This stock currently yields just less than three percent, but its growth is slowing.

With such international businesses, currency risk is a major issue and when you have country-specific problems like what recently transpired in Venezuela, it can wreak havoc with earnings.

Monster Beverage Corporation (NASDAQ: ) is about the same size as Dr Pepper Snapple Group, but it’s outperformed all three of the above companies on the stock market over the last five years.

The company is still a double-digit growth story, and it is more expensively price compared to its rivals. With this in mind, however, it remains more of a favorite among Wall Street analysts for the simple reason that it is able to generate a higher rate of growth than its peer group.

Sales for the company are expected to grow about 10% this year and another 10% in 2015, while earnings are expected to improve almost 30% this year, dropping down to 17% next year.

Having said that, I like PepsiCo for its earnings safety and its dividend. The company is experiencing stagnant growth; therefore, it will endeavor to keep its earnings per share elevated by buying back its own shares and effecting cost-reduction programs.

The global beverage business is alive and well, but it is mature and very slow growth. That’s why the dividend income is there to keep investors interested. It’s a group that you want to own for the income.

But I wouldn’t be enthusiastic about buying the shares at this particular time because they broke out of the recent consolidation and considering expected earnings, the stock is fully valued. If you own it, I would say it remains a worthwhile hold. But that’s not to say that investors should cross PepsiCo off their list altogether.

PepsiCo has proven to be a decent buy when it’s down, and if it were to retrench, it would be worth considering for income-seeking investors.

The fact that the company is two businesses—beverages and snacks—is a good combination and the quality of its brands makes for an investment-grade stock—investors just need to watch for the right time to buy.

Disclaimer: There is no magic formula to getting rich. Success in investment vehicles with the best prospects for price appreciation can only be achieved through proper and rigorous research and analysis. The opinions in this e-newsletter are just that, opinions of the authors. Information contained herein, while believed to be correct, is not guaranteed as accurate. Warning: Investing often involves high risks and you can lose a lot of money. Please do not invest with money you cannot afford to lose.

Original post