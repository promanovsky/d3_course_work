There's now an online store selling all things wearables and no surprise here -- Amazon is behind it.

In a release Tuesday the giant e-tailer said its Wearable Technology store is a one-stop shop featuring top brands such as Samsung, Jawbone and GoPro with devices ranging from smartwatches to activity and fitness trackers to wearable cameras.

"Wearable technology is an exciting category with rapid innovation and our customers are increasingly coming to Amazon to shop and learn about these devices," said John Nemeth, director of Wireless and Mobile Electronics at Amazon in an announcement.

"We're thrilled to bring our customers a store with the largest selection and great prices that helps eliminate the guesswork when deciding which wearable devices best fit their needs-whether that is tracking activity, staying connected through smart watches or capturing their next adventure with wearable cameras," states Nemeth.

The news comes as vendors and consumers are hot and heavy with wearables with innovative devices ranging from smartphones being used as blood and body fluid testers to Google Glass pilots in emergency rooms. New players, such as Narrative and Bionym, are coming into play and the market appears ready for a boom given expectations about device adoption.

Yet the Amazon store seems to understand that a good segment of its consumers may not be so familiar with such devices and has built a "learning center" that provides product videos and buying guides.

"These resources provide information about device compatibility, product comparisons and use-case suggestions to help customers find the right device for them. Customers can also take advantage of the "Editor's Corner" to find information about wearable technology industry news, device reviews and more," states the announcement.

Shoppers can browse by category and compare products, just like any other 'storefront' on Amazon, and have the same tools available. Categories include healthcare, fitness and wellness, cameras, smartwatches and even 'family, kids and pets.'

TAG wearables, Smartwatches, Amazon, Shopping

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.