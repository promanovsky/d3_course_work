After 10 years battling the incurable blood cancer called multiple myeloma, Stacy Erholtz, of Pequot Lakes, Minn., has now been disease-free for over six months -- thanks to the measles virus.

Erholtz and one other multiple myeloma patient were part of a recent experimental trial conducted by Mayo Clinic researchers demonstrating that cancer cells can be killed with injections of a genetically-engineered virus through a process known as virotherapy. The findings were published in the journal Mayo Clinic Proceedings.

"This is the first study to establish the feasibility of systemic oncolytic virotherapy for disseminated cancer," study author Dr. Stephen Russell, M.D., Ph.D., a Mayo Clinic hematologist and co-developer of the therapy, said in a statement. "These patients were not responsive to other therapies and had experienced several recurrences of their disease."

Erholtz said the results were almost immediate. "I had a [tumor] right here on my forehead the size of a golf ball," she told USA Today, "and within 36 hours it was gone."

Virotherapy dates back to the 1950s, according to the Mayo Clinic, and thousands of cancer patients have been treated with a host of various viruses. "However, this study provides the first well-documented case of a patient with disseminated cancer having a complete remission at all disease sites after virus administration," according to the Mayo Clinic statement. Because multiple myeloma patients often have weakened immune systems, the measles virus can work in this way even if patients have been vaccinated, the Star Tribune reported. The treatment requires an enormous dose -- enough to inoculate 10 million people, according to Minnesota's KARE 11.

"It's a landmark," Russell told the Star Tribune. "We've known for a long time that we can give a virus intravenously and destroy metastatic cancer in mice. Nobody's shown that you can do that in people before."

The second patient did not respond as well to the treatment, with the cancer returning nine months after the trial, KARE 11 reported. But Russell still sees the finding as cause for a larger clinical trial, and hopes for FDA approval of the treatment in the next four years.

"It's the way of the future," Erholtz told USA Today, "and I'm so excited for other people to experience this."

Hear more from Erholtz and Russell in the video below: