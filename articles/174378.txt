The SIM free Sony Xperia Z2 has been up for pre-order in the UK from a number of retailers for the last few weeks, and now Clove has announced that they will start shipping the device out from Friday the 9th of May.

The SIM free Sony Xperia Z2 is unlocked, which means you can use it with any of the mobile carriers without the need for a contract, and the device will retail for £540 including taxes in the UK.

The handset comes with some impressive specifications, which include a 5.2 inch display that has a full high definition resolution of 1920 x 1080 pixels.

Under the hood of the Sony Xperia Z2 we have a quad core 2.3GHz Snapdragon 801 mobile processor and 3GB of RAM, there is also 16GB of built in storage.

Should you need additional storage, the Sony Xperia Z2 is equipped with a microSD card slot that offers support for cards up to 128GB.

On top of that the device features Sony’s flagship mobile camera, on the back of the handset we have a 20.7 megapixel Sony Exmor RS camera, on the front there is a 2.2 megapixel Sony Exmor R camera.

You can find out more information about the SIM free Sony Xperia Z2 over at Clove at the link below, the handset will be available from the 9th of May.

Source Clove

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more