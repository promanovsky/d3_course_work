WASHINGTON - First lady Michelle Obama is answering Republicans in Congress who want to roll back healthier school meal standards, holding an event at the White House to highlight the success of the health guidelines.

The Tuesday event is an unusual move for the first lady, who has largely stayed away from policy fights since she lobbied for congressional passage of a child nutrition law in 2010.

Sam Kass, director of Mrs. Obama's Let's Move initiative to combat childhood obesity, said a Republican bill that would allow schools to opt out of the standards is "a real assault" on administration efforts to make foods healthier for kids.

Kass, who is a White House chef, said the event is an attempt to point out the successes of some schools that have implemented the standards, which were set by the Obama administration and Congress over the past several years. School nutrition officials from New York to Los Angeles will speak about how well the standards are working in their schools.

"She wants to have a conversation about what is really happening out in the country," as opposed to Washington, Kass said. "These standards are working."

An agriculture spending bill approved by a House subcommittee last week would allow schools to waive the standards if they have a net loss on school food programs for a six-month period. Rep. Robert Aderholt, R-Ala., who wrote the bill, said he was responding to requests from school food directors. The House Appropriations Committee is expected to approve the spending bill this week.

The standards championed by the first lady have been phased in over the past two school years, with more changes coming in 2014. The rules set fat, calorie, sugar and sodium limits on foods in the lunch line and beyond.

Sign up for coronavirus updates Get the latest on the fast-moving developments on the coronavirus and its impact on Long Island. By clicking Sign up, you agree to our privacy policy.

A note to our community: As a public service, this article is available for all. Newsday readers support our strong local journalism by subscribing. Please show you value this important work by becoming a subscriber now. SUBSCRIBE Cancel anytime

While many schools have had success putting the rules in place, others have said they are too restrictive and costly. Schools pushing for changes say limits on sodium and requirements for more whole grains are particularly challenging, while some school officials say kids are throwing away fruits and vegetables that are required.

The Agriculture Department, which administers the rules, has tweaked them along the way to try to help schools that have concerns. The department scrapped limits on the amount of proteins and grains that kids could eat after students complained they were hungry. Last week, USDA announced it would allow some schools to delay serving whole grain pastas just hours after the House subcommittee approved the opt-out language.

School groups have been split. The School Nutrition Association, which represents school nutrition directors and companies that sell food to schools, has lobbied for changes to the standards, saying some are too restrictive. The president of the group said Tuesday that the school nutrition officials speaking to the White House aren't representative of those who have concerns.

"Our request for flexibility under the new standards does not come from industry or politics, it comes from thousands of school cafeteria professionals who have shown how these overly prescriptive regulations are hindering their effort to get students to eat healthy school meals," said SNA's Leah Schmidt.

The national PTA is pushing lawmakers to keep the standards intact.

"At a time when families are working hard to live healthy lives, school meals should be supporting families' efforts, not working against them," PTA President Otha Thornton wrote to members of Congress.