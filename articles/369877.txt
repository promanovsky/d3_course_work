What’s the name of the show? Extant. No, that is not a spelling error. No, we do not know what it means.



When does it premiere? Wednesday, 9 July, at 9pm on CBS in the US; 10 July on Amazon Prime Instant Video in the UK

What is this show about? Halle Berry, in her first recurring TV role (she has an Emmy for playing Dorothy Dandridge in an HBO movie), plays infertile astronaut Molly Woods, who returns from a year alone aboard a space station to find out that she is pregnant. No one can figure it out. She also has a husband (ER’s Goran Visnjic) who wants her help raising Ethan, a robot baby that he invented.

What’s the show’s pedigree? It is executive produced by Steven Spielberg, which is promising. CBS ordered a 13-episode first season without seeing a pilot. It’s believed this is what the kids (and by kids I mean television executives) are calling an “event series”, which is what we used to call a miniseries. However, CBS did a one-season wonder with Under the Dome last summer; now that millions of people watch it every week, it’s back for an improbable second season.

What happens in the premiere? Well, like I said, Molly comes back from space pregnant. But she's covering it up from the private aeronautics company that put her in space. The company is run by the shadowy Hideki Yasumoto (Hiroyuki Sanada), the same man funding Molly’s husband and his brood of robot babies with completely human emotions. Her robot baby freaks out over ice cream and maybe kills a bird. Also we find out who might have impregnated Molly (spoiler alert: it could be the ghost of her dead first husband).

Which characters will you love? Halle Berry, duh, though her Molly seems mousey and disengaged most of the time. Yasumoto seems like a great villain (or is he?), quiet, brooding and meddlesome, but with just enough darkness behind the eyes to be interesting.



Which characters will you hate? You will definitely hate Molly’s short-tempered husband John, who thinks that robot babies are just as good as human babies. Also robot baby Ethan is the full-on creepy sci-fi kid that we’ve seen in every schlocky horror movie.



Are any of Meryl Streep’s children on this show? Yes, Grace Gummer plays John’s assistant or something.

Why does the title go from “Extinct” to “Extant” in the promos? I have no idea. So far there is no extinction, unless Halle Berry’s alien fetus is somehow going to destroy us all. (It could.)

What does the future look like? There are lots of cool inventions in the future. There are space shuttle toys that can actually fly; there are bathroom mirrors that you can touch that will show you the news and weather; there is a weird, clear trash receptacle at the end of the driveway that makes waste disappear; there are computers you control using magic gloves; there are translucent iPads; there is a Siri-like talking computer device in every home and space station; there are photo albums where the photos actually move (so they’re basically animated gif albums). And, yes, there are robot babies. Oh, there are also really chunky sweaters that all the men have to wear, and jumpsuits that are so awful that they are even unflattering on Halle Berry, possibly the sexiest woman to ever live.

How does Halle Berry’s hair look? Mostly good, except when she’s in space.



Is this show any good? The premiere episode is slick, tense, and well-plotted. The special effects are very good for TV, but the whole production has a polish that makes the future look like an architect’s rendering of a new park rather than an actual place. The plot seems to be a pastiche of rather tired sci-fi tropes. There’s the robot baby from AI, the alien baby from Prometheus, and the potentially rogue ship computer from 2001: A Space Odyssey. This isn’t necessarily a bad thing. If the show goes on to subvert these tropes in an interesting manner, or uses them to talk about our fears of fertility and child rearing in the modern age, then it could be insightful and intelligent. However, this is CBS, home of The Big Bang Theory, so it probably won't get that philosophical.



The biggest problem the show faces is that all the characters seem like real duds, at least so far. We’re supposed to root for Molly, but her reaction to finding out that she is inexplicably pregnant is so calm and measured that she may as well have just learned her favorite frozen yogurt store went out of business. The future may be full of wonderful toys, but apparently the only emotions that anyone has are deep confusion and mild annoyance. Maybe we just need to get to know them better?

What’s the best thing about it? The curiosity it gins up about just what is in Halle Berry’s tummy (and why this shady corporation seems to have let it happen).

What’s the worst thing about it? Just about all of the acting.

Should you watch this show? Yes, at least for a few episodes, and especially if you are a sci-fi (or Halle Berry) fan. If it does find something new and interesting to do with its basic subject matter of aliens and androids, it could be a deep, interesting mystery. Considering the CBS press release says that Halle’s phantom fetus changes humanity forever, it’s definitely an ambitious secret. However, if the show holds out on the clues or none of the characters get more nuanced or interesting, it’s probably best to give this the chop after episode four. And no matter what, don’t bother with season two, even for a robot baby of your own.