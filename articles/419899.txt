In May of this year, Apple VP Eddy Cue teased a massive product lineup coming soon.

Speaking at Re/code's annual conference, Cue said, "Later this year, we've got the best product pipeline that I've seen in my 25 years at Apple. I believe the products we've got coming are great."

That product pipeline is coming into focus. Here's what to expect in the coming months.

Two New iPhones

The next iPhone will probably look like this. ConceptsiPhone / YouTube

According to numerous reports, Apple will announce two new iPhone models on Sept. 9. One iPhone will have a 4.7-inch screen. The other will have a 5.5-inch screen. (The iPhone 5S has a 4-inch screen, which is smaller than most high-end phones today.)

A Wearable Wrist Computer

An artist's concept of the so-called iWatch. Behance/Todd Hamilton

The so-called iWatch will debut on Sept. 9, too, according to Re/code's John Paczkowski. The device will be able to track your health and fitness and let you control stuff in your home. It's also been said to include a curved display.

A Refresh For The iPad Air And iPad Mini

The iPad Air. Apple

According to Bloomberg, Apple has started producing new versions of the iPad Air and iPad Mini. Details are scarce, but Bloomberg hinted that the new tablets will have a special anti-glare coating on the screen. Assuming Apple sticks to its pattern, the new iPads will only be more powerful. They will probably look the same. There's aso a chance Apple could include the same fingerprint sensor it uses on the iPhone 5S.

Expect those new iPads around October.

A Giant iPad With A 12.9-Inch Screen

The big iPad won't be *this* big though. Business Insider

Another Bloomberg report points to an even larger iPad coming in early 2015. That iPad will supposedly have a 12.9-inch screen. For reference, the iPad Air has a 9.7-inch screen.

Why a bigger iPad? It'll likely be focused on productivity.

A New MacBook With A Sharper Screen

The MacBook Air. Flickr/Tim Trueman

There have been scattered reports that Apple is working on a new MacBook with a 12-inch screen. The device could be a new kind of MacBook Air and have a super-sharp Retina Display, just like Apple uses on iPads, iPhones, and the MacBook Pro.

What Else?

AP

There was some chatter earlier this year that Apple would introduce a new version of the Apple TV set-top box. However, because Apple reportedly had an agreement with Time Warner Cable to provide some content for the box, the plans may have been stalled when Comcast said it wanted to buy Time Warner for about $45 billion.

After the merger is finished and the dust settles, Apple could give it another go.