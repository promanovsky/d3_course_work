WARNING: Graphic image. A medical team has painstakingly repaired the disfiguring injuries to a woman's face, caused by radiation treatments for a cancer she never had that caused a gaping hole in her cheek and made her an outcast in a former Soviet republic.

Lessya Kotelevskaya was recovering following the 16-hour surgery the day before at University of Louisville Hospital.

Her surgeon, Dr. Jarrod Little, said the procedure to reconstruct her jawbone and cheek went according to plan.

"Lessya cannot wait to get back to her normal life," her cousin, Oleg Sennik, told reporters.

The 30-year-old's life spiraled into tragedy when she was diagnosed with terminal jaw cancer at age 19 in Kazakhstan after she was accidentally elbowed in the face at a basketball game and her jaw became swollen. The damage from radiation treatments made it difficult to eat and talk.

Sennik spent years searching for his younger cousin, and when he found her she was a mere 79 pounds (36 kilograms) and living in the shadows of life in Kazakhstan, where the Ukrainian native had lived since childhood. By the time she found out the cancer diagnosis was wrong, she had lost her husband and their clothing boutique. She scraped by for years with odd jobs at night so people wouldn't see her. At one point, she lived in the utility room of a car wash.

"She was rejected everywhere she went before," her cousin said.

Sennik brought Kotelevskaya and her young son to live with him last year in Louisville, where they found medical care to turn around her life.

The surgery included removing a leg bone that was conformed into a new jawbone, with the skin becoming the new inside covering of her mouth.

Dr Jarrod Little speaks about the 16-hour surgery he performed to repair damage to the face of Lessya Kotelevskaya. Photo: AP

"It couldn't have gone any better," said Little, a plastic and reconstructive surgeon with University of Louisville Physicians.

Before the procedure, Kotelevskaya could barely open her mouth and had to patch the hole in her right cheek to keep food and drink from seeping out. Now, she'll be able to open her jaw without problem, Little said.

Story continues

Kotelevskaya was not incurring expenses for the care. The surgery was described as a $1 million-plus procedure by a UofL Physicians spokeswoman.

"She came with no money," said Little, who donated his time. "She didn't have anything. With this devastating problem ... her insurance status at that point is irrelevant to me. She needs help and we can help her."

Kotelevskaya is expected to remain hospitalized for two to three weeks, Little said. She will likely need "touch up" procedures later, he said. She can also receive dental implants in about six months.

Kotelevskaya is in this country on a green card but hopes to become an American citizen, her cousin said.