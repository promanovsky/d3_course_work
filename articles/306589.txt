Amazon unveiled its highly anticipated Fire Phone smartphone and is now taking preorders. The 32GB retails for $199, while the 64GB is $299 when signing a new 2-year contract with AT&T.

The idea of Amazon launching its own smartphone has been rumored for years. It was often referred to as the 'Kindle Phone' and would run a modified version of Android, just like the company's Kindle Fire tablets. The device was also said to include a 3D user interface that would offer an advanced user experience not seen on any mobile device.

Amazon has dominated the standalone e-book reader world ever since it launched its first Kindle in 2007. The Kindle had a 6-inch 4-level gray scale display and 250MB of internal storage. When Amazon began taking online orders for the Kindle, the company hadn't anticipated just how popular the device had become leading up to its release. The original Kindle sold out in five and a half hours and had iPad-like pricing of $399. The device remained out of stock for 5 months before Amazon was finally able to meet demand.

Amazon's Kindle Fire tablets have been well received by tech critics and consumers for its price and exclusive access to Amazon's huge ecosystem. As sales of tablets continue to decline, Amazon is now looking to keep its customers tied into its offerings via a state of the art smartphone that is simple, intuitive and fun to use.

Amazon CEO, Jeff Bezos announced the Amazon Fire Phone at a press event today. "Fire Phone puts everything you love about Amazon in the palm of your hand-instant access to Amazon's vast content ecosystem and exclusive features like the Mayday button, ASAP, Second Screen, X-Ray, free unlimited photo storage, and more," said Jeff Bezos, Amazon.com Founder and CEO.

The Fire Phone uses a feature called Dynamic Perspective, which uses a custom-designed sensor system that allows the handset to perform different functions just by moving the device. A feature called Tilt allows users to navigate the smartphone without touching it; tilt to the left and you'll open a panel to navigate menus easily with one hand. Tilt it to the right and you're presented with different shortcuts for messages, photos, etc. You can even scroll the display by tilting the Fire Phone; Samsung has offered a similar feature in many of its Galaxy smartphones.

Amazon is now taking pre-orders for the Fire Phone and is offering the 32GB for $199 "the same as other 16GB smartphones" the company proudly proclaims and $299 for $64GB. To receive those subsidized prices, you'll need to sign a new 2-year contract with AT&T, the Amazon Fire Phone's exclusive carrier. Amazon is also offering one full year of Amazon Prime for one year, which allows access to Amazon's premium and paid services.

The Fire Phone features a 4.7-inch 1280 x 720 LCD display and is powered by a 2.2GHz quad-core Qualcomm Snapdragon 800 processor, Adreno 330 GPU (graphics processing unit), and 2GB of RAM. It runs Fire OS 3.5.0, has a 13-megapixel rear camera and 2.1-megapixel front facing camera. Its 2400 mAh battery will provide up to 22 hours of talk time and up to 285 hours of standby time.

You can pre-order the 32GB or 64GB Amazon Fire Phone now and receive it on July 25.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.