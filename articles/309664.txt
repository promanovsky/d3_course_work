The bee hive is seen on the South Lawn of the White House in Washington on April 9, 2009. (UPI Photo/Roger L. Wollenberg) | License Photo

WASHINGTON, June 20 (UPI) -- The ongoing mystery about why bee populations are declining and what to do about it has attracted the attention of the White House. Today, President Barack Obama created a new task force, putting a group of federally-backed scientists on the case.

In the memorandum announcing the new task force, Obama called on the participating experts to develop a strategy within 180 day for addressing and curbing the worrisome decline of honeybees, butterflies and other pollinators.

Advertisement

"Given the breadth, severity, and persistence of pollinator losses, it is critical to expand Federal efforts and take new steps to reverse pollinator losses and help restore populations to healthy levels," the president wrote in the memorandum.

Many theories exist for why bees and butterflies have been on the out and out -- pesticides, viruses, parasites, habitat loss -- but there's been little consensus among the scientific community.

The memorandum also calls on the Environmental Protection Agency to research the role of a class of pesticides called neonicotinoids, which have been implicated in the bee decline.

The European Union has already put restrictions the use of neonicotinoids, and Tiffany Finck-Haynes, a food campaigner for Friends of the Earth US, says Obama should work to do the same.

"He could restrict neonicotinoids today as the European Union has done and he should do that if he wants to protect our pollinators, our food systems, and our environment," Tiffany said.

The move by the White House, Friday, bolsters the presidents previous budget request for $50 million to study the factors behind bee losses and to protect pollinator habitat on federal lands.