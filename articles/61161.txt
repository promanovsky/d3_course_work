Tesla Motors Inc (NASDAQ:TSLA) got a Neutral rating and a price target of $230 from UBS analyst Colin Langan in a report released on 26 March 2014. The analysts praised the “disruptive model” of the company, but cautioned investors that the share prices today reflect achievements that are years down the road.

“[I]nvestors should appreciate that the downside this early in its life is material,” Langan writes. “The failure of a current or future product could quickly unravel all the progress.”

Future catalysts already priced in

Langan compared Tesla Motors Inc (NASDAQ:TSLA) and founder Elon Musk to other similar disruptive companies including Steve Jobs and Apple Inc. (NASDAQ:AAPL), and Amazon’s Jeff Bezos. In the automotive industry, a big challenge is to increase production, which is similar to tasks faced by Jobs and Bezos.

Giving an example of two less popular companies; Palm and Netscape, the analyst notes that both companies had innovative products that became popular with users, but both of them failed to capitalize on the early success to lay the foundation of a long lasting business. So, when rivals entered the market both Palm and Netscape were easily over taken.

People comparing Tesla Motors Inc (NASDAQ:TSLA) to Amazon and Apple are correct if one compares the stock chart of all three companies over the last decade or so, believes the analyst. Moving ahead, Tesla will face a lot of hurdles. Many of the significant near term catalysts are already reflected in the price, so the analyst believes the bulk of trading would be driven by daily news like New Jersey banning Tesla from selling cars directly to customers or the stock flirting with the 50-day moving average around $209.

Tesla, a perfect case study

Langan also expects a major decline in revenues related to the zero emission vehicle credits. Such revenue will directly impact the bottom line.

In a blog post last week, NYU finance professor Aswath Damodaran revised his stance on Tesla Motors Inc (NASDAQ:TSLA). The veteran admitted that Tesla has made some serious progress since he last valued the share to be around $67, last September. However, Damodaran still feels that the current price levels are not justified, and pegs the current value of shares between $99 and $119.

Damodaran views Tesla as “a perfect case study” for studying trading against investing dilemma “where you reject a stock as overvalued, only to see the stock price increase even further.”

On Wednesday, Tesla Motors Inc (NASDAQ:TSLA) shares were down 1.2% at $217.87. Year to date the stock is up 44%, and 473% in the last 12 months.