Over the years Kelly Osbourne has shared her outrageous and often colorful tattoos with the public. While some of those tattoos are on her list of regrets, Osbourne decided to get a very loud and risky tattoo in the last place people would expect.

It seems like old habits die hard. The Fashion Police panel member turned her life around after the MTV reality series The Osbournes, but still proves that you can’t completely take that rock and roll spirit out of the girl. Kelly shared her newest tattoo with everyone via Twitter and it’s a doozy.

Osbourne decided to take her already pretty radical shaved head to the next level, by inking herself with a tattoo on the side of her head. Posing with the tattoo artist, Kelly showed off her newly inked head. Remaining calm and collective during the tattoo session a photo shows her getting the work done, and afterward with the finished product.

Here’s a few photos of what Kelly looked like before she got her head branded with a new tattoo.

Here’s some from her tattoo session.

Sorry mum and dad but I love it! pic.twitter.com/xIiMwIJBT9 — Kelly Osbourne (@KellyOsbourne) June 29, 2014



Finally here’s Osbourne with the finished product. We’re sure she has some “stories” behind this tattoo to tell.

Although she’s proud to show off her new tattoo, she’s admitted over the years that she’s planning to get rid of her old tattoos she got while she was a teenager. It looks like this is a case of out with the old and in with the new.

Back in 2010 she told British magazine Closerthat she had plans to remove most of her fifteen tattoos. The decision happened while she was filming her movie Should’ve Been Romeo after make up artists had to cover her tattoos with makeup.

“I can’t say I missed them! I met with my dermatologist and I’ll start the laser treatment as soon as I have time. It will take several months, and while I’m not looking forward to the pain, I’ve heard it hurts less than getting the tattoo.”

Back in 2013 Osbourne opened up to Chelsea Handler about the process of removing some of her tattoos. At the time she was removing her arm tattoo.

“I’ve been starting to do my tattoo removal and right underneath you can start to see where the swelling is. It looks like I got bit by a snake, it’s kind of cool. I have a couple of tattoos that aren’t my best choices in life.”

Still that doesn’t mean she hates all of her tattoos. She explained to the British magazine.

“Some still have a special meaning, like the matching ones my brother Jack and I have of each other’s names, but now I feel like some were a mistake.”