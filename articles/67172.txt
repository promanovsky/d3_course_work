Actor Michael Chiklis arrives for Elyse Walker's The Pink Party at Santa Monica Airport in Santa Monica, California on October 19, 2013. The annual event benefits the Womens Cancer Program at Cedars-Sinai Samuel Oschin Comprehensive Cancer Institute. UPI/Jim Ruymen | License Photo

Actor Michael Chiklis is the latest to join the ranks of the American Horror Story elite in its fourth season, Freak Show.

The television show, which changes gears and tells a new story each season, will feature Chiklis as the father of a character played by Evan Peters and the ex-husband of the character played by Kathy Bates.

Advertisement

Also returning for the fourth season of the show is Jessica Lange, Sarah Paulson, Angela Basset and Frances Conroy.

The season is set in 1950 Jupiter, Fla., and will star Lange as the manager of one of the last-remaining freak shows in the United States.