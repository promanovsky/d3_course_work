Just two months after the death of his fashion designer girlfriend L’Wren Scott, Mick Jagger has received some joyous news. His granddaughter, Assisi, has given birth to a baby girl.

Her arrival creates a remarkable milestone for Rolling Stones frontman Jagger, 70, who can arguably claim to be the first great grandaddy of rock ‘n’ roll.

We will have to wait a few more weeks to see if the sprog has inherited his trademark pout as Assisi’s mother, Jade, is believed to have negotiated a lucrative deal with a magazine for a photoshoot.

At 21, Assisi, who lives in a converted barn in Cornwall with her boyfriend, 25-year-old chef Alex Key, is just a year older than Jade was when she gave birth to her. Assisi said in an interview: “The women in our family just tend to have children young, it runs in the genes.”

Jade, 42, is herself expecting her third child this month with her graphic designer husband Adrian Fillary. She had two daughters by Piers Jackson — Assisi, and Amba, 17.

Jade and Assisi partook in a rather unusual mother-daughter baby shower in March, following the revelation that Jade was six months pregnant alongside Assisi, who announced her own pregnancy in November.

As befitting the broad-minded Jagger household, the baby shower was thrown by Jerry Hall, the Texan model widely considered responsible for breaking up Jade’s parents, Mick and Bianca Jagger, in the 1970s.

Jagger, who has seven children with four women, and four grandchildren, spent last week in rehearsals with his Stones bandmates ahead of their performance in Norway on May 26. It will be the first time the group has been on stage since cancelling the Australian and New Zealand leg of their On Fire tour after the death of L’Wren Scott.

The 49-year-old fashion designer, who was found hanged in March, had been in a relationship with Jagger since 2001.