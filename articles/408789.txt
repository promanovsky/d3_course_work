'Guardians of the Galaxy' described as 'pirates for good'

Spacefaring heroes of the Marvel Universe star in this week’s “Guardians of the Galaxy” film. The team has a long history in Marvel Comics, associated with several characters.

Brian “Buck” Berlin of New World Comics will host a “Guardians of the Galaxy” superhero school at 10:30 a.m. Saturday for kids and fans to find out more about these cosmic heroes. Cosplayers dressed as the Guardians will answer questions about the Guardians characters: Star-Lord, Gamora, Drax, Rocket Raccoon and Groot.

The slightly offbeat characters include a talking raccoon and a sentient tree that says “I am Groot!”

Current “Guardians of the Galaxy” writer Brian Michael Bendis recently described the Guardians characters as “pirates for good.”