On last night's TONIGHT SHOW, after trading Easter stories, Jimmy Fallon and Mad Men star Christina Hendicks try on her favorite item of clothing, the kaftan dress. Check out last night's appearance below!

About The Tonight Show

Beginning Feb. 17, 2014, "The Tonight Show" returns to its New York origins when "The Tonight Show Starring Jimmy Fallon" makes its broadcast debut from Studio 6B in Rockefeller Center. Emmy Award- and Grammy Award-winning comedian Jimmy Fallon will bring a high-tempo energy to the storied NBC franchise with his welcoming interview style, love of audience participation, spot-on impersonations and innovative sketches.

An American television institution for almost 60 years, "The Tonight Show" will continue to be a home to big-name celebrity guests and a stage for top musical and comedic talent. Taking a cue from his unforgettable predecessors, including hosts Johnny Carson and Jay Leno, Fallon will carry on the tradition that audiences know and love - kicking off every show with the iconic "Tonight Show" monologue. Known for his huge online presence, Fallon will also bring along with him many of the popular segments, celebrity sketches and musical parodies that fans have grown to love on "Late Night," including #Hashtags, Thank You Notes and Slow Jam the News.

Critically praised Grammy winners The Roots will serve as "The Tonight Show" house band.

From Universal Television and Broadway Video, "The Tonight Show Starring Jimmy Fallon" is executive produced by Lorne Michaels and produced by Josh Lieb. Gavin Purcell produces. "The Tonight Show" tapes before a live studio audience.