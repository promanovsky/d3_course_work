A Mayo Clinic trial shows that the measles virus put one woman’s cancer completely into remission.

Could it be? Could measles be the key to killing cancer?

Related story You Should Know the Names of These Black Women Health Heroes

Virotherapy, which involves destroying cancer by injecting the body with a virus that kills cancer cells but doesn’t do the same to other normal tissues, can be effective in fighting multiple myeloma, a form of cancer. It is a cancer of plasma cells in the bone marrow that also leads to skeletal or soft tissue tumors. It is rarely cured, though it temporarily can respond to drugs that stimulate the immune system.

The Mayo Clinic studied two patients who received a single dose intravenously of an engineered measles virus (MV-NIS) that is toxic to myeloma plasma cells. In both patients, there was a decrease of bone marrow cancer and myeloma protein. One patient, a 49-year-old woman, has been in remission for more than six months.

www.youtube.com/embed/LImk-KdMT1w

“This is the first study to establish the feasibility of systemic oncolytic virotherapy for disseminated cancer,” said Stephen Russell, M.D., Ph.D., a Mayo Clinic hematologist and co-developer of the therapy. “These patients were not responsive to other therapies and had experienced several recurrences of their disease.”

The researchers say they reported on the two patients because they were the first that were on the highest possible dose and had limited previous exposure to measles (therefore, they had fewer antibodies). Also, they had no remaining treatment options.

Oncolytic virotherapy isn’t anything new, though, as its history dates back to the 1950s. But this study is the first to show that a patient with disseminated cancer can experience complete remission at all disease sites after being given the virus.

It’s such a big breakthrough that more of the MV-NIS therapy is being manufactured for a larger phase 2 clinical trial. The researchers said they also want to assess how well it works along with radioactive therapy using iodine-131.

Maybe it’s not a “cure” for cancer, but for now — at least for one woman — it is.

Recent health headlines

You can prevent cancer, says World Health Organization

E-cigarettes are not actually a healthy smoking alternative

New skin cancer threat: Manicures