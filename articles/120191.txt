DAKAR, Senegal -- Gambian authorities have written a letter to airlines flying into the West African country saying they cannot pick up passengers from countries where there have been suspected cases of Ebola.

An ongoing outbreak of the virus has claimed more than 100 lives in Guinea and Liberia. Senegal closed its land border with Guinea, and the Conakry airport has instituted health checks for departing passengers.

A letter from the Gambian Transport Ministry addressed to four airlines instructs them not to pick up passengers in the capitals of Guinea, Liberia or Sierra Leone. The letter, dated April 10, was obtained Monday by The Associated Press.

It does not mention Ebola, but Guinea and Liberia have been the epicenter of the disease. Sierra Leone was believed to have some cases at one point.