Apple

The hack affected a number of Apple users across Australia and New Zealand (as well as people using Australian devices overseas) and saw iPhones, iPads and Macs hit with messages threatening to wipe the devices unless US$100 was paid to a fake PayPal account in the name of Oleg Pliss.

Apple has released a statement on the hack to ZDNet, advising users to change their password and contact the company's customer service team or an Apple store for more assistance.

The Apple statement reads in full:

"Apple takes security very seriously and iCloud was not compromised during this incident. Impacted users should change their Apple ID password as soon as possible and avoid using the same user name and password for multiple services. Any users who need additional help can contact AppleCare or visit their local Apple Retail Store."

In the hours following news of the hack, Australian service providers were directing affected users to contact Apple if they had concerns. PayPal also reassured consumers saying that the Hotmail address listed by Oleg Pliss was not actually linked to a PayPal account and that user details and money were not compromised.