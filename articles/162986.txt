Measles cases are at their highest level in the U.S. for this point in the year since 1996, so public health officials are urging parents to vaccinate their children and are reminding health-care providers how to recognize the illness.

The U.S. Centers for Disease Control and Prevention said Thursday that 129 cases have been reported in 13 states since mid-April, mainly in California and New York City.

As of Wednesday, B.C.'s outbreak in eastern regions of the Fraser Valley has 416 confirmed cases of measles. Since fewer new cases and transmissions are occurring, the health authority hopes to declare the most intense period of the outbreak over soon, a spokeswoman said.

The Public Health Agency of Canada says cases have also been reported this year in Alberta, Saskatchewan, Manitoba and Ontario.

"This is the most measles cases reported in the first four months of the year since 1996," Dr. Anne Schuchat, the U.S. assistant surgeon general and director of the U.S. National Center for Immunization and Respiratory Diseases, told reporters.

"Measles has gotten off to an early and active start this year."

Half of the imported cases, or 17, were among returning American travellers or visitors from the Phillippines, where a recent measles epidemic has caused at least 20,000 illnesses.

Shuchat stressed that borders can't stop measles but vaccinations can.

The success of measles vaccination campaigns over decades means many younger doctors have never seen a case and some older ones could use a refresher on diagnosing it quickly and nipping its spread.

Among the 58 cases reported from California, at least 11 were infected in doctor's offices, hospitals or other health-care settings, according to a report released Thursday by CDC. New York City health officials say two of their 26 cases were infected in medical facilities.

To that end, a new commentary in the Annals of Internal Medicine offers doctors advice on how to isolate suspected measles patients.

Rapid control needed

"As more parents decline to vaccinate their children, measles incidence is increasing — a fact that alarms me both as a hospital epidemiologist and as a parent of a vulnerable infant too young to receive the measles vaccine," wrote Dr. Julia Shaklee Sammons, an infectious disease specialist at the Children's Hospital of Philadelphia.

"For suspected cases of measles, early reporting and rapid control efforts are vital to prevent spread in health-care facilities. Providers should encourage patients and their parents to call before coming into medical offices or emergency departments so that appropriate precautions can be taken."

If measles is suspected, precautions include isolating the patient in a room with special ventilation to keep the air from circulating around the building, having the patient put on surgical wear and donning surgical masks or respirators for health-care workers.



The measles virus spreads easily through the air and in closed rooms. Infected droplets can linger for up to two hours after the sick person leaves.

Measles causes a fever, runny nose, cough and a rash all over the body.

Unless physicians see a measles rash consistently, they may not be able to distinguish it from other rashes, said Dr. Mel Krajden, head of virology at the B.C. Centre for Disease Control. Krajden called vaccination key to containing measles.

Death occurs in one to three per 1,000 reported cases and is most common in young children or people who are immunocompromised, Sammons said.

The vaccine itself is not perfect.

"Today's measles outbreaks are too often the result of people opting out. Most of the people or 84 per cent of the U.S. cases that are reported to have measles this year so far were not vaccinated or didn't know their vaccination status. Of the unvaccinated U.S. residents, 68 per cent had what we call personal belief exemptions," Shuchat said.