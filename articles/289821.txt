Concerns over possible contamination resulted in a recall of 4,012 pounds of beef on Wednesday, June 11. The recall was labeled Class II by the U.S. Department of Agriculture, which says the situation is minor, with low-risk health problems a possibility.

The meat was processed and packaged between September 2013 and April 2014 by Fruitland American Meat, a Missouri-based company. While the cattle showed no signs of mad cow disease, otherwise known as bovine spongiform encephalopathy (BSE), the USDA found evidence of possible contamination in slaughter logs.

The recalled beef included bone-in Rain Crow Ranch Ribeye and quartered beef carcasses, both with establishment number 2316.

The Food Safety and Inspection Service (FSIS) of the USDA reviewed company logs and reported possible errors in the company employees' determination of age of the cattle. An error of this kind is nothing minor when it comes to raising cattle, as cattle that are 30 months and older must undergo surgical removal of the dorsal root ganglia of their nervous systems.

These spinal nerves are labeled "specific risk material" (SRM). "SRMs are tissues that may contain the infective agent in cattle infected with BSE, as well as materials that are closely associated with these potentially infective tissues," says the USDA in a statement made Wednesday.

The presence of SRMs in beef can lead to human diseases such as variant Creutzfeldt-Jakob disease. Found mostly in U.S. patients that travel from overseas, the disease is a variant form of a degenerative brain disease found in one in a million humans. Fortunately, no death in the U.S. from variant CJD has been linked to contaminated beef processed here.

Fruitland's beef products were issued to an unnamed restaurant in New York City and one in Kansas City, Missouri, as well as a Whole Foods distribution center in Connecticut. The center distributed the meat to 34 of its stores in the New England region. Thus far there are no reports of illnesses due to the contaminated beef, and the USDA predicts only a "remote probability" of health issues. The USDA asks that concerns be directed to the Fruitland American Meat company sales manager, and that reactions to contaminated meat be reported to healthcare providers.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.