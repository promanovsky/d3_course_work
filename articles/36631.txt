James Rebhorn, who has appeared in over 100 films, television shows and stage productions, has died aged 65.

The late actor was last seen playing the father of CIA officer Carrie Mathison (Claire Danes) on 'Homeland'.

Rebhorn passed away on Friday (March 21st) in his home due to suffering from skin cancer.

Dianne Busch, who is his agent, confirmed to The Hollywood Reporter.

"He died from melanoma, which had been diagnosed in 1992," Busch stated. "He fought it all this time. He died Friday afternoon at his home in New Jersey, where he had been receiving hospice care for a week and a half."

During a career spanning over five decades, Rebhorn featured in huge Hollywood films such as 'Scent of a Women' (1992), 'The Game' (1997), Independence Day' (1996) and 'Meet The Parents' (2000).

One of the last big budget movies James appeared in was 2011's 'Real Steel' starring alongside Hugh Jackman, he played 'Marvin' who was the rich uncle of 'Man Kenton'.

Rebhorn also had notable roles in several television series, including a recurring role as Special Agent Reese Hughes on USA's 'White Collar' and the district attorney who sent Jerry, Elaine, Kramer and George to jail on the series finale of Seinfeld in 1998.

Once news broke of Rebhorn's death, actress Frances Fisher, who played Rose's mother in 'Titanic,' were among the first to tweet about the late actor.

"Shocked and saddened to hear of James Rebhorn's passing," she posted on Sunday. "Jim was a consummate actor with whom I had the pleasure to work when we were young."

In another statement released by Dianne Busch, she also said: "He had been diagnosed in 1992 but fought it and continued to work as an actor. He had been at home receiving hospice care for the last two weeks. His family was with him. He will be greatly missed."



James Rebhorn died at 65 years-old