The Federal Aviation Administration's (FAA) office of Commercial Space Transportation has published a "Record of Decision" giving approval for a SpaceX Texas Launch Site in Cameron County, Texas.

This decision provides final environmental determination and approval "to support the issuance of launch licenses and/or experimental permits that would allow Space Exploration Technologies Corp. (SpaceX) to launch the Falcon 9 and Falcon Heavy orbital vertical launch vehicles and a variety of reusable suborbital launch vehicles from a launch site on privately owned property in Cameron County, Texas."

SpaceX has proposed to construct and operate a private launch site to accommodate the number of launches that the company has on its launch manifest.

The proposed private launch site is needed to provide SpaceX with an exclusive launch site that would allow the company to accommodate its launch manifest and meet tight launch windows.

SpaceX intends to apply to the FAA for launch licenses and/or experimental permits to conduct launches of the Falcon Program vehicles, and a variety of reusable suborbital launch vehicles, for a total of up to 12 commercial launch operations per year from the proposed launch site on privately owned property in Cameron County, Texas.

The FAA would likely issue launch specific licenses for the first few years of operation from the exclusive launch site. SpaceX may then apply for a launch operator license, which lasts for five years and covers the same family of vehicles.

The decision was signed on July 9 by Dr. George C. Nield, head of the FAA’s Commercial Space Transportation office.

For the FAA's entire Record of Decision text, visit:http://www.faa.gov/about/office_org/headquarters_offices/ast/environmental/nepa_doc

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Follow Miriam Kramer @mirikramer and Google+. Follow us @Spacedotcom, Facebook and Google+. Original article on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.