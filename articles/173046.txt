Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Selena Gomez has reportedly hired Katy Perry's manager after firing her parents last month.

The singer , 21 has hired Bradford Cobb to help run her schedule after she sacked her mum Mandy Teefey and step-dad Brian as her managerial team last month, according to Us Weekly.

Bradford has spent more than a decade in the industry and is recognised for turning artists into pop sensations, with Katy a perfect example.

Selena ended her professional relationship with her parents, who have controlled her affairs since 2007, as she believes she's "outgrown them".

The star has argued with her parents in the past over the direction of her career and her on/off relationship with troubled star Justin Bieber, but it is believed family friction wasn't to blame for their split.

The Spring Breakers actress still remains close with her parents, but they are reportedly annoyed about her decision.

(Image: Rex)

Meanwhile, Mandy and Brian made it clear last month they were unhappy about Selena spending time with Justin, who has had a number of run-ins with the law, as they believe he's responsible for driving her to rehab in January.

A source said previously: "They're doing their best to supervise them. They're tolerating it as long as they know that there is someone who has eyes on them all the time."

It comes after Selena deleted her former friends Kendall and Kylie Jenner, along with other pals, from Instagram.

She posted a thoughtful looking selfie this week , finally breaking her silence.

Sel's fans were quick to compliment the shot, saying she looks flawless and "the definition of beauty".

Here she is with Katy herself: