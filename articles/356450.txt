Members of Doctors Without Borders (MSF) put on protective gear at the isolation ward of the Donka Hospital in Conakry.

Editor's note: Since this piece was originally published, the outbreak has continued to spread. As of July 20, the World Health Organization's tally stood at 1,093 people infected, 660 dead. On July 27, the outbreak again hit headlines after two Americans — a doctor and an aid worker, both working in Liberia to fight the virus — tested positive for Ebola.

NAIROBI, Kenya — Regional health officials from 11 West African countries held an emergency meeting in Accra, Ghana, on Wednesday to discuss how to respond more effectively to the deadliest-ever outbreak of Ebola virus.

So far, it's already claimed at least 467 lives and infected 763 people. Poor health facilities and lack of knowledge mean it's expected to spread further. Late last month Bart Janssens, director of operations at medical charity Doctors Without Borders, warned that the disease was “out of control.”

The situation has since worsened with deaths and cases increasing by 20 percent in just a week, according to World Health Organisation (WHO) figures.

Liberia has made it a crime to hide suspected Ebola sufferers in an effort to halt its spread. Fearful victims have fled hospitals to seek the help of traditional healers, increasing the likelihood the disease will spread.

What is Ebola?

Ebola is a hemorrhagic fever that originates in bats and monkeys. Human-to-human transmission is by direct contact between people, including the dead, and by the exchange of body fluids.

Transmission can be prevented by simple precautions such as washing hands, avoiding contact with the sick and not touching corpses. This last point is more important than you may think: In West Africa, exuberant and close-contact mourning is the norm in burial ceremonies.

Medical workers are often infected through contact with patients and family members by the infected relatives they nurse at home.

What does it do?

Ebola has an incubation period of up to three weeks but once the disease takes hold deterioration is rapid and horrific.

The first symptoms are flu-like but within days muscle pains and headaches turn to vomiting and diarrhea, then kidney and liver failure that can lead to the uncontrollable internal and external bleeding that makes Ebola such a fearful sickness.

There is no vaccine. Ebola is fatal in up to 90 percent of cases, according to the WHO.

Why is this outbreak so worrying?

Ebola is usually a localized, rural disease, but this outbreak has a broad geographic spread and is reaching cities too.

Ebola takes its name from a river in the Democratic Republic of Congo near to a small village that was among the first places the disease was identified in 1976. There have been subsequent regular — but relatively small — outbreaks of Ebola in often isolated parts of tropical sub-Saharan Africa. (Different strains found in China and the Philippines are harmless to humans.)

The current West African outbreak was first identified in rural southern Guinea in February. By April it had reached the capital Conakry, a city of 2 million people.

It has since spread across the region’s unmarked and unguarded borders to neighboring Sierra Leone and Liberia, and moved from the countryside into more densely-populated urban areas, raising fears that it could spread still faster infecting many more people.

Will it spread out of the region, or across the Atlantic?

In the past, Ebola outbreaks have fizzled out close to where they began as victims died before they could travel far and spread the disease. Although the kind of nightmare scenarios imagined in the Hollywood films "Outbreak" and "Contagion" are unlikely to be realized, the fear is real: Cases have now been identified in capital cities with international airports rather than isolated villages deep in the forest as in the past.

The solution to the current outbreak in West Africa is to get countries working together to identify then isolate individuals and quarantine areas where the disease is found, especially border areas, and to quickly spread information to communities about what Ebola is and how it's transmitted.

But for most of those already infected, whatever action is agreed upon at the emergency summit in Accra will be too little and too late.