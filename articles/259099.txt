NEW YORK (TheStreet) -- BioCryst Pharmaceuticals (BCRX) - Get Report rose Tuesday after the company announced its experimental drug for treating a rare immune disorder was effective in decreasing the disease's attacks in a mid-stage trial.

The trial compared the drug, BCX4161, to a placebo and tested it in patients with hereditary angioedema (HAE), a genetic disease that manifests with sudden attacks of swelling of the skin or the mucous membranes. The attacks can disfigure, cause pain and could even threaten patients' lives.

The results showed those treated with the drug had an average of 0.82 attacks per week, compared to 1.27 attacks per week for those given a placebo.

Must Read: Warren Buffett's 25 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

The stock was up 8.83% to $9.80 at 11:17 a.m.

BCRX data by YCharts

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.