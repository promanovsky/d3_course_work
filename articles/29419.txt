Twitter Inc (NYSE:TWTR)’s website has been blocked in Turkey. The ban on the social networking site came just hours after Turkish prime minister Recep Tayyip Erdogan threatened to eradicate Twitter. “We will wipe out all of these,” he proclaimed Thursday. Now anyone trying to access the microblogging service in Turkey is directed to another website that lists three court rulings as reasons for the ban.

International community criticizes the ban on Twitter

In the past, Recep Tayyip Erdogan has also threatened to shut down YouTube and Facebook Inc (NASDAQ:FB)’s services in the country. But President Abdullah Gül opposed his view. The social networking services have enabled users to highlight corruption allegations on Erdogan’s inner circle. Erdogan said he doesn’t care what the international community has to say about it. He has the ability to use a court order to block social networking services.

The state-owned Anatolia news agency said the government had to block access to Twitter Inc (NYSE:TWTR) because it had ignored numerous Turkish court orders to remove certain links. Twitter responded through its official @policy feed that users in the country can still get around the ban by tweeting through their mobile text services. Neelie Kroes, the EU commissioner for digital agenda, criticized Turkey’s move, saying that the block is “pointless, groundless and cowardly.” She said that the international community will see it as censorship.

Erdogan himself was quite active on Twitter

Erdogan has been leading Turkey since 2003. During last year’s protests in Turkey, social media played a crucial role in the protests where more than 3.5 million people participated. An outraged Erdagon called social media “the worst menace to society.” He came under criticism once again last month when audio recordings in which Erdogan was telling his son to dispose of a large sum of cash spread across social media. That put him at the heart of a massive corruption scandal.

Surprisingly, Erdogan himself is (or was) quite active on Twitter Inc (NYSE:TWTR). He has tweeted 3,044 times and has 4.17 million followers on the microblogging site.

Twitter Inc (NYSE:TWTR) shares fell 2.19% on Thursday to close at $50.12.