A leading virologist who risked his own life to treat dozens of Ebola patients died Tuesday from the disease, officials said, as a major regional airline announced it was suspending flights to the cities hardest hit by an outbreak that has killed more than 670 people.

Dr. Sheik Umar Khan, who was praised as a national hero for treating the disease in Sierra Leone, was confirmed dead by health ministry officials there. He had been hospitalized in quarantine.

Health workers have been especially vulnerable to contracting Ebola, which is spread through bodily fluids such as saliva, sweat, blood and urine. Two American health workers are currently hospitalized with Ebola in neighbouring Liberia.

Sheik Umar Khan, who was the head doctor fighting the Ebola virus in Sierra Leone, had treated more than 100 victims before getting the deadly disease himself. He died July 29. (Umaru Fofana/Reuters) The Ebola outbreak is the largest in history with deaths blamed on the disease not only in Sierra Leone and Liberia, but also Guinea and Nigeria. The disease has no vaccine and no specific treatment, with a fatality rate of at least 60 percent.

Binyah Kesselly, chairman of the Liberia Airport Authority board, said police are now present at the airport in Monrovia to enforce screening of passengers.

"So if you have a flight and you are not complying with the rules, we will not allow you to board."

In a statement released Tuesday, Asky Airlines said it was temporarily halting flights not only to Monrovia — the capital of Liberia — but also to Freetown, Sierra Leone. Flights will continue to the capital of the third major country where people have died — Guinea — though passengers departing from there will be "screened for signs of the virus."

Passengers at the airline's hub in Lome, Togo also will be screened by medical teams, it said.

"Asky is determined to keep its passengers and staff safe during this unsettling time," the statement said.

The suspension comes after Patrick Sawyer, a 40-year-old American man of Liberian descent who worked for the West African nation's Finance Ministry, died Friday in Nigeria after taking several flights on Asky Airlines. At the time, Liberian authorities said they had not been requiring health checks of departing passengers in Monrovia.

His travels have caused widespread fear at a time when the outbreak shows no signs of slowing in West Africa, where medical facilities are scarce and where some affected communities have in panic attacked the international health workers trying to help them.

Health workers scrambling

Sawyer's sister had died of Ebola though he maintained he had not had close physical contact with her when she was sick. He took an Asky Airlines flight from Liberia to Ghana, then on to Togo and eventually to Nigeria where he was immediately taken into quarantine until his death.

Ebola has killed 632 people across Guinea, Liberia and Sierra Leone since an outbreak began in February, putting strain on a string of weak health systems facing one of the world's deadliest diseases. (Tommy Trenchard/Reuters) Now, health workers are scrambling to trace those who may have been exposed to Sawyer across West Africa, including flight attendants and fellow passengers.

At the Finance Ministry where Sawyer worked, officials announced they were temporarily shutting down operations. All employees who came into contact with Sawyer before he left for Nigeria were being placed under surveillance, it said.

Health experts say it is unlikely he could have infected others with the virus that can cause victims to bleed from the eyes, mouth and ears. Still, unsettling questions remain: how could a man whose sister recently died from Ebola manage to board a plane leaving the country? And worse: could Ebola become the latest disease to be spread by international air travel?

The World Health Organization is awaiting laboratory confirmation after Nigerian health authorities said Sawyer tested positive for Ebola, WHO spokesman Gregory Hartl said. The WHO has not recommended any travel restrictions since the outbreak came to light.

"We would have to consider any travel recommendations very carefully, but the best way to stop this outbreak is to put the necessary measures in place at the source of infection," Hartl said. Closing borders "might help, but it won't be exhaustive or foolproof."

Risk to travellers low

Medical staff working with Doctors Without Borders prepare to bring food to patients kept in an isolation area at an Ebola treatment centre in Kailahun, Sierra Leone. The Ebola outbreak has killed almost 700 people. (Tommy Trenchard/Reuters) The U.S. Centers for Disease Control and Prevention has issued a Level 2 travel alert, warning travellers to Liberia, Sierra Leone and Guinea to "avoid contact with blood and body fluids of infected people to protect themselves."

The risk of travellers contracting Ebola is considered low because it requires direct contact with bodily fluids or secretions such as urine, blood, sweat or saliva, experts say. Ebola can't be spread like flu through casual contact or breathing in the same air.

Patients are contagious only once the disease has progressed to the point they show symptoms, according to the WHO. And the most vulnerable are health care workers and relatives who come in much closer contact with the sick.

Still, witnesses say Sawyer was vomiting and had diarrhea aboard at least one of his flights with some 50 other passengers aboard. Ebola can be contracted from traces of feces or vomit, experts say.

Sawyer was immediately quarantined upon arrival in Lagos — a city of 21 million people — and Nigerian authorities say his fellow travellers were advised of Ebola's symptoms and then were allowed to leave. The incubation period can be as long as 21 days, meaning anyone infected may not fall ill for several weeks.

'Contact tracing'

Health officials rely on "contact tracing" — locating anyone who may have been exposed, and then anyone who may have come into contact with that person. That may prove impossible, given that other passengers journeyed on to dozens of other cities.

Patrick Sawyer had planned to visit his family in Minnesota next month to attend two of his three daughters' birthdays, his wife, Decontee Sawyer, told KSTP-TV in Minnesota.

Two American aid workers in Liberia have tested positive for the virus and are being treated there. Both had been working with a Christian group, and are described as being in "grave" condition.

Nigerians on edge

Meanwhile, the mere prospect of Ebola in Africa's most populous nation has Nigerians on edge.

It's an unprecedented public health scenario. Since 1976, when the virus was first discovered, Ebola outbreaks were limited to remote corners of Congo and Uganda, far from urban centres, and stayed within the borders of a single country. This time, cases first emerged in Guinea, and before long hundreds of others were stricken in Liberia and Sierra Leone.

The deadly Ebola outbreak has spread to Africa's most populous nation after Liberian official Patrick Sawyer vomited aboard an airplane to Nigeria and then died there, officials said Friday. (Sunday Alamba/Associated Press) Those are some of the poorest countries in the world, with few doctors and nurses to treat sick patients let alone determine who is well enough to travel. In Sawyer's case, it appears nothing was done to question him until he fell sick on his second flight with Asky Airlines. An airline spokesman would not comment on what precautions were being taken in the aftermath of Sawyer's journey.

International travellers departing from the capitals of Sierra Leone and Guinea are also being checked for signs of fever, airport officials said. Buckets of chlorine are also on hand at Sierra Leone's airport in Freetown for disinfection, authorities said.

Still, detecting Ebola in departing passengers might be tricky, since its initial symptoms are similar to many other diseases, including malaria and typhoid fever.

"It will be very difficult now to contain this outbreak because it's spread," Heymann said. "The chance to stop it quickly was months ago before it crossed borders ... but this can still be stopped if there is good hospital infection control, contact tracing and collaboration between countries."

Nigerian authorities so far have identified 59 people who came into contact with Sawyer and have tested 20, said Lagos State Health Commissioner Jide Idris. He said there have been no new cases of the disease.