Unnecessary brain scans for headaches are becoming increasingly more common, according to a new study from researchers at the University of Michigan Health System, in Ann Arbor. For the study the researchers reviewed outpatient statistics collected on patients seeking medical attention for headaches.

They found that between 2007 and 2010 about 51 million patients received care for headaches and 25 million did for migraines. About twelve percent of the headache patients had MRIs or CT scans as did ten percent of the migraine patients.

"During headache visits, brain scans are ordered an incredible amount of the time. In 1 to 3 percent of people you will find something on the MRI, whether it be a tumor or blood vessel malformation. You don't want to find something you weren't looking for. It can be anxiety provoking," the researchers explained.

"The number-one reason physicians give is patient reassurance. It's harder to talk a patient out of it than to just get the scan. But that patient reassurance isn't worth $1 billion a year. Facilities make a lot of money off of these tests. Imaging for headaches is on the list of tests patients should question," the researchers add.

For comments and feedback contact: editorial@rttnews.com

Health News