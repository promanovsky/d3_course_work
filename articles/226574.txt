Microsoft’s plan for its new Surface Pro 3 is to eliminate the choice between a tablet and a laptop. My initial verdict? Close, but not quite. Update: We have since published a complete review of the Surface Pro 3, which is based on a couple of weeks of real-world usage. You may want to read that up.

My compatriots here at Ubergizmo will provide a more expansive technical review of the otherwise impressive Surface Pro 3 in a few days. My initial hands-on thoughts are confined primarily to how well Microsoft has achieved its goal of ergonomically melding a tablet with a laptop.

Overall, Microsoft touts Surface Pro 3’s 800 gram weight as making it more pad-of-paper wieldy than, say, Apple’s weightier MacBook Air. And it is super thin and super light for a 12-inch tablet.

But it’s not the weight, it’s the area, that makes Surface not as wieldy as Microsoft or I would like. It’s a BIG tablet with a lot of extraneous parts. With its keyboard cover tucked behind it and the kickboard closed, Surface Pro 3 becomes a bit ungainly – feeling all the keyboard keys behind it was weird, for one thing – and its size made it top-heavy when gripped at the bottom or below its mid-section. Even with these caveats, it sure is easier to use like a pad of paper than a laptop.

Advertising

Tablet/laptop conversion

Surface’s tablet/laptop conversion is completely reliant on its keyboard ergonomics, of which Microsoft is well aware. From the first Surface in 2012, incorporating an ergonomically acceptable keyboard was always Microsoft’s iPad-killer goal, and often the center of Surface complaints.

Surface Pro 3’s keyboard is certainly an improvement on previous iterations. It feels more solid, and the scratch pad is both larger (actually, wider) and more responsive – perhaps too responsive. Once the cursor is situated atop a target, the mere act of pushing down on the scratch pad, especially if the Surface is perched precariously on your thighs, moves the cursor out of position.

It takes more care, thought and attention to complete the basic brainless point-and-click operation than it does on a more stable laptop.

Kickstand kinetics

Stability on a lap will be one of Microsoft’s primary Surface selling points. Surface Computing corporate VP Panos Pamay bragged the company had spent three years designing the hinge on the new built-in kickstand, which can be easily angled to any position and stays where you put it until intentional adjustment pressure is applied.

Oddly, the kickstand lies flush with the rear of the Surface. There is no notch in the Surface edge or on the kickstand, which means the kickstand almost needs to be dug out by your fingernails to deploy it.

The keyboard hinge also has been redesigned. It clicks in crisply and securely to Surface’s bottom perimeter edge for use on a tabletop. But to make it stable on your lap, the keyboard folds up around a half inch to magnetically seal itself to the bottom of the Surface bezel, creating a far more solid balance.

A laptop Surface?

The problem is, the Surface with tablet in “laptop” mode and the hinge pulled out to a comfortable angle extends the entire length of my thighs – and I’ve got looooong thighs (arguably, you may not).

When both keyboard and kickstand were fully deployed to create a comfortable viewing angle, the kickstand edge rested on the tip of my knees. Unfortunately, my arms then had to be folded inward and against my ribs to type – I looked and felt like a T-Rex. If I pushed the Surface out to a more comfortable typing distance, the hinge would topple over my knees.

Conversely, a laptop, because it is completely solid on the bottom, can be pushed out much further so the bottom of the keyboard is around a foot, instead of a few inches, from my waist.

"THE SURFACE PRO 3 IS FAR MORE STABLE ON YOUR LAP THAN ANY TABLET OR TABLET+KEYBOARD COMBO"

Plus, even though the Surface has a 12-inch screen, a 13-inch MacBook Air, which Pamay kept using as a point of reference (and which most reporters in the room were using) is around two inches wider, meaning you could spread your legs a bit further apart than you could while balancing the Surface. This extra couple of inches creates a larger wrist rest area to hold the laptop down as you type, which is what I’m doing now.

I was unable to duplicate this typing comfort with the Surface Pro 3. One wrong move, and the kickstand would have toppled over my knees.

Yes, the Surface Pro 3 is far more stable on your lap than any tablet or tablet+keyboard combo I’ve tried. But it still isn’t as stable as or equal too balancing a solid laptop on your lap. After your first few Surface Pro 3 laptop sessions, you may go scurrying back to a laptop, which of course defeats the Surface purpose.

Pen logistics

"SURFACE PRO 3 IS SLEEKLY DESIGNED ALL THE WAY AROUND, EXCEPT FOR THE PEN STORAGE"Microsoft has taken its not-a-stylus Pen to new functionality levels beyond Samsung’s S Pen. The demos of the New York Times Crossword Puzzle app, where handwritten letters are turned into text and you can cross out solved clues, was impressive, as was the lack of touch-to-ink latency, its Pen-click to automatically open OneNote, and the click-of-the-pen send-to-the-cloud functionality.

As noted, the Ubergizmo kids will delve more deeply into the many and wondrous capabilities of the Surface Pro 3 Pen. Ergonomically, however, I have a couple of quibbles.

Surface Pro 3 is sleekly designed all the way around – except for the Pen storage. Seemingly as an afterthought, Microsoft tacked on a tacky loose loop to the left side of the keyboard cover. From a style POV, this loop-with-pen flaps around rather unfashionably.

But firstly – on the left side? Since around 90 percent of the population is right-handed, why put the stylus on the left?

Secondly, trying to quickly remove or insert the Pen into the loose loop isn’t easy; the loop gives-and-takes instead of allowing a quick slip. A firmer loop would make this Pen in-and-out far less stubborn.

Thirdly, when typing, the outer edge of your wrist will encounter the Pen. The Pen’s not necessarily in the way, but its unfamiliarity – and Microsoft is stressing Surface’s ergonomic familiarity – adds a distraction while typing. None of these are major issues, but they add up.

Bottom line: You cannot deploy the Surface Pro 3 as quickly, as elegantly or with as much stability or comfort as a laptop. But, it is still more elegant or stable than any other tablet pretending to be a laptop, which makes it a positive, if slightly flawed, tablet step forward.

Filed in . Read more about Microsoft and Surface Pro 3.