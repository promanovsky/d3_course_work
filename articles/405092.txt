Drug-resistant malaria ‘may spread to Africa’

Share this article: Share Tweet Share Share Share Email Share

London - Drug-resistant malaria parasites have spread to border regions of Southeast Asia, seriously threatening global efforts to control and eliminate the mosquito-borne disease, researchers said on Wednesday. The scientists, who analysed blood samples from 1 241 malaria patients in 10 countries across Asia and Africa, found resistance to the world's most effective antimalarial drug, artemisinin, is now widespread in Southeast Asia. However, the study found no signs yet of resistance in the three African sites it covered in Kenya, Nigeria and Democratic Republic of the Congo. “It may still be possible to prevent the spread of artemisinin-resistant malaria parasites across Asia and then to Africa by eliminating them, but that window of opportunity is closing fast,” said Nicholas White, a professor of tropical medicine at Oxford University who led the research and is and chair of the Worldwide Antimalarial Resistance Network. More than half the world's people are at risk of malaria infection, and while there have been significant reductions in the numbers falling ill and dying from the mosquito-borne disease, it still kills more than 600 000 people each year.

Most malaria victims are children under five living in the poorest parts of sub-Saharan Africa.

From the late 1950s to the 1970s, chloroquine-resistant malaria parasites spread across Asia to Africa, leading to a resurgence of malaria cases and millions of deaths.

Chloroquine was replaced by sulphadoxine-pyrimethamine (SP), but resistance to SP subsequently emerged in western Cambodia and again spread to Africa.

SP was replaced by artemisinin combination treatment, or ACT, and experts say we now face the prospect of history repeating itself for a third time.

White cautioned that conventional malaria control approaches would not be enough. “We will need to take more radical action and make this a global public health priority, without delay,” he said in a statement released with his findings.

The study, published in the New England Journal of Medicine, enrolled infected adults and children at 15 trial sites in 10 malaria-endemic countries between May 2011 and April 2013.

Patients received a six-day antimalarial treatment, three days of an artemisinin derivative and a three-day course of ACT, and researchers analysed their blood to measure the rate at which parasites are cleared from it.

They found that artemisinin resistance in Plasmodium falciparum - the most deadly form of malaria-causing parasite - is now firmly established in western Cambodia, Thailand, Vietnam, eastern Myanmar and northern Cambodia.

There are also signs of emerging resistance in central Myanmar, southern Laos and northeastern Cambodia, they said.

“If resistance spreads out of Asia and into Africa much of the great progress in reducing deaths from malaria will be reversed,” said Jeremy Farrar, director of the Wellcome Trust global health charity. “This is not just a threat for the future, it is today's reality.”

The World Health Organisation said last year that four countries in Southeast Asia had reported artemisinin resistance in 2013, and 64 countries found evidence of insecticide resistance.

While there are a few new antimalarial drugs in the pipeline, including a once a day medicine from the Swiss drugmaker Novartis and a vaccine from Britain's GlaxoSmithKline, they are unlikely to be on the market and available for widespread use for at least several years yet.

Mid-stage trial results on the Novartis drug also published in the NEJM on Wednesday showed it cleared the parasites from the blood within 12 hours - a result described by Christopher Plowe, a malaria expert at University of Maryland School of Medicine in Baltimore, as “rare good news”.

Elizabeth Ashley, a clinical researcher at the Mahidol Oxford Tropical Medicine Research Unit who worked with White, said that until new drugs come to market, artemisinin medicines and resistance to them need to be watched carefully.

“The artemisinin drugs are arguably the best antimalarials we have ever had,” she said. “We need to conserve them in areas where they are still working well.” - Reuters