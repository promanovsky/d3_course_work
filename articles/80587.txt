The rupee breached the 60-per-dollar mark for the first time in eight months on Friday. It closed at 59.91 per dollar after earlier rising as high as 59.68, its strongest since July 30. The rupee rose 3.15 per cent in the March quarter, its best quarter since September quarter of 2012, when the currency gained nearly 5 per cent.

The strength in the rupee comes at a time when Indian stock markets are trading at record highs. The rally in markets and the currency has been fuelled by heavy buying by overseas investors. Foreign funds have purchased a net $3.7 billion in equities from the start of 2014 until March 27, while in debt the net inflows stand at $5.8 billion.

A turnaround in macroeconomic fundamentals, particularly the sharp narrowing in the current account deficit, has also helped the rupee. India's CAD is likely to be below 2 per cent in 2013-14 from a record high 4.8 per cent in 2012-13. Analysts have also credited Reserve Bank Governor Raghuram Rajan for the sharp reversal in the rupee's fortunes. The rupee has gained 13 per cent since September 2013 when Dr Rajan took over as RBI Governor.



The rupee's rise is good for importers, who will now have to pay less for buying products from abroad. So, oil marketing companies such as Indian Oil Company, BPCL and HPCL stand to gain. Lower fuel prices will also lead to further easing in inflation. The appreciating rupee will also benefit students studying abroad and overseas travelers.

However, the rising rupee poses a big risk to exporters, who will now earn less, unless they hike prices of their products. It's not surprising that IT stocks have fallen 11 per cent in the last one month as compared to 6 per cent rise in the Sensex.

There's much speculation whether the appreciation in the rupee will continue unabated. Will the rupee rise to 45-50 per dollar or will it stabilize around 60 per dollar in the near to medium term? Or is there a chance of the rupee falling again?

Most analysts believe that there's little headroom for the rupee from these levels.

Here's why,

1) Dealers told Reuters that RBI has been buying dollars intermittently over the last week to slow the rupee's rise and also to shore up its foreign exchange reserves.

2) Analysts say if rupee strengthens too much then imported items will start competing with domestic manufacturers and India's exports will become less competitive. The RBI is unlikely to allow that to happen.

3) The biggest reason for the rise in rupee is falling imports and rising exports. However, Kotak Securities says the best for India's trade balance may be over. Gold imports are lower due to restrictions and 'excluded' smuggled gold, capital goods imports are down reflecting low investment in the economy and exports have started to taper, possibly on rupee appreciation, Kotak added. This means when restrictions on gold imports will be eased or when cap goods imports rise, the deficit would widen pressuring the rupee.

4) According to HSBC Global Research, the Indian currency tends to benefit into an election, but struggles to maintain such positive momentum after the event.

5) Bank of America Merrill Lynch says UPA, NDA and United Front have all allowed the RBI to buy forex to secure rupee stability in the past. It expects Dr Rajan to hold Rs 60-65 per dollar levels if the USD settles at 1.30/per euro. This means that even if a stable government comes to power, as anticipated by markets, the rupee is unlikely to rise much from current levels. However, if a weak government assumes power post polls, the rupee may come under pressure as foreign investors may repatriate funds.

(With inputs from Reuters)