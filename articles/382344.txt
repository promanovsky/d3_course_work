Edmond native Joshua Ward was only 8 when “Outbreak” came out, but he still remembers the impact that the movie and other films like it had on him.

Although made for entertainment, films like “Outbreak,” a 1995 thriller about a virus with a 100 percent mortality rate, shed light on a world of pathogens and their nonhuman primate hosts, Ward said.

Ward, the director of scholar development and undergraduate research at Oklahoma State University, answered a few questions about the evolving Ebola outbreak in West Africa.

Ward’s published research extends from non-human primate genetics to human disease. He is a former research fellow at Harvard Medical School’s New England primate research center and former member of the primate immunogenetics and molecular ecology research group at the University of Cambridge.