New York: The Dow and S&P 500 on Friday closed at new records following fresh merger and acquisition activity, even as the Nasdaq Composite Index dipped slightly.

The Dow Jones Industrial Average gained 18.43 (0.11 percent) to 16,717.17, edging above the previous record by less than 2 points

The S&P 500 advanced 3.54 (0.18 percent) to 1,923.57, notching a record for the second straight day.

But the tech-rich Nasdaq fell 5.33 (0.13 percent) to 4,242.62.

Stocks rose after Valeant Pharmaceuticals raised its bid for Botox-maker Allergan for the second time this week. Valeant rose 1.5, while Allergan jumped 5.7 percent.

Dow member Microsoft announced a new partnership with Salesforce.com to employ Microsoft equipment on Salesforce applications and platforms. Microsoft gained 1.5 percent, while Salesforce fell 3.3 percent.

Home-improvement retailer Lowe`s advanced 0.2 percent after raising its quarterly dividend by 28 percent to 23 cents per share.

Casual clothing chain Express slumped 7.5 percent after reporting an 11 percent decline in first-quarter comparable sales and projecting annual earnings of 74-90 cents per share. Analysts had forecast profits of $1.14 per share.

Splunk, which works in "big data" analysis, sank 16.4 percent despite raising its forecast. The company now expects annual sales of $402-410 million, up from the previous projection of $400 million.

Deutsche Bank said investors were disappointed Splunk did not show stronger growth in billings and some other widely-watched metrics during the first quarter.

Value retailer Big Lots shot up 13.1 percent as earnings of 50 cents per share bested analyst forecasts by six cents.

Bond prices were mixed. The yield on the 10-year US Treasury held steady at 2.46 percent, the same level as Thursday, while the 30-year dipped to 3.31 percent from 3.33 percent. Bond prices and yields move inve