A few weeks after Apple announced HealthKit at its Worldwide Developers Conference, Google announced its own competitor at its own annual developers conference Wednesday, called Google Fit.

Google previewed the new health platform towards the end of its three-hour I/O conference.

Advertisement

Noom, a weightloss tracking solution, is one of the apps that is already integrated into Google Fit.

Developers will be able to use Google Fit tools in their apps so users can monitor their fitness. The idea is that users will be able to blend data from all sorts of apps and devices to get a complete picture of their overall fitness.Since I/O is primarily focused on developers, Google didn't spend too much time talking about the consumer-facing aspects of Google Fit, but development tools will be available in the coming weeks and we can expect to hear more about the product in the near future.

Adidas is also working with Google to integrate its tracking solutions in Google Fit.

Google also partnered with Nike, RunKeeper, Intel, Motorola, and LG.