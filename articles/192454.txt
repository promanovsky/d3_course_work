King Digital, the company behind Candy Crush, had a rough day. The stock was down 13.4%, putting it at $16.25 per share. The stock was priced at $22.50 when it IPO'd.

It started off as a relatively benign day for King. It reported earnings that were pretty good. Revenue was ahead of expectations, and even up from the fourth quarter. However, it wasn't enough.

Advertisement

The big problem for King seems to be that Candy Crush declined on a quarter-over-quarter basis. While the other games made up for the decline, investors are skeptical of the long term prospects for King.Plus, as you can see in this chart of King's revenue from our friends at Statista , it's basically gone flat for the last three quarters.