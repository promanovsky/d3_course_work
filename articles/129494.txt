William M. Welch

USA TODAY

LAS VEGAS – Fans of the Ford Mustang from around the world gathered Thursday for the 50th birthday of what enthusiasts call the most recognizable automobile brand in American history.

"Fifty years ago today, Ford dealers around the country were opening their doors and taking orders,'' said Hal Sperlich, a retired auto executive who was on the design team and became Mustang program manager with Lee Iacocca's team at Ford.

"It was an amazing time,'' Sperlich said, recounting for enthusiasts the creation of the car and the smashing success of Ford's 1964 marketing campaign that put the original "pony car'' on the front of magazines, newspapers and TV shows at introduction.

"If you had a Mustang,'' Sperlich recalled, "you were cool.''

Mustang's days of being cool aren't just in the rear view mirror, enthusiasts here say.

Thousands gathered on the date of the Mustang's introduction in 1964 to celebrate old and new models of the brand at the Las Vegas Speedway.

Enthusiasts from as far away as Sweden, France and New Zealand were among the participants. Many from U.S. locations rode in caravans to the desert gathering, making a Mustang about the easiest car to see on Interstate 15 through Las Vegas.

Every attendee had a story to tell about their Mustang.

Brent Peabody, 54, of Frazier Park, Calif., arrived in a black 1969 Mustang GT convertible that he saved from the crusher. He was at a junk yard in 2000 when he spotted the car, a victim of an engine fire, about to be loaded into a crushing machine. He intervened to stop the destruction, paid the junk yard owner $200 and towed the car home, where its restoration became a labor of love for himself and sons Kevin and Gary.

"It took us seven years to get it all together,'' he said of the restoration.

Many here were young drivers when the car was introduced and, smitten, made Mustangs a lifelong passion.

Peabody's first car was a 1971 Mustang Boss; he's had 22 Mustangs total, and once had 14 at one time. He's down to five now.

There were some very early cars present, such as the 1966 model Butch Eckhoff, 65, drove from Layton, Utah.

The retired Air Force veteran of Vietnam bought someone else's incomplete restoration project and finished the job, with a 289 cubic inch V-8, four barrel carburetor, upgraded interior and a rare factory custom paint color, "Ember Glo."

"We drive it a lot,'' he said.

He shows it a lot too – one year he went to more than 80 car shows and cruises to show off his Mustang.

A current Ford executive, chief financial officer Hau Thai-Tang, global vice president and former Mustang chief engineer, said the Mustang isn't Ford's biggest seller (that's the Focus) or most profitable vehicle (that's the F150 truck), but is its most important car nonetheless.

"It is the heart and soul of the company,'' he said.

##