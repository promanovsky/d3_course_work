Isla Vista: Santa Barbara County Sheriff's Department officials on Sunday identified the first three victims of the Isla Vista rampage, each found fatally stabbed Friday night inside an apartment not far from the University of California, Santa Barbara, campus.

Now, all the attacker's victims have been identified, and they were all UCSB students.

A sign advocating gun control is seen on a makeshift memorial for 20-year-old university student Christopher Michael-Martinez. Credit:Reuters

The latest names to be released - Cheng Yuan Hong, 20, of San Jose; George Chen, 19, of San Jose; and Weihan Wang, 20, of Fremont - were stabbed multiple times in unit No. 7 of a two-story, charcoal gray building on Seville Road. Authorities say 22-year-old Elliot Rodger attacked them before going on a rampage through Isla Vista, killing three more people before taking his own life.