It won't be nearly as much fun as eating candy bars, but a big study is being launched to see if pills containing the nutrients in dark chocolate can help prevent heart attacks and strokes.

The pills are so packed with nutrients that you'd have to eat a gazillion candy bars to get the amount being tested in this study, which will enroll 18,000 men and women nationwide.

"People eat chocolate because they enjoy it," not because they think it's good for them, and the idea of the study is to see whether there are health benefits from chocolate's ingredients minus the sugar and fat, said Dr. JoAnn Manson, preventive medicine chief at Harvard-affiliated Brigham and Women's Hospital in Boston.

The study will be the first large test of cocoa flavanols, which in previous smaller studies improved blood pressure, cholesterol, the body's use of insulin, artery health and other heart-related factors.

A second part of the study will test multivitamins to help prevent cancer. Earlier research suggested this benefit but involved just older, unusually healthy men. Researchers want to see if multivitamins lower cancer risk in a broader population.

More On This... The surprising health benefits of beer

The study will be sponsored by the National Heart, Lung and Blood Institute and Mars Inc., maker of M&M's and Snickers bars. The candy company has patented a way to extract flavanols from cocoa in high concentration and put them in capsules. Mars and some other companies sell cocoa extract capsules, but with less active ingredient than those that will be tested in the study; candy contains even less.

"You're not going to get these protective flavanols in most of the candy on the market. Cocoa flavanols are often destroyed by the processing," said Manson, who will lead the study with Howard Sesso at Brigham and others at Fred Hutchinson Cancer Research Center in Seattle.

Participants will get dummy pills or two capsules a day of cocoa flavanols for four years, and neither they nor the study leaders will know who is taking what during the study. The flavanol capsules are coated and have no taste, said Manson, who tried them herself.

In the other part of the study, participants will get dummy pills or daily multivitamins containing a broad range of nutrients.

Participants will be recruited from existing studies, which saves money and lets the study proceed much more quickly, Manson said, although some additional people with a strong interest in the research may be allowed to enroll. The women will come from the Women's Health Initiative study, the long-running research project best known for showing that menopause hormone pills might raise heart risks rather than lower them as had long been thought. Men will be recruited from other large studies.

Manson also is leading a government-funded study testing vitamin D pills in 26,000 men and women. Results are expected in three years.

People love vitamin supplements but "it's important not to jump on the bandwagon" and take pills before they are rigorously tested, she warned.

"More is not necessarily better," and research has shown surprising harm from some nutrients that once looked promising, she said.