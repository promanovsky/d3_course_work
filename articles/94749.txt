‘‘For many long months we starved. The best we could do was rewatch old episodes to remind ourselves of the many ingredients that make up this densely populated drama.’’

What could former Prime Minister Julia Gillard be writing about? The period in the wilderness after Kevin Rudd Mark II returned, throwing her out of her job?

A scene from fantasy TV show Game of Thrones.

The lean times when she and Tim departed from The Lodge, adjusting to life after politics and before the Adelaide mansion, sustaining themselves on nothing but the parliamentary pension and memories of the never-ending drama they had left behind?

Not at all. Ms Gillard has taken to the esoteric business of critiquing, for an online publication, the cult TV show Game of Thrones.