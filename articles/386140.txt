She said yes! Danica McKellar is engaged to boyfriend Scott Sveslosky.

Michella Bega, a rep for “The Wonder Years” alum and recent “Dancing With the Stars” contestant, confirmed the news to The Times.

McKellar also teased to the engagement using social media.

“So ... can you guess what my big news is??” the 39-year-old wrote, posting a pic of herself on Instagram flashing her large engagement ring.

Advertisement

“Yep, my amazing boyfriend Scott & I are engaged!! (He’s the one @iamvalc did a shout out to on #DWTS) :) Hugs to you all!” she added in a tweet.

McKellar is referring to the “DWTS” segment she did with pro partner Val Chmerkovskiy in which she gets emotional about her beau. Chmerkovskiy facetiously thanks Sveslosky for letting him dance with his girl.

“Scott and I are thrilled,” the actress said in a statement, adding that they plan to get married later this year.

It’s the second marriage for the actress, who wrote the New York Times bestseller “Math Doesn’t Suck” and three other books on math. She was previously married to Mike Verta in March 2009, and the pair finalized their divorce in 2013. McKellar and Verta have a 3-year-old son, Draco.

Advertisement

Sveslosky, 38, a partner in the international law firm Sheppard, Mullin, Richter & Hampton, was also previously wed and has a son, Hunter, 10.

McKellar added that she’s “looking forward to merging our families.”

On Tuesday, the actress and her “Wonder Years” costars Fred Savage, Jason Hervey, Olivia d’Abo, Alley Mills, Dan Lauria, Josh Saviano and Paul Joshua Pfeiffer reunited on “Good Morning America” for the first time in 16 years, and McKellar opened up about she and Savage having their first ever kiss on the show and in real life.

“The one good thing about getting your first kiss on camera is that you know for sure it’s going to happen,” she said.

Advertisement

“It was terrifying! We were both really scared and nervous and didn’t know what was going to happen or if we were going to do it right,” Savage added. “But all that stuff was very real ... I think that all of those things that were kind of happening on the show ... all grew out of something very real that was going on between us or in our lives.”

I still think math is no fun. Follow me @NardineSaad.