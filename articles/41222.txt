There is strong evidence that medical marijuana pills may reduce symptoms of spasticity and pain reported by multiple sclerosis patients, but little proof that smoking pot offers the same benefit, according to new alternative treatment guidelines released by the American Academy of Neurology.

The guidelines on complementary and alternative medicine, or CAM, treatments for MS were published Monday in the journal Neurology and are among the first from a national medical organization to suggest that doctors might offer cannabis treatment to patients.

Though advocates of medical marijuana use said the guidelines appeared to be part of a recent national trend in which doctors were seriously evaluating the use of cannabis, they argued that the federal government had stymied marijuana research.

“The reason there’s no evidence on inhaled cannabis is because it’s very difficult to study,” said Dr. Donald Abrams, a professor of clinical medicine at the University of California San Francisco and a marijuana researcher who was not involved in the guideline study. “The government really restricts studies of the plant.”

Advertisement

A panel of AAN researchers based their guidelines on a review of 115 clinical studies that examined a variety of alternative MS treatments, including the use of ginkgo biloba, magnets, bee sting venom, reflexology, a low-fat diet and over-the-counter supplements.

The only non-conventional treatment in which authors found “strong evidence” for patient-reported improvements was the use of oral cannabis extracts, or lab-manufactured pills.

“Basically, there is little evidence for the effectiveness of most CAM therapies that people use,” said Dr. Vijayshree Yadav, the lead author of the guidelines and an associate professor of neurology at Oregon Health & Science University.

Patient surveys suggest that up to 80% of MS patients employ some form of alternative treatment, including the smoking of “street” marijuana. Yadav said that very few high-quality studies have examined the effectiveness of these treatments, and there was simply too little evidence to support or refute their effectiveness.

Advertisement

“There is really a need for more research,” Yadav said.

MS occurs when the body’s immune system attacks nerve tissue in the brain and spinal cord, damaging the protective myelin sheath that covers nerve fibers.

Damage caused by the disease effects how electrical signals travel through the central nervous system, causing patients to suffer uncontrollable shaking of muscles, nerve pain, difficulty walking and other problems.

Both the cause and cure of MS remain unknown.

Advertisement

Yadav and her colleagues noted that nine short-term studies indicated that patients reported improvements in their spasticity or pain as a result of cannabis treatment, although their doctors were not able to objectively verify these improvements through standardized evaluations.

“There was a discordance between the subjective and objective outcome that was kind of consistent,” Yadav said.

Yadav said that though most standard FDA-approved MS drugs offer results that are objectively verified, she said it was possible that the improvements that came with cannabis were too small to be detected by physicians.

“Probably the effect was not profound, so doctors really didn’t see it, but the patient reported some improvements,” Yadav said.

Advertisement

Dr. Timothy Coetzee, of the National MS Society, who was not involved in the research, said the guidelines were “a significant step forward” in helping patients and doctors to understand current alternative therapies.

Spasticity, and pain from spasticity were very common symptoms in MS, he said.

“It is difficult to know if physicians will suggest these treatments to their patients,” said Coetzee, the society’s chief research officer. “Currently 20 states have legalized the use of marijuana for medical purposes ... There is significant unmet need for therapies that can address complex and painful symptoms often experienced by people with MS.”

Yadav and her colleagues wrote that there was “inadequate” data to determine whether smoked marijuana was safe or effective in treating symptoms of pain and spasticity in MS patients.

Advertisement

Abrams, who has researched marijuana’s effects on pain, said the reason for this lack of data had to do with the National Institute on Drug Abuse, or NIDA.

“The NIDA is the only source of cannabis for research,” Abrams said. “NIDA has a mandate from the federal government to only study substances of abuse as substances of abuse.”

That view was echoed by Dr. David Bearman, the executive vice president for the Academy of Cannabinoid Medicine/Society of Cannabis Clinicians, who was not involved in the guideline study.

“Part of the problem in the United States is that the NIDA has blocked almost all meaningful studies on cannabis,” Bearman said.

Advertisement

Bearman argues that while synthetic cannabis pills do offer pain relief, marijuana is cheaper, has fewer side effects and can be more effective.

He said the MS guidelines appeared to be part of a trend in the last couple years in which mainstream medicine has begun to look at cannabis as it would any other therapeutic agent.

“Hopefully we’ll see more of this in the future,” Bearman said. “I congratulate them on ignoring 100 years of propaganda.”

Yadav said she expected that doctors would be very cautious in considering the use of cannabis for their MS patients, due to legal issues and potential adverse side effects.

Advertisement

“Because cannabinoids have known psychoactive properties, their potential for psychopathologic and neurocognitive adverse effects is a concern,” Yadav and her colleagues wrote. “Depression and predisposal to psychosis have been reported with long-term cannabis exposure.”