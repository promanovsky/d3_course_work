'Borderline miraculous': Amy Purdy leaves Dancing With The Stars audience and judges in tears with inspirational story



One of the most emotional moments in Dancing With The Stars history on Monday night sparked a flood of tears.

As the contestants all opened up about the most important year of their life, winter Olympics medalist Amy Purdy spoke movingly of losing her legs - and how her father saved her life by giving her one of his kidneys.

Amy showed home video of her first steps after getting her initial set of false legs - revealing that it was dancing with her hero dad that came before walking.

Scroll down for video

Inspirational story: Amy Purdy was emotional on Monday night's broadcast of Dancing With The Stars after talking about her recovery from losing her legs and her father's support

Her dance partner Derek Hough then put her amazing story into the choreography for their night's dance after Amy admitted: 'I cannot put into words how grateful I am - that is why we are doing this dance.'

Their beautiful routine had the audience and judges wiping away tears.

It was all the more spectacular because she had to wear a new pair of prosthetic legs just to have feet that could point for the routine.

Biggest fan: Amy's father Stef wiped away tears after watching his daughter perform with Derek Hough

Touching performance: Amy thrilled the audience with her dancing

The tears flew even faster when Amy rushed over to hug her dad at the end of the routine - and with tears running down his own eyes he was seen saying: 'I'm so proud of her.'

'One thing I can guarantee to you - there won't be a dry eye in the country tonight,' judge Bruno Tonioli told her. 'That was so heart-wrenching, yet it was inspirational and life affirming. It really transcended dance. It was a message of universal value.

'Borderline miraculous, my darling. I love you for what you do, I really do.'

'It touched my brain with the level of difficulty and it touched my heart of the level of artistry,' Len Goodman told her. 'It was truly beautiful.'

Action athlete: Amy recalled how she lost both of her legs due to a bacterial infection and how she persevered to become a snowboarding athlete

Carrie Ann Inaba called her 'so inspiring', adding: 'The way you were able to balance on those toes just made it so much more beautiful.'

Guest judge Robin Roberts of Good Morning America fame gave Amy a nine score along with all the other judges.

Roberts told her: 'There are not enough superlatives, there just simply are not. I hope that you know what you are doing for people when they see you dance every week. I hope that you are feeling that love.

'Anyone who is facing a challenge, you are letting them know that they too - we too - can begin again anew.'

Nailed it: Amy and pro partner Derek Hough received nine scores all around

In footage shown before her performance, Amy spoke movingly of how she was just 19 when she lost her legs and kidney function after contracting bacterial meningitis.

'My dad gave me one of his kidneys,' she said. 'I always say that my dad gave me life twice. He brought me into this world, and then through his gift he kept me in this world.'

She also revealed that she had been convinced she would never walk again - until music inspired her to dance with her dad.

'And I thought if I can dance I can walk. And if I can walk I can snowboard. I can live a great life,' she said.

Strong performance: Amy was thrilled with her dance as she spoke to host Erin Andrews

Amy was one of several teams to get 9s across the boards from the judges - but the night belonged to one team who got the first 10 scores while collecting three of them.

Ice skater Meryl Davis's foxtrot with Maksim Chmerkovskiy got top scores from all of the judea but Len, even though he told them: 'Meryl and Maks, M&M - sweet and delicious.'

Most dramatically, Carrie Ann admitted she wept.

'I burst into tears at the middle of that routine because it was just so beautiful. You are something out of this world extraordinary. And the partnership took my breathe away and gave me goosebumps,' she said.

Skill on display: Meryl Davis and Maksim Chmerkovskiy made judge Carrie Ann Inaba weep over its beauty

Meryl's skating partner Charlie White got all 9s, but had one of the most eye-catching moments of the night - starting his jive to Pharrell's Happy by wearing the pop star's infamous big hat.

But the real stars of the night in many ways were some of the contestant's children who came onstage after their parents dedicated dances to them.

'It's bring your kids to work day!' host Tom Bergeron quipped.

He's happy: Charlie White scored all nines for his routine to the hit song Happy

The Wonder Years' star Danica McKellar had been teary-eyed as she spoke about how her most amazing year was giving birth to son Draco in 2010 - and ran up to hug him at the end of her routine, clutching him tight throughout the judging.

'It's not every day you get to dedicate a dance to your family,' she said. 'I know Draco doesn't quite understand it yet, but he will.'

She danced to the Billy Joel song Lullaby and teared up when she said the song she was about a child questioning their parent about death.

'I want him to know that I will always be there for him, even when I'm gone,' she said.

For her son: Danica McKellar dedicated her performance with Val Chmerkovskiy to her young son

The Price Is Right host Drew Carey also brought his son Connor out into the center of the stage.

As host Tom got down on his knee to ask Connor what he thought of his dad's performance, Connor instead seemed to be more interested in his dad's partner, Cheryl Burke.

'I think he did really good, and, um, I need her to teach me some of those moves,' he said cheekily, leading Cheryl to say she 'would give him a 10' for pick-up skills.

Drew teared up as he told how becoming a dad was the spark to lose all his weight.

'There was no way I was going to make 65 or 70 the way I was going,' he said. 'My dad died when I was eight - I don't want that to happen to Connor. I don't want him to grow up without a dad.'

Looking smooth: Drew Carey also dedicated his dance to his son

Meanwhile hunky boy band star James Maslow had the most surprising story - insisting he was bullied for being chubby as a kid.

'Kids do messed up stuff, malicious stuff,' he said of the bullying. 'And it did get to the point where I had to actually switch schools.'

His jive - with him dressed as a nerdy high school pupil and partner Peta Murgatroyd in a sexy school outfit with high white socks - was one of the performances to get all 9s.

'Was that an earthquake, an aftershock, or did you just rock this place?' Len asked. 'Full on, flat out and fantastic.'

'I tell you, you are a contender my darling,' Bruno said.

He's a contender: James Maslow scored all nines along with dance partner Peta Murgatroyd

NeNe Leakes dedicated her Rumba to husband Gregg, calling him her 'soulmate' - even while dancing with a topless Tony Dovolani showing off his amazing abs.

'Gregg Leakes, this rumba is for you,' NeNe insisted. 'There's gonna be a little bit of grinding, but don't worry about that - you know I'm coming home to you.'

Tony's abs got more attention than their actual dance, with Carrie Ann giving him the thumbs up as she told him: 'After 17 seasons - looking good Tony!'

Guest judge: Robin Roberts of Good Morning America fame was a guest judge on the show

But the biggest screams from the audience came for Aussie pop star Cody Simpson, who got 9s from all the judges but Len for his jazz performance to his own song, Surfboard.

'That was ridiculous - it was so good,' Carrie Ann gushed. 'I was eating it up!'

Actress Candace Cameron Bure performed last, getting all eights after having some of her former Full House castmates visiting her in rehearsals.

Surf's up: Cody Simpson danced to his own song Surfboard with pro partner Witney Carson

Energetic routine: Candace Cameron Bure scored all eights after her performance

But one contestant has performed for the very last time - with Star Wars legend Billy Dee Williams, 77, pulling out on doctor's orders.

'Yeah I've got this chronic back problem,' he explained.

'I've had a wonderful, brilliant, fantastic blast of a time and working with this wonderful beautiful young woman. Pure joy.

'And I want to thank every one of you folks who've been supporting us.'

As he got a standing ovation, his grandchildren - who he had planned on dedicating his dance to - ran out to hug him.