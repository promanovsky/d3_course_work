Earlier this month, Sir Tim Berners-Lee, creator of the World Wide Web, called for a "Bill of Rights" for the web on its 25th birthday. Late last night, Brazil broke ground and became the first country to pass a bill protecting the rights of internet users.

An "overwhelming" majority of Brazil's Chamber of Deputies voted for the internet bill of rights bill called the "Marco Civil da Internet," according to ZDNet. The bill, a result of a four-year process, is the first to set internet governance principles in Brazil, which protects freedom of expression, the right to privacy, and net neutrality -- the most emphasized provision in the piece of legislation. While the Marco Civil bill still needs to pass the Senate before getting approval by President Rousseff, it appears the most difficult process for its approval has been completed.

The Marco Civil bill was first officially drafted in 2009, and went through a long process of approval and consultation, including an 18-month period of discussion through the internet by various stakeholders, including telecommunications companies, government agencies, civil organizations, and users, according to Access Now.

While not perfect for internet activists, due to disagreements and extended wrangling over parts of the legislation, the Marco Civil legislation includes key provisions that protect netizens in Brazil -- some which appear to be a direct result of the 2013 revelations about the U.S. National Security Agency's surveillance activities, especially those relating to NSA's spying on Brazilian President Dilma Rousseff.

Some of the important aspects of the Marco Civil bill include:

Open Principles

Marco Civil asserts regulations supporting strong rights for Brazilians on the net, as well as future-aware principles for the internet's operation, including freedom of expression, open standards, protection of personal data, multi-stakeholder administration, and accessibility. Taken as a whole, the principles support an open, inclusive, and democratic culture for Brazil's internet, and implicitly limit central control over it.

Privacy

One privacy measure that Rousseff wanted to be included would have been a substantial protection against outside spying. The "forced data center localization" provision would have demanded that some kinds of internet companies keep their data centers physically within Brazil's boundaries. The provision created a great deal of controversy, because obviously that would create massive and expensive logistical problems, so to get it passed, it wasn't included.

Instead, Marco Civil asserts Brazilian jurisdiction over data and services in Brazil, and also requires consumer internet service providers (ISPs) to retain user data for no more than a year, which is shorter than the five year period previously proposed. In addition, other provisions in the bill create safe harbors for online intermediaries in Brazil, and prohibit Deep Packet Inspection (a common tool to filter out malicious traffic, but which can also be used for data mining and surveillance) when it threatens Brazilians' privacy at the level of their physical internet connection.

Net Neutrality

The most important, and most emphasized, portion of the revolutionary Marco Civil bill protects net neutrality -- something which the U.S. hasn't quite figured out yet. The bill says that ISPs must treat all data that goes through their network in the same way, and not set higher or lower speeds for different internet content or services. ISPs also can't suspend users' connections unless in the case of debt, and internet companies are responsible for delivering and maintaining the same quality of service agreed upon in contract terms -- meaning no throttling.

Brazil, Latin America and the Future of the Internet

While clearly not all countries in Latin America are internet-progressive, Latin American markets are increasingly driving many of the technology industry's priorities, with incredible market growth recently (with more potential growth), and populations that are increasingly conscious of the civil issues the internet brings.

The Marco Civil bill is the first of its kind in the world, and will likely become law before NETmundial, a global conference on internet governance that Brazil is hosting next month. In this way, Brazil and Latin America might lead the world towards Sir Berners-Lee's vision of a mature, civilly responsible net.