AOL (NYSE: AOL) announced revenue of $583.3 million, 8.36 percent higher than the same quarter last year. This was driven largely by international sales, which rose by 16 percent year over year.

The comparable EPS figure is $0.34, 24.44 percent lower than expected. However, actual EPS is $0.11. Much of the difference in figures can be attributed to restructuring and asset impairment, likely related to acquisitions.

CEO Tim Armstrong commented, “Q1 marks the fifth consecutive quarter of consumer, revenue and Adjusted OIBDA growth. AOL’s investment in global media and technology platforms is allowing AOL to compete on a global scale.”

Membership revenue fell by 10 percent year over year, however the churn rate was reduced from 1.9 to 1.5 percent.

Although volume has been very low, traders were not liking the news in premarket trading. The last trade that went through for the company was at $42, 4.3 percent lower than Tuesday’s closing price.