Facebook Inc. (FB) Wednesday reported a surge in profit for the first quarter, driven mainly by sharp growth in advertising revenues with both earnings and revenues trouncing Wall Street estimates. Shares of the social networking giant gained about four percent in after-hours trade, following the announcement of results.

In a separate development, Financial Chief David Ebersman announced his intentions to step down as CFO.

Facebook's revenues for the first quarter surged 72 percent to $2.50 billion from $1.46 billion a year ago. Thirty-nine analysts polled by Thomson Reuters had a consensus revenue estimate of $2.36 billion for the quarter.

Revenue from advertising, Facebook's main stream of income, surged 82 percent to $2.27 billion from last year, as the company continues to expand its mobile advertising.

Growth in mobile ad revenues is a very important factor for Facebook, as majority of its users prefer to log in through smartphones rather than desktops. Facebook's mobile advertising revenue represented about 59 percent of advertising revenue, up from about 30 percent last year.

Commenting on the results, Chief Executive and Founder Mark Zuckerberg said, "Facebook's is strong and growing, and this quarter was a great start to 2014. We've made some long term bets on the future while staying focused on executing and improving our core products and business. We're in great position to continue making progress towards our mission."

At the end of the first quarter, Facebook had 1.28 billion of monthly active users, an increase of 15 percent, while mobile monthly active users rose 35 percent to 1.01 billion. Daily active users rose 21 percent to 802 million, with mobile daily active users jumping 43 percent to 609 million.

Costs and expenses for the first quarter increased 32 percent to $1.43 billion, due to higher headcount and infrastructure expenses. Nevertheless, operating margin climbed to 43 percent for the first quarter from 26 percent last year.

Menlo Park, California-based Facebook's first-quarter profit surged to $642 million or $0.25 per share from $219 million or $0.09 per share last year.

Adjusted profit for the quarter rose to $885 million or $0.34 per share from $312 million or $0.12 per share in the prior year quarter. On average, 39 analysts expected earnings of $0.24 per share for the quarter. Analysts' estimates typically exclude special items.

Facebook's CFO David Ebersman announced his intention to step down after serving in the position for almost five years. Ebersman has decided to join back into healthcare industry.

Ebersman will be succeeded by David Wehner, currently Facebook's Vice President, Corporate Finance and Business Planning. Wehner had joined Facebook in November 2012 from Zynga, where he served as CFO.

FB closed Wednesday's trading at $61.36, down $1.67 or 2.65%, on the Nasdaq. The stock, however, gained $2.39 or 3.90% in after-hours trade.

For comments and feedback contact: editorial@rttnews.com

Business News