You love your dog and you want your dog to be happy. You know he likes two things above all else; meat and chewing. So of course, you’re going to reach for the jerky, a delicious meaty, chewy treat that not many a dog can resist. But if your dog knew what was good for him, he might take one sniff and walk away.



Since your dog probably won’t, it might be worth your time to monitor him more closely. Toxic jerky has been causing dogs illness since 2007, with 4,800 complaints having been filed. While only 580 of those reported resulted in death until October, that number has nearly doubled with a new report issued by the FDA as of May 1st.

ABC news aired this related report of pet owners shying away from jerky treats that threaten the lives of their favorite furry friends.

What’s even more disturbing is that the FDA has yet to pinpoint the actual cause of the toxic jerky. While the location of the affected dog treats has been narrowed down to producers in China, that’s a small relief for dog owners if the cause has yet to be identified and removed. Since many companies outsource jerky ingredients, even locally toxic components can end up in locally made jerky, as well. So, while your dog may be particularly fond of the treats, it may be in your best interest to avoid jerky altogether.

Of particular concern are jerky treats that include chicken, duck and sweet potato. These ingredients, rather than the location of purchase, are better indicators of your dog’s likelihood to be eating toxic jerky. Dog owners should try to avoid these treats especially.

So, what are the results of eating toxic jerky? Your dog may experience vomiting, diarrhea and lethargy, which may be symptoms of more dangerous problems such as gastrointestinal/liver disease, kidney or urinary disease, and, in rare cases, neurologic, dermatologic and immune system problems.

Furthermore, your dog doesn’t need that tasty jerky as part of its diet. The jerky can replaced with other dog friendly snacks, so it’s not surprising that the FDA reiterated that very message:

“The [FDA] continues to caution pet owners that jerky pet treats are not required for a balanced diet,” the May 16th report states.

In the same press release, the FDA ensured the public that it will continue the investigation and reassured dog owners:

“The agency continues to review case records, test treat samples from reported cases, screen tissue, blood, urinary and fecal samples, and communicate with the attending veterinarians and pet owners to thoroughly investigate select cases.”

They have gone so far as to request special attention from the CDC:

“While the Centers for Disease Control and Prevention primarily tracks cases of human illness, FDA has requested their expertise in collaborating on a study of cases reported to the FDA of sick dogs compared with ‘controls.'”

So, dog owners looking for answers can expect the issue to be thoroughly reviewed.

Please remember, if you love your dog, reach for something other than jerky chews. You could be saving him from getting sick or even saving your dog’s life.

Nearly 600 dogs have died after eating tainted jerky treats. Is your dog at risk? Find out more ahead >> http://t.co/2L4tGwSLXy — AnimalPlanet (@AnimalPlanet) October 25, 2013

FDA Investigation Links Tainted Jerky Treats To Dog Deaths http://t.co/YCMcNYANsF — BuzzFeed (@BuzzFeed) October 24, 2013