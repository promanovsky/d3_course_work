Valerie Cherish just won’t quit: Nine years after it last aired, “The Comeback” will return to HBO this fall for a limited, six-episode run.

The original series, a sharp satire of show business and reality television, was created by “Friends” star Lisa Kudrow and “Sex and the City” show runner Michael Patrick King. It follows Cherish, a washed-up sitcom actress, played in a meta twist by Kudrow, as she attempts to make a Hollywood comeback. Her efforts are filmed for a faux reality series, also called “The Comeback.”

The comedy, which received low ratings and middling reviews, was canceled in 2005 after just 13 episodes on HBO, prompting snarky headlines about Kudrow’s post-"Friends” career. Despite, or perhaps because of, its short run, the series has since been embraced as a cult classic. Rumors began to swirl about a possible return of “The Comeback” a few weeks ago, and on Monday HBO made it official.

“‘The Comeback’ holds a special place in the hearts of its many fans, including many of us here at HBO,” said president HBO programming president Michael Lombardo in a statement. “I can’t wait to find out what Valerie’s been up to since we last met.”

Advertisement

So just what might that be? HBO’s announcement included scant information about what the new season will entail, only hinting that “Valerie thinks she has it all figured out this time. She doesn’t.”