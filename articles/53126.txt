Washington: Researchers have discovered a miniature planet with two rings of ice and pebbles, located two billion kilometers out in the solar system between Saturn and Uranus.

The smaller celestial body, called Chariklo, was located in the Kuiper Belt, a collection of thousands of dwarf planets and comets in orbit beyond Neptune on the edge of our solar system.

But at some point it was thrown out of this belt and is now between Saturn and Uranus, where there is a collection of small objects, called Centaur. Chariklo is the largest of these objects with a diameter of 250 km.

This comet like object has been known for many years, but despite careful study, its rings have never been observed before now. This has happened because a new camera is now being used on the Danish telescope at ESO`s La Silla Observatory in Chile.

Uffe Grae Jorgensen , an astronomer in Astrophysics and Planetary Science at the Niels Bohr Institute at the University of Copenhagen, explains that when an object passes in front of a star there is a small dip in the star`s brightness and they could see that there was also a dip in the brightness outside the object. This showed that there was a ring of material in a disc around the little object, Chariklo.

How the rings were formed is a mystery, but Uffe Grae has a clear theory.

He said what they were witnessing is perhaps the unveiling of an object that is in the middle of the same stage of development as the Earth and the Moon 4.5 billion years ago, when there was a giant collision between Earth and another planet.

Jorgensen said that in the collision, material hurled out in all directions, forming a circular disc around the Earth, which gradually condensed and formed the Moon. Similarly, we believe that another celestial body crashed into Chariklo and a good deal of material was cast out and formed rings. If the two discs around Chariklo gathers and forms a moon, it will be approximately 2 km in diameter.

The results have been published in the journal Nature.