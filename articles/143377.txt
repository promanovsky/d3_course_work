The OnePlus One offers Samsung Galaxy S5 performance for less than half the price. If that doesn't make it attractive it also has a customisable OS and a 1080p display. There are a few downsides, but overall the OnePlus One is amazing value for money.

The Google Nexus 4, the Google Nexus 5, the Motorola Moto G, and the OnePlus One; this 5.5-inch bolt from out of the blue (well, China) joins an exclusive list of smartphones that offer an awful lot of smartphone for not a lot of money.

But having used the OnePlus One, I'm wondering whether it needs to be placed in a category all of its own.

Here is a device that rivals the HTC One M8 and the Samsung Galaxy S5 for raw specs, but at a new impressively low cost of just £219, $299 (around AU$320). That's less than half the price of those aforementioned big hitters.

It can even go toe-to-toe with some of the newer flagship devices, such as the HTC One M9, the Galaxy S6 and the Nexus 6.

Initially you were able to order a 16GB model for £229, $299, but that option was removed for a while from the official shop. It is now back on there with a Silk White version, but if you want one, you'll need to be fast.

The company has kept the OnePlus One relatively exclusive, though over the last few months it's opened the door slightly to non-invitees. Every Tuesday, the OnePlus One becomes available for 24 hours beginning at 8 am GMT. The door promptly closes afterwards, but at least access is loosening up slightly.

You can buy the OnePlus One from other stores, with prices for the 64GB edition reaching as low as £209 (around $324, AU$441) if you shop around, though if you can we'd still recommend buying from OnePlus directly for peace of mind.

The OnePlus One's successor, the OnePlus 2, is now official, and brings some beefed up specs for another tempting price. Although this means the OnePlus One feels slightly dated, it brings the added benefit of OnePlus lowering the price of the original even further.

Much of my early time with the OnePlus One was spent warily turning it around in my hands, like some kind of mysterious artefact of unknown origin, not quite ready to believe what was being promised of it. There has to be some compromise here, right?

Well, yes there is. In fact, there are several. But it's staggering how small they seem when weighed against that double-take-inducing price tag.

An issue to get out of the way early on is the availability of this handset. OnePlus started out with a slightly strange invite-only system, limiting the number of people who can order a handset.

It was so the startup firm can keep on top of production, but meant you had to hunt around for an invite - or try your luck with a 24 hour pre-order session the firm appears to be doing every now and then.

However since its launch the OnePlus One is now far more easy to get hold of, with the rather convoluted invite system moving over to the new OnePlus 2.

OnePlus will only ship the One to 16 countries, so if you're not in one of the following you're pretty much out of luck. Those countries are; Austria, Belgium, Canada, Denmark, Finland, France, Germany, Hong Kong, Italy, Netherlands, Portugal, Spain, Sweden, Taiwan, United Kingdom, and the United States.

Usually when a cheap smartphone boasts specifications that bloody the noses of the big boys, it's the design that suffers. It's far harder to make a solid, stylish, and hard-wearing mobile device than it is to throw in the latest off-the-shelf chip from Qualcomm.

However the OnePlus One is a pleasure to hold and to use. Okay, so it lacks the HTC One M9's gorgeous metallic sheen, and you won't turn any heads when you take it out of your pocket like you would with a flashy iPhone 6 Plus. But show me the phone that does.

The OnePlus One nevertheless feels great in the hand. It's primarily made up of a quality matte plastic shell that extends around the back and sides of the device. This isn't a unibody construction, and this rear panel can be removed for customisation purposes, but it's firmly fixed in place with minimal creaking or flexing.

There's a metal-effect plastic rim that separates this rear cover from the glass front, which cheapens the effect ever-so-slightly, but it's thin and unadorned. It does mean that the aforementioned glass frontage appears to stand out rather than melding into the body of the phone, but it's not an unpleasant effect.

All in all, it looks and feels like something of a cross between the Nexus 5 and the Nokia Lumia 1520.

The OnePlus One is not a particularly slim or light device, but then nor is it an absolute brick. At 152.9 x 75.9 x 8.9mm, its dimensions make it only slightly larger than the LG G4 and Sony Xperia Z3, the latter of which has a smaller 5.2-inch screen. What's more, the OnePlus One is three grams lighter than the Sony at 160g.

Of course, this is still a monster of a phone when you compare it to older or smaller devices. I always thought of my trusty old HTC One X as a bit of a beast, but the 4.7-inch phone feels positively dainty next to the OnePlus One. Meanwhile my iPhone 5S looked like a (rich) child's toy when held next to it.

I've mentioned it a few times now, but the OnePlus One's 5.5-inch display really is quite the specimen. At 5.5-inches it's bigger than both the One M8's and Galaxy S5's, though it has the same 1920 x 1080 Full HD resolution. Admittedly that makes it a little less pixel-dense, but with 401ppi I defy anyone to call it anything but sharp.

If you're thinking that OnePlus may have cut corners with the quality of this display, then think again. It's an IPS display, which means it's sharp and accurate even when viewed from an angle, and it's made by JDI, the company responsible for the One M8's excellent screen.

The default brightness seems a little weak, but crank it up and you'll get a picture that truly pops, with impressively deep blacks.

Badlands

Perhaps the best illustration of this is to boot up the gorgeous Badlands game with its inky-black silhouettes layered over detailed amber backgrounds.

It hasn't been plain sailing for the OnePlus One's display though, with a few users taking to the web complaining of a "yellow hue" at the bottom of the screen. OnePlus sent TechRadar two handsets, and having used both I can either that neither have suffered from this issue.

Around the back of the OnePlus One is the vaguely oblong black camera element that houses the lens and dual-LED flash. This has been allowed to jut out slightly, its flat surface peeking above the curved shell. I quite liked the effect, especially in concert with the funky OnePlus One logo situated below.

OnePlus has housed a tiny pair of stereo speakers on the bottom edge of the device - as held in portrait view - with two telltale rows of machined holes either side of the microUSB port.

Button placement is strong, with the power key situated two thirds of the way up on the right hand side and the elongated volume rocker opposite on the left hand side.

This is ideal for a device as large as the OnePlus One, as they always fall under a thumb or finger, whereas a top-mounted button would have required some finger contortion to reach single-handed.

OnePlus has also included permanent capacitive hardware keys underneath the screen, which proves to be a mixed blessing. There's a menu key, a home key, and a back-up key in that order. I found it very hard to see these keys, particularly in daylight, as they don't light up very much at all.

They're also mapped a little oddly by default, with multitasking set to an awkward double-tap of the home button.

On the plus side, you don't have to use these keys at all, and you can also remap the keys to your liking. We'll discuss the OnePlus One's impressive customisation potential in greater detail over the next few sections.

Just about the only glaring weakness of the OnePlus One's external design comes in the form of its SIM tray. It looks to be made of a cheaper, rougher form of plastic, and I found the access hole to be an absolute pig to use with my iPhone tool (which I had to use as the OnePlus didn't come in its packaging).

Indeed, it seems as if this SIM accessibility was a problem for whoever used the phone before me, as the hole had a tatty and worn look to it, like it had been hand-drilled with a Black and Decker and a cheap drill bit.

This is the only external sign that you're dealing with a new manufacturer's first attempt at a high-quality smartphone.