Rolls Royce is set to lose out on £2.6bn after Emirates Airline cancelled an order for 70 Airbus A350s.

The Dubai-based airline made the decision after reviewing their fleet requirements.

UK-based Rolls Royce, which provides engines for the aircraft, said that the move would take 3.5% from its order book, but said it was confident of filling the void.

"While disappointed with this decision, we are confident that the delivery slots which start towards the end of this decade vacated by Emirates will be taken up by other airlines. Demand for the Airbus A350 remains strong, with more than 700 aircraft and 1,400 Trent XWB engines already sold," said a statement on Rolls Royce's website.

The order for 50 or the A350-900s and 20 of the A350-1000 was placed back in 2007 with delivery expected in 2019. At the last Dubai Airshow, the airline ordered a further 50 craft.

An Airbus spokesperson has told IBTimes UK that since manufacturing had not begun on the orders, no jobs in the UK or further afield will be lost, since the company is confident of being able to replace the orders with other customers.

"Airbus is very confident in its A350 XWB programme. Half a year before entry into service, the A350 XWB order book stands at a healthy 742 firm orders. Interest in the game-changing A350 has always been very high with customers. Airbus expects the A350 order book to continue growing in 2014," said the pan-European manufacturer in a statement.

The aviation sector has enjoyed something of a renaissance over the past couple of years, with huge orders coming in from the Middle East and East Asia.

Earlier this year, VietJetAir (VJA)'s agreed to pay US$9bn for 63 Airbus craft, inking a deal first thrashed-out in Paris last September.

The package included US$6.4bn of purchases (including 42 of the A320 Neo, 14 of the A320 CEO and seven A321 CEO) as well as seven leased family aircraft worth US$2.6bn.

Previously, the boom in demand had been driven by orders placed at Dubai Airshows editions, as regional airlines expanded their operations to accommodate Dubai's growing status as a transport hub and growing passenger volume.

In 2013, Middle Eastern airlines reported 14% growth in passenger traffic – the highest since records began.

At last year's air show, Emirates spent a record $99bn on orders for 150 Boeing 777 aircraft and 50 A380 superjumbos.