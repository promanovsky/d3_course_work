SAN FRANCISCO -- The Federal Trade Commission and California Atty. Gen. Kamala Harris say that Facebook is misinterpreting how a children’s privacy law applies to teen privacy in a move that could undercut the giant social network in a federal court case in California.

Facebook users sued the company for using their images in ads on the service without their consent and later settled the class-action lawsuit in 2012.

Children’s advocates are challenging the settlement in an effort to require Facebook to get explicit permission from parents before using the personal information — as well as the images, likes and comments — of teens in advertising.

The advocates have asked the U.S. 9th Circuit Court of Appeals to vacate the Facebook settlement. They say the settlement violates the law in seven states, including California, that require parents’ permission before a child’s image can be used in advertising.

Advertisement

Facebook argued that because the Children’s Online Privacy and Protect Act, known as COPPA, only protects kids 12 and under, that states cannot enforce their own laws on teen privacy.

In a court filing in June, Facebook said that because federal privacy protections don’t apply to teens their activities on the Internet “should not be subject to parental consent requirements, even under the auspices of state law.”

Both the FTC and the state attorney general filed amicus briefs with the 9th circuit, disputing that interpretation of the law.

The FTC said that Facebook’s position is “wrong and should be rejected.” The agency did not take a position on the case.

Advertisement

The state attorney general said “possible preemption of state law is of concern.”

“Protecting children’s information is of particular importance, because of their still-developing capacities and the potential for misuse of their information on their futures. The attorney general has developed numerous consumer privacy protection guides, including instructions for parents on how to protect their children’s privacy online,” the brief states.

A Facebook spokeswoman said the California attorney general and the FTC “are not challenging the fairness of the settlement, which is the sole question on appeal.”

“The court correctly found that the settlement was a fair and reasonable resolution of the claims in this case after fully considering these issues,” she said.

Advertisement

University of Washington law professor Ryan Calo said it was unclear what effect the FTC and the attorney general weighing in would have on the case.

“The FTC does enforce COPPA so courts are likely to listen to what it has to say,” Calo said. “But I don’t know if it’s enough to overturn a district court judge’s ruling.”

Hudson Kingston, lawyer for the Center for Digital Democracy and other privacy advocates who are challenging the settlement, said the reading of the law by regulators “undermines one of Facebook’s key arguments that it can get out of this case without first addressing its weak privacy protections for teens.”

ALSO:

Advertisement

Facebook loosens privacy policy on teens’ posts

Facebook social ads settlement under fire from children’s advocates

Facebook removes controversial line about teens in privacy policy