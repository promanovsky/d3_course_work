The Dear Leader is mad. North Korea denounced Seth Rogen and James Franco's upcoming film, The Interview, again on Wednesday, June 25—only this time, dictator Kim Jong-un threatened the U.S. to ban the movie from being released.

A statement released by North Korea's official news agency, KCNA, said The Interview was created by "gangster filmmakers" and should not be screened.

"The act of making and screening such a movie that portrays an attack on our top leadership… is the most wanton act of terror and act of war, and is absolutely intolerable," a foreign ministry spokesperson said. The film, which hits theaters in October, tells the story of a talk show duo who score an interview with Kim, played by actor Randall Park. The CIA recruits Rogen and Franco's characters to assassinate the "supreme leader" of the Hermit Kingdom.

Added the spokesman: "If the U.S. administration allows and defends the showing of the film, a merciless counter-measure will be taken." (North Korea and U.S. relations have been strained since the mid-20th century.)

This is the second time in the last week that North Korea has publicly lashed out against The Interview. Speaking with The Telegraph UK on Friday, June 20, a top North Korean official called the movie a symbol of American society's "desperation." The cryptic statement added that President Barack Obama "should be careful in case the U.S. military wants to kill him as well."

Rogen, 32, the recipient of North Korea's backlash, has addressed the controversy with (what else?) comedy. "People don't usually wanna kill me for one of my movies until after they've paid 12 bucks for it," Rogen wrote Wednesday, June 25. "Hiyooooo!!!"