She said her first boyfriend when she was in grade 9 gangraped her with six of his friends. (AP)

'Baywatch' star Pamela Anderson has revealed that she was molested, raped and even gangraped when she was young.

The 46-year-old star said there was a time in her life when she wanted to be "off this earth" but her love for animals saved her during the launch of the Pamela Anderson Foundation here.

"I feel now might be the time to reveal some of my most painful memories... I did not have an easy childhood. Despite loving parents. I was molested from age 6-10 by my female babysitter.

"I went to a friend's boyfriend's house - while she was busy. The boyfriend's older brother decided he would teach me backgammon which led into a back massage which led into rape - my first heterosexual experience. He was 25 years old, I was 12," Anderson said while reading from a note during the launch.

She said her first boyfriend when she was in grade 9 gangraped her with six of his friends.

"Needless to say I had a hard time trusting humans-I just wanted off this earth," Pamela Anderson said.

The star recalled how she did not reveal these incidents to her mother, who tried to make ends meet by working two jobs as a waitress while her father was an alcoholic.

"Dad didn't always come home leaving us in tremendous pain and worry. I couldn't bare to give her any more disruptive information. I couldn't break her heart any more than it was breaking. I kept these events to myself. Sometimes when you smile, it's not because your happy. It's because your strong," she said.

Anderson credited her "affinity with animals" for saving her and she vowed to protect them and only them, who were the only real friends she had until her children were born.