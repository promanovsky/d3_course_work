The wait is finally over, Apple fans — the 25th annual Worldwide Developer Conference is underway. As has been the case in years past, Apple CEO Tim Cook began today’s huge event by taking the stage and delivering a State of the Apple Union address, of sorts. Apple’s chief executive began the WWDC 2014 keynote strong by unveiling OS X 10.10 Yosemite.

Cook noted that Apple’s last big release, OS X 10.9 Mavericks, is already on 51% of Mac computers. Craig Federighi then took the stage to discuss the key details of OS X 10.10.

Yosemite has a brand new user interface that carries forward many of the design elements Apple adopted in iOS 7. It’s nowhere near as flat, of course, but the links between the two platforms are now more clear than ever.

Of course, the bright look of iOS isn’t always appropriate for a desktop platform, so Apple introduced a dark mode in OS X 10.10.

Notification Center has been completely revamped as well with a brand new Today view, and additional information can now be added to Today, such as info from widgets.

Also completely redesigned is Spotlight, which adopts plenty of features from popular third-party universal search application Alfred App. The new spotlight will search the web as well as local data, and the UI is much more user friendly now.

Spotlight will also display live information. For example, if you search a movie, nearby showtimes will appear. If you search a location, a map will appear in Spotlight.

Following Spotlight, Apple also announced iCloud Drive.

iCloud Drive is basically a more full-feature Dropbox competitor. It will work on OS X, iOS and Windows.

MailDrop is another new OS X 10.10 feature that Apple unveiled on Monday. It solves the problem of sending emails with large attachments but automatically encrypts and stores them in the cloud so the messages are not rejected.

Safari has been completely redesigned for Yosemite.

Everything in the Safari interface now fits into a single bar at the top of the screen, leaving the rest of the UI free to display Web content.

The latest Safari build is also an industry leader when it comes to power consumption, according to Apple. The software has been optimized to use as little power as possible while streaming video or running various scripts.

Apple also on Monday unveiled a new Continuity feature that adds a wide variety of features that connect OS X and iOS devices more closely.

HandOff is one of the most impressive Continuity features. It allows users to work on one device and proximity awareness will allow that device to see other Apple devices nearby. A simple click can then transfer whatever you’re working on between devices.

Apple also announced that SMS messages will be included in the Messages app on OS X when iOS devices are connected, and phone calls will be able to use a Mac as a speaker phone.

Phone calls can be placed on a Mac running OS X 10.10 Yosemite — Craig even placed a call to Dr. Dre during the event.

Other data such as URLs can also be shared between devices with a simple flick gesture.

OS X 10.10 Yosemite will be made available to developers as a beta on Monday. It will then launch to the public this fall for free. Apple did announce a public beta program, however, that will give non-developers access to beta releases. Sign-ups will be taken on the Apple website.

For the latest breaking news from Apple’s WWDC 2014 keynote, visit our WWDC 2014 hub!