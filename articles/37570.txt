— Five former employees of imprisoned financier Bernard Madoff were convicted Monday at the end of a six-month trial that cast them as the long arms of their boss, telling an elaborate web of lies to hide a fraud that enriched them and cheated investors out of billions of dollars.

The trial, one of the longest in the storied history of Manhattan federal court, was the first to result from the massive fraud revealed in December 2008 when Madoff ran out of money and was arrested. And it was a renunciation of Madoff’s claim when he pleaded guilty to fraud charges in March 2009 before trial that he acted alone.

“The evidence was just overwhelming,” juror Craig Parise told reporters as he left the courthouse.

Another juror, Sheila Amata, said she hoped “this brings some level of closure” to the victims. She added that the “facts spoke for themselves” in the case and that Madoff, who is serving a 150-year prison sentence, “seemed to have a split personality.” Evidence showed him showering employees, friends and some select customers with favors and riches while he plundered the investment accounts of others.