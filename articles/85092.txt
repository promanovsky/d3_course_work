Microsoft has been accused of crossing a line and inconveniencing computer users who have yet to ditch Windows XP before it stops supporting the operating system on Tuesday.

From Friday a "pop-up" began appearing whenever XP users who run Microsoft’s free security software, Microsoft Security Essentials, turned on their computers.

Closing time: The end is nigh for Windows XP.

The pop-up warns XP support is about to end and directs users to a site where they can find out more about upgrading to a new operating system.

A Microsoft spokeswoman was unable to confirm whether there was any way of disabling the pop-ups, other than turning off Microsoft Security Essentials, which would leave XP users more exposed to malware.