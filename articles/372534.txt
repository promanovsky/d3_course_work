Samsung on Wednesday launched a new 'camera centric' smartphone Galaxy K Zoom, for pre-order on Amazon.com.

Galaxy K Zoom is equipped with a high end 20.7 MP BSI sensor camera, with optical image stabilization, 10x optical zoom, auto focus which comes with different picture modes such as 'SMART' Mode, 'Expert' Mode and 'My Mode'.

The smartphone sports a 4.8-inch Super AMOLED HD display with a 1280X720 pixel resolution powered by a hexa-core processor. The phone will run on Google's iOS, Android 4.4 KitKat.

The phone is out for pre-order on online shopping website Amazon.com; however the phone will also be available on Samsung's retail stores, priced at Rs. 29,999.