A report published Thursday in the journal Pediatrics raises questions about the safety of water births.

Immersion in water during the first stage of labour may be associated with decreased pain or use of anesthesia and decreased duration of labour, according to authors from the American Academy of Pediatrics and the American College of Obstetricians and Gynecologists.

But in the second stage of labour, the researchers found no evidence of benefit for mothers or infants linked to water delivery. In addition, they noted case reports of, "rare but serious adverse effects" in the newborn, including infection, umbilical-cord rupture while the newborn is moved through the water pool, respiratory distress and drowning.

Story continues below advertisement

The authors concluded that underwater delivery should be considered, "an experimental procedure that only should be performed within the context of an appropriately designed clinical trial with informed consent," they wrote.

Dr. Michelle Butler, director of the midwifery program at the University of British Columbia, questioned the report's description of water delivery as experimental. "I'm a little bit concerned about that," she said, noting that water births have been documented extensively and used safely in Europe for decades.

In Britain, approximately 1 per cent of births include a period of immersion, according to the report.

Giving birth in a gently heated pool of water was once considered a fringe choice, but in recent decades, water births have gone mainstream. Celebrities including Pamela Anderson, Gisele Bundchen, Jennifer Connelly and Thandie Newton have reportedly had water births, and many hospital maternity wards in North American now offer tubs for immersion during labour.

The report noted women may choose water births to avoid epidurals, since immersion in warm water may provide pain relief. And in theory, water births offer infants a smoother transition to the outside world as they emerge from the amniotic fluids of the uterus into a warm bath.

However, the safety and potential benefits of water birth are difficult to evaluate, according to the report authors, whose survey of the available scientific literature included a 2009 systematic review by the esteemed Cochrane Collaboration.

Studies on water births tend to have very small sample sizes and cannot be double-blinded due to the difficulty in creating placebo conditions comparable to a water birth. Moreover, labour and birth outcomes are influenced by many factors unrelated to immersion, including differences in the labour environment and the expertise of the health-care provider, as well as the timing and frequency of examinations, the authors noted.

Story continues below advertisement

Butler emphasized that adverse events during water births are extremely rare. She added that clinical trials have not shown that adverse events are more common in water delivery than in conventional births. In the 2009 Cochrane review, water births were linked to decreased use of pain medication, including epidurals, as well as a shorter first stage of labour. The Cochrane analysis found "no evidence of increased adverse effects to the fetus/neonate or woman from labouring in water or water birth," the report stated. But it also found no benefits to infants associated with maternal immersion during labour.

And, Butler pointed out, clinical trials may have overlooked benefits to infants that are difficult to quantify, including the postpartum experience. Research has shown that many women feel more relaxed and have a greater sense of control after undergoing water birth, she said. "This can be important in how the woman bonds with the infant."

Other considerations include the mother's experience of care, including pain management, as well as how she relates to her subsequent pregnancies based on her labour experience. "It boils down to how you measure that," Butler said.

As for safety, Butler noted that registered midwives follow evidence-based protocols regarding water births. Guidelines in provinces such as B.C. require midwives to monitor the mother's vital signs and the fetal heart rate, use a floating thermometer to keep the water temperature constant, encourage the mother to remain hydrated and deliver the placenta outside the tub to assess maternal bleeding.

Water births are not recommended for pre-term labour or pregnancies that involved complications, mothers infected with hepatitis B or C or HIV, and mothers who have been sedated, among other conditions.

Registered midwives are well aware of the contraindications for water delivery, such as preterm labour, Butler said, and when clinical practice guidelines are followed, "the research suggests it's at least as safe as usual care."