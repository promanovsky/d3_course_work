Don't laugh, but the discovery of the oldest known human poop is offering valuable scientific insight into the life of Neanderthals who lived in Spain some 50,000 years ago.

Scientists said on Wednesday they found five samples of human fecal matter at an archeological site called El Salt, in the floor of a rock shelter where Neanderthals once lived.

Analysis of the samples provided a new understanding of the diet of this extinct human species, offering the first evidence that Neanderthals were omnivores who also ate vegetables as part of their meat-heavy diet, they said.

'It's like any other fossil'

"So far, it is the only fossil evidence that gives us information of the ingestion and the regular meals of our ancestors," said Ainara Sistiaga, a geoarchaeologist at the Massachusetts Institute of Technology and University of La Laguna, who was one of the researchers.

"Understanding the diet of past human species closely related to our own will help us gain perspective on our evolutionary constraints and adaptability," Sistiaga added.

The researchers examined the fecal fossils for biologically derived indicators of the types of food the Neanderthals ate.

Their findings indicate that Neanderthals predominantly consumed meat, as suggested by high amounts of one such "biomarker" called coprostanol formed by the bacterial reduction of cholesterol in the gut. But they also found evidence for significant plant intake as shown by the presence of a compound called 5 beta-stigmastanol, found in plant sources.

"It's like any other fossil," added MIT geobiology professor Roger Summons, another of the researchers. "Fossils provide our most direct link with organisms from the past."

Neanderthals are the closest extinct relative to our species, Homo sapiens, and disappeared after early modern humans first trekked into Europe from Africa. Neanderthals are believed to have prospered across Europe and Asia from roughly 250,000 to 40,000 years ago and interbred with Homo sapiens before vanishing.

Berries, nuts part of diet

Scientists previously have hypothesized that Neanderthals were largely carnivorous with perhaps some vegetables but never before had direct evidence like these fossils provided.

"Sometimes in prehistoric societies, individuals used their teeth as tools, biting plants among other things. We can't assume they were actually eating plants based on finding microfossils in teeth," Sistiaga said.

The El Salt site shows evidence of long-time Neanderthal occupation, with numerous fireplaces and stone tools as well as animal and human remains.

The researchers could not identify the specific foods eaten but noted that animal remains suggested the Neanderthals hunted deer and horses. Sistiaga said evidence showed the presence of berries, nuts and tubers but "we cannot say anything about what kind of plants were actually eaten."

Neanderthals were shorter and stockier than the sleeker Homo sapiens. Many scientists dispute the outdated notion of Neanderthals as dimwitted brutes, pointing to evidence of complex hunting methods, likely communication via spoken language, and use of symbolic objects and pigments, probably for body painting.

The study was published in the journal PLOS ONE.