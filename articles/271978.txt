Patrick Ryan

USA TODAY

Well, this is a cents-ible move: Fans can purchase 50 Cent's new album Animal Ambition, out today, using Bitcoins, a hotly debated digital currency that doesn't require a bank.

The rapper's fifth studio album can be bought at shop.50cent.com using BitPay, a payment service that processes Bitcoin transactions. "We are excited to see high profile independent artists use Bitcoin and 50 Cent's trail as an innovator is outstanding," BitPay exectuve chairman Tony Gallippi said in a statement.

It's not the first time a rapper has had a brush with the popular currency: Kanye West filed a lawsuit in January against the creators of "Coinye West," a themed Bitcoin bearing his cartoon image.