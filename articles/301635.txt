The U.S. Patent and Trademark Office has canceled the trademark registration of the Washington Redskins, calling the name “disparaging to Native Americans.”

Although that doesn’t mean the Redskins will have to change their name, it limits the team’s legal options when third parties use the name and logo on T-shirts, posters and other items.

In other words, it ramps up the pressure on Redskins owner Daniel Snyder to change the name of his franchise because it hits him, and the NFL, in the wallet.

In a written statement, the Oneida Indian Nation said the development emphasizes that “taxpayer resources cannot be used to help private companies profit off the promotion of dictionary defined racial slurs.”

Advertisement

This is not the end of the issue, however. Proponents of a Redskins name change won at this stage in 1999 too, but the franchise and the NFL won an appeal to the U.S. District Court in 2009. The court ruled the plaintiffs didn’t have standing to file the lawsuit.

The team has the right to appeal this time around as well and will retain its trademark protection during the process.

Those petitioning the patent office included members of the Navajo Nation, Paiute Indian Tribe of Utah, Kiowa Tribe of Oklahoma and Muscogee Nation of Florida. All testified that they found the term offensive and disparaging.

Jillian Pappan, who described herself as Native American, compared the use of Redskins to a racial slur for African Americans, according to the decision. Pappan said people should not profit from dehumanizing Native Americans.

Advertisement

The patent office determined she and the others filing the petition had a real, personal stake in the outcome.

Read the decision: Amanda Blackhorse, Marcus Briggs-Cloud, Philip Gover, Jillian Pappan, and Courtney Tsotigh v. Pro-Football, Inc.