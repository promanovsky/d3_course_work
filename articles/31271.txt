It’s not just AT&T and other major US carriers who are offering attractive deals on the Samsung Galaxy S5, it seems the big retailers are jumping in the pre-orders bandwagon as well.

After AT&T $50 discount on the Gear 2 and Gear 2 Neo, as well as the $50 Google Play Credit from U.S. Cellular, Radioshack is also taking pre-orders of the Samsung Galaxy S5. It gets better here; the retailer is offering users with a $50 discount if they pre-order the device.

“The Samsung Galaxy S 5 will be one of the hottest mobile devices this year and RadioShack customers will be among the first to get a great deal on this new smartphone,” said Paul Rutenis, chief merchandising officer for RadioShack. “RadioShack associates are experts in helping customers find the newest technology and enhancing that technology with other state-of-the-art devices to accomplish even more.”

It’s not just the $50 discount on the latest flagship from the Korean OEM, if you have a spare smartphone lying around, you can take it to Radioshack and get a $75 to $300 credit which can be used to purchase the Samsung Galaxy S5, provided that the smartphone is in a good working condition.

Along with the Samsung Galaxy S5, the retailer will also offer Samsung’s latest smartwatches, including the Gear 2, Gear 2 Neo and Gear Fit.

How many of you are planning to get the Samsung Galaxy S5?

Source: Radioshack, PhoneArena

Have a look at our Best Smartwatch 2014 guide for more information on the best smart watches available at the moment.

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more