Sir Mick Jagger has said he is "still struggling to understand" how his "lover and best friend" could have ended her own life, after the apparent suicide of his long-term girlfriend L'Wren Scott.

In a message posted online, the Rolling Stones frontman said he will "never forget" the designer and former model who was found dead in her New York apartment on Monday morning.

The band also pulled the plug on their entire tour of Australia and New Zealand, which had been due to begin today, in the wake of the tragedy, which shocked the fashion world. The shows are expected to be rescheduled.

Sir Mick said: "I am still struggling to understand how my lover and best friend could end her life in this tragic way.

"We spent many wonderful years together and had made a great life for ourselves.

"She had great presence and her talent was much admired, not least by me.

"I have been touched by the tributes that people have paid to her, and also the personal messages of support that I have received. I will never forget her."

Scott (49) had been dating the 70-year-old rock star since 2001 after they met when she was the stylist at a photoshoot.

Shortly after her death was announced, it emerged that her British fashion company had been losing millions of pounds.

Accounts filed with Companies House in October show Scott's British company, LS Fashion Ltd, was running at a loss of more than £3.5m in 2012, up from more than £2.5m the previous year.

The documents also show the firm owed more than a million pounds, despite having assets totalling a similar amount. Scott, who was 6ft 4ins tall and towered at least half a foot over her boyfriend, became well-known in Britain after she got together with Sir Mick.

She designed stage outfits for notable headline performances by the band at Glastonbury Festival and Hyde Park in recent years.

It is understood that police do not suspect foul play. A spokesman for Scott asked for the privacy of her family and friends to be respected. The Stones said they were "deeply sorry and disappointed" to pull out of the six Australian shows, which should have begun in Perth, and one show in New Zealand, but hoped fans would understand.

A statement said: "Mick Jagger, Keith Richards, Charlie Watts and Ronnie Wood wish to thank all of their fans for their support at this difficult time and hope that they will fully understand the reason for this announcement."

New York City Police said Scott was found unconscious at her home in Manhattan.

PROFILE

L'Wren Scott was originally called Luann Bambrough, and brought up as a Mormon in Utah, but changed her name to launch her modelling career in her teens.

She worked in Paris with the likes of Thierry Mugler and Karl Lagerfeld, then returned to the US as a stylist. She was a wardrobe consultant for films Eyes Wide Shut and Ocean's Thirteen.

Scott launched her fashion line in 2006, with fans of her work said to include Sarah Jessica Parker, Nicole Kidman and Michelle Obama.

Belfast Telegraph