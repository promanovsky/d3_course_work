Australia has nudged higher in the rankings of the best country in the world to be a mother but needs to boost the number of women in parliament, Save the Children says.



Australia tied for ninth place with Belgium on the London-based aid agency's 2014 state of the world's mothers index of 178 countries, released on Tuesday. Australia was ranked 10th in 2013.

Finland, Norway and Sweden came in at numbers one, two and three this year, with Iceland, the Netherlands, Denmark, Spain and Germany also polling ahead of Australia.

The worst places to be a mother were Somalia, which ranked last at 178, behind the Democratic Republic of the Congo at 177 and Mali and Niger – both placed at 175.

Save the Children Australia's Dr Kate Worsley said Australia needed to do better on women's income and political participation to climb the list.

She said only 31% of federal MP positions in Australia were held by women, compared with Finland on 42%.

"We need to look at women's involvement in politics and that's because it ... means women's and children's issues are heard," said Worsley, who's a senior health adviser at the charity.

"Australia could do better in that area."

She said Australia would probably maintain its position in 2015 so long as support for women, especially during pregnancy, was maintained.

She urged policy makers to focus on the "risks to pregnant women and children and make sure we really continue to work to keep that level of service high".

She said women Australia remained much better off than women in countries further down the list such as Somalia, where one in 16 women died in childbirth. "In Australia it's one in 8000," Worsley said.

She also called on the federal government to "ringfence" Australia's foreign aid spend in next week's budget, noting that women overseas "still need the government's support".

Save the Children estimates that 800 mothers and 18,000 young children die each day from largely preventable causes.

Almost a third of child deaths happen in west and central Africa, while another third occur in south Asia, where high mortality rates are increasingly concentrated in socially excluded communities.

"Mothers and children face the highest risks of death, and steepest roads to recovery, in crisis situations that occur in fragile settings," the reports says.

The report compared 178 countries in terms of maternal health, child mortality, education and levels of women's income and political status.

