A Massachusetts mom who would do anything for her kids -- literally -- threw herself into the path of a rolling car to keep her twin girls from rolling straight into traffic.

Mindy Tran, of Lawrence, Mass., parked her car in the driveway of her new apartment and got out with her twin daughters, 2-year-olds Saleen and Sydney, still inside. When the car suddenly started to roll down the hill and Tran no longer had the strength to hold it, she did the only thing she could think to do.

Advertisement

"I had to use myself as a speed bump to slow it down enough for one of my neighbors to get in and completely stop the car," she told WCVB.

It was then that a neighbor was able to jump into the driver's seat to stop the vehicle. Firefighters eventually lifted the car off Tran and took her to the hospital, where she has remained since March 6.

She will have to undergo extensive rehabilitation, as the incident left her with severe pelvic fractures, a crushed knee and separated shoulder.

She has set up an online fundraising account to help pay for her medical expenses.

[WCVB, USA Today]