What is MERS coronavirus?

Middle East Respiratory Syndrome (MERS) is viral respiratory illness that first surfaced in Saudi Arabia in April 2012. It is caused by a coronavirus called MERS-CoV. The disease is referred to as Coronaviruses as they have crown-like projections on their surfaces. "Corona" in Latin means "crown" or "halo".

Recently, it has been confirmed that Arabian camel is the origin of the infectious disease.

Here are some important facts about this virus:

-The first case of MERS coronavirus was detected in June 2012 in a patient from Saudi Arabia who suffered from severe pneumonia.

- On May 2, 2014, the first US imported case of MERS was confirmed in a traveler from Saudi Arabia to the US.

- Studies suggest that the virus can spread from infected people to others through close contact.

-World Health Organisation has already notified that the potential of novel virus to be a pandemic or a global epidemic is there. However, WHO has stressed that there is no need to panic.

-The Kerala health department is on high alert after increasing number of the Middle East Respiratory Syndrome (MERS) cases have been reported in Saudi Arabia.

-MERS-CoV is not spread from the same coronavirus that caused severe acute respiratory syndrome (SARS) in 2003.

-Risks related to MERS-CoV include pneumonia, kidney failure and death.

Symptoms:

The symptoms of the disease are:

-Cough

-Mucous

-Shortness of breath

-Chest pain

-Fever

-Diarrhoea (in some cases)

-Pneumonia

-Kidney failure

Treatment:

According to CDC and WHO, there are no specific treatments and vaccine recommended for illnesses caused by MERS-CoV. Utmost, doctors can provide supportive medical care to help relieve the symptoms and provide required treatment to prevent, control or relieve complications and side effects.

Tips for protection against the virus:

Though there is no treatment available for this virus, but people can follow some preventive measures to ensure their safety. Here are some tips suggested by CDC:

-It is advised to wash one's hands often with soap to avoid the spread of infection. If soap and water are not available, use an alcohol-based hand sanitizer.

-Try covering your nose and mouth with a tissue when you cough or sneeze.

-If your hands are unwashed, avoid touching your eyes, nose and mouth.

-Avoid close contact, such as kissing, sharing cups, or sharing eating utensils, with sick people.

-Keep your surroundings clean. Disinfect frequently touched surfaces, such as doorknobs.

Compiled by: Ritu Singh and Shruti Saxena