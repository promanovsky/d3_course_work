Apple are expected to launch their new iPhone 6 next month, the company is said to be launching a 4.7 inch version of the handset, and a 5.5 inch model is expected later in the year.

Now we have some new photos, of what are reported to be the new iPhone 6 next to Apple’s current model, the iPhone 5S, have a look at the photos below of the two handsets next to each other.

The iPhone 6 is expected to come with a number of new features and updated hardware over the current model, which will include a larger display and a faster processor.

Apple’s iPhone 6 will come with a 4.7 inch display and it is said to be powered by Apple’s second generation 64-bit mobile processor, the Apple A8.

We have also heard that Apple will launch a 128GB model of the iPhone 6, and the device will also come with Apple’s TouchID, which was introduced with the iPhone 5S.

The Apple TouchID in the new iPhone 6 is said to be a new fingerprint sensor module that is designed to be more durable and also accurate than the current one used in the iPhone 5S.

Apple are rumored to be holding a press event on the 9th of September and the company is rumored to launch the handset on the 19th of September, as soon as we get some more details, we will let you guys know.

Source Gizmobic, yaya888

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more