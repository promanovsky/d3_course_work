PITTSBURGH (KDKA) — Who is more worried about Alzheimer’s disease? Men or women?

“I had a mother-in-law pass away from it at age 58. With my mother-in-law, every second, she’d ask me the same question over and over,” says one woman at a local mall.

“Men forget a lot. Like, women take care of the children, the home, work, bills, and men, they just kind of sit back and relax,” says one young man.

“It doesn’t run in our family, so I’m not really concerned,” says another man.

A report from the Alzheimer’s Association says women are more likely to develop the disease: one in six women, compared to one in 11 men.

Because the risk goes up with age, and because women generally live longer than men, that could be part of why we see this pattern. But the interactions between hormones, brain structure, and genetics are areas to study further.

What we do know about reducing risk is the same for both genders.

“Eat a really healthy diet, get as much education as you can, and keep your brain stimulated, exercise,” says Allegheny General Hospital neuropsychologist Carol Schramke, PhD.

Around five million people in the United States have Alzheimer’s disease; 3.2 million are women.

“Women are more likely to be affected by getting the disease, and also are more likely to be affected by having to care for someone with the disease, so we get to carry more of the burden any way you look at it,” Schramke adds.

People lose their memory, and eventually the disease is fatal, on average, in 10 years.

RELATED LINKS:

More Health News

More Reports by Dr. Maria Simbra

Join The Conversation On The KDKA Facebook Page

Stay Up To Date, Follow KDKA On Twitter