Selena Gomez recently made it pretty obvious recently that she’s no longer friends with Kendall and Kylie Jenner, deleting them off her Instagram followers as well as pictures of the group from the Coachella Music Festival and according to a new report it was that excessive weekend that made the brunette realise the girls aren’t the right friends for her.

It seemed like a match made in teen heaven when brunette Gomez hooked up with the Jenner girls, two of the most powerful stars in young, hot, Hollywood. But things went wrong pretty quickly it seems, as Gomez turned cold a few days after their outing to Coachella.

According to TMZ a source has revealed the reason behind the break up wasn’t anything to do with Kylie allegedly getting too close to Gomez’s on/off boyfriend Justin Bieber, but the weekend was too much partying for the 21 year-old.

The source apparently told the website that after being surrounded by drugs and alcohol and “hangers on” at Coachella, the Come And It babe realised she should stay away from that environment and so ditched Kylie and Kendall.

The report went on the claim the star is “sick” of that type of “scene” and sees it and the Jenner girls as a negative influence on her life.

Selena entered rehab for two weeks in January to receive treatment for undisclosed reasons. While a rep insisted it was not substance related, she has since hinted at difficulties in her life and making the right choices when it comes to those who influence her.

TMZ were reportedly told that the Spring Breakers star is determined not to be pulled down the wrong path.

What exactly happened at Coachella?