Vast oceans of water are believed to be sitting beneath Earth's surface locked into a newly discovered mineral called ringwoodite.

Scientists at the University of Alberta have found the first terrestrial sample of the water-rich gem that provides strong evidence of the existence of huge volumes of water under the earth.

Led by Graham Pearson, the team discovered that ringwoodite holds a significant amount of water, accounting for around 1.5% of its weight. The team believes the water is trapped between 410 and 660km beneath Earth's surface, somewhere between the upper and lower mantle.

Pearson, a diamond scientist, says the transition zone of Earth where they believe ringwoodite is sitting "might have as much water as all the world's oceans put together".

Published in the journal Nature, the team first discovered the mineral sample in 2008 in the Juina area of Mato Grosso, Brazil, where miners had found the host diamond in shallow river gravels. The diamond had been brought up to the surface through an extremely deeply derived volcanic rock.

The team only discovered ringwoodite by accident – they had been looking for a different mineral when they came across a dirty seemingly worthless brown diamond. After years of analysis, the 'worthless diamond' was confirmed as ringwoodite.

Ringwoodite, a form of the mineral peridot, has previously been found in meteorites but a terrestrial sample has never before been found – until now.

Pearson said the discovery has confirmed around half a century of experimental work by geophysicists, seismologists and other scientists analysing the Earth's interior. Their discovery lays to rest an age-old argument of whether the transition zone is as dry as a desert of water logged.

He said: "One of the reasons the Earth is such a dynamic planet is the presence of some water in its interior. Water changes everything about the way a planet works."