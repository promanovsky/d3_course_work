Disney Studios has confirmed that filming has commenced on Star Wars Episode VII at their Pinewood studios in London, despite the fact that the full cast has yet to be decided upon.

Speaking to The Hollywood Reporter, Disney studios chairman Alan Horn confirmed that filming has started without the full cast in place. He said: "We have a lot of them in place. We're just not completely done yet."

Horn also hinted that the rumoured problems with the script had been ironed out: "It has to be screenplay, screenplay, screenplay.

He added: "It actually is now."

Star Wars Episode VII, which will be directed by J.J. Abrams, is slated for release in December 2015, with original stars Harrison Ford, Mark Hamill and Carrie Fisher expected to play some part in the movie.

Gary Oldman, Jesse Plemons and Chiwetel Ejiofor are tipped to appear, while Girls actor Adam Driver will reportedly play the main villain.