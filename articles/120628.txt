New York: A New York regulator probing Credit Suisse`s role in enabling tax evasion has demanded a trove of additional documents from the bank, a person familiar with the situation said Tuesday.

Benjamin Lawsky, New York`s superintendent of financial services, last week subpoenaed Credit Suisse for banker`s emails, hard drives, personnel records, travel records, expense records and calendar entries.

The probe is investigating "whether any misrepresentations were made to the department, as well as the potential evasion of New York taxes," the source said.

The probe has also sought records of Roger Schaerer, the former head of Credit Suisse`s New York branch.

Schaerer was one of four Credit Suisse officials named in a February 2011 Department of Justice indictment who allegedly helped US customers open secret bank accounts in Switzerland to evade US taxes.

The indictment was expanded in July 2011 to name three additional former Credit Suisse bankers plus the founder of a Swiss trust company that worked closely with Credit Suisse.

Lawsky has also sought documents from a US Senate committee that concluded in a lengthy February report that the second largest Swiss bank held assets from more than 22,000 US customers worth as much as $12 billion.