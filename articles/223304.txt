African American and Caribbean men have the highest rate of prostate cancer worldwide and the reason may be partly due to a Vitamin D deficiency, according to Northwestern University researchers.

A study of the relationship between the vitamin and melanin finds that Vitamin D deficiency leads to an increased risk of diagnosis for those two populations but not for Caucasian males.

"Our report is the first to describe the association of vitamin D deficiency and outcomes of prostate biopsies in high-risk men with an abnormal [blood test or clinical exam]," the study states. "If vitamin D is involved in prostate cancer initiation or progression, it would provide a modifiable risk factor for primary prevention and secondary prevention to limit progression, especially in the highest risk group of African-American men."

The study, published in the May issue of Clinical Cancer Research, notes prostate cancer is the most common cancer among American men and the second leading cause of cancer-related deaths. The African American male population is 60 percent more likely than Caucasians to face a cancer diagnosis.

The research is just the latest in a flurry of reports regarding Vitamin D use and intake. One recent study claims vitamin D level doesn't play into human falls, another claims high levels can stave off breast cancer. Another report reveals low vitamin D levels may increase a person's pain sensitivity, and there's research on how low vitamin D led to higher pain for osteoarthritis patients who suffered a physical injury (called mechanical in study terms) to tissue.

In the Vitamin D prostate cancer research investigators tested vitamin D levels of nearly 700 men in Chicago and found African American men with even moderately low levels had higher risk for being diagnosed.

"The darker the color of the skin, the less effective sunlight is in producing vitamin D in skin," says Dr. Donald Trump, president and CEO of the Roswell Park Cancer Institute, the first cancer center in the nation, who was not involved in the study. "An African-American person is more likely to have lower levels of vitamin D than a European person, because the same amount of sun exposure doesn't generate the same amount of vitamin D for darker skin as it does for lighter skin."

"We know a lot about the fact that in a lab test tube or animal, the active form of vitamin D can moderate, slow, or stop prostate tumor cells and at high doses can even kill them. We don't know yet whether treating people with vitamin D will reduce the chance of getting [cancer]," Trump said. "We don't know for sure that it makes a difference, but I believe it does" Trump said.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.