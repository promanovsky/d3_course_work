Glen Cove is poised to restrict the sale of electronic cigarettes to minors when the City Council votes Tuesday on an amendment to city laws governing tobacco and nicotine.

"We are proposing amendments to our current code regarding the sale and advertising of tobacco products," Mayor Reginald Spinello said in an email.

He proposed the amendment, which would add nicotine products and electronic cigarettes to the city's restrictions on the sale and advertising of tobacco products. It will not include nicotine patches and gum used to eliminate dependence on nicotine.

"The City of Glen Cove has always been a leader in making our youth aware of the dangers of smoking," he said.

The City Council has scheduled a public hearing Tuesday on the amendment before voting on it that night.

Carol Meschkow, of the Tobacco Action Coalition of Long Island, praised the proposed amendment as a "trailblazer" that would help prevent the next generation from smoking. "Smoking is not an acceptable norm, and you're taking the lead by making this statement," Meschkow said at a pre-council meeting last week.

Electronic cigarettes, which heat up liquid nicotine that users inhale, have grown in popularity in recent years. That popularity has set off alarms over the potential health impact of the products that haven't faced the rigorous testing of tobacco.

Sign up for The Classroom newsletter. The pandemic has changed education on Long Island. Find out how. By clicking Sign up, you agree to our privacy policy.

Last month, the U.S. Food and Drug Administration proposed regulating e-cigarettes and other tobacco and nicotine products that it does not regulate.

The Glen Cove amendment would define e-cigarettes and chemicals used in them as "nicotine products" and add them to sections of the city code that make it illegal to sell tobacco to people under 19 years old. The amendment would also add e-cigs to a prohibition against advertising tobacco products within 1,000 feet of a school building, playground, day care or youth center if the advertising is outdoors or indoors if it is placed to be seen from outside.

"It's really a health issue, and this is targeted toward the younger people, our kids," City Councilman Timothy Tenke said in an interview. "Anything we can do to help or prevent them from picking up a bad habit or allowing these companies without any restrictions to target them, we're definitely going to do that."