The soberphone: App for alcoholics can set off an alarm and call for help if they stray too close to a bar



Alcoholics were given free 'sober smartphones' to test the app

Can alert counsellors if they stray near their favourite bar



Included panic button if they were tempted - leading to a call from a friend or counsellor

A smartphone app for recovering alcoholics that sounds an alert when they get too close to their favourite bars has been deemed a success in inital trials.

The sober app studied joins a host of others that serve as electronic shoulder angels, featuring a variety of options for trying to prevent alcoholics and drug addicts from relapsing.

Adults released from in-patient alcoholism treatment centers who got free sober smartphones reported fewer drinking days and more overall abstinence than those who got the usual follow-up support.

Scroll down for video



The app can automatically alert the user if they walk into a bar and offer advice and guidance

HOW IT WORKS The app issues daily messages of support and once a week asks questions designed to help counselors assess the person's struggle with sobriety.

It provides access to online support groups and counselors.

It also tracks users' location using the phone's GPS, and issues an alert if they are nearing a bar they used to frequent or their favorite liquor store.

It also features a 'panic button' that gives a struggling person instant access to distractions, reminders or even a nearby friend who can come give them support.



The results were based on patients' self-reporting on whether they resumed drinking, a potential limitation.



Still, addiction experts say the immediacy of smartphone-based help could make them a useful tool in fighting relapse.



Mark Wiitala, 32, took part in the study and says the app helped save his life.



He said the most helpful feature allowed him to connect to a network of peers who'd gone through the same recovery program.



The app made them immediately accessible for an encouraging text or phone call when he needed an emotional boost.



'It's an absolutely amazing tool,' said Wiitala, of Middlesex County, Mass.



He said he's continued to use it even though the study ended.



The study was published online Wednesday in JAMA Psychiatry.



It involved 271 adults followed for a year after in-patient treatment for alcoholism at one of several U.S. centers in the Midwest and Northeast.



They were randomly assigned to get a sober smartphone app for eight months plus usual follow-up treatment - typically referral to a self-help group - or usual follow-up alone.



The app includes a feature asking periodic questions by text or voicemail about how patients are doing.



If enough answers seem worrisome, the system automatically notifies a counselor who can then offer help.



The A-CHESS app, developed for recovering alcoholics, includes a panic button and sounds an alert when they get too close to taverns. The app is being commercially developed and is not yet available.

The panic button can be programmed to notify peers who are nearest to the patient when the button is pushed.



It also offers links to relaxation techniques to calm the patient while waiting for help.



'We've been told that makes a big difference,' said David Gustafson, the lead author and director of the Center for Health Enhancement Systems Studies at the University of Wisconsin in Madison.



He's among developers of the app, nicknamed A-CHESS after the center.



Gustafson said it is being commercially developed and is not yet available.



Differences in abstinence from drinking between the two groups didn't show up until late in the study.

At eight months, 78 percent of the smartphone users reported no drinking within the previous 30 days, versus 67 percent of the other patients.



At 12 months, those numbers increased slightly in the smartphone group and decreased slightly in the others.



Smartphone patients also had fewer "risky" drinking days per month than the others.

The study average was almost 1 1/2 days for the smartphone group versus almost three days for the others.



Risky drinking was defined as having more than four drinks over two hours for men and more than three drinks for women.



One drink was a 12-ounce bottle of beer, 5-ounce glass of wine, or 1.5-ounce shot of liquor.



The results for smartphone users were comparable to what has been seen with standard follow-up counseling or anti-addiction medication, said Daniel Falk a scientist-administrator at the National Institute on Alcohol Abuse and Alcoholism, which helped pay for the study.



He noted that alcohol abuse affects about 18 million Americans and that only about 25 percent who get treatment are able to remain abstinent for at least a year afterward.



Scientists are looking at new ways to try to improve those statistics.



'There is increasing excitement regarding technology-based tools in substance use treatment, prevention and education,' said Dr. Gail Basch, director of the addiction medicine program at Rush University Medical Center in Chicago.



Basch, who wasn't involved in the study, said proven methods for helping prevent relapse include patient monitoring and support from family and peers.



'A stand-alone mobile app may not be the answer, but one can see how it could fit in nicely,' she said.



'A real-time tool, as well as reminders throughout the day, could be very helpful for a recovering brain.'