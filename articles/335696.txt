Samsung has announced that its Galaxy Tab S tablets will be available in the UK from July 4.

The 10.5-inch and 8.4-inch Android slates will be in stock at various online and high street retailers, including John Lewis, Carphone Warehouse and Dixons Group.



The 8.4-inch model starts at £319 for the WiFi-only version, while the 10.5-inch WiFi edition is priced at £399.

Both devices feature ultra-slim designs, Super AMOLED 2560 x 1440 displays, Exynos 5 octo-core processors, and 3GB of RAM.

"Combining the market's most advanced display technology, Super AMOLED, with a beautifully sleek design, the Galaxy Tab S is a game-changer for the mobile industry and we're proud to be leading the way," said Samsung UK marketing chief Ines van Gennip.



"Offering an unrivalled visual and entertainment experience, unique features and a range of premium content in our thinnest model yet, the Galaxy Tab S is a must-have mobile device and we're delighted to introduce it to our customers."

Those who purchase the Galaxy Tab S will receive a free three-month subscription to Marvel Comics' Marvel Unlimited digital platform as part of an agreement between Samsung and the publisher.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io