Zynga is following through on a promise to bring its FarmVille franchise back to mobile, a crucial first step coming nearly a year after Don Mattrick was named CEO.

The new offering, called “FarmVille 2: Country Escape,” is a mobile app rather than a game played through a browser on a computer.

But the company says it has all the hallmarks of a FarmVille experience: animals, crops and all manner of other items that players have to manage.

They view the farm from a bird’s eye view, watching cows graze, farmhands fish and crops grow.

The game is being released for the iPhone, iPad and Google devices Thursday, and uses a similar model to its desktop game counterparts: Free to download, while Zynga will charge for additional items that help players move through the game quicker.

Click here to continue reading the full report on The Wall Street Journal.