NEW YORK (TheStreet) -- Google (GOOG) has had its price target decreased to $665 from $675, UBS said Thursday. The firm reiterated a "buy" but said the price revision was to "account for our slight downward revisions to revenue, EBITDA and FCF estimates."

Must Read: Warren Buffett's 10 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

----------------

Separately, TheStreet Ratings team rates GOOGLE INC as a Buy with a ratings score of B+. TheStreet Ratings Team has this to say about their recommendation:

"We rate GOOGLE INC (GOOGL) a BUY. This is driven by a number of strengths, which we believe should have a greater impact than any weaknesses, and should give investors a better performance opportunity than most stocks we cover. The company's strengths can be seen in multiple areas, such as its robust revenue growth, largely solid financial position with reasonable debt levels by most measures, reasonable valuation levels, good cash flow from operations and compelling growth in net income. We feel these strengths outweigh the fact that the company has had lackluster performance in the stock itself."

You can view the full analysis from the report here: GOOGL Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.