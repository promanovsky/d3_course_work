While signalling an end to further monetary tightening if consumer inflation moderated along the intended glide path mentioned by the Urjit Patel committee, the Reserve Bank of India (RBI) on Tuesday maintained the repo rate at eight per cent, in line with our expectations. However, it highlighted potential concern on achieving the targeted moderation in the Consumer Price Index (CPI) due to a below-average monsoon in 2014, resetting of various administered prices, etc.

Greater clarity on some risk factors highlighted by RBI such as monsoon dynamics, extent of the electricity rates revision and policy towards regular adjustment in fuel prices after the formation of a new government at the Centre will emerge by July-August 2014. Based on this, Icra does not anticipate monetary easing in the intervening period.

The impact of a below-average monsoon will be partly mitigated by replenished groundwater levels, healthy reservoir levels and substantial buffer stocks of grains. However, the nature of adjustment in food prices after the complete rollout of the National Food Security Act across states remains to be seen. Also, persisting supply-side issues relating to various agricultural commodities are likely to keep food inflation relatively high and determine the direction of monetary policy in the latter part of 2014.

Further, other components of the CPI are also expected to remain sticky in the near term.

In terms of the proposed liquidity measures, RBI's decision to reduce the overnight repo limits with a commensurate increase in the term repo limits will compel banks to improve cash/treasury management operations and aid them in developing a sovereign term money yield curve for short tenures. Additionally, the dynamics of the banking sector could see a change, as and when RBI finalises the framework for on-tap bank licences, as well as differentiated bank licences and mergers in the sector.

Naresh Takkar

MD & CEO, Icra