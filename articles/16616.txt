More Americans are confident about their retirement prospects for the first time in seven years, but even so, more than one-third of workers (36%) have a measly $1,000 saved for their later years, according to a new study by the Employee Benefit Research Institute. (Compare that to the 28% of workers who said they had $1,000 saved in last year's survey, and the picture gets a little more grim.)

As a whole, however, Americans are feeling more confident about retirement, with 18% saying they're "very confident," up from 13% in 2013. But this year’s confidence numbers are still lower than they were before the Great Recession, when one-quarter of Americans were feeling very confident about their golden years.



“We’re definitely moving in the wrong direction,” said Greg Burrows, senior vice president of retirement and investor services with the Principal Financial Group, which co-sponsored the report. “Increasingly, workers realize they need to save a lot more for retirement, and yet their actions aren’t following through.”



At a time when research has shown time and again how ill-prepared most workers are to support themselves through their golden years, the study offers a glimpse into what the "very confident" 18% have that, well, the rest of us don’t.



They know their number. A lot more goes into saving for the future than dollars and cents. You have to have some idea of how much you need to save. And people who take the time to calculate their retirement needs ahead of time are more likely to be on track.



About 44% of workers say they’ve run their numbers through a retirement calculator, helping them to save a whopping 40% more than the rest of us.



“Everyone else is really just guessing,” said Burrows. “Using savings calculators is a really important trigger for improving actions and savings rates. When we look at our own customers who use calculators, they take action and are more likely to make a change.”



You don’t need to hire a pricey financial planner to get your number. EBRI offers a free tool, as well as Bankrate, the AARP, and Kiplinger.



They’ve got money (and they know how to use it). Obviously, saving for the future is easier when there's more to start with. Workers earning more than $75,000 a year were far more likely to report feeling more confident about retirement than those earning less, according to the EBRI. On the other hand, of those workers who say they've saved less than $1,000 for retirement, 68% reported earning $35,000 or less.



When asked, more than half of workers blamed their low savings rates on day-to-day living expenses. But in many cases, daily budgets aren’t static and can be rearranged to free up funds for savings. What some people lack is the time and energy to revamp their entire household budget — especially for a goal that seems far off.



“In order to have a good opportunity to save, you have to develop an understanding and a plan for managing your spending and saving,” Burrows said. “It’s not easy, but it’s doable if people make the effort to understand their spending patterns and have a good command of their dollars and where they’re being spent.”



They have a designated retirement account. Saving is also easier when you've got a vehicle in place to do so. A whopping 90% of households who have a designated retirement account (like an IRA or 401(k)) actually contribute to it, according to EBRI. On the flipside, just 20% of workers who don’t have a retirement savings account say they’ve saved.



Not surprisingly, people who invested in retirement plans during the economic recovery saw the biggest spike in confidence this year, thanks to a much-needed boost from a bull market. Between 2013 and 2014, the rate of plan-holders who said they were confident about retirement jumped from 14% to 24% — twice as high as workers who didn’t have a retirement plan.



They’d love to work through retirement — but they don’t count on it. EBRI found that confident workers are more likely to have a realistic idea of when they’ll retire — and in most cases that means earlier rather than later.



There’s something of a reality gap between when today’s workers think they’ll retire and when they actually do. For example, only 9% of workers say they plan to retire early (before age 60) but nearly four times that number (35%) report actually retiring that early, according to the report. And just 18% of workers say they’ll retire before age 65, but again, far more people found themselves retiring in that age range than expected (32%).



“This difference between workers’ expected retirement age and retirees’ actual age of retirement suggests that a considerable gap exists between workers’ expectations and retirees’ experience,” the report says.



The biggest reason for early retirement: unexpected health issues. Overestimating your retirement age can be just as costly as underestimating it. For example, if you think you’ll work until 75, you might not consider things like long-term health insurance, which could make all the difference for the 70% of Americans expected to need long-term nursing care at some point in their lives.



“When you build a plan for retirement, don’t count on the ability to work in retirement,” Burrows said. “If you do get the opportunity, that’s fantastic. But it’s not something you should build your plan around because the reality is that the majority of workers don’t.”

Story continues

Have a retirement question or story to share? E-mail us here.

Read more:

Retired and broke: Social Security and the struggle to make it last

How Obamacare has changed my life — for better and for worse

How to plan for retirement at the 11th hour