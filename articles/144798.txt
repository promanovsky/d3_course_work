Leidos Holdings, Inc ( LDOS ) could be a stock to avoid from a technical perspective, as the firm is seeing unfavorable trends on the moving average crossover front. Recently, the 50 Day Moving Average for LDOS broke out below the 200 Day Simple Moving Average, suggesting short-term bearishness.

This has already started to take place, as the stock has moved lower by 11.3% in the past four weeks. Plus, the company currently has a Zacks Rank #5 (Strong Sell) meaning that now could definitely be the time to get out of this potentially in-trouble stock.

LEIDOS HOLDINGS (LDOS): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.