NBC's Emmy Award-winning musical-competition series "The Voice" has confirmed Coldplay frontman Chris Martin as the key adviser for the current season. As part of a new format change, Martin will serve as a mentor for the artists on each of the four teams, a "Voice" first.

Coldplay, which formed in 1997, has sold more than 60 million copies of their five No. 1 albums. Coldplay's sixth studio album, "Ghost Stories," is set for a May 19 global release.

The newly created The Battles: Round Two will begin Monday, March 31 and pit the remaining artists from each team-all previous winners and stolen artists-against one another to perform an agreed-upon song. Both artists will sing for their coach's approval in hopes of moving forward to the Playoff Rounds.

Martin advised each of the The Battles: Round Two performers on vocal techniques, stage presence and the importance of tailoring songs to fit their persona and strengths. These artists will continue to try and earn the support of their respective coaches, who will be forced to decide who will move on to the Playoff Rounds. Each coach will have the opportunity to strategically steal one more artist in this round.

The Live shows begin Monday, April 21. The top 12 artists will compete each week and the at-home audience will vote to save their favorite artists. In addition, during each Live results show, the three artists with the lowest number of votes will be eligible for the "Instant Save." These artists will each perform a new song that represents why they should earn the save. Then, America will have the opportunity to save their favorite performer by tweeting out #VoiceSave along with the artist's name. The two singers with the lowest number of votes will be sent home each week until one winner is announced as "The Voice" on May 20th.

"The Voice" is a presentation of Mark Burnett's One Three Inc., Talpa Media USA Inc. and Warner Horizon Television. The series was created by John de Mol, who serves as executive producer along with Burnett, Audrey Morrissey, Stijn Bakkers and Lee Metzger.

For more, visit NBC.com's official show site: http://www.nbc.com/TheVoice. Follow The Voice on Facebook at http://www.facebook.com/NBCTheVoice and on Twitter at @NBCTheVoice and #TheVoice.