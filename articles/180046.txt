Two studies published on Sunday assert that the blood of young mice “reverses aging in old mice, rejuvenating their muscles and brains.” Vampirism and immortality are one step closer to being actually, scientifically linked. It’s like a horror film but way worse, because isn’t real life actually the scariest horror show of all?

The experiments focused on how effective stem cells are over time. When cells die, stem cells produce new ones to replace them. As organisms get older, scientists determine that stem cells do not die off in aging creatures, but the instructions they get sent are what fail.

To test this a decade ago, researchers performed a procedure called parabiosis, stitching two mice—a younger one and an older one—together in order to share blood. The older mice grew new cells at a rate akin to more youthful mice, and the younger mice aged prematurely.

The most recent studies focused on a protein known as GDF11, the levels of which decrease in mice as they age. In one study, the authors hypothesized that the protein would increase blood flow within the brains of mice, allowing new neural tissue to grow. They were proven correct, and the mice, whose senses of smell had deteriorated, regained that sense.

RELATED: Apple Wins Patent Battle With Samsung, But Is Losing the War

In the other study:

When they surgically conjoined old and young mice so that they shared blood, the satellite cells in the old mice exhibited “restored genomic integrity.” Their satellite cell DNA was indistinguishable from the DNA in young mice, the authors wrote.

This is pretty cool, but as a reminder: these experiments were performed on mice. Not humans. Do not consume human blood in the hopes that it will make you younger.

It is very weird that I feel the need to write that but I will do it again, just in case: Do. Not. Consume. Human Blood.

Yet.

This article was originally published at http://www.thewire.com/technology/2014/05/vampires-proven-correct-blood-of-the-youth-is-probably-the-secret-to-living-forever/361688/

Story continues

Read more from The Wire

• Google Just Got Hit with a Major Class Action Lawsuit