Hugh Jackman has said he loves watching the X-Men movies with his children but forgot to warn his daughter about a nude scene in his latest film.

The 45-year-old actor told the New York Daily News, "It is actually really cool to watch it [X-Men: Days of Future Past] with them until you get to the moment where you're naked on screen and you forgot to warn your almost nine-year-old daughter. And she says, 'Dad, why aren't you wearing underwear?'"

Jackman has two children with his wife Deborra-Lee Furness; 8-year-old Ava and 14-year-old Oscar.