Paramount Pictures/YouTube screenshot by Chris Matyszczyk/CNET

I am not sure the world is braying for another Steve Jobs movie.

However, at least if Aaron Sorkin is writing it, there might be hope for wonderful lines and the joy of everyone speaking faster than they're thinking (which, in the case of some actors, is a very good thing).

There seem, however, to have been some creative differences over the attempt to make this movie.

Last week David Fincher, who made a splendid job of directing "The Social Network," was said to be off the project. Which meant that Christian Bale might also not participate.

This would be a tragedy. Bale is so vibrant and versatile that he could play Jesus Christ and Beelzebub. In the same movie.

Leonardo DiCaprio, however, seems to prefer acting so subtle that I always end up thinking I'm watching Leonardo DiCaprio.

It's a touch unsettling, therefore, that he is said to be in line to take over from Bale and play Leonardo DiCupertino.

The Hollywood Reporter offers that Danny Boyle -- he of "Slumdog Millionaire" and the quite brilliant opening ceremony for the London Olympics -- may be the director now chosen, and his preference to play the Apple titan might be the "Titanic" actor.

I'm sure that those with commercial minds will imagine that casting DiCaprio would bring in a broader audience. Moreover, he could probably get the Jobs whine down just fine.

However, I wonder if he would bring quite the nuances necessary to make the role memorable.

Directors have already trusted him to play Howard Hughes, J. Edgar Hoover, Jay Gatsby and, um, Romeo.

Perhaps, therefore, DiCaprio simply has the reality distortion field that secures him the roles he wants.