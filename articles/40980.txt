The man who is in hospital in Saskatchewan has tested negative for Ebola and other hemorrhagic fevers, according to provincial health officials and a World Health Organization spokesman.

On Monday, officials said a man who had recently travelled in West Africa was in critical condition in hospital in Saskatoon.

The man's co-workers have confirmed to CBC News that the patient is Rod Ogilvie, a geological engineer. He works for a geological services company based in Saskatoon.

Previously, health officials had noted that the patient had been working in Liberia and fell ill after he returned to Canada. He was initially examined for a suspected case of viral hemorrhagic fever, or VMH.

Laboratory tests, however, suggest the patient may be suffering from some other condition.

"The initial [VMH] diagnosis was negative," Dr. Denise Werker, Saskatchewan chief medical health officer, said Tuesday. "They are actually repeating another smear. There is additional testing that will be going on at the reference centre for parasitic diseases."

It is possible the patient has malaria, Werker added. That possibility was also referenced by officials from the World Health Organization on Tuesday.

"Unfortunately, we do not yet have a diagnosis for this man, so laboratory testing is ongoing," Werker said. "He remains in critical condition and under isolation."

Gregory Hartl, WHO's head of public relations, confirmed that Ebola, Marburg, Lassa and two other diseases have been ruled out.

The same findings were being reported by the Public Health Agency of Canada, based on lab tests done in a Winnipeg lab.

Officials repeated their view that the risk to Canadians is very low.