Two passenger planes narrowly avoided a mid-air collision at Newark Liberty airport in New Jersey last month, according to a preliminary report from the National Transportation Safety Board.

The two planes came within 400ft vertically and 150ft laterally of each other at 3pm on 24 April, when United Airlines flight 1243, a Boeing 737 arriving from San Francisco, passed over United Express flight 4100 operated by ExpressJet, an Embraer ERJ-145 plane departing to Memphis, Tennessee. The flight numbers and passenger data are not included in the NTSB report but have been cited by CNN.

Neither plane was damaged during the incident.

The report shows that when the Memphis flight was cleared for takeoff, the incoming Boeing 737 was three miles away. But when the Embraer began its takeoff on runway 4R, the San Francisco flight was only a mile from landing on intersecting runway 29.

Realising that the planes were too close, the air traffic controller warned the pilots, instructing the 737 from San Francisco to "go around" – that is, to abort its landing – and for the Embraer to maintain visual contact with it.

About 10 seconds after the 737 was ordered to "go around", a pilot on the Embraer jet told air traffic controllers that the flight crew had "kept the nose down". The pilot said the 737 was "real close", according to an archive recording.

Contacted moments later by air traffic control to confirm takeoff, the Memphis pilot repeated that the 737 was, "real close". The Boeing 737 passed over the ERJ-145 at the intersection of runways 29 and 4R, the NTSB report says.

The report does not say whether the 737 executed the order to go around. It is also unclear why the Embraer did not take off immediately after being cleared to do so.

Contacted by the Guardian, the NTSB declined say whether the 737 carried out the order to abort its landing. Spokesman Eric Weiss said via email that the question "will be answered during the course of the investigation". He added: "This is only the preliminary report."

United Airlines spokesman Christen David said in a statement: "We are working with the NTSB in its review of the incident.”

The Boeing 737 from San Francisco was carrying 160 passengers and six crew members, according to a United Airlines spokesperson. The ERJ-145 to Memphis was carrying 47 passengers and three crew members.