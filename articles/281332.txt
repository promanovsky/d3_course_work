As world turns their attention to Brazil and the fierce field action to come over the next couple of weeks, the 2014 World Cup's first kick was made by a person with paralysis using cutting-edge bionic technology.

The ceremonial first kick took place Thursday afternoon with the assistance of a high-tech exoskeleton, which is controlled by electrodes placed on the patient's head.

When the patient thinks about walking, the brain signals are transmitted to a computer which then activates the exoskeleton to move accordingly.

"It won't be herky-jerky," Alan Rudolph, Ph.D., the vice president of research at Colorado State University and managing director of the project, told Popular Science. "The integration of the hardware and human signals will be significant, with truly elegant fine motor control."

This has all been made possible by the Walk Again Project, a nonprofit international collaboration of researchers that focuses on using technology to help people with paraylsis walk again.

It's led by neuroscientist Dr. Miguel A.L. Nicolelis, who told CBS News in January that the World Cup is the perfect place to show the world this latest innovation.

"Football is a very big deal. The World Cup is the world's largest sports competition, the ultimate sharing opportunity," Nicolelis said. "We proposed to the government that instead of a regular musical or typical opening ceremony that has been done in the past, we could surprise the world by doing a scientific demonstration instead."

The project is supported by research from several institutions including Duke University Center for Neuroengineering and the Technical University of Munich, along with other prestigious universities in the U.S., Europe and Brazil.

"Sports can be a huge avenue to reach out to people that would never actually pay attention to science news," Nicolelis said. "I always wanted to show kids in Brazil how important science can be for society."