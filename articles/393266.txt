Online real estate market Zillow Inc. (NASDAQ:Z) announced Monday it has entered a “definitive agreement” to buy its competitor Trulia (NASDAQ:TRLA) for $3.5 billion in stock, making the prospective company the largest real estate website in the U.S.

Both brands will remain post-merger. Trulia stockholders will receive 0.444 shares of Zillow stock per share in the deal, which figures out to a 25 percent premium on Trulia’s closing price Friday, or $70.53 per share for Trulia’s $56.35. Share prices for Zillow and Trulia shot up 15 percent and 32 percent, respectively, when news surfaced of the prospective merger last week.

Trulia’s CEO Pete Flint will stay on to run Trulia and will join the board of directors. He will report to Zillow CEO Spencer Rascoff. Both boards have agreed on the deal and it will now go to the shareholders of each company.

Zillow and Trulia were founded in 2005 and their business models are nearly identical. Both make most of their money by selling advertising to real estate professionals who hope to get some of the tens of millions of eyes that visit each site every month. They have been each other's biggest competitors in a rivalry not unlike Grubhub and Seamless prior to their merger last year. As Bloomberg's Joshua Topolsky jests, the merger might mean users can finally choose one of the two:

Finally I can delete one of these apps. Zillow to Acquire Trulia for $3.5 Billion in Stock http://t.co/TFuDovANWL

— Joshua Topolsky (@joshuatopolsky) July 28, 2014

Each site reported record unique traffic in June; Zillow brought in 83 million uniques and Trulia reported 54 million. Zillow is the 37 th most popular website in the U.S., according to Alexa, while Trulia ranks 91st.

Zillow's stock has dropped just under $10 per share on Monday from Friday's closing price, while Trulia's rose from $54.35 to $64 before dropping once again to around $60 per share.

Business Insider's real estate blogger Jim Klinge noted prior to the merger that it would solidify Zillow-Trulia as the go-to place for both realtors and homebuyers. He says competitors will likely fall by the wayside and corporate real-estate firms will race to work directly with Zillow-Trulia.

Since 2012, Zillow has acquired two real estate search sites and a real estate analytical software firm. Rascoff expects the deal to close sometime in 2015.