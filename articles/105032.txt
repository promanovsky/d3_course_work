It's the silent enemy in our retirement accounts: High fees.

And now a new study finds that the typical 401(k) fees — adding up to a modest-sounding 1 percent a year — would erase $70,000 from an average worker's account over a four-decade career compared with lower-cost options. To compensate for the higher fees, someone would have to work an extra three years before retiring.

The study comes from the Center for American Progress, a liberal think tank. Its analysis, backed by industry and government data, suggests that U.S. workers, already struggling to save enough for retirement, are being further held back by fund costs.

Read MoreHow media hype can sabotage your retirement



"The corrosive effect of high fees in many of these retirement accounts forces many Americans to work years longer than necessary or than planned," the report, being released Friday, concludes.

Most savers have only a vague idea how much they're paying in 401(k) fees or what alternatives exist, though the information is provided in often dense and complex fund statements. High fees seldom lead to high returns. And critics say they hurt ordinary investors — much more so than, say, Wall Street's high-speed trading systems, which benefit pros and have increasingly drawn the eye of regulators.

Read MoreSavings showdown: Retirement for you vs. college for kids



Consider what would happen to a 25-year-old worker, earning the U.S. median income of $30,500, who puts 5 percent of his or her pay in a 401(k) account and whose employer chips in another 5 percent:

— If the plan charged 0.25 percent in annual fees, a widely available low-cost option, and the investment return averaged 6.8 percent a year, the account would equal $476,745 when the worker turned 67 (the age he or she could retire with full Social Security benefits).

— If the plan charged the typical 1 percent, the account would reach only $405,454 — a $71,000 shortfall.

— If the plan charged 1.3 percent — common for 401 (k) plans at small companies — the account would reach $380,649, a $96,000 shortfall. The worker would have to work four more years to make up the gap. (The analysis assumes the worker's pay rises 3.6 percent a year.)

The higher fees often accompany funds that try to beat market indexes by actively buying and selling securities. Index funds, which track benchmarks such as the Standard & Poor's 500, don't require active management and typically charge lower fees.

Read More

With stocks having hit record highs before being clobbered in recent days, many investors have been on edge over the market's ups and downs. But experts say timing the market is nearly impossible. By contrast, investors can increase their returns by limiting their funds' fees.

Most stock funds will match the performance of the entire market over time, so those with the lowest management costs will generate better returns, said Russel Kinnel, director of research for Morningstar.

"Fees are a crucial determinant of how well you do," Kinnel said.

Read MoreThree ways to curb your taxes near retirement



The difference in costs can be dramatic.

Each fund discloses its "expense ratio." This is the cost of operating the fund as a percentage of its assets. It includes things like record-keeping and legal expenses.

For one of its stock index funds, Vanguard lists an expense ratio of 0.05 percent. State Farm lists it at 0.76 percent for a similar fund. The ratio jumps to 1.73 percent for a Nasdaq-based investment managed by ProFunds.

"ProFunds are not typical index mutual funds but are designed for tactical investors who frequently purchase and redeem shares," said ProFunds spokesman Tucker Hewes. "The higher-than-normal expense ratios of these non-typical funds reflect the additional cost and efforts necessary to manage and operate them."

Read MoreWomen face retirement-saving challenges



Average fees also tend to vary based on the size of an employer's 401(k) plan. The total management costs for individual companies with plans with more than $1 billion in assets has averaged 0.35 percent a year, according to BrightScope, a firm that rates retirement plans. By contrast, corporate plans with less than $50 million in assets have total fees approaching 1 percent.

Higher management costs do far more to erode a typical American's long-term savings than does the high-speed trading highlighted in Michael Lewis' new book, "Flash Boys." Kinnel said computerized trades operating in milliseconds might cost a mutual fund 0.01 percent during the course of a year, a microscopic difference compared with yearly fees.

"Any effort to shine more light (on fees) and illustrating that impact is huge," Kinnel said. "Where we've fallen down most is not providing greater guidance for investors in selecting funds."

The Investment Company Institute, a trade group, said 401(k) fees for stock funds averaged 0.63 percent in 2012 (lower than the 1 percent average figure the Center for American Progress uses), down from 0.83 percent a decade earlier. The costs fell as more investors shifted into lower-cost index funds. They've also declined because funds that manage increasing sums of money have benefited from economies of scale.

Read More France has the longest retirement age: OECD



"Information that helps people make decisions is useful," said Sean Collins, the institute's senior director of industry and financial analysis. "Generally, people pay attention to cost. That shows up as investors tend to choose — including in 401k funds — investments that are in lower than average cost funds."

But many savers ignore fees.

In a 2009 experiment, researchers at Yale and Harvard found that even well-educated savers "overwhelmingly fail to minimize fees. Instead, they placed heavy weight on irrelevant attributes such as funds' (historical) annualized returns."

The Labor Department announced plans last month to update a 2012 rule for companies to disclose the fees charged to their 401(k) plans. Fee disclosures resulting from the 2012 rule proved tedious and confusing, said Phyllis Borzi, assistant secretary for the Labor Department's Employee Benefits Security Administration.

"Some are filled with legalese, some have information that's split between multiple documents," Borzi said.