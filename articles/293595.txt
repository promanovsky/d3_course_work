Caffeine affects boys and girls differently, a new research shows.

For the study, Jennifer Temple, associate professor in the Department of Exercise and Nutrition Sciences, University at Buffalo School of Public Health and Health Professions, analyzed the heart rate and blood pressure of pre and post-pubertal boys and girls. The researchers administered placebo and two doses of caffeine in pre-pubertal and post-pubertal boys and girls.

The team found that after puberty, boys and girls reported different heart rate and blood pressure changes upon consumption of caffeine. Girls experienced certain differences in caffeine effect during their menstrual cycles.

Previous studies have shown that caffeine increases blood pressure and decreases heart rate in children, teens and adults, including pre-adolescent boys and girls.

The aim of the new study was to find out whether gender differences in cardiovascular responses to caffeine happen after puberty and if those responses differ across phases of the menstrual cycle in girls.

"We found an interaction between gender and caffeine dose, with boys having a greater response to caffeine than girls, as well as interactions between pubertal phase, gender and caffeine dose, with gender differences present in post-pubertal, but not in pre-pubertal, participants," Temple said in a press release.

"Finally we found differences in responses to caffeine across the menstrual cycle in post-pubertal girls, with decreases in heart rate that were greater in the mid-luteal phase and blood pressure increases that were greater in the mid-follicular phase of the menstrual cycle," Temple added. "In this study, we were looking exclusively into the physical results of caffeine ingestion," she added.

The team explained that different phases of the menstrual cycle are marked by changing levels of hormones. They are known as the follicular phase that begins on the first day of menstruation and ends with ovulation, and the luteal phase, which follows ovulation and is marked by significantly higher levels of progesterone than in the previous phase.

The study team said more research is required to understand to what extent gender differences are impacted by physiological factors such as steroid hormone levels or by differences in patterns of caffeine use, caffeine use by peers or more autonomy and control over beverage purchases, Temple said.

The study was published in the journal 'Pediatrics.'

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.