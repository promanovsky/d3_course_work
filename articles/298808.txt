A South Carolina teen was reportedly forced to remove makeup before being allowed to get his photograph taken for his driver’s license.

Chase Culpepper, of Anderson, was informed by authorities he couldn’t misrepresent his identity, although he claims he was dressed the same as he does for work and school. He now wants the state’s Department of Motor Vehicles to allow him to re-take the photograph wearing his typical makeup, FoxCarolina.com reports.

“This is how I am every day,” Culpepper said. “And if a police officer wanted to recognize how I am, then he would want to see who I am in my picture as well.”

Culpepper received his license in March, but felt mistreated by DMV officials. He then visited the Transgender Legal Defense and Education Fund, which claims Culpepper’s rights have been violated.

DMV officials, however, stand by the agency’s photograph policy, which states that an applicant will not be allowed to take a photograph if he or she is “purposefully altering” their appearance so that it would misrepresent their identity.