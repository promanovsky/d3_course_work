In a recent segment on immigration reform, Fox News used the logo from BioShock Infinite, a first person shooter with a heavy dose of political farce that was apparently lost on the news show (h/t Zorine Te at GameSpot). Irrational Games co-founder and creative director Ken Levine tweeted that “The Fox thing is just a little ironic and funny. It’s fair use, for sure,” so there won’t be any legal spat over the logo, but anyone who has played the game knows that Fox inadvertently identified itself with scathing satire.

BioShock series combines politics and dark humor in a FPS

If you haven’t played any of the BioShock games, they’re first-person shooters that blend science fiction tropes with old-fashioned aesthetics. The original game was set in an underwater city that was supposed to be a laissez-faire society gone wrong (so you won’t mistake the games’ politics). Everyone has either become a monster or a victim, and the main character is just trying to escape in a game that’s equal parts horror and absurd.

BioShock Infinite switches venue, to a society in the clouds ruled by a religious despot. The main character is a former Pinkerton detective hired to investigate the city and track down a damsel in distress as standard video game tropes demand. But the city was built by secessionist racists who revere the Founding Fathers as literal religious figures, and live in fear that their black underclass will rise up in revolution. The game is intense (in one early scene the main character decides whether or not to participate in the public stoning of an interracial couple), and breaking down the religious dictatorship is a big part of the story.

BioShock Infinite probably isn’t the association Fox had in mind

So when former (and possibly future) presidential candidate Rick Perry stands in front of the symbol of a violent, racist (and fictional) society, and talks about securing the border, it’s kind of surreal.

None of that is to say that immigration isn’t a serious issue, and we’re absolutely not accusing Rick Perry of anything but a sincere desire to solve the problem. But if you’re trying to choose images that reinforce your position on tougher immigration, slapping up an homage to BioShock Infinite is about the worst decision you could make.