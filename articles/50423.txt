U.S. economic growth was a bit faster than previously estimated in the fourth quarter of 2013, displaying underlying strength that could bolster views that the slowdown in activity early in the year would be temporary.

Gross domestic product in the fourth quarter expanded at a 2.6 percent annual rate, the U.S. Commerce Department said, up from the 2.4 percent pace it reported last month.

Further, international investors should be encouraged by the U.S. growth rate. While it's hardly robust or indicative of a "Roaring '90s" expansion, the 2.6 percent growth rate eliminates the concern that the U.S. economy would fall into a recession. And absent a U.S. recession, and at least continued 7 percent GDP growth in China, the global economy will continue to expand in 2014 -- and that bodes well for international investors.

What's more, the U.S. economic picture was also brightened by other data on Thursday showing new applications for unemployment benefits falling last week to their lowest level in nearly four months, Reuters reported.

The revision, which was broadly in line with economists' expectations, reflected a stronger pace of consumer spending than previously estimated.

Although the revised pace of expansion was still significantly slower than the 4.1 percent rate logged in the July-September quarter, the composition of growth in the fourth quarter suggested underlying strength in the economy.

Consumer spending, which accounts for more than two-thirds of U.S. economic activity, was raised sharply higher, and the pace of restocking by businesses was not as robust as previously estimated.

In addition, business spending on equipment was a bit stronger than previously estimated, and the decline in government outlays was a little less pronounced, Reuters reported.

In a separate report, the Labor Department said initial claims for state unemployment benefits dropped 10,000 to a seasonally adjusted 311,000, the lowest level since November.

(Note: Reuters reports were included in this story.)