When Can You Get The COVID Vaccine?The COVID-19 vaccine will be available to millions of Americans this month, but who qualifies depends on certain factors.

Doctor Explains Why You Should Avoid Thanksgiving, Christmas Gatherings: 'Not Worth The Risk'Dr. Mallika Marshall warns against large family gatherings this year due to the risk of spreading COVID-19.

The Forgotten Fight Against COVID-19: Are You Protecting Your Phone?In the last few months, COVID-19 has drastically changed our norms. While everyone is social distancing, masking up and wiping down every door knob in a three-mile-radius, what about the germ magnets we all keep glued to our finger tips; our cell phones.

How Can I Safely BBQ With Friends, Family This Summer? Answers To Common Coronavirus Questions From Dr. Dave HnidaDr. Dave Hnida answers common questions about staying safe from coronavirus this summer while still enjoying the warm weather and company of friends.

How Safe Is Going On Vacation? Summer Coronavirus Questions Answered By Dr. Max GomezWith coronavirus limitations easing and summer in full swing, Dr. Max Gomez answers questions about safety while vacationing and enjoying outdoor activities.

Does Heat, Sun Kill Coronavirus? Frequently Asked Summertime Questions About COVID-19 Answered By Dr. Mallika MarshallDr. Mallika Marshall, a practicing physician, answers common questions about coronavirus safety this summer at the pool, the beach and out and about.