Pfizer, the US Viagra giant, has scrapped its takeover offer for Britain’s AstraZeneca, bringing an end to one of the most politically charged corporate deals in decades.

The company had until 5pm today to make up its mind on whether to formally walk away under UK takeover rules.

It now has to wait six months before launching another bid.

In a statement this afternoon, it said: "Following the AstraZeneca board's rejection of the proposal, Pfizer announces that it does not intend to make an offer for AstraZeneca."

Leif Johansson, chairman of AstraZeneca, said: "We note Pfizer's confirmation that it no longer intends to make an offer for AstraZeneca. We welcome the opportunity to continue building on the momentum we have already demonstrated as an independent company."

He added: "The Board is grateful to Pascal, his management team and to all our employees for their dedication and focus over a period of uncertainty."

Pfizer had taken pains to promise that 20 per cent of research and development jobs would remain in the UK, but its track record on previous acquisitions of slashing research budgets and closing research centres still worried the science community.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Memories were still vivid of Pfizer’s closure of its big development site in Kent, where it invented Viagra.

The ghost of Kraft’s takeover of Cadbury also loomed large. That deal saw the American processed cheese giant renege on a promise not to shut a Cadbury factory soon after the takeover had gone through.

AstraZeneca’s board had flatly refused to discuss a deal with the company until it offered more than £55 a share and made clearer proposals on how the deal would avoid disrupting its potentially lucrative programme of inventing new medicines.

At the heart of the controversy was Pfizer’s admission that a key reason for the deal was for it to save tax by moving its domicile, but not its headquarters, to low-tax UK.

Some shareholders were angry that it was this, rather than the long term health of the company, that was driving Pfizer’s management. Given that a large proportion of the purchase price was to be made in shares of the combined company, investors were more concerned about the future than they would have been if it was a simple cash deal.

However, other big Pfizer shareholders including its biggest investor, BlackRock, were irritated that AstraZeneca had not engaged in talks with Pfizer.

Chuka Umunna MP, Labour's Shadow Business Secretary, said: "It is welcome that Pfizer has kept to its promise not to mount a hostile takeover of AstraZeneca, Britain’s second largest pharmaceutical company, following the rejection of its advances by the AstraZeneca Board.

“We were clear from the start that we would judge the proposed deal on whether it was best for British science, jobs and industry. We were not confident this was the case which was why we said the Government should subject the deal to a public interest test to determine whether it would have a material adverse impact on the UK’s science and R&D base and, if so, we argued it should be blocked.”

The Conservatives have been keen to make it clear that this was an issue for the shareholders and company boards to decide upon rather than politicians. But they too were forced into making a show of demanding strong assurances on UK jobs.

Pfizer was also facing a growing backlash in the US, where a political head of steam appears to be building to block US companies from buying foreign companies in order to move their tax domicile to low tax havens like the UK.

At the weekend, it emerged that a US company had made a similar proposal to InterContinental Hotels Group – the British based biggest hotel operator in the world.