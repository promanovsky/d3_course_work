Staff of the Christian charity Samaritan's Purse putting on protective gear in the ELWA hospital in the Liberian capital Monrovia. (credit: ZOOM DOSSO/AFP/Getty Images)

DENVER (CBS4)– Some groups in Colorado are changing their plans to travel to help others after an Ebola outbreak has claimed the lives of more than 700 people.

The Centers for Disease Control and Prevention has issued a travel warning to Americans not to travel to three countries including Guinea, Liberia and Sierra Leone all in West Africa.

Denver’s “Youth With A Mission” were considering sending a group to Liberia but that option has been taken off the table.

Another local group “Seed” canceled their plans to send three teams to Sierra Leone. They said it was a tough decision but because of the virus there was no other option.

Pastor Tony Weedor and his wife, both Liberian natives, have been traveling to and from West Africa for years. Weedor returned from his most recent trip to Liberia in April, around the same time the Ebola virus started spreading.

“As soon as I got here I went to my primary doctor. Checked. Nothing,” said Weedor.

While he was there he met and worked alongside Dr. Kent Brantly and missionary Nancy Writebol. Writebol grew up in Colorado.

Shortly after returning to his home in Highlands Ranch, Weedor learned both of them had contracted the deadly disease.

“At first I was shocked and then angry and then frustrated that these are people who have no reason to be in Liberia but have gone there to help us and in helping us they are now sick,” said Weedor.

As a result, organizations in the U.S. are pulling their volunteers out of West African countries that have been impacted by the Ebola outbreak. The Peace Corps has already removed more than 300 people as a precaution and the CDC issued a warning to all Americans not to travel to the affected countries.

The Weedor’s family are among those still in Liberia. With the surrounding borders closed they are not allowed to leave. Hospitals have closed and supplies used to fight off infection are running low.

“It’s painful to be here and you can’t help,” said Weedor.

Weedor’s family has told him they are running out of gloves and hand sanitizer, things they need to try to protect themselves against the Ebola virus. They are trying to raise money to send to Samaritan’s Purse, so they can get help to those who need it.

LINK: Samaritan’s Purse