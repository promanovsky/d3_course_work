The Federal Reserve has sharply cut its forecast for U.S. growth this year, reflecting a shrinking economy last quarter caused mostly by harsh weather.

At the same time, the Fed has barely increased its estimate of inflation despite signs that consumer price increases are picking up. The Fed's benign inflation outlook suggests it doesn't feel rising pressure to raise short-term interest rates.

The Fed expects growth to be just 2.1 percent to 2.3 percent this year, down from 2.8 percent to 3 percent in its last projections in March. It thinks inflation will be a slight 1.5 percent to 1.7 percent by year's end, near its earlier estimate.

Still, more Fed members than in March expect its short-term rate to be 1 percent or higher by the end of 2015.

The Fed also sid it will further slow the pace of its bond purchases because a strengthening U.S. job market needs less support. But it's offering no clear signal about when it will start raising its benchmark short-term rate.

The Fed's decision came in a statement it released Wednesday after a two-day policy meeting. Most economists think a rate increase is at least a year away despite signs of rising inflation.

Its statement was nearly identical to the one the Fed issued after its last meeting in April. It reiterated its plan to keep short-term rates low "for a considerable time" after it ends its bond purchases, which have been intended to keep long-term loan rates low.

The reaction in financial markets was muted. Stocks edged slightly higher, and bond yields remained about where they were before the Fed made its announcement.

The central bank's decision to further pare its bond buying means its monthly purchases of long-term bonds will be reduced from $45 billion to $35 billion starting in July. It marked the fifth cut in the purchases since December as the Fed slows the support it's providing the economy.

The bond buying is expected to end altogether by fall.

The Fed's statement was approved on an 11-0 vote, with support from the Fed's three newest members: Vice Chairman Stanley Fischer, board member Lael Brainard and Loretta Mester, the new president of the Fed's regional bank in Cleveland.

The slight changes in the Fed's statement pointed to signs of a strengthening economy now that a brutal winter has passed. The statement said economic activity had "rebounded," with gains in the job market, household spending and business investment.

The statement signaled no concern about the recent acceleration in inflation. It repeated language on inflation in which the Fed said it was still concerned that inflation remained below its 2 percent target.