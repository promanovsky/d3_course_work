LinkedIn now boasts of over 300 million members, of which 100 million users come from the U.S. and the remaining from around the world. The announcement came on April 18 in a blog post by Deep Nishar, LinkedIn senior VP of products and user experience.

The new data on membership has seen as a huge milestone for the company who is already in the business for around 10 years.

"While this is an exciting moment, we still have a long way to go to realize our vision of creating economic opportunity for every one of the 3.3 billion people in the global workforce," Nishar says in the blog post.

He emphasizes that the company is working on providing personalized experiences based on members' identity, knowledge and network, seen to help all members of the LinkedIn community to reach their professional ambitions. One shift in the company's strategy was already fulfilled through its content products.

Likewise, he says mobile is critical in the business and the company intends to hit its so-called mobile moment, wherein the mobile will soon account for over 50 percent of its global traffic. He discloses that many of its members in locations such as the United Kingdom, Costa Rica, United Arab Emirates, Sweden, Singapore and Malaysia access the site more on mobile devices instead of their desktops. Daily statistics from the site shows that mobile use of the network accounts for 44,000 job applications in more than 200 countries, 1.45 million job views and 15 million profile views on the average.

To anticipate its mobile moment, the company began to develop multiple mobile apps for LinkedIn two years ago to satisfy the varied needs of various members. For instance, is its SlideShare app that serves as a mobile portfolio for members. More partnerships are said to be unveiled in the coming months with the likes of Nokia, Apple and Samsung.

"We already have a strong presence around the world and will continue to invest in building out the experiences we offer to our members in key countries," Nishkar says.

He adds that in its goal to bring further connection to over 140 million Chinese professionals in and out of China workforce, LinkedIn widened its presence in said country by launching the Simplified Chinese site in a beta version. He also discloses that with the first Economic Graph in the world; expect better connections among members, jobs, companies, skills, educational institutions and professional knowledge of world economy.

Research says the huge announcement of such milestone was released ahead of the statement on its first-quarter earnings on May 1, 2014. Analysts' expectation sees a 23 percent drop in its per-share income, but a 44 percent in revenue share. The company was said to be in trading at $175.42 per share as of 12.46 pm PT on Friday, which is up by $3.60 per share of 2.10 percent.

Other reports say LinkedIn is still way behind the 1.2 billion of monthly active users of Facebook; regardless, 300 million is a notable growth.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.