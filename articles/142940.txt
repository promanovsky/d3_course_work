The Ford EcoSport at a media presentation at the 2013 Bangkok motor show. — Reuters file pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SHANGHAI, April 20 — Global automakers are scrambling to meet the demands of China’s young urban professionals, who want a car that makes them stand out, yet don’t always have the money to splurge on a top-end model.

After nearly two decades of frenzied growth driven mainly by the very wealthy, China’s auto market is maturing, yet remains ferociously competitive. with manufacturers having to react quickly to shifting consumer trends.

Today’s emerging buyer has more modest financial means, yet aspires to own a car that’s “different”.

People such as Zhou Wenxi, a 32-year-old owner of a Shanghai cram-school, and Guo Yetao, 23, a software salesman from Hangzhou, are fuelling two trends: hot demand for smaller crossover sport utility vehicles such as Ford Motor Co’s EcoSport; and more interest in affordable, entry-level luxury cars such as the Audi A3.

The grunt from the front of the Lincoln MKX SUV concept car at Auto China 2014 in Beijing April 20, 2014. — Reuters pic The Lincoln MKX SUV concept car on show at Auto China 2014 in Beijing, April 20, 2014. Ford Motor Co’s premium brand may be late to China’s luxury boom, but its top executives say the upscale car market still has plenty of steam left for growth. — Reuters pic A visitor checks out a Geely GX7 at Auto China 2014 in Beijing April 20, 2014. — Reuters pic A Geely EC Cross concept car on show at Auto China 2014 in Beijing April 20, 2014. Geely Automobile Holdings Ltd is the parent company of Swedish carmaker Volvo. — Reuters pic Models pose with a Volvo concept car at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Visitors look at a Volkswagen New Midsize Coupe (NMC) concept car at Auto China 2014 in Beijing April 20, 2014. — Reuters pic A Volkswagen Lavida BlueMotion car on show at Auto China 2014 in Beijing April 20, 2014. — Reuters pic One for the album: A visitor with the Audi R8 Spyder at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Visitors giving the Audi S8 the once-over at Auto China 2014 in Beijing April 20, 2014. — Reuters pic The Volkswagen XL1 hybrid car as it looks from the side at Auto China 2014 in Beijing April 20, 2014. — Reuters pic The Volkswagen XL1 hybrid car on show at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Putting the shine to the new Toyota Corolla at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Staff getting a new Toyota Corolla ship-shape at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Daimler AG chief executive Dieter Zetsche speaks at the world premiere of the Mercedes Benz S63 AMG Coupe at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Daimler AG chief executive Dieter Zetsche speaks to the media at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Visitors crowd the Mercedes-Benz exhibition booth on the first day of Auto China 2014 in Beijing April 20, 2014. — Reuters pic The BMW Vision Future Luxury concept car on show at its world premiere at Auto China 2014 in Beijing April 20, 2014. — Reuters pic The Infiniti Q50 Eau Rouge concept car on show at Auto China 2014 in Beijing April 20, 2014. — Reuters pic The Infiniti Q30 concept car on show at Auto China 2014 in Beijing April 20, 2014. — Reuters pic Previous Next

There is a potential “seismic shift” in the influence these young urban professionals will have on China’s auto market, says Yale Zhang, head of Automotive Foresight, a Shanghai-based consultant.

“There are different tiers of young urban professionals. Some are rich and a bit older, others are more normal,” he said. “But they tend to be similar in two traits: they want to be different, and don’t necessarily have an endless amount of money to throw around.”

Guo, the Hangzhou salesman, recently took out a loan to buy an EcoSport crossover for 115,800 yuan (RM60,738), while Liu Yao, a 22-year-old office worker in Huaibei in Anhui province, borrowed from his parents to buy a Haval M4 mini-crossover from Great Wall Motor Co Ltd for 60,000 yuan.

Among the less well-off young urbanites, the EcoSport, at 94,800 yuan, has been a volume seller for Ford since its launch early last year. Sales last year were 59,680, and reached 17,392 in January-March of this year.

At this week’s Beijing auto show, car makers including PSA Peugeot Citroen, Hyundai Motor Co and local firm Haima Automobile Group will unveil new mini-SUV models. Chevrolet launched its Trax, a tiny crossover, in China earlier this month.

According to IHS, there were just five subcompact SUV models on sale in China in 2010. By 2020, there will be almost two dozen, driving up sales of this segment to more than 880,000 from 345,650 last year.

Affordable luxury

Other young professionals, maybe small-business owners or higher earners, are setting their sights higher.

The new A3 TDI as seen at the New York International Auto Show April 16, 2014: Perfect; not all that ostentatious. — Reuters pic

Zhou, the cram-school owner, paid about 280,000 yuan for a red Audi A3 compact sedan. “Audis aren’t flashy, and the A3 is entry-level, so the price isn’t outrageous,” she said.

Manufacturers will show off several more affordable, entry-level luxury cars at the Beijing show.

A crucial shift for them will be to produce more of these models in China, which would cut the price as they would not be subject to hefty import duty and other taxes.

Volume sales in this premium compact car sector have trebled to more than 52,000 in the past four years, according to IHS data, so remain, for now, a tiny niche in China’s total passenger car market of close to 16.8 million.

Some global luxury brands, such as Nissan Motor Co Ltd’s Infiniti, Honda Motor Co Ltd’s Acura and General Motors Co’s Cadillac, have been late to enter China, and now see its potential as a market for premium cars.

Infiniti is looking to improve price competitiveness with what global president Johan de Nysschen calls a “very aggressive... and rapid localisation” strategy — making locally as many as 80 per cent of the 100,000 cars Infiniti aims to sell in China in the medium term. The brand also aims to capture fast-emerging, young buyers with affordable entry-level luxury cars.

“You’ve got a big (wave) of people coming in now. They have disposable income; they are very much attuned to the premium brands,” de Nysschen told Reuters yesterday. “For us, and for other car companies, this presents a big opportunity because it’s a market force.”

Nissan has no compact car in its product line-up today, but Infiniti plans to bring the Q30 compact crossover — on display in Beijing as a concept vehicle — to China eventually after the model goes on sale in Europe next year.

For now, Infiniti is set to start making a couple of mid-size cars — the just-launched Q50 sedan and the QX50 crossover — at an existing Nissan plant in Hubei province later this year.

The Q50 now starts at a hefty 389,800 yuan in China because it’s imported. De Nysschen said the China-made model should be significantly cheaper once it’s made locally, and is not subject to China’s import duties and taxes. The Q50 starts at US$37,150 (RM120,420) in the United States.

Infiniti sold just 17,108 vehicles in China last year, though that was up 54 per cent from 2012. It plans to increase the number of its China retail outlets to 80 this year from 67 at present.

Ford last week said it would open its first eight Lincoln stores in several cities from October, and would have 60 stores by 2016.

Five years ago, there were fewer than a dozen luxury car models sold in China under five premium brands. Today, that has mushroomed to more than 90 models offered by 25 brands, says market research firm TNS.

To protect their market edge — and counter slowing economic growth and a government crackdown on excess — established luxury brands such as Audi, Mercedes-Benz and BMW are expanding their line-ups into entry-level models and producing more models in China to make them more affordable.

Audi, for example, priced its A3 hatchback from 199,000 yuan last month after it started making the car in Foshan, near Guangzhou. — Reuters