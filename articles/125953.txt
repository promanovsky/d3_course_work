Codenomicon/CNET

The Heartbleed bug has caused widespread anxiety, sent engineers scrambling into patch-mode, and likely prompted millions of users to re-invent their passwords, but so far there have been no accounts of attacks leveraging the bug... until now.

In the first known report of an attack using the security flaw, Canadian police have arrested a man who allegedly used Heartbleed to steal user data from the government's tax Web site, according to Reuters.

Authorities discovered earlier this week that the Canada Revenue Agency (CRA) site was hacked into over a six-hour period and the Heartbleed vulnerability was exploited to nab roughly 900 social insurance numbers and possibly other information from Canadian taxpayers.

"The CRA worked around the clock to implement a 'patch' for the bug, vigorously test all systems to ensure they were safe and secure, and re-launch our online services," said CRA commissioner Andrew Treusch in a statement. "The CRA is one of many organizations that was vulnerable to Heartbleed, despite our robust controls."

Police arrested Stephen Solis-Reyes, 19, in London, Ontario, on Wednesday and seized his computer equipment. He is allegedly associated with the attack, according to Reuters, and faces criminal charges of unauthorized use of computer and mischief in relation to data.

"It is believed that Solis-Reyes was able to extract private information held by CRA by exploiting the vulnerability known as the Heartbleed bug," the Royal Canadian Mounted Police said, according to Reuters.

News of the massive Heartbleed bug reverberated across the Internet last week showing how easily people's online data could be accessed. This particularly nasty vulnerability -- which has the capability to potentially extract people's usernames, passwords, and credit card information -- is said to have affected up to 500,000 Web sites, including Google, Facebook, Yahoo, and many more.

While the hack into the CRA appears to be the first reported attack with Heartbleed, it likely won't be the last.

Solis-Reyes is scheduled to appear in court on July 17.