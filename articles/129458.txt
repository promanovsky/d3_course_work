As point-and-shoot camera makers have continued to try to stave off the hard-charging smartphone market, built-in Wi-Fi and dedicated apps have become standard fare on most of the newer models being released today. Thus, media card manufacturer Eyefi is giving camera companies a boost in the relevancy game with a new subscription cloud service called, simply, Eyefi Cloud.

The benefit here is two-fold as this particular photo-centric cloud service offers instant back-up while also providing multi-device access convenience. For $50 a year, using the Eyefi Mobi card and the updated Eyefi app, all of your images can be transferred to both the device of you choice as well to the Eyefi Cloud.

For the uninitiated, Eyefi has been making wireless memory cards for years that instantly transfer images from the camera to your computer as soon as you are wirelessly within range of your PC. Late last year the company introduced a new version, called Mobi card , also designed to simply transfer images and videos wirelessly from a camera (or camcorder) to a smartphone. The Mobi instantly establishes a direct connection with your smartphone so there's no need to be in a Wi-Fi hotspot for the connection to work.

The focus with Eyefi Cloud is solely on photos as this isn't being set up as a cloud drive but instead designed as a cloud photo album. Think of this essentially as a cloud-based camera roll that is instantly syncing all of your devices as soon as you snap a picture.

While the $50 per year price is attractive, if you don't already own an Eyefi media card you're looking at shelling out roughly another $50.

With Dropbox and Google+ also offering variations on cloud syncing for smartphone shooters and the aforementioned built-in Wi-Fi becoming so common on today's digital cameras, it will be interesting to see if Eyefi can make a play here with their Eyefi Cloud launch.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.