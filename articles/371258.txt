Business Insider

Samsung announced earnings on Monday night, and they were worse than anyone expected — by a wide margin.

Operating income was 7.2 trillion won ($7.1 billion), a 24% drop on a year-over-year basis. Analysts were expecting 8 trillion won.

Sales were down to 52 trillion won, a 10% year-over-year drop.

To put this in context, last year, when Apple's stock was battered and people were murmuring that it was doomed, it had never posted negative sales growth.

So what's happening to Samsung?

Basically, at the high end, it's struggling to take share from Apple, and that market is quickly saturating.

At the low and mid-end, it's struggling to compete with Chinese phone makers who sell phones that are cheaper than Samsung but otherwise are pretty much the exact same thing.

This trouble for Samsung is not surprising.

In February at Mobile World Congress, the big mobile industry conference in Barcelona, we saw this coming. When I walked around the showroom floor, I had two takeaways: Android is everywhere, and there is an army of low-cost smartphone makers selling phones that look just like Samsung phones.

The following is what I wrote at the time (including the photos below):

On Android, it's just amazing to see all these no-name companies taking Android and making their own OSes. (Bigger companies, like Nokia and Yandex, are doing the same thing.)

I can't help think of Microsoft and how it totally whiffed on mobile. Billions of people are going to be using Android in the future.

As for Samsung ... coming from the U.S., I expected to see a bunch of Apple knock-offs. Apple is the leading company in the U.S. But so far, I've seen relatively few Apple clones. It's the Samsung look that's all the rage for copying.

Why so many Samsung knock-offs? If I had to guess, I'd say that it's easier to replicate the Samsung design and still have a decent product than it is to replicate the iPhone design and still have a decent product.

Seeing all of these phones that look just like Samsung phones boosted my respect for the company.

In a world of smartphones that all pretty much look the same and are approaching feature parity, that all have the same software (Android), it's really impressive that Samsung has built a standout smartphone brand.

All these phones are a little scary, though. The high end of the market seems to be saturating. Samsung has no desire to compete in the low end. And even if it did, the low end is crowded with companies selling $100 smartphones that look just like Samsung's $500 phones.

Looks and performance are two different things of course. The phones on the floor worked well. The cameras weren't great, but overall they seemed fine enough.

Here's a look at some of the phones that are copying Samsung:

This is the Grand S II from ZTE. That "S" looks a lot like the S in Samsung's S4 logo. The plastic backing on this phone also looks a lot like old Samsung plastic.

Business Insider

This is the S4 logo. See the similarity to the logo above?

Business Insider

And here's the back of an older Samsung phone that looks like that ZTE phone.

Steve Kovach, Business Insider

This is a big phablet from a company we never heard of called Umeox. We couldn't get a price on this phone. If we had to guess it will sell for $150-$200. Take a look at how it copies the stitching that Samsung is doing with its Note III.

Business Insider

Business Insider

For comparison, this is the Note III with faux leather backing and stitching.

Steve Kovach/Business Insider

Umeox also did the Samsung phone cover thing where it has a window that shows the front of the phone.

Business Insider

The S4 case with window.

William Wei, Business Insider

This was at the booth of JZH, a company that's doing 1 million in sales on a monthly basis. (Or so it tells me.) Its phones sell mostly in South America and cost $50-$90.

Business Insider

If that font looks familiar ... it's because it's a straight rip from Samsung.

Screenshot

These are more phones from JHZ. As you can see, they look a lot like Samsung's phones.

Business Insider

This was Konka, a Shenzen-based company, taking Samsung's stitching backing style.

Business Insider

More from Konka. This is the generic look/design of a Samsung. It's also all over the place here at MWC.

Business Insider

We didn't see this phone on display from Konka. It's an attempt to copy Apple and Samsung.

Business Insider

In closing, here's the real thing. A Samsung tablet.

Business Insider

And the Samsung Galaxy Note III.