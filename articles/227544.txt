A judge on Tuesday ruled a daughter of Casey Kasem can travel to Washington state, where the ailing radio personality is believed to be staying, and make medical decisions on his behalf, City News Service reported.

The decision by Los Angeles Superior Court Judge Daniel Murphy in favor of Kasem's daughter, Kerri Kasem, expands on powers she had been given last week when Murphy named her as the 82-year-old radio DJ's temporary conservator.

Kasem, who is known for his weekly top 40 countdown show and for being the voice of Shaggy on the "Scooby-Doo" cartoons, was located in Kitsap County, Wash. last week after attorneys for Kerri Kasem reported they did not know where he had been taken.

Kasem suffers from a form of dementia called Lewy body disease, and his children have been involved in a legal tussle with their stepmother, Jean Kasem, over visitation and caretaking for the radio personality.

Murphy, in his ruling on Tuesday, gave Kerri Kasem the power to travel to Washington state and make medical decisions on her father's behalf and he ruled that Jean Kasem must cooperate.

The judge also said Kerri Kasem, who also has a radio career, can hire a private investigator to assist her in dealing with the situation but the investigators must be unarmed, City News Service reported.

An attorney for Kerri Kasem did not return calls.