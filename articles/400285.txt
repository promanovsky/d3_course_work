Pan-African airline ASKY has suspended all flights to and from the capitals of Liberia and Sierra Leone, and the Liberian football association cancelled games as fears grow of the spread of the Ebola virus.

The move by the Togo-based carrier follows the death of one of its passengers from the virus after he flew from Liberia to Nigeria via the Togolese capital, Lomé.

Patrick Sawyer, 40, a US citizen who worked for the Liberian government, died in Lagos on Friday in Nigeria's first confirmed death from Ebola.

"The suspension of flights to Freetown and Monrovia by ASKY is related to the fight against the spread of the Ebola virus," said Afoussath Traore, a spokesman for the company.

"ASKY took this precautionary measure to ensure the safety of all: its passengers, staff and people of all countries covered by our network."

ASKY, which works in partnership with Ethiopian Airlines, serves 20 destinations in central and west Africa.

The virus moving across borders for the first time aboard the airline could spell new flight restrictions aimed at containing outbreaks, the world aviation agency said.

The International Civil Aviation Organisation secretary general, Raymond Benjamin, said: "Until now [the virus] had not impacted commercial aviation, but now we're affected.

"We will have to act quickly. We will consult with the WHO [World Health Organisation] to see what types of measures should be put in place."

On its website, ASKY also announced that it would stop transporting food from Conakry in Guinea and would screen all passengers travelling from the Guinean capital.

Nigerian carrier Arik also said on Sunday it was halting direct flights to Liberia and Sierra Leone.

In Liberia, the football association said in a statement on Tuesday that it had decided "to cease operations of football activities considering that football matches are contact sports, and Ebola is spread through bodily contact with an infected person".

Meanwhile the finance ministry in Liberia, where 127 people have died of the disease, said it had placed several senior officials under observation for three weeks after Sawyer died while on official business in Nigeria.

"All senior officials coming in direct or indirect contact with Mr Sawyer have been placed on the prescribed 21 days of observatory surveillance," the ministry said.

In Sierra Leone, a doctor in charge of an Ebola treatment centre became another victim of the virus.

"Dr Umar Khan died at 2pm," announced Brima Kargbo, the chief medical officer.

Khan was admitted last week into an anti-Ebola treatment facility run by the medical charity Doctors Without Borders (MSF) after testing positive for the virus.

Since March, there have been 1,201 cases of Ebola and 672 deaths in Guinea, Liberia and Sierra Leone.

Ebola can fell victims within days, causing severe fever and muscle pain, vomiting, diarrhoea and, in some cases, organ failure and unstoppable bleeding.

The highly contagious and often fatal disease spreads among humans via bodily fluids, including sweat, meaning one can get sick from touching an infected person.

With no vaccine, patients believed to have caught the virus must be isolated to prevent further contagion.

In Canada, local media reported on Tuesday that a Canadian doctor had put himself in quarantine as a precaution after spending weeks in west Africa treating patients with the virus alongside a US doctor who is now infected.

Azaria Marthyman of Victoria, British Columbia, had worked in Liberia with the Christian relief organisation Samaritan's Purse.

He has not tested positive, nor shown any symptoms since returning to Canada on Saturday, but one of his US colleagues, Dr Kent Brantly, is being treated for the disease.

"Azaria is symptom-free right now and there is no chance of being contagious with Ebola if you are not exhibiting symptoms," Melissa Strickland, a spokeswoman for Samaritan's Purse, told broadcaster CTV.

Brantly, 33, became infected with Ebola while working with patients in the Liberian capital as he helped treat victims.

David McRay, a friend and family medicine doctor in Fort Worth, Texas, told AFP Brantly was "not doing well. He is still in the early stages of the Ebola infection but having some daily struggles".

He added: "He has requested that I not talk in detail about his symptoms and what he is experiencing, but he is weak and quite ill."