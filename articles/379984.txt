John Shinal

SPECIAL FOR USA TODAY

SAN FRANCISCO — There's an old business maxim which argues that top executives who have been with a turnaround story for at least two years have become a part of their company's unsolved problems.

Yahoo CEO Marissa Mayer, however, who came on board exactly 24 months ago, has enjoyed a longer grace period than is typical from Wall Street investors, thanks to Alibaba's imminent IPO.

And it looks as if she's just been given even more breathing room to improve the Internet giant's fortunes.

In the company's second-quarter earnings announcement, the former Google executive expressed her disappointment with Yahoo's performance in frank terms:

"Our top priority is revenue growth, and by that measure, we are not satisfied with our Q2 results," she said in the statement.

Three of the company's most important financial measures fell year-over-year, with sales slipping 4% amid increased competition for online ad dollars; operating income plunging 72% on higher spending and restructuring charges; and net income dropping 19% despite getting an Alibaba boost.

Two years into her tenure in Yahoo's corner office, Mayer's string of investments, revamps and acquisitions have yet to drive consistent improvement in the company's top and bottom lines.

In addition to bad news on operations, however, the report also had a silver lining courtesy of Alibaba, the fast-growing and profitable online giant in which Yahoo has a large stake.

The two companies agreed to cut the number of shares in half that Yahoo will sell in the offering.

That means Yahoo Chief Financial Officer Ken Goldman can use more of the ongoing earnings from the company's now-larger interest in Alibaba to continue funding Mayer's turnaround plans.

Even better for shareholders, Yahoo plans to return half its after-tax proceeds from the IPO to investors.

Given that the Alibaba offering could become the largest tech IPO ever, those proceeds will fund a lot of stock buybacks and/or shareholder dividends.

One Yahoo stock analyst wrote a report on Monday saying that the Alibaba IPO valuation will need to be north of $150 billion to drive an upside in Yahoo's share price.

Yet, that was before the latest Alibaba good news, which means Mayer can, for now, continue to give investors what they want even without solving Yahoo's biggest problems.

They're problems which — after two years in charge — she now clearly owns.

John Shinal has covered tech and financial markets for more than 15 years at Bloomberg, BusinessWeek,The San Francisco Chronicle, Dow Jones MarketWatch, Wall Street Journal Digital Network and others. Follow him on Twitter: @johnshinal.