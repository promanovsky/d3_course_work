With a studio estimated opening of $100 million, "Transformers: Age of Extinction" is looking like the blockbuster that everyone assumed it would be, racking up a gaudy $23,000+ per venue at 4,233 total theaters around the country. Meanwhile, it was another very good weekend for R-rated comedy "22 Jump Street" as well as most of the other box office stalwarts, though "Think Like a Man Too" did slip by more than 60% from its phenomenal opening a week ago. It should also be the last weekend that "X-Men: Days of Future Past" lands in the top 10 after six strong weekends for distributor Fox, enough time to push it to $223 million in the U.S. and more than $700 million around the globe.

With a price tag of $210 million and three major blockbusters already in the franchise, "Transformers: Age of Extinction" had plenty of commercial expectations heading into opening weekend, though the Paramount distribution easily met high standards both domestically and abroad. "Age of Extinction" put up a sensational $41 million domestically on Friday before slowing down a bit over the rest of the weekend, though it still had enough to easily topple the $95 million opening of "Captain America: The Winter Soldier" to post the top opening of 2014. If the early studio estimates hold, "Age of Extinction" would be the first release to earn at least $100 million domestically in a weekend this year, which is something that happened three different times in 2013.

Consistent with the franchise, "Age of Extinction" also did very well outside of the U.S. in its first weekend, bringing in more than $200 million in foreign revenue to post a spectacular worldwide opening of $301 million. Considering the last entry, "Dark of the Moon," surpassed $1.1 billion in total box office revenue, it looks like "Age of Extinction" could once again challenge the $1 billion mark as long as it continues to find action audiences outside of the U.S.

But even though the franchise is clearly doing just fine, "Age of Extinction" still has quite a bit of work to do in order to keep up with the domestic totals of the previous installment. Released on a Wednesday, "Dark of the Moon" brought in a jaw-dropping $162 million domestically in its first five days in theaters - a total that "Age of Extinction" isn't likely approach no matter how strong numbers are on Monday and Tuesday. Still, with "Age of Extinction" likely dominating with action audiences all the way through the Fourth of July, the "Transformers" franchise is alive and well, leading to what should be one of the most lucrative box office runs of 2014 when all is said and done.

Another movie that continues to put up unbelievable numbers is "22 Jump Street," the R-rated comedy starring Jonah Hill and Channing Tatum. After just 17 days in theaters, "22 Jump Street" has already surpassed the $138 million domestic haul of "21 Jump Street" and appears to have really settled in with audiences. "22 Jump Street" ended up slipping just 44% from last weekend's totals despite the presence of "Think Like a Man Too," which looks like a clear sign that it still has plenty of ticket sales ahead. Though R-rated comedy "Tammy" will provide some competition around the holiday, "22 Jump Street" seems to be benefiting from strong word-of-mouth from adult audiences and should easily surpass "Neighbors" by next weekend, making it the top-grossing comedy of 2014 in less than a month.

Even with some major new competition, "X-Men: Days of Future Past" also showed that it still has a significant audience as well, grossing a decent $3.30 million domestically in its sixth weekend in theaters. More importantly, "Days of Future Past" has been a major hit abroad, pushing it to a very solid $712 million worldwide so far. Considering that none of the previous "X-Men" entries had even come that close to reaching $500 million worldwide, "Days of Future Past" has shown that the franchise is definitely heading in the right direction leading up to the May 2016 opening of "X-Men: Apocalypse."

Next weekend, the Fourth of July should have theaters across the country very busy, with "Transformers: Age of Extinction" very likely to stay as the number one box office draw well into July. Providing some competition, however, will be Melissa McCarthy's latest R-rated comedy "Tammy," R-rated horror flick "Deliver Us from Evil" as well as PG family film "Earth to Echo." On July 11th, "Dawn of the Planet of the Apes" will be looking to follow the success of the popular 2011 reboot "Rise of the Planet of the Apes," bringing major commercial expectations from distributor Fox.

Early Studio Box Office Estimates for 6/27/14 - 6/29/14 (in millions):

1) Transformers: Age of Extinction (Paramount): $100 [$105]

2) 22 Jump Street (Sony): $15.40 [$15]

3) How to Train Your Dragon 2 (Fox): $13.10 [$13]

4) Think Like a Man Too (Sony/Screen Gems): $10.40 [$14]

5) Maleficent (Disney): $8.24 [$6.9]

6) Jersey Boys (Warner Bros.): $7.61 [$7]

7) Edge of Tomorrow (Warner Bros.): $5.21 [$5]

8) The Fault in Our Stars (Fox): $4.80 [$4.9]

9) X-Men: Days of Future Past (Fox): $3.30 [$3]

10) Chef (ORF): $1.65 [$1.5]

For comments and feedback contact: editorial@rttnews.com