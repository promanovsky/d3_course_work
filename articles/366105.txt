Charlize Theron dazzled in a metallic gold mini dress as she sat with Sean Penn on the front row of the Christian Dior show for Paris Fashion Week this afternoon (July 7).

Rindoff/Dufour/French Select/Getty Images



The 38-year-old actress, who is the spokesperson for the Christian Dior J'Adore campaign, wore a gold, thigh-skimming frock with a fringed hem as she attended the Haute Couture Fall/Winter 2014-2015 collection at the Musee Rodin in the French capital. Penn arrived in aviator sunglasses and a smart black suit.

Pascal Le Segretain/Getty Images

Pascal Le Segretain/Getty Images



The tactile couple held hands, and Penn placed his hand on Theron's lap as they watched the showcase along with French actress Marion Cotillard and designer Raf Simons.

Rindoff/Dufour/Getty Images



Speaking about their relationship, the fiercely private star recently shared with reporters: "I am very happy. I will say that. I'm very happy. I'm not very good at talking about my private life, but we are very happy, yes."



Earlier today, Jennifer Lopez was also wowing photographers as she arrived at the Atelier Versace show in a strapless, white, floor-length Versace dress. She later posed with Donatella Versace at the show, which kicked off the fashion week.

Pascal Le Segretain/Getty Images



Paris Haute Couture Week runs from July 6 to 10.

Naomi Gordon Naomi Gordon is news writer mainly covering entertainment news with a focus on celebrity interviews and television.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io