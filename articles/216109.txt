Can an injection of the measles cure cancer? In a recent clinical trial, researchers say that a genetically modified strain of the virus showed an ability to target and reduce tumors in a human patient.

Writing in the journal Mayo Clinic Proceedings, lead study author and hematologist Dr. Stephen Russell said that after infusing two multiple myeloma patients with measles, one of them experienced “durable complete remission at all disease sites.”

The results, he said, raised new hopes for the use of virus-based cancer treatments.

“It is a breakthrough,” Russell said in a Mayo Clinic video. “We believe it can become a single-shot cure.”

Advertisement

(Russell and other authors have disclosed a financial interest in the developing technology.)

Scientists have been studying the potential use of herpes and other viruses to fight cancer for years, but until now successful results have been confined to animal experiments.

While a normal virus will attack healthy cells, researchers say, a modified virus can be made to attack malignant cells instead. However, the patient must not have already developed an immunity to the virus.

In this case, Russell and his colleagues developed a measles virus that targeted cells heavily coated with the protein CD46 -- the same protein that is heavily expressed in multiple myeloma cancer cells.

Advertisement

In multiple myeloma, abnormal plasma cells reproduce uncontrollably within bone marrow. As they accumulate, they limit the growth of normal plasma cells -- which help to fight infection -- and also cause bone lesions. Though the disease can be treated, doctors consider it incurable.

Russell and his colleagues described the effects of the modified virus on two women, ages 49 and 65, who had failed to respond to conventional treatments.

The younger woman, Russell said, showed “a remarkable response” to the treatment, although she quickly suffered a severe headache and nausea as the virus was administered.

Over a period of days a tumor on her forehead began to shrink, and then her cancer went into remission for nine months.

Advertisement

Russell said that there was a local relapse of the forehead tumor after that period, but that it was treated with radiotherapy. None of her other bone lesions returned, he said.

The older woman showed some improvement, although a number of tumors on her legs appeared to re-grow after initial signs of shrinking, Russell said.

In addition to the headache and nausea felt by the younger patient, both women experienced a recurring fever over a period of days, researchers said.

In an accompanying editorial, cancer researcher John Bell, of the Ottawa Hospital Research Institute, in Ontario, Canada, wrote that the study provided “compelling evidence” that a single infusion of a modified virus could trigger a systemic anti-tumor response.

Advertisement

However, he wrote that a lot more research still needed to be done on oncolytic viruses, or OVs.

“We still know remarkably little about the best way to deliver OVs intravenously to patients,” he wrote. “The speed and duration of the infusion, the quantity of the virus, and the number of doses to be administered are all poorly understood at this time.”