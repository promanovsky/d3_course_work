Wal-Mart Stores Inc. is recalling 174,000 dolls because the toy can overheat and potentially burn consumers.

The US Consumer Product Safety Commission said Tuesday that the "My Sweet Love / My Sweet Baby Cuddle Care Baby Doll" has a circuit board in its chest that can overheat, causing the surface of the doll to get hot and burn someone.

Wal-Mart has received 12 reports of incidents, including two burns or blisters to the thumb.

Consumers should stop using the product, remove the batteries and return them to any Wal-Mart store for a refund.

The electronic baby doll, made by Tak Ngai Electronic Toys Co. of China, was sold exclusively at Wal-Mart stores from 2012 through 2014 for $20.

It comes in pink floral clothing and a matching knit hat. The 16-inch doll is packaged with a toy medical check-up kit. The doll babbles when she gets "sick" and her cheeks turn red and she starts coughing. Using the medical kit pieces cause the symptoms to stop.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

The doll is identified by UPC 6-04576-16800-5 and a date code, found on a label sewn to the bottom of the doll, which begins with WM.

Consumers interested in finding out more about product safety recalls can visit the "Recalls" page on the Consumer Product Safety Commission website for a list of the latest products to be recalled and how to replace those products or receive a refund.