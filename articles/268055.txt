NEW YORK (MarketWatch) — The U.S. stock market ended Tuesday marginally lower, pausing after three straight days of gains that sent the S&P 500 and Dow Jones Industrial Average to record levels on Monday.

Investors found little comfort in a factory orders report, which was mostly in line with expectations, and better-than-expected monthly car sales.

The S&P 500 SPX, +0.47% closed less than a point lower at 1,924.24. The Dow industrials DJIA, +0.09% shed 21.29 points, or 0.1%, to 16,722.34.

The Nasdaq Composite COMP, +0.50% ended the day down 3.12 points, or 0.1%, at 4,234.08.

Follow MarketWatch’s live blog of today’s stock-market action.

Rob Stein, CEO of Astor Investment Management, said that stocks and long-term government bonds are most likely priced where they should be.

“The economy is growing very slowly, but still growing. The S&P 500 is up about 4% on the year and that is consistent with the current growth environment,” Stein said.

“The 10-year yield at 2.5% is not surprising given that the inflation is at 1%, unemployment in 6s and stalled growth in the first quarter. In fact, we fear slipping into a deflationary environment more than inflation rising,” he added.

Orders for goods produced in U.S. factories rose 0.7% in April, the Commerce Department said Tuesday. Economists surveyed by MarketWatch had expected orders to rise 0.6%. Factory orders climbed by a revised 1.5% in March, compared with a prior estimate of 0.9%. Orders for durable goods -- products meant to last at least three years -- rose 0.6% in April. Orders for nondurable goods increased 0.7%.

The auto industry was in focus after car makers released their monthly sales figures.

Shares in General Motors Co. GM, +0.19% and Ford Motors Co. F, rose 1.1% and 0.7% respectively after they released their monthly car sales in the U.S.

GM said U.S. vehicle sales were up 12.6% in May, marking its best monthly performance since August 2008, The Wall Street Journal reported. The results topped analyst expectations.

Ford reports a 3% rise in sales in May. Getty Images

Ford Motor Co. reported a 3% rise in sales in May. Chrysler Group LLC, owned by Fiat, reported 17% growth in sales in May, as its Jeep and Ram brands continued to deliver strong results.

In deal news, shares of Hillshire Brands Co. US:HSH jumped 9.5% after Pilgrim’s Pride raised its offer to buy the company for $55 a share in cash, valuing the deal at $7.7 billion. Pilgrim’s Pride said the new bid represents an increase of $1.3 billion from its initial offer. Hillshire said it would talk separately with Pilgrim’s Pride and Tyson Foods Inc. TSN, +0.34% , both of which are interested in buying the maker of Jimmy Dean sausages and Sara Lee desserts.

Krispy Kreme Doughnuts Inc. US:KKD shares slid 15% after the company lowered its earnings outlook for the fiscal year when it reported results late Monday.

Dollar General Corp. DG, -0.72% rose 3.9% after the discount retailer’s first-quarter earnings of 72 cents a share came in one penny a share less than Wall Street had projected. Net sales were $4.52 billion, compared with expectations of $4.56 billion.

In other financial markets

Also ahead of the start of U.S. trading, Asian stocks finished mostly higher, with Japanese shares closing at a two-month high. Stocks in Europe SXXP, +0.64% remained lower after April inflation in the euro zone came in at 0.5%, below expectations of a 0.6% reading.

U.S. Treasury prices fell, sending yields higher for a fourth consecutive session, as prices retraced their fall in recent weeks. The 10-year note US:10_YEAR yields rose to 2.6%.

Gold futures US:GCQ4 settled with a modest gain on Tuesday, their first in seven sessions, while oil futures CLN24, -2.05% settled slightly higher. The dollar index DXY, +0.03% fell to 80.534, from 80.634 late Monday.

More must-reads from MarketWatch:

The next four days could rock uneasy U.S. markets

Too much of your retirement money in stocks?

Bear echoes his call for a huge U.S. market crash as Europe darkens