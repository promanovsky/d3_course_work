It goes without saying that Samsung has at least two flagship devices where mobile communications is concerned – the Galaxy S series as well as the Galaxy Note range. While their current stalwarts are the Galaxy S5 and the Galaxy Note 3, the latter is due for a revamp, and that would mean the much talked about Galaxy Note 4. Having been spotted on the AnTuTu benchmark with an Exynos chipset alongside a 16MP shooter, which is in line with the expectation of 2014 flagship devices to carry this resolution on their cameras, it is then interesting to note that South Korea’s ETNews has reported otherwise, with the Galaxy Note 4 arriving with a 12MP camera with Optical Image Stabilization (OIS).

Advertising

This source also claims that Samsung and its hardware partners have already set up their production facilities good and proper. These production facilities are said to be located in Tianjin, China, where it will be the front runner for the 12MP camera’s mass production. Apart from that, the factory might even see additional investments thrown into the mix, which could result in up to 3 million units rolling off the production lines each month, now how about that? In addition, there are also whispers of a 3.7MP camera making its way to the front-facing shooter of the Galaxy Note 4.

Once again, all of these remain rumors until Samsung steps forward with actual, concrete and official information.

Filed in . Read more about Galaxy Note 4 and Samsung.