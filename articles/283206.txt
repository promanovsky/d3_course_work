Priceline is buying online restaurant reservation company OpenTablefor $2.6 billion. The deal should help Priceline, the online travel company, branch out into a new business segment

"Travelers are diners," said Priceline CEO and President Darren Huston, in a conference call. "It's the same customers. There's opportunity to cross promote brands."

Priceline will pay $103 per share, which is a 46% premium to OpenTable Inc.'s Thursday closing price of $70.43.

Shares of OpenTable soared $33.59, or 47.7%, to $104.02 -- above the offered price -- in morning trading Friday.

OpenTable seats more than 15 million diners per month at more than 31,000 restaurants. OpenTable allows users to make free reservations at restaurants through its website and mobile apps. It makes money by charging restaurants fees for the bookings. Users can also read reviews of the restaurants and view menus through the website.

The table reservations company will still be based in San Francisco and will operate as an independent business led by its current management team.

Huston said Priceline's first goal is to expand OpenTable internationally. Users can already book restaurants through OpenTable in London, Berlin, Hong Kong and other cities, but Huston said it wanted to bring it to more cities. Since Priceline already has "offices in every major city in the world," doing so should be seamless, he said.

OpenTable is also working on making it easier to sign up new restaurants to its service, said Huston. The company is working on creating a cloud-based system instead of using the hardware it currently needs restaurants to install to use OpenTable.

At Priceline, an average of more than 1 million guests stay in accommodations booked through one of its brands each night. It has more than 480,000 properties in more than 200 countries and territories worldwide.

The Norwalk, Conn., company changed its name from Priceline.com Inc. to Priceline Group Inc. in April to better reflect its overall business. Its brands include Booking.com, priceline.com, agoda.com, KAYAK and rentalcars.com.

Both companies' boards unanimously approved the transaction, which is targeted to close in the third quarter.

Shares of Priceline rose $5.89 to $1,231.89 in morning trading, and then dropped.