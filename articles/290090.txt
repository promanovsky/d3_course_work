

Crude oil jumped to the highest in more than eight months on concern that a civil war looms in Iraq, OPEC’s second-biggest producer. Oil extended gains, with Brent futures rising a fourth day on speculation the escalation of violence in Iraq will threaten crude output. WTI rallied 4.1 percent last week, its steepest weekly advance since April, while Brent added 4.3 percent, topping $114 June 13 for the first time in nine months. Iraq’s government is seeking to reassert control over ground now held by the breakaway al-Qaeda group, whose advance puts in doubt the unification of the Persian Gulf country. Sectarian strife is pushing the second-largest producer in the Organization of Petroleum Exporting Countries closer to civil war, three years after the U.S. withdrew its forces.

Oil Hour Chart



Gold climbed to near a three-week high as the escalation of violence in Iraq stoked concern crude supplies will be disrupted and boosted the appeal of haven assets. Asian stocks retreated with U.S. gained 6 percent this year, partly because of escalating tensions between Ukraine and Russia. Hedge funds cut wagers on rising agricultural prices at the fastest pace since January before the U.S. government predicted rising supplies of everything from wheat to rice. Money managers are now holding the smallest wager on farm goods including cotton and soybeans in almost four months.