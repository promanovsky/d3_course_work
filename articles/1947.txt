Former security contractor Edward Snowden, addressing a sympathetic crowd at a tech-heavy event in Austin, Texas, on Monday from a secret location in Russia, said proposed reforms at the National Security Agency show that he was vindicated in leaking classified material.

Snowden, who faces arrest if he steps foot on U.S. soil, spoke via a video link to a packed house at the annual South by Southwest (SXSW) gathering of tech industry experts, filmmakers and musicians. He said the U.S. government still has no idea what material he has provided to journalists.

"I saw that the Constitution was violated on a massive scale," Snowden said to applause, adding that his revelations of government spying on private communications have resulted in protections that have benefited the public and global society.

NSA officials declined to comment on the Snowden remarks.

Last year, Snowden, who had been working at a NSA facility as an employee of Booz Allen Hamilton, leaked a raft of secret documents that revealed a vast U.S. government system for monitoring phone and Internet data.

The leaks deeply embarrassed the Obama administration, which in January banned U.S. eavesdropping on the leaders of friendly countries and allies and began reining in the sweeping collection of Americans' phone data in a series of limited reforms triggered by Snowden's revelations.

Major companies also tightened up safeguards. But Snowden said the efforts are still not enough to protect privacy properly, calling for stepped-up encryption that would make mass government surveillance too costly to conduct.

"The government has gone and changed their talking points. They have changed their verbiage away from public interest to national interest," he said, adding that this poses the risk of losing control of representative democracy.

He said the government's priority has been an expansive and ill-executed system of massive information collection instead of protecting the vast amounts of intellectual property that support the U.S. economy.

"We've got the most to lose from being hacked," Snowden said.

U.S. Representative Mike Pompeo, a Republican from Kansas, wrote to SXSW organizers, calling on them to withdraw the invitation to Snowden, who he said deceived his employer and his country.

"Rewarding Mr. Snowden's behavior in this way encourages the very lawlessness he exhibited," Pompeo wrote.

To many in government and at the NSA, Snowden is a traitor who compromised the security of the United States. But for many at the conference he is a hero who protected privacy and civil liberties.

"To me, Snowden is a patriot who believed that what he did was in the best interests of his country," said Roeland Stekelenburg, creative director at the Dutch Internet firm Infostrada.

Snowden fled to Hong Kong and then to Russia, where he currently has asylum. The White House wants him returned to the United States for prosecution.

© Thomson Reuters 2014