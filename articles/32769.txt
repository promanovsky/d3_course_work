Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

It may have been 30 long years since Footloose was first released, but Kevin Bacon has shown he's still got all the moves in this spectacular entrance to The Tonight Show.

The Following actor , who played the lead roll of Ren McCormack in the 1984 blockbuster , is seen dancing his way through the corridors of the studio after Jimmy Fallon declares dancing is illegal and banned from the show.

At one point he even strips down to his vest as his recreates some of the most famous scenes from the iconic film.

There are also somersaults, impressive rail work and a group number that brings him into the studio with a burst of glitter.

The father-of-two's performance was pitch perfect, even puffing on a cigarette and taking a swig from a bottle of booze before smashing it against the wall - just as his character did in the iconic warehouse scene.

Joined by backing dancers, he shimmied through the hallways of the 'Tonight Show' studio and emerged in front of the audience, clad in a vintage maroon tuxedo.

Shouting his infamous line, "Let's dance!, Kevin launched into a show-stopped finale of Footloose's titular track to rapturous applause.

Even host Jimmy himself gets in on the action and throws some shapes!