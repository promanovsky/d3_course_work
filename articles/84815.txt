Watch Dogs, eat your heart out. An industrious little hacker  well, a 5-year-old kid  named Kristoffer Von Hassel found a fairly ingenious way to bypass security restrictions on the Xbox One.

In doing so, and by bringing the exploit to Microsoft's attention, he's been rewarded the title of "security researcher" from the company.

That, and he scored quite a bit of loot for his find: four free games, a cool $50, and a 12-month subscription to Xbox Live. Not too shabby, especially given what it took for Von Hassel to find said hole in the Xbox One's authentication system.

Here's how his exploit worked. Von Hassel's dad put a password on his Xbox Live account so that the 5-year-old couldn't (presumably) access it when he wasn't around. As 5-year-olds do, Von Hassel tried anyway. When his first attempt at the password didn't work, the Xbox One pulled up a second verification screen. Von Hassel filled this prompt with nothing but spaces and  Voila!  the Xbox One ($200.00 at eBay) authenticated him into his father's account.

What gave Von Hasssel away? Alas, youth: His parents caught him playing a game on the Xbox One that they didn't recall giving him access to. Once Von Hassel showed them his trick, his father filmed his son re-performing the exploit. A security researcher himself, the dad ended up sending that video off to Microsoft, and the rest is digital history.

"We're always listening to our customers and thank them for bringing issues to our attention. We take security seriously at Xbox and fixed the issue as soon as we learned about it," reads a statement from Microsoft.

Although it's probably good for Von Hassel's father, Robert Davies, that his son didn't figure out how to buy anything and everything from the Xbox Live Store, the dad is nevertheless proud of his kid's accomplishment. However, it apparently isn't Von Hassel's first foray in exploit finding. According to Yahoo Tech, the tyke had already found success bypassing his parents' smartphone toddler lock four years prior.

"Just being 5 years old and being able to find a vulnerability and latch onto that. I thought that was pretty cool," said Davies in an interview with San Diego news station KGTV.

Further Reading