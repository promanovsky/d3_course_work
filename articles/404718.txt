Health experts in Australia and New Zealand are preparing to tackle the deadly Ebola virus in case it reaches their countries.

Although health officials consider the risk of the virus reaching the Oceania region as low, they are closely monitoring the situation and have put in place contingency plans.

"May be we are a little bit lucky. We don't have huge traffic and travel between West Africa and New Zealand at the moment - but a theoretical risk is there. Just think about the 2009 [influenza] pandemic when we had students from Auckland to Mexico coming back; they carried the virus to New Zealand," said Sue Huang, virologist at the Institute of Environmental Science and Research.

She warned that if a traveller infected by the virus reaches New Zealand there is no safe place to test the bodily samples and isolate the virus.

"We will have to forward the sample to a reference lab overseas to deal with that if anything happens - the CDC [Centre for Disease Control and Prevention] in the US or may be Australia. We contacted people at the moment so we can prepare if anything happens," said Huang.

Chief of public health Darren Hunt said: "In the very unlikely event that New Zealand had cases of Ebola virus disease, the ministry would respond as it would to other emerging threats to health, through established protocols and processes such as the National Health Emergency Plan (NHEP)."

Fears of a global Ebola outbreak have risen after a virus-infected man was allowed to travel from Liberia to Nigeria, where he eventually died.

Precautionary measures in Europe

Several countries in Europe and Asia have announced precautionary measures.

The European Union has said the EU is ready to deal with the threat.

Foreign Secretary Philip Hammond told the BBC: "At the moment we don't think any British nationals [abroad] are affected and we are fairly confident there are no cases in the UK. But it is a threat. It is something we need to respond to and we will be doing so through the Cobra mechanism."

At least 1,201 Ebola cases have been registered in Guinea, Liberia and Sierra Leone since March this year, according to the World Health Organisation, and the virus has so far claimed 670 lives.