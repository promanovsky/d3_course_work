The Rolling Stones have returned to touring with a high-energy, sold-out show in Oslo, following a break of several months after the death of fashion designer and frontman Mick Jagger's partner, L'Wren Scott.

The classic rock quartet — comprising Jagger, Keith Richards, Charlie Watts and Ron Wood — played to approximately 23,000 fans at a late-night concert at Oslo's Telenor Arena Monday evening.

The Stones played hits such as Satisfaction, Brown Sugar, Sympathy for the Devil and You Can't Always Get What You Want (with Bergen's Edvard Grieg Youth Choir joining in on the latter), ultimately ending the gig with a fireworks display.

"It was a great rock 'n roll show," said 55-year-old fan Jan Martin Schultz, who attended the concert with two friends.

Having seen the Rolling Stones several times before, he also remarked on the band's energy onstage.

"I saw them in Gothenburg in 1982 and I thought then they seemed old. They didn't seem old tonight."

Several Norwegian music critics also gave the show a positive review in today's newspapers, noting in particular Jagger's physicality as he danced, strutted, sang for and joked with the audience.

The Stones suspended their current world tour in March after the death of Scott, who committed suicide in her Manhattan apartment.

At the time, the band was on the Australia and New Zealand leg of the tour. The shows were later rescheduled for this coming fall.

Jagger did not refer to Scott during Monday night's Oslo concert, which kicked off the band's upcoming European jaunt.