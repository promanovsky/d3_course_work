Putting on weight can increase women's risk of ovarian cancer, according to a new assessment of the evidence.

Ovarian cancer has a poor prognosis because it has few symptoms and is usually caught at a late stage. More than 7,000 women are diagnosed with ovarian cancer every year and there are about 4,300 deaths. If it is caught early more than 90% of women will survive for at least five years, but at the late stage that drops to less than 10%.

The World Cancer Research Fund (WCRF), which works on the prevention of cancer, has published an update of the risks for ovarian cancer, as part of its Continuous Update Project. For the first time, it says that overweight and obesity are a probable cause.

Weight is known to be an issue in some other cancers, including breast and bowel cancer. In the UK 61% of people (57% of women) are overweight or obese which, says the WCRF, puts them at greater risk of developing one of eight types of cancer than their normal-weight neighbours.

The WRCF says an estimated 23,400 cases of cancer – one in six – could be avoided if nobody was overweight. Maintaining a healthy weight is one of its 10 recommendations for avoiding cancer.

"We can now say with certainty that being overweight or obese increases the risk of developing ovarian cancer, just as it does with a number of other cancers such as breast, bowel and womb cancer," said Dr Rachel Thompson, head of research interpretation. "This means that women can make lifestyle changes to reduce their chances of getting ovarian cancer.

"Previously we only knew about risk factors that are fixed, such as age and family history of the disease, but now we can say that keeping to a healthy weight helps reduce the risk of getting ovarian cancer."

The increased risk is fairly small. The researchers analysed the results of 25 studies involving 4 million women, 16,000 of whom developed ovarian cancer. They found a 6% increased risk for every five extra BMI units (BMI – body mass index – is a calculation based on weight relative to height, and normal BMI levels are 18.5 to 25).

How significant a 6% increase for every extra BMI point is will depend on a woman's risk to begin with; it matters far more to someone who has a family history of it, for instance. But being overweight is one of the few ovarian cancer risk factors that women can do something about.

Adult height is also a risk, the WCRF says, because it is a result of the rate of growth in the womb and in childhood, which is also a factor in obesity.

The charity Target Ovarian Cancer pointed out that the additional risk was not massive, but it highlighted the benefits of healthier lifestyles.

"This new study supports earlier work that a healthy lifestyle can reduce your risk of ovarian cancer," said Dr Simon Newman, head of research. "For someone without a family history of ovarian cancer, their lifetime risk of developing the disease is 2%. The findings of this latest study suggest this increases to 2.24% for an obese person, which is still a low risk."

Gilda Witte, chief executive of the charity Ovarian Cancer Action, said the research finally provided clarity on the link between obesity and ovarian cancer.

"This robust report enables us to give clear guidance to women, and importantly for parents who can begin to encourage their daughters to adopt a healthy lifestyle as soon as possible so that the next generation has the best chance of reducing their risk," she said.