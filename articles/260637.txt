Google's first purpose-built self-driving car has arrived. The dinky two-seater looks like a cross between a Smart Car and a Nissan Micra, has no steering wheel, accelerator or brakes, and just two physical controls: "stop" and "go".

It's been a long time coming, and there's a lot more work for the company to do in the future. But right now, Google walks a knife-edge between over- and under-promising. Its cars could change the world – if anyone lets them. But there's also a certain sleight of hand involved in what it has shown off so far.

Are we nearly there yet?

The immediate problems for Google remain technical. It has reached the stage where its car can drive through the streets surrounding its headquarters in Mountain View, California, without any human intervention. But it's not just because of secrecy that it hasn't gone any further than that.

Google's cars use an array of sensors to map the world around them in real-time. On the roof, a spinning laser creates a 3D model of every major object surrounding it, be they fellow road users or potential hazards such as pedestrians and cyclists. Cameras on the front and sides supplement that model by looking out for important visual information such as road signs or traffic lights.

But the car can't gather quite enough information in real time for it to be safe. Instead, as the Atlantic's Alexis Madrigal explains, the company relies on ultra-precise maps of the area to help give it the information it needs.

"They're probably best thought of as ultra-precise digitisations of the physical world, all the way down to tiny details like the position and height of every single curb," Madrigal writes. "A normal digital map would show a road intersection; these maps [for the self=-driving car] would have a precision measured in inches."

With that model in place, all the car has to do is use its sensors to work out what's changed since then, comparing the real world to the model stored in its system.

But Google has mapped a grand total of 2,000 road miles to the precision required by its cars. California has over 170,000 miles of public road (PDF) and at least twice that if you include each lane as a separate "road". The UK alone has around 250,000 miles of road, and the firm hasn't even started here.

Laws and customs

But mapping the world is a project that Google's done before – twice. Its normal maps of the world have been around for the better part of a decade, and Street View, which requires driving physical cars through every road to be mapped, now covers 48 countries, including almost all of North America, Europe, South Africa, Australia and New Zealand, Japan and Taiwan.

Nor has it stopped; the mapping continues every day, all over the world, because streets and roads keep changing. What it needs to do is capture that data in fine-grained detail good enough for its cars. How long that will take isn't known.

The far trickier problem ahead is dealing with the changing social, political, and legal norms around driving.

Take the question Google's tackling right now: If a self-driving car breaks the law, who is at fault: the "driver", or the manufacturer?

At the California Department of Motor Vehicles (DMV), Google has spent the past few months trying to argue that the fault should lie with the manufacturer. "What we've been saying to the folks in the DMV, even in public session, for unmanned vehicles, we think the ticket should go to the company. Because the decisions are not being made by the individual," said the safety director of the self-driving car programme.

As for whether the car is even legal at all, the project's lead, Chris Urmson, argues that the law, in California at least, is already clear: "The law that was passed almost a year and a half ago made it quite clear that effectively driverless operation of vehicles was permitted in California and in general we believe that’s true across much of the US. What will happen in the not-too-distant future is the California DMV will issue regulations about the operation of self-driving vehicles, and I believe in the law there will be a a clause requiring a six-month notification period before vehicles without drivers are allowed on the road."

Trolley problems

But more difficult questions will inevitably arise; and how Google answers them will shape the progression of the technology.

Take a no-win situation for any driver: two children run out from behind a parked van, meaning there's no visible cue of their approach, and there isn't enough distance to brake.

At the same time, their mother runs (from behind a van too) into the road on the other side - she saw the danger. You can swerve and hit the mother, or carry on and hit the children. What do you do?



Such problems, and a class of others like it, are known in philosophy as "trolley problems". With tweaks, they can be used to draw out contradictions and paradoxes in our innate understanding of ethics, underscoring the fact that there seems to be no general answer to ethical problems which leaves everyone happy.

And yet Google has to decide on an answer anyway.

When a driver is faced with such decisions, we free them of responsibility whichever outcome they choose; the human mind just can't be expected to make difficult ethical decisions on a split-second basis.

But Google isn't working on a split-second basis. As Patrick Lin, the director of the Ethics & Emerging Sciences Group at California Polytechnic State University, wrote in Wired: "while human drivers may be forgiven for making a poor split-second reaction – for instance, crashing into a Pinto that’s prone to explode, instead of a more stable object – robot cars won’t enjoy that freedom. Programmers have all the time in the world to get it right. It’s the difference between premeditated murder and involuntary manslaughter."

These are questions that need answers. Without them, the first time a self-driving car is involved in a fatal collision – which is inevitable, no matter how much safer they are in aggregate than human-driven cars – the response could derail the whole project.

Over-deliver

And that would be unacceptable. Because a world where we've fully cracked the problems of self-driving cars would be significantly better than our own, and not just in the obvious ways.

The most immediate change would be safety. Google's cars have so far been involved in two incidents, neither of which was the fault of the software (one was rear-ended by a human driver; the other crashed while being driven manually).

They haven't yet been driven enough miles to be fully certified as safer than humans – on average in the UK there is one "slight casualty" for every 1.7m miles driven (and you have to drive six times further per road death). Google's cars have only clocked 700,000. Even so, a car which can't be driven drunk, which can't speed, and can't get distracted presents immense possibilities for the future.

But the bigger changes will come if the such cars overturn our entire idea of what "driving" is.

For instance, a car that needs no driver is also, in short order, a car that needs no occupants at all. It can drop you off at your destination, then head off to find a parking space, even if that's a mile out of town; or it can act as a simple courier, taking a package from A to B without needing to bring a person along for the ride.

In that world, though, why does anyone need to own a car at all? Google's Urmson touches on some of the possibilities in his interview with Recode's Liz Gannes: "My vision for this is eventually these vehicles will be shared, and it may be within a family, or it may be within a community, and that will result in less vehicles on the road, but they’ll be used much more efficiently, and that’s good for everyone."

That would make the taxi provider Uber or car-sharing scheme Zipcar look like relics from the stone age. But it also requires courage from the trailblazers.

Google isn't the only company working on self-driving cars: Volvo's 2014 model of its XC90 SUV can steer itself round corners, park at the tap of a button and automatically brake to avoid hitting pedestrians. But you can be certain that Vovlo isn't out to build a world where most people don't own a personal car. The easy way out would be to side with Elon Musk, and end up pushing for a system that is mostly the same as what we already have, because "it's incredibly hard to get the last few percent".

That would leave us still requiring driving tests, competent individuals behind the "wheel", car parks, and all the rest.

The alternative would be world-changing.

• How does Google's self-driving car work – and when can we drive one?