Kim Kardashian shows off her curvaceous derriere in figure hugging skirt while baring a glimpse of midriff in cropped top



Earlier in the day she hit up Barry's Bootcamp for a gruelling workout.



But by Wednesday evening, Kim Kardashian was ready to let loose and enjoy herself as she went to dinner in Beverly Hills with her pal Brittny Gastineau.



Kim made sure her famous derriere was on display in a figure-hugging red pencil skirt and cropped red top, which showed off a hint of her toned midriff.



Scroll down for video



Looking rearly good: Kim Kardashian was thrilled to be the centre of attention as she went out for dinner in a clinging red skirt and cropped red top

Kim favours the pencil skirt and cropped top style, which flatters her famous assets in all the right places.



She added nude sandals to finish her look.



As ever the outing was being filmed for her family's reality TV show Keeping Up With The Kardashians.



Brittny was more casual than her friend, wearing jeans, over the knee boots and a black top.



You looking at me? As ever, Kim grabbed attention as she enjoyed a girls' night out in Beverly Hills



Different styles: Kim and her pal Brittney Gastineau chose very different looks for their dinner date in LA



A new Kim? The star is thought to have ditched her fake tan and lashings of make-up for a more understated look ahead of her wedding to Kanye West in May



The duo were enjoying a catch up, with everyone moment captured by the reality TV cameras.

Kim is currently preparing for her May wedding to Kanye West, the father of her baby daughter North.

And while she was all dressed up on Wednesday night, the reality star appears to have undergone something of a make-under ahead of her nuptials.

Lady in red: Kim showed off her figure in a clinging red outfit - which flattered her curves



Shocking! Kim and Brittny caused chaos as they left and a valet stand was knocked over



Figure hugging: Kim's outfit showed off her fabulous post-baby figure



Where's Lisa? Kim and Brittny got in the car but there was no sign of Lisa



No men allowed: Kim and Brittny enjoyed a girls' night out in Beverly Hills on Wednesday

Wedding diet is working: Kim's slim figure was on display in her figure hugging red outfit



Mamma Mia! Brittny's mother Lisa, 54, looked years younger than her age as she left the restaurant with Kim and Brittny

Having stripped off her normal uniform of dark fake tan, false lashes and long hair extensions, a new Kim emerged on Monday.

Indeed on her night out on Wednesday, she went for less make-up than usual and couldn't have looked happier.



West was in the news this week as a source told TMZ the photographer he punched in July now wants to go to trial for a civil suit.

The criminal suit was settled on Tuesday.

Normally civil suits are settled out of court, but shutterbug Daniel Ramos feels he has such a strong case, a trial is worth it. His attorney is Gloria Allred.

All smiles: The girls seemed to be in fits of giggles as they prepared to head home after their sumptuous dinner date

Lady in red: The raven-haired beauty looked incredible in her midriff-baring scarlet ensemble



Oops-a-daisy: The girls caused quite a stir outside the eatery with the huge crowd of waiting paparazzi