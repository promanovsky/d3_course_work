Frames for Google's Glass device designed by Diane von Furstenberg are now on sale in the U.S.

The frames are currently available in a variety of sunglass tints and include a single pair of optical frames in different colors, according to TechCrunch.

All of the designs work with Glass technology, and they can be bought with one pair of the sunglasses and one pair of optical frames.

Each combination is available for $1,800 at online fashion retailer Net-A-Porter or from Google. Orders have a lead time of five to seven days, reported Apple Insider.

The frames are offered with a detachable sunglass shade and come in five different sets: shiny lagoon (teal frame with blue shade), shiny ink (charcoal frame with black shade), shiny elderberry (plum frame with bronze shade), matte java (brown frame with rose shade), and matte ice (white frame with green shade).

While Google is responsible for creating the titanium frames for Glass, Furstenberg is the first designer to put her own label on accessories for the device, TechCrunch reported. This particular line is being released during the Explorer Program, before the wearable computer is formally released.

The search giant started selling Glass through its Explorer program in the U.S. earlier this year. Prior to the launch, the company had been using its invite-based system to introduce new users to beta testing its devices.

Google's partnership with von Furstenberg is the company's first joint venture with a well-known fashion brand. The designer's involvement with the search giant's new line of Glass is believed by many to be aimed at bringing "fashion street cred" to the device, Apple Insider reported.

The Mountain View, Calif.-based company has also partnered with fashion watch maker Fossil to build Android Wear smart watches.

Google is planning to release new lines of Glass with other designers in the future, TechCrunch reported. These designers include Oakley, Ray-Ban and Luxottica.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.