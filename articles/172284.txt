PALO ALTO, CA. (TheStreet) -- I have now driven the all-electric Mercedes luxury car, and I'll attempt to compare it against its competitors, primarily the BMW i3 but also the Nissan LEAF and the Chevrolet Volt.

The electric Mercedes will start to appear in dealerships in July, and the base price is $42,375 including mandatory $925 destination charge. The gasoline and diesel-powered Mercedes B-Class has never been sold in the U.S., and there are not plans for them yet to do so.

The Mercedes all-electric is a short but tall station wagon. The length is 171.6 inches, and the width is 70.3 inches. These dimensions are important because they make this car extremely easy to park as well as maneuver in city traffic and other tight spaces.

Yet, the Mercedes has more than sufficient room for five adults at least six feet tall, and the luggage space is ample. Basically, the packaging of this car is outstanding. It is easy to get in and out, such as for a tall person or someone who has a bad back or is a bit older and stiffer.

What you really want to know is how it compares to the other major electric cars in its class. Most especially, it goes head-to-head with the electric-only version of the BMW i3, which is seeing its first US deliveries this week, having been in European consumer hands since last November 16.

Let's start with range. Mercedes expects to be EPA rated at 85 miles. My testing suggested that this is eminently achievable even during spirited driving. I wouldn't be surprised to see the range rated at 90 miles or more. As with the California-only Toyota RAV4 EV, however, you can charge it to a higher level, adding approximately 15% to capacity and range. This could push the range to just over 100 miles, perhaps 104 miles.

As of this writing, the release of the BMW i3's EPA-rated range was imminent. Most people have guessed the BMWs range somewhere around 90 miles as well.

From this perspective, given that the base prices of the BMW and Mercedes are similar, the Mercedes offers more: Up to 15% more range, plus what appears to be a higher level of standard equipment, including front-facing radar (part of a $2,500 option package on the BMW) and non-cloth seats.

When you combine this with the Mercedes fitting a 3rd person in the back seat, and the much larger luggage space, the choice seems easy. However, the story doesn't end there. The BMW offers two options that could be crucial decision factors for some buyers: 1. A $700 option, the BMW offers DC charging, which could give you almost 70 miles of range in 30 minutes. Mercedes has no such thing. 2. A $3,850 option, the BMW offers a gasoline generator with a two-gallon tank, adding close to 70 miles of extra range. This is likely to be a very popular option as it takes range anxiety off the table.

As a result, one could argue this Mercedes-BMW comparison two ways: Comparing the base cars, Mercedes is the clear value winner. However, the BMW can be optioned to deliver something where Mercedes simply doesn't compete. And either way, the Mercedes offers more interior room.

The struggle between Mercedes and BMW is one thing, but one could argue that the greater opportunity is to take sales from Nissan's best-selling EV, the LEAF. The price of a well-equipped LEAF is $35,870, which isn't that much less than the $42,375 Mercedes.

In comparison to the LEAF, the Mercedes is faster, has more interior room, is overall a more substantial car, and offers at least 15% more electric range. All of that for $6,505. Until the longer-range LEAF becomes available at some nondescript point in the future, I see the LEAF losing sales to the Mercedes.

What about the driving characteristics? The electric Mercedes offers acceleration between the faster BMW and the slightly slower Chevrolet Volt. The noise insulation is the best of these cars although the BMW feels at least just as rigid and well-built.

Thanks to its compact dimensions, the Mercedes feels very nimble on twisty roads. However, the BMW i3 is even more nimble, with extremely precise and direct steering. The Mercedes basically feels like a Volt on stilts, but with more noise insulation.

On its most aggressive setting, I found the Mercedes' regenerative braking to be slightly stronger than the Volt, but less strong and well-calibered than the BMW. A first for any car, the Mercedes offers an automatic/adaptive braking setting that applies more regen when the radar senses an object ahead. I didn't like the automatic setting in comparison to the heavy regen setting, which was more consistent in city traffic.

Mercedes provides an 8-year, 100,000 mile warranty for the battery, against more than 20% capacity loss. You basically take the car in for service once a year to get it checked.

All in all, the Mercedes B-Class electric seems very well-built, has a very sane and utilitarian body that is very easy to park, and behaves neutrally on the road. Its position in the competitive landscape is one of being better than BMW's and Chevrolet's offerings on some things, while being behind on other metrics. It is a most welcome addition to the choices.

Considering its relatively conservative styling, uncomplicated operation and neutral behavior, one could envision this all-electric Mercedes as a complement to the garages of existing loyal Mercedes owners, as a second or third car. The price is right, and the car provides lots of utility for what I call 'SSS' -- school, supermarket and soccer field. It could also be a commuter car. On an island such as Hawaii, it would be ideal.

The pure electric car market right now is suffering from public charging station availability being too unreliable, mostly because there being too may EVs fighting for too few spots. You can charge at home and at work, but other than that it has become a crapshoot. I see this problem becoming worse and worse for at least the next couple of years.

This makes the proposition for a pure EV a very difficult one who thinks rationally about not the 99% of the time when you travel a short distance, but about the inevitable 1% of the time when you forget to charge or have to travel longer on short and unexpected notice.

As a result, the larger market for plug-in cars is likely going to be hybrids, where you will still charge at home and at the office, but where you don't have to suffer from all public charging stations being busy almost all the time. Mercedes has already announced the S-class and C-class plug-in hybrids. Most likely, there will be such a version of every major Mercedes nameplate within the next three years, and for good reason.

The B-class electric is a wonderful car that will create a successful niche. It will take sales away from Nissan, and be a worthy competitor to the base version of the BMW i3.

At the time of submitting this article, the author had no position in the companies mentioned.