Do you know who can see what you are posting on Facebook, including your photos, birthday and personal cellphone number?

Chances are that you don't.

Facebook is worried that you will start sharing less - or maybe even move to more anonymous services - unless it helps you better manage your private information. On Thursday, the company announced that it would give a privacy checkup to every one of its 1.28 billion users worldwide.

Facebook, which is based in Menlo Park, California, will also change how it treats new users by initially setting their posts to be seen only by friends. Previously, those posts were accessible to anyone.

And it will explain to both current and new users that setting their privacy to "public" means that anyone can see their photos and posts.

The change in default settings and the person-by-person review is a sharp reversal for Facebook, whose privacy settings are famously complicated. Some users may be shocked when they see just how widely their personal information has been shared.

For most of its 10-year history, Facebook has pushed and sometimes forced its users to share more information more publicly, drawing fire from customers, regulators and privacy advocates across the globe. That helped make Facebook the world's largest social network and an advertising behemoth.

But the company recently concluded that its growth depended on customers' feeling more confident that they were sharing intimate details of their lives with only the right people.

"What we really want is to enable people to share what they want," Mark Zuckerberg, Facebook's co-founder and chief executive, said in a recent interview.

And more sharing means more growth and more opportunities to place ads for Facebook.

Zuckerberg has also watched the rapid growth of privacy-friendly services like WhatsApp and Snapchat and anonymous sharing apps like Secret and Whisper, which compete for the time of many Facebook users, especially the younger ones. That prompted him to strike a deal this year to buy WhatsApp for as much as $19 billion and take steps to make the Facebook social network more respectful of user privacy.

"Private communication has always been an important part of the picture, and I think it's increasingly important," he said. "Anything we can do that makes people feel more comfortable is really good."

Nearly 9 in 10 Internet users have taken steps online to remove or mask their digital footprints, according to a telephone survey of 1,002 U.S. adults in July by the Pew Research Center.

Facebook is also feeling enormous pressure from privacy regulators around the world, said Marc Rotenberg, president of the Electronic Privacy Information Center, an advocacy group. "If they don't make these changes, they're going to be sanctioned," he said.

The Federal Trade Commission found in 2011 that Facebook misled users about how widely their data was shared and the company agreed to stricter commission oversight for 20 years.

In Europe, privacy officials are reviewing the company's proposed acquisition of WhatsApp, which offers users much stricter privacy protection than Facebook does.

"They have gotten enough privacy black eyes at this point that I tend to believe that they realized they have to take care of consumers a lot better," said Pam Dixon, executive director of the World Privacy Forum, a nonprofit research and advocacy group.

Even as Facebook takes steps to empower its users on privacy, it continues to introduce features that raise new issues. On Wednesday, it announced an optional service for mobile phones that eavesdrops on the sounds in a room to try to identify any music or television shows that might be playing so that a user can share that information with friends.

However, Facebook has made several other moves recently that indicate that it is taking privacy more seriously. Last month, it began a location-sharing feature called Nearby Friends that is optional and provides only a user's general location. And it made changes to Facebook Login, a service that allows people to use their Facebook identities to log in to other sites and apps, that reduce the amount of information shared outside Facebook, and in some cases shares nothing at all.

In its announcement, Facebook said that when people signed in to post something on the company's popular social network, they would soon see a blue cartoon dinosaur that popped up with the message, "We just wanted to make sure you're sharing with the right people."

The service will then walk users through the privacy settings for their status updates, remind them of the applications that have permission to use their Facebook data, and review the privacy settings for some of the most private information on their profiles, like their hometown, employer, phone number and birth date.

"People sometimes feel that their information is more public than they want," Mike Nowak, a Facebook product manager who worked on the privacy checkup, said in an interview. "It's not fun when you share something and someone you didn't expect to be able to see it can see it."

Privacy advocates have long complained that Facebook's privacy controls, which allow people to set separate permissions for nearly every single item they put on the service, are too complicated.

And for years, Facebook has continued to push the envelope on privacy, despite those concerns.

For example, when it first allowed its users to share posts publicly in 2009, it decided that the default setting for all posts by new users would be public, which meant that anyone could see them.

Dixon said that particular setting was a sore point for her, since new users were the ones least likely to understand the implications of posting publicly.

Given Facebook's history, some people are wary about its latest moves.

"This is Potemkin privacy protection, a facade intended to assure regulators that Facebook is doing something to address widespread user concerns," Rotenberg said.

Facebook has been testing portions of the privacy checkup for months and has not yet decided how extensive it will be. The company is also trying to determine how it will work for the 341 million users who gain access to the service only through mobile devices.

Facebook is also considering broader privacy changes, like creating a dashboard that would make it easier to find and update a range of privacy settings.

"Privacy is not an all-or-nothing issue for users," said Lee Rainie, director of the Pew Research Center Internet and American Life Project. "They want to be able to adjust the dials."

© 2014 New York Times News Service

