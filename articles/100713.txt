next Image 1 of 2

prev Image 2 of 2

A "challenging" outbreak of the deadly Ebola virus in West Africa is expected to take from two to four months to contain, the World Health Organisation (WHO) said on Tuesday.

The virus is still spreading in three "hotspots" of Guinea Forestiere, a southeastern region some 900 km (560 miles) from Guinea's capital of Conakry, a city which has itself reported 20 cases to date, the United Nations agency said.

But the WHO said it was not recommending any travel restrictions for Guinea, which has a total of 157 suspected and confirmed cases including 101 deaths, or for Liberia, which has 21 suspected and confirmed cases, including 10 deaths.

"We fully expect to be engaged in this outbreak for the next two to three to four months before we are comfortable that we are through it," Dr. Keija Fukuda, WHO assistant director-general, told a Geneva news briefing.

"This is one of the most challenging outbreaks of Ebola we have ever faced," he said.

It is also the first to strike West Africa, where some people have become infected from burial practices that involve direct contact with body fluids of the deceased.

WHO medical officer Stephane Hugonnet, just back from Guinea, said: "In Guinea Forestiere, the outbreak is not over, this is the epicentre. As long as it is not over there, there will be cases exported to the rest of the country."

Some 50 foreign experts have been deployed to help with infection control measures at hospitals and to trace 600 known contacts of infected patients, he said.

Most new cases were linked to known transmission chains, which he said was "reassuring".