An oil and gas drilling platform stands offshore as waves churned from Tropical Storm Karen come ashore in Dauphin Island, Alabama Thomson Reuters

WASHINGTON (Reuters) - Oil producers considering swapping U.S. light crude abroad for the heavier imported oil needed by refiners to work around a decades-old ban on exporting domestic crude may find the strategy harder to execute than it looks on paper.

As U.S. production of light crude oil continues to boom, some companies and lawmakers are calling for the United States to reform its decades-old ban on most U.S. crude oil exports - a policy that followed the Arab oil embargo of the 1970s.

A breakthrough arguably came this week, when U.S. officials clarified that a type of ultra-light crude known as condensate could be exported after enough processing to qualify as a refined product, exports of which are allowed. Swaps would be another way to test the ban's limits.

In theory it should take just weeks for Washington to allow oil producers to execute a deal, since these swaps are allowed by law. But analysts say meeting the base requirement - that the imports be of the same quantity and quality as the exports - is easier said than done.

The current law does not clearly define quality - for example, whether a heavier crude such as the kind Mexico produces is of higher quality because it is compatible with U.S. refining capacity, or lower quality because of its density.

"Ensuring that crude swapped in is of the same quantity and quality as crude swapped out, which is a loose paraphrase of one of the regulation’s many stipulations, may be non-trivial," said Kevin Book, policy analyst at ClearView Energy Partners.

The Commerce Department's Bureau of Industry and Security, which oversees exports, has received at least one application for a permit to export crude through a swap deal, Reuters has reported.

Earlier this month, Continental Resources confirmed it has applied for a license for a swap "to further demonstrate the need for a free market for crude." The largest leaseholder in the booming North Dakota Bakken region did not disclose with which country it intends to swap.

Current law says U.S. oil can be exchanged in similar quantity "with persons or the government of an adjacent foreign state" or temporarily exported across parts of an adjacent country, and then reentered into the United States.

Adjacent countries could include Mexico and Panama, according to a BIS official.

U.S. regulations allow a swap of oil exports for imports only if the home-grown product cannot be marketed domestically for “compelling” economic reasons.

Analysts said that if a company makes a strong enough case about the negative economic impacts of excess crude oil production within the United States, approval could be relatively fast.

“If someone put in the right application that said ‘I’ve got a distressed crude oil, it’s a widget that no one needs and its backing up on production’…I would bet they would get an export license,” said Frank Verrastro, chair for energy and geopolitics at the Center for Strategic and International Studies.

Export backers said dislocations in supply will increase in the coming months and years as production from the Bakken and other plays, ill-suited to current refining needs, continues to rise.

North Dakota this month passed the one-million-barrel a day mark in crude production, the state's petroleum council said on June 17. In 2000, the state produced less than 100,000 barrels of oil per day.

SWEET AND SOUR

U.S. Senator Lisa Murkowski of Alaska, the top Republican on the Senate Energy Committee, has pushed for ending the export ban, citing a range of benefits that would flow to the United States.

Last month Murkowski issued a staff report saying swaps of light sweet crude to nearby countries would be one way to shrink a glut of that type of oil within U.S. borders.

"Exchanges cannot solve the mismatch between refineries geared to process heavy crudes and record production of lighter grades of petroleum, but they would be a partial measure," the report said.

U.S. Energy Information Administration chief Adam Sieminski has said that the domestic excess of light sweet oil and Mexico's excess of heavy sour oil offered an opportunity.

Sieminski said in May that the EIA has no immediate plans to study the option, but that companies and refining consultants should explore whether it makes economic sense.

"Mexico, I thought, was an easy one because they are right next door, but other countries in Latin America may also be suitable candidates – Venezuela would be one," Sieminski said.

Still, not all experts see swaps as a simple way to get around the export ban.

ClearView's Book said that for various reasons there is probably only a narrow range of price spreads between U.S. and Mexican crude that could make a swap worth doing.

And Amrita Sen, chief oil market analyst at Energy Aspects, said it is "cumbersome" to prove that the U.S. economy will be better off with swaps and that producers would not be able to sell the light crude domestically.

Physical limitations could also make it difficult for U.S. companies to justify swaps with some of the most likely prospects, including Mexico and South Korea, she said.

(Reporting by Valerie Volcovici; additional reporting by David Alire Garcia in Mexico City and Edward McAllister in New York; editing by Ros Krasny, Jonathan Leff and Cynthia Osterman)