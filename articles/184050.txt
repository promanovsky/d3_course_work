For women in the United States, the lifetime risk of dying from a pregnancy-related cause has increased 50 percent over the last 15 years, according to a new report.

Women who are 15 years old in 2014 face a lifetime risk of dying of 1 in 2,400, up from 1 in 3,700 for women who were 15 in the year 2000, according to the report from the charity organization Save the Children. That means that in 2014, a woman in America has the same lifetime risk of pregnancy-related death as a woman in Iran or Romania, the report said.

The reason for the increase in the U.S. and other developed countries is not known, the researchers said. But it could be due a rise in the number of women with high-risk pregnancies, such as pregnant women with diabetes, heart disease or obesity; as well as an increase in women carrying twins and other multiples (related to treatment with fertility drugs), and more women having children at older ages, which can increase the risk of pregnancy and childbirth complications.

"We have a lot more moms who are getting pregnant that have other medical conditions that can cause an increased risk of maternal morbidity," said Dr. Melissa Goist, an obstetrician and gynecologist at The Ohio State University Wexner Medical Center, who was not involved in the report.

The U.S. fares worse than most other developed countries in terms of women's lifetime risk of pregnancy-related death it ranks 46 out of 178 countries in the world. A woman in the United States is 10 times as likely to die from a pregnancy-related cause in her lifetime as a woman in Estonia, Greece or Singapore, the report said. [9 Uncommon Conditions That Pregnancy May Bring]

"Even developed countries really need to prioritize women and children to a greater degree," said Carolyn Miles, president and CEO of Save the Children. "And the U.S. right now, I would say is really not doing that," she said.

Several other developed countries have also seen an increase in maternal death risk in recent years. Since 2000, the lifetime risk of pregnancy-related death increased 19 percent in Switzerland, 23 percent in Spain, 27 percent in New Zealand, and 51 percent in Denmark. Still, the report notes that these countries have some of the lowest risks of maternal death in the world.

More accurate counts of maternal deaths may also play a role in the increases, the report said.

To better prioritize women's health in the United States, there should be an increased focus on risk factors such as obesity and diabetes, as well as efforts to make sure that every mother has prenatal care, Miles said.

Goist said that although women often put their child's needs ahead of their own, women need to take good care of themselves to be able to care for their children, as well as set a good example.

"We probably take the worst care of ourselves," Goist said. "We need to put our health and our wellness ahead of some of the other things we do."

The new report, released each year by Save the Children, ranks the best and worst places in the world to be a mother. The rankings, called the "Mother's Index," are based on five factors: A woman's lifetime risk of pregnancy-related death, the rate of death in children under age 5, a woman's expected years of formal schooling, the countries' gross national income per person, and the participation of women in government.

This year, the United States ranked 31 out of 178 countries in the Mother's Index, just behind South Korea and Poland. The top-ranking countries were Finland, Norway and Sweden, and the bottom-ranking countries were Niger, Democratic Republic of the Congo and Somalia. Most countries that have ranked in the bottom 10 have a recent history of armed conflict and persistent natural disasters, the report said.

Women and children are 14 times more likely to die in a disaster than men, the report said. Emergency response programs should include components that address the specific needs of women and newborns, such as clean delivery supplies for women who cannot give birth in a health facility, and mother-baby friendly centers for breast-feeding in emergencies, the report said. Even in the United States, 28 states do not meet the minimum standards for disaster planning in schools and child care, which include plans on how to reunite families if disaster strikes, the report said.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.