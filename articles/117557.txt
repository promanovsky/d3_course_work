Google has officially changed its Terms of Service to make it quite clear that users are consenting to the search giant scanning the content of their emails in order to allow the company deliver more targeted ads and better search results.

While it may now be saying it more openly, this is not a new practice.

Google has been scanning users' messages for many years, and the company had believed users "explicitly consented" to the practice by agreeing to various versions of the company's terms of service since 2008.

In a ruling last month, Judge Lucy Koh decided against giving class action status to a lawsuit brought by a number of claimants who believed Google had violated state and federal laws regarding the privacy of millions of its users.

Judge Koh said last month that Google's terms of service and privacy polices did not explicitly notify the plaintiffs "that Google would intercept users' emails for the purposes of creating user profiles or providing targeted advertising."

Probably as a result of those comments, Google has now decided to update its terms of service to make its actions very clear. It has added the following paragraph to the new terms which came into effect on Monday:

"Our automated systems analyse your content (including emails) to provide you personally relevant product features, such as customised search results, tailored advertising, and spam and malware detection. This analysis occurs as the content is sent, received, and when it is stored." Why advertise with us

Speaking about the changes, Google said in a statement: "We want our policies to be simple and easy for users to understand. These changes will give people even greater clarity and are based on feedback we've received over the last few months."