Actor Channing Tatum with his wife Jenna Dewan pose with his Trailblazer award backstage at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Actor Aaron Taylor-Johnson poses with his wife Sam Tyler-Johnson backstage during the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Aaron Taylor-Johnson (R) presents the award for Best Fight to Orlando Bloom at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

REFILE - CORRECTING SPELLING OF TAYLOR Actor Aaron Taylor-Johnson poses with his wife Sam Taylor-Johnson backstage during the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Actor Mark Wahlberg and his wife Rhea Durham pose backstage with his Generation Award at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Actor Mark Wahlberg and his wife Rhea Durham pose backstage with his Generation Award at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Jerry Ferrara, Adrian Grenier and Kevin Dillon (L-R) present the Generation Award at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Leslie Mann, Cameron Diaz, Nicki Minaj, and Kate Upton (L-R) present the award for Best Male Performance at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Josh Hutcherson accepts the award for Best Male Performance for "The Hunger Games: Catching Fire" at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Zac Efron (L), Seth Rogen (C) and Dave Franco present the award for Best Kiss at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Actor Channing Tatum poses with his Trailblazer award backstage at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Danny Moloshok

Eminem and Rihanna perform on stage at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Orlando Bloom introduces a performance by Eminem and Rihanna at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Zac Efron accepts the award for best shirtless performance for "That Awkward Moment" as his shirt is ripped open by presenter Rita Ora at the 2014 MTV Movie Awards in Los Angeles, California April 13, 2014. REUTERS/Lucy Nicholson

Actress Mila Kunis speaks onstage at the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Frederick M. Brown/Getty Images)

Actress Mila Kunis poses with the Best Villain award for 'Oz The Great and Powerful' in the press room during the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Jason Merritt/Getty Images for MTV)

LOS ANGELES, CA - APRIL 13: the Best Villain award for 'Oz The Great and Powerful' in the press room during the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Jason Merritt/Getty Images for MTV)

Actress Mila Kunis poses with the Best Villain award for 'Oz The Great and Powerful' in the press room during the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Jason Merritt/Getty Images for MTV)

Mila Kunis was thrilled to win Best Villain at the MTV Movie Awards on Sunday night.

The actress was nominated for her role as Theodora, the Wicked Witch Of the West, in Disney's Oz the Great and Powerful.

The 30-year-old was up against Barkhad Abdi for Captain Phillips, Benedict Cumberbatch for Star Trek Into Darkness, Michael Fassbender for 12 Years a Slave and Donald Sutherland for The Hunger Games: Catching Fire.

Although Mila skipped the red carpet, she was ready to accept her award when her name was called, reports E! News.

"Listen you guys. You just made my 12-year-old self's dream come true," Mila told the audience. "This is by the far the coolest award and I just realized I was the only woman nominated, and I won. So thank you guys."

The Jupiter Ascending star wore a little black dress that perfectly accentuated her growing baby bump.

Expand Close Actress Mila Kunis speaks onstage at the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Frederick M. Brown/Getty Images) / Facebook

Twitter

Email

Whatsapp Actress Mila Kunis speaks onstage at the 2014 MTV Movie Awards at Nokia Theatre L.A. Live on April 13, 2014 in Los Angeles, California. (Photo by Frederick M. Brown/Getty Images)

Mila Kunis showed off her baby bump in black

Mila is said to be expecting her first child with fiancé Ashton Kutcher, although neither party has publicly commented on either the engagement or baby yet.

The 30-year-old has previously commented on how important family is to her, and the kind of mother she wants to be.

"I want to be a present mom. When I was growing up both my mom and dad worked full-time. I guess the only thing I can say about that is they worked full-time in one location,” she told Playboy last year. “I'm never in the same place for more than two months. How am I ever going to have a family like that? You have to make compromises. If that means I do one movie a year, if people still want to see me and hire me and I don't suck by that point, great. But my only source of happiness can't be dependent on something so fickle. And I find this industry to be incredibly fickle."

Read More

Irish Independent