Today is the day that Ubisoft’s long awaited Watch Dogs is launched, and we’re sure that many gamers are eager to start playing the game. Unfortunately it seems that perhaps due to the influx of gamers and Ubisoft failing to anticipate the rush, many players have reported server errors that would not allow them to play the game.

Advertising

The issue has ballooned to over 30 pages on the Steam Community website where gamers have complained that they are being met with a “Uplay has detected an unrecoverable error” message. Ubisoft has since acknowledged the issue on Twitter, where they wrote, “We are experiencing issues with the authentication services. Players may experience long delays when trying to login in-game. More to come.”

They then followed up with more tweets several hours later, claiming that they are still working on their server authentication issues, and that they are doing everything that they can. The latest tweet by Ubisoft states that things appear to be working, but might require several tries by users in order to log in.

Several players in the forums have reported that they have managed to get into the game, while others are still having a hard time. Teething problems for new games are to be expected, especially with the influx of players that developers would have a hard time preparing for, since beta releases are usually more limited. In any case hopefully Ubisoft will resolve this in a timely manner, but have any of our readers been affected by this?

Filed in . Read more about Ubisoft and Watch Dogs.