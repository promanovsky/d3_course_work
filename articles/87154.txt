Gallup and Healthways surveyed people about their health in 189 metropolitan areas across the country, asking them questions about subjects like their diets, exercise habits, and whether or not they were obese.

We mapped the most obese cities in the U.S. using this data.

Advertisement

Now, here are the least obese cities. Each of these cities has fewer than one in five of its residents suffering from obesity.

The cities of Colorado and California have very low obesity rates. Boulder has an impressive rate of 12.4%, putting it in a class on its own.

Advertisement

Gallup's full survey results can be found on their website