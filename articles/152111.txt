Jan 31: “I have been diagnosed for awhile now but today is the day that it really became public.”

Feb. 16: “#mindysarmy isn’t just about me. It’s about anyone that is fighting with an illness or cancer or a hard time so try to remember that as you look down and see your wrist. Stay positive and always try to do the next right thing!”

Feb. 24: “Sleep eludes me yet again. I want to thank everyone for the kind words and the Chillicothe Gazette for making my story public to help others in the same boat.”