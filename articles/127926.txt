Corporate earnings pushed U.S. stocks mostly higher on Thursday, but it wasn't an easy ride up.

The stock market appeared set in the morning for its fourth consecutive positive open, but immediately turned negative as investors sold shares in Google (GOOG) and IBM (IBM). Those two market heavyweights reported disappointing earnings late Wednesday. Earnings from toy maker Mattel (MAT) and insurer UnitedHealth (UNH) also dragged down stocks.

But by midmorning, the market started to push higher as traders cheered upbeat results from Morgan Stanley (MS), General Electric (GE) and PepsiCo (PEP).

"We were expecting this earnings season to be pretty volatile, and it's proven to be true so far, in that we're seeing some differences in the results," said Paul Mangus, head of equity research and strategy for Wells Fargo Private Bank.

The Standard & Poor's 500 index rose two points, or 0.1 percent, to close at 1,864.85. Seven of the 10 industry sectors in the S&P 500 gained, led by energy stocks. The Nasdaq added nine points, or 0.2 percent, to finish at 4,095.52. The Dow Jones industrial average, however, fell 16 points, or 0.1 percent, to close at 16,408.54, hurt by the big drop in IBM.

U.S. stock markets will be closed in observance of Good Friday.

Bond prices fell, pushing up the yield on the 10-year Treasury note to 2.72 percent from 2.63 percent late Wednesday.

After selling off Internet and biotechnology companies last week on concerns the stocks were overvalued, investors turned their attention this week to how companies' businesses are performing. Investors have lowered their expectations for earnings following severe cold in much of the country this winter. That harsh weather weighed on everything from auto and home sales to hiring.

Investors are now eager to hear what CEOs have to say about business prospects going ahead.

Thursday's trading reflected buying and selling on earnings news, rather than a broader market theme taking hold, Mangus said.

"Going into this quarter, expectations are low, so if you disappoint on low expectations you're likely to be penalized," he said. "However, they also present the opportunity for some significant beats because the estimates are that low."

Among companies whose earnings pleasantly surprised investors was General Electric, which described the economic situation as "positive" and said its industrial division was doing well. Another positive signal came from PepsiCo, which reported a higher profit after slashing costs and selling more snacks.

GE gained 44 cents, or 1.7 percent, to close at $26.56, while PepsiCo added 78 cents, or about 1 percent, to finish at $85.55.

IBM, meanwhile, struggled with a decline in its hardware business in the latest quarter. Its stock slid $6.39, or 3.3 percent, to $190.01.

UnitedHealth Group said its income slid 8 percent in the first quarter as fees and funding cuts from the health care overhaul dented its performance. UnitedHealth fell $2.41, or 3.1 percent, to $75.78.

Despite the big-name decliners, the latest wave of quarterly results has been mostly positive, said John Fox, director of research at Fenimore Asset Management.

"The overall read across five or 10 or 15 earnings reports is positive," Fox said, noting that many companies have reaffirmed their earnings forecasts for the year. "The fundamental underpinnings are good, and I'm not hearing anything from management that changes that."

Investors will study more company earnings over the next couple of weeks as they try to determine whether the effects of the severe winter are easing.

Homebuilders, automakers and consumer discretionary companies should provide a better read on whether consumer demand has rebounded from the deep chill.

"The companies that would be most impacted by that have yet to report," Mangus said.