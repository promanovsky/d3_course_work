HTC One M8 has been put through a series of battery-life benchmarks by GSMArena to deduce its overall endurance capacity, which is on par with the Samsung's flagship phablet, the Galaxy Note 3.

HTC One M8 comes with a powerful 2600mAh battery which is 13% larger than its predecessor (HTC One) which ships with 2300mAh battery.

The new HTC One also incorporates the quintessential Extreme Power Saving Mode to extend the phone's battery life when running low on battery-charge.

However, for this battery-life test, folks at GSMArena have disabled power saving feature in order to replicate daily usage statistics without restricting the phone's features and performance.

GSMArena has tabulated the battery-life benchmark results for HTC One M8 by conducting a series of tests including Talk Time, Web Browsing and Video Playback. The overall benchmark report suggests mixed results for the handset, but also reveals excellent endurance rating in the overall result.

Talk Time

According to the benchmark report, the HTC One M8 amasses an impressive 20 hours of talk time on 3G network and thereby bettering its predecessor as well as beating the Galaxy S4 (with 2600 mAh battery) by a couple of hours.

Web-Browsing

In contrast, the M8 falls behind in the web browsing test by an hour against the original HTC One, which fared on par with the iPhone 5s. However, the HTC One M8 manages to beat the Galaxy S4 by a couple of hours while matching the scores with Galaxy Note 3.

Video Playback

Despite its larger screen, the HTC One M8 quite surprisingly outperforms the older HTC One by just over an hour in the video playback test. The reason for this is attributed to a more efficient video decoder on the former.

Overall Endurance Rating

With an impressive endurance rating of 71 hours, the HTC One M8 compensates for its below par standby time performance given its bigger battery.

On the contrary, the M8 stands out against its predecessor whose endurance rating of 48 hours failed to make an impression.

Furthermore, it is reported that HTC One M8 has the potential to last 60 more hours with last 20% battery charge or 30 more hours with 10% battery charge when Extreme Power Saving Mode is enabled.

Other key point of interest is that the HTC One M8 is rated to support Quick Charge 2.0 with the ability to charge the battery 75% faster.

On the downside, the battery charger shipped with the handset supports only Quick Charge 1.0 with just 40% faster charging capability than normal charging.

[Source: GSMArena]