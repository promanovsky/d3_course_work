From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

European owners of Nokia’s Windows RT tablet are in for a shock.

Nokia announced today that it’s suspending sales of the Nokia Lumia 2520 tablet in the U.K., Denmark, Germany, Finland, and other European countries after discovering an issue with its charger.

Apparently, the “AC-300” charger’s plastic casing can come loose, which exposes its internal components and leaves users at risk of electrical shock. The issue also affects Nokia’s Lumia 2520 travel charger, which is sold in the U.S. and several European countries.

Nokia recommends that affected customers stop using the chargers, and it’s also halting sales of the travel charger in the U.S.

Jo Harlow, Nokia’s EVP of Smart Devices, said in a statement that there have been no reported incidents from the charger issue. Nokia has also set up a special site to keep Lumia 2520 owners informed on updates around the issue.

Nokia says 30,000 chargers are affected, including 600 travel chargers sold in the U.S. (numbers that make it clear the Lumia 2520 didn’t sell very well).

The Lumia 2520 was Nokia’s first attempt at building a Windows RT tablet, but it seemed superfluous when compared to Microsoft’s much-improved Surface 2.