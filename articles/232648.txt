Angelina Jolie Reveals New Movie With Brad Pitt is 'Very Experimental' (VIDEO) Angelina Jolie Reveals New Movie With Brad Pitt is 'Very Experimental' (VIDEO)

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Angelina Jolie recently opened up about her upcoming movie with Brad Pitt and the actress revealed that the project will be "very experimental."

The Hollywood power couple are joining forces for a new project reportedly based on a script written by Jolie, according to People magazine.

Sources claim that the "exciting new venture" will be "set in Europe" and Jolie, 38, revealed details about the project.

"It's not a big movie, it's not an action movie," Jolie told Extra.

"It's the kind of movie we love, but aren't often cast in," she continued. "It's a very experimental, independent-type film where we get to be actors together and be really raw, open, try things."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

The couple's last film together, "Mr. & Mrs. Smith," raked in an estimated $478 million at the box office and critics anticipate that a new film would likely also do well based on Pitt and Jolie's powerful name recognition alone.

In recent months both Pitt, 50, and the Academy award-winning actress have been busy working on their respective World War II-era films. Pitt spent most of the winter in London filming "Fury" while his fiancee spent several months directing "Unbroken" in Australia which is currently in post-production.

Despite the long distance, the couple make it a priority to make time for one another in a bid to strengthen their bond. Pitt began dating Jolie in 2005 and the couple announced their engagement in 2012. They share six children Maddox, 12, Pax, 10, Zahara, 9, Shiloh, 7, and twins Knox and Vivienne, 5.

"He's my family. He's not just a lover and partner, which is wonderful, but he's my family now. We have history, we work hard to make it great, we don't kind of relax about it and take each other for granted," she explained,

"Like everybody, we have our challenges,"she said. "But we're fighting to make it great."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit