With AMC's "Mad Men" coming to an end, all eyes are on Don Draper actor Jon Hamm who has become more than just a pretty face for the hit series.

Hamm wasn't always a stud, however. In fact, one of his lowest moments was immortalized on television. Fans may like to laugh at Hamm's 1995 appearance on "The Big Date" dating show where he tried to win over a gal named Mary with "fabulous food, fabulous conversation, and a fabulous foot massage for an evening of total fabulosity." Back then, however, it wasn't a laughing matter for Hamm.

"I was actually at that time working as a set dresser for Cinemax soft-core-porn movies," Hamm reveals in the June issue of Vanity Fair. "It was soul-crushing."

Now, Hamm has a much more reputable role.

"He's the first, outside the writers' room," Matthew Weiner, "Mad Men" creator, said. "I try stuff out on him. When we get stuck in the writers' room, I go down there and have a conversation with him ... The relationship would be bullshit if we didn't have disagreements."

Indeed Weiner values Hamm's worth; he told NPR that he sometimes wakes up "in the middle of the night and I think, 'Oh my God, what if I didn't cast him?' You know? Well, I wouldn't have a show."

"It's become part of the legend of the show that he had to audition seven times, and that was not my doing," Weiner explained. "He was an unknown person and [the network] required some convincing to rest this multimillion-dollar property on an unknown person."

What "Mad Men" fans really care about is the future of Don Draper and the rest of the "Mad Men" crew. Season 7 being split into two parts has a lot of influence on how the final story will be told.

"It allowed for less digression, quite honestly," Weiner shared. "... When you're doing 13 episodes you can investigate every corner of the story if you want to. You can follow anybody home. This has made us really concentrate on the main characters -- that was one of the byproducts of it ... I'm writing Episode 12 right now. ... I actually haven't written the last two."

---

Follow Scharon Harding on Twitter: @SH____4