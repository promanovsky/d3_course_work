After Apple announced support for third-party keyboards on iOS 8 at WWDC 2014 on Monday, popular keyboard makers Swype, Swiftkey and Fleksy have jumped the gun to announce that their keyboards should be available on the operating system soon.

Swiftkey and Fleksy have made announcements on their websites and are providing beta access to early subscribers. On the other hand, the popular Android-only keyboard Swype was shown as a demo at the WWDC conference. Talking to Geekwire via email, Mike McSherry, the former Swype CEO said, "Excited to see it finally happen and I'm sure this will instantly capture millions of app downloads".

(Also see: 10 New iOS 8 Features Showcased at WWDC)



Apple hasn't allowed developers to access keyboard APIs in the past and the company believed that this would help in maintaining the best customer experience. As a result extremely popular keyboard replacements never made their way into the iOS ecosystem. There is a Swiftkey app that is currently available on iTunes which is basically a note-taking app and not as powerful as the standalone keyboard. Swiftkey basically scans through the user's social networking accounts and understands typing patterns to predict the next word. It is rather intuitive. Apple's new QuickType keyboard works almost similarly. Swiftkey has no affiliation with the engineering of Apple's new keyboard.

Swype, as the name suggests, uses a swipe mechanism which lets users run through the alphabets on a Qwerty keyboard to create a word. It also does prediction. Fleksy, which is already available on the app store and compatible only with 50 apps at the moment, features a patent pending autocorrect system that claims to work even when the user is not looking at the screen.

(Also see: 10 Big Changes in OS X Yosemite)

At the 25th anniversary edition WWDC, Apple unveiled a new version of OS X, called Yosemite, as well as the latest iteration of its mobile OS, iOS 8.