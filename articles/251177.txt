Modern Family star Sofia Vergara has called off her engagement to Nick Loeb after almost two years.

Taking to her WhoSay page the actress said, "Not that anyone should care, but in order to not give the press the chance to invent crazy and hurtful drama, I prefer to tell my fans personally that Nick and I have decided to be apart."

She continued, "We have been having too many problems with having to figure out how to spend time together and because of my work and now his, it's been getting worse and worse, not fun anymore. We are still very close but we believe is the best thing for us right now."