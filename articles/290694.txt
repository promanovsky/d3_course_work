Gazprom chief Alexei Miller said Ukraine's "unconstructive" attempts to blackmail the Russian company led to the breakdown in negotiations between the parties.

Thanks to the unconstructive position of the Ukrainian government, today a prepayment regime was introduced," Miller told Prime Minister Dmitry Medvedev in front of assembled journalists. "The debt we were asking for was less than that which was required for the time period ... but the Ukrainian side adopted a position that can only be called blackmail ... They wanted an ultra-low price."

Miller said that Ukraine's leaders threatened to siphon off Russian gas bound for Europe, if Moscow did not accept the price that Kiev wanted to pay for gas.

"Ukraine's leadership and the prime minister said that Gazprom and Russia must grant super-low gas prices for Ukraine, nearly the same as for members of the Customs Union [of Russia, Belarus and Kazakhstan], otherwise... Ukraine will not pay off its gas debt, will take our gas for free in the amount it needs," he said.

Russia's state-owned energy giant Gazprom halted supplies to Ukraine on Monday morning after Kiev and Moscow failed to reach a compromise in their gas price dispute ahead of a 0600GMT deadline.

Ukraine has been locked in a gas price dispute since Russia almost doubled the price it charges Ukraine for gas in the wake of its annexation of Crimea.

Ukraine currently pays for a set amount of gas, regardless of the volume it actually uses, at a rate of $485 per 1,000 cubic metres. It is the highest price paid by any of Gazprom's European customers and Kiev is seeking to bring the price closer to market levels.

Russia had offered to discount the price by $100 but Ukraine rejected the offer, saying it would have given Russia the chance to withdraw the discount at will.

Meanwhile, the European Union's Energy Commissioner Guenther Oettinger, who has brokered weeks of three-way talks, called on Russia to return to the negotiating table.

"My appeal to the Russian partners (is to consider) whether the proposed compromise would not be acceptable for them with some adjustments," he told a news conference.

"Further invitations for trilateral talks in June are foreseen."