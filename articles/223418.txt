Earlier this year, two global leaders in HIV prevention posed a provocative question: "Is an HIV vaccine necessary?" With so many other advances in HIV prevention occurring in the last few years, must we really keep heading down the long and difficult road to an effective vaccine?

The question was raised by Anthony Fauci, head of the US National Institutes for Health's AIDS research programs, and his colleague Hilary Marston. Their own answer - a resolute YES - is one most AIDS advocates and researchers would strongly agree with. Although recent breakthroughs, from voluntary medical male circumcision to oral pre-exposure prophylaxis and HIV "treatment as prevention," will help dramatically reduce HIV infections worldwide, it's clear they won't be enough to achieve, and sustain, an end to the AIDS epidemic. A vaccine remains a critical part of the solution.

In fact, the real question with AIDS vaccine research is not if, or even when, we'll have an effective vaccine. It's how to achieve it as quickly and efficiently as possible. With more promising research avenues open to us than ever before, this is a question we need to answer.

The AIDS vaccine research field is in a period of unprecedented momentum. Since 2009, we've had clear evidence from a clinical trial that a vaccine can actually reduce the risk of HIV infection. While the level of protection in that trial was modest, researchers are testing potentially improved versions of the same approach, known as a pox-protein prime-boost vaccine. Major clinical trials are being planned in South Africa and Thailand to provide a definitive answer on the efficacy of this pox-protein strategy.

But what's also exciting today is the diversity of strategies being pursued. As the field commemorates the annual HIV Vaccine Awareness Day this May 18th, we at AVAC released two new infographics to help tell the story.

AIDS Vaccines by the Numbers provides key numbers of trials, funding, discoveries and timelines, representing where the field is today and why we need to keep moving towards an effective vaccine.

And AIDS Vaccine Research Overview is an interactive graphic highlighting the current state of the field. Each research area is expanded with key detail about the current focus and advancements in science and critical issues that advocates should be tracking.

Many of the new, novel vaccine approaches remain primarily in the lab and in early trials. But this is the scientific way, where new ideas are tested and the most promising ones advanced for larger studies that will, eventually, lead to proven vaccines for use around the world.

The challenge is to maintain momentum for the time it takes to get a vaccine from the lab, through clinical trials, and into the field. We know this will be a long process, and HIV has proven to be an especially difficult target. Keeping the pace will require both sustained investment and a concrete plan to keep research on track.

Today the lion's share of funding for HIV vaccine research comes from the US government, largely through the National Institutes of Health. Recent US government budget cuts are beginning to jeopardize all aspects of health research, including this vital area. It is imperative that our nation honor its long-standing commitment to developing and delivering the tools that can help end the AIDS epidemic, both here in the US and globally. Meanwhile, additional funders need to step up, especially in the years ahead as we move into larger and more expensive clinical trials.

The research community also has a critical role to play, beyond simply pursuing individual vaccine candidates and studies. There is an urgent need for clear, prioritized plans for managing the current and emerging pipeline of vaccine candidates, so that we can prevent duplication and move the most promising strategies along as quickly as possible.

Finally, we need global, national and local leadership. At the global and national level, political and scientific leaders need to keep pressing the case for investments in AIDS vaccine research, even in the face of budget pressures and even as other prevention and treatment measures begin to reduce the rate of new HIV infections. At the local level, the prospects for many future vaccine trials will depend on community engagement and support from local leaders and advocates.

The path to an AIDS vaccine has always included many twists and turns over the years. While it remains unpredictable today, its destination is more visible than ever. If we can sustain our focus and our resolve, we will get there.

Check out AVAC's HVAD 2014 Advocacy Toolkit that includes a new set of infographics on the current state of the field, a set of fact sheets and other tools to help explain vaccine science in simple language.