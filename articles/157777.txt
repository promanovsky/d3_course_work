Meg Ryan has joined the cast of CBS' comedy pilot "How I Met Your Dad," TVGuide.com has confirmed.

The "Sleepless in Seattle" actress will play "Future Sally," the unseen narrator and older version of Greta Gerwig's character in the "How I Met Your Mother" spin-off pilot. The part corresponds to Bob Saget's role on the original series, where Saget provided the voice of Josh Radnor's Future Ted.

Get the scoop on 39 must-watch finales

The pilot, which has yet to receive a series order, follows Sally's search for the father of her children.