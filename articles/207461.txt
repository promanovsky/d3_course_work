First came An Inconvenient Truth. Then Fast Food Nation. Then Blackfish. Each showed the power of critically acclaimed, successful documentaries to alter perceptions about controversial issues ranging from global warming to mistreatment of animals in captivity and the behaviour of food industry giants.

Now comes Fed Up, a film that looks at the global problem of surging human obesity rates and obesity-related diseases. The film, produced by Laurie David, former wife of Seinfeld creator Larry David, and narrated by TV journalist Katie Couric, seeks to challenge decades of misconception and food industry-sponsored misinformation about diet and exercise, good and bad calories, fat genes and lifestyle. When it comes to obesity, fat may not be our friend but it's not the enemy that sugar is, says the film's scientific consultant Robert Lustig, a neuroendocrinologist, author and president of the Institute for Responsible Nutrition. It is a view that is gathering support from doctors.

A US government study recently found that 17% of children and young people aged between two and 19 are considered obese. Another predicted that today's American children will lead shorter lives than their parents. Laurie David, who made the climate change film An Inconvenient Truth, calls that statistic "sobering and tragic".

According to Lustig, however, neither obesity nor fat is the issue. "The food industry wants you to focus on three falsehoods that keep it from facing issues of culpability. One, it's about obesity. Two, a calorie is a calorie. Three, it's about personal responsibility."

If obesity was the issue, metabolic illnesses that typically show up in the obese would not be showing up at rates found in the normal-weight population. More than half the populations of the US and UK are experiencing effects normally associated with obesity. If more than half the population has problems, it can't be a behaviour issue. It must be an exposure problem. And that exposure is to sugar."

The film claims that fast-food chains and the makers of processed foods have added more sugar to "low fat" foods to make them more palatable.

The sugar surge adds up to a problem not only for low-income groups that are often associated with diet-related health issues, but for all levels of society, say the film-makers. The film says big business is poisoning us with food marketed under the guise of health benefits.

Early-onset type 2 diabetes, a condition associated with exposure to cane sugar and corn syrup, was virtually unknown a few years ago. If current rates continue, one in three Americans will have type 2 diabetes by 2050. "Obesity costs very little and is not dangerous in and of itself," says Lustig, who works with the UK's Action on Sugar campaign. "But diabetes costs a whole lot in terms of social evolution, decreased productivity, medical and pharmaceutical costs, and death."

But while the fight against obesity is championed by first lady Michelle Obama, efforts to curb the sugar industry have largely failed. In 2003 the Bush administration threatened to withhold US funding to the World Health Organisation if it published nutritional guidelines advocating that no more than 10% of calories in a daily diet should come from sugar. Moreover, Washington has sweetened the profits of the manufacturers of corn-based sweeteners by awarding billions of dollars in trade subsidies.

The film-makers say it is not in the interest of food, beverage or pharmaceutical companies to reduce sugar content. "It's too profitable," says Lustig. The pharmaceutical industry talks of diabetes treatment, not prevention. "The food industry makes a disease and the pharmaceutical industry treats it. They make out like bandits while the rest of us are being taken to the cleaners."

Lustig says laws are needed. The model for regulation is alcohol since alcohol metabolises as sugar and produces many of the same chronic diseases while fat metabolises differently.

But Lustig believes that education or government guidelines alone are inadequate to address substance abuse problems. "What's necessary is to limit availability to reduce consumption and reduce alcohol-related health problems," he says.

Proposals include putting health warnings on soft-drinks cans, giving equal advertising time to marketing fresh fruit and vegetables and voluntary agreements to reduce sugar content.

Lustig says: "If the food industry continues to obfuscate, we will never solve this and by 2026 we will not have healthcare because we will be broke. Food producers are going to have to be forced. There's only one group that can force them, and that's the government. There's one group that can force the government, and that's the people."

• This article was amended on 16 May 2014. An earlier version referred simply to "diabetes", rather than make the distinction that type 2 diabetes was meant. Type 1 diabetes is an auto-immune condition, not triggered by lifestyle, primarily affecting young children and adolescents.