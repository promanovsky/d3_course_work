Rolf Harris has been sentenced to the best part of 6 years in prison after he was found guilty for 12 counts of indecent assault. The sentence comes under the remit of “unduly lenient”, the office of the Attorney General has now confirmed.

Rolf Harris has been found guilty and sentenced on abuse charges - photo: Getty 2014, Tristan Fewings

The judge told Harris: "For well over 50 years you have been a popular entertainer and TV personality of international standing with a speciality in children's entertainment. You are also an artist of renown.

"You have been the recipient of a number of honours and awards over the years, you have done many good and charitable works and numerous people have attested to your positive good character.

"But the verdicts of the jury show that in the period from 1969 to 1986 you were also a sex offender, committing 12 indecent assaults on four victims who were variously aged between eight and 19 at the time."

More: BBC Resists Calls For Rolf Harris Enquiry saying Offenses "Do Not Relate" To Them

The 84-year-old was found guilty earlier in the week of assaulting four girls. Mr Justice Sweeney told Harris he took advantage of the trust his victims placed in him because of his celebrity status.

One of the victims, a friend of Harris’ daughter Bindi, explained the damage he had caused. "As a young girl I had aspirations to have a career, settle down and have a family," she said. "However, as a direct result of his actions, this has never materialised.

She added: "The knowledge of what he had done to me haunted me. However, his popularity with the British public made it harder for me to deal with." In his sentencing remarks, the judge said he had “no doubt” that Harris’ crimes caused her “severe psychological harm”.