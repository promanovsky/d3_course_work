I'll admit, I've been as hard as the next financial journalist on Microsoft's (NASDAQ:MSFT) efforts in the tablet space.

Over the last several years, as Apple's (NASDAQ:AAPL) iPad and manufacturers sporting Google's Android mobile operating system have developed the same seeming stranglehold on the tablet space that they also enjoy in the smartphone market, Microsoft foundered almost laughably in its initial attempts to crack this booming market.

Microsoft clearly didn't get it when it launched the set of Surfaces in 2012 (Surface RT, anyone?), and it showed in the marketplace. For example, in most of the fourth quarter 2014, Microsoft was estimated to have sold an estimated 1.4 million Surface tablets, compared to roughly 26 million iPads from Apple. Something was not working.

I felt that same sense of skepticism earlier this week as I sat alongside a slew of tech reporters and analysts as Microsoft unveiled its new Surface Pro 3 in New York City. But what I saw and experienced at that launch could represent a tectonic shift in the world of mobile computing. Here's what I found.

A tablet that can replace your laptop

This was the mantra repeatedly referenced by Microsoft's management team as the guiding principle behind the Surface Pro 3.

In their eyes, the grand potential of the tablet has always been the promise of being able to do everything the same kind of basic productivity work and content creation computing functions as a PC but in a form factor small enough to also cater to smaller scale content consumption like gaming and video streaming. Apple co-founder Steve Jobs also touched on this same theme when he introduced the iPad in 2010, although there's a subtle difference between Microsoft CEO Satya Nadella's message this and Jobs' four years ago. And after spending some time with it the midtiered version of the Surface Pro 3 powered by Intel's Core i5 processor, Microsoft appears to have achieved its goal.

The Surface Pro 3 is lighter than virtually any laptop this side of Apple's 11-inch Macbook Air, but clearly packs enough processing and productivity horsepower to meet the average worker's day-to-day needs. The pen (not stylus), albeit not perfect, has worked perhaps better than any writing instrument I've encountered for a touch-based surface in the past. Most importantly, Microsoft's suite of Office products work well enough to cover the majority of computing needs of an equivalent laptop when married with the improved snap-on keyboard and its better-than-expected track pad. Overall, Microsoft's produced a very solid product here.

This is the first Microsoft tablet that I can see gaining more than niche acceptance in the tablet market, which is significant in and of itself.

Microsoft's anti-iPad

After spending some first-hand time with Surface Pro 3, let's get one thing clear: the Surface Pro 3 is not a tablet. It's a laptop replacement. Perhaps it'd be better referred to as the anti-iPad, a tabletlike device that's better suited to content production than consumption.

In tablet mode, the Surface Pro 3 left a lot to be desired. The 12-inch screen size and hard edges make it somewhat unwieldy to carry and hold for long periods of time. It's just a tad too clunky to sit with comfortably on the couch for the kind of content consumption that's right in the wheelhouse for smaller tablets like Apple's iPad. It doesn't feel like a relaxation device, a point that sounds trivial but that matters in the competitive consumer market. So as far as Apple's iPad is concerned, the Surface Pro 3 failed to strike me as the potential iPad killer many in the press have made it out to be.

But when thought of as a laptop alternative, the Surface Pro 3 strikes me as the kind of device that could make serious inroads in the enterprise space. It's blend of size, speed, and reasonably portability could do quite well in the world of the mobile worker deep within the innards of some major corporation. After spending some time with the Surface Pro 3, it's easy to see how it could meet the varied needs to a modern IT worker whose job demands computing power both on-site and at the desk. This is best use-case for Microsoft's Surface Pro 3.

The great market segmentor

So as far as predictions go, I'm stopping well short of saying Microsoft's new Surface Pro could "kill" Apple's iPad or Macbook Air, which has also really never been an enterprise device either.

Forrester research estimates that by 2017, one of every five tablets sold will be for enterprise use. And that's exactly the market where I see Microsoft's new Surface Pro 3 fitting nicely. So instead of killing Apple's iPad, I see Microsoft most recent tablet (or laptop alternative) as the device the splits the tablet market more clearly between enterprise and consumer devices.

It's also unclear what effect Microsoft's recently released Windows for Apple's iPad could have on the Surface Pro 3's possible enterprise inroads, but it's worth noting as a potential swing factor in helping this segmentation play out.

But at the end of the day, as a device that's in my mind clearly better suited to work versus play, Microsoft's latest stab at the tablet market, although not without its own promise, fails to hold up to the "iPad killer" references we've been swamped with in the media this week, a fact that worth noting for both Apple and Microsoft investors alike.