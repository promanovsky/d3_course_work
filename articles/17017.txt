Malaysian authorities have called for differences to be put aside, in order to find missing flight MH370.

Speaking at a news conference in Kuala Lumpur, acting Transport Minister Hishammuddin Hussein said the search for the plane was "bigger than politics."

The Malaysia Airlines aircraft went missing on 8 March with 239 people on board. Some 26 countries are involved in search efforts.