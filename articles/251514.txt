Music

Carey's new album, 'Me. I Am Mariah... The Elusive Chanteuse', has been delayed several times due to many issues including the shoulder injury the singer obtained last summer.

May 26, 2014

AceShowbiz - Mariah Carey is back. Nearly five years since releasing her last proper studio album, 2009's "Memoirs of an Imperfect Angel", the singer is returning to the game with a new LP titled "Me. I Am Mariah... The Elusive Chanteuse".

Getting her fourteenth full-length done is not easy for Carey. She first announced that her new album would be called "The Art of Letting Go" and it's planned to be released in early 2013. The project was later delayed numerous times due to several problems including the injury she obtained when filming a music video last summer.

When announcing that her album would now be called "Me. I Am Mariah" late last month, Carey said that the title was inspired by a picture of herself that she drew when she was three years old.

"Please don't judge me for such a simplistic title. Come on, I was only three-and-a-half," she says in a video. "It was a creative visualization of how I saw myself with the purity of a child's heart before it was ever broken."

The album is due out on May 27. It will feature "#Beautiful", Carey's collaboration with Miguel which was released last year as the lead single off "The Art of Letting Go", as well as Wale-assisted "You Don't Know What to Do" that she debuted during her set at "Today" summer concert series earlier this month

In addition to those names, "Me. I Am Mariah" boasts guest appearances from Nas, Fabolous and Carey's twin children.