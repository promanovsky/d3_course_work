A child runs past a grand piano that has been left by the East River underneath the Brooklyn Bridge, in the Manhattan borough of New York May 31, 2014. REUTERS/Carlo Allegri

All eyes were on the monthly manufacturing reports. And then one data provider made an error. Actually, two errors.

First, the scoreboard:

Dow: 16,743.6 (+26.4, +0.1%)

16,743.6 (+26.4, +0.1%) S&P 500: 1,924.9 (+1.4, +0.0%)

1,924.9 (+1.4, +0.0%) Nasdaq: 4,237.2, (-5.4, -0.1%)

And now the top stories: