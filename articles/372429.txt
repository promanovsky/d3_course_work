Android’s open nature offers some substantial benefits to consumers versus rival mobile platforms such as Apple’s iOS operating system. There are also some negative affects, however, and one of them is the fact that Google’s Android devices are far more susceptible to malware attacks. We have seen plenty of reports on various Android security holes and malware in the past, and now a newly discovered flaw could end up costing Android device owners a fortune.

FROM EARLIER: Android users, beware: Huge security hole lets phones secretly spy on you and capture photos

According to a report from IDG News Service, a newly discovered Android vulnerability allows apps to make unauthorized phone calls or disrupt ongoing calls. By exploiting the security hole, malicious software would be able to place calls to premium-rate phone numbers, potentially resulting in hundreds of dollars worth of charges on the user’s account.

“An attacker could, for instance, trick victims into installing a tampered application and then use it to call premium-rate numbers they own or even regular ones and listen to the discussions in the range of the phone’s microphone,” Bogdan Botezatu, an analyst at Bitdefender, told IDG. “The premium-rate approach looks more plausible, especially since Android does not screen premium-rate numbers for voice as it happens with text messages.”

Researchers from security firm Curesec reportedly discovered the flaw, and they say Google has been informed. They also report that the flaw has been fixed in Android 4.4.4 KitKat, but it is still present in Android 4.1.x, 4.2.x and 4.3.x, as well as in Android 4.4.1, 4.4.2 and 4.4.3.

According to Google’s publicly available Android version distribution data, nearly 70% of Android devices currently in use could be affected by the flaw.