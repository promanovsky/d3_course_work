The recent, deadliest outbreak ever of Ebola, the killer virus that causes internal and external hemorrhaging, is raising new concerns about the disease's spread.

Doctors Without Borders, the only aid organization that is treating infected people in West Africa, says, "The scale of the current Ebola epidemic is unprecedented in terms of geographical distribution, people infected and deaths."

According to the World Health Organization, this is the first time Ebola has been epidemic in urban areas since it was initially recognized in Zaire in 1976. That increases the likelihood that someone carrying the virus could travel to other parts of the world.

The current outbreak, which began in March and affects Guinea, Liberia and Sierra Leone, is also the first in all three countries. Doctors without Borders is warning of a real risk that the epidemic will spread beyond the 60 locations in which patients have already been identified.

Nevertheless, Canadians need not be concerned about Ebola spreading to Canada, Dr. Tim Jagatic says.

Jagatic, from Windsor, Ont., treated Ebola patients in Guinea this spring as part of the international group Doctors Without Borders, and returns there next week.

Tom Geisbert, a microbiologist at the University of Texas, agrees that the risk of somebody coming from the affected area and causing a huge outbreak in North America or Europe is extremely low. Geisbert has authored or co-authored about one hundred studies on Ebola and related viruses.

Virus spreads via bodily fluids

Finda Marie Kamano, 33, reported extreme weakness, vomiting and dysentery, symptoms typical of those caused by the Ebola virus. When she arrives at the treatment centre in Conakry, Guinea, Kamano is seen by a nurse in a protective suit. (Sylvain Cherkaoui/MSF)

The virus spreads through contact with bodily fluids. Before symptoms appear, a period of between three and 21 days, that person is not yet contagious.

It's possible someone could travel to Canada, for example, and then become sick and contagious, but it's likely the risk would be quickly contained.

"Because we take common sense measures when we see someone who is ill — we keep our distance, we try not to touch them, we avoid contact — even before anybody knows what was wrong with that particular person, they would probably take these common sense approaches to avoid contact with them," explains Jagatic.

Compared to West Africa, Canada's "health infrastructure is so well built up that we would be able to prevent the spread if somebody were to come into the country contaminated," he says.

A 2009 study by the European Centre for Disease Prevention and Control found no evidence of transmission of Ebola on aircraft.

The study does mention the case of a passenger travelling from Gabon to seek treatment in South Africa in 1996 for what was later diagnosed as Ebola. Although the passenger displayed symptoms, there's no evidence the disease spread to anyone on the flight or in South Africa.

How Ebola is spreading

Jagatic says Ebola "is not a very contagious disease and the reason it's being spread in [West Africa] is because of cultural and behavioural practices."

A Doctors Without Borders team disinfects Finda Marie Kamano’s home in Gueckedou, Guinea, in April. The day before, tests confirmed she had Ebola. She died the next day. (Sylvain Cherkaoui/MSF)

As he explains, the body is most infectious at the time of death, "which is when they're cleaning the body for the funeral and then people at the funeral touch the body, and then, while grieving, they'll touch their faces, they'll touch their mouths and noses” — the mucous membranes of the body from where the virus is transmitted.

Geisbert notes that this is the first Ebola outbreak in West Africa, and that in the earlier outbreaks elsewhere on the continent the disease was easily contained because they happened in just one location. "If it's in one area, quarantine practices tend to work fairly well, it burns out."

He says this time Ebola is proving incredibly hard to control because it's occurring across a large geographic area, in "so many different places all at the same time."

Other factors playing a role, Geisbert points out, include people going into areas of the West African rainforest that weren't developed before, thereby increasing the probability of encountering the virus. He says bats are probably a reservoir for the virus.

And travel within the country or region has become much easier.

This Ebola strain the deadliest

The Ebola strain causing this epidemic, the Zaire, one of five different strains, is the deadliest one, health officials say.

In that first outbreak, in Zaire in 1976, death occurred in 88 per cent of the reported cases. There were 280 deaths in 1976, a number not exceeded until this year.

As of July 2, WHO reports 481 deaths and 779 confirmed, probable or suspect cases.

The last outbreak of this strain was in the Democratic Republic of the Congo in 2008-09 and the death rate was 47 per cent. That was preceded by an outbreak in 2007, also in Congo, that had a death rate of 71 per cent.

Jagatic says the virus has now reached densely populated urban areas, which allows it to propagate quickly. Also, because cities are travel nodes, "You've got people who infect and then move on, and people who've been infected and then move on."

Education key to containing outbreak

Dr. Tim Jagatic, part of the Doctors Without Borders team in Guinea, treated Ebola patients there in the spring and plans to return next week. (MSF)

Improving education about Ebola is key to containing this outbreak, both Jagatic and Giesbert told CBC News.

Based on his experience in Guinea, Jagatic says there needs to be more education and awareness about how the disease is spread. Then Ebola would be "relatively easy to prevent by avoiding contact, particularly with the dead bodies, by just keeping a safe distance from anybody who might be a suspected case."

Washing with soap and water and other things that raise the level of hygiene would also make a difference in helping put an end to this epidemic, he says.

Geisbert says that without that education, people who become infected, "don't want to report it, they hide, move to some other area. The chain keeps going and people go to other areas and spread it to another uninfected area."

In Liberia, where the Guardian newspaper reports that many people remain adamant the epidemic is a government hoax, the Health Ministry has been placing photos of Ebola-ravaged corpses in the media.

"They are very graphic but it is working. People are starting to see that Ebola is not just a spiritual thing that you can cure through going to church," Tolbert Nyenswah, Liberia's deputy chief medical officer, told the Guardian.