One of former President Donald Trump's defense lawyers dodged a question about the winner of the 2020 presidential election, saying his own judgement on the matter is "irrelevant."

After Trump's team concluded their arguments in his impeachment trial on Friday, senators had the opportunity to ask questions of both sides. Sen. Bernie Sanders (I-Vt.) asked whether prosecutors are "right when they claim that Trump was telling a big lie" when he repeatedly falsely claimed he won the 2020 election, "or, in your judgement, did Trump actually win the election?" Trump defense attorney Michael van der Veen declined to answer either way.

"In my judgement, it's irrelevant to the question before this body," Van der Veen responded. "What's relevant in this impeachment article is, were Mr. Trump's words inciteful to the point of violence and riot? That's the charge, that's the question. And the answer is no."

Trump is facing charges of inciting a riot at the Capitol building on Jan. 6 — his supporters who breached the building backed his false claims that widespread voter fraud affected the outcome of the election and that he won in a landslide.

Earlier in the question-and-answer session, Trump's defense team was also asked for details regarding when Trump learned about the breach at the Capitol building, as well as what "specific actions" he took "to bring the rioting to an end." Van der Veen referenced a tweet from Trump before complaining that there's been "absolutely no investigation" into this. Brendan Morrow