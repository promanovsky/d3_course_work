'I love you so much!': Kim Kardashian wishes niece Penelope Disick a happy 2nd birthday... as Khloe calls her a 'princess'



It's been two years since Kourtney Kardashian welcomed her daughter Penelope into the world.

And on Tuesday the entire family wished the little girl a very Happy Birthday with Instagram notes and photos.



Kim shared a shot of the child wearing pink cat-eyed sunglasses with the note, 'Happy Birthday my little pushka! I love u so much!!!!' And sister Khloe posted a photo of herself hugging the toddler with the caption, 'Birthday princess.'

Scroll down for video



Good auntie: Kim Kardashian, pictured in New York City on Monday, wished her niece Penelope Disick a happy birthday on Tuesday morning with the Instagram caption, 'I love you so much!'

All dressed up: The image the 33-year-old star shared was of Penelope in a denim top and floral skirt - no doubt by Kardashian Kids - and pink cat-eyed sunglasses and a matching pacifier while in a kitchen as beauty products were on the background

Even Kendall Jenner, who appeared in the Chanel show in Paris on Tuesday morning, sent out an early morning message.

'Happy birthday Poosh!!!' wrote the 18-year-old, who shared a black and white image of the youngster with rice on her face.



Mom Kris was the most wordy with a heartfelt message: 'Happy Birthday to my beautiful little angel princess Grandaughter Penelope!!!!! You are our little ray of sunshine and brighten every day....I love you sooooo much!!!! #blessed #lovebunny #family #birthdaygirl.'

So close: Khloe shared this snap on her social media where she's puckering up to the little girl

Penelope has been in the Hamptons with mom Kourtney and father Scott Disick - who also goes by the nickname The Lord - while the family tapes the reality TV spinoff Kourtney & Khloe Take The Hamptons.

On Monday Kourtney shared an image of Penelope leaning on her pregnant belly.

The 35-year-old was wearing a gold bikini as she reclined on a lounge chair on the lawn of their rental.

Photogenic: Kris Jenner posted this head shot and wrote, 'Happy Birthday to my beautiful little angel princess Grandaughter Penelope!!!!! You are our little ray of sunshine and brighten every day....I love you sooooo much!!!!'

Such long lashes: Kendall Jenner shared this black-and-white shot where Penelope has rice on her face

A step in the right direction: The 18-year-old model walked the runway of Chanel on Tuesday in Paris

Penelope's sibling is Mason Disick, who has already appeared on several seasons of Keeping Up With The Kardashians and reportedly has his own bank account.

Kourtney is pregnant with her third child, which she announced on her E! show.

She has yet to reveal what the gender is.

Penelope's parents have yet to wed or even get engaged, which has been a subject matter on the family's reality shows.

Proud of her bump: Mom Kourtney with Penelope on her stomach on Tuesday in the Hamptons; the beauty sported a gold bikini

The parents: Scott Disick and Kourtney holding hands in Southampton in June















