Following the market opening Monday, the Dow traded up 0.28 percent to 16,454.65 while the NASDAQ surged 0.36 percent to 4,110.23. The S&P also rose, gaining 0.25 percent to 1,869.44.

Leading and Lagging Sectors

Monday morning, the healthcare sector proved to be a source of strength for the market. Leading the sector was strength from Sarepta Therapeutics (NASDAQ: SRPT) and Momenta Pharmaceuticals (NASDAQ: MNTA). In trading on Monday, basic materials shares were relative laggards, down on the day by about 0.28 percent.

Top losers in the sector included POSCO (NYSE: PKX), off 2.7 percent, and Century Aluminum Co (NASDAQ: CENX), down 2.4 percent.

Top Headline

Halliburton (NYSE: HAL) posted a profit in the first quarter. Halliburton swung to a quarterly profit of $622 million, or $0.73 per share, versus a year-ago loss of $18 million, or $0.02 per share. Its income from continuing operations came in at $0.73 per share. Its total revenue climbed to $7.35 billion versus $6.97 billion. However, analysts were estimating earnings of $0.72 per share on revenue of $7.26 billion.

Equities Trading UP

Sarepta Therapeutics (NASDAQ: SRPT) shares shot up 52.70 percent to $37.26 on announcement that it will file NDA for Eteplirsen for the treatment of Duchenne Muscular Dystrophy by the year-end.

Shares of Advanced Micro Devices (NYSE: AMD) got a boost, shooting up 12.06 percent to $4.14 after the company reported upbeat quarterly earnings and issued a strong Q2 revenue forecast.

AstraZeneca PLC (NYSE: AZN) shares were also up, gaining 5.54 percent to $67.01 on report of potential acquisition by Pfizer (NYSE: PFE).

Equities Trading DOWN

Shares of Moneygram International (NASDAQ: MGI) were down 9.52 percent to $13.40. JMP Securities downgraded Moneygram from Market Outperform to Market Perform.

Acacia Research (NASDAQ: ACTG) shares tumbled 6.21 percent to $16.00 after the company reported weaker-than-expected Q1 results.

athenahealth (NASDAQ: ATHN) was down, falling 4.46 percent to $139.08 after the company reported downbeat quarterly results. Morgan Stanley analyst Ricky R. Goldwasser removed the $133.00 price target on athenahealth.

Commodities

In commodity news, oil traded down 0.14 percent to $104.15, while gold traded down 0.46 percent to $1,288.00.

Silver traded down 1 percent Monday to $19.40, while copper fell 0.02 percent to $3.03.

Eurozone

Europe's markets were quiet with British, Italian, French and German markets all closed for Easter holidays.

Economics

The Chicago Fed National Activity Index rose to 0.20 in March, versus a prior reading of 0.14. However, economists were expecting a reading of 0.20.

The Conference Board's index of leading indicators rose 0.80% in March, versus economists' expectations for a 0.70% growth.

The Treasury is set to auction 3-and 6-month bills.