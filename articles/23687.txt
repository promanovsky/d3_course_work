Sleepless nights could prove more damaging than previously thought, after the results of a new study suggested sleep deprivation can lead to a permanent loss of brain cells.

The “disturbing” research found mice experiencing chronic sleep loss saw 25 per cent of their brain cells die, according to research published yesterday in the Journal of Neuroscience.

Now, scientists fear humans could suffer the same neuronal injuries – which would also make attempting to ‘catch up’ on missed sleep pointless.

Researchers at the University of Pennsylvania School of Medicine set out to determine if chronic sleep loss damages brain cells involved in keeping the brain alert, and if this damage can be reversed.

Lead author Sigrid Veasey, a professor at the University, said previous research on humans has shown attention span and several other aspects of cognition do not return back to normal even with three days of recovery sleep, raising the possibility that permanent damage had been sustained.

The team studied lab mice, keeping them awake for periods of time that were similar to human sleep loss, such as when people work night shifts or long hours.

After three days of shift-work sleep patterns, mice lost 25 per cent of brain cells in part of the brain stem.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

"No one really thought that the brain could be irreversibly injured from sleep loss." Dr Veasey said. "This is the first report that sleep loss can actually result in a loss of neurons."

She said more research will need to be undertaken to see if people who regularly miss out on sleep could also suffer permanent brain cell damage.

If a similar phenomenon does occur in humans, the researchers believe it could be possible to develop a treatment that would protect against the harmful effects of losing sleep.

"If we can show that we can protect the cells and wakefulness, then we're launched in the direction of a promising therapeutic target for millions of shift workers."