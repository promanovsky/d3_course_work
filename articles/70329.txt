Robots aren’t just taking your jobs, they’re stealing your profits on stock trades, too.

The stock market has been rigged by a group of tech-savvy insiders who are using super-computers to game trades at the expense of normal investors, journalist Michael Lewis charges in his new book, “Flash Boys.”

The high-tech traders have spent hundreds of millions of dollars to roll out computer networks with high-speed fiber-optic connections that can quickly detect when investors place orders to buy stocks, according to the new book, which hits shelves this week.

The robots’ high-speed networks allow them to buy the stocks milliseconds in advance — enough time to push up the price for the investor that had made the original order.

“They’re able to identify your desire to, to buy shares in Microsoft and buy them in front of you and sell them back to you at a higher price,” Lewis said. “The speed advantage that the faster traders have is milliseconds … fractions of milliseconds.”

The villains are a “combination of these stock exchanges, the big Wall Street banks and high-frequency traders” who are bagging billions every year with the practice, Lewis said in an interview Sunday with “60 Minutes.”

The victims, Lewis adds, are “everybody who has an investment in the stock market.”

The author of Street classic “Liar’s Poker” said the high-tech tactics were deciphered by Brad Katsuyama, a former trader at the Royal Bank of Canada, who has begun a new exchange, IEX, that promises to level the playing field by creating a “speed bump” for the high-frequency robots. Among IEX’s investors is Greenlight Capital, run by David Einhorn.