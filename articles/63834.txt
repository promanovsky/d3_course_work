Australian search teams shifted their focus Saturday about 680 miles to the northeast of where they'd previously been looking for missing Malaysia Airlines Flight 370 in the Indian Ocean.

An analysis of satellite data newly released by Japan and Thailand suggested the Boeing 777 may not have flown as far south as initially thought. The new search area is closer to the Australian coast.

Advertisement

The plane went missing three weeks ago after taking off from Kuala Lumpur.

Malaysian Prime Minister Najib Razak announced Monday the plane crashed into the Indian Ocean near Perth, Australia, but so far no debris in that area has been directly linked to the flight.

Aircraft sent to the new search area located debris, included white objects, orange rope and a blue bag.

"At one point, sure, everybody on board got a little excited, but it's impossible to tell from that distance what anything is," said CNN's Kyung Lah, who was on the U.S. Navy P-8 search plane that spotted some of the debris.

[CNN]