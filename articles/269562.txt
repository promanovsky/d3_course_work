President Obama Proclaims June LGBT Month; Celebrates Extending Immigration, Family Benefits to Gay Couples President Obama Proclaims June LGBT Month; Celebrates Extending Immigration, Family Benefits to Gay Couples

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

President Barack Obama on Friday proclaimed June as being the lesbian, gay, bisexual and transgender pride month, after celebrating that his administration is also extending family and immigration benefits to legally married gay couples and calling for more freedoms for gays in the workplace.

"I, Barack Obama, president of the United States of America, by virtue of the authority vested in me by the Constitution and the laws of the United States, do hereby proclaim June 2014 as lesbian, gay, bisexual and transgender pride month," declared Obama in remarks released by the White House Friday.

"I call upon the people of the United States to eliminate prejudice everywhere it exists, and to celebrate the great diversity of the American people," he added.

Before making the declaration, Obama highlighted some key victories of the LGBT activist lobby made possible under his administration.

"Last year, supporters of equality celebrated the Supreme Court's decision to strike down a key provision of the Defense of Marriage Act, a ruling which, at long last, gave loving, committed families the respect and legal protections they deserve," said Obama.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"In keeping with this decision, my administration is extending family and spousal benefits — from immigration benefits to military family benefits — to legally married same-sex couples. My administration proudly stands alongside all those who fight for LGBT rights," he said.

Obama also commented that further protections are needed for LGBT groups.

"LGBT workers in too many states can be fired just because of their sexual orientation or gender identity; I continue to call on the Congress to correct this injustice by passing the Employment Non-Discrimination Act. And in the years ahead, we will remain dedicated to addressing health disparities within the LGBT community by implementing the Affordable Care Act and the National HIV/AIDS Strategy, which focuses on improving care while decreasing HIV transmission rates among communities most at risk," he said.

"Our commitment to advancing equality for the LGBT community extends far beyond our borders. In many places around the globe, LGBT people face persecution, arrest, or even state-sponsored execution. This is unacceptable. The United States calls on every nation to join us in defending the universal human rights of our LGBT brothers and sisters," he added.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit