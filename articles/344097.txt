Liberians face jail over Ebola

Share this article: Share Tweet Share Share Share Email Share

Monrovia, Liberia - Liberia's president warned Monday that anyone caught hiding suspected Ebola patients will be prosecuted. In an interview with state radio, Ellen Johnson Sirleaf expressed concern that some patients had been kept in homes and churches instead of receiving medical attention. “Let this warning go out: Anyone found or reported to be holding suspected Ebola cases in homes or prayer houses can be prosecuted under the law of Liberia,” Sirleaf said. Her comments came just days after Sierra Leone issued a similar warning, saying some patients had discharged themselves from the hospital and had gone into hiding. Health workers have encountered resistance throughout the region ever since Ebola cases were first confirmed in March, and some have even been attacked.

The outbreak of the disease in West Africa is already the deadliest on record, with 635 cases and 367 fatalities, according to the latest World Health Organization numbers. A majority of the deaths - 280 - have been in Guinea where cases were first reported.

In an update released Monday, Liberia's health ministry said the country had recorded 49 deaths as a result of Ebola, 26 of which were confirmed by laboratory tests.

Sierra Leone has tallied 46 deaths, according to numbers released last week. That number was revised downward from 78, however, after Sierra Leone's government requested that the World Health Organization only report laboratory-confirmed fatalities.

Laboratory testing is the only way to definitively confirm the presence of Ebola, though not all patients are tested because some don't receive medical treatment and some die before samples can be taken, especially in the early stages of an outbreak.

There is no cure for the deadly disease caused by the Ebola virus which has an incubation period of two to 21 days and starts with fever and fatigue before descending into headaches, vomiting, violent diarrhea and then multiple organ failure and massive internal bleeding.

Some symptoms of the disease can be treated, however, and health workers also stress the importance of submitting patients for medical care so they can be isolated. Ebola spreads through contact with the bodily fluids of infected people, including blood and sweat.

Liberia's health ministry has set up treatment centers and started a public service campaign to slow the spread of the disease, including training health professionals to use protective clothing while forbidding hospitals to turn away patients with Ebola symptoms.

Sirleaf's comments on Monday seemed partly intended to dispel rumors in Liberia that the outbreak isn't real.

“It is, as I speak, taking the lives of our citizens,” she said. “The disease is real and is in our country and can kill a lot of people.”

Sapa-AP