Apple is offering free battery replacement to select iPhone 5 owners under the iPhone 5 battery replacement program introduced on Friday.

Apple says it has determined that a "very small percentage" of iPhone 5 (Pictures) units may suddenly experience shorter battery life or need to be charged more frequently. The company adds that the affected iPhone 5 devices were sold between September 2012 and January 2013 and fall within a limited serial number range.

If your iPhone 5 "is experiencing these symptoms and meets the eligibility requirements", Apple says it will replace your iPhone 5's battery, free of charge.

The program opened to customers in US and China on Friday, August 22, and will be available in other countries from next Friday (August 29).

To see if your iPhone 5 is eligible, visit the iPhone 5 battery replacement program page on Apple's website and enter your serial number in the text box and click Submit. Apple will instantly tell you if you are eligible for a free battery replacement. If you don't know how to find the serial number of your iPhone, click here.

Even if you've bought your iPhone 5 outside the time window specified by Apple, it is worth entering your serial number to see if your unit is eligible as sometimes there can be a difference between the time Apple shipped the unit and when it was purchased by the end user, especially if its a resale or refurbished unit.

Apple has posted other information like where to bring in your iPhone for battery replacement if it is eligible, and instructions on how to backup your data before doing that. Check out the program page on Apple's website for the rest of the details.

Earlier this year, Apple had launched a sleep/ wake button replacement program for select iPhone 5 units.