Samsung has announced the Galaxy S5 mini, a smaller, 4.5-inch version of its flagship smartphone, the 5.1-inch Galaxy S5.

Unfortunately for those who wanted the same specs in a smaller package, the 4.5-inch Galaxy S5 mini definitely does not pack the same power as its older brother. It has a quad-core, 1.4GHz processor, an 8-megapixel camera, a 1280x720 pixel screen, 1.5GB RAM, 16GB of memory and a 2,100mAh battery.

Those specs are all a far cry from the S5's 2.1 or 2.5GHz processor, 2GB of RAM, 16-megapixel camera, full HD screen and a 2,800mAh battery.

The good news is that the S5 mini keeps what makes Samsung's flagship interesting — it's waterproof, and features a heart rate monitor and a fingerprint scanner. Both devices also share a very similar design.

The Galaxy S5 mini will be available in Russia in "early July," after which it will expand globally. It will be available in four colors at launch: black, white, blue and gold.