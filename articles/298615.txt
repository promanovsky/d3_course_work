Facebook has released a new disappearing photo and video stand-alone smartphone app called Slingshot that promises to compete with Snapchat.

The app allows users to take a photo or short video, add text or color, and then send it to any number of friends. The catch is that users have to “sling” a picture or video back in order to view messages sent from friends.

Viewing a photo or video message will then self-destruct and disappear after being viewed, similar to messages on Snapchat.

Slingshot is the second stand-alone app from Facebook Creative Labs, coming after the iOS-only Paper app, an alternative to Facebook’s normal app interface, was released in January.

Advertisement

Facebook attempted to buy Los Angeles-based Snapchat for $3 billion in 2013, but 23-year-old founder Evan Spiegel rejected the technology giant’s offer in hopes of receiving a better valuation for the company.

The Slingshot app will be available for download in the U.S. on Tuesday, and later in other countries.