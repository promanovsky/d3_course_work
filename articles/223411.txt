The new kinase inhibitor nintedanib used for cancer treatment has been discovered to slow the progression of a fatal lung disease significantly, Med Page Today reported.

The drug makes a dramatic difference to idiopathic pulmonary fibrosis (IPF), scientists from Southampton General Hospital found.

The condition, which affects mainly men and former smokers, causes inflammation and scarring of the lung tissue. According to UK MailOnline, with about 5,000 deaths and 5,000 new cases each year, sufferers have an average life expectancy of between three and five years.

By creating excess tissue and scarring and causing breathing problems, the drug blocks the chemical signals that could activate the disease.

In two studies, the breathing capacity of 638 people with IPF who used the drug was compared with 423 people who took a placebo, led by Professor Luca Richeldi, a consultant in respiratory medicine at Southampton General Hospital.

"Breathing capacity is measured by the amount of air a person can expel after their deepest breath, which is seriously affected in IPF sufferers," UK MailOnline reported. "Researchers found that the decline in the breathing capacity of patients taking nintedanib was cut by around half over the two studies."

Patients will be able to use the drug in the near future, with it being licensed globally within the next year.

"The impact of this study cannot be overstated. These are exciting results and mean nintedanib has the potential to offer patients a new direction in treating this aggressive disease," Professor Richeldi said.

"The prognosis for patients with IPF is worse than many cancers and cases are increasing by 5,000 a year in the UK alone, so the need for new, targeted and more effective treatments has never been greater," he said.

"Nintedanib specifically targets some key molecular pathways known to be involved in IPF, which gives us a much clearer insight into how the body works and how we might be able to further develop treatments."

He added, "We expect an advance use program to begin in Europe before next summer, so the drug may become available to at least some patients within the next few months."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.