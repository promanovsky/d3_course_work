Dublin-based aircraft leasing firm SMBC Aviation Capital has placed an order with Airbus for 115 aircraft valued at around $11.8bn (€8.5bn).

The firm, the world’s third largest aircraft lessor, signed the deal for 110 A320neo aircraft and five A320ceo aircraft, in one of the largest ever aircraft orders by an Irish company.

Peter Barrett, chief executive of SMBC Aviation Capital – formerly RBS Aerospace - said it was a landmark order for the firm and wider aircraft leasing industry.

“The A320 family has played a crucial role in SMBC Aviation Capital’s success to-date, and this order should be taken as a clear sign both of our commitment to the new generation of single-aisle aircraft as well as our ambitious plans to grow our business,” he said.

“The A320neo programme is progressing well, and we believe that the timing and structure of the order that we have placed will complement the specific needs and requirements of our customer base.”

The company will announce its engines selection at a later date.

Elsewhere Elix Aviation Capital, a turboprop leasing specialist firm in Dublin, announced the acquisition of 25 aircraft from Nordic Aviation, in a deal valued at more than $100m. The aircraft are leased to a number of leading regional airlines around the world.

And leasing firm Avolon yesterday announced a deal to buy six Boeing Dreamliners, with a list price of $1.5bn (€1.1bn).

The deals were announced at one of the world’s biggest aerospace shows in Farnborough near London.

Online Editors