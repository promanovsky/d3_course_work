"Sharkando 2: The Second One" stormed on to Syfy Wednesday night -- and quickly became a trending topic in Twitter.

The sequel, which comes on the heels of last summer's original TV movie, found Tara Reid and Ian Ziering reprising their roles alongside plenty of guest stars, including Matt Lauer, Mark McGrath, Kelly Osbourne and more.

"We shot this movie in 18 days, but the story has expanded. Once [production company] The Asylum realized what they can do with a few more dollars in the budget the script reads like a $100 million blockbuster," Ziering told CBS News. "From halfway through the first page, it starts. And it doesn't let up ever."

Ziering was among the stars tweeting Wednesday night during the premiere. See what he and others had to say: