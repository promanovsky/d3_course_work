One of nine planes searching the new area Friday found objects, though the Australian Maritime Safety Authority said on Twitter that it would likely be Saturday before one of the six ships on the way could follow up and determine whether the objects were plane wreckage. The new area is closer to land and has calmer weather than the old one, which will make searching easier.

PERTH, Australia (AP) — The search area for the lost Malaysian jetliner moved 680 miles to the northeast on Friday, as Australian officials said a new analysis of radar data suggests the plane had flown faster and therefore ran out of fuel more quickly than previously estimated.

Advertisement

‘‘We have moved on’’ from the old search area, which pilots had combed for the week, said John Young, manager of Australian Maritime Safety Authority emergency response division.

AMSA said the change in search areas came from new information based on continuing analysis of the radar data received soon after Malaysia Airlines Flight 370 lost communications and veered from its scheduled path March 8. The Beijing-bound flight carrying 239 people turned around soon after taking off from Kuala Lumpur, flew west toward the Malacca Strait and disappeared from radar.

The search area has changed several times since the plane vanished as experts analyzed a frustratingly small amount of data from the aircraft, including the radar signals and ‘‘pings’’ that a satellite picked up for several hours after radar contact was lost.

The latest analysis indicated the aircraft was traveling faster than previously estimated, resulting in increased fuel use and reducing the possible distance the aircraft could have flown before going down in the Indian Ocean. Just as a car loses gas efficiency when driving at high speeds, a plane will get less out of a tank of fuel when it flies faster.

Advertisement

Malaysia’s civil aviation chief, Azharuddin Abdul Rahman, told reporters in Kuala Lumpur that analysts at Boeing Co. in Seattle had helped with the analysis of the flight.

Planes and ships had spent a week searching about 2,500 kilometers (1,550 miles) southwest of Perth, Australia, the base for the search. Now they are searching about 1,850 kilometers (1,150 miles) west of the city.

‘‘This is our best estimate of the area in which the aircraft is likely to have crashed into the ocean,’’ Martin Dolan, chief commissioner of the Australian Transport Safety Bureau, said at a news conference in Canberra.

He said a wide range of scenarios went into the calculation. ‘‘We’re looking at the data from the so-called pinging of the satellite, the polling of the satellites, and that gives a distance from a satellite to the aircraft to within a reasonable approximation,’’ he said. He said that information was coupled with various projections of aircraft performance and the plane’s distance from the satellites at given times.

Dolan said the search now is for surface debris to give an indication of ‘‘where the main aircraft wreckage is likely to be. This has a long way to go.’’

Objects in the new search area were seen from a New Zealand air force plane, AMSA tweeted, adding that the find needed to be confirmed by ship.

Young indicated that the hundreds of floating objects detected over the last week by satellites, previously considered possible wreckage, weren’t from the plane after all.

Advertisement

‘‘In regards to the old areas, we have not seen any debris and I would not wish to classify any of the satellite imagery as debris, nor would I want to classify any of the few visual sightings that we made as debris. That’s just not justifiable from what we have seen,’’ he said.

But in Malaysia, Defense Minister Hishammuddin Hussein said at a news conference that because of ocean drifts, ‘‘this new search area could still be consistent with the potential objects identified by various satellite images over the past week.’’

The new search area is about 80 percent smaller than the old one, but it remains large: about 319,000 square kilometers (123,000 square miles), about the size of Poland.

Sea depths in the new area range from 2,000 meters (6,560 feet) to 4,000 meters (13,120 feet), Young said. There are trenches in the area that go even deeper, Australia’s national science agency said in a statement. That includes the Diamantina Trench, which is up to 7,300 meters deep, but it was unclear whether the deepest parts of the trench are in the search area.

If the wreckage is especially deep, that will complicate search efforts. The U.S. Navy is sending equipment that can hear black box pings up to about 6,100 meters deep, and an unmanned underwater vehicle that operates at depths up to 4,500 meters.

Young said a change in search area is not unusual.

‘‘This is the normal business of search and rescue operations — that new information comes to light, refined analyses take you to a different place,’’ Young told reporters. ‘‘I don’t count the original work as a waste of time.’’

Advertisement

He said the new search zone, being about 700 kilometers (434 miles) closer to mainland Australia, will be easier to reach. Planes used so much fuel getting to and from the old search area that had only about two hours of spotting time per sortie.

The new area also has better weather conditions than the old one, where searches were regularly scrapped because of storms, high winds and low visibility.

‘‘The search area has moved out of the ‘roaring 40s,’ which creates very adverse weather,’’ Young said, referring to the latitude of the previous search area. ‘‘I’m not sure that we’ll get perfect weather out there, but it’s likely to be better than we saw in the past.’’

Hishammuddin said although the new area is more focused, it ‘‘remains considerable,’’ and that ‘‘search conditions, although easier than before, remain challenging.’’

Australia’s HMAS Success was expected to arrive in the area Saturday, Young said. The Chinese Maritime Safety Administration patrol boat Haixun 01 was also on site, and several more Chinese ships were on their way.

Malaysian officials said earlier this week that satellite data confirmed the plane crashed into the southern Indian Ocean.

Authorities are rushing to find any piece of the plane to help them locate the so-called black boxes, or flight data and voice recorders, that will help solve the mystery of why the jet, en route to Beijing from Kuala Lumpur, flew so far off-course. The battery in the black box normally lasts for at least a month.

Advertisement

Officials are already preparing for the hunt for the black box. A special U.S. Navy towed pinger locator and Bluefin-21 Autonomous Underwater Vehicle are to be fitted onto an Australian vessel, the Ocean Shield, when it reaches Albany, a port near Perth, in a day or two, said a government the official, who spoke on condition of anonymity because he was not authorized to speak to the media.

He did not say how long it would take to reach the search area.

An Australian government statement said investigators with technical expertise in maritime operations, flight data recorders, and materials and aerospace engineering were aboard the Ocean Shield.

The official said the Chinese ships are also expected to have acoustic sensors that can listen for black box pingers.

For relatives of those missing, the various clues and failed searches so far have just added to their agonizing waits.

The lack of results from the search gave one Malaysian family a little hope that the authorities were wrong in their calculations.

Eliz Wong Yun Yi, 24, whose father was on the plane, said her family stopped watching the daily Malaysian news conference because they felt the government was not upfront with information. All the new satellite data added to the confusion, she said.

‘‘After so many days, still no plane. We will not believe what they say until the plane wreckage is found. I want to stay positive and believe that my father will come back,’’ she said. Her father, Wong Sai Sang, a 53-year-old Malaysian property sales manager, had been on his way to Beijing for work.

If and when any bit of wreckage from Flight 370 is recovered and identified, searchers will be able to narrow their hunt for the rest of the Boeing 777 and its flight data and cockpit voice recorders.

___

Wong reported from Kuala Lumpur. Associated Press writers Scott McDonald and Eileen Ng in Kuala Lumpur, Kristen Gelineau in Sydney, Rod McGuirk in Canberra, Australia, and Nick Perry in Wellington, New Zealand, contributed to this report.