Netflix had a tough time at the 2014 Emmys last night, failing to garner a single Emmy for its main shows Orange Is The New Black and House Of Cards.

Both series arguably had second seasons this year that were even better than their first but couldn't stand up to the might of the cable shows, with Breaking Bad dominating following an emphatic final season.

OITNB was tipped to see wins in the comedy categories following Uzo Aduba's success at the Creative Emmys, with Taylor Schilling and Kate Mulgrew both being nominated for prizes, but neither actress won and Modern Family snatched the award for outstanding comedy.

In pictures: Orange is the New Black cast at the Emmys Show all 6 1 /6 In pictures: Orange is the New Black cast at the Emmys In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast Kate Mulgrew, who plays the character of Red in OITNB Instagram/oitnb In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast The cast of Orange is the New Black hired a party bus to take them to the Emmy awards, and definitely looked like they were having the most fun Instagram/oitnb In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast Adrienne C. Moore (Black Cindy) Instagram/oitnb In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast Instagram/oitnb In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast Instagram/oitnb In pictures: Orange is the New Black cast at the Emmys Orange is the New Black cast Dascha "Dash" Polanco, who plays Dayanara Diaz Instagram/oitnb

The empty award nets didn't dampen the spirits of the OITNB cast, who shone on the red carpet after arriving to the Los Angeles ceremony on a party bus.

Season 3 of the show is currently filming and is expected to arrive on Netflix sometime next year.