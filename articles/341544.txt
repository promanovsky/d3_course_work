Hacker group Energetic Bear has threatened energy companies operating in the United States and Europe with a malware capable of nationwide power disruptions.

The group of hackers, also known by the name Dragonfly, were assessed by Symantec Corp, a security company based in the U.S. Officials from Symantec concluded the group has enough resources and government connections to launch an extreme power attack. Eric Chien, chief researcher at Security Technology and Response Team of Symantec, told Businessweek he was not sure whether the group was directly affiliated to a state or if they were trying to sell a certain government or ideology.

The attackers would likely affect pipeline operators, electricity generation facilities, and grid operators.

"The Dragonfly group is well resourced, with a range of malware tools at its disposal and is capable of launching attacks through a number of different vectors," Symantec said told Businessweek.

Symantec was aware of the operations of Dragonfly as early as 2012. At that time, Chien explained that most of the attacks were carried out for surveillance purposes. The group released malwares that infiltrated the energy companies' remote access. Once the malware planted itself inside the system, the group gained access to the controls similar to that of the industrial control system.

"When they do have that type of access, that motivation wouldn't be for espionage," Chien explained to Businessweek. "When we look at where they're at, we're very concerned about sabotage."

Dragonfly is not the only group targeting security companies worldwide. In 2010, a series of attacks called the Stuxnet attack were carried out and affected Iranian nuclear facilities. The FBI also found out that UglyGorilla, a China-based hacker, attempted to infiltrate the computer systems of the U.S. Military. The group was convicted by a U.S. grand jury on charges of fraud and espionage.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.