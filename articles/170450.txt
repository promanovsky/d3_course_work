By Steve Tawa

PHILADELPHIA (CBS) — The American Academy of Neurology, meeting in Philadelphia, has released a study on the use of medical marijuana in brain diseases.

The world’s largest association for neurologists says the review highlights the need for more high-quality studies.

Review author Dr. Gary Gronseth (seated on podium at right) says they looked at more than 1,000 studies but only 33 were “of reasonable quality,” in his view.

“The problem is, we don’t know it works for most neurologic conditions,” he tells KYW Newsradio.

Does that mean they have a long way to go?

“There is a long way to go. That’s why most of our conclusions are, ‘We can’t tell whether this works or doesn’t work.’ ”

The findings indicate certain forms of medical marijuana can help treat multiple sclerosis but not Parkinson’s Disease. And there is not enough evidence, he thinks, to show whether it’s safe or effective for treating Huntington’s Disease, Tourette Syndrome, or epilepsy.

Dr. Barbara Koppel, another review author (seated at center), also notes the dearth of high-quality studies on the long-term efficacy and safety of various forms of medical marijuana — including smoked, pill, or oral spray.

More than 12,000 visitors are attending the American Academy of Neurology conference, going on through this weekend at the Pennsylvania Convention Center.