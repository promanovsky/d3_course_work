The British government wasted half a billion pounds in stockpiling Tamiflu despite the drug doing very little to halt the spread of the swine flu pandemic, says a peer reviewed scientific paper.

According to the review in the British Medical Journal, the drug, which was given to thousands of Britons during the swine influenza outbreak, was based on Swiss pharmaceutical giant Roche's "false impression" of Tamiflu's effectiveness, which was based on its "sloppy science".

The Oxford University author also claimed that the drug could have worsened the flu symptoms, instead of easing them.

Tamiflu was given to 240,000 people in the UK at a rate of 1,000 a week.

However, the drug intake has been linked to suicides of children Japan.

Roche's UK medical director Dr Daniel Thurley responded: "We disagree with the overall conclusions of this report. Roche stands behind the wealth of data for Tamiflu and the decisions of public health agencies worldwide, including the US and European Centres for Disease Control and Prevention and the World Health Organisation."

"The report's methodology is often unclear and inappropriate, and their conclusions could potentially have serious public health implications. Neuraminidase inhibitors are a vital treatment option for patients with influenza."