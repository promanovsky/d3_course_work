“The Fault in Our Stars” has become the biggest pre-selling romantic drama in Fandango’s history, as the adaptation of John Green’s best-selling novel prepares to break millions of moviegoers’ hearts this weekend.

The online ticketer did not say how many moviegoers have reserved a spot in theaters, but the film is out-pacing “The Vow,” “Safe Haven” and “Dear John” at the same point in their lead-ups to release. “The Vow,” a 2012 romance with Channing Tatum and Rachel McAdams was the previous record-holder and opened to $41.2 million.

There is one important caveat: This group of romantic titles does not include the “Twilight” films, which Fandango characterizes as fantasy genre romances. Critics may use harsher descriptors.

“The Fault in Our Stars” features rising stars Shailene Woodley and Ansel Elgort and is projected to debut to more than $30 million this weekend. This “Love Story” for Millennials centers on two teenagers who meet and fall in love in a support group for cancer patients.

In a non-scientific survey, Fandango polled more than 1,000 ticket-buyers and found that 81% read the novel, 95% have been looking forward to a big-screen love story and 80% are planning to see the film with a group of friends.

Woodley, who starred in last spring’s “Divergent,” was cited as a motivating factor for buying a ticket by 64% of audience members.