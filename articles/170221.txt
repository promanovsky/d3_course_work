HONG KONG, April 28 (UPI) -- Researchers in China have identified two promising antibodies capable of blocking MERS infection -- a bit of hopeful news for health officials after MERS-related deaths topped the 100 mark in Saudi Arabia over the weekend.

According to research by doctors at China's Tsinghua University and Sichuan University, as well as the University of Hong Kong, antibodies MERS-4 and MERS-27 were both able to block a vital virus protein from interacting with the cellular receptor that allows MERS to take hold.

Advertisement

Doctors published the details of their search for a potential MERS cure in the latest issue of the journal Science Translational Medicine. The researchers hope their findings can be used to quickly develop a vaccine or effective treatment.

MERS is a type of coronavirus, similar to the SARS virus that swept across Asia and the rest of the world in 2003, killing 800 people. Since Middle East Respiratory Syndrome was identified in 2012, 102 victims in Saudi Arabia have been killed and 339 cases have been identified -- a 30-plus percent mortality rate.

RELATED Rita Ora settles sneaker contract dispute

Scientists think MERS currently crosses only from camels to humans, but there is concern that the virus could evolve to jump from human to human.

Officials have been tracking the MERS virus for the last two years, but the rate of infections has skyrocketed over the last several months. Infections have also begun to crop up outside of Saudi Arabia and its immediate neighbors.

No cases have been identified inside the United States, and the CDC has yet to issue any travel advisories.