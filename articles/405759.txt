Kerry's visit to New Delhi comes after an unusually large number of disputes between the world's largest democracies

US Secretary of State John Kerry on Thursday holds his first meetings with India's right-wing government as he seeks to reboot a relationship seen as a bulwark against a rising China.

Kerry's visit to New Delhi comes after an unusually large number of disputes between the world's largest democracies, including charges of US surveillance against Indian politicians and a trade rift that could scuttle a global customs deal.

The top US diplomat plans to hold a full day of talks with senior Indian leaders before meeting Friday with Prime Minister Narendra Modi.

Taking a break from the intense Middle East diplomacy that has dominated his tenure, Kerry will highlight other issues dear to his heart -- including climate change, with a meeting scheduled with Indian environmental scholars.

Stressing the theme of US-India cooperation to find solutions on climate change and other global challenges, Kerry is also expected to tour laboratories of the prestigious Indian Institute of Technology Delhi.

"Both of our nations pride ourselves on science and innovation. So the bottom line is -- this is up to us. It's up to us to deliver," Kerry said in a speech before departure. The United States and India, at odds during the Cold War, began to reconcile in the late 1990s with leaders describing the world's two largest democracies as natural allies.

The two multi-ethnic countries are top targets of Islamic extremists, and have both been wary about the rapid ascent of China, which has a long-running border dispute with India.

But Indian perceptions that the United States is insensitive to its concerns flared into the open in December, when US authorities arrested an Indian diplomat for allegedly mistreating her servant.

Kerry is paying his first visit to India since the episode, which led New Delhi to take retaliatory action against US diplomats.

More recently, India threatened to block a global pact to streamline customs procedures before Thursday's ratification deadline unless the World Trade Organisation approves its stockpiling of food for the poor. Rich nations say the policy distorts global trade.

Commerce Secretary Penny Pritzker, who is accompanying Kerry, said that the United States was "very disappointed" at India's stance, but was hopeful for a deal to salvage the WTO deal. "I am hopeful that between now and the end of the month, we will find a way forward which is mutually beneficial," she said in an interview with The Times of India.

Pritzker emphasised the common ground between the two countries, saying there was "great opportunity in this partnership".

But allegations that Modi's Bharatiya Janata Party had been the target of surveillance operations by the US National Security Agency while it was in opposition have added to the sense of grievance on the Indian side.

A US official travelling with Kerry, speaking on condition of anonymity, acknowledged that India was upset but said that the United States nonetheless has pursued a "growing and closer intelligence relationship" with New Delhi, including on Afghanistan.

India is among the nations most concerned by the US withdrawal of combat forces from Afghanistan planned this year. The former Taliban regime sheltered extremists who waged attacks against India.

AFP