IBM has moved deeper into defending business computers with a new service it claims will stop hackers before they do damage.

"The need for security to become part of our strategy has been natural," IBM vice president of security strategy Marc van Zadelhoff said.

According to IDC, IBM was the third largest seller of cyber defence software in 2013. Credit:Craig Warga

The century-old business technology titan made a priority of defending computer networks about two years ago, unifying resources from more than a dozen security firms it acquired.