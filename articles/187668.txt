Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Miley Cyrus has slammed claims she took a drug overdose which triggered her long spell in hospital recently.

The Wrecking Ball singer used a brief press conference to boast about her fine physical health after a severe allergic reaction meant she spent weeks in a hospital bed, but said it was NOT down to drugs.

We were kept up to date, of course, via her upsetting Instagram pictures and tweets throughout her time in hospital - and she is undoubtedly relieved to be back to her usual twerking self.

The former Disney star was forced to postpone her US tour last month but begins the European leg tonight in London and will resume performances in America on August 1.

But she said she was "the poster child for good health" as she prepared to embark on the UK stint of her arena tour - and promised to "f*** this place up".

The chart topper, urged by a member of her entourage not to answer the journalist's "poor" question about what had happened to trigger the illness, dryly added: "But when people are lying on the internet saying you've had an overdose, that makes you feel better."

She said: "I'm feeling good, I'm alive, so that's f*****g awesome. Look at me, I'm the poster child for good health.

"I've been laying down for three weeks. I'm gonna go off - I'm like a star waiting to explode.

"I didn't have a drug overdose. I took some s****y antibiotic my doctor gave me and I had an allergic reaction.

"I already had a death in my family so my immune system was already low."

She said fans could expect the unexpected.

"I'm gonna f*** this place up," she said. "We're gonna have a lot of fun. We're gonna be, like, tops off, f*****g screaming.

"You have no idea how ready I feel."

Cyrus has already spent her short time in the UK sightseeing ahead of tonight's gig at London's O2 Arena.

(Image: Instagram/mileycyrus)

Her on-stage antics and risque costumes have divided audiences, a reputation compounded by her provocative twerking at last year's VMAs.

Joking with journalists today, the former Hannah Montana star said strippers had enquired about where to get similar outfits.

She added: "You get a lot more famous if you show your t**s."

And she joked that many people thought her album and tour - which share the same name - where based on either the British dish bangers and mash, or a euphemism for breasts.

Neither was true, but Cyrus said there would be a strong English presence through her set.

She said: "I'm doing an Arctic Monkeys cover while I'm here. I was hoping they were gonna come but obviously they are in Australia on tour."

The tour, which will also feature a cover of a Coldplay song, continues in Leeds on Saturday and Glasgow, Manchester and Birmingham next Monday, Wednesday and Friday respectively.

Watch how excited she is for tonight here: