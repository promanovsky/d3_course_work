Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Bickering among couples can be a real health hazard and stressed out men are literally being nagged to death, according to new research.

Men subjected to nagging, constant demands and worries from their partners are twice as likely to die young than blokes who are less henpecked, Danish scientists claim.

But women are more immune from high maintenance partners as there is little effect on their death rates.

Some 10,000 men and women aged 36 to 52 from Denmark were quizzed about who made the most demands on them and caused the most conflict.

Researchers found that out of every 100,000 people, 315 deaths could be caused by spousal demands and worries.

The findings were published in the Journal of Epidemiology & Community Health.

Dr Rikke Lund, Section of Social Medicine, Department of Public Health, at University of Copenhagen, said: "It is interesting that we have identified that males who are exposed to worries and demands by their partners have higher mortality and are the ones we should focus on.

"We tend to struggle to reach this group with public health interventions and maybe we should be focused less on the individual and more on social networks as a whole."

Dr Lund suggested that men have fewer people in their social life and so less people to share their worries with.

He said: "Their partner is more important to them in a relatively small social network."

Stress is known to have physical effects on health, increasing the risk of heart disease and strokes and leading to "comfort eating" of junk food.