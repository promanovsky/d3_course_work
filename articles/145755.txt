British model Emma Appleton has claimed that celebrity photographer Terry Richardson promised her the opportunity to pose for Vogue in exchange for sex.

Appleton said the offer was made from Richardson's Facebook account on Sunday and involved a photo shoot in his hometown of New York City. Before deleting her account, Appleton uploaded a screenshot of a message sent to her via Twitter and Instagram.

"If I can f*ck you I will book you in ny for a.shoot [sic] for Vogue," the message from Richardson allegedly read.

The model then explained in more detail to her followers what happened after the offer was made. "I was like whaat [sic] and he said yes or no?" she replied to one Twitter user. When asked what her response was, she said she declined the offer with "no, I'm not your girl, bye ha."

"I've been modeling for 5 years and I've never had this before, it doesn't make it okay," she tweeted. "Beginning to wish I hadn't posted that... it doesn't matter who you are or the what the industry is, just be a decent human being."

"The fact people think this is acceptable blows my f*cking mind," she added.

Richardson's spokeswoman Candice Marks claimed the message was not real and denied her client's involvement. "This is obviously a fake. Terry did not send this text," said Marks. A spokesperson for Vogue said the magazine would not be commenting on the situation at the moment.