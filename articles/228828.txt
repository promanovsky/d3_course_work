Spoiler alert: these are the full explanations to the answers in our ultimate Mad Men quiz. Don't read on unless you have given it a go.

1: b) While eating lunch in For Those Who Think Young (season two, episode one), Don sees a beatnik reading the collection. When he asks him what's he reading, the man replies that he wouldn't like it. Don buys the book and, after reading, mails it to an unknown recipient, later revealed to be Anna Draper. The titular poem also provides the title for the final episode of season two.

2: a) The carousel first becomes a clear motif during Don's presentation for Kodak. The metaphor comes to represent Don's inability to escape the various cycles of deception and dishonesty – and inability to love – that first began during his childhood. A recent episode (season seven, episode four) closes with the song On a Carousel by The Hollies.

3: c) Numerous shots, images and costumes throughout season six visually align Sylvia with the women of the brothel.

4: d) For more on the storied history of Miss Porter's, check out the fascinating Vanity Fair profile The Code of Miss Porter's.

5: d) Don brought the dog home at the end of season one, episode three, The Marriage of Figaro.

6: c) Don Draper's New York apartment also resembles the "Ultimate Playboy Townhouse" featured in a 1962 issue of the magazine.

7: c) Mason Vale Cotton, Maxwell Huckabee, Aaron Hart, and Jared Gilmore have all played Bobby Draper.

8: a) Much of season four revolves around the attempt to maintain the relationship with Lucky Strike – see especially Christmas Comes But Once a Year (season four, episode two) and recover from its loss.

9: c) Betty is disdainful of Bishop's campaign work for Kennedy, and she certainly has complicated feelings about Glen, but her real animosity stems from her own fear of being alone.

10: c) The blog masters at Tom & Lorenzo have done a superb job of tracking the correlation between Joan's roses and the means in which they signify love.

11: c) The men who wear hats in Mad Men (Don, Roger) are one generation older than the men who do not (Pete, Harry, Stan, Ginsberg); their hats (and their old-fashioned suits) indicate their inability to adapt to the changing culture of the 1960s.

12: a) Like Don, Bob inveigled the men of Sterling Cooper into hiring him without merit; like Don, Bob is a man of uncertain and certainly fabricated past; like Don, Bob is tremendously resourceful and relies heavily on his charm and charisma.

13: a) "Wasp" (white Anglo-Saxon Protestant): the term is used to describe men and women of Pete's patrician attitude and standing.

14: c) In The Jet Set (season two, episode 11), Peggy agrees to accompany Kurt to a Bob Dylan concert. He comes over to her place in Brooklyn beforehand, and ends up cutting off her ponytail.

15: c) The Barbizon, known as "the sorority on 63rd Avenue", had a storied reputation and housed young, single women from the 1920s through the 1960s. Read more here.

16: a) The assassination of King is the focus of The Flood (season six, episode five); the Cuban missile crisis structures Meditations in an Emergency (season two, episode 13), and the death of Marilyn Monroe features in Six Month Leave (season two, episode nine).

17: c) Nelson Rockefeller served as the governor of New York before eventually serving as vice-president under Gerald Ford. Francis works as an aide to the governor's office and refers to him as "Rocky".

18: d) A Day's Work (season seven, episode two) takes place on Valentine's Day, Dark Shadows (Season five, episode nine) takes place on Thanksgiving, and Three Sundays culminates in Easter. Mad Men episodes often centre around American holidays, including Derby Day, Labor Day, fourth of July, and Halloween.

19: d) Peggy, Lois, Jane, Joan, Alison, Miss Blankenship, Megan, Caroline, Dawn, Meredith.

20: b) According to Patrick Fischler, who played Barrett, "Don Rickles is the one most people think. The Don Rickles part is just the insults; he's famous for that. I watched some Joey Bishop – I don't know if this came across or not, but I wanted [Jimmy] to be charming at the same time so he wasn't just hated. I wanted there to be some element of him being appealing to some people, and Joey Bishop had that."

21: d) The painting features in The Gold Violin (Season two, episode seven) when employees are asked to offer their "thoughts" on the painting. Jane, Cosgrove, Sal, and Harry all sneak into Cooper's office after hours to view it; when Joan finds out that Jane broke into the office, she fires her – an action that precipitates Jane and Roger's marriage.

22: d) Kevin is Joan and Roger's son, Tammy is Pete's daughter, Eddie is Cosgrove's son.

23: b) The story, Tapping a Maple on a Cold Vermont Morning, prompts Pete to attempt to get his own story published via Trudy's ex-boyfriend.

24: d) Ogilvy is referenced repeatedly throughout the seven seasons; his book, Confessions of an Advertising Man, was a bestseller and highly influential in the US advertising industry.