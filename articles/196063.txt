The U.S. Federal Reserve is in no rush to decide the appropriate size of its balance sheet, but if it ultimately shrinks it to a pre-crisis size, the process could take the better part of a decade, Fed Chair Janet Yellen said on Thursday.

Yellen, in testimony to a Senate panel, said no decision had yet been made on the central bank's portfolio of assets, which has swollen to $4.5 trillion from about $800 billion in 2007.

Three rounds of asset purchases meant to stimulate the economy in the wake of the 2007-2009 financial crisis have boosted the balance sheet to this record level. Unsatisfied with the U.S. recovery, the Fed is still adding $45 billion in bonds each month, though the purchases should end later this year.

Yellen said the portfolio should start to shrink once the Fed decides to raise near-zero interest rates.

Editor's Note:

38 Investments That Have a 96% Win Rate

"We've not decided, and we'll probably wait until we're in the process of normalizing policy to decide, just what our long-run balance sheet will be," she told lawmakers, adding it will be "substantially lower" than it is now.

While the central bank could sell the mortgage-based bonds it has accumulated, in the past it has telegraphed that it would more likely simply stop re-investing funds from expired assets and then, over years, let the assets run off the balance sheet naturally.

"If we do that and nothing more, it would probably take somewhere in the neighborhood of five to eight years to get it back to pre-crisis levels," Yellen said of halting reinvestments.

It was the Fed's most explicit time frame yet for the delicate task of shrinking its balance sheet to a more normal level. The massive portfolio has sparked worries that, once the Fed starts to raise rates, inflation will shoot up. The Fed could also absorb losses if it decides to sell assets in the years ahead, a potential political headache for Yellen.

The five-to-eight-year timeline generally aligns with estimates of both private economists and a paper published last year by top Fed economists. A paper by JPMorgan economists predicts shrinking the Fed's portfolio would take seven years.

A COMMUNITY BANKER FOR FED BOARD

Yellen, in her second day of testimony before lawmakers, again stressed that low inflation drags on economic growth. But she rejected the idea, floated by some private economists, of trying to boost inflation above the Fed's 2-percent target to ratchet down unemployment, saying it was critical to keep inflation expectations firmly anchored.

Inflation has been running just above 1 percent in the world's biggest economy while unemployment, albeit falling, is still elevated at 6.3 percent.

Asked repeatedly about fiscal policies, Yellen took a page from her predecessor Ben Bernanke and urged the Congress to address the nation's long-term budget challenges, warning that the current course was unsustainable.

"We can see that going out 20, 30, 50 years without some further shifts in fiscal policy, it's projected that the ratio of debt to GDP will rise to unsustainable levels," Yellen told the Senate Budget Committee.

"I would join my predecessor in saying that I do think it's important that the Congress address that issue," she said, adding recent tightening of fiscal policy had been one of the "headwinds" that had undercut the Fed's efforts to foster a stronger economic recovery.

In another exchange, Angus King, a democrat from Maine, asked Yellen if she would favor the addition of a community banker to the Fed board, which currently has only four of its seven seats filled.

"Certainly I am in favor of that," Yellen said, noting recently retired governors Elizabeth Duke and Sarah Bloom Raskin were very familiar with small banks. The Fed governor said she had conveyed her desire for a community banker on the Fed board to the White House, which nominates central bank governors.

Reuters reported last month that the White House was considering a former banking lawyer, as well as two others with direct community-banking experience, as possible nominees to the depleted Fed board.

Report:

Buffett Made 96% of His Wealth After 60