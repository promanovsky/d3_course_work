'Fast & Furious 7' to Film in Abu Dhabi

The Universal sequel will shoot in the capital of the United Arab Emirates in April.

Director James Wan's Fast & Furious 7 will soon resume production overseas.

The Universal title is set to be filmed in Abu Dhabi in April, according to a statement by The Abu Dhabi Film Commission and Twofour54, part of the United Arab Emirates city's media initiative. Stars Vin Diesel, Michelle Rodriguez, Tyrese Gibson and Chris "Ludacris" Bridges are said to be traveling to the locale.

"The transition into that Dom state of mind has always been an interesting one … only this time there is added purpose, a collective goal to make this the best one in the series," Vin Diesel wrote on Tuesday. "P.s. The long awaited completion of Seven, begins…"

Fast 7 has been subject to script rewrites since star Paul Walker died in a car crash in Valencia, Calif., on Nov. 30, 2013. In early January, sources told The Hollywood Reporter that Walker's character, Brian O'Conner, wouldn't be killed off in the film but will be "retired."

Footage for the action thriller had previously been filmed in Abu Dhabi in November 2013, the Twofour54 statement noted.

The film had originally been scheduled for release on July 11, but it was pushed back to April 10, 2015.