NEW YORK (TheStreet) - Burger King (BKW) and Tim Hortons (THI) investors are certainly cheering the prospects of a merger between the fast-food burger chain and the Canadian doughnut-and-coffee outlet that could threaten McDonald's (MCD) - Get Report supremacy in the quick-service food industry.

Shares of Burger King were surging 22% to $33.11, hitting a 52-week-high while Tim Hortons was rising 21% to $76.28, also hitting a new 52-week-high.

Burger King and Tim Hortons on Sunday disclosed that they're in discussions about a potential merger. If a new publicly-traded company is formed, it would be headquartered in Canada (Tim Hortons is based in Oakville, Ontario), making Burger King yet another U.S. company looking to move its headquarters out of the U.S., ostensibly to lower its tax rate. That's one reason equity traders appear to like the prospect for a merger.

But there are other reasons investors and consumers like this combination.

Watch the video below for more on what a merger of Burger King and Tim Hortons would mean:



WATCH: More market update videos on TheStreet TV

Tim Hortons operates 4,546 restaurants, mainly in Canada and in the U.S. Northeast while Burger King has more than 13,000 franchised locations in 98 countries. A combined Burger King and Tim Hortons would have 18,000 restaurants in 100 countries with about $22 billion in sales. The companies said a merger would create the world's third-largest fast-food restaurant company.

If a deal is consummated, Burger King and Tim Hortons would continue to operate as standalone brands, while "benefiting from shared corporate services, best practices and global scale and reach," the release says. "A key driver of these discussions is the potential to leverage Burger King's worldwide footprint and experience in global development to accelerate Tim Hortons' growth in international markets."

And while the two companies could probably create efficiencies in marketing, operations and streamlining their supply chains, here are three other reasons a deal could be yummy for both consumers and investors.

1. Breakfast is hot, hot, hot

A merger of Burger King and Tim Hortons is likely to produce innovative additions to both menus. We could see an offering of chicken-and-doughnuts at Tim Hortons, or a Canadian Maple doughnut-inspired shake at Burger King. It's been done before at other co-owned brands.

The move to merge appears to be borne of the need to boost the overall quality of the restaurants' offerings to better compete with fast-casual outlets like

Chipotle

(CMG) - Get Report

,

Panera Bread

(PNRA)

and

Smashburger

, among others.

McDonald's

and

Wendy's

(WEN) - Get Report

are under pressure to do the same.

Burger King's focus here is on breakfast. In its long rivalry with McDonald's, which has had a solid offering in the breakfast space for years, Burger King is anxious to carve out more morning customers.

Yum! Brands

(YUM) - Get Report

Taco Bell launched

its line of breakfast in March, which included the Waffle Taco, A.M. Crunchwrap and Breakfast Burrito.

And while Burger King has also had a breakfast offering, it needs to do more, says Sam Oches, managing editor of QSR Magazine, a trade publication based in Chapel Hill, N.C.

"These brands have been in the breakfast space forever but have not had anything innovative," Oches said in a phone interview. "In the last five to 10 years as you see lunch and dinner menus [move to] more premium fast casual [ideas] breakfast lagged behind."

Brands have realized the importance in winning customers earlier in the day, getting them accustomed to returning to the same outlet, Oches says.

Breakfast is "a huge opportunity for growth" and Burger King is "struggling right now to create brand excitement," he adds. "A lot of Burger King menu moves sort of come in the shadow of McDonald's. By adding Tim Hortons to the fold Burger King has a lot of potential to get into breakfast in a big way."

2. Coffee!

Even though Burger King sells a reasonable cup of coffee in Seattle's Best, it has yet to attract a devoted following in the manner of

Starbucks

(SBUX) - Get Report

,

Dunkin Brands

(DNKN) - Get Report

or even McDonald's McCafe line. So, enter Tim Hortons.

"Tim Hortons coffee is like an addiction," Oches says. Lots of morning customers, go to Tim Hortons for its coffee. They really like, and Burger Kings likes that. As popular as Dunkin and Starbucks coffees are in the U.S., "that's what it's like with Tim Hortons in coffee in Canada and in northern parts of the U.S.," Oches added. "I've talked to people in Buffalo -- Tim Hortons is a way of life [for them.]"

Ten years ago, Dunkin was a donuts brand, now it's coffee retailer. Morning java drives many customer visits, especially those with drive-through locations. Coffee is a great driver for incremental sales. Burger King's deal with Starbuck's Seattle's Best would likely be supplanted if a deal with Tim Hortons is consummated, Oches notes.

Another positive - Tim Hortons already sells packaged coffee, both ground coffee and single K-Cups - an area that is increasingly getting populated by restaurant brands. McDonald's said last week that it would be

selling packaged coffee

through a partnership with

Kraft

(KRFT)

, starting in 2015.

3.The SPAC is Back

Burger King's

potential deal for Tim Horton's indicates the special purpose acquisition vehicle (SPAC) is back after hibernating in Wall Street's wilderness for a decade.

Bill Ackman's Pershing Square Management and billionaire Nicolas Berggruen used a SPAC called

Justice Holdings

to take a 29% stake in Burger King in 2012 for $1.4 billion. As part of the deal, Pershing Square became Burger King's second-largest shareholder with a 10% stake, and Ackman became the burger joint's seventh-largest shareholder with a 1% stake in the company.

Increasingly, it appears the deal is poised for juicy returns. Burger King now trades at a market capitalization in excess of $10 billion, indicating that the SPAC deal has more than doubled in value in roughly two years since its close.

Burger King and its owners, 3G Capital, used Pershing and Berggruen's $1.4 billion SPAC investment as a way to return the company to stock markets. Other companies have since followed suit.

Platform Specialty Products

(PAH) - Get Report

, a chemicals industry specialist, listed on the

New York Stock Exchange

shortly after taking a 29% investment from Pershing Square and cutting its first-ever acquisition, the takeover of MacDermid. Since listing in January, Platform has continued to aggressively cut deals, acquiring Chemtura's AgroSolutions business for $1 billion in cash and stock. Shares in the SPAC have risen over 72% year-to-date.

Bill Ackman's refashioning of the SPAC began with Burger King. Other deals to emerge like Platform and

Quinpario Acquisition Corp

.

(JASN) - Get Report

may also prove

compelling stocks for investors to consider

.

--Written by Laurie Kulikowski and Antoine Gara in New York.

Follow @LKulikowski

Disclosure: TheStreet's editorial policy prohibits staff editors, reporters and analysts from holding positions in any individual stocks.