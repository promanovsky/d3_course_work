Project Morpheus hands-on: Wearing PS4’s VR future

GDC may not be over for 2014, but Sony undoubtedly stole the show with Project Morpheus, its new virtual reality headset for the PlayStation 4. Still in prototype form – and not expected to launch this year, Sony has confirmed – Project Morpheus saw round-the-booth queues of eager developers and media hoping to try out the roster of demo games prepared for the show. The good news is that, while incomplete today, there’s plenty to warrant excitement as we found when we tried out Project Morpheus ourselves.

Sony’s headset design is subject to change, but looks more akin to something from the company’s wearable display team than the PlayStation arm. That’s not really much of a surprise, given headsets like the Sony HMZ-T3W have helped educate Project Morpheus’ 3D stereoscopic display technology.

Putting the headset takes a little getting used to, particularly with slightly nervous Sony booth staff hovering around; two of the eight demo units out for the show broke during day one of demos, confirming their prototype status.

The black rear band pulls around the back of the head, while the top section rests on the upper forehead and keeps the bulky eyepiece positioned in front of you. The straps then ratchet tight, to keep it from moving around. It’s surprisingly not uncomfortable, with Sony taking care to design it so that it applies no pressure on the nose or cheeks, and we were able to wear it over prescription glasses without issue.

A switch under the front right edge allows the eyepiece to slide forward and backward, bringing the optics closer to, or further away from, your eyes. If you get that wrong, you can end up seeing either the ground underneath you or curved black bars at the side of the display.

Get it right, however, and it’s surprisingly how quickly you get used to being immersed in a virtual world. Sony had two demos for us to try: The Castle, in which you hack apart dummies wearing suits of armor using swords and a crossbow, and The Deep, which sinks you underwater in a diving cage that falls victim to some shark attention.

Project Morpheus and the PlayStation camera tracks movement of your head using the lights front and back – for 360-degree support – and other internal sensors, but for anything else you need one or more controllers. In The Castle, it’s the PlayStation Move that does duty for your hands, or more accurately two of them. Inside the headset you see your hands outstretched, and the trigger buttons on the Move wands control grip.

That way, we were able to look down, spot a sword standing upright next to us, and grab it, before going to work on the poor dummy in front of us. It’s tough to tell exact accuracy at this stage – “gameplay” as it was proved more to be hacking and slashing than precision swordsmanship – but it’s certainly fun, and we soon found ourselves wishing we could rip apart the armor with our “bare hands” or otherwise step out of the relatively limited options in the demo.

We also noticed a little calibration drift over time: sometimes, when we reached down for a sword, there was some jumping as the Move wand was poorly recognized or didn’t quite match up to where our hands were.

In The Deep, meanwhile, the DualShock controller steps in instead, though its only purpose was to move around and fire a flare gun. Here, what was particularly impressive was the full body tracking: looking down and seeing our wetsuit-clad torso and legs, then stepping over to the safety rail and peering down and around to see the underwater world.

Sony says that, aside from gaming, it envisages virtual tourism to be a key use for Project Morpheus when it launches – sending PS4 owners around the world or even, thanks to a collaboration with NASA, to Mars – and The Deep demonstrates that potential well. Combined with rich audio, it certainly proved to be immersive, though the fact that the display is grainier compared to Oculus Rift does jar a little at times.

While Oculus’ Rift may be the obvious comparison, Sony representatives insist that they feel a sense of solidarity, rather than competitiveness, with the VR startup. In fact, one on the Sony team told us, Oculus had “been surprised” by how open Sony was with its development and findings around VR; given Rift is a PR peripheral, and Project Morpheus is intended for the PS4, the two aren’t seen internally as direct competitors.

That won’t necessarily mean working together on VR any time soon; Sony has said that the two companies are collaborating only “in spirit”, despite the obvious appeal for gamers and game studios of a virtual reality standard.

Project Morpheus vs Oculus Rift DK2 – everything you need to know

It’s undoubtedly early days for Project Morpheus. The hardware and physical design aren’t settled upon, and won’t be for a while yet, and Sony has said that everything – including display resolution – could potentially change before the consumer version is released.

Having solid hardware is only the first step, however; without games and other software to make use of it, Project Morpheus is unlikely to find its way into PS4 owners’ homes. Judging by developer interest at Sony’s GDC 2014 booth, there’s undoubtedly curiosity there. Whether that translates to actual titles being developed remains to be seen.