The Turkish prime minister's decision to block access to Twitter is "not acceptable," the EU's digital commissioner Neelie Kroes told CNBC, stressing that she would do her utmost to make sure the people of Turkey had freedom of speech. Speaking to CNBC on Friday, Kroes said the move by Tayyip Erdogan was "embarrassing."



"It's cutting down freedom of speech - a value we're so proud of in the free world. He's going in the wrong direction," she said. "What we have to do is put it in plain language that it's not acceptable." On Friday, Turkey's courts ruled in favor of a ban after Erdogan threatened to "wipe out" access to Twitter in a speech, just 10 days ahead of key local elections. Shortly after, users of the website reported widespread outages across the country. Twitter users soon took to the micro-blogging website to condemn the move, using the searchable hashtag #TwitterisbannedinTurkey. Tweet 5 Tweet 1

And even Turkey's President Abdullah Gül expressed disapproval in a tweet that can be roughly translated as: "A complete closure of social media platforms cannot be approved of."

Tweet 3