Today in San Francisco Intel conducted an event in which many Chromebook related announcements were made by the company and its partners. Google, the company behind Chrome OS, also made an announcement. Within the next few weeks it is going to add offline support to Google Play Movies for Chrome OS. This would certainly be welcomed by anyone who uses the heavily internet dependent operating system.

Advertising

The reason why Chromebooks are so competitively priced is that the don’t need conventional notebook level hardware. That’s because the entire operating system and most of the applications live in the cloud. The obvious downside is that users require a reliable internet connection to truly be able to use the Chromebook for some actual work.

This limitation also hinders entertainment as well. While users can already save movies and TV shows from Google Play Movies for offline viewing on Android devices they can’t do that on Chromebooks. Following the addition of this support users will then be able to view movies and TV shows that they have saved even when there’s no internet connection.

The inability to do became one of the major sources of criticism for Chrome OS, which has otherwise been doing quite well. Chromebooks sales in the U.S. significantly increased last year as opposed to conventional notebooks, signalling that there is market for cheap laptops with a cloud based OS.

Filed in . Read more about Chrome Os and Chromebooks.