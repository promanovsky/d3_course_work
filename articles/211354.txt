Worldwide, 3.3 million deaths in 2012 were due to harmful use of alcohol, says a new report launched by WHO on Monday. Alcohol consumption can not only lead to dependence but also increases people's risk of developing more than 200 diseases including liver cirrhosis and some cancers. In addition, harmful drinking can lead to violence and injuries, according to the "Global status report on alcohol and 2014".

The report also finds that harmful use of alcohol makes people more susceptible to infectious diseases such as tuberculosis and pneumonia.

The report provides country profiles for alcohol consumption in the 194 WHO Member States, the impact on public health and policy responses.

"More needs to be done to protect populations from the negative health consequences of alcohol consumption," says Dr Oleg Chestnov, WHO Assistant Director-General for Noncommunicable Diseases and Mental Health. "The report clearly shows that there is no room for complacency when it comes to reducing the harmful use of alcohol," he added.

The report noted that some countries are already strengthening measures to protect people. These include increasing taxes on alcohol, limiting the availability of alcohol by raising the age limit, and regulating the marketing of alcoholic beverages.

On average every person in the world aged 15 years or older drinks 6.2 liters of pure alcohol per year. But as less than half the population (38.3 percent) actually drinks alcohol, this means that those who do drink consume on average 17 liters of pure alcohol annually.

The report also points to the fact that a higher percentage of deaths among men than among women are from alcohol-related causes - 7.6 percent of men's deaths and 4 percent of women's deaths.

Globally, Europe is the region with the highest consumption of alcohol per capita, with some of its countries having particularly high consumption rates. Trend analysis shows that the consumption level is stable over the last 5 years in the region, as well as in Africa and the Americas, though increases have been reported in the South-East Asia and the Western Pacific regions.

For comments and feedback contact: editorial@rttnews.com

Political News