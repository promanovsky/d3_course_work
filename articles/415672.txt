Nicki Minaj narrowly missed flashing a nipple whilst performing at the MTV Video Music Awards last night (24th August). The 31-year-old was on stage performing with Ariana Grande and Jessie J when her dress became undone and she was forced to complete her appearance clutching the top of her dress.



Nicki Minaj had problems with her revealing dress whilst performing at the 2014 VMAs.

Read More: Ariana, Iggy, Nicki and Miley! What To Expect From The 2014 MTV Video Music Awards.

The singer addressed the incident on Instagram, adding a picture of her leaving the stage. Minaj wrote "God is good. As long as a nipple didn't come out to play, I'm fine." She also took the opportunity to thank her team for their support, listing their names and writing: "Many more to name but they're all winners! No weak links. There's no greater feeling than having people who inspire you and who work passionately and relentlessly to get the job done."

Minaj opened the festivities, held at The Forum in California, performing 'Anaconda'. For her opening number she wore a skimpy green bikini but quickly changed so she could join Grande and Jessie J on stage for a rendition of 'Bang Bang'.



The singer had a narrow escape during her performance owing to her costume change.

Her stage manager explained, as E! reports, how little time Minaj had between performances to change. Manager Gary Hood estimated she had two minutes to change her costume but it evidently wasn't enough time. Apparently the issue had arisen in rehearsals but Minaj had decided to go ahead anyway. Hood said "Actually the same thing happened in the dress. She didn't quite make it. I mean, each of the performers had about two minutes. There just wasn't enough time for her."

Read More: Nicki Minaj's Backup Dancer Bitten By 6-Foot Boa Constrictor.