Samsung has packed in more power, an even better display and a great camera making the Note 4 for an excellent smartphone. It'll be too big for some and the S Pen is still questionable, but overlook this and you'll be happy.

Samsung Galaxy Note 4 is an older Android phone that remains valued for its durable, plastic design – the best Note smartphone to have this fake leather look to it vs the Note 5 that went with an all-glass look. If you can find it used, it's a lot cheaper than it was when it first launched, and won't explode on you like the Note 7 did last year. Here were our original thoughts on the Note 4 and its S Pen features.

Some said it would never catch on, but over four years on from the original Galaxy Note phablet the fourth generation device - the Samsung Galaxy Note 4 - launched.

The Galaxy Note's big screen and S Pen stylus are certainly not for everyone. The Samsung Galaxy S7 and Galaxy S7 Edge offer a more mainstream (and updated) smartphone setup, while the Note 4 brings together big power, a big screen and big productivity - even if it is a little long in the tooth now.

If you're after a top-end smartphone which won't break your palm, pocket or handbag then you're probably in the wrong place - this isn't even the best Note handset anymore - but the Galaxy Note 4 still has a lot to offer.

You can now pick it up for under £400, $400 (around AU$500) if you don't want to be tied down to a contract. That makes it decidedly cheaper than the Note 5, Note 7, S7 and S7 Edge.

The Galaxy Note 4 - king of the phablets?

The Samsung Galaxy Note 4 is up against the likes of the Nexus 6, LG G Flex 2, Huawei Ascend Mate 7 and the steeply priced iPhone 6 Plus.

It's also no longer the newest flagship in Samsung's fleet with the introduction of the Galaxy Note 5, while the Galaxy S7 and Galaxy S7 Edge provide the most cutting edge tech and an improved design.

Read our hands on: Samsung Galaxy Note 7 review

Unlike the previous three iterations Samsung hasn't deemed it necessary to increase the screen size of the Note 4, so it sticks with the same 5.7-inch dimensions of the Galaxy Note 3. It's not the same screen though, as Samsung has given the Galaxy Note 4 a hefty resolution boost - but more on that on the next page.

When it comes to design Samsung has definitely listened about its latest line-up feeling plasticky in the hand and has decided to give the Note 4 more of a premium finish.

The metal frame runs round the outside

There's a metal rim surrounding the handset, shielding the rest of the chassis like a velvet rope protecting celebs from real people in a club.

It sports exactly the same shape, style and rounded corners as the Galaxy Alpha, only on a bigger scale and thanks to the increased size the plastic rear is more noticeable here than it is on the Alpha.

Samsung has tried to make the removable plastic cover feel more premium by giving it a leather effect finish, but there's still no fooling your hand with that unmistakable texture.

It's not on the same level of design as the Galaxy S6 and S6 Edge which ditch plastic altogether, so if you really don't like this cheaper material then consider these two instead. The new Note 5 has taken the design a step further by mirroring the all metal and glass stylings of the Galaxy S6. If you're looking for the most premium phablet then head over to the newer Note in the series.

Still all plastic on the back.

A plus side to this though is the plastic rear does provide good grip - vital when you've got such a large handset in your hand and even though the Galaxy Note 4 is a touch wider than the all metal iPhone 6 Plus, I found the Samsung is easier to hold.

Even though the 6 Plus has a smaller, 5.5-inch display, it's actually taller than the Note 4 - something else the Samsung has over its Apple rival as it makes the phone more balanced in the hand.

Add to that the placement of the power/lock key on the right of the Galaxy Note 4 and you find that even for a big phone the key buttons are still easy enough to reach (if your palms are on the larger side).

It does have a lovely screen.

The Samsung Galaxy Note 4 measures 153.5 x 78.6 x 8.5mm, almost the same as its predecessor and smaller in width and depth than the Galaxy Note 2, which is impressive considering this is the first of the Note series to sport a metal frame.

I've also been using the 5.5-inch OnePlus One recently and there's really not a lot to choose between the two in terms of size.

It's safe to say then, if you're already accustomed to the larger league of smartphones then the Galaxy Note 4 will feel right at home in your palm.

There's a fingerprint scanner hidden here

I really like the way the Galaxy Note 4 feels in the hand and having used the Note 3 on a number of occasions, it definitely feels different and refined enough - plus it's light too, just a touch over the weight of the Note 3 at 176g.

Samsung's tried and tested physical home key still resides at the bottom of the screen, providing a location for the Korean firm to stuff in its fingerprint scanner, and that's flanked by two touch keys.

To the left of the home button is the Multi-tasking key, while on the right you get Back. Both Back and Multi-tasking are completely hidden when not illuminated by the backlights, providing a clean finish to the front of the Galaxy Note 4.

The Note 4 can be tricky to use one-handed

Some shuffling of the Note 4 in the hand is required to get your fingers in the right position to reach these during one handed use, and you'll need to be careful not to drop it.

Round the back you'll find a 16MP camera, LED flash and the heart rate monitor which also features on the Galaxy S5, S5 Mini and Galaxy Alpha.

In the bottom corner you'll also notice the end of the S Pen stylus hiding inside the body of the Galaxy Note 4, while behind the removable rear cover there's a microSIM port, microSD slot and a sizable 3220mAh battery.

You'll find a 16MP camera on the back

This, then, was the Note handset with the most premium and accomplished look and feel to date. Samsung is finally providing the build quality its top-end devices have been yearning for and it's since gone even further towards premium perfection with the Galaxy Note 5.

Samsung has managed to keep the dimensions manageable - it's certainly a lot easier to hold than the 6-inch Lumia 1520 and Ascend Mate 7 - and while the iPhone 6 Plus may look sleeker, the Galaxy Note 4 boasts a bigger, better screen and very similar dimensions.