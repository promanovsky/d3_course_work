SAN DIEGO, March 13 (UPI) -- Facebook moods are similar to face-to-face moods, positive posts beget positive posts and negative posts beget negative ones, U.S. researchers say.

Lead author James Fowler, a professor at the School of Medicine at the University of California, San Diego, and colleagues analyzed more than a billion anonymized status updates among more than 100 million users of Facebook in the top 100 most populous U.S. cities over 1,180 days, from January 2009 to March 2012. The researchers used a software program to measure the emotional content of each post.

Advertisement

Similar to face-to-face emotions, positive posts were more influential and more contagious, the study found.

"Our study suggests that people are not just choosing other people like themselves to associate with but actually causing their friends' emotional expressions to change," Fowler said in a statement.

"We have enough power in this data set to show that emotional expressions spread online and also that positive expressions spread more than negative."

The researchers said their findings have widespread implications. Emotions, "might ripple through social networks to generate large-scale synchrony that gives rise to clusters of happy and unhappy individuals."

As a result, "we may see greater spikes in global emotion that could generate increased volatility in everything from political systems to financial markets," the study said.

The researchers also suggested their findings are significant for public well being.

"If an emotional change in one person spreads and causes a change in many, then we may be dramatically underestimating the effectiveness of efforts to improve mental and physical health," Fowler said. "We should be doing everything we can to measure the effects of social networks and to learn how to magnify them so that we can create an epidemic of well being."

The findings were published in the journal Plos One.

RELATED Facebook adds options to identify user gender