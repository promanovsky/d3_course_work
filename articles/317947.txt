NEW YORK (TheStreet) -- Barnes & Noble (BKS) - Get Report stock is popping on Wednesday after the company announced the separation of its retail segment from its Nook Media business. The books retailer said the board had authorized the spinoff with the view of optimizing shareholder value.

The spinoff is expected to reach completion by the end of the first quarter of 2015.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

"We believe we are now in a better position to begin in earnest those steps necessary to accomplish a separation of NOOK Media and Barnes & Noble Retail. We have determined that these businesses will have the best chance of optimizing shareholder value if they are capitalized and operated separately," said CEO Michael Huseby in a statement.

Guggenheim Securities has been enlisted as financial advisor, while Cravath, Swaine & Moore is acting as legal counsel.

By late afternoon, shares had added 6.4% to $21.87. Trading volume of 3 million shares was triple its three-month daily average.

Must read:Warren Buffett's 25 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

BKS Price

data by

YCharts