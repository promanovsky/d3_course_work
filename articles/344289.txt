Jamie Dimon, CEO of US banking giant JPMorgan, has been diagnosed with throat cancer, but revealed it was curable and he would continue in his post.

In a memo sent to employees and shareholders on 1 July, Dimon, 58, said the ailment is at an early stage, adding that it had not spread beyond the original site and adjacent lymph nodes on the right side of his neck.

He underwent tests including a CAT scan, PET scan and a biopsy, and will begin radiation and chemotherapy treatment shortly at Memorial Sloan Kettering Cancer Center, which would take about eight weeks to complete.

"I wanted to let you know that I have just been diagnosed with throat cancer. The good news is that the prognosis from my doctors is excellent, the cancer was caught quickly, and my condition is curable," he wrote in the letter.

"While the treatment will curtail my travel during this period, I have been advised that I will be able to continue to be actively involved in our business, and we will continue to run the company as normal. Our Board has been fully briefed and is totally supportive."

He added that he will let them all know if his health situation changes.

Dimon made the announcement at the 10-year anniversary of his tenure at the bank. He joined JPMorgan in 2004 when it merged with Bank One Corp., where he served as chairman.

The cancer was discovered after Dimon, who was not feeling well about two weeks ago, went to see doctors, Reuters reported citing a person familiar with the matter.

JPMorgan shares, which closed up 0.6% on 1 July, fell 0.6% in after-hours trading.

Dimon has been serving as CEO and chairman of the largest bank in the US since 2006 – the longest tenure by any chief at the bank. He was credited for successfully steering the bank through the financial crisis and reviving its business.

Recently, the bank reached a record $13bn settlement with US authorities over the sale of faulty mortgage-backed securities in the run up to the financial crisis. It also inked a $2bn settlement for helping fraudster Bernard Madoff in his Ponzi scheme.

Read the entire letter below:

Subject line: Sharing some personal news

Dear Colleagues and Shareholders -

I wanted to let you know that I have just been diagnosed with throat cancer. The good news is that the prognosis from my doctors is excellent, the cancer was caught quickly, and my condition is curable. Following thorough tests that included a CAT scan, PET scan and a biopsy, the cancer is confined to the original site and the adjacent lymph nodes on the right side of my neck. Importantly, there is no evidence of cancer elsewhere in my body.

My evaluation and treatment plan are still being finalized, but at this time it appears I will begin radiation and chemotherapy treatment shortly at Memorial Sloan Kettering Hospital, which should take approximately eight weeks. While the treatment will curtail my travel during this period, I have been advised that I will be able to continue to be actively involved in our business, and we will continue to run the company as normal. Our Board has been fully briefed and is totally supportive.

As you all know, we have outstanding leaders across our businesses and functions – the best team I've ever had the privilege of working with – so our company will move forward together with confidence as we continue to deliver first-class results for our customers, communities and shareholders.

I feel very good now and will let all of you know if my health situation changes.

I appreciate your support and want to thank our employees for the amazing work they do day-in and day-out. I'm very proud to be part of this company and honored to be working with such an exceptional group of people.

Jamie