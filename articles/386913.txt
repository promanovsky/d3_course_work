U.S. lawmakers have criticized the apparently poor safety procedures in handling dangerous pathogens at labs of the Centers for Disease Control and Prevention despite prior warnings the center reportedly had from investigators.

At a hearing by the House Energy and Commerce subcommittee on oversight and investigations, representatives were skeptical that recent accidents involving anthrax, flu and smallpox pathogens were the "wake-up call" described by center director Dr. Tom Frieden.

The committee heard of multiple prior inspections of a CDC lab that mishandled anthrax that uncovered numerous areas of concern, including lab workers wearing torn gloves and systems meant to clear air from labs that blew exhaust fumes the wrong direction, directly back into the labs.

"A 'wake-up call?' That is a gross and dangerous understatement," said Pennsylvania Republican Tim Murphy, chairman of the committee.

In June, workers at the CDC in Atlanta experienced potential exposure to living anthrax bacteria when a researcher preparing a sample in a high-security laboratory did not sterilize it properly before transferring it to a lower-level facility where workers did not have to proper safety equipment for working on live anthrax.

More than 60 workers were offered antibiotics after the realization of the mistake, the first time an incident at the center has required treatment for workers potentially exposed to a pathogen, Richard Ebright, a molecular biology researcher at Rutgers University, said.

Anthrax isn't contagious, he said, but an accident with a microorganism that is contagious could be a disaster.

"In the event of a similar incident with one of those biological weapons agent pathogens, now not only are workers at risk but the community is at risk as well, Ebright said.

CDC director Frieden said the center is implementing "sweeping measures" to increase security and safety at its laboratories, including prohibiting transfers of pathogens out of the agency's highest-security laboratories and temporary closing of its Bioterror Rapid Response and Advanced Technology lab.

Representative Diana DeGette, D.-Colo., said the measures should have been in place long before the recent incidents.

"The record shows that CDC had ample warnings and should have been focused on problems in their high-containment labs long before the June anthrax release," DeGette said at the hearing. "I think you should have known about them. The fact that you did not indicates that key officials did not instill a culture of identifying and reporting safety problems up the chain of command."

TAG CDC, Anthrax, smallpox

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.