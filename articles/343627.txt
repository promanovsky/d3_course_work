The 30s and 40s are the ages at which it's time to lose that "eat right, exercise, die anyway" mentality, put down the cigarette, and get moving: Recent research says even the natural progression of coronary artery disease can be reversed, regardless of lifestyle sins of the past.

"It's not too late," said Bonnie Spring, lead investigator of the study and a professor of preventive medicine at Northwestern University Feinberg School of Medicine in Chicago. "You're not doomed if you've hit young adulthood and acquired some bad habits. You can still make a change and it will have a benefit for your heart."

The study examined 5,000 adults who had participated in the Coronary Artery Risk Development in Young Adults (CARDIA) study 20 years before, when they were between the ages of 18 and 30.

Researchers from Northwestern Medicine assessed the lifestyle and coronary artery calcification levels of the former CARDIA participants, now between the ages of 38 and 50.

Healthy lifestyle was considered as not being obese or overweight, exercising regularly, not smoking and sticking to a healthy diet with a low alcohol intake.

At the beginning of the CARDIA study, all five of these principles applied to less than 10 pe rcent of participants.

Twenty years later, 25 per cent of them had added at least one of the aforementioned healthy behaviors.

Researchers concluded that each addition of a healthy behavior was linked with reduced detectible coronary artery calcification and reduced thickness of the two innermost layers of arterial walls, both critical factors in evaluating cardiac health.

According to Spring, many healthcare professionals believe patients are unable to change their behavior, and others believe the damage caused by smoking and other bad habits is irreversible.

"Clearly, that's incorrect," says Spring. "Adulthood is not too late for healthy behavior changes to help the heart."

On the flip side, 40 per cent of participants had lost healthy lifestyle habits and were consequentially more at risk for coronary artery disease.

"That loss of healthy habits had a measurable negative impact on their coronary arteries," Spring said. "Each decrease in healthy lifestyle factors led to greater odds of detectable coronary artery calcification and higher intima-media thickness."

In hopes that the results of her study will provide a hopeful public service announcement, Spring wishes to communicate the following advice to adults everywhere:



Don't smoke; maintain a healthy body weight; do at least 30 minutes of moderate to vigorous activity five times a week; stick to a healthy diet, high in fiber, low in sodium with lots of fruit and vegetables; no more than one alcoholic beverage per day for women, no more than two for men.

"Adulthood isn't a 'safe period' when one can abandon healthy habits without doing damage to the heart, says Spring. "A healthy lifestyle requires upkeep to be maintained."

The study was published June 30 in the journal Circulation.