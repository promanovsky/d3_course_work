Endocyte Inc. looks to be well on the path to having their first drug approved for use, with the 18-year-old Indiana pharmaceutical company receiving the go-ahead for the ovarian cancer drug from European Union regulatory advisors.

The drug, Vynfinit (brand name for vintafolide), is a treatment for recurring ovarian cancer, created for women who have not successfully responded to chemotherapies. The EU advisors put forward their recommendation today, with formal approval due within the next three months. As a result of the recommendation, Endocyte's stock shares hit almost double their value, reaching their highest every point of $33.70 and closing at $13.53 (92 percent), at $28.17.

Philip Low, Purdue University chemistry professor and founder of Endocyte, welcomed the news. "There's a lot of people dying from platinum-resistant ovarian cancer that may see an extension of their life or perhaps a cure," he said. He continued, saying that the decision "validates the fundamental principle of using homing molecules to carry an effective therapeutic drug to cancer cells."

Endocyte partnered with Merck to produce the drug, which is designed to be used in conjunction with the chemotherapy medication doxorubicin. Upon announcing their partnership in 2012, Merck paid Endocyte $120 million. It's projected that Endocyte will generate revenue in excess of $1 billion if Vynfinit becomes a viable option for various types of cancer.

Indeed, though the drug is currently intended to treat ovarian cancer, there may be other uses. It works by attacking a folate receptor that only occurs in cancer cells - a technique that may be mimicked to treat other variations of cancer. Vynfinit then delivers doxorubicin directly to the cancer cells, enabling a more targeted approach to cancer treatment.

A midstage trial tested Vynfinit and doxorubicin on lung cancer patients, noting that the disease progressed more slowly in patients treated with the cocktail, compared with patients who underwent standard treatments. Life expectancy was higher as a result.

"We've studied vintafolide in two of the toughest-to-treat cancers and seen positive results and we view this as a continuing validation of the drug and the platform," said Endocyte CEO Ron Ellis in a conference call with Bloomberg.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.