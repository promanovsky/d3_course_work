A rarely seen gem proved to be rich in water upon closer examination, a find scientists say is further evidence that there are vast reserves of water locked hundreds of miles below the Earth's surface.

In a paper published in the journal Nature on Wednesday, University of Alberta researcher Graham Pearson and colleagues describe the first ever discovered sample of ringwoodite, a mineral similar to olivine (or peridot). Ringwoodite is stable basically only within the hot, pressurized environment deep within the Earth’s mantle.

Until now, most studied samples of ringwoodite have come from meteors. But Pearson and colleagues found a tiny fragment of the material, the first terrestrial sample, inside a brown diamond. The diamond was itself driven up from the depths in a piece of kimberlite, a volcanic rock. The sample was unearthed by miners in Brazil back in 2008.

“It's so small, this inclusion, it's extremely difficult to find, never mind work on," Pearson said in a statement Wednesday. "So it was a bit of a piece of luck, this discovery, as are many scientific discoveries."

When Pearson and colleagues further analyzed the tiny piece of ringwoodite trapped within the diamond (itself only about 3 millimeters wide), using infrared and X-ray techniques, they found that it contained a surprisingly high amount of water -- equivalent to about 1.5 percent of its total weight.

"This sample really provides extremely strong confirmation that there are local wet spots deep in the Earth in this area," Pearson said.

Other pieces of ringwoodite deep below the Earth's surface may be even more waterlogged. In lab experiments, synthetic versions of ringwoodite can hold up to 2.5 percent of their weight in water.

Scientists have long theorized that water might be trapped in the transition zone between the upper and lower mantles, located 410 and 660 kilometers (254 to 410 miles) below the planet's surface. But since we have no way of getting a person or robot down there at present, a lot of mysteries remain. How much water might be down there? Maybe a lot of water, says Pearson:

"That particular zone in the Earth, the transition zone, might have as much water as all the world's oceans put together," he said.

Knowing more about the presence of any kind of water in the earth’s mantle could help inform our knowledge of plate tectonics and volcanoes.

"One of the reasons the Earth is such a dynamic planet is the presence of some water in its interior," Pearson says. "Water changes everything about the way a planet works."

SOURCE: Pearson et al. “Hydrous mantle transition zone indicated by ringwoodite included within diamond.” Nature, 12 March 2014.