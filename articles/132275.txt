David Carrig

USA TODAY

Stocks rallied for a third straight day Wednesday as investors see encouraging signs from better-than-expected corporate earnings and economic reports.

The Dow Jones industrial average rose 162.29 points, or 1%, to close at 16,424.85. The three-day rally has put the Dow 398.10 points higher than its Friday close, making up for last week's 385.96-point loss.

The Standard & Poor's 500 index gained 19.33 points, or 1%, to 1,862.31 and is up 2.6% for the week. The Nasdaq composite index surged 52.06 points, or 1.3%, to 4,086.23 and has a 2.2% gain for the week.

Investors are closely watching corporate earnings as they try to determine if the negative impact the harsh winter weather had on many companies has begun to ease. Financial analysts expect first-quarter earnings for the S&P 500 will decline about 1.2%, according to S&P Capital IQ.

"We do think … the market is focused on what seems to be a worst-case scenario not being realized," said Jim Russell, senior equity strategist at U.S. Bank Wealth Management. "Expectations are low enough where many companies will either meet or beat expectations."

The latest economic report from the Federal Reserve supported the belief that economic activity is picking up after the harsh winter. The Fed's "beige book," a regional look at the nation's economy, found that from early March through mid-April, 10 of 12 Fed bank districts reported an expanding economy. That was better than the eight regions reporting growth in the previous report.

In a separate report, home building picked up in March, led by a 6% gain in single-family home construction. Builders started new homes at an annual rate of 946,000 last month, up 2.8% from February, the Commerce Department said. Manufacturing activity also picked up as U.S. factory output rose 0.5% in March, the Federal Reserve said.

In corporate news, shares of Yahoo surged 6.3% to $36.35 after investors were impressed by big revenue gains from Chinese e-commerce company Alibaba. Yahoo owns a 24% stake in Alibaba, and its growth overshadowed Yahoo's overall flat performance.

Bank of America reported a $276 million loss for the first quarter as legal costs stemming from the financial crisis took a toll. Shares dropped 1.6% to $16.13.

The yield on the 10-year Treasury note rose to 2.64% from 2.63% late Tuesday.

Global stocks were higher Wednesday as China's slowdown in the first quarter was less severe than expected. The world's second-largest economy expanded 7.4% from a year earlier, better than the average forecast of 7.3% growth.

In Asia, Hong Kong's Hang Seng index gained 0.1% to 22,696.01 and the Shanghai Composite added 0.2% to 2,105.12.

The Nikkei 225 index jumped 3% to 14,417.68 as a weaker yen boosted exporter stocks and Softbank shares surged 8.5% after Chinese e-commerce Alibaba Group Holding, in which it holds a 37% stake, reported strong earnings.

In Europe, Germany's DAX jumped 1.6% to 9,317.82 and France's CAC 40 gained 1.4% to 4,405.66. Britain's FTSE 100 rose 0.7% to 6,584.17.

In a volatile session Tuesday, the Dow rose 89.32 points, or 0.6%, to 16,262.56, after earlier rising as high as 16,273 and falling as low as 16,063. The Standard & Poor's 500 index gained 12.37 points, or 0.7% to 1,842.98. The Nasdaq composite index added 11.47 points, or 0.3%, to 4,034.16, climbing back from a low of 3,946.

Contributing: The Associated Press