Canadian drug maker Valeant Pharmaceuticals Inc. (VRX,VRX.TO), Wednesday began a tender offer for shares of Allergan Inc. (AGN), taking its $53-billion offer directly to shareholders after being repeatedly rebuffed by the Botox maker.

Separately, Allergan, in a statement said it will carefully review and evaluate the exchange offer, and advised shareholders to take no action at this time pending the review. Allergan stockholders will get 0.83 shares of Valeant stock and $72.00 in cash for every Allergan share held. The tender offer is set to expire at 5 p.m. ET on August 15, unless extended.

Valeant's Chief Executive Michael Pearson said, "We believe Allergan's stockholders should have the opportunity to express their views and we are confident that Allergan's stockholders will support this combination. This offer, together with Pershing Square's ongoing efforts to call a special meeting of Allergan stockholders, is part of Valeant's clear path to complete a transaction with Allergan."

In April, Valeant along with Pershing Square Capital Management L.P. submitted an unsolicited bid to acquire Allergan in a cash and stock deal that valued Allergan at $46 billion. However, Allergan's Board rejected the offer stating it created huge risks for its stockholders and was not in the best interests of the company.

In late May, Valeant Pharma sweetened its takeover bid for Allergan twice to about $53 billion. However, Allergan again rejected the offer last week.

William Ackman has also called for a special meeting of Allergan shareholders seeking to oust six directors from the current Board of Allergan and smoothen the process to close the deal. Pershing is the largest Allergan shareholder with a 9.7 percent stake in the company.

VRX is currently trading at $119.60, up $1.85 or 1.57%, while AGN is trading at $162.29, up $1.76 or 1.10%, both on the NYSE

For comments and feedback contact: editorial@rttnews.com

Business News