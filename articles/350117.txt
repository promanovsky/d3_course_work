Facebook COO Sheryl Sandberg, who was in India this week, gave a candid interview to CNBC-TV18's Executive Editor Shereen Bhan on the role of women in work.

Facebook COO Sheryl Sandberg who was in India this week gave a candid interview to CNBC-TV18's Executive Editor Shereen Bhan on the role of women in the corporate sector in the context of her book, Lean In.

Sandberg acknowledged in the interview that a lot of cultural and institutional barriers were holding back the rise of women in this workplace and that these need to change. Below are some edited responses by Sandberg.

On reasons as to why women's progress has stalled: Bhan pointed out to Sandberg that "things haven’t really changed, we haven’t seen rapid strides as far as women entering the workforce and the boardroom is concerned."

The Facebook COO said, "We did make progress for many decades and it is obvious that things are better than they were before...Progress for women in board or progress for women in top jobs it has been pretty stagnant. We got rid of some of the barriers, we still have more barriers to get rid of both institutional and public policies but we also really need to address our culture."

On the cultural reasons behind this stagnation, Sandberg said that women are often told that they are too aggressive at work, while men are never told that. "We know that men are more aggressive than women when it is actually measured fairly but we expect leadership from men and we don’t expect it from women and that is why women have a hard time leading. We need to change that," she said.

She also said that as far as the workplace is concerned, the attitudes towards women needed to change. As an example she said that instead of referring to girls as aggressive or bossy, "we could say actually she is not aggressive, she is results oriented." She added, "I tell people next time you see a little girl called bossy you walk up to the person who said it and you say that little girl is not bossy, that little girl has executive leadership skills."

On 'Women can't have it all': Sheryl agreed with Bhan that between the "Lean In" camp and the "You can't have it all camp" made famous by Anne-Marie Slaughter's essay on The Altantic, the truth for many women lies somewhere in between.

She said, "I have said this in my book, it is both. It absolutely is institutional reform and public policy reform around the world and it is also individual action and we need both to go hand in hand."

She added that there needs to be a change on the vocabulary for women particularly with the phrase ‘women can’t have it all’.

"No one ever says that about men, no one ever says men can’t have it all; they can, they do, they work and have children but we assume that women can't and we need to change our assumptions of what women can’t do to what they can," Sandberg pointed out.

On ways of working with women: Sandberg says that gender needs to be a part of conversation around the workplace and "from the point of view of competitive advantage, that rather than pretend gender doesn’t exist."

She said that instead of "pretending we are treating people equally because we are not, let’s acknowledge that we need to do more to encourage women to have the same opportunities as men. For example we know that women are the ones who give birth, that women need maternity leave, have we made it fair for women to re-enter and have the same opportunities."

On What Facebook is doing to help women get into technology:"We are working with a number of organisations. Both Facebook and the Lean In foundation work together to form Lean In circles through computer science departments and we are working on that as well. We definitely have to have address the pipeline," said the Facebook COO.

She also pointed out that one needs to convince girls that they are just as capable as boys of coding. "It is also important that we let our girls embrace technology. Everywhere in the world we are more protective of our daughters than our sons and that means often we take the computer away from our daughters, we take the phone away from our daughters and we hand it to our sons. Who do you think is going to become the computer scientist? So, everyone needs children to be safe online and we care a lot about this but we also need to believe our daughters can code as well as our sons," she said.

Read the full transcript of the interview here.