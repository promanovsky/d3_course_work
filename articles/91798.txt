next Image 1 of 2

prev Image 2 of 2

Planets abound in the night sky this month.

Three planets are accessible during convenient evening hours, while a fourth — the brightest one of all — will appear in the sky just before sunrise. And with clear skies and a little luck, this bright planet (Venus) can lead you to the most distant full-fledged world in the solar system (Neptune) on Saturday morning.

Mars' closest approach in six years

Vying with Jupiter for attention this week is Mars, which will be at opposition to the sun on Tuesday, appearing as an orange luminary low in the east-southeast at dusk. (The sun, Earth and Mars will all line up on this day, with Earth in the middle.) At magnitude -1.5, it is equal in brightness to Sirius, the brightest of all stars.

And at 8:53 a.m. ET on April 14 — the same night of a total lunar eclipse — Mars will make its closest approach to Earth since January 2008, coming within 57.4 million miles of Earth.

As Mars apparitions go, however, this one still offers below-average favorability. A 6-inch telescope with an eyepiece that magnifies 118-power will show Mars' rust-hued disk appearing as large as the full moon appears with the unaided eye, and will yield detail only grudgingly.

More On This...

Even so, observers may be able to spot new features in the light and dark markings that cover the planet's surface.

Jupiter, the planet king

The most prominent evening planet during these past few months has been Jupiter. The first star-like object that appears at dusk, it glows with a steady, silvery light high in the west through the rest of this month, with the two leading stars of Gemini, Pollux and Castor, floating even higher above it. This biggest of all the planets offers telescope users fine views for several hours after sunset.

[Best Night Sky Events of April 2014: Stargazing Guide]

Ever see Neptune?

This weekend brings a rather difficult but fascinating opportunity for telescopeusers: During the dawn twilight on Saturday, the dim, 8th-magnitude planet Neptune will appear low in the east-southeast sky, glowing as a pale blue dot only 0.7 degrees to the lower right of Venus. (Your clenched fist held at arm's length measures about 10 degrees.)

Through a small telescope, Venus will appear gibbous, 59 percent lit. This planet is the brightest morning "star" all month, but it's not very high up. Look for it blazing very low in the east-southeast right around the first light of dawn. As the sky brightens, Venus rises higher but appears to fade and shrink in the growing light of day.

Moon visits 'Lord of the Rings'

During the overnight hours of April 16, you can watch the moon as it approaches and then passes the planet Saturn.

When the pair first appears low above the east-southeast horizon shortly after 9:30 p.m. local daylight time, Saturn will be to the lower left of the moon, appearing as a bright yellowish-white "star." As the night wears on, the moon will drift toward Saturn slowly, covering roughly its own diameter each hour.

When the two worlds cross the meridian five hours later, Saturn will appear to be shining just over 1 degree above the moon. When dawn is brightening the sky a few hours later, the two objects will have moved over to the southwest sky, with the moon appearing off to the lower left of Saturn. The gas giant's iconic rings are currently tilted 22 degrees toward Earth.

Moon joins Venus — but where's Mercury?

On the morning of April 25, a lovely crescent moon hovers well above and to the right of Venus.

The only planet that is "out of the loop" for observers this month is Mercury. The speedy little planet arrives at superior conjunction — on the opposite side of the sun as seen from Earth — on April 26. During early May, Mercury will emerge into the evening sky to begin a very favorable apparition and will more than make up for its poor April showing.