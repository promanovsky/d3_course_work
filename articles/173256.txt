Euro zone inflation rose in April, reducing chances the European Central Bank will act soon to ward off potential deflation.

But inflation was below forecasts and still within the ECB's "danger zone" of under 1%.

Annual consumer inflation in the 18 countries sharing the euro nudged higher to 0.7% in April from March's 0.5%, which was the lowest since late 2009, the European Union's statistics office Eurostat said today.

The reading was lower than the 0.8% predicted in a Reuters poll despite higher spending over the Easter period.

This reflected the poor state of the euro zone economy after a long recession and with unemployment at near-record levels.

April's reading takes inflation back to where it was in February but it is well below 1.2% of April 2013.

The lack of a clear up-tick in consumer prices will keep pressure on the ECB to act to stimulate the economy although it is not expected to do so at its next policy meeting on May 8.

The ECB is due to publish updated staff forecasts for inflation and growth stretching to 2016 when its policymakers meet in June.



Today's figures show that consumer price inflation, excluding volatile energy and food costs, was a touch higher this month than in March at 1%.

But while April improved overall because of a smaller fall in energy prices, the pace of price rises in food, alcohol and tobacco was lower than the month before, showing Europeans remain unwilling to spend while economic growth is fragile.



The central bank targets inflation of just below 2% over the medium term and the International Monetary Fund wants to see the ECB take radical steps that could include quantitative easing, or money printing to buy assets.

