Electrical pulses delivered from a cochlear implant to the inner ear can be used to regrow auditory nerves to both enhance hearing and deliver gene therapy that could treat everything from Parkinson’s disease to depression, according to new research published today in Science Translational Medicine.

“Our work has implications far beyond hearing disorders,” said study co-author Matthias Klugmann from the University of New South Wales Translational Neuroscience Facility research team. “Gene therapy has been suggested as a treatment concept even for devastating neurological conditions and our technology provides a novel platform for safe and efficient gene transfer into tissues as delicate as the brain.”

SEE ALSO: Bionic Hand Restores Touch in Real Time

Knowing that auditory nerve endings regenerate if certain proteins are present, but that drugs and viral-based gene therapy couldn’t provide a safe method of delivery, researchers started looking at cochlear implants as a method of delivery.

The team found that electrical pulses delivered from the implant could transport the DNA to cells close to the implanted electrodes. Those cells would then produce those key proteins, called neurotrophins, that stimulate the regeneration.

The procedure, which took five years to develop, would help people who rely on cochlear implants to hear be able to differentiate a broader tonal range of sound, which would likely allow them to appreciate music.

“People with cochlear implants do well with understanding speech, but their perception of pitch can be poor, so they often miss out on the joy of music,” said researcher Gary Housley.

The electrical pulses could be administered during the implant procedure.

“The surgeon who installs the device would inject the DNA solution into the cochlea and then fire electrical impulses to trigger the DNA transfer once the implant is inserted.,” said study author Jeremy Pinyon.

This article originally published at Discovery News here