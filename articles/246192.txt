eBay: “Millions” of passwords changed after site hacked

Millions of eBay users have already changed their passwords after the auction site was hacked, eBay has confirmed, updating its homepage to finally make clear to visitors that their information could have been stolen. The security breach – which eBay revealed earlier this week, several months after it’s believed to have actually taken place – saw everything but payment details taken by hackers.

“Millions of eBay users have already updated their passwords,” the company’s new notice claims. Initially, eBay opted to advise users of the need to reset their credentials by email, but later added a link to the notice in a more visible place on the site.

eBay is yet to say exactly how many people saw their data taken. Although the company is advising every user to change their password now, we don’t know what proportion could actually be at risk.

“We are looking at other ways to strengthen security on eBay,” the notice continues. “In the coming days and weeks we may be introducing new security features.”

Whether that’s enough to pacify data protection investigators on both sides of the Atlantic remains to be seen. Earlier today, it was confirmed that several US states had opened a collaborative investigation into the hack, while counterparts in Europe are also looking into whether the auction site did enough to protect its records.

Some have criticized the site for storing more details than were legitimately necessary, such as date of birth.

eBay first spotted signs of the hack two weeks ago, it said on Wednesday, with compromised employee credentials being used to access the corporate network. It is working with the FBI and other agencies to figure out who might have been responsible.