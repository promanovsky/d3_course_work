When beards are ubiquitous, people perceive them to be less attractive, according to the study in Biology Letters by a team of Australian researchers at the University of New South Wales.

“The bigger the trend gets, the weaker the preference for beards and the tide will go out again,” researcher Robert Brooks told the Guardian Australia. “We may well be at peak beard.”