The "Star Wars Episode 7" production has reportedly shuffled around the filming schedule to give Harrison Ford time to recuperate from his injuries.

The Mirror U.K. reports an inside source claims Ford's injury won't delay the production as there are many actors and scenes that may be pushed up to accommodate the actor's potential absence. The actor was injured after a hydraulic door fell on him while on set.

"The initial day or two after the accident it was hard to move things around but now we have had more time we can juggle things, bring some scenes forward and push others back," the insider told the Mirror.

"There are lots of other actors in the movie so it is not a disaster," the source continued. "We are still confident the film can stay on schedule...At the moment it looks like Harrison is going to be off set for 6-8 weeks but the main priority is making sure he is OK."

However, the actor's son Ben Ford told Access Hollywood he may need a plate and screws in his ankle. Ben added the production might shoot Ford's scenes from the waist up until he fully recovers. The actor was reportedly airlifted to Oxford's John Radcliffe Hospital and hasn't been released from their care.

There have also been multiple reports claiming Ford suffered injuries to his chest and pelvis. However, a Lucasfilm and Disney representative has only confirmed an ankle jury at the time.

"Harrison Ford sustained an ankle injury during filming today on the set of Star Wars: Episode VII. He was taken to a local hospital and is receiving care. Shooting will continue as planned while he recuperates," a spokesperson for "Star Wars" told the Mirror.

"Star Wars Episode 7" is slated to hit theaters on Dec. 18, 2015.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.