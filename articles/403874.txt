Robert Pattinson insists he no longer "gives a s**t" that Kristen Stewart cheated on him.

The Twilight Saga star, who split from the 24-year-old actress last year after her brief affair with married director Rupert Sanders in 2012, says the demise of their relationship was ‘normal’ because they're ‘just young people.’

The 28-year-old British actor told the UK's Esquire magazine: ‘S**t happens, you know? It's just young people...it's normal! And honestly, who gives a s**t?’

Scroll down for video

Over it: Robert Pattinson insists he no longer 'gives a s**t' that Kristen Stewart cheated on him

But The Rover star still felt protective over the brunette beauty after they broke-up.

He explained: ‘The hardest part was talking about it afterwards. Because when you talk about other people, it affects them in ways you can't predict.

‘It's like that scene in ‘Doubt,’ where he's talking about how to take back gossip? They throw all those feathers from a pillow into the sky and you've got to go and collect all the feathers."

Life lesson: The Twilight Saga star says the demise of their relationship was ‘normal’ because they're ‘just young people’

Affair: Robert split from the 24-year-old actress last year after her brief affair with married director Rupert Sanders in 2012

Despite his success, Robert claims he doesn't understand why people are interested in him.

Cover star: The full interview is available in the September issue of Esquire which is in stores from July 31

He said: ‘I don't understand why. I think it goes through periods where you're assigned “this is the guy to follow”.

‘But whenever I see a bunch of paparazzi hanging out, I always think, “Oh God, what have they found out!

'"Oh, that love child! I totally forgot!”’.

The actor first realised he was famous when he showed up to a nightclub and his name was already on the guest list.

He said: ‘I was going to clubs in L.A. and you had to call the promoters ahead of time to get on the guest list.

'But one time I forgot to call, and I was on the list anyway.

‘That's when I knew. I showed up with mustard down my T-shirt and they're like giving me the wink, “Yeah, man, you're on the list.”’