Since “Jersey Boys,” the rags-to-riches-to-rags musical about Frankie Valli & the Four Seaso‎ns, was first staged on Broadway in 2005, there have been thousands of performances across North America. In the room at some of these cities sat a certain interested spectator, puzzling out what he saw.

“Every audience was the same,” Clint Eastwood said of the overwhelmingly warm reception. “I wondered a lot about why that was. I saw the show in Vegas, in San Francisco, in New York. They all responded in the same way.”

The director tries to bottle that feeling and uncork it anew with his latest work, a film adaptation of the Broadway blockbuster and Tony best musical winner,‎ opening Friday. Penned by the play’s book writers, Marshall Brickman and Rick Elice, and featuring three of the four principal actors reprising their roles from the stage, the “Jersey Boys” movie scrapes some of the show’s sugarcoating off the band’s personal and professional story while retaining the feel-good music that helped make it such a worldwide crowd-pleaser.

One of the more unusual stage-to-screen musical adaptations in recent memory — and the first of a coming crop, with “Into the Woods” and “Annie” hitting later this year — “Jersey Boys” unites veteran stage actors with the man known for grizzled westerns and biopics in a Hollywood venue.

Advertisement

It also highlights both the appeal and the challenges of taking a modern ‎stage smash and recalibrating it for the multiplex. Cast with no movie stars and opening at a time of year more hospitable to superhero escapades and R-rated raunch than older-skewing Broadway adaptations, “Jersey Boys” presents particular difficulties. Box-office tracking, which seeks to predict opening weekends, has been weak for the Warner Bros. musical (budgeted at a relatively modest $40 million) though the studio hopes its classic tunes offer an appealing option to filmgoers of a certain age and the possibility of a longer run.

The film begins with the rough Newark origins of the four Italian American principals‎ (the Army, the mob and fame are the only three ways out, band member Tommy DeVito tells us)‎, marinating in the details of their tough life before moving to familiar terrain.

After trying in vain to make it, the Four Seasons finally taste success when the quartet is locked in place: lead singer Valli (John Lloyd Young), hustling guitarist DeVito (Vincent Piazza, the only non-stage player), put-upon bassist Nick Massi (Michael Lomenda) and the final piece of the puzzle, keyboardist and songwriter Bob Gaudio (Erich Bergen).

The band then enjoys a meteoric ascent thanks to its wholesome image and sing-along tunes — for example, “Big Girls Don’t Cry,” “What a Night” and “Working My Way Back to You,” all of which get rousing treatments here. Eventually, though, they are brought low by DeVito’s gambling debts and criminal activities — and, more crucially, the loyalty Valli feels to him, which has far-reaching implications for the band.

Advertisement

The toe-tapping piece happily peddles nostalgia with re-created numbers on “The Ed Sullivan Show” and elsewhere. But, perhaps to a greater extent than the play, the “Jersey Boys” film isn’t so much concerned with glorifying a group that history has already recorded fondly as it is with exploring the cultural and other beliefs of the mid-century Italian American men ‎who formed it.

“There’s this clichéd but interesting idea of omerta, that you don’t let problems get out to the rest of the world; that’s what the movie is really dealing with,” Young said.

Added Piazza, “This is about the idea of being a man and how that’s changed.”

Piazza and Young are sitting with their co-stars and Eastwood in the throwback glamour of a Waldorf-Astoria bar. Inflections, hair styles and even finger jewelry mirror those of their characters to varying degrees. All were discovered on stage and have little big-screen experience. Most have at least some Italian American ancestry.

Advertisement

Stage actors can endure a rocky transition as they move to the director-oriented medium of film, but the actors said they were aided by Eastwood’s famously hands-off‎ approach to actors.

‎

”I saw in this documentary that Clint had said saying ‘action’ spooks the horses and children so you had to be gentle,” Young said. “He hadn’t seen me in 20 m‎ovies, so he knew he had to be gentle.”

An unconventional choice to some, Eastwood is making his first music-themed movie since 1988’s “Bird,” though he has been known for composing and other work in recent years. The director said he was drawn to the Four Seasons’ music, which he believes has endured more than iconoclastic bands of the era such as the Beatles or the Rolling Stones.

Yet in many ways, he said, the tunes here were secondary.

Advertisement

“I approached this as a drama about guys and just what they do is sing rather than a singing picture that has some drama to it,” Eastwood said. “They did jail time; they came from a rough place. These were very interesting people.”

He declined to go too deeply into what made him choose such unexpected material, simply saying he liked the story. (“I didn’t think I’d want to make a movie about kids‎ in Boston either but then I read ‘Mystic River,’” he said.)

But the actors said they saw in the work classic themes the director has explored in Westerns and other movies — masculinity, loyalty, the unique plight of the outsider.

“At first I wondered‎ about the connection,” Bergen said. “But [the director] made it work because he saw it as a Clint ‎Eastwood film.”

Advertisement

Some of the actors also had an unlikely presence in their lives: the real-life Valli and Gaudio are very much around and made their thoughts known on “Jersey Boys.” Both have been vocal about the stage and film adaptations generally, and not always enthusiastically. Valli was also regularly on set, (Eastwood noting casually that he had met him years ago “He and me and Elvis and James Dean, we’d all run into each other because we were the same generation,” he said.)

There were challenges before the first camera began to roll. The project had sat at Sony for several years before picked up by Warner Bros., then, when a version with “Iron Man” helmer Jon Favreau and the “Skyfall” writer John Logan didn’t pass muster, was put into turnaround by the studio before finding a second life with Eastwood.

One of the biggest obstacles came with the music itself, Brickman said.

“Psychological time is much quicker in a movie theater, so you can’t just photograph four guys singing,” he said. “A lot of directors have crashed on that little wrinkle. You have to use as many cinematic devices as possible — close-ups, cutaways, everything — to keep people watching.” (Brickman added that he thought the show’s depiction of the complicated bonds of family--the ersatz one of the band included--are what helped the stage show resonate so widely.)

Advertisement

The film is also aided by the fact that the main characters are musicians, allowing for performances to happen within the flow of the story. To preserve the stage feel, Eastwood also did not pre-record the singing but shot the music as part of the scenes.

Still, there are reasons many hit stage shows have trouble even getting to the screen, let alone succeeding there.

“This one lent itself to a movie,” Eastwood said. “But I guess some people didn’t see it that way because it’s been a hit for a long time and no one did much with it.” He shrugged. “This is just a great story. Usually it’s just as simple as that.”