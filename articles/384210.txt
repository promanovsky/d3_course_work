Samsung is reportedly planning to expand its Galaxy lineup with a new entry-level smartphone aimed at buyers in the emerging markets.

Samsung, the leader in smartphone and tablet business, is focusing on emerging markets as it preps for the launch of an all-new Galaxy V smartphone. The news is yet to get an official confirmation from the Korean tech giant, but the smartphone's retail listing on a Vietnam online store pretty much confirms it.

The new entry-level Galaxy V smartphone has low-end specs and an affordable price tag. In terms of specifications, the smartphone will feature a 4-inch display with 480 x 854-pixel resolution, 1.2GHz processor with no information on the chipset and a 512MB RAM. The Galaxy V will feature a 3-megapixel rear-facing camera with LED flash but lacks a front shooter.

Additional features include 4GB internal storage with option to expand up to 32GB via microSD card and connectivity options such as Wi-Fi 802.11 a/b/g/n, Wi-Fi Direct, Bluetooth 4.0, A-GPS and 3G are on board.

The low-end smartphone will run Android 4.4.2 right out of the box and packs a 1,500mAh battery, which is sufficient for specs so low. The device measures 121.2 x 62.7 x 10.65 mm and features dual-SIM support, according to GSM Arena.

Samsung's Galaxy V smartphone, with its specs and price, will go up against the newly-launched Nokia Lumia 630 and 635. Specs-wise, Samsung and Nokia phones are almost similar but the Lumia phones sells for a higher price. The Lumia 635 features a 4.5-inch display with screen resolution, processor, RAM same as Galaxy V. The camera is upgraded on the Lumia phone with 5-megapixels but still loses the front shooter.

The Samsung Galaxy V is likely to be a lot cheaper than any other smartphone of these specs in the market. GSM Arena says the device will retail for $108 without contract. Nokia is currently running a promotional offer on Lumia 635, which makes the smartphone available for $119.95.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.