Dr. Kent Brantly (R) carrying in an Ebola patient before he became contaminated with the disease himself. (Courtesy Samaritan's Purse)

An Atlanta hospital is preparing to treat one of the two American aid workers who contracted the highly contagious and deadly Ebola virus in West Africa.

It will be the first time that a patient diagnosed with Ebola will be known to be in the United States.

Emory University Hospital said in an email to Yahoo News late Thursday that it “has been informed that there are plans to transfer a patient with Ebola virus infection to its special facility containment unit within the next several days. We do not know at this time when the patient will arrive.”

But CNN reported that a long-range business jet is already en route to Monrovia, Liberia, where medical missionaries Dr. Kent Brantly and Nancy Writebol have been under quarantine for several days and are struggling to survive.

SIM missionary Nancy Writebol and her husband, David, have served in Africa for 10 years. (Courtesy photo)

Citing privacy laws, the hospital declined to say if either Brantly, 33, or Writebol, 59, will be cared for in a special isolation unit. The facility, one of four of its kind in the country, was built in collaboration with the Centers for Disease Control and Prevention.

It could not be determined late Thursday where the second American patient would be treated. In an email to Yahoo News, the CDC declined to reveal information about Brantly and Writebol. Messages to the missionaries' North Carolina-based sponsor organizations were not immediately returned.

There is no known cure or vaccine for Ebola, which is spread by direct contact with blood or other bodily fluids from a sick person. Mortality rates can range from 60 to 90 percent.

The Atlanta hospital said it will have the proper protocols in place.

“For this specially trained staff, these procedures are practiced on a regular basis throughout the year so we are fully prepared for this type of situation,” the hospital said in the statement.

[Related: Ebola-infected doctor's extraordinary sacrifice]

The CDC has said the risk of a traveler bringing the Ebola virus to the U.S. remains small. On Monday, the agency sent a health alert to U.S. doctors, updating them about the outbreak, The Associated Press reported.

Story continues

The alert stressed that they should ask about foreign travel in patients who come down with Ebola-like symptoms, including fever, headache, vomiting and diarrhea. Even if a traveler infected with Ebola came to the U.S., the risk of an outbreak is considered very low, CDC director Dr. Tom Frieden told reporters on Thursday. Patients are contagious only when they show symptoms, and U.S. hospitals are equipped to isolate cases and control spread of the virus.

Earlier in the day, the faith-based group Samaritan’s Purse described Brantly and Writebol as being in grave but stable condition.

Kent and Amber Brantly (family photo)

Amber Brantly, a nurse, was in Liberia with her husband and their two young children until recently, leaving to visit family in Texas. Dr. Brantly was due to join them this week until he became ill.

“Many people have been asking how I am doing,” Amber said in a written statement late Thursday. “The children and I are physically fine. We had left Liberia prior to Kent’s exposure to the virus.”

She asked for continued prayers for her husband and the estimated 1,300 others in West Africa who have been diagnosed with the virus. The outbreak, the worst since the disease first emerged in Africa nearly 40 years ago, has killed more than 700 people this year.

“I remain hopeful and believing that Kent will be healed from this dreadful disease,” Amber said in the statement. “I am grateful for the daily reports I receive from his doctors on the ground. He is strong and peaceful and confident in the love of Jesus Christ, which is his sustenance right now.”

(With reporting from the Associated Press.)

Follow Jason Sickles on Twitter (@jasonsickles).

Related Video: