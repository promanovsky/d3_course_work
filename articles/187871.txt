The teen at the center of a custody battle triggered by differing diagnoses by two Massachusetts hospitals will be transferred from the Bay State to a facility in her home state of Connecticut, according to Massachusetts officials, but her parents are not welcoming the decision.

Secretary of Health and Human Services John Polanowicz said Monday the "reunification" plan calls for 15-year-old Justina Pelletier to be transferred to a facility in Thompson, Conn., as the first step in a process that could eventually return her to the custody of her parents. A judge will make the final decision to return Justina to her family.

The Connecticut facility "will provide services to Justina with the ultimate goal of her returning home as soon as possible," writes John Polanowicz, secretary of the Executive Office of Health and Human Services, to Massachusetts lawmakers in a letter obtained by FoxNews.com.

The announcement, however, has infuriated the girl's parents, Lou and Linda Pelletier, of West Hartford, who claim the move is "not at all meant as a step toward regaining custody" and called it "barbaric."

"They’re just shipping her off to another psychiatric facility," Lou Pelletier told FoxNews.com Tuesday. "It’s disgusting."

"They’re totally ignoring her physical needs," he said, citing a heart condition the girl suffers from that Pelletier claims has gone untreated since the teen was transferred to Boston Children's Hospital last year.

Tufts Medical Center had treated Justina for mitochondrial disease, a disorder that affects cellular energy production. But Boston Children's Hospital later diagnosed her problems as psychiatric.

The court in March ordered the teen placed in the custody of the Massachusetts Department of Children and Families.

Justina will be transferred to the JRI Susan Wayne Center for Excellence in Thompson -- a facility her father claims specializes in HIV services and shock therapy. Lou Pelletier also said the facility is "very far away" from the family's home.

A spokesman for the facility, however, told FoxNews.com in an e-mail that, "NO JRI program uses shock therapy" and that the Susan Wayne Center for Excellence is also "not a provider of HIV services."

It remains unclear whether the facility is licensed in the state of Connecticut. The Connecticut attorney general referred all media inquires on the matter to the Connecticut Department of Children and Families.

In March, Massachusetts juvenile court Judge Joseph Johnston issued a four-page ruling blasting Pelletier’s parents for being verbally abusive and complicating efforts to bring the family together. The Pelletiers, meanwhile, have claimed the Bay State bureaucracy has been aligned against the family from the beginning.

The Massachusetts Department of Children and Families took emergency custody of Justina on Valentine’s Day 2013 after doctors at Tufts Medical Center, which had been treating her for a rare condition, and doctors as Boston Children’s Hospital, clashed over the cause of her medical problems, which included difficulty eating and walking.

At Tufts, Justina had been treated for mitochondrial disease, a group of rare genetic disorders affecting cellular energy production. When Justina began experiencing some gastrointestinal problems, the Tufts doctor treating her, Dr. Mark Korson, wanted the girl to visit Dr. Alejandro Flores at Boston Children's Hospital, according to the family's attorney, Phil Moran. Flores had treated Justina in the past, Moran said, and Korson thought it beneficial for the teen to see a gastroenterologist.

What happened next was a "tragic nightmare" for Justina, according to her father.

Justina was taken by ambulance to Boston Children's Hospital because she was in a wheelchair at the time and a heavy snowstorm was blanketing the region. Because she arrived by ambulance, she was taken directly into the hospital's emergency room, where a "resident refused to send her to Dr. Flores" and, "declared this was his case," according to Moran. He said the unnamed resident then called upon a psychologist who diagnosed Justina with somatoform disorder -- a mental condition in which a patient experiences symptoms that are real but have no physical or biological explanation. Justina was diagnosed with the disorder "within 25 minutes," Moran claims.

The Pelletier family rejected the new psychiatric diagnosis and wanted to bring Justina back to Tufts, Moran said. He claims the hospital tried to force the girl's parents to sign papers preventing them from seeking another opinion.

After tempers flared between the Pelletiers and staff at Boston Children's, the hospital notified the state that it suspected the parents of medical child abuse.

The girl was kept at Boston Children’s psychiatric ward for nearly a year, but was then slated to be transferred to another state facility. Johnston said the family, which vented its anger in various media interviews, hampered efforts to have her placed as near them as possible. She is currently being held at the Wayside Youth and Family Support Network facility in Framingham, Mass.

Pelletier has claimed Johnson ignored the testimony of his daughter's original doctors, who stand by the diagnosis of mitochondrial disease. In the more than a year since the ordeal began, the Pelletiers have only been allowed hourly visits each week with their daughter, whose condition, they say, has deteriorated. She has not attended school or church since the family lost custody of her, he said.

The Associated Press contributed to this report.