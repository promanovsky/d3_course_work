Former "America's Next Top Model" contestant Katie Cleary's husband died on Sunday, June 22 from a suspected suicide. According to TMZ, Andrew Stern was found dead from a self-inflicted gunshot wound to the head at a San Fernando Valley shooting range. He was 40-years-old.

Sources told TMZ that law enforcement officials interviewed Cleary and Stern's friends and family and concluded that he was "suffering significant stress over marital problems" and also had a "history of depression and financial issues."

Stern's alleged suicide came one month after the "Deal or No Deal" suitcase model was spotted in Cannes, France flirting with actors Leonardo DiCaprio and Adrian Grenier. Photos surfaced of Cleary and DiCaprio on the dance floor and whispering to each other, and she was also spotted out and about in France with Grenier.

A rep for the model told TMZ that the photos were a "non-issue" and Cleary and Stern had split almost eight months ago. Stern filed for divorce in April. However, his friends are painting a different picture and said that Stern was extremely upset that his marriage was falling apart and was equally upset over the photos of his wife flirting with the actors.

Reportedly, Stern filed for divorce because he was tired of Cleary "globetrotting and hobnobbing with celebs," a friend told TMZ. A different source told the Daily News that Cleary and DiCaprio were simply discussing work and were not flirting with each other in the photos.

"She runs a group, Peace 4 Animals. That's what they were talking about," the source said. "They left separately. Nothing [is] going on between them."

TMZ published a new report claiming that Stern's apparent suicide might also have been due to hormone replacement injections he was taking for his libido. Sources told the site that two years ago Stern went to a Beverly Hills health and wellness center because he was feeling lethargic and had a waning libido.

He was reportedly given a kit that included injections of testosterone and human growth hormones.

"The friends say he started experiencing drastic mood swings and became extremely irritable," TMZ wrote. "Andrew kept the regimen up for a year but couldn't take it anymore and stopped cold turkey."

Sources told the site that suddenly quitting something like hormone replacement therapy can be very dangerous "and in Andrew's case he spiraled into a deep depression, which got much worse because his business failed and everything then took a toll on his marriage."

One friend told TMZ that he talked to Stern on Saturday and he said he was having a "tough day" but promised that he would be ok.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.