Kiefer Sutherland has responded after Freddie Prinze Jr. labelled him “the most unprofessional dude in the world”.

Freddie, who starred alongside Kiefer in the eighth series of hit show 24 in 2010, launched an outspoken attack on Kiefer during an appearance at the Comic-Con event this weekend.

But now a representative for Kiefer has hit back at his former co-star’s claims.

“Kiefer worked with Freddie Prinze Jr. more than five years ago, and this is the first he has heard of Freddie‘s grievances,” Kiefer’s representative said. “Kiefer enjoyed working with Freddie and wishes him the best.”

The statement comes after TMZ.com cited new allegations that Freddie believed Kiefer’s alleged reliance on alcohol was to blame for his attitude on set.

A source reportedly close to Freddie told the website that Kiefer “would regularly show up on set drunk, sitting in his trailer often for hours, as everyone waited. It messed with the lives of the family of cast and crew.”

TMZ.com also claim that Freddie, who starred as Counter Terrorism Unit director Cole Ortiz in the series, had been set to take over from Kiefer as the lead in 24, but he pulled out after working with the actor.

However, a source at Fox, who broadcasted all nine series of the show including the most recent 24: Live Another Day, insisted that Freddie’s claims are entirely untrue.

“Kiefer was nothing but professional during the run of the show and is beloved by cast and crew,” the Fox source said. "It's so out of left field, five years later. We wouldn't have done another 24 if Kiefer were anything like Freddie described."

Online Editors