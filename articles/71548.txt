OnePlus will unveil their first smartphone at a press event on the 23rd of April, the company has been teasing the device for a while now.

The OnePlus One will come with CyanogenMod, and now we have some more information on what CyanogenMod will be like on the OnePlus One.

The OnePlus One will run CyanogenMod 11s, which is based on the latest version of Android, Android 4.4 Kit Kat, the photos below shows what the user interface in the device will look like.

OnePlus has already revealed some of the specifications on the new OnePlus One, the handset will feature a 5.5 inch full HD display with a resolution of 1920 x 1080 pixels.

We also know that the OnePlus One will come with a quad core Qualcomm Snapdragon 801 processor, as yet there are no details on what clock speed the processor will have.

The OnePlus One will also feature a 6 piece Sony Exmor IMX214 camera, which is a 13 megapixel camera, and the device will come with a 3,100 mAh battery.

The device will also come with a choice of two storage options, you get to choose between either 16GB or 64GB of built in storage.

The handset certainly sounds very interesting from what we have heard so far, and considering that the device will retail for around $400, it could end up being a very popular smartphone.

Source Engadget

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more