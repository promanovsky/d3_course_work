Hackers apparently based in Iran have mounted a three-year campaign of cyber-espionage against high-ranking U.S. and international officials, including a four-star admiral, to gather intelligence on economic sanctions, antinuclear proliferation efforts and other issues, according to cybersecurity investigators.

Using an elaborate ruse involving more than a dozen personas working for a fake U.S. news organization, the hackers developed connections to their targets through websites like Facebook and LinkedIn to trick them into giving up personal data and logon information, the investigators say.

The alleged campaign, which dates back at least to 2011 and is still under way, principally has focused on U.S. and Israeli targets in public and private sectors, but also has included similar officials in countries such as the U.K., Saudi Arabia, Syria and Iraq, according to the investigators.

The campaign was uncovered by the Dallas-based cybersecurity firm iSight Partners, which has been tracking it for six months. The iSight report provides the first detailed public look inside what the investigators say is an extensive cyber-espionage campaign against the U.S. by Iranian hackers, and shows to an extent not previously understood their ability to conduct extensive and lengthy targeting of key individuals, much in the mold of Chinese cyberspies.

"It is such a complex and broad-reaching, long-term espionage campaign for the Iranians," said Tiffany Jones, a senior vice president at iSight and a former National Security Council aide in the George W. Bush administration. "What they lack in technical sophistication, they make up in creativity and persistence."

Iranian hackers have developed a reputation for very public and destructive attacks on company websites and computer networks, particularly U.S. banks and Middle Eastern energy firms. But their clandestine cybertactics are less understood. They have infiltrated a U.S. Navy network and U.S. energy-company networks, The Wall Street Journal previously has reported.

In the past year, Iranian hackers have become among the top cybersecurity concerns for many U.S. military and intelligence officials who see them as more highly motivated to harm the U.S. than more traditional cyber-adversaries in Russia and China.

Iranian officials have denied any role in past hacking incidents, and charge the U.S. with being behind a massive cyberattack in recent years, unleashing the Stuxnet virus into Iranian computers.

Click for more from The Wall Street Journal