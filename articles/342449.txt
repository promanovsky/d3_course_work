Megan Fox on Baby: Motherhood Is 'Consuming,' Admits Feeling Mommy Guilt Megan Fox on Baby: Motherhood Is 'Consuming,' Admits Feeling Mommy Guilt

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Megan Fox opened up about the demands of motherhood just five months after welcoming her second child this week.

The "Teenage Mutant Ninja Turtles" star shares two sons with husband Brian Austin Green; Bodhi, five-months-old, and Noah, 21 months. Fox is also a step-mom to Green's 13-year-old son Kassius from a previous relationship. Consequently, the 28-year-old actress discussed motherhood and all of its challenges while speaking to Parents magazine.

"Before you have kids you really do not understand how much work it is and how consuming it is," Green said. "And then you have one and you're like, 'Oh my God, my baby is my whole world.' Every moment of the day is dedicated to this one baby and then all of a sudden you have two babies! Their needs are so different."

The "Transformers" star went on to admit that she is no exception to "mommy guilt" because of career as well as the needs of both children.

"It's really hard to manage because I also don't let them watch TV. It's not like I'm going to sit Noah in front of the television so I can take care of Bodhi," Fox explained to Parents. "I have to figure out how to incorporate Noah into the process and have him help me take care of Bodhi and make sure he doesn't get jealous and make sure nobody's neglected and everybody's needs are being met. As a mom it's hard because I don't feel like I'm ever giving either one of them 100 percent of my attention or 100 percent of myself, so I carry a lot of guilt."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Despite the challenges, Fox embraces the love for her children that has changed her life and enjoys watching her sons bond with one another.

"Noah's starting to interact with Bodhi," she said. "He'll try to comfort him sometimes when he cries and he'll do the 'sh sh sh sh.' And to watch him do that melts my heart. I'm excited for the future, to see them be brothers and to be best friends and I know there's gonna be lots of fighting, but there's also gonna be lots of hugs and kisses. It's sort of mind-blowing to think about how amazing the future is going to be with them."

Meanwhile, Fox and Green have been married since 2012 and are normally very private about their family. However, Fox has admitted she wants "at least two, probably three kids" in a past interview with Cosmopolitan magazine.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit