A three-day strike by the pilots' union at German airline Lufthansa will hit 16 of the airline's Irish flights.

Overall, 3,800 Lufthansa flights have been cancelled between now and Friday, affecting around 425,000 passengers.

Europe's largest airline described the industrial action over a long-running pay dispute as being one of the biggest strikes in its history.

Lufthansa was hit by a strike by airport workers last week.

It could face further protests in advance of a round of pay talks.