Another study has cast doubt on the existence of two of the first Earth-sized planets "discovered" outside our solar system that could potentially host liquid water, and therefore life. But it may also have uncovered a way to make detecting new planets easier.

When the discovery of Gliese 581 g was announced in 2010, there was a lot of excitement because it was in the habitable zone of its red dwarf star – the "Goldilocks" region – not too hot and not too cold – where liquid water, and therefore life, could potentially exist. The planet appeared to be just 1.2 to 1.4 times larger than Earth, and therefore likely to have a rocky surface.

Their signals are so small that just a very small tweak in how you analyze the data can make the difference to whether you see one planet or the other - Paul Robertson, Penn State University

Another planet in the system, Gliese 581 d, announced in 2007, was on the edge of the habitable zone.

"They were very high value targets if they were real," said Paul Robertson, a postdoctoral researcher at Penn state University and the lead author of the new study, published this week in Science.

"But unfortunately we found out that they weren't."

The two planets have actually been controversial since they were discovered using a method that looks for small changes in the colour of a star's light caused by the tug of a planet's gravity.

Robertson says it's because, if these particular planets exist, they have very low masses.

"And their signals are so small that just a very small tweak in how you analyze the data can make the difference to whether you see one planet or the other."

Several studies have disputed the planets' existence, based on different methods of analyzing the signals and ways of trying to sharpen them up. Among them was one by Phil Gregory, professor emeritus at the University of British Columbia. Another, by a Swiss team using additional data, prompted the scientists who discovered Gliese 581 g, Steve Vogt of the University of California Santa Cruz and Paul Butler of the Carnegie Institution for Science in Washington, D.C., to publish a new paper in 2012 criticizing the Swiss team's analysis and reaffirming the existence of Gliese 581g with their own analysis.

Robertson said that in his case, he and his colleagues were not trying to disprove the existence of Gliese 581 g, but to find a way to sharpen up small signals from planets and make them more visible by correcting for the activity of the star, which can also cause small shifts in the colour of a star's light.

Blue versus red

Vogt and Butler had previously done corrections using a standard method that looks at two colours emitted by calcium in a star, which are on the blue/violet end of the colour spectrum.

Robertson instead used a red colour emitted by hydrogen and analyzed the same data used in Vogt and Butler's 2012 paper.

Robertson said that works better in this case because Gliese 581 is a very cool star, and it doesn't put out a lot of blue light.

When applied to Gliese 581, the new correction made signals from three of the planets – b, c, and e - around the star appear sharper. The researchers say this confirms that Gliese has exactly three planets.

Robertson said that the technique should work in general for cooler, low mass stars and boost the signals of small planets.

"I hope this research points the way forward toward finding exciting new planets hidden beneath stellar signals," he added.

Vogt and Butler declined requests from CBC to comment on the new research.

University of British Columbia researcher Phil Gregory wrote in an email to CBC News that the new study improves understanding of the Gliese 581 system and will cause astronomers to pay more attention to all indicators of a star's activity, even if it "sadly removes a potentially interesting planet near the habitable zone."