Over the last year or so, Apple has been aggressively marketing in India, with full page cover ads in newspapers, television commercials, and numerous buyback schemes and price cuts. Wednesday's earnings call for the quarter ending in March highlighted how this has paid off, and although India remains an Android dominated market, Apple is making steady inroads here as well.

In the earnings call, Luca Maestri - Vice President of Finance and Corporate Controller, Apple said that "iPhone sales grew by strong double-digits year-over-year, and in India and Vietnam sales more than doubled." CEO Tim Cook later revealed that the company has seen 55% growth in India compared to the last quarter. Apple "established a new all-time record for total iPhone sales in the BRIC countries", Cook added.

(Also see: Apple reports strong iPhone sales, but iPad numbers are down)



While many were sceptical about moves such as reintroducing the iPhone 4 in India, and about the efficacy of the iPhone buyback schemes, it would appear that Apple is doing a good job of winning over smartphone users in India.

While a country specific number of handsets were not revealed, in the earnings call, Cook did say that the iPhone sales are at a record high, and attributed this at least partly to the popularity of devices like the iPhone 4s, which is available for just over Rs. 20,000 today.

This still seems like a very high price for a device which was first launched in 2011; in contrast, you can get a new device, such as the Micromax Canvas Knight for less, and you get a bigger and higher resolution screen, a bigger battery, a much more powerful processor and more RAM and more storage as well. You can see a full comparison here.

So why is it that an increasing number of people are buying what amounts to a dated handset? According to Cook, "85% of iPhone 4s buyers are switching from Android."

There are a number of factors at work; primary among which is the brand value that Apple commands. Aside from that, there is no denying that the app ecosystem on the iPhone is still one of the best - both the App Store and Google Play have a problem with clones and misleading apps, but this is much more pronounced on Google Play, where for a while the top selling paid app was an anti-virus app which actually did nothing at all!

Also, while the brands that are delivering "budget" handsets have been able to make significant improvements in build quality, there's still a huge difference in the feeling of holding one of these handsets, or something from a brand like Nokia, HTC, or Apple.

For Apple in India, the next big challenge is retail - there has been a lot of speculation over the last couple of years that Apple will enter the retail business here, but that has not taken place as yet. However, in the earnings call, Cook mentioned that, "we established an all time quarterly revenue record of almost $10 billion including the results from our retail stores," in China. He added that the new online and retail leader Angela Ahrendts will join Apple's executive team next week.

(Also see: Apple hires Burberry CEO Angela Ahrendts to oversee retail expansion)

With a strong presence established in China, and high growth in India, it might actually be possible that Apple looks towards India next; this is a move that could have strong consequences for the brand here.