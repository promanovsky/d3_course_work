BOSTON (CBS) – Former Boston Mayor Thomas M. Menino is undergoing treatment at Dana Farber Cancer Institute for an advanced form of cancer.

A spokesman for the former mayor confirmed a report of the diagnosis. The 71-year-old Menino left office in January after 20 years at the helm of the city.

Doctors told the Boston Globe the cancer was discovered in early February while doctors were examining Menino’s back because of chronic weakness in his legs.

They did a scan, and discovered that an advanced cancer of unknown origin has metastasized and spread to his liver and lymph nodes.

The former mayor began chemotherapy at Dana Farber in early March. The treatment is expected to continue for about three more sessions.

“I’ve never known Tom Menino to back down from a fight, and I don’t expect him to start now,” Mayor Marty Walsh said of his predecessor. “Mayor Menino has always been here for the people of Boston, and we’re behind him today, 100 percent. Our thoughts and prayers are with him, Angela, their family and many friends.”

Menino told the Globe he did not want people feeling sorry for him. He said he does not want sympathy, saying “we’ll get through it.”

The cancer is apparently unrelated to Menino’s two previous bouts with the disease – a lesion between his shoulder blades in 2003, and skin cancer two years ago.

Menino said he has not felt any pain and there was no earlier indication anything was wrong

“The mayor’s doing great. He is his usual self, cracking jokes and making people laugh,” said Dot Joyce, his longtime spokesman. “He has events tomorrow and throughout the week, and will continue to fight as he always does. He is one tough cookie.”

Menino called this the most profound challenge of his life. He continues his new job at Boston University and says he’ll be at work on Monday morning.

Share your thoughts: Send a message to former Boston Mayor Tom Menino as he battles cancer.

MORE LOCAL NEWS FROM CBS BOSTON

[display-posts category=”local” wrapper=”ul” posts_per_page=”4?”]