* Security forces ordered to impose strict measures

* Isolation unit in capital overflowing

* New isolation ward delayed by protests

* Liberia faces "humanitarian crisis" - assistant minister

* U.S. charity fighting virus withdraws non-essential staff

* U.S. Peace Corps to withdraw volunteers from area (Adds details on plan as announced, details from NGO)

By David Lewis and Emma Farge

DAKAR, July 30 (Reuters) - Liberia will close schools and consider quarantining some communities, it said on Wednesday, announcing the toughest measures yet imposed by a West African government to halt the worst Ebola outbreak on record.

Security forces in Liberia were ordered to enforce the steps, part of an action plan that includes placing all non-essential government workers on 30-day compulsory leave.

Ebola has been blamed for 672 deaths in Liberia, neighboring Guinea and Sierra Leone, according to World Health Organization figures, as under-funded healthcare systems have struggled to cope with the epidemic. Liberia accounted for just under one-fifth of those deaths.

"This is a major public health emergency. It's fierce, deadly and many of our countrymen are dying and we need to act to stop the spread," Lewis Brown, Liberia's information minister, told Reuters.

"We need the support of the international community now more than ever. We desperately need all the help we can get."

But highlighting international concern about the crisis, the U.S. Peace Corps said it was withdrawing 340 volunteers from Liberia, Sierra Leone and Guinea.

President Ellen Johnson Sirleaf said in a speech posted on the presidency's website that the government was considering quarantining several communities based on the recommendation of the health ministry.

An earlier draft of the measures sent to Reuters specified communities to be quarantined.

"When these measures are instituted, only health care workers will be permitted to move in and out of those areas. Food and other medical support will be provided to those communities and affected individuals," she said.

All markets in border areas are to be closed, she added.

Referring to the orders issued to the security forces to impose the plan, Brown, the information minister, added: "We are hoping there will be a level of understanding and that there will not be a need for exceptional force."

Mike Noyes, head of humanitarian response at Action Aid UK, said people affected by Ebola should be treated with compassion rather than "criminalized".

"Enforced isolation of a whole community is a medieval approach to controlling the spread of disease," he said.

The first cases of this outbreak were confirmed in Guinea's remote southeast in March. It then spread to the capital, Conakry, and into neighboring Liberia and Sierra Leone.

Concern deepened last week when a Liberian-American died from Ebola in Nigeria having traveled from Liberia. Authorities in Nigeria, as well as Ghana and Togo, where he passed through en route to Lagos, are trying to trace passengers who were on the same plane as him.

Some airlines in the region have cut routes to countries affected by Ebola despite the WHO saying it does not recommend travel restrictions as a step to control outbreaks.

Britain on Wednesday held a top-level government meeting to discuss the spread of Ebola in West Africa, saying the outbreak was a threat it needed to respond to. A U.S. administration official said on Monday President Barack Obama was also monitoring the situation.



OVERWHELMED

Earlier on Wednesday, Liberian health officials said an isolation unit for Ebola victims in Liberia's capital, Monrovia, was overrun with cases and health workers are being forced to treat up to 20 new patients in their homes.

Protests by the local community against the construction of a new isolation unit at Elwa Hospital have ended, said Tolbert Nyenswah, an assistant minister of health, but patients with Ebola symptoms will have to wait at home until work is finished.

"The staff here are overwhelmed. This is a humanitarian crisis in Liberia," Nyenswah told Reuters by telephone.

Nyenswah said the suspected patients were being treated by trained medical staff with full protective gear, but it would take at least 24-36 hours to build the new unit.

Initial resistance to building a new isolation unit highlighted the fear and mistrust health workers have faced across West Africa as they battle the outbreak, which has strained the region's weak health systems.

Dozens of local health workers - including Sierra Leone and Liberia's top two Ebola doctors - have died treating patients. Two Americans working for Samaritan's Purse, a U.S. charity operating in Liberia, were infected over the past week.

Samaritan's Purse said on Wednesday that Kent Brantly, a doctor working for the charity, and Nancy Writebol, a colleague who was also volunteering in Liberia, had shown a slight improvement but their condition was still serious.