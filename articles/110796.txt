As the season premiere's surprise cameo, the actress tells THR about auditioning with showrunner Matthew Weiner and playing the first character turned down by TV's beloved lothario: "I wonder if I should be offended."

Neve Campbell has been given strict instructions not to discuss her future on Mad Men. The actress, who made a surprise cameo during the final minutes of Sunday's season-seven premiere, is now subject to the drama's signature secrecy -- especially since her brief appearance ended on such an ambiguous note.

Campbell, instantly recognizable from Party of Five and the Scream films, took a seat beside Don Draper (Jon Hamm) during his red-eye flight from Los Angeles to New York. Her character, a young widow to an older gentleman, explains that she was out West spreading her dead husband's ashes at Disneyland. This being Mad Men, Draper's candid conversation with his seatmate turned into flirtation, and they end up getting cozy for the duration of the flight.

STORY: 'Mad Men' Creator Matt Weiner Says So Long to 1968

But there will be no mile-high jokes this time. Despite her nudge-nudge, wink-wink suggestion that they share a cab back to the city, she becomes one of the first women to get turned down by the lady-killer. The pair depart the flight separately, apparently never to see each other again. Campbell won't say if her Mad Men moment is already over, but she did speak with The Hollywood Reporter about her audition, her time on set and her own stance on airplane conversation.

How does it feel being the first woman to get turned down by Don Draper?

I wonder if I should be offended. It makes sense for the episode, I think, and it makes sense for what he's going through. We're in the final season, so hopefully there are going to be some transitions.

Did you get a call for the part?

My manager called and said Matt [Weiner] was considering a few people for this role and asked if would I consider auditioning -- and I said absolutely. I didn't know anything other than I had two scenes to read.

What is Matt like to audition for?

It's actually refreshing. Often you go into auditions and people don't have much direction to give you -- or people don't give you much of an opinion of what they want. But because Matt is really detail-oriented, it was all very clear. We had a conversation about my opinion on the character, but he was very specific about what he wanted, which is why I think the show has done so well. I think he's almost obsessive-compulsive about it. But I had heard their audition room with him was a very nice, creative process.

STORY: 7 Questions for Mad Men Season 7

Had you watched all of the show?

I watched the first season, but I actually moved to England for eight years and really stopped watching TV. Then, when I came back, I had a baby, so I didn't even have time. When he gave me the role, he asked me to watch the last season, and I basically crammed.

What's the biggest change between the first and sixth season?

The sideburns.

Do people chat you up on flights?

It always depends on who you end up sitting with and how ballsy they are. I don't want to talk on an airplane. It's quiet time. I'm almost sad we're allowed to use cell phones now. It was the one place you could go where you had to shut off.

Like all things Mad Men, your appearance has been very hush-hush. Have you experienced that before?

The Scream movies, for sure. People kept trying to publish the scenes online, so we had fake scripts out so people would think they had the right one. There was a lot of evasion.

What kind of scene partner is Jon Hamm?

He sets a really nice tone on set. I think you can always tell from how the crew is behaving, how the number ones are. If you're a number one, it's important to set a nice atmosphere, be professional, have fun and take it seriously. I think Jon does that, and the crew follows his lead.

PHOTOS: 'Mad Men' Season 7 Preview

What was your experience on set?

Everyone seems to love each other. They're all really enthusiastic about the job. They have a patio that they've built outside the hair and makeup trailers so they can sit outside and play dominos all day long.

Did you play?

I can play Mexican train dominoes, but not the regular kind.

You just passed on a pilot. Do you think about going back to TV full-time?

TNT offered me a show four weeks ago, but it was a procedural, and it was an outright lead. I'm navigating a very different world, as a mom, in terms of the projects I want to take. I'm willing to go back to TV if it's the right role, the right size and the right quality -- that's probably the reason why I'm developing a show with my partner right now. We're in the middle of research, and we're pitching to AMC, HBO and Showtime in a couple of weeks. We'll see what happens.