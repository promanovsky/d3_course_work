When Facebook (NASDAQ:FB) revealed Slingshot, its would-be Snapchat killer, it came with a demand: send a photo message if you want to see what your friends sent you. Will the feature be a boon to the fledgling network, and by extension, Facebook stock? Should on-the-fence investors buy into Slingshot's promise?

Guest host Alison Southwick puts these questions to Fool analysts Nathan Alderman and Tim Beyers in this episode of 1-Up On Wall Street, The Motley Fool's web show in which we talk about the big-money names behind your favorite movies, toys, video games, comics, and more.

The feature in question is called reply to-unlock and requires that users "sling" new messages to others before seeing what else they've received. To Nathan, the strategy amounts to holding data hostage in hopes of getting more users to post content. (Photos and videos, in this case.)

He doesn't see it working out well for Facebook. Instead, he likens Slingshot to Amazon.com's (NASDAQ:AMZN) efforts with the Fire Phone, which uses Firefly to lure users into buying more stuff. As Daring Fireball's John Gruber put it in a recent blog post: "If they give you phones in hell, this is the sort of app that's on them."

Tim agrees that reply to-unlock is a gimmick. But he also understands why Facebook introduced the feature. Too often, messages go into the ether and languish there. Sometimes a reply never comes. With Slingshot, participants are encouraged (OK, obligated) to engage in and admittedly simplistic act of digital dialogue. The resulting data should lead to better advertising while fueling Facebook's fast-growing mobile business, which was responsible for 79% of overall traffic last quarter.

What's more, Tim says Slingshot isn't much different than other efforts to gamify the digital experience. Foursquare started as a game and to this day remains a growing, successful location-based social network with long-term potential.

Now it's your turn. Click the video to watch as Alison puts Nathan and Tim on the spot, and then leave a comment to let us know your take on Facebook stock at current prices. You can also follow us on Twitter for more segments and regular geek news updates!