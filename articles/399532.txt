By Casey Sullivan

(Reuters) - OkCupid’s disclosure that the popular dating website intentionally misled couples about their suitability could open it up to a U.S. Federal Trade Commission inquiry, according to lawyers and experts in consumer-protection law.

On Monday, President Christian Rudder disclosed in a blog post that OkCupid had conducted experiments on its users, including a test to see whether its assessment of their matchability led to successful dating.

"To test this, we took pairs of bad matches ... and told them they were exceptionally good for each other," Rudder wrote. "When we tell people they are a good match, they act as if they are. Even when they should be wrong for each other."

OkCupid's actions, at least four legal experts said, appear to be in violation of a provision in the FTC act that prohibits "unfair and deceptive" practices by a company that result in misleading or harming consumers.

“When you’re matching people up with individuals who are not good matches, that would certainly be deceptive,” said Jesse Brody of law firm Manatt Phelps & Phillips.

Other legal experts said it would be difficult to prove deception.

"From a consumer-protection perspective it can be argued that the 'experiment' was an effort to ensure and confirm the efficacy of the service," said Robert deBrauwere of Pryor Cashman.

Rudder said in a statement to Reuters on Tuesday that more than 1 million people had logged on to OkCupid since he wrote about the experiment, and that the website had received fewer than 10 complaints.

He did not respond to a question about whether anyone had threatened to sue. The "handful" of users subjected to the experiment were notified at its conclusion, he said. This type of "diagnostic research" is permitted by the site's terms of service, Rudder added.

OkCupid's privacy policy says, among other things, "We may use information that we collect about you to perform research and analysis about your use of, or interest in, our products, services, or content ..."

William McGeveran, a law professor at University of Minnesota Law School, said giving wrong information to customers could have legal ramifications. "The FTC has been more aggressively (seeking enforcement) over information handling."

An FTC spokeswoman, Mary Engle, declined to discuss whether the agency was investigating OkCupid, a free website with some paid premium services. The service is owned by IAC/InterActive Corp.

Engle said that when the FTC considered which cases to investigate, it looked at whether there was harm in the deception. The agency was most likely to probe practices that caused economic or health injuries.

"If it's completely free, it's not clear what the consumer injury would be," Engle said.

(This version of the story was refiled to correct paragraph 9 to show all users affected by the OkCupid experiment were notified after the test)

(Reporting by Casey Sullivan in New York; Additional reporting by Diane Bartz in Washington DC; Editing by Ted Botha and Prudence Crowther)