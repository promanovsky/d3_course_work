Leaked screenshots of a new web interface for Gmail reveal that Google may be quietly testing a major design overhaul.

The pictures, which were first published on Geek, show a collection of potential new features, though the screenshots haven't been verified.

The new features are similar to the updates reportedly in the works for Gmail's mobile app. A new "pin" feature would let you drag and highlight an email at the top of your inbox — a function that would replace the existing "star" feature.

A major change also comes to the menu layout on the left, which looks strikingly similar to the look Google has rolled out for YouTube's menu. In addition to filtering email groups such as social, promotions, forums and updates, Gmail adds tabs for travel, purchases and finances. There is also a collapsible Hangouts feature on the righthand side, so you can easily connect with other users.

Image: Geek.com

Also included in the screenshots are functions (displayed as bubbles) on the bottom right that give users a quick prompt to compose an email, set a reminder and essentially create a to-do list that can be left unfinished.

Image: Geek.com

On mobile, the company is said to be testing a "snooze" button that sets a timer to go back and read certain emails, but it's unclear if this would roll out to the web version too.

It's also uncertain if and when the new look and features will ever see the light of day (if the screenshots are valid). But since the new interface represents a major departure from what's in place now, it's likely to roll out to a larger audience someday. Google wouldn't spend time on such an update if it didn't have big plans for it.

The company has not responded to Mashable's request for comment.

BONUS: The Many Faces of Gmail: A 10-Year Retrospective