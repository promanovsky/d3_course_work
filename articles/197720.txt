From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

E-book subscription service Oyster just fattened up its catalog.

After raising a total of $17 million to bring the Netflix model to e-books, Oyster is taking a moment to bask in its early success: the startup just surpassed half a million titles and managed to win over new deals with publishing behemoths HarperCollins, Chronicle, and Wiley.

Oyster’s promise is simple: “read unlimited books, anytime, anywhere.” The firm’s iPhone and iPad app debuted in October 2013 with just 100,00 titles. At 500,000, Oyster is set to eclipse Amazon’s own Kindle “lending library,” which currently sits at 500,000 titles.

Oyster, which competes with firms like Scribd and the less interesting Entitled, refused to share the size of its user-base, but told VentureBeat by phone that its subscribers spend an average of 45 minutes a day reading within the app.

While Oyster’s future rests in the hands of aging publishers, chief executive Eric Stromberg tells VentureBeat that the company is exploring ways to shake up the market — from the Spritz experimental reading tech to new social features.

According to Stromberg, “social is an important piece of what we’re building. We do believe that often the best recommendations come from friends. What we’ve built is version one of that, but we do want to continue to build on that. It’s really a focus for us in 2014: how do we use social to create a high signal vs noise around books?”