SAN DIEGO — A cruise ship from Los Angeles pulled into San Diego Thursday morning with several dozen passengers sick with flu-like symptoms.

Most of the passengers on the Crown Princess will enjoy a day of shopping and sightseeing in San Diego. But a reported 83 passengers who are sick will remain on the ship, according to the cruise line.

The illness could be Novovirus, which causes diarrhea and vomiting, is a highly contagious viral infection and, according to the San Diego County Health Department, is common to people living in close quarters, such as “nursing homes or cruise ships.”

The Crown Princess can accommodate 3,080 passengers. The week-long cruise began Saturday in Los Angeles and has included stops in San Francisco and Santa Barbara.

Advertisement

Since the outbreak, the ship’s staff has increased sanitation procedures on railings and door handles to “interrupt the person-to-person spread of this virus,” according to a statement issued by the cruise line.

tony.perry@latimes.com

Twitter: @LATsandiego