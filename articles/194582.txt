Jada Pinkett Smith has responded angrily to controversy surrounding an image of her 13-year-old daughter, Willow Smith, lying in bed with a 20-year-old actor, branding critics “covert paedophiles”.

“Here’s the deal,” she told the TMZ camera crew, who lay in wait for her arrival at LAX Airport in Los Angeles earlier.

“There was nothing sexual about that picture. Or that situation.

“You guys are projecting your trash onto it, and you’re acting like covert paedophiles, and that’s not cool."

The black-and-white shot of the young singer, casually reclining with topless actor Moises Arias, was initially posted via his Instagram account.

Arias, who rose to fame starring opposite Miley Cyrus in Hannah Montana, has since deleted the image.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

However, not before hundreds of his fans reposted it via various social media channels, or Smith’s behaviour – though appearing innocent – was deemed by online critics to be too mature for a newly teenage girl.

Loading....

It led to many questioning Pinkett Smith and husband Will Smith's alternative parenting techniques, which had come under fire for allowing their children to behave in a way far beyond their years.

Speaking of her decision to “let” Willow shave her head at the tender age of 12, Pinkett-Smith posted via her official Facebook page:

“The question why I would let Willow cut her hair, first thelet must be challenged.

“This is a world where women [and] girls are constantly reminded that they don't belong to themselves — that their bodies are not their own, nor their power or self-determination. I made a promise to endow my little girl with the power to always know that her body, spirit and her mind are her domain.

“Willow cut her hair because her beauty, her value, her worth is not measured by the length of her hair. It's also a statement that claims that even little girls have the right to own themselves and should not be a slave to even their mother's deepest insecurities, hopes and desires.”

The parents were famously “divorced” by their 16-year-old son, Jaden Smith, for his 15th birthday in 2013.