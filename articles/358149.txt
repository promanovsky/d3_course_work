Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Rolf Harris has today been sentenced to five years and nine months in prison following a string of sick sex attacks on young girls over nearly 20 years.

The disgraced 84-year-old entertainer was found guilty on Monday of all 12 charges he faced after he indecently assaulted four girls.

As the judge passed sentence, Harris showed no emotion in the dock. He was led away by two guards with one carrying his suitcase for him. He will serve half of the sentence before being released on licence.

Harris was told he had caused his victims severe psychological harm and had shown no remorse for his crimes.

The judge told him that his reputation lies in ruins following his convictions but he has no one to blame but himself.

CLICK HERE FOR LIVE UPDATES FROM THE SENTENCING

Nine of the assaults took place between 1968 and 1985 - one on a girl aged seven or eight and the rest on teenagers between 14 and 19.

The remaining three guilty verdicts were for three counts of indecent assault on Tonya Lee, who has waived her right to anonymity, in 1986 when she was 15.

Before he was sentenced, Southwark Crown Court heard victim impact statements written by the four women Harris is convicted of abusing.

(Image: WENN)

In a statement read out to the court, the former friend of his daughter Bindi said: "The attacks that happened have made me feel dirty, grubby and disgusting. The whole sordid saga has traumatised me."

She said the effects of Harris's abuse had been with her for many years, and had been the cause of a drinking habit she developed at an early age.

"As a young girl I had aspirations to have a career, settle down and have a family," she said. "However, as a direct result of his actions, this has never materialised.

"The knowledge of what he had done to me haunted me. However, his popularity with the British public made it harder for me to deal with."

The statement went on: "Rolf Harris had a hold over me that made me a quivering wreck", adding: "He made me feel like a sexual object, he used and abused me to such a degree that it made me feel worthless."

The woman said she had been convinced nobody would believe her, but her family had when she finally told them.

She said she had been "dry" since 2000 but still suffered panic attacks and severe anxiety and was unable to communicate and socialise with people outside a small circle, making her world "very small".

Harris was pictured travelling to court by boat this morning as he prepared to be locked up for carrying out the assaults.

The disgraced entertainer was pictured leaving his Berkshire home on the boat in an attempt to avoid reporters.