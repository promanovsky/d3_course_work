E-cigarettes are more effective than nicotine patches and gum in helping people to quit smoking, according to a study that challenges the negative views of some public health experts.

The issue of e-cigarettes has become a public health battleground, alarming those who think that their marketing and use in public places where smoking is banned risks re-normalizing tobacco.

Supporters say the vast majority of smokers are using e-cigarettes to kick their tobacco habit and that the health consequences of nicotine use without the tar from cigarettes appear, as yet, to be far less of a problem.

The study, by a team from University College London, looked at attempts of nearly 6,000 people to stop smoking and found that, while engaging with the NHS smoking cessation services was the most effective way to quit, using e-cigarettes beat nicotine replacement therapy as well as the efforts of people to stop with no help at all.

Professor Robert West of the department of epidemiology and public health, the senior author of the study, said that it was extremely important to find out how well e-cigarettes worked as a quitting tool. "It really could affect literally millions of lives. We need to know," he said.

He admitted, however, that it was a controversial area. He also acknowledged opponents' fears and suspicions about the commercial involvement of scientists. "I don't and will not take any money from any e-cigarette manufacturer," he said. His department does take money from pharmaceutical companies that make smoking cessation drugs, but they are rethinking that. "I need to be able to talk about e-cigarettes without even the conception of conflict of interest," he said.

The study, published in the journal Addictions, was based on surveys of people who had stopped smoking in a 12-month period between July 2009 and February 2014. In recent years, an increasing proportion report using e-cigarettes rather than over-the-counter nicotine replacement therapy such as patches or gum. Many try to quit without support. Those who cut out cigarettes completely do better than those who try to reduce the numbers they smoke. A very small proportion get help from the NHS smoking cessation services.

When the results were adjusted to account for the differences between the smokers in terms of background, age and other variables, those using e-cigarettes were around 60% more likely to quit than those using nicotine replacement therapy or just willpower.

"It is one piece of the jigsaw and not a final answer," said West. "It may be different in different countries. If it [use of e-cigarettes for quitting] continues to grow, we will expect a public health benefit."

He said there were misconceptions about e-cigarettes. "Despite what a lot of people think, e-cigarettes are not good news for the tobacco industry and the tobacco industry would like them to go away. They sell tobacco and would like to go on doing that," he said.

The European commission has decided that e-cigarettes should be regulated as consumer products below a certain nicotine strength, but that the higher strength versions, which campaigners say those trying to stop smoking need, should be regulated as medicines. In this country that would be the responsibility of the Medicines and Healthcare Products Regulatory Authority (MHRA). Two MHRA licenses have so far been applied for, both by tobacco companies. West says that is because only the tobacco industry has the money and the time necessary to go through the licensing process, unlike the small and medium-sized e-cigarette manufacturers, which is a problem.

If e-cigarette use as a smoking cessation tool were to be supported in any way by the NHS, it would have to be via MHRA-licensed products to ensure quality, he said.

"I think it would be perfectly reasonable for people to consider e-cigarettes for use with their patients," he said. "The NHS would only get involved where the products had that mark of approval. I don't think the NHS could engage with e-cigarettes outside that process."

Pharmaceutical companies such as GSK and Pfizer, which make smoking cessation drugs, are among the opponents of e-cigarettes. "They are losing sales hand over fist to e-cigarettes and are incentivised to make it appear they are not effective," said West.

A section of the public health community is also hostile. "It is related to a broad distaste for large corporations making large amounts of money out of psychoactive drugs," he said.

"You might see some of it as a puritanical ethic, which is a strong driver."

This article originally appeared on guardian.co.uk