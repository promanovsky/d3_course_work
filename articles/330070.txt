Using data from the Massachusetts Department of Public Heath, The Boston Globe has created an interactive map showing where approved and rejected marijuana dispensaries are located throughout the state.

Dispensaries that remain approved near Boston include Garden Remedies, Inc. in Newton; New England Treatment Access, Inc. in Brookline and Ermont in Quincy.

Eliminated dispensaries near Boston include The Greeneway Wellness Foundation, Inc. in Cambridge and Good Chemistry of Massachusetts, Inc. in Boston.

The Boston Globe reported today that “State health officials announced Friday that, they have eliminated 9 out of the 20 applicants given initial approval for medical marijuana dispensaries.’’

Some dispensaries could open by November, but most will not be open until February 2015, according to Karen van Unen, the executive director of the state’s medical marijuana program during a news conference, The Globe reported.