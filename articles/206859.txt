A troop of Boy Scouts rescued Ann Curry in New York’s Harriman State Park last month after she fell and broke her ankle while hiking reports The New York Post.



According to a blog post on scoutingmagazine.org, Scouts from Troop and Crew 368 from Berkeley Heights, NJ, were on a training hike when they spotted Curry sitting on the side of a trail with her leg extended. The NBC News reporter told the Scouts she thought she broke her ­ankle, and the boys sprang into action.



“They splinted it up perfectly,” said Scoutmaster Rick Jurgens. To transport Curry down rough terrain to the base of the park, the Scouts used sticks and a tarp to make a stretcher, and carried her to her car where her husband and son were waiting.



Troop 368 didn’t know they’d saved someone famous until park rangers told them afterward that the woman they had rescued was Curry.



The injured reporter, whose leg will take 10 to 12 weeks to heal, wrote a letter to the troop thanking them: “I feel enormously lucky that you came along at just the right moment, and were so willing to help a stranger in need.”