James Martin/CNET

Vic Gundotra, head of Google's social network Google+, announced in a Google+ post Thursday that he is leaving the company effective immediately.

His departure follows a rumor posted two days ago on the app Secret that, "Vic Gundotra is interviewing." A person familiar with the matter said that Gundotra had problems with Google's management team.

CEO Larry Page praised Gundotra in his own Google+ post, thanking him for "a tremendous almost eight years at Google" and for building Google+ "from nothing."

"I really enjoy using Google+ on a daily basis, especially the auto awesome movies which I really love sharing with my family and friends," said Page.

In addition to spearheading the development of Google+, Page congratulated Gundotra for his work on guiding the teams that introduced turn-by-turn navigation to Google Maps, and improving Google's developer relations, which Page described as "disparate efforts" before Gundotra fixed them.

However, his tenure was also a rocky one. Soon after the launch of Google+, Gundotra came under heavy criticism for insisting that Google+ users must display their real names, as opposed to pseudonyms.

Gundotra came to Google in 2007, following a 16-year stint at Microsoft as a general manager and a year off working on charitable causes.

Gundotra has not revealed where he will be going, although his abrupt departure indicates that he could be going to a competitor. The future of the social network that Gundotra founded is almost as unclear as Gundotra's next move.

Going forward, Google+ will not be led by Gundotra's right-hand man and the social network's product manager Bradley Horowitz, reported Recode. Instead, Google confirmed that Google+ Vice President of Engineering David Besbris will take command of Google+.

What Besbris' plans for Google+ are as-yet unknown. There's been a lot of speculation that popular Google+ services like Photos will be broken off and the social network will be re-purposed as a platform.

However, Google "vehemently" denied the rumors to CNET.

"Today's news has no impact on our Google+ strategy -- we have an incredibly talented team that will continue to build great user experiences across Google+, Hangouts and Photos," a Google representative said.

Update, 5:53 p.m. PT: Adds additional background.

Update, 5:29 p.m. PT: Adds Google denial of Google+ rumors.

Update, 11:45 a.m. PT: Adds Google confirmation of Besbris' new role.