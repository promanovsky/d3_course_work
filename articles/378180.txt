Microsoft is reportedly preparing for its biggest round of job cuts in five years, affecting thousands of staff worldwide.

The redundancies, which will include the firm's Xbox division, could even become the most extensive in the company's history, according to Bloomberg.

Microsoft



Xbox's UK-based European marketing division is said to have been earmarked for significant cuts, along with engineering teams and divisions related to the Nokia business Microsoft acquired earlier this year.

Meanwhile, a source has told MCV that the Xbox team in Reading will be "significantly impacted", with an internal announcement expected this Thursday.

The report follows comments from Microsoft CEO Satya Nadella, who recently pledged his commitment to the firm's Xbox arm and downplayed rumours that it could be sold off.

"Microsoft will continue to vigorously innovate and delight gamers with Xbox," he said. "Xbox is one of the most-revered consumer brands, with a growing online community and service, and a raving fan base.



"Bottom line, we will continue to innovate and grow our fan base with Xbox while also creating additive business value for Microsoft."

Microsoft could reportedly announce the redundancies as early as this week.

Watch Digital Spy's Xbox One hardware review below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io