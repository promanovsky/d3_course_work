Forget the Gold Rush—California saw a flood of people enrolling in Obamacare after its deadline was effectively extended until Tax Day.



As of Tuesday, California had enrolled about 3.3 million in either private insurance plans or the state's version of Medicaid since Oct. 1, officials announced Thursday.

Of that total, the state's Obamacare exchange signed up nearly 1.4 million people in private health insurance plans by the close of open enrollment, officials said. That tally exceeds original projections by nearly 816,000 people.

"That is a huge number," said Peter Lee, executive director of the Covered California exchange, of the 1,395,929 official sign-ups since enrollment began Oct. 1.

Read MoreObamacare helped 10 million uninsured, says Gallup



The remaining 1.9 million people enrolled in Medi-Cal, the state's Medicaid program, officials said. Unlike enrollment in private Obamacare plans, which is closed until next fall, enrollment in Medi-Cal is open year-round.



Lee said a significant number of people took advantage of the deadline extension that California offered people who had started applications for private plans by the original March 31 deadline, but had not completed their enrollment by then.