WEIRD Al Yankovic has poked fun at Pharrell Williams’ Happy in his latest parody.

The musical prankster’s version is called Tacky and features dancing cameos from stars such as Modern Family’s Eric Stonestreet and Jack Black.

Tacky is the first track from Weird Al’s new album, Mandatory Fun, with the Grammy-winner set to release a new video each day this week.

Apart from Pharrell’s Happy, the 54-year-old also parodies Robin Thicke’s song Blurred Lines (his version is called Word Crimes), Lorde’s Royals (Foil) and Imagine Dragons’ Radioactive (Inactive).