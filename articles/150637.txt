Tori Spelling’s revealing docu-series ‘True Tori’ premieres Tuesday, April 22 on Lifetime and she’s not hiding anything from the cameras. Click to WATCH Dean McDermott admitting that sex with Tori isn’t so great.

Tori Spelling and Dean McDermott‘s relationship is anything but happily ever after, and in this brand new clip, the duo tells all in couple’s therapy. Dean admits to cheating, Tori cries and then Dean bashes their sex life. Keep reading to see Tori break down into tears.

‘True Tori’: Dean McDermott Admits Sex With Tori Spelling ‘Wasn’t Fantastic’ — WATCH

Lifetime’s True Tori documents Tori Spelling and Dean McDermott’s rocky relationship, as the couple tries to remedy their unknown future together. And if this teaser is any indication, their future isn’t looking so bright.

“We have four kids so in the sex department, there were ebbs and flows,” Dean says, as he sits next to his wife Tori in couple’s therapy. Tori doesn’t agree: “We had a great relationship and we had a great sex life.” But Dean disagrees.

“We would have sex once every two weeks,” he says. “It wasn’t fantastic.”

How rude! A weeping Tori reveals, “I can never give him enough sex. He’s never going to be happy with just me.” The therapist tells Dean that his marriage expectations are unreasonable and like a fairytale. Then Tori asks Dean why he cheated on her with another woman and he simply responds, “I wasn’t attracted to her. It was just like a warm body.”

Tori expresses her anger and, as HollywoodLife.com previously reported, Dean gives his version of an apology, saying, “My worst nightmare is what I did…I cheated on my wife. That’s my worst nightmare.”

Tori stands up for herself asking if his worst nightmare was really cheating or was it just getting caught. Dean, who admitted sex was an “escape” just like drugs and alcohol, is stunned and left speechless.

True Tori premieres on Lifetime tonight, April 22 at 10/9c.

HollywoodLifers, how do you think the therapy session will end up? Do you think that Tori and Dean will work out their problems? Let us know if you think Tori should stick with Dean or throw him to the curb!

— Elizabeth Wagmeister

Follow @EWagmeister

More Reality TV News: