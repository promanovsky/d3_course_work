Updated from 8:24 a.m. EST April 8, 2014 to include Sesame Street's new video on-demand service, Sesame GO.

NEW YORK (TheStreet) - I spent the better part of the past weekend on my couch "binge watching" Netflix's (NFLX) - Get ReportHouse of Cards -- the hugely successful, Emmy-winning original series launched by the streaming service in early 2013. It was one of the reasons I decided to recently pony up for a monthly subscription. I may be late to watching two seasons of House of Cards, but to Netflix, that doesn't matter. At 44 million members and growing, I'm the kind of customer Netflix is looking to appeal to by launching more original series.

The fact that Netflix, among others, has been so successful in creating original content for its subscribers has huge implications in consumers' living rooms, Hollywood, the boardrooms of America's media giants and even in parts of Silicon Valley. The media industry is in a state of flux and it's anybody's guess as to who will be the ultimate winner.

Admittedly, I have not made it through the entire two seasons yet and when I get home tonight, I'll forgo my usual television programming to continue my binge -- again exactly what Netflix wants - where the value of the service is so compelling that I wouldn't dream of cancelling my $8 a month subscription.

Netflix isn't the only company that wants me to binge watch something I can only get through its service. From Amazon (AMZN) - Get Report, Hulu and Yahoo! (YHOO) to even Microsoft's (MSFT) - Get Report Xbox, a host of companies are getting into the original content game, further rocking the already teetering traditional cable television industry. By answering consumer demand for television programming when they want it (on-demand) and how they want it (via their laptops, tablets, iPhones and yes, even their TVs), streaming services are eyeing the dollar signs whether in subscriptions or advertising, by providing a new way to give consumers what they want. The stakes are incredibly high.

"I think we're headed toward a convergence. The [poster child] of all this is House of Cards on Netflix and the fact that it's gotten consideration and even won some Emmy awards," said Brad Adgate,senior vice president of research at Horizon Media. "The second season became an event for Netflix subscribers and binge viewing. That really set the tone that streaming video is a viable competition to what we would call traditional television. That opened a door for other original series to be streamed online."

"That whole broadcast TV model that's been with us for 50-plus years is slowly going away," he added. "You're seeing the impact. Even the networks are ordering shows without a pilot, which is what Netflix did with House of Cards. The model is changing and it's all being driven by cable and now streaming video."

Even Sesame Street is eyeing potential revenue from an on-demand service. Sesame Workshop, the nonprofit educational organization behind the children's long-standing television show launched a subscription video-on-demand service for $3.99 a month or $29.99 annually where viewers can watch Sesame Street episodes, Sesame Street Classic episodes and Pinky Dinky Doo episodes.

Further fueling the fire is the fact that consumers don't care how they watch shows. For every millennial that's comfortable watching shows via their iPhone, a proportionate number of Baby Boomers prefer to watch on a traditional television. Despite the technological difference, they share one common trait: the content has to be good. In the "golden age of content," there has been no shortage - from AMC Networks' (AMCX) - Get ReportWalking Dead to Amazon's Alpha House. Still, a hit show is a formula that no one can 100% predict, so TV networks are not only competing with themselves and premium channels like Time Warner's (TWX) HBO and CBS' (CBS) - Get Report Showtime, but the slew of nontraditional Web-only companies eager to get in the game.

"These companies are very, very good at analyzing data and they use it to further their businesses, but when it comes to what's going to be a hit show ... It's a very mysterious thing as to what's going to resonate with people," said Paul Verna, senior analyst at eMarketer.

The consumer may have more choice now when it comes to watching television and can even go so far as to drop their traditional cable TV package, but there are still limitations, specifically different companies gaining exclusive content deals. For instance, I don't have Amazon Prime so that means I can't watch Downton Abbey, which Amazon gained the exclusive rights to recently. But I do have Netflix and HBO, so House of Cards and Game of Thrones is viewable.

Verna says this is still a disadvantage for consumers because they're not likely to buy all the services, they will still have to pick and choose what best suits them. And that means it will be hard for one streaming service to win over them all. "The audience is there, but the audience is very fragmented."

Adgate agrees. "There no real reach vehicle online. You're not reaching a lot of viewers. This is just hyper-fragmented," he says. "There's just more and more choices meaning there is just going to be more and more programs to watch and it's going to get harder and harder for any particular program or advertiser" to dominate.

This is where the original content will play an important role. "This is going to be a very competitive landscape in the coming years," Adgate noted.

It will also be "the next hot product category with advertisers. The ones who watch these shows are the ones who advertisers covet -- the 18 to 49 age group," Adgate stated.

Indeed it already is. Yahoo! is hoping to woo advertisers by culling four new Web series that would rival shows on Netflix or premium cable channels, according to The Wall Street Journal.

Last week, just before Amazon announced its new Fire TV set-top box, it also proudly boasted it greenlighted six new original shows for Prime Instant Video and announced that Alpha House, its first foray into original episodic programming starring John Goodman, would return for a second season.

It's also appealing to studios, directors and actors. House of Cards' central character is played by Kevin Spacey, while Amazon's Alpha House stars John Goodman. "They're willing to do it because it's a shorter time commitment that allows them to work on other projects. [If you have] an A-list director or cast behind it, they're not going to put up something that's not going to be good," Adgate said.

"One of the reasons that [House of Cards] ended up going to Netflix in the first place is because the story they wanted to tell did not fit a formula that suited [TV], where you have a pilot," says Adam Mosam, CEO and founder of Pivotshare, a platform that helps small media publishers monetize their online content by creating an online marketplace for the sale and purchase of said content. "They wanted to tell their story. It's more like a 13-hour long movie. In that case it comes down to creative freedom -- that's what Netflix is offering." Pivotshare clients include Jillian Michaels, Kirk Cameron and Jeremy Irons.

Companies are bringing in big time executives to cull and expand their original offerings. Microsoft hired CBS executive Nancy Tellem in 2012 to head up its Xbox Entertainment Studios. Microsoft has recently greenlighted six original series for its new Xbox television, specifically "shows that can be combined with the interactive components to encourage users to engage across consoles, phones and tablets" according to a BloombergBusinessWeek article on Monday.

Last week Hulu announced Craig Erwich was joining the Web-based television company as a senior vice president and head of content. Erwich, who began his new job on Monday, was most recently the EVP of Warner Horizon Television, the unit of Warner Bros. (owned by Time Warner), that produces scripted cable and reality television series. There, Erwich oversaw development, production and business operations for the past seven years, which included the creation of hit series including Pretty Little Liars, The Voice, The Bachelorette, House, Prison Break and 24.

"Craig is ready to hit the ground running and lead us as we increase our overall content offerings, and continue to invest in original first-run TV programming, last night's TV, and great library TV from the U.S. and other markets," Hulu's CEO Mike Hopkins said in a blog post about the hire. "Craig is the perfect guy for the job - he has been developing shows and programming networks for over 20 years."

Hulu is about to launch its latest original series, Deadbeat, on April 9 as well as the second season of The Awesomes this summer. With Erwich coming on board, you can be sure that adding more original content will be a key strategy for Hulu.

So does this mean we could see Apple (AAPL) - Get Report getting into original content?

The company keeps adding channels to its Apple TV set-top box. While it's not in Apple's direct wheelhouse, "the more expertise they gain in things like music and movies the more they are in a position to fund things themselves," eMarketer's Verna says, such as allotting development money for an incubator specifically for studio production. "I would think it's a natural evolution of where Apple has been."

Will consumers be in the cross hairs or the winners as the media industry is recreated?

--Written by Laurie Kulikowski in New York.

Follow @LKulikowski

Disclosure: TheStreet's editorial policy prohibits staff editors, reporters and analysts from holding positions in any individual stocks.