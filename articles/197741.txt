James R. Healey

USATODAY

General Motors says it is recalling 8,590 of its 2014 Buick LaCrosse and Chevrolet Malibu sedans because the factory might have put rear brakes on the front wheels.

That won't cause an immediate problem. But the brakes will wear out faster, which could, in extreme cases, cause part of the front brake assembly to come loose and the brakes to fail, according to the car company.

Rear brakes typically are skimpier than fronts, because the car's weight shifts forward when stopping, putting the load on the front brakes. In the case of the GM cars, the back brake rotors are about a quarter-inch smaller than the fronts.

The recall covers 8,208 vehicles manufactured for the U.S. between Jan. 29 and March 31, 2014. The majority of the affected vehicles are in dealer stock. Only 1,694 are in owners' hands.

Another 209 cars in Canada and 173 in Mexico are part of the recall.

GM says only "a very small percentage of the recalled vehicles" will have the flaw.

Owners of the recalled Chevrolet and Buick models can get free replacement cars while waiting for theirs to be inspected and, if necessay, repaired, GM says.

The fast pace of recall announcements could, perversely, be an accomplishment. CEO Mary Barra earlier this year appointed a vice president president in charge of global vehicle safety, Jeff Boyer.

He can report directly to Barra, if necessary to cut red tape, and is expected to clean house on recalls. The quickened pace could be a sign he is successfully finding and dealing with problems that previously would have festered.

Barra and GM have been on the hot seat after a recall that began in February and grew to 2.6 million 2003 - 2011 small cars with faulty ignition switches. They can unexpectedly move and shut of the engines and disable the airbags.

GM links the faulty switches to 12 deaths and 31 crashes in the U.S., and to one fatal crash in Canada.

Others:

•A few days ago, GM recalled 56,214 Sautrn Aura sedans because a transmission cable can come loose and prevent the vehicles from being shifted into the proper gear. A driver can shift the gear lever into "park" but the transmission won't engage that setting and the cars can roll away. Earlier this year, GM recalled 2014 Buick Regal, Chevrolet Impala, 2014 Buick LaCrosse, Verano and Enclave, 2014 Chevrolet Malibu, Cruze and Traverse and 2014 GMC Acadia for a similar problem.

•Also within the past few days, GM recalled 51,640 of its popular 2014 Buick Enclave, Chevrolet Traverse and GMC Acadia crossover SUVs to fix inaccurate gas gauges. If the vehicles unexpectedly run out of gas, they can stall and cause a crash.

•In March, GM recalled about 172,000 Chevrolet Cruze 1.4-liter turbo sedans from the 2013 and 2014 model years. Their right front wheels could come loose when a faulty axle breaks.

•Mid-March, GM recalled 1.18 million full-size crossover SUVs because seat-mounted side airbags could fail due to a fault in the wiring. Those are 2008-2013 Buick Enclave and GM Acadia, 2009-2013 Chevrolet Traverse, 2008-2010 Saturn Outlook.

•At the same time, it recalled 63,900 2013 and 2014 Cadillac XTS full-size sedans because a plug in the brake system can come loose, allowing corrosion into the system, causing overheating that can lead to engine-compartment fires. GM said it announced the recall after reports of two fires at dealerships in dealer-owned cars, and two reports of warranty claims, but not fires, in customers' cars.

•Also in mid-March, GM recalled 303,000 2009-2014 big commercial vans. Unbelted front passengers in full-size Chevrolet Express and GMC Savana vans can suffer head injuries in crashes or hard stops because the material covering the passenger-side air bag doesn't meet federal standards

Passengers who wear safety belts are not at risk, GM said.