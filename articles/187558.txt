Record producer Clive Davis has become the latest big name to shun hotels owned by Sultan Hassanal Bolkiah, who announced last week that he would proceed with the introduction of the Islamic Sharia law, a criminal code that permits severing of limbs and death by stoning for people who are gay or commit adultery.



Clive Davis Has Joined Others In Boycotting The Dorchester Chain Of Hotels.

Through the Brunei Investment Agency, the Sultan owns the Dorchester Collection luxury hotel chain, which includes the Hotel Bel-Air and The Beverly Hills Hotel in Los Angeles. As a result of the Brunei law-making, celebrities and other powerful individuals who used to frequent one of the high-end hotels have collectively put their foot down to boycott the chain.

Outraged stars including Sharon Osbourne, Stephen Fry, Ellen Degeneres, and Richard Branson have taken to social media to denounce the hotels. The latter businessman ruled that no Virgin employee or family will stay at the Dorchester group "until the Sultan abides by basic human rights."



Sharon Osbourne Is One Of Those Outraged By The Introduction Of Sharia Law In Brunei.

The hotels have also taken a hit from those cancelling their functions and large-scale events, including Jeffrey Katzenberg's The Motion Picture & Television Fund (MPTF) Night Before Oscars event as well as the Women's rights organization the Feminist Majority Foundation's Global Women's Rights Awards, which is co-chaired by Jay Leno and his wife Mavis.

Additionally, the Hollywood Reporter has notified the Beverly Hills Hotel that it will not hold its annual Women in Entertainment breakfast there and the teen suicide prevention charity Teen Line has forfeited its $60,000 deposit in order to move its event elsewhere.

No @Virgin employee, nor our family, will stay at Dorchester Hotels until the Sultan abides by basic human rights http://t.co/k1hMHAS5ft — Richard Branson (@richardbranson) May 3, 2014

It has been speculated that the hotel boycott will have a greater impact on the employees of each of the shunned establishments, considering Bolkiah is estimated to be worth $20 billion. "There are no job losses yet, but there is definitely a feeling of instability and the staff is concerned," said a source via FOX411. "Without Hollywood types eating there or staying in the rooms and booking major Hollywood functions, there is no need to keep extra employees on."

However, Hollywood as a whole has been commended for its actions and the MPTF has responded to fears for hotel employees, saying said it "cannot condone or tolerate these harsh and repressive laws and as a result support a business owned by the Sultan of Brunei."



Ellen Degeneres Is Amongst Those Who Have Taken A Stance Against The Brutal Sharia Law.

The fund added "We sincerely regret that the employees and management of the hotel may suffer because of our response and the response of many other organizations that have aligned against this outrageous and unacceptable legal code."

In a statement, the chain has implied that the incidence of an individua such as the Sultan being behind everyday companies and brands is not isolated: "Most of us are not aware of the investors behind the brands that have become an integral part of our everyday life, from the gas we put in our cars, to the clothes we wear, to the way we use social media, and to the hotels we frequent."



Richard Branson Has Publically Voiced His And Virgin's Boycott Of The Dorchester Collection.

They also indicated that the laws of Brunei will not affect the way the hotels are run. "We continue to abide by the laws of the countries we operate in and do not tolerate any form of discrimination of any kind. The laws that exist in other countries outside of where Dorchester Collection operates do not affect the policies that govern how we run our hotels," said a Dorchester rep.

The Beverly Hills City Council will reportedly be meeting today to discuss a resolution to the situation by condemning Brunei's new laws and encouraging "the government of Brunei to divest itself of the Beverly Hills Hotel."

More: NBC to broadcast Richard Branson's first space flight.

More: Clive Davis & his war of words with singer Kelly Clarkson.