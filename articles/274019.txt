MOUNTAIN VIEW, Calif., June 4 (UPI) -- Google is offering a new extension for its Chroime web browser that will let users encrypt their emails making snooping more difficult for the NSA.

The extension, called End-to-End, is based on open source encryption standard, OpenPGP, and will enable users to encrypt an email from the time it leaves their computers until it is decrypted by the recipient. The tool will require that both the recipient and sender use End-to-End or some other encryption tool to send emails.

Advertisement

"It's important that the government not overstep," Eric Grosse, Google's chief of security, said in an interview last week. "We don't want any government breaking the security of the Internet."

The new tool will make it more difficult for the National Security Agency or other agencies to collect collect information from a person's email. While encryption doesn't mean that the agency or hackers can't access a user's email, it will force them to hack directly into computers to access email rather than catching emails in transit.

Decrypting intercepted emails is very labor intensive and requires a great deal of technical expertise, despite cryptographic advances made over the years.

"Emails that are encrypted as they're routed from sender to receiver are like sealed envelopes, and less vulnerable to snooping -- whether by bad actors or through government surveillance -- than postcards," said Brandon Long, the tech lead for the Gmail Delivery Team.

Google also released a memo Tuesday stating that while the company was committed to encrypting emails sent from its servers it was difficult to ensure safety once the email was beyond its grasp. It estimates that 40 to 50 percent of emails sent between Gmail and other email providers aren't encrypted at all.

RELATED Diane von Furstenberg designs frames for Google Glass

The End-to-End extension is currently not available and it being shared with a few with the Google developers community to be tested. The extension will be available on Chrome Web Store once it is ready.

RELATED FCC comments section crashes after John Oliver asks trolls to inundate the system