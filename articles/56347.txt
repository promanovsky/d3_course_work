The United Nations Security Council will meet Friday to discuss the ongoing crisis in Ukraine as concerns mount over new details about Russia's military buildup on the Ukrainian border, including Moscow's reported efforts to camouflage troops and equipment.

The council is expected to meet privately Friday afternoon, a day after the U.N. General Assembly approved a resolution affirming Ukraine's territorial integrity and deeming the referendum that led to Russia's annexation of the Crimean Peninsula illegal.

The vote came as the Pentagon said there are no indications that Russian forces along the border with Ukraine are carrying out the kind of legitimate military exercises that Moscow has cited as the reason for their controversial deployment in the region, Reuters reported.

"We've seen no specific indications that these -- that exercises -- are taking place," Pentagon spokesman Rear Adm. John Kirby told reporters.

[pullquote]

More On This... Russia agrees to turn over some weapons, military equipment in Crimea to Ukraine

The Wall Street Journal reported late Friday that Russian troops are concealing their positions and establishing supply lines, stoking fears among U.S. intelligence agencies that have struggled to assess Russian President Vladimir Putin's plans.

Senior U.S. military officials told the newspaper that Russia's efforts to camouflage its forces and equipment could be designed to obscure images taken by American spy satellites or to conceal the location and size of their force from the Ukrainian military.

Another senior military official told The Journal that the Pentagon is concerned that the Russians have moved into place additional supplies, such as food and spare parts, which could support both a routine exercise or a prolonged military incursion into Ukraine.

"They are positioning logistics. That is necessary for the exercise but could also be used for further aggression if they choose to go," the official said. "They have in place the capability, capacity and readiness they would need should they choose to conduct further aggression."

U.S. Defense officials told Fox News the numbers of troops far exceeds the amount needed for a training exercise. And the fact that there is no real evidence any large-scale exercises have occurred, and that none of the troops have returned to their bases, is also concerning to U.S. observers.

Some have estimated the troop strength to be at about 30,000 -- Rep. Michael Turner, R-Ohio, though, claimed Thursday that the number could be as high as 80,000. It is believed that an additional 50,000 troops may have flooded the region in the last few days.

Meanwhile, new U.S. intelligence assessments say there are more indications than ever that Russia could invade eastern Ukraine. The new thinking is based mostly off analysis of public information, such as heightened rhetoric from Putin and his claims that Russian-speaking people in Ukraine face "brutality." He is building a public case for more military action, according to senior U.S. officials.

"The thinking in the U.S. government is that the likelihood of a major Russian incursion into Ukraine has increased," a senior U.S. official told Fox News.

In Rome as part of an overseas tour, President Obama stressed the need for the U.S. to support Ukraine. The Senate, shortly after noon, approved the first major Ukraine aid bill -- one which also includes sanctions against Russia. The House approved a different version, but each would provide $1 billion to Ukraine, and lawmakers are trying to iron out the differences before the end of the day.

The massive troop buildup along the border is reminiscent of Russia's military movements prior to the conflicts in Chechnya and Georgia, one official said.

A Defense official said if Russia were to invade the mainland, Ukraine would attempt to defend itself and this would be "far from a bloodless event as we saw in Crimea." However, Ukraine would be outmatched, this official said.

Senior security officials in Moscow told Putin that Russia faces a growing threat from the U.S. and its allies, Reuters reported. He confirmed that Moscow is taking taking "offensive counterintelligence" measures to blunt Western efforts to weaken Russia in the region, the report said.

"The lawful desire of the people of the peoples do Crimea and eastern Ukrainian regions is causing hysteria in the United States and its allies," Alexander Malevany, the deputy head of the Federal Security Service, reportedly told Interfax.

The latest assessment offers a consensus view of intelligence agencies and the U.S. military. The assessment also takes into account that Putin likely has the desire to create a land bridge into Crimea.

Putin may also believe that if he is to pay a price with the international community in the form of sanctions, he is better off getting everything out of this incursion that he wants, one senior U.S. official told Fox News.

Amid the warnings, the commander of NATO forces in Europe briefed lawmakers Thursday on the threat posed by Russian forces.

Gen. Philip Breedlove, NATO's supreme allied commander for Europe, gave a classified briefing Thursday morning to members of the House Armed Services Committee. He plans to give several briefings, including on the Senate side.

"We're all concerned about what Russia is doing on the border of Ukraine," Breedlove said after the first briefing. "The size of the forces have a message that are not congruous with respecting the borders."

After the briefing, one committee aide said: "Nothing that happened in the briefing calmed the sense of alarm expressed by members yesterday."

Over the weekend, Breedlove raised the possibility that Moscow could move to expand its territory by annexing Transdniestria, a breakaway state whose 1990 claim of independence from the former Soviet republic of Moldova has gone unrecognized by the rest of the world.

Meanwhile on Tuesday, Ukraine's Foreign Minister Andriy Deshchytsia said his country plans to go back to the U.N. Security Council and will take its case against the annexation of Crimea to other international organizations and individual countries.

He said that will include holding an "intensive dialogue" at the Ukraine-NATO ministerial meeting next week.

When asked about the possibility of a formal link between NATO and Ukraine, which Russia strongly opposes, Deshchytsia said "we are talking about all possible options to resolve this conflict."

He said the government's position remains to solve the situation peacefully and diplomatically and NATO membership "at the moment is not on agenda." It would require a decision of parliament for Ukraine to apply for membership in the Western military alliance, he said.

Ukraine needs "good relations ... partnership relations with Russia, and good contact between people of Ukraine and people of Russia," Deshchytsia said.

Fox News' Jonathan Wachtel, Justin Fishel, James Rosen, Chad Pergram, Nick Kalman and The Associated Press contributed to this report.

Click here for more from The Wall Street Journal.