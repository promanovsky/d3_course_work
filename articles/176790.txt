The European Commission has said Ireland's economy will grow by 1.7% this year and by 3% in 2015, and that unemployment will continue to fall.

However, the commission's Spring Economic Forecast warns that a shortage of credit for small and medium-sized businesses and for the construction sector could create bottlenecks in the economy, thereby hindering long-term growth.

According to the forecast, unemployment in Ireland should fall from 13.1% last year to 11.4% this year and 10.2% next year.

At 11.8% in March, Irish unemployment was in line with the eurozone average for the first time since 2008.

However, there are also warnings of risks to Ireland's recovery due to external factors, given the openness of the economy, and the risk of an increase in energy prices due to the crisis in Ukraine.

There are also warnings that Irish SMEs are still hindered by a lack of access to credit.

It said a similar shortage of credit for the construction sector could lead to bottlenecks in the supply of residential and commercial property.

The commission said that Ireland's debt peaked at the end of 2013 at a level of 124% of GDP.

The forecast describes labour market activity as "robust" but cautions that public and private deleveraging - the paying down of debt - continues to weigh upon the recovery.

Despite a worse than expected performance last year (0.3% contraction), this year the Irish economy is showing "steady improvements across all key labour market indicators and a surge in machinery and equipment investment".

The commission said that real GDP growth of 1.7% this year is due to strong growth in the UK and continuing growth in the eurozone.

The forecast, however, cautions that Ireland is subject to potential risks from the global economy.

The commission said: "Although key trading partners are expected to grow, an increasingly uncertain external environment poses downside risks to net exports, given Ireland's openness and dependence on international trade.

"Any possible rise in energy prices related to recent political tensions could also have a particularly high impact on Ireland, which covers around 85% of its energy needs with imports."

The commission also warned of the risks posed by banks and financial institutions still not relaxing lending conditions.

It said: "Impaired access to finance and the effects of legacy debts continue to pose risks for SMEs seeking to replenish capital stocks and seize new business opportunities.

"Impaired credit channels are also a risk to the construction sector, which, if not addressed, may lead to bottlenecks in the supply of new residential and commercial property."

The commission suggested that credit for SMEs and the building sector would remain tight so long as non-performing loans were not dealt with.

"The successful resolution of non-performing loans is a precondition for the restoration of credit channels and for sustaining the economic recovery beyond the short term," it concluded.

Minister for Finance Michael Noonan welcomed the forecast this afternoon.

He said it was in line with the Government's expectations of an average of 3% growth through to 2020, which would fulfil the medium-term economic programme.