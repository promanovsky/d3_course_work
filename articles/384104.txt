So far, various leaks about the Samsung Galaxy F aka Galaxy S5 Prime have suggested the existence of the device. Now, the latest rumours out of Korea suggest that the Galaxy F will debut on August 13 as the Galaxy Alpha.

Expected to be the premium variant of the Samsung Galaxy S5 (Pictures | Review), the rumoured Galaxy F or Galaxy S5 Prime is said to be a metal body smartphone to counter the iPhone 6 and LG G3, according to a report by a Korean website ETNews citing industry sources. The website didn't share more details about the device's specifications, but did mention an August 13 launch date and the Galaxy Alpha name.

As learnt from some of the previous reports, Samsung might offer a slightly smaller full-HD display on the Galaxy Alpha (aka Galaxy S5 Prime) than the Galaxy S5 flagship, or a similar-sized QHD display to counter LG G3. As far back as last year, reports suggested that Samsung had started mass producing a 5.25-inch Quad-HD (1440x2560 pixels) AMOLED display. Other rumoured specifications of the anticipated Galaxy S5 Prime smartphone include a Qualcomm Snapdragon 805 processor, and 3GB of RAM.

(Also see: Samsung Galaxy S5 Prime 'SM-G906L' Listed on Bluetooth SIG Database)



The rumoured Galaxy F (aka Galaxy S5 Prime, Galaxy Alpha) smartphone has also been showcased in an alleged press render image by serial tipster @evleaks. As seen in the alleged render, the Galaxy Alpha aka Galaxy F smartphone appears to sport a similar design to the Galaxy S5, though it seems to have an aluminium chassis with a brushed metal finish instead of the chrome plastic body with the perforated finish seen on the flagship. The rear seems to house a heart rate sensor alongside the camera.

(Also see: Samsung Galaxy F aka Galaxy S5 Prime Allegedly Spotted in Images)



Last month, another alleged image of the rumoured Samsung Galaxy Alpha showcased LG G3-like thin side bezels, as well as slightly slimmer top and bottom bezels as compared with the Galaxy S5.