Doesn't look like those mediation sessions worked: Apple and Samsung are back in court today as the iPhone maker accuses Samsung of copying some of its software and wants $40 per device sold as restitution (that's around £25 or AU$45).

If the courts find in Apple's favour, the ruling could cost Samsung around $2 billion (about £1.2bn / AU$2.2bn), but Samsung hopes to bring Google into the fray to detail how Samsung came up with the elements of its Android overlay software of its own accord.

If Apple gets its way, we could see Samsung Galaxy device prices hiked up to cover the shortfall.

Systematic

Just under two years ago, courts ruled that Samsung had infringed on some of Apple's iPhone and iPad related patents, ordering the Korean company to cough up $900 million in damages (which Samsung is still contesting). Earlier this year, the court continued to find in Apple's favour.

This time around, Apple says Samsung has "systematically" copied its devices, alleging that more recent Galaxy ranges infringe on five of its patents.

These include slide to unlock and the concept of tapping a phone number or address that appears in web search results to call that number or open the address in a mapping app.

Not to be outdone, Samsung says Apple is using two of its wireless tech developments that speed up data exchange on iPhones and iPads.

Today sees the two companies begin the jury selection process - which proved problematic in the last round of court battles because so many people in the San Joes area have ties to one or other of the companies. Maybe they should call in some rural jurors.