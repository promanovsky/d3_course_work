From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Millions of people around the world immediately go to the Web for information after feeling a mysterious ache, pain, rash, or bump. This often results in either a panic attack or a false sense of calm. Doctors have warned against this practice since the days of Netscape, and now a new report puts some science behind their fears.

Researchers at Campbell University in North Carolina compared Wikipedia entries on 10 of the costliest health problems with peer-reviewed medical research on the same illnesses. Those illnesses included heart disease, lung cancer, depression, and hypertension, among others.

The researchers found that nine out of the 10 Wikipedia entries studied contained inaccurate and sometimes dangerously misleading information. “Wikipedia articles … contain many errors when checked against standard peer-reviewed sources,” the report states. “Caution should be used when using Wikipedia to answer questions regarding patient care.”

At Wikipedia anybody can contribute to entries on health problems — no medical training (or even common sense) is required.

“While Wikipedia is a convenient tool for conducting research, from a public health standpoint patients should not use it as a primary resource because those articles do not go through the same peer-review process as medical journals,” said the report’s lead author, Dr. Robert Hasty in a statement.

And there’s a lot of health information on Wikipedia. The site contains more than 31 million entries, and at least 20,000 of them are health-related, the report says.

The study findings were published in this month’s Journal of the American Osteopathic Association. You can see the full text of the study here.

Via: Daily Mail