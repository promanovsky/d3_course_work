Apple plans to buy Beats Electronics, which makes headphones and has an up-and-coming streaming music service. (Reuters)

Apple Inc's plan to pay $3.2 billion for Beats Electronics, which makes headphones and has an up-and-coming streaming music service, may herald a bolder use of its swelling cash pile and galvanize the iPhone maker's move into wearables.

Investors are still struggling to make sense of why CEO Tim Cook would break Apple's decades-long tradition of relatively minor acquisitions of about $200 million to $400 million. Most observers say it's going after a mix of technology and music industry talent in an effort to revive digital music sales.

Shares in the iPhone maker, which have been stuck mostly at or below $600 for over a year, ended 0.4 percent lower at $585.50 in the first day of trading after the news broke. Apple declined to comment on the deal, which sources have said may still fall through.

Some analysts said the planned acquisition would be in line with Apple's other classic efforts to give consumers what they never knew they wanted -- a key tenet of the late Steve Jobs. While not a fan of streaming music, he believed music was integral to the mobile experience.

But Apple's year-old stab at music streaming, iTunes Radio, has failed to catch on broadly.

Streaming music services generally rely on software algorithms to put together lists of tunes. Beats claims a customised system that uses software relies and "handpicked" playlists.

Analysts point out the potential for Apple to tailor recommendations to a listener's mood or activity, as future wearables measure such things as heart rate, sunlight and noise.

A smartwatch-based streaming music service could deliver the right tunes at the right time, the same way smartphones are evolving toward "predictive" or contextual actions.

"This is a critical time. They need to get the iPhone 6 right, and wearables right," Cowen & Co analyst Tim Arcuri said. "This technology helps to create a content delivery mechanism that's unlike what anybody else has."

Apple could also benefit from Beats' reputation for cool products, attributed to music producer Jimmy Iovine and co-founder Dr Dre, who on Friday proclaimed himself hip-hop's first billionaire.

"Its branding would make the wearable device, whether attached at the hip or wrapped around the wrist, conspicuously cool," Forrester analyst James McQuivey said. "Who would have thought that wearing big honkin' headphones would be cool today"

OPENING THE FLOODGATES

To be sure, Beats fledgling music service and its highly touted curation system is but several months old.

Apple may be signalling a newfound willingness to dip into its cash hoard, beyond just doling out increasing dividends and buybacks, which will go down well with investors who have faulted the company for excessive caution.

Wall Street has been somewhat forgiving of outsized purchases by Apple's Silicon Valley's brethren. Google's $3.2 billion purchase of Nest, and even Facebook's acquisition of red-hot startups WhatsApp and Oculus VR, have won plaudits as aggressive bets on the future of connected devices.

But the looming Apple-Beats deal hasn't earned quite the same kind of indulgence.

"Apple should look at the Nest deal that Google did and say 'we should have done that'," said one Silicon Valley banker. "It makes me wonder, is this the best use of Apples cash Are they so concerned about the rest of their business that this is the best idea that they can come up with "

Cook is under pressure to deliver on his promise of new product categories this year and reclaim the lead in tech industry innovation.

While still a force to be reckoned with, it remains an also-ran in hot tech sectors like social media and cloud computing. And with Google's Android now installed on more than 80 percent of new phones sold, its iOS platform is under attack.

And in past years, Apple has made several forays into Internet services such as social media and mobile advertising, with limited to mixed success.

Some think Apple may now use its cash to make a move.

"The fact that the company is shopping encourages some of us to anticipate what it could spend its hard-earned money on next," McQuivey wrote in a blogpost.