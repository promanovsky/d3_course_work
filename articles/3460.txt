SXSW: Jon Favreau Says 'Chef' Marks Return to 'Swingers'-Style Filmmaking (Q&A)

"To speak to an audience of your peers is a real luxury right now," the filmmaker explains ahead of his South by Southwest world premiere.

The 2014 South by Southwest Film Conference and Festival kicks off on Friday with the world premiere of Jon Favreau's Chef -- almost certainly the first road-trip movie set in a food truck. Written, directed by, starring and co-produced by Favreau, the movie follows a divorced Los Angeles chef who quits his job in order to explore America in search of his culinary mojo.

Featuring an A-list cast that includes Scarlett Johansson, Robert Downey Jr., Sofia Vergara and Dustin Hoffman, Chef in many ways marks the 47-year-old Iron Man helmer's homecoming to the indie film world that put him on the map with 1996's Swingers, which he wrote and starred in.

The Hollywood Reporter spoke to Favreau ahead of his film's Austin debut.

What drew you into the world of food trucks?

I've always been fascinated by chefs and the worlds of chefs -- what they do is incredibly cinematic. It's hard to show the creative process onscreen in a way that captures it [in a way] that's visual. You show people playing poker or hacking into a computer; it feels so significant in the script and then when you see it on the screen, it loses something. But there's something about cooking -- food being prepared is incredibly captivating. It became just a fun box of tools to use as a director.

The image of the chef has certainly evolved from "guy in a tall puffy hat" to "down 'n' dirty rock star."

The world of chefs is fun because these are characters who are right now being focused on by the public. They never signed on to be a frontman. Some are more elegant about it than others, but they always seem to be coming from a very sincere place. [The movie] captures the conundrum of their creative process: people trying to find their voice, a way to fit within the system and yet express their individuality.

You appeared on Top Chef New Orleans this season with bad-boy L.A. restaurateur Roy Choi. Are you a Top Chef fan?

I've seen every one of them. I relate. It's different, I'm in the film business, but you're still dealing with a lot of the same things that they are. And your heart goes out to them, trying to do something unique and being very sure of themselves but by the same token completely hostage to the opinions of their customers.

The contestants are so hard on themselves.

Yes. I find it really endearing. And it was a great character to play. At this point in my life, there aren't many things I can play. I'm not going to be a football player anymore. But a chef I'm believable as, and I trained very hard and got to know Roy Choi. He started the whole food truck trend in Los Angeles.

He was pretty tough on the Top Chef contestants.

That's who was teaching me! That's my sensei. They're all tough. They have different bedside manners, but they all hold their craft to a very high standard and are hardest on themselves.

You pop up in so many movies, but this is your first starring role in a while. Why now?

It's been a long while. I didn't set out to do it for that reason. A script hit me, and I wrote it in like two weeks. That was the first time that had happened to me since Swingers. And I've been around long enough to know that if you're lucky enough for the inspiration to hit you, you've got to get out of the way and welcome it. It's a gift. I'm not the type that can just write something from nothing once a year. It has to land on me fully formed. It all clicked.

How autobiographical is your character?

Being a dad and also being somebody with a career, it was fun to make a story about somebody in this stage of life. With most movies you get to make, especially the big ones, you're dealing with a much younger audience and usually escapism. I wanted to make something about the lessons of life and about reality, not something that takes you out of it. Also it's about a broken home -- [my character] is divorced, and my parents were divorced. I think I was tapping into some of that, too.

Is Chef then in the same realm as Swingers, tonally?

It had the same challenges that Swingers had. It will be an R; we say "f--k" more than once -- but it's something that I'm comfortable with my kids seeing. Not every beat is being sold for laughs. The beats are about character and emotion -- I never have to push anything further than I want to. When you're going for a big studio comedy, the joke tally better be pretty high and you better have some big comedy set pieces. That was one of the issues when I was trying to get Swingers made for the first time, which is that there weren't any broad comedy set pieces. It's more comedy that comes out of being emotionally attached to the characters.

Is it a relief to not have a superhero movie riding on your shoulders?

I was really excited to do something that was not part of a big committee and a big collaboration. It's about a character in transition in life, a person trying to find his voice and reconnect with his passions. To me that's the story of many people that I know that are my age. To speak to an audience of your peers is a real luxury right now. Right now movies are very expensive, and you have to appeal to men and women, young and old, the domestic audience and overseas. That's the name of the game. Otherwise you're doing a movie that's very small or doing a TV show. So this was me seizing an opportunity to do something that was completely satisfying for me artistically, and I feel very lucky to have done it. It was really exciting to go back to my roots in independent film and face the challenges that I used to face with time and money -- but not having to face the challenges of making a case for my creative vision in a roomful of people who all might have different opinions.

It seems a great fit, but in your own words, why South by Southwest?

I thought it was a great place to premiere because it's sort of outside the system. I don't have memories attached other than just happy memories from traveling through Austin. But I think the personality of the festival matches the personality of the film.