We all thought he would take it to his grave, but "Sopranos" creator David Chase has finally answered the question that’s been on every “Sopranos” viewer’s minds since the notorious cut to black in 2007. Is Tony Soprano dead?

[SPOILER AHEAD]

“No,” Chase reportedly told film critic Martha P. Nochimson, when she dared to ask the question he likely dreads. “No he isn’t.” And that’s all he said.

It’s unclear exactly when the conversation took place. In Nochimson’s account, published in Vox, she says that she and Chase have stayed in touch since her first interviews with him for a book about gangster films. They "have continued to exchange ideas through e-mails and discussions at Upper East Side coffee bars and restaurants." And on one occasion, Nochimson unceremoniously asked the question that comes to many minds when they think of “The Sopranos.”

I had been talking with Chase for a few years when I finally asked him whether Tony was dead. We were in a tiny coffee shop, when, in the middle of a low-key chat about a writing problem I was having, I popped the question. Chase startled me by turning toward me and saying with sudden, explosive anger, "Why are we talking about this?" I answered, "I'm just curious." And then, for whatever reason, he told me.

But not everyone is buying Nochimson’s story. The author of a widely circulated, extraordinarily detailed blog post arguing – quite convincingly -- that Tony Soprano did in fact die in the finale of the acclaimed HBO series, thinks Chase may not have been entirely sincere.

“I'm not saying that Chase didn't say it … but it's clear that he was annoyed and angry about having to deal with the question and may have just said it to appease her,” said the author, who did not provide his name. He suggested that Nochimson might have a bias, based on an essay that she published in the 2011 book “Sopranos Essential Reader,” which he said kept open the possibility that Tony dodged the bullet. “She also sent a message to my site years ago claiming, incredibly, that Chase told her personally that Tony is still alive," he said.

IBTimes has not yet been able to verify all of the author’s claims, and there is a case to be made that he, too, has a bias. Further, since Nochimson does not specify when this conversation took place, it’s conceivable that it’s the same conversation she was referring to in her message to him.

But his argument that the reveal “goes against SO MUCH of what Chase has said” is easy to support. Chase has been notoriously resistant to discussing the finale. As a May 2014 Forbes article pointed out, Chase left the country when the finale aired and prohibited his “Sopranos” crew from discussing it. Also in May, Chase again refused to answer the question that has been posed to him countless times.

“Did Tony get killed in the diner or did he not get killed? I’m not trying to be coy about it. It’s not the point if he’s alive or dead,” Chase reportedly said. “The whole show is about death. All I’m able to say is gobbledygook.”

Alan Sepinwall, author of “The Revolution Was Televised,” has interviewed Chase numerous times, and was never able to convince him to explain the final scene. In a 2012 Slate post, Sepinwall writes that Chase “can’t imagine a circumstance under which he might change his mind.”

Complex Magazine pointed out that James Gandolfini’s death in 2013 might have made it easier for Chase to loosen his grip on the secret of Tony’s fate:

As morbid as it is to say, the tragic death of James Gandolfini may have made it easier for Chase to spill this closely-guarded secret. The majority of fans, it seems, wanted Tony to be alive; this explanation satisfies that crowd, but with no Gandolfini, any further episodes or a movie would simply be out of the question. For Chase, it is a really, really twisted win-win.

After the Gandolfini tragedy, I wrote a story arguing, similarly, that the actor’s death might reframe our perceptions of the finale – since Tony Soprano and James Gandolfini were one and the same in so many minds – but in my view, Gandolfini’s death punctuated a belief that Tony died in that diner.

Sepinwall also questioned Nochimson’s conclusion. Writing for HitFix, he said “I've long been in the 'Tony lives' camp, feeling that Chase had no interest to a definitive conclusion to Tony's story,” but suggested that Chase might have answered the question to avoid being asked it interminably:

As his initial anger with Nochimson's question suggests, I think he was just tired of this being the first, last and only thing people ever wanted to talk about with this great show that he put so much of himself into. Chase is not an optimist by nature, but maybe he's hoping that by giving a (mostly) definitive answer to that question, people can finally get back to discussing all the other great and fascinating parts of "The Sopranos" as a whole.



It’s not at all surprising that some fans and critics are resistant to closing the book on the question of Tony Soprano’s fate. Maybe David Chase knows his audience better than we gave him credit for. Because despite the collective fury at the most maddening cliffhanger in television history, the mystery keeps the show alive for us. But it seems that some of us might secretly want Tony dead.