Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

If you thought Lady Gaga couldn't get any more bizarre - think again!

Her latest music video lasts (!) a staggering 12 MINUTES and sees the star almost naked as a fallen angel, buried in a pool, performing with the cast of the Real Housewives of Beverly Hills, and feeling her crotch.

It has already been watched by 2 million viewers across the world and here we have it in full for you to, er, enjoy.

And with all its madness, the entire thing took six days to make and was filmed at the Hearst Castle in San Simeon, California.

There's also a cameo of Andy Cohen, who presents the season finales of the series, and Michael Jackson, Ghandi, John Lennon and Jesus are brought back to life for a starring role (we said it was weird!)

Speaking on the Today show, Lady Gaga tried o explain the concept behind the video, saying: "I live between two things - I live between Art and Pop – all the time.

"You have to [crack the whip] to keep the pace going.

"We had our work cut out for us with this video. It was a very big production.

'We shot for six days and it was just such a pleasure working with the whole team. We had a great time.'

Gaga was so thrilled with the team behind the new film that she dedicates an entire four minutes of the vid to credits.