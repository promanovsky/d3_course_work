Doctors at the University of Michigan's CS Mott Children's Hospital have successfully used their 3D-printed bioresorbable devices to save a second baby's life by opening his airways to let him breathe properly.

Eighteen-month-old Garrett Peterson was saved by pioneering doctors Glenn Green and Scott Hollister who had developed a 3D printed device first used successfully in 2013 on Kaiba Gionfriddo, a baby who suffered from a serious form of tracheobronchomalacia.

Green and Hollister created the device by creating a 3D model of Kaiba's airways and then custom-made a tracheal splint using a biopolymer called polycaprolactone, which had to be sewn around the baby's airway to allow expansion. The splint is eventually reabsorbed by the body.

Garrett, who suffers from the same rare condition, was so fragile that he had never been taken home by his parents or even held in their arms.

Despite being put on a ventilator at the highest settings, it was not uncommon for Garrett to stop breathing and turn blue up to five times a day. Medical staff had to resuscitate him using heavy medication.

He was put into a medically induced coma as when he was awake he would try to fight against the ventilator.

"It's really hard to watch your child suffocate and pass out before you could revive him and bring him back, over and over," said Jake Peterson, Garrett's father.

"Nothing would stop him from turning blue. Just lifting his legs for diaper change would collapse his airways and that was it. There was nothing we could do to help him," said his mother Natalie.

The Petersons read about the 3D printed device but were unsure if it were the best option.

However, when Garrett's gut began to shut down from the high pressure of the ventilator and he was in critical condition, they decided to try the device.

"It was highly questionable whether or not he would survive," Green said.

"Severe tracheobronchomalacia has been a condition that has frustrated me for years. I've seen children die from it.

"To see this device work, for a second time, it's a major accomplishment and offers hope for these children."