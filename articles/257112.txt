Google Inc (NASDAQ:GOOGL) (NASDAQ:GOOG) is considering purchasing camera start-up Dropcam according to a report today in The Information. The report did not provide details or the price Google would pay for the acquisition.

Google moving into home appliances



Dropcam revealed plans a few weeks ago to design people detection software available to Dropcam’s cloud video recording subscribers by the start of August. Dropcam Tabs are another device offered by the firm, which have an optional motion sensor accessory to track movement and motion with the help of three-axis accelerometer and a three axis magnetometer.

In February, Google Inc (NASDAQ:GOOGL) (NASDAQ:GOOG) acquired Nest Labs, the manufacturer of the Learning Thermostat and the Protect smoke and carbon monoxide detector for $3.2 billion. According to a report in the Financial Times, Apple is also looking to launch a new smart home platform next month, where iPhone and iPad users will be able to control a home’s lights, security system and other connected appliances.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Last year, Nest CEO Tony Fadell told The Verge that development in the home security segment will be aligned with the Nest’s mission to enhance the technology for the 21st century. Fadell, while releasing the Protect smoke detector, said that safety should not be annoying and that the company is working on reinventing the unloved category.

Privacy issues must be considered

Dropcam was established in 2009 with funding from investor Mitch Kapor. The start-up offers a camera for $150 that streams footage to phone and computers. Last July, the San Francisco-based start-up closed a $30 million Series C funding round, bringing the total amount raised to $47.8 million.

Google Inc (NASDAQ:GOOGL) (NASDAQ:GOOG)’s policy of collecting user information is seen as a bit too much by the critics and tracking via live video feeds is also likely to be criticized. However, Google will most probably not take data from Dropcam’s cameras if it acquires the start-up. However, the internet giant could bypass the privacy concerns by finding ways to collect data on other metrics like device deployment trends.

Last week, Google Inc (NASDAQ:GOOGL) (NASDAQ:GOOG) filed documents with the U.S. Securities and Exchange Commission detailing that one day it will offer ads car dashboards, thermostats, glasses and watches. However, Google said later that the document was not worded right, and conveyed to the media that it does not intend to follow through on the plans in the document in the near future.