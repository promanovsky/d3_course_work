When Google purchases Motorola Mobility, it acquired a number of patents, Google then decided to sue Apple over these patents, and then Apple counter sued Google, and now Apple and Google have ended their lawsuit and come to an agreement.

Apple and Google have been fighting it our over various patents for some time, and Apple has been suing various handset makers like Samsung relating to Google’s Android OS, the new agreement does not affect Apple’s lawsuits against Samsung.

Until the new agreement was reached, Apple and Google have been engaged in around 20 lawsuits against each other in Germany and the US.

The two company’s have now agreed to settle all these lawsuits and have also agreed to work together in the future on ‘some areas of patent reform’.

According to the WSJ, Google may have been encouraged by the US regulators to settle its patent disputes with Apple relating to the Motorola patents, and Apple may have decided to drop their lawsuits against Google’s Motorola as it is now owned by Lenovo, although Google still owns the patents it acquired when it purchased Motorola.

It will be interesting to see if the two companies can work together in the future, to come to agreements on patents that will avoid all of the various court cases.

Source WSJ

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more