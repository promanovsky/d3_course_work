Consumption of liquor in India has increased during the past decade as 32 per cent men and 10.6 per cent females above 15 years of age were found to be drinking alcohol.

The revelations were made in an World Health Organization (WHO) report which cited the data of 2010.

Drinking habit in populous China and India is rising fast as people are earning more money, the report said.

According to the experts, alcohol abuse is emerging as a major public-health problem in the country.

Shekhar Saxena, who heads the WHO's Mental Health and Substance Abuse department said today that alcohol kills 3.3 million people worldwide each year which actually translates into one death every 10 seconds.

Alcohol caused some 3.3 million deaths in 2012, whereas HIV/AIDS led to 2.8 percent, tuberculosis--1.7 percent of deaths and violence is responsible for just 0.9 percent, WHO study said.