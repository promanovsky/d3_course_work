NEW YORK — In another promising development in AIDS prevention, scientists have shown that monkeys can be protected against infection with a vaginal gel even when it is used as long as three hours after sex.

If it works in humans, such a gel would be useful in countries where women have little protection against domestic violence or rape, because they could apply it after a partner fell asleep, or a clinic could administer it after a rape.

But if the technique does move into human trials, scientists said, it is more likely that women would be asked to try to use it both before and after sex.