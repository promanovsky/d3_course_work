The negative impact of being bullied in childhood remains persistent and pervasive, and lasts nearly 40 years later as well, it has been revealed.

According to the study by King's College London, the findings come from the British National Child Development Study which includes data on all 7,771 children born in England, Scotland and Wales during one week in 1958.

The parents of the children provided information on their child's exposure to bullying when they were aged 7 and 11. The children were then followed up until the age of 50.

Dr Ryu Takizawa, lead author of the paper from the Institute of Psychiatry at King's College London, said,"Our study shows that the effects of bullying are still visible nearly four decades later. The impact of bullying is persistent and pervasive, with health, social and economic consequences lasting well into adulthood."

Individuals who were bullied in childhood were more likely to have poorer physical and psychological health and cognitive functioning at age 50, and those who were frequently bullied in childhood were at an increased risk of depression, anxiety disorders, and suicidal thoughts.

The harmful effect of bullying remained even when other factors including childhood IQ, emotional and behavioural problems, parents' socioeconomic status and low parental involvement, were taken into account.

The study is the first to look at the effects of bullying beyond early adulthood, and is published in the American Journal of Psychiatry.