Accra: The United Nations health agency said it expected the worst Ebola outbreak in history to continue its deadly rampage through west Africa for at least "several months".

The highly-contagious tropical bug has infected hundreds of people in Guinea, Liberia and Sierra Leone, with the latest World Health Organisation (WHO) figures showing that confirmed or suspected cases had left 467 people dead and experts fearing it could spread throughout the region.

Keiji Fukuda, the UN agency`s assistant director-general of health security, said at the close of a regional summit of health ministers on the crisis it was "impossible to give a clear answer" on how far the epidemic could spread or when it might begin to retreat.

"I certainly expect that we are going to be dealing with this outbreak minimum for a few months to several months," he told AFP.

"I really hope for us to see a turnaround where we begin to see a decrease in cases in the next several weeks."

Marie-Christine Ferir, of medical aid agency Doctors Without Borders (MSF), echoed the assessment, saying the outbreak could "continue for about a few weeks, or perhaps months in certain parts".

The warning came as health ministers from 12 nations wrapped up two days of talks in Accra with global experts in communicable diseases, with debate raging over the measures required to stop Ebola in its tracks.

They were expected to make a raft of recommendations to regional governments and to WHO on containing the disease, including the launch of a USD 10 million war chest to boost medical aid in the worst hit regions.

There are five species of Ebola, three of which -- Zaire, Sudan and Bundibugyo -- can kill humans.

Zaire Ebola, the deadliest and the species behind the current outbreak, can fell its victims within days, causing severe fever and muscle pain, weakness, vomiting and diarrhoea -- in some cases shutting down organs and causing unstoppable bleeding.

There have been 21 Ebola outbreaks -- not including isolated cases involving only one patient -- since the virus first spread to humans in the Democratic Republic of Congo, then known as Zaire, in 1976.

Before the current crisis, Ebola had killed 1,587 people, two-thirds of those infected, according to an AFP tally based on WHO data.