The Solar Electric Power Association (SEPA) announced the 2013 winners for its "Top 10 Utility Solar Rankings" today, and the winners span from Hawaii to Georgia.

The list ranks utilities on their 2013 solar installations alone, providing investors with a unique perspective on which companies are loading up on solar assets. California-based PG&E Corporation (NYSE:PCG) topped the list, with 1,471 MW installed.

Sempra Energy's (NYSE:SRE) San Diego Gas & Electric Company snagged silver, but its 643 MW paled in comparison to PG&E. Arizona Public Service's 417 MW secured third place, breaking the California hold on top spots.

Overall, California contains a whopping 5,537 MW of solar power, with Arizona following behind with 1,026 MW.

According to the report, 2013 was a landmark year for solar. The renewable energy surpassed 4,000 MW in 2013 alone, putting our nation's total capacity over 10,500 MW. The recent surge is well supported and diversified as well. Around one-third of new solar came from distributed power, and 70% of utilities responding to SEPA's survey noted that they currently offer customers solar incentives.