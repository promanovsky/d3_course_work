"Lindsay" -- which yours truly called "utterly absorbing," an opinion clearly in the minority and possibly an overstatement when viewed in the cold light of dawn -- was seen by only 693,000 viewers. That seems like a small number in TV terms because it is.

OWN did see the sliver lining, of course: "This marks OWN’s highest rating in the Sunday 10 p.m. hour in 27 weeks among women 25-54."

And in fairness to, ummm, myself, I did indeed think "Lindsay" was absorbing if only because it tried to humanize her, which doesn't exactly happen every time you see her on screen. This coulda been a disaster. It was not. Have at me if you disagree... Comment below!