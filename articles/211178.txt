Welcome back to Dancing with the Stars. The finals are fast approaching, which means that the ersatz stars have been dividing their time among sweating, swearing and finally figuring out what “flicks and kicks” are, and, of course, applying extra Vaseline to every body part they can reach for that extra shine.

To help determine who would head to the final round of competition, the powers that be invited actual expert Kenny Ortega to join the judges’ panel. Kenny makes up for the fact that he is not Redfoo by being a real choreographer with an actual dance résumé (who also directed the High School Musical trilogy and is thus owned in part by Disney).

In order to fill two hours of prime-time television, the stars and their pro partners were required to do two dances and publicly declare their personal icons. Surprisingly, no one chose Redfoo.

Here’s what happened on Dancing with the Stars:

Candace Cameron Bure and Mark Ballas: While Candace’s actual hero is Jesus, he is not currently available for television interviews. So she chose Angela Thomas, a Bible teacher and author, who loves the word of God almost as much as Candace does. Angela offered some advice on joy, which Candace incorporated into her Viennese Waltz. The judges were moderately impressed with the routine, doling out 34/40.

Safety first: Candace and Mark are safe and going into the finals as are James Maslow and Peta Murgatroyd. Amy Purdy and Derek Hough and perennial front-runners Meryl Davis and Maksim Chmerkovskiy are in jeopardy, as are Charlie White and Sharna Burgess, who got a perfect score last week. Just go have a seat and think about what you’ve done, America.

Charlie White and Sharna Burgess: Charlie’s personal icon is Olympic figure skater Scott Hamilton, who is unfortunately contractually bound to NBC and can’t make an appearance. (Just guessing!) They deliver a smooth fox-trot set to “New York, New York” that Kenny called “indescribably delightful.” 40/40

NB: Charlie and Sharna eared a perfect score this week and a perfect score last week, so it is going to be super embarrassing when they get cut tonight.

Amy Purdy and Derek Hough: For the quickstep, Amy strapped on some running prosthetics that look less like feet and more like spatulas. They add a spring to her step and act as a crazy accessory to boot. Dancing a bouncy and light routine to “You Can’t Hurry Love” the duo was truly dynamic. The judges loved the routine, though Len Goodman wanted a little more body contact, because he’s into that sort of thing. 39/40

Even better than a mirror-ball trophy: Oprah called to wish Amy good luck and to promise that if they win, she will take them out to dinner. That’s right — dinner, with Oprah.

James Maslow and Peta Murgatroyd: Record producer L.A. Reid stopped by James’ rehearsal room to give him some insight into Michael Jackson and, naturally, cross-promote the new Michael Jackson album he put together. Their cha-cha to MJ was bold because it incorporated a moonwalk into a cha-cha. Bruno Tonioli dubbed it “effortlessly cool.” 40/40

Meryl Davis and Maksim Chmerkovskiy: Kristi Yamaguchi isn’t just an Olympic ice skater, she’s also a DWTS champion who is required by law to appear on the show as often as possible, so naturally she is Meryl’s icon. Their jive to Elvis Presley’s “Hound Dog” earned a standing ovation from the judges, with Len blurting from the bench, “That was fantastic!” Maks gave all the judges kisses, and it seemed like the judges kind of regretted that they a) gave out so many 10s already, or b) that they can’t give Meryl and Maks the numerical equivalent of A+++++. 40/40

Macy’s Stars of Dance: Derek showed off his impressive choreography skills once again, staging a routine that moved through the DWTS set with grace and ease. While Derek’s pomade addiction and wispy flesh-colored mustache make him generally suspect (and sticky probably), there’s no denying his incredible talent for choreography.

Candace and Mark, Part 2: For the semifinals, Candace asked “WWJD?” and the answer was apparently to wear what was undoubtedly her most risqué outfit of the season and dance a jazz routine to Janet Jackson’s “Nasty.” Bruno suggested that she “explore her dark side more,” because she mixed Fosse and Jackson to great effect. Carrie Ann Inaba wanted more attitude, which probably means she’s not getting a perfect score. 38/40, which are Candace’s first 10s of the season.

Charlie and Sharna, Part 2: For their second dance, Charlie and Sharna opted to dance a samba to Notorious B.I.G.’s “Mo Money, Mo Problems,” because that is the world we live in now. The judges all thought it was a fine routine (apparently they aren’t judging on sacrilege) and mentioned several times that Charlie deserved to be in the finals. 36/40

Amy and Derek, Part 2: Amy returned to her regular legs for her jazz routine. Derek’s avant-garde choreography meant that Amy got to spend a good portion of the routine rhythmically sitting on a desk in lingerie — and the judges loved it. Kenny told Derek, “You redefine choreography for this generation,” and Derek looked like he might die right then and there. 39/40

James and Peta, Part 2: For their final routine, James and Peta delivered a sultry rumba set to “Islands in the Stream,” which is the most sultry duet ever performed by Dolly Parton and Kenny Rogers. “You did not disappoint,” said Carrie Ann after the routine, although Kenny noted, “You must extend that brilliance into your hands,” which you need to be wearing Spandex and sparkles to understand. 36/40

Meryl and Maks, Part 2: 40/40. Do you need to know more than that? It was a perfectly executed Viennese Waltz that deserved every point it earned. Go YouTube that number.

Who goes home: Since the worst dancer is already safe, when it comes to the finale, America (oh, who are we kidding: America’s nanas who had their grandson configure their AOL dial-up and a handful of Big Time Rush fans) will just have to live with themselves and the knowledge that they let some excellent dancers go home.

No. 1 sign it’s not a talent show: Amy and Derek were pronounced safe, which meant one of the ice-dancing Olympians is going home. Yes, the two dancers who have consistently been at the top of the leaderboard all season are in jeopardy.

So long, farewell, auf Wiedersehen, goodbye: Charlie and Sharna, who had perfect scores for the past two weeks, headed off the ballroom floor and into the audience to cheer on Meryl in the finals.

MORE: RECAP: Game of Thrones Watch — Family Law

MORE: RECAP: Mad Men Watch — ‘The Runaways’

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.