NYSE EuroNext flag hangs outside the NYSE.

Stocks making moves before the bell Friday:

Express—The retailer rose in pre-market trading after private-equity firm Sycamore Partners announced a 9.9 percent stake in the company and said it might be interested in acquiring it.

General Motors—The battered auto maker recalled another set of cars, this time its Chevrolet Camaros.

Finisar—The provider of telecommunications equipment fell sharply in pre-market trading a day after reporting fourth-quarter profit beneath expectations.