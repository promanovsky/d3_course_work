A new study has revealed that middle-aged adults with a history of problem drinking are more than twice as likely to suffer from severe memory impairment in later life.

The study by researchers from the University of Exeter Medical School highlights the hitherto largely unknown link between harmful patterns of alcohol consumption and problems with memory later in life - problems which may place people at a high risk of developing dementia.

Iain Lang said that they already know there is an association between dementia risk and levels of current alcohol consumption - that understanding is based on asking older people how much they drink and then observing whether they develop problems. But this is only one part of the puzzle and we know little about the consequences of alcohol consumption earlier in life. What they did here is investigate the relatively unknown association between having a drinking problem at any point in life and experiencing problems with memory later in life.

The study was published in the American Journal of Geriatric Psychiatry.