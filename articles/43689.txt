Singer Taylor Swift spent part of her weekend (22-23Mar14) helping to lift sick children's spirits by visiting a cancer unit at a New York hospital.

The We Are Never Ever Getting Back Together hitmaker stopped by the Memorial Sloan-Kettering Cancer Center on Saturday (22Mar14) to take pictures with the ailing kids and video-chat with their friends and families.

After the visit, a fan reached out to Swift via Twitter.com, writing, "Thank-you @taylorswift13 4 visiting Shelby (and others) @ hospital and staying 4+ hours when u were only sched. (scheduled) for one. thanks (sic)."

This isn't the first time Swift has made time for her sick fans - last year (13), she took time out of her busy schedule to visit a hospital in Pennsylvania in between her tour gigs.