An online dating site is urging its users to boycott the Firefox web browser because the boss of the company behind it opposes gay marriage.

OkCupid said those who sought to “deny love” and “enforce misery, shame and frustration are our enemies”, asking people to switch to a different browser in a message that popped up automatically for users of the browser made by Mozilla.

Brendan Eich, Mozilla’s chief executive, gave $1,000 (approximately £600) in support of California’s Proposition 8 campaign in 2008, an initiative which opposed same-sex marriage. According to Mozilla, Mr Eich invented JavaScript, the Internet’s most widely used programming language.

In its message, OkCupid apologises for interrupting its users’ search for love, then proceeds to explain why they should switch to another browser.

“Mozilla’s new CEO, Brendan Eich, is an opponent of equal rights for gay couples. We would therefore prefer that our users not use Mozilla software to access OkCupid,” it says.

“Equality for gay relationships is personally important to many of us here at OkCupid. But it’s professionally important to the entire company. OkCupid is for creating love.

“Those who seek to deny love and instead enforce misery, shame, and frustration are our enemies, and we wish them nothing but failure.”

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The message, reported by the TechCrunch website, admits that “politics is normally not the business of a website”.

But it adds: “We’ve devoted the last ten years to bringing people — all people — together.

“If individuals like Mr Eich had their way, then roughly 8 per cent of the relationships we’ve worked so hard to bring about would be illegal.”

OkCupid provides a link at the bottom of the page to allow Firefox users to continue onto the dating part of the site.