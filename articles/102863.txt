The National Security Agency has flatly denied a Bloomberg report published early Friday that claims the NSA knew about the Heartbleed bug for “at least two years” but exploited it to gather intelligence rather than report it to OpenSSL.

NSA spokeswoman Vanee Vines told the Washington Post that the “NSA was not aware of the recently identified vulnerability in OpenSSL, the so-called Heartbleed vulnerability, until it was made public in a private-sector cybersecurity report," and that "reports that say otherwise are wrong.”

Vines declined to comment directly to Bloomberg earlier on Friday for their story. The White House also contended that no government agency knew about Heartbleed before it was exposed earlier this week, saying that it is “in the national interest” to notify relevant parties about such bugs but added “unless there is a clear national security or law enforcement need.”

Bloomberg reporter Michael Riley cited two anonymous sources for the information in their report.

If Riley’s account is confirmed, the NSA’s actions left millions of users and companies vulnerable to data mining and attacks from anyone that was able to exploit the flaw, like hackers and security agencies like the NSA. Responses to the NSA denial have thus far been marked by skepticism.

Many Americans distrust the NSA because of conflicting statements in the past. For example, James Clapper later recanted his 2013 congressional testimony in which he claimed the NSA intelligence-gathering programs were smaller than the public thought. In an apologetic letter to Senate Intelligence Committee Chairman Dianne Feinstein, Clapper admitted to erroneous statements.