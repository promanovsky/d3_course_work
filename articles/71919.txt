The negative effects of climate change are already beginning to be felt in every part of the world and yet countries are ill-prepared for the potentially immense impacts on food security, water supplies and human health, a major report has concluded.

In the most comprehensive study yet into the effects of rising levels of carbon dioxide in the atmosphere, the UN's Intergovernmental Panel on Climate Change (IPCC) warns that global warming could undermine economic growth and increase poverty.

Scientists identified Britain as one of the countries most at risk from some of the more immediate negative effects of climate change, with the UK and northern Europe warned to expect increased coastal and inland flooding, heatwaves and droughts.

The IPCC found that these unwanted impacts have already extended beyond any potential benefits of rising temperatures and that they will worsen if global-average temperatures continue to rise by the expected lower limit of 2C by 2100 - and will become potentially catastrophic if temperatures rise higher than 4C.

In pictures: Changing climate around the world Show all 15 1 /15 In pictures: Changing climate around the world In pictures: Changing climate around the world Greenland Calved icebergs from the nearby Twin Glaciers are seen floating on the water in Qaqortoq, Greenland In pictures: Changing climate around the world Iran Oroumieh, one of the biggest saltwater lakes on Earth, has shrunk more than 80 percent to 1,000 square kilometers in the past decade. It shrinks mainly because of climate change, expanded irrigation for surrounding farms and the damming of rivers that feed the body of water In pictures: Changing climate around the world Greenland A boat navigates among calved icebergs from the nearby Twin Glaciers in Qaqortoq, Greenland. Boats are a crucial mode of transportation in the country that has few roads. As cities like Miami, New York and other vulnerable spots around the world strategize about how to respond to climate change, many Greenlanders simply do what theyve always done: adapt. 'Were used to change, said Greenlander Pilu Neilsen. 'We learn to adapt to whatever comes. If all the glaciers melt, well just get more land In pictures: Changing climate around the world Norway The Svalbard Global Seed Vault is seen after being inaugurated in Longyearbyen, Norway. The 'doomsday' seed vault built to protect millions of food crops from climate change, wars and natural disasters opened deep within an Arctic mountain in the remote Norwegian archipelago of Svalbard In pictures: Changing climate around the world France A technician preparing to drain a vast underground lake at the Tete Rousse glacier on the Mont Blanc Alpine mountain, to avert a potentially disatrous flood. Some 65,000 cubic metres (2.3 million cubic feet) of water have gathered in a cavity, dangerously raising the pressure beneath the mountain, a favourite spot for holiday makers in Saint-Gervais-les-Bains In pictures: Changing climate around the world Switzerland Cracked mud is picture at sunrise in the dried shores of Lake Gruyere affected by continuous drought near the western Switzerland village of Avry-devant-Pont. A leading climate scientist warned that Europe should take action over increasing drought and floods, stressing that some climate change trends were clear despite variations in predictions In pictures: Changing climate around the world USA Cattle graze on grassland that remains dry and brown at the height of the rainy season in south of Bakersfield, California. Its third straight year of unprecedented drought, California is experiencing its driest year on record, dating back 119 years, and dating back as far as 500 years, according to some scientists who study tree rings In pictures: Changing climate around the world Pakistan An aerial view shows tents of flood-displaced people surrounded by water in southern Sehwan town. United Nations Framework Convention on Climate Change (UNFCCC) executive secretary Christiana Figueres met with people displaced by last year's devastating floods. Catastrophic monsoon rains that swept through the country in 2010 and affected some 20 million people, destroyed 1.7 million homes and damaged 5.4 million acres of arable land In pictures: Changing climate around the world Australia An aerial view of flooding in North Wagga Wagga. Climate change is amplifying risks from drought, floods, storm and rising seas, threatening all countries but small island states, poor nations and arid regions in particular, UN experts warned In pictures: Changing climate around the world Honduras Damages caused by a landslide on the Pan-American highway near La Moramulca, 55 Km south of Tegucigalpa. International highways have been washed out, villages isolated and thousands of families have lost homes and crops in a region that the United Nations has classified as one of the most affected by climate change In pictures: Changing climate around the world Indonesia A resident sprays water on a peatland fire in Pekanbaru district in Riau province on Indonesia's Sumatra island. Indonesia, an archipelago of 17,000 islands, is one of the world's biggest carbon emitters because of rampant deforestation. US Secretary of State John Kerry Sunday issued a clarion call for nations to do to more to combat climate change, calling it 'the world's largest weapon of mass destruction' In pictures: Changing climate around the world Indonesia An excavator clearing a peatland forest area for a palm oil plantations in Trumon subdistrict, Aceh province, on Indonesia's Sumatra island. As Southeast Asia's largest economy grows rapidly, swathes of biodiverse forests across the archipelago of 17,000 islands have been cleared to make way for paper and palm oil plantations, as well as for mining and agriculture. The destruction has ravaged biodiversity, placing animals such as orangutans and Sumatran tigers in danger of extinction, while also leading to the release of vast amounts of climate change-causing carbon dioxide In pictures: Changing climate around the world Bangladesh Stagnant rain water with tannery waste make the Hazaribagh area in Old Dhaka as well as Buriganga River the most polluted. Each year during the seven-month long dry season between October and April the Buriganga River becomes totally stagnant with its upstream region drying up and becoming polluted from toxic waste from city industries In pictures: Changing climate around the world Bangladesh Waste water from Dhaka city drained to the River Buriganga contributes to its pollutions. On the World Water Day observed in 2007 under the theme Coping with Water Scarcity, under the leadership of the Food and Agriculture Organization of the United Nations, DrikNEWS explores some of the images of the river. UN-Water has identified coping with water scarcity as part of the strategic issues and priorities requiring joint UN action. The theme highlights the significance of cooperation and importance of an integrated approach to water resource management of water at international, national and local levels In pictures: Changing climate around the world China Heavy smog has been lingering in northern and eastern parts of China, disturbing the traffic, worsening air pollution and forcing the closure of schools. China's Environment Ministry said it will send inspection teams to provinces and cities most seriously affected by smog to ensure rules on fighting air pollution are being enforced

In a blunt and often pessimistic assessment of climate-change impacts - the fifth assessment since 1990 - the IPCC scientists give a stark warning about what the world should expect if global temperatures continue to rise as predicted without mitigation or adaptation.

"In recent decades, changes in climate have caused impacts on natural and human systems on all continents and across the oceans," says the report Climate Change 2014 Impacts, Adaptation and Vulnerability, formally released early this morning by the IPCC after a final editorial meeting in Yokohama, Japan.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

"Throughout the 21st century, climate-change impacts are projected to slow down economic growth, make poverty more difficult, further erode food security, and prolong existing and create new poverty traps, the latter particularly in urban areas and emerging hot spots of hunger," the report states.

Scientists in Britain said it is the clearest warning yet of what could happen if the world continues to prevaricate over cuts in emissions. "Climate change is happening, there are big risks for everyone and no place in the world is immune from them," said Professor Neil Adger of Exeter University, one of the many lead authors of the report.

Nearly 2,000 experts from around the world contributed to the report, written by 436 authors and edited by 309 lead authors and review editors of the IPCC's working group II. It was by far the most detailed investigation to date of the global impacts of climate change - extending from oceans to mountains and from the poles to the equator.

Christopher Field, of the Carnegie Institution's Department of Global Ecology in California and co-chair of working group II, said that the observed impacts of climate change are already widespread and that "we're not talking about hypothetical events".

The much-touted benefits of climate change - such as the ability to grow some crops such as vineyards at higher northerly latitudes - dwindle into insignificance compared to the enormous challenges produced by the much bigger downsides of a warmer and more stressed world, the report suggests.

"It is true that we can't find many benefits of climate change and I believe it's because there aren't many benefits, even though we tried really hard to find them," Dr Field said. "There are a few places where there are a few benefits of warming but there are many other places where there are widespread negative impacts," he said.

The evidence for climate-change impacts is the strongest and most comprehensive for natural systems, such as melting mountain glaciers and polar ice, and the earlier and earlier signs of spring. However, impacts on human systems, such as a rise in certain tropical diseases, can also be attributed to climate change.

"We live in an era of man-made climate change. In many cases, we are not prepared for the climate-related risks that we already face," said Vicente Barros, co-chair of the IPCC working group II.

The climate-related impacts studied by the IPCC included:

Food security

Crop yields have increased in general over recent decades but the rate of improvement would have been even faster had it not been for climate change. The signature of rising temperatures and heat stress are already showing on yield of wheat and maize, the report says.

"All aspects of food security are potentially affected by climate change, including food access, utilisation and price stability," it says.

Freshwater supplies

As global temperature rise, then so does the fraction of the human population that are affected by either water scarcity or river flooding. "Climate change over the 21st century is projected to reduce renewable surface water and groundwater resources significantly in most dry subtropical regions," the IPCC says.

Loss of species

The risk of plant and animal extinctions increases under all climate change scenarios, but they get worse with higher temperatures. Loss of trees and forest dieback will be a particular problem in a warmer world, the report says.

"A large fraction of both terrestrial and freshwater species face increased extinction risk under projected climate change during and beyond the 21st century, especially as climate change interacts with other stressors such as habitat modification, over-exploitation, pollution and invasive species," it says.

Ocean acidification

Coral reefs and shelled marine creatures, especially the smaller animals at the base of the marine food chain, are at special risk of rising carbon dioxide concentrations, which are causing the oceans to become more acidic and less alkaline. This in turn will affect human populations that rely on sea fish as a food source.

Global economy

Economic losses due to climate change are difficult to assess and many past estimates have not taken into account the catastrophic changes that could result from the climate passing a "tipping point". Losses, however, are more likely than not to be greater, rather than smaller, than an estimated range of between 0.2 and 2 per cent of global income loss due to a temperature rise of about 2C.

Human security

Climate change can indirectly increase the risk of violent conflicts, such as civil wars, by amplifying the well-documented "drivers" such as poverty and economic shocks. Climate change will also increase the risk of unplanned displacement of people and a change in migration patterns, the report says.

Scientists said that the messages about the threat posed by climate change in the 21st century have never been clearer but there is still time to mitigate the worst effects by cutting greenhouse gas emissions with sustainable energy sources as well as to adapt to the expected changes with technological improvements.

Professor Corinne Le Quere, director of the Tyndall Centre for Climate Change Research at the University of East Anglia, said that the study is not "just another report" but the scientific consensus reached by hundreds of scientists.

"The human influence on climate change is clear. The atmosphere and oceans are warming, the snow cover is shrinking, the Arctic sea ice is melting, sea levels are rising, the oceans are acidifying, some extreme weather events are on the rise, ecosystems and natural habitats will be upset.

Climate change threatens food security and world economies," Professor Le Quere said.

Meat and cheese may have to be off the menu if there is to be any hope of hitting climate change targets.

A separate study says cutting greenhouse gas emissions from energy use and transport will not be enough on its own to hold down the global temperature rise.

The research indicates it will also be necessary to slash emissions from agriculture - meaning curbing meat and dairy consumption. Without such action, nitrous oxide emissions from fields and methane from livestock may double by 2070, making it impossible to meet the UN target.