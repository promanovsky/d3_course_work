People whose training regimens include intense endurance workouts might actually be at increased risk for heart damage, according to a couple of big new studies released yesterday - but researchers caution that they don't want people to take that as an excuse to skip the gym.

Instead, these results might help doctors figure out why certain people develop irregular heartbeats and how exercise might affect someone who already has heart problems.

How healthy people might end up with faulty hearts later in life

Advertisement

Some people who train intensely more than five hours a week at age 30 are more likely to develop an irregular heartbeat later on in life, especially if they seriously cut back on exercise by the time they turn approximately 60, according to one of the two studies published in Heart, a British Medical Journal publication.Researchers looked at a group of approximately 44,000 Swedish men, with an age range between 45 and 79 and an average age of 60. In 1997, they asked them how frequently they exercised at ages 15, 30, 50, and their current age.

At the end of 2009, they followed up on the group to see if any had developed atrial fibrillation, an irregular heartbeat, which can increase the risk of stroke, cause chest pain, and even lead to heart failure. In the general population, atrial fibrillation affects almost 8% of people between 65 and 74, with the risk increasing further as people age.

Advertisement

In the group of Swedish men, those who exercised more than five hours a week when they were 30 but less than one hour a week when they were 60 were significantly more likely to have developed an irregular heartbeat than those doing almost no exercise the whole time.

This increased risk held even after researchers controlled for other diseases, body mass index, smoking history, aspirin use, and alcohol consumption.

The results seem to suggest that being sedentary in middle age may be more problematic in some ways for those who were super active at 30 than for those who were lazy all along.

But before you let your running sneakers gather dust, consider that moderate levels of exercise were universally beneficial, and that even intense exercise had protective effects for those in middle age.

Advertisement

Moderate exercise is best for those already with heart disease

In the second study, researchers looked at a group of just over 1,000 Germans who were receiving medical rehabilitation for issues related to heart disease. They followed up with them repeatedly over the next 10 years, checking their heart health and seeing how frequently they exercised.

In this group, about 40% said they hiked, cycled, played sports, or gardened two to four times a week. About 30% said they worked out more frequently than that, and 30% did less, including 10% who said they almost never did anything physical.

Advertisement

Those who did no exercise were the most likely to die of a cardiovascular event, and working out only once a week was also tied to increased mortality. However, the group that was least likely to die or suffer a heart attack or stroke wasn't those who got the most exercise, it was those who worked out a moderate amount: two to four times a week.

Once people started working out five times a week or more, their likelihood of death and other problems rose again, though it didn't get as high as for those who did nothing.

Why this doesn't mean you should cut down on exercise

Both of these studies do match some previous data that says athletes are more likely to develop irregular heartbeats than the general population and that exercise-induced inflammation could overstress a burdened heart. But both come with an important note of caution, which is that they rely on people accurately reporting how much exercise they get.

Advertisement

In the one that looked at the Swedish men, participants may not have accurately remembered how much they worked out at age 30. Perceptions of intensity might also vary greatly from person to person, making it hard to draw solid conclusions from the findings. Plus, the study only looked at men, and women could have different outcomes.In the German study that looked at those who already had heart disease, researchers relied on participants' accounts of both how frequently and how intensely they worked out. Those self-reports might not be completely accurate.

The researchers mention the caution required when relying on self-reports, which is echoed alongside other concerns in an editorial also published in Heart.

Advertisement

Physicians Eduard Guasch and Lluís Mont argue that these results are helpful for researchers and for doctors advising individual patients, but less important for general workout guidelines. They say that even though being aware of potential adverse effects is useful, there is "no other known therapy" as beneficial as physical exercise for preventing or recovering from heart issues.

Make no mistake: Regular physical activity is absolutely crucial to your health.

"A thin line separates accurate information and unnecessary alarmism leading to inactivity and consequent heart disease," the editorial argues. "The beneficial effects of exercise are definitely not to be questioned; on the contrary, they should be reinforced."