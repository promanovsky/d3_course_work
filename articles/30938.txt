Netflix's Reed Hastings Calls for Strong Net Neutrality

Less than a year ago, the Netflix chief's stock was at $285 a share. On July 30, it closed at just $57.75 thanks to slowing domestic growth and the high cost of expanding overseas.

The Netflix CEO wrote in a blog post that the online streamer's recent interconnection deal with Comcast shouldn't be necessary.

Weeks after Netflix announced a deal with Comcast to provide faster connections to its subscribers, the online streamer has come out against such deals.

CEO Reed Hastings wrote on the company's blog that large Internet service providers such as Comcast and AT&T shouldn't be able to exact tolls from content distributors in exchange for better Internet.

"If this kind of leverage is effective against Netflix, which is pretty large, imagine the plight of smaller services today and in the future," he wrote.

The blog post is part of Netflix's effort to support the Federal Communication Commission's enforcement of an open Internet, known as net neutrality. The agency is in the process of drafting new anti-discrimination rules after a federal appeals court struck down its previous set of rules in January.

Technically, the FCC's net neutrality rules would not affect the deal signed in February between Netflix and Comcast. Netflix essentially agreed to pay Comcast to connect directly with its servers to provide faster Internet to its subscribers, a process known as peering. Comcast is not giving Netflix preferential treatment as part of the deal.

VIDEO: TV Executives Roundtable: Frustrations With Netflix

But Hastings noted that a broader description of net neutrality would prevent Comcast from charging Netflix and others.

"This weak net neutrality isn't enough to protect an open, competitive Internet; a stronger form of net neutrality is required," he wrote. "Strong net neutrality additionally prevents ISPs from charging a toll for interconnection to services like Netflix, YouTube, or Skype, or intermediaries such as Cogent, Akamai or Level 3, to deliver the services and data requested by ISP residential subscribers. Instead, they must provide sufficient access to their network without charge."

He added that Netflix will continue to pay these fees if it means providing a better experience to subscribers.

"While in the short term Netflix will in cases reluctantly pay large ISPs to ensure a high quality member experience, we will continue to fight for the Internet the world needs and deserves."

PHOTO: Kevin Spacey and Robin Wright Toast 'House of Cards' Season 2

Comcast has responded to Hastings' post with its own declaration of support for net neutrality.

"There has been no company that has had a stronger commitment to openness of the Internet than Comcast," said Comcast evp David Cohen. "We supported the FCC's Open Internet rules because they struck the appropriate balance between consumer protection and reasonable network management rights for ISPs. We are now the only ISP in the country that is bound by them."

He added that asking content providers such as Netflix to pay for interconnection is nothing new.

"Providers like Netflix have always paid for their interconnection to the Internet and have always had ample options to ensure that their customers receive an optimal performance through all ISPs at a fair price," he said. "We are happy that Comcast and Netflix were able to reach an amicable, market-based solution to our interconnection issues and believe that our agreement demonstrates the effectiveness of the market as a mechanism to deal with these matters."