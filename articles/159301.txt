hidden

Microsoft's new chief executive got off to a winning start with Wall Street on Thursday as the world's largest software company eased past analysts' profit estimates despite the pressure of falling computer sales.

Its shares rose almost 3 percent in after-hours trading to $40.96, keeping the stock at levels not seen since the turn-of-the-century Internet stock boom.

Microsoft shares are up about 8 percent since company veteran Satya Nadella took over as CEO in early February, and are up 19 percent since his predecessor Steve Ballmer announced plans to retire last August, easily outpacing the Standard & Poor's 500.

Investors are excited about Nadella's focus on mobile and cloud, or Internet-based, computing, designed to take Microsoft beyond its traditional PC-based Windows business. Nadella is set to face analysts' questions for the first time in public on a conference call later on Thursday.

"This quarter is a nice step in the right direction for Nadella and Microsoft," said Daniel Ives, an analyst at FBR Capital Markets. "We would characterize these results as solid in a choppy IT spending environment."

Nadella's emphasis on cloud computing helped its server software business, while a softer-than-expected decline in PC sales limited damage to the bottom line.

The Redmond, Washington-based company reported quarterly profit of $5.66 billion, or 68 cents per share, compared with $6.05 billion or 72 cents in the year-ago quarter.

The decline was exaggerated by deferred revenue boosting the year-ago figure, and the latest quarter's profit beat Wall Street's average estimate of 63 cents per share, according to Thomson Reuters I/B/E/S.

Sales fell 0.4 percent to $20.4 billion, meeting analysts' average estimate.

Personal computer sales fell by as much as 4.4 percent in the quarter, according to the two major technology research firms, making the eighth straight quarter of declines as tablets and smartphones gain in popularity.

That decline was likely muted by the end of Microsoft's support for its decade-old Windows XP system in early April, which appears to have prompted many people to buy a new computer.

Reuters