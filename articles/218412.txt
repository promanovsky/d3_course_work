The greatest names in British TV are colliding on May 18, with stars like Jamie Dornan, Helena Bonham Carter, and more hoping to lock down a BAFTA award. It’s going to be one classy red carpet — and you can stream it below!

Get ready for an across-the-pond version of the Emmys! The BAFTA Television Awards are set for May 18, featuring some of the biggest names in TV both in the UK and the world. Tune in below to see all the dashing stars take the carpet — from Jamie Dornan on — to celebrate their big night!

BAFTA Television Awards Live Stream — Watch Online

While you may not be totally in tune with what’s on the tube (I think they call TVs that in England) in the UK, the BAFTA Television Awards showcase huge series and actors that transcend the British borders.

CLICK HERE TO WATCH THE BAFTA LIVE STREAM

Fifty Shades of Grey star is being celebrated for his small-screen efforts on the show The Fall — he’ll go up against stars like Dominic West and Sean Harris in the Best Actor category. Another renown actor, Helena Bonham Carter, meanwhile, is nominated for Best Actress for her role in Burton and Taylor.

Some shows of note are also hoping to have big nights at the BAFTAs. Top of the Lake, starring the Golden Globe-winning actress Elisabeth Moss, and BBC’s Broadchurch are both up for Best Drama Series.

But the real show is going to be on the red carpet. No one does class like the Brits, so we’re sure this is going to be one fashion event you’re not gonna want to miss. It all starts at 1 p.m. ET, so check out the live stream, and then come back later for all the Billboard Music Awards coverage you could dream of! It’s all happening today!

— Andrew Gruttadaro

Follow @AndrewGrutt

More BAFTA News: