Updated from 11:56 a.m. ET to include analyst comments, afternoon share prices.

NEW YORK (TheStreet) -- Don't think Blackstone Group (BX) - Get Report, the majority owner of Pinnacle Foods (PF) , was surprised by Pilgrim's Pride's (PPC) - Get Report interest in buying Hillshire Brands (HSH) , potentially nixing a $6.6 billion deal takeover of Pinnacle by Hillshire that was announced on May 12.

Differing termination fees paid to Pinnacle Foods in the event Hillshire Brands wasn't able to acquire the company signals it was aware a suitor might emerge for Hillshire. That knowledge of a potential bid indicates Pinnacle shareholders may be overreacting to Pilgrim's Pride unsolicited offer to buy Hillshire Brands for $6.4 billion, disclosed on Tuesday morning.

Pinnacle Foods shares were falling nearly 7% in late Tuesday trading, while Hillshire Brands shares were rising over 22% to $45.39. Pilgrim's Pride shares were rising over 1% in afternoon trading to $25.49 as a result of its all-cash offer, trimming some early trading gains.



WATCH: More market update videos on TheStreet TV | More videos from Debra Borchardt

Two Term Fees

Pinnacle was poised to receive $43 million if Hillshire shareholders didn't support the May 12 merger and $163 million if a suitor emerged for Hillshire. Those two separate termination fees -- one in the event Hillshire shareholders hated the Pinnacle merger and one in the event Hillshire became a takeover target -- were slightly unusual.

However, Pilgrim's Pride's decision to unveil a $45 a share unsolicited premilinary proposal for Hillshire Brands on Tuesday explains the dual termination fees paid to Pinnacle Foods, which continued to count Blackstone Group as a controlling shareholder after its April 2013 initial public offering.

In fact, Blackstone and Pinnacle Foods were likely aware that Pilgrim's Pride was interested in buying Hillshire Brands when they negotiated the May 12 transaction. Pilgrim's Pride said in its proposal for Hillshire Brands it had met with the company's management about a possible transaction on February 20th.

"[W]e are writing to convey our proposal to acquire The Hillshire Brands Company ('Hillshire' or the 'Company'). As expressed during our meeting in Chicago on February 20, 2014 we have the utmost respect for Hillshire, its leadership and its employees, and, as you are well aware, it has long been our desire to acquire the Company," Pilgrim's Price CEO Sean Connolly said in a Tuesday letter to Hillshire Brands.

That might complicate how investors read Pilgrim's Pride's $6.4 billion proposal for Hillshire. Yes, Pinnacle and Blackstone would receive a total of $163 million if their deal with Hillshire fell apart due to a takeover of the parent. Still, it is unlikely an investor as smart as Blackstone would support a sale of a large, publicly traded portfolio company that it believed was likely to fail.

Christine Anderson, a Blackstone spokesperson, declined to comment.

Negotiations to Begin?

Pilgrim's Pride doesn't actually have a formal offer on the table yet, however, Hillshire Brands said on Tuesday it would review the $45 a share in cash takeover proposal.

"We continue to strongly believe in the strategic merits and value creation potential provided by the proposed transaction with Pinnacle Foods," Hillshire Brands said on Tuesday. "Consistent with its fiduciary duties, and in consultation with its independent financial and legal advisors, Hillshire Brands' Board will thoroughly review the Pilgrim's Pride proposal," the company added.

The takeover proposal by Pilgrim's Pride is subject to due diligence, the support of Hillshire's board and regulatory approvals. Nothing Pilgrim's Pride unveiled on Tuesday is binding. Pilgrim's Pride has hired investment bank Lazard and law firm Cravath, Swaine & Moore to work on its Hillshire proposal.

There are now two Hillshire deals to think about: One where Hillshire buys Pinnacle Foods for $6.6 billion in cash and stock and one where Pilgrim's Pride buys Hillshire for $6.4 billion in cash. It is probably best to believe the Pinnacle Foods deal is Plan A for Hillshire and that Pilgrim's Pride is plan B.

A Better Deal?

Some analysts who had thought Hillshire wouldn't find a buyer to thwart its Pinnacle merger now see greater value in the Pilgrim's Pride offer. Robert Moskow, a packaged foods analyst at Credit Suisse initially said on May 15 strategic bidders for Hillshire would be unlikely and noted Brazilian firm JDS, the owner of Pilgrim's Pride, might have too stretched a balance sheet to consummate a merger. Moskow also noted Tyson, Hormel and Kraft might balk at the prospect of acquiring Hillshire.

On Tuesday, Moskow changed his tune. The analyst said the Pilgrim's Pride proposal has a far superior financial rationale than Hillshire Brands offer for Pinnacle Foods.

"As is typical in these situations, we think that Hillshire's Board of Directors will hold out for a slightly higher premium (perhaps 3-5% [over the current proposal]), but that they eventually will agree to Pilgrim's offer," Moskow said. He said it was unlikely Hillshire would adopt a so-called 'poison pill' to thwart an unsolicited takeover bid.

Hillshire Brands spokesperson Mike Cummins declined to comment beyond the company's public statements when reached by telephone.

Bottom Line: It doesn't seem likely Blackstone Group or Pinnacle Foods were surprised by an unsolicited bid Pilgrim's Pride for Hillshire Brands. If Pilgrim's Pride does win Hillshire, it may indicate Blackstone was outmaneuvered on the deal.

>>Read More:

Bank of America Tries to Bounce Back After $4 Billion Capital Blunder

Pilgrim's Pride Bids $6.4B for Hillshire



-- Written by Antoine Gara in New York.

Follow @AntoineGara