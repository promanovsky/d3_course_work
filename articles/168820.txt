The late-night TV shuffle continues: After 10 years at the helm of his 12:3o a.m. late-night show, Craig Ferguson will step down from “The Late Late Show” in December.

Ferguson told his studio audience the news Monday afternoon, and a few hours later, CBS sent out the announcement confirming it.

“During his 10 years as host, Craig has elevated CBS to new creative and competitive heights at 12:30,” said the network’s entertainment chairman, Nina Tassler. “He infused the broadcast with tremendous energy, unique comedy, insightful interviews and some of the most heartfelt monologues seen on television.”

During the show’s taping, the audience responded to the news with a very sad-sounding “awww.” “I’ll be stepping down at the end of this year in December and then I’ll go and do something else…I’m thinking carpentry, but I haven’t made my mind up yet,” Ferguson said. “I feel like doing this show for 10 years, that’s enough,” he added.

Ferguson also weighed with an official statement. “CBS and I are not getting divorced, we are ‘consciously uncoupling,’ ” he said in the network’s announcement. “But we will still spend holidays together and share custody of the fake horse and robot skeleton, both of whom we love very much.”

Yep, fake horse and robot skeleton. They’re both hallmarks of Ferguson’s wacky late-night hour, and possibly part of the reason CBS declined to promote him to the 11:35 p.m. “Late Show” when David Letterman announced his retirement earlier this month — he’s not quite mainstream enough. Although Ferguson, 51, has a developed a devoted cult following over the years, this isn’t a very surprising move. Given that Stephen Colbert is taking over “The Late Show” once Letterman steps down in 2015, it’s likely that CBS wants to start fresh with a brand-new late-night slate.

For the record, Ferguson told Variety, he was already planning to leave this year before Letterman announced his impending retirement. CBS convinced him to stay on board until the end of 2014. “It’s an inevitable thing when David announced his retirement, people are going to say ‘Oh he’s leaving because of that,'” Ferguson said. “No matter what I say or what I do, they’re gonna say that.”

Meanwhile, CBS added in its release that Ferguson, a Scottish-born actor who is also best known for his long-running role on “The Drew Carey Show,” has several projects in the works, including hosting the syndicated “Celebrity Name Game” this fall and developing shows for his own production company.

Ferguson took over the “Late Late Show” in 2005 after Tom Snyder (1995 to 1999) and Craig Kilborn (1999 to 2004) both hosted, and he quickly found an audience that enjoyed a host wildly different from anyone else that was on. Airing directly after “Late Show With David Letterman,” the show is owned by Worldwide Pants, Letterman’s production company.

Ferguson has gained a cult following over the years for his off-the-cuff interviews, as he will dramatically rip up his blue note cards before a guest sits down and ask whatever comes to his mind. He’s also known for many goofy running jokes on the show throughout the years, including his snake mug and love of puppets.

But Ferguson has quite the serious side that’s earned high praise. In addition to an Emmy Award win in 2006, the show won a Peabody Award in 2009 after his intense interview with Archbishop Desmond Tutu. He never shies away from candidly addressing tragic topics in the news, from Aurora shootings to the Boston Marathon bombings. Ferguson also addresses intensely personal issues: One of his most talked-about moments was a long monologue in 2007 about battling his own demons with addiction, as he announced he wouldn’t tell any jokes about Britney Spears’s infamous meltdown.

He has also notoriously refused to take part in the “late night” wars that have plagued his fellow hosts. (Sample comment when the Conan O’Brien-Jay Leno madness was going on at NBC: “It is a bunch of middle-aged white guys arguing over who gets X million dollars. Who gives an ay-carumba?”)

“I don’t give a (expletive) about the late-night wars… If late-night is a club, I don’t want to be a member,” Ferguson was once quoted as saying years ago. He later clarified to Entertainment Weekly, “What I meant by that was, I don’t want somebody to lump me in with a whole form of TV that I’m not necessarily a real part of. We do a show that’s on late at night, but it’s not ‘a late-night talk show.’”

The Post’s TV critic Hank Stuever profiled Ferguson just after the show debuted in 2005, and wrote: “Ferguson’s show already gives off light beams of jolliness, even as you get the feeling that it’s possibly a happy train wreck. In an age of snark, it’s almost too retro: A good-looking man walks out, has an accent, tells some jokes, makes chitchat, introduces a band, and nobody gets hurt.”

“I don’t try to put a positive spin on things when I talk to guests, or the audience,” Ferguson said at the time. “I think that’s the reason I talk about my ex-wife on the show, or being a dad, or alcoholism, to say, yes of course I’m a member of the human race and yes of course I have interactions which succeed and fail, and other than that I’m okay and I’m doing all right and this show, you know, we’re not here to talk about our feelings and help you out with your [expletive], because I don’t even know ya. It’s TV.”

(This post has been updated.)

RELATED

Stephen Colbert visits Letterman and his future late-night home

Stephen Colbert will replace David Letterman on “The Late Show”

Letterman’s replacement: Can he (or she!) please be a grown up?

David Letterman explains why it was time for him to retire.

Some of Letterman’s best moments through the years

Letterman’s retirement signals a possible generational shift for late-night TV

Here’s what happened the moment David Letterman announced his retirement

David Letterman: 2012 Kennedy Center Honors profile

GALLERY: David Letterman’s late shows