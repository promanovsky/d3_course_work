The researcher who is calling for the retraction, Teruhiko Wakayama, is a highly-respected stem cell scientist who was the first to clone a mouse. His involvement with the papers was one of the key reasons that outside scientists who were skeptical of the stunningly simple method -- dipping mature cells in an acid bath to turn them into stem cells -- took it seriously.

For weeks, the papers published in the journal Nature by a team of Boston and Japanese scientists have been under intense scrutiny, which has revealed problems with some of the images and an allegation of plagiarism in the Methods section of one of the papers.

According to Japanese news reports , a respected scientist and co-author of two high-profile papers that described a controversial technique for creating stem cells has called for the studies to be withdrawn because of errors .

“I’m no longer sure that the articles are correct,” Wakayama said at a press conference given Monday evening in Japan, according to The Japan News.

Advertisement

Wakayama told another news agency, NHK World, that he is now not sure the cells he was given by his coauthors, to check whether they could develop into any cell in the body, were truly created by the technique reported.

“He said he is convinced his experiments were correct, but is no longer sure about the credibility of the data used as preconditions for the experiments,” NHK World reported.

Last week, the RIKEN Institute in Japan, where many of the scientists involved in the studies work, released a detailed protocol explaining how to make the cells. It was an effort to clarify a situation that has been increasingly murky, because of doubts raised about whether the work was repeatable. Even Wakayama told Nature News that although he was able to do the procedure successfully once, with help from the lead author, Haruko Obokata, he has not since been able to repeat the experiment on his own.

Advertisement

But the protocol itself raised many questions. Paul Knoepfler, a stem cell scientist at the University of California, Davis, whose blog has become a resource for information about the technique, described what appeared to be contradictions and red flags.

Wakayama told NHK World that his review of data brought to meetings revealed many serious problems, including some with images. The news agency reported that he has asked the entire team to retract the papers and then have the work reviewed by outside scientists.

Wakayama did not respond immediately to an e-mail, but Dr. Charles Vacanti of Brigham and Women’s Hospital, the senior author of the paper describing the technique for creating stem cells, said he only learned about Wakayama’s request Monday morning, through news reports.

“I’ve not communicated with him yet. This is the first I’ve heard of it. I have zero information,” Vacanti said. He said that he still stands behind the papers, and that so far the problems with the papers have been errors that do nothing to endanger the overall result.

“I think there were some simple, perhaps sloppy mistakes that I thought were honest mistakes,” Vacanti said. “I don’t have any information that would make me change my opinion of what I said before.”

An investigation into the papers has been ongoing at the RIKEN Institute, where Obokata, the lead author of both of the papers, works. Obokata did not immediately respond to an e-mail.

Advertisement

In a statement, the journal said the investigation is not complete.

“Issues relating to the papers have been brought to Nature’s attention and we are conducting an on-going investigation. We have no further comment at this stage,” a spokeswoman for Nature wrote in an e-mail.

Carolyn Y. Johnson can be reached at cjohnson@globe.com. Follow her on Twitter @carolynyjohnson.