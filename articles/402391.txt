An employee of Hyundai Motor walks past the Grandeur sedan car on display at its dealership in Seoul Thomson Reuters

DETROIT (Reuters) - South Korean automaker Hyundai Motor Co. will recall about 883,000 Sonata mid-sized sedans in the United States and Puerto Rico because a potentially defective transmission-shift cable could increase the risk of a crash.

The recall affects certain Sonata cars from model years 2011 to 2014, in which the transmission-shift cable could detach from the shift-lever pin, causing the gear selection not to match the indicated gear, according to documents posted Wednesday by U.S. safety regulators.

That would cause the cars to move in an unintended or unexpected direction, the documents by the National Highway Traffic Safety Administration said.

The automaker identified 1,171 warranty claims and seven incidents, including not being able to take the car out of park, related to this issue, the documents said.

A spokesman said the company was not aware of any accidents, injuries or fatalities relating to the issue.

A driver may select the 'park' position, but the vehicle may not be in park and could roll away, the documents said.

Dealers will repair the connection between the shift cable and shift lever at no cost, as needed, the documents said.

Hyundai expects owners will begin to be notified of the recall by mail during the third quarter.

About 880,000 of the affected vehicles were sold in the United States and the rest in Puerto Rico, according to the NHTSA documents.

A Hyundai spokesman did not know if vehicles outside the United States were affected.

Hyundai will also recall 5,650 of its new 2015 Sonata cars in the United States and Puerto Rico because one or both front-brake calipers may crack, making the brakes less effective and raising the risk of a crash, the documents said.

The recall does not affect vehicles outside the United States, a spokesman said.

The automaker was not aware of any accidents, injuries or deaths relating to the issue. Fewer than 200 of the cars are in consumer hands and dealers were told not to sell the remaining affected vehicles until they were repaired, the spokesman said.

Hyundai contacted the affected owners last month and told them not to drive their cars, according to the NHTSA documents. Dealers will replace the calipers at no cost.

(Reporting by Ben Klayman in Detroit; Editing by Bernadette Baum)