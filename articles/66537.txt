A patent published by the United States Patent and Trademark Office Thursday confirms that Apple Inc. (NASDAQ:AAPL) has been working on touchscreen implementations that utilize sapphire material.

A patent filed by Apple on Sept. 23, 2013, and published on Thursday indicates that the Cupertino, Calif.-based tech giant has been working on an oleophobic (fingerprint and oil-resistant) coating, on sapphire material, which is rumored to be used for the next-generation iPhone. The sapphire-related patent correlates with Apple's efforts to ramp up sapphire production at its new sapphire manufacturing facility in Mesa, Ariz.

The patent filed by Apple details a process used to apply a fingerprint-resistant coating to a base layer, such as a single-sapphire crystal.

Apple describes the potential for this material in its patent description:

“A surface layer may be formed on the transition layer, with a substantially silica content, for example substantially 100% silica or silica glass, and the surface coating may be oleophobic. A portable electronic device may comprise the coated component, the portable device may include a window, the oleophobic coating may be provided on an exterior surface of the window, and the window may also include a touch screen.”

Photo: United States Patent and Trademark Office

Analyst Mark Margolis notes that the particular patent application describes the oleophobic layer application process in a way that could allow for “batch processing” of a solid piece of sapphire instead of applying the fingerprint-resistant layer to individual device screens.

While Apple currently uses sapphire for its iPhone 5S camera lens cover and the Touch ID home button, this latest patent filed indicates that Apple is looking to utilize the hard material in larger ways.

Ever since Apple announced its manufacturing partnership with GT Advanced Technologies (NASDAQ:GTAT) in Mesa, Ariz., there has been constant speculation surrounding the move. While Apple has been relatively mum about specifics surrounding its partnership with GT, industry-watchers have speculated that Apple may use the Arizona plant to manufacture sapphire screens for a next-generation iPhone, which some have called the iPhone 6.

In addition to the newly published patent by Apple, a February 2014 public statement by Tom Gutierrez, GT's president and chief executive officer, said, “We expect to return to profitability during the second half of 2014,” correlating with the rumored fall 2014 launch window for the next iPhone.

Sapphire material has been highly rumored to be utilized more extensively in the next batch of Apple iOS devices. But some critics, including Gorilla Glass manufacturer Corning Inc. (NYSE:GLW), have deemed the material to be inferior to more traditional smartphone display materials, such as glass. Earlier in March, Corning Senior Vice President Tony Tripeny criticized sapphire as a display material at the Morgan Stanley (NYSE:MS) Media and Telecom Conference:

“When we look at it, we see a lot of disadvantages of sapphire versus Gorilla Glass. It's about 10 times more expensive. It's about 1.6 times heavier. It's environmentally unfriendly. It takes about 100 times more energy to generate a sapphire crystal than it does glass. It transmits less light, which it means either dimmer devices or shorter battery life. It continues to break.”

Apple and GT Advanced Technologies appear to be pressing forward with sapphire manufacturing despite the criticism.