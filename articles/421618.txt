Pierce Brosnan‘s spy thriller “November Man” is getting the jump on the Labor Day weekend at the box office with its Wednesday opening.

Espionage action films are familiar turf for Brosnan, who played the iconic 007 in the James Bond movies “Tomorrow Never Dies,” “The World Is Not Enough” and “Die Another Day” more than a decade ago

In the Relativity Media-distributed “November Man,” he plays former CIA agent Peter Devereaux, who is lured out of retirement for a final mission to protect a key witness (former Bond girl Olga Kurylenko) in a case involving Russian war crimes. That puts him at odds with the Kremlin and his former CIA protege, played by Luke Bracey, and the fun — as in gunfights, car chases and explosions — begins.

Watch video: Pierce Brosnan Shoots Guns, Blows Things Up and Saves the Girl in ‘The November Man’ Trailer

Mature moviegoers are the target audience for the thriller, for which Michael Finch and Karl Gajdusek wrote the screenplay. It’s based on Bill Granger’s 1997 novel “There are No Spies,” from his bestselling “November Man” book series.

Analysts and Relativity are projecting “November Man” will take in around $12 million over the long weekend that ends the summer movie season. There is only one other film debuting, the Universal horror thriller “As Above, So Below,” which opens Friday.

“I think they’d love to catch some of that Liam Neeson fire,” said BoxOffice.com vice-president and senior analyst Phil Contrino, referencing the star who found a new niche as action star after turning 60 in films including the “Taken” movies and “Non-Stop.”

Also read: Pierce Brosnan Gets ‘Horrible Feeling’ Watching Himself as James Bond

“November Man,” a $3 million acquisition, is a good fit with Relativity’s low-risk strategy. The studio has already commissioned a sequel which it characterized as “in early development.” It’s a production of Brosnan’s Irish Dreamtime and Das Films, in association with The Solution Entertainment Group, Palmstar Media Capital and Merced Media Partners. The Solution is overseeing foreign distribution.

Brosnan is an executive producer on “November Man,” which is directed by veteran Roger Donaldson (“The Bank Job”), with whom he teamed on “Dante’s Peak” in 1997. Beau St. Clair and Siriam Das produced.

“3 Days to Kill,” a spy thriller starring Kevin Costner that Relativity released in February, offers a good comparison for “November Man.” That film opened to $12.2 million and topped out at $30 million domestically, and added another $12 million from overseas.

Watch video: Jimmy Fallon Schools Pierce Brosnan in Classic ‘Goldeneye’ Video Game

Movies that target older audiences rarely burn it up on social media; the numbers for “November Man” are ahead of those for “3 Days” on Facebook but trail it on Twitter. It was third in advance sales at online ticket broker Fandango on Tuesday, behind “Guardians of the Galaxy” and “Teenage Mutant Ninja Turtles.”

‘November Man” will be in 2,750 theaters starting at midnight Tuesday.