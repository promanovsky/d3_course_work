Shutterstock photo

Investing.com -

Investing.com - Gold and silver futures edged higher on Thursday, as investors looked ahead to key U.S. economic data later in the day for further indications on the strength of the economy and the future course of monetary policy.

On the Comex division of the New York Mercantile Exchange, gold for August delivery tacked on 0.28%, or $3.70, to trade at $1,303.50 a troy ounce during U.S. morning hours.

Prices held in a tight range between $1,298.20 and $1,308.80. Gold ended Wednesday's session up 0.21%, or $2.70, to settle at $1,299.80 an ounce.

Futures were likely to find support at $1,292.60, the low from July 15 and resistance at $1,314.40, the high from July 15.

Also on the Comex, silver for September delivery inched up 0.3%, or 6.2 cents, to trade at $20.83 a troy ounce. Prices fell to $20.63 on Wednesday, the weakest level since June 20.

The U.S. was to publish reports on initial jobless claims, housing starts, building permits, and the Philly Fed manufacturing index later in the day.

Gold and silver prices have been under heavy selling pressure in recent sessions amid speculation that the Federal Reserve could hike U.S. interest rates sooner than expected.

Fed Chair Janet Yellen said earlier in the week that the central bank could start raising interest rates sooner than expected if the U.S. labor market continues to improve more quickly than anticipated.

However, the Fed chair also said that if the recovery was disappointing monetary policy would remain accommodative.

Meanwhile, sentiment was cautious as the U.S. and the European Union announced on Wednesday a fresh round of sanctions against Russia, following the annexation of Crimea in April and ongoing tensions in the rest of Ukraine. The U.S. package was the largest round of penalties so far.

In response to the sanctions, Russian President Vladimir Putin said that relations with the U.S. are in danger of reaching a "dead end" and could damage U.S. business interests in his country.

Elsewhere in metals trading, copper for September delivery dipped 0.15%, or 0.5 cents, to trade at $3.210 a pound, as jitters over a possible bond default in China's construction sector weighed.

Northern Shanxi-based construction firm Huatong Road & Bridge Group warned on Wednesday that it may default on a 400 million yuan bond set to mature on July 23, triggering concerns over the near-term demand outlook in China.

Concerns over domestic bond defaults stoked investor worries that financing deals, which have locked up vast quantities of copper could unravel.

A cooler property sector not only weighs on demand for copper as construction material, but also dampens consumption from the home appliances sector.

China is the world's largest copper consumer, accounting for almost 40% of world consumption last year.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.