Cars are reflected in a Nissan Motor logo at the company's dealership in Tokyo Thomson Reuters

YOKOHAMA Japan (Reuters) - Nissan Motor Co posted a 13.4 percent rise in April-June operating profit from a year earlier as Japan's second-biggest car maker saw improved sales in the United States after it changed senior management overseeing operations there.

Nissan said on Monday that its first-quarter operating profit was 122.6 billion yen ($1.20 billion), exceeding the 109.1 billion yen mean estimate of 12 analysts polled by Thomson Reuters I/B/E/S.

It stuck with an annual operating profit forecast of 535 billion yen for the financial year ending in March 2015.

Last financial year, Nissan posted a 4.8 percent operating profit margin, the worst among its Japanese peers, squeezed by the cost of a rapid expansion drive aimed at lifting its global market share.

($1 = 101.8700 Japanese Yen)

(Reporting by Yoko Kubota; Editing by Kenneth Maxwell)