Bachelor nation is mourning. Bachelorette Andi Dorfman is finished filming season 10 of the ABC reality TV show, but the death of one of her suitors, Eric Hill, remains "devastating."

"It had a huge effect on all of us," Dorfman, 26, told Us Weekly in a conference call on Wednesday, May 14. "We heard the news and it was just crushing." (As show host Chris Harrison revealed last month, producers broke away from traditional Bachelor franchise protocol to isolate the stars from the outside world, and opted to tell contestants about Hill's April 20 death. "We never would have kept it from them," Harrison told ABC News on April 30.)

Of the very moment when she and the contestants learned of Hill's death, Dorfman recalled: "You can imagine how much shock and devastation everyone felt. It's something we never expected, it's something we never predicted, and it's something that was really hard on us."

"I don't want to talk too much about what our initial reactions were," Dorfman continued. "I'd rather spend the time just talking about who Eric was. This is a guy who really lived life to the fullest and inspired all of us."

Indeed, before joining The Bachelorette, Hill had been on a quest to visit every country in the world in record time. The travel-loving daredevil documented his journey on his website, GO With Eric. "I remember wanting to see every country in the world since I was in kindergarten," Hill, 30, wrote on his blog.

Hill and Dorfman hit it off right away, and he was even given the coveted first one-on-one date with her. Ultimately, he was sent home, but he made a lasting impression on the ABC franchise's cast and crew. "He became a part of our family," Dorfman said of Hill. "We lost a family member." Hill's death "was hard on everybody," she concluded.

The Atlanta-based assistant district attorney added, "I think we're holding onto the legacy here. He really did live life to the fullest and that’s something that every single person, cast, crew, production, myself included, will take away from Eric and this experience with him."

Dorfman's season of The Bachelorette will air beginning May 19. Her suitors include a former pro basketball player, a "pantsapreneur," and a farmer from Iowa.