Paul McCartney, as it turns out, was quite under the weather.

The Inquisitr reported back in May that the former Beatle cancelled some shows in Japan due to a virus; he later cancelled the remainder of his Japan tour with plans to resume shows July 5 in the U.S. – an intention he kept Saturday in Albany.

At the time of the cancellation, McCartney said he was merely following doctor’s advice, stating that he was going to “take it easy for just a few more days,” as quoted by Rolling Stone. The publication also stated that people within the industry were “concerned” about the musician’s health. McCartney turned 72 on June 18.

Bert Holman, manager of the Allman Brothers Band, told Rolling Stone the tours can be grueling on rock musicians who have years of experience – the Allman Brothers also had to cancel shows this year because of Gregg Allman’s bronchitis. Said Holman:

“Paul puts on a very strenuous show — he’s up there two and a half hours singing his heart out and playing his ass off. You do that every day, it starts to get really grueling.”

But more to the point – rock stars who put it all into their performance want to take preventive measures to make sure they don’t get sick. Grateful Dead manager Tim Jorstad told Rolling Stone: “These aging rock & rollers are more susceptible to health issues. When they’re on the road, these guys are very, very focused on staying healthy, and isolating themselves from anybody who is sick.”

It goes without saying, therefore, that McCartney would want to rest up if afflicted with even the hint of an infection.

Saturday’s show was just under three hours in length and featured 38 songs – hardly a mediocre effort on McCartney’s part. The Hollywood Reporter recounts that McCartney had only briefly been hospitalized in Tokyo in May and that his last performance was May 1 in Costa Rica. The “brief stay in hospital” was six days, according to the BBC.

In addition to a mix of Beatles classics and songs from his solo career, McCartney performed what he said was his first public rendering of “On My Way to Work.” McCartney’s wife Nancy was in the audience at Albany, and McCartney also paid tribune to late band mates John Lennon and George Harrison, as well as guitarist Jimi Hendrix – the latter by including an instrumental interlude of “Purple Haze.”

The next stop on McCartney’s “Out There” tour is scheduled for Monday in Pittsburgh.

[Image: Google]