For almost 35 years, astrophysicist Alan Guth has been studying the cosmos in search for evidence of long lost, leftover particles from the Big Bang.

However, Guth didn't find the particles he was looking for but instead theorized that the universe swelled faster than the speed of light upon the Bang, according to the New York Times.

What he theorized in 1979 would be the essentials needed for a team of astrophysicists at the Harvard-Smithsonian Center for Astrophysics to discover three decades later.

On Monday, astronomer John M. Kovac and his team announced they had detected ripples, or gravitational waves, in the fabric of space-time, an accomplishment that further supports the idea of a universe being stretched apart when it was only about a trillionth of a trillionth of a trillionth of a second old -- 10 to the minus 35 seconds.

The discovery helps explain a few paradoxes in the 13.8 billion-year-old universe including how the sky is not jagged or warped but uniform from pole to pole. Essentially the engorgement, like a balloon, irons out the irregularities.

"(It's) a direct image of gravitational waves across the entire sky, showing us the early universe," Kovac said.

So the particles Guth were looking for aren't gone, they've just been thinned out. Guth said he didn't think the discovery would ever happen in his lifetime and that he was "bowled over" with the news.

"With nature, you have to be lucky," Guth said. "Apparently we have been lucky."

Jamie Bock, a Caltech physics professor, laboratory senior research scientist at Jet Propulsion Laboratory and the project's co-leader, said that their discovery is a glimpse into the past.

"The implications for this detection stagger the mind," Bock said. "We are measuring a signal that comes from the dawn of time."

The team aimed a BICEP2 telescope -- Background Imaging of Cosmic Extragalactic Polarization -- at the South Pole, observing a faint glow left over from the Big Bang known as the cosmic microwave background.

The observations took place over the course of three years as Kovac and his team kept all detail close to the chest by not emailing any information but rather having Kovac hand deliver all drafts to the select few of the scientific community involved.

If the groundbreaking discovery of inflation proves correct, then the 14-billion light-years in space that we can see from earth is merely an minuscule fraction of the entire universe.

Kovac and his fellow contributors' findings is the first of its kind in proving that the universe is expanding as well as providing insight into famed physicist Albert Einstein's theory of general relativity.

Kovac insists that the chances of the discovery being a fluke are one in 3.5 million.

"It was a very special moment, and one that we took very seriously as scientists," Kovac said.