The phablet sized iPhone 6 is rumored to be a little pricier then the normal one, with an expected launch a few weeks after the announcement of the 4.7-inch version — some say both will launch at the same time in September.

The larger iPhone 6 is rumored to be $100 expensive than the normal one, but it seems consumers are willing to pay a higher price for a bigger screen. Raymond James analyst Tavis McCourt mentioned in a research note says that 5.5-inch iPhone 6 may cost $100 higher, but consumers may be willing to pay an extra $100 for a larger display.

“Apple will likely charge a $100 premium for the 5.5 inch version that media reports have suggested will be available a few months after the 4.7 inch version,” Raymond James analyst Tavis McCourt told clients in a note. He further added, “Our June consumer survey points to continued growth in the willingness of iPhone users to pay $100 more for a bigger screened iPhone, with now a full one-third of survey respondents willing to pay a $100 premium.”

In fact, he added, “data seems to suggest meaningful demand for a larger screen, which should logically mean the iPhone 6-cycle will be strong for upgrade sales, which combined with modest contribution from wearables should cause a modest acceleration in revenue growth in fiscal 2015.”

Now, this is interesting. I’m not sure if Apple will increase the price of the alleged phablet, but several rumors have pointed towards a similar information — a higher price tag.

Apple iPhone 6 is rumroed to launch in September with A8 processor, a better camera, bigger display and iOS 8 as its operating system, with health-related features on board.

We’ll update you as soon as we get any more information.

Source: Market Watch, Macrumors

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more