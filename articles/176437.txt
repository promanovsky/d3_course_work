Target

Target CEO, President, and Chairman Gregg Steinhafel is officially out at the mega-retailer.

Target on Monday announced that Steinhafel, who spent 35 years with the company, has resigned effective immediately. John Mulligan, the retail chain's chief financial officer, will serve as interim president and CEO until a permanent replacement is chosen.

Steinhafel resignation is perhaps no surprise. Last year, Target was hit by a massive data breach that left the personal information of its customers open to hackers. It's believed that more than 110 million people were affected in the breach -- everything from names, addresses, and even credit card information was stolen.

Security experts criticized the retailer for the massive breach, saying the malware hackers used to access Target's point-of-sale terminals could have -- and should have -- been detected long before it was. Still, the company says it responded seriously to the incident and is providing free credit monitoring and insurance for those who have been affected by the breach.

In the board's statement, it noted that Steinhafel "held himself personally accountable" for the data breach and said he would work tirelessly to fix the problem. In his own resignation letter, Steinhafel stopped short of taking full responsibility for the breach, but did say that now that the company has taken "a number of steps to further enhance data security, putting the right people, processes, and systems in place."

"With several key milestones behind us, now is the right time for new leadership at Target," Steinhafel wrote in his letter to the board.

Steinhafel also resigned from Target's board and is being replaced by Roxanne S. Austin, a current board member who will serve as non-executive chair until the company chooses a permanent replacement.

Steinhafel's resignation comes just days after the company hired Bob DeRodes as its new chief information officer. DeRodes, who coincidentally (or perhaps not) starts on Monday, will be tasked with improving Target's security and leading the compan's long-term digital strategy.

DeRodes was a strategically important hire for the embattled retailer. He has a respectable pedigree in the security community, acting in a technology consulting capacity in a wide range of US government agencies, including the Department of Homeland Security and the Department of Defense. He has also provided advisory services to a number of major companies.

Target's board is now tasked with finding a permanent replacement for CEO. The company has retained executive search firm Korn Ferry to help it find a suitable replacement.

CNET has contacted Target for additional comment on the news. We will update this story when we have more information.

Target's shares are down $1.04, or 1.68 percent, to $60.97 following the resignation announcement.