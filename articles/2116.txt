Reforms vindicate leaks: Snowden

Share this article: Share Tweet Share Share Share Email Share

Austin, Texas - Former security contractor Edward Snowden, facing arrest if he steps foot on US soil, said the changes the American government and private companies have made about managing massive amounts of data helped vindicate his leaks of classified material. Speaking to a sympathetic and tech-savvy crowd at the South by Southwest conference in Austin, Texas, via a video link from Russia, Snowden added that the US government still has no idea what material he has provided to journalists. “I saw that the Constitution was violated on a massive scale,” Snowden said to applause, adding that his revelations of government spying on private communications have resulted in protections that have benefited the public and global society. Last year, Snowden, who had been working at a National Security Agency facility as an employee of Booz Allen Hamilton, leaked a raft of secret documents that revealed a vast US government system for monitoring phone and Internet data. The leaks deeply embarrassed the Obama administration, which in January banned US eavesdropping on the leaders of friendly countries and allies and began reining in the sweeping collection of Americans' phone data in a series of limited reforms triggered by Snowden's revelations.

Major companies also tightened up safeguards, but Snowden said that is still not enough to protect privacy properly.

“The government has gone and changed their talking points. They have changed their verbiage away from public interest to national interest,” he said, adding that this poses the risk of losing control of representative democracy.

To many in government and at the National Security Agency, Snowden is a traitor who compromised the security of the United States. But for many at the conference he is a hero who protected privacy and civil liberties.

“To me, Snowden is a patriot who believed that what he did was in the best interests of his country,” said Roeland Stekelenburg, a creative director at the Dutch Internet firm Infostrada.

Snowden fled to Hong Kong and then to Russia, where he currently has asylum. The White House wants him returned to the United States for prosecution.

Reuters