Google's Nest Labs has announced a new developer programme and application programming interface (API) that will allow third-party developers to integrate their smart devices with Nest smart smoke alarms and thermostats.

API partners that have been sanctioned include Logitech, smart LED maker LIFX, fitness band Jawbone, Mercedes and Whirlpool - which will all now carry the "Works with Nest" tag.

Applications developed under these partnerships include Mercedes cars communicating (via a smartphone) with the Nest Learning Thermostat to let it know when the user is on their way home, as well as the Jawbone UP24 signalling when the user is going to wake up. With this information, the thermostat will start to heat up or cool down the home depending on the set preferences.

Apple recently showed its hand in the home automation space with the launch of HomeKit, a new platform which will allow manufacturers of smart devices to integrate more closely with Apple's mobile software.

"The Nest Learning Thermostat and Nest Protect alarm are already helping people to save energy, stay comfortable, and improve home safety - but that's only the beginning," said Matt Rogers, founder and vice president of engineering at Nest.

"Our goal has always been to bring this kind of thoughtfulness to the rest of your home and life - and that's what the Nest Developer Programme is all about. We've worked with iconic brands... to build seamless and practical Works with Nest experiences for your home."

Google bought Nest in January for $3.2 billion (£1.88bn) but it has been operating as a separate entity within Google ever since. It has made clear its plans to expand beyond the smart thermostat and smoke detector products it offers at the moment, and this week's $555m acquisition of DropCam offered an insight into the next product it could launch.

Allowing data sharing with third parties is likely to raise concerns about user privacy, though Rogers claimed in a blogpost at the time of the Google acquisition that the company's privacy policy "clearly limits the use of customer information to providing and improving Nest's products and services."

The announcement comes ahead of Google I/O, the company's annual developer conference kicking off in San Francisco tomorrow where the company is likely to give more of an indication of how Nest fits into the company's overall plan for tackling the home automation market.

As well as details about its home automation plans, Google is expected to announce major updates for Android, the Nexus hardware and Google Glass, as well as the full debut of the company's Android Wear smartwatches.