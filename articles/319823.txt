Controversy -- it still sells records.

Days after refuting comments she’d evidently made to England’s Guardian about wanting to be dead, Lana Del Rey topped the Billboard 200 on Wednesday with her album “Ultraviolence.” The record, Del Rey’s second full-length following 2012’s “Born to Die,” sold 182,000 copies in its first week, the week that ended June 22, according to Nielsen SoundScan -- the singer’s best sales week yet.

As Billboard’s Keith Caulfield writes, Del Rey got to No. 1 without doing the standard round of release-week TV performances.

But audiences in Southern California have had several opportunities to see Del Rey onstage, including at April’s Coachella Valley Music and Arts Festival and at a show last month at the Shrine Expo Hall. The singer has also made a string of widely viewed music videos, a medium that might be her most natural habitat.

Advertisement

Behind Del Rey, the young British soul singer Sam Smith entered Billboard’s chart at No. 2 with his strong debut, “In the Lonely Hour.” That disc sold 166,000 copies, sales that Caulfield attributed in part to appearances on “Late Show With David Letterman” and “Good Morning America.”

Smith also played a concert last week at New York’s Apollo Theater, where Mary J. Blige joined him for a much-discussed performance of Smith’s song “Stay With Me.”

At No. 3, with sales of 110,000 copies, is Linkin Park’s “The Hunting Party,” on which the L.A.-based hard-rock band returns to the aggression of its early albums following several artier, more textured efforts.

Other new records debuting in the top 10 this week include Willie Nelson’s “Band of Brothers,” Jennifer Lopez’s “A.K.A.” and “While(1<2)” by the dance-music star Deadmau5.

Advertisement

Twitter: @mikaelwood