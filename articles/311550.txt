Google have released their wearable technology device, Glass, in the UK.

The device, which is still a prototype at the moment, costs £1,000 and you have to be over 18 to buy it.

Glass lets you film stuff and check the internet on the move, with a built-in camera and microphone to record photos, video and sounds.

Google says Glass is a hands-free, quick alternative to smartphones - but some people are concerned about privacy and that using Glass while driving would be unsafe.

Check out the video to watch Ayshah chatting to tech expert Mar Dixon.