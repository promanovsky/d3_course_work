Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Spreading butter on your bread may be no worse for your heart than using a ‘healthy’ low-fat alternative, according to new research by the British Heart Foundation.

Health experts have typically warned people to cut back on foods containing saturated fat - such as cheese, cream and fatty meat - for nearly four decades.

But now an international study by scientists reveals there is no definite link between saturated fats and heart problems.

And the research, looking at 600,000 people from 18 different countries, found ‘healthy’ polyunsaturated fats like sunflower spreads or olive oil are not necessarily good for you.

The study did find a weak link between heart disease and two types of fat found in palm oil and animal products while a dairy fat called margaric acid actually reduced the risk.

Omega-3 fatty acids found in oily fish were also found to lower the risk of heart disease although supplements appeared to have no benefit.

Lead researcher Dr Rajiv Chowdhury said: “These are interesting results that potentially stimulate new lines of scientific inquiry and encourage careful reappraisal of our current nutritional guidelines.

“Cardiovascular disease, in which the principal manifestation is coronary heart disease, remains the single leading cause of death and disability worldwide.

“In 2008, more than 17 million people died from a cardiovascular cause globally.

“With so many affected by this illness, it is critical to have appropriate prevention guidelines which are informed by the best available scientific evidence.”

The study - co-funded by the British Heart Foundation - looked at data from 72 studies with more than 600,000 people.

But heart experts stressed that the findings of the study did not mean it was safe to tuck into lumps of lard.

Too much saturated fat of the kind found in cakes, biscuits, fatty cuts of meat, cheese and cream may still increase the amount of cholesterol in the blood, which can lead to the risk of developing heart disease.

Guidelines suggest that men should eat no more than 30g a day and women no more than 20g.

Prof Jeremy Pearson, the British Heart Foundation’s associate medical director, said: “This research is not saying that you can eat as much fat as you like.

“Too much fat is bad for you. But, sadly, this analysis suggests there isn’t enough evidence to say that a diet rich in polyunsaturated fats but low in saturated fats reduces the risk of cardiovascular disease.

“Alongside taking any necessary medication, the best way to stay heart healthy is to stop smoking, stay active, and ensure our whole diet is healthy - and this means considering not only the fats in our diet but also our intake of salt, sugar and fruit and vegetables.”