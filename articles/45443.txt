It is safe to say that Facebook’s recent (surprising) announcement that they would be acquiring Oculus VR caught many by surprise. After all Facebook has never really expressed that much interest in gaming, save for the Facebook Games feature, not to mention there has not been any indication that Facebook was even remotely interested in the company to begin with.

Advertising

In fact Facebook’s acquisition of companies like WhatsApp and Instagram made perfect sense since they had to do with communications and social media, but as is the case with most acquisitions, this was not an overnight decision, nor was it an impulsive one either.

According to sources close to the situation, as reported by Re/Code, it seems that Facebook’s CEO, Mark Zuckerberg, began his quest to acquire Oculus VR about three months ago. They had initially valued the company at $1.5 billion, but as we can see in today’s announcement, they bumped it up to $2 billion instead ($400 million in the cash, the rest of it being Facebook shares).

In fact according to the sources, it seems that Oculus’ CEO, Brenda Iribe, left the DICE gaming conference in Las Vegas to personally demo the technology for Zuckerberg, which safe to say left Zuckerberg impressed enough to begin working on a deal to acquire the technology and the company.

Oculus’ investors initially resisted the idea to sell, claiming it was too early to sell and that the technology was still at the forefront. One of the sources claims to be surprised, stating that they had initially thought that of all companies, Google would have been the most interested. However it was over the last weekend that Zuckerberg pushed for the deal to be completed.

Now we have seen some reservation regarding Facebook’s acquisition of Oculus VR, especially by Minecraft’s creator, Markus “Notch” Persson who stated that Mojang would be cancelling development of Minecraft for the Oculus Rift for the device, claiming that Facebook “creeps him out”.

Filed in . Read more about Facebook, Oculus Rift, Virtual Reality (VR) and Wearable Tech.