Amazon.com Inc. is preparing to launch its long-rumored smartphone in the second half of the year, The Wall Street Journal reported on Friday, citing people briefed on the company's plans.

The Internet retailer would jump into a crowded market dominated by Apple Inc. and Samsung Electronics Co. Ltd.

The company has recently been demonstrating versions of the handset to developers in San Francisco and Seattle. It intends to announce the device in June and ship to stores around the end of September, the newspaper cited the unidentified sources as saying.

Amazon has made great strides into the hardware arena as it seeks to boost sales of digital content and puts its online store in front of more users. Amazon recently launched its $99 Fire TV video-streaming box and its Kindle e-readers and Fire tablets already command respectable U.S. market share after just a few years on the market.

Rumors of an Amazon-designed smartphone have circulated for years, though executives have previously played down ambitions to leap into a heavily competitive and increasingly saturated market.

Apple and Samsung, which once accounted for the lion's share of the smartphone market, are struggling to maintain margins as new entrants such as Huawei and Lenovo target the lower-income segment.

To stand out from the crowd, Amazon intends to equip its phones with screens that display three-dimensional images without a need for special glasses, the Journal said.

Amazon officials were not immediately available for comment.