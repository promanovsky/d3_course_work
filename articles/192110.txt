Supporting the global oil complex, Libyan oil exports remain low because rebels refused to cooperate with the new prime minister to reopen major oil ports, and China posted data that showed oil imports rose in April.

U.S. crude oil fell on Thursday after a rally driven by a drop in commercial crude stocks topped out at a key technical level, while Brent fell as the market awaited developments in the Ukraine conflict.

Brent crude , the international benchmark, was down 10 cents near $108 per barrel, after settling $1.07 higher on Wednesday. U.S. crude ended 51 cents lower at $100.26 per barrel. The contract had gained $1.27 in the previous session.

In the U.S., the number of Americans filing new claims for unemployment benefits fell more than expected last week, indicating a stronger labor market that helped lift equities, though U.S. crude shrugged it off. Analysts noted U.S. crude gave up its rally after hitting resistance at the 200-day moving average, a key technical level.

On Wednesday, oil futures rose by more than $1 on both sides of the Atlantic after data from the U.S. Energy Information Administration (EIA) showed an unexpected drop in U.S. inventories last week, although total stocks remained close to record high levels.

Separatist rebels in eastern Ukraine rebuffed Russian President Vladimir Putin's call for them to postpone a referendum on independence, casting a shadow over a possible chance to ease the crisis and rekindling political concerns that have buoyed Brent. Putin's surprise move on Wednesday and his announcement that Russian troops had withdrawn from the border with Ukraine have weighed on Brent, but caution remained in the markets as NATO and the White House said they were still waiting for evidence.

For more information on commodities prices, please click here.