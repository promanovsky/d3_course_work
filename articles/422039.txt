Justin Bieber posted a cuddled-up selfie with Selena Gomez on Aug. 27 — and he hasn’t deleted it! Justin seems to be shouting to the world that he and Selena are back on and so in love!

Justin Bieber, 20, and Selena Gomez, 22, look adorable in a black-and-white photo Justin posted on Instagram. According to a report, the duo flew to Canada together for a top-secret vacation on Aug. 27!

Justin Bieber Posts Cute Selfie With Selena Gomez: Going On Vacation Together

Justin has taken Selena with him to Canada — how sweet! Justin was spotted arriving in Toronto, Canada, on Aug. 27, according to Ocean Up. Selena was spotted getting off the plane with Justin when he landed!

Hours after landing in Canada, Justin posted the black-and-white photo with Selena on Instagram. In the pic, Justin makes his signature puppy-dog eyes and stares right into the camera. Selena leans on Justin, looks away and has her hand up by her neck. They’re not showing any PDA in the pic, but they are very close to each other!

Do you think Justin brought Selena to Canada for a romantic vacation? Maybe they’re going to visit Justin’s family (he’s from Stratford, Ontario)! Whatever the reason for the trip up North, we’re so happy Justin is sharing photos with Sel for the world to see. These two are so cute!

Justin and Selena have been dropping subtle hints that they’re back “on,” but Justin deletes every pic he posts of Selena online! On Aug. 26, JB put up a sexy selfie of Selena while she was prepping to appear on the Chelsea Lately series finale. But within hours of posting the gorgeous pic of Sel, Justin deleted it.

We’re thrilled that Justin hasn’t deleted his pic with Sel in Canada yet! These two have no reason to hide their love from the world! If they’re happy, then we’re happy.

What do YOU think about Justin’s new selfie with Selena in Canada, HollywoodLifers? Do you think it’s sweet Justin took Sel to Canada with him? Let us know!

— Megan Ross

More Justin Bieber & Selena Gomez News: