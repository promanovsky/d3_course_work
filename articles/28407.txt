The conundrum has stumped doctors for years. Why do neurodegenerative diseases such as Alzheimer's affect only the elderly? Why do some people live to be over 100 with normal brain functioning while others develop dementia decades earlier?

Now, a new study by Harvard scientists points to a possible answer, one that could spark further research that -- ultimately -- could lead to new drugs and treatments for dementia.

Researchers have found that a protein active during fetal brain development, called REST, switches back on later in life to protect aging neurons from various stresses, including the toxic effects of abnormal proteins. But in those with Alzheimer's and mild cognitive impairment, the protein -- RE1-Silencing Transcription factor -- is absent from key brain regions.

"Our work raises the possibility that the abnormal protein aggregates associated with Alzheimer's and other neurodegenerative diseases may not be sufficient to cause dementia; you may also need a failure of the brain's stress response system," said Bruce Yankner, Harvard Medical School professor of genetics and leader of the study, in a release.

"If true, this opens up a new area in terms of treatment possibilities for the more than 5 million Americans currently living with Alzheimer's disease," said Yankner, who in the 1990s was the first to demonstrate the toxic effects of amyloid beta, the hallmark abnormal protein in Alzheimer's.

The research, published Wednesday in the journal Nature, underscores a different way of looking at neurodegenerative diseases. Instead of focusing on the negative changes that cause disease, researchers examined trouble spots in the brain's ability to protect itself over time.

Yankner said the study suggests a person may be able to resist the toxic effects of Alzheimer's if REST levels remain high.

"If we could activate this stress-resistance gene network with drugs, it might be possible to intervene in the disease quite early," he added in a release. "Since Alzheimer's strikes late in life, delaying the onset of disease by just a few years could have a very substantial impact."

In separate, but related research, a new study out of Temple University's School of Medicine this week suggests chronic sleep disturbances could speed up the onset of dementia and Alzheimer's disease in older adults.

Yet another recent study found that a simple blood test could detect with 90 percent accuracy whether or not a healthy person will develop Alzheimer's.

As dementia rates rise, researchers believe preventive studies such as this will be critical in finding a cure. Some estimates say the number of people living with dementia will double to nearly 70 million by 2030.