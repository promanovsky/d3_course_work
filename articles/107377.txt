Stephen Colbert has made a career out of playing a bombastic conservative commentator, first on “The Daily Show” and for nearly 10 years on “The Colbert Report.”

But his next move, succeeding David Letterman on CBS’ “The Late Show,” might be the toughest assignment yet. Colbert’s comedy will no longer be guided by his faux personality, but his real one.

EARLIER: Stephen Colbert to Replace David Letterman on ‘Late Show’

“Colbert Report’s” longevity has defied the odds and the skeptics who didn’t think the joke would remain funny week in and week out for an extended run. To the credit of Colbert and his team, they’ve been savvy about keeping it fresh with an eclectic mix of guests, performers, running gags (as elaborate as him running for President) and the occasional out-of-town stand.

The move to “The Late Show” will introduce America to the real Stephen Colbert, who turns 50 next month, for the first time on a TV stage. He has said that he’s tried to keep his actual political leanings out of “Colbert Report,” but glimpses have come through by virtue of the topics he’s tackled.

POLL: Which ‘Daily Show’ Correspondent Should Replace Stephen Colbert?

Colbert has mercilessly skewered and examined the topic of campaign finance reform in ways that undoubtedly enlighten his audience to the influence of Big Money in politics. He expressed support for America’s military in broadcasting four episodes of his show from a military base in Iraq in 2009 in connection with the USO. He testified in Congress in 2010 on the need for greater protections (rather than sanctions) for immigrant farm workers.

On the show, Colbert frequently refers to himself as “America’s foremost Catholic,” and he has featured guests that represent the progressive wing of the church, including activist Sister Simone Campbell.

Friends and colleagues say Colbert off-camera is warm, generous and big-hearted, reflecting his upbringing in Charleston, S.C., where he was the youngest of 11 children. His father, James, was a physician and educator who worked for the Medical U. of South Carolina.

ANALYSIS: With Stephen Colbert, CBS Makes the Perfect Choice

Colbert also has a natural empathy borne out of the tragedy that hit when he was 10. On Sept. 11, 1974, his father and two of his brothers were killed in a plane crash. By many accounts, the blow made Colbert an introvert who retreated into books, notably the fantasy novels of J.R.R. Tolkien. Colbert has shown off his mastery of the Tolkien canon on his show — so much so that he earned a cameo in Peter Jackson’s “The Hobbit: The Desolation of Smaug.”

At the time of his father and brothers’ deaths, Colbert’s sister Elizabeth was 19 and a student at the U. of South Carolina, but she returned home to help their mother, Lorna, and her siblings. The closeness between Elizabeth and Stephen was demonstrated decades later on “Colbert Report” when he enthused about his pride in her decision to run for Congress (as a Democrat) in South Carolina last year.

Stephen Colbert’s interest in acting was evident in high school when he began appearing in school plays. Colbert enrolled in the Hampden-Sydney College of Virginia with the intention of majoring in philosophy. But he soon transferred to Northwestern U. to major in theater, graduating in 1986, according to Biography.com.

From there, he joined the famed Second City comedy troupe, where he met Amy Sedaris and Paul Dinello while spending two years on the road. The trio of Colbert, Sedaris and Dinello went on to create the sketch show “Exit 57,” which ran on Comedy Central in 1995-96, and “Strangers With Candy,” a spoof of TV afterschool specials that ran on Comedy Central in 1999-2000 and yielded a Comedy Central-distributed feature in 2005.

Colbert’s primetime debut came as a writer and actor on ABC’s short-lived “The Dana Carvey Show.” That 1996 sketch series was packed with up-and-comers including Colbert’s future fellow “Daily Show” correspondent Steve Carell, and its writing staff included Louis C.K., Robert Smigel and Charlie Kaufman. He also wrote for “Saturday Night Live” and was a voice on “SNL’s recurring animated series “The Ambiguously Gay Duo,” which he co-wrote with Smigel.

Colbert began working for the “The Daily Show” as a writer-producer and appearing as the conservative commentator in 1997. He became so popular as his appearances increased that “The Colbert Report” was born as a “Daily Show” companion half-hour on Oct. 17, 2005.

Colbert, who is married with three children, is known to be friendly with most of his latenight brethren, including Letterman, who indicated as much in a statement on Thursday.

“Stephen has always been a real friend to me,” Letterman said. “I’m very excited for him, and I’m flattered that CBS chose him.”

Colbert of course is also tight with Jon Stewart (whose production company is behind “Colbert Report”) — as is obvious every time the two are together on screen. “The Colbert Report” last year snapped “Daily Show’s” 10-year streak of winning the Emmy for variety show. Backstage as he basked in the win, Colbert was quick to point out that Stewart was still a winner in his capacity as exec producer of “Colbert.”

(Pictured: President Clinton and Stephen Colbert on ‘The Colbert Report’ in 2013)