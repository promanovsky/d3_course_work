Google says it will stop trying to sell adverts based on data collected about students via its education products

Google has said it will no longer try to sell advertising based on personal information collected about students using products made for schools.

The changes revise some of the policies governing the tech giant's Apps For Education products.

Among other things, Google says it will no longer scan the text of Gmails sent through Apps For Education for clues about students' interests.

The scanning would give the company a better idea about what adverts to show them.

Google is also removing an option that allowed school administrators to show Gmail ads when students were using Apps For Education.

The California company had been automatically blocking the adverts unless an administrator changed the controls.

More than 30 million students, teachers and administrators use the Apps For Education products.