Barclays led the prey to the predators.

That’s the heart of the fraud charge New York Attorney General Eric Schneiderman made against the British on Wednesday over its dark pool, claiming Barclays deliberately lied to investors about how many predatory high-frequency traders used the platform.

“There was no protection for any ordinary investor in Barclays’ dark pool,” Schneiderman said at a press conference where he announced the charges.

“Barclays grew its dark pool by telling investors they were diving into safe waters, when in fact it was full of predators who were there at Barclays’ invitation,” he added.

The AG is seeking “a lot” in injunctive and monetary damages, he said, though he didn’t say how much.

Barclays misled investors in order to try to capture a greater share of stock market trading, the AG said.

“This is the very first fraud-related lawsuit against dark pools and will likely be the first of many,” said Andrew Stoltmann, a securities lawyer.

Known as LX, Barclays’ dark pool is the second-largest by volume, next to Credit Suisse’s platform, according to data from Finra.

Barclays also lied about how much volume it directed to its own platform in order to collect more on fees, Schneiderman alleged.

One LX honcho tried to get an underling to change a presentation that said that Barclays directed 75 percent of its traffic to the LX platform — and when the employee refused to change it to 35 percent, he was fired, Schneiderman claimed.

“I had always liked the idea that we were being transparent, but happy to take liberties if we can all agree,” one vice president of sales said in an e-mail, according to the AG’s office.

Mark Lane, a Barclays spokesman, said in a statement, “We take these allegations very seriously. Barclays has been cooperating with the New York Attorney General and the SEC and has been examining this matter internally. The integrity of the markets is a top priority of Barclays.”