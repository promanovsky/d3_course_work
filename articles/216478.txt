The Centers for Disease Control and Prevention have identified an Illinois man who appears to be the first home-grown case of MERS in the United States.

The unidentified man does not have an active case of Middle East respiratory syndrome, CDC officials said Saturday. But blood tests show that his immune system has made antibodies to fight the MERS coronavirus -- an indication that he has been infected.

Unlike the two U.S. residents who have been diagnosed with MERS -- one in Indiana and one in Florida -- the Illinois man has not traveled to the Middle East, where the virus first appeared in 2012. However, he met with the Indiana patient twice and had close contact with him during those visits, according to the CDC.

Dr. David Swerdlow, the epidemiologist in charge of the CDC’s MERS response, told reporters that the two men were conducting business meetings, during which they shook hands. The first of the two meetings, on April 25, had them sitting face-to-face about six feet apart for more than half an hour. The second meeting, on April 26, was shorter.

Advertisement

The Illinois man was never sick enough to seek out medical care. Health officials tested him as part of their investigation into all the known contacts of the Indiana patient, a healthcare worker who returned from Saudi Arabia four days before he was admitted to Community Hospital in Munster, Ind., on April 28.

Health officials will now reach out to people who have been in close contact with the Illinois man to “notify, test, and monitor” them, according to the CDC.

“It’s possible that as the investigation continues others may also test positive for MERS-CoV infection but not get sick,” Swerdlow said in a statement.

Local health officials have been keeping tabs on the Illinois man since May 3. His blood test results showing MERS antibodies were reported to the CDC on Friday night.

Advertisement

As of Saturday, the Illinois man is “feeling well,” the CDC says.

So is the Indiana patient, who was identified as the first case of MERS in the U.S. on May 2. He has been discharged from the Munster hospital and is “fully recovered,” according to the CDC.

The Florida patient is also a healthcare worker who traveled from Saudi Arabia to Orlando to visit relatives there. He began to experience flu-like symptoms on a plane from Jidda to London on May 1. He was admitted to a hospital on May 8, where he is currently “isolated” and “doing well,” the CDC says.

There is no link between the Indiana and Florida patients.

Advertisement

DNA evidence indicates that the virus that causes MERS originated in camels before mutating in a way that allowed it to spread to humans. So far, it does not appear to spread easily among people -- cases of spread involve close contact, such as between patients and their healthcare providers.

But health officials are worried about the virus because people do not have any natural defense against it, and it has been fatal in about 30% of known cases. As of Thursday, the World Health Organization has announced 572 laboratory-confirmed cases of MERS, including 173 deaths.

Symptoms of the respiratory infection may include shortness of breath, coughing and fever. The CDC urges people to protect themselves by taking routine precautions such as washing their hands, disinfecting germy surfaces and avoiding people who are sick. There is no vaccine or specific treatment for MERS.

At this time, there is no need for Americans wishing to vist the Arabian Peninsula to cancel their plans, the CDC says. Travelers should monitor their health while overseas and once they return, according to the agency.

Advertisement

“This latest development does not change CDC’s current recommendations to prevent the spread of MERS,” Swerdlow said. “If new information is learned that requires us to change our prevention recommendations, we can do so.”