IMF sees $100bn Russian capital outflow

Share this article: Share Tweet Share Share Share Email Share

Moscow - The International Monetary Fund has cut its 2014 growth forecast for Russia and expects capital outflow of $100 billion this year, the Fund's mission chief to Moscow said on Wednesday. Antonio Spilimbergo also told reporters that international sanctions imposed on Moscow over the crisis in Ukraine, and the threat of more to come, were hurting the economy and threatened investment. He said the IMF expected Russia to grow just 0.2 percent this year, compared with an earlier forecast of 1.3 percent, and added there were “considerable downside risks”. “The difficult situation and especially the uncertainty surrounding the geopolitical situation and follow up of sanctions and escalation of sanctions are weighing very negatively on the investment climate,” Spilimbergo said. He added that Russia, which grew 1.3 percent last year, was already experiencing recession.

“If you understand by recession two quarters of negative economic growth, then Russia is experiencing recession now,” Spilimbergo told reporters.

The economy contracted quarter-on-quarter in the first three months of this year and Spilimbergo's comments made clear he expected further contraction.

He said the IMF forecast was for 1 percent growth in 2015.

The Fund's projection for capital outflows was in line with the Russian government's own forecast.

The central bank has said nearly $64 billion left Russia in the first quarter of 2014.

Spilimbergo said the central bank's decision to raise interest rates last week would reduce inflation but would not be enough.

The IMF said depreciation in the rouble would put pressure on inflation but forecast consumer prices would rise more than 6 percent in 2014.

Spilimbergo said Russia needed to ensure macroeconomic stability to offset the impact of geopolitical tensions.

He said the country also needed a flexible exchange rate and tighter fiscal policy to overcome economic hurdles, but the government had been right to stick with a budget rule limiting government spending.

Then Ukraine crisis has pushed relations between Russia and the West to their lowest since the end of the Cold War and has led to the imposition of sanctions on some Russian individuals and companies close to Russian President Vladimir Putin. - Reuters