Immortality's not what it used to be.

Just ask Paul Bettany, the British-born Brooklynite who stars alongside Johnny Depp and Rebecca Hall in the sci-fi computer thriller "Transcendence." Bettany has played both ends of a scientific spectrum -- Charles Darwin in 2009's "Creation," and now a neuroscientist mapping the human brain. ("It must be the accent," he said. "They don't know that beneath the accent beats the heart of a total moron.")

But he's a diligent actor. "And being a diligent actor, I went to see a man at Cal-Tech," Bettany said. "His name was Christof Koch. I sat down with him and said, 'You read the script?' He said, 'Yes, I read the script.' And I said, 'Well, I was wondering, how far-fetched is it?' He said, 'Thirty years.' I said, 'Excuse me?' He said, 'Thirty years.' I said, 'But you are talking about uploading the brain to a computer, its emotion, its memories, its knowledge. We're talking about a kind of immortality.' He said, 'Yes.'

"These people aren't kooks," Bettany said. (Professor Koch has since left Cal-Tech to become the chief scientific officer at the Allen Institute for Brain Science in Seattle.) "The foremost neuroscientists in the world think our brains are inextricably linked to machines."

Anyone who's tried to circumnavigate a texter in the supermarket knows that. But in "Transcendence," which opens Friday, it's the brain of Johnny Depp that's uploaded to the Internet, after the body of his character -- Will Caster, the world's foremost researcher into artificial intelligence -- becomes the victim of an assassin's radioactive bullet. His wife and fellow scientist Evelyn (Hall), desperate to somehow save him, enters Will's self into his A.I. prototype computer, and then onto the Internet -- where he has access to everything, and begins a seemingly malevolent process of global domination.

The film marks the directorial debut of famed cinematographer Wally Pfister ("The Dark Knight"), who has spent the past several years working almost exclusively with director Christopher Nolan ("The Dark Knight Rises," "Inception") and with "Transcendence" has begun a new career, with about as complicated a story as might be imaginable.

"I know, what a moron," Pfister laughed. "I had my eyes on 'My Dinner With Andre II,' but nobody wanted to make that.

Sign up for Newsday's Entertainment newsletter Get the latest on celebs, TV and more. By clicking Sign up, you agree to our privacy policy.

"It's weird," he said, noting that it was the core idea of "Transcendence" that got him to finally change careers. "What I found was, you don't know what's going to attract you until it's there, and then you just have to decide whether you're going to be able to conquer it or not."

FILM, NOT DIGITAL

Pfister's effort to get Jack Paglen's script on-screen involved a mix of old and new: He is an avowed champion of film, so there's no digital photography in the movie, despite all its futuristic/ fantastical effects. "It's all shot on anamorphic 35 mm, which is still the best capture medium. It has higher image quality than anything digital."

When it came to his actors, however, he went rogue. Once the character of Will is uploaded, he's only seen -- with one small exception -- as a kind of hologram emanating from the computer, and it's this way that he communicates with Evelyn. One could imagine Depp being off at a Club Med, delivering his performance by Skype. Not so.

"Here's the thing," said Hall, perhaps the most rapidly rising of young female stars. "I was terrified. Evelyn is a great character, she carries a lot of the film's emotional weight, she makes emotionally questionable choices. But I'm not so good with reading to a script person inside a green box" and imagining her co-star beside her.

JOHNNY ON THE SET

Pfister wasn't into that, either, she said. "So what we ended up doing was having Johnny in a small room, or usually a tent adjacent to the set, where he would sit with a TV beaming him everything we did, and I had an earpiece on, so I had Johnny in my head the whole time." Their interactions were photographed in real time.

The actress said she'd not heard of anyone doing that before. But at any rate, Depp was always there. "He was not in the Bahamas," she laughed. "I can testify to that."

What Hall agreed to was that "Transcendence," with its anti-technology terrorists, artificial-intelligence pioneers and brains on the Internet and -- most important -- impossibly romantic romance, was a crazy story. "It's a crazy story," she agreed. "And it's supposed to be. And I like that it was this gripping kind of conventional action narrative that was actually unconventional, with a very emotional center.... People will probably go crazy for it. Or be a little bit scared."

-----

Depp in other disguises

One of the complaints you hear about Johnny Depp is that he seldom appears on-screen as the person we recognize as Johnny Depp. Over the past decade, his career has been dominated by disguises, many of them the manifestations of Tim Burton's curious mind, such as Willie Wonka, The Mad Hatter and Barnabas Collins ("Dark Shadows"). He has spent very much time under bandannas and mascara (Jack Sparrow, of the innumerable "Pirates of the Caribbean" films), rode out of the West with a dead crow on his head (as Tonto in "The Lone Ranger," 2013) and outraged a few audiences as the bewigged and syphilitic Earl of Rochester (in 2004's "The Libertine"). So, even though its star spends much of his time inside a computer, "Transcendence" is one of those rare Depp movies in which the hero looks like Depp. Here are some others.

THE RUM DIARIES (2011) Probably less significant as cinema than as the meeting point for Depp and current fiancee Amber Heard, Bruce Robinson's too-deliberately gonzo comedy is based on the work of the late Hunter S. Thompson, who probably would have suggested that everyone start drinking heavily.

THE TOURIST (2010) Crash of the Titans: Depp and Angelina Jolie starred in this stillborn thriller from director Florian Henckel von Donnersmarck ("The Lives of Others"), a movie most memorable for having been nominated for all those Golden Globes as a "comedy/musical."

PUBLIC ENEMIES (2009) Michael Mann's underrated biopic about John Dillinger, the Prohibition-era gangster/ bank robber, has an astonishing supporting cast.

FINDING NEVERLAND (2004) Depp got to dress up as an Edwardian Englishman, but his presentation was pretty straightforward when he played J.M. Barrie opposite Kate Winslet, as the mother of the boys who inspired Barrie to write "Peter Pan.