Cloned embryos yield stem cells for diabetes

Share this article: Share Tweet Share Share Share Email Share

New York - And now there are three: in the wake of announcements from laboratories in Oregon and California that they had created human embryos by cloning cells of living people, a lab in New York announced on Monday that it had done that and more. In addition to cloning the cells of a woman with diabetes, producing embryos and stem cells that are her perfect genetic matches, scientists got the stem cells to differentiate into cells able to secrete insulin. That raised hopes for realizing a long-held dream of stem cell research, namely, creating patient-specific replacement cells for people with diabetes, Parkinson's disease, heart failure and other devastating conditions. But it also suggested that what the Catholic Church and other right-to-life advocates have long warned of - scientists creating human embryos to order - could be imminent. The trio of successes “increases the likelihood that human embryos will be produced to generate therapy for a specific individual,” said bioethicist Insoo Hyun of Case Western Reserve University School of Medicine in Cleveland. And “the creation of more human embryos for scientific experiments is certain.” The accelerating progress in embryonic stem cell research began last May. Scientists, led by Shoukhrat Mitalipov of Oregon Health & Science University, reported they had created healthy, early-stage human embryos - hollow balls of about 150 cells - by fusing ova with cells from a fetus, in one experiment, and an infant in another.

Earlier this month, scientists at the CHA Stem Cell Institute in Seoul, South Korea, announced they had managed the same feat with skin cells from two adult men.

In each case, scientists used a version of the technique that created the sheep Dolly in 1996, the first clone of an adult mammal. Called somatic cell nuclear transfer (SCNT), the recipe calls for removing the nuclear DNA from an ovum, fusing it with a cell from a living person, and stimulating each ovum to begin dividing and multiplying. The resulting embryo includes stem cells that can differentiate into any human cell type.

While that sounds simple enough, immense technical hurdles kept scientists from achieving human SCNT over more than a decade of attempts. Now that they have a reliable recipe, including the right nutrients to sustain the eggs and the right timing to start it dividing, they have “a reproducible, reliable way to create patient-specific stem cells” via cloning, said Dr. Robert Lanza, chief scientific officer of Advanced Cell Technology and co-author of the CHA paper.

In the latest study, published online in Nature, scientists led by Dieter Egli of the privately-funded New York Stem Cell Foundation Research Institute derived insulin-making “beta cells” from the embryos they cloned from a 32-year-old with type-1 diabetes, an autoimmune disease in which the immune system attacks beta cells. Beta cells do not function in that incurable form of diabetes, often known as juvenile diabetes, which is treated with insulin.

The beta cells produce as much insulin as those in a healthy human pancreas, Egli said. When transplanted into lab mice, the cells functioned normally, making insulin in response to glucose.

Egli has no plans to transplant stem-cell-derived beta cells into patients with type 1-diabetes, in large part because the new cells will meet the same fate as the patient's native beta cells.

For one thing, in type-1 diabetes the immune system chews up beta cells, so the same fate could await cells transplanted into a diabetic, Egli said.

One of the most important uses of the newly created beta cells will therefore be for research, not therapy, said biologist Douglas Melton of the Harvard Stem Cell Institute, who was not involved in the study.

A key research imperative, experts said, is animal experiments to compare the therapeutic potential of cloned embryonic stem cells with stem cells made by “reprogramming” adult cells. This technique does not create or require human embryos, and so was hailed as a way to have stem-cell research without the ethical baggage.

It turns out that some cells produced this way self-destruct or die young, however, suggesting that the embryonic kind will be necessary after all.

That has resurrected a debate that flared in 2001, when U.S. President George W. Bush declared that federal funds could not be used to create human embryos for research. Existing stem cell lines, or IVF embryos, were therefore used.

But with the creation of patient-specific stem cells now scientific reality, there will likely be greater scientific interest in creating human embryos for research, returning the moral debate to the front burner.

“They are cloning human embryos and killing them for their cells,” biologist David Prentice of the pro-life Family Research Council said. “There should be much more outrage.” - Reuters