SAN FRANCISCO (MarketWatch) — Gold futures on Monday settled with their biggest monthly gain since February, as traders awaited the market’s next likely catalysts, which will likely come in the form of monthly U.S. employment data and the European Central Bank meeting later this week.

Gold for August delivery US:GCQ4 tacked on $2, or 0.2%, for the session to settle at $1,322 an ounce on the Comex division of the New York Mercantile Exchange.

Prices for the yellow metal, tracking the most-active contracts, saw gains of around 6.1% for the month and 3% for the quarter.

It’s unlikely that traders will take any large bets ahead of the U.S. nonfarm payroll data due out on Thursday, said Naeem Aslam, chief market analyst at AvaTrade. “It is the most important data on earth, and it can cause a big fluctuation for gold and especially given the GDP data print last week.”

Last week, data showed that the U.S. economy contracted by 2.9% in the first quarter, marking the biggest drop since early 2009.

How to consolidate mutual fund holdings

Gold saw little reaction to U.S. reports released Monday showing that Chicago PMI retreated in June and May pending home sales jumped 6.1%.

Over in Europe, inflation in the euro zone remained stable at 0.5% in June, missing forecasts of a 0.6% reading ahead of the ECB’s meeting on Thursday.

The “wild card” for the market is the geopolitical tensions, said Chris Gaffney, senior market strategist at EverBank Wealth Management, who mentioned Iraq, Ukraine and North Korea, in particular. “Any flare up in these areas could quickly lead to another round of ‘safe haven’ buying in the precious metals,” he said.

On Comex Monday, September silver US:SIU4 fell nearly 8 cents, or 0.4%, to settle at $21.06 an ounce following a gain of around 0.6% last week. For the month, prices were up nearly 13% for the month and logged a quarterly gain of 6.6%. Read Commodities Corner: Why silver has been outperforming gold.

October platinum US:PLV4 settled at $1,482.90, climbing $2.60, or 0.2% and September palladium US:PAU4 added 30 cents to $843.15 an ounce. Platinum futures rose 2.1% for the month, and saw a rise of 4.4% for the quarter, while palladium was up roughly 0.8% for the month and up 8.5% for the quarter.

High-grade copper for September delivery US:HGU4 settled at $3.20 a pound, up nearly 4 cents, or 1.1%. Tracking the most-active contracts, prices were up around 2.5% for the month and gained 5.9% for the quarter.

Shares of gold and silver miners turned higher Monday afternoon, with the Philadelphia Gold and Silver Index XAU, +0.31% up 1.2%. Month to date, the index was up nearly 18%, on pace for a quarterly gain of around 10%. Shares of the SPDR Gold Trust GLD, -0.30% tacked on 0.8%, trading 6% higher on the month and 3.3% higher for the quarter.

More must-read stories on MarketWatch:

Corn futures set for 5-month low with stocks up 39% from a year ago

J.P. Morgan says now is a great time to invest in emerging markets

Janet Yellen Georgetown neighbors complain about ‘doughnut bellies’ of security detail