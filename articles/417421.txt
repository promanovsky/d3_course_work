American Airlines and US Airways are pulling flight listings from Orbitz-run websites in a long-running dispute over fees that the travel company charges to list and sell the flights.

Shares of Orbitz Worldwide Inc. fell nearly 5 percent on Tuesday.

American said that it had already dropped its fares from Orbitz and that US Airways listings would be pulled on Monday.

Corporate customers of the airlines will still be able to book travel through Orbitz, but individuals will have to go to the airlines' websites or other travel agents, including online rivals of Orbitz such as Expedia Inc.

American's withdrawal extends to Orbitz's other websites, such as CheapTickets and European-based ebookers.com. However, those sites were still selling seats on American flights that were listed by American's so-called code-sharing partners, including British Airways and Japan Airlines.

Because of that quirk, and the fact that corporate travel will be unaffected, the impact of American's decision is likely to be felt mostly by Orbitzcustomers booking leisure travel within the U.S.

Scott Kirby, president of American Airlines Group Inc., which owns both airlines, said that his company "worked tirelessly with Orbitz to reach a deal with the economics that allow us to keep costs low and compete with low-cost carriers." One such airline, Southwest — which doesn't list on Orbitz either — saves on commissions by selling 80 percent of its tickets on its own website.

Orbitz suggested that American has the most to lose in the latest dispute.

"Our sites offer hundreds of airlines which are eager to capture the revenue American is choosing to forego," Orbitz spokeswoman Marita Hudson Thomas said in a statement, "and we will continue to show our customers a broad range of flight options to thousands of destinations in the U.S. and worldwide."

American has been trying for several years to cut commission spending by selling more tickets directly to consumers. That has led to fights with online travel agencies and intermediaries such as Travelport and Sabre that operate ticket-distribution systems.

American pulled its flights off Orbitz in 2010, but an Illinois court ruled in 2011 that American had to make the listings available through Orbitz. American later sued the travel agency, claiming that it violated antitrust laws by downplaying American flights when consumers searched the site. The companies reached a settlement last year. Terms were not disclosed.

Expedia also stopped showing American flights for a time, although that ban was lifted after the companies agreed on a new contract in 2011.

"It's not like we haven't seen this before," said Rick Seaney, CEO of the travel website FareCompare.com. "This is likely a negotiating ploy" that could signal further negotiations between the companies.

Seaney said American probably won't lose much business on routes and at airports it dominates, such as Dallas and Miami. But it could see some effect on competitive routes such as New York-Los Angeles, he said.

Orbitz, which was created in 2000 by five of the biggest U.S. airlines, now sells more hotel bookings than air travel. In 2008, airline tickets were 39 percent of its revenue and hotels were 27 percent. Last year, hotels accounted for 35 percent and air travel was 29 percent.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

American Airlines' press statement on the Orbitz flight withdrawal is below:

American Airlines has withdrawn its fares from consumer websites powered by Orbitz, effective immediately. American Airlines Group has notified Orbitz it also will withdraw US Airways fares on Sept. 1, 2014. Corporate clients that use Orbitz for Business to book travel are not affected by this change. "We have worked tirelessly with Orbitz to reach a deal with the economics that allow us to keep costs low and compete with low-cost carriers," saidScott Kirby, President – American Airlines. "While our fares are no longer on Orbitz, there are a multitude of other options available for our customers, including brick and mortar agencies, online travel agencies, and our own websites." American expects these changes will have minimal disruptions for its customers. Customers can continue to purchase tickets and all options for travel on American and US Airways through aa.com and usairways.com. American and US Airways fares are also available through reservations agents and other travel agencies. Tickets already purchased through Orbitz websites remain valid for travel, but changes to reservations must be made through each airline's reservations department.

Shares of Orbitz closed down 39 cents, or 4.6 percent, at $8.04 after dropping 8.5 percent earlier in the session. American Airlines shares fell 33 cents to $39.09 on the news Tuesday.