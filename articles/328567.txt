Boy Meets World left the television scene with two famous words from Mr. Feeny, “Class dismissed.” When one story closed its books, another one opened as Girl Meets World is set to make its long-awaited debut tonight on Disney Channel. Ben Savage and Danielle Fishel will make their emphatic return to the sitcom scene as Cory Matthews and Topanga Matthews in New York City with their two kids: Riley and Auggie.

If you remember, Topanga received a scholarship to law school in New York while the couple was finishing up their second year of college at PennBrook University. Ironically enough, Cory is now a middle school teacher with a few familiar faces in his class. His daughter Riley and best friend Maya attend the class with Farkle Minkus, a familiar last name for Cory and Topanga.

Girl Meets World will offer a new-age feel without it being a repeat of Boy Meets World. As the Los Angeles Times put it, Girl Meets World is a “passing of the baton” from its predecessor. Fishel said in an interview with People that it will pick up where Boy Meets World left off.

I had the opportunity to watch the pilot episode of Girl Meets World already and it was simply phenomenal. As a die-hard BMW fan, it contained the proper elements of real-life by combining similar storylines that BMW accommodated.

Riley (Rowan Blanchard) befriended an edgy girl by the name of Maya Hart (Sabrina Carpenter). Without giving too much away, she tried to get Maya to own up to her mistakes and do the right things in life. One of those mistakes was acting up in school.

Sound familiar?

That is just one similarity Girl Meets World has with the prequel. Creators Michael Jacobs and April Kelly both knew it couldn’t be the same show, but with a few quirks, it could capture the same audience it did from 1993 to 2000:

“Disney called and asked for a new ‘Boy Meets World,’ which I would never do. They appreciated that, and asked if there was anything I’d do involving the show. So I thought about it, because they were very passionate about wanting to do it. I took a look around at what was on television, and I saw that there wasn’t anything on that resembled that sort of storytelling.”

Jacobs goes on to say that it would be more interesting if Cory and Topanga had a daughter. He also noted that it’s 2014 and the world is much rougher than it used to be. It would create great television by having the original characters dealing with the new atmosphere in Girl Meets World.

Girl Meets World will have a 21-episode first season. Like the New York Times said, it will not be a guaranteed hit. Many elements must come together in order for a television show to succeed. Fans of Boy Meets World could see the new-age show and swiftly judge.

It will be different, that’s a guarantee.

Girl Meets World debuts at 9:45 pm EST. tonight on Disney Channel. My advice having already watched it: give it a chance and it may surprise. you. Boy Meets World‘s first season wasn’t great, but it turned out to be one of the most successful shows of the 90’s.

[Images via Google and Forbes]