Somehow, despite budget cuts, frivolous questioning by the House Science Committee, and quibbles between the U.S. and Russia over who should have access to the International Space Station, astronauts have managed to capture the cultural imagination, once again.

This year, we've seen science — particularly space science — reenter the cultural landscape in full force. Seth MacFarlane, of Family Guy and Ted fame, produced a reboot of the 1980s cult classic series about astrobiology, Cosmos, which aired on Fox and was well-received by fans and critics alike. Last week, Netflix announced that it will reboot the beloved '90s series Magic School Bus, a cartoon featuring kids learning about science from their kooky teacher, Miss Frizzle. Bill Nye, the Science Guy, while not an astronaut, made himself a hero just for debating creationists. Oh, Sandra Bullock won an Oscar for acting out every horrifying space travel hypothetical you can imagine. Even Star Wars is coming back.

Maybe it's because we're coming off the year of the nerds, but there are a few other reasons that space travel, and space travelers, are having a moment in the U.S.

RELATED: Florida Man Discovers It's Legal to Ride Sharks After Taking a Swim with One

Social Media

As PopSugar pointed out, astronauts are especially adept at social media, probably because their selfies from space always look really cool:

An 'EVA Selfie' of me. Notice the Earth reflected in my visor. pic.twitter.com/ABjOWakyzF — Rick Mastracchio (@AstroRM) April 25, 2014

As do their Earth photos:

Passed over my favorite volcano field again today. pic.twitter.com/loaaPq37C3 — Reid Wiseman (@astro_reid) June 16, 2014

As do their Vines:

Story continues

NASA can also use its Twitter page to direct followers to videos on YouTube they might not have otherwise sought out — like this one, showing ISS astronauts playing zero-gravity soccer ahead of the World Cup:

Astronauts' use of social media allows us a peek into what life is like on the International Space Station, one we rarely got to see in the early days of the program. We get to see the hardware that populates the inner sanctum of the station, learn how astronauts sleep, what they eat, and how they spend their down time. (Watching sporting events is a big part of it.) We even get excited when astronauts aboard the ISS get an espresso machine (ISSpresso, to be accurate) for the first time:

RELATED: Actually, You're Cutting Your Cake Just Fine

Is it weird to be proud of our office espresso machine because it has a cuz n space? ISSpresso http://t.co/UcPH72TLT1 @IgniteWithUs @Lavazza — Anthony Catanese (@thecatanese) June 14, 2014

These platforms let us connect with astronauts in a new way, and witness a little bit of outer space through their eyes.

Corporate Interest

In addition to public interest, space travel finally has some major private money on its side. In an effort to be able to travel autonomously to the ISS (U.S. astronauts currently rely on Russian spaceships to ferry them to and from the space station) NASA is seeking out private companies to build space taxis. Some powerful players are bidding to be NASA's next couriers — including Elon Musk's SpaceX, Boeing and Sierra Nevada.

NASA has also started reaching out to the public in a more modern way. In a cheeky bid for attention from tech-savvy entrepreneurs (or at least would-be entrepreneurs) the agency ran a panel at SXSW inviting citizen scientists to help NASA search for asteroids. Other companies are turning towards space travel as a next business frontier independently from NASA. Google and Virgin Galactic, for example, are reportedly discussing some type of satellite deal, suggesting that large companies are eager for a piece of the space pie.

RELATED: Justice Department Tries One Last Time to Charge Nazi Guard Living in the U.S.

Environmental Fears

It's hard to separate our interest in space exploration from fears that this planet won't be able to comfortably sustain life for much longer. Projects like Mars One, which plans on sending a community of earthlings to colonize Mars and never return to Earth, seem to be capitalizing on this not unreasonable concern.

This article was originally published at http://www.thewire.com/national/2014/06/why-astronauts-are-cool-again/372942/

Read more from The Wire

• Washington's Football Team Just Lost Its Trademark on the Name 'Redskins'