Aidan Leffler, 10, of Bellevue, Wash. He has Duchenne muscular dystrophy and has begun to lose the use of his legs. His mother, Mindy Leffler, is one of many parents who have been pushing FDA to speed up approval of a promising new drug to treat the disease. (Courtesy of Mindy Leffler) (Courtesy of Mindy Leffler/Courtesy of Mindy Leffler)

For much of last fall and winter and into this spring, scientists at the Food and Drug Administration wavered over what to do about a potential new drug to slow the progression of­­ ­Duchenne muscular dystrophy, a devastating disease that overwhelmingly affects boys, leaving most in wheelchairs by their teens and dead in their 20s.

In a clinical trial involving 12 boys, the drug appeared to have halted physical decline. That gave hope to parents desperate for a treatment, but regulators expressed skepticism about the reliability of the study’s results.

Convinced that this drug could succeed where others had failed, the families of patients mounted an aggressive campaign to pressure the FDA. They argued that it was up to the agency to decide whether their sons would be the last generation of boys to die from Duchenne, or the first to survive. They hired a public relations firm and bombarded Facebook and Twitter, posted online YouTube videos of boys benefiting from the drug, rounded up more than 100,000 signatures for an official White House petition, visited lawmakers on Capitol Hill and made their case to top FDA officials in a series of face-to-face meetings.

On Monday, Massachusetts-based Sarepta Therapeutics disclosed that although the FDA reiterated its skepticism regarding data surrounding the drug, eteplirsen, the agency has detailed a potential path forward for the drug and indicated a willingness to consider it for accelerated approval. That news came five months after the FDA told Sarepta that it would be “premature” for the company to seek approval and suggested that Sarepta might first need to conduct a larger trial, which could mean years of delay.

The company, whose stock soared by 40 percent Monday, said it plans to file a new drug application with the agency and will push forward with a larger “confirmatory” study that will allow more boys access to eteplirsen and give regulators more robust data about its safety and effectiveness.

“This provides the opportunity to get the drug approved and in the hands of all the boys who can benefit from it sometime in 2015,” Chris Garabedian, Sarepta’s chief executive, said in an interview. “I think that’s a huge win for the [Duchenne] community and for Sarepta.”

Although the FDA action doesn’t guarantee approval, the development brought relief and joy to many Duchenne families and put eteplirsen a step closer to becoming the first treatment for the disease, which affects about 15,000 boys in the United States.

But difficult questions remain, and they go beyond any one drug:

How does the FDA balance its traditional role of ensuring that drugs that reach the market are both safe and effective with the obvious urgency to make treatments available to children facing a fatal disease? How much weight should the agency give to the voice of parents and patients, who often are willing to shoulder outsize risks? Should the FDA go ahead and approve promising drugs if it isn’t convinced that they will work over the long term, knowing they will cost patients and insurers hundreds of thousands of dollars a year?

“It’s a big dilemma,” said Louis Kunkel, a Harvard geneticist who is credited with discovering the gene behind Duchenne and who has aided in the search for a cure. “I’ve waffled on my opinion of this. If you listen to the families, they make a good argument why this should be available. . . . [But] how well it’s working is an academic argument that hasn’t been settled yet. And I think that’s where the hesitancy of the FDA is. . . . It’s an extremely hard thing.”

For its part, the FDA seems to be walking a careful line with ete­plirsen. Regulators have made clear that they still harbor doubts even as they give Sarepta a chance to make its case without requiring a larger study before initial approval. On Monday, the agency said only that it recognizes “the devastating nature of the disease on patients and their families” and is working with patient groups and pharmaceutical companies to make effective Duchenne drugs available as soon as possible.

For many parents of children with Duchenne, the case seems clear-cut: Eteplirsen is producing results, they say. And though questions remain about exactly why it works, it’s better to go ahead and treat boys who are dying while scientists parse the particulars.

“Kids on this drug are having remarkably different lives than they would have otherwise,” said Mindy Leffler of Bellevue, Wash., who has been pushing for the drug’s approval in hopes of getting it for her 10-year-old son, Aidan, who has begun to lose function in his legs. “When you’re talking about something that has zero side effects, with good potential to work, I don’t understand the need for caution moving forward. . . . Who the hell cares, if the kid is walking when he shouldn’t be?”

Duchenne is caused by a mutation in the gene responsible for producing dystrophin, an essential protein involved in muscle function. Without dystrophin, even routine activities can damage muscles, causing them to die and be replaced by fat and other tissue. Duchenne is typically diagnosed in boys by age 6. The disease’s steady progression leaves many boys immobile by the time they are teens. It eventually affects the heart and respiratory muscles, making breathing difficult.

Eteplirsen uses an approach called exon skipping, intended to bypass the existing mutation and restore a gene’s ability to produce enough dystrophin to transform Duchenne into a milder and more sustainable form of muscular dystrophy known as Becker. Only about 13 percent of Duchenne patients have mutations that could benefit from eteplirsen, but Sarepta is developing similar drugs to treat boys with other exon deletions, along with clinical trials to test them.

In the more than two years since Sarepta’s clinical trial began, the drug appears to have substantially halted the decline for boys in the trial without causing harmful side effects. “It’s unequivocal,” said Jerry Mendell, a pediatric expert at Nationwide Children’s Hospital in Columbus, Ohio, and the lead investigator on the Sarepta trial. “I see it as a game-changer.”

Not everyone faults the FDA for taking so long to decide how to proceed. Eric Hoffman, director of the Center for Genetic Medicine Research at Children’s National Medical Center and a longtime Duchenne researcher, said valid questions remain about eteplirsen’s dystrophin-producing abilities and how effective Sarepta’s drug really is.

“I’ve personally been very impressed with the FDA,” he said. “There’s every indication they are taking this extremely seriously.”

Hoffman said he understands the families’ sense of urgency, and he shares optimism about exon skipping. But he said the public campaign to pressure the FDA has divided the community more than united it.

“It becomes this form of bullying that puts everybody between a rock and hard place,” he said. “Unless you’re supporting kicking out the head of the FDA and granting accelerated approval as quickly as possible, then you’re supporting killing Duchenne kids.”

He said that the reality is more nuanced and that FDA reviewers do want to help patients but are wary about setting a precedent by approving high-priced drugs they aren’t yet convinced will work.

Despite the tensions of the past year, Monday brought cautious optimism for parents such as Jenn McNary of Massachusetts. Her 12-year-old son, Max, has flourished during two years in the ete­plirsen trial. But his 15-year-old brother, Austin, did not qualify for the Sarepta trial. He must use a wheelchair and recently began to lose the ability to feed himself. McNary believes that if Austin can get access to the drug soon, it could halt his decline. She plans to continue to push the FDA to act quickly.

“This is about stopping the progression of the disease; this was never a cure,” McNary said. “If we could just freeze it while we let smart people figure it out further, that would be fine with me. . . . The one thing that none of us have is any time to waste.”