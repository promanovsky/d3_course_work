The European Union on Tuesday imposed a new round of sanctions on Russia over what it called threats to Ukraine’s independence, joining the Obama administration’s decision the previous day to ratchet up punishment even as it acknowledged that a new round of economic sanctions is unlikely to bring an immediate change in Russian behavior.

The European measures froze the E.U.-based assets of 10 Russian officials and five Ukrainians, all of whom the European Union linked to unrest in eastern Ukraine. They were also banned from travel to the 28 E.U. nations. Unlike the U.S. sanctions, which also hit Russian business executives, no prominent private-sector individuals were named by Europe. Only one person was also on the list of U.S. sanctions targets.

The European Union has been more reluctant than the United States to target Russian businesses, in part because E.U. companies have far stronger economic relations with Russia than their U.S. counterparts. U.S. officials have indicated that they were ready to issue new sanctions last week but decided to wait for the European Union in order to project a unified front against what they say is Russia’s escalating campaign to destabilize Ukraine.

“I am alarmed by the worsening security situation in eastern Ukraine,” E.U. foreign policy chief Catherine Ashton said in a statement Tuesday. “The downward spiral of violence and intimidation undermines the normal functioning of the legitimate state institutions.”

She said that the European Union would consider “possible additional individual measures” depending on how the situation evolves.

1 of 49 Full Screen Autoplay Close May 1, 2014 Thursday April 30, 2014 Tuesday April 29, 2014 Tuesday April 28, 2014 Monday April 27, 2014 Sunday Skip Ad × In eastern Ukraine, pro-Russia militants clash with pro-Ukraine protesters View Photos The mayor of Ukraine’s second-largest city was in critical condition after an assassination attempt, and pro-Russia militants attacked pro-Ukraine protesters as the country descended into lawlessness. Caption The Ukrainian army launched its first major assault on a rebel stronghold in the eastern Ukrainian city of Slovyansk on Friday, provoking the heaviest fighting since a pro-Russian uprising began a month ago. Two military helicopters were shot down, and at least three people were reported killed. May 2, 2014 A Ukrainian soldier stands in front of Pro-Russian Civilians at a checkpoint near the town of Slovyansk in eastern Ukraine. Russia has massed tens of thousands of troops along the border with Ukraine, and the Ukrainian army’s assault on Friday raised the risks of a Russian military response. Baz Ratner/Reuters Wait 1 second to continue.

The European measures brought new uncertainty to a tense hostage situation in the eastern Ukrainian city of Slovyansk, where pro-Russian separatists are holding seven observers from the Organization for Security and Cooperation in Europe and negotiations for their release are proceeding fitfully.

“We will return to dialogue on the status of ‘prisoners of war’ only after the European Union lifts these sanctions,” Vyacheslav Ponomaryov, the self-appointed “people’s mayor” of Slovyansk, told the Interfax news agency Tuesday. He added that if the sanctions hold, his separatists would not allow E.U. officials access to the region at all.

In London, U.S. Attorney General Eric H. Holder Jr. and British Home Secretary Theresa May met to discuss ways to help Ukraine assets believed to have been stolen by the governmenment of ousted president Viktor Yanukovych. Holder said the process, which might take years, would be led by a newly established “klepltocracy squad” within the FBI.

“We pledge this morning that we will never stop fighting alongside Ukraine and its partners to make the progress we need,” Holder said.

The new U.S. measures froze the assets of seven Russian individuals and 17 companies associated with them, and prohibited any U.S. dealings with them. All were identified as closely linked to Russian President Vladimir Putin. The Obama administration also announced new restrictions on Russia’s import of U.S. goods deemed to contribute to its “military capabilities.”

Some U.S. lawmakers and Ukrainian politicians decried the actions as too weak and called for immediate, full-scale sanctions against the Russian banking and energy sectors. Officials in Moscow condemned the announcement but insisted that their economy could weather the restrictions.

American officials asserted that Russia is already showing signs of significant economic pain, with capital fleeing, investment falling and its debt downgraded to the brink of junk bonds. The goal is to gradually increase pressure until “Russia sees the dead end that it’s going down in Ukraine,” a senior administration official said.

“We don’t expect there would be an immediate change in Russian policy,” and there will be “daily fluctuations here and there” in the financial markets, said the official, one of several who briefed reporters in a conference call on the new measures Monday on the condition of anonymity.

“What we need to do is to steadily show the Russians that there are going to be much more severe economic pain, much more severe political isolation and, frankly, that Russia stands far more to lose, continuing these actions over time,” than if it stands down in Ukraine.

President Obama made the same case in a news conference in the Philippines, calling the sanctions a “calibrated” response to Russian actions and saying, “We don’t yet know whether it is going to work.” He emphasized that “the next phase — if, in fact, we saw further Russian aggression,” including troop incursions into eastern Ukraine — could be sanctions against broad sectors of the Russian economy.

Several Russian economic analysts agreed with the long-term U.S. assessment, saying the general climate of political unpredictabilty and strained ties with the West was far more costly than specific sanctions.

“For the economic picture, it’s impossible for this to have any good result,” said Evgeny Gontmakher, a former adviser to Prime Minister Dmitry Medvedev who now works at the Institute of Social Development, a liberal policy institute.

The Russian officials targeted for the new E.U. sanctions Tuesday included Valery Gerasimov, the chief of staff of the Russian armed forces, and Dmitry Kozak, a deputy prime minister, who was the only person also named by the United States the day before. The Europeans also cited the shadowy commander of pro-Russian forces in Slovyansk, Igor Strelkov, who they said was an active-duty officer of Russia’s elite Main Intelligence Directorate.

Five pro-Russian separatists from eastern Ukraine, including the self-proclaimed governor of the “People’s Republic of Donetsk,” Denys Pushylin, were also listed.

Russian officials said the Obama administration doesn’t understand the nature of the crisis in Ukraine. “It is a distorted mirror of foreign policy, not a responsible approach to the situation,” said Deputy Foreign Minister Sergei Ryabkov.

Ryabkov added that the Russian government will take reciprocal measures. “We have never been concealing that we have capabilities for such a response at our disposal, as well as a set of measures, which is rather large, that will be used,” he told the Interfax news agency, without elaboration.

Among those identified for U.S. sanctions Monday was Igor Sechin, president of Rosneft, Russia’s leading state-owned petroleum company.

Others cited by the United States are either connected to last month’s annexation by Russia of the Ukrainian region of Crimea or are otherwise involved in its foreign and security policy. Businessman Sergey Chemezov — who lived in the same East German apartment complex as Putin when Putin was stationed there as a KGB officer in the late 1980s — is the head of state-owned Rostec, a major arms and technology exporter that has long supplied Boeing with titanium for its airplanes and has projects with Canadian aircraft-maker Bombardier and Italian tire company Pirelli.

Vyacheslav Volodin, Putin’s first deputy chief of staff, is one of the Russian president’s closest advisers. The Treasury Department said he was involved in Russia’s decision to take over Crimea.

Oleg Belavantsev is Putin’s envoy to Crimea; Evegeniy Murov is the director of Russia’s powerful Federal Protective Service, which protects the president. Alexei Pushkov is head of the foreign affairs committee of the lower house of Russia’s parliament.

All of the individuals sanctioned are now banned from receiving U.S. visas. Sechin’s Rosneft has major joint oil-exploration projects with Exxon Mobil, and British oil giant BP owns a 19.75 percent stake in the company. Sechin will no longer be able to travel to the United States for business consultations, but he said Monday that the sanctions would have little effect on Rosneft’s projects or its bottom line.

“I view the latest steps by Washington as a high evaluation of the effectiveness of our work,” he said in a statement. “We assure our shareholders and partners, including Americans, that this effectiveness will not decrease and that our cooperation will not suffer but will develop dynamically.”

BP America said in a statement that it will “comply with all relevant sanctions” but that “we are committed to our investment in Rosneft, and we intend to remain a successful, long term investor in Russia.”

The 17 companies listed Monday, from banks to a construction company to a rail freight operator, are all tied to businesspeople and officials close to Putin who had been targeted in earlier rounds of U.S. sanctions. Two firms are controlled by close Putin friends Boris and Arkady Rotenberg, and three by Yuri Kovalchuk, who is described by U.S. officials as Putin’s personal banker.

Eleven are owned or controlled by Gennady Timchenko, a businessman who last month sold a nearly 50 percent stake in the Swiss-based oil-trading firm Gunvor shortly before he was targeted with sanctions.

Putin on Monday did not directly address the new sanctions, but said that “Western partners” were targeting Russia’s defense industry to weaken it and force it to rely on imports from Ukrainian factories. “We will achieve our goals regardless,” he told lawmakers in the northwestern city of Petrozavodsk. “It is just a matter of time and money.”

In a statement, GOP Sens. Bob Corker (Tenn.), ranking Republican on the Foreign Relations Committee, and Kelly Ayotte (N.H.) of the Armed Services Committee said “incremental” sanctions are insufficient and called for full-scale measures against Russia’s financial and energy sectors.

Rep. Eliot L. Engel (N.Y.), ranking Democrat on the House Foreign Affairs Committee, said that the new sanctions are “a step in the right direction” but that “I believe we need to go further.”

In Ukraine, former defense minister and current presidential candidate Anatoliy Gritsenko said the West has not meaningfully helped Ukraine in its time of need.

“The U.S. and [Britain] betrayed Ukraine,” he said. “They encouraged us to give up our nuclear weapons and said they would come to our defense,” he said, referring to agreements reached at the demise of the Soviet Union. “But instead, they’re just ‘deeply concerned.’ ”

DeYoung reported from Washington. Griff Witte in Ukraine contributed to this report.