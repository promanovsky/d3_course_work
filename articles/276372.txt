European stocks are poised to open lower on Friday as growing instability and violence in Iraq pushed up oil prices to a nine-month high.

The crisis in Iraq escalated on Thursday as Iraqi Kurdish forces took advantage of the disarray of the government in Baghdad to seize key military installations in the major oil city of Kirkuk and vowed to march on to Baghdad.

U.S. President Barack Obama said he is weighing a range of short-term military options, including airstrikes, to support Iraq's government against advancing Sunni militants. Brent crude futures are firmer at a nine-month high near $114 a barrel after climbing more than $2 to over $112 a barrel on Thursday.

In news out of Ukraine, the country's Interior Minister Arsen Avakov has accused Russia of sending three tanks and other military vehicles across the border into eastern Ukraine to help pro-Russian separatists there.

On the economic front, investors eye Eurozone employment as well as German trade and consumer price data in the European session for directional cues. The British pound gained ground against its key counterparts after Bank of England governor Mark Carney signaled that interest rate could rise sooner than forecast, due to strong economic growth.

In a speech at the annual Mansion House in the City of London to financial and political leaders on Thursday, the governor suggested that interest rate hike could happen sooner than currently expect, although the decision would be dependent on data and is not a pre-set course.

Across the Atlantic, investors await a pair of reports on producer price inflation and consumer sentiment for fresh clues to the Fed's future course of action. Intel shares jumped in extended U.S. trading on Thursday after the chipmaker raised its second quarter and full-year guidance, citing stronger than expected demand for PCs.

Asian stocks are turning in a mixed performance, paring early losses, as investors await a slew of Chinese data for further direction. The safe-haven yen weakened against the dollar after the Bank of Japan held off expanding its stimulus program, diverging from the European Central Bank that cut interest rates to record lows and unveiled a huge package of monetary stimulus last week.

In domestic corporate news, Italian banking giant UniCredit SpA announced that its online banking unit FinecoBank S.p.A will list up to 30 percent of its share capital in an initial public offering that could value the company at up to 2.668 billion euros, or $3.62 billion.

Abengoa Yield Plc has priced its upsized initial public offering of 24.85 million ordinary shares at $29.00 per share, above the estimated range to raise about $720 million.

Dassault Systèmes, a developer of 3D design software, backed its fiscal 2014 adjusted earnings forecast, and said it expects to double earnings in fiscal 2019.

Gerry Weber International AG reported first-half net income of 32.7 million euros, up from 29.3 million euros last year.

European stocks ended little changed on Thursday, as concerns over the pace of global growth and worries that escalating violence in Iraq may disrupt oil supplies overshadowed investor optimism over positive Eurozone industrial production data. The German DAX and France's CAC 40 slipped around 0.1 percent each, but the FTSE of the U.K. edged up marginally.

U.S. stocks fell notably overnight, with escalating violence in Iraq and disappointing retail sales and jobless claims data weighing on the markets. The Dow and the S&P 500 dropped about 0.7 percent each, while the tech-heavy Nasdaq fell 0.8 percent.

For comments and feedback contact: editorial@rttnews.com