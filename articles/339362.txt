NEW YORK (TheStreet) -- Hormel Foods Corp. (HRL) - Get Report agreed yesterday to buy Muscle Milk maker CytoSport Holdings Inc. for about $450 million, as the meat processor looks to expand into other retail food businesses, the Wall Street Journal reports.

Hormel Foods stock closed at $49.35, near its 52 week high of $49.87.



Must Read: Warren Buffett's 25 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates HORMEL FOODS CORP as a Buy with a ratings score of A. TheStreet Ratings Team has this to say about their recommendation:

"We rate HORMEL FOODS CORP (HRL) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its revenue growth, largely solid financial position with reasonable debt levels by most measures, growth in earnings per share, increase in net income and solid stock price performance. We feel these strengths outweigh the fact that the company shows weak operating cash flow."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

HRL's revenue growth has slightly outpaced the industry average of 3.1%. Since the same quarter one year prior, revenues slightly increased by 4.3%. This growth in revenue appears to have trickled down to the company's bottom line, improving the earnings per share.

HRL's debt-to-equity ratio is very low at 0.07 and is currently below that of the industry average, implying that there has been very successful management of debt levels. Along with the favorable debt-to-equity ratio, the company maintains an adequate quick ratio of 1.37, which illustrates the ability to avoid short-term cash problems.

HORMEL FOODS CORP has improved earnings per share by 13.0% in the most recent quarter compared to the same quarter a year ago. The company has demonstrated a pattern of positive earnings per share growth over the past two years. We feel that this trend should continue. During the past fiscal year, HORMEL FOODS CORP increased its bottom line by earning $1.94 versus $1.86 in the prior year. This year, the market expects an improvement in earnings ($2.18 versus $1.94).

The net income growth from the same quarter one year ago has exceeded that of the Food Products industry average, but is less than that of the S&P 500. The net income increased by 11.6% when compared to the same quarter one year prior, going from $125.52 million to $140.09 million.

Compared to where it was 12 months ago, this stock has enjoyed a nice rise of 26.10% which was in line with the performance of the S&P 500 Index. We feel that the stock's sharp appreciation over the last year has driven it to a price level which is now somewhat expensive compared to the rest of its industry. The other strengths this company shows, however, justify the higher price levels.

You can view the full analysis from the report here: HRL Ratings Report



STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.





