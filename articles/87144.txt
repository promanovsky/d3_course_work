A Gallup study has found that the Huntington-Ashland-Ironton metropolitan area is the most obese community in the U.S.

The Gallp-Healthways Well-Being Index found that 39.5% of the adult population of the area, which straddles West Virginia, Kentucky and Ohio, are overweight. Other problem communities are McAllen-Edinburg-Mission (38.3%), Tx., and the Hagerstown-Martinsburg metropolitan area (36.7%) in Maryland and West Virginia.

Boulder, Colo. remains the city with the lowest obesity rates in the US. Just 12.4% of Boulder’s residents are deemed overweight — the lowest of any community surveyed in the U.S. Also in the top three were Naples-Marco Island (16.5%), Fl., and Fort Collins-Loveland (18.2%), Colo.

Boulder has had the lowest obesity rate in five of the last six surveys conducted since Gallup and Healthways began conducting the study in 2008.

Nationally, the U.S.’s obesity rate jumped to 27.1% last year, the highest Gallup has recorded since tracking began six years ago.

[Gallup]

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.