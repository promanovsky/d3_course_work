The local government of Texas is deliberating whether they should impose stricter regulations on the use of hepatitis C treatment for patients covered by the Medicaid health insurance.

The Texas Health and Human Services Commission, the agency that oversees Medicaid, has come up with a new policy which will enable patients to get Sovaldi provided that they need it.

"The board asked state staff to meet with stakeholders, including gastroenterologists, about the criteria, whether it was too strict, and the prior authorization process," spokeswoman for Texas Health and Human Services Commission, Stephanie Goodman told Reuters. "We're looking at our options and how that will affect our timeline to get the drug covered."

The discussions in Texas are seen to be a focal point in the changes in policy governing the use of Sovaldi. Texas is a populous state and there are many hepatitis C patients living in it. Discussions about the Gilead treatment are also underway in other places including Colorado, Virginia, and California. Texas has also planned to take some steps to allow their treasury to make supplemental payments in order to offset Sovaldi's high costs.

Sovaldi has received so much attention after the World Health Organization criticized its very expensive price. Although this is the first drug to provide a cure for the disease, treatment with Sovaldi for all hepatitis C patients in the United States may cost $200 billion, and this may drastically affect state budgets.

Discussions about the regulation of Sovaldi's use are still ongoing in California, however, Florida has already decided to use Sovaldi for every Medicaid patient but they reiterated that they will be monitoring its use and administration.

Meanwhile, the Democrats are questioning the expensive price of Sovaldi as it will cost a patient $1,000 per pill. However, Gilead responded that it is a fair price and the lowest possible amount they can offer.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.