Last week we heard that Android 4.4.3 KitKat had turned up on Samsung’s developer website, and now we have some more information on the Android 4.4.3 update for various Samsung devices.

According to the guys over at Sammobile, the Android 4.4.3 update is being tested on the Samsung Galaxy S5 and also the Samsung Galaxy S4 LTE-A.

The update is apparently being integrated with Samsung’s own applications on the above devices, so we may have to wait a little while until it is released.

It looks like it may come at the end of June, and considering that the Google I/O Developer Conference takes place at the end of June, we are expecting to see the Android 4.4.3 update announced at the event.

Some details were also revealed about the Android 4.4.2 Kit Kat update for various other Samsung devices, and it looks like a number of devices will get the update next month.

The Samsung devices that are expected to get Android 4.4.2 Kit Kat next month include the Galaxy Grand 2, Galaxy Mega 5.8 and the Galaxy Mega 6.3, as they updates are apparently in the final testing stages.

Source Sammobile

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more