In the latest challenge to the reach of law enforcement in the digital age, Microsoft and its Web-based email service are pushing back against a US government search warrant for customer emails stored in a data center overseas.

In court papers made public Monday, Microsoft's attorneys list their objections to a judge's order that the company comply with a warrant issued in December for a customer's email-account data stored in Dublin, Ireland.

"The government takes the extraordinary position," the filing reads, "that by merely serving such a warrant on any US-based email provider, it has the right to obtain the private emails of any subscriber, no matter where in the world the data may be located, and without the knowledge or consent of the subscriber or the relevant foreign government where the data is stored."

The release of the court papers follows a year of high-profile challenges to government access to data, which were sparked by former National Security Agency contractor Edward Snowden's leak of secret agency documents.

Among other things, the Snowden controversy raised questions about whether laws related to search and seizure need to be updated in light of rapid technological change that's made it easier to scoop up all sorts of data, including private phone calls, email exchanges, photos, and videos.

Data regularly zips around the planet now, too, traversing national borders and spending time in foreign data centers. That's raised concern that US defense and law enforcement agencies have been meddling where they shouldn't and treading on the rights of foreign citizens. Overseas governments and customers have also worried that US companies might be in the back pocket of the NSA and other government outfits.

Microsoft's lawyers pick up on those last themes, cautioning that allowing warrants like the one in question would "violate international laws and treaties, and reduce the privacy protection of everyone on the planet," as well as "have a significant negative impact on Microsoft's business, and the competitiveness of US cloud providers in general."

"Over the course of the past year," the filing reads, "Microsoft and other US technology companies have faced growing mistrust and concern about their ability to protect the privacy of personal information located outside the United States. The government's position in this case further erodes that trust, and will ultimately erode the leadership of US technology companies in the global market."

The government, on the other hand, said in its own filing in April that Microsoft's position would lead to "absurd" and "arbitrary" results that would have "a devastating impact on the government's ability to conduct criminal investigations."

"It is entirely conceivable," government lawyers wrote, "that, based on where a service provider decided to store information on a given day due to its own technical requirements, or even its views on whether the government should have access to the data, it could reject [a warrant] one day but accept it the next."

"Microsoft's position creates a[n]...absurdity," the government argued elsewhere in the document, "because it stores email content overseas based on where its subscribers claim to live. Under Microsoft's view...criminals using a US service provider could avoid lawful process to obtain stored content in their accounts simply by representing falsely that they live outside the country."

The case so far has hinged on legal arcana involving subpoenas versus search warrants, as well as the implications of international treaties and the relevance of the 1986 Electronic Communications Privacy Act. The government's reply to the Microsoft filing is expected in July, with oral arguments in the case to begin several weeks after that. Verizon filed a friend-of-the-court brief Tuesday in support of Microsoft's position.

The identity and nationality of the holder of the Microsoft email account in question have not been revealed. The warrant is thought to be related to a narcotics case.

Microsoft fights warrant for overseas data