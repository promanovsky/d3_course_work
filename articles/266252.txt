Young, white and doing heroin

Share this article: Share Tweet Share Share Share Email Share

Washington - Anna Richter played high school basketball and grew up near a golf course in Centreville, Virginia, where the average family makes more than $100 000 per year. She was 15 or 16 when she began popping prescription painkillers to get high. A couple of years later, she snorted heroin for the first time. “I tried it and I loved it. And I could get it any time I wanted,” she told AFP. Not only was heroin cheaper than pills like OxyContin or Vicodin, it gave her a euphoric high like none she had ever experienced. “All those self-defeating thoughts were gone,” she said.

By the time she was 20, she was injecting heroin daily. She dropped out of college, moved back in with her parents and regularly stole money from them to feed her habit.

“There were a ton of overdoses in the area and I lost a lot of friends in Virginia due to this disease,” she said.

But none of that stopped her. The heroin users she knew were much like her - affluent, white and from educated families. They had endured no major struggles on which to blame their behaviour.

Heroin “has become so much more common. It is not like that dirty drug that people think of any more,” Richter said.

Indeed, a new study has found that heroin is increasingly the drug of choice for young people in the American suburbs, where an epidemic of prescription painkiller abuse has opened the path toward the cheaper street drug.

A study, “The Changing Face of Heroin in the United States,” spans the past 50 years and shows how heroin has made its way from the back alleys to the backyards of middle class America.

The data is based on nearly 2 800 patient surveys filled out between 2010 and 2013 at drug treatment centres across the United States.

On average, people are 23 when they start using heroin in the United States, according to the findings in the Journal of the American Medical Association (JAMA) Psychiatry.

More than 90 percent of those who began using heroin in the past decade were white.

Back in the 1960s and 1970s, more than 80 percent of heroin users were African-American males who lived in the inner cities and who began using at around age 16.

“In the past, heroin was a drug that introduced people to narcotics,” said lead study author Theodore Cicero, a researcher at Washington University.

“But what we're seeing now is that most people using heroin begin with prescription painkillers such as OxyContin, Percocet or Vicodin, and only switch to heroin when their prescription drug habits get too expensive.”

Cost is a key factor for addicts, even in wealthy areas, experts say.

Painkillers are often $1 per milligram, or $80 for an 80 mg pill, whereas a bag of heroin could sell for $10 to $25.

A crackdown on pill mills and doctors who overprescribe or illegally distribute prescription pain medications has shrunk supplies of opioids over the past decade.

In addition, non-crushable forms of some pills have been released to discourage people from snorting and injecting them.

But experts say these changes have only pushed more drug abusers toward heroin.

“A lot of them seek out heroin because they don't want to go through withdrawal,” said Kevin Bandy, clinical director at Hanley Center, a rehabilitation facility in West Palm Beach Florida.

About 467 000 people reported heroin use in 2012, the Centres for Disease Control and Prevention has said.

The number of heroin users has more than doubled since 2007, and deaths from heroin overdose are also on the rise in some parts of the country, experts say.

About 3 000 people die in the US of heroin overdoses each year, and more than 16 000 die from prescription painkillers according to government data.

Highly addictive, heroin can be even harder to quit because of the stigma it carries, according to Bandy.

“It is going to be pretty difficult to go tell your family you are having a problem with heroin. That would definitely get some gasps out of everyone in the room,” he said.

Richter said she was not ready to seek help when her parents finally gave her an ultimatum - get treatment or get out of the house.

She went through two week of painful detox, followed by six months of rehab, exploring the roots of her addiction and what caused her to seek out drugs.

“I am clean and sober for six years now,” she told AFP.

Richter said she has found a career - and a purpose in life - helping others enter rehab.

But she admits that she doesn't know how to prevent other affluent suburban youth from succumbing to heroin.

“I can't sit here and say there is something out there to stop these kids from doing it,” she said. “But we can show them that there is a solution. There is a way out.” - Sapa-AFP