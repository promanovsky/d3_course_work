After nine years on the air, CBS's How I Met Your Mother takes its final bow tonight. Below, check out two sneak peeks from tonight's special one-hour series finale airing 8:00-9:01 PM, ET/PT on the CBS Television Network.

In the episode titled 'Last Forever Parts One and Two', Ted finally finishes telling his kids the story of how he met their mother.

HOW I MET YOUR MOTHER, now in its ninth and final season, is a comedy about Ted and how he fell in love. This season takes place over the course of Barney and Robin's wedding weekend, when the lives of the gang change dramatically. Ted is about to move to Chicago, Marshall and Lily are about to move to Rome, and Robin and Barney are about to become husband and wife. The series is narrated through flashbacks from the future.