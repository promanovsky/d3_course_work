CBD was pleased to see that the proposed merger between 21st Century Fox and Time Warner will not loosen Rupert Murdoch's grip on the family firm.

The mostly scrip-funded bid would see the Murdoch family's economic interest diluted to a single digit percentage, but the scrip issued to his new investors from Time Warner will thankfully be non-voting stock.

We wouldn't want those pesky Time Warner types actually wielding any influence now would we.

The Murdochs will remain firmly in charge with 40 per cent of the shares that are actually allowed to vote on company proceedings. And in a belt and braces approach, there is always the poison pill provisions to see off any clever marauder.

Lachlan from a distant perspective