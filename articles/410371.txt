British manufacturing growth slows

Share this article: Share Tweet Share Share Share Email Share

London - British manufacturing grew at its slowest pace in a year in July, possibly reflecting the prospect of higher interest rates at home and the impact of conflict in Ukraine, a survey showed on Friday. The Markit/CIPS UK Manufacturing Purchasing Managers' Index (PMI) fell to 55.4 from 57.2 in June - its lowest level since July 2013 and well below the lowest forecast in a Reuters poll. Growth remained strong by historical standards and the slowdown was in line with the Bank of England's view that Britain's strong recovery will slow in coming months. But the drop may also be a symptom of uncertainty in British and European markets about the crisis in Ukraine, as well as the likelihood that monetary policy will tighten in Britain, said Rob Dobson, senior economist at Markit. Sterling fell to a seven week-low against the dollar after the data, which some investors saw as weighing against an early interest rate hike by the Bank of England.

British government bond prices rose slightly.

“It remains too early to gauge the impact of the Ukraine crisis, but the worry is that the combined effects of expected policy tightening, heightened economic uncertainty and sluggish trade could mean manufacturing growth could suddenly weaken more than expected,” Dobson said.

Economists took the slowdown in their stride.

“It shouldn't set off any alarm bells as the indicator is still consistent with a decent pace of growth across the sector and continued prospects for more job creation,” said Lee Hopley, the chief economist at EEF, a manufacturers' organisation.

But there would be an extra focus on Tuesday's release of the PMI for Britain's dominant services sector, economists said.

Markit said growth in new export orders slowed to a four-month low and backlogs of work fell at the fastest pace since May 2013 with some firms saying they had excess capacity.

Employment in manufacturing grew in July at its slowest pace since October last year.

The BoE is watching spare capacity in the economy closely as it considers when to raise interest rates from their record low 0.5 percent.

The Bank is expected to keep rates on hold when it meets next week and lower its estimate of the slack in the economy when it updates its forecasts on August 13.

The BoE is also watching for signs of inflation pressures.

The Markit survey showed that manufacturers' output prices were stable in July but input prices rose, albeit modestly, for a third month.

Companies cited higher prices for metals, plastics and timber, but some reported a reduction in costs of imported goods, linking the fall to the appreciation of sterling.

The International Monetary Fund said this week that sterling was overvalued by 5 to 10 percent.

- Detailed PMI data are only available under licence from Markit and customers need to apply to Markit for a licence. - Reuters