The authors of a paper published in PNAS on Monday said their research revealed that when spouses getting angry at one another, it is often caused by low blood glucose levels, meaning the significant other should find some food for their mate quickly.

Ohio State University communication and psychology professor Brad Bushman, a lead author of the study, wrote that loss of self-control is a factor that contributes to aggression among intimate partners while self-control is linked to nutrition, the Los Angeles Times reported.

"Self-control requires energy, and that energy is provided in part by glucose," Bushman said. "Glucose is made from nutritious intake that becomes converted into neurotransmitters that provide energy for brain processes. Low glucose levels can undermine self-control because people have insufficient energy to overcome challenges and unwanted impulses."

To study the effects of "hanger," as the researchers dubbed the act of being angry while hungry, Bushman and his team observed the glucose levels of 107 married couples for three weeks.

The researchers checked the couples' glucose levels in the morning, before breakfast and at night before they went to bed. At the end of the day, the researchers asked the couples to demonstrate their emotions toward their spouses onto a voodoo doll.

"To obtain daily measures of aggressive inclinations toward their partner, each participant received a voodoo doll along with 51 pins and was told: 'This doll represents your spouse. At the end of each day ... insert between 0 and 51 pins in the doll, depending how angry you are with your spouse,'" the authors wrote.

After 21 days, the couples were then asked to play a "game" in a laboratory, which measure aggressive behavior.

"Participants were told that they would compete with their spouse to see who could press a button faster when a target square turned red on the computer, and that the winner on each trial could blast the loser with loud noise through headphones," according to the study.

The noises included sounds of fingernails scratching a chalkboard, a dentists' drill and an ambulance siren. The spouses could crank up the volume to a level of 105 decibels as well as control the duration, between a half a second to five seconds.

The game, however, was fake. The couples sat in separate rooms and were told the outcomes would happen. No one was actually blasted with those sounds and the game was rigged.

The researchers found that lower glucose levels meant more pins in the voodoo doll and louder, longer noises.

"Results suggest that interventions designed to provide individuals with metabolic energy might foster more harmonious couple interactions," authors concluded.