Teenagers have found a way to maximize the effect of a caffeine rush by smoking their coffee.

Smoking coffee joints, where ground coffee beans are rolled up into cigarette paper, offers "vaporization of the bean [that] produces none of the characteristic bitterness of brewed coffee," wrote one tutorial on smoking coffee, according to TheBlaze.com.

The effect is a sudden jolt of energy from the concentrated caffeine. Users can also smoke a whole coffee bean by stuffing it into the end of a glass tube. A tiny piece of copper, from a scouring pad, is inserted behind the bean.

Once it's ignited, "the coffee bean should melt into the scouring pad and produce white coffee vapor," the tutorial said.

"I needed something fast, cheap and easy that delivered the caffeinated experience my patrons crave," the tutorial's author wrote, The Blaze reported. "After some soul-searching in the alleyway of the more dubious neighborhoods near my hometown, I found it."

Doctors told WCCB-TV that smoking coffee is a potential health hazard. Side effects include hallucinations, dizziness, trouble breathing and vomiting.

Doctors say the trend, which is nothing new, has increased in recent weeks, WCCB-TV reported. There are several viral videos of teenagers lighting up coffee joints.

"I also don't' recommend doing it at midnight when you have class the next morning- it will keep you up," one user on Reddit wrote in a 2012 post titled "Stupid idea of the week," according to The Blaze.

"Unless, of course, you need to study: then, I highly recommend it. If you need a long-lasting burst of energy, you could do a lot worse than smoking a coffee joint. If you have nothing to do and you don't need to sleep for a while, a coffee joint is a great way to spend a few hours."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.