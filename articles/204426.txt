Chrysler Group LLC, the U.S. automaker owned by Fiat SpA, lost $690 million in the first quarter when it bought the shares held by a union retiree medical trust, paving the way for integration of the companies.

Chrysler reported a modified operating profit of $586 million, before about $1.2 billion in charges related to the stock transaction with the United Auto Workers union health-care trust. Marchionne said last week that recall costs hurt Chrysler’s profit.

Chrysler, which had reported 10 straight profitable quarters, has provided most of the income for what is set to become Fiat Chrysler Automobiles NV later this year. The combined company, with a listing in New York and headquarters in London, will be the world’s seventh largest, said Sergio Marchionne, the chief executive officer of both automakers.

Fiat SpA last week reported a 622 million euro ($866 million) trading profit, calculated using IFRS accounting, that was less than the 854 million-euro average of five analyst estimates. The Chrysler results today used GAAP.

Chrysler has reported 49 straight months of year-on-year growth in U.S. vehicle sales, powered by its Ram and Jeep light-truck brands.

Marchionne on May 6 laid out plans to invest 55 billion euros to more than double earnings before interest and taxes to a range of 8.7 billion euros to 9.8 billion euros from 3.5 billion euros last year. Fiat shares fell almost 12 percent on May 7, the most in two and a half years, which Marchionne called an overreaction.

Fiat Chrysler Automobiles will have its main stock listing in New York while it will be registered in the Netherlands with the fiscal domicile in the U.K. for tax purposes. The headquarters will be in London

“The board, my office and some of my functions need to operate out of London,” Marchionne said during a CNH Industrial NV press conference last week in Auburn Hills, Michigan. Marchionne is chairman of CNH, which makes Case bulldozers and Iveco buses.