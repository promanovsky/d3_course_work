Amazon.com Inc launched a new product named Amazon Dash on Friday that allows the user to add groceries and household goods to their shopping lists using the company's AmazonFresh service.

A black-and-white hand-held wand-shaped remote-control features a microphone, speaker as well as a bar-code reader and links directly to the user's AmazonFresh account.

However, the device is available only for users of the AmazonFresh which currently operates exclusively in Southern California, San Francisco and Seattle. The device is free during the trial period, according to the product's website.

However, signing up for Amazon Dash is by invitation only while the AmazonFresh service is currently available only Southern California, San Francisco and Seattle.

The online retailer has been steadily expanding towards electronics manufacturing businesses, starting with the Kindle e-reader which was first launched in 2007, and the Fire TV streaming set-top box announced earlier this week, even as it seeks new ways to energize a gradually slowing core retail business.

Amazon has been steadily expanding its "Fresh" online grocery business, targeting one of the largest retail sectors yet to be upended by online commerce. The company has plans to launch AmazonFresh, which has operated in Seattle for years, in roughly 20 urban areas in 2014, including some outside the United States.

A successful foray into groceries could also help underwrite the development of a broad-based delivery service employing Amazon trucks to deliver directly to homes, which could have implications for UPS, FedEx and other package delivery companies that currently ship Amazon goods.

Still, groceries have proven to be one of the most difficult sectors for online retailers to manage successfully. One of the most richly-funded start-ups of the dot-com era, Webvan, was a spectacular failure as the cost of developing the warehouse and delivery infrastructure proved overwhelming.

Amazon was unavailable for comment regarding the public availability of the device.