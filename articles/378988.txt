Deep in the forests of southern Guinea, the first victims fell ill with high fevers. People assumed it was the perennial killer malaria and had no reason to fear touching the bodies, as is the custom in traditional funerals.

Some desperate relatives brought their loved ones to the distant capital in search of better medical care, unknowingly spreading what ultimately was discovered to be Ebola, one of the world's most deadly diseases.

Ebola, a hemorrhagic fever that can cause its victims to bleed from the ears and nose, had never before been seen in this part of West Africa where medical clinics are few and far between. The disease has turned up in at least two other countries — Liberia and Sierra Leone — and 539 deaths have been attributed to the outbreak that is now the largest on record.

The key to halting Ebola is isolating the sick, but fear and panic have sent some patients into hiding, complicating efforts to stop its spread. Ebola has reached the capitals of all three countries, and the World Health Organization reported 44 new cases including 21 deaths on Friday.

There has been "a gross misjudgment across the board in gauging the severity and scale of damage the current Ebola outbreak can unleash," the aid group Plan International warned earlier this month.

"There are no cases from outside Africa to date. The threat of it spreading though is very much there," said Dr. Unni Krishnan, head of disaster preparedness and response for the aid group.

The longer it takes to find and follow up with people who have come in contact with sick people, the more difficult it will be to control the outbreak. - Anja Wolz

Preachers are calling for divine intervention, and panicked residents in remote areas have on multiple occasions attacked the very health workers sent to help them. In one town in Sierra Leone, residents partially burned down a treatment centre over fears that the drugs given to victims were actually causing the disease.

Activists are trying to spread awareness in the countryside where literacy is low, even through a song penned about Ebola.

"It has no cure, but it can be prevented; let us fight it together. Let's protect ourselves, our families and our nation," sings the chorus.

"Do not touch people with the signs of Ebola," sings musician and activist Juli Endee. "Don't eat bush meat. Don't play with monkey and baboons. Plums that bats have bitten or half-eaten, don't eat them."

Guinea first notified WHO about the emergence of Ebola in March and soon after cases were reported in neighbouring Liberia. Two months later there were hopes that the outbreak was waning, but then people began falling ill in Sierra Leone.

Doctors without Borders says it fears the number of patients now being treated in Sierra Leone could be "just the tip of the iceberg." Nearly 40 were reported in a single village in the country's east.

"We're under massive time pressure: The longer it takes to find and follow up with people who have come in contact with sick people, the more difficult it will be to control the outbreak," said Anja Wolz, emergency co-ordinator for the group, also referred to by its French name Medecins Sans Frontieres.

This Ebola virus is a new strain and did not spread to West Africa from previous outbreaks in Uganda and Congo, researchers say. Many believe it is linked to the human consumption of bats carrying the virus. Many of those who have fallen ill in the current outbreak are family members of victims and the health workers who treated them.

There is no cure and no vaccine for Ebola, and those who have survived managed to do so only by receiving rehydration and other supportive treatment. Ebola's high fatality rate means many of those brought to health clinics have been merely kept as comfortable as possible in quarantine as they await death. As a result, some families have been afraid to take sick loved ones to the clinics.

'Police treated us like we were aliens'

"Let this warning go out: Anyone found or reported to be holding suspected Ebola cases in homes or prayer houses can be prosecuted under the law of Liberia," President Ellen Johnson Sirleaf stated recently.

Her comments came just days after Sierra Leone issued a similar warning, saying some patients had discharged themselves from the hospital and had gone into hiding.

At the airport in Guinea's capital, departing passengers must undergo temperature screening, and those with a fever are pulled aside for further evaluation. Still, the stigma of Ebola follows Guineans well outside the region.

"The police treated us like we were aliens. They said they didn't want us in their country because of the disease affecting Guinea," says Tafsir Sow, a businessman who was briefly detained at the airport in Casablanca, Morocco before continuing on to Paris. "I had tears in my eyes."

Still, WHO health officials are hopeful they will be able to get the situation under control in the next several weeks. A recent conference in the capital of Ghana brought together health authorities from across the affected areas, and the countries agreed on a common approach to fight Ebola.

"When you have it spread, of course it's moving in the wrong direction," said Dr. Keiji Fukuda, WHO's assistant director-general for health security and environment. "You want to see the number of infections going down. So we really have to redouble our efforts. But saying that it's out of control makes it sound like there are no solutions. This is a virus for which there are very clear solutions."