What's The Problem of Palm Sunday? DesiringGod Writer Explains What's The Problem of Palm Sunday? DesiringGod Writer Explains

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

On Palm Sunday, we reflect on how people spread out palm branches before Jesus as He entered Jerusalem days before He was crucified. But, there is more to what this day signifies than just the palm branches or people's profession about the "Messiah," says one Christian writer.

It wasn't the palm branches that were the problem for the Pharisees so much as what the people were saying, writes Jonathan Parnell, a writer and content strategist at Desiring God, in a blog post. The piece is the first in the ministry's Holy Week series called "The Final Days of Jesus" that is inspired by the new book of the same title by authors Justin Taylor and Andreas Kostenberger.

According to Luke 19:38, as Jesus entered Jerusalem, the people began rejoicing and praising God, shouting, "Blessed is the King who comes in the name of the Lord!" Some Pharisees tried to get Jesus to make the crowd stop, Parnell says. They asked Him to rebuke the people for giving Him a welcome reserved only for Israel's Savior.

"Jesus doesn't stop them, though. He says, instead, that if the people weren't saying it then the rocks themselves would cry out. Of course, Jesus is the Messiah. He has come to Jerusalem to save His people."

While the people – unlike the Pharisees – accepted Jesus as the "Savior" that day, their profession changed by the following Friday as they shouted, "Crucify him."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"The people wanted salvation and success, remember. Which means, they wanted the Messiah to march into the city and do hard business with Rome. They wanted to be free from Gentile oppression, even if by force, even if by threats and plagues and a split sea, as they recounted so well in their history. They wanted another exodus, one that expelled the Romans.

"Instead, what they got by Friday morning was a bloodied has-been, a man in Roman custody, rejected by their own leaders, standing next to an infamous criminal called Barabbas. They wanted an incomparable king, but they would see a beaten blasphemer. Or so they thought," the writer says.

Are we any better today? "If we know our hearts apart from grace, if we could listen in on this crowd, we'd hear our shouts along with theirs. We'd hear our praise, hollow as it were, and then, by Friday, 'ashamed we'd hear our mocking voice call out among the scoffers,'" he adds.

But there is hope as we observe the first day of Holy Week. "It is not the righteous, after all, who Jesus came to save, but sinners. Sinners like us," Parnell concludes.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit