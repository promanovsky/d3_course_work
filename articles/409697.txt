BALTIMORE (WJZ) — It’s a natural part of the Chesapeake that turns dangerous during warm weather–a flesh-eating bacteria in the bay that can mutilate and kill.

Alex DeMetrick reports there have been 16 infections this summer already.

Go on the Center for Disease Control and Prevention’s website and you can find images of Vibrio vulnificus. Pictures of what the bacteria does to flesh are too gruesome to show.

“It’s a vicious bacteria. If it gets starts in an arm or a leg, truly sometimes amputation is the only cure,” said Dr. Bill Howard, University of Maryland.

The Chesapeake Bay in the summer is perfect for Vibrio to grow and multiply. A cut or even a scrape is all it takes to get infected. It almost cost Rodney Donald his foot.

“My leg was burning and then I got chills real bad. I couldn’t stop shaking. They took me into emergency surgery right away and they started cutting where the bacteria had actually started eating the flesh,” Donald said.

Donald’s foot was saved. If Vibrio goes untreated: “Death. Yeah,” said Dr. Howard.

Because it lives in brackish and even saltier ocean water, Vibrio and the infections it causes are not unique to the bay.

“We just got off the boat and we were unloading,” said Jacob Ahlers.

When Ahlers got a small cut near his Florida home: “Nine-thirty the next morning, foot swollen, completely red, hot to the touch,” his mother, Donna Ahlers, said.

Jacob’s foot nearly tripled in size before antibiotics knocked the virus out.

In Florida, there have been 30 Vibrio infections this year. In the Chesapeake Bay area, there have been 16.

Whether it’s boating, fishing or swimming: “If you get a cut, if something happens, think vibrio and get urgent care. And I mean urgent. Not tomorrow. Same day,” Dr. Howard said.

Health experts also warn Vibrio can infect people by eating raw seafood and shellfish.

Other Local News:

[display-posts category=”local” wrapper=”ul” posts_per_page=”5″]