Abbey Gibb

KING-TV, Seattle

SEATTLE — The nation's second legal recreational marijuana market is opening in Washington on Tuesday, but it might not be what state voters expected when they legalized the sale of heavily taxed pot 20 months ago.

The state's Liquor Control Board issued 24 marijuana retail licenses Monday. Stores can open Tuesday if they're ready, but it wasn't clear how many will be. The Liquor Control Board was overwhelmed with nearly 7,000 pot-license applications, and reviewing them has been slow.

Colorado, which began recreational sales in January, also experienced a bumpy start that featured few outlets, tremendous demand, lines and quick sellouts. But many of those issues have been smoothed over.

In Seattle, Cannabis City will be the only location ready to go on Washington state's opening day.

"It's definitely exciting but also a little scary just having that much responsibility. All eyes are on us right now because we're the first," said Cannabis City Manager Amber McGowan.

She said the first pot delivery will come early Tuesday. Two-gram bags will sell for $54 each. But the shipment of just 2,265 small bags won't come close to enough product for the 5,000 people they expect to start lining up before doors open at noon.

While 79 growers have been licensed since Initiative 502 won voter approval, most don't expect to have their first shipment ready until late summer, the board said.

"Will there be shortages?" asks Randy Simmons, the board's legal-pot project manager. "The answer to that is yes."

Entrepreneurs who want to make pot-infused sodas, brownies or other treats must get approval for their products, and so far no one has. People like Alison Draisin, who currently makes pot-infused brownies and other treats for medical marijuana patients, will have new hoops to jump through before the general public gets a taste.

The edible-makers will have to have child-proof packaging. And the board doesn't want gummy candies or anything that would appeal to children. All edible-makers also need to have their kitchens pass a state inspection. Two have been tested so far: One failed, with results pending on the other.

"I wanted to see how I-502 was going to pan out before applying for a 502 license so if there's a second round we definitely would apply," said Draisin, who runs Ettalew's Medibles.

Contributing: John Bacon, USA TODAY