Financial services giant JPMorgan Chase & Co. (JPM) Friday reported a 19 percent fall in first-quarter profit, reflecting lower net revenue and higher provision for credit losses, despite a decline in expenses. Both earnings per share as well as revenues missed analysts' expectations. The shares are down about 4 percent in pre-market trading.

Looking ahead, Jamie Dimon, chairman and chief executive officer of the company said, "...we will dedicate extraordinary effort in 2014 adapting to the new global financial architecture, and we will continue to make significant progress on our control agenda."

For the first quarter, the company's net income declined to $5.27 billion or $1.28 per share from $6.53 billion or $1.59 per share in the previous year.

On average, 26 analysts polled by Thomson Reuters expected earnings per share of $1.40 for the quarter. Analysts' estimates typically exclude one-time items.

On a reported basis, total net revenues declined 8 percent to $22.99 billion from $25.12 billion a year earlier. On managed basis, total net revenue slid year-on-year to $23.86 billion from $25.85 billion last year. Twenty analysts estimated revenues of $24.53 billion for the quarter.

Non-interest revenue fell 12 percent to $13 billion. Net interest income was $10.9 billion, down 2 percent from last year.

Provision for credit losses climbed 38 percent to $850 million. Non-interest expense decreased 5 percent to $14.64 billion.

The firm's Tier 1 capital ratio was 12.1 percent, up from 11.6 percent a year ago. Return on tangible common equity was 13 percent, compared with 17 percent in the prior year.

Segment-wise, Consumer & Community Banking revenues declined 10 percent to $10.46 billion. Net interest income declined 3 percent, driven by spread compression in Credit Card, Auto and Consumer & Business Banking, and by lower mortgage warehouse balances.

Provision for credit losses in the segment climbed 49 percent from a year ago. Net income dropped 25 percent to $1.94 billion, due to lower net revenue and higher provision for credit losses.

Within the segment, Consumer & Business Banking reported net income of $740 million, an increase of 15 percent from last year. Net revenues were up 5 percent. Mortgage Banking net income was $114 million, a decrease of $559 million from the prior year.

In the Corporate & Investment Bank division, net revenue fell 15 percent to $8.61 billion. Provision for Credit losses surged 345 percent. Net income fell 24 percent to $1.98 billion.

Asset Management revenues increased 5 percent to $2.78 billion, while, net income dropped 9 percent to $441 million.

As announced earlier, the company said its board intends to increase the second-quarter common stock dividend to $0.40 per share from the current $0.38 per share.

The firm repurchased $0.4 billion of common equity in the first quarter and the board has authorized to repurchase $6.5 billion of common equity through the first quarter of 2015.

JPM closed Thursday's regular trading at $57.40. In the pre-market activity on Friday, the shares are down 3.75 percent.

For comments and feedback contact: editorial@rttnews.com

Business News