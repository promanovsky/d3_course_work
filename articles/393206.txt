Real estate website Zillow will buy competitor Trulia for $3.5 billion in stock, but Realtors shouldn't be too concerned about the combined company hurting business, said Tim Rood, a former Fannie Mae executive who is now partner and managing director of Collingwood Group.

"Realtors, they're independent contractors. They just want to win more customers. So if Zillow wants to spend all of its money advertising and Realtors can get more targeted, pre-qualified buyers, they're perfectly happy," Rood said in an interview with CNBC's "Street Signs."

However, he said there are a number of things about the Zillow-Trulia business model that will promote the high producers at the detriment of the low producers who only sell a few houses a year. That, in turn, can hurt the National Association of Realtors and the multiple listing services. About 80 percent of real estate sales go to 20 percent of the producers, but 100 percent of them pay dues to the National Association of Realtors and the multiple listing services, he noted.



"Some producers—two, three properties a year, it doesn't take much to kick them out and therefore you're going to lose potentially ... 80 percent of your revenue," Rood said.



Analyst Bradley Safalow of PAA Research, who is short shares of Zillow, said neither company has changed the game at this point.



"They already have vast consumer traffic yet very little has changed in terms of their lead quality, lead volume or even the underpinnings of how the residential real estate industry works and that's because at the end of the day, 80 to 98 percent of residential real estates are chosen based on referrals," he said.