PITTSBURGH (KDKA) — Could a headband be a fix for migraines?

The Food and Drug Administration is convinced by Belgian studies that this could be helpful to those who suffer from this common type of headache with throbbing pain, nausea and light and sound sensitivity.

“People who really suffer with migraine are frequently in the emergency room, because they don’t have something that sort of consistently helps,” says Allegheny General Hospital neurologist Dr. Andrea Synowiec. “And for those people, I anticipate this is something they’re very excited about.”

The FDA approved Cefaly, a battery-operated band that goes across the forehead with self-adhesive electrodes. The idea is to electrically stimulate for 20 minutes a day the trigeminal nerve, which senses pain in the face and parts of the head.

The approval is based on a clinical trial of 67 adults who had at least two attacks a month. After going without medication for three months, they got either the actual device or a placebo device.

Those who got the actual device needed less medication and had fewer days with migraine.

“It’s a novel way of looking at treatment, something that is non-pharmacologic, which, for some people, the pills just make them feel sick,” Dr Synowiec continues.

Also, a patient satisfaction study of more than 2,000 people in Belgium and France showed that 53 percent were happy with it and would buy one for continued use. It costs $300 in Canada.

Since it was just approved, doctors here haven’t had any experience with it.

“As larger groups of people are exposed to the treatment, it will become more apparent what is a big game changer and what really doesn’t have much of an impact,” says Dr. Synowiec.

Some people noticed a tingling feeling at the electrode sites. Some complained it made them sleepy. Some got a headache afterward.

RELATED LINKS:

More Health News

More Reports by Dr. Maria Simbra

Join The Conversation On The KDKA Facebook Page

Stay Up To Date, Follow KDKA On Twitter