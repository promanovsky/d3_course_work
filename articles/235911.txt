Facebook is adding technology to its mobile apps that recognises the music, movies or television shows its users are enjoying, in the company's latest move to give prominence to media and entertainment on its social network.

The new feature, which must be activated by the user and is off by default, attempts to recognise the music or video playing in the background any time a Facebook user composes a status update. It will also provide a 30-second preview for the user's friends.

Listen up: Facebook's new feature automatically detects what you are listening to or watching.

"When writing a status update – if you choose to turn the feature on – you'll have the option to use your phone's microphone to identify what song is playing or what show or movie is on TV," Facebook product manager Aryeh Selekman said.