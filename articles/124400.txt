American Express said Wednesday its net income climbed in the first quarter, helped by higher spending by its cardholders even as cold winter weather gripped much of the country.

The New York-based company said its cardholders spent 6 percent more in the first three months of the year, driving up revenue for both its U.S. and international card businesses.

"While consumers remain cautious about taking on additional debt, we continued to see a modest increase in card member loan balances," AmEx CEO Kenneth Chenault said.

After the stock market closed Wednesday, AmEx said its net income rose 12 percent to $1.43 billion, or $1.33 per share, in the three months ending March 31. That compares with net income of $1.28 billion, or $1.15 per share, in the same period of 2013.

Revenue rose 4 percent to $8.2 billion from $7.88 billion the year before.

AmEx's latest results beat Wall Street's expectations for profit but fell short on revenue. Analysts had expected earnings of $1.30 per share on $8.35 billion in revenue in the latest quarter, according to the data provider FactSet.

AmEx's credit-card holders tend to be more affluent than other credit card users, which has helped bolster the company throughout the slow economic recovery.

Unlike Visa and MasterCard and other credit-card companies, American Express issues its own cards. So when cardholders charge more on their AmEx cards, the company receives more fees from stores as well as more income from interest.

Shares in AmEx ended the regular trading day up $1.36, or 2 percent, to $87.40. In after-hours trading, they dropped 45 cents to $86.95 following the release of the earnings report.

Another credit-card issuer, Capital One Financial, turned in slightly higher first-quarter profit late Wednesday.

The McLean, Va.-based Capital One reported net income of $1.14 billion, or $1.96 per share, for the three months ending March 31. That compares with net income of $1.04 billion, or $1.77 per share, in the same period of 2013. Revenue dipped 3 percent to $5.37 billion from $5.55 billion.

On average, analysts expected earnings of $1.69 per share on $5.45 billion in revenue, according to FactSet. Shares in Capital One slipped 6 cents to $75.10 in after-hours trading.