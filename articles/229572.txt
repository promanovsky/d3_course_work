Current electronic medical implants are an impressive feat of modern science, but the large batteries and difficult recharging processes dictate their use and placement in the body.

Researchers at Stanford University have developed a new method for transferring power into the body, which could lead to tiny, wireless medical implants no larger than a grain of rice.

Ada Poon, lead researcher and assistant professor of electrical engineering at Stanford, created a new way to control electromagnetic waves inside the body, combining the characteristics of near-field and far-field waves.

In practice, you could charge an implant by holding a power source about the size of a credit card above the implant, outside of the body.

"We need to make these devices as small as possible to more easily implant them deep in the body and create new ways to treat illness and alleviate pain," Poon said.

The researchers have tested the implants and charging technology in a pig and a rabbit, and now, the team is preparing to test the system on humans.

Still, it could be some time before we see the technology in use; according to the university, it may take “several years” to satisfy safety requirements.

The findings were published Monday in the Proceedings of the National Academy of Sciences