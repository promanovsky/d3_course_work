Chicago (TheStreet) -- U.S.-China flying is the wave of the future. It is expanding rapidly, never more rapidly than this month. Airports welcome the service, passengers love new non-stop destinations and airline crews tend to be proud to work the flights.

It's just too bad that in general air service between the U.S. and China is not yet profitable.

Eleven days in June were the most important period for China/U.S. aviation since the historic 2007 bilateral agreement vastly expanded flying between the two countries.

On June 9, United (UAL) - Get Report began thrice-weekly service between San Francisco and Chengdu, the first commercial aviation flight ever between North America and the interior to China. On June 11, American (AAL) - Get Report added flights from Dallas to Hong Kong and Shanghai. On June 16, Delta (DAL) - Get Report launched Seattle-Hong Kong service. On June 20, Chinese carrier Hainan began Boston-Beijing service, which will be daily for much of the summer.

Not to mention the Honolulu-Beijing service that Hawaiian (HA) - Get Report began on April 16, three months after Air China began Beijing-Honolulu service.

"Everybody wants more access to China," said aviation consultant Bob Mann. "The conundrum is that there's already too much. The amount of capacity in the market is outstripping the demand."

Mann compared China flying to gold mining. "You dig a hole for a while. Eventually you hopefully reach your goal: making money."

In the four years from August 2010 through August 2014, the number of flights between China and Hong Kong, and the U.S., increased by 73% and capacity as measured by available seat miles grew by 58%, according to statistics compiled from schedule databases. Hawaii service is not included in the statistics.

The biggest increase was at Hainan Airlines, which boosted capacity by 415% during the four-year period, adding flights to Chicago, Seattle and Boston. Hainan was adding off a small base. In August it will fly 80 flights between China and the U.S. and offer about 105,000 ASMs, eighth of the nine carriers in the market.

United and Cathay Pacific, which operates a Hong Kong hub, offer the most flights to China. With the addition of the San Francisco-Chengdu, United serves 11 routes with four Chinese destinations: Beijing, Chengdu, Shanghai and Hong Kong.

The fastest-growing carriers in the market have been Hainan Airlines, with capacity up 415% in four years; China Southern, up 294%, and American, up 176%.

Hainan began Beijing-Boston service on Friday. The carrier also flies from Beijing to Chicago and Seattle, as well as Toronto, but it could not start Boston until the Boeing (BA) - Get Report 787 became available.

Joel Chusid, U.S. executive director for Hainan, said future new flights are likely to serve secondary destinations such as Chengdu in China and Boston. "You are seeing new service to secondary cities in China and to a lesser degree in the U.S.," he said. One reason is that Chinese carriers are prohibited from competing with one another on most routes.

Asked whether Hainan makes money on its U.S. flights, Chusid said, "In the beginning we did not, but the situation is improving because the market is growing. The company is profitable, but I would say the domestic routes subsidize the international. International is more of a challenge. Some make money and some are developmental."

For instance, Chusid noted, Hainan ended its Beijing-Zurich service in October. But he said some of the North America flights are now profitable and the others are promising.

American CEO Doug Parker told reporters June 11 that American's Asia routes are investments, not profit centers.

"We've seen nice improvement, and indeed our Asia revenue per ASM [available seat mile] over the past year has grown at a rate in excess of the industry, so we feel very good about the future prospects," Parker said, according to The Dallas Morning News. "But these, like a lot of routes at airlines, are investments and haven't yet been profitable," he said.

United's first-quarter Pacific passenger revenue fell by 6.3%. As of the first quarter, United had 7% of its total capacity in China/Hong Kong markets, while America and Delta each had about 2% of capacity there, according to Deutsche Bank analyst Mike Linenberg.

"There's no doubt that competitive capacity pressures from the U.S. to Asia, particularly to China, where we are the largest U.S. airline by far, have ... pressured our unit revenue," said CEO Jeff Smisek, during United's first-quarter earnings call.

"That said, we make good money in Asia today, even with that pressure," Smisek said. "We expect to continue to make good money to Asia. We expect to not only maintain our lead there, but we would have opportunities to grow in the new markets."

Meanwhile, Delta said its first-quarter China results improved, although it did not say whether China is profitable. "One bright area in the Pacific portfolio has been China, with all of our China market showing year-over-year unit revenue gains despite a 15% capacity increase," said Delta President Bastian said on the carrier's first-quarter earnings call.

Written by Ted Reed in Charlotte, N.C.

To contact this writer, click here.

Follow @tedreednc

Disclosure: TheStreet's editorial policy prohibits staff editors, reporters and analysts from holding positions in any individual stocks.



United Pilots: We Flew to Chengdu on a Boeing 787 and Loved It

United Leadsin China ButAmerican and DeltaWant Bigger Shares

Biggest US Airways Union Backs Contract Deal, Breaks Logjam at American