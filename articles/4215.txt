Do you often play games, check emails or respond to office calls on your cell phone while with family on a dinner? This phone addiction can damage your emotional bonding with kids soon.

In a first-of-its-kind study to examine how children behave with adults on phones, researchers from Boston Medical Centre studied 55 parents while they had food at a restaurant with children.

They found that one in three parents used their phones almost continuously during the meals.

“A lack of eye contact and interaction with children can reduce the bond with them,” alerted Jenny Radesky, a behavioural paediatrics expert.

Almost 73 per cent of the adults used their phone at least once during each meal.

More than 15 per cent used their phones towards the end of the meal and continued to use it until they left the restaurant, said the study.

When parents spent a long time looking at their phones, their children had a tendency to seek attention.

“Parents who were highly absorbed in their devices seemed to have more negative or less engaged interactions with children,” Radesky was quoted as saying.

Children became distracted and wanted to know why the parents were using their phones during the family time.

This could cause problems with the child’s development and reduce the level of bond between a parent and a child, said the findings, to be published in the April issue of the journal Pediatrics.