Bandits hit Samsung factory in Brazil

Share this article: Share Tweet Share Share Share Email Share

Rio de Janeiro - Bandits attacked a Samsung Electronics factory near Sao Paulo late on Sunday and held workers hostage while they stole truckloads of smartphones, tablets and notebook computers that the company valued at $6.3-million. The dramatic heist, carried out by armed robbers who hijacked a shuttle used by factory employees, took place overnight during nearly four hours in Campinas, an industrial hub in the south-eastern state of Sao Paulo. A spokesman for the state's public security secretariat said the bandits made off with seven trucks laden with more than 40 000 Samsung products. State police are investigating the attack, he added, but said no suspects have yet been identified. A spokesman for Samsung in Sao Paulo confirmed the heist. He said roughly 100 employees were present at the factory during the robbery, but declined to give further details. “We have co-operated fully with the police investigation that is under way and will do our best to avoid any sort of repeat incident,” said Samsung Eletronica da Amazonia, the manufacturing unit of the South Korean electronics giant in Brazil, in a statement.

The police report, as related to Reuters by the spokesman for the state security secretariat, reads like the script of a daring crime movie.

After stopping the shuttle on its way to the factory, seven armed assailants took over the vehicle while colleagues took all but two of the eight employees originally on board to a remote location, where they were set free.

Just before midnight, the assailants proceeded with the two other employees to the factory, where they disarmed security guards, gathered the rest of the plant's employees and made sure none of them could communicate with the outside.

With the coast clear, the attackers then allowed 13 other culprits, driving the getaway trucks, into the factory. The robbers, communicating with one another by radio and cellphones, then proceeded to load the vehicles with the loot.

Using stills allegedly taken from the factory security cameras, local news media showed images of the alleged culprits, dressed in black and wearing dark headgear, roaming about the factory and pushing crates of goods toward the trucks with pallet loaders. - Reuters