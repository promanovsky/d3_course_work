AURORA (CBS4) – She’s been cleared to return to school in Grand Junction with her shaved head, but now a little girl has brought support for her friend with cancer to the Denver area.

Kamryn Renfro was told to stay home from school Monday for violating her school’s dress code. She shaved her head to support her friend, Delaney Clements, who has cancer.

Even though there has been a lot of media attention, Delany’s friends are much more focused on her getting better as she checked into Children’s Hospital Wednesday afternoon.

Delaney may have had a lot of questions about school rules, but she’s confident about how close friendships work.

“I did it because she’s my best friend and I didn’t want her to be the only one laughed at,” Kamryn said.

Delaney is one very grateful cancer patient.

“I was really happy that there was somebody there for me so I wasn’t alone because people would laugh at me and stare at me sometimes,” Delaney said.

RELATED: School Temporarily Suspends Girl Who Shaved Head To Support Friend With Cancer

But there was one rule that had the best friends very confused.

“She shaved her head for me to support me and be there for me and she got kicked out of school because she didn’t have any hair,” Delaney said.

Caprock Academy said Kamryn violated the school’s dress code and would not be allowed back to class. Kamryn had to spend the entire day Monday on the playground. The school’s board of directors overturned the decision Tuesday night.

Delaney lost her hair after 3 ½ years of chemotherapy, 28 rounds of radiation, a bone marrow transplant, and a disease that seems to just not give up.

“It’s called neuroblastoma and it’s Stage 4. It attacks the central nervous system and it’s all in your bone marrow,” Delaney said.

Delaney hasn’t lost her spirit and she certainly hasn’t lost her close friends.

“It feels amazing inside. I’m just so happy that there’s somebody there for me,” she said.

Kamryn and two other of Delaney’s friends are in Aurora at Children’s Hospital as a force for Delaney as she goes in for yet another treatment.

“Bald is beautiful and your hair doesn’t tell your personality,” Delaney said.

The well-behaved girls say breaking one school rule won’t break them up.

“I feel great,” Delaney said.

When asked about all the notoriety, Delaney said, “I never knew we could become so famous just for being bald.”