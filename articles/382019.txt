South African Nobel Prize-winning author Nadine Gordimer, an uncompromising moralist who became one of the most powerful voices against the injustices of apartheid, has died, her family said on Monday.

Gordimer, who was awarded the Nobel Prize for Literature in 1991, died peacefully at her Johannesburg home on Sunday evening in the presence of her children, Hugo and Oriane, a statement from the family said. She was 90.

"She cared most deeply about South Africa, its culture, its people and its on-going struggle to realize its new democracy," the statement said.

Regarded by many as South Africa's leading writer, Gordimer was renowned as a rigid moralist whose novels and short stories reflected the drama of human life and emotion in a society warped by decades of white-minority rule.

Many of her stories dealt with the themes of love, hate and friendship under the pressures of the racially segregated rule that ended in 1994, when Nelson Mandela became South Africa's first black president.

I cannot simply damn apartheid when there is human injustice to be found everywhere else - Nadine Gordimer, shortly before winning the Nobel Prize for literature

A member of Mandela's African National Congress (ANC) - banned under apartheid - Gordimer used her pen to battle against the injustices of white rule for decades, earning her the enmity of sections of the establishment.

But Gordimer, a petite lady with a crystal-clear gaze, did not restrict her writing to a damning indictment of apartheid, cutting through the web of human hypocrisy and deceit wherever she found it.

“"I cannot simply damn apartheid when there is human injustice to be found everywhere else," she told Reuters shortly before winning her Nobel prize.

Despite her hatred of the apartheid, Gordimer – seen here with Nelson Mandela – remained proud of her heritage. (Radu Sigheti/Reuters)

In later years, she became a vocal campaigner in the HIV/AIDS movement, lobbying and fund-raising on behalf of the Treatment Action Campaign, a group pushing for the South African government to provide free, life-saving drugs to sufferers.

Novels banned by apartheid censors

The daughter of a Lithuanian Jewish watchmaker, Gordimer started writing in earnest at the age of nine.

A lonely childhood triggered an intense study of the ordinary people around her, especially the customers in her father's jewellery shop and the migrant black workers in her native East Rand outside Johannesburg.

A teenage naivety was eventually replaced by a sense of rebellion and as her talent and reading public grew, her liberal leanings earned her the reputation of a radical.

Eventually the government censors clamped down and banned three of her works in the 1960s and 1970s, despite her growing prestige abroad and her acceptance as one of the foremost authors in the English language.

The first book to be banned was A World of Strangers, the story of an apolitical Briton drifting into friendships with black South Africans and uncovering the schizophrenia of living in segregated Johannesburg in the 1950s.

In 1979, Burger's Daughter was banished from the shelves for its portrayal of a woman's attempt to establish her own identity after her father's death in jail makes him a political hero.

Despite Gordimer's place in the international elite, she maintained a passionate concern for those struggling at the bottom of South Africa's literary heap.

“"It humbles me to see someone sitting in the corner of a township shack he shares with 10 others, trying to write in the most impossible of conditions," she said.

Despite her hatred of the apartheid, Gordimer remained proud of her heritage and said she only once considered emigrating - to nearby Zambia.

“"Then I discovered the truth, which was that in Zambia I was regarded by black friends as a European, a stranger," she said. "It is only here that I can be what I am: a white African."