The smile says it all! Kim Kardashian shared a throwback picture of herself, Anna Wintour, and daughter North West via Instagram on Thursday, May 1. The moment it was taken? When the Keeping Up With the Kardashians reality star was officially told that she and fiance Kanye West would be gracing the cover of Vogue!

"#TBT when Anna met North! Oh and told us about our Vogue shoot!" the 33-year-old wrote.

"#BestDayEver #Vogue #NorthOnlyPayingAttentionToHerDaddyTakingThePic," she added. (The snapshot appears to have been taken on Nov. 19, five months before their April issue release.)

In the snapshot, the trio is sitting down for a meal at what appears to be the Mercer Hotel lobby in New York City. Kardashian is dressed in a brown fur coat while holding her baby girl in her arms. The Vogue Editor-in-Chief, meanwhile, has her arms crossed and is (gasp!) smiling while wearing black sunglasses.

Kardashian and West, 36, finally got their wish when they made the cover of the fashion bible last month. At the time, Wintour made a strong argument on why she decided to take the plunge.

PHOTOS: The many faces of Anna Wintour

"Part of the pleasure of editing Vogue, one that lies in a long tradition of this magazine, is being able to feature those who define the culture at any given moment, who stir things up, whose presence in the world shapes the way it looks and influences the way we see it," the 64-year-old wrote. "I think we can all agree on the fact that that role is currently being played by Kim and Kanye to a T. (Or perhaps that should be to a K?)"