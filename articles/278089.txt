AT&T has asked the FCC to let it start a 'transition' of its networks, moving to all 'Internet-based' (IP) services, with 'trials' to start in Alabama and Florida. AT&T also claims that it needs to merge with Direct TV as it will not only start rolling out 'fiber optic cities' but will also supply broadband service to rural areas that are not being served with broadband today.

But we found disturbing proof in the last few months that AT&T's own statements about their proposed plans directly contradicts previous commitments to provide broadband -- or worse, it exposes a case of deception.

Truth is, AT&T can 'say anything', which has been the company's modus operandi for decades. The company's track record clearly shows that they can say whatever they want, and it has helped to push through mergers or deregulation -- with broadband always being the carrot.

Case in point: By 2007, AT&T should have completed upgrading 100% of their 22 states to broadband, based on the AT&T-BellSouth merger. And in 2004, AT&T told the FCC that it would start deployment of 100 Mbps fiber-to-the-home services.

But here's the problem; the AT&T IP Transition information and the Direct TV merger press release exposes the fact that AT&T never completed the 100% broadband deployment to everyone, even with wireless -- and it appears they may have committed perjury by telling the FCC that they had.

Based on AT&T's recent statements, it is time to halt AT&T's IP Transition and merger plans and have the FCC and states should start investigations about previous commitments because AT&T's new plan is all about shutting off the wires and replacing them with, well, 'say-anything' wireless solutions.

Here are a few examples from AT&T's previous commitments and the current Direct TV and IP Transition information -- all from the master of the 'Say Anything': AT&T.

Commitment: 100% of AT&T's 22 States Should have Already been Upgraded to Broadband

In 2007, AT&T merged with BellSouth and the new merged company claimed it would have 100% of their 22-state territory capable of broadband, albeit slow at 200 kbps in one direction, but it was the 'standard' set by the FCC.

I decided to just cut out the actual text from the AT&T-BellSouth merger agreement as someone might suggest that I was making this up.

And AT&T signed documents that they had completed their commitments and yet an article in Huffington Post in 2012 found that AT&T had not fulfilled its promises.

AT&T Claimed It Would be Doing Fiber-to-the-Home with 100 Mbps Services in 2004

In 2004, then-Chairman of the FCC, Michael Powell, closed the fiber optic-based AT&T and Verizon networks to direct competition (known as 'unbundling') because SBC and BellSouth (now both part of AT&T) were about to deploy 100 Mbps, fiber-to-the-home or fiber-to-the-curb -- starting in 2004. Now the head of the NCTA, the cable association, Powell was either gullible or failed to bother with facts, or...

Powell gives his reason for closing the networks as 'commitments' for 100 Mbps, fiber-optic based services by SBC (now AT&T), October 2004

"In my separate statement to the Triennial Review Order and in countless other statements during my seven years at the Commission, I have emphasized that 'broadband deployment is the most central communications policy objective of our day'. Today, we take another important step forward to realize this objective.... By removing unbundling obligations for fiber-based technologies, today's decision holds great promise for consumers, the telecommunications sector and the American economy. The networks we are considering in this item offer speeds of up to 100 Mbps and exist largely where no provider has undertaken the expense and risk of pulling fiber all the way to a home. "SBC has committed to serve 300,000 households with a FTTH network while BellSouth has deployed a deep fiber network to approximately 1 million homes. Other carriers are taking similar actions."'

Note: According to the FCC: "FTTH" is "Fiber to the Home", where "the fiber optic wire starts at the customer's location". "FTTC", "Fiber to the Curb'" was defined as 500 feet from a customer's premises.

"In granting such relief, we first define FTTC loops. Specifically, a FTTC loop is a fiber transmission facility connecting to copper distribution plant that is not more than 500 feet from the customer's premises."

AT&T's U-Verse is not 'fiber-to-the-home' or curb but is actually 'copper-to-the-home' and uses the existing, aging copper wires that are part of the state utility, while the fiber is somewhere within ½ mile, not 500 feet from the home. And yet, the current networks are closed to competitors based on this failure by Powell and the next FCC administration to actually track these SBC and BellSouth 'commitments'.

AT&T-Direct TV Deal Shows that They Never did the Upgrades

The AT&T-Direct TV merger press release of May 19th, 2014 claims that a major reason for the merger is that it will bring broadband to 15 million customers in AT&T's territories that do not have high speed service today.

"15 Million Customer Locations Get More High Speed Broadband Competition. AT&T will use the merger synergies to expand its plans to build and enhance high-speed broadband service to 15 million customer locations, mostly in rural areas where AT&T does not provide high-speed broadband service today, utilizing a combination of technologies including fiber to the premises and fixed wireless local loop capabilities."

If AT&T is already supposed to have 100% completed, how can 15 million locations -- at least 20% of all AT&T areas, not already have high speed broadband?

AT&T's VIP Announcement Shows that 25% May Never Have Been Upgraded with Broadband.

In AT&T's 2013 announcement of its "VIP plan", AT&T made it clear that they had failed to properly upgrade and maintain about 25% of their entire 22 states over the last two decades.



"AT&T plans to expand and enhance its wireline IP network to 57 million customer locations (consumer and small business) or 75 percent of all customer locations in its wireline service area by year-end 2015.

This network expansion will consist of:

U-verse. AT&T plans to expand U-verse (TV, Internet, Voice over IP) by more than one-third or about 8.5 million additional customer locations, for a total potential U-verse market of 33 million customer locations¹. The expansion is expected to be essentially complete by year-end 2015.

"In the 25 percent of AT&T's wireline customer locations where it's currently not economically feasible to build a competitive IP wireline network, the company said it will utilize its expanding 4G LTE wireless network -- as it becomes available -- to offer voice and high-speed IP Internet services .

Do the math. AT&T has 76 million 'locations' according to their own statements. (Note: If 75% equals 57 million then 100% is 76 million.) AT&T will have a total of 33 million locations by the end of 2015 -- which means that AT&T will only have about 40% of their 22 states covered with TV competition.

And it also states that 25% won't ever get properly upgraded -- and this obviously was the case in the year 2007.

The IP Transition Trial Documents Prove AT&T Never Completed the AT&T-Bellsouth Commitments

AT&T IP Transition Trial documents state that there are areas that were never upgraded and there are areas that may even be 'shut off' and forced onto wireless. And talk about a real 'we-don't-care-about-our-customers' plan, whole areas are supposed to be given "Wireless Home Phone", which is like the Verizon wireless product "Voice Link", that was offered on Fire Island after the Sandy storm as a replacement for a wired service in 2013, and there was a revolt.

AT&T writes that there are areas where they have no current solution, even with wireless products:



"The living units in the "IP Wireline Red and IP Wireless Red" category will not have an IP-based alternative available from AT&T. AT&T continues to consider options for these living units."

And then they supply this chart and redacted information to reassure us that we should 'trust them'.

And even in this trial, AT&T hasn't figured out how to serve 4%. Does this mean that AT&T never upgraded this 4%, even though under the AT&T-BellSouth commitment was for 100%? (Carbon Hill Alabama is an AT&T IP transition trial site.)

Did AT&T commit perjury in the AT&T-BellSouth agreement as it is clear that they never completed 100% with 200 kbps broadband service?

Even the "Wireless Home Phone"-- VoiceLink Chart is Deceptive

But for pure snake oil, even in the details of AT&T' IP Transition Trial documents, AT&T clearly doesn't believe in telling the truth. This chart shows that AT&T, for their Wireless Home Phone product, has essentially given a totally false collection of answers that is exposed when one reads the fine print. (Notice that there are asterisks on almost every 'Yes' answer). If this chart looks familiar it should -- it is identical to Verizon's' Voice Link applications' chart.

Here are the notes for this chart - Nothing was truthful.



"Notes a-h: Currently, Wireless Home Phone and Wireless Home Phone and Internet, which are CMRS, comply with the Commission's existing 911 requirements for CMRS, and do not provide E-911 with street address. Nor does Wireless Home Phone and Wireless Home Phone and Internet currently support alarm monitoring, medical alert and credit card validation applications. However, AT&T currently is developing enhancements that will provide all of these applications which we plan to introduce in the [CONFIDENTIAL - NOT FOR PUBLIC DISCLOSURE]. "Notes i-j: 800 number service is not supported by Wireless Home Phone/Wireless Home Phone and Internet for consumer customers.

Notes k-m: U-verse Voice, Wireless Home Phone and Wireless Home Phone and Internet do not support collect calling."

How many customers are going to be 'shut off'?

The AT&T- Direct TV merger should not be considered and the AT&T IP transition should be halted and investigations started into whether AT&T mislead regulators to close down the networks or during merger process that allowed AT&T to merge with BellSouth.

Moreover, we will come back to AT&T's IP Transition and trials.