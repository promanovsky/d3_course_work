John Travolta came by jet. Justin Bieber made a beeline to the bar. Leonardo DiCaprio mingled with Marion Cotillard. And Robin Thicke sang about his ex.

Again.

Thursday night’s annual amfAR event — an auction, dinner, and after-party — reaffirmed its reputation as the celebrity highlight of the Cannes Film Festival, with a glitzy guest that included Oscar winners, Hollywood legends, top singers and perhaps every supermodel in existence.

The charitable event, which takes place a stone’s throw away at the five-star Hotel du Cap-Eden-Roc in Cap d’Antibes every year, raised a record $35 million (Dh128.6 million) for AIDS research. Part of that haul came from the sale of Damien Hirst’s gilded woolly mammoth skeleton, the Gone but not Forgotten work, encased in a glass box trimmed with gold, was one of three works from the British artist put up for auction.

Bollywood diva Aishwarya Rai Bachchan attended the gala along with her actor husband Abhishek Bachchan. Dressed in a shimmery Armani Prive gown, the 40-year-old actress sported bold red-lips, turquoise eyes laden with multiple coats of mascara and wore her hair open with soft curls. Abhishek also looked dapper in black bandhgala jacket as he posed with his wife on the red carpet.

The evening’s host, Sharon Stone, lauded the crowd for coming together to help eradicate AIDS.

“I know all of you have lost someone to AIDS,” she said. “Now we are at the beginning of the end of AIDS.”

But they also came to hobnob and party. Here are the highlights of the starry night:

The charity catwalk

The Cannes red carpet had competition as guests — famous and otherwise — brought out their best for the evening. There were gowns billowing over with tool, short cocktail dresses, and beaded and bedazzled creations. Some women were bold, with plunging necklines and risque outfits that barely contained body parts. Even some men got creative at the black-tie event — one sported a bow-tie that was gold.

Paris Hilton wore a beaded floor-length ensemble. The cool temperatures and occasional rain made her nervous about her choice (the cocktail hour and part of the after-party is outdoors). “I was sent about 50 gowns,” she said. “I was actually going to wear white but then it was raining and I got nervous, so I changed to this gold dress.”

Euros, not dollars

Stone need several reminders that the items she was auctioning off were more expensive than she was announcing to the audience. As the actress played auctioneer, she frequently bellowed out the price of the item in dollars — and was quickly corrected by a man on a mic who stressed it was in euros. The difference between the two is considerable. An item selling for $1 million would be about €734,000. But an item selling for €1 million would be nearly $1.4 million.

The fashion world cares

Former French Vogue editor Carine Roitfeld organised a 42-piece fashion show under the theme “red,” the colour symbolising the fight against the disease. Top international houses design the gowns, including Chanel, Christian Dior, Louis Vuitton and Dolce & Gabbana. The looks, showcased by some of the world’s top models including Karlie Kloss, Joan Smalls and Chanel Iman, were up for auction.

To boost bidding, Stone also threw in the revealing Pucci dress she was wearing. “Make it 43,” she bellowed. The collection sold for $4.8 million.

Karl Lagerfeld also put his iconic fingerless gloves up for auction, but they fetched considerably less.

Carla Bruni, auctioneer

“Ladies, ladies, ladies, look at my necklace,” purred Carla Bruni. The 46-year-old ex-supermodel and former French first lady offered up her Bulgari serpentine necklace for the hammer; it sold for $545,000. Bruni heaped praise on Stone’s auctioning skills: “Every time she says something, she makes the people wanting to buy and to participate in this evening, so I think she’s the best ambassador that the amfAR can have.”

Space travel with DiCaprio

A planned voyage into space with DiCaprio was meant to have been fully booked. Luckily, amfAR invitees were informed of a space seat on the inter-planetary expedition planned for 2015. It sold for nearly $1 million.

Pining over Paula

Robin Thicke can’t stop making references to his soon-to-be ex-wife, Paula Patton. The R&B singer, who closed the night with a performance, started off his set referencing how it was inspired by the woman he met when he was 16 (the pair first started dating when they were in their teens). Later, he sang his new tune Get Her Back. But his performance was far from downbeat. Thicke sang Michael Jackson’s classic Rock With You before closing out his set with his own smash, Blurred Lines, which brought a host of guests onto the stage to dance with him, including Stone.

Other performers included opera singer Andrea Bocelli, who sung a warbling Italian version of My Way by Frank Sinatra; The Man singer Aloe Blacc, who had the crowd on their feet dancing; and Lana Del Ray.

After-party

The post-celebration may have been more entertaining than the actual event. Instead of heading home after the event wrapped around 1am, guests headed to the picturesque pool area. Jessica Chastain, Rosario Dawson, Dita Von Teese and model Aymeline Valade, who played Betty Catroux in the Cannes competition film Saint Laurent, were among those holding court while others danced to under the moonlit sky. The party went on into the wee morning hours. Quentin Tarantino was seen walking into the entrance around 3am.