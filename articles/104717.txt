On the heels of last year's "Beauty Sketches" video, Dove posted another social experiment to YouTube on Wednesday that teaches a powerful lesson about how we perceive ourselves.

A series of women were invited to be a part of a research group to test a product called RB-X — a patch worn directly on the arm. Participants were asked to leave the patch on for 12 hours a day (for two weeks) and record a video diary about how they felt throughout the process.

SEE ALSO: Dove Short Film Embraces 'Selfies' to Redefine How We Perceive Beauty

Participants weren't told what was inside the RB-X patch, but knew it was supposed to enhance the way they see their own beauty. Although most didn't notice a change at first, many later said they became felt more confident, social and willing to try new things, such as clothes they wouldn't have worn in the past.

"It's been a life altering experience," one participant said. "I'd love for people to have the type of change I've had by trying the beauty patch."

Toward the end of the video, the researchers show each person what's inside the RB-X patch. You might be able to guess where this is headed early on, but the big reveal is touching. Watching their reactions is alone worth your time.

"We have heard from thousands of women about how their complicated relationship with beauty affects their overall confidence and happiness," said Jennifer Bremner, brand building director of skin cleansing for Dove. “By illustrating through the Dove: Patches film that a positive state of mind and openness can help them feel more beautiful, we hope to inspire all women and help change the way they see themselves.”