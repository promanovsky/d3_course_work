Carl Icahn just made a few quick bucks at the dollar stores — a few hundred million, that is.

The cage-rattling billionaire reaped about $175 million Monday as deep-discount retailer Family Dollar agreed to be acquired by rival Dollar Tree for $8.5 billion.

That’s because Icahn, who lately has prodded Family Dollar to sell itself, had amassed a 9.4-percent stake in Family Dollar less than two months earlier by scooping up $265.8 million in stock and options.

Family Dollar’s Monday sale price equated to $74.50 a share, but the options gave Icahn the right to buy Family Dollar stock at $38 a share, allowing the Wall Street tycoon to book a tidy profit.

Billionaire Nelson Peltz, a longtime Family Dollar investor with a 7.4-percent stake, whose son-in-law Edward Garden sits on the retailer’s board, threw his support behind the Dollar Tree deal on Monday.

Peltz in 2011 had tried unsuccessfully to jumpstart a bidding war for Family Dollar by offering $60 a share. At the time, Chairman and CEO Howard Levine resisted a sale of the company his father founded.

But recently, Levine has softened his stance, with some investors noting he has been playing plenty of golf despite his embattled position.

Under the merger agreement, Levine will become an employee of Dollar Tree while continuing to run the Family Dollar stores.

Icahn on Monday said he still sees “a handful of potential buyers” that he believed would be a better fit for Family Dollar.

“(We) are hopeful that one or more of them will surface,” Icahn said in a statement.

Icahn had previously touted Family Dollar’s larger rival, Dollar General, as a possible acquirer. A Credit Suisse report last fall estimated Dollar General could pay as much as $90 to $100 a share for Family Dollar. But Dollar General recently passed on the opportunity to bid for Family Dollar, insiders said.

Dollar General CEO Rick Dreiling, who had long opposed a deal with Family Dollar, plans to retire next year and a replacement hasn’t yet been named.

Dollar General likewise has been buying back hundreds of millions of its own stock in recent months, depleting its ability to fund an acquisition.

Meanwhile, the smaller Dollar Tree chain is taking on a heavy debt load to fund the merger, and its pockets aren’t deep enough to raise its offer significantly.

Accordingly, investors on Monday placed a modest bet on the prospect of a bidding war for Family Dollar, with the shares closing at $75.74 — $1.24 above the price of the deal.