Ebola can kill victims within days, causing severe fever and muscle pain, vomiting, diarrhoea and, in some cases, organ failure and unstoppable bleeding.

London: Fears that the west African Ebola outbreak could spread to other continents grew on Wednesday with European and Asian countries on alert and a leading medical charity warning the epidemic was out of control.

Doctors Without Borders (MSF) said the crisis gripping Guinea, Liberia and Sierra Leone would only get worse and warned there was no overarching strategy to handle the world's worst outbreak of the disease.

US Christian charity Samaritan's Purse was temporarily withdrawing its non-essential staff from Liberia, it said, citing regional "instability and ongoing security issues".

Hong Kong announced quarantine measures for suspected cases, although one woman arriving from Africa with possible symptoms tested negative, while the EU said it was ready to deal with the threat.

The International Civil Aviation Organization (ICAO) has held talks with global health officials on potential measures to halt the spread of the disease.

In Britain, where one person has tested negative for the disease, Foreign Secretary Philip Hammond said it was regarded as "a very serious threat".

An emergency meeting had decided that the best approach was to provide "additional resources to deal with the disease at source" in West Africa, he added.

Ebola can kill victims within days, causing severe fever and muscle pain, vomiting, diarrhoea and, in some cases, organ failure and unstoppable bleeding.

Since March, there have been 1,201 cases of Ebola and 672 deaths in Guinea, Liberia and Sierra Leone, according to the World Health Organization (WHO).

The US Peace Corps announced Wednesday it was pulling hundreds of volunteers from the three countries.

There are currently 102 Peace Corps volunteers in Guinea working on agriculture, education and health, 108 in Liberia and 130 in Sierra Leone.

EU 'equipped and ready

The European Union is equipped and ready to treat victims should the deadly virus be found in its 28 member states, an EU source said in Brussels.

"We cannot rule out the possibility that an infected person arrives in Europe but the EU has the means to track and contain any outbreak rapidly," the source said.

The isolation and negative testing of a suspected case in Valencia in Spain showed that the "system worked", added the source.

"The level of contamination on the ground is extremely worrying and we need to scale up our action before many more lives are lost," said EU Humanitarian Aid Commissioner Kristalina Georgieva.

In Hong Kong, a densely populated city previously scarred by disease outbreaks such as the 2003 SARS epidemic, health officials confirmed they would quarantine as a precautionary measure any visitors from Guinea, Sierra Leone and Liberia who showed fever symptoms.

One woman arriving in the southern Chinese city from Africa, who showed symptoms including fever and vomiting, has tested negative for Ebola.

Australia said Thursday it was well prepared in the unlikely event that the Ebola virus reached its shores. Australia has already warned against travel to Guinea, Liberia and Sierra Leone.

On Tuesday a meeting of the Communicable Diseases Network of Australia was convened, including key infectious diseases doctors and state and federal health authorities, to discuss ways to respond if Ebola was detected.

"While the possibility of Ebola coming to Australia is very low, we are closely monitoring the overseas outbreak and Australia's domestic response," chief medical officer Chris Baggoley said.

All border protection agencies were on alert for possible Ebola symptoms in people arriving by air or sea, Baggoley confirmed.

Meanwhile, Thai health authorities said they had ordered all hospitals to monitor patients for any symptoms, particularly nationals or foreign tourists who had been in the outbreak area.

Epidemic 'out of control'

Bart Janssens, MSF's director of operations, warned that governments and global bodies had no "overarching view" of how to tackle the outbreak.

"This epidemic is unprecedented, absolutely out of control and the situation can only get worse, because it is still spreading, above all in Liberia and Sierra Leone, in some very important hotspots," he said.

"If the situation does not improve fairly quickly, there is a real risk of new countries being affected," he told La Libre Belgique newspaper.

A British doctor volunteering in Sierra Leone treating Ebola patients told Metro newspaper that medical staff were swamped.

"The main challenge here, though, is that the health authorities just don't have the infrastructure to cope. They're overwhelmed," Benjamin Black said.

A top doctor in charge of a Sierra Leone treatment centre died of the virus earlier this week.

In Canada, local media reported that a Canadian doctor had put himself in quarantine as a precaution after spending weeks in west Africa treating patients with the virus alongside an American doctor, who is now infected.

A spokesman for the French foreign ministry said they were offering technical support and expertise on the ground in west Africa.

And Liberia announced it was shutting all schools and placing "non-essential" government workers on 30 days' leave.

Togo-based pan-African airline ASKY, which serves 20 destinations, on Tuesday halted all flights to and from Liberia and Sierra Leone following the death of a passenger from the virus.

The 40-year-old man, who travelled from Liberia, died in Lagos on Friday in Nigeria's first confirmed death from Ebola.

The virus crossing borders for the first time by plane could lead to new flight restrictions aimed at containing outbreaks, the world aviation agency said.

"Until now (the virus) had not impacted commercial aviation, but now we're affected," ICAO secretary general Raymond Benjamin said.

"We will have to act quickly."

AFP