Guinea is battling to contain an outbreak of Ebola, which currently includes 122 suspected cases and 78 deaths , not all of which are confirmed to be the deadly virus.

Health authorities believe it has spread to the country's capital, Conakry, and to neighboring Liberia. Residents of West Africa are already terrified, but anyone who has read "The Hot Zone" fears it will only get worse.

Advertisement

Fundamentally, Ebola is easy to contain. It's not a question of needing high technology. It's about respecting the basics of hygiene, and about isolation, quarantine and protecting yourself - in particular protecting healthcare workers, because they are very exposed.

The symptoms of Ebola often include uncontrollable bleeding, and the deadliest strains of the virus kill up to 90% of people infected . Yet most outbreaks are successfully contained. Is the current outbreak any different?The first thing to realize: It's not that easily spread. Peter Piot, one of the the scientists who discovered the Ebola virus in 1976, has emphasized that it's not like a highly contagious flu. As he wrote in The Independent

It's really "a disease of poverty and neglect of health systems," he writes. It often spreads when hospital workers or mourners come into contact with bodily fluids like blood and vomit, when hospitals use unsanitized needles, or when people handle or eat the meat of infected animals, like bats.

Advertisement

But part of what makes this Ebola outbreak alarming is that recent outbreaks have been in East Africa, and no one can say why it is suddenly in West Africa.

"This is a virus that is highly unpredictable," Piot told Reuters. "This time it popped up in Guinea where it has never been detected before. Why there? Why now?"

Ebola's emergence in East Africa and its spread to Conakry is a cause for serious concern - the virus does not usually "jump" from place to place. But right now there's not yet reason to fear that it will not be successfully contained, especially with international effort now focused on doing just that. A suspected case of a Canadian who had recently travelled to Liberia turned out to be a false alarm , for example.

At this point, Guinea says it has isolated those infected