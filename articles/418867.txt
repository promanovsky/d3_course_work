Some women can have it all. Movie star looks and entrepreneurial spirit appear to be a great combination for Jessica Alba, whose eco-friendly The Honest Company raised $70 million in two years. The company is worth $1 billion after its latest funding round.

In addition to this latest Series C round, the company announced plans to expand into China as it looks forward to an IPO.

Also read: ‘Sin City’ B-Roll Footage Shows Jessica Alba, Joseph Gordon-Levitt and Lots of Green Screen

“We believe being a public company is the best path for us going forward and it’s good to get that validation early on,” Honest Chief Executive and co-founder Brian Lee said, according to The Wall Street Journal.

The baby product company launched in early 2012 and is expected to see revenue of over $150 million in 2014. More than 80 percent of its revenue is sourced from subscription services, which send moms deliveries of environmentally-friendly baby needs like diapers and pacifiers.

Wellington Management Company led the Series C funding round, which included investors such as Lightspeed Venture Partners and Iconiq Capital.

Alba is represented by CAA, 3 Arts Entertainment and PMK*BMC.