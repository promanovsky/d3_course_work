'True Blood' Season 7 Spoilers: 'There's No One Left,' Trailer Reveals Shockers 'True Blood' Season 7 Spoilers: 'There's No One Left,' Trailer Reveals Shockers

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

The seventh and finale season of "True Blood" returns to HBO this summer and promises a jam-packed season of action, including several shocking plot twists.

Previously released spoilers included news that no character on "True Blood" is safe from being killed- including Eric, according to the new trailer. The sneak peek noticably leaves out actor Alexander Skarsgard, who portrays Eric, the debonair vampire whose fate hung in the balance at the end of season six.

Furthermore, the "True Blood" season seven peek reveals just how the bloodshed occurs; "sick" vampires target humans in the final season, and only Sookie can stop them.

"There was an attack at the church tonight ... There's no one left," Sookie, portrayed by Anna Paquin, narrated during the trailer. "Now the good folks of this town are turning on each other and acting like animals. Why they'd do such a thing?"

Meanwhile, Ryan Kwanten who portrays Jason Stackhouse warned fans that beloved characters would meet their demise in season seven.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

"People are going to be dying," the actor revealed to Access Hollywood. "Some of your favorites are not going to last the season, unfortunately."

Although details on the final season are scarce, HBO president Michael Lombardo said that the series "has been nothing short of defining" for the network.

"Together with its legions of fans, it will be hard to say goodbye to the residents of Bon Temps, but I look forward to what promises to be a fantastic final chapter of this incredible show," he said in a statement.

The final season of "True Blood" premieres on HBO June 22 at 9 p.m. EST. The series stars Paquin as well as Stephen Moyer, Joe Manganiello and Sam Trammell, among many others.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit