In case you haven’t heard yet, Facebook today announced that its going to leverage users’ browsing habits to serve more targeted ads to them. Its basically going to share all of the website and app data it collects about users and let advertisers target their ads accordingly. This is nothing new, it already happens on the internet, but this is a first for Facebook. Even though it doesn’t stop Facebook from collecting data, there is a way to opt out of Facebook’s new targeted ads.

Advertising

The social network can keep an eye on how its users move across the broader internet by measuring their interaction through “Like” buttons and comment sections. So if you’re browsing for a new pair of shoes online, and Facebook finds out, you can expect to see ads related to that particular product in your feed.

It also announced the launch of a new tool today called “ad preferences.” Its accessible from every ad on Facebook which explains to the user why they’re seeing a specific advertisement and also lets them add or remove interests that Facebook uses to show ads. If you don’t want to see ads related to shoes, remove shoes from your interests.

This “interest-based advertising” will soon hit Facebook users in the U.S., the company hasn’t said if it has plans to expand this into other markets as well.

To opt out, visit the website developed by the Digital Advertising Alliance to tell Facebook, and other companies that employ similar practices, that you don’t want ads served on the basis of your interests. Those who have AdBlocker Plus installed, or any other plugin that disables cookies, will have to disable it first before they can opt out.

As previously mentioned, opting out doesn’t stop Facebook from collecting data about its users, even if it can’t share it with advertisers. So doing this just might help you sleep a little bit better at night.

Filed in . Read more about Facebook.