Angry at hubby? Eat something sweet

Share this article: Share Tweet Share Share Share Email Share

Washington - Low blood sugar isn't good for marital bliss because it makes spouses more prone to anger and aggression, says a new study. The findings are based on experiments with 107 married couples asked to monitor their glucose levels before breakfast and bed every day for 21 days. In addition, researchers gave them voodoo dolls representing their significant other along with 51 pins. They were then told that, at the end each day over the three-week period, they should secretly stick pins into the dolls indicating how angry they were with their better half. Turns out that the lower the spouse's blood sugar levels, the more pins he or she stuck into the dolls. “When they had lower blood glucose, they felt angrier and took it out on the dolls representing their spouse,” said lead study author Brad Bushman of Ohio State University.

“Even those who reported they had good relationships with their spouses were more likely to express anger if their blood glucose levels were lower.”

Following that experiment, the couples were brought into a lab where - split up in separate rooms - they were told they would play a computer game against their spouse.

Each time they won, they were told, they would get to decide how loud of a noise would be blasted in the headphones of their significant other - and for how long.

That actually didn't end up happening, and they weren't really playing against their spouse but against a computer that let them win about 50 percent of the time.

Still, the results showed that people with lower average levels of evening blood sugar “sent” louder and longer noise to their spouse.

What's more, further analysis determined that those who pricked their voodoo dolls with more pins were more likely to deliver louder and longer blasts of noise.

“We found a clear link between aggressive impulses as seen with the dolls and actual aggressive behaviour,” Bushman said.

He added that glucose serves as fuel for the brain, an organ which consumes about 20 percent of our calories though it only makes up two percent of our body weight.

Self-control needed to handle anger and aggressive impulses requires energy that is partly provided by glucose, he noted.

“It's simple advice but it works: Before you have a difficult conversation with your spouse, make sure you're not hungry.”

The findings appear in the Proceedings of the National Academy of Sciences. - Sapa-AFP