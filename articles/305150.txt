SAN FRANCISCO (MarketWatch) — BlackBerry Ltd. rallied on Thursday after reporting an unexpected profit, while Amazon.com Inc. retreated a day after unveiling a new 3-D enabled smartphone.

BlackBerry US:BBRY shares jumped 9.3%. The struggling smartphone maker reported a profit of $23 million, surprising investors after a string of losses. BlackBerry’s adjusted results, a per-share loss of 11 cents, beat expectations but revenue slumped 69% to $966 million. Also read: BlackBerry sends short-sellers scrambling.

Shares of Amazon.com AMZN, +0.48% are off 2%. Amazon on Wednesday unveiled a smartphone with 3-D-like features and Firefly, which can recognize bar codes and phone numbers. MarketWatch tech columnist Therese Poletti says it could become a smartphone game-changer.

Gainers

BlackBerry shares are rising. Reuters

Shares of American Apparel Inc. US:APP surged 7.3%. The retailer late Wednesday ousted founder and Chief Executive Dov Charney. “We take no joy in this, but the board felt it was the right thing to do,” said Allan Mayer, the board’s new co-chairman.

Even with the move higher Thursday, the retailer’s shares were trading at 74 cents. The company has officially been a penny stock since about 2009, when shares fell below $5.

Kroger Co. KR, +1.65% shares gained 5.3%. The supermarket chain on Thursday reported a better-than-expected first-quarter profit of $501 million, or 98 cents a share, up from $481 million, or 92 cents a share, in the year-earlier period. Excluding special charges, the supermarket chain said it earned $1.09 a share.

Shares of Markit Ltd. US:MRKT rallied 13% to $27.17 on their first day of trading. The Wall Street financial-data provider sold 53.5 million shares to strong demand, pricing the IPO at $24 a share.

Decliners

Coach Inc. US:COH shares slid 8.8%, falling for a second day. The stock is off 13% so far this month and down 37% year to date. “COH’s primary problem is a lack of traction in its core product (handbags) in its core market (North America). Our READ Survey suggests continued weakness in the June quarter,” said analyst David Schick at Stifel Nicolaus in a report published earlier this week.

Juniper Networks Inc. JNPR, -1.98% shares fell 4%. The stock was downgraded to neutral from outperform on Thursday at Mizuho Securities. “With the growth story more muted and estimates extended, we move to the sidelines on JNPR; our price target drops to $26 from $32,” said analyst Matthew Hoffman in a note.

Pier 1 Imports Inc. US:PIR shares slumped 13%. The home-goods retailer lowered its full-year earnings forecast on Thursday, citing the “highly promotional” retail environment that has been weighing on gross profits. Pier 1 said it now expects per-share earnings of $1.14 to $1.22 for the year, down 2 cents from prior guidance, and also posted disappointing first-quarter earnings.

Tickers to Watch

RAD: Rite Aid Corp. RAD, -3.25% shares fell 3.2%. The drugstore said Thursday its first-quarter profit fell to $41.4 million, or four cents a share, from $89.7 million, or nine cents a share, in the year-ago period, with per-share results in line with its prior outlook. Chief Executive John Standley said increased drug costs and reimbursement rate pressure weighed on results.

RHT: Red Hat Inc. US:RHT shares rose 3.8%. The software company’s adjusted per-share earnings and revenue for the first quarter beat expectations on Wednesday, when it also agreed to acquire eNovance for about 70 million euros ($95 million).

GMCR: Keurig Green Mountain US:GMCR shares dropped 3.6%. Analyst at BTIG Research initiated coverage of the stock with a neutral rating and price target of $124, according to Benzinga.com.

Buckingham Research also recommended investors to pocket profits due to the stock’s limited upside, The Wall Street Journal reported.

More must-reads on MarketWatch:

American Apparel rockets higher as board ousts CEO Dov Charney

Low returns on stocks are the new normal

The rocket ship one economist sees in the dot plot