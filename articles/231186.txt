

WATCH: More tech videos on TheStreet TV | More videos from Brittany Umar

NEW YORK (TheStreet) -- Google has been named the most valuable global brand by marketing firm Millward Brown. The tech titan was able to topple Apple, which held the top spot for three years.

VIDEO TRANSCRIPT:

Google (GOOG) - Get Report is the most valuable brand in the world.

That's according to the annual BrandZ study by marketing firm Millward Brown, which calculates Google's brand value at more than $158 billion, a 40% rise from last year. That tops the brand value of Apple (AAPL) - Get Report, which the study found has a brand value of over $147 billion, falling by 20% and landing it in second place.

Don't mistake the findings for meaning which company is the most valuable. Millward Brown analyzes financial information to determine how much of a company's value comes from customers' views of it, how likely they are to remain loyal and other factors.

In short, Millward Brown finds that you are likely to be more loyal to Google than you are to Apple -- this year, anyway. Apple previously held the top spot for three years.

Other notables this year include McDonald's (MCD) - Get Report, the highest-ranking non-tech company at number five, Visa (V) - Get Report and Amazon (AMZN) - Get Report, which both saw 41% increases in brand value from last year, and Twitter (TWTR) - Get Report and LinkedIn (LNKD) , which entered the top 100 brands for the first time.

In New York, I'm Brittany Umar reporting for TheStreet.

-- Written by Brittany Umar in New York.