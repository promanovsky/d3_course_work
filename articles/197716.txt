Oyster, a startup that offers offers unlimited access to its collection of e-books for $9.99 per month, has crossed a nice milestone — it says its library now includes 500,000 books, compared to 100,000 when it launched in September of last year.

The company also said it recently expanded its partnership with HarperCollins, bringing 10,000 more of the publisher’s titles (including Beautiful Ruins by Jess Walter, The Happiness Project by Gretchen Rubin, and American Gods by Neil Gaiman) into Oyster. And it has signed new deals with publishers including McSweeney’s, Chronicle Books, Grove Atlantic, and Wiley.

The challenge for isn’t just getting a lot of books but getting the new, popular titles. Oyster and its competitors are often compared to Netflix, and when people complain about the selection on Netflix, they care less about how many movies are available total, and more about whether Netflix has the specific movies that they’re looking for.

Oyster co-founder and CEO Eric Stormberg told me that he isn’t focused on new releases, but instead on “recent releases.” In other words, he’s hoping to get books into Oyster about three months after their initial publication.

Stromberg declined to provide any subscriber data, but he argued that the fact that the company is expanding his deals with publishers and bringing on new ones is a sign that the numbers are good.

I also asked Stromberg about the emergence of competing services like Scribd and Entitle. He responded that the team is staying focused on two things — having the best selection and providing a well-designed reading experience.

On the selection front, Stromberg noted that when he looked at the 100 most popular titles on Oyster, about half of them aren’t available on competing services: “That’s a really great signal to us that our titles are unique and appealing to our subscribers.”

Oyster is currently available for iOS devices, but Stromberg said it will launch an Android app later this year.