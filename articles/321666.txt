Call it the last straw.

New York's top court Thursday ended New York City's attempt to ban large-sized sugary drinks, ruling that the city had "exceeded the scope of its regulatory authority."

In a 4-2 decision that was more about procedure than the merits of the ban, the state Court of Appeals said that the city's Board of Health, under former Mayor Michael Bloomberg, tried to make an end-run around the City Council when it imposed the ban on drinks larger than 16 ounces.

"The Board of Health engaged in law-making beyond its regulatory authority," Judge Eugene Pigott Jr. wrote for the majority. "It is clear that the Board of Health wrote the Portion Cap Rule without benefit of legislative guidance."

The ruling upheld similar decisions by lower courts to strike down the ban.

In a testy, 26-page rebuttal, the two dissenting judges said the city health board has well-established authority to impose a ban -- just as it legally outlawed trans fats in foods in 2007, even though the City Council hadn't approve the prohibition.

In this instance, soda-ban opponents asked the court "to strike down an unpopular regulation, not an illegal one," Judge Susan Read wrote.

Sign up to receive The 1600 Get our inside look at the White House and goings on in the Joe Biden administration. By clicking Sign up, you agree to our privacy policy.

"To sum up, if the People of the City or State of New York are uncomfortable with the expansive powers first bestowed by the New York State Legislature on the New York City Board of Health over 150 years ago, they have every right and ability to call on their elected representatives to effect change," Read wrote. "This court, however, does not."

Mayor Bill de Blasio, who continued the soda-ban fight after succeeding Bloomberg, called the outcome "disappointing" and said he will continue to attack the issue.

"The negative effects of sugary drink over-consumption on New Yorkers' health, particularly among low-income communities, are irrefutable," de Blasio said in a statement. "While we are still examining the court's decision, it is our responsibility to address the causes of this epidemic, and the city is actively reviewing all of its options to protect the health and well-being of our communities."

City Council Speaker Melissa Mark-Viverito applauded the court's decision -- primarily because of the way, she said, Bloomberg circumvented the City Council, rather than the premise of the ban. She indicated the council could consider "other ways" to tackle the issue.

"I'm glad for the decision," Mark-Viverito said. "The reasons for implementing these policies were maybe sound ones, but the way they went about it were maybe not well thought out and was going to be onerous . . . If the mayor was to implement a policy that needed council review, we'll review it. Right? We'll have hearings and we'll discuss it and we'll take appropriate action. Based on what the courts have said, maybe the policy will take a different form that maybe is more acceptable to us."

Bloomberg, in a statement, called the ruling "unfortunate" and said obesity was the "only major public health issue in our country that is getting worse." He vowed to continue a public awareness campaign and to support soda taxes as a way to curb consumption.

Julio Orellana, 42, a sales executive from Dungan Hills, Staten Island, said the ban "was good for New York. The restaurants are out of hand with the oversized sodas. Sometimes regulation makes businesses do the right thing."

But Cordelle Theodule, 20, a secretary from Jamaica, Queens, was glad to see the ban rejected. "There are so many more important crises in the city . . ." she said. Soda size "should be the people's option: They should know how much they can drink."

With Matthew Chayes, Emily Ngo, Sheila Anne Feeney and Zoë Lake