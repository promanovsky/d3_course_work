You won't see any sexually explicit content on Google Ads anymore. This is because Google finally made a change to the AdWords policy to disallow words like 'oral sex' and 'masturbation'. However, ads for dating sites and adult entertainment are still allowed.

Conservative lobby groups in the United States brought about the change. These groups called to limit access to pornography. Showing of such ads lead to more people gaining access to porn sites. Pornography may be legal in the U.S. but there is a limit to what can be shown and allowed.

Google Adwords, the search engine's advertising network, has been around since 2002 but it's only after 12 years that the policy on explicit content is being changed. A few advertisers are not aware of this so most of them are surprised about the regulation.

Last year in June, Google already put a ban on porn blogs from earning money through ads. Adult websites hosted on Blogger, the Google-owned blog network, were not permitted to make money via the advertising network.

Google also put a limit on showing explicit results on the pages with the SafeSearch open. These are some changes parents and conservatives have wanted to see for some time now. The recent change in policy would even make them happier.

AdWords is Google's biggest moneymaker. Last quarter, the search company made $13.8 billion in ad revenue alone. The figure is 90% of Google's total revenue so you can see this policy change might bring a huge impact.

Google sent out a notice to affected advertisers via an email. "Our system identified your account as potentially affected by this policy change. We ask that you make any necessary changes to your ads and sites to comply so that your campaigns can continue to run." writes Google.

Meanwhile, those in the porn industry are reacting to the change and that Google is turning back on the industry that helped them in the first place.

Google will start to disapprove ads and sites that promote hardcore pornography especially those related to masturbation and other sexual activities. Google has also asked the advertisers to change their ads and sites and to comply with the change in policy.

It's still not clear how this change will impact Google's ad revenue, as well as, the porn industry in general. In the meantime, parents can be assured of a "safer" search engine for their children.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.