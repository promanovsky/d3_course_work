Prosecutors in Connecticut are dropping a disorderly conduct case against Paul Simon and his wife, Edie Brickell, stemming from a fight at the couple's home.

The 72-year-old Simon and 48-year-old Brickell had been scheduled to be in court Tuesday, but they did not appear. Prosecutors told a judge they were declining to pursue the case, meaning the charges will be dropped and eventually erased after 13 months.

The couple fought 26 April inside a cottage on their property in New Canaan, police said.

Brickell told officers she confronted her husband after he did something to "break her heart", according to police, but she did not provide any details. She told police Simon shoved her and she slapped him. The report says Simon suffered a superficial cut to his ear and Brickell, who smelled of alcohol, had a bruise on her wrist.

Simon is a 12-time Grammy winner and member of the Songwriters Hall of Fame and Rock and Roll Hall of Fame – as half of the duo of Simon and Garfunkel and as a solo artist.

Brickell and Simon said in court that it is unusual for them to argue: 'We're fine, we love each other. We had an argument, it's over'. Guardian

Brickell is perhaps best known for the song What I Am, recorded with her band the New Bohemians and released in 1988. She collaborated last year with comedian Steve Martin, who has an acclaimed career as a folk musician, for the roots album Love Has Come For You.

The singers were married in 1992. They have three children.