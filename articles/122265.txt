Android 4.4.3, KitKat Update to Roll Out for Galaxy S5 and Galaxy S4 Android 4.4.3, KitKat Update to Roll Out for Galaxy S5 and Galaxy S4

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

The Android 4.4.3, KitKat update will roll out to the Galaxy S5 and Galaxy S4 Google Play Editions before the end of this month.

International variants are expected to receive it in either May or June of this year. The carrier versions should receive the small update before the end of the year.

Android 4.4.3 is already rolling out to Nexus 5 users on Sprint.

The build number for the new software is KTU48F and it enables the smartphone to work on Sprint's LTE network using bands 26 and 41.

Various bugs have also been dealt with including Random reboots, Bluetooth, Wi-Fi and problems with the camera that stemmed from applications running in the background that are continually trying to access the its application. Skype tries to access the camera regularly from its background service, which is the number one trigger for this bug.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Nexus 5 owners seemed to notice this once they updated their handsets to Android 4.4.2. Google recently stated that it is preparing a fix as the issue has been made known on the company's issue tracking forum. It will probably come in the form of Android 4.4.3.

A temporary solution to the problem at this time is to reboot the device. Uninstalling third-party apps also might help. This should be helpful to those in areas where they have not received the update as it will most likely roll out in stages.

Google launched the Nexus 5 at the end of last year. Specifications on the device include a Qualcomm Snapdragon 800 MSM8974 processor running at up to 2.3GHz, a 4.96-inch LCD 1080p display, 2GB RAM, Adreno 330 GPU, 16 or 32GB internal memory, a 2300 mAh battery and an 8-megapixel camera with Optical Image Stabilization. It is also one of the first devices to run Google's latest Android 4.4 KitKat operating system.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit