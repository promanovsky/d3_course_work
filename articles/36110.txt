Twitter -- the microblogging platform whose identity is closely intertwined with hashtags and the @ symbol -- is now considering removing those two elements from its service.

Vivian Schiller, the company’s head of news, this week called hashtags and the @-reply symbol “arcane,” hinting that both may be moved into the background of Twitter’s service, according to BuzzFeed. Schiller made her comments at the Newspaper Assn. of America’s mediaXchange conference in Denver.

Already, Twitter has begun phasing out the @ symbol on the Android alpha test group version of its app, according to BuzzFeed. A screenshot taken by a BuzzFeed reader shows what a feed looks like without the @ symbol.

VIDEO: Holy Grail screen protector a tough guard but not bulletproof

Advertisement

However, since the BuzzFeed report, Schiller has backed off of her comments, tweeting that Twitter is “not at all” phasing out hashtags and the @ symbol.

The hashtag and @-reply protocols are seen as part of the reason it has been so difficult for Twitter to attract new users. While those already on the service know how Twitter works, new users often find it daunting to understand and use Twitter.

The question Twitter has to ask itself is if it is worth removing hashtags and @ replies in order to attract new users at the risk of alienating its loyal user base.

Twitter could not be reached for comment.

Advertisement

ALSO:

After NSA revelations, Gmail enables secure connection for all

Sprint, T-Mobile announce Samsung Galaxy S5 reservations, pre-orders

Obama ditching BlackBerry? White House reportedly testing Android devices