From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

This may be my last post as an impoverished journalist, because I’m going where the big bucks are. A new report finds that high-tech jobs pay on average $156,000 a year in San Francisco and $195,000 in the wider Silicon Valley area.

The revelations come from real estate firm, JLL, which analyzed the most recent Bureau of Labor Statistics jobs report [PDF]. To be sure, it’s not just high-tech workers cashing in. Silicon Valley interns are also making multiples of what the average San Francisco earns.

Top talent in Silicon Valley is in such high demand that CEOs, including Facebook’s Mark Zuckerberg himself, are reaching out directly to rising stars. I regularly meet tech founders who tell me that they would hire every single talented engineer they could find. That is, they have no budget. Each star engineer is worth at least their salary.

Both the White House and education startups are funneling an extraordinary amount of resources to prepare enough students for next-generation jobs. And those graduating from vocational schools seem to be getting enviable salaries. A recent survey of coding bootcamps found that graduates increased their salaries by 44 percent ($55,000 to $85,000). Some of these graduates had no coding experience, and others had no college degree, before joining these programs.

A note on the numbers: “We take all the aggregate wages in the tech sector and divide it by the number of (tech) jobs,” said JLL’s director of research for Northern California, Amber Schiada. Averages can be heavily skewed by superrich workers,m and Silicon Valley is a very unequal place. Payscale, which gets data from employees rather than the government, estimates that a senior software engineer makes about $122,000.

It’s hard to tell which dataset is more accurate, since neither is a census. But the overall trend is that tech workers are making an extraordinary amount of money.

Maybe it’s time for more folks to make a job switch.