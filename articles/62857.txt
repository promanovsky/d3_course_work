“I am willing to show #Asian community I care by introducing the Ching-Chong Ding-Dong Foundation for Sensitivity to Orientals or Whatever”

Like most things that emerge from the Colbert universe, that (as the context of the joke made clear) was satire — satire intended to skewer Redskins owner Daniel Snyder, who recently launched the Original Americans Foundation at a time when the name of his squad is under fire for being racist.

AD

The satire wasn’t working for Park, who launched #CancelColbert, not to mention a massive discussion about how we mix race and humor, and whether we should at all.

AD

White people–this is an easy and specific thing I am asking you to do. I don’t want you to educate yourself. I want action. #CancelColbert — Suey Park (@suey_park) March 28, 2014

#CancelColbert because racist humor comes from racism. — Suey Park (@suey_park) March 27, 2014

As Slate’s Dave Weigel wrote, Park “started hashtags like Comedy Central started six-episode sketch shows.” All this set the table for Park’s appearance on HuffPost Live. That host Zepps didn’t agree with Park on this stuff was clear from the start, when he asked, “Why cancel Colbert? What did you hope to achieve with that?”

After calling that a “loaded question,” Park went on to say that “Asian Americans are always a punchline.”

More condescension from Zepps followed. He asked Park if she understood the “point of satire.” “Of course I understand satire, I’m a writer,” Park zipped back, way ahead of Zepps. “Satire is supposed to punch up. And [Colbert is] not doing that when he draws parallels to orientalism to make a point about native American mascots.”

AD

Fireworks entered the picture when Park found problems with how Zepps was framing the issue of satire and racism: “I feel like it’s incredibly patronizing for you to paint these questions this way, especially as a white man. I don’t expect you to be able to understand what people of color are actually seeing with regard to #CancelColbert. He has a history of making jokes…”

AD

Interrupting a touch, Zepps responded that being a white man doesn’t keep him from “being able to think.”

This wasn’t headed in a good direction. “Well, white men definitely feel like they’re entitled to talk over me,” said Park. “They definitely feel like they’re entitled to kind of minimalize my experiences…”

AD

Zepps: “No one’s minimalizing your experiences, no one’s minimalizing your right to have an opinion,” he said, before amping up the scorn: “It’s just a stupid opinion.”

Park: “You just called my opinion stupid, you just called my opinion stupid. That’s incredibly unproductive. And I don’t think I’m going to enact the labor of explaining to you why it’s incredibly offensive and patronizing.”