The U.S. spends more money on health care compared with other industrialized countries, but Americans still get the least bang for their buck -- and many still don't have access to care -- according to a report just published by the Commonwealth Fund. The report from the private health care research foundation examined data on expenditures, delivery and access to health care services among 11 industrialized countries: Australia, Canada, France, Germany, the Netherlands, New Zealand, Norway, Sweden, Switzerland, the U.K. and the U.S.

Overall, the U.K. and Switzerland were rated highest for factors that included quality, access, efficiency and equity of health care. The U.S., Canada and France overall ranked lowest. The U.S. was found to perform worst in areas concerning cost of care, efficiency, equity and overall health of its citizens, even though health care expenditures were highest per capita compared with the other 10 countries in the report.

In 2011, the U.S. spent $8,508 per capita in health care expenditures, compared with $3,405 per capita in the U.K., which was the country with the highest ranked health care system overall.

Commonwealth Fund

"Although the U.S. spends more on health care than any other country and has the highest proportion of specialist physicians, survey findings indicate that from the patients' perspective, and based on outcome indicators, the performance of American health care is severely lacking," write the authors in the report. "The nation's substantial investment in health care is not yielding returns in terms of public satisfaction or health outcomes."

For quality of care assessment, the authors ranked countries in categories such as effectiveness, safety and coordination of care. Norway and Sweden performed lowest overall for these factors, while the U.K., Australia and Switzerland were rated highest.

Though the U.K. scored best in most categories, the country ranked second worst after the U.S. in terms of the health of its residents. This was based on factors such as adult and infant mortality rates, receptiveness to medical services and life expectancy at age 60. Citizens of France, Sweden and Switzerland were found to be healthiest overall.

The authors of the report note that the U.S. stands apart from other industrialized countries because it does not offer universal health insurance, meaning lower income individuals often don't have sufficient access to health care -- especially preventive medicine -- compared with other countries.

However, universal health care has its drawbacks. In Canada, for example, health care may pose little financial burden but this often means patients must wait longer for needed services.

Commonwealth Fund

Though the Affordable Care Act in the U.S. has improved access to medical services for many in this country, researchers say is still too early to tell if these new health care policies will improve the overall health of Americans.

"These results indicate a consistent relationship between how a country performs in terms of equity and how patients rate other dimensions of performance: the lower the performance score for equity, the lower the performance on other measures," the authors write in their report. "This suggests that, when a country fails to meet the needs of the most vulnerable, it also fails to meet needs for the average citizen."

The report, now in its fourth year, used data from the Commonwealth Fund 2011 International Health Policy Survey of Sicker Adults; the Commonwealth Fund 2012 International Health Policy Survey of Primary Care Physicians; and the Commonwealth Fund 2013 International Health Policy Survey. The report from 2013 is based on surveys of more than 20,000 adults about health care experiences in their respective countries. Additionally, the researchers incorporated data from the World Health Organization and the Organization for Economic Cooperation and Development.