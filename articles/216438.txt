High salt intake has been linked to elevated blood pressure and stroke; a recent study showed that people with high blood pressure often prefer salty foods.

Recent findings suggest people with high blood pressure tend to be "salt-seeking," HealthDay reported.

The researchers hope to encourage salt-loving people with high blood pressure to try an alternative.

"By adding nonsalt spices to food," White said. He added that "it is important to know that alternative spices could reduce sodium [salt] intake and potentially lower blood pressure," Doctor William White, current president of the American Society of Hypertension (ASH), told HealthDay.

The researchers looked at 118 participants divided into four groups. Two of the groups were in their 30s and either did or did not have high blood pressure; the other two included people in their 70s.

Each participant was offered the option of one of three choices of French bread some of which were saltier than others.

The team found that the participants without high blood pressure had a preference for the saltier bread, but those with high blood pressure in both age groups preferred the "highly salted" bread.

A few weeks later the participants were given the same bread options, but in this case they were seasoned with oregano. In this part of the study all of the groups showed a shift in preference towards the less salty bread.

The researchers concluded that people with high blood pressure do have a preference for saltier foods, but this can be combated with salt-free spices.

Further research is needed before a "definitive conclusion" is reached.

"This trial did not set out to determine if there is cause and effect," Doctor Domenic Sica, ASH's new president-elect, told HealthDay. "At this time, it's simply an association that needs to be further clarified in larger studies, with a more rigorous trial design."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.