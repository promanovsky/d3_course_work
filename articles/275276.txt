'Edge of Tomorrow' and 5 Other Movies With Major Title Changes

The Tom Cruise starrer, originally called "All You Need Is Kill" is just the latest high-profile film to alter its title. THR takes a look back at some of the best-known revisions and why they were made.

What's in a name? Would Edge of Tomorrow be as appealing to audiences with its original title, All You Need Is Kill? Probably not, reasoned those who decided to change the title of the Warner Bros. film starring Tom Cruise and Emily Blunt, which hits theaters on Friday. Long known as All You Need Is Kill after the book on which Edge of Tomorrow is based, the movie's moniker was changed to get rid of "kill."

Edge is just the latest film to alter its previously well-known title prior to its release. Other famous examples include Pretty Woman, originally called 3000; Hancock, previously titled Tonight He Comes; and this summer's Begin Again, known as Can a Song Save Your Life? as recently as its Toronto Film Festival debut last September.

VIDEOS: The Most Anticipated Summer Blockbusters of 2014

THR takes a look back at why Edge and five other movies changed their names.

New Title: Edge of Tomorrow

Old Title: All You Need Is Kill

Tom Cruise and Emily Blunt's upcoming action movie, in which Cruise plays a soldier with the ability to restart the day every time he dies, was long known as All You Need Is Kill, which is also the title of the book on which the movie is based. But filmmakers revised the title feeling that the original, with the word "kill," was too problematic.

"I think the word 'kill' in a title is very tricky in today's world," producer Edwin Stoff told The Hollywood Reporter at Edge of Tomorrow's New York premiere. "I don't know that people want to be bombarded with that word. I don't know that people want to be opening the newspaper and seeing that word. We see it enough in real newspaper headlines, and I don't think we need to see it when we're looking at a movie."

PHOTOS: Top 10 Movie Songs of All Time

New Title: Begin Again

Old Title: Can a Song Save Your Life?

When Keira Knightley and Mark Ruffalo's upcoming movie Begin Again debuted at last fall's Toronto Film Festival, it was known as Can a Song Save Your Life? The film was a hot property in Toronto, with The Weinstein Co. quickly snatching up U.S. rights despite strong interest from rival buyers. But test audiences weren't so crazy about the old title.

"We'd been testing the movie throughout the fall and into the winter, and it's been testing consistently -- incredibly well across four quadrants in an exciting way. But the title was unilaterally disliked by everyone," producer Anthony Bregman told THR at Begin Again's Tribeca Film Festival premiere. "It was getting an 11 percent approval rating, which is pretty low. … A lot of people said it sounded like a different type of movie, like a softer movie than it was. They just hated the title."

New Title: Lee Daniels' The Butler

Old Title: The Butler

Last summer, The Weinstein Co. was all prepared to call its Lee Daniels-directed film about a White House butler The Butler until the MPAA's Title Registration Bureau ruled that the film, starring Forest Whitaker and Oprah Winfrey, could not use that title, which is also the name of a 1916 Warner Bros. short film. TWC appealed the decision and tried to get Warner Bros. to back down, but TRB's appeals board agreed with the earlier decision, so the title was changed to Lee Daniels' The Butler.

PHOTOS: Movies That Became TV Shows

New Title: Hancock

Old Title: Tonight, He Comes

The 2008 Will Smith film about an alcoholic superhero was originally titled Tonight, He Comes. That cringe-inducing name referred to an early draft of the script that sounds far different from the action movie that Sony eventually released -- at least according to director Peter Berg.

"It was about a superhero alcoholic who could not make love because if he comes -- if he climaxed -- he would kill a woman with the power of his climax, and it was this really kind of dark, twisted script," Berg told ESPN's Bill Simmons.

The title was later changed to John Hancock in reference to an autograph Smith's character gives a fan, according to co-star Daeg Faerch, before being shortened to just Hancock.

PHOTOS: 35 of 2014's Most Anticipated Movies

New Title: Scream

Old Title: Scary Movie

1996's satirical horror film was originally called Scary Movie before Bob and Harvey Weinstein, aided by Michael Jackson, decided to change it to Scream in part to better convey the mood of the film. Bob explained why they made the change in a 2004 interview with Quentin Tarantino. "When I bought Scary Movie I said, 'Let me make sure I bought the right script -- am I buying a frightening scary movie with irony and some humor, or is it a funny movie that also happens to be scary?' And I said, 'If it’s the first way around, I think you’ve got the wrong title for it.' Harvey, he calls people up at two in the morning. He assumes you’re awake, with no sense of apology, no 'I’m sorry to wake you up,' he just starts in the middle of the sentence. He goes, 'Scream -- it's Scream. Listen, I’m watching this video with Michael Jackson and Janet Jackson called Scream -- that’s the title of your movie."

New Title: Pretty Woman

Old Title: 3000

Julia Roberts' breakthrough film Pretty Woman was originally called 3000 in reference to the amount of money paid for a week's worth of the hooker's company. The 3000 script was much darker, according to Turner Classic Movies, telling the story of a drug-addicted hooker who's rescued for a week and then returned to the streets. Disney, which made and distributed the movie, reportedly thought 3000 sounded too science fiction-y, so they changed it to the name of the Roy Orbison song that plays during the film's memorable shopping montage. Unfortunately, though, director Garry Marshall made the cast jackets with "$3,000" on them before the title was changed. "[Roberts] learned from me that before you make the jacket, you've got to name the picture," Marshall recalled in 2007.