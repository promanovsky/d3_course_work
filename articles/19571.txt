Country singer Jason Aldean is dating Brittany Kerr, the former American Idol star with whom he was caught cheating on his wife with back in September 2012.

Aldean and Kerr have been keeping things on the down-low, but have nevertheless been together since he filed for divorce from wife Jessica Ussery in 2013.

Back in 2012, the married Aldean, 37, was captured on camera kissing Brittany Kerr, also a former NBA cheerleader, at an outdoor bar in West Hollywood.

The "Night Train" singer apologized for the incident on Facebook immediately once the story hit celebrity gossip sites, admitting that he "screwed up."

"I had too much to drink, let the party get out of hand and acted inappropriately ... I left alone, caught the bus to our next show and that's the end of the story."

Clearly it wasn't. It's not known when they hooked up again, but here we are.

Aldean and Ussery, his wife of over 10 years with whom he has two daughters, reportedly tried to make their marriage work after the scandal, to no avail.

Just seven months later, Aldean filed for divorce from Jessica Ussery.

"This is a really tough time for my entire family," Aldean said after.

"Jessica and I have been together since we were teenagers. We've been through a lot of ups and downs over the years as we grew up together as a couple."

"She will always be important to me because she is the mother of my children, and I know that we will both always make our daughters our No. 1 priority."

Kerr is his priority now, too. On Friday, March 14, Kerr was photographed watching Aldean's concert at the Amphitheater at The Wharf in Orange Beach, Ala.

Maybe she can be one of his backup singers? You be the judge ...