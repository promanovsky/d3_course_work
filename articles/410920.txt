For six straight months, the U.S. economy has added more than 200,000 jobs, according to government data released Friday morning, the longest streak since the mid-'90s.

The Labor Department reported 209,000 net new jobs were created in July, though the unemployment rate edged up slightly to 6.2 percent as more workers joined the labor force. The report was the latest in a string of upbeat data suggesting the country’s economic recovery has shifted into the next gear.

Industries with the strongest hiring were professional and business services, as well as manufacturing and construction. That dovetailed with a private estimate of job growth earlier this week indicating broad-based improvement in the labor market. That analysis, by human resources firm ADP, showed both larger corporations and smaller businesses were adding workers.

“There was some meat on the bones," said Diane Swonk, chief economist at Mesirow Financial. "This wasn’t just temporary hires.”

Two top officials at the Federal Reserve said Friday that the improving picture in recent months means that the central bank can start withdrawing its support for the recovery more quickly. The Fed is pumping billions of dollars each month into the economy and is expected to end that program in October. In addition, it has kept its benchmark short-term interest rate at zero since 2008.

Speaking on CNBC, Dallas Fed President Richard Fisher said he believes the date of the first interest rate hike has moved "significantly earlier." In a separate statement, Philadelphia Fed President Charles I. Plosser indicated he wants rates to rise significantly before the end of the year.

The federal funds rate "remains well behind what I consider to be appropriate given our goals," he said in a statement.

Wall Street was largely flat in morning trading following the data deluge. Investors seemed to be tempering their concerns about the economy and geopolitical tensions after Thursday's massive selloff that erased all of the gains in the Dow Jones Industrial Average for the year.

Yet the government data revealed that progress in several key trouble spots in the job market stalled in July. The ranks of part-time workers who would like more hours was essentially unchanged at 7.5 million in July after spiking the previous month. The number of people out of work for six months or more also barely budged at 3.2 million, representing about one-third of the unemployed. And the labor force participation rate rose slightly to 62.9 percent.

Federal Reserve Chair Janet Yellen has cited those indicators as potential signs that the job market is weaker than the official hiring and unemployment numbers suggest. In an official statement released Wednesday, the central bank acknowledged the broad gains in employment but included a caveat.

“There remains significant underutilization of labor resources,” the statement read.

Giving the Fed some breathing room is the low level of inflation. According to Friday's data, average hourly earnings for all employees inched up one cent to $24.45 in July. Over the past year, wages have gone up about 2 percent, on par with the rate of growth over the past several years. Other government numbers released Friday showed prices rose 1.6 percent over the year ending in June, well below the Fed's longer-run target of 2 percent.

That data also showed consumer spending picked up in June, increasing 0.4 percent from the previous month. But the growth in personal incomes held steady, and the savings rate remained at 5.3 percent.