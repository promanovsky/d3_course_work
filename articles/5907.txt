Electronic Arts and Respawn Entertainment’s “Titanfall” is the first major game release for the Xbox One this year, and it faces all of the hype that comes with that distinction.

The game is a first-person shooter — meaning players see the action as if they were in the world itself — and puts players in the shoes of either a pilot or in the cockpit of a massive robot, called a titan.

The game hits store shelves Tuesday and is both tremendous fun and a great draw for the Xbox One. In fact, Microsoft is hoping it will be the next “Halo.”

That’s not to say it will necessarily become the iconic franchise that “Halo” has. But the firm has placed its bets on making “Titanfall” a console-maker in the same way that the original “Halo” was for the first Xbox — the must-have game that makes it worth buying the system.

Having a killer game with a social bent is a strong strategy to picking up players and could help the Xbox One close the 2 million unit gap it has with Sony’s $399 PlayStation 4.

Microsoft is certainly hoping so, at any rate, offering “Titanfall” bundles for potential Xbox buyers, essentially knocking the $60 price of the game off of the new console’s $499 price tag. The game is also available on PC, and an Xbox 360 release is due in the coming weeks.

But enough business. Let’s talk gameplay.

The strength of “Titanfall” is in the fun of its gameplay, because it’s not heavy on story, and its graphics, while fine, aren’t anything to write home about. And it’s not a game you can enjoy all by yourself. It is a purely multiplayer game, meaning that your Internet connection is key, as is your ability to work in a team. The game has a handful of modes, which prize survival, strategy and teamwork in different measures. With several survival modes, as well as a strategic-point capture mode and a capture the flag mode, it will feel familiar to both “Halo” and “Call of Duty” multiplayer fans — but not so familiar that you’ll feel bored.

“Titanfall” gets around being stale in a couple of key ways. For one, the way you move when you’re in your titan is completely different from how you move out of it. As a pilot, on foot, you have a range of movement at your disposal that forces you to read the terrain all while zipping through battle. These include wall runs and jumps, as well as a temporary stealth mode, that will make you feel like the world’s most dangerous free-runner. As fun as dipping and diving your way through the maps can be, the key part of the game starts after you’ve spent a certain amount of time on the ground and then notice that your titan is ready to drop onto the battlefield.

All of a sudden, your very own giant robot is falling from the sky.

Once you clamber in, you’re in charge of some serious firepower, although the designers have done a fine job of balancing the firepower bonanza with longer-range weapons and defensive options. The game also lets you disembark and reenter into the titan when necessary, and the robot can act as a guard or follow you even when you’re not in the cockpit. The titans are tough, meaning that being in one gives less-experienced players some time to get their bearings and taking one down is still no cakewalk for veterans.

As for the game’s overall system, “Titanfall” is all about knowing how you play and how to capi­tal­ize on your strengths. The game breaks down your performance into exact detail, so you’re well-informed about what kind of player you are.

To customize your titan, or your character, “Titanfall” has a collectible card system of one-time-use powers. You earn cards based on good performances in battle, and you are allowed to take up to three of those cards with you. These can do things like cut down on the time it takes to get your titan on the field or grant you useful powers on the battlefield.

So is “Titanfall” the “Halo” of the Xbox One? It’s still too early to tell. Until the public servers are completely open and the community takes on a more cohesive identity of its own, it’s difficult to say exactly how the game will mature. But with a well-balanced game to appeal to newbies and experts, plus a familiar formula that stays above cliche, “Titanfall” looks likes a strong prospect for being a system seller.