E-cigarettes may not be an effective method for quitting smoking, according to research conducted at the University of California. The study, published in JAMA Internal Medicine, looked at self reported data from 88 e-cig smokers.

Despite the small sample size, researchers reported that use of e-cigarette did not coincide with a program of successful smoking cessation.

"Regulations should prohibit advertising claiming or suggesting that e-cigarettes are effective smoking cessation devices until claims are supported by scientific evidence," said lead author, Rachel A. Grana.

However, speaking with Reuters, Dr. Michael Siegel, who was uninvolved with the research, said that the study had too many holes:

"We need solid data that's based on solid science before we make decisions. I hope no one would take this research letter and make any conclusion based on it."

For comments and feedback contact: editorial@rttnews.com

Health News