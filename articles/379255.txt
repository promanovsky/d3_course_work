A third of all Alzheimer's cases worldwide are preventable, a new study published by the University of Cambridge has found.

The research, which was carried out by Cambridge University’s Institute of Public Health and published in the medical journal The Lancet on Monday, said that one in three Alzheimer’s cases could be avoided if there was a change to people’s lifestyles.

Using population data, researchers were able to work out the seven main risk factors that brought about Alzheimer’s in later life.

A lack of exercise, smoking and better diets were all seen as significant factors in making people more susceptible to the disease, while other factors such as low educational attainment, depression and other conditions brought on by poor lifestyle were also seen as contributors.

The researchers led by Professor Carol Brayne, predicted that if these “modifiable risk factors” were reduced by just 10 per cent, nearly nine million cases of the disease could be prevented.

In Britain they found that a 10 per cent reduction in risk factors would reduce cases by 200,000 in 2050.

There are currently 820,000 people in the UK living with Alzheimer's and there is a fear that this number could continue to grow as Britain’s population ages.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Proffessor Carol Brayne, from the Institute of Public Health at the University of Cambridge, told the BBC that: "Although there is no single way to treat dementia, we may be able to take steps to reduce our risk of developing dementia at older ages.

"We know what many of these factors are, and that they are often linked.

"Simply tackling physical inactivity, for example, will reduce levels of obesity, hypertension and diabetes, and prevent some people from developing dementia.

The seven Alzheimer's risk factors Show all 7 1 /7 The seven Alzheimer's risk factors The seven Alzheimer's risk factors Hypertension 8 percent of Alzheimer's cases are linked to mid-life hypertension Getty The seven Alzheimer's risk factors Smoking Smoking accounts for 11 percent of Alzheimer's cases Getty The seven Alzheimer's risk factors Obesity Midlife obesity accounts for 7 percent of Alzheimer's cases PA The seven Alzheimer's risk factors Low Educational attainment Low education or simply not using your brain enough accounts for 7 percent of Alzheimer's cases Getty The seven Alzheimer's risk factors Diabetes Problems with blood sugar control kick off the list of modifiable risk factors for Alzheimer's.The study suggests that 3 percent of Alzheimer's cases are linked to diabetes The seven Alzheimer's risk factors Depression 15 percent of Alzheimer's cases may stem from depression Rex The seven Alzheimer's risk factors Too little exercise Not enough physical activity is the number one preventable factor that contributes to Alzheimer's cases Rex Features

"As well as being healthier in old age in general, it's a win-win situation."

It is estimated that the number of people to suffer from Alzheimer’s will treble to more than 106 million sufferers in 2050.

In the US, UK and Europe, physical was seen as the biggest contributor to Alzheimer's, with about a third of their adult population physically inactive.

A similar study carried out by the University of California in 2011, had suggested that one in two cases of Alzheimer’s could be prevented if these risk factors reduced; however, the new study claims that the number is lower.

The lead researcher of the 2011 study, Dr Deborah Barnes, welcomed the new findings and said that studies like this were very important to the medical profession.

“It's important that we have as accurate an estimate of the projected prevalence of Alzheimer's as possible, as well as accurate estimates of the potential impact of lifestyle changes at a societal level,"

"Our hope is that these estimates will help public health professionals and health policy makers design effective strategies to prevent and manage this disease."

Dr Simon Ridley, head of research at charity Alzheimer's Research UK, also supported the findings but was keen to stress the need for continued research.

He said: “We still do not fully understand the mechanisms behind how these factors are related to the onset of Alzheimer's.