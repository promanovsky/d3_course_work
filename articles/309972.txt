Seoul: North Korea has condemned Australian Foreign Affairs Minister Julie Bishop as a US "stooge" after she criticised its leader Kim Jong-un in an interview.

Pyongyang's foreign ministry on Sunday described Ms Bishop as "no more than a stooge carrying out the US hostile policy" toward it.

Foreign Affairs Minister Julie Bishop with US Ambassador John Berry at the Alliance 21 conference in Canberra last week. Pyongyang has threatened to "punish" her for being a "US stooge". Credit:Andrew Meares

North Korea "will never pardon but resolutely punish anyone who dares slander the dignity of its supreme leadership", a ministry spokesman told Pyongyang's official news agency without elaborating.

Last week Ms Bishop in an interview with US radio station Voice of America criticised Mr Kim and his pursuit of nuclear weapons.