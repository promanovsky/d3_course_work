University of Maryland researchers have found a significant increase in cervical cancer rates, especially among women aged between 65 and 69 years.

It seems that cervical cancer rates in the United States might have been highly underestimated. According to the University of Maryland researchers, cervical cancer rates especially among 65 to 69-year-old women are much higher than previously believed.

Here are some of the reasons why previous estimates are lower than the actual rates.

- Current U.S. cervical cancer screening guidelines do not recommend routine Pap smears for women over 65 years if their prior test results have been normal.

- Current estimations also take into account women who had hysterectomies in which the lower part of the uterus, the cervix, was removed.

Under such scenarios, previous estimates have an age-standardized rate of about 12 cases of cervical cancer per 100,000 women in the United States. Risks of developing this type of cancer were believed to peak at 40-44 years of age and then level off.

The new study found that when women with hysterectomies were excluded, the rate of cervical cancer increased to 18.6 cases per 100,000 women. Researchers also found that the risks peaked at a higher age before leveling off. This change was more prominent in women aged between 65-69 years.

"The higher rates of cervical cancer after correction for hysterectomy highlight the fact that, although a large proportion of cervical cancer has been prevented through early detection and treatment, it remains a significant problem," the authors conclude. Human Papillomavirus (HPV) infections cause virtually all cervical cancers, and the researchers stress the need for widespread HPV vaccination to protect women against the virus.

Cervical cancer rates among women aged between 65 to 69 years were 84 percent higher than previously thought (27.4 cases per 100,000 women vs. 14.8 cases per 100,000 women). Discrepancy in rates among both African and white women were observed.

Rates for white women increased from 13.5 cases per 100,000 to 24.7 cases per 100,000 women. Similarly, rates for African women of the same age group increased from 23.5 cases per 100,000 to 53 cases per 100,000 women.

"Our corrected calculations show that women just past 65 years, when current guidelines state that screenings can stop for many women, have the highest rate of cervical cancer," said Anne F. Rositch, the study's lead author, in a statement. "It will be important to consider these findings when reevaluating risk and screening guidelines for cervical cancer in older women and the appropriate age to stop screening."

The study was funded by the Career Development Award for Bridging Interdisciplinary Research Careers in Women's Health and published online in the journal Cancer.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.