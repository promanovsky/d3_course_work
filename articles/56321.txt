The Russian navy will acquire Ukrainian military-trained combat dolphins and seals from the region of Crimea following its formal annexation.

The mammals, which were serving under the Ukrainian armed forces before Crimea's breakaway, are to be incorporated into the Russian forces after being trained to suit the requirements of Russia's fleet in the region.

The bottlenose dolphins and fur seals will be used to find sunken objects as well as detect enemy forces under water.

"Engineers are developing new aquarium technologies for new programmes to more efficiently use dolphins under water," a spokesperson for the Sevastopol State Aquarium in the Crimean capital told Russia's RIA Novosti on condition of anonymity.

The training presently given to the mammals is said to be outdated and once they are inducted into the Russian naval forces, a new system is expected to be implemented.

"Our specialists developed new devices that convert dolphins' underwater sonar detection of targets into a signal to the operator's monitor. But the Ukrainian navy lacked funds for such knowhow, and some projects had to be mothballed," said an associate at the Crimean aquarium.

"This means the search for items on the seafloor, patrolling the marine area, locating submarines and combat seals. We expect that this will become a separate sphere for military training for the Russian navy."

However, critics of the programme remain sceptical that use of animals such as dolphins or seals may not yield the desired results in a combat scenario as there are several constraints in training them.

There are only two combat dolphin training centres in the world – one based in San Diego and the other in Sevastopol.

The Soviet-run facility, operated in the interests of the Ukrainian navy, was believed to have shut down following the collapse of the USSR. However, it was resumed in 2012 by the Ukrainian military.