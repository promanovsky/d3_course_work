Known for its raunchy scenes, it should not come as a surprise that at least six female characters in HBO's fantasy drama Game of Thrones are either current or former porn stars.

Sibel Kekilli, who plays Tyrion Lannister's stubborn lover Shae, made her name in German adult movies a decade ago under the stage name of Dilara. She admitted recently that the greatest challenge for her in the hit series was matching the acting ability of her co-stars.

She said: "You feel like 'oh my god, I act really bad' and he's really good. I just want to try and keep it up, to give your best."

She also lamented that it was always the women, and never the men, who got their kit off on-screen.

"Why are all the women naked?" she asked. "I mean... good-looking guys. I want to see Conleth Hill and [Nikolaj Coster-Waldau] and Peter Dinklage."

Briton Samantha Bentley is another porn star lined up for the show though her role has not been specified yet. She was crowned Best Female Performer at the UK Adult Producers Awards in 2013 and an Adult Video News Award in 2013 for Best All-Girl Group Sex Scene. The 26-year-old has appeared in 50 adult films including her first self-titled release, Samantha Bentley is Filthy.

Jessica Jensen, who is due to appear in season 4 in an unspecified role, has been in the adult industry since 2011. She has appeared in 30 hardcore movies. She was named Best Newcomer at the Producers Awards in 2012. She also briefly appeared in the 2013 cinematic movie Filth.

Aeryn Walker is an Australian amateur porn star, who has a penchant for "costume-dressing". She will appear as one of the Craster's wives in season four. Incestous Craster, played by Robert Pugh, often marries his daughters when they are old enough and is an ally of the Night's Watch.

Maisie Dee was another British porn actor on the show's books. She played the character Daisy, one of Petyr Baelish's prostitutes after joining the Game of Thrones cast in season 2. She will not be appearing in season four.

Indian porn star Sahara Knite worked in the adult industry before she played another of Baelish's prostitutes. She appeared in one of the show's most popular sex scenes alongside Esme Bianco but her role finished in season 2.

In an interview in The Times, actor Charles Dance who plays evil Tywin Lannister, said he was astonished by the extent and the style of the sexual couplings when he saw the early episodes.

"All the rumpy-pumpy and it was all doggy fashion. I said to David and Dan (the executive producers David Benioff and D.B Weiss): 'Has the missionary position not come into being in the Seven Kingdoms? They said: 'We wanted it to be quite animalistic, Charles.' I was quite amazed by it."