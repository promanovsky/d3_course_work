Veteran entertainer Rolf Harris arrives with daughter Bindi (left), wife Alwen and niece Jenny (right) at Southwark Crown Court, London where where he faces charges of alleged indecent assaults on under-age girls

Court artist sketch by Elizabeth Cook of Rolf Harris in the dock at Southwark Crown Court, London as a witness gives evidence behind a curtain watched by the judge

Rolf Harris has become the biggest celebrity scalp to be claimed by detectives from high-profile sex crime investigation Operation Yewtree.

The 84-year-old is the second person to be convicted under the national inquiry, which was set up in the wake of abuse claims against Jimmy Savile.

Police were contacted by one alleged victim, named in seven of the 12 charges that Harris faced, after she heard the claims made about late DJ Savile and gained the confidence to come forward in November 2012.

The other three then contacted officers - two in the UK and one in Australia - after British newspaper The Sun publicly named Harris as a Yewtree suspect for the first time on April 18 last year.

Police and prosecutors have previously faced questions over the success of Operation Yewtree, which has so far seen two convictions out of 17 arrests.

Scotland Yard has defended the investigation, saying officers have to take alleged victims seriously, particularly in the wake of national failings over Savile.

Eight people have been told they will face no further action; three suspects, including broadcaster Paul Gambaccini, remain on bail; and a total of six people have been charged.

Former popstar Gary Glitter and ex-Radio One DJ Chris Denning are currently going through the court system, and driver David Smith was due to face trial but died before he could do so.

DJ Dave Lee Travis was the first person to go through a trial under Yewtree, but the proceedings ended with him being acquitted on 12 indecent assault charges, and a hung jury on one count of the same charge and a count of sexual assault.

Prosecutors later confirmed that they would go to a retrial on the two charges, and that Travis would face an additional count of indecent assault.

PA Media