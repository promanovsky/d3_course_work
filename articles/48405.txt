NEW YORK (TheStreet) -- It doesn't take long to know when you're in the presence of greatness. Which is why I can say that after living with and using the new HTC One M8 for a short period of time this is one heckuva great Android smartphone.

I will tell you right off the bat that the new One isn't perfect. It's larger and slightly (0.6 oz) heavier than last year's One. And there are some things that have been left unsaid, such as the exact megapixel count for the new, dual-lens rear camera. But harping on those early observations would be nitpicking. Early on, I'm learning the One is much, much more.

The One M8 has a super-fast (2.3 GHz) Qualcomm (QCOM) - Get Report Snapdragon 801 quad-core CPU, 2 GB of RAM and a beautiful 5-inch HD touchscreen. The phone will come with either 16 GB or 32 GB of internal storage and there's now a microSD card expansion slot for when you need to carry around more of everything.

The M8 runs on the latest version of Google's (GOOG) - Get Report Android OS - 4.4.2/Kit Kat - and uses what HTC jokingly described as its "Sixth Sense". It's really the sixth (and best so far) version of HTC's Sense screen layout app, which mines your personal Facebook (FB) - Get Report, Twitter (TWTR) - Get Report, email, SMS messages and a lot more, laying out everything in a separate information information stream on the phone's home screen. It is quite useful for quick checks and very addictive.

There's a large 5MP camera up-front and that twin-lens deal on the back. HTC doesn't specify the megapixel of the rear camera system. It's called the HTC UltraPixel and is said to sport a new "ImageChip 2" capable of producing large pixels though its large sensor and the primary 28 mm, f2.0 lens. It's capable of producing full 1080p videos. The second rear lens is for capturing "depth information" for each photo to help make future editing decisions a snap.

We're guessing the camera still measures 4 MP (same as last year's model) but those two lenses make a difference. In our early testing the back camera system seemed to produce even more terrific photos. There are so many new controls and features we'll happily be learning about/playing with them all over the next few months.

For the record, the 2013 HTC One (M7) had a 4.7-inch screen, that 4 megapixel rear camera, a Qualcomm (QCOM) - Get Report 600 processor, either 32 or 64 GB of internal storage (no SD card expansion was offered) and it ran on Android 4.1 (Jelly Bean). HTC also introduced a One Mini (4.3-inch screen) and One Max (5.9-inch display) in the months following.

Just like with last year's model, if you hold the phone in the landscape (horizontal) position you realize that there are stereo speakers on the front of the phone. Two audio channels separated by the screen. As an extra bonus those speakers actually sound great - some of the best smartphone sound ever. HTC calls the system "Boom Sound". And boy can it play loud. It's like the volume control can go to "11". I never felt I had to max-out the volume control to "rock out to the sounds".

The One M8's performance has been stellar so far. Everything works smoothly and quickly. Very quickly, as a matter of fact. The M8 also stays cool to the touch even after taxing requests like streaming video for an hour or two. Last year's One sometime got a little warm to the touch under the same test conditions.

As for reserve power, we found the new 2600mAh battery should be able to last through an average full day's use, including set-up, playing, streaming, downloading, reading, talking, shooting/editing photos/videos. In short, everything you might want to do with a modern-day smartphone.

As with any new smartphone, the One M8 also has a slew of new accessories. There is one in particular you need to know about. It's called the "Dot View". It is a rubberized-plastic cover - front and back - which protects the phone. But the front is a grid of see-through dots and it uses the phone's internal proximity sensors to present the user with a rudimentary but incredibly useful view of the current time and message icon. It is a very clever idea which can be initiated by double-tapping the front cover.

Overall, the phone is a winner. And, HTC's decision to make the handset available immediately after the announcement (U.S. and UK.) was a brilliant marketing ploy. A quick jump start in two of the world's most competitive, high-end smartphone markets is something other manufacturers will probably want to emulate in the very near future.

And, those other manufacturers aren't standing still. There are new flagship phones coming in the next few months from Samsung (Galaxy S5), LG (G3) and Apple (AAPL) - Get Report (iPhone 6) just to name a few. HTC needs this M8 to be a success and if our testing is any indication, the new One could put up a good fight.

Verizon (VZ) - Get Report has the One M8 in its stores right now. And you can order it from AT&T (T) - Get Report, Sprint (S) - Get Report and T-Mobile (TMUS) - Get Report immediately or wait until April 11 to buy one in person at their retail shops. Expect to pay somewhere in the $200-$250 range with a contract or $650 or so without contract. There are also special Google Play and HTC Developer editions of the M8 available on those companies' respective Web sites.

Written by Gary Krakow in New York.

To submit a news tip, send an email to tips@thestreet.com.

Gary Krakow is TheStreet's Senior Technology Correspondent.