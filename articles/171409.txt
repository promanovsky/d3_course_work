EBAY Beats, but Taxes Eat Up Profits

Ebay Inc ( EBAY ) reported earnings after the bell today, posting an EPS of $0.58, and Revenues of $4.26 billion. These numbers beat both the Zacks Consensus Earnings Estimate of $0.57, and the Zacks Consensus Revenues Estimate of $4.231 billion.

The company increased the upper end of their guidance for Q2 2014, from $4.40 billion to between $4.325 billion and $4.425 billion. But the company did decrease their Q2 guidance, excluding one-time items, of $0.67 to $0.69, from $0.70. Management also touched on the Marketplaces segment, their e-commerce platform, came in at $2.2 billion, which was a 12% gain from a year ago numbers.

Another announcement was that management decided to repatriate $9 billion of foreign cash. This caused a one-time discrete tax charge of $3 billion dollars, but brought $6 billion back into the U.S. This hurt both the top and bottom lines for the company in Q1.

Over the past several months Carl Icahn has been making waves, demanding that Ebay split off the payments subsidiary PayPal, and add two independent directors to the board. On May 13, Mr. Icahn jointly announced an agreement whereas he would withdraw his proposal to separate the segments, but the board capitulated to Mr. Icahn and added David Dorman as an independent director to its board. Mr. Icahn did state later, that he still believes that PayPal would be better off as a separate company. Moreover, Mr. Icahn stated that, "he (CEO John Donahoe) and I have agreed to meet regularly when he is in New York to discuss strategic alternatives." So this issue might not be completely finished, at least in Carl's eyes.

In afterhours trading, EBAY is down over 3% on high volume. Zacks will publish a fully detailed earnings report tomorrow morning.

Want more insights from Zacks? See our latest free report 5 Stocks to Double . Click here to receive this free report now >>>

EBAY INC (EBAY): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.