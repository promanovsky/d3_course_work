More American Horror Story news heading your way! Finally, the hotly anticipated fourth season of the Emmy award-winning show has a setting and a vague plot outline, as revealed by showrunner Ryan Murphy. According to Murphy’s announcement from earlier this week that the new installment of the series would be set in early-1950s Florida. Jessica Lange, who has appeared in each of the series' first three seasons, will play a German ex-pat who runs a carnival sideshow.



Showrunner Ryan Murphy came out this week with several AHS announcements.

The Shield’s Michael Chiklis has also been announced for a part in the upcoming season. Chiklis will play the former husband of Kathy Bates' character and the father of Evan Peters' character. In more good news, queens of everything Sarah Paulson, Angela Basset and Frances Conroy will all return for the new season, playing performers in Lange's carnival – it all sounds very fun and exciting. Or terrifying and often gross, depending on your point of view.



Jessica Lange [l] and Angela Bassett [r] - a winning team, if ever there was one.

All this was revealed by Murphy at PaleyFest this week, where he also shared the following sound bite about the show’s last season, Coven: "This was a season about female empowerment," he said, and co-creator, Brad Falchuk agreed (quotes via Rolling Stone): "Each season is about something behind the genre, and this season was about women, and about mothers and daughters. You can’t have a more female horror genre than witches. If you want to tell a female story, there’s no other choice."

Beyond a fundamental misunderstanding of the whole “women are people” thing, it sounds like Murphy has it down for season 4. Unfortunately, there is no premiere date yet.



Michael Chiklis will join the cast for Season 4.

Read more: American Horror Story - What Do We Know So Far?



Read more: Lea Michelle Gunning for AHS Role