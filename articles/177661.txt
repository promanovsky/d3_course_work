Xbox Nearing 'Halo' Development Deal With Showtime

The live-action series based on the "Halo" video game comes from executive producer Steven Spielberg.

Xbox Entertainment Studios is nearing a deal to develop its highly anticipated live-action Halo project at Showtime, THR has confirmed.

If a deal is completed, the Santa Monica studio would bring the project from EP Steven Spielberg first to the cable network and later its Xbox consoles.

Representatives from Xbox and Showtime declined to comment.

Studio chief Nancy Tellem presented at the Digital Content NewFronts on Monday, showing off the company's upcoming programming slate. The presentation was noticeably light on updates about Halo, but at a media preview the week before Tellem intimated that she hoped to soon have news about the project, which was created in partnership with 343 Industries and Spielberg's Amblin Television.

Xbox's current programming slate includes a soccer reality series timed to the 2014 World Cup and a live stream of the Bonnaroo music festival. Humans -- its first scripted drama, based on the Swedish format -- will debut in 2015 on both Xbox consoles and Britain's Channel 4.

Email: Natalie.Jarvey@THR.com

Twitter: @NatJarv