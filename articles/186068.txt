Video game publisher Activision Blizzard Inc (ATVI), Tuesday reported a decline in first-quarter profit, hurt mainly by a drop in revenues. Nonetheless, results for the quarter topped Wall Street estimates, partly on strong sales of Skylanders and Call of Duty franchises.

Activision detailed some weak earnings guidance for the second quarter, but lifted its expectations for calendar year 2014. Following the announcement, Activision shares gained 4 percent in after-hours trade on the Nasdaq.

Activision's better-than-expected results were attributed to sales of Skylanders and Call of Duty franchises, as well as record digital sales of World of Warcraft, Diablo, and Hearthstone: Heroes of Warcraft.

CEO Bobby Kotick has been trying to buoy growth as Activision faces competition from next generation consoles from other companies, coupled with gamers' preference for low-ticket games played on smartphones and tablets. Kotick said Activision has a strong product pipeline for the balance of the year.

For the first quarter, Santa Monica, California-based Activision posted quarterly net earnings of $293 million, compared with $456 million a year ago. On a per share basis, earnings for the quarter remained unchanged at $0.40, reflecting a lower share count.

Excluding items, adjusted earnings for the quarter were $240 million or $0.19 per share, compared with $247 million or $0.17 per share in the prior year. On average, 23 analysts polled by Thomson Reuters expected earnings of $0.10 per share for the quarter. Analysts' estimates typically exclude special items.

Revenues for the first quarter totaled $1.11 billion, compared with $1.32 billion last year. On an adjusted basis, revenues for the quarter were lower at $772 million, compared with $804 million a year ago. Twenty-three analysts had a consensus revenue estimate of $688.27 million for the quarter.

For the second quarter, Activision expect adjusted earnings in the range of $0.01 per share and adjusted revenues of $600 million. Analysts currently expect earnings of $0.05 per share on revenues of $580 million.

For calendar year 2014, Activision now projects, on an adjusted basis, earnings of $1.27 per share and revenues of about $4.68 billion. The company had earlier estimated earnings of $1.26 per share and revenues of $4.6 billion.

Analysts currently expect earnings of $1.29 per share on revenues of $4.65 billion.

Activision stock closed Tuesday at $19.31, down $0.11 or 0.57%, on a volume of 8 million shares. In after hours, the stock gained $0.92 or 4.76%, to trade at $20.23. In the past year, the stock trended in the range of $13.27 - $21.50.

For comments and feedback contact: editorial@rttnews.com

Business News