Hundreds of Jewish travellers gathered in Kathmandu today to attend what organisers claim is the world's biggest Passover celebration as food supplies, delayed for weeks due to a diplomats' strike, arrived hours before the feast.

Chabad House Nepal, which organises the celebration every year, usually relies on the Israeli embassy to help import supplies for the feast in the impoverished Himalayan nation.

The orthodox Jewish group has organised the event for more than a decade, overcoming roadblocks ranging from a ten-year Maoist insurgency to the recent strike by Israeli diplomats.

"We were prepared to organise the celebration with or without the shipment, but we are very glad it has arrived," said Rabbi Chezki Lifshitz, co-director of Chabad House Nepal.

Supplies, stuck in the Indian port of Kolkata since last month, arrived this afternoon and included one tonne of unleavened bread, 2,000 bottles of wine, 3,000 pieces of gefilte fish (poached ground fish) and other traditional Jewish foods.

Lifshitz had earlier appealed to Israeli backpackers to bring food supplies with them to Kathmandu before diplomats ended their strike over salaries two weeks ago and returned to work.

"Every year we have a story to tell... But we find a way to come together, celebrate and make the feast successful," Lifshitz told AFP.

Passover is usually celebrated with family, but 25 years ago the Israeli embassy invited Jewish visitors to Kathmandu to share in a meal which kicked off the annual tradition.

This year the table was set for more than 1,000 visitors at an upscale hotel in Kathmandu.

"Many travellers plan their trips so that they can be in Nepal to attend this," said Yisroel New, a 24-year-old American student who flew to Kathmandu last week to help organise the feast.

"Many feel that this is an eye-opening experience for them to witness the ritual in its authenticity," New told AFP.

For Passover, kosher laws forbid leavened and fermented grain products. These include foods like breads and pasta.

The requirement for unleavened food is because when the Jews escaped Egypt they had no time to let their breads rise. Instead, for Passover, Jews eat matzo, an unleavened bread.