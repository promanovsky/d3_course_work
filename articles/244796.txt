Sleep apnea has long been associated with heart issues given the condition stops the heart from beating but now a new study reveals it may also play a role in hearing loss.

An estimated 18 million Americans reportedly suffer from sleep apnea, a medical condition in which the body stops breathing during sleep.

A study conducted by researchers at the Albany Medical Center reveals both high and low frequency hearing impairment has been linked with sleep apnea.

The study involved nearly 14,000 people from the Hispanic community.

"We found that sleep apnea was independently associated with hearing impairment at both high and low frequencies after adjustment for other possible causes of hearing loss," said lead author Amit Chopra, MD, currently at the Albany Medical Center in New York, stated in a press release on the study.

Among the 13,967 study subjects, 9.9 percent had at least moderate sleep apnea, 19.0 percent had high frequency hearing impairment, 1.5 percent had low frequency hearing impairment, and 8.4 percent had both high and low frequency hearing impairment.

The study reveals hearing impairment appears to be more common among Cuban and Puerto Rican individuals and those with a higher body mass index.

Sleep apnea was independently associated with a 31 percent increase in high frequency hearing impairment, a 90 percent increase in low frequency hearing impairment, and a 38 percent increase in combined high and low frequency hearing impairment in analysis.

"Patients with sleep apnea are at increased risk for a number of comorbidities, including heart disease and diabetes, and our findings indicate that sleep apnea is also associated with an increased risk of hearing impairment," said Chopra.

"The mechanisms underlying this relationship merit further exploration. Potential pathways linking sleep apnea and hearing impairment may include adverse effects of sleep apnea on vascular supply to the cochlea via inflammation and vascular remodeling or noise trauma from snoring."

Another study this week also focused on sleep apnea, but this time on how the link to strokes is as high for women as for men.

The report claims earlier studies may have underestimated the connection between females with sleep apnea and the risk of stroke.

"Our results could have a substantial impact on our thinking of the risks associated with sleep apnea in women," the study's lead author, Dr. Suzie Bertisch, of Beth Israel Deaconess Medical Center in Boston, said in a news release from the American Thoracic Society.

"From a clinical standpoint, the results could help clinicians provide more proactive treatment for reducing cardiovascular risk in their female [sleep apnea] patients," she added.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.