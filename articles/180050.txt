Scientists have found that when the blood of young mice is injected into older mice it rejuvenates the brain and muscle function in the older mice, raising hopes that molecules in the blood may be identified so older humans can experience the same benefits.

The scientists, working on three related studies, combined the blood circulation of an old mouse to that of a young one and found dramatic improvements in the older mouse’s muscle and brain with stem cells in both the areas were able to produce neurons and muscle tissue after four weeks, the New York Times said Monday. The scientists also discovered that injecting a special protein, found in young blood, into old mice made them stronger than their control peers, who were given only saline.

“I am extremely excited,” Rudolph Tanzi, a professor of neurology at Harvard Medical School, who was not involved in the research, said. “These findings could be a game changer.”

The first study, published Sunday in Nature Medicine, was conducted by a group of researchers from Stanford University and the University of California at San Francisco. While the other two studies, published in the journal Science on Sunday, came from researchers at the Harvard Stem Cell Institute.

“The Stanford group has been working in this area for a while, but we weren’t involved in their study,” Amy Wagers, the Science study author and a biologist of the Harvard institute, told The Washington Post. “All of the studies are very consistent — the data are complementary and support one another.”

According to the first study, led by Saul Villeda of UC San Francisco, the process of extracting the plasma from the younger mice and injecting it into the older ones’ brain helps create new neuronal connections, reversing the effects of age and improving the older mice's cognitive functions.

Here is an excerpt from the study:

At the cognitive level, systemic administration of young blood plasma into aged mice improved age-related cognitive impairments in both contextual fear conditioning and spatial learning and memory. Structural and cognitive enhancements elicited by exposure to young blood are mediated, in part, by activation of the cyclic AMP response element binding protein (Creb) in the aged hippocampus. Our data indicate that exposure of aged mice to young blood late in life is capable of rejuvenating synaptic plasticity and improving cognitive function.

The scientists said that the discovery could help develop better treatments for Alzheimer's disease and other brain conditions caused by aging.

The other two studies, led by Wagers, showed that injections of a protein called GDF11 found in humans as well as mice improves the hearts of the old mice. It also revitalizes the stem cells in the older mice, leading to the creation of new tissues.

“We think an effect of GDF11 is the improved vascularity and blood flow, which is associated with increased neurogenesis,” Lee Rubin of Harvard’s Department of Stem Cell and Regenerative Biology said in a statement. “However, the increased blood flow should have more widespread effects on brain function. We do think that, at least in principle, there will be a way to reverse some of the cognitive decline that takes place during aging, perhaps even with a single protein.”