hidden

Microsoft India today slashed the price of its gaming console XBox 360 250GB by 12 per cent to Rs 21,990.

It has also reduced the price of the XBox 360 250GB Kinect by 8.3 per cent to Rs 31,990.

“We are committed to offer great experiences to our consumers at compelling price points. This price drop on 250GB Sku’s, coupled with our recently launched Xbox Live Gold Rush promo, will provide great value to our community of fans and gamers,” Microsoft India Director (Interactive Entertainment Business) Anshu Mor said in a statement.

These consoles were earlier available for Rs Rs 24,990 (250GB) and Rs 34,890 (Kinect), the statement added.

The consoles are currently available in the market with additional freebies like free games and Xbox Live Gold membership. On subscribing to the Xbox Live Gold programme, gamers will get 36 free games, discounts on future purchases as well as access to premium entertainment apps and the Xbox online multiplayer gaming network.

The gamers will also get two free games every month and access to free-to-play titles such as Warface and World of Tanks. Subscribers can also redeem nine Xbox Arcade Games for free through the year.

The price drop, however, is limited to the 250GB Xbox consoles and does not extend to the 4GB 360 and 4GB Kinect.

PTI