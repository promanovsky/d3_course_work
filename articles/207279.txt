Some three million deaths worldwide a year are linked to alcohol abuse, it is claimed.

More than three million deaths worldwide in 2012 were due to harmful use of alcohol - and Europe is the region with the highest consumption of alcohol per capita, according to a report.

Alcohol consumption can not only lead to dependence but also increases the risk of developing more than 200 diseases including liver cirrhosis and some cancers, the World Health Organisation (WHO) said.

A report launched by the United Nations health agency found harmful use of alcohol - drinking that causes detrimental health and social consequences for the drinker, the people around the drinker and society at large - led to 3.3 million deaths around the world in 2012.

The Global status report on alcohol and health 2014 also found that harmful use of alcohol makes people more susceptible to infectious diseases such as tuberculosis and pneumonia.

The report provides country profiles for alcohol consumption in the 194 WHO member states, the impact on public health and policy responses.

Globally, Europe is the region with the highest consumption of alcohol per capita, with some of its countries having particularly high consumption rates, according to WHO.

Trend analysis shows that the consumption level is stable over the last five years in the region, as well as in Africa and the Americas, though increases have been reported in the South-East Asia and the Western Pacific regions.

Dr Oleg Chestnov, WHO assistant director-general for non-communicable diseases and mental health, said there is "no room for complacency".

"More needs to be done to protect populations from the negative health consequences of alcohol consumption.

"The report clearly shows that there is no room for complacency when it comes to reducing the harmful use of alcohol," he said.

On average every person in the world aged 15 or older drinks 6.2 litres of pure alcohol per year.

But as less than half the population (38.3%) actually drinks alcohol, this means that those who do drink consume on average 17 litres of pure alcohol annually, WHO said.

The report also points out that a higher percentage of deaths among men than among women are from alcohol-related causes - 7.6% of men's deaths and 4% of women's deaths - though there is evidence that women may be more vulnerable to some alcohol-related health conditions compared to men.

The report's authors said there is concern over the steady increase in alcohol use among women.

Dr Shekhar Saxena, director for mental health and substance abuse at WHO, said: "We found that worldwide about 16% of drinkers engage in heavy episodic drinking - often referred to as 'binge-drinking' - which is the most harmful to health.

"Lower-income groups are more affected by the social and health consequences of alcohol. They often lack quality health care and are less protected by functional family or community networks."

The report also highlights the need for action by countries including national leadership to develop policies to reduce harmful use of alcohol, national awareness-raising activities and health services to deliver prevention and treatment services, in particular increasing prevention, treatment and care for patients and their families, and supporting initiatives for screening and brief interventions.