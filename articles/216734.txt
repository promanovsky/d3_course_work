When not busy saving lives, you imagine paramedics to drive around stoically, reflecting on what horrors they have been dispatched to that day. But not this one.

Scroll down for the video

The man is seen cranking New York radio station Hot 97 and furiously voguing to a club mix of Rihanna's "Pour It Up".

"Oh!" he proceeds to bellow into his walkie talkie as the beat drops, unprofessionally, perhaps irresponsibly, but clearing having a wonderful time.

The CCTV footage of his freak out states 'DRIVING: NORMAL', but it is anything but.

It is not clear where the EMT is from (or whether he is still in employment after this) but the station suggests the New York or New Jersey area.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here