Zac Efron recently took part in a very candid interview with The Hollywood Reporter, speaking in length about his past drug and alcohol issues.

But the 26 year-old doesn't regret this at all, in fact he feels "great" about it.

Efron spoke with Matt Lauer on NBC's Today on Monday (May 5th), about how he feels now his addiction past is no longer a secret.

"Actually, it was a weight off my chest. It was great. It was a fantastic time," he told the host.

The Hollywood hunk explained how he could apply these issues into his role in 'Neighbors.'

"It was a rough year, It's kind of funny: Neighbors is such a great icing on the cake for all of this," he said. "I was able to kind of channel all of that and put it back into this movie. I feel great. I feel really blessed to be here working with this group of group of guys. I couldn't be better right now. I'm really excited."

Efron was on the daytime talk show with his co-star Dave Franco, who expressed his excitement for audiences to see Zac in a different kind of role and what it could mean for his career.

"I'm really excited for Zac because I think a lot of people have this preconception of who he is, and once you see this movie, I think his teenybopper image is going to be completely shattered, and all these new projects that wouldn't have come to him otherwise are just going to flood toward him," Franco said.

Watch Efron and Franco's appearance on 'Today' below



MORE: Seth Rogen Shoots Zac Efron After Pranks Go Wrong

The future doesn't just look bright for the 'High School Musical' star's film career, but for his personal life as well.

MORE: 'Neighbors' Reviews: Cheap, Nasty, Juvenile and Brilliant

During the candid interview with THR, Efron explained how he had recently joined Alcoholics Anonymous and now sees a therapist. "I just started going," he says of AA. "And I think it's changed my life. I'm much more comfortable in my own skin. Things are so much easier now."

Watch the trailer for 'Neighbors' here:

