It was certainly one of the most lavish wedding ceremonies celebville has ever hosted, with Kanye West treating his new bride Kim Kardashian like a royal princess – but the rapper’s generosity hasn’t stopped there. West is rumoured to have forked out on a hefty wedding gift for Kim, a portrait of the gorgeous brunette.

The 36-year-old singer reportedly commissioned London artist Bambi to create the master-piece.

”It’s the first non-narcissistic thing Kanye has done because he specifically didn’t want to be in the painting himself,” a source told Daily Star. ”He says he wants Kim looking as much like a princess as possible.”

The insider added: ”Kanye has said he doesn’t care how much the painting costs. This is likely to be Bambi’s biggest sale to date.”

The showbiz couple, who are parents to 11-month-old daughter North, said “I do” in a romantic ceremony in Florence over the weekend and were joined by 600 guests. Kim flaunted her curvaceous body in a Givenchy figure-hugging custom-made gown for the ceremony.

Mr & Mrs Kanye West are now rumoured to be enjoying the first few days of their European honeymoon in Ireland before they embark on a mini Eurotrip over the next few weeks.