NASA has a landing-on-Mars problem, and that strange flying-saucer-shaped test vehicle in the picture above may help them solve it.

Since 1976, when NASA’s Viking probe first landed on the Red Planet, the agency has relied on the same parachute design to help all its Mars probes and rovers descend to the planet’s surface intact.

So far, that Viking-era parachute system has worked fabulously. Most recently, it helped the 1-ton Curiosity rover survive its ‘Seven Minutes of Terror’ and land safely on the planet. But if NASA wants to land bigger spacecraft on Mars, the landing system will need to change.

“We were really pushing the envelope on what we can squeeze out of that parachute with Curiosity,” said Mark Adler, who is responsible for testing new decelerating technologies at NASA’s Jet Propulsion Laboratory in La Cañada Flintridge. “Curiosity is awesome, but we want bigger.”

Advertisement

To that end, a team of engineers at JPL has been trying to come up with new ways to slow down a big spacecraft moving faster than the speed of sound, and this must be done in the thin atmosphere of Mars without adding a lot of weight to the spacecraft.

The project is known as the Low Density Supersonic Decelerators (LDSD) mission.

On Wednesday, the team presented to the media an almost-finished rocket-powered vehicle that would allow them to test some of their ideas.

Sewn into the underside of the test vehicle is a giant parachute 100 feet in diameter, about twice as big as the one used by Curiosity.

Advertisement

Around the rim of the vehicle is a drag device inspired by a puffer fish’s ability to change its size rapidly without changing its mass. It is essentially an inner tube that can inflate in a fraction of a second, changing the diameter of the saucer from 15 feet to 20 feet, which will help slow it enough to deploy the giant parachute.

Testing these new technologies has proved challenging, however. The new parachute is so big that it won’t fit in any of the wind tunnels that NASA traditionally uses to test its parachutes. So, the LDSD team had to try something else.

In a few days, the agency will move the vehicle from the clean room at JPL where it is being built to the Navy’s Pacific Missile Range Facility in Hawaii. There, in the first week of June, it will be carried to an altitude of 120,000 feet by a giant balloon. Then rockets on the vehicle will take over, pushing it to an altitude of 180,000 feet and helping to reach supersonic speeds. The thin atmosphere at this altitude is similar to the thin atmosphere on Mars.

Once the vehicle is going 3.5 times the speed of sound, the inner tube should inflate, slowing the vehicle down to 2.5 times the speed of sound, when it is safe to release the parachute.

Advertisement

Will it work? Ian Clark, LDSD principal investigator, said he kind of hoped it wouldn’t -- at least not yet.

“It’s still extremely experimental, and we are pushing beyond any technologies that we already have,” he said. “If it all goes successfully, it means we weren’t pushing hard enough.”