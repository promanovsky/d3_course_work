The recently launched HTC One (M8) comes with a brand new version of Sense on top of Android 4.4 KitKat that brings several new software features and improves existing ones. To help users better understand Sense 6 and what it has to offer, HTC has published seven videos on its ShowMe YouTube channel, in which it demos some of the most important Sense 6 features – additionally, the company has also released three First Look videos highlighting the new HTC One’s design and camera features.

The Sense 6 tips videos show users how to access the features described, covering the UFocus and Bokeh camera effects; Duo Camera’s Foregrounder, Dimension Plus and Seasons features; Zoe Camera and Zoe functionality; the updated HTC BlinkFeed app; Dot View case notifications and gestures controls; and the new Motion Launch gestures. Strangely, one of the phone’s more interesting features – the new battery life saving tech – isn’t demoed in a video of its own.

BGR has thoroughly reviewed the HTC One (M8,) highlighting several of these new Sense 6 features.

The seven new HTC One videos follow below.













