Who needs Facebook Inc (NASDAQ:FB)’s $3 billion? According to a recent article in the Wall Street Journal, Snapchat Inc founders Evan Spiegel and Bobby Murphy are now officially billionaires. The article claims that an ongoing financing round including at least $20 million from venture capital firm Kleiner Perkins Caufield & Byers values the firm at a cool $10 billion.

The WSJ sources also mentioned that DST Global, Yuri Milner’s Russian investment firm, also invested in Snapchat earlier this spring in a round that valued the company at nearly $7 billion.

Exclusive: Izzy Englander’s Millennium Management Focuses On Longer Term Capital Earlier this month, Greylock Capital Associates, an emerging markets hedge fund, filed for bankruptcy protection in New York assets under management dwindled from nearly $1 billion in 2017 to $450 million at the end of 2020. After three years of losses, Bloomberg reported that assets could drop below $100 million by the end of the Read More

To date Snapchat has raised $163 million in five funding rounds from seven investors, including the recent $50 million from hedge fund Coatue Management.

Snapchat founders likely worth over $2 billion each

Forbes says that Spiegel and Morphy could now be worth as much as $2 billion each. They estimated in an earlier story on Snapchat that each man still owned around 25% of the company as of December 2013.

And even though the company has sought further funding since then from both Coatue Management and now Kleiner Perkins, given the relatively small numbers, they probably have not diluted their stakes significantly.

Forbes also reported a couple of weeks ago that Snapchat had authorized the sale of 17.4 million shares at just $0.001 each. The purpose for this sale was not known. However, those 17.4 million shares only represent around a 4% stake in Snapchat, so even if the new authorization was tied to the new funding round, it still means Spiegel and Murphy have not parted with much of their equity yet.

Snapchat continues to grow

According to reports from a number of industry analysts, Snapchat’s user base continues to expand rapidly. Channel checks suggest it has grown to 100 million monthly active users as of August with no sign of slowing down. The social media and digital image concern has not earned a single dollar yet, but is in the process of exploring possible revenue models such as advertising, money-transfer and content discovery.