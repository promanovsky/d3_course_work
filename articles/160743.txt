Time Warner Cable, Inc. (TWC), a provider of video, high-speed data and voice services, Thursday said first-quarter profit increased from the previous year, amid a modest growth in revenues. Adjusted earnings topped expectations, while revenues fell shy of estimates.

The company is being bought by cable giant Comcast Corp. (CMCSK,CMCSA) for $45.2 billion in an all-stock deal.

Time Warner Cable CEO Rob Marcus said: "Our residential subscriber growth was the best in five years and our services revenue growth was close to 25 percent. These results underscore our commitment to deliver on our financial and operating plan as we prepare for our merger with Comcast."

Net income attributable to TWC shareholders rose to $479 million or $1.70 per share from the prior-year quarter's $401 million or $1.34 per share.

Adjusted net income attributable to TWC Shareholders totaled $1.78 per share. This excludes some merger-related and restructuring costs and impact of certain state and local tax matters.

On average, 23 analysts polled by Thomson Reuters expected earnings per share of $1.68. Analysts' estimates typically exclude one-time items.

Revenue rose to $5.582 billion from $5.475 billion. Analysts expected revenues of $5.64 billion.

The revenue growth was driven primarily by 24.4 percent increase in business services revenue and 10.8 percent growth in residential high-speed data revenue.

Average monthly revenue per residential customer relationship or ARPU grew 0.6 percent to $105.45. Residential high-speed data ARPU increased 8.7 percent to $46.32.

Segment-wise, Residential Services' revenue slid nearly 1 percent, as a result of decreases in video and voice revenue, partially offset by an increase in high-speed data revenue.

Video customers fell 34,000, while High-speed data and Voice customers added 269,000 and 107,000, respectively.

In Business Services, revenue increased more than 24 percent, primarily due to increases in high-speed data and voice subscribers, organic growth in cell tower backhaul revenue and $29 million of revenue from DukeNet, which was acquired on December 31, 2013.

TWC closed up 1 percent on Wednesday at $139.87. The stock is gaining 1.5 percent in pre-market activity.

For comments and feedback contact: editorial@rttnews.com

Business News