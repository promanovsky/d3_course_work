That puts the end to quantitative easing at the front end of market expectations. This is the third QE cycle in a program that has stretched the central bank's balance sheet to $4.4 trillion.

"If the economy progresses about as the Committee expects, warranting reductions in the pace of purchases at each upcoming meeting, this final reduction would occur following the October meeting."

"Participants generally agreed that if incoming information continued to support its expectation of improvement in labor market conditions and a return of inflation toward its longer-run objective, it would be appropriate to complete asset purchases with a $15 billion reduction in the pace of purchases in order to avoid having the small, remaining level of purchases receive undue focus among investors," the minutes stated.

Federal Reserve officials indicated at their June meeting that the monthly bond-buying program could end sooner rather than later—with an October exit growing increasingly likely.

Read MoreFed tapers $10 billion more; economic outlook cut



Markets showed measured reaction to the news, with stocks adding to modest gains and government bond yields slightly lower after the two-year note momentarily touched a three-year high.

"Everything was expected, I think the markets continue on," Ernie Cecilia, of Bryn Mawr Trust, told CNBC. "I don't think there was anything that troubling, certainly from a market perspective, or that surprising, from a market perspective."



The Fed at its June meeting voted to reduce the program another $10 billion to $35 billion, while keeping its target funds rate near zero.

Minutes indicated little taste for increasing rates ahead of schedule, with market expectations that the first hike probably won't come until mid-2015 despite improving economic data that has seen both unemployment and inflation at or near the Fed's target levels.

Read MoreBoom! Job growth accelerates, rate falls to 6.1%



"The Committee again stated that it currently anticipated that it likely would be appropriate to maintain the current target range for the federal funds rate for a considerable time after the asset purchase program ends, especially if projected inflation continued to run below the Committee's 2 percent longer-run goal, and provided that longer-term inflation expectations remained well anchored," the minutes said.

There is an active debate over the timing of the Fed's first rate hike, with some arguing the central bank is prepared to move more quickly than markets might expect.

The QE program—often referred to as "money printing" though it is done entirely with digitized funds—has accompanied a massive surge in the stock market while generating uneven results in the broader economy. As payrolls grow by more than 200,000 a month wages have remained stagnant and the first quarter saw gross domestic product contract 2.9 percent.

Some Fed Open Market Committee members worried that ballooning asset prices pose a threat.

Read MoreThe rich get richer as stock buybacks surge



"Participants also discussed whether some recent trends in financial markets might suggest that investors were not appropriately taking account of risks in their investment decisions. In particular, low implied volatility in equity, currency, and fixed-income markets as well as signs of increased risk-taking were viewed by some participants as an indication that market participants were not factoring in sufficient uncertainty about the path of the economy and monetary policy," the minutes stated.

—By CNBC's Jeff Cox. CNBC.com's Michelle Fox contributed to this report.