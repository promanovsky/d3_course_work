TORONTO -- It turns out the best-laid research plans involving mice and men have a bit of a wrinkle -- the lab rodents appear to react differently to male scientists during experiments than they do to females.

And that could skew results of numerous medical studies in which the critters are used as stand-ins for humans, suggests Jeffrey Mogil, a professor of behavioural neuroscience at McGill University, whose lab set out to test the responses of mice to male versus female experimenters.

Mogil directs a group of scientists who focus on pain, including its genetic and neurological underpinnings. Part of their work involves inducing discomfort in lab mice with the ultimate goal of finding drugs that can relieve pain in humans.

But a funny thing kept happening: sometimes, the mice didn't react as expected when given an injection in a limb meant to induce pain and pain behaviours, including a specific set of facial expressions known as the mouse grimace scale previously developed by Mogil's lab.

Initially, the experimenters thought there was something wrong with the inflammatory agent they had injected.

Yet when they left the room, the mice would start exhibiting signs they were in pain, suggesting that the experimenters themselves were somehow causing pain inhibition -- or analgesia -- in the animals.

"This was something that people had sort of whispered about at (scientific) meetings for years, but as far as we can tell, no one ever tried to investigate whether it was true," said Mogil, who asked his lab to conduct dozens of studies to determine whether the suspicion had any validity.

"And to our great surprise it was true, but only half-true because it was only male experimenters and not female experimenters (that affected the mice)," he said from Montreal.

In their study, published online Monday in the journal Nature Methods, Mogil's team showed that lab rodents become stressed in the presence of male researchers. Stress leads to the release of chemicals in the body that act as pain suppressors.

And the underlying reason for this rodent response? Males smell different than females.

"We found that this was olfactory because we could replace the male experimenter with a T-shirt worn by a male and that also produces analgesia," he said. "And it has nothing specifically to do with humans, because you can use bedding from almost any animal, as long as it's male and has testosterone.

"They're not analgesic to bedding of mice they know. They're only analgesic to bedding of mice they don't know or guinea pigs or rats or cats or dogs, it doesn't matter.

"What's driving this effect are olfactory stimuli that are released from the armpit, specifically ones that are released from the armpits of men in higher concentrations than in women."

The finding is important because researchers need to be aware that the sex of an experimenter may alter the outcome of tests -- and that could potentially invalidate research conclusions.

"There's been a lot of wringing of hands and gnashing of teeth over the last year or so about the idea that pre-clinical research doesn't replicate," said Mogil, explaining that scientists have not been able to reproduce the findings of some high-profile studies involving animal models like lab mice.

Being able to repeatedly get the same results when studying an experimental drug or procedure from one study to another is the gold standard of medical research.

So when animal research findings can't be replicated, some researchers conclude they must be invalid, caused perhaps by false-positive test results.

"What these data suggest strongly," Mogil said of his lab's findings, "is that there's another explanation, that they're not false positives at all. What it is is that different laboratories have slightly different environments where their studies are conducted.

"So I think this provides a big part of the reason why it's so hard to replicate. You change any little thing in the laboratory environment and your results will be different."

Douglas Wahlsten, a genetic neuroscientist who has studied mouse behaviour extensively, says the McGill paper shows "very strong evidence" that chemical odours emanating from males can influence test results.

But Wahlsten, a professor emeritus at the University of Alberta, said similar studies need to be done using different strains of lab mice, as well as in different laboratory environments to see if the findings hold up.

"The way a lab is built and set up, the way the air circulates ... are really critically important when you want to study odours," he said from Salt Spring Island, B.C., where he now lives.

Wahlsten believes the McGill study will have some influence on other researchers, and he noted the so-called experimenter effect "needs to be taken very seriously."

"I think that the gender of the person doing the test is a factor, and they've shown this," he said. "It's something we need to be aware of and we need to control for it to the extent that we can."

Mogil said the study obviously doesn't mean that scientists should get rid of male experimenters and hire only females, but should take the gender difference into account when analyzing data and reporting results.

"This is an example of things we should be taking into account, but we don't. People simply don't put in the methods section (of their published studies) what the gender of the experimenter was.

"But these data suggest that starting now, they really need to."

The goal of studies like this is to show scientists how to do better research, no matter what issue they are examining, he said.

"This is a finding that's going to make scientific research better, more reliable than it's been before."