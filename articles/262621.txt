Top Healthcare stocks:

JNJ: +0.58%

PFE: +0.44%

ABT: -1.47%

MRK: -1.09%

AMGN: +0.01%

Healthcare shares were mixed in pre-market trade on Wednesday.

In healthcare stocks news, Sanofi ( SNY ) said it's planning to seek approval to sell Eli Lilly ( LLY )'s erectile dysfunction drug Cialis without a prescription after the companies reached an agreement on marketing rights. The drug is currently available by prescription only as a treatment for men with erectile dysfunction.

Sanofi will apply for approval of Cialis as an over-the-counter treatment in the U.S., Europe, Canada and Australia, and will hold exclusive rights to market Cialis OTC after receipt of all necessary regulatory approvals.

And, Valeant Pharmaceuticals (VRX, VRX.TO) said it's selling its rights to skincare products Restylane, Perlane, Emervel, Sculptra, and Dysport to Nestle S.A. for $1.4 billion in cash. Nestle is acquiring Galderma S.A., which supplies the products to Valeant, and would expect to operate the acquired assets through Galderma.

VRX was flat in U.S. pre-market trading on Wednesday and closed at $129.98 Tuesday.

Finally, Valeant also hiked its acquisition offer for Allergan ( AGN ) today, detailing that for each Allergan share it is offering $58.30 in cash, representing an increase of $10, or approximately 21%, in the cash portion of the consideration.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.