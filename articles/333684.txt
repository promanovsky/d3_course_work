Meshach Taylor, who played a lovable ex-convict surrounded by boisterous Southern belles on the sitcom "Designing Women" and appeared in numerous other TV and film roles, died of cancer at age 67, his agent said Sunday.

Taylor died Saturday at his home near Los Angeles, according to agent Dede Binder.

Taylor got an Emmy nod for his portrayal of Anthony Bouvier on "Designing Women" from 1986 to 1993. Then he costarred for four seasons on another successful comedy, "Dave's World," as the best friend of a newspaper humor columnist played by the series' star, Harry Anderson.

Other series included the cult favorite "Buffalo Bill" and the popular Nickelodeon comedy "Ned's Declassified School Survival Guide."

Taylor's movie roles included a flamboyant window dresser in the 1987 comedy-romance "Mannequin" as well as "Damien: Omen II."

He guested on many series including "Hannah Montana," ''The Unit," ''Hill Street Blues," ''Barney Miller," ''Lou Grant," ''The Drew Carey Show," and, in an episode that aired in January, "Criminal Minds," which stars Joe Montegna, with whom Taylor performed early in his career as a fellow member of Chicago's Organic Theater Company. Taylor also had been a member of that city's Goodman Theatre.

Subscribe to the Entertainment newsletter By clicking Sign up, you agree to our privacy policy.

The Boston-born Taylor started acting in community shows in New Orleans, where his father was dean of students at Dillard University. He continued doing roles in Indianapolis after his father moved to Indiana University as dean of the college of arts and sciences.

After college, Taylor got a job at an Indianapolis radio station, where he rose from a "flunky job" to Statehouse reporter, he recalled in an interview with The Associated Press in 1989.

"It was interesting for a while," he said. "But once you get involved in Indiana politics you see what a yawn it is."

Resuming his acting pursuit, he set up a black arts theater to keep kids off the street, then joined the national touring company of "Hair." His acting career was launched.

After "Hair," he became a part of the burgeoning theater world in Chicago, where he stayed until 1979 before heading for Los Angeles.

Taylor played the assistant director in "Buffalo Bill," the short-lived NBC sitcom about an arrogant and self-centered talk show host played by Dabney Coleman. It lasted just one season, 1983-84, disappointing its small but fervent following.

Seemingly his gig on "Designing Women" could have been even more short-lived. It was initially a one-shot.

"It was for the Thanksgiving show, about halfway through the first season," Taylor said. But producer Linda Bloodworth-Thomason told him if the character clicked with audiences he could stay.

It did. He spun comic gold with co-stars Jean Smart, Dixie Carter, Annie Potts and Delta Burke, and never left.

Meanwhile, his real life worked its way into one episode.

"We were doing some promotional work in Lubbock, Texas, and somehow Delta Burke and I got booked into the same hotel suite," he said. They alerted their respective significant others to the mix-up, then muddled through with the shared accommodations.

"When we got back I told Linda, and she put it into a show: We got stranded at a motel during a blizzard and ended up in the same bed!"

Taylor is survived by his four children and his wife, Bianca Ferguson.