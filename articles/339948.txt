From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Google argues it didn’t violate the U.S. Wiretap Act when it tapped into unencrypted Wi-Fi networks to collect data for its Street View mapping project. The highest court in the United States disagrees.

The Supreme Court today declined to hear Google’s challenge to a federal appeals court ruling that the Wiretap Act covers data on unencrypted, in-home Wi-Fi networks. That means Google must face the class-action lawsuit alleging the company illegally snooped on people from 2008 to 2010 to bolster its Street View data.

During that period, Google would automatically scan unencrypted Wi-Fi networks to verify its Street View cars’ location. In the process, it scooped up all types of “payload data,” including emails, usernames, passwords, and images sent over the web. Google acknowledged and halted the data collection in 2010, saying it was a mistake and that it never used the information as part of any product or service.

In June 2011, a U.S. District Judge in San Francisco allowed plaintiffs in several private lawsuits to pursue federal Wiretap Act claims against Google.

Last year, Google reached a settlement with 38 U.S. states and the District of Columbia, which were investigating the matter. The company agreed to pay $7 million and destroy data collected in the U.S.

But Google still faces the private lawsuit, which it attempted to dismiss. The Wiretap Act bars the interception of electronic communications, but the company argued that unencrypted Wi-Fi signals weren’t covered under the Wiretap Act, as they were “readily available to the public.” But Google’s argument relies on a decades-old definition of “radio communication,” which is inapplicable to local Wi-Fi signals, the appeals court decided — a ruling the Supreme Court has upheld.

Via: Reuters.