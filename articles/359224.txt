Headbanging is bad for your brain, according to one report which said it can lead to brain bleeding.

What? You don’t believe us? Check out this report from The Associated Press about a German man who injured himself headbanging to some Motörhead:

“Last January, doctors at Hannover Medical School saw a 50-year-old man who complained of constant, worsening headaches. The patient, who was not identified, had no history of head injuries or substance abuse problems but said he had been headbanging regularly for years — most recently at a Motorhead concert he attended with his son. “After a scan, doctors discovered their patient had a brain bleed and needed a hole drilled into his brain to drain the blood. The patient’s headaches soon disappeared. In a follow-up scan, the doctors saw he had a benign cyst which might have made the metal aficionado more vulnerable to a brain injury.”

That is no joke. It sounds like a serious medical situation can be caused by headbanging to some Motörhead or other rock bands.

An article on The Huffington Post said while the individual in the situation above may have damaged his brain with all that headbanging, the authors of a study on headbanging that cited the Motörhead incident are actually not advising people against the activity.

“‘We are not smarta**es who advise against headbanging,’ lead author Dr. Ariyan Pirayesh Islamian of the Hannover Medical School in Germany told the CBC. “‘Our purpose was not only to entertain the readership with a quite comical case report on complications of headbanging that confirms the reputation of Motörhead as undoubtedly one of the hardest rock ‘n’ roll bands on Earth, but to sensitize the medical world for a certain subgroup of fans that may be endangered when indulging themselves in excessive headbanging,’ he said.”

As for whether or not Motörhead is one of the hardest rock ‘n’ roll bands on Earth, we will let you, the reader, decide. As for us, we are going to look back fondly on Bruce Springstein’s most recent American tour and enjoy his rendition of “Highway to Hell.” Say what you will about Bruce, but never say that he is not among the hardest rockers on the planet. If anything, his rocking of AC/DC earlier this year in Australia showed just how hard the man can rock. Enjoy the video of his performance below and whatever you do, don’t give yourself a brain bleed from any headbanging.

[Image via Flickr Creative Commons]