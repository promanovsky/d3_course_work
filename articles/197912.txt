The NVIDIA Shield is an interesting device because it is a handheld gaming console powered by Android. We’ve seen such devices in the past, although the NVIDIA Shield does take things to the next level with its controls and design, not to mention its graphics as well. Now according to a recent report, Valve will be bringing Portal onto the Shield device next week, but that’s not all.

Advertising

Valve has recently teased that Half-Life 2 will also be making its way onto the Shield handheld console too. According to the folks at Android Police, they have recently received in the mail a green crowbar (pictured above). There is an inscription which reads, “What would Gordon do?”, along with Half-Life logo and the NVIDIA Shield icon.

Given that Portal is based on the same Source engine as Half-Life 2, we guess if Shield can run Portal, it should have no problems running the latter game as well. However it is still a pretty ambitious port because Half-Life 2 can be a pretty long game, depending on how you play it. Assuming that these two ports are successful, we can only imagine what other games Valve could bring to the table.

Other games that run on the Source engine includes Counter-Strike, DotA 2, Left 4 Dead, Team Fortress 2, and so on. All those games are already playable on the PC, but at the same time we’re sure that gamers will not mind being able to play such games while on the go too. In the meantime no word on when Half-Life 2 will make its way onto the NVIDIA Shield, but we’ll be keeping our eyes and ears peeled for more info. In the meantime who else is excited?

Filed in . Read more about Half-life 2, NVIDIA, NVIDIA Shield and Valve.