There are some reviews left by users, whether it be for a product, a service, or a restaurant, that does not allow the seller/owner to respond. We’re not sure why that is since it gives the person a chance to explain themselves.

Advertising

Well the good news is that if you’re planning on developing for the Windows Phone platform, or if you’ve already developed for the platform, you will be pleased to learn that thanks to a recent Windows Phone 8.1 leak, it looks like developers will soon be able to respond to reviews left on their apps.

This is thanks to a feature that allows users to flag a comment as being offensive or inappropriate, or flag it for containing virus or malware, and one of the options is to report a developer for an inappropriate response to a review.

While it would be ideal that developers react professionally to negative reviews, that’s just not how the way the world works and sometimes when it becomes too personal, the comments starts getting inappropriate and maybe even offensive, hence the feature.

However this also points to the ability for a developer to defend themselves against trolls or misunderstanding which could have led to a negative review of their app.. This feature cannot be confirmed yet but with Windows Phone 8.1 expected to be announced at the Microsoft BUILD conference, we guess we will learn more then.

Filed in . Read more about Apps and Windows Phone 8.