Actress Kerry Washington accepts the Lucy Award for Excellence in Television onstage at Women In Film 2014 Crystal + Lucy Awards

Kerry Washington made her first live television appearance since giving birth at Sunday’s BET Awards.

The Scandal star stepped out in a floral Dolce & Gabbana dress, white pumps and an emerald clutch on the red carpet before heading into the Nokia Theatre L.A. Live in Los Angeles, California.

Kerry served as a presenter at the annual prize-giving celebrating black entertainment, and was nominated for Best Actress for her work on hit US TV drama Scandal, although 12 Years a Slave star Lupita Nyong’o took home the prize.

E! News reports her appearance on the programme marked her first on live TV since welcoming her daughter Isabelle Amarachi in April. The baby girl is Kerry’s first child with husband Nnamdi Asomugha.

According to the outlet, Kerry made her first red carpet appearance after giving birth just two weeks ago, at the Women in Film's Crystal + Lucy Awards in Los Angeles on June 11.

The actress opened up to the Los Angeles Times recently about her new addition and her stellar career.

"I feel really, really blessed. I just feel really blessed that I'm kind of living extraordinary dreams come true in my work life and in my personal life,” she said.

The star, who is notoriously tight-lipped about her private life, also took to Twitter on Mother’s Day in the United States to share her gratitude with her followers.

"Well...I still won't talk about my personal life on here. But...I see your tweets. And I am filled with gratitude! XO #HappyMothersDay,” she wrote.

Online Editors