All her life Melissa Carleton wanted to have her own bundle of joy. However, when the much-awaited moment finally came Thursday, she was not fully awake to experience it.

The 39-year-old new mother from Fresno, California is in a state of comatose for ten weeks now, following a seizure that prompted her brain operation last March and left her "unable to fully wake up," as per her doctors. Carleton was diagnosed with a large brain tumor last year, a heart-wrenching news that almost dampened the dreams of a budding family she is starting with her husband Brian Lande.

Via cesarean section, the sleeping Carleton gave birth to a baby boy, who was beautifully named West Nathaniel Lande.

"Two months ago our doctors and I were struggling to ensure that our son would survive and hopefully stay in the womb until he reached 28 weeks old. We had been extremely worried that baby boy had been injured by Melissa's brain event," wrote Lande in the GoFundMe website dedicated to raise funds for Carleton.

When Carleton found out about her illness, she was in the middle of her most joyous years as a soon-to-be mom. She and Lande just got married in Norway last year and shortly they found themselves expecting a baby but the news about the operation threatened the couple's bliss. Her baby was still in the second trimester and she decided to postpone the operation until after she gives birth to her first child.

"She was also terrified that she would miss the baby and something would happen to her and she wouldn't get to meet our son and she wouldn't get to participate in being a mother," Lande told KFSN-TV.

However, one morning at the hospital, Carleton, who was suffering from severe headaches for weeks, had a seizure which left doctors no other choice but to proceed with the operation. Since then she had not woken up.

Carleton's father John Farrell is still hopeful Carleton would finally be able to cuddle her child, a blessing that she has long awaited but was not given the chance to relish the moment. In the recent weeks, Carleton's family have observed signs the new mother is awake. She would open her eyes and squeeze her family member's hands but Farrell said she became "most awake" on the morning before she gave birth.

"In a few tender moments, she reached out to Brian, took his cheek, pulled his cheek down to her face and held it there," Farrell said in an interview with ABC News. "It was the first time she had hugged Brian since this trauma happened."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.