Jupiter's trademark Great Red Spot -- a swirling anti-cyclonic storm larger than Earth -- has shrunk to its smallest size ever measured.

According to Amy Simon of NASA's Goddard Space Flight Center in Greenbelt, Maryland, recent NASA Hubble Space Telescope observations confirm that the Great Red Spot now is approximately 10,250 miles across. Astronomers have followed this downsizing since the 1930s.

Historic observations as far back as the late 1800s gauged the storm to be as large as 25,500 miles on its long axis. NASA Voyager 1 and Voyager 2 flybys of Jupiter in 1979 measured it to be 14,500 miles across. In 1995, a Hubble photo showed the long axis of the spot at an estimated 13,020 miles across. And in a 2009 photo, it was measured at 11,130 miles across.

Beginning in 2012, amateur observations revealed a noticeable increase in the rate at which the spot is shrinking -- by 580 miles per year -- changing its shape from an oval to a circle.

Simon's team plans to study the motions of the small eddies and the internal dynamics of the storm to determine whether these eddies can feed or sap momentum entering the upwelling vortex, resulting in this yet unexplained shrinkage.

For comments and feedback contact: editorial@rttnews.com

Political News