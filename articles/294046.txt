General Motors says it needs to change or replace the keys for about 3.4 million cars because they could cause the ignition switch to move out of position if they're carrying too much weight.

GM said in a statement Monday that the switches can rotate out of "run" if the key has excess weight and the car "experiences some jarring event," such as hitting a pothole or crossing a railroad track.

That can shut off the engines and disable power steering, causing drivers to lose control. Also, the air bags won't work. The recall affects seven cars with model years ranging from 2000 to 2014.

GM is already recalling 2.6 million older small cars, mostly in the U.S., for a similar problem where the ignition switch slips out of "run" and causes an engine stall. In that case, the problem is with the mechanics of the switch. In this latest recall, GM says the problem is with the design of the key.

GM began reviewing ignition switches across its line-up after initiating the earlier recall. GM links that switch problem to 13 deaths. GM says it knows of eight crashes and six injuries tied to the latest ignition switch recall.

The company also raised its expected second-quarter charge for recall expenses to $700 million.

The latest recall covers the 2005-2009 Buick LaCrosse, 2006-2014 Chevrolet Impala, 2000 to 2005 Cadillac Deville, 2004-2011 Cadillac DTS, the 2006-2011 Buick Lucerne, the 2004 and 2005 Buick Regal LS and GS, and the Chevy Monte Carlo from the 2006 through 2008 model years.

GM says dealers will add an insert to the car keys to change the hole from a slot to a circle. The company says that until the repairs are made, owners should remove everything from their keychains and drive with only the key in the ignition.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

GM also is recalling 166,000 other cars for a series of other problems.

The recalls announced Monday bring to 44 the total number of GM recalls this year, covering 17.73 million vehicles in the U.S. and more than 20 million worldwide. The company has surpassed its old U.S. full-year recall record of 10.75 million vehicles set in 2004.