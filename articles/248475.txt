All in all, many people suggested, it was better to have searched for meteors early Saturday morning and not seen them than never to have looked at all.

Published reports had indicated that a cascade of shooting stars might streak through the morning skies as a shower of comet dust plunged through Earth’s atmosphere and burned in incandescent bursts.

That was not the way it turned out: not in Washington nor in many other places. This was not a complete surprise: Astronomers had cautioned that the brilliant shower, however much anticipated, might not show.

Despite the disappointment, many people reacted much like Suzanne Haversack of Laurel, Md. She “did not see any meteors,” but she liked what she did see. She said she “thought the sky was gorgeous,” and the stars, fixed in their heavenly places, “were so beautiful.”

We “could have had a spectacular meteor shower,” said University of Arizona astronomer Carl W. Hergenrother, who is also secretary of the American Meteor Society, but “we didn’t.”

Rather than scores of meteors per hour, observers saw perhaps one an hour, he said. But this is not a comet with which science has had a great deal of experience, and “we went in a little blind,” he said.

Twitter messages posted Saturday indicated that many people were taking the experience in stride, like Haversack.

The night was pleasant enough, lacking in the chill conditions that might discourage amateur sky-watching. Few if any reports of eyestrain or stiff necks could be found. And the weekend timing relieved any fears of showing up bleary-eyed at work.

In a Twitter message sent to the Washington Post’s Capital Weather Gang site, one would-be witness to a shooting-star shower said, “I did not see any meteors, but I did see the Milky Way!”

Also, the Universe Today Web site said that the shower “did produce some unusually slow meteors” and one spectacular fireball.

Additionally, the Space.com Web site said that although the display did not reach meteor-storm levels, there were enough “celestial fireworks” to amaze many stargazers.