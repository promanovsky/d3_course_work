Popular stand-up comedian John Pinette, best known for his role on 'Seinfeld', was found dead at a Pittsburgh hotel room. He was 50.

The actor, who was found dead at the hotel on April 5, was determined to have died of natural causes, according to the spokesperson for the Medical Examiner's office in Pennsylvania, said the Hollywood Reporter.

The comedian, born in Boston in 1964, had a regular touring presence.

Pinette's film credits include roles in 'Junior' (1994), 'The Punisher' (2004) and 'The Last Godfather' (2010).

He starred in several specials, including 'Still Hungry' (2011) and 'I'm Starvin' (2006).

The comedian also played Edna Turnblad during a national touring production of 'Hairspray' in 2004.

On television, he appeared in the series' Vinnie & Bobby', Parker Lewis 'Can't Lose' and 'High Tide'.

His role in the last 'Seinfeld' episode in 1998 saw the comic as a man who was robbed and had his car stolen.