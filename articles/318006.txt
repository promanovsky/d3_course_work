By Sneha Shankar - Swedish furniture-maker Ikea is set to increase the minimum wage, by the highest percentage in the last 10 years, for its employees in the U.S.

The official announcement, which is to be made Thursday, will declare a hike in the minimum wage for nearly half of its 11,000 hourly workers by an average of 17 percent to $10.76 an hour from the current average pay of $9.17. The increase is reportedly to be implemented from Jan. 1, 2015, and the actual dollar value of the new minimum wage will vary based on the cost of living in the 22 states where Ikea operates stores across the U.S., Associated Press reported.

“It's all centered around the Ikea vision, which is to create a better everyday life for the many people,” Rob Olson, Ikea’s acting president for the U.S. and its chief financial officer, said, according to The Huffington Post, adding: “The many people is, of course, our customers and consumers, but it's also our co-workers.”

The world's largest furniture maker reportedly said that despite differences in absolute minimum wages across the country, the number would remain above $9 across its 38 stores, and added that it does not plan to increase prices to accommodate the wage increase. Olson reportedly added that the company has also launched a separate retirement account for its workers last year.

The company calculated the percentage increase in wages based on the MIT Living Wage calculator, which takes into account housing, food, medical, transportation costs and annual taxes, reports said.

“What we decided to do was stop looking at the competition and focus solely on the co-worker," Olson said, according to The Huffington Post, adding: "A co-worker in one market isn't concerned with what it costs to live in another market. That's the way we've looked at it."

Ikea's move follows U.S. President Barack Obama's endorsement of a bill to raise the federal minimum wage to $10.10 an hour by 2016 from the current rate of $7.25 despite arguments against the hike, which critics say will further strain the federal budget.

The company “will revisit wages at least once annually and will monitor new developments” in the country’s laws, a spokesperson told The Washington Post.