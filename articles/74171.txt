Thirteen years passed between when a General Motors engineer signed off on an ignition switch suspected of being faulty and this month, when the company apologized for the use of the switches installed in 1.4 million vehicles and linked to at least 13 deaths.

This week, members of the US House and Senate will bring top GM and National Highway Traffic Safety Administration (NHTSA) officials to Capitol Hill to ask: Why did it take so long to get things right?

The situation is a public relations nightmare for the automaker, which has successfully rebooted its brand since its 2009 federal bailout, emphasizing fuel efficiency and improved design. In January, the company won more positive press by making Mary Barra the first woman CEO of a major automaker.

Ms. Barra has publicly apologized and a massive recall is underway, involving six models manufactured between 2003 and 2007. The company has not yet admitted liability. On Monday, GM announced another recall related to the sudden loss of power steering. Some 1.3 million Chevrolet, Saturn, and Pontiac cars are affected.

The troubles will force GM to answer questions about processes and management structures that no longer exist, as well as officials who are no longer with the company.

Barra will be “quick to point out how things are different now and how the company has changed,” says Jessica Caldwell, a senior automotive analyst with Edmunds.com. “That will be the key point for GM. It’s going to help them that the mistakes happened under a different regime.”

In remarks released late Monday, and which are expected to be given to House members Tuesday, Barra said: “I cannot tell you why it took years for a safety defect to be announced in that program, but I can tell you that we will find out. When we have answers, we will be fully transparent with you, with our regulators, and with our customers.”

Problems with the ignition switch, which reportedly caused some car engines to stall or shut down, were first reported as early as 2001. The switch was flagged three times during internal review, but none of the cases were classified as priorities, according to Automotive News. Internal company communications show engineers did not emphasize the potential problems of the switches to regulators and inquiries led nowhere.

When asked in a June 2013 deposition whether the company had “made a business decision not to fix the problem,” Gary Altman, a program engineering manager for GM, answered “that’s what happened, yes.”

Delphi, the supplier that provided the switch, told investigators that GM approved the faulty switch in February 2002 even though GM engineers had already said it did not match specifications for the vehicles.

Then, Delphi says GM ordered a redesign of the part in 2006 but did not address previous model-year vehicles or the original switches sold as a part in automotive stores, according to documents collected by the NHTSA. Delphi also told investigators the improved switch still failed to live up to original specifications for the 2007 Cobalt.

GM has laid blame on the engineer who approved the change, which it says was proposed by Delphi. The engineer's identity had been unknown until Monday, when it was revealed by House Democrats, according to Automotive News.

The hearings will likely focus on the individuals involved in that interchange and the reasons why the change did not go far enough – including why the change did not trigger a recall for the earlier defective part.

“It’s going to come down to what exactly happened and who knew what,” Ms. Caldwell says.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

The NHTSA will also be under the spotlight, with Acting Administrator David Friedman testifying Tuesday. The agency will be asked why it did not intervene earlier and whether its methods of investigation caused regulators to miss the ignition problem.

After Tuesday’s House hearing, there is a Senate subcommittee hearing on the recall Wednesday. On Thursday, the NHTSA is expected to receive answers to 107 questions it asked GM about the recall.