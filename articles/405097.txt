It has been recently revealed that resistance to the main drug, artemisinin, used to treat Plasmodium falciparum malaria has been spreading at an alarming stage in Southeast Asia.

It has likely been caused by a genetic mutation in the parasites, Plasmodium falciparum (P. falciparum). However, a six-day course of artemisinin-based combination therapy, as opposed to a standard three-day course, has proved highly effective in treating the drug-resistant malaria cases.

The research conducted by an international team of scientists including those from the National Institute of Allergy and Infectious Diseases (NIAID), part of the National Institutes of suggested that P. falciparum parasites with a mutant version of a gene called K13-propeller were resistant to artemisinin.

Researchers found that the geographic distribution of these mutant parasites in Western Cambodia corresponded with the recent spread of drug resistance among malaria patients in that region. Although artemisinin continued to effectively clear malaria infections among patients in this region, the parasites with the genetic mutation were eliminated more slowly.

Slow-clearing infections strongly associated with this genetic mutation were found in additional areas, validating this marker of resistance outside of Cambodia. Artemisinin resistance has been now firmly established in areas of Cambodia, Myanmar, Thailand and Vietnam, according to the authors.

To contain the further spread of artemisinin resistance, continued geographical monitoring would be needed as well as a re-examination of standard malaria treatment regimens and the development of new therapy options would be significant.

The study is published in the New England Journal of Medicine.