Repairs to overhead power lines in the Channel Tunnel, where a fault caused Eurostar cancellations on Monday, are almost complete Eurotunnel has said.

Engineers running checks on newly installed wires said the tunnel was expected to be open in both directions from 10:00 BST.

Four Eurostar trains have been cancelled, but other services are expected to run normally.

Ben Ando reports.