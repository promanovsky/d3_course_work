It’s been a while since we’ve seen any crazy antics from Charlie Sheen, but that changed when a video recently surfaced of the actor wandering around a Taco Bell drive-thru late at night.

An admittedly “hammered’’ Sheen was captured on camera greeting some fans outside of the fast food restaurant.

In the clip, he can be seen shaking hands and showing off his odd collection of tattoos.

You can check out the bizarre video embedded below.

[Warning: video contains explicit language.]

Just keep on #winning Charlie Sheen.