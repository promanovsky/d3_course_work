Microsoft has bundled "Titanfall" with the Xbox One at retail, essentially offering a free copy to people who buy the console. Analysts say that likely subsidizes the cost itself. The potential short term losses that come with that bundle will be regained over time as the Xbox One matures and owners begin to purchase other games and buy subscriptions to Xbox Live and other services.

"Titanfall" is perhaps even more important to publisher Electronic Arts , which bet big on the developers when they left Activision in a 2010 dispute. EA shares have been rising in anticipation of the game's release, climbing 29 percent this year. Eric Handler of MKM Partners expects the game to sell 3 million copies before the end of the month.

In many ways, "Titanfall" is as important to the Xbox One as "Halo" was to the original gaming console. It's hoped to be a system seller—a new franchise that becomes a tentpole for players to rally around—and one that keeps Microsoft at the top of the console hill.

"Titanfall," a new game from the creators of the "Call of Duty" juggernaut, hits shelves on Tuesday. It's expected to be one of the biggest games of 2014. And it's exclusive to Microsoft systems.

Sony may have gained an early lead in the battle of the next generation consoles, but Microsoft is launching its counteroffensive—and it's a big one.

The bundle could also help close the sales gap between the Xbox One and the PlayStation 4, which with a retail price $100 lower than Microsoft's console has so far proven to be the leader in this console generation.

Still, sales of the Xbox One have hardly been disappointing. Microsoft pointed out last month that the system has sold 2.29 times faster than the Xbox 360 did in the U.S. in its first three months on the market.

But Sony announced last week that life-to-date sales of the PS4 had topped 6 million units worldwide. The last hard sales number update Microsoft gave for the Xbox One came in early January, when it announced sales had topped 3 million in 2013.

(Read more: Why the PlayStation 4 is outselling the Xbox One)

And in January, PS4 sales were nearly double the Xbox One, according to BMO Capital Markets analyst Edward Williams. That came despite Sony's system being supply constrained as it prepared to launch in Japan.

That information, when viewed with the decision to bundle "Titanfall" with the Xbox One, could lead some investors to jump to the conclusion that Microsoft is worried about its position in the market. Analysts say, however, that these are the very early days of the current generation of consoles, making it highly unlikely Microsoft is worried about losing any significant ground.

"I don't think Microsoft is losing any sleep five months after launch," said P.J. McNealy, of Digital World Research. "This is a multiyear battle, not just between now and Easter. ... I think the 'Titanfall' bundle was planned regardless of whether Sony was at 3, 6 or 10 million [PS4 units sold]."

(Read more: Watching other people play video games…that's esports)

Beyond the impact for Microsoft and EA, "Titanfall" could have a positive ripple effect throughout the industry. Over 2 million players took part in the game's public beta test, which helped spread excitement about the game. As many of those gamers purchase the final retail edition, they might buy an additional game. And that could help the video game industry see year-over-year sales gains for the first time in several years by the end of 2014.

"We expect the release of EA's 'Titanfall' in March to help revive sales for both current and next generation software, and think that a string of easy comparisons over the next six months will drive software sales into positive territory," said Michael Pachter of Wedbush Securities.

—By Chris Morris, special to CNBC.com.