Liberia’s Ebola crisis ‘will get worse’

Share this article: Share Tweet Share Share Share Email Share

Monrovia - The Ebola epidemic in Liberia is set to get worse and many cases of the deadly disease are not included in the official tally, the head of the United States’ top public health body said on Wednesday. “The cases are increasing. I wish I did not have to say this, but it is going to get worse before it gets better,” Tom Frieden, the director of the Centres for Disease Control and Prevention, told a news conference in Monrovia. “The world has never seen an outbreak of Ebola like this. Consequently, not only are the numbers large, but we know there are many more cases than has been diagnosed and reported,” he said. Liberia has been hardest-hit by the epidemic now raging through West Africa, with 624 deaths and 1 082 cases since the start of the year. The World Health Organisation said the “unprecedented” outbreak has killed 1 427 people this year, while 2 615 people have been infected with the disease.

But the WHO also believes its count is likely far too low, due in part to community resistance to outside medical staff and a lack of access to infected areas.

With Liberia struggling to contain the spread of the virus, Frieden met the country's leader, President Ellen Johnson Sirleaf, on Monday, to discuss ways to combat the epidemic.

He said there was a need for “urgent action” as he called on Liberians “to come together” to fight the epidemic, which has brought the country to its knees.

“It is not going to be easy... (but) one good thing about Ebola is that we know how to stop it. There are misconceptions but we know how it spreads,” he said.

He asked people not to touch sick relatives and to make sure they stayed out of contact with bodily fluids - such as sweat and blood - which can transmit the virus.

Ebola, a deadly form of haemorrhagic fever, causes fever, vomiting, diarrhoea and internal bleeding. The virus has been fatal in just over half of cases in the current outbreak, the largest on record. - Sapa-AFP