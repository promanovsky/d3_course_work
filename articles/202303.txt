If you are looking for something to do this weekend, go see The Amazing Spider-Man 2! This action packed film is fun for the entire family. At 142 minutes, your mind will be blown away by Spider-Man 2‘s visual effects, the outrageous villains, the humor, the romance. There is so much so soak in.

The chemistry between Spider-Man (Andrew Garfield) and his love interest, Gwen (Emma Stone) is undeniable. Their scenes together sizzle with romance and just the right amount of giddiness. Although they are both good actors, it is hard to hide the real thing, as Garfield and Stone are a couple in reality, as per Huffington Post. (This makes Spider-Man very happy.)

The Amazing Spider-Man 2 has been a monster hit at the box office, raking in over $100 million domestically in its first week, as reported by Deadline. That is amazing in and of itself. But Andrew Garfield, who plays the role of Spider-Man (aka Peter Parker), sheds a new light on The Amazing Spider-Man 2.

While most moviegoers will enjoy The Amazing Spider-Man 2 for all the obvious reasons, Garfield wants kids to take home something a little deeper than what is on the surface, as reported by The Business Insider. Garfield has a heart for kids having worked closely with the Worldwide Orphans since 2011 by improving the lives of orphaned children. (Sounds just like something Spider-Man would do.)

Garfield gave a heartwarming sentiment to The Business Insider stating, “The way I feel about Spider-Man and Peter Parker is he is a metaphor for all of our lives in the sense that we are all Peter Parker,” Garfield told us. “We are all ordinary, we all [have] the same imperfections and struggles. We all have flaws and we all are fallible. And we are all Spider-Man in a sense that we have something extraordinary to give. We have some superpower whether that be for heroism, whether that be for art, whether that be for creativity, whether that be for science, mathematics, athleticism.”

The Amazing Spider-Man 2 lets the moviegoer see the vulnerable side of Peter Parker. We learn more about his parents, his childhood, his relationship with his Aunt May (Sally Field), and his love as a young man for Gwen. Spider-Man comes full circle.

“The beautiful thing about Peter and Spider-Man is it makes everyone in the audience go, ‘I can be extraordinary, too. In fact, I just am extraordinary, I just have to identify what my personal, individual extraordinariness is,'” Garfield told The Business Insider. “So if they can get that from this movie, even on a subconscious level, I think that would be wonderful.”

Andrew, that is exactly what we get from The Amazing Spider-Man 2! Well done, Andrew, well done.

Photo Credits: Associated Press and Electroshadow.com