During its Fiscal 2014 Year-End Financial Results, Barnes & Noble has announced that it will separate the companies retail and NOOK Media businesses into two separate public companies.

The decision by Barnes and Noble has been taken to help the main company move on without the NOOK Media side which is dragging down the main business.

The NOOK Media business saw revenues decrease 22.3% for the quarter and 35.2% for the year. Michael P. Huseby, Chief Executive Officer of Barnes & Noble explained in a statement today :

“In fiscal 2014 we have taken certain actions to strengthen the Company, including the ongoing rationalization of the NOOK business, growing the College business through new contract acquisitions and increased offerings to students and faculty, and initiatives to improve Retail’s sales trends,”

We have determined that these businesses will have the best chance of optimizing shareholder value if they are capitalized and operated separately,”-“We fully expect that our Retail and NOOK Media businesses will continue to have long-term, successful business relationships with each other after separation.”

Although how long the NOOK business will survive once away from the main company is another matter. The separation of the companies is expected to be completed by the first quarter of the next calendar year, says Barnes & Noble. For more information on the new NOOK Media seperation by B&N jump over to the statement issued today on the official website.

Source: Barnes & Noble : Tech Crunch

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more