The Hubble Space Telescope is celebrating its 24th year in space. To celebrate, NASA released a series of new images of a stellar nursery, captured by the space observatory.

These magnificent new photographs were taken of infant stars in the Monkey Head Nebula. This cloud of gas and dust lies 6,400 light years from our home world. As new stars first ignite their nuclear fuel, they radiate ultraviolet light, which pushes at the gas and dust around the infant stellar body. This light can ionize the gas in nebula, providing the atoms with an electric charge.

Hubble recorded these images in February 2014, viewing the object in infrared light. Although invisible to human eyes, these frequencies of light are used to keep food hot in restaurants. When UV light from young stars heats the charged gas (mostly hydrogen), it begins to warm, radiating infrared light.

Hubble has delighted people worldwide with a wide variety of observations but that mission got off to a rough start. Astronomers, both professional and amateur, were delighted when the space observatory successfully launched into space on 24 April, 1990. That optimism was short-lived, as operators soon discovered the main mirror was ground incorrectly, and only produced blurry images. The spacecraft became the object of ridicule and jokes on television and in movies. In The Naked Gun 2 ½, a model of the Hubble was displayed alongside the Edsel, Titanic, and the Hindenburg.

For three years, NASA officials used the telescope to do some valuable science, although the orbiting observatory was not operating nearly according to design. Luckily for all of us, the Hubble Space Telescope was designed to be serviced by humans in space. On 2 December 1993, the space shuttle Endeavor lifted off from Cape Canaveral, on a mission to save the telescope. This mission successfully installed a corrective optics package on the telescope, bringing the Universe into proper focus.

Since that time, Hubble has shown the world thousands of images, from our local neighborhood of planets to the depths of the Cosmos. Moreover, Hubble has narrowed the possible age of the Universe to between 13 and 14 billion years. The mission also led to the discovery of dark energy, which causes the expansion of the Universe to accelerate over time.

NASA officials are saying these new images of the Monkey Head Nebula are a small taste of the photographs that will be possible with the new James Webb Telescope, scheduled for launch in 2018.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.