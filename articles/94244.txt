Amazon is extending its Fire brand to the living room with Fire TV - a $99 streaming set-top box that claims to do a little bit of everything.

Like the Apple TV and Roku, Amazon's home entertainment console is capable of streaming movies and TV shows via Netflix, Hulu, Flickster and a host of other applications.

Advertisement

With established competitors like Roku and Apple already dominating the home entertainment space, Amazon hopes to stand out in a few ways.First, the online retail giant claims it has revolutionized the way you search for content on your TV. Rather than having to meticulously type each letter to find a movie or television show, the Fire TV's remote control comes with an embedded microphone for voice searches.

Second, Amazon is attempting to win over video game lovers with its gaming platform available on the Fire TV. It's the first set-top streaming box that's trying to take gaming seriously. In addition to releasing its own titles through its own development studio, Fire TV can run popular Android mobile games such as "The Walking Dead," "Dead Trigger" and "Despicable Me: Minion Rush."

Advertisement

While gaming is one of the key aspects that makes the Fire TV different from its rivals, Amazon is also taking a risk by betting that consumers will actually want to play Android games on their television. These types of products haven't amassed much interest in the past, as gadgets like the Ouya Android gaming console and Nvidia's Shield handheld Android gaming system have shown.

There's certainly potential for the Fire TV to establish a presence in the set-top box space, but the current experience doesn't make it seem too much more compelling that Roku.

Disclosure: Jeff Bezos is an investor in Business Insider through his personal investment company Bezos Expeditions.