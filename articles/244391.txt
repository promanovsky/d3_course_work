Two U.S. food companies this week issued voluntary recalls of walnuts and hummus dips sold at major retailers after listeria was detected in a sampling of the products, the Food and Drug Administration said on Thursday. The recalls come weeks after WalMart Stores settled lawsuits with the families of 23 people who died from a 2011 listeria outbreak linked to cantaloupe grown at a Colorado farm and sold by the retailer.

Walnuts by Sherman Produce recalled due to possible health risk. Source: FDA

St. Louis-based Sherman Produce said it would begin recalling 241 cases of bulk walnuts, after a recent routine sampling of the product purchased by stores in Missouri and Illinois revealed traces of listeria, the FDA said in a statement.

Massachusetts prepared foods manufacturer Lansal, commonly known as Hot Mama's Foods, said it would voluntarily pull hummus and dip products sold at Target, Trader Joe's and other retailers, the administration said.

Lansal launched the recall of about 14,860 pounds of hummus after a single 10-ounce container of Target Archer Farms Traditional Hummus surveyed by the Texas Department of Health tested positive for listeria, the administration said.

Listeria monocytogenes can lead to serious, even fatal, infections especially in young children, the elderly and those with weakened immune systems. Infections can have particularly harmful effects for pregnant women, including miscarriage and stillbirths.

