Music powerhouse couple Beyoncé and Jay Z had a special surprise for fans on the opening night of their On The Run Tour.

The “Crazy in Love” couple showed previously unseen footage of their private 2008 wedding.

And if that wasn’t enough to drive the audience wild, the pair went on to play an intimate film depicting the birth of their now two-year-old daughter Blue Ivy.

Beyoncé, 32, and Jay Z, 44, took to the stage at the Sun Life Stadium in Miami, Florida, on Wednesday night.

The couple are well-known for remaining tight-lipped on their personal life and, although the footage projected onto a large screen behind the stage lasted only a few seconds, it was a departure from their usual conduct.

During a rendition of "Young Forever", the pair were seen saying their vows and exchanging rings, before making their way down the aisle.

And in other scenes shown during "Halo", the proud parents were shown cradling Blue Ivy, who was also seen crawling and taking her first steps.

Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The trio were believed to have been attending an afterparty for the 2014 Met Gala Ball.

Following the leak, they broke their silence only to say that they were "united". In a statement to the Associated Press they said "families have problems and we're no different".