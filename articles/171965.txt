Documentary filmmakers digging in a New Mexico landfill Saturday unearthed hundreds of "E.T. the Extra-Terrestrial" cartridges, considered by some the worst video game ever made and blamed for contributing to the downfall of the video game industry in the 1980s.

Some gamers speculate that thousands or even millions of the unwanted cartridges made by Atari were buried in a landfill in Alamogordo, about 200 miles southeast of Albuquerque.

Who dumped the videos, how many they buried and why they did it inspired the dig and a documentary of the event by Microsoft's Xbox Entertainment Studios.

The first batch of "E.T." games was discovered under layers of trash after about three hours of digging, a Microsoft spokeswoman said, putting to rest questions about whether the cartridges would be found at all.

She could not immediately provide an exact count of how many cartridges were uncovered.

The game was a design and marketing failure after it was rushed out to coincide with the release of Steven Spielberg's 1982 hit movie "E.T. the Extra-Terrestrial," and it contributed to a collapse of the video game industry in its early years.

Atari is believed to have been saddled with most of the 5 million "E.T." game cartridges produced. According to New York Times reports at the time, the game manufacturer buried the games in the New Mexico desert in the middle of the night.

A game enthusiast later tracked down the suspected burial site and spread the word about the location, said Sam Claiborn, an editor at video game news site IGN.

The approximate size of the dig site at an old Alamogordo landfill measures 150 feet by 150 feet off the city's main commercial street.

"For a lot of people, it's something that they've wondered about and it's been rumored and talked about for 30 years, and they just want an answer," said Zak Penn, the film's director.

When the game was first released in 1982 it retailed for around $29.99, but now often sells on eBay for less than $5.

"I don't know how much people would pay for a broken ET game, but as a piece of history, it has a much different value," Penn said.

The Los Angeles Times' Hero Complex blog noted "E.T." sold 1.5 million copies. Still, it was a disappointment.

As Hero Complex wrote: "Despite its place on the list of Atari’s all-time top-selling titles, this release is one of the most notorious games in 2600 history. Rushed into production after Steven Spielberg’s movie became a box office sensation, the game was expected to sell many millions of copies, but sold just a fraction of that. The unsold cartridges were famously buried in a New Mexico landfill; the game’s failure was the beginning of a long decline in Atari’s fortunes."

Reuters and L.A. Times contributed.