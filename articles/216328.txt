Obese women are likely to lose the battle against breast cancer unless they reduce weight, a recent study bared.

Though previous researches have confirmed that the risk of getting cancer in obese people is greater, the study to be presented at the American Society of Clinical Oncology's (ASCO) 50th annual meeting in Chicago presented a new finding: that obese women with breast cancer have roughly 34 percent chance of succumbing to the treacherous disease.

Obesity is found to be lethal to women with cancer that responds to high levels of estrogen. Past studies have shown that sex hormones are the ones responsible for fat distribution throughout the body. Consequently, fat cells produce estrogen as well. Hence, too much estrogen caused by obesity can promote estrogen-receptor positive breast cancer in women.

The same goes for those who are younger and have not reached their menopausal phase yet, since they produce the highest amount of estrogen during this particular stage in their lives.

Meanwhile, obesity is not an issue for women who reached menopausal stage and those who had breast cancer that show negative response to estrogen.

The study from the University of Oxford in Britain led by professor Hongchao Pan gathered data from 80,000 women with breast cancer and conducted 70 trials. In the sample, 20,000 pre-menopausal women have estrogen-receptor positive cancer.

"Despite everyone knowing the truth of this, the levels of overweight and obesity in the U.S. continue to climb," ASCO president Dr. Clifford Hudis told NBC News. "Knowing that it is a negative health factor in so many domains is not yet an effective way of changing behavior."

A person is considered obese if their body mass index or BMI goes beyond 30 kg/m2. In the United States, data from Gallup showed that obesity in adults rose to 27.2 percent in 2013 from 26.2 percent in 2012, the largest annual increase since 2009.

In a recent survey, ASCO said that roughly one in ten U.S. citizens is unaware and does not mind much about the effects of obesity to cancer.

To date, breast cancer is the second leading cancer among American women, following lung cancer.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.