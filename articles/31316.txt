NEW YORK (TheStreet) -- Sprint (S) - Get Report was gaining 3.1% to $9.38 Friday after the carrier announced an April 11 release date for the Samsung Galaxy S5 and Gear Fit smartwatch Friday, with pre-orders starting today.

Like AT&T (T) - Get Report, Sprint will charge $199.99 for the Galaxy S5 on a two-year contract. The carrier will also offer the phone through its Sprint Easy pay plan which lets customers get the phone with no down payment, paying the full cost of the phone through 24 monthly payments of $27.09.

The carrier will offer Samsung's new flagship phone in Charcoal Black and Shimmery White.

Pre-orders for the phone begin today in stores and online, the phone will be available on April 11.

Sprint will also offer the Samsung Gear Fit smartwatch for $199.99, the Gear 2 Neo for $199.9, and the Gear 2 for $299.99. All three smartwatches will be available on April 11.

Must read:Warren Buffett's 10 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

S data by YCharts

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.