The rock 'n' roll star Courtney Love has posted a picture which she claims to be debris and an oil slick from the missing Malaysian Airlines flight MH370 on her facebook page.

Although the singer admits she is "no expert" on the subject of missing aeroplanes, she has provided an annotated screen grab, with red arrows pointing to the areas of ocean showing "oil" and "plane".

She wrote: "I'm no expert but up close this does look like a plane and an oil slick. Prayers go out to the families #MH370 and it's like a mile away Pulau Perak, where they "last" tracked it 5°39'08.5"N 98°50'38.0"E but what do I know?"

What indeed. The attention-seeking rock chick is no stranger to controversy.

After husband Kurt Cobain's death, Love dated comedian Steve Coogan. After allegedly becoming pregnant with Coogan's child and then aborted it, she was quoted as saying "What does it make me look like that I have slept with Alan Partridge? Given the A-grade stars I've dated it's embarrassing. I mean... Alan Partridge!"

She also famously grabbed Quentin Tarantino's Oscar statue to hit Vanity Fair journalist Lynn Hirschberg. In another attention grabbing incident she attempted to sabotage a Madonna interview on MTV's VMA show.

Rolling Stone officially tagged the 49-year-old as "the most controversial woman in the history of rock". Here are a few of Love's previous outbursts:

"No, I was never an alcoholic; I was a big drug addict. It's really a huge secret, don't tell anyone coz it will ruin my image." - during the Alan Carr chat show.

"For years, despite having impeccable taste, I didn't understand how to convey that I had impeccable taste." – on her reputation to New York Times.

"I like to behave in an extremely normal, wholesome manner for the most part in my daily life. Even if mentally I'm consumed with sick visions of violence, terror, sex and death."

"I don't mean to be a diva, but some days you wake up and you're Barbara Streisand."

"I don't want to have a penis. I want to be a girl and I want to wear dresses and have nice perfume and do things that girls do. So I'm not interested in looking like a boy or playing like a boy. That sounds like a really obvious, blatant thing to say, and I shouldn't have to say that to anybody."

"I like there to be some testosterone in rock, and it's like I'm the one in the dress who has to provide it."