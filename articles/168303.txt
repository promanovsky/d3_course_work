We’ve seen it onstage and we’ve seen it on the big screen. Get ready for the live version of Grease to head to your living rooms in 2015.

Photo credit: WENN

Everyone is getting in on the live musical act for television. It’s the Fox network’s turn now that they have announced a live version of the Broadway hit, Grease.

NBC had a monster hit over the holiday season with The Sound of Music Live!, starring Carrie Underwood. Fox is hoping to capitalize on that same audience with a property that has been successful onstage and on film.

On Monday, Fox president, Kevin Reilly, revealed the special would air in 2015. They are expected to hire a young ensemble cast, according to E! News.

In a statement, Fox’s senior vice president of event series, Shana C. Waterman, shared, “Its iconic characters and addictive songs make it the perfect fit for Fox, and we’re going to give it the kind of star power and production quality to make every Sandy, Danny, Rizzo and Kenickie out there want to get up and sing along.”

Fans can expect to hear favorite songs like “Grease Lightnin’,” “Summer Nights” and “You’re the One That I Want.” The show will be a blend of the 1971 stage show and the 1978 film that starred John Travolta and Olivia Newton-John.

Paramount TV president, Amy Powell, said, “It’s incredibly exciting to have one of our first major network productions be based on this universally celebrated Paramount title… Fox’s passion for engaging audiences with bold storytelling and live musical formats make it a perfect home for this special broadcast.”

Grease is definitely the word.