How slouching when sitting and driving for long periods without breaks is causing EIGHT in ten Britons to get severe lower back pain



Lower back pain causes more disability than other conditions, claims study



It is often triggered by simple activities such as standing for a long period

The number of people affected is set to rise with world's booming population

Lower back pain is the most common type of back pain in the country with around eight in ten people affected by it at some point in their life (file picture)

Lower back pain causes more disability than any other condition with around eight in ten people in Britain affected by it, a new study has revealed.



It is the most common type of back pain in the country and the number of people affected is set to rise as the world's booming population age.



It is often triggered by simple everyday activities such as bending awkwardly, lifting incorrectly, standing for long periods of time, slouching when sitting and driving for long periods without taking breaks.

Symptoms range from tension and stiffness to pain and soreness and is caused by structures in the back as opposed to rare conditions such as cancer or a fracture.



It affects the area of the body between the bottom of the ribcage and the top of the legs and occurs in a delicate area of muscles, nerves, bones and joints which continuously works hard to support the weight of the upper body.



The findings are based on data for the Global Burden of Disease 2010 study, which assesses ill health and disability arising from all conditions in 187 countries split into 21 regions for 1990, 2005, and 2010.



Researchers looked at the prevalence, incidence, remission, duration, and risk of death associated with low back pain in 117 published studies covering 47 countries and 16 of the 21 Global Disease world regions.



They also reviewed surveys in five countries about the impact of acute and severe chronic low back pain with and without leg pain and data from national health surveys.



They then assessed the toll taken by low back pain in terms of disability adjusted life years (DALYs).

These are worked out, by combining the number of years of life lost as a result of early death, and the number of years lived with disability.



Out of all 291 conditions studied in the 2010 study, low back pain came top of the league table in terms of years lost to disability and sixth in terms of DALYs.



It was ranked as the greatest contributor to disability in 12 of the 21 world regions, and the greatest contributor to overall burden in Western Europe and Australasia.



Lower back pain is often triggered by everyday activities such as bending awkwardly, lifting incorrectly, standing for long periods of time, slouching when sitting and driving for long periods without taking breaks (file picture)

Almost one in 10 people (9.4 per cent) had low back pain, with the number of DALYs rising from 58.2million in 1990 to 83million in 2010.



The prevalence of low back pain was highest in Western Europe, followed by North Africa and the Middle East, and lowest in the Caribbean and Latin America.



The authors writing in the Annals of the Rheumatic Diseases said: ‘With ageing populations throughout the world, but especially in low and middle income countries, the number of people living with low back pain will increase substantially over coming decades.



‘Governments, health service and research providers and donors need to pay far greater attention to the burden that low back pain causes than what they had done previously.’





