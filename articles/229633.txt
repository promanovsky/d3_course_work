Sharon Doyle puts sunscreen on the face of 9-year-old Savannah Stidham as they visit the beach. (credit: Joe Raedle/Getty Images)

NEW YORK(CBSNewYork) — The unofficial start of summer serves as an annual reminder to use sunscreen whenever you head outdoors.

As CBS 2’s Dr. Max Gomez reported, there may be no consumer product that is as misunderstood as sunscreen. Doctors say that it should be used year round, but beach and tanning season are prime time for sun protection.

There are a number of common myths associated with sunscreen, first is the belief that the FDA tests all products before they are sold.

“The FDA doesn’t really test sunscreen at all. It depends on what the manufacturers claim and that’s basically it,” Dr. Bruce Katz, Juva Skin and Laser Center, explained.

A high SPF number may sound comforting, but as Dr. Katz explained it’s the number that 30 is essential.

“As long as it’s an SPF of 30 you’re fine. You’re protected very well. Anything higher than that is purely marketing,” he said.

You could go all the way up to SPF 100 and only get another percentage point or two in protection, according to experts.

When it comes to so-called natural sunscreens it is important to remember that the term ‘natural’ is not regulated by the FDA, so it’s basically meaningless on a sunscreen label.

Dr. Katz also had some important tips for those who prefer to use spray-on sunscreens.

“Spray sunscreens are fine as long as you don’t inhale them and also after you apply them, you spread them around so they’re evenly distributed,” he said

However, the biggest myth to surface recently has been that ‘drinkable’ sunscreens can make water molecules in the skin vibrate to block UV rays.

“Drinkable sunscreens are really nonsense. There are all kinds of marketing schemes to sell sunscreen. This is by far the wildest,” Dr. Katz said.

The most important thing to remember about sunscreen is to use it early, often, and liberally. About a shot glass full should be applied to the entire body about 20 minutes before sun exposure. It should also be re-applied every couple of hours, or sooner if you happen to be sweating or swimming.

Kids do not need a special formula, but should not use sunscreen until they are at least 6-months-old.

You May Also Be Interested In These Stories

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]