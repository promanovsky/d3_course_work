Miley Cyrus’ friendship with Flaming Lips’ frontman Wayne Coyne might seem puzzling at first, but only on the surface. After all, the Flaming Lips are said to be one of Cyrus’ favorite groups and have collaborated on stage and in the studio with the 21-year-old superstar.

Their most recent antics, though, have made for a baffling chapter in their storied friendship. While together at a Los Angeles house party this weekend, Coyne and Cyrus apparently celebrated their unusual union by getting matching chest tattoos of Cyrus’ deceased dog.

Coyne posted multiple photos of the tattoos on his Instagram account, including a shot of Cyrus in the middle of getting inked. The tattoo features the dog howling the name of the Flaming Lips’ upcoming album, “With a Little Help From My Fwends.”

The album will cover the Beatles’ treasured 1967 record, “Sgt. Pepper’s Lonely Hearts Club Band.” Due Oct. 28, the record will feature Cyrus on a version of “Lucy in the Sky With Diamonds.”

Advertisement

As evidenced by the Flaming Lips’ newly released video, Coyne and Cyrus are continuing their efforts to surprise fans. Coyne’s latest creation, “Blonde SuperFreak Steals the Magic Brain,” is a bizarre, five-minute short detailing a complicated race for President John F. Kennedy’s brain. Coyne told Rolling Stone that, in psychedelic fashion, the brain “contains the original formula for the drug LSD.”

Moby also appears in the video and serves as a freakish cult leader in pursuit of the brain. A bedridden Cyrus is infuriated after the brain is stolen and, as Coyne describes, “enlists a burned-faced Santa and a lesbian Bigfoot (that are hovering in a nearby spaceship) to hunt down the blond superfreak that stole her brain.”

The Flaming Lips appear, costumed as flowers, mushrooms and rainbows.

According to Coyne, Cyrus was not felling well during the making of the video. “And even though she was still quite ill, she was full of laughs and great absurd suggestions,” he said.

Follow me on Twitter @jimeasterhouse

