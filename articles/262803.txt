First lady Michelle Obama on Tuesday made a rare foray into a Capitol Hill dispute, using a White House event to rally support for school meal nutrition rules that Republicans are pushing to loosen.

The first lady successfully lobbied for those standards in 2010, and has since become the public face of this and other initiatives aimed at improving the dietary habits of children.

But some schools have complained the rules are too restrictive and costly -- and House Republicans have a new bill aimed at addressing those concerns. An agriculture spending bill approved by a House subcommittee last week would allow schools to waive the standards if they have a net loss on school food programs for a six-month period.

Seeing her nutritional standards under threat, the first lady hosted a discussion with school leaders on Tuesday afternoon at the White House where she ripped efforts to roll back those guidelines.

"This is unacceptable," Obama said.

The first lady said families realize the country is facing a "health crisis," and the "last thing we can afford to do right now is play politics with kids' health."

Sam Kass, director of Mrs. Obama's Let's Move initiative to combat childhood obesity, called the GOP bill "a real assault" on administration efforts to make foods healthier for kids.

The first lady rarely enters congressional policy debates, despite being a powerful advocate for the president on the campaign trail during the past two presidential elections. But childhood nutrition is her signature issue, and the House bill would aim to undermine the standards she lobbied for.

Rep. Robert Aderholt, R-Ala., who wrote the bill, said he was responding to requests from school food directors.

The standards championed by the first lady have been phased in over the past two school years, with more changes coming in 2014. The rules set fat, calorie, sugar and sodium limits on foods in the lunch line and beyond.

While many schools have had success putting the rules in place, others have said they are too restrictive and costly. Schools pushing for changes say limits on sodium and requirements for more whole grains are particularly challenging, while some school officials say kids are throwing away fruits and vegetables that are required.

The Agriculture Department, which administers the rules, has tweaked them along the way to try to help schools that have concerns. The department scrapped limits on the amount of proteins and grains that kids could eat after students complained they were hungry. Last week, USDA announced it would allow some schools to delay serving whole grain pastas just hours after the House subcommittee approved the opt-out language.

School groups have been split. The School Nutrition Association, which represents school nutrition directors and companies that sell food to schools, has lobbied for changes to the standards, saying some are too restrictive.

The national PTA is pushing lawmakers to keep the standards intact.

"At a time when families are working hard to live healthy lives, school meals should be supporting families' efforts, not working against them," PTA President Otha Thornton wrote to members of Congress.

The Associated Press contributed to this report.