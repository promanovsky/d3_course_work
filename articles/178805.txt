Rocker Gregg Allman is suing to stop movie producers from reviving a film based on his life story after a camera assistant was killed by a freight train while working on the project in southeast Georgia.

A lawsuit filed by the Allman Brothers Band singer in Savannah, Ga., says Unclaimed Freight Productions lost its rights to make the film "Midnight Rider" when it failed to begin principal photography by Feb. 28. Production was suspended after a train plowed into director Randall Miller's crew on a trestle Feb. 20. Crew member Sarah Jones was killed, and six others were injured.

Filming of the movie, starring William Hurt and All-American Rejects frontman Tyson Ritter as older and younger versions of the Allman Brothers Band singer and founder, was just getting started when crew members were struck in February.



The lawsuit filed last week says producers have told Allman's representatives they intend to revive the project despite the singer's objections. A spokeswoman for the producers said Monday they had no comment.