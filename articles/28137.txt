Conspiracy theories surround us. Witness the reactions on the internet to the tragic and mysterious disappearance of Malaysia Airlines Flight 370. Perhaps North Korea hijacked the plane. Perhaps the Chinese are responsible. Maybe aliens did it. Or, as an influential legislator in Iran contended to the New York Times, perhaps the US “kidnapped” the lost plane in an effort to “sabotage the relationship between Iran and China and South East Asia”.

Pick your topic: Ukraine, the National Security Agency, assassinations of national leaders, recent economic crises, the authorship of William Shakespeare’s plays — it is child’s play to assemble a host of apparent clues, and to connect a bunch of dots, to support a relevant conspiracy theory. In recent years, for example, many Americans have become convinced that the US (or Israel) was responsible for the attacks of September 11, that the US government concocted HIV/Aids and that federal agencies have conspired to hide the association between vaccines and autism.

Why do people accept such theories?

The first explanation points to people’s predispositions. Some of us count as “conspiracists” in the sense that we have a strong inclination to accept such theories. Not surprisingly, conspiracists tend to have a sense of personal powerlessness: They are also more likely to conspire themselves. Here is an excellent predictor of whether people will accept a particular conspiracy theory: Do they accept other conspiracy theories? If you tend to think that the Apollo moon landings were faked, you are more likely to believe that the US was behind the 9/11 attacks. With a little introspection, many of us know, almost immediately, whether we are inclined to accept conspiracy theories.

Remarkably, people who accept one conspiracy theory tend to accept another conspiracy theory that is logically inconsistent with it. People who believe that Princess Diana faked her own death are more likely to think that she was murdered. People who believe that Osama Bin Laden was already dead when US forces invaded his compound are more likely to believe that he is still alive.

The second set of explanations points to the close relationship between conspiracy theories and social networks, especially close-knit or isolated ones. Few of us have personal or direct knowledge about the causes of some terrible event — a missing plane, a terrorist attack, an assassination, an outbreak of disease.

If one person within a network insists that a conspiracy was at work, others within that network may well believe it.

Once the belief begins to spread, a lot of people within the network may accept it as well, on the theory that a spreading belief cannot possibly be wrong. And once that happens, “confirmation bias” tends to kick in, so that people give special weight to information that supports their view. They also treat contradictory information as irrelevant or perhaps even as proof of conspiracy. (Why would people — “they” — deny it if it were not true?)

A third explanation emphasises how human beings are inclined to react to terrible events. Such events produce outrage, suspicion and fear. Sometimes the perpetrator is self-evident, as in the case of many terrorist attacks, but if there is no clear perpetrator — as with a missing plane, a child’s disability or the outbreak of a disease — people may go hunting for the malicious agent behind it all.

To be sure, some conspiracy theories turn out to be true. Republican officials, operating at the behest of the White House, did, in fact, bug the Democratic National Committee’s headquarters at the Watergate complex. In the 1950s, the Central Intelligence Agency did, in fact, administer LSD and related drugs to unknowing subjects in an effort to investigate the possibility of “mind control”. In 1947, space aliens did, in fact, land in Roswell, N.M., and the government covered it all up. Well, maybe not.

Even when false, most conspiracy theories are harmless. Consider the theory, popular among younger members of our society, that a secret group of elves, working in a remote location under the leadership of a mysterious “Santa Claus,” make and distribute presents on Christmas Eve. And in a free society, conspiracy theories must be allowed even if they are both false and harmful. But sometimes conspiracy theories create real dangers.

If people think that scientists have conspired to cover up the harms of vaccines, they will be less likely to vaccinate their children. That is a problem.

Unfortunately, beliefs in false conspiracy theories are also peculiarly resistant to correction. Recent research suggests that in the context of the alleged autism-vaccination link, current public health communications are unhelpful, even when they enlist facts to set the record straight.

Efforts to establish the truth may even be self-defeating, because they can increase suspicion and thus strengthen the very beliefs that they were meant to correct.

Such efforts are far more likely to succeed if they begin by affirming, rather than attacking, the basic values and commitments of those who are inclined to accept the theory.

Conspiracists like to say that the truth is out there. They are right. The challenge is to persuade them to find their way toward it.

— Washington Post

Cass R. Sunstein, the Robert Walmsley university professor at Harvard Law School, is a Bloomberg View columnist. This article is adapted from the opening chapter of his new book, Conspiracy Theories and Other Dangerous Ideas, published by Simon & Schuster. Sunstein is a former administrator of the White House Office of Information and Regulatory Affairs.