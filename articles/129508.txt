(NEW YORK-AFP) - Ford celebrated the 50th birthday of its beloved Mustang by displaying a new model of the convertible on top of the Empire State Building in New York on Wednesday.

The Triple Yellow 2015 Mustang was taken apart, whisked up by elevator to the observation deck and painstakingly reassembled overnight in freezing wind, rain and snow.

More than 1,000 feet above the crowded streets of Manhattan, the observation deck is too high to send the car up by crane and although open air, too narrow for a helicopter drop.

So a team of eight or nine spent more than five hours reassembling the car by hand overnight, battling strong wind on the 86th floor of New York's second tallest building.

"Zero visibility to start with," George Samulski, manager of Ford North America design fabrication, told AFP. "It got better as the night went on, but it was bad, it was cold, it was windy."

By sunrise, the convertible was glittering and the views stunning.

Visitors can admire the car until Thursday. After the deck closes to the public at 2 a.m. on Friday, the crew will dismantle the car all over again and remove it.

It is the second time a Ford Mustang has gone on display atop the Empire State Building. The first time was in 1965.

The stunt marks 50 years since US auto giant Ford put the first Mustang on sale during an expo in New York on April 17, 1964.

Ford received 22,000 orders on the first day and sold 418,000 in one year.

The 50th birthday will be celebrated in great pomp at the New York International Auto Show from April 18 to 27.