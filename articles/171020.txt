Daniel Terdiman/CNET





Twitter shares plummeted in after-hours trading as Wall Street reacted to news that the social network's user growth continued to slow during the first quarter.

Sales more than doubled to $250.5 million in the period ended March 31, from $114.3 million a year earlier. Profit, after excluding some costs, broke even on an adjusted per share basis. Wall Street expected the San Francisco-based company to lose 3 cents per share on $241.5 million in sales.

Though it beat analysts expectations, Twitter disappointed with its user growth. It had 255 million monthly active users, up just 5.8 percent from 241 million the quarter earlier. Wall Street had made it clear after the company -- which makes nearly all its sales from advertising -- posted its fourth quarter earnings that it needed to do something to bump up its user base. In February, CEO Dick Costolo admitted that the service's experience for new users was far less than ideal, and that Twitter planned major initiatives to make things easier for new users from the moment they join the social network. His plan doesn't seem to be working. The company's tepid user growth in the first quarter -- it added just 14 million users -- did nothing to allay fears that it's growth has stalled.

Twitter's shares fell more than 10 percent in late trading.

Investors were also put off by Twitter's 2014 sales forecast of $1.2 billion to $1.25 billion, which fell short of analyst expectations of $1.24 billion. Mostly, though, they were concerned that the company's user growth hasn't accelerated enough.

It "seems like the number of [monthly average users] were slightly below expectations, especially after so many big events in Q1," said CRT Capital Group media analyst Neil Doshi, like the "Olympics, Super Bowl, Oscars, Grammys, Ukraine, etc."

Twitter, of course, has not been blind to concerns about, or reasons for, that slow growth. In February, during its Q4 earnings conference call, the company was candid that it understood that the major reason it has not been able to match growth rates of competing social networks like Facebook, Instagram, and Snapchat, is that new users find Twitter difficult to use.

As a result, the company said then that it expected to undergo a series of modest changes to the Twitter service intended to make it easier for new users to figure out what they're doing. Costolo was clear that it's vital to the service's stickiness that someone new understand it from day one.

To date, though, the biggest change has been a roll-out of new Twitter profiles reminiscent of Facebook's. The service has also boosted the presence of photos and videos in tweets, and has been reported to be tinkering with minor semantic changes like renaming retweets, though such modifications appear to have been experiments. There have also been rumors that the company is considering doing away with fundamental Twitter elements such as @ symbols and hashtags. The thinking is that, although core users love those tools, they are mysterious to newcomers unfamiliar with such conventions.

To be sure, no one could have realistically expected Twitter to dramatically reverse its slowing growth in just one quarter. That's likely to be a longer-term effort. But Wall Street no doubt was looking for some evidence the company could arrest the trend, and perhaps wanted to see Twitter roll out a stronger set of new features intended to make the service easier to use.

Twitter, though, felt that the quarter was a success given that it doubled revenue year over year, and showed some user growth. We had a great first quarter," CEO Dick Costolo said during today's earnings conference call. "Revenue growth was up 119 percent fueled by two things: increased engagement and user growth. Last quarter I spoke about a number of initiatives to drive user growth, and I like the progress we've made there."

But investors were not optimistic going into today's earnings report that Twitter was going to announce the progress they wanted. A consensus of analysts had predicted the company would report a loss of 3 cents a share. Analysts had also predicted Twitter would report revenue of $241.5 million for the quarter. Those estimates proved to be off base, as the company posted sales of $250.5 million.

Even so, Wall Street seems to have hoped for more. I have a sense that investors expected Twitter to have much better growth than they posted," said Gartner analyst Brian Blau. "Based on what Costolo said at the previous quarterly earnings call I believe it will take them the better part of 2014 to fully deploy site and app changes that would bring better better growth, so the modest growth this quarter means they are moving in the right direction. But maybe not as fast as investors had wanted."

More highlights from Twitter's first quarter: