tech2 News Staff

Apple has already breached one major market with its CarPlay offering, and it looks like the next big play will be for home automation.

According to an FT report, Apple is preparing to launch a software platform geared towards home automation, with iPhones and iPads being used to control your lights, security systems, cooling and heating, and other appliances. Apple’s move comes on the back of Google’s foray into the automated home space with the purchase of smart thermostat maker Nest Labs. Apple’s efforts are described as being part of the company’s plans for the “internet of things” sphere, which ties into the huge iPhone accessory market.

Apple will reveal the plans at WWDC on June 2, which will include touchless controls, based on geo-fencing. Lights might come on when you enter the home and your phone notes your location, for example. FT’s report goes on to say that the new Apple TV box, expected to launch later this year will be used as a hub to control other devices.

“Apple has been talking to a select group of other device makers whose smart home products will be certified to work with its forthcoming new system and then be sold in its retail stores,” the report says. Apple has been said to be working on a retail revamp as well, as indicated by its hiring of fashion industry veteran Angela Ahrendts as retail chief.

The report is vague on the obvious privacy questions raised by such a system, but says Apple is going to emphasise on the security and privacy aspects at the announcement.