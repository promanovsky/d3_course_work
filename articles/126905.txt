There's big money in becoming a reality TV trainwreck these days, so Tori Spelling is promoting her upcoming Lifetime series True Tori by revealing every ugly detail about her marriage to Dean McDermott.

The actress recently told Us Weekly that she'll "never trust" McDermott again.

Rumors that McDermott cheated on Spelling have been confirmed, so Tori certainly has plenty of reasons to doubt her husband of eight years, but we've gotta ask: why stay married to someone you know you'll never trust?

Oh, right - there's a reality show to be made!

Tori seems to at least partially blame herself for Dean's infidelities, as she bawled that she can "never give enough sex" in a preview clip of True Tori.

Sounds like Tori and Dean's awful marriage might make for some decent entertainment!

The show doesn't even premiere until April 22, but Tori and Dean have been airing their dirty laundry for our amusement for years.

First we learned that McDermott banged fellow Canadian Emily Goodhand behind Tori's back, then he entered rehab for some reason, then Dean ordered Tori to go to rehab... the list goes on and all this drama took place before the couple even started filming their show.

Hopefully they saved something for the cameras. We also hope Tori and Dean somehow hide all this insanity from the four children they raise together.