Last year major fears emerged that many developing economies would face a severe cash crunch, which in turn would retrain local economies and contribute to instability. Now, many policy makers, analysts, and investors are breathing a sigh of relief. Investors are once again taking their funds to emerging markets, supplying much needed cash and easing fears of liquidity crunches.

Last June, investors pulled over $30 billion dollars from bonds and other markets in emerging countries. This worried many policy makers, especially across Asia and other hot markets. Over the last 11 months, however, investors have injected more than $220 billion dollars into emerging markets. May was an especially high performing month, with $45 billion dollars having been pumped into markets.

Livermore Strategic Opportunities February 2021 Update Livermore Strategic Opportunities, LP performance update for the month of February 2021. Q4 2020 hedge fund letters, conferences and more Many of you are witnessing first hand that our country, economy, (and now stock market) are all very fractured and becoming extremely challenged. Therefore, our hedge fund's theme remains focused on specific sectors and companies. Read More

Fears of global turbulence subsiding

Last year, when the United States moved to curtail its quantitative easing program, which has created huge levels of liquidity and made dollars cheap, many emerging markets, and especially Indonesia, Turkey, Brazil and other hot markets, felt a sudden cash crunch. This generated large levels of turbulence in these emerging markets.

The reason was simple. Under the qualitative easing program, the dollar’s value was artificially restrained. With all the cheap dollars floating around many investors decided to park their money in hot markets. With that program being curtailed, however, and the dollar set to rise, the United States suddenly began to look more attractive.

Now, fears of that impending collapse are subsiding as markets have normalized and cash is now flowing back into emerging markets. Investors are hunting for profits and many emerging markets are once again looking like attractive places to park money and invest for the future.

Western markets less attractive?

When it comes to attracting investment dollars, the limited amount of global capital means that it’s a zero-sum game, at least in the short run. The more money that flows to emerging markets, the less money that’s available to invest in Western markets. Of course, if investments result in strong economic growth, there’s a good chance that all boats will be lifted at once.

So the question then comes down to why investors find emerging markets so attractive, while Western markets are looking increasingly stagnant. Despite stability in Europe and good jobs reports in the Unite States, most Western markets are still suffering from sluggish economic growth. Of course, this could just be markets re-stabilizing themselves after the turbulence suffered by emerging markets last year.