The stock market in extended gains driven by momentum from small companies ahead of a key meeting of the Federal Reserve Open Market Committee (FOMC) tomorrow.

Yesterday, U.S. equities rallied over reports regarding corporate acquisition transactions and the expansion of industrial production output in the country, which overshadowed fears of ongoing sectarian insurgency in Iraq.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

The benchmark index climbed 5.1% this year on better-than-expected economic data, and monthly assets purchases by the Federal Reserve. The S&P 500 is trading at a multiple of 16x the projected earnings of companies. The index was trading at a multiple of 15.5x earlier this year.

John Canally, an economic strategis at LPL Financial Corp told Bloomberg, ““Overall, you’re still in a market environment where the path of least resistance is up. Another bump tomorrow could be the FOMC, although the outcome is largely already priced in.”

Data today revealed that the cost of living in the United States went up more than expected, an indication that inflation will move closer to the target rate of the Federal Reserve. The consumer price index rose 0.4%, the highest increase since February last year.

Mark Luschini, chief investment strategist ast Janney Montgomery Scott LLC commented, “Probably the most troubling number for investors is the CPI number. Both numbers put those inflation readings around the Fed’s target policy of 2%.” He added that policy makers could push forward a rate increase.

U.S. Markets

Dow Jones Industrial Average (DJIA)- 16,808.49 (+0.16%)

S&P 500- 1,941.99 (+0.22 %)

NASDAQ- 4,337.23 (+0.37%)

Russell 2000- 1,175.50 (+0.74%)

European Markets

EURO STOXX 50 Price EUR- 3,275.33 (+0.43%)

FTSE 100 Index- 6,766.77 (+0.18%)

Deutsche Borse AG German Stock Index DAX- 9,920.32.98 (+0.37%)

Asia-Pacific Markets

Nikkei 225- 14,975.97 (+0.29%)

Hong Kong Hang Seng Index- 23,203.59 (-0.42%)

Shanghai Shenzhen CSI 300 Index- 2,169.67 (-1.01%)

Stocks in Focus

The stock price of Medtronic, Inc (NYSE:MDT) gained 2.58% to $61.58 per share after analysts at Morgan Stanley upgraded their rating for the stock to Overweight from Equal Weight on potential returns/synergies from its agreement to acquire Covidien plc (NYSE:COV) for $42.9 billion.

Shares of Edwards Lifesciences Corp (NYSE:EW) climbed 4.61% to $82.06 per share after its SAPIEN XT transcatheter aortic heart valve received approval from the United States Food and Drug Administration (FDA). The company said its aortic heart valve along with NovaFlex+transfemoral delivery system will be immediately available to patients at leading cardiovascular centers across the United States.

Netflix, Inc. (NYSE:NFLX) jumped 3.11% to $443.31 as Morgan Stanley analysts resumed their coverage on the stock with an Overweight rating and a $500 price target. Shares of the online video streaming company have gained 20% this year. The company recorded a 298% gain in last year.