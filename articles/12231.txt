A Long Island hospital is sending out over 4,000 letters to patients recommending that they be tested for hepatitis and HIV because of risk due to an insulin pen.

Newsday reports that in a letter dated Feb. 22, South Nassau Communities Hospital said patients may have received insulin from an insulin pen reservoir that could have been used more than once.

Hospital spokesman Damian Becker said no one was observed reusing the insulin pen reservoir, but a nurse was heard saying it was all right to do so.

He says the hospital was recommending testing out of an "abundance of caution."

The hospital says the risk of infection is "extremely low." It has set up a toll-free hotline for patients to schedule a blood test. The number is 516-208-0029.

Click for more from My Fox New York.