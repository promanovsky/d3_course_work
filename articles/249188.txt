Quentin Tarantino Celebrates 'Pulp Fiction' at Cannes: 'Now Rip Out a Joint and Light It Up'

UPDATED: Tarantino, Uma Thurman and John Travolta show up on the beach to host a packed public screening of the cult classic two decades after it won the Cannes Film Festival's top prize.

CANNES -- Cannes Film Festival darling Quentin Tarantino stole the show Friday night as he celebrated the 20th anniversary of winning the Palme d'Or for Pulp Fiction.

The maverick filmmaker, along with stars Uma Thurman and John Travolta, treated the masses to a public screening of the cult classic on the beach at Cannes. Standing on a makeshift stage in front of a giant screen, Tarantino welcomed his two actors, who, one by one, walked down a sandy aisle before flanking their Pulp Fiction director.

CANNES STORY: Todd McCarthy on 'Pulp Fiction' at 20, Secret Screening and Drinks With Tarantino

"I've seen Pulp Fiction under every circumstance a person can see it except this one," proclaimed Tarantino to the roaring crowd. "Now rip out a joint and light it up. Are we ready to get started?"

Harvey Weinstein, who released Pulp Fiction, participated in the festivities and received a special shout-out from Tarantino, who asked the audience to applaud the man who made the movie "a smashing success all over planet Earth." Tarantino also introduced Pulp Fiction producer Lawrence Bender.

Borrowing a page from this year's Academy Awards ceremony, organizers handed out pizza before the movie got underway.

Prior to the screening, the Pulp Fiction posse walked a more official red carpet at the Palais and then attended a party hosted by Miramax. (Weinstein no longer owns the company.) Tarantino, Travolta and actress Kelly Preston, Travolta's wife, were even spotted dancing spontaneously before sitting down to dinner.

Hours earlier, Cannes Film Festival director Fremaux arranged a press conference hosted by Tarantino, where the director revealed he'd like to take unused footage from Django Unchained and make a four-year television miniseries.

Thurman flew in for the event, while Tarantino was already scheduled to be at the festival for a 50th anniversary showing of Sergio Leone's A Fistful of Dollars. Likewise, Travolta was in Cannes for AmfAR.