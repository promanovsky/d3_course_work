Keith Richards is to publish a book for children which is due to be released this autumn. The Rolling Stones guitarist has teamed up with his daughter, Theodora Richards, to produce an autobiographical book aimed at children. Gus & Me: The Story of My Granddad and My First Guitar will detail Richards' relationship with his grandfather Theodore Augustus Dupree.



Keith Richards Is Publishing A Children's Book.

Known as Gus, Richards' grandfather was in a jazz big band and introduced Keith to music at a young age. Richards' artist daughter was named after Gus and will illustrate the upcoming book. "To be able to explore my father's relationship with his grandfather was a gift in itself," she said in a statement, via the LA Times. "The things that I've learnt during this whole process have just been life affirming."



Richards Has Penned The Book With His Daughter, Theodora.

"I have just become a grandfather for the fifth time, so I know what I'm talking about," said 70 year-old Richards, who has five children. "The bond, the special bond, between kids and grandparents is unique and should be treasured. This is a story of one of those magical moments. May I be as great a grandfather as Gus was to me."



The Pair Have Teamed Up To Write & Illustrate A Story Based On The Rocker's Childhood.

Richards' new book was written in conjunction with Barnaby Harris and Bill Shapiro and will be published simultaneously in hardcover and e-book editions. The hardcover will be packaged with a CD that includes bonus content.

Gus & Me... will follow on from the rocker's 2010 memoir Life and is due to be released on the 9th September. We're guessing Gus didn't teach little Keith anything about snorting ancestor's ashes...