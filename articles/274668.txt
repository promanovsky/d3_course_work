Jonah Hill Vows to Never Again Use Gay Slur (Video)

Hill's best supporting actor nod for The Wolf of Wall Street marks his second Oscar nomination. He also was nominated for 2011's Moneyball.

The "22 Jump Street" star continued to address the comments he made over the weekend, revealing what he's learned from the incident and how he's taking action.

22 Jump Street star Jonah Hill continued his promotion of the sequel on what has turned into somewhat of an apology tour in the wake of video surfacing of the Oscar nominee making a gay slur to a photographer.

After apologizing for calling the photographer a "f----t" on Tuesday's Howard Stern Show and The Tonight Show, Hill addressed the incident again on Wednesday's Good Morning America.

PHOTOS: 'Duck Dynasty's' Phil Robertson and Other Stars Who've Made Anti-Gay Remarks

But this time he went beyond merely apologizing to talking about the effects of such language, and he vowed to never use the slur again.

"Words can be really destructive. This kind of defamatory language can have a profound effect on someone who's questioning their sexuality. And it's never OK to use that kind of language," Hill said. "And I personally vow to never, ever use that word ever again and I urge everyone to do the same."

In fact, it's possible Hill got tips on dealing with the incident from his 22 Jump Street co-star Channing Tatum, whom Hill said he turns to for life advice.

"If I ever need life advice, he's the guy that I would ask, for sure," he said, advising GMA's Amy Robach that she should feel free to consult Tatum about the same when he stops by on Thursday. "So if you have any problems or anything you can ask him for advice. "

VIDEOS: The Most Anticipated Summer Blockbusters of 2014

Hill added that Tatum is great at everything, including giving advice and doing his own stunts, which Tatum does most of, while Hill, as he put it, does "pretty much little to none of all stunts."

He explained that he is hanging off a helicopter in one of the movie's big scenes, but not the truck his character's shown dangling from. His stunt double did the latter.

Hill revealed one other crucial thing that the 30-year-old learned while filming at a college campus.

Watch the video below to find out what that was and to see Hill's full GMA interview.



ABC US News | ABC International News