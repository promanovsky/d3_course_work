The Supreme Court has declined to hear Google's appeal of a ruling that it pried into people's online lives through their Wi-Fi systems as part of its drive to collect information for its Street View mapping project.

The justices did not comment Monday in leaving in place a ruling that Google employees violated the federal wiretap law when they rolled through residential streets with car cameras to shoot photos for Street View.

The federal appeals court in San Francisco said the information picked up from unencrypted Wi-Fi signals included emails, usernames, passwords, images and documents.

Google had argued that it did not run afoul of the wiretap law because data transmitted over a Wi-Fi network is a radio communication that is readily accessible to the public.