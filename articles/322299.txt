US consumer spending rose less than expected in May, prompting economists to downgrade estimates for second-quarter growth.



There is, however, little doubt the economy is expanding. Another report on Thursday showed the number of Americans seeking unemployment benefits fell again last week.



Even as they lowered growth forecasts for this quarter, economists noted a jump in spending on durable goods and said they were uncertain how much spending was really buckling given problems calculating outlays for healthcare.



Healthcare spending has been volatile with the implementation of President Barack Obama's signature law early this year, but the swings should subside as the year progresses, economists said.



"We have evidence of consumer spending continuing at a very good pace in June. That limits my concerns," said Anthony Karydakis, chief economic strategist at Miller Tabak in New York.



Consumer spending increased 0.2 per cent in May after being flat in April, and was down for a second straight month when adjusted for inflation, the Commerce Department said.



That suggests consumer spending, which accounts for more than two-thirds of US economic activity, could struggle to regain momentum this quarter after growing at its slowest pace in nearly five years in the first three months of the year.



Spending in May was probably constrained by healthcare, as inflation-adjusted outlays on services fell for a second month. Spending on automobiles, however, surged, accounting for more than half of the 1 per cent rise in durable goods.



At the same time, income increased for a fifth successive month, with savings hitting an eight-month high.



"This should give some ammunition for consumers going forward," said Eugenio Aleman, a senior economist at Wells Fargo Securities in Charlotte, North Carolina, who nevertheless cautioned that rising gasoline prices presented a risk.



Healthcare was behind a sharp downward revision to 1st quarter gross domestic product data, released on Wednesday. The government slashed its growth estimate to show the economy contracting at a 2.9 per cent annual rate, the worst performance in five years, instead of only a 1 per cent pace.



In the wake of the spending data, second-quarter growth estimates which had ranged as high as a 4.0 per cent rate were cut to as low as a 2.2 per cent pace.



Inflation trending higher



US stocks were trading lower, pressured by comments from a top Federal Reserve official who said the central bank might need to move interest rates up more quickly than markets expect. US Treasury debt prices rose, while the dollar was little changed against a basket of currencies.





In a separate report, the Labor Department said new applications for state unemployment benefits slipped 2,000 to a seasonally adjusted 312,000 for the week ended June 21.The declining claims suggest a recent streak of monthly payroll job gains above 200,000 is likely to be sustained, lending the economy enough momentum for inflation to start perking up.A price index for consumer spending increased 0.2 per cent in May, rising by the same margin for a third consecutive month.In the 12 months through May, the personal consumption expenditures (PCE) price index was up 1.8 per cent, the largest gain since October 2012. It had advanced 1.6 per cent in April.Excluding food and energy, prices also posted a 0.2 per cent gain. This so-called core index increased 1.5 per cent from a year ago, the biggest gain since February last year.Both gauges, however, remain below the US central bank's 2 per cent goal."These are not scary inflation numbers by any means but we are getting close to the Fed's target," said John Ryding, chief economist at RDQ Economics in New York."The inflation rate is another signal that suggests the Fed should begin renormalizing monetary policy sooner than the market expects."St. Louis Federal Reserve Bank President James Bullard told Fox Business News that his forecasts suggest inflation would reach the central bank's target this year with the jobless rate falling below 6 per cent. It stood at 6.3 per cent in May."You are basically going to be near normal on both dimensions basically later this year," he said. "That's shocking, and I don't think markets, and I'm not sure policymakers, have really digested that that's where we are."

The Fed, which is scaling back the amount of money it is pumping into the economy through monthly bond purchases, has kept its benchmark lending rate near zero since December 2008.Copyright: Thomson Reuters 2014