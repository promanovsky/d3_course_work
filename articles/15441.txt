PITTSBURGH (KDKA) – A recall has been announced on the popular Fitbit wireless wristband activity tracker after thousands of complaints of itchy rashes, blisters, and burns.

The Consumer Product Safety Commission announced the recall of one-million units of the Fitbit Force because of the rashes.

The Fitbit Force is a pedometer, sleep tracker and watch that retails for about $130.

The CPSC says they have received about 10,000 complaints that the wristband is causing skin irritations, and about 250 reports of blistering.

One Fitbit Force user Cara Fish, reported that her skin felt like leather, and she’d never experienced anything that itched or hurt like it.

The CPSC recall notice attributed the reaction to “the stainless steel casing, materials used in the strap, or adhesives used to assemble the product, resulting in redness, rashes or blistering where the skin has been in contact with the tracker.”

Fitbit issued the following statement about the recall:

“We are looking into reports from a very limited number of Fitbit Force users who have been experiencing skin irritation, possibly as a result of an allergy to nickel, an element of surgical-grade stainless steel used in the device.

We suggest that consumers experiencing any irritation discontinue using the product and contact Fitbit at force@fitbit.com if they have additional questions. Customers may also contact Fitbit for an immediate refund or replacement with a different Fitbit product.

We are sorry that even a few consumers have experienced these problems and assure you that we are looking at ways to modify the product so that anyone can wear the Fitbit Force comfortably. We will continue to update our customers with the latest information.”

Fitbit Force users should call 1-888-656-6381 or click here for information on how to receive a refund for their recalled device.