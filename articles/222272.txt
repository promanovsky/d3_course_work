Cannes: 'Maps to the Stars' Director David Cronenberg Defends Satire

From left: John Cusack, Julianne Moore, director David Cronenberg, Mia Wasikowska and Rob Pattinson at the photocall for Maps to the Stars.

"To see it only as an attack on Hollywood shortchanges the movie," the filmmaker declares, noting that the film's themes are universal.

David Cronenberg protectively reached out and put his big arms around the star-studded cast of his Maps to the Stars at the movie's press conference Monday as the assembled media demanded to know how accurately the film reflects life in?Hollywood.

Maps, which unspooled in competition, takes a caustic and eviscerating look at the moviemaking community, portraying actors as narcissistic, vacuous and in constant search of validation. It was written by Bruce Wagner, whose books long have detailed the darker side of Hollywood.

PHOTOS: The Complete Cannes 2014 Official Selection

"I can answer for all of them," Cronenberg volunteered. "There is no real person like them at all."

"To see it only as an attack on Hollywood is, I think, shortchanging the movie," the director continued. "You could set this in Silicon Valley or on Wall Street -- anyplace people are desperate and fearful. You could set it anywhere and have the same ring of?truth."

Julianne Moore, who plays an aging actress desperately hoping to make a comeback, said, "I love the movie business; I'm not here to disparage it. It is all about the desire to be seen."

John Cusack, who plays a self-help guru, described the film as "a very lurid fever dream about Hollywood" but agreed it's not really about Hollywood at all, saying, "I just thought of it as a very familiar ecosystem, and it was a heightened myth of it."

Offered Wagner: "To amend Andy Warhol, who said, 'In the future everyone will be famous for 15 minutes,' I think in the future, which is now, everyone will be famous all the time. As long as there is the need to be seen, this will exist. And it's not unique to Hollywood or to our time. The Kardashians existed in the time of the pharaohs."