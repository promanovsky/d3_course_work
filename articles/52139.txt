Samsung has a very high profile launch coming up with the Galaxy S5, but it's now being reported the company will launch new Galaxy Mega Phablets in the near future.

After the success Samsung experienced with the release of its larger Galaxy Note phablets, the company decided to try out even larger displays on a new series it names Galaxy Mega. The Galaxy Mega was announced last year and consisted of two models; a 5.8-inch device and a 6.3-inch smartphone. Sprint and AT&T both carried the Galaxy Mega 6.3 and a new report claims the carriers will also offer the second-generation Galaxy Mega smartphones.

SamMobile reports that Samsung's in-house Exynos processor will power the upcoming Galaxy Mega devices, and likely the same hexa-core model found in the company's recently introduced Galaxy Note 3 Neo. The processor would be a step up from the processor currently used in the Galaxy Mega 6.3. The site also believes the tablet-sized smartphone will run Android 4.4.2 and Samsung's new TouchWiz user interface.

The Galaxy Mega line has been seen as an inexpensive way for users to try out a Samsung smartphone with a display that falls into the company's more premium Galaxy Note phablet territory. It's unclear if Samsung will use the faux-leather rear case it currently uses on the Galaxy Note 3 and some of its tablets, or possibly the Galaxy S5's dimpled rear case.

The site doesn't list when we might possibly see the new Galaxy Mega launched, but if we use last year's introduction as a possible launch date, that would happen to fall on April 11, which is also the same day Samsung will launch the Galaxy S5 during a worldwide simultaneous launch. It's pretty safe to assume that the Galaxy Mega announcement won't be coming on April 11.

Whenever it comes to unannounced Samsung devices, it's especially always worth remembering to take these reports with a huge grain of salt. It certainly seems plausible that this is in Samsung's plan, as we're already seeing some of the same moves it made last year with devices and is reportedly going to introduce a Galaxy S5 Active and Galaxy S5 Zoom and presumable a Galaxy S5 Mini. We'll keep you updated.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.