Soon to be wed reality TV star Kim Kardashian is in the midst of planning a huge shindig, but the famous for being famous star took some time out to voice her thoughts on racism today — an issue she says now weighs heavily upon her as the mother to a bi-racial child.

Kim Kardashian has been involved with fiancé Kanye West for some time, but she said that her relationship didn’t really open her eyes to issues involving race. When daughter North West was born, however, Kim says that a lot more became clear “seeing through my daughter’s eyes the side of life that isn’t always so pretty.”

In an essay titled “On My Mind,” Kardashian walks us through her change of heart on racism and race in the months since North arrived.

Quite plainly, Kim admits that even despite Kanye’s concern over the issue, she was never really motivated to care much.

She writes:

“To be honest, before I had North, I never really gave racism or discrimination a lot of thought. It is obviously a topic that Kanye is passionate about, but I guess it was easier for me to believe that it was someone else’s battle.”

Now Kim says that she’s recently “read and personally experienced some incidents that have sickened [her] and made [her] take notice.”

Kardashian adds:

“I realize that racism and discrimination are still alive, and just as hateful and deadly as they ever have been… I feel a responsibility as a mother, a public figure, a human being, to do what I can to make sure that not only my child, but all children, don’t have to grow up in a world where they are judged by the color of their skin, or their gender, or their sexual orientation. I want my daughter growing up in a world where love for one another is the most important thing.”

Over on Twitter, the star posted her comment link:

On my mind… http://t.co/Tp5VScVaFD — Kim Kardashian (@KimKardashian) May 7, 2014

Fans replied:

@KimKardashian this is amazing Kim! Sadly people continue these type of behaviors and it shouldn’t be tolerated! — Maheen (@kardashiankiss1) May 7, 2014

@KimKardashian that takes a lot of guts to post that online, thank you. — Brianna (@MissBriannaJD) May 7, 2014

You can read Kim Kardashian’s complete essay on her change of heart regarding racism over on her Celebuzz blog.