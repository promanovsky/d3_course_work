The worst place to be a mother

Share this article: Share Tweet Share Share Share Email Share

London - Somalia is the worst country on Earth to be a mother, according to a report published by Save the Children which calls for more action to protect mothers and children in crisis-hit areas. The London-based charity estimates that 800 mothers and 18 000 young children are dying around the world every day from largely preventable causes. Finding ways to meet the health and nutritional needs of this vulnerable group is particularly vital in fragile states and humanitarian crises, it says in its annual “State of the World's Mothers” report. Almost a third of child deaths are found in West and Central Africa, while another third occur in South Asia, where high mortality rates are increasingly concentrated in socially-excluded communities. The charity compared 178 countries in terms of maternal health, child mortality, education and levels of women's income and political status.

Somalia came bottom of the global rankings, although only narrowly below the Democratic Republic of Congo - the lowest ranking country last year - followed by Niger, Mali and Guinea-Bissau.

“It's no surprise that the 10 toughest places to be a mother in this year's Mothers' Index all have a recent history of armed conflict and are considered to be fragile states,” said Save the Children International's chief executive Jasmine Whitbread.

“The poorest mothers have it the hardest: the report once again points out the disheartening disparity between mothers in rich and poor countries.”

Three years ago Afghanistan was the worst place to be a mother, but it is now ranked 146th due to progress in cutting child and maternal death.

By contrast Syria has slumped from 65th place in 2011 to 115th in 2014, after the conflict caused “the collapse of what had been a functioning health system, and threatens to set back progress by a generation”, the report says.

More than 60 million women and children needed humanitarian assistance this year, the report says.

While more than half of maternal and child deaths occur in crisis-affected places, the majority of these deaths were still preventable, it argues.

Top of the list of the charity's recommendations is improving access to high quality healthcare, but it also emphasises the benefits of investing in women's education and economic empowerment.

“Every country must be better prepared to assist mothers and children in emergencies,” the report said.

However, it conceded: “Ending preventable deaths of mothers and children will not be possible until fragile countries become more stable and health care more accessible.”

Finland is the best place to be a mother, followed by Norway, Sweden, Iceland and the Netherlands.

The highest non-European countries were Australia (9th), Singapore (15th) and New Zealand (17th), while the United States came in 31st place and China in 61st place.

The lowest non-African countries were Haiti (168th), Papua New Guinea (164th) and Yemen (162nd).

Top 10:

1 - Finland

2 - Norway

3 - Sweden

4 - Iceland

5 - Netherlands

6 - Denmark

7 - Spain

8 - Germany

9 - Australia

9 - Belgium

Bottom 10:

178 - Somalia

177 - Democratic Republic of Congo

175 - Mali

175 - Niger

173 - Central African Republic

172 - Sierra Leone

174 - Guinea-Bissau

171 - Nigeria

170 - Chad

169 - Ivory Coast. - Sapa-AFP