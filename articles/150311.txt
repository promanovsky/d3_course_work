What sort of Start menu are we in for?

Update: Windows 9 is now known as Windows 10. Want to know more about when you can get your hands on it? Check out our in-depth Windows 10 release date page

At Microsoft Build 2014 we found out what was in store for Windows 8.1 Update 1, but now attention is shifting to what lays ahead.

WZOR, the Russian pirate group, has released some new details on the next Microsoft OS update. Although the group seems certain it's coming this autumn, it's not sure if Microsoft will decided to call it Windows 8.1 Update 2 or shift to Windows 8.2.

Naming conventions aside, this update will purportedly bring the new Start menu we saw at Build. Similar to the Windows 7 Start menu, this throwback would allow users to look through a list of their programs while retaining some of the Metro aesthetic, complete with integrated Live Tiles.

These Live Tiles will not only launch programs but also update with new information on the weather and inbox counts.

On cloud nine

What's more, the WZOR group has stumbled upon more information for the next full-version update to Microsoft's OS, Windows 9.

One of the biggest changes with the next version of Windows is it could live entirely on the cloud, similar to Google's Chrome OS. WZOR claims there is a group working on a prototype operating system in which the client software is free to download and additional functionality requires a subscription.

The main core of the OS supposedly resides in the system's BIOS and operates similar to Microsoft's budget Windows Starter package. For any additional functionality users would need an internet connection to download and connect to apps.

With this in mind, WZOR believes the next full version of Windows could be free, though it's important to note the pirates couldn't confirm this.

It's a bit too early to start talking about Windows 9 but with Bing cards being dealt last week, Microsoft clearly isn't done doling out updates for its platforms any time soon.

Via Myce