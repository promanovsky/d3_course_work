Hometown dates turned into a hard-to-watch moment on ‘The Bachelorette’ on July 7 when Chris Harrison sat Andi Dorfman and her bachelors down to break the news that Eric Hill had died. I’m not sure there was a perfect way to address this tragedy, but I do think I found a way that would’ve honored Eric more without feeling borderline exploitative.

A weird thing happened on The Bachelorette last night, July 7 — they went to Chris Harrison‘s house. The host had Andi Dorfman and her bachelors over for an “impromptu” (I’m still on the fence about how on-the-spot this really was) meeting to tell them that former contestant Eric Hill had passed away in a tragic hang-gliding accident. The camera was then set down — but just perfectly enough so that the moment could still be captured — and the Bachelorette crew members broke the fourth wall to mourn alongside Chris, Andi and the bachelors. It was a sad, emotional moment. But it was also awkward and icky and felt distinctly made for television.

Andi Dorfman & Cast Mourn Eric Hill’s Death

While it did partly feel like the producers were truly saddened by Eric’s death, it was also hard to watch the moment without feeling like they were also rubbing their hands together off-camera and evilly whispering, “Yes! Yes! Drama! Emotion!” Because as much as they might not want to admit it, The Bachelorette benefitted from airing the moment Andi and her guys found out about Eric.

Chris Harrison defended the show’s decision, writing, “What happened was horribly sad and tragic, but to me acting like it just didn’t happen and going on like Eric never existed seemed horribly dishonest and disrespectful,” in a blog post for Entertainment Weekly.

The problem with that is that moment last night wasn’t for Eric. The way The Bachelorette handled it, it more or less became all about Andi, her bachelors, and even the crew and how they all reacted to the death. Andi seemed broken up because she had been mean to Eric in their last conversation before she sent him home, not because a young, kind soul was gone. It was all about her! Her feelings! Her anguish! Her difficult “journey” that she has to continue!

She’s the star of the show, so I get that — but you can’t claim to pay tribute to a dead man by putting your star further into the spotlight. It just comes off as exploitative, whether you’re trying to be or not.

How ‘The Bachelorette’ Should’ve Handled Eric Hill’s Death

So I have a retroactive solution, courtesy of an avid Bachelorette follower:

How about a nice message from Eric Hill’s family instead of a bunch of crying #TheBachelorette cast members who didn’t truly know the guy? — Bachelor Bros (@brobachelor) July 8, 2014

That sounds more like it.

If The Bachelorette truly wanted to step out of their pre-produced zone, they should have actually put down the camera for a second and let the people who really knew Eric pay tribute to him. This would have made the moment entirely about Eric and would have eliminated any lurking feeling that The Bachelorette was just showing something to make their narrative more emotional and dramatic.

In his blog, Chris also says that they were in constant contact with Eric’s family, so maybe there’s a chance The Bachelorette presented this idea and the family shot it down. In which case, the show should’ve just decided to scrap any further coverage of Eric’s death altogether. They canceled a rose ceremony to talk to Andi about it (Her feelings!), they addressed it before the season even started, and I’m sure they’ll have a candlelight vigil during “Bachelors Tell All” in a couple weeks — they’ve clearly already squeezed out plenty.

Eric’s gone. It’s tragic, I know. But more and more it’s feeling like The Bachelorette just wants to keep bringing it up because they’re hard-pressed for drama.

Do you agree? Let me know.

— Andrew Gruttadaro

Follow @AndrewGrutt

More ‘Bachelorette’ News: