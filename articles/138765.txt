The final trailer for Twenty-First Century Fox's (NASDAQ:FOXA) X-Men: Days of Future Past aired this week and already has nearly 8 million views. But is there enough enthusiasm for the movie to bring the studio a much-needed box office win?

Host Ellen Bowman puts this question to Fool analysts Nathan Alderman and Tim Beyers in this episode of 1-Up On Wall Street, The Motley Fool's web show in which we talk about the big-money names behind your favorite movies, toys, video games, comics, and more.

As fans, Nathan and Tim say the trailer combines epic action with the feel of a heist caper. Mix in mutants showing off their powers in ways we've never before seen on screen and you've got a formula that could pay off nicely with longtime fans of the X-Men comic books.

Unfortunately, the financial story isn't as simple. Multiple reports say X-Men: Days of Future Past could be the most expensive superhero epic ever made. If so, that would put it north of the $270 million spent to produce Superman Returns, which is also from director Bryan Singer.

Tim says that could be a problem when you consider history and the level of marketing support X-Men: Days of Future Past is enjoying right now. Unlike Walt Disney's Marvel Studios movies or Sony's Spider-Man epics, no X-film has ever cracked $500 million in worldwide box office grosses, according to data supplied by The-Numbers.com.

Nathan says that Fox's goal may not be to earn quick-hit profits from X-Men: Days of Future Past. Rather, the studio may be maneuvering to jump-start the franchise with an epic tale that begs for lower cost spin-off solo movies featuring popular characters such as Gambit, Mystique, or even Deadpool.

Now it's your turn to weigh in. What's your box office prediction for X-Men: Days of Future Past? Do you believe the movie will revitalize the X-Men franchise and lead to spin-offs? Click the video to watch as Ellen puts Nathan and Tim on the spot, and then be sure to follow us on Twitter for more segments and regular geek news updates!