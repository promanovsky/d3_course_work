Allergan, Inc. (NYSE:AGN) is reportedly seeking offers from Sanofi SA (ADR) (NYSE:SNY) and Johnson & Johnson (NYSE:JNJ), reports Bloomberg. The network cites unnamed sources familiar with the matter. Valeant Pharmaceuticals Intl Inc (NYSE:VRX) (LON:VRX) recently made an unsolicited bid of $45.7 billion for Allergan—a bid that activist investor Bill Ackman practically arranged and now is pushing Allergan to take.

Allergan in early discussions

According to Bloomberg’s sources, Allergan, Inc. (NYSE:AGN)’s discussions with both Sanofi SA (ADR) (NYSE:SNY) and Johnson & Johnson (NYSE:JNJ) are in the early stages. However, it is unclear whether either of them wants to acquire Allergan. Both of the two potential suitors are said to be weighing options on the matter.

Allergan, Inc. (NYSE:AGN) apparently doesn’t know what kind of response to give Valeant Pharmaceuticals Intl Inc (NYSE:VRX) (LON:VRX). The sources said the company wants to see if it can sell itself to a bigger rival. Thus far, Allergan has not responded to Valeant’s request for one of its board members to hold discussions with Bill Ackman.

Bayer may also want Allergan

Another source apparently told Bloomberg that German drug maker Bayer AG (ETR:BAYN) (FRA:BAYN) could also save Allergan, Inc. (NYSE:AGN) from a takeover by Valeant Pharmaceuticals Intl Inc (NYSE:VRX) (LON:VRX). The person reportedly said investment banks have stepped into the fray by asking Bayer if it wants to buy Allergan. However, they also said that Bayer is more focused on trying to win the auction for Merck & Co., Inc. (NYSE:MRK)

Other possibilities for Allergan

Analysts have said if Allergan, Inc. (NYSE:AGN) wants to avoid being bought by Valeant Pharmaceuticals Intl Inc (NYSE:VRX) (LON:VRX), another possibility would be to acquire another company. That would make it more difficult for Valeant to buy it. Also the company could give in and begin official negotiations with Valeant. Allergan may be seeking another suitor in order to demonstrate that it is worth more than the value Valeant is placing on it.