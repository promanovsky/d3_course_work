The House passed legislation Monday to require the Food and Drug Administration to speed approval of new types of sunscreen in the wake of a regulatory backlog that has stalled their introduction.

The agency hasn't added a new active sunscreen ingredient to its approved list in 15 years. Eight ingredient applications have been pending before the FDA, some for as long as a decade, for products that are widely sold across the world.

The ingredients pending approval help fight skin cancer by blocking UV rays, prolonging sunscreen's effect or making it easier to apply, according to dermatologists and other skin-care advocates.

The Sunscreen Innovation Act, which passed on a voice vote, would impose deadlines on the FDA to ensure it approves existing and future sunscreen ingredient applications faster.

A nearly identical bill is pending before the Senate Committee on Health, Education, Labor and Pensions. Sen. Johnny Isakson (R., Ga.), a principal sponsor of the bill, said he expects the Senate will vote on a version in September. The other main sponsor is Sen. Jack Reed (D., R.I.).

"If you've got products that are being used around the world that you can't use in the U.S. because the process has stalled—that's just inexcusable," said Mr. Isakson, who has had melanoma.

Health-policy staffers for the House and Senate said the FDA has blamed the languishing ingredient applications on a lack of sufficient resources.