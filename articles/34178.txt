A MYSTEROUS illness has killed at least 23 people in southern Guinea in six weeks, but the disease has yet to be positively identified, the health ministry announced Thursday.

“A feverish sickness whose first symptoms were observed on February 9 has claimed at least 23 lives, including that of the director of the Macenta district hospital and three staff, out of a total of 36 cases,” said Sakoba Keita, the doctor in charge of the ministry’s preventive wing.

“The administrative district of Gueckedou is the worst affected, with 13 deaths for 19 cases,” Dr Keita added, noting that “the illness is characterised by fever, diarrhoea, vomiting, and bleeding in the cases of some patients.”

Medical experts have been sent to the district to try to identify the highly contagious disease, whose symptoms are similar to those caused by Lassa fever, yellow fever and Ebola.

About 30 test samples have been sent to France for analysis, with results expected “within 48 hours”, the doctor said, adding that further samples are due to be examined at the Pasteur Institute in the Senegalese capital Dakar.

Protective measures have been taken on the spot, including quarantine for each of the patients, as well as individual and collective measures affecting the local population.

Three thousand health kits intended for medical personnel and residents of high-risk areas are awaited from Burkina Faso, while the Swiss branch of Doctors Without Borders (MSF) has already sent 100 tonnes of medical equipment.