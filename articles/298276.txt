Amazon are holding a press event tomorrow where they are expected to announce the first Amazon Smartphone, and now according to a recent report, the device will be exclusive to mobile carrier AT&T in the U.S. at launch.

Amazon already has a partnership with AT&T for its Kindle Fire tablet and e-readers,so the partnership for the new Amazon Smartphone is hardly surprising.

So far we have heard that this new smartphone will come with a 4.7 inch HD display with a resolution of 1280 x 720 pixels, the device will also feature a quad core Qualcomm Snapdragon processor.

Other rumored specifications on the Amazon Smartphone include 2GB of RAM and a choice of either 16GB or 32GB of included storage, the device is also expected to feature a microSD card slot and front and rear facing cameras.

The front cameras on the Amazon handset are said to be used to create a number of 3D effects on the handsets display, and there are rumored to be multiple cameras up front, exactly how this will work remains to be seen.

We should have full details on this new Smartphone from Amazon when it is made official tomorrow. We are hoping Amazon will put the device on sale some time shortly after it becomes official.

Source WSJ

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more