Angara is built to replace the Soyuz, a workhorse of the Soviet and then Russian space program, designed more than four decades ago. Following the retirement of the U.S. space shuttles, Soyuz currently serves as the only way to deliver crews to the International Space Station.

The botched attempt to launch the Angara booster rocket was the latest mishap to dog Russia’s troubled space industries, whose Soviet-era glory has faded in a series of launch failures.

MOSCOW (AP) — The first launch of Russia’s new space rocket after two decades of development was aborted Friday moments before its blastoff, as President Vladimir Putin was watching via live feed.

Advertisement

Angara has been developed in several versions with various payloads of up to 35 metric tons. Its development has dragged on since 1994. Its first launch had been planned for 2005, but has been continuously pushed back.

Space officials said that an automatic safety system aborted the rocket’s blastoff from the Plesetsk launch pad in northwestern Russia for an unspecified reason.

Putin looked unperturbed during a video conference when the chief of the military space forces reported the problem from Plesetsk.

‘‘Keep working calmly without haste and accurately analyze everything,’’ Putin said.

The launch was tentatively put off until Saturday.

Last month, the launch of Russia’s Proton-M booster also ended in failure, and the same type of rocket also failed at launch last July, leading to the loss of three navigation satellites. The July mishap was tracked to a flaw in the assembly line process involving a poorly qualified worker who crudely violated production technology.

Observers say the nation’s space program has been hampered by a brain drain and a steady erosion of engineering and quality standards.