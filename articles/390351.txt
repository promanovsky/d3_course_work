My colleague and friend Pim de Kuijer died on his way to an Aids conference in Melbourne, a journey which typified his concern for others.



Pim was Dutch and a parliamentary lobbyist for Stop Aids Now! I knew him from Amsterdam, through my job as an editor of a European multilingual magazine in Paris. I checked all of my emails with him a moment ago. The best of them remain lost, in my old work email. But I still have one of his old CVs.



I had just started my job when Pim was recruited to join me on a trip to Barcelona in 2007, a mission to write about the city. He was writing about unlicensed street performers, and he got an amazing, surreal picture of a clown being told off by a policeman on the Ramblas. Perfect.

He was one of my very best, most enthusiastic writers and stayed in constant contact with me. As an election observer he got to travel a little, and was a real global citizen.

He was not paid for the articles he wrote for us in Paris, but he sent dispatches with the very best of intentions: to show another side of a same old story, and to appease his passion for exploring the unknown.

The countries he covered included Ukraine and Russia, where he was an election observer for the Dutch foreign ministry.

We had a kind, mutually supportive relationship, though I was not in touch with him in recent years. He had returned to the Netherlands after a long period of soul-searching, adventure and travels when we last spoke. He had written a little memoir which he wanted to publish and asked me to read through. He was the first of my colleagues to start a first book and I was so proud. It was only 66 pages, divided into little chapters for different countries he had been to: Kosovo, Sierra Leone, and also Malaysia, where he was to return following the Aids conference.

Pim's last post and photo is from Schiphol airport. His friends and acquaintances will continue to mourn him online, as we are so used to in our generation. I don't think Pim and I were even on Facebook when we met. My friendship with him symbolises an era when so many of us young Europeans were looking for each other, to work together, to share our languages and stories, in the mid-00s. At 32, he was one year older than me.

I've spoken briefly to Pim's brother, Paul, on Facebook. He is happy for our support, and for everyone to know how wonderful Pim was, though there are plenty of others who can attest to that far better than I.

Pim interned for the former MEPs Lousewies van der Laan and Emine Bozkurt. As well as his work for Stop Aids Now!, he was active for the liberal political party D66. He held training workshops for politicians in Sarajevo and went on several election missions, most recently in Egypt.

Van der Laan said in a statement: ''Last Wednesday Pim and I met up in The Hague for a coffee as we always do when in the same town. We caught a train to Amsterdam together and spent a lovely few hours catching up. We had developed a tradition of him meeting me at foreign airports (Beijing, Sarajevo) and then trying to install some democracy into the place, while catching up, having good food and celebrating life and shared values.

“No one should have died on that flight and I still can't get my head around the fact that he was killed. I was already furious with Putin for allowing nationalist hotheads to make east Ukraine into a danger zone, but now that passenger airplanes are being shot out of the sky, this madness must be stopped.

“Pim believed in understanding between countries, the rule of law and equality for all and fought for his values through his work and his political activities. Let's try to live up to his legacy and work even harder towards a peaceful world.''

My former colleague Thamar Zijlstra, also from Amsterdam, remembered Pim coming out in public at a comedy show. She said Pim was “most of all loved by many”. She added: “He was such a great advocate for democracy and human rights.”

• This article was amended on 21 July to correct the spelling of Thamar Zijlstra's name.