Is Ryan Gosling going to be a father?

Share this article: Share Tweet Share Share Share Email Share

London - Eva Mendes and Ryan Gosling are expecting their first child. The couple, who started dating in 2011, have confirmed they will welcome a baby into the world later this year. A source also told People.com that the 40-year-old actress, who previously insisted she wasn't planning to get married or have children, has completely changed since she started dating The Notebook heartthrob, 33. The insider said: "With Ryan things are different... She’s very independent but she's content with her partner. She finally found the person she really wants to be with." The brunette beauty - who is rumoured to be seven months pregnant - hasn't been photographed with her boyfriend since November and hasn't been seen out in public since March, skipping the Cannes International Film Festival in May, sparking rumours they were on the rocks.

A source close to the actress previously said: "She's been ready for motherhood for a while now and to be sharing this experience with Ryan is a dream come true for her!"

Eva brushed off speculation she was pregnant in February after refusing to go through airport security scanners.

She said at the time: "It's so ridiculous. Because it all started because I was didn't want to go through the scanners at the airport.

"You know those x-ray scanners, which are really creepy. They basically see you naked, right. And not only that, but there's a radiation aspect to it, so I always opt out. I always ask for a personal pat down." - Bang Showbiz