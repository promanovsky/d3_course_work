Consumer stocks were moderately lower Thursday with shares of consumer staples companies in the S&P 500 retreating about 0.5%. Shares of consumer discretionary firms in the S&P 500 were down about 0.2%.

In company news, GoPro ( GPRO ) rallied in its market debut Thursday, with shares rising over 30% after the specialty camera-maker late yesterday priced its initial public offering of 8.9 million shares of its Class A common stock at $24 each, the high end of its expected range.

After deducting underwriter discounts and commissions, the company received about $200.7 million in net proceeds. Early GPRO investors sold another 8.9 million of their shares as part of the IPO, also pocketing a combined $200.7 million in net proceeds.

Shares were up nearly 31% at $31.43 each in recent trade, earlier climbing as much as 37.5% to a high as $33.00 a share this morning following a $28.65 open.

In other sector news,

(+) NATR, (+14.9%) Announces strategic alliance with Fosun Pharma as its local partner in China. Fosun buys 15% NATR stake, with NATR using $46.3 mln in proceeds to fund operations in China as well as a special $1.50 per share special dividend.

(-) BBBY, (-8.9%) Q1 EPS of $0.93 trails by $0.02 per share. Revenue rises 1.7% to $2.66 bln, roughly in-line with $2.69 bln consensus. Sees Q2 earnings of $1.08 to $1.16, missing estimates by at least $0.04 per share.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.