The 'life changing' electrical implant that can zap paralysed patient's legs into moving again

Four patients have tried the device - all have been able to move their legs

Experts say experimental treatment not a cure - but results are 'promising'

Electrical impulses shock spinal cord into temporarily repairing itself

A groundbreaking electrical implant has help four paralyzed patients move their legs again.

When the implanted device is activated, the men can wiggle their toes, lift their legs and stand briefly.



Experts say it's a promising development but warn that the experimental treatment isn't a cure.



Scroll down for video



Kent Stephenson was the second person to undergo epidural stimulation of the spinal cord, voluntarily raises his leg while stimulated at the Human Locomotion Research Center laboratory

HOW IT WORKS Researchers implanted a 16-electrode array on the patients spinal cords, allowing them to electrically stimulate it. Experts are are also working to develop a new high-density, 27-electrode array in rats to determine if it can provide finer, more robust control of locomotion. 'The technology we used in these four individuals was initially designed for the suppression of back pain, and our animal experiments have told us that we can do much better," said Reggie Edgerton of UCLA .

'For a given type of movement, we want to be able to select exactly where and how to stimulate the spinal cord.

'We just don't have that flexibility in the current technology.'

Three years ago, doctors reported that zapping a paralyzed man's spinal cord with electricity allowed him to stand and move his legs.



Now they've done the same with three other patients, suggesting their original success was no fluke.

However, they aren't able to walk and still use wheelchairs to get around.

'There is no miracle cure on the way,' said Peter Ellaway, an emeritus professor of physiology at Imperial College London, who had no role in the study.

'But this could certainly give paralyzed people more independence and it could still be a life-changer for them.'

In a new study published Tuesday in the British journal Brain, researchers gave an update on Rob Summers, of Portland, Oregon, the first to try the treatment, and described successful results for all three of the other men who have tried it.



All had been paralyzed from below the neck or chest for at least two years from a spinal cord injury.

The study's lead author, Claudia Angeli of the Kentucky Spinal Cord Research Center at the University of Louisville, said she believes the device's zapping of the spinal cord helps it to receive simple commands from the brain, through circuitry that some doctors had assumed was beyond repair after severe paralysis.

Dustin Shillcox, 29, of Green River, Wyoming, was seriously injured in a car crash in 2010.



Last year, he had the electrical device surgically implanted in his lower back in Kentucky.



Five days later, he wiggled his toes and moved one of his feet for the first time.

'It was very exciting and emotional,' said Shillcox. 'It brought me a lot of hope.'

Shillcox now practices moving his legs for about an hour a day at home in addition to therapy sessions in the lab, sometimes wearing a Superman T-shirt for inspiration.

From left to right, are Andrew Meas, Dustin Shillcox, Kent Stephenson and Rob Summers, the first four to undergo task-specific training with the new implant.

He said it has given him more confidence and he feels more comfortable going out.

'The future is very exciting for people with spinal cord injuries,' he said.

The study's other two participants — Kent Stephenson of Mount Pleasant, Texas and Andrew Meas of Louisville, Kentucky — have had similar results.

'I'm able to (make) these voluntary movements and it really changed my life,' Stephenson said.



He said the electrical device lets him ride on an off-road utility vehicle all day with his friends and get out of the wheelchair.

'I've seen some benefits of (the device) training even when it's turned off,' he added.



'There have been huge improvements in bowel, bladder and sexual function.'

The new study was paid for by the U.S. National Institutes of Health, the Christopher and Dana Reeve Foundation and others.

Experts said refining the use of electrical stimulators for people with paralysis might eventually prove more effective than standard approaches, including medicines and physical therapy.

'In the next five to 10 years, we may have one of the first therapies that can improve the quality of life for people with a spinal cord injury,' said Gregoire Courtine, a paralysis expert at the Swiss Federal Institute of Technology in Lausanne, who was not part of the study.

Ellaway said it was unrealistic to think that paralyzed people would be able to walk after such treatment but it was feasible they might eventually be able to stand unaided or take a few steps.

'The next step will be to see how long this improvement persists or if they will need this implant for the rest of their lives,' he said.