U.S. stocks kicked off a new month and new quarter with solid gains as U.S. manufacturing growth improved in March.

The indexes have now logged a three-day winning streak, an accomplishment which was completely absent throughout all of March.

The S&P 500 hit new all-time highs of 1,885.84 while the Nasdaq index outperformed the S&P 500 and the Big Board.

The Dow gained 0.46 percent, closing at 16,532.61.

The S&P 500 gained 0.70 percent, closing at 1,885.52.

The Nasdaq gained 1.64 percent, closing at 4,268.04.

Gold lost 0.27 percent, trading at $1,280.30 an ounce.

Oil lost 2.02 percent, trading at $99.53.

Silver gained 0.17 percent, trading at $19.78 an ounce.

News of Note

ICSC Retail Store Sales rose 3.6 percent week over weak, compared to a decline of 1.5 percent last week.

Redbook Chain Store Sales rose 2.3 percent year over year, compared to a 3.1 percent gain last week.

U.S. March PMI Manufacturing fell to 55.5 from 57.1 in February and missing the consensus of 56.8.

February Construction Spending rose 0.1 percent, in-line with expectation.

Recommended: 'Talented Blonde' Kristin Bentz Talks Retail And Her Favorite Company To Hate

March ISM Manufacturing Index rose to 53.7 from a prior 53.2 but still fell short of the 54.0 consensus. New Orders rose to 55.1 from 54.5 while Supplier Deliveries fell to 54.0 from 58.5.

March global Manufacturing rose to 52.4 from 53.2 in February.

China's official manufacturing PMI increased to 50.3 in March from February's 50.2. While the index focuses on large state-owned enterprises, the HBSC index, which allocates weight to smaller private companies fell to 48 in March from 48.5 in February.

The Reserve Bank of India kept its benchmark repurchase rate unchanged at 8.0 percent and its cash-reserve ratio at 4.0 percent.

Eurozone manufacturing PMI fell to 53 in March from 53.2 in February.

Analyst Upgrades and Downgrades of Note

Analysts at Deutsche Bank maintained a Buy rating on Anadarko Petroleum (NYSE: APC) with a price target raised to $116 from a previous $112. Shares gained 2.10 percent, closing at $86.54.

Analysts at Stifel Nicolaus downgraded Beam (NYSE: BEAM) to Hold from Buy with an $80 price target. Shares finished the day un-changed at $83.30.

Analysts at Deutsche Bank maintained a Hold rating on continental Resources (NYSE: CLR) with a price target raised to $128 from a previous $121. Shares lost 1.15 percent, closing at $122.85.

Analysts at Baird upgraded Tableau Software (NASDAQ: DATA) to Outperform from Neutral with a price target of $95. Shares gained 6.27 percent, closing at $80.85.

Analysts at Deutsche Bank maintained a Buy rating on EOG Resources (NYSE: EOG) with a price target raised to $218 from a previous $214. Shares gained 0.90 percent, closing at $98.97.

Analysts at JPMorgan maintained an Overweight rating of Intuitive Surgical (NASDAQ: ISRG) with a price target raised to $550 from a previous $475. Shares gained 12.70 percent, closing at $493.60.

Analysts at Oppenheimer downgraded InvenSense (NASDAQ: INVN) to Perform from Outperform and removed its $20 price target. Shares lost 1.56 percent, closing at $23.30.

Analysts at Cantor Fitzgerald downgraded Potash Corp (NYSE: POT) to Hold from Buy. Shares lost 2.46 percent, closing at $35.33.

Analysts at Deutsche Bank maintained a Hold rating on Pioneer Natural Resources (NYSE: PXD) with a price target raised to $209 from a previous $208. Shares lost 0.75 percent, closing at $185.73.

Analysts at UBS initiated coverage of Stratasys (NASDAQ: SSYS) with a Buy rating and $125 price target. Shares gained 4.60 percent, closing at $110.97.

Analysts at Citigroup maintained a Buy rating on Teva Pharmaceuticals (NASDAQ: TEVA) with a price target raised to $70 from a previous $60. Shares hit new 52 week highs of $53.10 before closing the day at $53.04.

Equities-Specific News of Note

According to Reuters, Japan Display will begin making LCDs for a 4.7 inch screen Apple (NASDAQ: AAPL) iPhone as soon as May. Production of a larger 5.5 inch display is expected to begin several months later due to several challenges. Shares gained 0.95 percent, closing at $541.85.

Recommended: Win At Investing: Play Moneyball

Pricline.com (NASDAQ: PCLN) officially changed its name to The Priceline Group. Shares gained 4.99 percent, closing at $1,251.37.

BHP Billiton (NYSE: BHP) said that it is considering its strategic options which includes a A$20 billion spinoff of non-core operations, or selling off its non-core assets individually. Shares gained 2.04 percent, closing at $69.15.

Royal Dutch Shell (NYSE: RDS/A) said that it will purchase more of its equipment from Chinese suppliers for its U.S. shale business to cut costs. Shares gained 0.29 percent, closing at $73.27.

A district judge upheld the validity of Eli Lilly's (NYSE: LLY) patent for Alimta, a lung-cancer drug which will maintain exclusivity until 2022. Shares lost 0.07 percent, closing at $58.82.

Alcoa (NYSE: AA) plans to invest $40 million to expand its rolling mill in Itapissuma, Brazil as the company expects demand to rise seven percent annually over the next three years. Shares hit new 52 week highs of $13.18 before closing the day at $13.03, up 1.24 percent.

Hewlett-Packard (NYSE: HPQ) is settling a class-action suit for $57 million in which former CEO Leo Apotheker and the management team was accused of defrauding investors by abandoning business models and fraud allegations. Shares gained new 52 week highs of $33.45 before closing the day at $33.23, up 2.69 percent.

Starboard Value is appealing to Darden Restaurants (NYSE: DRI) shareholders to hold a special meeting to discuss the company's proposed Red Lobster spinoff. Shares gained 1.42 percent, closing at $51.48.

Family Dollar (NYSE: FDO) plans to introduce more than 400 new food items across its more than 8,000 stores across the U.S. Shares lost 0.10 percent, closing at $57.95.

Wal-Mart (NYSE: WMT) plans to sell eight year and 12 year euro bonds. This marks the first time in five years the retailer will sell bonds denominated in euros. Shares gained 0.44 percent, closing at $76.77.

Marvell Technology Group (NASDAQ: MRVL) has been ordered to pay nearly $1.54 billion by a district judge for breaching disk drive patents held by Carnegie Mellon University. Marvell was previously ordered to pay Carnegie Mellon $1.17 billion in December 2012. Shares hit new 52 week highs of $16.65 before closing the day at $16.21.

Winners of Note

Celgene (NASDAQ: CELG) entered a collaboration deal with Forma Therapeutics in which Celgene will use Forma's screening technique to identity potential new drugs to license. Shares gained 5.01 percent, closing at $146.60.

The FDA has approved Intuitive Surgical's (NASDAQ: ISRG) new da Vinci Xi Surgical System. The new surgical system allows the surgeon to have a greater range of motion, and longer instrument shafts are designed to give the surgeon greater operative reach. Other improvements include a new overhead instrument arm architecture, a new endoscope digital architecture and an ability to attach the endoscope to any arm. Shares gained 12.70 percent, closing at $493.60.

Related: Shares of Intuitive Surgical Soar as Company Introduces its Da Vinci Xi Surgical System

Decliners of Note

A U.S. Disctrict Court in Delaware ruled that The Medicines Company (NASDAQ: MDCO) failed to prove that Hospira's generic Angiomax product infringes on its existing patents. The court ruled that Hospira's Abbreviated New Drug Applications did not meet the “efficient mixing” claim limitation and therefore does not infringe on Medicines Co ‘727 and ‘343 patents. Shares hit new 52 week lows of $23.53 before closing the day at $24.02, up 15.48 percent.

Earnings of Note

After the market closed, Apollo Education Group (NASDAQ: APOL) reported its second quarter results. The company announced an EPS of $0.28, beating the consensus estimate of $0.19. Revenue of $679.1 million missed the consensus estimate of $659.04 million. Shares were trading lower by 4.44 percent, trading at $33.60 following the earnings release.

Quote of the Day

"Shame on both of you for falsely accusing literally thousands of people and possibly scaring millions of investors in an effort to promote a business model." - BATS Global Markets president William O'Brian speaking on CNBC today on high-speed trading and accusations by IEX founder Brad Katsuyama and author Michael Lewis asserting that the stock market is rigged.