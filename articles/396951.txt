It's time for The Bachelorette Andi Dorfman to hand out her final rose! (Warning: Spoilers ahead!)

After months of jaw-dropping dates and envy-worthy kisses, this season of The Bachelorette finally came to a close Monday night with an extremely lavish affair and (Gasp!) an engagement. (Seriously, how gorgeous was that ring?!)

Over the two-hour event, the sought-after bachelorette went through the traditional emotional torment of deciding which suitor had her heart. So did Andi, 27, choose Nick Viall, the software salesman from Wisconsin with the ridiculously large family and passionate love of scarves? Or did she go with Josh Murray, the Atlanta native and former professional baseball star who always enjoys a good V-neck t-shirt?

Continue reading...