Adam Shell, and Matt Krantz

USATODAY

Wall Street investors hoping that stocks could extend their three-day rally are way out of luck -- stocks plunged and closed sharply lower Thursday.

A drop in the biotechnology sector led stocks lower as Biogen Idec, Gilead Sciences and other biotech companies extended a recent slump.

The Nasdaq composite index, prone to big drops lately, tumbled 129.79 points, or 3.1%, to 4,054.11. It was the worst point drop since Aug. 18, 2011, when the index dropped 131.05 points.

The Dow Jones industrial average dropped 266.96 points, or 1.6%, to 16,170.22 and the Standard & Poor's 500 index plunged 39.10 points, or 2.1%, to 1,833.08.

After making big gains last year, biotechs have been crushed in recent weeks as they come under pressure to lower prices for their drugs. Biogen dropped 4.4% to $287.35 and Gilead slid 7.3% to $65.48. Both roughly doubled in value last year.

Many "momentum" names like Facebook, Netflix and Twitter also fell. Many of these stocks have had huge runs to the upside and have gotten pricey, and many investors are selling to protect profits.

If investors think the Nasdaq has taken them on a roller-coaster ride in the past week, they are right. Getting killed Thursday on the Nasdaq:

1. Biotech. The iShares Nasdaq Biotechnology ETF dropped 5.6%.

2. Facebook: Shares of the social media stock are down 5.2% to $59.16, erasing a big chunk of Wednesday's 7.3% surge, which was the best gain on the Standard & Poor's 500 stock index.

3. Tesla. The electric-car maker tumbled 5.9% to $204.19.

4. Netflix. Shares of the video-streaming company fell 5.2% to $334.73.

5. Twitter. It hasn't been a "tweet" day for Twitter, either, with shares down 2.7% to $41.34.

In government bond trading, the yield on the 10-year Treasury note dipped to 2.65% from 2.69% late Wednesday. The price of crude oil fell 20 cents to close at $103.40 a barrel. Gold climbed $14.60 to settle at $1,320.50 an ounce.

Ally Financial's stock slumped in its market debut. The former financing arm of General Motors raised $2.4 billion in an initial public offering Wednesday, allowing the federal government to recoup the money used to bail out the company during the 2008 financial crisis. Ally dropped $1.02, or 4.1%, to $23.98.

The stock drop Thursday wiped out the gains made in the previous two session. Stocks rose Wednesday after the minutes from the Federal Reserve's March 18-19 meeting revealed a consensus that short-term rates shouldn't be increased anytime soon.

The Dow Jones industrial average rose 181.04 points, or 1.1%, to 16,437.18. The Standard & Poor's 500 index gained 20.22 points, or 1.1%, to 1,872.18 and the Nasdaq composite index jumped 70.91 points, or 1.7%, to 4,183.90.

Contributing: USA TODAY's William Cummings, Associated Press