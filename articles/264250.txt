A new report finds that American businesses and institutions are failing to meet the cybersecurity threat posed by hackers at home and abroad.

“One thing is very clear: The cybersecurity programs of U.S. organizations do not rival the persistence, tactical skills, and technological prowess of their potential cyber adversaries,” finds the 2014 U.S. State of Cybercrime Survey. “Today, common criminals, organized crime rings, and nation-states leverage sophisticated techniques to launch attacks that are highly targeted and very difficult to detect.”

Syria, Iran and Russia are cited as a “a particularly pernicious threat.”

The authors of the report—PricewaterhouseCoopers, the CERT division of software engineering at Carnegie Mellon, CSO magazine, and the U.S. Secret Service—say their findings are based on a survey of more than 500 U.S. business executives, law enforcement services and government agencies, as well as previous research and recommendations provided by the National Institute of Standards and Technology.

The report lays out the mounting threat to infrastructure systems like gas pipelines and the electrical grid as well as the disproportionately high financial costs of cybercrime in America compared to the rest of the world’s organizations.

The report advises companies to invest in protecting the “crown jewels” of a company, such as customers’ financial information for a retailer and trade secrets for a pharmaceutical company. Several large companies, including Target and eBay, have recently admitted being infiltrated by hackers. In Target’s case, an estimated 40 million customers had credit and debit card data stolen.

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.