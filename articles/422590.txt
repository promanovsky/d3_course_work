GlaxoSmithKline's experimental Ebola vaccine could be tested on humans in the UK, US, the Gambia and Mali in the next few weeks, in a race to contain the deadly virus that has claimed more than 1,500 lives in west Africa.

The news came as the World Health Organisation (WHO) warned that the Ebola epidemic could eventually exceed 20,000 cases. Bruce Aylward, WHO's assistant director-general for emergency operations, said: "This far outstrips any historic Ebola outbreak in numbers. The largest outbreak in the past was about 400 cases."

Human trials of GSK's experimental vaccine – which Britain's largest pharmaceuticals company is developing with the US National Institutes of Health (NIH) – are to be fast-tracked, with funding from an international consortium. Vaccines normally take 10 years to develop, but GSK said it hopes to finish the first phase of trials by the end of 2014. It will start making up to 10,000 doses of the vaccine at the same time as the initial clinical trials, so that – if they prove successful – stocks could be made available immediately to the WHO to vaccinate people in high-risk communities.

The Ebola outbreak has killed 1,552 people in Sierra Leone, Liberia, Guinea and Nigeria out of 3,069 cases, according to the WHO's latest figures. It has declared the epidemic an international health emergency and published an action plan on Thursday to contain the disease within six to nine months.

If regulatory approvals are granted, the UK research teams could start vaccinating healthy volunteers from mid-September. In the US, the NIH could start phase 1 trials – the first test on humans to assess safety and efficacy – at its clinical centre in Bethesda, Maryland, as soon as next week after receiving the green light from the US health regulator, the Food and Drug Administration. The vaccine has already shown promising results in primates exposed to Ebola, without significant adverse effects.

A £2.8m grant from the Wellcome Trust, the Medical Research Council and the Department for International Development will fund safety tests by a team led by Prof Adrian Hill, director of the Jenner Institute at the University of Oxford. If successful, the trial will be extended to volunteers at the MRC unit in the Gambia, and to Bamako, Mali.

The US Centres for Disease Control and Prevention has begun discussions with ministry of health officials in Nigeria about conducting a phase 1 safety study of the vaccine among healthy adults.

The Oxford study will involve 60 healthy volunteers, while those in the Gambia and Mali will each involve 40. Each set of volunteers will be split into groups of 20 who will receive different doses of the vaccine, which is based on a type of chimpanzee cold virus and does not contain infectious Ebola virus material. Two Ebola genes have been inserted into the vaccine, and express a single Ebola virus protein to generate an immune response.

Hill said: "The tragic events unfolding in Africa demand an urgent response. In recent years, similar investigational vaccines have safely immunised infants and adults against a range of diseases including malaria, HIV and Hepatitis C. We, and all our partners on this project, are optimistic that this candidate vaccine may prove useful against Ebola."

The experimental vaccine is against the Zaire species of Ebola, which is the one circulating in west Africa and for which there is no cure. The vaccine was designed by Nancy J Sullivan, the head of the biodefence research section in NIAID's Vaccine Research Centre (part of NIH), in collaboration with researchers at the Swiss-Italian biotech firm Okairos, acquired by GSK last May for €250m (£150m).

NIAID is also supporting work on other early-stage Ebola vaccines, including one from Johnson & Johnson's Crucell division that is expected to enter early clinical studies around the turn of the year.

NewLink Genetics, based in Iowa, is also working on an Ebola vaccine and has just struck a deal with another firm to manufacture the product and increase its supply, before starting preclinical trials.

Experimental Ebola treatments include ZMapp, made by San Diego-based Mapp Biopharmaceutical. The drug has been given to Will Pooley, the British nurse who contracted the virus in Sierra Leone and has been flown back to London.