Women who have bariatric surgery may be less likely to contract uterine cancer, according to research presented at the latest Society of Gynecologic Oncology meeting. The study found that obesity nearly tripled the risk of uterine cancer, while those women who maintained weight loss following bariatric surgery recorded a 71% lower risk of developing uterine cancer.

"A history of bariatric surgery is associated with a substantial and clinically significant reduced risk of uterine cancer," said lead author Kristy Ward.

"Our previous work, in agreement with the findings of others, has indicated that the risk of uterine malignancy increases linearly with BMI. Along with the findings of this current study, this supports that obesity may be a modifiable risk factor related to development of endometrial cancer."

Of the groups studied, non-obese with a history of bariatric surgery had the lowest risk of uterine cancer at 270/100,000 admissions, while obese women without a history of bariatric surgery had the highest incidence at 1,409/100,000 admissions.

For comments and feedback contact: editorial@rttnews.com

Health News