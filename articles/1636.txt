Spritz, a new app for Samsung devices, claims it can help one read 1,000 words per minute.

Spritz Speed Reading Technology for Samsung Claims It Can Help You Read 1,000 Words Per Minute

Spritz, a program for Samsung devices, claims it can help one read 1,000 words per minute.

It works by showing you a word at a time and it displays the words in a fast timeframe. The words then speed up faster and one has to try and get the meaning faster.

The highest setting is 1,000 words per minute, but reviews say that looking at around 500 words per minute is mind-boggling. Around 250 words per minute is fine.

It also positions the words around central letters, meaning they’re easier to read.

According to Time, Spritz can allow one to read “Harry Potter and the Sorcerer’s Stone” in 77, while The Bible takes about 13 hours at 1,000 words.

Spritz developers say they spent around three years developing the technology that makes it work. And they told Discovery it can be learned in about five minutes.

As of late February, around a million people downloaded it, the company says.

“Reading is inherently time consuming because your eyes have to move from word to word and line to line. Traditional reading also consumes huge amounts of physical space on a page or screen, which limits reading effectiveness on small displays. Scrolling, pinching, and resizing a reading area doesn’t fix the problem and only frustrates people,” the developers say its website.

It adds: “Now, with compact text streaming from Spritz, content can be streamed one word at a time, without forcing your eyes to spend time moving around the page. Spritz makes streaming your content easy and more comfortable, especially on small displays.”

“Our ‘Redicle’ technology enhances readability even more by using horizontal lines and hash marks to direct your eyes to the red letter in each word, so you can focus on the content that interests you. Best of all, Spritz’s patent-pending technology can integrate into photos, maps, videos, and websites to promote more effective communication,” it says.

Elite Daily posted video of the technology in motion.