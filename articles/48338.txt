Sarah Tew/CNET

Buyers of the new HTC One M8 will want to resist taking the phone apart to attempt any repairs, according to a new teardown from iFixit.

Hacking through the innards of HTC's new flagship phone introduced on Tuesday, the iFixit team found the journey fraught with hazards and obstacles. In other words, this teardown trip is not for the faint of heart.

iFixit's journey through the HTC One M8 started out promisingly enough. Easy-to-remove screws keep the body together instead of relying on "gobs of adhesive." Messy tendrils of display cables have been replaced by spring contacts, allowing the team to remove the phone's assembly much more easily.

But then the trip begins to go downhill.

The motherboard is glued to the base, challenging all who try to remove it. Why attempt such a challenge in the first place? The motherboard must be removed in order to get to the battery, which itself is adhered to the LCD shield.

More glue is found adhered to the daughterboard, calling iFixit's special spudger tool into action once again. After eventually stripping out most of the front panel, the team finally arrives at the phone's display but to some dismay.

"We've gotten accustomed to Apple's display-out-first iPhone assembly -- which greatly simplifies display repairs -- so we're not sure why some manufacturers insist on burying both the screen and the battery," iFixit says. "At least make one easy to replace!"

And here's where the teardown takes a turn for the worse. Applying heat around the edges of the glass screen to remove it, iFixit cuts the digitizer cable, sending the phone to an early death.

So just where and how did the trip go wrong?

The HTC One M8 was difficult, though not impossible, to open without damaging the rear case. Trapped beneath the motherboard, the battery is hard to replace. The display assembly can't be removed without traipsing through the entire phone, a problem if your screen breaks and needs to be replaced. And finally, many of the components are stuck with lots of tape and adhesive, a chore to remove.

The final grade on iFixit's repairability score: 2 out of 10 (10 being the easiest to repair).