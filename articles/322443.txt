Who needs to watch Argentina play in the World Cup? The country’s legal maneuvers to avoid paying billionaire Paul Singer and his hedge fund pals are more exciting — and with much more serious consequences.

Here’s the latest play-by-play:

On Thursday, Argentina announced it had deposited millions of dollars to pay some bondholders Monday — seemingly in defiance of a court order.

Minutes later, Federal Judge Thomas Griesa denied the country a stay of that order.

No sooner had that occurred than Singer’s lawyers whipped out a letter to Griesa calling Argentina’s latest move a “brazen step in violation of this Court’s orders” and said that “it warrants a swift and decisive response.”

By 5:30, Griesa had ordered a hearing on the matter for 10:30 Friday morning.

Earlier, Griesa said Argentina’s request for a stay was “not appropriate” and referred the dispute to the “special master” appointed Wednesday to handle negotiations among the parties.

Argentina wanted the stay to avoid paying Singer and allies $1.65 billion on Monday when it is scheduled to make a quarterly coupon payment to other bondholders.

The country said paying Singer would trigger some $132 billion in additional bond payments, including claims from the restructured bondholders. A clause in those bonds said no other investor can benefit more.

Right before Griesa issued his order, Argentina Economy Minister Axel Kicillof said the country had deposited $832 million in the Bank of New York Mellon in Buenos Aires, explaining it signaled Argentina’s “willingness to pay.”

Some $539 million of that would go to the holders of exchange bonds, whose coupon payment is due June 30.

Bank of New York would be in violation of the court if it transferred the payment to those bondholders, who agreed to a debt swap, without also paying the so-called holdouts, led by Singer, who refused the deal and demand to be paid in full.

Singer’s lawyer wrote Griesa that it had contacted the bank “to remind it of its obligations under this Court’s orders.”