DFW Weather: North Texas Braces For Historic Winter Storm This Weekend North Texas is currently under a Winter Storm Warning that lasts through Monday.

Disaster Declaration Issued For Texas Due To Winter Weather Texas Gov. Greg Abbott has issued a disaster declaration for all counties due to "severe winter weather" impacting the state.

With 'Near Blizzard Conditions Possible' In North Texas, ERCOT Expects Record-Breaking Electric UseWith what is predicted to be historically cold weather this weekend through the middle of next week in Texas, the state's electrical grid operator is expecting record-setting usage.