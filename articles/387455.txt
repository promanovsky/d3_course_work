GETTY The Malaysian Airlines plane was shot down over Ukraine

FREE now and never miss the top politics stories again SUBSCRIBE Invalid email Sign up fornow and never miss the top politics stories again When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.





The team found dead bodies strewn on the ground in the village of Grabovoand near the Russian border and at the heart of fighting in the wartorn country. Around 30 monitors arrived 24 hours after the Boeing 777 went down in Ukraine after allegedly being shot out of the sky killing all 298 people onboard.The team found dead bodies strewn on the ground in the village of Grabovoand near the Russian border and at the heart of fighting in the wartorn country. **CLICK HERE TO SEE MORE HORRIFYING IMAGES OF THE CRASH SITE** It was claimed that the investigators had to deal with strict restrictions enforced by armed militiamen on the ground and were not allowed full access by them.



The investigators face a challenge deciphering a disaster scene that spread eight square miles of contested ground in eastern Ukraine.



Thomas Greminger, from the Organisation for Security and Cooperation in Europe, said the investigators were not given the access they "expected".







EPA World Health Organisation spokesman Glenn Thomas is among the dead

He said: "They did not have the kind of access that they expected. They did not have the freedom of movement that they need to do their job.



"The crash site is not sealed off.



"In the current circumstances, they were not able to help securing this corridor that would allow access for those that would want to investigate." Both the Ukrainian government and pro-Russian insurgents have blamed each other for downing the passenger jet with a surface-to-air missile.



There is growing evidence the plane was shot out of the sky by separatists and the global community has demanded an international investigation.

Ten Britons were on the plane, which was travelling from Amsterdam to Kuala Lumpur, along with 189 passengers from the Netherlands. Both the Ukrainian government and pro-Russian insurgents have blamed each other for downing the passenger jet with a surface-to-air missile.There is growing evidence the plane was shot out of the sky by separatists and the global community has demanded an international investigation.Ten Britons were on the plane, which was travelling from Amsterdam to Kuala Lumpur, along with 189 passengers from the Netherlands.

The plane was travelling at an altitude of 33,000 feet when it was hit. Emergency workers and police officers are currently searching through the wreckage. Russian President Vladimir Putin did not deny that pro-Russian rebels were behind the attack, but laid the blame with war-torn Ukraine.

SWNS Richard Mayne from Leicester was one of the victims

It is an absolutely shocking incident and cannot be allowed to stand David Cameron

He said: "This tragedy would not have happened if there were peace on this land, if the military actions had not been renewed in south-east Ukraine. "And, certainly, the state over whose territory this occurred bears responsibility for this awful tragedy." However, Ukraine was quick to blame separatists, with President Petro Poroshenko describing the incident as a "terrorist attack". Reports have claimed the black box has been found by separatists and handed over to Moscow - potentially hindering an international investigation into the tragedy. David Cameron, speaking after he chaired an emergency Cobra meeting today, said: "If, as seems possible, this was brought down then those responsible must be brought to account and we must lose no time in doing that.

AP Distraught relatives of MH17 passengers at Schiphol Airport

"It is an absolutely shocking incident and cannot be allowed to stand." He urged both Moscow and Kiev to do everything possible to help, adding: "We've got to get to the bottom of what happened, and how this happened." The Duke of Cambridge also spoke of his "deep sadness" over the disaster. Speaking at an event at Australia House in London, Prince William said it was a "particularly cruel tragedy" for Malaysia coming so soon after the disappearance of another Malaysia Airlines plane. Dutch Prime Minister Mark Rutte ordered flags to fly at half mast at government buildings across the country to mark what is thought to be the worst air disaster in the country's history. He said: "The whole of the Netherlands is in mourning. This beautiful summer day has ended in the blackest possible way."

AP The luggage of the passengers is being collected

Forty-four Malaysians were also among the victims, including 15 crew members and two infants. The country is still reeling from the last Malaysian Airlines disaster, in which a plane carrying 227 passengers disappeared in March. Malaysian Prime Minister Najib Razak told a press conference: "If it transpires that the plane was indeed shot down, we insist that the perpetrators must swiftly be brought to justice. "This is a tragic day, in what has already been a tragic year, for Malaysia." The death toll also includes 27 Australians, 12 Indonesians, four Germans, four Belgians, and three Filipinos. A Canadian, and a New Zealander were also on board. There are four passengers whose nationalities have yet to be confirmed.

AP The wreckage was still smouldering last night

American Senator John McCain said there "would be hell to pay" if the plane was shot down by the Russian military or separatists. He said it would be "embarrassing" to "leap to conclusions", but added: "If it is the result of either separatist or Russian actions mistakenly believing this was a Ukrainian war plane, I think there's going to be hell to pay and there should be." President Barack Obama, who described the crash as a "terrible tragedy", has already held talks with Vladimir Putin. Angela Merkel joined calls for the perpetrators to be brought to justice. She said: "These events have once again shown us that what is required is a political solution and above all that it is also Russia that is responsible for what is happening in Ukraine at the moment." She added that she was making "a very clear call for the Russian president and government to make their contribution to bringing about a political solution."

EPA Relatives of a Malaysian victim

Australian Prime Minister Tony Abbott called for an "absolutely comprehensive" investigation, and called for "no protecting of people who may be backed by Russia but who may have been involved in this terrible event". He added: "It was not an accident, it was a crime, and criminals should not be allowed to get away with what they have done. "It was downed by a missile which seems to have been launched by Russian-backed rebels, and again I want to stress that Australia takes a very dim view of countries which are facilitating the killing of Australian citizens." One of the British victims has been identified as Glenn Thomas, a British media relations co-ordinator for the World Health Organisation. Newcastle United fans John Alder and Liam Sweeney were also among those killed as well as Leeds University student Richard Mayne and Loughborough University student Ben Pocock. Up to 100 passengers on the plane, which was travelling from Amsterdam to Kuala Lumpur, were reportedly delegates on their way to an international conference on Aids in Melbourne, including world-renowned researcher and former International Aids Society president Joep Lange.

ALAMY A picture of a Buk missile system that is believed to have to downed the MH17 plane

Ukraine claimed that it had evidence that proved pro-Russian rebels were responsible for the tragedy. Ukraine's ambassador to Nato Ihor Dolhov told BBC Radio 4's Today programme: "Evidence and information we have as now confirms it was pro-Russians (who caused) this tragedy which took the lives of 298 people, including Brits - please accept our condolences. "We intercepted and distributed scripts of phone calls from separatists to Moscow. This exchange by phone of information confirms that separatists are responsible and involved. "But that's not the only evidence. The Ukrainian defence ministry clearly stated we didn't have any facility and any arms systems available at the region to act like that. "We are ready to co-operate and we have requested additional information from all possible sources, including Nato headquarters."

REUTERS The site of the crash

IG Shocking footage uploaded to YouTube showed the burning wreckage of MH17

A Ukrainian newspaper has published details of phone conversations which it says are between Russian intelligence officers and separatist commanders, in which a man claims "we have just shot down a plane". The conversations, which have not been independently verified, appear to show rebels describing the carnage. One man described as a rebel nicknamed Major, says the plane was shot down by 'Cossacks from the Chernukhino roadblock', adding: "It is definitely a civilian plane... there was a lot of people on board." He later says: "I am only surveying the scene where the first bodies fell. There are the remains of internal brackets, seats and bodies." It is thought that rebels may have shot the plane with a surface to air missile in the belief that it was a Ukrainian military aircraft. A local man at Grabavo, where the wreckage landed, said: "I was working in the field on my tractor when I heard the sound of a plane and then a bang. "Then I saw the plane hit the ground and break in two. There was thick black smoke."

CATERS Information of the path Malaysia Airlines flight MH17 was on when it was downed

Another villager described how the body of a woman fell through her roof. Irina Tipunova, 65, said: "There was a howling noise and everything started to rattle. Then objects started falling out of the sky. "And then I heard a roar and she landed in the kitchen." Villager Aleks Noit added: “Wreckage and bodies fell on the private houses in the village and near the hospital. People in uniform collected corpses.” An emergency worker at the scene described seeing at least 100 bodies, scattered around the area. Several villagers are believed to be among the dead. There were also reports of an unknown aircraft flying near the downed passenger jet. Broken pieces of the wings were marked with blue and red paint, the same colours as the emblem of the Malaysian Airlines emblem, surrounded where the plane went down. Footage was uploaded to YouTube allegedly showing the smoking wreckage left by the Malaysia Airlines passenger plane. The clip showed a residential area in Ukraine while plumes of black smoke reaching to the sky can be seen in the background.

IG It was claimed that flight MH17 was shot down by Russians after it entered their airspace

REUTERS Separatist rebels in Donetsk said they were not responsible for shooting down the plane

The vice president of Malaysia Airlines said today that relatives of the victims would receive £3,000 towards their immediate needs. It was also claimed that the airline planned to fly relatives to the Ukraine. The MH17 crash marks the second tragedy involving Malaysia Airlines this year. In March, flight MH370 carrying 239 people went missing and despite an extensive search not a single trace of it has been found. The country's government later said they believed the plane had crashed and all aboard MH370 were dead. Ukraine and Russia have been waging a separatist conflict in the Donetsk region, where the flight was downed.