The World Health Organization has requested countries to raise taxes on tobacco to curb its usage.

Tobacco related death numbers are astounding. Estimates from the CDC reveal that tobacco kills more than half of its users, with one person dying every 6 seconds.

May 31 is marked as World No Tobacco Day. The World Health Organization recently released a report requesting countries to raise taxes on tobacco, in an attempt to curb its usage and prevent people from getting addicted to it.

"Raising taxes on tobacco is the most effective way to reduce use and save lives," said WHO Director-General Dr Margaret Chan in a press statement. "Determined action on tobacco tax policy hits the industry where it hurts."

Raising taxes will be most effective in discouraging the young and the poor from using tobacco. Additionally, it will also encourage regular smokers to either quit or reduce their daily usage.

"Price increases are 2 to 3 times more effective in reducing tobacco use among young people than among older adults," said Dr Douglas Bettcher, Director of the Department for Prevention of Noncommunicable Diseases at WHO. "Tax policy can be divisive, but this is the tax rise everyone can support. As tobacco taxes go up, death and disease go down."

Raising tobacco taxes can also benefit the economy of a country. According to calculations made by the health organization, if a country raises tobacco taxes by 50 percent per pack, it can earn an extra US$ 101 billion in global revenue. These funds can be used for better health and other social programs.

France and the Philippines have already raised their tobacco tax and have reaped several benefits. France made many changes in its cigarette prices between the early 1990s and 2005. As a consequence of these changes, tobacco sales fell by over 50 percent and death rates due to lung cancer dropped dramatically among young men. The Philippine government also collected more than the expected revenue after increasing tobacco taxes. More than 85 percent of this revenue will be spent on health services.

Tobacco Tax Hike In The Past

In recent years, almost every state in the U.S. and the federal government has increased tobacco taxes. The average state cigarette tax is currently $1.51 per pack, but rates vary widely from 17 cents in Missouri to $4.35 in New York. According to the American Lung Association's annual State of Tobacco Control report, New York has the highest tobacco tax. More than 80 percent of the city's adult smokers start before age 21, according to Health Commissioner Thomas Farley

However, there are some people who fear that raising tobacco rates may have negative consequences. Setting $10.50 minimum prices for packs of cigarettes and little cigars won't stop young people from smoking and will instead drive consumers to illegal sellers, a retailer group said. A group of convenience-store operators said revised rates may lead to fewer jobs as the demand for tobacco products may reduce.

On February 4, 2009, the Children's Health Insurance Program Reauthorization Act of 2009 was signed into law, under President Barack Obama's administration. The Act raised the federal tax rate for cigarettes on April 1, 2009 from $0.39 per pack to $1.01 per pack

Consumer spending on tobacco rose from $80 billion in 2008 to $98 billion in 2011 in inflation-adjusted dollars - even though the amount of tobacco purchased fell 11 percent, Bureau of Economic Analysis data show. Higher taxes accounted for about half that spending increase. The rest went to tobacco companies and retailers.

According to the Clean Indoor Air Act, introduced by the U.S. federal government in 2003, smoking in bars, restaurants, bingo halls and bowling alleys was illegal and could attract a penalty of up to $2,000.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.