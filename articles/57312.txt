Ford is giving Americans a choice: Either you can be a self-absorbed Cadillac owner who spends his entire life working so he can buy expensive things, or a civic-minded Ford owner who pours her heart and soul into a project aimed at making the world a better place.

That's the message in a new Ford commercial skewering Cadillac for a controversial recent ad that ruffled feathers by presenting what some felt was a misguided perspective on the American work ethic.

The Cadillac ad starred actor Neil McDonough as a rich guy looking out at his pool and pondering why he and other Americans work so hard when people in other countries take off the whole month of August.

He then turns to the camera and gives an impassioned monologue while walking around his expensive house. In it, he describes Americans as "crazy-driven, hard-working believers" who do bold things like go to the moon.

He ultimately walks out to his new Cadillac ELR hybrid coupe. "It's pretty simple. You work hard, you create your own luck, and you've got to believe anything is possible. As for all the stuff? That's the upside of only taking two weeks off in August. N'est-ce pas?"

In order to draw a contrast, Ford mimicked the structure of Cadillac's commercial. Only it decided to use Pasho Murray, a real woman who founded Detroit Dirt , a company that turns waste into compost and sells it to people who want to create urban gardens.

Murray looks out at a landfill and wonders why Americans aren't more like other countries who buy locally grown food.

She gives her speech while walking through a local restaurant where she collects food scraps for compost. She says Americans are starting to get more food from local markets because they're "crazy entrepreneurs trying to make the world better."

She then walks out to her Ford C-Max Energi and draws a conclusion of her own. "It's pretty simple. You work hard, you believe that anything is possible, and you try to make the world better. You try. As for helping the city grow good, green, healthy vegetables? That's the upside of giving a damn. N'est-ce pas?"

Here's Cadillac's ad: