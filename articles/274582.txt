This is really unexpected.

Warner Bros. just delayed one of its biggest movies of the summer, "Jupiter Ascending," until next year.

The big-budget sci-fi movie, starring Channing Tatum and Mila Kunis, was set to be the next movie from the Wachowski siblings - the duo behind "The Matrix" series.

How abrupt is this change in schedule?

Well, the movie was due for release in about seven weeks on July 18.

Instead, the movie will now be released February 6. That's the same weekend WB released "The Lego Movie" this year and it turned into a break-out hit dominating the box office for four weeks.

While it's smart to put a big movie out when there's little competition, the unexpected move raises some serious questions about the Wachowski picture.

According to Variety, the move was made to finish "more than 2,000 special effects shots in the film."

"With the July release date, they were just not going to make it on time," said domestic distribution chief Dan Fellman.

While that may be the case, there may be other concerns about the $150 million movie.

There hasn't been a lot of buzz about the original sci-fi flick at all.

Tatum's last summer movie "White House Down" ended up underperforming for Sony (granted it was overshadowed by the very similar "Olympus Has Fallen").

And the last time the Wachowskis tried their hands at another sci-fi flick for Warner Bros. we ended up with Tom Hanks and Halle Berry's "Cloud Atlas." The $100 million film made $130 million at theaters.

You would think Warner Bros. would want to keep a movie in the July 18 weekend slot. It's one of the best performing weekends for the studio producing two "Dark Knight" and "Harry Potter" hits.

This is the second time Warner Bros. has recently removed a film from this weekend. The "Batman V Superman" movie was originally slated for July 17, 2015 before being pushed back to April 2016.



The other factor to consider is that Warner Bros. has no shortage of movies coming out. That's both a good and a bad thing because none of them are surefire hits for the studio.

There's "Tammy" starring Melissa McCarthy. While the actress has been a box-office hit, the trailers haven't offered much past a fast food robbery for viewers to latch onto. Then there's Clint Eastwood's "Jersey Boys" adaptation out later this month and a tornado thriller "Into the Storm" on the way this August.

Tom Cruise's "Edge of Tomorrow," out Friday, is getting favorable reviews - as it should, it's pretty good - but the $175+ million film already isn't performing well overseas. That's where Cruise's movies usually make a lot of their money. So far it's made $23.8 million.

Either WB didn't see this movie performing as well and the studio is doing some pre-damage control to offset other potential misfires or they wanted to save it for early next year to have the first big hit of 2015.

Since the track record for February releases isn't stellar, the latter doesn't seem the case.

If "Jupiter Ascending" underperforms in the February slot the good news for WB is that there should still be money rolling in from the final installment of "The Hobbit" out this holiday.