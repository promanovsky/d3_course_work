The U.S. Department of Transportation said on Wednesday there is no need 'at this time' to advise owners of recalled General Motors cars to park them until repairs are made.

The agency added that the recalled vehicle's safety risk is "sufficiently mitigated" by recommended actions issued by the car maker.

Last month, A federal judge rejected a bid to compel GM to tell customers to stop driving millions of cars that have been recalled for defective ignition switches.

Attorneys representing several vehicle owners had sought an emergency order directing GM to issue "park it now" notices for the 2.6 million vehicles that have been recalled since February over the switches, saying they were too dangerous to remain on the road.

This story is developing. Please check back for further updates.