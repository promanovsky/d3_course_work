The World Organization (WHO) has warned that the spread of polio has become an international public emergency.

According to the international agency, outbreaks in Asia, Africa and Middle East are an "extraordinary event" which needs a coordinated "international response" and recommends citizens of affected countries travelling abroad carry a vaccination certificate, the BBC reported.

WHO has already recorded 68 cases of polio by 30 April and declared that Pakistan, Cameroon, and Syria "pose the greatest risk of further wild poliovirus exportations in 2014".

Bruce Aylward, WHO Assistant Director General, said that if the situation remains unchecked, then it could result in failure to eradicate globally one of the world's most serious vaccine preventable diseases.

According to WHO, Afghanistan, Equatorial Guinea, Ethiopia, Iraq, Israel, Somalia and Nigeria have been listed as the countries that are "posing an ongoing risk for new wild poliovirus exportations in 2014.