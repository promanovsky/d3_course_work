NEW YORK(CBSNewYork) — It was a mad dash Saturday for many Rockland County residents to get vaccinated for Hepatitis A.

More than 200 people showed up at the County Fire Training Center in Pomona for the free shots after the Rockland County Health Department warned that a waiter at La Fontana restaurant in Nyack was diagnosed with the disease.

Hundreds of people may have been exposed.

Among those who may have been exposed: 17 nuns celebrating the Feast of the Annunciation.

“These things happen. We have to go with the flow as they say, and just enjoy life one day at a time,” one nun said.

“We have never had a problem with this restaurant. They’re very popular. I will tell you they’ve been very cooperative,” another said.

Anyone who dined at the restaurant between March 19 and March 28 should be on the alert for flu-like symptoms.

Patrons who dined in the restaurant between March 29 and April 1 still have time to get vaccinated.

The Rockland County Health Department will also be offering the free shots on Sunday, starting at 11 a.m. at the County Fire Training Center in Pomona.

You May Also Be Interested In These Stories

[display-posts category=”news” wrapper=”ul” posts_per_page=”4″]