America’s hiring spree kicked into full gear in June as the economy added more than 200,000 jobs for the fifth month in a row, according to government data released Thursday.

The streak is the longest since the late 1990s and provides convincing evidence that the recovery has rebounded after unexpectedly shrinking during this year’s harsh winter. The Labor Department reported 288,000 net new jobs were created in June, and the unemployment rate dropped to 6.1 percent.

“We’re achieving escape velocity," said Joseph LaVorgna, chief U.S. economist at Deutsche Bank. "It's all there for some real strength."

Markets jumped on the news Thursday, with the blue-chip Dow Jones average up nearly 0.5 percent in morning trading -- and breaking 17,00o for the first time. The broader Standard & Poor's 500-stock index was up 0.4 percent.

The solid numbers reinforced other encouraging data released this week suggesting the economy may finally be ready for liftoff. Auto sales -- a consistently bright spot in the recovery -- heated up even more in June. They clocked in at 17 million at an annualized rate for the best month in eight years. In addition, a closely watched private indicator of the labor market by human resources consulting firm ADP offered a similarly stellar prediction 0f 281,000 jobs added last month. The spike was driven in part by the construction industry, which created the most jobs since 2006.

Perhaps most important, Gallup found that 45 percent of Americans were working full-time in June, one of the highest rates since the polling company began tracking the figure in four years ago. The government data released Thursday mirrored those results, with the employment-to-population ratio rising to 59 percent, the highest level since 2009.

“While few might agree that the economy has fully recovered from the Great Recession, there is no doubt that the job market is much stronger now than in prior years,” Gallup said in its report.

The job gains were spread across a range of sectors, indicating the recovery is broad-based. Leading the way was professional and business services, which added a net 67,000 jobs. Retail and food service were next, while manufacturing and financial services also enjoyed significant gains.

The government data also showed average hourly earnings jumped six cents to $24.45. Estimates of hiring in April and May were also revised upward by 29,000 net new jobs.

The strong hiring in June far exceeded the consensus forecast of about 215,000 jobs and could present a quandary for the Federal Reserve. The nation's central bank is in the midst of phasing out its trillion-dollar bond-buying program and is starting to debate when it should raise short-term interest rates, which stand at zero. The Fed has been treading carefully for fear of undermining the recovery or disrupting the markets. But an increasingly vocal chorus of critics contend that the rapidly improving labor market is a sign that the central bank may have waited too long to tighten policy.

"I would say the Fed is really behind the curve," LaVorgna said.

Indeed, the drop in the unemployment rate in June means it is already at the level that the Fed had predicted it would land at the end of the year. But Fed Chair Janet Yellen has argued that the decline may moderate as a stronger labor market pulls in workers who had given up looking for a job.

Thursday's data included several reminders of the stubborn weaknesses that have bedeviled the nation’s progress. The number of long-term unemployed workers declined but remained elevated at more than 3 million, and they represent about a third of those without a job. The ranks of people working part-time even though they would like more hours actually increased by more than 250,000. Economists say those factors help make the unemployment rate a less reliable reading of the state of the recovery than it once was.

“This is one of the strongest [jobs] reports we’ve seen since the end of the Great Recession," U.S. Labor Secretary Thomas Perez said in an interview. But, he added, "we also know there is a tremendous amount of unfinished business. Too many people remain at the sidelines."