Governor Andrew Cuomo announced an ambitious plan Sunday to end New York state's three-decade HIV epidemic by the year 2020 through a boost in testing, reducing new infections and expanding treatment, the Associated Press reported. If successful, it would be the first time that the number of people living with HIV would have gone down since the crisis began in 1981.

The state's aim is to reduce new HIV diagnoses to 750 by the end of the decade, down from 3,000 expected this year and 14,000 new cases of the disease in 1993, the governor said. "Thirty years ago, New York was the epicenter of the AIDS crisis," Cuomo said. "Today I am proud to announce that we are in a position to be the first state in the nation committed to ending this epidemic."

As a means to expand treatment, the state's Department of Health has negotiated bulk rebates with three companies producing HIV drugs, including taking steps to make it easier to get tested, changing how HIV cases are tracked to ensure patients continue to receive treatment, and boosting access to "pre-exposure" drugs that can help high-risk people avoid infection.

Although cost estimates of the plan were not revealed by Cuomo, he said the state would end up saving more than $300 million per year by 2020 through reduction in payments of medical care for those suffering from HIV.

The governor's announcement was praised by groups that have long advocated for HIV patients, saying how it shows that efforts to fight the disease are paying off, and an affliction that once seemed unbeatable can now be successfully fought. "We have the tools and know-how to end the AIDS epidemic in New York, the only question is whether we have the political will," said Jason Walker, an organizer at VOCAL-NY, which advocates for low-income HIV patients. "Even without a vaccine or cure, Cuomo understands that we can dramatically reduce new infections below epidemic levels and ensure all people living with HIV achieve optimal health."

In the last 10 years, the number of new HIV cases in New York has dropped nearly 40 percent because of better, faster tests; access to condoms; public outreach campaigns and other initiatives. Meanwhile, more effective treatments have led those living with the disease to love longer.

While the goal of bringing the disease to below epidemic levels "is ambitious," said Mark Harrington, executive director of the anti-HIV organization Treatment Action Group, it is still "grounded in reality."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.