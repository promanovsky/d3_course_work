LONDON (AP) — British scientists say there isn’t enough evidence to prove the antiviral drug Tamiflu reduces the spread of flu.

The findings published Thursday in the journal BMJ question whether governments worldwide were justified in spending millions on the drug during the 2009 swine flu pandemic.

Researchers at the Cochrane Collaboration and BMJ have long argued there weren’t enough trials to prove Tamiflu works and say after getting access to internal trials conducted by its maker, Roche AG, they found Tamiflu reduces flu symptoms by about a half a day in adults but has little effect in children. Compared to people taking a placebo pill, there was no proof people taking Tamiflu were less likely to be hospitalized or suffer serious flu complications.

Experts not involved in the research pointed out that Tamiflu works best when given early and that it’s hard for doctors to catch people at that stage.

“It’s not that the drugs don’t work, it’s just difficult to use them to their best effect,” said Wendy Barclay, an influenza expert at Imperial College London, in a statement. She cited some problems with the way scientists at the Cochrane group and BMJ interpreted the data and said it was wise of governments to stockpile Tamiflu since there are few drugs available to treat flu. “If another pandemic came tomorrow and the (U.K.) government had no drug with which to treat thousands of (flu) patients, I imagine there would be a public outcry.”

Swiss pharmaceutical Roche said they “fundamentally disagree” with the overall conclusions of the BMJ series and said more than 100 countries have approved Tamiflu worldwide. The U.S. spent more than $1.3 billion on its antiviral stockpile while Britain spent nearly $711 million on Tamiflu in its pandemic preparations, according to government documents cited by the Cochrane group and BMJ.

Public health agencies including the World Health Organization and the U.S. Centers for Disease Control and Prevention continue to recommend Tamiflu be used to treat flu and say it has proven helpful in treating patients with novel strains like bird flu.

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)

Latest News:

[display-posts category=”news,sports” wrapper=”ul” posts_per_page=”5″]

Top Trending: