SAN FRANCISCO — Most Americans who recently learned about a major breakdown in Internet security have been trying to protect themselves but a group nearly as large is unaware of the threat, according to a new survey.

The findings released Wednesday by the Pew Research Center provide a gauge on how Web surfers have been responding to a computer bug nicknamed “Heartbleed.” The flaw in a key piece of security technology used by more than 500,000 websites had been exposing online passwords and other sensitive data to potential theft for more than two years.

After word of the problem got out on April 7, affected websites began to close the Heartbleed loophole and security specialists recommended that Web surfers change their online passwords as a precaution.

That advice apparently resonated among those who read about in the extensive media coverage of the Heartbleed risks.

Passwords were changed or online accounts were closed by 39 percent of the Internet users in Pew’s telephone survey of 1,501 adults taken in the U.S. from April 23-27.

But 36 percent of the Internet users participating in the survey hadn’t heard about Heartbleed at all.

The almost equal division between people insulating themselves from Heartbleed and those unaware of the problem shows there is still a knowledge gap even as the Internet and mobile devices make it quicker and easier to find all kinds of information.

“There are some people who are pretty tuned in and are in an action frame of mind and then there others that don’t know about the news that is breaking,” said Lee Rainie, director of Pew Research’s Internet Project.

Better educated and more affluent Internet users tended to pay the most attention to Heartbleed. Roughly three-fourths of the Internet users aware of Heartbleed had college educations and lived in households with annual incomes of at least $75,000, according to Pew.

Although Heartbleed stories appeared for several days online and in print publications, most websites bitten by the bug didn’t send warnings to their users.

Major banking websites and other widely trafficked websites weren’t affected by Heartbleed, factors that Rainie said may have caused many people to pay less attention to the media’s coverage of the bug.

Only 19 percent of the survey respondents said they had heard “a lot” about Heartbleed. By comparison, 46 percent said they had heard “a lot” about the escalating tensions between Russia and Ukraine.

Just 6 percent of the survey participants believed Heartbleed led to their online information being ripped off.

More ‘Heartbleed’ [display-posts tag=”heartbleed” wrapper=”ul” posts_per_page=”4″]

(© Copyright 2014 The Associated Press. All Rights Reserved. This material may not be published, broadcast, rewritten or redistributed.)