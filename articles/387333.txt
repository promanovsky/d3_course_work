Morgan Stanley CEO James Gorman’s big bet on managing money for wealthier clients paid off again after the bank beat earnings expectations.

The firm said Thursday that second-quarter profit nearly doubled as revenue from its thriving wealth management division offset a widespread trading slump on Wall Street.

Wealth management revenues last quarter rose 5 percent, to $3.71 billion. The bank has seen an 11 percent spike in investor assets from last year, to just over $2 trillion at the end of June.

Morgan Stanley also completed its takeover of wealth management group Smith Barney from Citigroup last year.

In contrast, revenues from fixed income trading, which includes bonds and currencies, fell 14 percent to $1.06 billion, in line with the rest of the Wall Street.

Net income jumped 97 percent to $1.94 billion, or 94 cents a share. Excluding accounting adjustments for debt and a tax benefit, profit was 60 cents a share, topping the consensus estimate of 55 cents. Revenue rose 1.1 percent to $8.61 billion.

Morgan Stanley shares closed down slightly Thursday to $36.30.