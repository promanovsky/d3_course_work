Sony’s head of U.S. mobile operations Ravi Nookala has explained this week in an interview with CNET that Sony would not be one of Android Wear’s OEM partners, at least in the foreseeable future.

Instead Sony will be concentrating on developing its own Android-based operating system that currently powers both the older generation Sony SmartWatch and SmartWatch 2 devices.

“We’ve already invested time and resources on this platform, and we will continue in that direction,” Nookala told CNET. It also seems that Sony’s Android operating system may offer more autonomy to users and developers when compared to Google’s Android Wear operating system which was announced earlier this month.

Google’s Android Wear operating system is a slimmed down version of its smartphone Android operating system, that has been designed to provide users and developers with a method of displaying notifications from smartphone applications rather than run applications directly on wrist worn devices, unlike Sony’s Android OS. The LG G Watch, smartwatch will be the first device to debut running with Android Wear installed.

Source: Tech Crunch : CNET

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more