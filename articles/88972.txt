Speculation on who will fill David Letterman's slot after his retirement from CBS' Late Show has centered on a number of names, including Neil Patrick Harris.

Harris, who currently is starring in Broadway's Hedwig and the Angry Itch, responded to the speculation during an unrelated press conference with New York Sen. Charles E. Schumer and various Broadway stars on the pros of theater tax breaks.

PHOTOS: 17 Comedians and Hosts Who Might Land at 'Late Show' (Photos)

"I'm super-focused on Hedwig at the moment, so that hasn't even been a conversation at all," the How I Met Your Mother star replied when asked about the possibility of his taking over Late Show. "But I'm a big fan of CBS and Les [Moonves], so who knows?"

Asked if the late-night gig would be fun, Harris replied: "It would be an asinine amount of work."

Another reporter asked whether the show should relocate to California after Los Angeles mayor Eric Garcetti urged Moonves in a written letter to move locations. But Harris was interrupted by Schumer, who said: "I think we saw with NBC how great it is to have the shows be in New York, and that's why they're flocking back here...and doing everything we can to keep Letterman in New York."

Other names that the media has speculated on as possible replacements include Tina Fey, Stephen Colbert, Jay Leno, Conan O'Brien, Ellen DeGeneres, Chelsea Handler and Whoopi Goldberg, who shot down the rumor on her daytime talk show. "What do you mean? I've got a job here. I work for ABC," said Goldberg on The View.

Leno also weighed in on the rumors. When asked by Extra if Leno would ever "fill in" for Letterman, Leno replied: "There's nothing to fill in."

VIDEO: David Letterman to Retire From CBS' 'Late Show' in 2015

Harris recently finished his ninth and final season of How I Met Your Mother, which drew 12.9 million viewers, and hosted the Emmys twice.

Letterman's contract expires in 2015. He announced the news of his retirement on Thursday's Late Show.

"The man who owns this network, Leslie Moonves, he and I have had a relationship for years and years and years, and we have had this conversation in the past, and we agreed that we would work together on this circumstance and the timing of this circumstance. And I phoned him just before the program, and I said, 'Leslie, it's been great, you've been great, and the network has been great, but I'm retiring,' " Letterman told his audience.