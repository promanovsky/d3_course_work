Nicki Minaj suffered an apparent wardrobe malfunction on Sunday night's VMA Awards.

Minaj was wearing a small black zipped dress during her show-opening performance of "Bang Bang" with Ariana Grande and Jessie J. The dress appeared to unzip while she was onstage, and Minaj managed to keep it closed by holding it open during the entire performance.

She afterwards told reporters: "You all know I had a wardrobe malfunction." A source near Minaj noted that they were not surprised that the accident happened: "She could never get into that dress. During the dress rehearsal she did the number numerous times and could not make it into the dress zipped up even once. I've no idea why she didn't just decide to wear something else. She just didn't have the time to change from one outfit into another, there was no way."

The singer's performance rehearsals were plagued with problems, including an incident in which a boa constrictor slated to appear in the "Anaconda" performance bit one of her dancers, leading MTV to ban its appearance in Sunday night's show.

For comments and feedback contact: editorial@rttnews.com

Entertainment News