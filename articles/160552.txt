INTERNET agency Storyful has teamed up with Facebook to give a newsfeed to users of the world's biggest social network.

Storyful, which was founded by former 'Prime Time' presenter Mark Little, was bought by Rupert Murdoch's News Corporation for $25m (€18m) in December.

It said its new project, called FB Newswire, will be constantly updated with the best content from the worlds of news, weather, viral, entertainment, lifestyle, sports and celebrity.

Aine Kerr, Storyful's managing editor, said: "Our technology and verification process, coupled with Facebook's user base, platform and global reach, make for an exciting news venture."

Facebook users share an estimated 684,478 pieces of content every minute.

Irish Independent