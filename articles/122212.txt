Samsung doesn't provide adequate security protection with the fingerprint sensor on its new Galaxy S5 smartphone -- at least that's the contention of a group of researchers who were able to hack their way past the sensor.

In a video of the hack, a researcher from Security Research Labs demonstrated how he was able to bypass the fingerprint security by using a "wood glue spoof" made from a mold taken from a photo of a fingerprint smudge left on a smartphone screen. The hack itself employed the same technique used to hack past the fingerprint scanner in Apple's iPhone 5S last year.

But the S5 was given a thumbs-down by the researchers for one critical flaw. The fingerprint scanner allows for multiple incorrect attempts without requiring a password. So someone could potentially keep trying one fingerprint spoof after another until access is finally achieved.

The Galaxy S5's fingerprint scan can also be associated with certain secure apps and services. So once the initial scan gains entry to the phone, someone can open an app such as PayPal with no further security or identification required. As shown in the video, the person is able to log in to PayPal, giving him the ability to access the owner's account.

SRLabs is a Berlin-based security research and consulting think tank that has investigated mobile networks, SIM cards, payment terminals, and other systems for security issues.

"Despite being one of the premium phone's flagship features, Samsung's implementation of fingerprint authentication leaves much to be desired," the researcher in the video said. "The finger scanner feature in Samsung's Galaxy S5 raises additional security concerns to those already voiced about comparable implementations."

In response to SRLabs' findings, PayPal issued the following statement:

"While we take the findings from Security Research Labs very seriously, we are still confident that fingerprint authentication offers an easier and more secure way to pay on mobile devices than passwords or credit cards. PayPal never stores or even has access to your actual fingerprint with authentication on the Galaxy S5. The scan unlocks a secure cryptographic key that serves as a password replacement for the phone. We can simply deactivate the key from a lost or stolen device, and you can create a new one. PayPal also uses sophisticated fraud and risk management tools to try to prevent fraud before it happens. However, in the rare instances that it does, you are covered by our purchase protection policy."

CNET contacted Samsung for comment and will update the story with any further details.

This article originally appeared on CNET.