The U.S. Food and Drug Administration approved Cyramza to treat patients with advanced stages of stomach cancer.

The ramucirumab has been approved to treat people with gastroesophageal junction adenocarcinoma, a form of cancer located in the region where the esophagus joins the stomach.

"Although the rates of stomach cancer in the United States have decreased over the past 40 years, patients require new treatment options, particularly when they no longer respond to other therapies," said Richard Pazdur, M.D., director of the Office of Hematology and Oncology Products in the FDA's Center for Drug Evaluation and Research. "Cyramza is new treatment option that has demonstrated an ability to extend patients' lives and slow tumor growth."

The angiogenesis inhibitor blocks blood supply to tumors, preventing further growth. The new treatment is meant especially for patients who have cancer tumors that can't be removed through surgery or tumors that have started to spread after being treated with a fluoropyrimidine- or platinum-containing therapy.

Eli Lilly, the marketers of the drug proved the safety and effectiveness of Cyramza in a clinical trial conducted on 355 participants with unresectable or metastatic stomach or gastroesophageal junction cancer. Approximately 66 percent of the participants received Cyramza while the remaining participants were treated with a placebo.

Patients treated with Cyramza experienced a median overall survival of 5.2 months compared to 3.8 months in participants receiving placebo. Researchers also found that Cyramza-treated patients experienced a delay in tumor progression compared to the patients that were treated with a placebo.

A second clinical trial was conducted to reconfirm the safety of Cyramza. Researchers looked into how effective Cyramza plus paclitaxel (another cancer drug) was compared to paclitaxel alone. A significant improvement in overall survival was observed when patients were treated with Cyramza plus paclitaxel.

Side effects of the treatment include diarrhea and high blood pressure.

The National Cancer Institute estimates that approximately 22,000 Americans will be diagnosed with some form of stomach cancer this year, and almost 11,000 will die from it.

Last year, the FDA granted approval for Herceptin in combination with cisplatin and a fluoropyrimidine for the treatment of patients with HER2-overexpressing metastatic gastric or gastroesophageal (GE) junction adenocarcinoma who have not received prior treatment for metastatic disease.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.