Updated from 5:14 p.m. ET with comment on Citigroup from CSLA analyst Mike Mayo.

NEW YORK (TheStreet) -- The Federal Reserve Wednesday afternoon released the results of its annual Comprehensive Capital Analysis and Review (CCAR), with five of 30 banks having their 2014 capital plans rejected.

Citigroup (C) - Get ReportC and Zions Bancorporation (ZION) - Get ReportZION had their capital plans rejected by the Federal Reserve, along with three foreign-held U.S. bank holding companies: HSBC North America Holdings (a unit of HSBC HSBC), RBS Citizens Financial Group (held by Royal Bank of Scotland (RBS) - Get ReportRBS) and Santander Holdings USA, which is held by Banco Santander (SAN) - Get ReportSAN.

Citigroup's shares were down 5.6% in after-market trading, to $47.32.

It was no surprise to see Zions Bancorporation having its capital plan rejected when the Fed completed CCAR, in light of the bank's failure in in the first round of stress tests last week.

The Federal Reserve objected to the other four banks' capital plans, based on "qualitative concerns."

All five banks will be expected to submit revised capital plans to the regulator, and aside from Zions, could still deploy excess capital over the next year.

CCAR is the second part of the Fed's annual stress-test process for the largest U.S. banks. The first part of the tests -- the Dodd-Frank Act Stress Tests (DFAS) -- were completed last week, and gauged 30 banks' ability to remain well-capitalized, with minimum Tier 1 common equity ratios of at least 5.0%, through a nine-quarter "severely adverse" economic scenario.

This year's scenario assumes an increase in the U.S. unemployment of four percentage points, with the unemployment rate peaking at 11.25% in mid-2015. The scenario also includes a decline in real U.S. GDP of nearly 4.75% through the end of 2014, a 50% decline in equity prices and a 25% decline in home prices.

For banks considered global systemically important financial institutions (G-SIFIs), this year's scenario also includes "the instantaneous and unexpected default of the bank holding company's counterparty with the largest net stressed losses."

Another new element in this year's stress tests is a "Global Market Shock" component of the severely adverse economic scenario, which the Federal Reserve describes as "one-time, hypothetical shocks to a large set of risk factors."

All of the banks subject to DFAST passed the stress tests, with the exception of Zions Bancorporation which came through with a minimum Tier 1 common equity ratio of 3.6%, according to the corrected results released by the Federal Reserve Monday. The bank passing DFAST with the lowest minimum Tier 1 common equity ratio was Bank of America (BAC) - Get ReportBAC, at 5.9%.

CCAR incorporates stress-tested banks' annual plans to deploy capital through dividend increases, share buybacks and/or acquisitions to the same severely adverse scenario. CCAR covers capital deployment from the second quarter of 2014 through the first quarter of 2015.

""With each year we have seen broad improvement in the industry's ability to assess its capital needs under stress and continuing improvements to the risk-measurement and -management practices that support good capital planning," Federal Reserve Governor Daniel Tarullo said in the regulator's press release. "However, both the firms and supervisors have more work to do as we continue to raise expectations for the quality of risk management in the nation's largest banks."

This year's group of banks subject to DFAST and CCAR was expanded by 12 banks, including six foreign-held holding companies. Three of those had their capital plans rejected. Zions Bancorporation was among the regional banks added to this year's stress-test list.

Zions said in a press release Wednesday afternoon that its 2014 capital plan had included a common-equity raise of $400 million, which would have raised its minimum Tier 1 common equity ratio in the CCAR stress test to 4.5%, which would still be below the 5.0% required to pass DFAST. Zions will submit a revised capital plan by April 30, but the company didn't say whether or not it was planning for a larger common-equity raise.

Citigroup



In its report on CCAR results, the Federal Reserve said its objection to Citigroup's capital plan "reflects significantly heightened supervisory expectations for the largest and most complex BHCs in all aspects of capital planning."

Citigroup is unique among the largest U.S. banks in that it derives most of its revenue and earnings from outside North America.

The Fed described Citigroup's inability to properly stress-test its various business sufficiently for the regulator to approve its capital plan:

While Citigroup has made considerable progress in improving its general risk-management and control practices over the past several years, its 2014 capital plan reflected a number of deficiencies in its capital planning practices, including in some areas that had been previously identified by supervisors as requiring attention, but for which there was not sufficient improvement. Practices with specific deficiencies included Citigroup's ability to project revenue and losses under a stressful scenario for material parts of the firm's global operations, and its ability to develop scenarios for its internal stress testing that adequately reflect and stress its full range of business activities and exposures. Taken in isolation, each of the deficiencies would not have been deemed critical enough to warrant an objection, but, when viewed together, they raise sufficient concerns regarding the overall reliability of Citigroup's capital planning process to warrant an objection to the capital plan and require a resubmission.

Here's Citigroup's entire press release following the Federal Reserve's announcement:

New York - The Federal Reserve Board (Fed) today announced that it objected to the capital plan submitted by Citi as part of the 2014 Comprehensive Capital Analysis and Review (CCAR). The capital actions requested by Citi included a $6.4 billion common stock repurchase program through the first quarter of 2015 and an increase of Citi's quarterly common stock dividend to $0.05. Citi will be permitted to continue with its current capital actions through the first quarter of 2015. These include a $1.2 billion common stock repurchase program and a common stock dividend of $0.01 per share per quarter. These actions are subject to approval by Citi's Board of Directors in the normal course. Michael Corbat, Citi's Chief Executive Officer, said: "Needless to say, we are deeply disappointed by the Fed's decision regarding the additional capital actions we requested. The additional capital actions represented a modest level of capital return and still allowed Citi to exceed the required threshold on a quantitative basis. "We will continue to work closely with the Fed to better understand their concerns so that we can bring our capital planning process in line with their expectations and meet their standards on a qualitative basis as well. We have not yet made a decision as to when we will resubmit our plan. "We clearly are being challenged to meet the highest standards in the CCAR process. Despite whatever shortcomings the Fed saw in our capital planning process, we have made tremendous progress over the past several years in enhancing our capital position and Citi remains one of the best-capitalized financial institutions in the world. We will continue to work incredibly hard to serve our clients and generate the returns our shareholders expect and deserve," Mr. Corbat concluded.

The large U.S. banks subject to CCAR were expected to make their own announcements of plans for capital deployment late Wednesday or early Thursday.

Mayo

CLSA analyst Mike Mayo -- who has been a harsh critic of Citigroup over the years but curently rates the stock a "buy" -- wrote in a client note late Wednesday that the Fed's rejection of Citi's capital plan was a "shocker to us."

"Citi needs to make this defeat into victory by improving the pace of restructuring, including a possible breakup (maybe time to sell Banamex, as mentioned a year ago), enhancing governance and holding managers more accountable, especially after management's reassurance about financial processes."

Banamex stands for Banco Nacional de Mexico -- Citigroup's subsidiary bank in Mexico -- which last month discovered an accounts receivable fraud that led to a $400 million loss and forced the parent company to restate its fourth-quarter and 2013 financial results.

Mayo lowered his price target for Citigroup's shares to $58 from $60, while lowering his 2014 earnings estimate by a nickel to $4.55 a share and lowering his 2015 EPS estimate to $5.55 from $5.70.

Mayo aimed some harsh criticism at the Federal Reserve, referring to "the new world of Big Brother Banking," which has given regulators " a new level of regulatory discretion." The Fed's rejection of Citi's capital plan, after all, wasn't based on the company's level of capital.

"The Fed's decision makes it look like there is no level of capital that is sufficient whereby Citi could repurchase stock," Mayo wrote.

Other Banks' Capital-Deployment Announcements

Morgan Stanley (MS) - Get ReportMS announced it would buy back up to $1 billion in common shares from the second quarter of 2014 through the first quarter of 2015, and that it would raise its quarterly dividend on common shares to 10 cents from 5 cents, beginning with the dividend declared in the second quarter.

Bank of America said it would buy back up to $4 billion in common shares from the second quarter of 2014 through the first quarter of 2015, and that it would raise its quarterly dividend to 5 cents from a penny. Also on Wednesday, after the market close, Fannie Mae (FNMA) and Freddie Mac (FMCC) both announced large mortgage-backed securities settlements with Bank of America.

JPMorgan Chase JPM said it would repurchase up to $6.5 billion in common shares from the second quarter of 2014 through the first quarter of 2015, and would raise its quarterly dividend to 40 cents from 38 cents.

Goldman Sachs (GS) - Get ReportGS didn't make a specific announcement of how much excess capital it would deploy, but CEO Lloyd Blankfein in a statement said, "Our capital plan provides flexibility to manage our capital resources dynamically and return excess capital to our shareholders."

Wells Fargo WFC said it had received approval from the Federal Reserve to raise its quarterly dividend to 35 cents from 30 cents, and "a proposed increase in common stock repurchase activity for 2014 compared with 2013." The company during 2013 Wells Fargo bought back $5.1 billion in common shares, although the company's share count only declined slightly, because of stock issuance for employee compensation.

Bank of America Reaches $14.35B Settlement With Fannie and Freddie

CFPB Report Points to Payday Lending Clampdown

Follow @PhilipvanDoorn

Philip W. van Doorn is a member of TheStreet's banking and finance team, commenting on industry and regulatory trends. He previously served as the senior analyst for TheStreet.com Ratings, responsible for assigning financial strength ratings to banks and savings and loan institutions. Mr. van Doorn previously served as a loan operations officer at Riverside National Bank in Fort Pierce, Fla., and as a credit analyst at the Federal Home Loan Bank of New York, where he monitored banks in New York, New Jersey and Puerto Rico. Mr. van Doorn has additional experience in the mutual fund and computer software industries. He holds a bachelor of science in business administration from Long Island University.