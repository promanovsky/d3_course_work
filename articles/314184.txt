People genetically prone to Alzheimer’s who went to college, worked in complex fields and stayed engaged intellectually held off the disease almost a decade longer than others, a study found.

Lifelong intellectual activities such as playing music or reading kept the mind fit as people aged and also delayed Alzheimer’s by years for those at risk of the disease who weren’t college educated or worked at challenging jobs, the researchers said in the study published today in JAMA Neurology.

More than 5 million Americans have Alzheimer’s disease, the most common form of dementia, and the number is expected to triple by 2050, according to the Alzheimer’s Association. Today’s findings show that intellectual enrichment pursued over a lifetime may help reduce the number of people who will develop the disease, the authors said.

“Keeping your brain mentally stimulated is a lifelong enterprise,” David Knopman, a study author and a professor of neurology at the Mayo Clinic in Rochester, Minnesota, said in a telephone interview. “If one can remain intellectually active and stimulated throughout one’s lifespan, that’s protective against late-life dementia. Staying mentally active is definitely good for your brain.”

Currently there are no effective treatments for Alzheimer’s. A report by the Alzheimer’s Association projected that any treatment that could delay the onset of Alzheimer’s by five years would reduce the expected number of patients with the disease in the U.S. by about 43 percent by 2050.

Reducing Dementia

Knopman said providing mid-to late-life cognitive activities across the population may not be as beneficial as an actual treatment for Alzheimer’s and dementia. Still if new cases of dementia were reduced even by a fraction it would be a “great success.”

Researchers studied 1,995 people ages 70 to 89 without dementia who lived in Minnesota. They looked at their education and occupation and their mid-to late-life cognitive activity.

The more education a person had and the more complex the job, the higher a person’s memory was as they got older, the researchers found. The study also showed that those who engaged in higher levels of brain activities from at least age 40, also had greater memory levels as they aged.

Among those with an average education and job complexity, brain stimulating pastimes can delay onset of dementia by about 7.3 years compared with people with low levels of mental stimulation in aging. For those who carry the ApoE4 gene, a risk factor for Alzheimer’s found in about 25 percent of the U.S. population, brain stimulating pastimes can delay the onset of the disease by about 3.5 years.

The researchers didn’t predict the delay of Alzheimer’s among those in the highly educated, intellectually active group who aren’t genetic carriers because it exceeded 10 years.

“The greater the cognitive reserve that people have the more delayed the onset of dementia is,” Knopman said.