Call it a non-fix: According to the Wall Street Journal, FCC Chairman Tom Wheeler has tweaked the language of his proposed rules to allow content providers to pay for faster delivery of their content across an ISPs network.

He has not recanted that proposal. Instead, according to the Journal, “the new language by FCC Chairman Tom Wheeler to be circulated as early as Monday is an attempt to address criticism of his proposal unveiled last month that would ban broadband providers from blocking or slowing down websites,” but would still let companies that are content-intensive “pay [ISPs] for faster delivery of Web content to customers.”

Doesn’t that feel precisely the same as the plan before? Yes, but, this time, the Journal continues, we’re going to have “language that would make clear that the FCC will scrutinize the deals to make sure that the broadband providers don’t unfairly put nonpaying companies’ content at a disadvantage.” So, the paid advantage would be “fair.” Defining that isn’t going to be easy.



This is the same stuff as before, written down with a different color pen. Wheeler has made comments before that indicated he won’t allow for the Internet to become bifurcated to the disadvantage of those who don’t pay. But if advantage is for sale, how can that not be the case?

If I pay for an advantage, and you deem is fair, is it?

The FCC will take comment on the proposal. If you are opposed to the concept, making more noise, and not less, has thus far had impact and likely will continue to do so. Just a thought.