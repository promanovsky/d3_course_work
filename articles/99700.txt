CBS announced yesterday that Stephen Colbert, the host, writer and executive producer of the Emmy and Peabody Award-winning "The Colbert Report," will succeed David Letterman as the host of THE LATE SHOW, effective when Mr. Letterman retires from the broadcast.

Letterman, the legendary, critically acclaimed host of the CBS late night series for 21 years, announced his retirement on his April 3 broadcast. Colbert's premiere date as host of THE LATE SHOW will be announced after Mr. Lettermen determines a timetable for his final broadcasts in 2015.

Since the news was announced, many have speculated over whether Colbert will hold on to the buffonish signature character he portrays on his nightly Comedy Central show. Commenting through his publicist Thursday morning, Colbert revealed that he plans to drop his alter ego when he takes over the reins of Late Show.

"We'll all get to find out how much of him was me," joked the Emmy winning host. "I'm looking forward to it."

In a statement released yesterday, Colbert shared his excitement over his new assignment. "Simply being a guest on David Letterman's show has been a highlight of my career. I never dreamed that I would follow in his footsteps, though everyone in late night follows Dave's lead. I'm thrilled and grateful that CBS chose me."

He added, "Now, if you'll excuse me, I have to go grind a gap in my front teeth."

Source: New York Post