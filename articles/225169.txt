By Rachael Rettner, Senior Writer

Published: 05/19/2014 03:23 PM EDT on LiveScience

Pancreatic cancer is projected to become the second most common cause of cancer-related death in the United States in 2030, overtaking deaths from breast and colon cancers, according to new research.

Currently, the top three causes of cancer-related death in the United States are lung, colorectal and breast cancers. Pancreatic canceris fourth, followed by prostate and liver cancers. In 2030, lung cancer will remain the top killer, but pancreatic cancer will move to second, followed by liver cancer and colorectal cancer, according to the report from the Pancreatic Cancer Action Network (PCAN), a charity organization that advocates for pancreatic cancer research.

The reason for the increases in deaths from pancreatic and liver cancers is partly due to changes in demographics, including an increase in the number of people ages 65 and older, who are at greater risk for cancer in general, said study researcher Lynn Matrisian, vice president of scientific and medical affairs at PCAN. In addition, some minority groups, such as African Americans, are at greater risk for pancreatic cancer than whites, so changes in minority group populations also affect the risk of the disease. [10 Do's and Don'ts to Reduce Your Risk of Cancer]

And although increased screening has led to improvements in the detection of some cancers, such as colorectal cancer,the same is not true for pancreatic and liver cancers, Matrisian said.

"We've been able to turn the tide on other cancers" because of investments in research to better understand them, Matrisian said. "It's now time to realize that we need to start turning the tide on pancreatic cancer with the same kind of tools," Matrisian said at a news conference today (May 19).

Pancreatic cancer has a very low survival rate: 94 percent of patients diagnosed with the disease die within five years, according to the PCAN. That's partly because the disease has few warning signs and is usually not diagnosed until the late stages. There is currently no screening test for pancreatic cancer.

To come up with the new estimates, the researchers looked at expected changes in demographics in the U.S. over the next two decades, as well as changes in the percentage of new cancer cases and cancer death rates that were seen between 2006 and 2010.

In 2030, there are projected to be 156,000 deaths from lung cancer, 63,000 deaths from pancreatic cancer, 51,000 deaths from liver cancer and 47,000 deaths from colorectal cancer, among men and women combined in the United States, the study found.

However, breast, prostate and lung cancers are projected to remain the top three cancer diagnoses in 2030, in terms of total number of cases. Thyroid cancer is projected to overtake colorectal cancer in the fourth spot, the researchers said, pointing to the increases in thyroid-cancer diagnoses in recent years.

The study, funded by PCAN, will be published in the June 1 issue of the journal Cancer Research.

It's important to note that the researchers assumed that changes in cancer incidence and death rates that took place between 2006 and 2010 would continue to occur for the next 20 years.

"This is unlikely to hold true," said Ahmedin Jemal, vice president of surveillance research at the American Cancer Society, who was not involved in the study. "It is very, very difficult to predict the future."

For example, if new ways to detect or treat pancreatic or liver cancers were discovered, that could change the death rates, Jemal said.

Matrisian agreed, and said the study researchers would like to see these projections change because of increased efforts to understand these diseases.

Follow Rachael Rettner @RachaelRettner. Follow Live Science @livescience, Facebook & Google+. Original article on Live Science.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>