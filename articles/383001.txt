Organic foods are proven to have more nutritional benefits than those conventionally raised variants, shows a recent British study.

The study, said to be the largest of its kind, claims the superior benefits of organic harvest can be attributed to the process of producing the crops. Through this, organic produce has fewer residues from regular pesticide use and more antioxidants.

"This study is telling a powerful story of how organic plant-based foods are nutritionally superior and deliver bona fide health benefits," Washington State University researcher Charles Benbrook says in a statement.

The researchers examined 343 peer-reviewed publications that compared the nutritional quality and safety of organic produce against traditional plant-based food, which include grains, vegetables and fruits. Majority of these publications studied the crops developed in the same area and similar soils. They then made use of complex meta-analysis procedures to measure the differences between non-organic and organic harvests.

The study indicates that a plant grown in a traditional method usually will have access to synthetic nitrogen in high levels and will use the extra resources into creating starches and sugars. Because of this, the harvested part of the plant oftentimes will have nutrients—include good antioxidants—in lower concentrations.

Since organic foods and crops have no synthetic chemical pesticides, these are more likely to produce polyphenols and phenols that help fight off pest attacks and other related injuries. For humans, it helps avert health concerns, such as stroke, some cancers and coronary heart disease, which are triggered by oxidative damage.

The study also indicates that traditionally grown foods have three to four times chance of pesticide residues than organic foods because organic farmers don’t apply synthetic pesticides. Non-organic crops are also found to have approximately twice of cadmium content than organic crops. Cadmium is a lethal heavy metal toxin.

In general, organic foods have 18 to 69 percent higher concentrations of antioxidants, so people who go for everything organic would receive 20 to 40 percent more of antioxidants—without the increase in calorie intake.

“In conclusion, organic crops, on average, have higher concentrations of antioxidants, lower concentrations of Cd and a lower incidence of pesticide residues than the non-organic comparators across regions and production seasons,” reads the study.

Newcastle University scientists in the UK headed the study. Benbrook is the sole American co-author in the study whose role was to help design the scientific study, co-write the paper and review scientific texts, specifically on North and South American studies.

The British Journal of Nutrition published the study titled Higher antioxidant and lower cadmium concentrations and lower incidence of pesticide residues in organically grown crops: a systematic literature review and meta-analyses.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.