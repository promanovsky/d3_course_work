In a report published Thursday, Morgan Stanley analyst John Egbert reiterated an Equal-Weight rating on Netflix (NASDAQ: NFLX).

In the report, Morgan Stanley noted, “Netflix continues to innovate on its consumer SVOD experience and we believe it has the opportunity to reach over 100MM subscribers globally. We believe much of its growth will come from international markets going forward, and believe the non-US opportunity will grow as broadband infrastructure improves.”

Netflix closed on Wednesday at $331.41.