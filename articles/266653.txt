Almost a third of the world is now fat, and no country has been able to curb obesity rates in the last three decades, according to a new global analysis.

Researchers found more than 2 billion people worldwide are now overweight or obese. The highest rates were in the Middle East and North Africa, where nearly 60 percent of men and 65 percent of women are heavy. The U.S. has about 13 percent of the world's fat population, a greater percentage than any other country. China and India combined have about 15 percent.

"It's pretty grim," said Christopher Murray of the Institute for Health Metrics and Evaluation at the University of Washington, who led the study. He and colleagues reviewed more than 1,700 studies covering 188 countries from 1980 to 2013. "When we realized that not a single country has had a significant decline in obesity, that tells you how hard a challenge this is."

Murray said there was a strong link between income and obesity; as people get richer, their waistlines also tend to start bulging. He said scientists have noticed accompanying spikes in diabetes and that rates of cancers linked to weight, like pancreatic cancer, are also rising.

The new report was paid for by the Bill & Melinda Gates Foundation and published online Thursday in the journal, Lancet.

Last week, the World Health Organization established a high-level commission tasked with ending childhood obesity.

"Our children are getting fatter," Dr. Margaret Chan, WHO's director-general, said bluntly during a speech at the agency's annual meeting in Geneva. "Parts of the world are quite literally eating themselves to death." Earlier this year, WHO said that no more than 5 percent of your daily calories should come from sugar.

"Modernization has not been good for health," said Syed Shah, an obesity expert at United Arab Emirates University, who found obesity rates have jumped five times in the last 20 years even in a handful of remote Himalayan villages in Pakistan. His research was presented this week at a conference in Bulgaria. "Years ago, people had to walk for hours if they wanted to make a phone call," he said. "Now everyone has a cellphone."

Shah also said the villagers no longer have to rely on their own farms for food.

"There are roads for (companies) to bring in their processed foods and the people don't have to slaughter their own animals for meat and oil," he said. "No one knew about Coke and Pepsi 20 years ago. Now it's everywhere."

In Britain, the independent health watchdog issued new advice Wednesday recommending that heavy people be sent to free weight-loss classes to drop about 3 percent of their weight. It reasoned that losing just a few pounds improves health and is more realistic. About two in three adults in the U.K. are overweight, making it the fattest country in Western Europe.

"This is not something where you can just wake up one morning and say, `I am going to lose 10 pounds,'" said Mike Kelly, the agency's public health director, in a statement. "It takes resolve and it takes encouragement."