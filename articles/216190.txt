Can viruses such as measles or polio be the next cure for cancer?

The answer is maybe.

Two recent studies from the Mayo Clinic and Duke University Medical Center detailed how researchers utilized the measles virus and the polio virus to destroy cancer cells.

The excellent news is that in these very small patient trials, there were some significant successes. One patient went into total remission from multiple myeloma after receiving a strain of the measles virus, and another patient, who suffered from brain cancer, was treated with the polio virus and seems to be in remission as well.

So how is this all possible?

This research is all part of a new medical field of oncolytic virotherapy. The “proof of concept” studies stem from many years of animal research, analyzing how viruses can penetrate certain types of cancer cells. A typical cancer cell moves very fast and replicates very rapidly. Therefore, some viruses have an affinity to get into these cells and use them as incubators, so the viruses can multiply at a fast rate, as well. But once these viruses are attached, the cancer cells essentially explode and release the virus into the body.

With this mechanism in mind, doctors utilized very large doses of the measles virus and polio virus on small number of cancer patients. Just as expected, the viral strains penetrated and destroyed the tumor cells, ultimately killing the cancer. After the therapy, the patient’s own immune system did the rest by ingesting the leftover cellular debris from both the cancer cells and the engineered viruses.

Though this is remarkable, let’s also look at the other side of the coin. These are extremely delicate trials, which must be conducted under very strict protocols, and we don’t know yet whether this will be applicable to all types of cancers.

One of the limitations of using viruses to kill cancer cells is that the patient should be void of immunity, as too strong of an immunity towards the measles virus will limit its effectiveness of multiplying in the cancer cells. However, many cancer patients are already immunosuppressed, so this may only be a partial problem.

The other potential complication revolves around the toxic effects of being exposed to enormous amounts of live viruse. As was the case in these small studies, one patient developed nausea, vomiting and very high temperatures. Additionally, viral toxicity could lead to permanent damage of our immune system, creating other problems such as guillain barre syndrome – which causes the body’s immune system to attack the nerves.

The final word here is that these studies shine a bright light on the future of cancer therapy. I think that all of the scientists who participated in these historical trials should be congratulated, and I hope that these cases lead to larger human trials and one day, clinical treatments.