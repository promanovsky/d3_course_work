In spite of the dovish tone from Janet Yellen at the press conference last week, the short term rates markets are betting that the Fed will become even more dovish in the months to come. Fed funds rates trajectory implied by the futures markets is significantly below the median projection by the FOMC.

Fed funds vs the Fed

Fed funds and futures traders are playing the carry game - going long futures a couple of years out and riding them down the curve. The strategy had worked in the past. But, is the Fed really going to turn more dovish? Barclays researchers believe that just the opposite will occur and the trajectory of rate hikes will steepen.

Barclays: - The “dots” also showed a faster pace for the hiking cycle. Notably, this occurred despite the fact that the revisions to the Fed’s inflation and UR [unemployment rate] projections were not very aggressive. We believe revisions are likely to continue in the same direction, i.e., lower UR and higher inflation; in turn, the path for the funds rate implied by the “dots” should continue to steepen.

The spread between the market and the FOMC is now close to extreme levels. The Fed's projected rate "dots" have been rising, while the futures traders continue to ignore it.

Fed vs market

At some point however this is going to come to a head.

Barclays: - ... on balance, the Fed’s embedded economic projections are not too aggressive. While Fed Chair Yellen’s dovish stance may be playing a role in keeping rate hike expectations muted for now, if inflation surprises to the upside, Fedspeak can take a far less dovish tone (note the abrupt change by the BoE).