Bob Hastings, known to McHale's Navy fans as Lt. Elroy Carpenter, died Monday, June 30, of prostate cancer. He was 89.

His granddaughter Allison Knowles confirmed his death to the Associated Press on July 2, noting that he was at home in Burbank, Calif., at the time of his passing.

Hastings' role in the 1960s sitcom McHale's Navy was his most famous, but he also appeared on Dennis The Menace, The Munsters, Green Acres, and All in the Family (as bar owner Kelsey). In the 1980s, he had small parts in The Dukes of Hazzard, The Greatest American Hero, Murder, She Wrote, and Remington Steele.

More recently, he provided the voice of Commissioner James Gordon in Batman: The Animated Series and The New Batman Adventures. He also did some voiceover work for video games, including Batman: Mystery of the Batwoman and Mafia II.