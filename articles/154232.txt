It’s HBO Everywhere.

HBO boss Richard Plepler unlocked the premium channel’s extensive library of top-shelf content for Jeff Bezos’ Amazon, making it available online for the first time for those who don’t have a cable subscription.

The exclusive deal, estimated to be worth as much as $300 million over next three years, according to industry sources, is aimed at widening HBO’s audience and creating a new revenue stream to pay for expensive original programming.

The deal covers mostly older shows — episodes won’t become available until three years after their initial airing — like “The Sopranos,” “Six Feet Under” and “The Wire.” HBO’s biggest hit, “Game of Thrones,” isn’t part of the deal.

Still, Amazon is betting the move will boost subscribers for its instant video service, which is included with its $99-a-year Prime membership. Amazon just raised the price from $79.

HBO’s coveted content has long been kept beyond reach of rival streaming services, and was only available via the Internet to subscribers of HBO Go, its online offering.

Netflix, Amazon’s biggest streaming rival, took a hit on the news, sending the stock fell 5.2 percent to $353.50.

The Amazon deal also raised eyebrows among cable and satellite-TV distributors who benefit from their mutual relationship with HBO.

“HBO sells cable and cable sells HBO. It’s a pretty serious departure from that,” said one cable industry observer.

Dan Cryan, an analyst at consultancy IHS said: “HBO is trying to walk a tightrope between getting more money for its older content and not agitating cable partners on the other.”