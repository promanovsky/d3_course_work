Yen weaker after BoJ

Investing.com - The Japanese yen weakened against the dollar after the Bank of Japan on Friday kept ultra-loose monetary in place at its latest board meeting.

"The Bank of Japan will conduct money market operations so that the monetary base will increase at an annual pace of about ¥60 to ¥70 trillion," the BoJ said.

On the current economic climate, the BoJ repeated its overall assessment, saying,"Japan's economy has continued to recover moderately as a trend, although the subsequent decline in demand following the front-loaded increase prior to the consumption tax hike has been observed."

BoJ and government policymakers have said the drag from the April sales tax hike to 8% from 5% appears to be easing. The latest government surveys have also shown that consumer sentiment rebounded in May.

Japan industrial production and capacity utilization is due at 1330 Tokyo, (0430 GMT) for April. The Industrial Production number came in at minus 2.5% in the preliminary reading, capacity utilization came in at a gain of 0.4% in March.

traded at 101.79, up 0.08%, after the BoJ announcement.

In China comes retail sales and industrial production data for May at 1330 Beijing (0530 GMT). Retail sales rose 11.9% year-on-year in April and are expected to have accelerated to 12.1%. Industrial production, which came in at a gain of 8.7% year-on-year in April, is expected at a rise of 8.8%.

traded at 0.9414, down 0.13%, ahead of the data closely followed by Australia as the top export destination for commodities such as iron ore.

Overnight, lukewarm U.S. retail sales data weakened the dollar against the euroy by reminding investors that recovery still battles headwinds and the timing of Federal Reserve interest-rate hikes still remains up in the air.

The Commerce Department reported earlier that U.S. retail sales rose 0.3% in May, missing expectations for a 0.6% gain. However, retail sales for April were revised up to a 0.5% gain from a previously reported increase of 0.1%.

Core retail sales, which exclude automobile sales, eased up 0.1% in May, disappointing forecasts for a 0.2% increase. Core sales in April were revised up to 0.4% from a previously reported flat reading.

Separately, the Labor Department reported that the number of individuals filing for initial jobless benefits in the week ending June 7 increased by 4,000 to 317,000, confounding expectations for a decline of 3,000, though markets largely shrugged off the report.

The data kept markets rethinking how much time will elapse after the Federal Reserve concludes stimulus programs and before the U.S. central bank hikes interest rates.

Meanwhile in Europe, data released earlier revealed that euro zone industrial production rose 0.8% in April, according to Eurostat, beating forecasts for a 0.4% increase.

Still, the single currency battled pressures of its own after the ECB cut all its main rates to record lows last Thursday and imposed negative deposit rates on commercial lenders for the first time in an effort to stave off the deflationary risks in the euro zone.

On Friday, the U.S. is to round up the week with data on producer price inflation and preliminary data on consumer sentiment from the University of Michigan.