Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

It's that time of year again, and stars of the biggest TV shows of the moment are battling it out for a prize - wearing their most stunning and jaw-dropping outfits.

The BAFTA TV Awards kicks off tonight in London for another year, as the likes of Julie Walters, Naomi Campbell, Jamie Dornan and more arrive in all their glamour for the star-studded red carpet.

Hosted by comedian Graham Norton, shows from Ant and Dec's Saturday Night Takeaway, to new hit Gogglebox are all up for gongs - and the leading stars are all there to accept them.

Now, we understand not all of you will be able to get ring-side seats so to speak, so we have brought it right to you, in the comfort of your own homes.

So grab a cuppa, sit back and enjoy as the biggest names in television show us just how beautiful they all are.