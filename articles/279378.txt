HOW TO TRAIN YOUR DRAGON 2: 4 STARS

To paraphrase the tagline of the original “Superman” movie for “How to Train Your Dragon 2,” “You will believe a dragon can fly.”

The story begins five years after the original 2010 movie. Hiccup (Jay Baruchel) is now an older and wiser teenager and master Dragon Whisperer. Through his efforts the citizens of his hometown, the Viking village of Berk, no longer fear dragons. In fact, the fire breathers have become part of the fabric of life. They have dragon races—that resemble Harry Potter’s Quidditch matches—and live a peaceful life co-existing with their serpentine pals.

Peace is threatened when Dragon Trappers, lead by the evil Drago Bludvist (Djimon Hounsou), set their eye on the domesticated dragons of Berk. To avoid a war Hiccup and his girlfriend Astrid (America Ferrera) must change Drago’s mind about enslaving dragons.

The follow-up to “How to Train Your Dragon” half-a-billion-dollar grossing coming-of-age story is more of an action/adventure movie than the original. It begins with a stunning aerobatics sequence that shows Hiccup and his trusty sidekick Toothless soaring through the air doing barrel rolls, loops and stunts usually only seen at airshows. The slick and sassy scene sets the tone for the rest of the movie. It’s a wild ride and one that reinvents the franchise.

Director Dean DeBlois has taken some chances with the story, deepening and darkening the tone with subplots about family relationships, prejudice and sacrifice. Some of the imagery is intense—the “alpha” dragons look like they sprung from the mind of H.P. Lovecraft—and may be a bit traumatic for toddlers, but should be fine for kids 6 and up.

It’s not all sturm and drang, however. Baruchel brings the fun with his expressive voice and the script is gently humorous. The focus is firmly on the action/adventure aspects of the story, but there are laughs along the way for ids and adults.

Without slavishly aping the original it thematically expands the universe, building on ideas established in the movie that audiences first fell in love with. In other words, it’s a sequel, with recognizable characters and situations, but also works as a stand-alone film.

Most of all it’s about the on screen imagery. Inventive sequences—it “rains” fish at feeding time in the dragon sanctuary for instance—and beautiful animation carry the day.

“How to Train Your Dragon 2” is high on spectacle and never wastes an opportunity to entertain the eye and up the wonder factor, but it’s not just shock and awe. An emotional subplot regarding family adds some weight to the fantasy elements of the story.

22 JUMP STREET: 3 ½ STARS

Jonah Hill and Channing Tatum are producers on “22 Jump Street,” which, I guess, explains the large number of jokes about how much everything cost. At one point Hill actually says, “It’s way more expensive for no reason at all.”

I don’t know how much the movie cost to make, but the self-aware jokes did make me laugh even though “22” is essentially a remake of the first film, with a few more Laurel and Hardy slapstick gags and amped up explosions.

The “21 Jump Street” high school undercover cops Schmidt and Jenko (Hill and Tatum) are back, but this time they’re narco cops. That is until they botch an investigation into drug lord Ghost’s (Peter Stormare) operation. Their failure gets them demoted back to the 22 Jump Street (they moved across the road) program.

Jump Street’s Captain Dickson (Ice Cube) sends them undercover as unlikely brothers Brad and Doug McQuaid, to college to arrest the supplier of a drug named WHYPHY (WiFi). The bumbling, but self-confident duo infiltrates the college, but campus life—frat house parties, football and girls—threaten to blow apart their partnership. “Maybe we should investigate other people,” says Jenko, “sow our cop oats.”

“22 Jump Street’s” end credit sequence, which maps out the next sequels from number 3 to installment 43—they go to Beauty and Magic School among other places of higher learning— is probably the funniest part of the movie. Co-directors Phil Lord and Christopher Miller—they also made “The Lego Movie”—expertly parody Hollywood’s obsession with grinding a good thing into the ground and grab a few laughs while they do it.

The stuff that comes before is amiable, relying on the Mutt and Jeff chemistry of the neurotic Hill and all-American Tatum for laughs. It’s boisterous and aims to please, but best of all are the self-referential jokes. By clowning around about the difficulty in making the sequel better than the original they’re winking at the audience, acknowledging that this is basically a spoof of Hollywood sequels. It’s meta and kind of brilliant.

It isn’t, however, a laugh a minute. Ill-timed jokes about Maya Angelou and Tracy Morgan are sore thumbs, while the bromance between Schmidt and Jenko is played out until it begins to feel like the punch line to a bad, politically incorrect gag.

Better are Tatum’s malapropisms. The dim-witted cop says, "I thought we had Cate Blanchett on this assignment," when he means "carte blanche," and confuses “anal” and “annal.” They’re easy jokes, but Channing milks laughs out of them.

There will likely be a “23 Jump Street”—the film shows us an under-construction condo at that address—which will hopefully have the same subversive sense of fun, but more actual jokes.

THE DOUBLE: 3 STARS

“The Double” plays like a movie made by the love child of David Lynch and Terry Gilliam. Based on Fyodor Dostoevsky novella about a man who finds his life being usurped by his doppelgänger, it is a quietly surreal story about the existential misfortune of a man (Jesse Eisenberg) with no sense of himself.

Eisenberg is Simon, an insecure twenty-something trying to make a name for himself, personally and professionally, to no avail. His boss (Wallace Shawn) ignores his ideas and even his mother isn’t a fan. He’s in love with co-worker Hannah (Mia Wasikowska), who lives in an apartment across the street from him, but like everyone else Hannah looks right through Simon.

“I have all these things that I want to say to her,” he says, “like how I can tell she's a lonely person, even if other people can't. Cause I know what it feels like to be lost and lonely and invisible.”

Everything changes when James (Eisenberg again) is hired at work. Physically he’s Simon’s doppelgänger, an exact match, but personality-wise he a polar opposite. Confident and charismatic, he excels at work and worst of all, Hannah wants to date him.

In front of the camera “The Double” writer-director Richard Ayoade is best known for playing computer nerd Maurice Moss on the much-loved British sitcom “The IT Crowd.” Behind the camera his work takes a much more darkly comedic approach. His first film, “Submarine,” was an edgy coming-of-age story that earned him a BAFTA nomination for Outstanding Debut by a British Writer, Director or Producer.

“The Double” strays into even stranger territory. Imagine “The Nutty Professor’s” Professor Julius Kelp / Buddy Love filtered through Dostoevsky’s “mystery of spiritual existence.” Ayoade creates a personal dystopia, inhabited by Simon, Hannah and James; a stylized study of paranoia with a few laughs thrown in. It’s an unabashedly weird movie that lets its freak flag fly.

This is Eisenberg’s film. He and Michael Cera (who tread on similar dual character territory in 2009's “Youth in Revolt”) have made careers playing up the socially awkward nature of their characters, so half of “The Social Network” actor’s performance is no surprise. His work as Simon is something we’ve seen before from him, but his take on James is fresh, accomplished with shifts in body language. He effectively plays two characters in one movie.

In the end “The Double” stands as a unique movie, rich in Orwellian details and with good performances, but marred by a difficult, confusing story that may alienate less adventurous viewers.