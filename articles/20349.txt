Are Saturated Fats as Bad as We Have Been Led to Believe?

A US heart researcher looks set to inflame an argument over saturated fats. It has long been a health mantra that too much saturated fat like cheese and butter contributes to higher cholesterol and an increased risk of heart disease. But an editorial published in Open Heart suggests that saturated fats aren’t as bad as we have been led to believe.

Author James DiNicolantonio, a cardiovascular research scientist at Saint Luke’s Mid America Heart Institute, argues that the association between the high proportion of fat in total consumed calories and an increased risk of degenerative heart disease was based on selective data from the 1950s that overstated the risk. And the recommendations that it led to – that we lower our intake of saturated fat and cholesterol and increase carbohydrate – has given rise to the surging obesity.

The association between fat calories and heart disease risk was first made by Ancel Keys in the 1950s who first suggested a correlation between between cholesterol levels and cardiovascular disease (CVD), which he later published in his Seven Countries Study. DiNicolantonio argues that another 16 countries were excluded from the data that would have made the association less clear and led to different dietary advice.

“It was believed that since fat is the most ‘calorie-dense’ of the macro-nutrients, a reduction in its consumption would lead to a reduction in calories and a subsequent decrease in the incidence of obesity, as well as diabetes and the metabolic syndrome,” he said. But turning to carbohydrates such as sugar and corn syrup has led to a parallel increase in diabetes and obesity in the US.

Instead there’s “a strong argument that the increase in the consumption of refined carbohydrates was the causative dietary factor for diabetes and the obesity epidemic in the US,” he said.

The article concludes that while diets low in saturated fat can lower “bad” LDL cholesterol, switching to carbohydrates may increase another type of LDL cholesterol. In a study that compared two low-calorie diets, one low-fat and one low-carb, the latter showed better results. Overall, he argued, no large observational studies have shown that low fat diets cut heart disease risk.

Changing Fat

Our view of fats has changed over time and rather than one homogeneous group, they fit into a number of categories, with some thought to be good and some bad. Unsaturated (less calorific compared to saturated fat), polyunsaturates (found in nuts, seeds and fish) and monounsaturates (found in red meat, olives, avocados) have a good reputation when it comes to lowering cholesterol and heart disease risk, while saturated fats (meat, cheese) and artificial trans fats such as hydrogenated vegetable oil, aren’t so good.

While trans fats are still very much seen as bad, troubles over the established belief about saturated fats and heart disease risk surfaced in 2010 after the authors of a meta-analysis of 21 studies and nearly 350,000 subjects concluded there was “no significant evidence” that dietary saturated fat was associated with an increased risk of coronary heart disease or cardiovascular disease.

Last October, British cardiologist Aseem Malhotra published an article in the BMJ titled “Saturated fat is not the major issue”. In it he called for a re-evaluation of how we view saturated fat’s role in heart disease. He said that though there was universal agreement over trans fats saturated fat was unnecessarily demonised.

“Low-fat diets have paradoxically made people more obese because people are consuming more things like sugar that are typically seen as low calorie,” he said.

“One of the problems is that there’s misinformation among a lot of people … Now we’re learning that it doesn’t work: rocketing obesity and type 2 diabetes. We need people to go back to eating normal food, which includes eating saturated fats like cheese.”

He said “normal” meant avoiding things marketed as low fat and low in cholesterol and said the Mediterranean diet would be the ideal; rich in olive oil, nuts, fruit and vegetables and low in refined carbohydrates. Saturated fat was part of a healthy diet, he said, but not from fast food. “If you have a diet that has saturated fat in it and non-processed foods, then effect is nominal and maybe even slightly beneficial.”

He added: “The message going out has to be the right message, clearly the reality is that it hasn’t worked.”

No Debate

While Brian Ratcliffe, professor of nutrition at Robert Gordon University in Scotland, praised DiNicolantonio for “a welcome addition to the debate” that challenged “dietary dogma”, other experts warned that the article was only likely to lead to confusion.

David Sullivan, clinical associate professor at Sydney University, said: “This article, and others like it, slip between non-identical terms to suit their argument. It switches between total and saturated fat, as well as total and LDL cholesterol. Likewise, it picks and chooses endpoints to suit – so-called ‘surrogates’ like blood test results, weight, CVD, cancer, and total mortality.”

Sullivan said that there were some studies that did deviate from what was expected, but that this was “understandable in the complex field of nutrition.” He said the 2010 meta-analysis (supported by the National Dairy Council) also found that evidence from studies was consistent in finding that the risk of coronary heart disease was reduced when saturated fat was replaced with polyunsaturated fat. In other words saturated fat many not increase your risk (as the study found) but substituting with a better fat could lower it.

Sullivan said a Mediterranean style diet recommended by DiNicolantonio was “most definitely a low saturated fat diet.”

Tom Sanders, head of diabetes and nutritional sciences in King’s College London’s School of Medicine, said: “This article rubbishes the relationship with saturated fat and CVD, misrepresents the scientific evidence and then goes on to put the blame on sugar.”

“It is beyond reasonable doubt that elevated LDL cholesterol is a major determinant of risk factor for cardiovascular disease. The saturated fatty acids palmitic, myristic and lauric acids raise LDL cholesterol in increasing order in meta-analysis human experimental studies. Sugar intake does not affect LDL-cholesterol or blood pressure.”

Mediterranean Diet

So what are we to believe? The Mediterranean diet(ironically, developed by Keys), healthy (but not low-fat), rich in omega-3 fatty acids, lots of fruit and vegetables and less red meat, appears to go some way in bridging the gap.

Tony Blakely, a professor at the University of Otago in Wellington, New Zealand, has suggested more agreement than might at first appear. “There is a reasonably solid core of agreement if one looks closely,” he said. These included “the emerging consensus that overeating of refined carbohydrates is bad for you,” and “agreement on the benefits of eating more fruit and vegetables – especially to substitute other aspects of the diet.”

Woman having bread with cheese image from Shutterstock.com

This article was originally published on theconversation.com read the original here.