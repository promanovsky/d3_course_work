RESIDENCY MATCHES ANNOUNCED FOR

GRADUATING MEDICAL STUDENTS

Rockford, Ill., -- After four years of determination and hard work in medical school, 44 graduating students learned where they will spend the next three to six years of their medical training. The Rockford students matched in 14 specialties and 15 states. Forty-eight percent of Rockford students matched in primary care specialties including family medicine, internal medicine and pediatrics.

Match Day was held at 11:15 a.m., Friday, March 21 at the University of Illinois College of Medicine at Rockford. Medical students from all medical schools across the country heard their match results at the same time.

Matching students with residency programs is a computerized process, through which medical students rank their preferred residency programs, while residency programs rank their preferred applicants. Students may choose from a number of medical specialties, including family medicine, internal medicine, emergency medicine and ophthalmology.

The University of Illinois College of Medicine at Rockford is located at 1601 Parkview Avenue and has served the area since 1971 as a regional academic health science center.

See below for matches and hometowns.

University of Illinois College of Medicine at Rockford

Match List – Class of 2014

Specialty

Anesthesiology

Emmett Culligan Stanford University Programs, Stanford, CA

James Han University of Illinois College of Medicine, Chicago, IL

Jenny Jun Rush University Medical Center, Chicago, IL

University of Illinois College of Medicine/UIH, Chicago, IL (Medicine-Preliminary)

Emergency Medicine

Christopher Chesnut Western Michigan University School of Medicine, Kalamazoo, MI

Adam Duley University of Tennessee College of Medicine, Chattanooga, TN

Patrick Holland Advocate Christ Medical Center, Oak Lawn, IL

Michael Kennedy Advocate Christ Medical Center, Oak Lawn, IL

Scott Lotz Maricopa Medical Center, Phoenix, AZ

David Morales UCLA Medical Center, Los Angeles, CA

Schon Roberts Nellis Air Force Base/University of Nevada, Las Vegas, NV

Family Medicine

Caesar Arturo Scripps Mercy Hospital, Chula Vista, CA

Anne Catalano Grand Rapids Medical Education Partners, Grand Rapids, MI

Eric Elleby La Crosse-Mayo/Gundersen Lutheran Medical Center, La Crosse, WI

Theresa Kowalski Providence Hospital, Southfield, MI

Lolita Ontiveros University of Illinois College of Medicine, Rockford, IL

Hayley Ralph St. Louis University School of Medicine/Belleville, St. Louis, MO

Jessica Richardson University of Illinois College of Medicine /UIH, Chicago, IL

Lauren Wallace University of Illinois Methodist Medical Center, Peoria, IL

Laura Watt Union Hospital, Terre Haute, IN

Internal Medicine

Carlos Concepcion University of Illinois St. Francis Medical Center, Peoria, IL

Denisse Diaz Mt. Sinai Medical Center, Miami Beach, FL

Jordan Orr University of Alabama Medical Center, Birmingham, AL

Samantha Pabich University of Wisconsin Hospital and Clinics, Madison, WI (Medicine-Primary)

Joseph Puetz University of Missouri/University Hospitals, Columbia, MO

Nicholas Royal Grand Rapids Medical Education Partners, Grand Rapids, MI

Karla Talavera University of California Riverside School of Medicine, Riverside, CA

Internal Medicine/Pediatrics

Mallory Kelly University of Illinois-St. Francis Medical Center, Peoria, IL

Obstetrics and Gynecology

Michelle Apple Stanford University Programs, Stanford, CA

Ophthalmology

Kunal Patel Indiana University School of Medicine, Indianapolis, IN

St. Vincent Hospital Center, Indianapolis, IN (Medicine-Preliminary)

Orthopaedic Surgery

Joshua Lawrenz Cleveland Clinic Foundation, Cleveland, OH

Rick Wang University of Illinois College of Medicine, Chicago, IL

Pathology-Anatomic and Clinical

Jefree Schulte University of Chicago Medical Center, Chicago, IL

Pediatrics

Rashi Bamzai University of Rochester/Strong Memorial Hospital, Rochester, NY

Jessica Chiang Advocate Lutheran General Hospital, Park Ridge, IL

Andrew Grim Naval Medical Center, San Diego, CA

Mackenzie Lowery University of Louisville School of Medicine, Louisville, KY

Fatima Zaheer Advocate Christ Medical Center, Oak Lawn, IL

Radiology-Diagnostic

Allison Retzer SIU School of Medicine & Affiliated Hospitals, Springfield, IL

Radiation Oncology

Joon Lee Henry Ford Hospital, Detroit, MI

Wayne State University School of Medicine, Detroit, MI (Transitional)

General Surgery

Todd Bierman University of South Alabama Hospitals, Mobile, AL

Maxwell Busch University of Minnesota Medical School, Minneapolis, MN

David Chiang Carle Foundation Hospital, Urbana, IL

Susan Wcislak University of Tennessee College of Medicine, Memphis, TN