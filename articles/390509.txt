chikungunya fever sick patient

Marie Arago / Reuters

Marqui Ducarme is aided by his wife after contracting the Chikungunya virus at his home in Port-au-Prince, May 23, 2014. The painful mosquito-borne virus called Chikungunya is spreading quickly through the Caribbean, causing alarm in Haiti and the neighboring Dominican Republic.

The first report of chikungunya in the Americas surfaced in the Caribbean in December 2013. By March, there were 15,000 such reports. Now, according to the latest numbers from the regional office of the World Health Organization, there are 350,580 suspected cases of this incredibly painful viral infection.

Chikungunya is spread by mosquitoes. It involves a sudden fever and, in most cases, severe joint pain that usually lasts a few days but has been known to continue for months and even years. There's no cure, but doctors can help relieve the patient's symptoms.

"It's a really pathetic, nasty disease," Joe Conlin, a medical entomologist and spokesperson for American Mosquito Control Association told Business Insider in 2013. "I've... seen and heard children just screaming for days on end because of the pain."

Chikungunya was first identified in 1952, according to the World Health Organization, and there were sporadic outbreaks in Asia and Africa in the 1960s and 1970s. But in the past decade, it's become a much larger threat. "Since 2004, chikungunya has expanded its geographical range, causing sustained epidemics of unprecedented magnitude in Asia and Africa," notes a recent report from the CDC.

The virus reached epidemic proportions in India in 2006, when 1.5 million people fell sick. It jumped to Europe in 2007, infecting 197 people in Italy.

For years, researchers have been expecting mosquito-borne diseases like West Nile to increase in the Americas, and it seems chikungunya's time has come.

The Dominican Republic has had 193,395 suspected cases since December, more than any other country in the current outbreak. There have also been tens of thousands of cases in Haiti, Martinique, and Guadaloupe, and a scattering of other cases elsewhere — including El Salvador, Puerto Rico, Saint Martin, and Saint Lucia.

Story continues

As of July 8, the Centers for Disease Control and Prevention had counted 136 cases in the contiguous United States, but all of these were "travel-associated." That means the chikungunya was found in visitors or in American residents returning from areas where the disease is prevalent. Still, that's much higher than the recent average just 28 local cases each year.

Chikungunya is spread by mosquitoes, which means it doesn't spread from an infected person to someone else directly. The right species of mosquito needs to be present to act as an intermediary, and these species are endemic in parts of the United States.

When an infected person is bitten by the right species of mosquito, that mosquito sucks up their infected blood and can then pass on the disease when it bites another person.

"To date, no local transmission has been identified in the continental United States," the CDC notes, meaning we haven't yet seen it spreading through mosquitoes in the U.S. But if an infected person is bitten on U.S. soil, "these imported cases could result in local spread of the virus."

While effective mosquito control and widespread air conditioning make huge numbers of cases unlikely here, it's probably just a matter of time until we see this painful disease begin to spread throughout the U.S. In a recent issue of the journal Emerging Infectious Diseases, Roger Nasci, of the CDC's Division of Vector-Borne Diseases, called the spread of chikungunya to mainland South, Central, and North America "inevitable."

Here are the full numbers from the Pan-American Health Organization, not including imported cases:

chikungunya count

PAHO/WHO

h/t @LJBeil





More From Business Insider

