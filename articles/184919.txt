The pharmaceutical industry continued its massive spurt of acquisitions today with the announcement of a Merck & Co., Inc. (NYSE:MRK) deal to sell its entire consumer unit to Bayer AG (FRA:BAYN) (ETR:BAYN). The companies agreed on a price of $14.2 billion for the business. Once the deal is closed Bayer will take control of a number of over the counter medicines that will secure its place at second biggest in the market.

Merck & Co., Inc. (NYSE:MRK) revealed earlier in 2014 that it was looking for a buyer to pick up its consumer business as it had decided to concentrate on its core pharmaceuticals trade. Last year the business accounted for just 4% of Merck revenue, or $1.9 billion. The company’s fortunes in recent years have been impressive, but its consumer arm has done little to alter the course of the company.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Bayer jumps at consumer opportunity

Johnson & Johnson still holds the top spot in the over-the counter medicine business, but Bayer AG (FRA:BAYN) (ETR:BAYN) seems set on changing that. Today’s $14.2 billion acquisition will see brands like Coppertone sunscreen and anti-allergy medicine Claritin become part of the Bayer stable.

Becoming the biggest name in the consumer drug industry is the explicit goal of Bayer AG (FRA:BAYN) (ETR:BAYN) CEO Marijn Dekkers. In a statement concerning today’s acquisition the executive said, “This acquisition marks a major milestone on our path towards global leadership in the attractive non-prescription medicines business.”

Bayer AG (FRA:BAYN) (ETR:BAYN) is taking on debt in order to fund the $14.2 billion acquisition. The company said that it had secured financing from Bank of America among other creditors, in order to pay for the consumer business. Shares in Bayer declined slightly on today’s market as a result of the transaction. The firm is trading at a P/E of close to 24 indicating that the market already has a lot of growth priced into the company.

The shake up in the pharmaceuticals industry in recent weeks has been of great interest to investors. Companies like Bayer AG (FRA:BAYN) (ETR:BAYN) are focusing themselves on the consumer side of medicine, while Merck is taking the opposite tack. The miscellany in between, which at Bayer includes a plastics business, is being shuffled among several other players in the market.

Merck concentrates on drug research

Merck & Co., Inc. (NYSE:MRK), in complete antipathy to Bayer, does not see its future in consumer over the counter medications at all. The company wants to delve deeper into the unpredictable world of pharmaceutical research, and the deal today included some cooperation on that front between the companies. Merck will apparently be helping Bayer with research on some drugs it is currently developing.

The company’s CEO was just as explicit as Bayer’s chief in his plans for the future of his company. Kenneth C. Frazier said “By unlocking value in Merck Consumer Care, we’re able to further our goal of being the premier research-intensive biopharmaceutical company through targeted investments that strengthen our product portfolio and enhance our pipeline,”

Merck & Co., Inc. (NYSE:MRK) shareholders didn’t seem as happy with the deal as the firm’s CEO. On Tuesday morning the company’s shares lost more than 2% of their value in response to the Bayer AG (FRA:BAYN) (ETR:BAYN) deal.