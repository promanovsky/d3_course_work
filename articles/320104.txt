The Holiday and Magnificent Seven star Eli Wallach dies aged 98



US actor Eli Wallach has died aged 98.



The star, who appeared in The Holiday and The Good, the Bad and the Ugly, passed away on Tuesday, his daughter has confirmed, according to The New York Times.

Eli leaves behind 87-year-old actress Anne Jackson, who he married in 1948, and their three children, Peter, Roberta and Katherine.

Scroll down for video



Late star: US actor Eli Wallach, who appeared in The Holiday and The Good, the Bad and the Ugly, has died aged 98

He started his career in 1956 and went on to star in several huge movies including western The Good, the Bad and the Ugly alongside fellow legendary actor Clint Eastwood.



Eli appeared alongside wife Anne on several occasions, mainly on-stage.

This included them starring together in a 1978 version of The Diary of Anne Frank, which was a particularly special occasion for the couple as it featured their daughters, Roberta, who played Anne, and Katherine as Anne's sister.

Happy times: Eli tied the knot to stage actress Anne Jackson in 1948 and appeared alongside her on several occasions, mainly on-stage

The star lost the sight in his left eye as the result of a stroke and in his 2005 autobiography he described the incident as happening 'some years ago'.



His latest role was in 2010 drama Wall Street Never Sleeps, which also starred Michael Douglas and Shia LaBeouf.



He made his name as a stage actor in New York in 1945, appearing in several productions such as The Rose Tattoo and The Diary of Anne Frank.

One of his more notable recent appearances came in The Holiday alongside Jude Law, Cameron Diaz, Jack Black and Kate Winslet.

Starring role: The actor had a starring role in The Good, the Bad and the Ugly as comical character Tuco, who becomes reluctant partners with Clint Eastwood's (right) alter-ego 'Blondie'.

Eli starred as Arthur Abbott, the neighbour of Cameron's character Amanda Woods who became friends with Kate's alter-ego Iris Simpkins.

The actor had a starring role in 1966 Western movie The Good, the Bad and the Ugly as comical character Tuco, who becomes reluctant partners with Clint Eastwood's alter-ego 'Blondie'.



He also appeared as a Mexican bandit in Western movie The Magnificent Seven.



Mixing business with pleasure: Eli (left) alongside his wife Anne (right) and Timothy Patrick Murphy as his on-screen son in 1984 film Sam's Son Huge stars: Eli (left) starring alongside fellow screen legend William Shatner in courtroom drama Indict and Convict

What's more, he appeared alongside screen siren Marilyn Monroe in 1961's Misfits and starred as New York Mafia boss Don Altobello with Al Pacino in The Godfather Part III.



Despite his glittering film career, Eli was never nominated for an Academy Award.



However, he was awarded an honorary Oscar in November 2010.



The Academy of Motion Picture Arts and Sciences praised Eli for being 'the quintessential chameleon, effortlessly inhabiting a wide range of characters, while putting his inimitable stamp on every role'.



Legend: The star, pictured at the Emmy Awards in 2010, lost the sight in his left eye as the result of a stroke and in his 2005 autobiography he described the incident as happening 'some years ago'









