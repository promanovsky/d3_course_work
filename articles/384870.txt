Apple will pay up to $400 million to compensate consumers ensnared in a plot to raise the prices of digital books unless the company overturns a court decision attesting to its pivotal role in the collusion.

The terms of the settlement disclosed in a Wednesday court filing came a month after an attorney suing Apple notified U.S. District Judge Denise Cote that an agreement had been reached to avert a trial.

Lawsuits filed on behalf of digital book buyers had been seeking damages of up to $840 million for a price-fixing scheme that Cote ruled had been orchestrated by Apple Inc.

Apple is appealing Cote’s decision. The Cupertino, California, company won’t have to pay the $400 million settlement if it prevails.

Five major book publishers previously reached settlements totaling $166 million.