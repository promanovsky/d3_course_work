A U.S. Food and Drug Administration (FDA) panel approved an at-home stool test that screens for colorectal cancer with an accuracy of at least 90 percent.

The 10-member committee unanimously voted in approval of the new screening method, HealthDay reported. The FDA is not required to honor this ruling, but they most likely will.

Last year alone 143,000 new cases of colorectal cancers were diagnosed 50,000 Americans died from the cancer.

One-third of Americans over the age of 50 do not get recommended colorectal screening procedures such as colonoscopies, possibly because they are perceived as too invasive.

Other stool tests such as FIT (fecal immunochemical testing) simply test for blood in the stool; this new screening method, dubbed Cologuard, also checks for abnormal DNA indicative of a tumor or abnormality.

"The advantage [is] that some lesions, even cancers, don't bleed very much," Doctor Steven Itzkowitz, director of the gastroenterology fellowship program at the Icahn School of Medicine at Mount Sinai in New York City, told HealthDay.

"By increasing the pick-up rate in this way," Itzkowitz said, "we found that the new test had a 92 percent sensitivity for detection of colorectal cancer. That kind of result is really unprecedented for a noninvasive stool-based screening."

The new method was tested on about 10,000 men and women aged 50 and older. Each patient was screened using the new method, the FIT test, and a standard colonoscopy. Colonoscopy screenings were found to spot signs of colorectal cancer with the highest accuracy, but many forgo the procedure due to the discomfort involved.

Colonoscopies predicted colorectal cancer in 65 of 757 participants found to have advanced precancerous lesions; Cologuard predicted 60 of these cases while the FIT test found only 48.

"It's not a perfect test," Itzkowitz said. "But neither is a colonoscopy. Also, I don't think we're saying that this test should be done as a replacement for a colonoscopy, but rather as an adjunct. Certainly if a person who does this test comes out with a positive reading then they will need to do a colonoscopy afterwards to confirm it."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.