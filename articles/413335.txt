The missile and its three-stage rocket booster system fell back on the Kodiak Launch Complex, a commercial facility on Kodiak Island, about 300 miles southwest of Anchorage. “Fortunately, no one was injured,” said Maureen Schumann, a spokeswoman at the Pentagon.

The Pentagon said in a news release that the Army’s Space and Missile Defense Command and Army Forces Strategic Command launched the test flight shortly after 4 a.m. Eastern Daylight Time. The test “was terminated near the launch pad shortly after lift-off to ensure public safety,” the Defense Department said. An extensive investigation will be conducted, defense officials said.

AD

AD

The Pentagon released no images or videos of the test, but the Web site for the radio station KMXT 100.1 FM in Alaska posted a photograph of the explosion that it reported was taken from about 12 miles away. Witnesses told the station that the rocket carrying the weapon lifted off but turned nose down and quickly self-destructed or exploded after hitting the ground.

It marked the second test flight for the program. The first, in November 2011, successfully launched the hypersonic “glide vehicle” about 2,500 miles from the Pacific Missile Range Facility in Hawaii to the Ronald Reagan Ballistic Missile Test Site, an island installation in the Pacific’s Marshall Islands. The glider and rocket system were developed by Sandia National Laboratories, which is overseen by the Energy Department and managed by Lockheed Martin.

The explosion leaves it unclear what’s next for the project. As Breaking Defense noted in March, senior defense officials planned to figure that out if Monday’s test was successful. The hypersonic weapon is part of the larger Prompt Global Strike program, which researches ultra-fast projectiles and was established after the terrorist attacks of Sept. 11, 2001, to attack time-sensitive targets that could threaten large numbers of people, according to a Daily Beast report.