Amgen Inc. said it plans to reduce its global workforce by 12% to 15% and close facilities in two states as part of a restructuring that aims to focus resources on developing new drugs.

The company was a biotechnology pioneer and is the largest biotech by sales. But the products that propelled its growth are aging and some are facing the threat of low-cost competition. In announcing cost-cutting plans Tuesday, Amgen joins the ranks of Big Pharmas who have been laying off employees and closing plants. It had already emulated traditional drug makers by doing a large deal last year to add a medicine discovered outside its laboratories.

Amgen said it plans to reduce staff by 2,400 to 2,900, beginning later this year and continuing through 2015. Most of the workforce reductions are set for the U.S. and will span the range of job types, including sales representatives, an Amgen spokesman said. The company currently employs about 20,000.

The job cuts will be combined with eliminating layers of management, using fewer buildings at its headquarters site and closing two laboratories in Washington state and two manufacturing plants in Colorado, the spokesman said.

At the same time, Amgen plans to expand its drug research-and-development in the biotechnology hubs of South San Francisco, Calif., and Cambridge, Mass., as well as retain its headquarters in Thousand Oaks, Calif., with a smaller staff in fewer existing buildings.

The company expects to record restructuring-related charges between $775 million and $950 million, primarily incurred this year and next year. It expects to save about $700 million in operating expenses in 2016, but plans to use most of the savings to launch new products, the spokesman said.

Chief Executive Robert A. Bradway said the restructuring plans were announced from "a position of strength."

Amgen reported that its second-quarter earnings rose 23% as the company benefited from higher revenue and increased profitability of its arthritis drug Enbrel. The company also raised its 2014 earnings and revenue targets.

Shares of Amgen rose 3.6% to $127.74 in after-hours trading.

Last year, Amgen bought Onyx Pharmaceuticals Inc. for $10.4 billion and its flagship product, Kyprolis.

The drug was approved in the U.S. in 2012 as a treatment for a blood cancer known as multiple myeloma. Amgen is betting that current studies of Kyprolis will lead to approvals for additional uses, with study data anticipated in the current quarter. In the second quarter, Kyprolis sales were $78 million, up from $68 million in the first quarter.

Amgen reported a profit of $1.55 billion, or $2.01 a share, up from $1.26 billion, or $1.65 a share, a year earlier. Excluding acquisition-related and other items, adjusted per-share earnings rose to $2.37 from $1.89. Revenue increased 11% to $5.18 billion.

Analysts polled by Thomson Reuters expected per-share profit of $2.07 and revenue of $4.9 billion.

Sales of Enbrel increased 7% to $1.2 billion, driven mostly by higher pricing and underlying demand.

Combined sales of Neulasta and Neupogen, both of which are used to prevent infections in patients receiving chemotherapy, fell 1% as a decline in Neupogen sales offset a slight increase in Neulasta sales.

Neupogen sales fell 9%, which the company attributed to a positive Medicaid rebate adjustment in the year-earlier period. The drug is facing generic competition in the U.S. from the launch late last year by Teva Pharmaceutical Industries Ltd. of its Neupogen-similar treatment, Granix.

Sales of osteoporosis drugs Xgeva and Prolia rose 20% and 40%, respectively, with both benefiting from stronger volume.

Write to Tess Stynes at tess.stynes@wsj.com and Jonathan D. Rockoff at jonathan.rockoff@wsj.com

Access Investor Kit for Teva Pharmaceutical Industries Ltd.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=IL0006290147

Access Investor Kit for Amgen, Inc.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US0311621009

Access Investor Kit for Teva Pharmaceutical Industries Ltd.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US8816242098

Subscribe to WSJ: http://online.wsj.com?mod=djnwires