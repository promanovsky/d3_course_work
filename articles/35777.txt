Americans believe that climate change will have serious consequences for the United States, a new HuffPost/YouGov poll shows. But there's a catch: Most think they'll be long gone before the consequences become catastrophic.

The survey adds nuance to the story told by a recent Gallup poll that showed most do not consider global warming to be a "serious threat" to themselves.

Both surveys found that many Americans think at least some consequences of climate change have already hit. Forty-nine percent in the HuffPost/YouGov poll said they have already felt its impact, while only 32 percent said they had not. Nineteen percent weren't sure. Even more -- 62 percent -- said they thought they would personally feel the impact of climate change at some point in their lifetimes.

Similarly, the Gallup poll found that 54 percent of Americans think the effects of global warming have already begun. A total of 65 percent said that the effects either had already begun, would begin in the next few years or would begin within their lifetimes.

Among the consequences that most Americans expect to see within their own lifetimes, according to the HuffPost/YouGov poll, are more frequent or intense storms like hurricanes (59 percent), greater difficulty in growing crops (53 percent) and certain areas possibly becoming less habitable (50 percent). Forty-nine percent said climate change will lead to rising sea levels that threaten coastal areas.

Still, the Gallup poll found that only 36 percent of Americans expect global warming to pose a "serious threat to you or your way of life within your lifetime." The HuffPost/YouGov poll likewise found that only 36 percent think climate change will cause major harm to the United States during their lifetimes, while 33 percent think it will cause minor harm and 18 percent think it will cause no harm at all.

Beyond the span of their own lives, however, the HuffPost/YouGov poll shows that most Americans do see the impact of climate change as dire.

Fifty-six percent said that climate change will pose a serious threat to the American way of life at some point in the future, while only 26 percent said it won't. And by a 54 percent to 28 percent margin, most think that it will even pose a serious threat to human life.

Americans' feeling that climate change will have extreme consequences, but just not soon, may explain why they told Gallup both that they worry about the environment fairly little compared to other issues and that the environment should be given priority even if it curbs economic growth.

White House senior counselor John Podesta cited polls showing that Americans don't yet place a high priority on dealing with climate change as a major reason for the launch of the Obama administration's Climate Data Initiative on Wednesday.

Concern about climate change divides along party lines. The HuffPost/YouGov poll found Republicans much less likely than Democrats or independents to say that they had felt the impact of climate change or to predict future consequences of any sort. Of course, Republicans are also much less likely to say that human-caused climate change is occurring at all. Eighty-one percent of Democrats, 53 percent of independents and only 27 percent of Republicans said they think climate change is happening as a result of human activity.

The HuffPost/YouGov poll was conducted March 15-17 among 1,000 U.S. adults using a sample selected from YouGov's opt-in online panel to match the demographics and other characteristics of the adult U.S. population. Factors considered include age, race, gender, education, employment, income, marital status, number of children, voter registration, time and location of Internet access, interest in politics, religion and church attendance.

The Huffington Post has teamed up with YouGov to conduct daily opinion polls. You can learn more about this project and take part in YouGov's nationally representative opinion polling.

