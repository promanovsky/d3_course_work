They're the biggest lizards to roam the earth.

Komodo dragons are not just any old lizards. In their native habitat, Komodos can grow as long as 9 feet and weigh up to 350 pounds.

"They always spark a lot of public interest," said Bronx Zoo director Jim Breheny.

This week, the zoo welcomed three rare Komodos to their new home.

"When people come to the Bronx Zoo and they view the Komodos in this exhibit and they see these animals exploring and interacting with each other and their environment, they're going to get a different sense of what a reptile is," Breheny said.

Highly elusive, Komodo dragons are the closest link to dinosaurs. They are from just one place in the world - a tiny Indonesian island that gave them their name.

And to make them feel right at home, Bronx Zoo officials literally left no stone unturned.

Replicating the Komodo's natural habitat is all the handiwork of a team of artisans who planned, constructed and then painted a landscape over the last two years.

"They're professional artists. They all have creative lives outside of the zoo, and they just love the work," said exhibit director Gary Smith last August.

"It's really challenging. It uses a lot of imagination; it's an exciting process," Smith said.

Authenticity meant attention to every painstaking detail using models built to scale before any ground-breaking began. Then came the hard part, duplicating their ideas in the real world.

"The soil in here is matched to soil in Komodo Island so that it looks real," said Smith.

Because Komodos "dig" during mating season to lay their eggs, workers had to ensure their pens were at least 3 feet deep.

"She'll lay her eggs in there, and it'll be nice and warm for them and then they incubate," Smith said. "The floor has radiant heat in it as well. There's going be water, hot water, run through pipes in the floor, and that will radiate through the dirt ... and then there'll be a warm pool for them to go into."

Of course all this attention to detail isn't just for the dragons.

It's estimated there are less than 2,500 Komodo dragons in the wild today, with as few as 350 breeding females.

The Wildlife Conservation Society and the Bronx Zoo hope their exhibit will motivate the public to learn more about the animals and hopefully to care about their fate.

"I think this window into the biology and the habits of the Komodo dragon is going to increase people's appreciation for this species and hopefully not just Komodo dragons but all reptiles," said Breheny.