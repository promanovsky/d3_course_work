It's official! Olivia Palermo and Johannes Huebl are married!

The couple of six years tied the knot in Bedford, New York. Palermo posted the first official wedding photo and blogged about the ceremony on her site on Saturday, June 28.

"The two said their vows amongst only a handful of family members and closest friends as they were surrounded by ivy and blossoming flowers in a quiet park tucked away in Bedford, New York," the site read. “We really wanted to keep this beautiful day very private and special to us and enjoyed the whole day with our family and two friends."

Palermo, who appeared on MTV's The City alongside Whitney Port, started dating her handsome now-hubby back in 2008. She wore a three-piece Carolina Herrera bridal set.

"Friends set us up, and it just fell right into place," the stylish star told Brides magazine in their June 2014 issue. "We're a great team. We understand and respect each other, and we make each other better people."

"It also helps that we both work in fashion and can travel the world together," Palermo added. "We get to see the world through each other's eyes."

Huebl popped the question to his longtime love last January while on vacation in St. Barts.

The couple then announced the news of their engagement with a sweet Instagram video, where Palermo danced around with her massive new rock on. The German model captioned the post: "Happy New Year to everybody and this is how we celebrate it: OP said Y E S."