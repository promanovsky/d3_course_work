The news apps will be welcomed by Glass users, who have been limited in their options for Glassware. (Credit:Google)

SAN FRANCISCO, June 24 (UPI) -- Google launched a flurry of new apps for the Glass, or as it like to call them Glassware, ahead of its annual developers conference in San Francisco Wednesday.

The new apps will include Shazam, Livestream, a World Cup app and eight other new apps that will be available on Google's head-mounted device. The new apps increase Google's offerings for the Glass by 80 percent.

Advertisement

The new apps also include Duolingo, Runtastic, 94Fifty Basketball, The Guardian, GuidiGO, Zombies, Run!, Star Chart, Goal.com, and musiXmatch. Cooking app Allthecooks will get a major update.

Goal.com will give Glass users World Cup updates and notifications pushed directly to their device. The Shazam app will let users to recognize songs by using the command, "OK Glass, recognize this song." GuidiGo will provide Glass users information for 27 destinations and lets them download 250 guided tours.

The Runtastic app will guide users through their workouts and keep track of progress, like the distance they ran or how many sit-ups they've completed.

The announcement comes a day after the company announced that it would begin to sell the device in the U.K. for $1,700.