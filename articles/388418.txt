Apple and IBM recently announced a partnership that would help Apple sell more iPhones and iPads to Enterprise, but according to a recent report, the deal could also have other benefits for Apple, and may help them with Siri.

Apple’s Siri is the company’s voice activated virtual assistant, and according to a recent report by Venture Beat, Apple’s Siri could possibly get a lot smarter, thanks to the IBM deal.

According to the report, Apple’s Siri could possibly team up with IBM’s Watson, which would benefit both companies, and could make Siri a lot more useful than it is in its current state.

“If Siri partners with Watson, people will take notice,” the source said. “People think Siri is a joke. With Watson, no other cognitive technology out there has the ability to beat the world’s best Jeopardy contestants. And what this partnership means is Watson having access to Apple’s data, which would help power Watson’s own cognitive data. So Watson and Siri would fit together perfectly, like a puzzle.

“And remember, Siri is a consumer-facing toy compared to Watson. While Siri is more or less is designed for consumers to use, Watson is for data experts. One of the problems is that Watson’s members don’t have access to consumer data through Google search. Access to Apple’s data will help improve Siri.”

Whether or not IBM’s Watson and Apple’s Siri will be used together remains to be seen, it certainly would give Apple a serious edge against other voice activated assistants from other platforms.

Source Cult of Mac

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more