Standup stalwart John Pinette, famous for his cameo in the Seinfeld finale, and beloved by fellow comedians, was found dead in a hotel room on Pittsburgh Saturday night. He was 50 years old.

While there has been no official cause of death released, his manager Larry Schapiro told Hollywood Reporter that Pinette died of a pulmonary embolism.

The Boston native was renowned for his self-deprecating style of standup and was in the middle of a U.S.-Canada tour when he passed away.

Social media outlets were abuzz with messages honoring the comedian after the news of Pinette’s death broke, many from fellow comics.

John Pinette and I worked on a film together. He was one of the most generous people I’ve ever worked with. A hilarious comedian. Sad loss. — Dane Cook (@DaneCook) April 7, 2014

So sad to hear about the passing of @JohnPinette. He was always so funny, so generous and so kind. — Jim Gaffigan (@JimGaffigan) April 6, 2014

John Pinette was a very funny guy and a powerful performer. I got a chance to see him kill it many times when I lived in Boston. RIP — Joe Rogan (@joerogan) April 7, 2014

Just was informed a great comedian and buddy John Pinette passed away Saturday. Horrible news. Never failed to make us cry laughing. On or — Larry The Cable Guy (@GitRDoneLarry) April 6, 2014

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.