‘Scandal’ star Columbus Short, who plays the role of Harrison Wright on the hit ABC show, is currently living a real-life scandal. The actor is now wanted by police, who have issued a warrant for his arrest, stemming from a recent bar fight that left another man unconscious.

A Los Angeles judge issued a warrant for Columbus Short‘s arrest on March 26, after the actor allegedly sucker punched a man at a local bar. This isn’t the first time the Scandal star has gotten into trouble with the law because of his “nasty temper.” In fact, he’s had “countless blow-ups,” a source tells HollywoodLife.com EXCLUSIVELY.

Columbus Short’s Arrest Warrant — ‘Scandal’ Star Wanted For Alleged Battery

Columbus is wanted by the police for one count of felony aggravated battery resulting from an alleged brutal fight at Gabe’s Bar and Grill in Los Angeles on March 15, TMZ is reporting.

If found guilty, Columbus could face a maximum sentence of four years in prison. His bail has been set at $50,000. Columbus still has not turned himself in.

Columbus’ Alleged Violent Past Revealed

In 2010, Columbus allegedly got into a fight with a man at LA Fitness Gym and allegedly knocked his teeth out!

“Columbus is no stranger to trouble. He’s had countless blow-ups! This guy has one nasty, nasty temper,” a source tells HollywoodLife.com EXCLUSIVELY.

Before he made his big break as an actor, Columbus was a professional hip hop dancer.

“He used to pal around with these thugs long before he was famous. I would never, ever want to be on his bad side,” the insider adds. “The guy needs major help before he seriously injures another person.”

Yikes! We sure hope Columbus turns himself in and gets the help he needs!

What do YOU think, HollywoodLifers? Will Columbus get fired from Scandal because of his violent temper? Do YOU think he needs anger management? Let us know.

— Reporting by Mickey Powell

More ‘Scandal’ News: