The UN climate panel today said Himalayan glaciers, whose meltwater is vital for hundreds of millions of people, could lose between half and two-thirds of their mass by 2100.

The estimate by the Intergovernmental Panel on Climate Change (IPCC) revisits a blunder in its last overview that tarnished the group's reputation when it warned the glaciers could vanish by 2035.

In a massive Fifth Assessment Report on climate impacts released in Yokohama, Japan, the IPCC said Himalayan glaciers would shrink by 45 percent by 2100, if Earth's average surface temperature rose by 1.8 degrees Celsius (3.6 degrees Fahrenheit).

Under a far warmer scenario of 3.7 C (6.66 F), the reduction would be 68 percent.

The benchmark year -- the starting point from which these reductions are calculated -- is 2006.

The two estimates derive from the average of 14 computer simulations in a scientific study published in 2013, the IPCC said.

"It is virtually certain that these projections are more reliable than an earlier erroneous assessment (in 2007)... Of complete disappearance by 2035," the report said.

In 2007, the IPCC's Fourth Assessment Report provided a political jolt on climate change, warning of the risk to weather systems from heat-trapping fossil-fuel emissions.

The report helped the IPCC win a co-share of the Nobel Peace Prize and gave momentum to the ultimately unsuccessful effort to forge a world climate pact in Copenhagen in 2009.

But the IPCC's reputation was hurt when several mistakes came to light.

One was that the Himalayan glaciers could be lost by 2035 if warming continued unabated -- an assessment later traced to a magazine article, and which the panel acknowledged as erroneous in January 2010.

It also erred in judging how much of The Netherlands lies below sea level.

The errors were seized upon by climate sceptics as evidence that the IPCC was flawed or biased.

An independent probe, ordered by UN Secretary General Ban Ki-moon, instructed the IPCC to carry out reforms but did not dispute the report's core findings.