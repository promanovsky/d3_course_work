The Nest Thermostat Is Now Available Through Google Play

Google’s latest toy is available for purchase on Google Play for $249. Starting today, conveniently to coincide with Earth Day, Google started selling Nest Learning Thermostat on its web store.

Since the Nest Protect is temporary unavailable, only the Learning Thermostat is on Google Play right now.

The price is the same, though. The Learning Thermostat is still $249 through Google like at other retailers including Lowes, Nest.com and Amazon. Google states that the product will ship from warehouse within 1-2 business days from the time it was ordered.

The Google Play product listing briefly appeared yesterday but was pulled. At the time Google failed to comment on the listing, likely because it was preparing to actually start selling the device.

For those keeping track, this makes the Nest Learning Thermostat the only product available on both Google and Apple’s retail site.