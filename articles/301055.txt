Dr Oz slammed for touting diet products

Share this article: Share Tweet Share Share Share Email Share

Washington - A US Senate panel probing bogus diet product ads took celebrity doctor Mehmet Oz to task on Tuesday for touting weight-loss products on his syndicated television show. Missouri Democratic Senator Claire McCaskill, the chairwoman of the Commerce subcommittee on consumer protection, said Oz had a role in perpetuating weight-loss fraud through his show. “I don't get why you need to say this stuff because you know it's not true,” McCaskill said at the hearing, a follow-up to a Federal Trade Commission crackdown in January on fraudulent diet products. She said Oz's promotion tended to boost sales and prompted scam artists to sell questionable products using deceptive ads. “When you call a product a miracle, and it's something you can buy and it's something that gives people false hope, I just don't understand why you need to go there,” McCaskill said.

Oz, the host of The Dr. Oz Show, said the products gave people hope to keep trying to lose weight. Nearly 70 percent of Americans are overweight or obese, and he said the No. 1 topic asked about on his website was weight loss.

“I actually do personally believe in the items that I talk about on the show,” said Oz, a Columbia University professor.

“I recognise that oftentimes they don't have the scientific muster to present as fact. I would give my audience the advice I give my family all the time, and I have given my family these products.”

Part of the hearing focused on green coffee extract, a dietary supplement Oz touted in 2012 as a “miracle.” The show heightened interest in the product, and Oz testified he devoted much of a second show to telling viewers how his name was being used unscrupulously to sell it.

Mary Koelbel Engle, an FTC official, testified that the agency filed suit over the supplement, charging deceptive claims and promotion.

A 2011 FTC survey of consumer fraud showed more consumers were victims of bogus weight-loss products than any other frauds covered by the survey, she said.

Americans were forecast to spend $2.4-billion on weight-loss services last year, and the figure was expected to rise to $2.7-billion in 2018, she said. - Reuters