Swiss drug maker Roche has agreed to buy US-based Seragon Pharmaceuticals, which specialises in breast cancer treatments and research.

Roche said its Genentech unit will pay $725m (£423m, €530m) in cash for privately held Seragon and could shell out up to another $1bn depending on the attainment of certain development benchmarks.

Seragon's development portfolio includes a breast cancer drug that is in the first phase of human trials.

The transaction is expected to close in the third-quarter of 2014. On completion, Seragon's portfolio will be integrated into Genentech Research and Early Development, Roche said in a statement.

Roche's stock was trading 0.30% higher at 09:15 CEST in Zurich.

"Seragon's lead product candidate, ARN-810, is a next-generation selective estrogen receptor degrader (SERD) that is currently in Phase I clinical trials for patients who have hormone receptor-positive breast cancer and have failed current hormonal agents. These next-generation SERDs complement Genentech's existing research and development programs in breast cancer," according to Roche.

Richard Scheller, executive vice president and head of Genentech Research and Early Development said in the statement: "This year, breast cancer will claim the lives of nearly 40,000 women in the US, and up to half of these women will have a disease that is driven by the estrogen receptor. We believe these investigational oral SERDs could one day redefine the standard of care for hormone receptor-positive breast cancer."

In June, Roche agreed to acquire US gene-sequencing firm Genia Technologies for as much as $350m

Back in May Roche said Chinese authorities visited its offices in eastern China, making it the second Swiss pharmaceutical firm in that country after Novartis to attract government scrutiny amid a crackdown on pharmaceutical sector corruption.