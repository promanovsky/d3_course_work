Health workers teach people about the Ebola virus and how to prevent infection, in Conakry, Guinea, Monday, March 31, 2014. Health authorities in Guinea are facing an "unprecedented epidemic" of Ebola, the international aid group Doctors Without Borders warned Monday as the death toll from the disease that causes severe bleeding reached 78. The outbreak of Ebola in Guinea poses challenges never seen in previous outbreaks that involved "more remote locations as opposed to urban areas," said Doctors Without Borders. (AP Photo/ Youssouf Bah)

Ebola Virus Outbreak: Symptoms of Deadly Disease Causing Deaths in Africa

As the ebola virus outbreak in Africa continues, here’s a look at symptoms of the virus.

This list comes from the Centers for Disease Control and Prevention in the United States, which has a five-person team in Guinea and a two-person team in Liberia assisting the numerous international organizations and the local health ministries with the outbreak.

Symptoms of Ebola HF typically include:

Fever

Headache

Joint and muscle aches

Weakness

Diarrhea

Vomiting

Stomach pain

Lack of appetite

And some patients may experience:

A rash

Red eyes

Hiccups

Cough

Sore throat

Chest pain

Difficulty breathing

Difficulty swallowing

Bleeding inside and outside of the body

Symptoms may appear anywhere from 2 to 21 days after exposure to ebolavirus though 8-10 days is most common.

Some who become sick with Ebola HF are able to recover, while others do not. The reasons behind this are not yet fully understood. However, it is known that patients who die usually have not developed a significant immune response to the virus at the time of death.

See an update on the situation in Africa from the Associated Press below.

Death toll in Ebola outbreak rises to 121

DAKAR, Senegal—An outbreak of Ebola in West Africa has been linked to the deaths of more than 120 people, according to the latest World Health Organization count.

There is no vaccine and no cure for the deadly virus, and its appearance in West Africa, far from its usual sites in Central and East Africa, has caused some panic.

Health workers are trying to contain its spread, tracking down anyone with whom the sick have had contact. Mali announced Tuesday that samples from all its suspected cases had tested negative for the disease.

Malian Health Minister Ousmane Kone said that the country had sent out 10 samples for testing at labs in the United States and Senegal, and all were declared negative for Ebola. There are no other known suspected cases in the country.

As of Monday, the U.N. health agency said it had recorded a total of 200 suspected or confirmed cases of Ebola, the majority of which are in Guinea. That figure includes some of the Mali cases that the government now says are negative. The organization said the deaths of 121 people in Guinea and Liberia have been linked to the disease.

Officials have said the current outbreak could last months.