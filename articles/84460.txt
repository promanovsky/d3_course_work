Graphene may as well be called the most interesting material in the world.

It's stronger than a diamond and pretty much anything else known to man. It's an atom thick. It's incredibly conductive and enhances communication. It could potentially replace silicon and ultimately transform electronics.

One of its only limitations is size; most of its qualities cannot be produced at a scale that is useful in manufacturing consumer-level products. That is, until now.

See also: 9 Top Tech Breakthroughs of 2013

Researchers around the globe have experimented with graphene since its discovery in 2004. Now, scientists at Samsung's Advanced Institute of Technology (SAIT) and Sungkyunkwan University in South Korea have announced the discovery of a new method for "growing large area, single crystal wafer scale graphene."

"The new method...synthesizes large-area graphene into a single crystal on a semiconductor, maintaining its electric and mechanical properties," Samsung wrote. It "repeatedly synthesizes single crystal graphene on the current semiconductor wafer scale."

Previous attempts at growing large-scale graphene wafers were unsuccessful, Samsung said, because they deteriorated some of graphene's key qualities.

It could be a major breakthrough for graphene, but it's also a major bummer for technologies companies hoping to cash in on a graphene electronics bonanza.

Handset manufacturer Nokia (soon to be fully owned by Microsoft) has been in the graphene research game since 2006. Last year, the company got a $2.3 billion grant from the European Union's Future and Emerging Technologies program to develop graphene-based technology solutions. Samsung and Sungkyunkwan University's graphene project is funded by Korea’s Ministry of Science, ICT and Future Planning (MSIP), and like Nokia, they have been studying the material since 2006.

It's unclear whether Samsung's breakthrough will give the electronics company the lead in graphene-based electronics, or whether it plans to share the breakthrough. Samsung already makes displays for many of the world's most popular mobile devices, including the iPhone and iPad.

One thing that remains unaddressed in Samsung's announcement is graphene's other critical limitation — one that could prevent it from unseating silicon as the go-to material for electronics. Graphene lacks an "energy gap" and can't be turned on and off. Put simply, it's super-conductive and is never really "off." Transistors work by switching on and off, sometimes millions of times a second, to help power the world's processors.

Some say that "doping" the graphene, or adding chemicals to it to adjust its properties, could help, but it's unclear whether that would change some of its most prized properties, like incredible strength.

The future of technology may, to a certain extent, hinge on researchers figuring this out. Silicon prices are rising as supply steadily dwindles. Companies like Samsung, Nokia, Apple, Qualcomm and others need another option, and nothing is more attractive than graphene. If Samsung truly has cracked this commercial production nut, the future of gadgetry could be very interesting indeed.