A man pumps petrol into a motorcycle at a petrol station in Hanoi December 18, 2013. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SINGAPORE, July 29 — Oil prices slipped further in Asian trade today on fears about global demand but losses were limited by concerns about the crisis in Ukraine, analysts said.

US benchmark West Texas Intermediate for September delivery was down 33 cents at US$101.34 (RM321.70) a barrel in afternoon trade and Brent crude for September tumbled 13 cents at US$107.44.

Singapore’s United Overseas Bank said prices were hit by “concerns about global demand”, but added that fears of tougher western sanctions against Russia over its support of separatist rebels in Ukraine were providing support.

The European Union is expected today to impose fresh measures against Russia, but there are fears the move could hit the struggling eurozone economy as Russia is a key supplier of energy to the region.

Investors are also keeping an eye on the release of key US data, including on second quarter growth and job creation as well as consumer confidence in the world’s biggest oil consumer.

Also, tomrrow will see the US Federal Reserve kick off its latest policy meeting. While bank policymakers are expected to keep interest rates at record lows and further cut their stimulus programme investors are hoping for an indication that monetary policy could be tightened soon.

Singapore banking giant DBS said there should be “no surprises” as the Fed has already signalled that it will end its massive economic stimulus measures later this year.

“The Fed has already telegraphed its intention to end asset purchases by October,” DBS said, adding that its intentions would become clearer during its annual symposium on August 21-23 in Jackson Hole, Wyoming. — AFP