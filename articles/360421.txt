The Federal Trade Commission's (FTC) recent lawsuit against T-Mobile has many wondering, just what does this mean for the future of the company? The nation's fourth-largest carrier is in the midst of merger talks with No. 3 Sprint and a lawsuit doesn't exactly help convince regulators right? If some are right, then there shouldn't be a problem at all.

Quick recap: the FTC accused T-Mobile Tuesday with a lawsuit alleging that the carrier has skimmed hundreds of millions of dollars through "bogus charges" stemming from SMS frauds known as "cramming." Pretty egregious, but it should derail Sprint and T-Mobile talks.

"Complaints like this traditionally are not a serious obstacle to a merger, rather when you have a merger pending before the commission there is tremendous pressure to settle things before the merger goes through," says Harold Feld, senior vice president at consumer advocacy group Public Knowledge.

"If there are arguments that there is a systemic problem [at T-Mobile] it may lead to some kind of merger condition."

The lawsuit brings forth charges against T-Mobile that the company continued to allow fraudulent charges to slip through even after all the major U.S. carriers agreed to stop cramming. T-Mobile is, in fact, in the middle of an outreach-refund process concerning cramming.

"We have seen the complaint filed today by the FTC and find it to be unfounded and without merit. In fact T-Mobile stopped billing for these Premium SMS services last year and launched a proactive program to provide full refunds for any customer that feels that they were charged for something they did not want," reads a response from T-Mobile CEO and president John Legere, who is not at all pleased with the developments.

The FTC lawsuit brings a third major government agency into the fray. The Federal Communications Commission (FCC) and the Department of Justice's antitrust branch are already looking at T-Mobile in regards to a merger with Sprint. Both agencies have to grant approval for a deal to go through. So far, most of the scrutiny falls on consolidation issues -- whether reducing the number of heavy national carriers from four to three is a good idea.

Antitrust telecom law firm lawyer Glenn Manishin from Troutman Sanders, however, thinks that there's enough meat in the two companies' (especially Sprint's) arguments for a merger to pass through.

"There is a good case to be made that they each lack scale on their own to compete with Verizon and AT&T," he said.

Which is exactly what Sprint has been saying for months now.

Both Sprint parent company SoftBank CEO Masayoshi Son and Sprint CEO Dan Hesse have expressed the necessity for both companies to have deeper pockets in order to properly compete in the U.S. wireless market.

"If you have more customers, you can afford to build a larger network," Sprint CEO Dan Hesse told CNET in an interview. "Only then do you have the revenue to justify building in smaller suburbs and rural areas."

"If you live in an urban core, you will have access to AT&T and Verizon and you'll also likely have access to T-Mobile and Sprint. But when you go to less populated areas, Sprint and T-Mobile might not be there."

Verizon is currently the largest U.S. wireless service provider with 122 million subscribers, followed by AT&T with 116 million. Sprint has close to 55 million, while T-Mobile has around 50 million. If you do the math, you'll notice that Sprint and T-Mobile combined are still smaller than AT&T. This creates some serious issues in terms of financial inequality.

"I brought the network war and price war [to Japan]. I'd like to bring that to the States," Son said to industry officials in March. "I would like to provide an alternative to the oligopolistic situation that two-thirds of American households can only get access to one or two providers. I'd like to be a third alternative with 10 times the speed and lower price."

Although nothing has been settled yet, Sprint and T-Mobile are expected to announce a $40 billion deal sometime later this summer, possibly in hopes of riding the wave of recent telecom mergers in play.

For more stories like this, follow us on Twitter!