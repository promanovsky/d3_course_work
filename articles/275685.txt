As headlines cover new releases from the ASCO conference and pharmaceutical company shares resemble heart monitor displays, one acronym stands out: “ASCO”. Here are five things to know about the ASCO to better understand the news, stock actions and advances in oncology.

1. What is the ASCO?

The ASCO stands for the American Society of Clinical Oncology. The Society was founded in 1964 by seven physicians as part of the American Association of Cancer research. According to the ASCO history, the group “recognized the need for the creation of a separate society dedicated to issues unique to clinical oncology.”

Drs. Fred Ansfield, Robert Talley, Harry Bisel, William Wilson, Herman Freckman, Jane Wright and Arnoldus Goudsmit met for the first organization meeting on April 9, 1964. The founding group met at the Edgewater Beach Hotel in Chicago for lunch.

Related: Survey: Is Carl Icahn In Trouble Following The FBI And SEC Probe?

Today, the American Society of Clinical Oncology has almost 35,000 oncology professionals from more than 120 countries with 22 committees. The society is governed by a board of 19 members and 14 directors.

The ASCO's vision statements are as follows,

”All patients with cancer will have lifelong access to high-quality, effective, affordable and compassionate care;

The most accurate cancer information will be available so that patients and physicians can make informed decisions about cancer prevention and treatment;

Information we learn from every patient will be used to accelerate progress against cancer;

Resources will exist to attract the best clinicians and investigators to provide optimal patient care and to conduct transformative research;

ASCO will be recognized as the most trusted source of cancer information worldwide.”

2. What is the ASCO Conference?

Following the first meeting in 1964, the American Society of Clinical Oncology hosts an Annual Meeting.

The 2014 Annual Meeting represents the 50th annual meeting by the Society. More than 25,000 oncology professionals will come together to discuss scientific presentations and educational content from May 30 to June 3. This year's lectures and research follow the theme of “Science and Society.”

Related: Puma Biotechnology Shares Tumble +25 Percent, Citi Remains Optimistic

3. What is Oncology?

Oncology is the branch of medicine which includes the diagnosis and treatment of cancers.

A doctor specializing in the treatment of cancer is called an oncologist. Cancer.net describes the three primary disciplines as medical, surgical and radiation oncology. Gynecologic, pediatric and hematologist-oncologists are also recognized by the American Society of Clinical Oncology.

An oncologist is responsible for a patient from the diagnosis of the cancer throughout the course of the disease. Patients with cancer are often treated by a multidisciplinary team of oncologists, as treatment often involves a combination of chemotherapy, radiation and surgery.

4. How can an investor interpret the medical results?

Throughout the year, pharmaceutical companies and the FDA publish reports on a drug's clinical trials. The trials are conducted in four phases, each of which is designed to answer a separate research question. The trial administers the drug to part of the group, and a placebo to the others.

In Phase I, the drug is given to a small group to determine the safety, dosage amount and possible side effects. Phase II looks at similar question, but given to a larger group of patients. The third Phase has the drug, once again, administered to a large group so that researchers can confirm the efficacy of the drug, side effects, and even compare it to other treatments.

Upon the successful completion of Phase III, the drug is usually marketed. Phase IV studies the effect of the drug is populations and looks for long-term side effects associated with it.

Related: Update Credit Suisse Initiates On Alder Biopharmaceuticals, Optomistic on Derisked Assets

To interpret these results, investors and the general public look for key phrases including “efficacy”, “failure”, “lack of additional side effects”, “serious side effects”, “positive”, “continuing to the next phase” and “ready to market”. Generally, the results of a trial include the percentage of patients who saw a result or serious side effects, compared with the placebo group.

A “good phase result” will likely show a high percent of patients responding well to the drug, limited side effects and the approval to continue to the next phase.

5. How can news surrounding the ASCO affect pharma stocks?

Based on the results of a clinical trial, investors can often understand or predict the trend of a pharmaceutical company.

Pharmaceutical companies make money by selling drugs. As each phase gets approved, the drug is closer to being marketed and, thus, bringing in profit for shareholders.

It is common for “good results” from a trial or a rumor of positive results to cause share prices to gain more than 20 percent. Likewise, serious side effects and concerns over a drug's efficacy have a direct correlation with the pharma company's stock price to drastically fall.