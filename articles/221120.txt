Robin Thicke is looking to win back affection from his estranged wife, debuting a new tune, "Get Her Back," during the upcoming Billboard Music Awards.

"I should've kissed you longer / I should've held you stronger / And I'll wait for forever for you," he sings on the regretful tune.

The pair announced their split in February after nine years of marriage. At recent concerts, Thicke has publicly reached out to Patton to no avail.

Other performers at the event include Jennifer Lopez, Pitbull, 5 Seconds of Summer, Florida Georgia Line, Jason Derulo, Imagine Dragons, Luke Bryan, John Legend and OneRepublic.

The Billboard Music Awards air live on ABC on May 18 at 8 p.m. E.T. from the MGM Grand Garden Arena in Las Vegas.

For comments and feedback contact: editorial@rttnews.com

Entertainment News