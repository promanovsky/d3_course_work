Washington: The world is getting warmer, as greenhouse gases reach historic highs and Arctic sea ice melts, making 2013 one of the hottest years on record, international scientists said today.

The annual State of the Climate report 2013 is a review of scientific data and weather events over the past year, compiled by 425 scientists from 57 countries.

The report looks at essential climate variables, much like a doctor checks a person`s vital signs at an annual checkup, said Tom Karl, director of the National Oceanic and Atmospheric Administration`s National Climatic Data Center.

While Karl declined to give a diagnosis for the planet, he said the report shows some surprises but an ongoing trend that continues the warming pattern seen in recent decades.

"If we want to do an analogy to human health, if we are looking at our weight gain and we are trying to maintain an ideal weight, we are continuing to see ourselves put on more weight from year to year," Karl told reporters.

"The planet, its state of the climate is changing more rapidly in today`s world than in any time in modern civilization," Karl added.

Global temperatures were among the warmest on record worldwide, with four major datasets showing 2013 ranked between second and sixth for all-time heat, the report found.

"Australia observed its warmest year on record, while Argentina had its second warmest and New Zealand its third warmest," said the report.

Sea surface temperatures also rose, making last year among the 10 warmest on record.

The Arctic marked its seventh warmest year since records began in the early 1900s.

Arctic sea ice cover was the sixth lowest since satellite observations began in 1979.

Meanwhile, Antarctic sea ice has been increasing particularly at the end of winter when it is at its maximum, about one to two percent growth per decade.

"This is a conundrum as to why the Arctic ice cover is behaving differently than the Antarctic," said James Renwick, associate professor in the school of geography at Victoria University of Wellington, New Zealand.

The report is published in the peer-reviewed Bulletin of the American Meteorological Society.