On Saturday, Colorado Gov. John Hickenlooper signed a groundbreaking bill into law that gives terminally ill patients access to experimental medicine not yet approved by the Food and Drug Administration.

Under the Right to Try bill, doctors and patients, with the consent and permission of the pharmaceutical company, can receive experimental treatments. Insurance companies do not have to pay for the treatment but the drug manufacturers have to option of charging patients for the medicine or giving it to them for free, Fox News reported.

FDA approval of medication and drugs often times can take months if not years, and time is simply not a luxury many patients have. The Right to Try law allows patients to circumvent some of the red tape that comes along with the approval process.

Democratic State Rep. Joann Ginal, who was a co-sponsor of the bill, did not attend the signature ceremony at Fort Collins because she was tending to her older brother Tom, who suffers from a rare blood cancer.

"Thank you to everyone for passing this bill, which may bring hope to people like Tom when all else faild," Ginal said in a statement read by State Rep. Randy Fischer. "The types of treatments envisioned in this bill gave my brother more time and hopefully will do the same for others."

Colorado remains the first state in the nation to pass such a law, but Missouri and Louisiana are not far behind as lawmakers unanimously passed similar legislation in the past few weeks. Arizona voters will decide at the polls in November if they want their state's version of the law to pass.

While the laws have been met with overwhelming support as they would offer terminally ill patients a fighting chance to survive and beat their diseases, some opponents have said this legislation undermines federal law as well as the FDA's drug development process, according to Fox.