When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

The musician will use a keynote speech at the film and music conference on Tuesday (11Mar14) to unveil his PonoMusic.com service and PonoPlayer device, which is intended to offer a better-quality alternative to iPods and other MP3 players.

A statement issued by Young reads: "It's about the music, real music. We want to move digital music into the 21st century and PonoMusic does that. We couldn't be more excited - not for ourselves, but for those that are moved by what music means in their lives."

Young will set up a Kickstarter.com page on Saturday (15Mar14) to allow fans to reserve the 128GB playback device online, with early customers scoring a discount on the £249 item, which will be capable of storing high-resolution music to allow listeners to experience "studio master-quality digital music at the highest audio fidelity possible, bringing the true emotion and detail of the music, the way the artist recorded it, to life".

The PonoPlayer was developed by Young with engineers at Ayre Acoustics. Young debuted a prototype of the device during a TV appearance in 2012, but plans to launch the player and downloading service in 2013 were subsequently delayed until this year (14).