NEW YORK (TheStreet) - Chipotle (CMG) - Get Report proved that once again sales continue to defy expectations, however the burrito chain fell short of bottom line estimates as operating margins took a hit from higher food costs in the quarter.

Shares were rising 3% to $569.14 before the market opened after Chipotle said that first-quarter net income rose 8.5% to $83.1 million, or $2.64 a share, missing analysts' estimates by 22 cents, according to Thomson Reuters. Yet revenue rose 24.4% to $904.2 million, beating the average estimate of $874 million.

The company said that comparable restaurant sales rose 13.4% in the March-ending quarter, compared to estimates of 8.8%, as tallied by Consensus Metrix. Chipotle attributed the growth to increased traffic and to a lesser extent an increase in average customer checks. Comps also benefited from one extra trading day in the quarter as compared to the first quarter of 2013.

Restaurant level operating margins fell 40 basis points to 25.9% during the quarter, from higher food costs partially offset by "favorable sales leverage" in labor and occupancy costs. Food costs were 34.5% of revenue, an increase of 150 basis points driven by higher commodity costs as a result of "inflationary pressures" in beef, avocados, and cheese prices, the company said.

Chipotle opened 44 new restaurants in the quarter, bringing its total to 1,637. Chipotle restaurants are company owned stores, not franchised like other fast-casual eateries.

"We are delighted that more and more people are choosing to visit our restaurants every day allowing us to deliver double digit comps during the quarter. Our food culture has always been a defining characteristic of Chipotle and continues to set us apart from other restaurants. We are confident that our special food culture will continue to attract more customers to visit Chipotle as customers better understand and connect how natural and high quality ingredients that are freshly prepared result in better tasting food," said Steve Ells, Chipotle's founder, chairman and CEO.

Denver-based Chipotle also said that its board of directors approved the investment of up to an additional $100 million, exclusive of commissions, to repurchase shares of our common stock.

The company reiterated that it expects to open between 180 and 195 stores this year. It also said it expects comparable sales to be in the high single digits, excluding any menu price increases. Chipotle is set to hold a conference call starting at 11 a.m. EST to discuss the results.

--Written by Laurie Kulikowski in New York.

Follow @LKulikowski

Disclosure: TheStreet's editorial policy prohibits staff editors, reporters and analysts from holding positions in any individual stocks.