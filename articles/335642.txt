A fresh report on Samsung Galaxy Note 4 has surfaced online via South Korean publication ETNews (Google translated), which suggests that the company may be shipping the phablet with a 12-megapixel rear camera.

In other words, it is speculated that Samsung will be trading off a few megapixels sensor resolution on the phablet's primary camera to accommodate the fully optimised optical image stabilisation (OIS) feature in a thinner frame.

According to G4Games, the 12MP main camera will be paired with a 3.7MP front-facing camera, while speculation is rife that Samsung has already set up production facilities to start mass producing the 12MP unit at a facility in Tianjin, China.

The company is expected to make further investments in the factory infrastructure for a net yield of 1 to 3 million units per month.

The latest rumour, however, should be taken with a pinch of salt as it contradicts Samsung's tradition of using the same camera unit (16MP sensor) on the Note series as its Galaxy S flagship smartphone.

Furthermore, it is ascertained that the latest speculation also contradicts earlier leaks and rumours wherein the Note 4 showed the presence of a 16MP main camera in a recent AnTuTu benchmark.

Among other rumoured specifications, the Note 4 is expected to release in two models – SM-N910S and SM-N910C. The latter will ship with Exynos 5433 chipset featuring an octa-core processor with ARM based Mali-T760 GPU, whereas the former will be powered by Snapdragon 805 APQ8084 Krait 450 chipset based quad-core processor clocked at 2.5GHz and paired with Adreno 420 graphics unit.

Both the Note 4 variants are expected to feature 5.7in displays with quad HD screen resolutions (2560 x 1440 pixels) and include 3GB of RAM as well as 32GB of internal storage.

It is widely speculated that the Samsung Galaxy Note 4 could be officially unveiled at the upcoming IFA annual conference in Berlin with its market availability date set for 25 September.