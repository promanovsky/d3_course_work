NEW YORK (TheStreet) -- Exxon Mobil (XOM) - Get Report shares fell, down -4.2% to $98.94, after reporting a second quarter drop in oil and gas production that resulted in its lowest output in almost five years.

Oil and gas output during the quarter was down 5.7% to an equivalent of 3.84 million barrels a day, short of analyst expectations of 3.96 million barrels a day.

Compounding the decline, U.S. and EU sanctions against Russia announced on Tuesday could hurt the company's collaboration with Russia which holds an estimated $8 trillion worth of crude underground, according to Bloomberg.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates EXXON MOBIL CORP as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate EXXON MOBIL CORP (XOM) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its attractive valuation levels, good cash flow from operations, increase in stock price during the past year and largely solid financial position with reasonable debt levels by most measures. We feel these strengths outweigh the fact that the company shows low profit margins."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

Net operating cash flow has increased to $15,103.00 million or 11.11% when compared to the same quarter last year. Despite an increase in cash flow, EXXON MOBIL CORP's average is still marginally south of the industry average growth rate of 16.72%.

Compared to where it was 12 months ago, the stock is up, but it has so far lagged the appreciation in the S&P 500. Turning our attention to the future direction of the stock, it goes without saying that even the best stocks can fall in an overall down market. However, in any other environment, this stock still has good upside potential despite the fact that it has already risen in the past year.

XOM's debt-to-equity ratio is very low at 0.12 and is currently below that of the industry average, implying that there has been very successful management of debt levels. Despite the fact that XOM's debt-to-equity ratio is low, the quick ratio, which is currently 0.55, displays a potential problem in covering short-term cash needs.

Regardless of the drop in revenue, the company managed to outperform against the industry average of 3.5%. Since the same quarter one year prior, revenues slightly dropped by 2.1%. The declining revenue appears to have seeped down to the company's bottom line, decreasing earnings per share.

You can view the full analysis from the report here: XOM Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet's Joe Deaux has details on Exxon's Q2 results:



WATCH: More market update videos on TheStreet TV | More videos from Joe Deaux