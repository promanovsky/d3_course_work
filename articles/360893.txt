We know David Arquette and his ex-wife Courteney Cox are pretty close, but could a double wedding be on the cards? Well, maybe they’re not that close, but it’s a little strange that the former couple would both become engaged within a few short days of each other.

Yes, that’s right: David Arquette and his beautiful girlfriend Christina McLarty have taken their relationship to the next level and have become engaged.

The Scream 4 actor popped the question to his entertainment reporter lady during a family dinner in Malibu, California, with their newborn son Charlie West and Arquette’s and Cox’s daughter Coco present, E! News reports.

The dinner took place with a gorgeous ocean view as the backdrop around 6:10 p.m., and at sunset, the romantic star decided to get down on one knee and ask McLarty to be his wife — hot on the heels of his ex-wife’s engagement to Snow Patrol rocker Johnny McDaid.

Courteney Cox engaged to Snow Patrol rocker Johnny McDaid >>

“Coco, did you know?” McLarty asked her fiancé’s daughter. The 8-year-old replied, “Maybe,” and then giggled, “Engaged! Engaged!”

As for the engagement bauble? Apparently, the ring had a large blue center stone.

It seemed like this day was bound to come soon, though because the loved-up pair have been dating since the summer of 2011.

Courteney Cox, David Arquette and their modern-day family >>

“She’s adorable. I’m not one to run around with different girls. I like someone that makes me feel good and that I can make feel good,” Arquette said at the time during a radio interview with Howard Stern.

Maybe the happy couple will add to their growing little family, too, in the near future, having just welcomed their first child together in April. Whatever is in store next for this pair, we wish them all the best in their futures together.