The euro declined against most major currencies in European deals on Tuesday following comments of European Central Bank governing council member and Bundesbank chief Jens Weidmann that negative rates are appropriate to prevent the euro's strength.

The euro declined to 1.3805 against the greenback and 141.08 against the yen. Against the pound, the euro eased to 0.8368, from an early 6-day high of 0.8394.

The next possible downside target for the euro is seen around 1.37 against the greenback, 0.83 against the pound and 140.00 against the yen.

For comments and feedback contact: editorial@rttnews.com

Forex News