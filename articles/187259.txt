In theaters Friday, May 9

2 1/2 stars (out of 4 stars)

In terms of fun, this frat comedy ranks somewhere between a friendly game of beer pong and a raucous toga party. But it does deserve credit for giving it the old college try.

Seth Rogen and Rose Byrne are Mac and Kelly, your typical married couple living in the suburbs. Life is good: They have an adorable baby daughter, their house is cozy, and judging by the first scene, they still can get it on without abandon. Ok, maybe they're still attached to their youth — but hey, what 30-something wants to wake up and be a fully domesticated adult?

One sunny day, a moving truck pulls up next door: Say what's up to the rowdy men of Delta Psi Beta, led by Zac Efron's Teddy. (Marvels Rogen upon seeing the beefy stud: "He looks like something a gay guy designed in a laboratory.") Yep, these bros just wanna rock 'n' roll all night and party every day. Their first strategy to subdue the noisy neighbors is to play it cool. Befriend them. Maybe party with them. Definitely bring over weed. A night later, the thumping is unbearable. After a police intervention fails spectacularly, they plot the animal house's demise. The turf war is on!

To be sure, this premise is as flimsy as a fake ID. Mac and Kelly devise a plan; the guys thwart it; round and round they go. Of course they could always just move, but then the credits would just roll after 20 minutes. (FYI, their realtor discourages it.) And, not to nitpick any semblance of logic in this kind of film, but how convenient that none of the other neighbors on the block are perturbed by the blasting music or the beer cans strewn on the lawn.

Yet, surprisingly, the flick zips along with the help of some witty jokes and set pieces. (No typical dumb-jock film would dare incorporate a party scene in which all the guys dress as their favorite Robert De Niro movie characters.) Rogen and Efron make for charming buds and adversaries, whether they're riffing on the best movie Batman or throwing punches. Though the still-boyish Efron has struggled to find his post-High School Musical footing, he gives his most endearing performance to date. (Leave it to Rogen, always an open and relaxed onscreen presence, to bring out his inner-class clown.)

It's the contrived racy moments that bore to the core. Really, is a zoom-in shot of Byrne's veiny, lactating breasts supposed to be hilarious? How about the guys putting plaster over their. . . well, never mind. These rated-R gags were daring, say, circa 1999 — and now seem like a desperate attempt to drum up Twitter buzz. That said, there's absolutely no justification for a sequence in which Mac and Kelly's infant innocently puts a condom in her mouth on the driveway, prompting the pair to lose their minds because they think she might have contacted HIV.

Ah well. Amid all the juvenile antics, the flick goes down easy. It doesn't even leave one hint of a hangover.