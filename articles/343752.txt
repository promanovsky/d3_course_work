The testimony of Oscar Pistorius's manager Peet Van Zyle revealed to the court details about the runner's obsession with safety - part of a raft of revelations and details that moved Pistorius to tears.

Van Zyl described how Pistorius was paranoid about his safety and was nervous in public places. During a trip to New York Pistorius seized Van Zyl's arm in fright at the sound of a loud noise.

"It was very strange he would grab my arm, being a young male," Van Zyl said under questioning by prosecutor Gerrie Nel.

Pistorius's obsession about safety was also apparent when he stayed foreign hotels where he made sure his bedroom door was locked at all times, said his manager.

Pistorius had a "heightened sense of awareness", which was manifested in behaviour such as driving fast to avoid being followed, parking only in well-lit areas, never answering doors without the latch on, noted Van Zyl.

When dining at restaurants he always picked a seat with the best view of the room. "It was interesting he always wanted to be seated so that he could see what was happening," VanZyl said.

Van Zyl also stated that Pistorius rarely lost his temper, and had on just two occasions: when he was accused of cheating for wanting to race able-bodied athletes on his blades, and also at a BBC journalist.

Van Zyle also said Pistorius and Steenkamp enjoyed a close and loving relationship. The paralympian regularly told Van Zyl about his love for the glamorous model and how he wanted to take her on trips, the manager stated

It was revealed that Pistorius had been very excited by Van Zyl's suggestion that he should take Steenkamp, 29, to a concert by opera singer Andrea Bocelli.

It was also revealed that Pistorius planned to retire from athletics in 2017.

The 2012 London Olympics were a high point for Pistorius, according to Van Zyl. "His profile was raised to global sports icon status, " he said.

"The London Olympics was about two people: Usain Bolt and Oscar Pistorius."

Van Zyle is described as being like a father figure to Pistorius. He was one of three people Pistorius on the night of the shooting, along with Pistorious's brother Carl and friend Justin Divaris.

The court was told yesterday Pistorius was not suffering from a mental disorder when he killed Steenkamp.

He denies murdering her in the early hours of Valentine's Day. He claims he thought she was an intruder in his bathroom. The prosecution claims he blasted her three times following a row between the couple.

The trial continues.

andrea bocelli