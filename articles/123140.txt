With rumours of AC/DC calling it quits swirling around the Internet, frontman Brian Johnson has shot down speculation that the band is retiring.

The band has revealed, however, that founding member Malcolm Young is taking a break from the band due to “ill health.”

In an exclusive interview with The Telegraph released Wednesday, Johnson said the band is still planning to get together to work on new material.

“We are definitely getting together in May in Vancouver,” Johnson told The Telegraph. “We’re going to pick up some guitars, have a plonk, and see if anybody has got any tunes or ideas. If anything happens, we’ll record it.”

He made the claim just days after Australian radio station 6PR reported that one of AC/DC’s members was sick. Johnson confirmed to The Telegraph that “one of the boys has a debilitating illness” but declined to say who.



That news was later announced via the band’s Facebook page.

“After forty years of life dedicated to AC/DC, guitarist and founding member Malcolm Young is taking a break from the band due to ill health. Malcolm would like to thank the group’s diehard legions of fans worldwide for their never-ending love and support,” the band said in a short statement posted Wednesday morning.



“In light of this news, AC/DC asks that Malcolm and his family’s privacy be respected during this time. The band will continue to make music.”

One of the most successful bands of all time, AC/DC has sold more than 200 million records since its inception in 1973. Earlier this year, reports surfaced that the band would embark on a tour to celebrate their 40th anniversary.

In an interview with iHeartRadio in February, Johnson said the band would be booking studio time in Vancouver starting in May.