Researchers are tracking a great white shark that has traveled nearly 4,000 miles to South Florida waters after she was tagged in Massachusetts.

The shark, named "Katharine" by nonprofit shark research group OCEARCH, was last tracked by satellite Sunday at 11:05 a.m. in waters just east of Biscayne National Park.

OCEARCH has been following Katharine since August 19, when she was tagged in Cape Cod, Mass.

The shark is about 14-feet long and weighs about 2,300 pounds. She has traveled a total of 3,685 miles since she was tagged nine months ago.

Katharine the shark also has a Twitter feed, where comical posts can be found, such as, "I like spots where no one bugs me," and, "You know I'm not the only one here, right?"

For more on Katharine and other great white sharks, visit OCEARCH.org