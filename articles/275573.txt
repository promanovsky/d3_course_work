A controversial new fertility procedure, dubbed “Three-Parent IVF”, that uses two embryos to form one healthy baby, may be a reality in the UK within the next two years. According to the BBC, fertility specialists have figured out a way for babies to contain genetic material from three biological parents.

There are two techniques, according to the BBC. One would combine materials from the eggs of two women and then fertilize the newly assembled egg with the sperm from one male. Another more controversial method involves using material from two separate fertilized embryos to form one modified embryo, which will develop into one baby. The goal of both methods would not be to create confusion in the nuclear family structure or offer polygamists an opportunity to all create new life together; the process is an attempt to prevent deadly mitochondrial disease from passing from mother to child.

The BBC reports that one out of every 6,500 babies are born with severe mitochondrial disease. The disease leads to health issues like weakened muscles, blindness and death. These babies, due to the disease passed to them genetically from their mothers, causes babies to have insufficient energy to carry out normal body functions. The goal of the new process, that would use cell-material from three biological parents, is to allow women who carry the genetics responsible for the disease to give birth to healthy children.

Both methods are controversial, but the second process is the more controversial fertility procedure of the two. Two embryos are initially created by fertilizing two separate eggs with a father’s sperm. The first embryo’s egg would come from the hopeful mother who may pass along mitochondrial disease. The second embryo’s egg would come from a donor mother.

After two embryos are formed from the fertilized eggs, the pronuclei are removed from both of the embryos. Only the intended parents’ pronuclei is kept, because the pronuclei contains the genetic information itself. Then, the intended parents’ genetic information is put into the embryo made from the donor egg and the father’s sperm. This way, the genes of the intended parents are carried on within the healthy embryo. The extra embryo, sans pronuclei created from the father and the mother and containing unhealthy mitochondria, is discarded. The left-over pronuclei from the embryo created by the father and the donor mother is similarly discarded. The all new embryo, containing material from the sex cells of three parents, is then placed inside the intended mother. If everything works as planned, the three-parent embryo will implant and grow into a healthy fetus.

Scientists estimate this process is expected to be available in the UK in the next two years.

According to the BBC, Professor Robin Lovell-Badge, from the Medical Research Council, said, “The direction of travel still suggests that it is all safe, but we don’t know what’s round the corner so we’re being a little cautious.”

More tests will be needed. Scientists also are unsure what risks may befall babies in subsequent generations, and they need to investigate this concern in more detail as well.

The BBC explained what a baby born with material from two embryos would look like genetically:

“Mitochondria contain their own genes in their own set of DNA. It means any babies produced would contain genetic material from three people. The vast majority would come from the mother and father, but also mitochondrial DNA from the donor woman. This would be a permanent form of genetic modification, which would be passed down through the generations.”

Professor Andy Greenfield chaired the scientific review panel and asked “Are these techniques safe in humans? We won’t know that until it’s actually done in humans.”

He added, “Until a healthy baby is born we cannot say 100 percent that these techniques are safe, if you think back to when IVF was a new technology all of these questions were asked before IVF.”

The BBC reported that it appears as though the UK government is behind the procedure that would take material from two embryos to create one baby.

Chief medical officer Professor Dame Sally Davies said last year, “It’s only right that we look to introduce this life-saving treatment as soon as we can.”

The UK Department of Health plans to publish its official response in the coming months. A UK Department of Health spokesperson explained, “Mitochondrial donation will give women who carry severe mitochondrial disease the opportunity to have children without passing on devastating genetic disorders.”

Keeping the UK in the lead in the fertility-treatment arena is also an important consideration. According to Reuters, the UK government said in 2013 that was drafting legislation that would allow the treatments if scientists were able to show the procedure was safe and effective. Meanwhile, across the pond, the United States FDA has put together a committee to determine if similar trials should begin in the states.

Inquisitr want to know what you think of these procedures. How do you feel about taking cellular material from two separate embryos just after fertilization that were made from three parents in order to create a healthier child for women at risk of passing along mitochondria disease?