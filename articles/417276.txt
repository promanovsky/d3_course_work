Visual artists walk with signs that read in French, "Ebola leave us alone," during a rally in Abidjan, Ivory Coast, to draw attention to the plight of people suffering from the outbreak.

Sept. 4, 2014 Visual artists walk with signs that read in French, "Ebola leave us alone," during a rally in Abidjan, Ivory Coast, to draw attention to the plight of people suffering from the outbreak. Sia Kambou/AFP/Getty Images

Governments move to stop the epidemic from spreading further as the death toll from the virus continues to grow.

Governments move to stop the latest outbreak from spreading further as death toll from virus tops 1,300.

Governments move to stop the latest outbreak from spreading further as death toll from virus tops 1,300.

Tom R. Frieden, director of the U.S. Centers for Disease Control and Prevention, was supposed to fly to West Africa on Monday to gauge the effects of the world’s worst Ebola outbreak.

Then his flight was canceled.

Brussels Airlines was forced to halt flights to the affected region after Senegal’s refusal over the weekend to allow the Belgium-based carrier to touch down in Senegal’s capital, Dakar, for crew changes. Senegal was sending a clear signal that it wanted nothing to do with flights going to Liberia, Guinea or Sierra Leone, where the outbreak rages on. This move was just the latest by a growing wave of countries and airlines that appear to want to stave off the Ebola threat by stopping travel in and out of places confronting the virus.

Frieden, along with other U.S. officials, scrambled to jump on one of the region’s few remaining options — a Delta flight to the Liberian capital that arrived Sunday, said Jeremy Konyndyk, director of the U.S. Agency for International Development’s Office of Foreign Disaster Assistance, who was part of the mad dash.

The increasing flight cancellations and border closures have alarmed public health officials worldwide. The cancellations have frightened residents in the affected countries who fear they may be trapped within their national borders. They also worry about the economic fallout from their dwindling air access to other countries.

The World Health Organization warned Monday that these curbs will only make the outbreak harder to deal with. That message was reinforced by the United Nations, which said that moving in medical supplies and personnel was being “severely hampered” by the restrictions.

David Nabarro, who is leading WHO’s Ebola response effort, acknowledged Monday that the unprecedented scale of the outbreak is scary but stressed the importance of maintaining regular air routes and normal borders.

“Yes, we understand it,” Nabarro said in Freetown, referring to the restrictions placed by regional countries. “But on the other hand, it’s making the job a whole lot harder.”

Keiji Fukuda, WHO’s assistant director general for health security, said it was essential to restore the confidence of airlines and foreign governments. Passengers are screened for fever and other symptoms before boarding planes in the Ebola-affected countries. And even if an Ebola-stricken passenger found a seat on an aircraft, transmission of the virus — which is not airborne and which requires direct contact with bodily fluids — is difficult.

Passengers and crew should not be alarmed, Fukuda said. “We believe they are at very low risk of getting Ebola,” he said.

Last month, Asky Airlines stopped flying to Liberia and Sierra Leone after a Liberian passenger on a flight to Lagos, Nigeria, died from Ebola and sparked a small outbreak in that country. Nigeria’s largest flight operator, Arik Air, stopped flying to Liberia and Sierra Leone. Then Emirates became the first major player to drop West Africa. Air Cote d’Ivoire also grounded flights, as did British Airways. Ebola prompted Korean Air to halt its three weekly flights to Nairobi, even though Kenya is on the opposite side of the African continent.

Borders are being closed, too. South Africa banned travelers from Guinea, Liberia and Sierra Leone. Senegal closed its border with Guinea. Chad closed its border with Nigeria.

Last week, Kenya Airways stopped flying to Liberia and Sierra Leone. The small airliner Gambia Bird had already stopped flights to the affected region.

In Sierra Leone, just two commercial airliners — Royal Air Maroc and Air France — still serve Lungi International Airport.

Air France, which flies to Guinea and Sierra Leone, is under pressure from a crew union to stop its flights because some staff members are worried about being exposed to the deadly virus. The union called the continuation of flights “inconceivable” given the risks.

But Air France has said that it has studied the issue and thinks it can safely maintain its schedule.

In Freetown, uncertainty about flight options has fueled rumors. The few remaining flights are booked solid. Residents who might otherwise stick around are pondering jumping on a plane just so they are not stuck if the airport goes silent.

“It’s very manic. It’s been a headache,” said Geoffrey Awoonor-Renner, who runs VSL Travel, a travel-related services agency, in Freetown.

The travel agent was dealing with one client, eager to get to Europe, who had booked flights twice — on Gambia Bird and then on Brussels Airlines — only to see the carrier cancel on him. He cannot find another flight.

“We’re scrambling,” Awoonor-Renner said.

The flight situation also has proved to be a burden for the WHO delegation now visiting Freetown. The team members were supposed to fly out on Brussels Airlines. On Monday, there was word that the carrier was negotiating with another country for access so crew changes could be made. Maybe flights would resume.

In the meantime, WHO officials were grounded. They were hoping that a small U.N. plane might become available.