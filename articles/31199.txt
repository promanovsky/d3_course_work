tech2 News Staff

In a move to get more people on Windows 8, specially those still using the 13-year old Windows XP, Microsoft has come out with a strategy. It is offering a discount of $100 (approx Rs 6100) if you get a new PC with Windows 8 on-board.

Microsoft store is running this promotion as it approaches closer to April 8th, the day when official support for Windows XP will end. The offer is valid on machines that cost over $699 (approx Rs 42500) or more which are purchased via the online store. You also get 90 days of tech support and a utility for migrating your files for free. This offer is expected to run til 15th June, which is still 9 weeks from the date of the end of Windows XP support.

According to recent estimates, around 15 per cent PCs running Windows still run on XP and naturally Microsoft is desperate to get more users on Windows 8.

It is not just consumers who are on Windows XP, but also a lot of commercial establishments that have yet to upgrade. In fact, many public as well as private commercial institutions are yet to upgrade. Most alarmingly, Windows XP still powers most of the world’s ATM machines- estimated to be about 95 percent – which puts your friendly neighbourhood cash machine in grave danger of being used by hackers to steal financial data. Microsoft plans to pull plug on Windows XP on 8 April, but will continue to support its security products through July 2015. However, the company has warned, “the effectiveness of antimalware solutions on out-of-support operating systems is limited.”