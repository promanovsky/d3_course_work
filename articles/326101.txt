BRAZZAVILLE, Republic of the Congo, June 26 (UPI) -- The World Health Organization said it was "gravely concerned" about the "further international spread" of the Ebola virus, already responsible for some 400 deaths. In a message released Thursday, the group called for "drastic action" to stymie the spread.

The outbreak began in the West African country of Guinea; the disease has since spread to neighboring nations Sierra Leone and Liberia. Officials remain worried that the virus could move elsewhere.

Advertisement

"There is an urgent need to intensify response efforts...this is the only way that the outbreak will be effectively addressed," WHO officials pleaded.

WHO has already sent 150 experts West Africa to try to slow the spread. But even though officials last week denied the virus was increasing its pace of infection, their recent statement acknowledged that: "there has been significant increase in the number of daily reported cases and deaths."

The state from WHO comes only days after the international aid group Doctors Without Borders claimed the outbreak was "out of control."

And though it's quickly become the single most deadly outbreak in Ebola's history, it could be worse. Previous outbreaks have featured mortality rates close to 95 percent. The current outbreak has killed roughly 60 percent of those infected.

No vaccine or cure currently exists for the flu-like virus, which causes fever with chills, joint pain, muscle pain and chest pain.

But even as international groups like WHO and other aid workers sound the alarms, experts say the chance Ebola making its way to the U.S. or Europe remains quite low -- most travel in the region is local.