Facebook Q1 Revenue Surges 72 Percent

Stock Markets Apr 23, 2014 04:45PM ET

Facebook Q1 Revenue Surges 72 Percent By Reuters - SAN FRANCISCO (Reuters) - Facebook Inc ( ) mobile advertising business continued to accelerate in the first three months of the year, helping the Internet social networking company top Wall Street's revenue target. Facebook said that mobile ads represented 59 percent of its ad revenue in the first quarter, up from 30 percent in the year-ago period. Facebook's overall revenue grew 72 percent year-on-year to $2.5 billion in the first quarter, above the $2.36 billion expected by analysts polled by Thomson Reuters I/B/E/S.

Facebook Q1 Revenue Surges 72 Percent

Related Articles