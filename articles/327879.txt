LG has jumped into the wearable space with its new G Watch powered by Google's latest Android Wear platform.

LG, the renowned smartphone maker, is adding a new category to its product portfolio, wearables. By introducing the latest G Watch at the annual Google I/O conference, Thursday, LG becomes one of the first smartwatch brands powered by Google's Android Wear platform. The new addition comes alongside the announcement of Samsung Gear Live, which also runs on Android Wear, at the I/O keynote.

The smartwatches are setting a new trend among smartphone users. One can get all the necessary information on the wrists at the right time with the new range of smartwatches. LG's new G Watch is just one such example that pairs with any Android 4.3-powered smartphone and is up for grabs in the U.S. and 11 other countries.

"As one of the first to market with Android Wear, LG wanted to develop a product that functions as an essential companion device but most importantly, was simple to use," Dr. Jong-seok Park, president and CEO of the LG Electronics Mobile Communications Company, said in a press release. "We wanted a device that would be simple to learn and so intuitive that users wouldn't even have to think about how to use its features. That's what the LG G Watch is all about."

LG G Watch is powered by 1.2GHz Qualcomm Snapdragon 400 processor with 512MB RAM and 4GB storage. The G watch is expected to run longer as it is equipped with 400mAh power battery and the 1.65-inch always on display sports 280 x 280 pixels, compared to Gear Live's 320 pixels resolution and 300mAh battery. The LG watch uses Bluetooth 4.0 LE connection to pair with smartphones and mirror the activities. It h as voice recognition, which helped cut down the buttons on the watch. The IP67 certification will keep the G watch dust and water resistant.

LG also offers changeable 22mm watch straps, but it will ship with a matching silicone strap. It comes in two colors, black titan and white gold and the G Watch can be pre-ordered via Google Play Store for $229 in 12 markets including the US, Canada, UK, France, Germany, Italy, Spain, South Korea and Japan. It will soon expand to retailers in 27 markets such as Australia, Brazil and Russia, LG said.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.