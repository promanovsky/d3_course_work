North American stock markets regained ground Friday after nervousness about Chinese growth, corporate earnings and stock valuations shook investors on Thursday.

The S&P/TSX composite index was down 11.32 points to 14,296.78.

The Canadian dollar dropped 0.22 of a cent to 91.20 cents (U.S.).

Story continues below advertisement

A disappointing earnings report before the open from banking giant JPMorgan Chase helped push the Dow Jones industrials down 20.78 points Friday morning to 16,149.44, the Nasdaq composite index was up 10.31 points to 4,064.42 and the S&P 500 index climbed 1.19 points to 1,834.27.

JPMorgan Chase shares backed off 3.16 per cent to $55.58 as it reported its first-quarter profit fell 19 per cent to US$5.3-billion, or $1.28 a share. Revenue fell eight per cent to $22.99-billion, led by weak trading revenue. Analysts had expected earnings of $1.39 a share on revenue of $24.43-billion, according to FactSet.

On Thursday, the TSX dropped 128 points and Dow tumbled 267 points following disappointing trade data from China that raised another round of questions about the health of the world's second-biggest economy. Traders are now looking ahead to next week when the Chinese government releases its first-quarter growth figures.

There was another sign of economic weakness in data out Friday which showed that China's growth in auto sales decelerated further in March with sales up 7.9 per cent to 1.7 million vehicles, down from February's 11.3 per cent growth.

Biotech and technology sectors were particularly in investors' cross hairs.

After making big gains last year, biotechs have been crushed in recent weeks as they come under pressure to lower prices for their drugs. And traders continued to punish some of the biggest tech high flyers from last year, including Facebook and Google.

"A grab-bag of concerns have suddenly come into focus," said BMO Capital Markets senior economist Robert Kavcic.

Story continues below advertisement

"With valuations richer than they've been through much of the recovery to date and sentiment high, the end of (the Federal Reserve's quantitative easing) and an eventual shift to tightening could be a tough pill for stocks to take in the near term."

The industrials sector led decliners, down 0.7 per cent after Caterpillar heavy equipment dealer Finning International Inc. provided preliminary revenue numbers for the first quarter of 2014. Its shares fell 2.85 per cent to $28.61 (Canadian) as it said that new equipment revenues in South America were significantly lower than the prior year quarter. But it added that, on a consolidated basis, revenues for the 2014 first quarter are expected to be approximately $1.676-billion, up eight per cent from a year ago.

The financials sector was also tepid, down 0.5 per cent following the JPMorgan Chase report.

The energy sector was off 0.1 per cent while the April crude contract on the New York Mercantile Exchange edged up 52 cents to $103.92 (U.S.) a barrel.

The gold sector led advancers, up 0.65 per cent while June bullion faded $1.80 to $1,318.70 an ounce.

The base metals group was up 0.47 per cent while May copper shed early gains and was unchanged at $3.04 a pound.