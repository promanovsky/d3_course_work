WHY is Pod such a sex god?

That question has been bugging Game of Thrones fans for more than a year. Early in season three, Podrick Payne’s boss, Tyrion, tried to reward him for a job well done by paying for some “quality time” in a brothel. This is Westeros, remember.

MORE: Did Lena Headey spoil the Game of Thrones finale?

Tyrion took the young man into the brothel and left him in the presence of several scantily clad women. You can guess what happened next.

When Pod eventually emerged from the place, he was carrying a bag full of Tyrion’s cash. Apparently, the women had enjoyed their time with him so much that they made him take the money back.

What in R’hllor’s name did shy, modest young Podrick to do those women? Nobody knew, but everyone wondered.

Ahead of today’s Game of Thrones finale, news.com.au interviewed Daniel Portman, the 22-year-old Scottish actor who plays Pod on the show.

Daniel is coming to Australia for Oz Comic-Con Melbourne, which will take place in the city’s Royal Exhibition Building on July 5 and 6. You can buy tickets here, if you’re so inclined.

MORE: Game of Thrones, episode nine, reviewed

We asked Daniel what it was like to work on set alongside Peter Dinklage. We explored the inner depths of his character’s mind. And yes, we asked him what really happened inside that brothel.

SAM: Pod seemed to be pretty good with a spear during the Battle of the Blackwater. Does he have a hidden, dangerous side?

DANIEL: It was the fight or flight instinct, I reckon. He just saw the only guy that he had any sort of nice relationship with (Tyrion), the only guy who looked after him was about to get killed, and he didn’t want to let that happen. The way I played it was that really an instinct took over, and he stabs the guy with the spear, and as soon as he’s done it he snaps out of that almost, sort of adrenalin rush. He goes into a sort of, different mode. As soon as it’s happened, it’s almost as though he’s back to being that shy lad.

SAM: I love the odd couples in Game of Thrones. You’re in one of them, with Brienne. Sandor and Arya are there as well. Why does that dynamic between Pod and Brienne work so well?

DANIEL: I think it’s been a series of discovery in terms of what Podrick’s like, because you really don’t know much about him until this year. He’s just standing in the background, and he’s done whatever he’s done in the brothel, but the general assumption is that he’s not very sharp. But as the series has developed, he’s actually been quite useful. He knows things. He’s been in a lot of conversations with powerful people. He’s much smarter than everybody thinks. He knows about quite a lot of things that Brienne wouldn’t know about.

SAM: Have you enjoyed working alongside Gwendoline Christie (Brienne)?

DANIEL: It was really nice for me actually, to get the chance to work with somebody else, because for the first two series I was involved in I worked solely with Jerome Flynn (Bronn) and Peter Dinklage (Tyrion). We had a great time working together, but it’s nice to mix things up. Gwendoline and I hit it off straight away.

SAM: As you said, you did a lot of work alongside Peter Dinklage. What’s the guy like off camera?

DANIEL: He’s exactly as you would expect. You know, he’s charming, funny, he takes his work very seriously, but not too seriously. He’s up for having a laugh. He’s just a genuinely good bloke, and we had a great time together. Peter was very good in terms of helping me settle in and helping me not be nervous about it. The same with Jerome, you know, Jerome was very good.

SAM: There seems to be a lot secretly going on inside Pod’s head. What does he really want out of life?

DANIEL: At the moment, where we are in season four, I would say Pod wants to be back in King’s Landing with Tyrion. What he wants to be most of all is to be with his father figure type person, one of his friends, you know? But to be honest, aside from that, I think he wants to be a good guy. He’s always wanted to be a knight, more than anything. I think he puts people above himself. And he’s quite unique in Westeros, in that regard.

SAM: His facial expressions are also pretty unique. I feel like they really sum up Pod’s character. Do you put a lot of thought into those expressions?

DANIEL: It’s really better to be organic about it. Partly it’s the writers writing it in, and partly it’s me experimenting with stuff, and partly it’s the director giving it the direction to go in. I think it just happens, so far. I don’t spend so much time thinking about it.

SAM: Have you read the books? Do you know what’s coming over the next few seasons, or are you scratching around in the dark like me?

DANIEL: I’ve read the first four books, up until A Feast for Crows. But really, as we’ve seen in the show thus far, they’re starting to deviate from the storyline in the books here and there, so everyone’s really in the dark.

SAM: I believe your dad played Rodrik Cassel on the show. You play Podrick. Rodrik and Podrick! How the hell did that happen?

DANIEL: It’s a big fat coincidence! It’s a really nice coincidence actually. I started filming series two when he was finishing his time on the show, so it was really nice to cross over.

SAM: Finally, I have to ask ... what really happened in that brothel? What makes Pod such a sex god?

DANIEL: He’s a listener. He’s a listener. He’s a very tender, nice young man who knows what he’s doing. Like in the battle, he goes into a different mode and lets his inner animal take over.

There you have it. Not as specific or as graphic as we would have liked, perhaps, but your imagination has plenty to go on nonetheless.

In the meantime, tell us what you think of Podrick, and Daniel Portman, on Twitter: @SamClench | @DanielPortman | @newscomauHQ