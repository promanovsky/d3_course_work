'The way you love our daughter fills me

with so much love!' Kim Kardashian shares adorable photo of Kanye West napping

with North on Father's Day



It seems that Father's Day tuckered out poor old Kanye West.



With the celebration falling on the same day as daughter North's first birthday, the doting daddy succumbed to a snooze as their fun-filled day came to an end - watching a sports game from the comfort of his bed with his mini-me tucked sweetly under his arm.



Of course, snap-happy Kim Kardashian couldn't resist sharing a sweet photo with fans on social media of her husband and precious daughter lying fast asleep in their matching grey outfits.

Scroll down for video



Adorable! Kim Kardashian shared an adorable photo of Kanye West and the couple's daughter, North, fast asleep in bed dressed in matching grey outfits after a tiring day celebrating both Father's Day and the precious little girl's first birthday in New York City on Sunday



It's clear the 33-year-old reality star was overflowing with love as she provided an elaborate caption with the heartwarming snapshot.



'This is what life is about! Our baby girl turned 1 today! We played so hard they passed out while we were watching the game!'



She continued: 'Happy Father's Day to the best daddy in the world! The way you love our daughter and protect her makes me filled with so much love! #BestDayEver #Twins #HappyFathersDay #HappyBirthday.'

In the photo, Kanye reclined beneath plush white bedding wearing a snug-fitting grey T-shirt, with

a gold chain around his neck.

'Happy Fathers Day #KanyeWest !!!! You are an amazing Daddy and I love you!!!! #blessed': Kris Jenner shared a photo collage of her new son-in-law on Instagram in celebration of Father's Day



Meanwhile, North's milestone first birthday will be celebrated by her huge family with not one, but two blowout parties.

A source disclosed to E!: 'This weekend, Kanye is performing at Bonaroo and they will fly to New

York City for a low-key Father's Day and birthday party.

'They are throwing her a big party next weekend and Kourtney [Kardashian], Khloé Kardashian and [the] kids [are] coming home for it,' the source added of the second celebration.



Two times the fun! While the infant's birthday fell on Father's Day, the family will celebrate her milestone with a big party next week, with Kim's sisters Kourtney and Khloé jetting back to LA from the Hamptons for the special occasion

Kim and Kanye weren't the only ones celebrating Father's Day on Sunday, with Kim's sister Kourtney taking to Instagram to pay tribute to her long-time partner and baby daddy, Scott Disick. 'Happy Father's Day @letthelordbewithyou! Thank you for loving being a daddy more than anything else,' she captioned a sweet shot of the 31-year-old sitting in a swing chair with the couple's two children - Mason, four, and Penelope, who turns two next month. 'Happy Father's Day @letthelordbewithyou! Thank you for loving being a daddy more than anything else': Kourtney Kardashian - who is expecting her third child with long-time partner Scott Disick - took to Instagram on Sunday to thank him for being a great dad to their kids Mason, four, and Penelope, two next month











