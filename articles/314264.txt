The U.S. Food and Drug Administration is extending the public comment period on proposed new rulemaking focused on e-cigarettes through August 8.

In April the FDA announced new efforts to review further regulation of e-cigarettes in response to consumer concerns the devices are being increasingly marketed toward children. Currently the FDA regulates cigarettes, cigarette tobacco, roll-your-own tobacco and smokeless tobacco. Proposed new products would include electronic cigarettes, cigars, pipe tobacco, certain dissolvables that are not "smokeless tobacco," gels, and waterpipe tobacco.

Once the proposed rule becomes final, FDA states it "will be able to use powerful regulatory tools, such as age restrictions and rigorous scientific review of new tobacco products and claims to reduce tobacco-related disease and death."

The FDA action would make e-cigerattes subject to the Federal Food, Drug, and Cosmetic Act, as amended by the Family Smoking Prevention and Tobacco Control Act.

The proposed rules would prohibit the sale of "covered tobacco products" to individuals under the age of 18 and to require the display of health warnings on cigarette tobacco, roll-your own tobacco, and covered tobacco product packages and in advertisements.

"FDA is taking this action to address the public health concerns associated with the use of tobacco products," states the FDA website

The federal action comes as states are increasing tackling the issue of e-cigarettes. The most recent news came this weekend when it was reported that Michigan Governor Rick Snyder is mulling vetoing an e-cigarette law that would not allow minors to buy the electronic cigarettes.

The states legislature has approved three e-cigarette laws. The proposed rules would prohibit e-cig sales and use of e-cigs by those under the age of 18. The laws also note that the e-cigs re not tobacco products which do fall under the Food and Drug Administration regulations.

"I've had issues with that bill," said Snyder last week, adding that it will get "special attention" and extra review. "Is it a tobacco product or not?"

The Michigan proposed rulemaking is just the latest in a continual series of actions regarding e-cigarettes.

The Michigan news comes on the heels of California Congresswoman Jackie Speier proposing that Congress regulate electronic smoking devices the same way the government regulates traditional tobacco.

"With flavors like gummy bear, cotton candy, and chocolate cake, our kids are literally vaping these things up," said U.S. Rep. Jackie Speier. "With ads using sex and sex appeal, our teens are lusting after these objects."

The San Francisco lawmaker debuted her new legislative proposal on Friday and claims the e-cig industry is intentionally trying to lure in under-age smokers with marketing strategies.

Her proposed bill would restrict advertisements on TV, according to the lawmaker's spokeswoman, Katrina Rill. Speier intends to formally introduce this week.

She aims to stop marketing of e-cigs to minors and also require the FDA to mandate childproof packaging standards and dosage limitations.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.