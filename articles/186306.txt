It seems the South Korean electronics giant is getting ready to introduce a new family of tablets soon. The new tablet line-up will reportedly be called Samsung Galaxy Tab S, and will first have two different display sizes on offer.

In a report by Sammobile, the Tab S series will get be world's first ever tablet device to feature WQXGA (2560 x1600) resolution displays, and it will also be Samsung's first ever tablet series to feature fingerprint sensors.

It claims that Samsung has dropped the plan of introducing six variants and is now sticking to Wi-Fi and LTE variant for displays sizes 8.4-inch and 10.5-inch. The four variants will be dubbed Galaxy Tab S 8.4-inch Wi-Fi (SM-T700), and LTE (SM-T705), Galaxy Tab S 10.5-inch Wi-Fi (SM-T800), and LTE (SM-T805). The website has also posted few screenshots of AnTuTu benchmark and CPU-Z listing of the alleged Galaxy Tab S.

If the resolution claims are true, then the Galaxy Tab S 8.4 would feature a pixel density of roughly 360ppi, beating the 8.9-inch iPad mini with Retina (326ppi) and the 7-inch Nexus 7 (2013) (323ppi). The Galaxy Tab S 10.5 would have a less impressive 288ppi.

According to the report, the Galaxy Tab S will run Android 4.4.2 KitKat out-of-the-box with no confirmation on whether it would be running Samsung's new Magazine UX seen on the Galaxy TabPRO and Galaxy NotePRO (Review | Images).

The Galaxy Tab S tablets are said to run on the octa-core Exynos 5 (5420) SoC (quad-core 1.9GHz Cortex-A15 and quad-core 1.3GHz Cortex-A7) with a hexa-core ARM Mali-T628 GPU clocked at 533MHz.

It will have a 3GB of 32-bit dual-channel 933MHz LPDDR3e RAM. The Galaxy Tab S tablets are said to feature 8-megapixel rear cameras with 1080p full-HD video recording, and 2.1-megapixel front cameras.

Rumoured connectivity options on the Galaxy Tab S tablets include be Wi-Fi 802.11 (a/b/g/n), Wi-Fi Direct, DLNA, Wi-Fi hotspot, Micro-USB 2.0, Bluetooth 4.0 LE/ANT+, GPS, GLONASS and an IR blaster.

Further, the report claims that the design of the tablets will be similar to the recently launched Galaxy Tab 4 series. Samsung is also expected to introduce a new feature called Multi-User login, which will be unique to the Galaxy Tab S. The report says, "This feature uses Android's native multi-user functionality but enhances it further by allowing the user to log in to his/her account simply by swiping a finger on the home button from the lock screen."