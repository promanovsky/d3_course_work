Former Smiths frontman Morrissey has cancelled the remainder of his US tour after collapsing and being taken to hospital.

The singer announced the end of the tour, including his scheduled June 21 date with Cliff Richard in New York, with "unimaginable sorrow".

A message posted on True To You, a Morrissey fan site used for his official announcements, said he collapsed after his show in Boston on Saturday.

It said he fell ill after catching "a horrendous cold" from his support act Kristeen Young in Miami, adding: "Morrissey received medical attention in Miami, and again in Boston, but it was not enough to shake off the virus, the recovery time for which is too lengthy to meet the final nine shows of the tour."

PA Media