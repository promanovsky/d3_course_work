Lilly spokesman Celeste Stanley told the Associated Press they don’t expect to launch the over-the-counter version of the drug before 2018.

The move could be an effort to get a leg up on competition from other companies who can sell generic versions of the drug after it loses patent protection in 2017.

AD

An over-the-counter option could also help combat black market sales of the drug. Advertised online, the illicit pills are not safe because they are often fake or adulterated, Dave Ricks, a senior vice president at Lilly, told USA Today. A large unmet demand exists for over-the-counter Cialis because many men suffering from impotence don’t feel comfortable talking to a doctor about the problem, Ricks said. He estimated that half of American men older than 40 suffer from erectile dysfunction.

AD

Cialis can be dangerous when taken with heart medications that contain nitrates, which may be a concern for regulators as they consider whether to allow men to buy the drug without seeing a doctor. Another consideration: Impotence can be a sign of other medical problems that could go undiscovered if patients skip the doctor and head straight for the pharmacy.

Approval by the FDA is “a big if,” the Wall Street Journal reported.

AD

Cialis is a top seller, generating $2.2 billion in worldwide sales last year. It was Lilly’s fourth best-selling drug. To date, more than 45 million men worldwide have used it.

Lilly and Sanofi didn’t reveal the financial terms of their agreement. Under the deal, Lilly will make the drug, which they are calling “Cialis OTC,” and Sanofi will sell it in markets where it is approved. Lilly currently makes the drug, which is sold in tablet form, in Puerto Rico.