'Having more kids is easy for me... and Amber should have the opportunity': Johnny Depp gushes about his fiancée Ms Heard and their plans for a family



They’re the Hollywood couple on everyone’s lips – and they’re usually very keen to keep their private life just that, private.

But Johnny Depp, 50, opened up about his feelings for his 27-year-old fiancé Amber Heard in a recent interview with John Hiscock at The Mirror, explain he ‘loves kids’ and would happily have a family of 100 with his new flame – who is rumoured to be pregnant already.

He said: ‘As far as having more kids, it’s easy for me – and Amber should have the opportunity to have kids if she wants. Let’s face it, practising for it is fun and it’s all wonderful.’

Scroll down for video

Gorgeous couple: Johnny and Amber pictured in Los Angeles in February

The Pirates Of The Caribbean star also touched on his relationship with his ex-wife, French star Vanessa Paradis.

The couple split in 2012 after 14 years together - not married - and have two children together; Lily-Rose is 14 and Jack aged 12.

But Johnny assured fans there was no animosity between he and Vanessa, saying: ‘Vanessa and I get along wonderfully. There’s no weirdness at all, no hostility – life happens and we spent 14 great years together.

Smooches: The star made one fan's day as he kissed her on the cheek while speaking to fans at the Transcendence premiere in California on Thursday In demand: Johnny posed for pictures with fans at the premiere launching his latest movie

‘We see each other all the time and giggle and hang out just like we always did.’

Johnny popped the question to Amber on Christmas Eve, but a date has yet to be announced for their wedding.

The couple met in 2010 when they filmed The Rum Diary - about journalist Paul Kemp - in Puerto Rico – but they only confirmed their relationship last year, preferring not to put their romance in the spotlight.

Autographs: Depp has been busy making the publicity rounds to promote his new film Transcendence, which opens April 18

Speaking more about his love for Amber, the star added: ‘One of the first times I sat down with her I was astonished at how smart she is. There’s a really strong, brilliant woman in there who has a lot to say.’

Depp has been busy making the publicity rounds to promote his new film Transcendence, which opens April 18.

The hunk’s latest film Transcendence comes out in theatres on April 18.