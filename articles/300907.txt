In this order issued Tuesday, Massachusetts Juvenile Court Judge Joseph Johnston returned custody of Justina Pelletier to her parents, Linda and Lou Pelletier.

SUFFOLK, SS.

BOSTON DIVISION

DOCKET NO.: 13CP0034BO

IN RE: CARE AND PROTECTION OF

JUSTINA PELLETIER

ORDER ON PETITIONS FOR REVIEW AND REDETERMINATION

In consideration of the evidence presented in the assented-to petitions for review and redetermination, this court finds:

1. Following the disposition order of March 25, 2014, Justina was placed on May 12, 2014, by the Massachusetts Department of Children and Services (“MA DCF”) at the Justice Resource Institute Susan Wayne Center for Excellence (“JRI”) in Thompson, Connecticut.

Advertisement

2. JRI has proved to be an excellent placement in which Justina and her family have received necessary and appropriate support and services working towards the goal of Justina’s return home. Services have included individual therapy for Justina several times per week, family therapy, medical care, recreational activity, occupational therapy, and physical therapy. Justina returned to school full-time and is engaged and excited to be in the classroom.

3. Since placement at JRI, Justina’s parents, Linda and Lou Pelletier, have been cooperative and engaged in services. A structured visitation plan was initiated immediately. Family members have visited Justina daily. Visits progressed well and were increased to extended, unmonitored visits off the JRI campus.

4. The West Hartford Schools Department convened a meeting on June 6, 2014, and created a comprehensive special education plan for Justina. The individual education plan includes small group and individual classroom instruction, occupational therapy, speech/language therapy, physical therapy, and individual counseling. These services will be put in place immediately upon Justina’s return home.

5. JRI collaborated with Justina’s family, MA DCF, and other collaterals in developing an appropriate plan identifying the necessary support and services for Justina’s return home. The plan addresses medical care and case management, daily access to special education services, individual therapy, and family therapy. Most of these services are ready to begin immediately and others very shortly. I credit the opinion of the director of JRI that with the many qualified, trained professionals assisting in Justina’s care, should an urgent situation arise, there will be immediate assessment, intervention, and support for Justina. It is a solid plan for Justina and her family that addresses Justina’s complex needs.

Advertisement

6. I find that the parties have shown by credible evidence that circumstances have changed since the adjudication on December 20, 2013, that Justina is a child in need of care and protection pursuant to G.L. c. 119, §§ 24-26. Effective Wednesday, June 18, 2014, this care and protection petition is dismissed and custody of Justina is returned to her parents, Linda and Lou Pelletier.

Joseph F. Johnston, Justice

Juvenile Court Department