The U.S. military has chosen two universities to lead a program to develop brain implants to restore memory to veterans who have suffered brain injuries, officials said at a news conference Tuesday.

The Restoring Active Memory (RAM) program is a project of the Defense Advanced Research Projects Agency (DARPA), the branch of the U.S. Department of Defense charged with developing next-generation technologies for the military. The initiative aims to develop wireless, fully implantable "neuroprosthetics" for service members suffering from traumatic brain injury or illness, DARPA Program Manager Justin Sanchez said at the news conference.

DARPA has selected two teams of researchers to develop the implants: The University of California, Los Angeles (UCLA) and the University of Pennsylvania, in Philadelphia. [Humanoid Robots to Flying Cars: 10 Coolest DARPA Projects]

Bridging the memory gap

More than 270,000 U.S. veterans have been diagnosed with traumatic brain injury (TBI) since 2000, and the condition affects about 1.7 million civilians in the United States each year, according to DARPA. TBI interferes with the ability to recall memories from before the injury, and also the capacity to form or retain new memories.

Currently, few treatments for TBI-related memory loss exist, but DARPA is trying to change that, Sanchez said. Deep brain stimulation, the use of implanted electrodes to deliver electrical signals to specific parts of the brain, has already demonstrated success in treating Parkinson's disease and other chronic brain conditions. Building on these advances, "we're developing new neuroprosthetics to bridge the gap in an injured brain to restore memory function," Sanchez said.

There are many different types of memory, but the RAM program will focus on a kind known as declarative memory knowledge that can be consciously recalled, such as events, times or places.

For example, imagine you need to go to the store. To find it, you need to know where the store is located and what it's called. A person with a traumatic brain injury often has trouble remembering these basic facts, Sanchez said.

As part of the RAM program, UCLA will receive up to $15 million, and the University of Pennsylvania will receive up to $22.5 million in funding, over a four-year period. In addition, Lawrence Livermore National Laboratory, a federal research facility located in Livermore, California, will receive up to $2.5 million. The funding will depend on whether the institutions meet a series of technical milestones, ranging from recording neural signals to developing the hardware for chronic implants.

The UCLA team will focus on studying memory processes in the entorhinal cortex, an area of the brain known as the gateway of memory formation. Researchers will stimulate and record from neurons in patients with epilepsy who already have brain implants as part of their monitoring and treatment. The researchers will also develop computer models of how to stimulate the brain to re-establish memory function.

The University of Pennsylvania team will focus more on modeling how brain circuits work together more broadly, especially those in the brain's frontal cortex, an area involved in the formation of long-term memories. The university is collaborating with Minneapolis-based biomedical device company Medtronic to develop a memory prosthesis system. [Flying Saucers to Mind Control: 7 Declassified Military & CIA Secrets]

So far, the research will focus solely on restoring memory, Sanchez told Live Science. "We are not doing any research in the domain of erasing memories," he said.

Food for thought

Still, developing devices to restore memory is not easy. Huge technological and scientific challenges exist, DARPA officials said. First, researchers will need to develop new devices and hardware to wirelessly record and produce electrical signals in the brain. Then, scientists will need to develop new computational models to describe the brain circuits involved in memory formation. Finally, new algorithms will be needed to sense and interpret neural signals, to determine the appropriate stimulation required to restore memory.

Once these devices have been developed, they will be tested in clinical studies in humans, as well as in animal studies. All of the research will undergo an initial review by institutional review boards, as well as a secondary review by a panel made up of officials from the Department of Defense. New devices will require the approval of the Food and Drug Administration before they are made clinically available, according to DARPA.

Throughout the research, DARPA will adhere to ethical standards the agency summarized as "the 3 C's" character, consent and consequence. DARPA has created a panel to assess these standards and provide input on the research, said William Casebeer, a DARPA program manager involved in bioethics.

"There's a huge amount of actual suffering we could potentially alleviate if [the program] is successful," Casebeer said.

The RAM program is one of two programs that aim to develop brain implants to restore mental function in veterans. Both are part of the broader BRAIN Initiative, aproposed $4.5 billion, 12-year brain-mapping effort launched by President Barack Obama in April 2013.