Over-the-counter drugs sit on shelves in front of the pharmacy at a Wal-Mart store in Chicago. (UPI Photo/Brian Kersey) | License Photo

WASHINGTON, May 7 (UPI) -- After a review of scientific data, the U.S. Food and Drug Administration said only those who had suffered a heart attack or stroke benefited from a low-dose daily aspirin.

The FDA consumer update said there was little evidence a daily low-dose aspirin helped prevent a heart attack or stroke for those who never had a cardiovascular event. The review said serious risks associated with the use of daily low-dose aspirin included increased risk of bleeding in the stomach and brain.

Advertisement

However, for patients who have had a cardiovascular event -- heart attack or stroke -- the known benefits of aspirin outweighed the risk of bleeding.

The FDA said people should only take a daily low-dose -- 80 milligrams -- aspirin after talking to a physician who can assess both the benefits and risks for the patient.

RELATED Daily aspirin use in women reduced ovarian cancer risk

"We encourage patients to talk to their healthcare provider about the best treatment for their individual situation," the FDA said.

"The kinds of evidence FDA uses to make regulatory decisions, which have broad public health implications, may be different from those used by a physician treating a specific patient."

The federal agency said it issued the statement following its rejection of Bayer Healthcare’s decade-old petition requesting approval of a primary prevention indication -- or the reduction of risk of having a first heart attack or stroke. Bayer had asked for an amendment to a rule allowing it to market an aspirin -- 75 mg to 325 mg -- to prevent a first heart attack for those with chronic heart disease or those with a positive assessment from a healthcare provider.

RELATED Taking aspirin at bedtime may reduce heart attack risk

RELATED Aspirin plus drug reduces stroke risk