The Dow Jones Industrials (DJINDICES:^DJI) rose 63 points on Friday, closing the week with a solid gain as investors continued to feel more optimistic about the prospects for the U.S. economy. Yet the real excitement in the stock market was in the S&P 500 (SNPINDEX:^GSPC), which closed above the 1,900 mark for the first time. For both stock indexes, market milestones have come at a furious pace over the past year or so, and despite calls for a Dow correction, there have been few signs of capitulation among investors lately.

A history of milestones

It was just over a year ago that the S&P 500 climbed above the 1,600 level for the first time, marking the end of a 13-year journey since the index hit 1,500. That was the longest time that it had taken the S&P to reach a new century-mark since 1985, with the great technology-stock bust and the financial crisis combining to hold the benchmark back.

Reaching that 1,600 milestone also required a major changing of the guard in terms of the stocks that contributed the most to the market's gains. During the 1990s, the S&P 500 hit new 100-point marks several times each year, and thousand-point moves for the Dow Jones Industrials became routine as well. Technology stocks were largely responsible for the moves in the S&P, and when they fell, the index went with them.

Similarly, during the housing boom, financial stocks gained supremacy over the market. Yet in the market meltdown following the financial crisis, those stocks were the hardest hit, and even now, many of the largest financial institutions haven't seen their share prices come even close to pre-crisis levels.

The new bull market

Since last May, though, the S&P has experienced three 100-point milestones. The index took less than three months before hitting 1,700 in early August, and then just less than four months to reach the 1,800 level in late November. In that context, the six-month delay to reach 1,900 might have made some bullish investors impatient. The Dow also took just over six months to climb from 15,000 to 16,000.

Of course, the key to remember is that these milestones become more meaningless as they get closer together on a percentage basis. Going from 1,800 to 1,900 on the S&P 500 represented only a rise of 5.6%. That's far less than a year's typical gain for the stock market historically.

Milestone levels are fun to observe, and given the amount of attention they get from ordinary investors, they can play a key psychological role in determining future market direction. But don't get wrapped up in the hype over a given round figure. Otherwise, the dangers of anchoring can lead you to make investing mistakes you could later regret.