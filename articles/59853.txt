Nonsmokers tend to be healthier and likely get to live longer than those who smoke but it's another thing if you do not smoke and live in a community where smokers light cigarettes anywhere they please.

Because of the health hazards attached to secondhand smoke, nonsmokers may still be at risk of getting exposed to nicotine and other harmful chemicals and be at risk of their toxic effects. Living in places where smoking in public is prohibited therefore offers more health benefits to nonsmokers and this is proven by a new study that suggests banning of smoking in public places is beneficial particularly to young children.

While the effects of smoke-free legislations may vary from one place to another, a study published in The Lancet medical journal March 28 shows that banning smoking in public places generally has a positive impact on child's health. Anti-smoking laws, in particular, can help reduce premature births and asthma attacks in children by as much as 10 percent.

In the study, the researchers examined the effects of anti-smoking laws on child health by analyzing 11 earlier studies comprised of over 2.5 million births and 247,168 asthma-related visits to the hospital. The researchers found that the rate of preterm birth and asthma-related hospital admissions dropped by 10 percent after legislations prohibiting smoking in public places were implemented.

"Smoke-free legislation is associated with substantial reductions in preterm births and hospital attendance for asthma," the researchers wrote. "Together with the health benefits in adults, this study provides strong support for WHO recommendations to create smoke-free environments."

Study senior author Aziz Sheikh from the School for Public Health and Primary Care (CAPHRI), Maastricht University in Netherlands urged countries that have not yet implemented anti-smoking laws to reconsider their position.

"This research has demonstrated the very considerable potential that smoke-free legislation offers to reduce preterm births and childhood asthma attacks," Sheikh said. "The many countries that are yet to enforce smoke-free legislation should in the light of these findings reconsider their positions on this important health policy question."

The World Health Organization (WHO) reports that tobacco is responsible for about 6 million deaths worldwide per year and this includes over 600,000 nonsmokers. The UN agency said that if the trend in smoking continues, tobacco-related deaths may increase to 8 million per year by 2030.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.