

Casey Kasem (Eric Jamison/Associated Press)

Famed disc jockey Casey Kasem, who died Sunday at age 82 and was the longtime host of the massively popular syndicated “Top 40” shows, was known as the King of the Countdowns. But Kasem had a very active life outside of his radio gigs, including a side career as a voice-over artist and someone who served as an activist for many political causes. The highlights:

* He was a very successful voice-over actor, doing hundreds of voices for various animated series and commercials — most famously, the happily clueless Shaggy in the “Scooby Doo” cartoons. In addition to the many “Scooby Doo” movies, his voice could be heard in “Josie and the Pussycats,” “Johnny Bravo” and even “Blue’s Clues.”

[Related: Casey Kasem, king of the top-40 countdown and ‘Scooby Doo’ voiceover artist, dies]

* He had plenty of acting cameos on live\-action series, too, including “Saved by the Bell” (he played himself in the series premiere) and “Hawaii Five-O.”

* He dreamed of being a pro baseball player when he was younger.

* He supported a myriad of causes. Billboard once asked him to list them all: “Ecology and rain forests; the Great American Meat-Out (for which he is spokesman); fair representation of Arabs and Jews in the media; anti-smoking; and what he calls a stance ‘way in the extreme in the progressive left’ of politics.”

[Related: As radio and pop culture splintered, Casey Kasem was keeping it together]

* Born Kemal Amen Kasem, his parents were Lebanese Druze immigrants. He was heavily involved in supporting Arab American rights, and the American Druze Society named him “Man of the Year” in 1996. “No one has done more in Hollywood to challenge the negative stereotype of Arabs in the media than Casey. No one has done more to challenge fellow entertainers to oppose war in the Middle East and support peace,” Arab American Institute President James Zogby once wrote. “Casey is the brightest star in our Arab American constellation.”

* He was very outspoken about his political views; a 1990 People story noted that “Kasem, 58, has turned up this past month on the ‘MacNeil/Lehrer Report,’ CNN and elsewhere to oppose the U.S. military presence in the Middle East and to counter growing anti-Arab hostility back home.”

* He was a vegan, which led to him turning down many endorsement deals for chain restaurants. This included cereal, both because it involved milk and also because he didn’t approve. “I just turned down a [Frosted] Cheerios commercial because I didn’t feel this is something I would want my child to eat,” he told Billboard.

* As part of his anti-homelessness activism, Kasem (joined by actress Carol Kane and actor Max Gail) slept on the streets of Los Angeles “to draw attention to the plight of the homeless,” according to the Los Angeles Times in March 1987.

* He was arrested at an anti-nuclear protest in 1988 along with actress Teri Garr, actor Robert Blake and activist Daniel Ellsberg at a nuclear testing grounds in Nevada. “Garr, Blake, Ellsberg and Kasem were arrested when they stepped across a cattle guard on the road leading to the main entrance of the desert site,” The Post reported.

* His conscience ran deep when it came to animated shows: He would turn down voice-over roles on cartoons that he felt were too “violent or mean-spirited.”

* His first wife was actress Linda Myers; they had three kids: Kerri, Julie and Mike. They were married for seven years and divorced in 1979. (Kerri was the most visible of the kids in an ugly, public battle with their stepmother, Jean, regarding Kasem’s care in the last months of his life as he suffered from Lewy body dementia.)

* A year after his divorce from Myers, he married Jean: They lived in a Holmby Hills, Calif., home that had a tennis court and a small golf course. They had one daughter, Liberty, after Jean suffered eight miscarriages, according to a People magazine article.