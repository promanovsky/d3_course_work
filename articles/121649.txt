The U.K. unemployment rate declined below the 7 percent threshold during three months to February, data from the Office for National Statistics showed Wednesday.

The jobless rate was 6.9 percent of the labor force, down from 7.1 percent in September to November period, and below 7.9 percent posted in three months to February 2013.

Last year, the Bank of England pledged not to hike interest rate until the jobless rate hit 7 percent. As the unemployment started falling faster than estimated, the BoE assured that interest rates will not be raised before the second quarter of 2015.

ONS said pay including bonuses for employees during December to February was 1.7 percent higher than a year earlier, matching consumer price inflation of 1.7 percent in February. Excluding bonus, wages gained 1.4 percent.

The claimant count fell by 30,400 in March from February. The claimant count fell to 3.4 percent, in line with forecast, from 3.5 percent in February.

For comments and feedback contact: editorial@rttnews.com

Economic News

What parts of the world are seeing the best (and worst) economic performances lately? Click here to check out our Econ Scorecard and find out! See up-to-the-moment rankings for the best and worst performers in GDP, unemployment rate, inflation and much more.