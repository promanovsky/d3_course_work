Bryan Singer has denied a man's claims that he sexually abused him as a teen and says he's pulling out of promotional events surrounding his new film, "X-Men: Days of Future Past," in the wake of the allegations.

In a statement obtained by "Entertainment Tonight," Singer said: "The allegations against me are outrageous, vicious and completely false. I do not want these fictitious claims to divert ANY attention from 'X-Men: Days of Future Past.' This fantastic film is a labor of love and one of the greatest experiences of my career. So, out of respect to all of the extraordinary contributions from the incredibly talented actors and crew involved, I've decided not to participate in the upcoming media events for the film. However, I promise when this situation is over, the facts will show this to be the sick twisted shake down it is. I want to thank fans, friends and family for all their amazing and overwhelming support."

His statement comes a week after he was sued in Hawaii by Michael Egan III, a former aspiring model and actor, who claims the director sexually abused him during a trip in Hawaii in 1999. Egan has said Singer abused him when he was as young as 15 years old at a home in Los Angeles, but his case only focused on the alleged abuse in Hawaii.

Singer's attorney has denied the director was in Hawaii when the alleged abuse occurred.

Egan's attorney Jeff Herman said Singer's denials would not affect his pursuit of the case. "We have a good-faith belief in the allegations, and we will litigate this," Herman said.

Egan has also sued three other entertainment industry figures -- two former television executives and a theater producer -- for substantially similar claims.

"None of the defendants in Michael Egan's cases have admitted to abusing him," Herman said. "I've gotten other threats from the other lawyers, which I find interesting. I'm not deterred from bringing these cases forward."

The lawsuits were filed in Hawaii under a law that temporarily suspends the statute of limitations in civil sex-abuse cases. None of the men has been criminally charged, and the statute of limitations for any such charges has passed.

In order to file the cases, Egan had to be evaluated by a psychologist who signed a notarized "Certificate of Merit" that includes facts and opinions supporting an opinion that he was sexually abused and had a psychological or physical injury. The certificate detailing Egan's allegations against Singer has been filed under seal.

Egan claims he was lured into a sex ring run by a former digital entertainment company executive with promises of auditions for acting, modeling and commercial jobs. He was put on the company's payroll as an actor and forced to have sex with adult men at parties within Hollywood's entertainment industry, he says.

"X-Men: Days of Future Past" is due to open in theaters May 23.