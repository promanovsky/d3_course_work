Emirates Airline placed the largest single order in aviation history, a request for 150 Boeing Co. 777x planes worth $56 billion, to expand what is already the world’s largest wide-body fleet.

The deal for 115 400-seat 777-9Xs and 35 smaller 777-8Xs firms up a commitment announced last year, and has an option for 50 more aircraft that would boost the value to $75 billion, Dubai-based Emirates said in a joint statement with Boeing.

“The 777X will offer us operational flexibility in terms of range, more passenger capacity and fuel efficiency,” Emirates President Tim Clark said today in the release. The first aircraft are due for delivery in 2020, he said.

While financing options haven’t been announced, some U.S. carriers led by Delta Air Lines Inc. have protested government-backed competitors including Emirates receiving loan guarantees from the U.S. Export-Import Bank, which helps foreign companies buy American goods. Atlanta-based Delta has said the bank’s charter should expire at the end of September unless the 80- year-old lender drops its support for such purchases of wide- body jets.

“Emirates is backed not only by its government, but also by our own,” Delta Chief Executive Officer Richard Anderson told the House Financial Services Committee on June 25. “Emirates can devote a substantial portion of its Ex-Im sponsored savings to enhance its competitive position vis-a-vis U.S. carriers.”

Value Increases

At the end of March, the bank had at risk $32 billion in support of wide-body jets, or 28 percent of total outstanding loans, guarantees and other financing, according to a report today from the U.S. Government Accountability Office. The value of such deals generally increased from fiscal year 2004 to 2012, according to the report.

“The GAO’s recent study illustrates why the Export-Import Bank is in need of reform,” according to an unattributed statement posted on Delta’s website. “The bank devotes too much of its funds to finance wide-body jets purchased by foreign competitors of U.S. airlines and supports too many foreign airlines that simply don’t need the help,” it said.

Delta spokesman Trebor Banstetter said in an e-mail Wednesday the company didn’t have a comment on Emirates’ purchase of the 777s. Ex-Im officials didn’t respond to requests for comment.

Financing Options

It’s not known whether the bank will play a role in the purchase. Airlines don’t usually weigh financing options until the planes are prepared for delivery, which for Emirates isn’t scheduled until later in the decade.

“They typically wouldn’t be arranging financing this far in advance,” Tim Neale, a spokesman for Chicago-based Boeing, said in a phone interview. He said Boeing’s customers usually finance most of their purchases by tapping private markets or by using their own cash.

The 777X order was initially announced at the Dubai Air Show in November, and is being firmed up days before next week’s Farnborough expo in the U.K.

Emirates finalized its order with Boeing less than a month after scrapping a deal with European rival Airbus Group NV for 70 A350 wide-body planes valued at $16 billion.

The deal positions Emirates, the largest airline by international traffic, as lead operator of the first twin-jet able to haul a jumbo’s payload, aided by a revamp with new General Electric Co. engines and Boeing’s largest-ever wing.

The Persian Gulf carrier, the No. 1 operator of both the current generation 777 and Airbus Group NV A380 superjumbo, is splurging on capacity as it builds Dubai into the leading hub for long-haul transfer flights.