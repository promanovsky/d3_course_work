There's a new image of what could be the iPhone 6 on Chinese microblogging site Weibo.

French site Nowwhereelse.fr first found the posting. The caption for the photo on Weibo says, "These are the pictures of the real iPhone 6 which were taken at Foxconn."

As always, take this with a grain of salt. Lots of people could be leaking fake images. For what it's worth, these images look totally feasible, and fit with previously leaked schematics.

Advertisement

The one thing that looks weird is the camera that seems to jut out from the phone. That makes us doubt that these are real. But, who knows! It's fun to see what people are passing around. For now, treat it as such.



