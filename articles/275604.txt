Centenarians are more prone to dying in hospitals than pneumonia patients [GETTY/MODEL USED]

FREE health tips to live a long and happy life SUBSCRIBE Invalid email Sign up forhealth tips to live a long and happy life When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

More people aged 100-plus are “outliving death from chronic illness” such as cancer but falling victim to treat- able conditions such as pneumonia. If people aged 100-plus had treatment specially tailored to their needs, more would be able to die in their homes or care homes.

We need to plan for health care services that meet the ‘hidden needs’ of this group Dr Catherine Evans

That would cut the NHS bill and ensure a better quality end-of-life experience, researchers at King’s College, London, found. Dr Catherine Evans, clinical lecturer in palliative care at the Cicely Saunders Institute at King’s College, said: “We need to plan for health care services that meet the ‘hidden needs’ of this group, who may decline rapidly if they succumb to an infection or pneumonia. “We need to boost high-quality care home capacity and responsive primary and community health services to enable people to remain in a comfortable, familiar environment in their last months of life.