Chocoholic bacteria found in your stomach may be the primary reason why dark chocolate is good to your heart. Researchers of a new study said that certain bacteria in your gut break down chocolate compounds that are otherwise difficult to digest and absorb and then ferment them into compounds that are beneficial to your heart.

In a study presented at the 247th National Meeting & Exposition of the American Chemical Society (ACS) in Dallas Tuesday, researchers from the Louisiana State University tested cocoa powder, the main ingredient of chocolates, using a model of the digestive track made up of modified test tubes to simulate normal digestion.

The researchers found that long molecules known as polyphenolic polymers present in cocoa fiber were too large they are poorly absorbed and remained within the gastrointestinal tract. These molecules, however, are eaten by certain bacteria in the colon, which then ferment them and release them as inflammatory compounds that are good for the heart.

"In our study we found that the fiber is fermented and the large polyphenolic polymers are metabolized to smaller molecules, which are more easily absorbed. These smaller polymers exhibit anti-inflammatory activity," said study author John Finley of Louisiana State University. "When these compounds are absorbed by the body, they lessen the inflammation of cardiovascular tissue, reducing the long-term risk of stroke."

Study researcher and undergraduate student Maria Moore explained that there are two kinds of bacteria found in the stomach, the good ones and the bad ones. The bad bacteria which include Clostridia and E. coli cause bloating, gas, diarrhea and constipation while the good bacteria help produce compounds that are good for the body.

"The good microbes, such as Bifidobacterium and lactic acid bacteria, feast on chocolate," Moore said. "When you eat dark chocolate, they grow and ferment it, producing compounds that are anti-inflammatory."

Prebiotics, which stimulate the growth of good bacteria in the digestive system, can apparently help convert polyphenolics into beneficial anti-inflammatory compounds. "When you ingest prebiotics, the beneficial gut microbial population increases and outcompetes any undesirable microbes in the gut, like those that cause stomach problems," Finley said. Prebiotics can be found in raw banana, raw garlic and cooked onion.

Finley has likewise recommended eating dark chocolates with solid fruits such as pomegranates and acai to experience more health benefits.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.