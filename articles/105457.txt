Nasa has announced that it will begin tests of a new ‘flying saucer’ that could one day help land people on Mars.

Unfortunately for conspiracy buffs, this isn’t the space agency finally confessing to developing those classic-look UFOs that plagued rural types in 1950s America, but the public’s first look at a new type of planetary lander known as the Low Density Supersonic Decelerator (LDSD).

This June, residents of the tiny Hawaiian island of Kauai will be treated to the unusual sight of this vehicle plummeting to the surface of the ocean after being hauled up to a height of 55 kilometres through a combination of rockets and high-altitude balloons.

The LDSD will fall from this height with its descent slowed from a speed of Mach 3.5 to lower than Mach 2 through a combination of inflatable discs and single giant parachute which will drastically increase the craft’s atmospheric drag.

It’s hoped that this technology will allow Nasa to land even larger payloads on the surface of Mars – a planet whose thin atmosphere (its only 1 per cent as dense as Earth’s) makes touching down without a bang extremely difficult.

The design of the LDSD was inspired by the Hawaiian puffer fish which increase size without adding mass. Image: Getty

"It may seem obvious, but the difference between landing and crashing is stopping," Allen Chen from Nasa’s Jet Propulsion Laboratory told New Scientist. “We really only have two options for stopping at Mars: rockets and aerodynamic drag."

Nasa’s current landing techniques have been in use since the 1976 Viking mission which deployed parachutes and rockets to safely drop a pair of landers on Mars. However, as these robots get heavier and more complex scientists are having difficulty ensuring their safe descent.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

The car-sized Curiosity rover weighed just under a tonne but Nasa calculates that future manned missions could require anything between 40 to 100 times heavier loads. Rockets powerful enough to slow down this sort of load would end up destabilising the craft. This is bad enough when you’re risking a $2 billion lander like Curiosity, but out of the question if humans are the payload.

LDSD is attempting to solve this quandary through the use of balloon-like cushions that would rapidly inflate around the payload, increasing its surface area and consequently its atmospheric drag. A 33.5-metre parachute could then be safely deployed once the craft has been slowed.