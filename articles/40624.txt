Celebrity

'When I am home I am like, shoes are off, I'm making him dinner,' the Mother Monster explains when asked about her relationship with her actor boyfriend.

Mar 25, 2014

AceShowbiz - Eccentric singer Lady GaGa has revealed that boyfriend Taylor Kinney is the one in charge of their relationship. When asked if she's submissive to her actor beau, the Mother Monster responded, "Yes, actually."

"He is totally in charge. I mean when I am home I am like, shoes are off, I'm making him dinner," the New York native said in a new interview on Sirius XM's "Morning Mash-Up". "He has a job too and he is really busy!"

The 27-year-old star, who has been dating Kinney since 2011, went on explaining why she chose to become less active at home. "I'm in charge all day long, the last thing I want to do is tell him what to do," she continued.

In the interview, GaGa also talked about her star-studded "G.U.Y." music video which was released over the weekend.

"I was interested in the way that we experience icons through technology and how when you are on your phone or on the internet, Jesus and Lisa Vanderpump and John Lennon and Andy Cohen kind of all exist in the same space now," she said. "That's what the video is for me as a feeling. When I am watching it, it's how I feel when I am looking at pop culture through the new lenses that we all have to look through them."