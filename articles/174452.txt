tech2 News Staff

Mid-range smartphone battle is certainly heating up. Just days after Sony launched its latest Xperia T2 Ultra Dual, HTC had also launched its very own mid-range smartphone, the Desire 816. Both are very much at sparring distance, thanks to their large displays, quad-core processors and decent-sounding cameras. But which one takes the cake? We compare the specs to see which one fares better.

HTC Desire 816

This was one of the three phones that were recently launched by HTC. HTC Desire 816 comes loaded with a 1.6GHz quad-core Qualcomm Snapdragon 400 processor. The processor has been teamed up with 1.5GB RAM.

It sports a 5.5 inch Super LCD2 display that has 1280x720 resolution. The phone will come loaded with Android operating system. It is not yet known which version will come with the phone. For data storage purpose, HTC has given 8GB of on board storage space which can be further expanded upto 32GB thanks microSD card slot provided with in the phone.

The primary camera of Desire 816 has 13MP resolution along with LED flash. The front camera comes with 5MP resolution. The connectivity features list of the phone includes Bluetooth, Wi-Fi, NFC, GPS, 3G and LTE. HTC has loaded the phone with a 2600 mAh battery that should go through an entire day without much trouble.

Sony Xperia T2 Ultra Dual

The dual-SIM phablet has been especially designed for the Indian market and is tipped as the successor to the Xperia T. It comes with a 1.4GHz quad-core Qualcomm Snapdragon 400 (MSM8928) processor under its hood. The processor has been coupled with 1GB RAM.

Xperia T2 Ultra Dual has a massive 6-inch display that comes with Sony’s Mobile BRAVIA Engine 2. It comes with Android 4.3 (Jelly Bean) out of the box and could be in line for a KitKat update too.

Connectivity features loaded with T2 Ultra Dual include Bluetooth, Wi-Fi, NFC, GPS and 3G. Surprisingly though, the phablet does not support LTE. Sony has loaded the Xperia phablet with 13MP primary camera with LED flash and 1.1MP front camera. To phablet has been fitted with a powerful 3600 mAh battery.

Take a look the comparison table below:

Verdict

In terms of pure specs the HTC Desire 816 is a clear winner, though Sony Xperia T2 Ultra does have a much bigger battery. If you are purely looking at a big phone for the corresponding increase in battery life, then the T2 Ultra is a good deal, but we feel overall, the HTC Desire 816 has an upper hand.