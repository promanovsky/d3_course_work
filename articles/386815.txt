Drugmaker Pfizer Inc. (PFE) Wednesday announced that the primary study endpoint was met in a Phase 3 study comparing a prophylaxis regimen of BeneFIX Coagulation Factor IX (Recombinant) 100 IU/kg once-weekly to on-demand treatment in people with moderately severe to severe hemophilia B. The company noted that once-weekly Prophylaxis Treatment with BeneFIX significantly reduced annualized bleeding rate compared to on-demand treatment.

BeneFIX is a recombinant coagulation factor IX product indicated for the control, prevention and perioperative management of bleeding episodes in adult and pediatric patients with hemophilia B.

This study was a Phase 3, open-label, non-randomized two-period study consisting of six months of on-demand therapy only, followed by 12 months of routine prophylaxis with BeneFIX 100 IU/kg once-weekly.

The top-line results of the study showed positive results, and hemophilia B patients taking once-weekly BeneFIX (100 IU/Kg) showed a statistically significant reduction in the annualized bleeding rate (ABR) relative to on-demand treatment with BeneFIX, the company said.

Study results also showed that prophylaxis treatment significantly reduced both spontaneous and traumatic ABR compared to on-demand treatment with BeneFIX.

Adverse events observed in the study, for both the prophylaxis and on-demand periods, were consistent with the known adverse event profile of BeneFIX, the company noted.

For comments and feedback contact: editorial@rttnews.com

Business News