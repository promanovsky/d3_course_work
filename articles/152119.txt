Internet use can help reduce depression in elderly people, a latest study states.

Researchers examined data of over 3,000 American seniors. The information was collected from the Health and Retirement Survey that interviewed over 22,000 elderly people.

The study analysis showed that depression risk among the seniors reduced by 30 percent after they started using Internet. However, the effect was not seen in participants with a history of depression. But the researchers said that depressive symptoms in these respondents decreased a bit.

"Internet use continues to reduce depression, even when controlling for that prior depressive state," lead researcher Shelia Cotten, professor of telecommunications, information studies and media from Michigan State University, said in a press release.

"This is one of the largest and most comprehensive surveys of its kind," Cotten said. - See more at: http://msutoday.msu.edu/news/2014/internet-use-can-help-ward-off-depression-among-elderly/#sthash.Rd3HRFwl.dpuf

The Internet use greatly impacted the seniors who stayed alone. The researchers advised the older people to start actively using the Internet and remain social in the other aspects of life.

"If you sit in front of a computer all day, ignoring the roles you have in life and the things you need to accomplish as part of your daily life, then it's going to have a negative impact on you," Cotten explained in the university's release. "But if you're using it in moderation and you're doing things that enhance your life, then the impacts are likely to be positive in terms of health and well-being."

The study was published in the 'Journal of Gerontology: Psychological Sciences and Social Sciences.'

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.