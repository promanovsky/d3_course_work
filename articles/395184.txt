The US space agency's Opportunity rover has clocked more miles on Mars than any man-made vehicle to reach another celestial body, according to Nasa.

Since arriving on the red planet in 2005, the solar-powered robot has journeyed across 25 miles (40 kilometres) of Martian terrain. That surpasses the previous record, held by the Soviet Union's Lunokhod 2 rover, which landed on the Moon in 1973.

"Opportunity has driven farther than any other wheeled vehicle on another world," said project manager John Callas of Nasa's jet propulsion laboratory (JPL) in Pasadena, California.

"This is so remarkable considering Opportunity was intended to drive about one kilometre and was never designed for distance."

Opportunity and its twin rover, Spirit – now defunct – discovered wet environmental conditions on Mars, some of which are mild enough to support life.

Opportunity is now exploring the Endeavour crater on Mars. Its next-generation robotic counterpart, the Curiosity rover, launched in 2012 and is operating near the Gale crater on Mars.

Nasa said the Lunokhod 2 rover landed on Earth's moon on 15 January 1973, and drove about 24 miles in less than five months.

Those figures are based on calculations recently made using images from Nasa's lunar reconnaissance orbiter cameras that reveal Lunokhod 2's tracks, the US space agency said.