The American Academy of Pediatrics and the American College of Obstetricians and Gynecologists concluded that soaking in a tub of water during the early part of labor can reduce a woman’s pain and might shorten labor and the need for an epidural or other painkillers. But the two groups frowned on women staying submerged during the final pushing stage of labor since the “safety and efficacy” of delivering a baby into a pool of water “have not been established.”

Gisele Bundchen delivered her and Tom Brady’s son, Benjamin, at home three years ago while submerged in a water bath; she said in media interviews that the drug-free birth didn’t hurt at all. Actresses Jennifer Connelly and Pamela Anderson also swear by the water births they had naturally at home. But two prominent physician groups issued a new recommendation on Thursday advising against the practice, except in very controlled experimental conditions.

Advertisement

The committee of pediatricians and obstetricians pointed to a laundry list of worries doctors have about underwater deliveries, including the possibility of infections in both mother and baby from contaminated water, a dangerous rise in the baby’s temperature from the hot water, umbilical cord rupture after delivery that triggers shock or breathing problems in the baby — or even the rare possibility of drowning.

A 2010 study cited in the new recommendation describes four babies delivered in water who wound up with severe breathing problems right after birth, one of whom died of a sepsis infection.

But the experts also admitted that no one really knows just how risky water deliveries are because scant studies have been done to study their risks and benefits. A 2009 analysis of the 12 small clinical trials assessing the use of tubs during labor found no evidence that the practice was more dangerous than routine labor on a birthing bed, but only three of the 12 studies actually looked at the risks of delivering a baby under water.

Advertisement

Given the dearth of research, obstetricians should consider underwater deliveries to be “an experimental procedure,” the recommendations stated, that should only be performed in a research setting where women are informed about possible risks to themselves and their babies.

I'm sure some fans of underwater births may be inclined to disagree. What do you think? Does laboring in water make sense?

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.