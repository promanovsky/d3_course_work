Six Iranians arrested for appearing in a video singing along to an American pop song were released on Wednesday, one day after being detained for what police called their "obscene" behaviour, one of them said in an online posting.

In the clip, the three women and three men dance and lip synch to Pharrell Williams' Happy, imitating the official video of the international hit. The women are not wearing headscarves, as demanded by Iran's Islamic law.





Mobile users can watch the video here.

Their arrest caused outrage among fans of the song — a catchy anthem about feeling happy — who took to social media to denounce what they saw as a heavy-handed response by the Islamic Republic to a piece of harmless fun.

On Twitter, Pharrell Williams denounced the arrest of the dancing Iranian youths as "beyond sad." ( Zach Cordner/Invision/Associated Press)

Pharrell Williams himself criticized the arrests. "It's beyond sad these kids were arrested for trying to spread happiness," he tweeted on Tuesday.

The incident has highlighted the struggle between Iran's conservatives and those who hope President Hassan Rouhani, who has eased the country's antagonistic stance with the West, might also relax the Islamic Republic's social strictures.

Iran's state-run TV broadcast a program on Tuesday, apparently showing the six expressing regret. "They had promised us not to publish the video," said one of the women.

Tehran's police chief, Hossein Sajedinia, said he had ordered the arrest of the six youngsters because they had helped

create an "obscene video clip that offended the public morals and was released in cyberspace," the ISNA news agency said.

A source close to the six said they had been released on bail.

Mobile users can watch the clip here.