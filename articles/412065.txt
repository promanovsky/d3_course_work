A rather PLANE outfit Kim! Kardashian is surprisingly conservative in black leggings as she jets out of LAX... but still shows off cleavage in low cut top



She is known for her love of clinging dresses which show off her famous curves to the maximum.

But Kim Kardashian was looking rather more conservative on Thursday as she and her family jetted out of LAX.



The 33-year-old reality star arrived at the terminal sporting a long black jacket over black leggings despite the soaring Los Angeles temperatures.

Scroll down for video

Back to black: Kim Kardashian looked surprisingly conservative as she jetted out of LAX with mother Kris Jenner on Thursday

However, Kim still managed to show off her famous cleavage in a plunging top and tottered through the terminal in strappy pink sandals.



She was joined by her half sister Kendall Jenner, who towered above the star in an all-white ensemble, teamed with skyscraper peep toe grey booties and a blue denim jacket.

A large designer bright red bag added a pop of colour to the look.

Dressing for comfort: Kim wore a casual ensemble for the flight, although it was unclear where the family were heading



Keeping up: Kim was joined by sister Kendall and mother Kris



Keeping up: Kim was joined by her sister Kendall and mother Kris



Their mother Kris Jenner looked a little more dressed for comfort in a cream embroidered maxi-dress, strappy sandals, a black bag and sunglasses.



Despite covering up her famous figure for the flight, Kim ensured her fans got a dose of her wearing a typically more revealing outfit earlier in the day, posting several 'throwback Thursday' pictures of herself in a bikini carrying her daughter North.



'#TBT me and my beach babe in Punta Mita, Mexico,' she wrote next to one shot showing her walking along the beach, dipping her feet in the ocean.

Towering above the rest: Kendall showed off her tall frame in an all-white outfit and denim jacket



Slimline: The wannabe model showed off her stunning figure in the ensemble



A dream in cream: Kris Jenner looked stylish as she emerged from the family SUV for the flight



Another photograph was captioned: '#TBT bye beach #PuntaMita #Mexico' and showed Kim's pert posterior as she walked away from the tide.

There were no sign of Kim and Kendall's other famous sisters Khloe and Kylie for Thursday's journey.



Kris's oldest daughter Kourtney is currently in New York with partner Scott Disick and their two children Mason and Penelope where she has been filming spin-off show Kourtney & Khloe Take The Hamptons.



Baby love: Kim Kardashian posted a picture on Instagram of herself in a white bikini clutching baby North while on holiday in Mexico, earlier on Thursday

