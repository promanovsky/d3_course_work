Microsoft just announced its third-generation Surface tablet, the Surface Pro 3. The company is pushing it as a true tablet alternative to the traditional laptop.

Like Microsoft's previous generation Surface Pro slates, it runs on Windows 8.1 Pro out of the box. The company claims that it's thinner, faster, and lighter than the Surface Pro 2.

Advertisement

It runs on Intel's battery-efficient Haswell processors, and like a laptop it can be configured to run up to Core i7 standards (i.e. very fast). Essentially, Microsoft has packed into a tablet format the same computing power you'd find on a high-end Windows laptop.Microsoft has also added a larger screen to the Surface Pro 3, making it a 12-inch tablet as opposed to its 10-inch predecessors.

The Surface Pro 3 goes on sale Wednesday starting at $799. Here's a look at everything it can do.