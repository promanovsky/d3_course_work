BlackBerry might not be winning the market share game against the smartphone juggernauts of iOS and Android, but the company is continuing its efforts to keep its nameand servicesat the forefront of users' minds regardless of which platform they prefer.

At least, that seems to be the case with the company's multi-platform launches of the BlackBerry Messenger app. It's October 2013 delivery of BlackBerry Messenger for iOS and Android brought 40 million new registered users into the fold by late December, and it only seems natural that BlackBerry would want to get BBM on as many phones as possible.

Enter: The Windows Phone version of the app, which is available now.

If you happen to be a multi-platform BlackBerry Messenger user, you'll likely note that BlackBerry Messenger looks a bit different on Windows Phone than it does on iOS or Android. BlackBerry has attempted to keep the Windows Phone look and feel in its app (tiles aplenty) instead of pushing one default design through for BlackBerry Messenger as a whole, which will no doubt please those more used to Microsoft's interface.

"Customers love apps that are simple, easy to use and beautifully designed. Over the last couple of releases we've focused on simplifying the BBM user experience to make it easier to sign up, sign in and add BBM contacts. When it comes to design, we've heard users asking for a native experience that matches the experience they're accustomed to on their phone," BlackBerry said in a blog post.

BlackBerry Messenger has actually been available as a beta download for Windows Phone users since July 10. Those looking to partake were first required to send in an application to BlackBerry.

Further Reading

Mobile App Reviews