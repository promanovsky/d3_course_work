Google deleting raises BBC concerns

Share this article: Share Tweet Share Share Share Email Share

Madrid - Google’s decision to remove some search results to a British Broadcasting Corporation 2007 article about Merrill Lynch has drawn criticism from the UK state-funded media company. Google alerted BBC Economics Editor Robert Peston that it would no longer show certain search outcomes linking to his post entitled “Merrill’s mess,” Peston said on his BBC blog yesterday. The decision followed a May ruling by the European Court of Justice that allows members of the public to request “inadequate, irrelevant, no longer relevant, or excessive” data be deleted from Google searches. “We’re surprised that this is the outcome of the ECJ ruling and concerned at the implications of the removal from search of this type of material,” the BBC said in an e-mailed statement to Bloomberg News today. Google didn’t tell Peston who requested the removal, he said in his blog.

Because the item can still be found on Google using some other combinations of word searches, the implication is that the request may have come from a commentator, he wrote.

“We’ve recently started taking action on the removals request,” Mountain View, California-based Google said in a statement.

“It’s a new and evolving process for us and we’ll continue to listen to feedback and we’ll also work with data protection authorities and others as we comply with the ruling.”

Online Tool

The BBC story was critical of Merrill Lynch, saying the investment bank was acting as a risk taker rather than an agent or broker.

Merrill’s board, in negotiating Chairman Stan O’Neal’s departure, was trying to “preserve the integrity of a giant, money-making collective,” according to the blog, dated October 29, 2007.

Victoria Garrod, a London-based spokeswoman for Bank of America, which bought Merrill Lynch in 2009, had no immediate comment when reached by phone.

A representative for O’Neal couldn’t immediately be reached for comment.

Since the May 13 court verdict, Google has started offering an online tool to allow people to ask for search results to be redacted.

The right-to-be-forgotten ruling was a surprise for Google and other companies already facing greater scrutiny over privacy practices in the 28-nation European Union.

Google received 70,000 requests for removal of 267,500 URLs between May 29 and June 30, according to an e-mailed statement today with the latest data.

The countries that made the most requests were France, with 47,927, and Germany, with 47,014, according to the statement.

Request Volume

“The compliance burden on Google is huge and it’s ballooning, particularly with the recent publicity,” said David Copping, a partner at London-based law firm Farrer & Co.

“How Google will deal with the huge volume of requests, when each is so complicated, is very difficult and each case has to be assessed on its merits.”

At issue is the fact the EU cast a wide net in terms of what a person can have removed from search results, he said.

The information doesn’t have to be untrue to be erased, he said.

“Faced with liability it seems Google will err on the side of caution and take things down; that’s traditionally been approach of big tech companies,” Copping said. - Bloomberg News