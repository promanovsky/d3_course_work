The stagnant numbers would seem to fly in the face of the strong publicity the law earned by passing 7 million and then 8 million sign-ups. For a law that had experienced almost nothing but bad news for months, one would think a little good news would lead to at least a little recovery.

AD

AD

And some polls initially suggested that might be the case. But whatever momentum the law carried from the sign-ups announcement -- and a later projection that it will actually cost less than previously thought -- has gone by the wayside.

The stark numbers are bad news for Democrats, but they also shouldn't be surprising. Attitudes on the law have not fluctuated much since its passage in 2010 and are deeply entwined with long-held partisan loyalties, helped along by a highly political public debate.

What's more, Americans' biggest complaints about the health law are pretty well etched in stone. They existed well before the Web site's troubles, and the number of Americans who sign up for the law was never the root of the opposition. This was laid out clearly in the new Pew poll.

AD

Pew asked those who disapproved of the law the major reasons for their opposition:

AD

80 percent said a major reason for their opposition was "too much government involvement in health care"

76 percent said the law is "too expensive for the country"

58 percent cited the law's requirement that everyone must have health insurance

57 percent feared their "own health care may suffer"

Asked for their No. 1 reason for disliking the law, 42 percent cited "too much government involvement."

This particular complaint will be almost impossible for the law to overcome. It doesn't matter how well the government-run insurance marketplace works, if someone fundamentally opposes the whole idea of a government-run marketplace in the first place. In fact, news of 8 million signups in government-run marketplaces may only serve to remind some of the tightening nexus between government and the health insurance market. If you don't like Obamacare, having more people sign up might make you like it less rather than more.

AD

Other complaints about the law are also hard to combat. The mandate for individuals to buy health insurance has long been one of the law's most unpopular features, and while certain aspects of it have been delayed (for small employers to offer plans, for instance), it's not going anywhere. And concerns about the law's costs, which are widespread, will be especially hard to overcome going forward. Even if the law succeeds in reducing the growth rate of health-care costs, it's almost certain the actual costs of health care will continue to go up -- something the law will be blamed for by the vast majority of people who have already decided that they don't like it.

AD

And then there's the whole matter of whether people even processed the good news about Obamacare.

The Kaiser poll from last week actually showed that about three times as many people thought the sign-ups for the law came in below goals (57 percent) rather than above them (21 percent). (The Congressional Budget Office set a goal for enrollments at 7 million.)

AD

What's perhaps most striking is that 43 percent of people correctly stated that the law signed up 8 million people, but less than half of them knew that this exceeded expectations.

Essentially, what happened is that Americans have gotten so used to the string of bad news about Obamacare that they simply filed away the sign-ups news in the same folder. And while the White House attempted to play up the news with its "victory lap" news conference, Americans just aren't all that keen on this kind of process story.

All of which suggests that the health-care law needs a far more substantial -- and sustained -- string of good news before Americans' perceptions of the law show any real shift toward the better.