Let’s get right to the point: We still don’t know what Marissa Mayer plans to do after Alibaba’s IPO.

Yahoo Inc. has a 24 percent stake in the Chinese e-commerce behemoth and is required to sell off 9 percent of its holding as part of the initial public offering. That could bring in $10 billion -- raising questions about what the Yahoo CEO might do to strengthen her floundering company.

Mayer touched on many topics in her much-anticipated session with TechCrunch founder Michael Arrington on Wednesday, the final day of Disrupt NY, but she sidestepped the subject on everyone’s minds.

“It’s of critical importance to our investors,” she said, deflecting Arrington’s question of what Yahoo would do if it “just happened” to find itself with $10 billion to spend. “We will continue to be good stewards of capital.”

If the answers sounded like lawyers wrote them, they probably were. The Securities and Exchange Commission released Alibaba’s F-1 form less than 24 hours ago, and Yahoo is in a “quiet period.”

Instead, the Disrupt conversation focused on Yahoo’s direction as a company, which has gotten muddled by a number of acquisitions -- like microblogging platform Tumblr and social media data firm Vizify -- since Mayer took over in July 2012.

She discussed the Internet company’s expansion into media with the launch of Yahoo News Digest, Yahoo Magazines and original video content. Mayer also said that Yahoo would begin live-streaming concerts as of July 1 thanks to a new partnership with LiveNation.

During her nearly two-year tenure, she has worked to “trim the fat” at Yahoo and believes the company is more efficient than ever, she said. But what about a recent Wall Street Journal article saying that without Yahoo Japan or Alibaba, Yahoo’s value is “getting closer to zero”?

“I do think we have created value,” Mayer said, adding that she believes Yahoo is “undervalued.”

“Certainly Alibaba has created value, as well as Yahoo Japan. Our products today function much better, and we’ve generated a lot of traffic growth and user growth.”

As she did at events like the Consumer Electronics Show in January, Mayer portrayed Yahoo as a mobile-first company. She told the Disrupt NY audience that there were only about 60 Yahoo employees dedicated to mobile when she first started – “one of the big missed opportunities,” she said – and that there are now 500. The focus on mobile was justification for the $1.1 billion Yahoo spent on Tumblr.

Mayer also talked a bit about Henrique de Castro, the former Yahoo COO who received a $58 million severance package after being with the company for only 15 months.

“Enrique was not a fit,” Mayer said. “Overall, I think that it was the right time for us to go our separate ways.”

Mayer urged consumers looking for an introduction to Yahoo products to try the News Digest app for iOS.

The app isn’t currently available to Android users.

Until they see what else Mayer and her team can offer, consumers -- and Wall Street -- may remain skeptical.