The antioxidant found in red wine and dark chocolate is often praised for its health benefits, but new research suggests it does not promote longevity after all.

A new study suggests the antioxidant, called Resveratrol, does not reduce the risk of conditions such as "cardiovascular disease, cancer and inflammation," a JAMA Network Journals news release reported.

Many people believe the "French Paradox" is a result of the culture's love for red wine. In France there are relatively low rates of coronary heart disease, despite a diet high in cholesterol and fat.

This paradox is often believed to be linked to the "resveratrol and other polyphenols contained in wine," the news release reported.

Preliminary evidence has also pointed towards anti-inflammatory effects associated with these substances as well as a reduced cancer rate and blood vessel stiffness.

To make their findings the researchers looked at a sample of 783 men and women 65 years of age or older who participated in the Aging in the Chianti Region study between the years of 1998 and 2009. The study took place in two Italian villages.

The study's goal was to determine if the culture's high consumption of resveratrol had an effect on rates of "inflammation, cancer, cardiovascular disease, and death," the news release reported.

The participants' resveratrol levels were measured every 24 hours through urine samples.

During a nine-year follow up 34.3 percent of the participants passed away. Out of those free of cardiovascular disease at enrollment 27.2 percent developed the condition during the follow-up; 4.6 percent of the cancer-free patients developed it during the follow-up.

The team found urine Resveratro levels did not have an effect on instances of "death, inflammation, cardiovascular disease or cancer," the news release reported.

"In conclusion, this prospective study of nearly 800 older community-dwelling adults shows no association between urinary resveratrol metabolites and longevity. This study suggests that dietary resveratrol from Western diets in community-dwelling older adults does not have a substantial influence on inflammation, cardiovascular disease, cancer, or longevity," the researchers said in the news release.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.