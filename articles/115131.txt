‘The Lion And The Rose’, the April 13 episode of ‘Game Of Thrones’, featured a pretty shocking death — but many were not too sad to see that person go. Read on to find out who was killed, and the many reasons why they had it coming! SPOILERS are ahead!

The April 13 episode of Game of Thrones featured a royal wedding, and in Westeros, we all know that a royal wedding spells death for a least one unlucky attendee. However, many are not exactly mourning this character’s loss. Read on, where we count the ways why this character was just asking to be murdered. And I can’t be more clear: if you click in, you will be spoiled.

‘Game Of Thrones’: Why Joffrey Baratheon Deserved To Die

Was there anything more satisfying than the death of Joffrey Baratheon (Jack Gleeson) at his own wedding? Watching him clutch his neck once he realized he had been poisoned was the most righteous and gratifying moment of the series. While yes, he’s just a child, and yes, he’s simply a product of incestuous circumstance and the machinations of a bunch of old people who thought it would be a good idea to put him on the throne, he had it coming. As we list off Joffrey’s greatest misdeeds, celebrate his death with us in the comments!

1. He executed Sansa’s direwolf, Lady

Aaaall the way back in Season 1, Sansa (Sophie Turner) got a first glimpse of the monster she nearly married when Joffrey saw Arya swordfighting with her commoner friend, Mycah. Joffrey challenged him, Mycah was too scared to move, and Joffrey cut Mycah’s face. Then Arya charged Joffrey with her sword, Joffrey was disarmed, he lunged after Arya, and Nymeria, Arya’s direwolf, attacked him. Nymeria ran off, but Lady was executed in her stead.

2. He had Ned Stark executed

Ned Stark was accused of treason for speaking the truth — that is, that Joffrey is an unholy abomination with no right to sit on the throne — and Sansa pleaded with Joffrey to spare Ned’s life, if only he confessed to treason and accepted Joffrey as his king. Joffrey agreed, and then had Ned killed anyway.

3. He made Sansa look at Ned’s head on a pike — and then beat her

Because the charm never ends, once Ned was decapitated, his head was put on a pike as a warning to other would-be challengers of the throne. He told her that there was a pike for her brother, Robb, and she sassed him, saying that maybe Robb will bring her Joffrey’s head. Joffrey than had her beaten by Ser Meryn Trant, one of his Kingsguard.

‘Game Of Thrones’: Joffrey’s Worst Moments

4. He ordered a city-wide massacre of Robert Baratheon’s innocent bastards

What better way to secure your shaky claim to the throne than to execute all those who have a true right to it? When rumors about Joffrey’s parentage reached his ears, he confronted his mother, threatened her life, and then had all the black-haired Baretheon bastards murdered.

5. He threatened to rape Sansa

Poor Sansa. After she was forced to marry Tyrion (and Joffrey hid Tyrion’s footstool at the ceremony so that he would be humiliated), Joffrey more or less promised her that just because she’s wed to another, that won’t keep him from her chambers. Tyrion was not thrilled, but really, I think we can all agree it was only a matter of time before Joffrey did something even more horrific to Sansa than he has already.

And so on, and so forth. On the day he was killed, he humiliated Tyrion further by having a troupe of little people play out The War Of The Five Kings and asked Tyrion to participate — but not before one of the little people gruesomely and vulgarly mocked the death of Sansa’s brother, Robb.

I mean… asking for it. These are just the highlights; avid viewers of the Game Of Thrones series and readers of the Song Of Ice And Fire novels know that this is just the tip of the iceberg for that “vicious idiot” of a King (Tyrion’s words).

What do you think, HollywoodLifers? I mean, obviously he had it coming, but what was the first horrifying moment of Joffrey’s that made you think that Westeros would be way better off without him? And for readers of the books, my gosh, please don’t spoil in the comments.

— Amanda Michelle Steiner

Follow @AmandaMichl

More ‘Game Of Thrones’: