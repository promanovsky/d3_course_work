Tokyo: Japan says it will redesign its controversial Antarctic whaling mission in a bid to make it more scientific, after a United Nations court ruled it was a commercial hunt masquerading as research.

The bullish response, which could see harpoon ships back in the Southern Ocean next year, sets Tokyo back on a collision course with environmentalists.

Japan's minister of agriculture, forestry and fisheries Yoshimasa Hayashi: "Our country will firmly maintain its basic policy of conducting whaling for research." He is pictured here speaking at a whale meat tasting event in Tokyo. Credit:AP

Campaigners had hailed the decision by the International Court of Justice, with hopes that it might herald the end of a practice they view as barbaric.

"We will carry out extensive studies in cooperation with ministries concerned to submit a new research program by this autumn to the International Whaling Commission (IWC), reflecting the criteria laid out in the verdict," said Yoshimasa Hayashi, minister of agriculture, forestry and fisheries.