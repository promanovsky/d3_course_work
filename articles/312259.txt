Remember last week Nokia teased an announcement on their website? Based on the teaser, many had speculated that this could be a new Nokia Android phone, and sure enough it is. Nokia has since officially announced the Nokia X2 and for those who were looking forward to an Android powerhouse, it looks like you guys will be disappointed.

Advertising

However for those who love the Nokia brand and just want something cheap and functional, perhaps the Nokia X2 with its price tag of €99 could be of interest to you. In terms of hardware, it’s not the most powerful but like we said, it should be good enough to get the job done. The phone will be powered by a 1.2GHz dual-core Snapdragon 200 processor under the hood.

It will come with a 4.3-inch WVGA ClearBlack LCD display, 1GB of RAM, 4GB of onboard storage, a microSD card slot for memory expansion and 15GB of free OneDrive cloud storage, a 5MP rear-facing camera, a VGA front-facing camera, and a removable 1,800mAh battery. It will also support dual SIM functionality, so if you own more than one line, this should come in handy.

According to Nokia, the Nokia X2 will be available “immediately” in select countries. They did not specify which countries the phone will be available in so you will have to check your local Nokia website to see if it is available.

Filed in . Read more about Android, Microsoft and Nokia.