Luxury group Burberry Group Plc. (BRBY.L) Wednesday said total revenues in the six months ended March 31, increased 16 percent to 1.298 billion pounds from 1.116 billion pounds in the prior year. Underlying revenues climbed 19 percent.

Retail revenue increased to 928 million pounds from 840 million pounds. Comparable sales growth was 12 percent, in line with the company's expectations.

Wholesale revenues advanced to 333 million pounds from 220 million pounds. Wholesale revenue excluding Beauty was 240 million pounds.



Angela Ahrendts, Chief Executive Officer, said, "We are pleased with our second half performance, with total revenue up 19% and retail sales up 13%, underpinned by the planned increase in investment in offline and online retail, innovative customer service and marketing.

Looking ahead to fiscal 2015, the company expects a material adverse impact on reported profit at current exchange rates.

For comments and feedback contact: editorial@rttnews.com

Business News