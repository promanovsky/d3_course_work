Kim and Kanye are set to tie-the-knot in just 15 days and it seems one of Kim’s wedding guests has leaked the top secret invite! It’s incredibly sleek and simple, the opposite of her last wedding to Kris Humphries! HollywoodLifers, check out the pic and tell us what you think!

Kim Kardashian can’t keep everything about her wedding under wraps. The Keeping Up With The Kardashians star and Kanye West will indeed marry on May 24, and now the invitation that was reportedly hand delivered to guests, has just made it into the hands of Us Weekly. It’s grey, gold invite, actually just keep reading to see for yourself!

Kim Kardashian & Kanye West’s Wedding Invitations Revealed

The invites are not what we would have expected, they’re incredibly minimalistic but we think they are absolutely perfect!

Click here to see the wedding invite!

The details on the invite are scant, but they do say that the ceremony will be black tie, and that a day before the wedding there will be a dinner at six o’clock in the evening. The dinner will be cocktail attire.

We love the fact that guests have to wait until they get to the hotel for the location of where the ceremony will be, too! We actually broke the news that Kimye weren’t telling guests until they got to France so that they could keep everything super quiet and a surprise for everyone.

Bruce Jenner Will Walk Kim Down The Aisle

Bruce Jenner is going to be walking Kim down the aisle, according to E! and we really think that’s wonderful. Despite everything that’s going on with Bruce and her mom, Kris Jenner, we think it’s so sweet that Kim still wants him to play such a large role in her big day.

Now that Kim is back in LA after going to the Met Ball on May 5, we bet she’s gearing up to leave any day now for Paris! Although the wedding will not be filmed, we told you that she plans to film everything leading up to the big day!

HollywoodLifers, what do you think of Kimye’s wedding invitation!

— Chloe Melas

More Kim Kardashian Wedding News: