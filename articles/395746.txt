Peter Jackson and the cast of The Hobbit had a trip down memory lane as the final film, The Battle Of The Five Armies, launched a full-scale assault on Comic-Con.

The cast from the final instalment of The Hobbit trilogy including Cate Blanchett, Orlando Bloom, Elijah Wood, Evangeline Lilly and Luke Evans, as well as Benedict Cumberbatch and Andy Serkis, teased the end of an era as they showed off the first trailer for the fantasy film at the pop-culture convention.

The trailer showed the dragon Smaug (played by Benedict) setting Lake-town ablaze, Cate's royal elf Galadriel kissing the forehead of Sir Ian McKellen's beleaguered wizard Gandalf and legions of elves and orcs racing into battle.

They also showed a blooper reel that highlighted funny moments left on the cutting room floor from both The Lord Of The Rings and The Hobbit movies. These included Sir Ian riffing on other actors, fluffing his lines and, in one instance, lifting Gandalf's robe to show off a pair of white briefs.

Stephen Colbert, dressed as the shaggy-haired character he cameoed as in The Hobbit, moderated the panel at the San Diego Convention Centre.

"That was really just for you, Stephen," filmmaker Peter Jackson said about the footage. "I mean, these other people are here, as well."

Benedict and Andy, who portray the computer-generated Smaug and Gollum respectively, spoke in character as their fantastical characters when pressed by fans.

The Hobbit: The Battle Of The Five Armies is set to debut in December.