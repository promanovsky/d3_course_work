A new study found a causal link between high blood pressure and vitamin D deficiencies.

The findings suggest that making sure patients have enough vitamin D could help prevent hypertension, a University of South Australia news release reported.

"We set up a Mendelian randomisation study using genetic data from the D-CarDia collaboration involving more than 140,000 individuals of European ancestry from across Europe and North America," Professor Elina Hyppönen said in the news release.

"We used two common genetic variants that affect circulating 25-hydroxyvitamin D concentrations, which are generally used to determine a person's vitamin D status, to measure the causal effect between vitamin D status and blood pressure and hypertension risk," she said.

The researchers found that for each 10 percent increase in circulating 25-hydroxyvitamin D concentration there was a drop in systolic blood pressure.

"Statistically the association translated to just over an eight percent decrease in the odds of developing hypertension," Hyppönen said.

Some small scale studies have looked at this issue in the past, but this is the first time a direct causal link has been shown. The findings of this study could have important implications for people who struggle with vitamin D deficiencies.

"The potential to prevent and reduce high blood pressure with vitamin D in place of more expensive medications is certainly something researchers can now usefully explore in greater depth," Hyppönen said.

There still a possibility that the finding occurred by chance.

"What will be really important is an independent replication of this study," Prof Hyppönen said. "And to advance the work further, we need to do more research using [randomized] controlled studies to look at and confirm causality and importantly assess the clinical benefits of vitamin D supplements and dosage levels."

The work was funded by the British Heart Foundation, UK Medical Research Council, and Academy of Finland.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.