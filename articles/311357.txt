Singapore: Gold prices remained in range-bound in early trade Tuesday as investors watched developments in violence-wracked Iraq, while platinum steadied after two days of losses as strikes in top producer South Africa came to an end.

South Africa`s AMCU union declared a five-month platinum strike "officially over" on Monday, bringing to an end the longest work stoppage in the country`s history. Miners are expected to return to work this week.

Platinum and palladium had risen in recent months as investors expected the strikes to squeeze supplies.

"An end to the mining strike may keep pressure on the platinum group metals (PGMs) in the near-term as investors may choose to take profits given the recent price run-up," HSBC analysts said in a note.

"While this may be negative for the PGMs in the short-term, we remain bullish in the medium to longer-term from a fundamental perspective due to supply and demand tightness."

Platinum eased 0.5 percent early in Tuesday`s session, before settling to trade up 0.2 percent at USD 1,454.45 an ounce by 0157 GMT. Palldium also rose after early losses.

Meanwhile, spot gold eased nearly 0.1 percent to USD 1,316.70 an ounce.

Gold has been boosted recently by escalating violence in Iraq, where Sunni tribes have joined a militant takeover of northern Iraq.

Secretary of State John Kerry on Monday promised "intense and sustained" U.S. support for Iraq, but said the divided country would only survive if its leaders took urgent steps to bring it together.

Investor sentiment towards gold, often seen as an investment-hedge, has improved with SPDR Gold Trust, the world`s top gold-backed exchange-traded fund, notching a 2.4 tonne increase in holdings to 785.02 tonnes on Monday.

However, physical demand is still weak in major markets China and India as consumers expect prices to fall.