Stephen Colbert dedicated an entire episode to explaining away the terrible tweet read 'round the world and defended his show against those who called for its cancellation on Twitter with the hashtag, #CancelColbert. He also took the time to remind the American people of his show's great value and tried to imagine the world without him - needless to say, it was very bleak.

The Colbert Report reappeared on Monday night in the most dramatic fashion possible. Instead of the typical red, white and blue intro and getting down to business, Colbert took his time staging a satirical look at what the world would look like without him and his show. Native American's cried solitary tears, the Statue of Liberty was frozen over, all of the staff was fired and the Colbert Report awning was taken down. It was all quite traumatic, really.

Except for the part where it wasn't real. Colbert came onscreen looking like a man vindicated.

"Folks, I'm still here," he said. "The dark forces trying to silence my message of core conservative principles mixed with youth friendly product placement have been thwarted."

Then, he turned introspective, considering what would have happened to the world had the #CancelColbert campaign actually been successful.

"We almost lost me," he said, emotionally. "I'm never going to take me for granted ever again."

He even spent the entire "Who's Attacking Me Now?" segment explaining what happened with "The Ching-Chong Ding-Dong Foundation for Sensitivity to Orientals or Whatever" Twitter debacle. Essentially, Colbert was making fun of the Washington Redskins owner Daniel Snyder and his brand-new charity, called "The Washington Redskins Original Americans Foundation." Colbert thought he would amplify the irony of Snyder's choice of charity by creating his own fake charity, mocking another ethnic group: Asians.

Unfortunately, Asians on Twitter didn't get the joke. Many tweeted back angrily and eventually, those offended by the Colbert Report's tweet joined together under the hashtag banner of #CancelColbert. Colbert apologized for the tweet and explained its origin on Monday night, in hopes of assuaging viewers.

"Internet equality is more important than ever, as I learned this weekend when the interwebs tried to swallow me whole," he explained. "But I am proud to say that I got lodged in its throat and it hacked me back up like a hastily chewed chicken wing."

Colbert also explained that someone else is responsible for the tweets that are posted on the @ColbertReport account, while his @StephenAtHome is his personal Twitter name that he actually uses.

"When the twit hit the fan," he said. "The brain trust over at my network took the tweet down."

"I don't even see race," Colbert said, claiming that there was no way that he was racist.

Even though Colbert's explanation and mock mourning over the possible demise of his show were hilarious and true, perhaps the best part of Monday night's show was his perfect analysis of Twitter:

"Who would have thought that a means of communication limited to 140 characters would ever create misunderstandings?"

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.