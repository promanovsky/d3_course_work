The ECB's Mario Draghi has given his clearest indication yet that the bank could print money to buy assets if inflation slows.

EUROPEAN Central Bank President (ECB) Mario Draghi gave his clearest indication yet that the bank could print money to buy assets if inflation slows, and identified a rise in the euro as a potential trigger for action.

While stressing that the ECB sees inflation remaining low for a prolonged period before rising towards its target of just below 2pc, Draghi set out three scenarios for ECB action.

He said that all else being equal, a rise in the euro could also prompt the ECB to act.

The euro fell to $1.3815 after Draghi's comments were released from around $1.3835 before he spoke, regaining some ground later.

Draghi (below) said a worsening of the medium-term outlook for inflation "would be the context for a more broad-based asset purchase programme".

The comments were the clearest indication yet from Draghi that the ECB would be prepared to embark on a full-blown quantitative easing (QE) programme – printing money to buy assets – if the inflation outlook deteriorated further.

Eurozone inflation is running at 0.5pc – far below the ECB's medium-term target – although Draghi again said the bank sees it remaining low for a prolonged period before gradually rising back towards 2pc.

The ECB's June policy meeting is growing in significance as economists expect an inflation reading due on April 30 to show a monthly rise. A Reuters poll points to a reading of 0.7pc.

Draghi set out a scenario for responding to "an undue tightening" of monetary conditions.

Should that happen, the ECB could respond with a variety of conventional measures, among them "a further lowering of the interest rate corridor, including a negative deposit rate".

"Finally, if necessary, the measures could include new liquidity injections via our liquidity operations, including longer-term fixed-rate operations," he said in his speech, entitled 'Monetary Policy Communication in Turbulent Times'.

Irish Independent