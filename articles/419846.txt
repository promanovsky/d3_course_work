The iPhone 6's unveiling date has been all but confirmed to be Sept. 9, and it's not the only new gadget we might see that day. According to reports from Re/code, Apple's long rumored iWatch will be revealed alongside its smartphone cousin, in what could be one of Apple's biggest product launch events yet.

While this latest report conflicts with rumors about the iWatch's purported October launch window, it makes a whole lot of sense for Apple to debut its first-ever wearable alongside the highly anticipated iPhone 6. The smartwatch has been rumored to function as a companion device that requires an iPhone to function, and showing off the two devices in unison could ultimately be what sells consumers on Apple's entry into the already-crowded wearables space.

MORE: Why the iWatch Already Beats Android Wear

If the iWatch lives up to speculation, the device will sport a curved OLED display, and will presumably deliver iOS notifications and apps to your wrist. The smartwatch is expected to sport biometric sensors for tracking stats like calories burned, sleep and even blood oxygen levels, which would conveniently funnel into Apple's HealthKit interface that's coming to iOS 8 this fall.

Both the iWatch and iPhone 6 have had almost as many rumored release dates as Apple has smartphones, but it seems like a safe bet that both devices will hit the market before the year is over. With new smartwatches like the Motorola Moto 360, Samsung Galaxy Gear Solo and ASUS ZenWatch all expected to arrive soon, a Sept. 9 launch date for the iWatch can help ensure Apple gets an early start in this fall's scrappy wearable race.

Mike Andronico is an Associate Editor at Tom's Guide. He doesn't wear a watch. Follow Mike @MikeAndronico and on Google+. Follow us @TomsGuide, on Facebook and on Google+

Copyright 2014 Toms Guides , a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.