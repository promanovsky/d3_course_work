LOS ANGELES, July 2 (UPI) -- Susan Sarandon plays a flirtatious grandmother trying to prove she still has "it" in the movie Tammy.

Melissa McCarthy plays Tammy, a woman who goes to stay with her crude, boozing grandmother Pearl (Sarandon) after she loses her job and learns that her husband has been cheating on her.

Advertisement

Tammy hits theaterson July 2.