Nick Young and Iggy Azalea are the newest ‘it’ couple, and the NBA star and ‘Fancy’ singer are totally adorable together! Find out below how Nick keeps the romance alive!

Nick Young, 29, and Iggy Azalea, 24, are basically the most adorable couple ever whenever they hit an event together, and their appearance at the 2014 EPSY Awards on July 16 was no exception — even though Drake called them a “poor man’s Jay [Z] and [Beyonce]!” Shady, shady. Anyway, HollywoodLife.com spoke to Nick EXCLUSIVELY that very night, and he told us all about how he keeps the romance alive in their busy lives!

Nick Young Dating Iggy Azalea: The Romantic Way He Wins Her Heart

“Fancy” is basically the song of the summer, as much as some haters try to deny it, and Iggy has become one busy girl. Nick, for his part, is in the off season from playing for the Los Angeles Lakers, so he does his best to support his girl.

“We always hang out and I always try to take her mind off what is going on out there,” Nick told HollywoodLife.com. “We will go roller skating or we will go to Six Flags — we have a good time!

Nick Young: ‘I Have To Judge [Iggy Azalea] On How She Dresses’

So sweet! This couple’s the realest. They can also make fun of each other, too. We asked Nick whether he’d be watching Iggy perform or whether he’d be sticking to red carpet duties, and he replied that he’d be doing “A little bit of both, a little bit of both!

“I will walk the red carpet but I feel that I will be watching a little bit because I have to judge her on how she dresses, she better be prepared, I am going to be hard on her!” So funny. And considering that Iggy looked amazing at the ESPY Awards, we’re pretty sure that she passed Nick’s “strict” standards.

So, HollywoodLifers, what do you think of Iggy and Swaggy P as a couple? Love ’em? Hate ’em? Are you just jelly? Let us know!

— Amanda Mitchell

More Iggy Azalea & Nick Young News: