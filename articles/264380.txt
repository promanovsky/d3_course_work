Google’s self-driving car has stop and go buttons and a top speed of 25mph[GOOGLE/AP]

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up forfor the biggest new releases, reviews and tech hacks

The US technology giant previously fitted sensors and computers to existing cars, but now plans to build 100 of its own vehicles.

The Google car has no steering wheel or pedals, just stop and go buttons and a screen to display the route.

The prototype, which has an appealing friendly “face” created by the headlights and detailing on the front, seats just two people but does have storage space.

Project director Chris Urmson said: “They won’t have a steering wheel, accelerator pedal or brake pedal because they don’t need them.

"Our software and sensors do all the work.

"The vehicles will be very basic.

"We want to learn from them and adapt them as quickly as possible, but they’ll take you where you want at the push of a button.”

Google believes self-driving cars will help improve road safety and transform mobility.