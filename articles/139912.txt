Our April cover story, "Stay Anonymous Online," drove a lot of sales and more than a little bit of controversy. In a world where we are constantly tracked by retailers, ISPs, brands, friends, the NSA, and yes, even publishers, it turns out the members of the American public are desperate to protect the last precious details of their private lives. Our advice was solid and useful. And then Heartbleed made it all moot.

Heartbleed, as you probably know by now, is the biggest hole ever found in the fundamental infrastructure of the Internet. That little lock in the upper-right corner of your browser that we tech guys keep telling you to look for, the one that means you've established a secure connection? It turns out it's been broken since 2011.

To be clear, Heartbleed is not a "virus," as one clueless host for National Public Radio recently described it. It is a vulnerability in OpenSSL, which is an open-source encryption protocol that establishes a secure connection between a client and a server. By misrepresenting the data that travels between the two systems, information can be captured from the memory of one or both.

Subscribe today to the PC Magazine Digital Edition for iOS devices.

Heartbleed was caused by a simple coding error made by a German programmer on New Years' Eve 2011. No one caught it. Rather, if someone did catch it, they didn't say anything, which may be even scarier.

You may not think you use OpenSLL, but you do. It's used by two-thirds of all websites. Banks, search engines, online retailers, and even connected home appliances use the protocol. OpenSSL support is built into a lot of networking hardware and it may even be used by the very VPN client your company depends on to secure its internal systems.

The exposed information could be anything residing in your system memory, so it would take some sifting to find the valuable stuff. Making sense of it would not be trivial, but it's possible. Passwords, Social Security numbers, emails, documents—all of them could be vulnerable. If someone wanted to intercept information on a "secure" connection, they could do it.

Worse, there isn't much you can do to protect yourself. All of the affected sites and services are rolling out a new patched version of OpenSSL, but until they do there is no real reason to change your passwords. More important, if anyone has a collection of existing network traffic from the Heartbleed era, there is nothing to keep them from using the Heartbleed vulnerability to decrypt that data set. The damage can't be undone.

It seems like there should be more to the story, but there really isn't. A massive hole in the Internet left traffic exposed for years. No one knows if it was exploited. The industry is fixing it. There is nothing you can do.

No, this is not exactly the kind of technological empowerment we are fans of here at PC Magazine, but we may have to get used to this story. It could happen again.

On a much lighter note, we test and review two excellent smartphones in this issue. The HTC One (M8) earns high marks for its build quality and sophistication. The Galaxy S5 carries on Samsung's tradition of loading its flagship phones with every conceivable feature. They are both outstanding phones; which you prefer is as much a matter of personal taste, but our review can help you make the choice.

The good news is that both phones are running the latest version of Android, version 4.4. Did I mention that more than 90 million Android 4.1 devices are still vulnerable to Heartbleed and will probably never get updates?

Could be time for an upgrade.