First is that Carl Icahn has further cemented his legacy in the Activist Investor Hall of Fame. According to filings, Icahn's 9.4-percent stake in Family Dollar that he disclosed on June 6 is valued at $174 million as of today. The second angle is a Dollar Tree led by its current CEO, not the failing leader of Family Dollar Howard Levine, will move quickly to shutter underperforming Family Dollar stores and those that operate alongside a Dollar Tree at strip centers.



Read MoreWhy Carl Icahn has Family Dollar in his crosshairs

Another point is that Walmart is in for BIG trouble for staying true to its insular corporate culture (one where it believes it's doing everything correctly) and failing to pay a premium to acquire instant square footage and name reorganization inherent to Family Dollar.

For Walmart, as soon as the Dollar Tree/Family Dollar deal closes in early 2015, it will immediately start the clock on a competitive firestorm. Here is what to look for:



1. A monster dollar store will have been formed that is closer to the economically-sensitive customers in rural and urban markets that view a trip to Walmart as expensive. That is, if said customer has an automobile to venture out to a Walmart.



2. A monster dollar store that will invest its deal synergies (estimated loosely at $300 million ... I expect more as the Dollar Tree cleans up the operational mess that is Family Dollar) to price its merchandise more competitively (especially doing the holidays), counteracting Walmart's ongoing efforts to reduce prices for food and consumable products. Here come the weekend circular price wars!

Read MoreFamily Dollar deal could shake up discount retail

3. A monster dollar store dead set on continuing to gobble up new retail sites and open stores that boast improved interior layouts and expanded selections, notably in fresh and frozen foods. Such an inevitable outcome hints at below plan returns for Walmart's new, snazzy-looking smaller store formats called Neighborhood Markets and Express, which the company has pitched to its widow and orphan investor base as its next long-term sales and earnings growth drivers.