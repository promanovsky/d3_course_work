Zee Media Bureau

New Delhi: There is some good news for people suffering from hear loss as researchers have found a way to use electrical pulses from cochlear implants to deliver new genes into the ear and regrow damaged auditory nerves.

Researchers at University of New South Wales (UNSW) in Australia studied for five years and used power from a cochlear implant to deliver gene therapy to regrow auditory nerves and help in hearing.

The five-year study focused on regenerating surviving nerves after age-related or environmental hearing loss, using existing cochlear technology, which can partially restore hearing by providing direct electrical stimulation to auditory nerves in the inner ear.

Current cochlear implants cannot restore hearing to normal, and the electrode design has changed little in decades.

According to the researchers, auditory nerve endings can regenerate if neurotrophins, a naturally occurring family of proteins crucial for the development, function and survival of neurons, are delivered to the auditory portion of the inner ear known as the cochlea.

But until now, research has stalled because safe, localised delivery of the neurotrophins can neither be achieved using drug delivery, nor by viral-based gene therapy.

In the new study involving completely deaf guinea pigs, the researchers developed a way of using electrical pulses delivered from the cochlear implant to deliver a solution of DNA containing the brain-derived neurotrophic factor (BDNF) gene to the cells close to the array of implanted electrodes.

Within a few hours, cells in the cochlea took up the DNA and started to produce neurotrophins, they said. The researchers tested the animals` hearing using the auditory brain-stem response test, a common technique used to measure hearing in newborn babies.

They found that animals that were once completely deaf had their hearing restored to almost normal levels.

The research has been published in the journal Science Translational Medicine.

(With Agencies inputs)