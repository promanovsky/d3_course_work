Topping tech headlines Monday, Microsoft released OneNote for Mac and announced that all versions of its note-taking and sharing software are now free.

The Mac version joins those already available for the PC, Windows tablet, Windows Phone, iPad, iPhone, Android, and the Web. Content syncs across all platforms and users get 7GB of free OneDrive cloud storage with no monthly upload limits. Interested customers can visit onenote.com to download the application for free. OneNote also now includes a cloud API, so any developer can integrate the program into their app or service.

In other Apple news, rumors of an 8GB iPhone 5c hit the Web Monday. Cupertino followed through today, releasing the smaller-capacity iPhone 5c in Europe. It sports the same features as the iPhone 5c that launched in September.

Meanwhile, U.S. officials are taking steps to relinquish control of the Internet address system. The National Telecommunications and Information Administration (NTIA) within the Commerce Department has asked the Internet Corporation for Assigned Names and Numbers (ICANN) to start the process of transitioning oversight of the Internet's domain name system (DNS) to a non-governmental entity. For more, see Stop the TinFoil Hat Misinformation About ICANN.

Be sure to check out PCMag Live from Monday in the video, as well as a few other stories making headlines in the links below.

Further Reading

Game Reviews