Skipping Friday for some big phone launches, T-Mobile CEO John Legere has finally unveiled the last in a trio of announcements. This time, the carrier is doing away with domestic overage fees.

The Un-carrier promises it's eradicating all domestic overage charges for its customers. This new policy applies to all users on T-Mobile's consumer plans whether it be a Simple Choice plan, the new Simple Starter, or an older plan.

Overage fees will disappear for T-Mobile customers starting May 1, meaning they won't show up on your June bill.

Challenging carriers

Legere also tied the announcement with a Change.org petition calling for other carriers to do the same.

"The Un-carrier is eliminating one of the most widely despised wireless industry practices," Legere said in a statement. "And I'm also laying down a challenge to my counterparts at AT&T, Verizon and Sprint, to do the same."

No stranger to leading social media uprisings, Legere previously introduced T-Mobile's "Jump!" upgrade program with a call to arms on Twitter using the hashtag #Hate2Wait.

It should be interesting to see how Verizon, AT&T and Sprint move in reaction to all these new initiatives. As we've seen previously, all the other carriers quickly implemented their own early upgrade services soon after Jump! was announced last July

The total package

T-Mobile has made some major strides to change up its plans and give customers more bang for their buck. Last week, Legere started this latest Un-carrier push by unveiling a new $40/month Simple Starter plan with unlimited talk and text, plus 500MB of 4G data.

It followed up the next day with Operation Tablet Freedom, giving 4G LTE tablet users, who also have a voice plan with the company, 1.2GB of free mobile data. The Un-carrier also promised to offer 4G-connected tablets for the same price as their Wi-Fi-only variants.

On April 11, the company put things on hold for the launch of the Samsung Galaxy S5 and the HTC One M8. It offered the two new high-end Android phones for nothing down with no annual service contract and no extra fees attached.

Legere also promised he has something even bigger planned for later this year with a new Un-carrier 5.0 initiative. Stay tuned for more.