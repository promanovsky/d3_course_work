So, how cute are these two? Emma Stone and Andrew Garfield had a message for paparazzi when they stepped out in New York on Tuesday.

“The Amazing Spider-Man 2" stars, who have been dating since 2011, strolled around the Big Apple concealing their faces with signs that bore handwritten messages directing attention to a number of youth and cancer organizations.

The photographers were following them anyway, so why not?

“Good morning!” Stone’s note said. “We were eating and saw a group of guys with cameras outside. And so we thought, let’s try this again. We don’t need the attention, but these wonderful organizations do.” It ended with an arrow pointing toward her beau’s missive.

Advertisement

Garfield’s note continued with “www.youthmentoring.org, www.autismspeaks.org, (and don’t forget:) www.wwo.org, www.gildasclubnyc.org. Here’s to the stuff that matters. Have a good day!”

Using their powers for good? How very heroic.

It’s not the first time the high-profile pair have used their celebrity to promote nonprofits. In September 2012, they pulled a similar stunt that also spotlighted the Worldwide Orphans Foundation and Gilda’s Club.

Take note, Shia LaBeouf.

Advertisement

They saw the sign. Follow me @NardineSaad.