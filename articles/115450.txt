Emmy and Golden Globe-winner Bryan Cranston helped out a fan in need.

Following a performance on Broadway Friday night, the former "Breaking Bad" star was greeted by fans outside the Neil Simon Theatre. One of the autograph-seekers, who identified himself as Stefan, asked the actor if he could record a message to his potential prom date.

Cranston obliged, telling "Maddy" that she should accept the date request, or else "tread lightly" as Walter White would say.

Cranston is starring on Broadway as President Lyndon B. Johnson on Broadway in "All The Way," running until June 29.

He will next be seen on the big screen in the new "Godzilla" film, which opens in theaters on May 16.