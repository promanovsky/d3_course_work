Codenomicon

We've all woken up on New Year's Day regretting what happened the night before, but this puts things in perspective: the man who accidentally introduced the Heartbleed bug to the Web did so on New Year's Eve.

Heartbleed is the name given to a vulnerability in the OpenSSL underpinning large sections of the Web, which potentially exposes passwords and other data from various sites. The code that contains the bug was written by programmer Robin Seggelmann, who admits he "missed the necessary validation by an oversight."

Seggelmann wrote the code in question to enable a function called Heartbeat in OpenSSL's Transport Layer Security system, which exchanges a packet of random data between your computer and a server to confirm they're connected.

The problem is that hackers can tell their computer to lie about how much data is in the original packet, which causes the server to pad out the packet with data pulled from its memory before sending it back.

Now playing: Watch this: Tips to protect yourself from Heartbleed

Seggelmann submitted the code at 11:59pm on New Year's Eve 2011, but claims the timing had nothing to do with the mistake. Although the bug was also missed by the review process for OpenSSL, an open source project written and reviewed by volunteers, Seggelmann told British newspaper The Guardian that the bug's eventual discovery shows the value of publically available open source code.

The vulnerability was spotted this week by researchers at Google and Codenomicon, and has seen tech companies and websites scrambling to close the loophole before anything bad happens -- find out here which sites have protected themselves against Heartbleed.