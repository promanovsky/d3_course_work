Microsoft (NASDAQ:MSFT) is beating the drum in regards to the end of mainstream support of key Windows products such as Windows 7, Windows Server 2003, and Office 2010 Service Pack 1. Mainstream support for Windows 7 is scheduled to come to an end on Jan. 13, 2015, after which the OS will be placed under extended support. Support for Office 2010 SP1 ends on Oct. 14, 2014. Meanwhile, mainstream support for Windows Server will come to an end on Jan.13, 2015.

Below are a few definitions worth knowing:

Mainstream support: This is the five-year period during which Microsoft offers free security patches and fixes, as well as security updates for its products.

Extended support: This is the period (often five years) during which Microsoft offers free security fixes for its products, but requires users to pay for other types of updates.

End of support: This is when Microsoft completely ends support for its products (like it did with Windows XP in April of this year.) it no longer provides any security updates, or any other updates, whatsoever.

The withdrawal of mainstream support for Windows 7 means that Microsoft will continue to provide free security fixes for the OS for another five years, but will require users to pay for other types of updates.

Windows 7 is the most popular Desktop OS

Windows 7, with a market share of 50.55%, is not only the most popular Windows OS, but is also the most popular Desktop OS in the world. Over half of the world's PCs are currently running on Windows 7.

PC Operating System Market Share % Windows 7 50.55 Windows XP 25.31 Windows 8 5.93 Windows 8.1 6.61 Mac OS X 10.9 3.95 Windows Vista 2.95 Linux 1.74 Others 2.96

Importance of the Windows ecosystem to Microsoft

Despite having made some big missteps during the mobile revolution, Microsoft remains a very profitable company. This is thanks mainly to its huge installed base of Windows OSes, Windows Server technologies, and Microsoft Office. Microsoft made $19.23 billion, or 24.7% of its overall fiscal 2013 revenue, from the Windows ecosystem, including revenue from Windows for Surface tablet and other hardware.

Windows XP is the second-most popular desktop OS, with a 25.31% chunk of the market. Interestingly, Windows XP has more than double the combined market shares of Windows 8 and Windows 8.1, three months after Microsoft ended support for the OS.

A study done last year by Tech Pro Research revealed that the majority of Windows XP users planned to shift base to Windows 7 once Microsoft withdrew its XP support. Fully 37% of Windows XP users planned to stick with the venerable OS.

Looking at how market shares of Windows XP and Windows 7 have been trending for the last 12 months, the Tech Pro Research study appears quite valid. Windows XP had a 33.66% share of the market vs. 45.63% for Windows 7 in August 2013. During the last 12 months, Windows XP market share has fallen to 25.31% while Windows 7 has shot up to 50.55%; this suggests that Windows XP users could be shifting to Windows 7.

Meanwhile Windows 8 market share has fallen from 7.41% to 5.93% over a similar period, while Windows 8.1 market share has grown from 0.24% to 6.61%. Additionally, the market share for ''Others'' has fallen from 13.01% to 7.65%. Interestingly, Apple's (NASDAQ:AAPL) Mac OS X 10.9 Mavericks has seen its market share jump from 0.05% to 3.95% over the same period. This suggests that the OS is also becoming increasingly popular with users.

Windows XP still has a lot of users clinging to it, and it appears as if many are gradually moving to Windows 7. However, once Microsoft withdraws mainstream support for Windows 7, it's very likely that many users will shun Windows 7 for another OS. The Tech Pro study revealed that a full 11% of Windows XP users preferred moving to Linux compared to just 5% for Windows 8 and 8.1 combined. Withdrawing mainstream support for Windows 7 is only likely to accelerate this process.

If 20% of the remaining Windows XP users now decide to jump ship and join the Linux camp, and a few more decide to move to the Mac camp after Microsoft withdraws mainstream support for Windows 7, that would in turn mean that the Windows ecosystem loses at least 5.54% of its overall users. That might not look like too much, but when you consider that Microsoft made over $19 billion from the Windows ecosystem last year, that level of customer churn would mean the company loses over $1 billion in potential revenue.

Microsoft loses $1 billion on Xbox sales, and another $400 million on Surface Tablet sales each year. The company uses profits from its lucrative Android patent portfolio to support these loss-making ventures. With these patents now threatened, however, the continued existence of Xbox and the Surface could be in serious jeopardy if Microsoft experiences a huge attrition rate in its Windows user base.

Conclusion

It would perhaps be more prudent for Microsoft to first develop a desktop OS that will achieve the popularity and wide acceptance of Windows 7 or Windows XP before withdrawing mainstream support for Windows 7. From the look of things, Windows 8 or Windows 8.1 just won't cut it. Without an extension, the Redmond giant risks losing a sizable chunk of Windows users to rival OSes.