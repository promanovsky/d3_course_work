The iPhone 6 is expected to be released later this year and there have been many rumors surrounding the specs of the device. For the most part the rumors are claiming that Apple will turn to the use of a larger display that would finally put them on par with Android devices who have long had smartphones with huge displays.

Advertising

Now thanks to a new rumor from electronics analyst, Sun Chang Xu (via her Weibo account), it seems that additional features of the iPhone 6 have been revealed, and these features include new sensors built into the phone such as a temperature, atmospheric pressure, and humidity sensors.

We’re not sure what kind of plans Apple might have with these sensors, but it should be interesting to see what Apple will be able to come up with. In fact Samsung’s Galaxy S4 actually did come with an atmospheric pressure sensor of its own, although we guess it wasn’t really that big of a deal since no one really talked about it.

It is possible that with these sensors, Apple might be trying to cater to the outdoors user who might require such sensors and readings if they were planning a camping trip or a hiking trip up a mountain where such information might come in handy.

Interestingly enough there was no mention about any health-related sensors, given that recent alleged iOS 8 screenshots showed an app called “Healthbook” and the fact that Apple has been on a health expert hiring spree, although it has been speculated that those hirings might be iWatch related, not necessarily iPhone-related.

Filed in . Read more about Iphone 6.