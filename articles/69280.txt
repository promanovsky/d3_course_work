When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up fornow for the biggest moments from morning TV

The actress passed away on Sunday morning (30Mar14) at a nursing home in Sussex after a short illness.

Paying tribute to his client, her agent Phil Belfield says, "She was extraordinary, she had so much energy and vitality with a love for theatre and acting.

"A shining star has gone out and Kate will be dearly missed by all who knew and have worked with her."

O'Mara, famed for her glamorous looks, started her career in the 1960s, but became a household name after playing Joan Collins' manipulative sister Cassandra Morrell in 1980s soap opera Dynasty.

She also enjoyed high-profile roles in sci-fi series Doctor Who and TV drama Howard's Way, as well as guest appearances in cult comedies Absolutely Fabulous and Benidorm in 2012, which became her final on-screen performance.