A recent report by the Center for Science in the Public Interest reveals the unhealthiest restaurant meals in America.

The center's nutrition experts reviewed menus at 200 restaurant chains in the U.S. to identify and rank the nation's unhealthiest meals for its annual "Xtreme Eating" list.

Restaurants who made the list include The Cheesecake Factory, Maggiano's, and Red Robin.

Advertisement

Here's the list, ranked lowest to highest by calories.





8. The Cheesecake Factory: Reese's Peanut Butter Chocolate Cake Cheesecake (1,500 calories)



Fudge, caramel, and Reese's Peanut Butter Cups are combined with the restaurant's classic cheesecake filling to make this indulgent treat. The dessert has 1,500 calories, 43 grams of saturated fat and 21 teaspoons of sugar.



"It's like eating an entire four-serving (17 oz.) Sara Lee frozen Classic Original Cream Cheesecake topped with a cup of Breyers Chocolate Ice Cream," the report says.

Advertisement

7. Chevys Fresh Mex: Super Cinco Combo (1,920 calories)





The dish includes two enchiladas - one beef and one chicken - a taco, a pork tamale and a chile relleno for 1,920 calories, 36 grams of saturated fat and 3,950 grams of milligrams of sodium.

6. BJ's Restaurant and Brewhouse: Signature Deep Dish Chicken Bacon Ranch Pizza (2,160 calories)





Advertisement

The dish contains "grilled garlic chicken, smoked bacon, jack and cheddar cheese, red onions, diced tomatoes and a drizzle of ranch," according to the restaurant's description. With the deep dish crust it comes to 2,160 calories, 30 grams of saturated fat, and 4,680 milligrams of sodium, according to the report.

5. The Cheesecake Factory: Farfalle with Roasted Chicken and Roasted Garlic (2,410 calories)





Advertisement

4. Maggiano's Little Italy: Prime New York Steak Contadina Style (2,420 calories)

This pasta dish contains chicken, mushrooms ,tomato, pancetta, peas and caramelized onions in a cream sauce for 2,410 calories, 63 grams of saturated fat and 1,370 milligrams of sodium.

The steak is prepared "Contadina Style," meaning it includes two Italian sausage links, potatoes, roasted peppers and mushrooms, caramelized onions, sun-dried tomatoes, steak jus, and garlic butter.

"The 1,250- calorie steak alone is like eating five McDonald's Quarter Pounder beef patties," according to the report. "The 1,170-calorie Contadina-style 'garnish' adds another four Quarter Pounder patties, with a Cheeseburger on the side.

Advertisement

3. Famous Dave's: "Big Slab" of St. Louis-Style Spareribs (2,770 calories)

"Each slab, which is 'slathered with sauce over an open flame,' yields nearly 1.5 pounds of meat," according to the report. The meal comes with two sides and a corn bread muffin. If you choose fries and baked beans as your sides, you are eating 2,770 calories, 54 grams of saturated fat, 4,320 milligrams of sodium and 14 teaspoons of sugar.

2. The Cheesecake Factory: Bruléed French Toast (2,780 calories)

The meal has 66 grams of saturated fat and 5,620 milligrams of sodium.

With 2,780 calories, 93 grams of saturated fat (almost five days' worth), 2,230 milligrams of sodium, and 24 teaspoons of sugar, "

you'd have to swim laps for seven hours" to burn off this meal.

Advertisement

1. Red Robin 'Monster' Burger and Bottomless Fries (3,540 calories)

The most unhealthy meal in America is Red Robin's massive "Monster" burger and "bottomless" fries.

Advertisement

The 3,540-calorie meal, which also includes a salted caramel milkshake, contains a whopping 69 grams of saturated fat and 6,280 milligrams of sodium, which is four times the recommended daily serving. The dish is estimated to contain almost three quarters of a cup of added sugar. "To dispose of those calories, the average person would need to walk briskly for a full 12 hours," according to the report.