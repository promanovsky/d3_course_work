It has recently been reported that L'Wren Scott was quite troubled and struggling, just four days prior to committing suicide in her Manhattan apartment.

Crazy Days and Nights, published a post on Thursday, March 13 about how a "C list celebrity but is A list in her own little corner of the celebrity world" had disappeared and was going through a rough time with her cheating, shorter "permanent A-lister" boyfriend.

Although the name of the subject in question was left out of the post, the blogger later updated it after Scott's tragic death was confirmed on Monday and claimed that the designer was indeed the subject.

Mick Jagger's representative immediately denied any claims that he and Scott had separated by saying that "of course" the long-term couple was still together at the time of the recent death, despite the fact that the cause for the Rolling Stones frontman's two previous marriages was cheating.

Although it is not clear how the anonymous writer would have any inside knowledge to substantiate the previous claims, the blind items alludes to the fact that L'Wren Scott cancelled her London Fashion Week show a few months before her death. At the time, she cited delayed fabric orders but her financial filings have shown that the U.K. end of her luxury brand was in debt.

"There are reports she had a breakdown because he was cheating on her," the post reads. "She cancelled a huge event she was working on and has retreated into a hospital to get help although some people say she is hiding out at her home with medical professionals."

It has been also claimed that Scott's friends were aware that she was going through a difficult time, however no one even attempted to help her. "Her stylist friends, fashion friends, p.r. friends, critic friends all say they knew she was hemorrhaging financially," a source said. "Nobody helped."