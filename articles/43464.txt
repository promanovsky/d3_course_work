A small U.S. study raises new questions about whether using electronic cigarettes will lead people to quit smoking, adding to the debate over how tightly the products should be regulated.

The study, which looked at the habits of 88 smokers who also used e-cigarettes, was published as a research letter in the journal JAMA Internal Medicine on Monday. It found that smokers who also used e-cigarettes were no more likely to quit smoking after a year, compared to smokers who didn't use the devices.

Outside experts say the small number of respondents, and a lack of data on whether they intentionally used e-cigarettes to help them quit smoking, mean the findings from the Center for Tobacco Control Research and Education at the University of California, San Francisco can't take the place of much more rigorous study on the subject.

E-cigarettes were first introduced in China in 2004 and have since grown into a $2 billion industry. The battery-powered devices let users inhale nicotine-infused vapors, which don't contain the harmful tar and carbon monoxide in tobacco.

At issue is how strictly U.S. health regulators should control the products. Advocates say e-cigarettes can help smokers quit. Public health experts fear they can serve as a gateway to smoking for the uninitiated, particularly teenagers. Leading U.S. brands include blu by Lorillard Inc and products from privately-held NJOY and Logic Technology.

A previous report from the UK found that people who use e-cigarettes primarily want to replace traditional cigarettes (see Reuters Health story here: reut.rs/1ceF7nT).

"We did not find a relationship between using an e-cigarette and reducing cigarette consumption," Rachel Grana, the lead researcher on the new study, told Reuters Health.

Grana and colleagues at the University of California, San Francisco analyzed 2011 survey data collected from 949 smokers. Of those, 88 reported using e-cigarettes.

When the researchers looked at those smokers' responses a year later, they found that the people who reported using e-cigarettes in the 2011 survey were no more likely to quit smoking than the people who didn't use e-cigarettes.

For those who were still smoking in 2012, using e-cigarettes also didn't appear to change how many traditional cigarettes people smoked per day.

The researchers note that the small number of participants who reported using e-cigarettes may have limited their ability to detect a link between quitting smoking and using the device.

Dr. Michael Siegel, who was not involved with the new research, told Reuters Health that the new study had several design flaws, including that the researchers did not know why some of the participants tried e-cigarettes or how long they had used them. Siegel is an expert on community health at Boston University School of Public Health and has studied e-cigarette research.

By comparing people who smoked regular cigarettes and those who smoked e-cigarettes, the researchers are assuming "that the groups are exactly equivalent in terms of their motivations and their levels of addiction to cigarettes," Siegel said. "You can't make those assumptions. You're not dealing with comparable groups."

In an emailed statement, Grana and fellow researchers acknowledged that they did not have information on the participants' motivations to use e-cigarettes, but said their analysis took into account other factors known to be linked to quitting smoking, such as their stated intention to quit and how many cigarettes they already smoked each day.

"These factors may also reflect motivations to use e-cigarettes, as e-cigarettes are frequently marketed and perceived as cessation aids," they wrote. "While these factors predicted quitting as expected, we found that e-cigarette use did not predict quitting.

Siegel also pointed out that only about eight percent of the people surveyed said they had any intention to quit smoking within the next month. He hopes people will reserve judgment on e-cigarettes until randomized controlled studies - considered the "gold standard" of medical research - are published.

"We need solid data that's based on solid science before we make decisions," he said. "I hope no one would take this research letter and make any conclusion based on it."