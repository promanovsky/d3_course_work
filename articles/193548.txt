Scientists have discovered 14 new species of 'dancing frogs' in the jungle mountains of the Western Ghats in southern India.

Numbers of the tiny acrobatic amphibians, named after the unusual kicks they use to attract mates, have been declining dramatically during the 12 years in which the species were chronicled through morphological descriptions and molecular DNA markers.

The frogs breed after the annual monsoon season in fast-flowing streams, but their habitats are rapidly declining.

"It's like a Hollywood movie, both joyful and sad. On the one hand, we have brought these beautiful frogs into public knowledge. But about 80% are outside protected areas, and in some places, it was as if nature itself was crying," said Sathyabhama Das Biju, a University of Delhi professor and the project's lead scientist.

The tiny frogs are no larger than a walnut and are easily swept away by stream currents. This means breeding only takes place once the level of the water has dropped significantly. If streams hold less water or dry out too early, the frogs do not have the right conditions to mate.

"Compared with other frogs, these are so sensitive to this habitat that any change might be devastating for them," Biju said. "Back in 2006, we saw maybe 400 to 500 hopping around during the egg-laying season. But each year there were less, and in the end even if you worked very hard it was difficult to catch even 100."

The research has revealed the number of known Indian dancing frogs is now just 24. They live in the Western Ghats, a mountain range along the western side of the country, with the majority of the species living in a single, small area.

Seven of the frog species live in highly degraded habitats, where logging or new plantations were taking over. Another 12 species were in areas that appeared in ecological decline.

Despite their name, only the male frogs dance - in a unique breeding behaviour called foot-flagging. The amphibians whip their legs out to garner the attention of females who may not be able to hear mating croaks over the sound of flowing water.

The bigger the frog, the more they dance. The movement is also used to fend off rival males, as the sex ratio for amphibians is around 100 males to one female.

There are other dancing frogs in Central America and Southeast Asia, but the Indian family, known by the scientific name Micrixalidae, evolved separately about 85 million years ago.

Around 325 globally threatened animal species live in the Western Ghats, which is a "biodiversity hotspot".

In 2010, a report by India's Environment Ministry said the area was likely to be hard-hit by changing rainfall patterns due to climate change, as well as erratic monsoon patterns.

Amphibians are particularly vulnerable. At least one-third of the world's known 6,000 frog species are threatened with extinction from habitat loss, pollution, changing temperatures or exotic diseases spread by invasive animals and pests, according to the Global Wildlife Conservation.

The research was published in the Ceylon Journal of Science.