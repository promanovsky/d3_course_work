Game of Thrones writer George RR Martin has offered a glimpse into the latest installment from his A Song of Ice and Fire series.

The author released an entire chapter from the unfinished book called The Winds of Winter on his website.

Picture Perfect



According to the 65-year-old author, the chapter titled 'Mercy' has been with him for a while and was rewritten a dozen times.

"The first draft was written more than a decade ago," Martin said. "Originally, it was intended to be the opening Arya chapter after the infamous 'five-year gap', her first appearance in A Dance with Dragons, as initially conceived."

The chapter follows a character of the same name and opens: "She woke with a gasp, not knowing who she was, or where.

"The smell of blood was heavy in her nostrils… or was that her nightmare, lingering? She had dreamed of wolves again, of running through some dark pine forest with a great pack at her heels, hard on the scent of prey."

Game of Thrones returns for its fourth season on Sunday, April 6, with HBO and UK broadcaster Sky Atlantic simulcasting the first new episode.

Is simulcasting US TV shows the future for British broadcasters?

Watch a trailer for Game of Thrones season 4 below:

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io