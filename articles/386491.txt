Two women exchange rings during their wedding ceremony in the hallway of the Oakland County Courthouse in Pontiac, Michigan March 22, 2014. ― Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

NEW YORK, July 16 — US government data released yesterday showed that 2.3 per cent of American adults are either gay or bisexual and that these men and women more often reported serious anxiety and having self-destructive habits than their straight peers.

This year’s National Health Interview Survey was the first to ask about sexual orientation in addition to health habits in its 57-year history, the US Centers for Disease Control and Prevention said.

Bisexual women were twice as likely to experience serious anxiety while bisexual men were more likely to indulge in binge drinking than others, according to the survey.

In the survey of more than 34,500 Americans age 18 and above, 1.6 per cent reported that they were gay and 0.7 per cent reported that they were bisexual. A total of 96.6 per cent reported being straight and 1.1 per cent either said they were “something else,” did not know or declined to answer.

Roughly twice as many women than men identified as bisexual, with 0.9 per cent of female respondents saying they were attracted to both sexes. Bisexual women were twice as likely to report having serious anxiety than any other group, with almost 11 per cent saying they had been distressed in the past month.

Among bisexual men, almost 52 per cent said they had five drinks or more in a night during the past year compared with only 31 per cent of straight men.

Gays and bisexuals fared as well or better than their straight peers in some areas, like exercising, taking HIV tests and receiving flu vaccines.

“The differences went both ways, and it depends on the specific indicator that you were looking at,” said Brian Ward, a lead author on the survey and health statistician for the CDC’s statistical arm.

The CDC said the survey findings were in line with other research on health and sexual orientation, though a smaller proportion of people in this survey identified as bisexual than in other studies. — Reuters