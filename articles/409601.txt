Sierra Leone cyclist Moses Sesay was placed in quarantine after he was thought to have contracted Ebola and transported it to Britain.

Having been placed in isolation for 4 days and tested for the virus, the 32-year-old endurance cyclist has revealed the trauma he experienced when he was told he might have Ebola just one day after the Opening Ceremony in Glasgow.

Sesay said: “I was sick. I felt tired and listless. All the doctors were in special suits to treat me – they dressed like I had Ebola. I was very scared.”





After tests Sesay was told he had not contracted the virus, one which has killed 729 people in Africa and put the World Health Organisation on high alert.

“I was admitted for four days and they tested me for Ebola. It came back negative but they did it again and this time sent it to London, where it was also negative” Sesay said.

“After that, the doctors dressed normally again so I was relieved. I would like to thank the Scottish medical staff, they were very good.”

Ebola symptoms typically start two days to three weeks after contracting the virus, with a fever, throat and muscle pains, and headaches. Photo: Reuters

A Glasgow 2014 spokesperson issued a statement to reassure the public that there was no Ebola in the Athletes Village of the Glasgow 2014 Commonwealth Games.

“We can confirm an athlete was tested for a number of things when he fell ill last week, including Ebola” he said.

“The tests were negative and the athlete competed in his event on Thursday."

Sesay subsequently finished last in the men’s cycling time trial.





The CDC issued its first high-level travel warning in more than a decade

In Africa, The World Health Organisation raised the death toll by 57 to 729 on Friday, announcing that 122 new cases had been detected between Friday and Monday last week.

"The Ebola virus disease poses an extraordinary challenge to our nation," Sierra Leone's leader Ernest Bai Koroma said in a televised address to the nation.

"Consequently... I hereby proclaim a state of public emergency to enable us to take a more robust approach to deal with the Ebola outbreak."

Story continues

Koroma confirmed he had cancelled a trip to the summit of around 50 African leaders in Washington next week.

He announced a raft of measures as part of the state of emergency, including quarantining Ebola-hit areas and cancelling foreign trips by ministers.

Sierra Leone, which has seen 233 deaths, on Thursday buried medic Umar Khan, described by Koroma as a "national hero" who saved the lives of more than 100 Ebola patients before succumbing to the tropical bug.





How are the Centers for Disease Control taking precautions?

The UK announced a £2m package of assistance to the International Federation of the Red Cross (IFRC) and Médecins sans Frontières, which are tackling Ebola in Sierra Leone and Liberia. The European commission said it would allocate an additional €2m (£1.6m) – on top of €1.9m – to help contain the spread of the epidemic.





United States prepares for Ebola

In the United States, Emory University Hospital in Atlanta said on Friday that it was preparing a special isolation unit to receive a patient with Ebola disease “within the next several days”.

“We do not know at this time when the patient will arrive,” Emory said in a statement. The university also did not say whether the patient was one of two Americans battling Ebola infection in Liberia – charity workers Nancy Writebol and Dr. Kent Brantly.

“Emory University Hospital has a specially built isolation unit set up in collaboration with the CDC to treat patients who are exposed to certain serious infectious diseases,” the hospital said. “It is physically separate from other patient areas and has unique equipment and infrastructure that provide an extraordinarily high level of clinical isolation. It is one of only four such facilities in the country."

Experts say isolating patients with Ebola is important. The disease is not terribly contagious, but it can spread via bodily fluids and healthcare workers are at special risk. They must wear layers of protective equipment including boots, gloves, a full body suit and face and eye protection.



Daily News Bulletin



