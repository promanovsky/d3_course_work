By Rachael Rettner, Senior Writer

Published: 04/16/2014 07:04 PM EDT on LiveScience

At least one in 20 U.S. adults, or 12 million people yearly, may be misdiagnosed when they go to see their doctors, a new study suggests.

What's more, researchers estimated that about half of these diagnosis errors could lead to serious harm, such as when doctors fail to follow up on "red flags" for cancer in patients who are ultimately diagnosed with the condition.

The findings "should provide a foundation for policymakers, health care organizations and researchers to strengthen efforts to measure and reduce diagnostic errors," the researchers wrote in their study. [7 Medical Myths Even Doctors Believe]

Many previous studies on patient safety have focused on issues in hospitals, such as hospital-related infections and medication errors, the researchers said. Estimating the number of misdiagnoses in patients who are not admitted to the hospital has been difficult. In part, that's because these cases are challenging to detect since they can involve multiple visits to a doctor. Some studies have used malpractice claims, but these do not represent the population as a whole, the researchers said.

In the new study, the researchers used information from a sample of doctors' clinic visits (people who were not hospitalized), and reviewed hundreds of medical records to determine whether patients were misdiagnosed.

In one analysis, the researchers reviewed information from people who unexpectedly returned to the doctor soon after their initial visits. (An unexpected visit could indicate a suddenly occurring condition, or worsening chronic condition, that the doctor missed.)

The researchers also reviewed information from a sample of colon cancer and lung cancer patients, and looked for cases in which doctors did not follow up on "red flags" for these conditions, such as a positive colon cancer screening test, or an abnormal chest X-ray.

The researchers extrapolated the error rates from these results to the larger U.S. population, taking into consideration that about 80 percent of adults visit the doctor each year.

Overall, about 5 percent of adults who visit the doctor are misdiagnosed annually, according to the estimates.

The error rates for colon and lung cancer were much lower: An estimated 0.007 percent of adults have diagnostic errors related to colon cancer, and an estimated 0.013 percent of adults have errors related to lung cancer each year, the study finds.

Although these rates were small, "delayed cancer diagnosis is believed to be one of the most harmful and costly types of diagnostic error in the outpatient setting," the researchers said.

In a previous study, the same group of researchers estimated that about half of all diagnosis errors are harmful, meaning that misdiagnoses potentially harm 6 million U.S. adults each year, the researchers said.

The researchers noted that the study likely did not capture all types of diagnosis errors. For example, the medical records the researchers reviewed likely failed to document some errors. This means that the true rate of misdiagnoses could be higher, the researchers said.

The study, by researchers at the Houston Veterans Affairs Center for Innovations in Quality, Effectiveness and Safety, is published online today (April 16) in the journal BMJ Quality & Safety.

Follow Rachael Rettner @RachaelRettner. Follow Live Science @livescience, Facebook & Google+. Original article on Live Science.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>