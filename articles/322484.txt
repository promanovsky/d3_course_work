This month the Supreme Court handed Argentina a legal, psychological and financial defeat that continues to reverberate through the South American country — and the international financial community.

By leaving in place lower court rulings, the Supreme Court required Argentina to pay $1.3 billion before July 30 to holders of the country’s sovereign debt bonds. The action, combined with a related ruling, effectively meant that Argentina lost a 13-year legal battle and must now struggle to limit the damage.

AD

AD

The debate over the court action and Argentina’s response is unlikely to come to an end anytime soon. Analysts have different views on whether the court's action is going to affect the way developing countries restructure their debt, whether the relationship between Argentina and its bondholders is likely to deteriorate further, and whether the country is considering the right options as it tries to move forward.

Here is a quick summary of the court's recent action and three things to help you understand this long-lasting case — and why it matters to Argentina and to the rest of the world.

The Supreme Court issued two decisions this month. First, it refused to hear Argentina’s appeal of a lower court decision requiring it to pay holdouts who did not participate in debt restructurings in 2005 and 2010. Separately, the court issued a 7-to-1 decision allowing these bondholders to subpoena banks as they seek to trace Argentina’s assets domestically and abroad.

AD

AD

1) 'Hedges' drive the legal action — and the debate

The plaintiffs represent hedge funds and other investors, many of whom bought debt bonds years after Argentina's 2001 default at a steep discount. They did so with a long-shot hope of making a hefty profit over time. Investors who buy distressed debt at a discount are dubbed “vulture capitalists,” for trying to profit from the financial struggles of developing countries.

“The hedges are called hedges because they don’t negotiate,” Argentine Economy Minister Axel Kicillof said last week during a news conference in Buenos Aires.

He compared vulture fund practices with the Argentine movie "Carancho" (Vulture). In the film, an ill-intentioned lawyer makes money by encouraging accident survivors to sue for the sake of profit-making, getting away with the scam thanks to loopholes in the legal system.

AD

AD

Who are “hedges” in this case? The winning plaintiff group comprises many creditors, led by NML Capital, an investment fund controlled by billionaire Paul Singer, a major Republican donor. In a news release, NML urged the Argentine government to comply with Judge Thomas Griesa’s ruling, “benefiting the Argentine economy as well as their international position,” it said. Compliance would dramatically benefit the economic bottom line of the hedge funds themselves, of course.

While the hedges are derided as vultures by Argentina and many others, there are economists who argue that their profit-seeking performed a vital service, reestablishing the importance of contract rules in the loosely governed world of sovereign debt financing. In addition, the efforts by the hedge funds have raised the bar for default by Argentina and other nations. The effort to search bank and other records internationally may increase transparency around previously hidden government transactions. Of course, the hedges have the opportunity to make enormous profits: more than 1,600 percent, according to an estimate from the Argentine government, in profits accruing to NML alone if it were to be paid in full.

2) The global effect

AD

AD

Risk investment means that privately run companies are willing to lend money to stakeholders who will have a hard time paying back. That’s often the case for developing countries.

Argentina and its allies argued that a victory for the hedge funds in this case would unsettle international debt markets, making it difficult for struggling countries to restructure their debts while providing economic incentive for vulture investors to become even more aggressive in pursuing countries ill-prepared to pay.

“This case empowers these predatory hedge funds to continue a behavior that targets either countries in financial distress or the poorest countries,” said Eric LeCompte, executive director of Jubilee, a religious organization battling vulture investor tactics.

AD

He and others have argued that U.S. court rulings could have a spillover effect on the economic and social welfare of developing countries.

AD

But outside of Argentina, there was little early sign of the ruling having a negative impact, at least on foreign bond markets. The Wall Street Journal said the reaction revealed the “relative isolation” of Argentina compared with the rest of the global financial system. The Economist noted that Ecuador, which defaulted on $3.2 billion of debt as recently as 2008, smoothly returned to the bond market during the same week as the ruling against Argentina. A law professor from Georgetown University, Anna Gelpern, took issue with those predicting dark consequences for other struggling countries from the Argentine experience.

“Argentina is so different, so unique that nothing that happens here really affects anybody else, as a matter of fact,” she said when asked about the ruling. “Argentina might be unique, but that’s not how the law works. So the rules have changed, and the rules have changed in an important way. Sovereign debt was unenforceable before this, and sovereign debt is enforceable in a very damaging way after this.”

AD

“Most countries don’t have the money or the political will to do what Argentina did,” she said, responding to a question about whether other countries may be affected. “Argentina is extreme. Because Argentina has been, depending on when you start to count, nine to 13 years running away from its creditors. Peru settled within months.… So what this ruling does is it really puts the weakest players at the highest risk. The sovereign debt restructuring regime and the sovereign debt management regime are not particularly healthy. They need to be fixed, but they don’t need to be fixed in this way.”

AD

3) The Argentine effect

While the effect of these rulings on international markets and economies remains somewhat uncertain, there is no doubt that it will have a serious impact on Argentina.

AD

When the ruling was communicated by Griesa, the first response of Argentine President Cristina Fernández de Kirchner was to call it “extortion.”

With the 2015 presidential election in Argentina looming, Fernández de Kirchner is unlikely to admit defeat. Apart from the public statements declaring that Argentina will not submit to the hedge demands, several left-wing and pro-government youth sectors in Argentina have added a nationalist flavor to the discussion with protests and Che Guevara-inspired grafitti that say “Patria o Buitres” (“The Nation or the Hedges”).

There are a few scenarios on how this situation may be resolved. The first — and perhaps the most radical one — is to pay 100 percent of what is demanded by the ruling. If the $1.3 billion asked by holdouts is paid, chances are that the rest of the debtholders who reached an agreement with Argentina in 2005 and 2010 will request a similar payment, which could mean $15 billion, half of the Argentine national reserves, leading the country to default, the government said.

AD

AD

Some economists counter by saying that other creditors won’t demand a better deal and that, in any case, the government can afford to pay, as indicated by the recent financial agreement Argentina signed with the Paris Club.

The second possible scenario was mentioned in another recent presidential speech: Asking bondholders to operate under Argentine law instead of New York law. Changing the payment jurisdiction from the United States to Argentina would allow Argentina to reduce costs. However, this action would violate the orders of the court, Griesa said.

Another possibility is defaulting, instead of paying. According to a report published by the financial services company Standard & Poor's, choosing this path would damage the trust of the creditors who did renegotiate in the past and the world’s banks wouldn’t rely on Argentina, discouraging foreign investment.

AD

“If Argentina defaults, the country would be completely excluded from the financial markets. There would be devaluation, inflation, unemployment, and economic activity would go down,” said Marcos Leonetti, economist and blogger with La Economia Online. Both the Argentine government and the holdouts expressed their intention to avoid default.

A fourth option is that the Argentine government negotiates smaller payments to NML and other holdouts. This is considered the most likely outcome at the moment.