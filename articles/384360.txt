A giant hole has opened up in Siberia, and nobody quite knows why.

The massive hole, which spans about 262 feet in diameter, was spotted recently on the Yamal Peninsula in Russia, commonly known as the "end of the world," The Siberian Times reported. The depth of the hole is not yet known.

New video footage shows the mysterious hole surrounded by vegetation in a seemingly remote area. The ground on the outskirts of the hole appears to have been displaced, perhaps by whatever caused the hole to form.

Given the crater's proximity to a natural gas field, one theory is that the hole formed following an explosion, caused by a mixture of gas, salt and water igniting underground.

So far, local officials have ruled out a space rock as the culprit behind the hole.

"We can definitely say that it is not a meteorite," a spokesman for the Yamal branch of the country's Emergencies Ministry told The Siberian Times, adding that there are no further details yet.

Dr. Chris Fogwill, a polar scientist at the University of New South Wales in Australia, said the hole may be what's left of a pingo, a heap of Earth-covered ice that is found in the Arctic and subarctic. If the pingo was large enough, and melted, it potentially could have created a giant hole.

"Certainly from the images I've seen it looks like a periglacial feature, perhaps a collapsed pingo," Fogwill told The Sydney Morning Herald. "This is obviously a very extreme version of that, and if there’s been any interaction with the gas in the area, that is a question that could only be answered by going there."

In fact, a team of scientists has been dispatched to study the Siberian hole and take samples from the surrounding region. They are reportedly due to arrive Wednesday. Until then, the mystery continues.