Monday night is a great night to watch the annual Lyrid Meteor shower, which will peak between April 21 and 22—just in time for Earth Day.

The annual April shower— named after the constellation its nearest, Lyra— will peak at around 20 meteors per minute according to NASA, giving viewers watching either on NASA’s live stream or out in the wilderness a glimpse of the falling meteors streaming, glowing tails.

NASA recommends outdoor viewers settle in dark, clear-skied locations far away from city lights. The best viewing will be between midnight and dawn local time, which gives you plenty of time to head someplace dark. If you can’t make it outdoors (or you’re stuck on a well-lit city block) check out NASA’s live stream, which will air starting at 8:30 pm ET. Either way, don’t forget to look up!

Here’s a peak at the 2012 shower, so you can see what’s in store:

Get our Space Newsletter. Sign up to receive the week's news in space. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.