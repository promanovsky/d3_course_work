The patient with the first confirmed case of Middle Eastern Respiratory Syndrome in the U.S. will most likely be discharged from Community Hospital in Munster, Indiana, soon, according to the CDC and Community Hospital officials, who spoke at a press conference today.

Hospital officials say that they are still working out details, but once released, the patient is expected to be isolated at home until the CDC decides it's safe for him to travel again.

Advertisement

His family members and the approximately 50 hospital employees who were exposed to him before protective measures were put into place have been put in home isolation, with instructions to wear a mask if they absolutely must go out.

Currently he is recovering and in good spirits, according to Dr. Alan Kumar, the chief medical information officer at the hospital.Kumar says that the patient doesn't seem to have passed the disease on to anyone else.

Although most people infected with MERS show symptoms in five days, the virus can take as many as 14 days to show up.

Advertisement

How MERS got here

Anyone exposed will be tested again at the end of that 14 day window.

The Indiana patient showed up at Munster Community Hospital's emergency room on April 28th. Fortunately, no other patients were exposed to him before the infectious disease unit at the hospital reviewed his travel history and saw that he'd recently arrived from Riyadh, Saudi Arabia, where he is a healthcare worker.

Next steps

Advertisement

The patient was not aware of any contact with anyone with MERS, though the Saudi hospital he works at did have some patients with the disease.After he was infected, the patient flew from Riyadh through London Heathrow to Chicago. The CDC is using the airline manifest to try and get in touch with the approximately 100 people that were on the flight that brought the patient from London to Chicago, according to Dr. Daniel Feikin, a CDC epidemiologist. So far they've been able to contact about 75 of them. Public health authorities in the UK are taking similar measures with patients that were on the flight from Riyadh.

CDC officials have also been working with the state of Indiana to contact the approximately 10 bus passengers who rode from Chicago to Indiana with the patient.

Since the virus has only jumped from one person to another in cases of extremely close contact, officials think it's extremely unlikely that anyone who was on the flight or the bus could have been infected by the patient.

Advertisement

No reason for panic

No one the patient came into contact with has shown any symptoms.

While experts are not yet sure where the virus originates, the most likely source is contact with camels.

Beyond that, officials have only seen transmission to a person when someone - usually a family member or a healthcare worker - has very close with an infected patient, "caring for them, touching them, [or] coming into contact with their respiratory droplets," according to Feikin.

Advertisement

MERS is caused by a coronavirus, like SARS. While MERS is not as contagious as SARS, healthcare authorities are being especially cautious since the 2003 SARS outbreak in the Toronto healthcare system infected 257 and killed 33.Hospital officials in Indiana have been meticulous about the precautions they've taken to prevent the virus from spreading, going so far as to expel the air from the infected patient's hospital room through a filter, even though they don't think the virus can spread through the air.

Saudi Arabia, the hub of the outbreak, just reported 25 new cases of MERS, bringing the total in that country to 396. So far, about 30% of those infected have died. Most of those people, Feikin said, have been elderly or suffering from underlying health conditions. Still, the recent spike in cases is alarming; it may mean that officials are getting better at spotting it, or that the virus has become more contagious.

Advertisement

Answering a question about whether or not we could expect more cases in the U.S., Feikin said that even though we can't predict what we'll see, it is certainly possible that if the virus continues to spread, MERS could show up on U.S. soil again.