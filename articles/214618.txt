Jupiter's Great Red Spot is a massive feature that has been observed for close to 200 years. Recently, the raging storm has been shrinking at an increased rate and new Hubble Space Telescope photos catch the Great Red Spot at its smallest size ever.

The diameter of Jupiter's Great Red Spot, at its widest, was measured at 41,000 kilometers (25,476 miles) in the late 1800s, Hubble officials said in a statement. Earth's diameter, by comparison, is 12,742 kilometers (7,918 miles). More recent observations, from the 1970s and the 1980s, via NASA's Voyager spacecraft, put the Great Red Spot's diameter at 23,335 kilometers (around 14,500 miles). The new Hubble observations were taken on April 21, using the space telescope's Wide Field Camera 3.

Photo: NASA, ESA and A. Simon (Goddard Space Flight Center)

Much like the mystery surrounding the Great Red Spot's staying power, astronomers aren't sure what is causing the giant storm on Jupiter to shrink. The Great Red Spot is an anticyclone, spinning counter clockwise, in the southern hemisphere of Jupiter, with wind speeds of 430 to 630 kilometers per hour (270 to 425 miles per hour), NASA said. The origin of the storm's signature red color is also unknown.

Hubble observed the Great Red Spot of Jupiter in 1995, 2009 and 2014, and noticed a dramatic change in its diameter. NASA said that the diameter of the storm was 13,020 miles in 1995, 11,130 miles wide in 2009 and currently 10,250 miles wide. Previous studies estimate the rate of shrinkage at 580 miles per year. The astronomers also noted that the shape of the Great Red Spot has changed, going from an oval to a circle.

Photo: NASA, ESA, and A. Simon (Goddard Space Flight Center)

Amy Simon, from NASA's Goddard Space Flight Center, said eddies, or circular currents of gas, may be changing the composition of the Great Red Spot, which could be diminishing in its power. "In our new observations it is apparent that very small eddies are feeding into the storm," Simon said in a statement. "We hypothesized that these may be responsible for the accelerated change by altering the internal dynamics and energy of the Great Red Spot."