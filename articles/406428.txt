Exxon Mobil Corp., the world’s largest energy company, fell after reporting worldwide oil and natural gas production declined to the lowest level in almost five years. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

CHICAGO, July 31 — Exxon Mobil Corp., the world’s largest energy company, fell after reporting worldwide oil and natural gas production declined to the lowest level in almost five years.

Exxon declined 1.5 per cent to US$101.65 (RM324.27) at 8:22am in New York, before the start of regular trading on U.S. markets. Oil and gas output dropped 5.7 per cent to the equivalent of 3.84 million barrels of crude a day, the lowest since the third quarter of 2009, according to data compiled by Bloomberg. Exxon had been expected to post daily output equivalent to 3.96 million barrels, based on the average of six analysts’ estimates.

The company is allocating US$39.8 billion to capital projects this year, including hundreds of millions for an exploratory well in Russia’s Kara Sea, as part of a 29-year agreement signed with Moscow-based OAO Rosneft in 2011. Exxon is going after crude in Russia’s Arctic regions in an effort to extract some of the largest crude reserves and reverse a trend of declining production for the company.

Sanctions threaten to halt that progress after the US and European Union said July 29 they would restrict the export of technologies for energy production to Russia.

Excluding one-time items, per-share profit for the second quarter was 19 cents more than the US$1.86 average of 21 estimates compiled by Bloomberg. Net income climbed to US$8.78 billion, or US$2.05 a share, from US$6.86 billion, or US$1.55, a year earlier, the Irving, Texas-based company said in a statement today.

The company, which has a market value of US$443 billion, has 14 buy ratings from analysts, 13 holds and five sells. — Bloomberg