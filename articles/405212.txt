A flesh-eating bacterium has been found in warm Florida waters and has already infected 11 individuals.

Vibrio vulnificus is related to the bacterium that causes cholera; it lives in warm saltwater, ABC news reported. If ingested the bacteria can cause symptoms such as vomiting and diarrhea, but if it infects an open wound the effects can be more gruesome. This type of skin infection is characterized by "skin breakdown and ulcerations," according to the Centers for Disease Control and Prevention (CDC), ABC News reported. The infection can also be acquired from eating raw shellfish.

"Vibrio vulnificus is a rare cause of disease, but it is also underreported. Between 1988 and 2006, the Centers for Disease Control and Prevention (CDC) received reports of more than 900 Vibrio vulnificus infections from the Gulf Coast states, where most cases occur," the Florida Department of Health said in a statement.

"It's rare, but we wanted to get the warning out there to make sure people are educated about it," Department of Health Deputy Press Secretary Pamela Crane told Broward/Palm Beach New Times.

The disease can be fatal if it makes its way into the bloodstream. About 50 percent of people who get this type of infection die, New Times reported. People with weakened immune systems are especially vulnerable, and are 80 percent more likely to develop the disease than healthy people.

The bacterium is not isolated to Florida; "Alabama, Louisiana, Texas and Mississippi" have also reported cases and in 2013 an outbreak caused 104 people across 13 states to fall ill, ABC reported.

Most people who contract the disease are treated with antibiotics, but skin infections could require surgery or even amputations. To avoid the bacteria individuals should avoid exposing open wounds to warm saltwater or shellfish, wear protective clothing when handling shellfish, and cook shellfish thoroughly before consuming.

WATCH:



ABC News | More ABC News Videos

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.