Apple is reportedly in talks to acquire Beats, the headphone and streaming music service company, for $3.2 billion.

Beats is the brainchild of rapper Dr. Dre and record producer Jimmy Iovine.

Advertisement

Music Catalog

But some people are wondering why Apple would want to buy the company. That's because the headphones Beats makes aren't very good , and its streaming music service, which launched earlier this year, is widely considered to be sub-par to Spotify.Let's take a closer look at the streaming services and see how they compare.

Both Spotify and Beats Music have libraries featuring over 20 million songs. In five years, only 20% of Spotify's library has been listened to by anyone.

Tie

Advertisement

Music Discovery

Spotify and Beats Music take two very different approaches to helping you discover new music. Once you create an account with Beats, you have to select what kind of music you like. Beats first shows you a selection of genres and instructs you to tap on the genres you like, and press and hold the ones you don't.

t requires a bit of work to set up, but at least it gives you a very personalized experience.

Advertisement

After you select the genres you like and dislike, you're instructed to do the same with artists. Tap on three artists you like, and press and hold the ones you hate. I

Beats also has a cool feature called "The Sentence," which helps you find the right music for the right situation. One morning at work, I was listening to the "I'm at work & feel like pre-partying with myself to electronic" sentence. It made the day fly right on by.

Though, with Spotify's recent acquisition of The Echo Nest, we expect its music discovery tools to get better over time. The Echo Nest uses machine learning technology to leverage massive amounts of data about music. The Echo Nest can analyze who you are as a music fan and then deliver song recommendations to you and even predict what type of music you're going to listen to.

Advertisement

Back in April, Spotify made it easier than ever to find songs you want to listen to . Spotify will also do all of the work for you by suggesting entire playlists for you to listen to depending on the day of the week and/or your mood.

Winner: Beats Music

Price

Both services cost $9.99 per month, and both offer discounts through wireless carriers.

Spotify gives Sprint subscribers discounted access to the its streaming music service, and Beats Music does the same for AT&T subscribers.

Advertisement

But with Spotify, you'll never have to pay if you don't want to. Back in January, Spotify unveiled free, ad-supported unlimited music streaming

Winner: Spotify

Advertisement

Design and Functionality

Both Spotify and Beats Music offer apps for desktop and mobile, but Beats Music ultimately fails on desktop. For example, you can't even access your library and you can't use one of the best features, The Sentence. For some reason, you can only do that on Beats Music's mobile apps.

Advertisement

Overall Winner: Spotify

Last month, Spotify unveiled its most dramatic redesign since launching in 2008. Most notable in the design are the changes in color scheme. Instead of gray and white, Spotify is now mostly black.Spotify

Even though there are a fair amount of similarities, Spotify is ultimately the best option for easy access to your favorite songs, no matter where you are and which device you're on.