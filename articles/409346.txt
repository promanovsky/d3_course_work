A new gene linked to suicide risk has been discovered, and researchers say the finding could lead to a blood test that predicts a person's risk of attempting suicide.

In the study, researchers scanned the genes of brain tissue samples from people who had died by suicide, and compared these genes with those of people who died of other causes. The scientists found that a genetic mutation, in a gene called SKA2, was more common among the people who died by suicide. The researchers also found a chemical change, called an epigenetic change, on that same gene that was more common among people who committed suicide than in those who died from other causes.

"We have found a gene that we think could be really important for consistently identifying a range of behaviors" having to do with suicide, said study researcher Zachary Kaminsky, an assistant professor of psychiatry and behavioral sciences at the Johns Hopkins University School of Medicine.

Next, the researchers examined whether these genetic changes could predict a person's risk of having suicidal thoughts or attempting suicide. Using blood samples from 325 people, the scientists created a model that took into account whether a person had the SKA2 genetic mutation and the epigenetic change, as well as the person's age, sex, and stress and anxiety levels. The researchers tested this model on 22 people ages 15 to 24, and about 50 pregnant women, who all gave blood samples. Scientists then followed up with these individuals to see whether they had experienced suicidal thoughts or attempted suicide. [5 Myths About Suicide, Debunked]

The model correctly identified 80 percent to 96 percent of people who experienced suicidal thoughts or attempted suicide. It was more accurate among people at severe risk for suicide.

The researchers said they suspect that the genetic changes in SKA2 may be involved in shutting down the body's response to stress. Kaminsky likened the genetic changes to faulty brakes on a car: Without stress, it's like having a parked car with bad brakes, but once stress occurs, having brakes that work is important, or else the car can get out of control.

Because the study was small, the results are preliminary and more research is needed to confirm the findings, the researchers said.

If the findings are confirmed and lead to a blood test for suicide risk, such a test might be used to screen people in psychiatric emergency rooms or to determine how closely a person needs to be monitored for suicide risk, the researcher said.

The study was published online Tuesday (July 29) in the American Journal of Psychiatry. Kaminsky and one of his colleagues hold a patent on evaluating people's risk of suicidal behavior using genetic changes in SKA2.

Last year, another group of researchers found certain markers in the blood were linked to suicidal thoughts in people with bipolar disorder.