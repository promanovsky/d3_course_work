It seems everyone's getting an assistant these days - but not the type who could make you a cup of coffee at least.

Previously, it was Apple's personal digital assistant and knowledge navigator Siri that made headlines. Now, Google has its own predictive digital assistant called Google Now for desktops and laptops when using Chrome.

Google announced that it's ready to roll out its digital assistant to desktop or laptop versions of Chrome, as its supplement to mobile versions, starting on Monday and hopefully be completed in the next few weeks. This makes it easier for users to also see the contextual notifications from their Windows and Mac OS X desktop or laptop.

"If you use Google Now on your mobile device, you can see certain Now cards on your desktop computer if you're signed into Chrome, including weather, sports scores, commute traffic, and event reminders cards," Google says in a blog post.

"Some of these cards may be based on the location of your mobile device," Google added, reminding that the Location Reporting and Location History should be turned on.

Google Now initially debuted to desktop users in February 2014, though limited only to Chrome's beta version and available only in English. Monday's update makes the notifications available in all languages.

Nothing has been noted on interface design changes with the current rollout, which only seems to show that Google is perhaps happy with it.

It works this way, though: when you check your emails before the day ends, you might get a traffic alert that would advise you to leave work earlier than usual so as to beat the traffic jam. Thanks to the real-time traffic alerts, you can also now learn about accidents and other possible reasons for traffic jams.

Sports buff can also search for the score of any basketball game, as the Google Now can recognize and search it on your behalf.

You can also turn off Now cards in Chrome at any time by clicking the bell icon on your computer screen to open the Notifications Center. Click the gear icon in the Notifications center, and then you can uncheck the box next to Google Now. The locations of the said icons depend on the type of your computer, if you're using Windows or Mac.

Enabling Now on your desktop is as easy as signing into Chrome with the same Google account that you use on iOS or Android device that has an installed Google Search app.

Other settings have been provided in the detailed blog post.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.