India's manufacturing activity continued to rise in May, results of a survey by Markit Economics and HSBC Bank showed Monday.

The HSBC manufacturing purchasing managers' index rose to 51.4 in May from 51.3 in April. Nevetheless, the index remained below the series average.

Manufacturing output expanded for the seventh straight month and at the same rate in May as in April. New orders rose despite the negative effects of powercuts and elections.

Incoming new work expanded for the seventh consecutive month. New orders from abroad increased for the eighth straight month at a quicker rate than in April. The survey report suggested that the rise in new export was fueled by favourable exchange rates.

Employment grew at a steady rate in May, expanding for the eighth consecutive month.

Purchasing activity increased at a moderate rate in May, reflecting an increase in new orders.

Input costs rose at the weakest rate in twelve months, while output prices rose at a modest rate, but was weaker than the series average.

Frederic Neumann, Co-Head of Asian Economic Research at HSBC, commented that, though input price pressures have eased, the RBI cannot take down its inflation guards because of the continued rise in output prices.

For comments and feedback contact: editorial@rttnews.com

Business News