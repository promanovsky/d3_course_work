Marijuana use may lead to an increased risk of a person suffering from sleep problems, say U.S. researchers studying cannabis' impact on public health.

Scientists at the University of Pennsylvania in Philadelphia say adults who began their marijuana use before the age of 15 show a strong association with problems including impaired nighttime sleep quality and daytime sleepiness.

In the study, researchers looked at data collected from 1,811 adults reporting using cannabis as part of the 2007-2008 National Health and Nutrition Examination Survey.

Information was collected about sleeping patterns, the age at which people first used cannabis and how often they used marijuana in a typical month.

The findings suggest both past and current cannabis users are much more at risk of suffering sleep problems than those who've never used marijuana, the researchers said.

"The most surprising finding," says lead author Jilesh Chheda, "was that there was a strong relationship with age of first use, no matter how often people were currently using marijuana. People who started using early were more likely to have sleep problems as an adult."

Those problems could include difficulty in falling asleep, staying asleep, finding the sleep period nonrestorative, and experiencing sleepiness during waking hours, the researchers said.

Sleep problems are generally classified as severe when they occur on at least half of nights in any given month, they said.

"Marijuana use is common, with about half of adults having reported using it at some point in their life," said Chheda.

"As it becomes legal in many states (in the U.S.), it will be important to understand the impact of marijuana use on public health, as its impact on sleep in the 'real world' is not well known," he said.

The study should be taken only as demonstrating a link between marijuana use and sleep problems and does not serve as a determination of causality, the researchers emphasized.

Still, they said, the results strongly suggest beginning marijuana use during adolescence may lead to a higher chance of subsequent insomnia problems.

Ironically, insomnia might even be a reason people start using or stay with marijuana, the researchers noted, although the study suggests it is ineffective if they're continuing to experience sleep problems.

They will present the study's full results on June 4 in Minneapolis, Minn., at Sleep 2014, the 28th annual meeting of the Associated Professional Sleep Societies.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.