Google is known for offering hardware direct to customers at lower-than-most prices, and the company may take it one step further in offering a truly budget Nexus phone.

That's the word coming from a Chinese publication that reports on "everything MediaTek," but more on that connection in a minute.

The main hook here is that Google's next Nexus handset is rumored to cost as low as $100 (about £59, AU$107). The phone would likely fall in the lower mid-range spectrum, and likely wouldn't replace the Nexus 5.

A $100 price tag would be significantly cheaper than previous Nexuses; the Nexus 4 sold for $300 (£239, about AU$321) at launch, and the Nexus 5 followed with a slightly higher $349/$399 (£299/£339, about AU$374/AU$427) fee.

Ticking with Tek

The only other detail to go on with this rumored budget Nexus is it could run a MediaTek processor.

HTC partnered with MediaTek to offer the entry-level Desire 310, and the chip maker is China's leader in smartphone silicon. The country is gobbling up budget phones by the barrel, and Google could be taking a cue from Apple and others in targeting the region with low-cost hardware.

We'll keep an eye out for more on this supposed cheaper Nexus, so stay tuned for more.

Via G for Games