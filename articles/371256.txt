The South Korean electronics giant said Tuesday its operating income was $7.1 billion for the three months ended June 30, down 24 percent from a year earlier. That was significantly below analysts’ expectations. The company releases its full quarterly financial results later in the month.

The result highlights how dependent the company has become on smartphones for its earnings. Sales growth in high-end Android devices has waned after several years of rapid expansion. Growth remains robust in emerging markets where cheap competitors have sprung up.

SEOUL — Samsung Electronics Co. said operating profit declined to a two-year low in the second quarter, hit by the strong local currency and slowing demand for smartphones in China.

Advertisement

The figure was the lowest since the second quarter of 2012. Quarterly sales fell 10 percent from the previous year.

Samsung, which usually doesn’t elaborate on its financials until its full report later in the month, issued a rare statement to explain its result, which Nomura analyst Chung Chang-won described as ‘‘very disappointing.’’

The company blamed the lower profit on the South Korean currency’s appreciation against the US dollar and the euro, as well as most emerging market currencies. The won hit a 6-year high against the US dollar earlier this month.

It also said sales of medium- and low-end smartphones were weak in China and in some European countries because of stronger competition and sluggish demand. Fewer consumers in China bought handsets that run on 3G mobile networks as they waited for faster 4G networks.

Samsung, which earlier this year vowed to aggressively expand its tablet sales, acknowledged it faced some challenges in selling such computers. Consumers weren’t replacing their tablet PCs as often as smartphones, while smartphones with a giant screen of 5 or 6 inches, such as Samsung’s Galaxy Note series, replaced demand for smaller tablets that measure about 7 or 8 inches.

Advertisement

‘‘Expectations have been lowered on Samsung,’’ said Will Cho, an analyst at KDB Daewoo Securities. ‘‘With intensified competition in the mid- and low-end smartphones, it will be tough to stay as lucrative as in the past.’’

About three in every 10 smartphones sold globally were made by Samsung in 2013, and the company’s handset sales will likely improve during the current quarter. But Cho said how much profit it can take would be more important than how many handsets it can sell because most sales growth would come from cheap smartphones.

Some analysts expressed concerns about Samsung’s prospects.

‘‘Though we anticipate some positive earnings impact for the component businesses (in the third quarter), we see growing uncertainty over Samsung’s future earnings in the long-term,’’ Chung, the Nomura Financial Investment analyst, said in a commentary.

Chung cited Apple’s upcoming release of the iPhone 6, the increasing difficulty in standing out from a plethora of other Android devices, and the falling appeal of premium smartphones, a reminder of the PC market that came to be flooded with cheap products.

Samsung is expected to announce an upgrade of the Galaxy Note series in the fall around the time when Apple usually upgrades its iPhone.