South African writer and Nobel Peace Prize winner Nadine Gordimer has died at age 90. Gordimer’s literary work about apartheid has touched the lives of many and her death has been met with great sadness.

According to the New York Times, the novelist who based her themes on the racial injustices within the country during the apartheid era died on Sunday in Johannesburg, South Africa.

Here are a few facts that you may not have known about her.

1. Ms. Gordimer was the author of more than two dozen works of fiction

Gordimer’s works included short stories, political essays and novels. She continued to write for over seven decades. Her first book of stories was Face to Face in 1949, and her first novel was The Lying Days, in 1953.

2. She was not a political person, but could not ignore the injustices surrounding her

Gordimer explored every aspect of South Africa in her work and was not afraid to expose the injustices that existed within the apartheid era. Her work vividly outlined the restrictions on black people by the white minority.

“I am not a political person by nature,” Ms. Gordimer once said. “I don’t suppose if I had lived elsewhere, my writing would have reflected politics much, if at all.”

3. She wanted to be a ballerina

As a child Gordimer had dreams of becoming a ballerina, but her love for dance was squashed by her mother’s worries that something would happen to her because she was thought to have had a rapid heartbeat.

4. Three of her books were banned in South Africa during apartheid

The government wanted to censor what was going on in the country at the time, and Gordimer’s work threatened that. Her novel A World of Strangers, published in 1958, was banned for 12 years. Another novel, The Late Bourgeois World, published in 1966, was banned for 10 years and Burger’s Daughter, published in 1979, was banned for several months.

5. She was never detained or persecuted for her work

Although Gordimer’s works conflicted with the South African apartheid government, there was never any action taken against her for her stories. Although, as mentioned above, some of her work was censored.

6. She was born to Jewish immigrant parents

Gordimer was born to Jewish immigrant parents on Nov. 20, 1923. The A World of Strangers author’s father was a watchmaker and her mother was from Britain. Her parents did not have a happy marriage, as Gordimer revealed in an interview with The Paris Review in 1983.

“I suspect she was sometimes in love with other men,” she confessed. “But my mother would never have dreamed of having an affair.”

7. She has an older sister, Betty

8. Her work exposed the racial divisions within the country

It was thanks to Gordimer’s books that international readers learned of the apartheid government’s regressive legislation that discriminated between races.

9. It took her three decades before she ventured out of South Africa

According to the New York Times, aside from a trip to what is now known as Zimbabwe, Gordimer did not leave South Africa until she was 30 years old.

10. She married a dentist, and then an art dealer

In 1949 Gordimer married a dentist named Gerald Gavron, and while their marriage only lasted three years, they had a daughter together, Oriane. Gordimer then married art dealer Reinhold H. Cassirer two years later. They had a son Hugo together. Gordimer is survived by her son and daughter.