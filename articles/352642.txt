Bob Hastings, a radio actor who found his footing in television, notably for portraying “yes man” Lt. Elroy Carpenter on the popular 1960s sitcom “McHale’s Navy,” died Monday of prostate cancer at his Burbank home, his family said. He was 89.

Hastings began his career in radio in 1936 at age 11. He was featured in “Coast-to-Coast on a Bus,” “The National Barn Dance,” “The Sea Hound” and other serial programs. After serving in the Army Air Forces as a B-29 navigator during World War II, he returned to radio work. Until 1953 he starred in the title role of “Archie Andrews,” the show based on Bob Montana’s popular comic strip featuring fellow Riverdale teenagers Jughead, Veronica and Betty.

In the late 1940s Hastings began working in television, starting with “Captain Video and His Video Rangers.” He appeared several times on “The Phil Silvers Show” in the late 1950s and continued acting on television for decades. On “McHale’s Navy,” which aired on ABC from 1962 to 1966, he was part of the supporting cast for Lt. Commander Quinton McHale, played by Ernest Borgnine.

A successful voice actor, Hastings also provided the voice of the Raven on “The Munsters,” Commissioner Gordon on various “Batman” shows and Superboy in 1970s TV cartoons.

Advertisement

Born April 18, 1925, in Brooklyn, N.Y., Robert Francis Hastings was the third son of Hazel Mae Kirk and Charles Benedict Hastings Sr. He met his future wife, Joan, as a teenager, and they married in 1948.

Besides his wife of 66 years, Hastings is survived by daughters Tricia Stone and Mary Joan Shaughnessy; sons Bob Jr. and Michael, who is a former Burbank mayor; 10 grandchildren, 11 great-grandchildren and his brother Don, who played Dr. Bob Hughes on the soap opera “As The World Turns.”

alicia.banks@latimes.com

Twitter: @AliciaDotBanks