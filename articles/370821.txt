Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The Swiss helicopter firm on stand-by to airlift F1 legend Michael Schumacher to his home after his life and death coma battle called police today over claims his medical files had been stolen.

Rega air service was set to chopper the seven-times world champion back to his home in Lausanne from the hospital in Grenoble in France the ski accident in December which left him fighting for his life.

But now the helicopter bosses themselves want police to investigate claims his medical files had been put up for sale.

The medical records, said to consist of a few pages written by his doctors in the French hospital where 45-year-old Schumacher had been in a coma since the accident, were allegedly being sold across Europe for €50,000 - around £40,000.

Police initially carried out searches of computers at the University of Grenbole Hospital where Schumacher had been airlifted to.

Now prosecutors in Grenoble are probing a sender’s IP address following the hospital computer search,according to reports in France and Switzerland.

A helicopter was on stand-by to fly Schumacher back to his home in Lausanne three weeks ago, as his manager Sabine Kehm, said his medical files had been “clearly stolen” and were being offered for sale.

Schumacher was eventually driven back to a rehab centre near his Swiss mansion by road.

But Zurich-based Rega said it had filed a complaint with Zurich prosecutors in the wake of reports focusing on its role in handling the German sports star's files.

Rega said an investigation would provide "comprehensive clarity".

The air service has confirmed it was given the files by a French hospital when it arranged a transfer for the Formula One world champion.

But in the end he was moved by road to a rehab clinic near his home last month to continue his recovery from a December 29 skiing accident.

Rega said it had no reason to believe any of its employees did anything wrong.