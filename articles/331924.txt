YouTube on Thursday has unveiled a sneak preview of new features and updates that the team is planning to release in the coming months. The company has also released a YouTube Creator Studio app for Android devices to manage channel content on the go. The iOS version of the app will be launched in "in coming weeks", while a redesign of the Creator Studio will come to the desktop too.

The YouTube Creator Studio app for Android 4.0 or higher devices can be downloaded via Google Play. Along with the content management on the go, the app enables users to check out latest stats, respond to comments, and get customized push notifications as well.

In the upcoming feature announcement on the blog, YouTube's creators team teased the upcoming 48 frames and 60 frames per second support for its videos.

YouTube is also planning to introduce new features for video channel creators to engage subscribers, in various activities such as Fan Funding and Subtitle Contribution from fans. In order for channel owners to get funding from fans, YouTube will add an option that will allow viewers to contribute money soon.

"We'll be adding another option for you, where fans will be able to contribute money to support your channel at any time, for any reason, "said YT creators team on its blog.

At present, YouTube is testing the Fan Funding feature on desktop and Android with selected channels only. Other channel creators can apply to test this feature with a signing up on the website. Another in-testing feature to be rolled out soon is where fans can also contribute to subtitles from different locations.

Not just that, YouTube has more additions for channel creators, like the new Info Cards, which could be programed to work across desktop, phones and tablets. Creators will be able to use added analytics along with playlists analytics. YouTube has also updated its Audio Library with thousands of royalty-free sound effects for channel content creators. Other than that, the company has also partnered with SiriusXM satellite radio to promote artists.

"We love supporting artists, and so do our friends at SiriusXM. That's why we teamed up to launch "The YouTube 15," a weekly show on SiriusXM's Hits 1 hosted by Jenna Marbles and featuring the biggest names and rising stars in music from YouTube," noted on blog.