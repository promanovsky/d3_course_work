'Going on set was magical, tactile and real': Kevin Smith reveals how Star Wars Episode VII studio visit reduced him to tears



He is a famous Star Wars fan, having even incorporated scenes about the franchise into his cult Clerks movies.



So understandably, it was an overwhelming experience for Kevin Smith to visit the new J.J. Abrams Star Wars set in London recently



The 43-year-old comic and director was left in tears after spending the day at UK's Pinewood Studios posting a picture of his emotional face a week ago.



Teary: Kevin Smith posted a picture of his tear-streaked emotional face shortly after visiting the Star Wars set in London a week ago

'Visited JJ and his EP VII set,' he wrote alongside the snap. 'I signed the NDA so all I can share are this old Bantha-Tracks subscriber's tears and snotty nose of joy. The Force is WITH this movie. Holy Sith...'

Despite the non disclosure agreement, Kevin, who described the films as his 'f***ing religion' when he was younger, went into a little more detail during a Q&A at the Neuchatel International Fantastic Film Festival in Switzerland this weekend.



'What I saw, I absolutely loved. It was tactile - it was real,' he told the audience, as reported by Us Weekly. 'It wasn't a series of f***ing green screens and blue screens in which later on digital characters would be added. It was there, it was happening.

Fans: Kevin Smith, pictured at a Star Wars convention back in 2012, and J.J. Abrams, who is directing the new movie



'I saw old friends that I haven't seen since my childhood, who aren't really friends, but I love them more than some of my f***ing relatives. I saw uniforms, I saw artillery that I haven't seen since I was a kid.

'I saw them shooting an actual sequence in a set that is real. I walked across the set, there were explosions. And it looked like a shot right out of a f***ing Star Wars movie. I watched them do it four times, standing next to J.J. I was like, "Man, good for you. You're doing it!"'



'I was a little kid who f***ing loved Star Wars who wondered if anyone in the world felt and thought the way I did,' he told the audience.



Episode VII: Back in April the old and new cast members were announced including Harrison Ford, Daisy Ridley, Carrie Fisher, Peter Mayhew, Domhnall Gleeson, Anthony Daniels, Mark Hamill, Andy Serkis, Oscar Isaac, John Boyega and Adam Driver

The film is the as-yet-untitled seventh instalment of the George Lucas created franchise and will see Harrison Ford, 71, Mark Hamill, 62, and Carrie Fisher, 57, reprise their original roles of Han Solo, Luke Skywalker and Princess Leia respectively.

Invited on set by acquaintance Abrams via email 'out of the blue' during a visit to the English capital, Kevin said: ' When I went to the set, something happened - [it was] f***ing magical.'

And he was unable to contain his emotions when Abram's assistant showed him round a full scale Millennium Falcon - Han Solo's iconic spaceship from all three original movies.



'My foot went on the landing ramp, and 10 years dropped off my life,' he recalled. 'I take another step, I'm a f***ing 18-year-old again. I take another step, 12. I take another step, I'm f***ing nine-years old when Empire Strikes Back comes out.

Use the force: Mark Hamill, Carrie Fisher and Harrison Ford will reprise their roles as Luke Skywalker, Princess Leia and Han Solo in the new film



'And then when I get to my f***ing last step, man, I'm seven-years old, standing on the ramp of the Millennium Falcon. And all the s*** I thought about myself and know about myself to be true... was f***ing gone. And I just started crying. I was connected to my childhood in such a primal way.'

Smith's words will be reassuring to many fans of the original three Star Wars movies in the wake of what many see as disappointing prequel films The Phantom Menace, Attack Of The Clones and Revenge Of The Sith, which were criticised for wooden acting and over-reliance on CGI effects.

'He's building a tactile world, a world you can touch,' said Smith about Abrams. 'And he's replicating it with all the love of somebody that has the world's greatest collection of Star Wars figures... It is like the Field of Dreams. And if J.J. builds it, we're all gonna come, hard. He's pulling it off.'

Emotional visit: Smith revealed that stepping into a life-sized Millennium Falcon, shown here in the 1977 Star Wars film took him back to his childhood













