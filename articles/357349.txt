Facebook to buy video-advertising tools

Share this article: Share Tweet Share Share Share Email Share

San Francisco - Facebook agreed to acquire LiveRail, a startup that helps businesses buy more relevant video advertisements, its first move into serving video promotions on the Web beyond its own social network. Terms weren’t disclosed. The deal, announced in a blog post today, is part of Facebook’s push to offer better video ads around the Web. The company earlier this year started offering TV-like marketing clips on its site, the world’s largest social network. Facebook and Internet rivals such as Google and Twitter have made a string of acquisitions in recent months to add tools for marketers.

The companies are competing to control more of the online-advertising market outside of their main websites.

LiveRail serves video ads on the Web for ABC Family, A&E Networks, Gannett and Major League Baseball.

“We believe that LiveRail, Facebook and the premium publishers it serves have an opportunity to make video ads better and more relevant for the hundreds of millions of people who watch digital video every month,” Brian Boland, a vice president at Menlo Park, California-based Facebook, said in the blog post.

“Publishers will benefit as well because more relevant ads will help them make the most out of every opportunity they have to show an ad.”

San Francisco-based LiveRail lets companies and organisations improve targeting for video ads, automatically raising and lowering the price they’re willing to pay for a spot based on changing data about the audience.

The market for digital-video advertising in the US will grow 42 percent this year to $5.96 billion, according to researcher EMarketer Inc. - Bloomberg News