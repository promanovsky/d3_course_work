Mice that ate a gluten-free diet during pregnancy had a lower risk of having offspring with Diabetes 1.

The phenomenon could be true in humans as well, a University of Copenhagen news release reported.

"Preliminary tests show that a gluten-free diet in humans has a positive effect on children with newly diagnosed type 1 diabetes. We therefore hope that a gluten-free diet during pregnancy and lactation may be enough to protect high-risk children from developing diabetes later in life," assistant professor Camilla Hartmann Friis Hansen from the Department of Veterinary Disease Biology, Faculty of Health and Medical Sciences said in the news release reported.

The researchers are optimistic that these findings will apply to humans as well.

"Early intervention makes a lot of sense because type 1 diabetes develops early in life. We also know from existing experiments that a gluten-free diet has a beneficial effect on type 1 diabetes," Axel Kornerup from the Department of Veterinary Disease Biology, Faculty of Health and Medical Sciences said in the news release.

"This new study beautifully substantiates our research into a gluten-free diet as an effective weapon against type 1 diabetes," Professor Karsten Buschard from the Bartholin Institute at Rigshospitalet in Copenhagen said in the news release.

A gluten-free diet can change one's intestinal flora, which affects immune system development including type 1 diabetes.

"We have not been able to start a large-scale clinical test to either prove or disprove our hypothesis about the gluten-free diet," Buschard said.

The researchers hope to have the opportunity to continue their research in the future.

"If we find out how gluten or certain intestinal bacteria modify the immune system and the beta-cell physiology, this knowledge can be used to develop new treatments," Buschard said in the news release.

The findings were published in the journal Diabetes.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.