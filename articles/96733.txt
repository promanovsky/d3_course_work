Yesterday's release of the minutes from last month's Federal Open Market Committee meeting were all the spark the Dow Jones Industrial Average needed to fire up and motor higher. This has been the modus operandi of the Dow over the past couple years: cruise past all major worries and keep moving up.

A number of factors continue to drive the Dow higher, including improved U.S. GDP growth of 4.1% and 2.6% for the third and fourth quarters, respectively; a near-multiyear low in the unemployment level of 6.7%; a strong rebound in retail sales, including the best year for autos since 2007; and the continuation of near-record-low lending rates, which are allowing consumers to refinance at attractive rates and encouraging businesses to hire and expand using debt financing.

But not everyone agrees that the market, or the Dow, could head higher. Skeptics, such as myself, would be quick to point out that the drawdown of quantitative easing could expose bond prices to downward pressure since the Fed isn't buying as many long-term Treasuries. Because bond prices and bond yields have an inverse relationship, we could be staring down higher interest rates within the next couple of quarters, which has the potential to bring housing growth and job expansion to a grinding halt.

In addition, a number of companies have stepped up their cost-cutting and share buybacks in an effort to mask weak top-line growth. While certainly serving their purpose of boosting shareholder value, these tools aren't sustainable long-term solutions to keep the rally going.

Despite this group of dissenters, there exists a select group of "most loved" Dow components that short-sellers wouldn't dare bet against. That's why today, as we do every month, I suggest we take a deeper dive into these three loved Dow stocks. Why, you wonder? Because these companies offer insight as to what to look for in a steady business so we can apply that knowledge to future stock research and hopefully locate similar businesses.

Here are the Dow's three most loved stocks:

Company Short Interest as a % of Outstanding Shares United Technologies (NYSE:RTX) 0.72% General Electric (NYSE:GE) 0.76% Wal-Mart (NYSE:WMT) 0.77%

United Technologies

Why are short-sellers avoiding United Technologies?

It certainly wouldn't have been my initial guess, but aerospace and industrial juggernaut United Technologies is the Dow's least short-sold stock. The reasoning behind that optimism is threefold. First, United Technologies has been able to grow both its top and bottom line despite fears of a reduction in government spending. With this "worst is behind us" attitude, optimists have been bidding United Technologies' shares even higher. Second, the company's multiple business segments keep it highly diversified. In other words, if one segment struggles there's an opportunity for another to shine and hedge its downside. Finally, United Technologies last month reaffirmed its 2014 earnings-per-share and revenue forecast of $6.55-$6.85 on $64 billion in sales, crushing short-sellers' expectations that its growth was slowing.

Do investors have a reason to worry?

United Technologies has strong cash flow and a well-diversified business model, so the only factor that investors really need to concern themselves with is government spending. As I noted above, it appears that many of the steep budget-cut rumors were overstated; however, it's still worth noting that while revenue grew 9% last year, it rose by just 2% in the fourth quarter. It'll pay for investors to monitor in the upcoming quarter whether this revenue swoon is a sign that government spending is still questionable or if this was a one-time blip.

General Electric

Why are short-sellers avoiding General Electric?

As with UTC, there are a number of easily identifiable reasons why short-sellers stay away from General Electric. Perhaps the largest is General Electric's business diversity. Because GE has its fingers in everything from building turbines for the energy sector to diagnostic machines in the health care industry, there's a good chance that even if the economy weakened, at least a few of GE's segments would outperform. Second, GE's recovery remains on track, with its most recent quarterly report showing a backlog increase to $244 billion, global sales up 13% in growth markets and 8% in the U.S., and overall orders up 8% in total. So long as General Electric's top and bottom lines are headed in the right direction, pessimists are being kept at bay. Lastly, GE's dividend has bumped up to 3.4%, and short-sellers tend to avoid high-yielding companies, as that payment will come out of their own wallet.

Do investors have a reason to worry?

I don't believe investors have much to worry about with GE. The company made all of the right and necessary moves to shore up its financial arm since the recession, leading to a loan portfolio of much improved quality, and has been quick to raise its dividend when possible to demonstrate that it's focused on returning profits to shareholders. With so many avenues of growth in the energy and health care sectors, I don't see why anyone would go out of their way to bet against General Electric.

Wal-Mart

Why are short-sellers avoiding Wal-Mart?

Finally, we have the king of all retailers: Wal-Mart. There are two key reasons why short-sellers tend to keep their distance from Wal-Mart. First, Wal-Mart is so massive and has such impressive cash flow that it can literally walk all over local mom-and-pop stores. This comparative advantage based on price and product diversity provides a good reason to never bet against the company. Also, short-sellers tend to be very short-term-oriented, and Wal-Mart's beta of 0.4 means it's not very volatile. Its sluggish movement provides enough of an impetus to keep pessimists away.

Do investors have a reason to worry?