A new analysis of the rate at which the Antarctic ice sheet is melting has found the continent is losing almost 160 billion tonnes of ice a year, twice as much as when it was last surveyed.

The three-year long series of observations found the newly measured losses from Antarctica are enough to raise global sea levels by 0.45mm each year.

The observations were carried out by the European Space Agency's CryoSat satellite.

The study found that over the three years leading up to 2013, West Antarctica, East Antarctica and the Antarctic Peninsula lost 134 billion tonnes, 3 billion tonnes and 23 billion tonnes of ice each year respectively.

The research was conducted by a team of scientists from the UK's Centre for Polar Observation and Modelling and published in the journal Geophysical Research Letters.

The average rate of ice thinning in West Antarctica has increased compared to previous measurements.

This area's yearly loss, according to the scientists, is now a third more than measured by an amalgamation of measurements by other satellites over the five years before CryoSat's launch in 2010.

The pattern of imbalance continues to be dominated by glaciers thinning in the Amundsen Sea sector of West Antarctica, where ice loss continues to be most pronounced along fast flowing ice streams.

Thinning rates are 4m-8m per year at so-called grounding lines where the ice streams lift up off the land and begin to float out over the ocean.

The CryoSat carries a radar altimeter that can measure fine detail of the surface height variation of ice.

It surveys 96% of the continent and has boosted coverage over coastal regions where today's ice losses are concentrated.

Newly mapped areas have brought altimeter observations closer to estimates based on other approaches.