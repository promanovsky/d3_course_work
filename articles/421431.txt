So you pour your heart and soul into essays about race and prisons ... then an anthropologist tells you that Hello Kitty isn’t a cat and, well, the Internet explodes (complete with Aaron Paul animated GIF).

I did a story Tuesday about a couple of Hello Kitty-related events coming to Los Angeles. In the process, I discovered that her creators at Sanrio insist that Hello Kitty is not a cat.

“She’s a cartoon character,” Christine R. Yano, anthropologist and curator of the Japanese American National Museum’s upcoming Hello Kitty retrospective, told me. “She is a little girl. She is a friend. But she is not a cat. She’s never depicted on all fours. She walks and sits like a two-legged creature.”

Before I knew it, there were stories on LAist, Vanity Fair and Time.com. Even Josh Groban was tweeting about it: “Hello Kitty is a cat. She has whiskers and a cat nose. Girls don’t look like that. Stop this nonsense.”

Advertisement

Snoopy injected some sanity into the conversation by confirming to the world that he is indeed a dog.

Before my story, anyone who had done some careful Googling could have turned up the fact that Kitty was not a cat, as a PDF document of Sanrio’s art usage guidelines will tell you. As it states: “Hello Kitty is not a cat, she is a girl. Please do not make/use animal references.”

But who the heck thinks to Google “Hello Kitty is not a cat”? Certainly, not me.

Naturally, all of this raises the question: If Hello Kitty is not a cat, then what is she? A snow leopard? A maneki neko fortune cat? A Catwoman-esque cat-human hybrid? A socialite with too much plastic surgery?

Advertisement

Or does Kitty exist on another plane? More of a conceptual cat than the physical embodiment of a cat? Is she like Prince when he changed his name to an unpronounceable symbol? Or Marina Abramovic who has turned her thoughts into art? Is any of this real or are we living in the Matrix?

Thank goodness the Chupacabra still exists. I know because I saw him in a bar in downtown L.A.

Find me on the Twitter: @cmonstah. I am not a cat. But I am part monster.