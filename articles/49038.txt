Just to annoy the religious critics of his movie some more, filmmaker and self-professed Darren Aronofsky has called his retelling of the story of Noah, "the least biblical biblical film ever made."

Russell Crowe Plays 'Noah'

Somehow, Aronofksy has made a secular movie about the Biblical figure, painting Noah - played here by Russell Crowe - as an environmentalist in a movie that doesn't mention God once. Not once.

The retelling clearly has some serious departments from the biblical account of the event, provoking outcry from Christian groups. Studio Paramount were said to be unhappy about the whole thing and fought Aronofksy, unsuccessfully, to get the final edit. They also wanted to throw a Christian rock song in at the end - but again, met resistance from the notoriously no-nonsense director.

The filmmaker told The Telegraph: "The biblical story of Noah can be found in the book of Genesis."

The critics love Noah.

"Inventive, ambitious, brutal and beautiful: a potent mythological epic. But also willfully challenging, as likely to infuriate as inspire," said Dan Jolin of Empire magazine.

"Darren Aronofsky wrestles one of scripture's most primal stories to the ground and extracts something vital and audacious, while also pushing some aggressive environmentalism, in Noah," wrote Todd McCarthy of Hollywood Reporter.

"It is never less than fascinating - and sometimes dazzling - in its ambitions," wrote Scott Foundas of Variety.

Darren's Aronofsky's godless 'Noah' hits theaters on April 4, 2014.

More: Emma Watson shares of biblical grotty styling tips

More: Religious movies that were banned

Watch the 'Noah' feauturette:

