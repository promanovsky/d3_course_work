Google Now's card interface that pops up important information as and when users need them on their Android phones, will now also be available for desktop and laptop users via the Google Chrome browser. The news comes via the Google Chrome Google+ account.

"Starting today and rolling out over the next few weeks, Google Now notifications will be available to Chrome users on their desktop or laptop computers. To enable this feature, simply sign in to Chrome with the same Google Account you're using for Google Now on Android or iOS."

Like seen on smartphones, Google Now cards on Chrome browser will provide information on weather, sports, traffic, event reminders and more.

Google notes that Now on Chrome shows a subset of the cards you see on your mobile device, which uses your device's location. If you don't want to see Google Now in Chrome, you can turn it off at any time. Windows users can click the bell icon in the lower right corner of your computer screen to open the Notifications Center. Mac users will see it in the upper right corner. Once inside the Notifications Center, click the gear icon and uncheck the box next to "Google Now".

More details can be found from the 'Google Now Cards in Chrome' support page.

Recently Google Search app for Android devices got a couple of voice command feature updates. The app updates enable users to capture image or video and to play music by saying 'Ok Google' followed by the command.