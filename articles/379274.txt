A woman in Tennessee last week became the first person to be charged under a controversial state law that can count illegal drug use during pregnancy as assault.

26-year-old Mallory Loyola was arrested July 8 and charged with misdemeanor assault after she and her newborn baby tested positive for meth, ABC News reports. Monroe County Sheriff Bill Bivens told a local news station in Knoxville, Tenn., that Loyola admitted to smoking meth days before the birth of her child.

Earlier this month, a law went into effect that allows mothers to be “prosecuted for assault for the illegal use of a narcotic drug while pregnant” if the child is harmed by, or becomes addicted to, the drug.

Critics of the law say it discourages women battling addiction from seeking treatment or pre-natal care. The American Civil Liberties Union of Tennessee in particular has called the law “dangerous” and it said it raises “serious constitutional concerns regarding equal treatment under the law.” The law does allow anyone charged, as a defense, to enter a treatment program while pregnant and complete it following the birth.

“Anytime someone is addicted and they can’t get off for their own child, their own flesh and blood, it’s sad,” Bivens said. “Hopefully [Loyola’s arrest] will send a signal to other women who are pregnant and have a drug problem to seek help. That’s what we want them to do.”

Loyola was released on a $2,000 bail, ABC News reports.

[ABC News]

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Nolan Feeney at nolan.feeney@time.com.