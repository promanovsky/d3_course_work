Infertile men who have abnormalities in their sperm may be at higher risk of dying than men with normal sperm, a new study suggests.

In the study, young and middle-age men who were infertile because they had several problems with their sperm such as low sperm count and impaired sperm movement were twice as likely to die over an eight-year period compared to men with normal sperm.

It's important to note that the overall risk of dying for all men in the study was quite low: less than 1 percent of the men in the study died, which is lower than would be expected for men in the general population.

Still, the link between a higher risk of dying and sperm abnormalities held even after the researchers took into account factors that could affect the men's risk of dying, such as age and health conditions, including diabetes or heart failure. [Sexy Swimmers: 7 Facts About Sperm]

The reason for the findings is not known. It could be that men with sperm abnormalities tend to have undetected health problems that result in a higher risk of death, said study researcher Dr. Michael Eisenberg, an assistant professor of urology at Stanford University School of Medicine.

The men in the study were relatively young between ages 20 and 50 and they were trying to have children, which suggests they were reasonably healthy and planning for the future, Eisenberg said.

The findings should prompt additional research into the link between sperm abnormalities and risk of death, the researchers said.

Previous studies have suggested a link between infertility and poor health outcomes later in life. For example, a 2010 study found that men who were evaluated for infertility were at increased risk of developing prostate cancer. However, other studies on sperm abnormalities and risk of death have had conflicting results, with some studies finding an increased risk and others finding no link.

The new study analyzed information from about 12,000 men who visited fertility clinics in California and Texas, and who were followed for about eight years. Information was collected about the men's sperm counts, semen volume, sperm motility (ability to swim) and sperm shape.

There were 69 deaths during the study period. Men who had abnormalities in at least two of the sperm characteristics were 2.3 times more likely to die during the study period than men with normal sperm. The more sperm abnormalities men had, the greater the risk of death was.

The new finding "supports concerted efforts to better understand the interrelated and perhaps conditional nature of male fecundity and health across the lifespan," Germaine Buck Louis, director of the division of intramural population health research at the Eunice Kennedy Shriver National Institute of Child Health and Human Development, wrote in an accompanying editorial.

The study was not able to account for lifestyle factors, such as smoking, that could affect risk of dying as well as fertility. In addition, the study followed men for a relatively short period, and the findings could change if men are followed for longer periods.

The study was published May 16 in the journal Human Reproduction.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.