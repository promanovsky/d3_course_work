San Francisco - Facebook’s website appeared to be back up on Thursday a few minutes after it displayed a message saying: “Sorry, something went wrong.”

“We are working on getting this fixed as soon as we can,” the website's homepage read at 08h15 GMT.

The outage was reported in several countries - including China, Singapore and India.

The cause of the outage was not immediately known.

The company did not immediately respond to an email seeking comment. - Reuters