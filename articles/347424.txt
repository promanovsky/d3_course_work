'Monty Python Live (Mostly)': Theater Review

The first of 10 farewell shows by the British comedy legends is an uneven mix of classic sketches, archival footage and songs in the key of "Spamalot."

LONDON – What happens when iconoclasts become icons? A mere 45 years after their BBC television debut, the groundbreaking comedy team Monty Python has never been more popular or more mainstream. With a combined age of 357 years, the five surviving members played the first of 10 reunion shows in front of a rapturous crowd at the 20,000-capacity O2 Arena on Tuesday.

Initially announced as a single farewell concert, this show sold out in less than a minute last November. The final performance of the run is being beamed into cinemas worldwide on July 20, with cable TV screenings and home entertainment formats to follow.

LIST Hollywood's 100 Favorite Films

The Pythons have been talking down these shows in interviews, with Michael Palin branding much of their comedy as "crap" and Terry Gilliam calling the reunion "depressing." All candidly admit they agreed to this last lucrative comeback to pay off alimony, mortgages and legal bills. Charitable fans may have assumed this was a typically Python-esque bit of self-deprecating irony. Watching this lavish two-hour carnival of comedy, animation, music and dance, it is immediately clear that a lot of work has gone into this production. But arguably not enough of it from the Pythons themselves.

The director is Eric Idle, chief self-appointed curator of the Python estate in recent years, and driving force behind the group’s Broadway and West End musical Spamalot. Indeed, this show may be modeled a little too closely on that song-and-dance smash. It’s not quite the full Monty onstage — one of the six original members, Graham Chapman, died of cancer in 1989, hence this show’s cheerfully dark subtitle: "One Down, Five to Go." But Chapman does make a few appearances in archival video clips. Longtime Python auxiliary member Carol Cleveland also joins the cast, earning warm cheers at the O2.

The most obvious precedent for this kind of comeback is not in comedy but music, with countless old rockers reforming for cash-in farewell tours. Back in 2007, the surviving members of Led Zeppelin played this same arena for one night only, earning hysterically positive reviews for what will likely prove to be their final show together. But a more germane comparison for Python is probably The Beatles, since these comedy veterans enjoy much the same beloved national treasure status in Britain. George Harrison, a friend of the Pythons who financed several of their film projects, once claimed they were the natural heirs to the Fab Four.

But can a comedy troupe just turn up onstage and play their greatest hits? It appears so. Framed by a giant music-hall arch rendered in Gilliam’s signature psychedelic visual style, the show starts strongly with a Doctor Who Tardis stunt and a blast of pure Bunuel-esque surrealism involving Vikings and llamas. This is followed in short order by the classic "Four Yorkshiremen" sketch, a sharply observed satire about rich old men competing over the exaggerated awfulness of their humble origins.

Later in the show, Palin and John Cleese revive the terrific "Argument" sketch while Idle and Terry Jones perform "Nudge Nudge," both classic Python two-handers, with a slippery linguistic energy reminiscent of primetime Harold Pinter. All stand up well today, their dialogue largely intact but with tighter pacing geared to modern audiences.

PHOTOS Penny Lane's Coat, 'Terminator' Skull: Iconic Movie Props From the Top 100 Films

Less impressive is the large number of vintage TV clips that play on giant screens between live sketches, some dragging on for several minutes. Almost everyone in the arena will have seen this footage countless times, and the few who have not could find it on YouTube in seconds. Even more problematic are the Broadway-style musical numbers, mostly written and performed by Idle, which rely heavily on a smutty and dated humor that feels more Benny Hill than Monty Python.

In fairness, no Python fan would argue with Palin's traditional musical turn, "The Lumberjack Song," or the full-cast version of "Always Look on the Bright Side of Life" from Life of Brian, which Idle saves for the show's grand finale. But more obscure numbers such as "Christmas in Heaven," "Sit on My Face" or "I Like Chinese" lack the wit to justify the rousing Spamalot treatment they receive here. Idle the director is indulging Idle the performer, and everybody else is letting him get away with it.

PHOTOS Inside THR's Comedy Actor Roundtable With Andy Samberg, Matt LeBlanc, Tony Hale

A few comic lines have been updated, adding a sprinkle of contemporary references, but not many. The "Dead Parrot" and "Cheese Shop" sketches, two of Python's best-loved greatest hits, are smartly segued together like a musical medley. Cleese and Palin essentially play the same characters in each, and share an agreeable naughty-schoolboy chemistry on stage, forgetting lines and stifling giggles. More of this formal experimentation would have been welcome. The cameo appearances by Stephen Fry and, on film, Professor Stephen Hawking are nice touches, but disappointingly brief.

Comedy dates more quickly than most art forms, and the social attitudes embodied in these sketches could benefit from a contemporary remix. It is obviously not fair to judge the pop culture of yesteryear by the standards of today, but the Pythons must be the last people alive who find creaky caricatures of camp, mincing, cross-dressing homosexuals amusing. Likewise the dance routines, featuring young women in skimpy underwear and naked plastic breasts, feel jarringly anachronistic. What was once daring and risqué now feels perilously close to old-school, unexamined, casual sexism. Python’s legacy is a mixed one, but it is smarter and more progressive than this.

In fairness, there is still much to enjoy in this show, which ends with a mass audience sing-along and a standing ovation. And there is huge warmth in the arena toward the Python team, amplified by the strong likelihood that this will be their last waltz together. It is hard to begrudge them this final cash-in, even if it ultimately feels more like a victory for commerce than for comedy.

Venue: O2 Arena, London (runs through July 20)

Cast: John Cleese, Eric Idle, Terry Gilliam, Terry Jones, Michael Palin, Carol Cleveland

Writers: Graham Chapman, John Cleese, Eric Idle, Terry Gilliam, Terry Jones, Michael Palin

Director: Eric Idle

Producer: Jim Beach

Designer: Terry Gilliam

Set design: Stufish Entertainment Architects

Musical director and conductor: John Du Prez

Choreographer: Arlene Phillips

Costume designer: Hazel Pethig

Lighting designer: Patrick Woodroffe

Presented by Phil McIntyre Entertainments