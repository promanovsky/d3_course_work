NEW YORK (TheStreet) -- Microsoft (MSFT) - Get Report is in the process of a turnaround under new CEO Satya Nadella that investors believe will revolve around Office 365 software subscriptions, however, the company's third-quarter earnings may be clouded by the looming acquisition of Nokia's (NOK) - Get Report handset business.

Such is the Microsoft that Nadella inherited when taking the reins of the company in February 2014, torn between what many believe is an undervalued business software platform and a jumble of mostly failing consumer technology products.

Those issues now will come to the forefront as Nadella seeks to communicate a vision for Microsoft to investors, customers and analysts after the departure of long-tenured CEO Steve Ballmer. While Ballmer boosted Microsoft's sales and earnings significantly in his tenure, he was also excoriated for a string of failed consumer hardware launches such as Zune, the Windows Phone and the Surface tablet.

Nadella appears ready to commit to Office 365 and other recent launches like Azure as the fulcrum of a re-vitalization of the tech behemoth. He also has publicly supported Microsoft's tablet and smartphone products, in addition to the company's Nokia acquisition.

The former has boosted Microsoft's shares in recent months, while it is the latter that may mute investor interest in near-term results.

According to Morgan Stanley analysts, the near-term focus of investors and management guidance will likely hinge on the impact of Nokia, growth in the PC market and software growth after the expiration Windows XP. "Increased execution risk and waning near-term catalysts leave us on the sidelines," Morgan Stanley said.

Overall, Microsoft is expected to earn $20.4 billion in fiscal third-quarter revenue, generating a $5.2 billion net profit or earning per share (EPS) of 63 cents, according to Bloomberg data.

On the top and bottom lines, Wall Street consensus has Microsoft's earnings falling slightly year over year.

Morgan Stanley forecasts slightly better PC-based revenue, momentum in sales of the Surface 2 off of a low base and strong video game software sales. Those figures, according to estimates, may be offset by stagnant Xbox One sales.

Surface sales are expected to reach 1.4 million units due to the Surface 2, however, those sales reflect just a sub-5% share of the tablet market.

Microsoft's Nokia acquisition is expected to close on Friday, and Morgan Stanley expects to hear guidance on the impact of the deal on the company's 2014 outlook. Guidance may prove a headwind.

ValueAct Likens Microsoft to Adobe

When asked about Microsoft's turnaround strategy, ValueAct's Jeffrey Ubben told TheStreet it is similar to a revitalization orchestrated at Adobe (ADBE) - Get Report under CEO Shantanu Narayen. Ubben said that like Adobe, Microsoft will move in the direction of a subscription software-as-a-service (SaaS) model, which may eventually drive pricing power and rising profitability.

Ubben, who spoke to TheStreet at the IMN Active-Passive Investor Conference on Tuesday, said Microsoft, having already launched Office 365 and Azure with a SaaS pricing model, is further along than Adobe was when the hedge fund first invested in a turnaround. He also pointed to Microsoft's recent introduction of Word, Excel and PowerPoint to iPad tablets as another positive recent development for the company.

Mason Morfit, a ValueAct executive, recently was given a seat as an independent director on Microsoft's board.

All About Nadella

"Investors will focus on the insights around the opening of Office to the iOS platform and beyond and we will be looking for outlines of the new strategic directions to grow the company out of the PC era, as well as the impact of Nokia and Office 365 on FY15 guidance," Sterne Agee analyst Robert Breza said in a April 22 note to clients.

Breza said he expects CEO Nadella to dedicate his comments to Microsoft's transition from PCs and towards tablets, mobile devices, and cloud computing. Sterne Agee holds a 'neutral' rating on Microsoft's shares given the company's turnaround process.

"[We] believe most investors will be listening for more focused comments regarding the company's ability to grow the business within the Enterprise side of the market, while stabilizing the Consumer market," Breza said.

-- Written by Antoine Gara in New York.

Follow @AntoineGara