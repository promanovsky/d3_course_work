A group of Los Angeles area congressional representatives are calling on the FCC to mediate an impasse between Time Warner Cable and other multichannel providers that has left about 70% of the region’s viewers unable to get access to the cabler’s Dodgers channel.

Time Warner Cable reportedly paid more than $8 billion in 2013 for a 25-year deal for distribution rights to the Dodgers via SportsNet LA, which is controls along with the team. But Time Warner Cable has been unable to reach deals for distribution of the channel to other multichannel providers, including AT&T U-verse, Charter Communications, Cox Communications, DirecTV, Dish Network, Mediacom, Suddenlink Communications and Verizon FiOS.

Rep. Tony Cardenas (D-Calif.) released a letter to FCC chairman Tom Wheeler in which Cardenas and seven other lawmakers arguing that the stalemate has “reached a point where mediation by the FCC is necessary.”

“We have concerns that the current dispute may set a precedent for vertically integrated companies to hold the consumer hostage to assert unfair market dominance,” they said in the letter. “The FCC must ensure that we have a competitive market and no one company has an unfair advantage at the expense of consumers.”

Other lawmakers signing the letter include Alan Lowenthal, Linda Sanchez, Janice Hahn, Lucille Roybal-Allard, Brad Sherman, Julia Brownley and Judy Chu. All are Democrats.

The FCC had no immediate comment. It is in the process of reviewing the proposed merger of Comcast with Time Warner Cable, and the issue of the availability of sports rights has been an issue in congressional hearings.

The lawmakers wrote that the issue not only is important to Dodgers fans but to restaurants and bars that are “now unable to meet the viewing demands of their customers.”

Los Angeles Mayor Eric Garcetti urged a resolution of the impasse at the Cable Show in April, but he has said little about the issue since then.

Update: Six lawmakers — Sherman, Hahn, Lowenthal, Rep. Karen Bass (D-Calif.), Rep. Gloria Negrete McLeod (D-Calif.) and Rep. Grace Napolitano (D-Calif.) — are urging Time Warner Cable, DirecTV and other multichannel providers to enter into binding arbitration. “This will be a fair and fast way to return programming to consumers,” they wrote. The letter is here.

A Time Warner Cable spokesman said they “are willing to enter into binding arbitration with DirecTV, and we appreciate the Congressman’s concern for Dodger fans. We prefer to reach agreements through private business negotiations, but given the current circumstance, we are willing to agree to binding arbitration and to allow DirecTV customers to watch the Dodgers games while the arbitration is concluded.”

