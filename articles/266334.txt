Michelle Obama hits hard on school food

Share this article: Share Tweet Share Share Share Email Share

Washington - First lady Michelle Obama made a rare but strident foray into political debate slamming a Republican bid to change US school nutrition standards as “unacceptable.” Lawmakers in the US House of Representatives, dominated by the conservative opposition to President Barack Obama, have proposed exempting some school districts under budgetary constraints from following nutritional regulations. In 2010, the president, a Democrat, signed a law on the regulations, based on recommendations from the National Academy of Sciences. The rules require schools to offer students balanced meals including fruits and vegetables to try to reduce the obesity rate in children, a serious public health concern. The issue is very close to the heart of the first lady, who is a sponsor of the youth exercise program “Let's move,” and a healthy eating advocate.

Weakening the rules already in effect “is unacceptable,” she told reporters after a meeting with nutrition experts and school district food managers.

“It's unacceptable to me not just as First Lady but also as a mother.”

“The stakes couldn't be higher on this issue,” the first lady said, pointing to adult and child obesity rates.

She stressed that she believes nutrition experts, like the Institute of Medicine, which operates under the National Academy of Sciences, should set standards, not Congress.

“We have to be willing to fight the hard fight now,” she said. “Rolling things back is not the answer.” - Sapa-AFP