The release of the Samsung Galaxy S5 is coming up pretty quickly. After Samsung announced it in late February, we are just now hearing about pre-orders and it launching on April 11th in about 150 countries. Today, pre-orders started at AT&T, US Cellular and Sprint, with T-Mobile beginning on Monday, March 24th. Now we're hearing that RadioShack is doing pre-orders and that they have a special offer for you as well.

Radioshack is offering you a $50 discount when pre-ordering the Galaxy S5 through them. However, they have not confirmed any carriers just yet, but since just about every carrier has confirmed pre-orders starting today, I'd bet that you'll be able to choose any of the carriers that Radioshack sells phones for. Which is basically everyone but T-Mobile. Additionally, this deal only works for pre-orders made in-store, unfortunately, so you'll have to actually find a RadioShack and pre-order yours. RadioShack says that the Samsung Galaxy S5's will be available for pick-up in-store starting in mid-April, which is about the same time as everyone else. Also that they are subject to stock availability.

Advertisement

Additionally, RadioShack is also offering you $75-300 off of a brand new Galaxy S5 when you trade in a select smartphone through their Trade & Save program. While that might seem like a nice idea, you can usually get more money if you sell your device through a service like Swappa.

So that's another retailer that is starting pre-orders for the Samsung Galaxy S5. It should only be a matter of time before Best Buy and Amazon announce theirs. Which they are normally cheaper than the carriers are anyways, so that's always a nice way to buy a brand new device, especially if you're buying it on-contract.

How many of our readers are planning to grab the Galaxy S5 from Radioshack? Let us know in the comments below.