Jessica Alba may have played an exotic dancer in 'Sin City', but the actress insists she will not go fully nude for any role.

The actress, 33, said she refused many a times to get naked on-screen during the duration of her 30-year acting career, reported Us magazine.

"I don't want my grandparents to see my body. That's it. It would be weird at Christmas. And, I mean, really, if you look at the movies I have done, getting naked would never 'elevate' the picture," she said.

The mother to Honor, 5, and Haven, 2, is looking gorgeous on the cover of Glamour's early summer installment, wearing a white Isabel Marant tank top tucked into lightweight Brunello Cucinelli trousers.

The new issue will be on newsstands on May 13.