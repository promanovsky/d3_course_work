In another Instagram post, Kim Kardashian shared a snap in which she kisses one dolphin.

Reality TV star Kim Kardashian shared an Instagram video of herself swimming with dolphins in a pool while on her honeymoon with Kanye West in Mexico earlier this month.

In the video, the 33-year-old mother of one is being pulled through the water by two friendly dolphins at Vallarta Adventures' Dolphin Adventure, reported Us magazine.

Wearing a life vest and black bikini, Kardashian can be heard laughing and squealing while instructors cheer her on.

In another Instagram post, Kardashian shared a snap in which she kisses one dolphin.

The couple's 90 minute programme also included an underwater ride with the dolphins.