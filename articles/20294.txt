Two new studies suggest that taking fish oil has no beneficial effect in preventing heart diseases. These prompted the American Heart Association (AHA) nutrition committee to review and incorporate new findings in carefully handling or recommending food supplements.

Researchers from both studies found that the omega-3 fatty acids in fish oil supplements, like other types of dietary fats, do not protect the heart from illnesses.

The first study, led by Rajiv Chowdhury, M.D., a cardiovascular epidemiologist at the University of Cambridge, analyzed the results of 72 studies from 18 nations involving 600,000 participants. These researches involved observational studies of fatty acids from dietary intake and fatty acid biomarkers, and randomized, controlled trials of intake of fatty acid supplementation.

Their analysis revealed that only trans fats have negative effects on the heart.

The other study, led by Evangelos C. Rizos, M.D., Ph.D., of the University Hospital of Ioannin in Greece, involved 4,203 participants aged between 50 and 80 previously diagnosed with intermediate or advanced macular degeneration in one eye brought by aging. The participants were given polyunsaturated fatty acids, macular xanthophylls, a combination of the two, or placebos. They were then followed for an average of 4.8 years.

After the study, they found no decrease in the risk of heart diseases in elderly participants with age-related macular degeneration.

"Saturated fats are not essentially the main problem when it comes to risk of heart disease," said Chowdhury to Healthday. "Also, omega-3 or omega-6 fatty acids have no or little impact on reducing cardiovascular disease outcomes."

Linda Van Horn, M.D., a member of the nutrition committee of the American Heart Association (AHA), agreed with these findings. She said in an interview with Healthday, "There's no question that eating fish provides tremendous value in reducing risk for cardiovascular disease, but the use of a supplement -- whether it's a fish oil or any other nutrient -- really needs to be handled carefully."

Further details on Chowdhury's study can be read on the March 18 issue of Annals of Internal Medicine, while Rizos' study can be read on the March 17 issue of JAMA Internal Medicine.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.