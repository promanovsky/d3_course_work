On Friday an online report claimed that due to significant layoffs at Nike’s digital sports division, the hardware side of FuelBand was going to be shut down. Nike is a prominent company involved in the wearable market so its exit would certainly have attracted headlines. However it appears that the FuelBand isn’t going anywhere, at least for now. The company has confirmed that despite layoffs it is not going to give up on the FuelBand just yet.

Advertising

In a statement provided to Re/code, a Nike spokesperson said that the FuelBand remains an “important part” of the company’s business, adding that Nike will continue to improve FuelBand App, launch new METALUXE colors as well as sell and support the wearable device itself “for the foreseeable future.” The statement does leave a lot of room open for the possibility that Nike might now launch any new products in the FuelBand lineup, even though it has decided to support existing products.

Sources who spoke with the scribe claim that Nike executives debated for months what to do with the FuelBand division, which is apparently weighing the company down due to high expenses, low margins as well as manufacturing challenges.

One thing is for sure though, even if Nike exists, its not like the FuelBand will leave a hole that won’t be filled. There are already a plethora of activity tracking and fitness related wearable devices on the market. With Apple expected to throw its hat in the ring as well, customers will remain spoilt for choice nonetheless.

Filed in . Read more about Fuelband, Nike and Wearable Tech.