‘Gone Girl’ is coming to a theater near you on Oct. 3, and we’ve got the dramatic new trailer right here! Watch!

Ben Affleck proclaims his innocence in the first official trailer for the highly anticipated screen adaptation of Gillian Flynn‘s bestselling thriller Gone Girl, but the case is far from closed. Watch the 41-year-old Oscar winner hunt for clues in the mysterious disappearance and possible murder of his wife, played by Rosamund Pike, 35.

‘Gone Girl’ Trailer: Ben Affleck Claims He Didn’t Murder His Wife

The first official trailer for the David Fincher-directed flick is packed with intrigue, romance and so much more.

In the clip, which is set to an eerie cover of the Charles Azvanour song “She“, we get our first glimpse of Ben Affleck as Nick Dunne and Rosamund Pike as his wife, Amy Dunne, who mysteriously goes missing on their fifth wedding anniversary. All signs point to Nick as the culprit — but did he really kill his wife?

Nick maintains his innocence in the trailer, saying, “I did not kill my wife. I am not a murderer.” But he definitely seems to have a dark side — as evidenced by him smashing plates and yelling in his wife’s face.

We’re sure this flick will keep us on the edge of our seats from start to finish!

‘Gone Girl’ — Why Director David Fincher Chose Ben Affleck As His Leading Man

People are already buzzing about about Ben’s performance in upcoming thriller, and director David Fincher is not surprised. He knew Ben would be perfect for the part of Nick Dunne because the actor-director already knows what it feels like to be hunted by the media.

“No one will find you out faster than the guy who made Argo,” David said in an interview with Entertainment Weekly. “Ben inherently knows what [getting that negative media attention] is like. He knows what it’s like to be hunted.”

So what do YOU think, HollywoodLifers? Are you excited to see Gone Girl? Do you think Ben will get an Oscar nod for his role in the flick? Let us know!

— Tierney McAfee

More Movie News: