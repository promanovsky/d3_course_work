NEW YORK -- CEO Jeff Bezos' annual letter to shareholders offers a glimpse into Amazon's internal workings and what it is aiming for in the future, including more grocery services and the much-discussed drone delivery.

In the letter released Thursday, Bezos outlined Amazon's offerings, including its fresh grocery business called Prime Fresh, which it has offered for five years in Seattle and expanded to Los Angeles and San Francisco. For $299 a year members get same-day and early morning delivery on groceries and other items ranging from toys to electronics and household goods. Bezos said the goal is to expand to more cities over time.

After making a hubbub about testing delivery by aerial drones in December, the company said its Prime Air team is testing fifth- and sixth-generation aerial vehicles and in the design phase on generations seven and eight. In December Bezos said Amazon was working on creating unmanned aircraft to deliver packages, but said it would take years to advance the technology and for the Federal Aviation Administration to create the necessary rules and regulations.

In a quirk, Bezos also said Amazon offers employees money to leave the company, a program called Pay to Quit, modeled after a similar program at Zappos. Once a year, the company offers $2,000 to quit, adding $1,000 a year, up to a maximum of $5,000.

"The goal is to encourage folks to take a moment and think about what they really want," Bezos said. "In the long-run, an employee staying somewhere they don't want to be isn't healthy for the employee or the company."

Amazon.com Inc. has changed the consumer shopping landscape, transforming itself into the largest U.S. online retailer, selling books, gadgets and most everything else, usually at cheaper prices than its competitors. The Seattle company has long focused on spending the money it makes to add business and expand into new areas, from movie streaming to e-readers, grocery delivery and most recently a set-top streaming device called Fire TV.

Investors have largely forgiven thin profit margins and zeroed in on the company's solid revenue growth and long-term prospects. In its most recent quarter, the company said net income and revenue both grew but results fell short of expectations. Amazon's stock is down 17 per cent from the beginning of the year.

Shares fell $14.79, or 4.5 per cent to close at $317.01 amid a broader market sell-off. The Dow Jones Industrial Average ended the day down 267 points or 1.6 per cent.