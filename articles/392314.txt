Microsoft announced on Thursday that it will let go of 18,000 employees in the next six months as part of its shift to devices and services.

Rumors about the biggest job cut after the 5,800 lay-off in 2009 first surfaced when new chief executive Satya Nadella issued a memo last week hinting about upcoming engineering and organizational changes needed to turn the company around. He wrote that the changes are needed as the company shifts its focus from devices and services to productivity, mobile, and cloud.

"Over the course of July, the senior leadership team and I will share more on the engineering and organization changes we believe are needed," Nadella wrote in the memo. "Nothing is off the table in how we think about shifting our culture."

On Tuesday, sources of Bloomberg said the job cuts will probably affect Nokia and Microsoft divisions with overlapping responsibilities as it seeks to integrate Nokia's handset unit into its domain. Microsoft acquired Nokia's handset unit in September of last year and promised to save $600 million in annual costs 18 months after the deal.

Microsoft refused to comment when asked to confirm the rumors and assumptions until CEO Nadella issued another memo detailing the changes.

"The first step to building the right organization for our ambitions is to realign our workforce. With this in mind, we will begin to reduce the size of our overall workforce by up to 18,000 jobs in the next year," Nadella wrote.

Nadella explained 12,500 employees who will lose their jobs will be coming from the Nokia Devices and Services division. The employees who will be affected will be notified over the course of the next six months.

The memo further explained that the company needed the job cuts to simplify work and to align the role of Nokia into Microsoft. The company plans make the Android-based Nokia X to become a Windows phone.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.