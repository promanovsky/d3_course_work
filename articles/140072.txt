Targeted ads based on your location? You better believe it

Facebook has confirmed the data mined through the new Nearby Friends feature may one day be used for advertising and marketing purporses

The new tool, which is rolling out to iOS and Android apps in the coming weeks, allows users to share their location with select friends enabling pals to detect when they're in the locale and arrange meet ups offline.

At the time of the announcement on Thursday, Facebook made no mention of sharing the data with advertisers, which would have no doubt coloured the reaction to the feature.

Belatedly, a Facebook spokesperson has confirmed to TechCrunch: "At this time it's not being used for advertising or marketing, but in the future it will be."

Tracking your travels

Facebook is pitching the tool as a valuable new feature for users. It sends occasional notifications informing them how often friends, who've also agreed to be seen, are in the locale.

It will also follow users on their travels, allowing friends to pitch in with suggestions or recommendations wherever they roam around the world.

Knowing the end goal may be (or always was?) geared towards targeted advertisements may see some users change their views on the feature.

Will you be switching on Nearby Friends when it hits your smartphone? Let us know your thoughts below.

Via CNET