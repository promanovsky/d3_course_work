Pharmaceuticals company Durata Therapeutics Inc (DRTX) said Friday the U.S. Food and Drug Administration has approved Dalvance (dalbavancin), a new antibacterial drug used to treat adults with skin infections.

Dalvance is intended to treat acute bacterial skin and skin structure infections, or ABSSSI, caused by certain susceptible bacteria like Staphylococcus aureus (including methicillin-susceptible and methicillin-resistant strains) and Streptococcus pyogenes. The treatment is administered intravenously.

Dalvance is the first drug designated as a Qualified Infectious Disease Product, or QIDP, to receive FDA approval. Under the Generating Antibiotic Incentives Now (GAIN) title of the FDA Safety and Innovation Act, Dalvance was granted QIDP designation because it is an antibacterial or antifungal human drug intended to treat serious or life-threatening infections.

As part of its QIDP designation, Dalvance was given priority review, which provides an expedited review of the drug's application. Dalvance's QIDP designation also qualifies it for an additional five years of marketing exclusivity to be added to certain exclusivity periods already provided by the Food, Drug and Cosmetic Act.

Dalvance's safety and efficacy were evaluated in two clinical trials with a total of 1,289 adults with ABSSSI. Participants were randomly assigned to receive Dalvance or vancomycin, another antibacterial drug. Results showed Dalvance was as effective as vancomycin for the treatment of ABSSSI.

The most common side effects identified in the clinical trials were nausea, headache and diarrhea. In the trials, more participants in the Dalvance group had elevations in one of their liver enzyme tests. The Dalvance drug label provides recommendations on dosage adjustment in patients with renal impairment.

According to Chicago, Illinois-based Durata, Dalvance is a second generation, semi-synthetic lipoglycopeptide, which consists of a lipophilic side-chain added to an enhanced glycopeptide backbone.



Durata stock closed Friday at $16.96, up $0.95 or 5.93%, on a volume of 839k shares on the Nasdaq.

For comments and feedback contact: editorial@rttnews.com