The Tri-star symbol of excellence: PMI data shows growth was again led by Germany, Europe’s largest economy. — Reuters file pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LONDON, April 23 — The euro zone’s private sector has started the second quarter on its strongest footing in nearly three years, but burgeoning new orders were again mainly buoyed by firms cutting prices, surveys showed today.

The bloc’s services industry performed better than any of the 36 economists polled by Reuters had expected, and manufacturers also had a stronger month than the median forecast had suggested.

“It’s pretty encouraging considering what we have seen for years. We are looking at 0.5 per cent quarter-on-quarter GDP (gross domestic product) growth if we continue to see this level,” said Chris Williamson, chief economist at Markit.

“The main concern is that signs of deflationary forces are still very much apparent.”

Worryingly for policymakers, who have struggled to bring inflation up to their 2 per cent target ceiling, service firms cut prices for the 29th month in a row — and did so at a steeper pace than in March.

Inflation fell to just 0.5 per cent in March, its sixth straight month in what European Central Bank president Mario Draghi has called a “danger zone” below 1 per cent and keeping pressure on the ECB to intervene.

Having already chopped its main interest rate to near zero and loaned banks more than €1 trillion (RM4.5 trillion) of cheap cash, the ECB left monetary policy unchanged this month but opened the door to turning on the money-printing presses to boost the economy and keep inflation from staying too low.

However, Markit’s flash Composite Purchasing Managers’ Index, which is widely regarded as a good gauge of growth, suggested the economic support, at least, may not be necessary.

It jumped to 54.0 in April from 53.1 in March, above the 50 mark that divides growth from contraction for the 10th month and chalking up its highest reading since May 2011. A Reuters poll had predicted no change.

Growth was again led by Germany, whose PMI, covering Europe’s largest economy, jumped from March and was only just shy of February’s 32-month high.

The rest of the bloc also performed well apart from France, where although the index held above 50 for the second month running it was well down from the previous reading.

“Perhaps the best news came from the rest of the region, where the fastest rate of growth seen since early 2011 suggests that the recovery in the periphery is gaining traction,” Williamson said.

Job boost

The services PMI bounced to 53.1 from March’s 52.2, its highest since June 2011, but the output price index fell to 48.4 from 48.6, slipping to a four-month low.

The PMI covering manufacturers rose to 53.3 from 53.0, beating forecasts for no change. A sub-index measuring output that feeds into the composite PMI climbed to 56.5 from 55.6.

To meet the growing output factories increased headcount at the fastest rate since August 2011. The employment subindex rose to 51.3 from 50.3, its fourth month in expansionary territory.

Unemployment in the bloc fell slightly in February, although the rate remained at 11.9 per cent after downward revisions of the past few months, the European statistics office Eurostat said earlier this month.

And the outlook for next month is relatively bright — the composite new business index rose to 52.8, its highest in nearly three years, from 52.5 in March.

“Clearly we should see the pace of growth continue into May and possibly June as well. Companies are beginning to feel this is something sustainable,” Williamson said. — Reuters