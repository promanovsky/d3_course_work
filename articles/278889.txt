More details are emerging about the ankle injury Harrison Ford suffered on Thursday while filming Star Wars Episode VII, which landed him in the hospital.

For starters, the incident happened at the famed Pinewood Studios in London, where the hugely anticipated J.J. Abrams production is being filmed. Ford was reportedly hurt on a soundstage while shooting a scene for Star Wars Episode VII.

The New York Daily News reports that Harrison Ford was filming next to Han Solo’s spacecraft, the Millennium Falcon when the accident occurred. Part of the vehicle fell on the actor’s ankle injuring him.

Rumors that the Millennium Falcon would be making an appearance in Star Wars Episode VII have been rampant in previous weeks and some photos were leaked by TMZ. Abrams showed a sense of humor when he tweeted this teaser about all the behind-the-scenes photos making the rounds online:

Ford was airlifted to a nearby hospital after one of the hydraulic doors from the famed Millennium Falcon came crashing to the ground, a source revealed to The Hollywood Reporter. The source adds that medics were called in and treated the veteran actor on a soundstage.

A spokesperson for Disney confirmed the injury to Harrison Ford in a statement:

“Harrison Ford sustained an ankle injury during filming today. He was taken to a local hospital and is receiving care.”

Although the exact nature of the injury is not clear, BBC reports that Thames Valley police confirmed that a 71-year-old man was injured at Pinewood Studios and was airlifted to Oxford’s John Radcliffe Hospital “with injuries which are not believed to be life-threatening.”

Disney downplayed the severity of the incident that injured Harrison Ford. However, it is a setback as his character is believed to be the focus of Star Wars Episode VII. The continuation of the saga is set to hit theaters on December 18, 2015, and it’s currently unclear how the actor’s injury will affect the release date.

Reps for Harrison Ford have not issued an official statement regarding the injury he suffered on the set of Star Wars Episode VII or his current condition. The actor reprises his role from the original trilogy that kicked off one of the most successful franchises of all time.

Harrison Ford was cast alongside original co-stars Mark Hamill, Carrie Fisher, Anthony Daniels, Peter Mayhew, and Kenny Baker. They will be joined by newcomers John Boyega, Daisy Ridley, Adam Driver, Oscar Isaac, Andy Serkis, Domhnall Gleeson, Lupita Nyong’o, Gwendoline Christie, and Max von Sydow.

[Image via Lucasfilm]