There is a Crumbs Bake Shop’s cupcake on eBay currently going for $255. It's being advertised as the "last" Crumbs cupcake. But the distinction may not hold true for much longer: An investor group may revive the flagging gourmet cupcake chain.

An investor group plans to provide financing for Crumb after the business closed all of its stores on Monday, according to CNBC. Marcus Lemonis, Chairman and CEO of Camping World and Good Sam Enterprises and a host on CNBC, and Fischer Enterprises, which bought Dippin’ Dots Ice Cream out bankruptcy, are two parties involved in the deal. Lemonis wants to incorporate other holdings, such as Sweet Pete’s, into one entity with Crumbs, according to CNBC.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Earlier, Lemonis seem to have dropped hints on social media. His Twitter account is filled with tweets and retweets of stories related to Crumbs’ closure, paired with his cautioning replies:

An obituary for @CrumbsBakeShop: our love letter to the best cupcakes in the game. http://t.co/jByI7ckuwP pic.twitter.com/ySZK4CwwHi — E! Online (@eonline) July 8, 2014

Although the Nasdaq Stock Market suspended trading of Crumbs on July 1, it still listed Crumbs under its ticker ‘CRMB.” In fact, shares went up by more than 1300 percent to $0.45 as of Thursday afternoon.Crumbs went public in 2011, after opening its first store in 2003. But high operating costs and expensive cupcakes’ decline in popularity contributed to Crumbs’ financial downfall, and the chain abruptly shut down operations Monday morning.

To avoid repeating its financial problems, Crumbs should take note from Krispy Kreme, says Darren Tristano, executive vice president at Technomic Inc., a Chicago research and consulting firm that specializes in the food industry. Krispy Kreme had expanded very quickly and had high operating costs, but then the donut business had to shut down to figure out how to make it profitable, he says in a phone interview with the Monitor. Now, Krisby Kreme has smaller shops and have broadened their menu, Triastano says.