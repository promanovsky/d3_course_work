After stopping at a fancy sushi restaurant in Japan, President Obama went to Tokyo's Miraikan Science Museum to kick a soccer ball with Honda's Asimo humanoid robot.

Advertisement

Asimo recently got a suite of upgrades to make the robot more human-like (we profiled them here ). Asimo is being developed with the aim of one day being an everyday robot that helps people out around the house. The Wall Street Journal reports that President Obama called Asimo's soccer skills "pretty impressive" and even described the robots as "a little scary. They were too lifelike."

Here's full video from the AP: