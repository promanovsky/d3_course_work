NEW YORK -- Lady Gaga is a bona fide pop star, but the singer says recording jazz music was easier than pop.

Gaga has spent two years recording an album of jazz standards with Tony Bennett called "Cheek to Cheek," to be released Sept. 23.

"You know, it's funny, but jazz comes a little more comfortable for me than pop music, than R&B music," Gaga said in an interview Monday. "I've sung jazz since I was 13 years old, which is kind of like my little secret that Tony found out. So this is almost easier for me than anything else."

Gaga made the comments with Bennett by her side ahead of the duo's taped performance at Jazz at Lincoln Center in New York City for PBS, which will air in the fall. The album's first single, a cover of Cole Porter's "Anything Goes," was released Tuesday. Other selections from the Great American Songbook that appear on "Cheek to Cheek" include "It Don't Mean a Thing (If It Ain't Got That Swing)," "Sophisticated Lady," "Lush Life" and the title track.

The 28-year-old and 87-year-old Bennett first collaborated on his Grammy-winning, platinum-selling 2011 album, "Duets II." Bennett said fans will be impressed with Gaga's vocal performance on the upcoming album.

"They're going to say we had no idea she sings that well," he said.

"And they're gonna say they had no idea that Tony dressed so crazy," Gaga added, as Bennett smiled. "When you come out in your meat dress Tony, nobody is going to know what's going on."

"Cheek to Cheek" is Gaga's first release since last year's "Artpop." Bennett released a collaborative album with Latin singers in 2012 called "Viva Duets."

The duo performed at the Montreal Jazz Festival earlier this month and surprised high school students at Bennett's Frank Sinatra School of Arts in Queens last month.

Gaga, who is currently on her "Artrave: The Artpop Ball" tour, says she is "actually happier than I've ever been."

"There's 60 years between us, and when we sing, there's no distance," she added about working with Bennett.