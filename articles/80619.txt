The Reserve Bank may maintain status quo in the upcoming annual monetary policy on April 1 as retail inflation, especially in food items, is yet to show definite signs of moderation.

"It would be quite a tough call for the RBI in the given scenario...I expect the RBI to maintain status quo," HSBC country head Naina Lal Kidwai told PTI.

RBI too had earlier indicated that its priority would be to bring down inflation, although India Inc has been putting pressure on the central bank to cut rates in order to boost growth.

In its third quarter review of monetary policy, the Reserve Bank of India (RBI) in January raised the key repo rate by 0.25 per cent to 8 per cent in a bid to curb

inflation.

Besides outlook on inflation, the central bank would also take into account the strengthening rupee and its impact on exports, she added.

Strengthening of the rupee against dollar in the past few days following inflow of foreign currency has put pressure on exports. In addition, unseasonal rains during this month may stoke food inflation in the near term.

The RBI is scheduled to announce its annual monetary policy for 2014-15 on April 1.

"In my view RBI may go for a pause this time," Federal Bank managing director Shyam Srinivasan said.

According to Punjab National Bank chairman and managing director, K R Kamath, the RBI action will depend on outlook on inflation.

The annual rate of inflation, based on the monthly wholesale price index, stood at 4.68 per cent in February. Retail inflation was at a 25-month low of 8.1 per cent in the month.

Morgan Stanley said volatility in food prices and a base effect will result in the CPI inflation to go up to 8.5 per cent in the near-term and cool off to 6.5 per cent by December.

"We see risks emerging to the food inflation outlook due to the recent weather-related concerns prompted by unseasonal rain and hailstorms in some parts of the country," it said.

Raghuram Rajan, who took charge as Governor of the apex bank last September, raised the rates during his first policy announcement, rightly foreseeing a pressure on the inflation front. He increased it again for a third time since he took charge, in January, when the market was expecting a pause.

It can be noted that even though the RBI has not formally adopted inflation targeting, it has gone public on targeting consumer price inflation down to 8 per cent by January 2015 and further down to 6 per cent by January 2016, as per the recommendations of the Patel committee.

According to SBI's economic research department, the present economic situation did not warrant any rate hike in the forthcoming monetary policy announcement for FY'15.

However, since RBI has surprised the markets in past few policies, there is a possibility of rates going either side, but with a larger probability of a rate hike, the report said.