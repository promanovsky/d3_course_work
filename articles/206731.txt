Eminem has made a public apology to his mother Debbie Mathers in the emotional video for his new single, Headlights. Watch it here.

The promo, released on Mother's Day in the US, was directed by Spike Lee and sees the 41-year-old rapper reunite with Debbie to apologize for "years of verbal abuse" and also make a plea for a united family.