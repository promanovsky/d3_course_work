Kelsey Mays, Cars.com

Strong sales and continued domestic-parts sourcing kept the Ford F-150 and Toyota Camry in the top two spots in Cars.com's 2014 American-Made Index, but the order below that has changed. GM's Michigan-built three-row crossovers (the Buick Enclave, Chevrolet Traverse and GMC Acadia) had taken at least one spot for the last three consecutive years, but their domestic-parts content fell below 75 percent in 2014. The Dodge Avenger sedan, meanwhile, placed third last year but faces immediate discontinuation without a clear successor and is disqualified.

The 2014 list has four newcomers: the Chevrolet Corvette, Honda Ridgeline, Honda Crosstour and Dodge SRT Viper. This is the first year any of those have made the list.

That's because only these 10 cars were eligible for the AMI, the fewest in the study's nine-year history. For the 2014 model year, just 13 models assembled in the U.S. have domestic-parts content of 75 percent or higher, according to the National Highway Traffic Safety Administration, but three of those, including the Avenger, were disqualified because they're being discontinued. In the 2013 model year, 14 cars met that threshold. Twenty cars met the threshold in the 2012 model year, and 30 cars met it a year before that.

Cars.com Editor's Note

In today's global economy, there's no easy way to determine just how American a car is. Many cars built in the U.S., for example, are assembled using parts that come from elsewhere. Some cars assembled in the U.S. from largely American-made parts don't sell well, meaning fewer Americans are employed to build them. Cars.com's American-Made Index recognizes cars that are built here, have a high percentage of domestic parts and are bought in large numbers by American consumers.

Domestic-parts content stems from Congress' 1992 American Automobile Labeling Act, which groups the U.S. and Canada under the same "domestic" umbrella. It's one of the bill's imperfections, but the AALA is the only domestic-parts labeling system car shoppers can find on every new car sold in America. Other domestic-content ratings — namely those used for the North American Free Trade Agreement and the corporate average fuel economy programs — are unpublished, give a simple over/under indication or lump even more countries, like Mexico, into the "domestic" pool.