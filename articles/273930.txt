There are lots of opinions on "the most important thing" that Apple announced yesterday. But for my money it was software SVP Craig Federighi's brief discussion of the way Touch ID — Apple's fingerprint security button on the iPhone 5S — has been expanded to support apps you've downloaded from the App Store.

Think about your bank account, in the form of an app.

Would you want such a sensitive piece of data sitting on your phone? Probably not. Even if you use a mobile payments or banking app — and most people don't — you'll need a password, and that's cumbersome. It means unlocking your phone with a password, and then tapping in another password to get to something like Venmo or PayPal.

In the e-commerce and retail business, that's referred to as "friction." Anything that is harder than using cash or a credit card is useless in terms of making mobile payments popular.

Also, passwords are terrible. Huge numbers of people use something like "password1234" or some other generic password. Think about your own life: You probably have dozens of online accounts that all need passwords. How many of them have the same password? Or one that is easy to guess?

Apple just fixed all that.

Because Touch ID means the only person who can use your iPhone is you, all your apps are now, in theory, completely secure. You can hand your iPhone 5S to a thief, and there is almost nothing he can do to open it (unless he has a fresh copy of your fingerprints).

In other words, the iPhone is finally useful as a secure cash store.

This could be huge. Remember, Apple has a database of nearly 1 billion credit cards already on file, via iTunes and the App Store. That's more accounts than Amazon. In essence, Apple has built an e-commerce userbase and is now rolling out the security tech that can activate it.

The mainstream media has yet to catch on to this, but the trade press has begun to wake up. Re/code said:

... every payment industry insider I spoke with on Monday fully expects Touch ID to eventually play a role in letting you pay for stuff outside of the App Store with your iPhone, in partner apps or even in the physical stores of partner retailers. Eventually, the authentication technology could help digital wallet apps become easier to use in the battle to replace real wallets at the checkout counter of physical stores.

Techcrunch:

The science fiction thinking on this is that you’ll eventually use Touch ID to open your door (with Apple’s Homekit) or buy stuff on Amazon.

... For all Apple’s posturing, this is actually one of its minute design details that does have the potential to change everything. Last night a friend of mine lost her phone, and when she went to the “Lost and Found” to pick it up, the person at the counter started asking her a series of questions to prove the phone was hers.

In response, she simply said, “Touch ID” and proceeded to unlock the phone with the tip of her finger.

Wall Street analysts like it precisely because Touch ID is one of those things that can be used for anything that requires a payment or a login. It's a universal application, like software or the web itself:

The extension of Touch ID functionality to third-party apps was a surprise and could be important for enterprise apps.

And guess, what? All Apple devices will likely get it in the future:

Currently, Touch ID is only available on the iPhone 5s, though Apple is widely expected to expand the capability to all iOS devices through the year.