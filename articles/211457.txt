On May 2, the Centers for Disease Control and Prevention announced that they had confirmed the first case of Middle Eastern Respiratory Syndrome (MERS) in the United States. That patient was in Indiana - he had travelled from Riyadh via London and Chicago.

This afternoon, they will be providing details on the second confirmed case in the US. This patient is in Florida.

Advertisement

No other details are known at this time.We will update this post with more details as they are announced, beginning at 2pm ET.

The virus that causes MERS is related to SARS. It's less contagious but more frequently fatal, and most cases so far have been found in Saudi Arabia.