Shutterstock photo

Investing.com -

Investing.com - U.S. stocks opened lower on Wednesday, as market sentiment remained under pressure after the World Bank lowered its outlook for global economic growth.

During early U.S. trade, the Dow 30 declined 0.39%, the S&P 500 shed 0.30%, while the NASDAQ Composite slid 0.32%.

Late Tuesday, the World Bank said tensions in Ukraine and bad weather in the U.S. weighed on global economic expansion in the first half of 2014 and estimated the global economy would grow 2.8% this year, down from a previous forecast of 3.2% made in January.

In the telecom sector, Sprint Corp (NYSE:S) tumbled 1.02% after saying it is planning to use a cloud-based mobile service from ItsOn Inc., which lets carriers manage billing and offer customers flexible plans.

Google Inc (NASDAQ:GOOGL) added to losses, down 0.72%, after the tech giant announced that it will be acquiring satellite company Skybox Imaging for $500 million, in a move to boost its mapping services and improve Internet access.

The company also said it is building software and hardware tools to manage power lines and other infrastructure in order help utilities deliver electricity to homes and businesses more efficiently.

In other news, American International Group (NYSE:AIG) retreated 0.76% after promoting Peter Hancock to chief executive officer after his predecessor stabilized the insurance company and paid back a 2008 government bailout.

Across the Atlantic, European stock markets were lower The DJ Euro Stoxx 50 declined 0.49%, France's CAC 40 retreated 0.58%, Germany's DAX dropped 0.87%, while Britain's FTSE 100 lost 0.58%.

During the Asian trading session, Hong Kong's Hang Seng slipped 0.25%, while Japan's Nikkei 225 climbed 0.50%.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.