United Arab Emirates has plans to send the first Arab spaceship to Mars in 2021. The prime minister and Dubai ruler Sheikh Mohammad bin Rashid Al Maktoum claims the mission should highlight the positive contributions from the Arab world. He added that they also chose the difficult journey to Mars because such challenges inspire and motivate.

UAE’s plans to visit Mars

A statement from the cabinet added the unmanned space probe will boost the local economy and increase the talent pool in technology and aerospace fields.The UAE already made investments in space technologies which have surpassed $5.44 billion (or 20 billion dirhams). The country already has Al-Yah Satellite Communications, Dubai Sat, and Thuraya Satellite Communications.

The agency will run the space technology sector while supervising the probe mission. It will also take approximately nine months to get to Mars. This will be the ninth country in the entire world to explore the planet Mars.

USGS released a new map of Mars

The United States Geological Society (USGS) recently released the most thorough map of Mars. The new map was actually created thanks to observations from 4 orbiting spacecraft for about 16 years. This map should give scientists a better observation of Mars. The USGS lead author Kenneth Tanaka added that spacecraft exploration of Mars in the last few decades significantly improve their own understanding of what science processes, geological materials, and event helped shape the planet to what it is today. He added this map will bring research together to a holistic context highlighting the key relationships of time and space. This offers new information to create and test out hypotheses.

The map covers the entire surface of Mars. It also pieces together information to create a bigger picture. There is little doubt this new map will help UAE’s upcoming journey to Mars.

via: AlJazeera