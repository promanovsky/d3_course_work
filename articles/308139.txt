New York: Business software giant Oracle said on Monday it was buying rival Micros Systems for $5.3 billion (Dh19.5 billion) in a deal to expand its big data efforts in key retail sectors.

The acquisition is the biggest for Oracle since buying Sun Microsystems in a deal announced in 2009 and closed in 2010.

The all-cash takeover comes from Oracle’s $39 billion cash stockpile and comes after the software giant founded by Larry Ellison produced disappointing results over the past quarter.

“Oracle has successfully helped customers across multiple industries, harness the power of cloud, mobile, social, big data and the Internet of things to transform their businesses,” said Oracle’s president Mark Hurd.

“We anticipate delivering compelling advantages to companies within the hospitality and retail industries with the acquisition of Micros.”

Raimo Lenschow at Barclays said the deal “is a net positive for Oracle,” by boosting its presence in the retail market — particularly for advanced retail terminals using embedded chips for greater security.

“Micros is well positioned competitively in the point-of-sale space, where we believe a secular change is underway as ‘chip and pin’ payment technology gains traction globally and traditional credit card terminals fall out of favour,” Lenschow said in a note to clients.

Trey Thoelcke at 24/7 Wall Street said the deal could help Oracle fill important gaps.

“Oracle, which was late to the cloud, has been playing catch-up by launching its own cloud-based products and acquiring smaller cloud companies, such as marketing software maker Responsys Inc,” he said in a blog post.

Micros has some 567,000 customers, including hotels, casinos, restaurants, retail, leisure and entertainment providers in more than 180 countries.

Its products include so-called point-of-sale devices — which replace cash registers — and tablets installed in restaurants to allow diners to see and order directly from menus.

Oracle said the acquisition would have a positive impact on its results immediately.

Micros was founded in 1977 in Columbia, Maryland, near the US capital Washington.