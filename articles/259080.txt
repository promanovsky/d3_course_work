The Defense Advanced Research Projects Agency (DARPA) has announced plans for a cutting-edge technology-based research program to develop a tiny, implanted chip in the skull to treat psychiatric disorders such as anxiety, PTSD and major depression.

The aim is to find new, more effective treatment options for veterans and service members with such debilitating conditions.

The project, called Systems-Based Neurotechnology for Emerging Therapies (SUBNETS), will study what exactly goes wrong with brain networks in people with certain psychiatric disorders. The researchers expect to ultimately use the project results to develop potential therapies aimed at "teaching" the brain to "unlearn" the detrimental patterns that underlie such disorders.

Research teams from a number of institutions will participate, led by the University of California, San Francisco (UCSF) and Massachusetts General Hospital (MGH). The team at UCSF will focus first on recording neurological activity in different regions of the brain to identify pathways associated with anxiety and depression. Then they hope to develop devices to stimulate precise locations in the brain to strengthen alternative circuits.

"Human brain recording can now reveal aspects of mental illness that have been inaccessible to scientists and doctors," UCSF neurosurgeon Dr. Edward F. Chang, team leader on the new project said in a statement. "By analyzing patterns of interaction among brain regions known to be involved in mental illness we can get a more detailed look than ever before at what might be malfunctioning, and we can then develop technology to correct it."

In this artist's concept, a miniature electronic device would be placed between a patient's skull and scalp. A series of electrodes placed at varying depths in different regions of the brain would record neurological data. A clinician could wirelessly review the data and communicate with the device to prescribe tailored therapies. Massachusetts General Hospital and Draper Labs

Meanwhile, the team at MGH will work with Draper Laboratories to develop a tiny device that could be implanted under a patient's skull. Electrodes would record neurological data, and then the device could be programmed to deliver tailored therapy to the brain.

Researchers say their work will build on the established concept of deep brain stimulation, which has been used successfully to correct brain circuitry that has gone awry in movement disorders like Parkinson's disease.

The program is based on the idea that the brain has a certain amount of plasticity, which means that its anatomy and physiology can actually alter over time to support the organ's normal functioning. The notion of brain plasticity is different from previously held ideas of the adult brain as a "finished" organ that cannot undergo changes. And, because the brain is plastic, researchers are hoping that it can be trained to start functioning normally after the onset of a psychiatric disorder.

"The brain is very different from all other organs because of its networking and adaptability," Justin Sanchez, the DARPA program manager for SUBNETS, said in a statement.

In addition to the human suffering they cause, psychiatric disorders also take a serious economic toll. For instance, anxiety disorders such as panic disorders, phobias and post-traumatic stress disorder are estimated to cost $42 billion a year in the United States, which is nearly one-third of the country's total mental health care costs, according to the Anxiety and Depression Association of America.

"There are millions of people for whom these disorders are not well treated," Dr. Vikaas Sohal, an assistant professor of psychiatry at UCSF, said in a statement. "These patients are often not able to keep their jobs or to work at all, because they're constantly struggling with symptoms of their illnesses and the pain and suffering they cause."

The SUBNETS project was launched in support of President Obama's Brain Initiative - a $100 million program designed to advance the understanding of human brain function and fund new technologies to battle neurological and psychiatric diseases.