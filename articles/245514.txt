NYSE Euronext CEO Duncan Niederauer will retire in August 2014, moving up the date in light of the rapid integration of InterContinentalExchange and the NYSE, according to a statement from the company.

Thomas Farley, the COO of the NYSE, will take the title of President of NYSE Group, effective immediately.

"It has been a privilege to lead this great, iconic organization, and I am very proud of all that we accomplished during my time at NYSE Euronext," said Neideraurer in a press release. "The transition to ICE ownership has gone smoothly, and with our integration well on track, accelerating the final stages of my transition will only extend that progress and provide clarity on future leadership."



During Niederauer's tenure, the percentage of technology IPO's listed on the NYSE rose from 13 percent in 2006 to 54 percent in 2013, according to the statement.

He also drove the sale of NYSE Euronext to ICE.

This story is developing. Please check back for further updates.

for the latest on the markets.