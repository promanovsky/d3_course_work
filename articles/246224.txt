LG G3 has leaked extensively in the past few days. We’ve seen some teasers from LG, as well as a handful of unofficial press renders of the purported device. It went so far that images of the pre-launch presentation made its way to the internet, revealing the full set of specifications of the device.

Earlier today, we spotted a leaked press render of the LG G3 for T-Mobile. Now, @evleaks has shared a press render of the alleged LG G3 with Verizon branding on it. You do know very well what this means, it will hit the Big Red once it’s officially announced by the Korean manufacturer.

The alleged handset is said to come with a 5.5-inch display sporting a resolution of 2560 x 1440 pixels, with Snapdragon 801 chipset under the hood.

The purported LG G3 will also feature 3GB of RAM and 32GB of internal storage with a MicroSD card slot for expansion. In the rear, it will come with a 13MP camera with optical image stabilization plus for photos and videos. Other rumored features include a 3,000 mAh battery, 2MP front-facing camera, LTE connectivity and Android 4.4.2 KitKat as its operating system.

LG has scheduled a press event on May 27th where the company is expected to announce the handset, along with the LG G Watch. We’ll update you as soon as it gets official.

Source: @evleaks

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more