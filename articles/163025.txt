Cairo: As the number of reported cases of the potentially deadly Middle East Respiratory Syndrome surged this week, public health experts are struggling to understand where it originated, how it is spread and why there has been a sudden spike in infections.

In the past week, significant outbreaks occurred in Saudi Arabia's coastal business hub of Jeddah and in the neighbouring United Arab Emirates’ capital Abu Dhabi, both involving healthcare workers, infectious diseases experts said.

Graphic: Mallory Brangan

The World Health Organisation is particularly worried these two clusters could indicate there is an “evolving risk” in the spread of MERS. Pointing to “critical information gaps” regarding the transmission of the virus, it has offered to work jointly with national health authorities in Saudi Arabia and the United Arab Emirates to investigate the outbreaks in order to determine the transmission chain.

It is not yet clear whether either country has taken it up on the offer.