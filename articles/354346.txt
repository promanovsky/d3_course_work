Should Google be the arbiter on what is and is not publicly available on the Web? What happens when a person's request to be anonymous conflicts with the freedom of the press?

These are some of the questions being raised as Google begins to remove search results in its first implementation of the European Union's so-called right-to-be-forgotten ruling. The ruling lets people in the European Union submit a form to Google requesting that they be removed from its search results if they feel the information on them is outdated or reveals personal details they want to keep private. A team at Google then determines whether that person's right to be anonymous is more important than keeping that information front and center in its search results for ease of public access. To date, Google has received a reported 50,000 requests for removal since the ruling was implemented last week.

The exact details of the search giant's data removal procedure, however, remains unclear. But the procedure was called into question this week when several European news organizations received e-mails from Google alerting them that some of the articles they had published on their website had been removed – notably, articles from the UK's Guardian newspaper and the BBC.

The Guardian reported Wednesday that it received notice that six of its articles had been removed from Google's search results. Three of those articles concerned Dougie McDonald, a former soccer Premier League referee who resigned his post after lying about awarding a controversial penalty kick. The BBC, meanwhile, was informed that a 2007 blog post by Robert Peston on how former Merrill Lynch chief executive Stan O'Neal was forced out of the investment bank.

Why did Google remove these posts? And, when Google is often the first and only place people turn to for information, what do these removals mean for the freedom of information in the digital age?

Well, removal from Google's search results does not mean removal from the Internet, nor does it mean removal from Google. Rather, it removes the results that would normally appear when searching a certain person's name. However, there are still other ways to get that same information. For example, doing a search for the BBC's coverage of Merrill Lynch at the time of the financial crisis would still bring up that same Robert Peston article about Stan O'Neal. Further, this ruling only applies to the EU. Names that have been removed from EU search results will still be available on Google search results outside of the EU – in the US, for example. Moreover, users in the EU can still search on google.com – the American version of Google – as opposed to Google's European counterparts, like google.co.uk, and receive the same search results that they would have received before any removals.

All of these work-arounds seem to indicate that Google has not fully embraced the EU's ruling hook, line, and sinker.

"We’ve recently started taking action on the removals requests we’ve received after the European Court of Justice decision," a Google spokesperson told The Wall Street Journal. "This is a new and evolving process for us. We’ll continue to listen to feedback, and we’ll also work with data-protection authorities and others as we comply with the ruling."

Regardless, any sort of tampering with Google search results could be viewed as detrimental to important information that has been out in the public and then seemingly disappears in the blink of an eye.

"I’m sure there are people who’ve got really hideous stuff written about them of an untrue nature," Mr. Peston told The Wall Street Journal. "But I can’t see remotely how it’s in the public interest to hide my story, and one sort of slightly worries that this is the beginning of a very messy process—and one that is ultimately damaging to the freedom of the press."

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Both The Guardian and the BBC have said that they have been given no opportunities to challenge their removals, again raising the question as to whether Google should be able to unilaterally eliminate a search result without consulting all affected parties.

For now, we'll have to wait and watch this saga of removal plays out as Google weighs individual privacy versus the public's right to know.