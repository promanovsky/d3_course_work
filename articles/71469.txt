tech2 News Staff

As Samsung and Apple, head to court for a second round of their patent war in San Jose, California, Judge Lucy Koh of the US District Court of Northern California has overruled Samsung's objection to an instructional video on the patent system that will been shown to the jury.

According to CNET , Samsung had a problem with the video because it "includes several scenes that show Apple products." You can check out the video on juror training below:

Recode, which first reported the story, says that Samsung saw the video as a plug for Apple and instead wanted the jury to see an older video which doesn't include Apple products.

The report quotes Samsung's motion as saying, “Because Apple’s alleged innovation is a central disputed issue in this trial, it would be highly prejudicial to Samsung to show the jury — before any evidence is introduced — an official instructional video that depicts Apple products in such a context.Doing so would raise serious concerns about Samsung’s ability to obtain a ‘fair trial’ by ‘impartial’ jurors,” which is one of the most ‘fundamental’ interests that exists under the Constitution.”

The latest list of offending items on the patent war includes have been expanded to include some newer devices.

Apple claims the following Samsung products now infringe on their patents: Admire, Galaxy Nexus, Galaxy Note, Galaxy Note II, Galaxy SII, Galaxy SII Epic 4G Touch, Galaxy SII Skyrocket, Galaxy SIII, Galaxy Tab II 10.1, and Stratosphere.

Samsung claims the following Apple products infringe on Samsung patents: iPhone 4, iPhone 4S, iPhone 5, iPad 2, iPad 3, iPad 4, iPad mini, iPod touch (5th generation), iPod touch (4th generation), and MacBook Pro.

Put in simple terms, Apple says Samsung stole five patents on newer devices, including Galaxy smartphones and tablets. In a counterclaim, Samsung says Apple stole two of its ideas to use on iPhones and iPads.