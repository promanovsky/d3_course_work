A Swiss helicopter company today denied that any of its staff had been involved in leaking a medical file on injured Formula One champion Michael Schumacher and filed a lawsuit over the theft.

The denial by Rega, which organised the sportsman's transport from a French hospital to Switzerland last month, came a day after French prosecutors tracked down the IP address of the computer used in the theft to a helicopter firm in Zurich, where Rega is based.

Rega has said it received a medical dossier on Schumacher, who was injured in a devastating ski accident in December, but denied that its staff had been involved in attempts to sell the stolen information.

"Rega has no evidence that any of its employees failed in this regard. At this stage, we assume that the rights of the patient to medical confidentiality have been preserved," it said.

The Zurich-based company, which is the main operator of air ambulances in Switzerland, said it has not been made aware of "any investigations or proceedings by the authorities" in relation to the case.

"For the sake of absolute clarity in relation to this affair, Rega has today filed a complaint against persons unknown with prosecutors in Zurich," the firm said.

The records were stolen and offered for sale for 50,000 euros (USD 68,000) last month.

French police have opened a criminal probe into the theft of the data, which was offered to a number of journalists.

French prosecutors on Monday said they had tracked down the IP address of the computer used by the alleged thief, which they said was in the "office of a helicopter firm based in Zurich". The name of the firm was not released.

The driver spent more than five months in hospital in the French city of Grenoble after sustaining serious head injuries in a skiing accident at Meribel on December 29.

Schumacher, long a resident of Switzerland, was brought by ambulance on June 16 from Grenoble to a cutting-edge clinic in the Swiss city of Lausanne.

The transfer was conducted with utmost secrecy.

Rega had on Monday confirmed that it had provided a medical opinion on the merits of a transfer operation -- and thus received a medical file -- and had organised the ambulance.