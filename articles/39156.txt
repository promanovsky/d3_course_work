Lieff Cabraser Heimann and Bernstein

While other Silicon Valley companies were allegedly colluding to prevent recruiting of each other's employees, Facebook refused Google's request to participate, according to a recent court filing.

Facebook Chief Operating Officer Sheryl Sandberg said in a court filing unsealed Friday that she was approached around 2008 by a senior Google executive with a proposal to limit recruiting and hiring activities between the two companies but declined. The lawsuit, which is scheduled to go to trial in May, accuses Adobe Systems, Apple, Google, Intel, Intuit, Lucasfilm, and Pixar of conspiring to eliminate competition for employees by fixing wages and agreeing not to actively recruit from each other.

"In or about August 2008, I was contacted by Jonathan Rosenberg, who was then at Google," Sandberg said in her filing. "Mr. Rosenberg expressed concern about what he described as the perceived rate at which Facebook could hire employees from Google. Around the same time, I also discussed a similar topic with Omid Kordestani, who was also at Google."

"I declined at that time to limit Facebook's recruitment or hiring of Google employees," she said. "Nor have I made or authorized any such agreement between Facebook and Google since that time.

Both Google and Facebook, which is not a party to the lawsuit, declined further comment on the matter.

The lawsuit, which was filed in 2011 by former employees of the named companies, accuses the companies of carrying out this "interconnected web" of agreements between 2005 and 2009. The lawsuit was granted class action status last October by US District Court Judge Lucy H. Koh and is slated to go to trial in May.

The suit focuses specifically on the companies targeted by a 2009 antitrust investigation by the US Department of Justice. That investigation and the civil lawsuit that followed were settled back in September 2011, with the aforementioned companies agreeing to discontinue the non-solicitation agreements. Nonetheless, the suit says the companies are still profiting in the aftermath of the practice.