Watch Justin Bieber perform live at Coachella with emerging rapper Chance The Rapper.

Watch Justin Bieber perform live at Coachella with emerging rapper Chance The Rapper....

BEYONCE dazzled with sister Solange, Jay-Z wowed with Nas and then Justin Bieber attempted to bask in the cool of Coachella with a surprise cameo.

It seemed at first Bieber was trying to protect his anonymity when he jumped on stage with American hip hop rising star Chance The Rapper to perform their Confident collaboration.

The huge sunglasses and bucket hat Bieber sported may have been more of a fashion statement for the retirement community of Palm Springs near the Coachella site.

And the gangsta-lite look completed with bandannas, big chain and baggy shorts appeared to suggest Bieber is keen to maintain his bad boy persona.

There were plenty of shapes thrown and some crotch-grabbing before Bieber took to social media to declare “that was fun”.

On-again girlfriend Selena Gomez was spotted among the celebrities taking in the three-day festival at polo fields in Indio, California.

His afternoon spot came the day after beaming Beyonce and Solange wowed festival punters with their choreographed routine during Losing You, finishing off with huge sisterly hugs.

Gwen Stefani made her first post-baby gig appearance, jumping up with Pharrell Williams to perform her hit Hollaback Girl.

Other A-listers spotted in the crowd were Modern Family star Sarah Hyland, Coachella regulars Katy Perry and Kelly Osbourne, alongside singing actors Jared Leto and Vanessa Hudgens.

###