Sony Corp., maker of the world’s best-selling video-game console, resumed service at its online networks after a hacking attack knocked them off line and a company executive was targeted with a bomb threat.

PlayStation Network and Sony Entertainment Network, which enable gamers to play each other and access content, had been taken off line by a so-called distributed denial of service attack, the Tokyo-based company said. The company has seen no evidence that personal information was accessed by computer hackers who drove artificially high traffic to Sony servers, it said.

A Twitter Inc. account with the name Lizard Squad posted messages claiming responsibility for the PlayStation Network outage and also comments about explosives on a flight carrying John Smedley, president of Sony’s online entertainment unit. The threat of explosive material on the plane was referred to the FBI, the company said earlier today.

American Airlines Flight 362 from Dallas to San Diego was diverted to Phoenix for a security-related issue, Michelle Mohr, a spokeswoman for the carrier, said by phone.

Smedley posted on his verified Twitter account of his planned flight to San Diego before Lizard Squad’s message about explosives was posted.

“Yes. My plane was diverted. Not going to discuss more than that. Justice will find these guys,” Smedley wrote on Twitter after the plane landed.

Maintenance on the networks scheduled for today won’t proceed as planned, Sony said.

In 2011, Sony suspended operations on the PlayStation Network for six weeks after a hacking attack, which led to the theft of data from more than 100 million accounts.

The company said it improved security after that attack by increasing firewalls and adding software to monitor intrusions and system vulnerabilities.