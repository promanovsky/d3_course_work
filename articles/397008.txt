On tonight's finale of ABC's "The Bachelorette," Andi Dorfman will make her final decision between Josh Murray and Nick Viall. While her love story is just beginning, we took a look back to see what became of the former "Bachelorette" couples.

It's been more than 10 years since Trista Rehn and Ryan Sutter found love on the first season of "The Bachelorette." They are now happily married with two beautiful children, but not all couples have stood the test of time. Check out the timeline below to see what happened to your favorite bachelorettes after the final rose.

Photo: International Business Times