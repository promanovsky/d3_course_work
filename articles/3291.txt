"Before there were girls, there was the first Girl."

So begins the Girls parody to end all parodies, featuring Lena Dunham in all her hipster glory on Saturday Night Live. In the video, Dunham apes her own hit HBO comedy with a biblical spoof in which she stars as Eve, "a struggling twenty-something in the garden of Eden."

The video was one of the highlights of Dunham's first time hosting SNL this weekend. Dunham and other cast members also did a nice job riffing on ABC's show Scandal.

Dunham wasn't the only actor to make an appearance on the latest SNL. Liam Neeson made a cameo during a cold open in which President Obama (played by cast member Jay Pharoah) gave an address on the international conflict in the Ukraine.