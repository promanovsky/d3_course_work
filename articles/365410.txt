SOLANGE Knowles has finally addressed that infamous elevator scuffle with brother-in-law Jay Z, saying that the family has well and truly kissed and made up.

Speaking to Lucky magazine, the singer referred to the incident as “that thing”, before quickly changing the subject.

“What’s important is that my family and I are all good,” she told the magazine. “What we had to say collectively was in the statement that we put out, and we all feel at peace with that.”

Said statement was released back in May, after security footage leaked showing Solange attacking the rapper after the Met Gala in New York, as sister Beyonce stood back and watched.

“As a result of the public release of the elevator security footage from Monday, May 5th, there has been a great deal of speculation about what triggered the unfortunate incident,” the statement read. “But the most important thing is that our family has worked through it. Jay and Solange each assume their share of responsibility for what has occurred.

“They both acknowledge their role in this private matter that has played out in the public. They both have apologised to each other and we have moved forward as a united family.”

While the source of the fight has never been revealed, a source told Us Weekly that it “was over something so small and stupid”.