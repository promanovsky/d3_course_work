Google has updated its Hangouts app, and introduced a number of new features and enhancements to the app, including merged conversations, widget support and a bunch of performance improvements.

Merged conversations is a new feature, more like iMessage on the iPhone, which merges SMS and Hangouts conversation with a same recipient. Users also get the ability to merge and unmerge conversations at will.

The new update also brings a homescreen widget which let users add a widget to the homescreen for quick access to recent conversations. The complete change log of the updated Hangouts app is not available in the Google Play Store, but Google’s Mike Dodd shared it on Google+:

Merged conversations : SMS and Hangout conversations with the same recipient are now combined into a single conversation. You can control whether you want to send a message via Hangouts or SMS with the flip of a switch, and different message types will be easy to tell apart in the conversation. Of course, you can always merge and unmerge conversations if you’d like.

: SMS and Hangout conversations with the same recipient are now combined into a single conversation. You can control whether you want to send a message via Hangouts or SMS with the flip of a switch, and different message types will be easy to tell apart in the conversation. Of course, you can always merge and unmerge conversations if you’d like. Simplified contact list : now there’s two main sections in contacts — People you Hangout With, and Phone Contacts — making it simpler to navigate, and easier to use for SMS.

: now there’s two main sections in contacts — People you Hangout With, and Phone Contacts — making it simpler to navigate, and easier to use for SMS. Homescreen widget : add the Hangouts widget to your homescreen for quick access to your recent conversations.

: add the Hangouts widget to your homescreen for quick access to your recent conversations. Performance improvements: today’s update includes better quality video calls, as well as improved SMS and MMS reliability.

The update process is gradual, and don’t worry if you can’t spot the update. It should roll out this week, so keep checking the Google Play store and download the latest Hangouts app.

Source: Google+

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more