The argument of why Tesla Motors Inc (NASDAQ:TSLA) is like Apple Inc. (NASDAQ:AAPL) certainly isn’t a new one. This time around though, we have it from the perspective of an Apple expert. Forbes contributor Ken Kam tells us the view of Marketocracy’s Eugene Groysman.

Tesla breaks auto boundaries

Groysman said he first became interested in Tesla Motors Inc (NASDAQ:TSLA) when he started seeing the automaker’s cars around Milwaukee, where he lives. He said if Tesla vehicles made their way from large cities like New York or Chicago, then it means that they have become more cost effective. He believes that technologies must become cost effective before they can disrupt any industry.

According to the Apple Inc. (NASDAQ:AAPL), he sees Tesla Motors Inc (NASDAQ:TSLA) has surpassing the boundaries which have held back other automakers. He notes that both Nissan Motor Co., Ltd. (ADR) (OTCMKTS:NSANY) (TYO:7201) and Toyota Motor Corp (ADR) (NYSE:TM) (TYO:7203) both have electric vehicles, but they’re just an additional offering. Tesla, meanwhile, only offers electric vehicles. He called the Model S “a very compelling car.”

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

What Tesla can do to disrupt

Groysman agreed with others in saying that Tesla Motors Inc (NASDAQ:TSLA) must continue to improve its battery. The automaker is working on making the battery more cost effective for average drivers. Also Tesla must continue to improve the range and cut down the amount of time it takes to charge the vehicle. He believes that the superchargers are the first step in Tesla’s disruption of the auto industry. However, he believes that a five-minute charge would more directly place Tesla in competition with traditional cars because that would be about in line with a standard fill-up at a gas station.

So in a nutshell, Tesla Motors Inc (NASDAQ:TSLA) must spread its supercharger stations throughout the nation, build its planned gigafactory and beat back the political opposition which has developed in a number of states. Auto dealers have been resisting Tesla’s direct to consumer sales model because they see it as a threat.

Having faith in Tesla

[drizzle]

He also stated that in order to profit off of Tesla Motors Inc (NASDAQ:TSLA)’s stock, investors will have to have faith that CEO Elon Musk is enough like Apple Inc. (NASDAQ:AAPL) CEO Steve jobs. Jobs provided very effective leadership, and Musk will have to do the same thing. Groysman believes he is up to the task though.

He predicts continued volatility in Tesla Motors Inc (NASDAQ:TSLA) shares, although he thinks they will continue to rise as long as the automaker keeps making progress in its quest to make a more cost effective electric vehicle. He believes the automaker’s stock could even double when its car does become cost effective. In addition, he suggests that Ford Motor Company (NYSE:F) and General Motors Company (NYSE:GM) may have to cut out dealerships or else compete with a major disadvantage in terms of cost.

[/drizzle]