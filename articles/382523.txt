"Birds of a feather flock together," says social network researcher James Fowler. "And now we know why."

Compared to strangers living in the same area, people who are friends are much more likely to to share a number of closely related genes, according to a new study co-authored by Fowler, a professor of medical genetics and political science at the University of California, San Diego, who studies the intersection of natural and social sciences.

Fowler and his colleague Nicholas Christakis, professor of sociology, evolutionary biology, and medicine at Yale, examined 1.5 million markers of genetic relatedness in more than 1,900 people.

Their study concluded that, overall, friends are about as genetically similar as fourth cousins. The genes that friends had most in common were olfactory genes connected to the sense of smell, while genes for disease immunity were least likely to be shared. The study is published today in Proceedings of the National Academies of Sciences.

Both olfactory and immune genes changed rapidly over evolutionary history, Fowler told CBS News. "The genes we share are the genes that evolved fastest. Human friendship has turbocharged evolution."

He cited an example of how some genes could create synergies to help friends survive over evolutionary history: "If you are the first person to get a mutation to speak language, it is worthless without friends who also have that mutation. You need friends to get the benefit of language. Many traits are built on this property."

Olfactory genes work together to allow you to detect and interpret odors. In a press release from UCSD Fowler explains why we might share the same smell genes between friends: "Our sense of smell draws us to similar environments. It is not hard to imagine that people who like the scent of coffee, for example, hang out at cafes more and so meet and befriend each other." Fowler says that the reason may be deeper than that, but it will take more research to figure it out.

Why aren't genes for immunity more likely to be shared? Researchers have previously found that spouses tend to have lots of genes in common, but very different collections of immune genes from each other, so the finding that friends have different - perhaps complementary - immune profiles does not surprise Fowler. He points out that "being surrounded by people who are immune to different pathogens protects you, and prevents the spread of diseases."

Scientists have examined several ways that people might detect immune genes in others, like face shape and body odor, but the mechanism remains unclear.

The researchers used data collected as part of The Framingham Heart Study , a long-term study involving thousands of residents in Massachusetts.

Fowler and Christakis have studied social networks for years and coauthored a best-selling book, "Connected."