SAN FRANCISCO (MarketWatch) — Microsoft Corp. may soon be ready to fill a gaping hole in its product line that has been a common gripe of industry analysts for years: Releasing a version of its popular Office software suite for Apple Inc.’s iPad.

According to several media reports, Microsoft MSFT, +0.20% Chief Executive Satya Nadella will hold an event in San Francisco on March 27 to unveil Office for the iPad as part of his strategy aimed at focusing Microsoft on cloud-computing and mobile technology for consumers as well as the software giant’s enterprise customer base.

“It is clearly a directional or strategic benefit to Microsoft that the company is moving Office beyond PCs and Windows to faster-growth platforms and cloud services,” said Nomura Research analyst Rick Sherlund, in a research note.

The potential for using Office to draw in customers that use the iPad is huge. Bernstein Research analyst Mark Moerdler said that Apple AAPL, +0.18% has sold around 195 million iPads, and had an installed base of about 150 million of the tablets at the end of 2013. Moerdler estimates that if even 10% of the iPad installed base were to subscribe to Office, Microsoft could add around 15 million new subscribers and generate between $1.1 billion and $1.5 billion more in subscription revenue from Office every year.

Microsoft has been developing a version of Office for the iPad for at least a year, but has been hesitant to release it for fear that it would cannibalize the company’s own Windows franchise and tablet offerings, which already offer the software suite.

Raimo Lenschow, an analyst with Barclays, said that Office for iPad “is something that many investors have wanted the company to embrace for a long time” because, in addition to giving a boost to Office revenue, it would also signal that Microsoft is moving towards a more serious strategy across multiple technology platforms.

“It seems like it would be a change in tactics for the new CEO,” said Raimo Lenschow, of Barclays. “And Nadella wants to put people more towards adoption of cloud-services because he sees that as the future.”

The initial reaction among investors suggests that Lenschow is right about the market’s reaction toward Office on the iPad: Microsoft’s shares rose more than 4%, to a 52-week-high of $39.90 a share. Apple’s stock also rose by $2.77 a share to $529.54.

Moerdler added that if Microsoft does announce Office for the iPad ahead of launching a new touch version of Office it will likely be doing so “to counter alternate word processing, spreadsheet and presentation software from Apple, Google, Evernote and others, [and] drive more users to Microsoft office 365 subscriptions.”

Office 365 is a subscription-based suite of software products such as Microsoft Word, as well as cloud-based storage, for consumers and businesses.

More tech news from MarketWatch:

Amazon to ship video-streaming device in April

Wal-Mart looking for a cut of the used videogame business

H-P seen gaining as IBM exits key market