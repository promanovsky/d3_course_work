HTC has updated its flagship HTC One smartphone by giving it a larger screen, better software and more camera features.

The original HTC One received good reviews and was named the best smartphone of 2013 at the mobile industry's premier trade show in Barcelona, Spain, last month. But HTC has failed to translate that glowing praise into sales.

Big toy: The One (M8) screen measures 5 inches diagonally. Credit:AP

Pre-orders for the new smartphone, officially the HTC One (M8), are now available online and it will be available from Telstra, Optus and Vodafone on Tuesday, April 1.

The phone is priced at $899 when bought outright in Australia, and will also be available on subsidised contracts from the three major telcos.