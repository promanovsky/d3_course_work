UK pound holds four-day gain

Share this article: Share Tweet Share Share Share Email Share

London - The pound held a four-day advance against the dollar before the Bank of England releases minutes of its May meeting, as investors search for signs on whether policy makers are moving closer to increasing interest rates. UK government bonds were little changed before a report economists said will show retail sales excluding auto fuel climbed 0.5 percent in April, after sliding 0.4 percent the previous month. Bank of England Deputy Governor and Monetary Policy Committee member Charlie Bean said yesterday policy makers face potential “potholes” when it comes to exiting the extraordinary stimulus measures they implemented during the recession. “While markets have probably become accustomed to strong UK data, a bigger potential market mover would be a shift in tone by the BOE,” BNP Paribas SA strategists led by Steven Saywell, the London-based global head of foreign-exchange strategy, wrote in a note today. “We will be watching for signs of dissent in today’s MPC minutes.”

The pound was little changed at $1.6858 as of 8:52 a.m. London time after rising 0.4 percent in the previous four days.

Sterling was at 81.36 pence per euro, after appreciating to 81.20 pence yesterday, the strongest level since January 2013.

The Bank of England kept its benchmark interest rate at a record-low 0.5 percent this month, where it has been since March 2009. Policy makers said in their quarterly Inflation Report published last week that while the level of spare capacity in the economy had “narrowed slightly” in the past three months, there “remains scope to make greater inroads into slack before raising” borrowing costs.

Rates Outlook

Speaking in London yesterday, Bank of England’s Bean said the neutral level of interest rates would “gradually edge up” but would probably be “materially lower” than it was before the crisis for the next five years.

“I do not expect central banks’ collective management of the exit from the present exceptionally stimulatory monetary stance will be easy,” Bean said.

“Market interest rates are bound to become more volatile along the exit path, however well central banks communicate their intentions.”

The Bank of England also said last week that some reduction in the stock of assets it bought in its quantitative-easing program could be achieved without active sales, as the gilts in the portfolio mature.

While Bean said this was a possibility, the fact that the weighted-average maturity of the bonds held is more than 12 years means that might take too long.

The 10-year yield was at 2.61 percent today.

The rate climbed nine basis points, or 0.09 percentage point, in the previous three days.

The price of the 2.25 percent gilt due September 2023 was 97.035.

Gilts returned 3.7 percent this year through yesterday, Bloomberg World Bond Indexes show.

German bonds gained 4 percent and Treasuries earned 3.2 percent.

The UK currency has gained 10 percent in the past 12 months, the best performer among 10 developed-nation currencies tracked by Bloomberg Correlation-Weighted Indexes.

The euro added 4.7 percent, while the dollar fell 2.1 percent. - Bloomberg News