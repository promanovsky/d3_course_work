Energy shares were mixed in pre-market trade Monday as crude oil futures dropped.

Light, sweet crude oil for August delivery was down 0.11% at $100.72 per barrel. In other energy futures, heating oil was flat at $2.86 per gallon while natural gas was down 0.48% at $4.13 per million British thermal units.

In energy ETFs, the United States Oil Fund ( USO ) was down 0.05% at $37.14. The United States Natural Gas ETF ( UNG ) was down 0.61% at $22.71.

In other energy news, oil and gas producer Whiting Petroleum ( WLL ) shares were up more than 4% after it said over the weekend that it has signed a definitive agreement to buy energy company Kodiak Oil & Gas ( KOG ) in an all-stock transaction valued at $6 billion, including Kodiak's net debt of $2.2 billion as of March 31.

The merger is expected to create a Bakken/Three Forks producer with over 107,000 barrels of oil equivalent per day of production in Q1 2014, 855,000 combined net acres and an inventory of 3,460 net future drilling locations.

KOG was up 2.7% at $14.62 pre-market.

As agreed, Kodiak shareholders will receive 0.177 of a share of Whiting stock in exchange for each share of Kodiak common stock they hold, representing consideration to each Kodiak shareholder of $13.90 per share based on the closing price of Whiting common stock on July 11. This represents a premium of approximately 5.1% to the volume weighted average price of Kodiak for the last 60 trading days.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

Copyright (C) 2016 MTNewswires.com. All rights reserved. Unauthorized reproduction is strictly prohibited.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.