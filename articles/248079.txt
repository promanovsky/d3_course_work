Updated from 6:51 a.m. EDT

Here are 10 things you should know for Thursday, May 22:

1. -- U.S. stock futures were rising Thursday and Asian shares ended mostly higher after a manufacturing survey suggested the slowdown in China's economy is flattening out.

European stocks were flat.

Japan's Nikkei 225 index gained 2.1%. China's Shanghai Composite Index dipped 0.2%.



2. -- The economic calendar in the U.S. on Thursday includes weekly initial jobless claims at 8:30 a.m. EDT, existing home sales for April at 10 a.m., and leading indicators for April at 10 a.m.

3. -- U.S. stocks on Wednesday advanced as investors cheered meeting minutes of the Federal Reserve's policy-making arm, which didn't signal a move to raise the benchmark interest rate any time soon.

The Dow Jones Industrial Average climbed 0.97% to close at 16,533.06, while the S&P 500 rose 0.81% to 1,888.03. The Nasdaq jumped 0.85% to 4,131.54.



4. -- Tobacco companies Reynolds American (RAI) and Lorillard (LO) are in advanced merger talks, according to reports.

Reynolds American is the second-largest tobacco company in the U.S., while Lorillard is the third-largest. No. 1 Altria has a 50% share of the U.S. tobacco market.

The potential takeover would combine Reynolds brands such as Camel and Pall Mall with Lorillard's Newport, the top-selling menthol cigarette. It also would give Reynolds a commanding position in the fast-developing market for electronic cigarettes, according to The Wall Street Journal. Lorillard's blu e-cigarette has nearly a 50% market share in U.S. gasoline and convenience stores.

Reynolds shares weren't active in premarket trading on Thursdaay. Lorillard shares dipped 3 cents.



5. -- Printer and computer maker Hewlett-Packard (HPQ) - Get Report is forecast after Thursday's closing bell to report fiscal second-quarter earnings of 88 cents a share on revenue of $27.41 billion.

HP's earnings will be closely watched by investors as they look to see whether CEOMeg Whitman can continue to cut costs and turnaround one of Silicon Valley's most prestigious companies.

6. -- McDonald's (MCD) - Get Report is scheduled to hold its annual shareholders' meeting on Thursday, where it is expected to be confronted on issues over low pay for fast-food workers, the company's executive pay packages and marketing to children.

Protesters were arrested on Wednesday after refusing to leave corporate property outside McDonald's headquarters.



7. -- Electronics retailer Best Buy (BBY) - Get Report posted first-quarter adjusted profit of 33 cents a share, which was ahead of Wall Street expectations. Sales fell 3.3% to $9.04 billion, below forecasts of $9.21 billion.

The stock fell 5.3% in premarket trading to $24.02 after Best Buy said it expects same-store sales declines in the second and third quarters.

8. -- Vivendi, the French conglomerate, said it planned to sell half its stake in Activision Blizzard (ATVI) - Get Report.

Vivendi will sell 41.5 million shares of the video game publisher. The offering is expected to close on May 28.

The stake is worth about $866 million based on Activision's closing price Wednesday of $20.87.

9. -- The initial public offering for Chinese online retailer JD.com was priced above expectations as it raised almost $1.8 billion.

In the offering, 93.7 million American depositary shares were sold for $19 each. It was indicated the IPO would be priced at between $16 and $18 a share.



10. -- Unilever (UL) - Get Report said it agreed to sell ownership of its Ragu and Bertolli brand sauces in North America to Japan's Mizkan Group for $2.15 billion.

>> Read More: Don't Even Think About Selling Facebook

>> Read More: Apple's 7:1 Stock Split: Take a Bite Before June 2

>>Read More: Hedge Funds Hate These 5 Stocks - Should You?





-- Written by Joseph Woelfel

To contact the writer of this article, click here:Joseph Woelfel

To submit a news tip, send an email to:tips@thestreet.com.

Follow @josephwoe58

Copyright 2014 TheStreet.com Inc. All rights reserved. This material may not be published, broadcast, rewritten, or redistributed. AP contributed to this report.