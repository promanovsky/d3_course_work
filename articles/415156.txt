Chelsea captain John Terry has paid tribute to Lord Attenborough, the club's life president, who has died at the age of 90.

The actor and filmmaker trained with the club as part of his preparation for his role in Brighton Rock - and remained a fan for the rest of his life.

John Terry said that Lord Attenborough was a passionate and knowledgeable fan - who was always welcomed in the dressing room.