Microsoft promises to never email snoop again

Microsoft won’t go snooping through users’ private content in Outlook, OneDrive, or other services, even if it believes it might find evidence that secrets stolen from the firm are there, as the company changes its policies in reaction to outcry earlier this month. Microsoft had admitted it dug through Hotmail messages back in 2012 as part of an investigation into details of Windows 8 leaked by an employee; however, as a result of the negative reaction from users since the revelation, Microsoft now says it will take a different approach, in a move that may force similar changes from Google and Apple.

“Effective immediately,” Brad Smith, General Counsel and Executive Vice President of Legal and Corporate Affairs at Microsoft, said today, “if we receive information indicating that someone is using our services to traffic in stolen intellectual or physical property from Microsoft, we will not inspect a customer’s private content ourselves. Instead, we will refer the matter to law enforcement if further action is required.”

The changes will also be reflected in an updated version of the terms of service for products like Outlook, making it legally binding.

As Smith reiterates, Microsoft was within its rights to go through the former manager’s inbox. However, given the company has taken a vocal stance – among others – on things like government surveillance and transparency, the lawyer admits, it seems only fair to take a more stringent approach to its own investigations.

Microsoft had already revised its polices as a result of the outcry, announcing it would in future consult both a separate legal team from any internal investigation and an outside attorney, a former federal judge. However, the hands-off approach will now be even more stringent, with Microsoft handing over all responsibility for data-analysis to law enforcement.

Whether this will force a similar reaction by Google and Apple, which each operate popular cloud services like Gmail and iCloud, remains to be seen. However, Microsoft seems to be aiming to push their hand.

As a result of Microsoft’s prompting, the Center for Democracy and Technology will be holding talks with stakeholders in the industry – including the Electronic Frontier Foundation – to figure out a code of best-practice that’s industry-wide.