Centenarians are more likely to die of "old age" rather than the chronic conditions often associated with old age such as cancer, compared to older adults younger than 100.

Researchers from King's College London found that 61% of centenarians were most likely to die in a care home and 27% were likely to die in a hospital. Around 10% were found to die at home and 0.2% in hospice care.

Dr Catherine Evans, clinical lecturer in palliative care, said: "Centenarians have outlived death from chronic illness, but they are a group living with increasing frailty and vulnerability to pneumonia and other poor health outcomes."

The study examined the cause and place of death in 35,867 centenarians in England between 2001 and 2010, and compared these findings with those of people who died in their 80s to 90s.

Old age was the most common cause of certifying death (28%), followed by pneumonia at 18%. Strokes accounted for 10%, heart disease 9% and other circulatory diseases were the cause in 10%.

Dementia and Alzheimer's disease accounted for 6% while cancer was just 4%. Pneumonia was the cause of the largest group of hospital deaths.

The research also linked Office for National Statistics death registration data for England taken between 2001 and 2010, with the area level data on deprivation (a measure of socioeconomic status of the region), settlement type place of residence and care home bed capacity.

People aged 100 years or over are a rapidly growing demographic across the world, projected to reach 3,224,000 by 2050. In the UK, the number of centenarians has doubled every 10 years since 1956 and is estimated to reach over half a million by 2066.

Meanwhile, the number of centenarian deaths per year in England increased by 56% in 10 years from 2,823 in 2001 to 4,393 in 2010. The 10-year study included 35,867 people with a median age of 101 years at time of death, of whom 87% were women.

The researchers warned that new health services were necessary for the rise in 100-year-olds, which could help to reduce reliance on hospital admission at the end of life and ensure a better quality of life.

They added that the proportion of elderly people dying in hospital in England was high.

"In the Netherlands and Finland, more than three-quarters of people aged over 90 die in a long-term care setting such as a nursing home; far fewer die in hospital," the team said.

"Hospital admission in the last weeks of life accounts for a third of the total cost of end-of-life care per patient. Increasing the number of care home beds could reduce the reliance on hospital care, but we need to ensure caliber services are provided by GPs, community nurses and other healthcare working with social care providers to enable people to remain in their usual residence at the end of life if they choose."

The study, funded by the National Institute for Health Research Health Services, was published in the journal PLOS Medicine.