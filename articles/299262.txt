General Motors Co. (GM) announced Monday that it is recalling 3.16 million midsized and large cars in the U.S. to fix their ignition switches.

The recall covers vehicles made between 2000 and 2014, according to Forbes.

The recall is the second to take place in consecutive business days, with the German automaker recalling over 500,000 Camaro sports cars and other models on Friday.

The issue with the ignition key led to GM's recall of 2.19 million small cars in the U.S. in February and March, USA Today reported. The faulty part is connected to 54 crashes and 13 deaths in previously recalled vehicles.

GM said that it was aware eight crashes and six injuries related to the most recent recall. However, the company has not been informed of any deaths connected to the problem.

The problem with the switches is that if the key is carrying extra weight and is jerked by the driver, the ignition switch might move out of the "run" position, which would shut the engine off and disable the airbags.

Instead of replacing the whole ignition switch, GM is looking to use an insert that fills a slot on the head of the key. The insert would leave only a small hole for a key ring. The company said the insert will reduce the leverage on the key, which would reduce the likelihood of the key rotating out of "run" if the car has a "jarring road event" like hitting a pothole or railroad tracks.

The recalled cars include the 2005-2009 Buick Lacrosse, 2006-2011 Buick Lucerne, 2004-2005 Buick Regal LS GS, 2006-2014 Chevrolet Impala, 2000-2005 Cadillac DeVille, 2004-2011 Cadillac DTS, and 2006-2008 Chevrolet Monte Carlo, Forbes reported.

GM urged customers to take everything off the key and to only drive with the ignition key until the cars are brought to dealers. The company said it will fix the vehicles' keys "in the next few weeks."

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.