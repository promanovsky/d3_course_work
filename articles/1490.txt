General Motors Co needs to get through its ignition-switch recall and the resulting federal investigation quickly to avoid any lasting damage to its brand, but the federal probe could keep the problem in the public eye for at least six months.

Analysts and academics warn that a misstep by GM could leave it with a lingering headache, something Toyota Motor Corp experienced from 2009 to 2011 with recalls linked to sudden acceleration. The Japanese automaker was criticized for being slow to react to complaints and initially blaming the drivers.

GM is currently interviewing employees dating back to the discovery in 2004 of the problem with the ignition switch, which has since been linked to 13 deaths, sources previously said. Meanwhile, U.S. safety regulators have opened an investigation into whether the No. 1 U.S. automaker reacted swiftly enough in its recall last month of more than 1.6 million vehicles.

GM Chief Executive Mary Barra on Tuesday in a letter to GM employees said she deeply regretted the circumstances but was pleased with the company's response and the focus will be on customer safety and satisfaction.

Moving past the recall and related fallout will be critical to ensure the Detroit company continues the rebound since its 2009 bankruptcy reorganization, analysts said.

"They need to get past this as quickly as possible," said George Cook, a marketing professor at the Simon Business School at the University of Rochester.

"You cannot be reactive in dealing with the American car-buying public," added Cook, formerly an executive at Ford Motor for 10 years. "You have to be proactive and I think they'll be forgiving if it's not really, really serious. People have short memories about that stuff."

GM's recall was to correct a condition that may allow the engine and other components, including front airbags, to be unintentionally turned off.

GM has said the weight on the key ring, road conditions or some other jarring event may cause the ignition switch to move out of the "run" position, turning off the engine and most of the car's electrical components. GM has recommended that owners use only the ignition key with nothing else on the key ring.

The company said last week that the initial replacement parts will be available in early April.

Trent Ross, senior vice president of reputation and risk management at Ipsos Public Affairs, said GM needs to take ownership of the issue right away. Several analysts lauded Barra for her letter.

"GM needs to show it's doing the right thing," he said. "And Mary Barra has to demonstrate that the buck stops with the CEO."

In her letter to employees, Barra emphasized that whether the company's reputation and sales suffer is not the issue.

"Our company's reputation won't be determined by the recall itself, but by how we address the problem going forward," she said.

GM faces a fine of up to $35 million from U.S. National Highway Traffic Safety Administration in addition to recall costs it has not outlined. Analysts agreed the big costs could come from lawsuits likely to result from the recall and probe.

Toyota, for instance, settled economic-loss claims in its case for more than $1 billion and is negotiating settlement of hundreds of personal-injury lawsuits. It also is in talks to settle a criminal probe by the U.S. Justice Department for a reported $1 billion.

However, that has not hurt sales or stopped Toyota from ranking highly in consumer quality rankings conducted by J.D. Power and Associates, suggesting that any damage to GM could be fleeting, analysts said.

GM's case could take time, however, given the Detroit company's internal probe and the investigation opened by the NHTSA, which Cook said could last at least six months.

Allan Kam, a former enforcement attorney with NHTSA, said the agency's 107-question "special order" was unusual. "I've never seen one quite as detailed as this one," he said.

Kam speculated GM would prefer to settle the NHTSA probe in a way that would allow its answers not to be published widely. However, the mounting pressure on NHTSA, with members of Congress sending letters of inquiry, may make that outcome unlikely.

Copyright: Thomson Reuters 2014