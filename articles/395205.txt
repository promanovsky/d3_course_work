NASA’s Opportunity Mars Rover has set a new record for the longest off-Earth driving distance, the administration announced.

Opportunity has driven 25 miles since it arrived on Mars in 2004. It crossed the milestone after a 157-foot drive on Sunday.

“This is so remarkable considering Opportunity was intended to drive about one kilometer and was never designed for distance,” John Callas, Mars Exploration Rover Project Manager, said in a statement. “But what is really important is not how many miles the rover has racked up, but how much exploration and discovery we have accomplished over that distance.”

Opportunity is traveling along the rim of the Endeavor Crater, where NASA has gathered evidence of ancient water supply that was less acidic than those studied elsewhere on the planet.

As the rover approached this milestone, the team behind it named a 20-foot-wide Mars crater after the previous record holder, the Soviet Union’s Lunokhod 2, which drove 24.2 miles on the moon in 1973.

Get our Space Newsletter. Sign up to receive the week's news in space. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Nolan Feeney at nolan.feeney@time.com.