Shutterstock photo

Investing.com -

Investing.com - The number of people who filed for unemployment assistance in the U.S. last week fell broadly in line with market expectations, easing concerns over the strength of the labor market, official data showed on Thursday.

In a report, the U.S. Department of Labor said the number of individuals filing for initial jobless benefits in the week ending June 21 declined by 2,000 to a seasonally adjusted 312,000 from the previous week's revised total of 314,000.

Analysts had expected jobless claims to fall by 4,000 to 310,000 last week.

Continuing jobless claims in the week ended June 14 rose to 2.571 million from 2.559 million in the preceding week. Analysts had expected continuing claims to increase to 2.570 million.

The four-week moving average was 314,250, a decrease of 2,000 from the previous week's total of 312,250. The monthly average is seen as a more accurate gauge of labor trends because it reduces volatility in the week-to-week data.

Following the release of the data, the U.S. dollar held on to gains against the euro, with EUR/USD shedding 0.15% to trade at 1.3609, compared to 1.3604 ahead of the data.

Meanwhile, the outlook for U.S. equity markets was mildly lower. The Dow pointed to a loss of 0.1% at the open, the S&P 500 indicated a decline of 0.15%, while the Nasdaq 100 signaled a fall of 0.1%.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.