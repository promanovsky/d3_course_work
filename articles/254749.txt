“X-Men: Days of Future Past” will gross $111 million by the end of the four-day holiday weekend, according to studio estimates distributed early Monday. Factoring in overseas grosses, Bryan Singer‘s film should top $300 million worldwide in its first few days.

That is the biggest opening of any movie in the “X-Men” franchise, and it kicks off what should be a stellar summer for its distributor, Fox.

The studio already scored two hits in April — “Rio 2” and “The Other Woman.” While the former has grossed just $122 million in the United States, it has surpassed $400 million worldwide thanks to its massive popularity overseas.

Also read: ‘X-Men’ Stars Evan Peters, Shawn Ashmore Nerd Out About ‘X-Men: The Animated Series’ and Favorite Characters (Video)

“The Other Woman,” a romantic comedy that received middling reviews, has been a big hit thanks to a dearth of movies that appeal to women. With $4.5 million in projected grosses this weekend, the $40 million movie has banked $78 million in the United States – and $164 million worldwide.

This is but a sign of what’s to come for the studio, whose next release is “The Fault in Our Stars,” a teen drama that has rival studios in awe. Based on a book by best-selling author and vlogger John Green, the movie boasts a massive following online and a movie star-in-training in Shailene Woodley.

Also read: ‘Fault in Our Stars’ Rising: How Social Media Is Turning the Bestseller Into a Shailene Woodley Box-Office Hit

Its social mentions rival those of fellow blockbusters “X-Men,” “22 Jump Street” and “Maleficent” – not bad for a movie that cost $12 million to produce.

It remains to be seen whether this massive social buzz will translate into tickets sold, but advanced tracking projects an opening of more than $30 million. Fox is underselling the numbers, as all studios tend to do in advance of a release.

See video: ‘X-Men’ Stars Evan Peters, Shawn Ashmore Nerd Out About ‘X-Men: The Animated Series’ and Favorite Characters

Fox’s next release will be “How to Train Your Dragon 2,” as sure a hit as there is this summer. Though the bulk of the grosses will go to DreamWorks Animation, Fox will also benefit from the biggest animated film of the season.

Following “Dragon” is “Dawn of the Planet of the Apes,” another sequel that poses just one question for its distributor: how high?

The only question mark on the entire schedule is “Let’s Be Cops,” an action comedy set for mid-August. If it hits, all the better. If not, it is a gentle gust during an otherwise serene summer.

See video: ‘X-Men’ in 3 Minutes: Everything You Need to Know Before Seeing ‘Days of Future Past’

As for the rest of the box office, “Godzilla” grossed $38 million and had another strong weekend on IMAX screens, which stuck with the primordial monster rather than jumping to the mutants of “X-Men.”

“Blended” finished third with $17 million for the weekend, a pedestrian opening for the Adam Sandler-Drew Barrymore romantic comedy. It barely held off “Neighbors,” which has topped $100 million in just three weeks.

See photos: X-Men for Dummies: 17 Mutants You Need to Know Before ‘Days of Future Past’

“Million Dollar Arm,” the baseball drama starring Jon Hamm, dipped below $10 million over the holiday, a disappointing result for Disney.

“X-Men” grossed more than all of those movies put together, though next weekend it faces a potent newcomer, Disney’s “Maleficent.”