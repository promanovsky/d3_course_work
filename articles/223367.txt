NEW YORK (TheStreet) -- InterMune (ITMN) surged to a one-year high of $39.90 on Monday after the biotechnology company reported data on its lung disease drug pirfenidone, also known as Esbriet, that could lead to U.S. regulatory approval.

The drug is already approved in Europe to treat idiopathic pulmonary fibrosis, a disease that scars the lungs and that currently has no cure or known cause. U.S. regulators rejected the drug in 2010 and requested a new trial to determine the drug's effectiveness.

Pirfenidone and Boehringer Ingelheim's nintedanib both showed effectiveness in slowing the disease's progression. The two drugs were studied separately and were not compared to each other directly. For more on this story, read TheStreet's Adam Feuerstein's article here.

Must Read: InterMune Higher on Superior Lung Drug Data, FDA Resubmission Next

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

InterMune will submit Pirfenidone for approval in the next few weeks and could be on the market in the first quarter of 2015.

The stock was up 13.82% to $39.05 at 2:15 p.m.

ITMN data by YCharts

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.