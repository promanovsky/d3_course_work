Samsung's CEO Lee Kun-hee is in a stable condition in hospital after suffering a heart attack at his home on Sunday.

Lee had to be resuscitated at a hospital near his home before he was transferred to a Samsung-run hospital, where he was diagnosed with an acute myocardial infarction and operated on.

The company said Lee was in a stable condition on Sunday afternoon.

The 72-year-old is the head of the world's biggest technology company which was founded by his father Lee Byung-chul. Lee took over control of the company in 1987 and has transformed the company from one primarily focused on memory chip manufacturing into a sprawling conglomerate which has seen rapid growth in recent years thanks to the huge success of its smartphone business.

However the company has seen its earnings fall in the last six months, and its share price has fallen over the past 12 months.

Lee's health has come under scrutiny in recent years following his treatment for lung cancer in 2000. He appeared frail when he spoke to staff during his annual new year's address in January.

There has been a lot of speculation regarding Lee's successor and the timing of that appointment, with most believing that Lee's son and Samsung vice-chairman Lee Jae-yong is the anointed one.