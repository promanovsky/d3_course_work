Mozilla offers FCC a fresh take on net neutrality

Net neutrality is a hot-button topic, and the FCC currently hovers a finger right over it. A new proposal by Mozilla has some interesting fundamentals in place, and deftly challenges the FCC’s understanding of the matter altogether. If Mozilla has their way, the FCC will turn the Internet on its ear — and that may be the best thing for all.



In new rules being proposed by the FCC, the ISP would be allowed to charge service such as YouTube a higher fee for access to us, the end user. That would essentially place the Internet in the hands of the wealthy, leaving start-ups and bootstrap sites wanting.

For some, the crux of the argument is whether or not the FCC should/could/would reclassify Internet Service Providers (ISP), allowing the federal committee to oversee them much differently. For Mozilla, the FCC is misguided, and likely can’t be trusted. Saying they want the FCC to “modernize its understanding of Internet access services” because “Internet users and developers cannot know whether future FCC Chairs will maintain vigilance”, the Firefox maker wants the FCC to act wholly differently in regard to Net Neutrality.

Mozilla is suggesting the FCC reclassify the content providers like YouTube (or wherever else you go on the Internet), not so much the ISP. The crux of the argument for some is whether or not the FCC should reclassify ISPs themselves, but Mozilla’s argument sidesteps that by recognizing the third party — content providers. By reclassifying those edge providers, the FCC has better oversight on net neutrality, and they don’t have to revisit decades of rulings and classifications for ISPs — according to Mozilla.

The entire brief is available via the Mozilla blog, and we suggest anyone concerned with an open Internet as it currently stands to take a look. It’s a fresh take on a tired argument, which is appreciated. Mozilla has the petition before the FCC currently, and says they “plan to continue working with policymakers and thought leaders on Internet policy to develop and advance these ideas over the next few weeks.”

Source: Mozilla

