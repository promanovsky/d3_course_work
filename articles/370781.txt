Fifty new cases of Ebola and 25 deaths have been reported in Sierra Leone, Liberia and Guinea since July 3, as the deadly virus continues to spread in families, the World Health Organization (WHO) said on Tuesday.

In a statement, the United Nations agency said that the latest figures from health ministries in the three countries showed a total of 844 cases including 518 deaths in the epidemic that began in February.

Guinea's ministry reported two deaths since July 3, but no new cases in the past week, the WHO said, calling the situation in the affected region of West Africa a "mixed picture".

Sierra Leone accounted for 34 of the new cases and 14 deaths, while Liberia reported 16 new cases and 9 deaths, it said, adding: "These numbers indicate that active viral transmission continues in the community."

WHO spokeswoman Fadela Chaib, speaking to a Geneva news briefing earlier on Tuesday, said: "This means that the two main modes of transmission are home care, people who care for their relative at home, and during funerals, are still ongoing.

"If we don't stop the transmission in the several hotspots in the three countries we will not be able to say that we control the outbreak," she said.

West African countries and international health organizations adopted a fresh strategy last Thursday to fight the world's deadliest Ebola epidemic to date. Measures include better surveillance to detect the virus and enhancing cross-border cooperation.