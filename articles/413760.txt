Could an erupting Icelandic volcano be about to jeopardise international air travel once more? Four years ago, ash from Eyjafjallajökull wrought aviation havoc in northern Europe, grounding flights for up to a week. Now there are rumblings that suggest another eruption may be imminent. What could it mean for travellers — and what rights do you have if you are stranded? Simon Calder, travel correspondent, reports.

Icelandic geology can have a disastrous impact on airline schedules. Why, exactly?

As passengers found to their cost in 2010, the aviation authorities are extremely focused on preventing aircraft flying through airspace contaminated with volcanic ash, which can damage and even shut down engines if they ingest significant quantities of it. While there are numerous potential locations where volcanoes could erupt, Iceland is particularly seismically active. And following the eruption of Eyjafjallajökull in 2010, airspace across northern Europe was closed —wrecking the travel plans of 7 million passengers and costing airlines over £1bn.



You’ve been in contact with aviation sources in Iceland — what have you been hearing?



They — like everyone else keen on flying within Europe and across the Atlantic — are keeping a close eye on the ever-restless Icelandic terrain. The Bardarbunga volcano has been simmering for the past week, but until now the hope was that it was merely what’s known as a “tourist eruption” rather than anything more serious. That means that travel firms would be able to run excursions to see the seething volcano, but it wouldn’t be powerful enough to project thousands of tons of volcanic ash into the atmosphere - which would present a threat to aviation. But yesterday the Icelandic Meterological Office said that broader seismic activity under the Vattnajokull glacier had been detected - which could signal a significant eruption. They raised the five-point threat level from orange to the highest: red.

Later today they lowered the level back to orange but said there was no indication that activity was slowing.





Virgin Atlantic has said it re-routed a London-San Francisco flight to avoid the area. How significant is that?



Detours mean extra time in the air and higher fuel burn, which airlines don’t do lightly. The only airspace closure so far is directly over the site of the seismic activity. If it was just a matter of the plane taking a slight detour, the effect would have been a few minutes’ extra flight and a modest additional fuel burn. But if the affected area broadens, the decision could extend some journey times substantially. Airlines largely follow so-called “great circle” routes — meaning that many flights between the UK and the western part of North America fly close to, or over, Iceland. For example, Heathrow and Vancouver are on almost the same latitude, but the most direct route between the two cities goes way north.



Could things get as bad as in 2010?

That was an extraordinary week, with all aviation grounded in northern Europe and the government sending buses and ships to Spain to collect stranded tourists. The airlines were furious at the time because they felt the aviation authorities hugely over-reacted — and indeed to force the UK government’s hand, British Airways despatched long-range jets full of passengers from around the world to London. They were all allowed to land. Since then there’s been lots of research done to avoid such a calamitous closure of airspace. But if large expanses of the North Atlantic airspace are closed that could start wreaking some havoc with airline schedules, and lead to cancellations.

Thousands of flights were cancelled in 2010, leaving British passengers stranded around the world. Source: Getty Images. (Getty Images)



What are your rights if you do get stuck abroad?

INDY/ GO Weekly Newsletter TIME TO TRAVEL! Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/GO newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ GO Weekly Newsletter TIME TO TRAVEL! Thanks for signing up to the INDY/GO newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here