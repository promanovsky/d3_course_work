SA backs Nigeria’s economic step up

Share this article: Share Tweet Share Share Share Email Share

Johannesburg - The announcement that Nigeria is now the biggest economy in sub-Saharan Africa proves that the continent is rising, the ministry of finance said on Monday. “The announcement resonates with South Africa's consistent message since 1994 that we want to see more African economies grow and live up to their potential, just as we continue striving to do so in with our own economy,” the ministry said in a statement. “South Africa has been and will continue to benefit from faster economic growth in the rest of the continent.” On Sunday, Nigeria reportedly became Africa's biggest economy after its government announced a rebasing of its gross domestic product. On Monday, the finance ministry welcomed the announcement, but pointed out that the South African government and the private sector continued to play a role in the growth and development of Africa.

“In Nigeria's case, the wholesale and retail and the telecommunications sectors... have big participation by South African firms, who have played a big role in the growth and development of the two sectors.

“This is a positive story of African countries contributing to re-shaping each other's economies through increased investment,” it said.

The Democratic Alliance bemoaned the news that Nigeria had overtaken South Africa as the biggest economy.

“The (African National Congress) government’s failure to achieve South Africa’s growth potential over the past two decades has now led to our economy being overtaken as the largest economy in Africa,” DA MP Tim Harris said in a statement.

“We believe that South Africa has the potential to do much better. With decisive leadership, the right policy mix and a government committed to job creating inclusive economic growth, we can again challenge Nigeria for the number one spot.”

Harris said the DA had a plan which would help the country achieve its economic growth potential and take it back to the number one spot.

The plan included providing political leadership that would restore investor confidence, boosting trade to the rest of Africa and investing 10 percent of GDP in infrastructure. - Sapa