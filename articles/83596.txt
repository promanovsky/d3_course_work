The World Bank downgraded its growth outlook for the developing countries in East Asia, but expects almost steady growth this year compared to 2013, as recovery in high-income economies ramp up confidence.

According to the East Asia Pacific Economic Update released on Monday, developing East Asia will grow by 7.1 percent this year, down from the 7.2 percent estimated in October.

The growth is forecast to remain at 7.1 percent in 2015 and 2016 as well. The region's grew by 7.2 percent in 2013.

"The tailwinds from improving global trade will offset the headwinds from the tightening of global financial ," the lender said.

China's growth is forecast to slow marginally to 7.6 percent in 2014 and 7.5 percent in 2015, from its current pace of 7.7 percent each in 2012 and 2013. The estimate for 2014 was lowered from 7.7 percent.

Excluding China, the developing region will grow only 5 percent, slightly down from 5.2 percent in 2013, the World Bank estimated.

Flexible currencies will help East Asia to deal with external shocks, including potential capital-flow reversals. Moreover, most countries have adequate reserves to cover temporary trade and external shocks.

A slower-than-expected recovery in advanced economies, a rise in global interest rates, and increased volatility in commodity prices reminds that the region is still vulnerable to adverse global developments, said Bert Hofman, chief economist of the World Bank's East Asia and Pacific Region.

Hofman urged nations to continue structural reforms to improve underlying growth potential and enhance market confidence.

Indonesia's growth is expected to slow to 5.3 percent in 2014 from 5.8 percent last year. Likewise, Philippines is likely to grow 6.6 percent, down from 7.2 percent in 2013.

Meanwhile, Thailand is forecast to grow by 3 percent, up from 2.9 percent, and Malaysia's growth is expected to accelerate to 4.9 percent from 4.7 percent.

For comments and feedback contact: editorial@rttnews.com

Business News