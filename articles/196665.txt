Apple Inc is close to paying a record $3.2 billion for Beats Electronics, two people with knowledge of the matter said, an expensive foray into music streaming and headphone gear that would mark a departure for the usually cash-conservative iPhone maker.

Both companies are hashing out details and the envisioned deal could still fall through, one person told Reuters on condition of anonymity because the discussions were private.

A second source familiar with the matter told Reuters that Apple was in the market for a subscription-based music service to complement its "iRadio" ad-based offering, launched in 2013 as part of an attempt to jump into a music-streaming arena then split between a handful of startups such as Pandora Inc.

Founded by rapper Dr. Dre and legendary music producer Jimmy Iovine, Beats Electronics is best known for its "Beats by Dr Dre" line of trendy headphones that vie with the likes of Skullcandy Inc, Sennheiser Electronic and Bose Corp.

This year, it launched a music service that has won plaudits for its slick design and human music curation, versus the computer-algorithms that determine playlists for most of its rivals.

But analysts on Thursday questioned whether Beats, valued at just $1 billion during its last funding round in September, was worth that price.

Apple had more than $130 billion in cash as of the end of March, but the vast majority of that is parked abroad and investors have called on the company to return more cash in the form of dividends and buybacks.

Apple-watchers have speculated that the company that upended the music industry and today is the single largest seller of tunes was contemplating a Spotify-like on-demand music service to go with iRadio service and iTunes.

"This is really puzzling," said Forrester analyst James McQuivey, who said there was huge overlap between the two companies' customer base. "You buy companies today to get technologies that no one else or customers that no one has."

"They must have something hidden under the hood," he said.

In two of the largest deals this year, Facebook paid $19 billion for WhatsApp and its half-billion users, and it paid $2 billion for Oculus VR and its cutting-edge virtual reality headset.

Apple declined to comment on the report. Beats Electronics did not respond to requests for comment on the news, which was reported first by the Financial Times.

UNDER PRESSURE

Apple has not made a billion-dollar acquisition in at least a decade.

The company prefers to develop and design its products in-house, though it has tended to pay several hundred million dollars for small but important bits of technology to propel its core consumer electronics business, such as the acquisition of PA Semi in 2008 that led to the processor now found in all iPhones.

The company has been under pressure to try to revitalise growth as iPhone sales slow in a rapidly maturing market.

Critics have also accused the company of slowly "losing its cool" and innovative edge to new and upcoming technology companies, and missing the music-streaming bandwagon.

Technology giants Google and Amazon began jockeying for position in music last year, looking at ways to make streaming profitable and to develop a service seen as crucial to retaining users in an increasingly mobile environment.

For Google and Apple especially, the endeavour was critical to ensure users remain loyal to their mobile products.

They realized they had to stake out a place or risk ceding control of one of the largest components of mobile device usage. Analysts estimate roughly half of smartphone users listen to music on their device, making it the fourth most popular media-related activity after social networking, games and news.

Apple launched its own streaming music service last year, hoping to jump into the fast-expanding arena as growth of its iTunes service falters.

Apple's Chief Executive Tim Cooks met with Iovine, the Beats CEO, last year on a potential partnership involving Beats's planned music-streaming service, Reuters reported in March, citing sources.

Dre - who guided the careers of a string of rap artists such as Eminem and 50 Cent - compared his company with Apple in 2011. "Were trying to eventually be second to Apple. And I dont think thats a bad position," Dre told The Fader music website.

Beats Electronics received a $500 million investment from Carlyle Group in September that valued the company at over $1 billion.

It also bought back in September a 24.84 percent stake held by Taiwan smartphone maker HTC Corp, which once held as much as 50.1 percent of the company.