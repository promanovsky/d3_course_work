'I am an optimist, I am hopeful that within the period of today... there is a common ground that is found,' US Commerce Secretary Penny Pritzker, accompanying Secretary of State John Kerry during annual strategic talks with India, told NDTV.

New Delhi: The United States said on Thursday it was hopeful that differences between India and much of the rest of the world over a major trade agreement could be resolved in time, with only hours remaining before the deal has to be signed.

New Delhi has insisted that, in exchange for signing the trade facilitation agreement, it must see more progress on a parallel pact giving it more freedom to subsidise and stockpile food grains than is allowed by World Trade Organisation rules.

The WTO deal must be signed in Geneva on Thursday, and India's ultimatum has revived doubts about the future of the WTO as a negotiating body.

"I am an optimist, I am hopeful that within the period of today... there is a common ground that is found," US Commerce Secretary Penny Pritzker, accompanying Secretary of State John Kerry during annual strategic talks with India, told NDTV.

India's new nationalist government has demanded a halt to a globally agreed timetable on new customs rules and said a permanent agreement on food stockpiling and subsidies aimed at supporting the poor must be in place at the same time, well ahead of a 2017 target agreed last December in Bali.

Kerry warned India it stood to lose if it refused to budge.

"Right now India has a four-year window where it's been given a safe harbour where nothing happens," he told NDTV.

"If they don't sign up and be part of the agreement, they will lose that and then (they will) be out of line or out of the compliance with the WTO."

Pritzker said serious efforts were underway on Thursday to save the deal, which proponents say could add $1 trillion to the global economy and create 21 million jobs.

Deal without India?

As trade officials in Geneva tried to rescue the deal, India's Trade Minister Nirmala Sitharaman said New Delhi's position remained unchanged.

An Indian government source added separately that the Bali deal need not collapse even if the July 31 deadline is not met.

But several diplomats said New Delhi's stance could derail the whole process of world trade liberalisation, leading some WTO nations to discuss informally the last-ditch idea of excluding India from the agreement.

"If India does end up blocking (on Thursday) there is already a group of members who are interested in pursuing that path," a source involved in the discussions said.

"A dozen or so" of the WTO's 160 members had informally discussed pushing ahead with the trade facilitation agreement with less than 100 percent participation, the source said.

An Australian trade official with knowledge of the talks said a group of countries including the United States, European Union, Australia, Japan, Canada and Norway began discussing the possibility in Geneva on Wednesday afternoon.

A Japanese official familiar with the negotiations said Japan was still working on reaching a consensus, while a State Department official travelling with Kerry in India said the United States continued to talk with India on the deal.

A WTO spokesman said the group's director-general would hold meetings throughout the day to "avert a crisis.

"Delegations are showing real commitment to finding a solution and the director-general remains hopeful that a solution can be found," he said.

"Active Discussion"

Technical details would still have to be ironed out, but there was a "credible core group" that would be ready to start talking about a deal without India when WTO diplomats return from their summer break, the Australian official said.

To what extent the alternative proposal, and India's hardline position, were part of political brinkmanship was unclear. New Delhi's absence from any agreement would be a setback given its size and importance in global trade.

"What began as a murmur has become a much more active discussion in Geneva and I think that there are a lot of members in town right now that have reached the reluctant conclusion that that may be the only way to go," the official said.

Trade diplomats had previously said they were reluctant to consider the idea of the all-but-India option for the customs pact, partly because it would be hard to exclude one free rider and partly because the agreement in its current state requires an amendment to the existing WTO treaty, which appears to make India’s cooperation vital.

Others pointed out that many countries, including China and Brazil, have already notified the WTO of steps they plan to take to implement the customs accord immediately.

Other nations have begun bringing the rules into domestic law, and the WTO has set up a funding mechanism to assist.

One trade diplomat in Geneva described two worlds moving in parallel, with a few WTO members wrangling with India, and others moving ahead as if oblivious to India’s objections.

Reuters