The U.S. economy has seen unprecedented job growth this year and that will benefit both the economy and the unemployed if the trend continues. This past month's jobs report has shone a silver lining on future U.S. job growth.

The new report, released on Thursday since the federal government will be closed on Friday due to the Independence Day holiday, explains a greater than expected job growth in the economy and unemployment levels not seen since 2008. The June report published by the Labor Department said that 288,000 new jobs were added to the economy and unemployment dropped to 6.1 percent.

The beginning of 2014 did not see impressive job growth, with numbers remaining stagnant and, at times, dipping; however, many attributed this to the severe winter. Now with summer's arrival, the job numbers have improved. According to the New York Times, unemployment has not been this low since September 2008 and the economy has, overall, seen a continuous improvement since the beginning of 2013.

Though the recovery has been steady it has also moved at a snail's pace. Over the last five months the job growth has averaged 200,000 jobs and it was expected that June would be the same but it was the exception. The Times reports that the second quarter has seen growth rate over three percent, including a revision of the May jobs report that increased the numbers by 7,000 to 224,000.

According to Reuters, beating the 212,000-job forecast by more than 70,000 jobs has led to stock index futures trading slightly higher and for the U.S. dollar to gain against a number of foreign currencies.

The decrease in unemployment has also contributed to other stable figures. The number of Americans who are employed or looking for a job has held steady at 62.8 percent and the number of Americans with a job has risen since August 2009 to 59 percent.

However, Neil Irwin of the Times explains that we should not be as jovial over the jobs report since the increase could be attributed, in part, to the winter's constant snow fall that forced schools to be open later into June. He warns that the increase may disappear next month.