Extant on CBS: Premiere Date, Time; TV and Live Stream Info for New Halle Berry, Steven Spielberg Show

Extant is set to premiere on CBS on Wednesday, July 9.

The new science fiction show follows astronaut Molly Woods (Halle Berry) after she returns from a 13-month solo mission in outer space.

But as the trailer indicates, Woods had a secret encounter with some type of being while in the station. When she sees a doctor she finds out she’s pregnant.

The show features several other twists that are immediately evident (and likely others that are not), including Woods and her husband John having an android son.

The show will air on Wednesdays from 9 p.m. EDT through 10 p.m. Live streaming will be available through CBS.

The first season is slated to run for 13 episodes.

Besides Berry’s involvement, Steven Spielberg is an executive producer.

See descriptions for the first several episodes below.

Episode 1 — “Re-entry”

Astronaut Molly Woods tries to reconnect with her husband, John, and son, Ethan, after returning from a 13-month solo mission in outer space. Molly’s mystifying experiences in space lead to events that will ultimately change the course of human history.

Episode 2 — “Extinct”

After Molly collapses and experiences a vision of her encounter on the Seraphim [space station], Sam agrees to give her an ultrasound so she can learn more about her mysterious pregnancy. Also, Molly tracks down Kryger and he reveals the disturbing details of his own solo mission that the International Space Exploration Agency (ISEA) is keeping secret.

Episode 3 — “Wish You Were Here”

Molly has to decide if she is going to reveal her pregnancy to John after he confronts her about her distant behavior. Meanwhile, John and Molly face parents who are suspicious of Ethan when he goes to school for the first time.