CNET

SAN JOSE, Calif. -- Samsung on Monday kicked off its patent infringement suit against Apple, presenting evidence and testimony to argue the iPhone maker copied two of its patents.

The Korean electronics maker rested its defense against Apple shortly before 2 p.m. PT Monday before starting arguments for its own claims.

Samsung's first witness in its offense, Michael Freeman, testified about the creation of the '239 patent for video transmission. Freeman said he and his family invented the technology in the early 1990s and received a patent in 1994. They sold the patent and another to Samsung in October 2011 for $2.3 million.

Dan Schonfeld, a professor of computer science at the University of Illinois at Chicago, then testified that Apple infringed the '239 patent in its iPhone through the use of FaceTime and a feature for attaching video to messages and mail.

Ken Parulski, another expert who was part of the Kodak team that developed the world's first color digital camera, testified that Apple infringed another Samsung patent for organizing video and photos in folders.

"This is a fundamental invention that's widely used today," he said.

Almost two years after Apple and Samsung faced off in a messy patent dispute, the smartphone and tablet rivals have returned to the same San Jose, Calif., courtroom to argue once again over patents before Federal Judge Lucy Koh. Apple is arguing that Samsung infringed on five of its patents for the iPhone, its biggest moneymaker, and that Apple is due $2 billion for that infringement. Samsung wants about $7 million from Apple for infringing two of its software patents.

While the companies are asking for damages, the case is about more than money. What's really at stake is the market for mobile devices. Apple now gets two-thirds of its sales from the iPhone and iPad; South Korea-based Samsung is the world's largest maker of smartphones; and both want to keep dominating the market. So far, Apple is ahead when it comes to litigation in the US. Samsung has been ordered to pay the company about $930 million in damages.

Most Samsung features that Apple says infringe are items that are a part of Android, Google's mobile operating system that powers Samsung's devices. All patents except one, called "slide to unlock," are built into Android. Apple has argued the patent-infringement trial has nothing to do with Android. However, Samsung argues that Apple's suit is an " attack on Android" and that Google had invented certain features before Apple patented them.

In the case, Apple and Samsung have accused each other of copying features used in their popular smartphones and tablets, and the jury will have to decide who actually infringed and how much money is due. This trial involves different patents and newer devices than the ones disputed at trial in August 2012 and in a damages retrial in November 2013. For instance, the new trial involves the iPhone 5 , released in September 2012, and Samsung's Galaxy S3 , which also debuted in 2012.

The latest trial kicked off March 31 with jury selection. The following day featured opening arguments and testimony by Phil Schiller, Apple's head of marketing. Other witnesses who have testified include Greg Christie, an Apple engineer who invented the slide-to-unlock iPhone feature; Thomas Deniau, a France-based Apple engineer who helped develop the company's quick link technology; and Justin Denison, chief strategy officer of Samsung Telecommunications America. Denison's testimony came via a deposition video.

Apple experts who took the stand over the past few weeks included Andrew Cockburn, a professor of computer science and software engineering at the University of Canterbury, New Zealand; Todd Mowry, a professor of computer science at Carnegie Mellon University; and Alex Snoeren, a professor of computer science and engineering at the University of California at San Diego.

The crux of Apple's case came with two expert witnesses, John Hauser, the Kirin professor of marketing at the MIT Sloan School of Management; and Christopher Vellturo, an economist and principal at consultancy Quantitative Economic Solutions. Hauser conducted a conjoint study that determined Apple's patented features made Samsung's devices more appealing, while Vellturo determined the amount of damages Apple should be due for Samsung's infringement -- $2.191 billion.

Samsung, which launched its defense April 11 after Apple rested its case, called several Google engineers to the stand to testify about the early days of Android and technology they created before Apple received its patents. Hiroshi Lockheimer, Google vice president of engineering for Android, said his company never copied iPhone features for Android. Other Google Android engineers, Bjorn Bringert and Dianne Hackborn, also testified about features of the operating system.

High-ranking Samsung executives, including former Samsung Telecommunications America CEO Dale Sohn and STA Chief Marketing Officer Todd Pendleton, also took the stand during the weeks-long trial. The two executives testified about Samsung's marketing push for the Galaxy S2 and other devices, saying a shift in the Korean company's sales and marketing efforts -- not copying Apple -- boosted its position in the smartphone market.

The past several days of testimony have largely been experts hired by Samsung to dispute the validity of Apple's patents and to argue that Samsung didn't infringe. The experts include Martin Rinard, an MIT professor of computer science; Saul Greenberg, a professor of human computer interaction at the University of Calgary in Canada; Kevin Jeffay, professor of computer science at the University of North Carolina, Chapel Hill; and Daniel Wigdor, a computer science professor at the University of Toronto.

David Reibstein, chaired professor of marketing at the University of Pennsylvania's Wharton School of Business, on Friday refuted Apple expert Hauser's testimony from earlier this month. NYU Stern School of Business professor Tulin Erdem, meanwhile, on Friday also testified that she conducted her own studies, using eye tracking, to determine what devices consumers would buy. She concluded that Apple's patented features didn't boost desire for Samsung's products.

Earlier Monday, Judith Chevalier, a professor of economics and finance at the Yale University School of Management who was hired by Samsung, said her analysis determined that a reasonable royalty for Samsung's assumed infringement would be $1.75 per device, or $38.4 million overall. Apple had argued it deserved $40 per device for infringement as well as lost profits for a total of $2.191 billion.

There are seven patents at issue in the latest case -- five held by Apple and two by Samsung. Apple has accused Samsung of infringing US patents Nos. 5,946,647; 6,847,959; 7,761,414; 8,046,721; and 8,074,172. All relate to software features, such as "quick links" for '647, universal search for '959, background syncing for '414, slide-to-unlock for '721, and automatic word correction for '172. Overall, Apple argues that the patents enable ease of use and make a user interface more engaging.

Samsung, meanwhile, has accused Apple of infringing US patents Nos. 6,226,449 and 5,579,239. The '449 patent, which Samsung purchased from Hitachi, involves camera and folder organization functionality. The '239 patent, which Samsung also acquired, covers video transmission functionality and could have implications for Apple's use of FaceTime.

The Samsung gadgets that Apple says infringe are the Admire, Galaxy Nexus , Galaxy Note , Galaxy Note 2, Galaxy S2, Galaxy S2 Epic 4G Touch, Galaxy S2 Skyrocket, Galaxy S3, Galaxy Tab 2 10.1, and the Stratosphere. Samsung, meanwhile, says the iPhone 4 , iPhone 4S, iPhone 5, iPad 2 , iPad 3, iPad 4, iPad Mini, iPod Touch (fifth generation) and iPod Touch (fourth generation) all infringe.

The arguments by Apple and Samsung in the latest case should finish by the end of April. Court will be in session three days each week -- Mondays, Tuesdays, and Fridays -- though the jury will deliberate every business day until it has reached a verdict. Closing arguments likely will take place April 28.

As of the end of the day Monday, Apple had five hours and six minutes left to present its case and defend itself against Samsung's accusation. Samsung had one hour and two minutes left.

Updated at 4:35 p.m. PT with additional testimony details.