In his book, Geithner makes a strong case that much of what he—and the Bush administration before him—did was both terribly unfair and absolutely necessary to prevent a systemic collapse that would have crushed innocent people and led to a prolonged depression.

Did he and the rest of the Obama administration bail out Wall Street while not doing enough to bail out Main Street through more aggressive actions to assist struggling homeowners? Did he fail to push a stronger reform bill that would have truly cut down on the size and risk of the nation's largest financial institutions?

The former Treasury secretary has been on a media tour to promote his new book, "Stress Test: Reflections on Financial Crises," and the familiar arguments about his tenure have once again come to the fore.

"It was messy. It wasn't perfect. And it seemed deeply unfair, because paradoxically, in a panic, to rescue people from the risk of mass unemployment, you're going to be doing things that look like you're helping the arsonists," Geithner told Politico in a recent interview. "It is just inescapable. And it feels terrible. And we hated doing it. But the alternative would have been much more unfair to the innocent victims of the crises."

The problem, of course, is it's hard to test that thesis.

There really is no way to know for sure if the whole system would have collapsed if Geithner did some of the things his critics wanted him to do, such as invalidating AIG bonuses, or nationalizing Citigroup or Bank of America or firing more bank executives.

Read More Is the tea party over?



Geithner is absolutely certain that the plane, to use the metaphor he prefers, would have crashed with the entire U.S. public aboard if he hadn't helped land it with an overwhelming show of force to support the financial system without threatening any kind of punitive actions for bank shareholders or bondholders that might have caused the contagion to spread much further.

And he also makes a compelling case that even if you can't know for sure, why would you want to risk finding out?

Geithner, of course, will never convince his liberal critics who are as equally certain that he bought into Wall Street "bail us out or we will destroy the economy" rhetoric because he had spent so long only listening to the titans of finance.

Read MoreWhy these elections matter for the US Chamber of Commerce



None of this would matter so much beyond an interesting intellectual debate if it weren't so critical to our current political moment.

The fallout from the bailouts and the sluggish economy—which some blame on the Obama administration's failure to do more for average citizens—has both demoralized Democrats in ways that could depress voter turnout in this year's midterms and force potential 2016 Democratic presidential candidates to the left.

In Politico, I write about this phenomena, noting that Hillary Clinton, essentially the de facto Democratic nominee unless she decides not to run, is already sounding more like leftist economist Thomas Piketty in her speechmaking.

Last week, in a little-noticed but very important address to the New America Foundation, Clinton invoked the French scholar's work on economic inequality: "Economists have documented how the share of income and wealth going to those at the very top has risen sharply over the last generation," Clinton said. "Some are calling it a throwback to the Gilded Age of the Robber Barons."

—By Ben White. White is Politico's chief economic correspondent and a CNBC contributor. He also authors the daily tip sheet Politico Morning Money [politico.com/morningmoney]. Follow him on Twitter @morningmoneyben.