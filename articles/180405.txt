The European Central Bank has come under further pressure to head off a potentially destructive fall in prices across the eurozone after the Organisation for Economic Cooperation and Development (OECD) warned that without quick action the situation could worsen.

The Paris-based thinktank said in its twice-yearly global economic outlook that the eurozone remained mired in a period of low growth coupled with high unemployment and low inflation that threatened to derail the recovery.

Its acting chief economist, Rintaro Tamaki, urged the ECB to end months of procrastination with policies to boost demand that could include mimicking the US Federal Reserve and Bank of England, which have printed billions of dollars and pounds to make it easier for households and businesses to borrow money.

He said: "Given persisting downside risks, high unemployment, below-target inflation and high levels of government debt, monetary policies need to remain accommodative in the main OECD areas.

"In particular, we call on the ECB to take new policy actions to move inflation more decisively towards target and to be ready for additional non-conventional stimulus if inflation were to show no clear sign of returning there." The warning will prove embarrassing for the ECB chief, Mario Draghi, and the rest of the central bank's governing council, which have come under sustained criticism for allowing inflation to fall in recent months to as low as 0.5% without easing credit conditions.

Economists fear that falling prices will convince shoppers to delay major purchases, forcing employers to cut back production and scrap planned wage rises. Without a steady, controlled rise in prices, there is little incentive for consumers to bring forward purchases and speed up economic growth.

The OECD, which counts the world's richest nations among its members, said the recovery was strengthening in 2014 but at a slightly slower pace than predicted last year. The UK and US recoveries remain strong, it said, but the eurozone has slipped back.

This year, growth among OECD members is predicted to be 2.2% compared with last year's forecast of 2.3%, though next year growth will expand at a faster pace of 2.8% compared with the previous forecast of 2.7%.

The thinktank praised the UK's growth rate, which it expects to reach 3.2% this year, above the rate predicted by the International Monetary Fund and the UK government's independent forecaster, the Office for Budget Responsibility.

Moves to increase interest rates are expected to go ahead next year in both the UK and US, following a sustained period of economic growth.

In the UK, the OECD said higher interest rates should go ahead despite further cuts in government spending scheduled for 2015 amounting to 1% of total public spending.

However, it said George Osborne needed to restrain the housing market to prevent a bubble growing that could wreck the recovery.

It said: "With the unemployment rate having fallen below 7%, the Bank of England has shifted towards a more flexible form of forward guidance. Policy interest rates are expected to begin to rise in 2015 as economic slack narrows and inflation pressures gradually build up.

"Further prudential regulation measures should be considered to ensure a balanced housing market recovery. Fiscal consolidation of about 1% of GDP should be implemented in 2015, as planned, to strengthen public finance sustainability," it said.