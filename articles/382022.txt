Over the course of more than six decades, the Nobel-winning South Africa author Nadine Gordimer, who died on Sunday at 90, wrote more than a dozen novels and many more short stories. It’s a daunting oeuvre, throughout which she often returned to themes related to apartheid. For those daunted by her extensive bibliography, here’s where to start:

Face to Face

Year: 1949

Gordimer’s first novel was still a few years away, but Face to Face — a collection of short stories — was the young author’s first book.

Telling Times : Writing and Living, 1954-2008

Year: 2010

Telling Times wasn’t Gordimer’s last book (in 2012, her novel No Time Like the Present was published) but it’s the place to look for Gordimer’s nonfiction. The compendium of a half-century of work ranges from autobiography and travelogue to reflections on South African history and the great leaders of her time. The New York Times review of the book said that, even though a collection so vast is bound to have ups and downs, the work “reveals the power of ‘engagement,’ in the broad and humane sense.”

Burger’s Daughter

Year: 1979

One of her best-known works, Burger’s Daughter concerns the life of a young daughter of South African anti-apartheid activists and how politics affects the personal. Along with A World of Strangers and The Late Bourgeois World, it’s one of the three Gordimer works banned by the South African government; Burger’s Daughter was the subject of a 1980 book about that nation’s censorship practices. Gordimer later said that she wasn’t surprised the book was banned, but that “if you are a writer you must write what you see.”

The Conservationist:

Year: 1974

Gordimer won the Booker Prize — one of literature’s most prestigious — for this novel, about a rich South African man who buys a farm in order to find meaning in his life. The work was later shortlisted for the extra-prestigious “Best of the Booker” prize.

Loot

Year: 2003 for the collection of the same name; the story is copyright 1999.

For those who want to read her work right away, this is the way to go: the short story “Loot” is available for free on the Nobel website.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Write to Lily Rothman at lily.rothman@time.com.