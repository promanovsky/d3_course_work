Celebrity

The Coldplay frontman, who recently announced separation from Gwyneth Paltrow, attends Rock and Roll Hall of Fame induction ceremony in Brooklyn to honor Peter Gabriel.

Apr 11, 2014

AceShowbiz - Chris Martin was spotted without his wedding ring when he made his first public appearance since announcing split from Gwyneth Paltrow. The Coldplay member attended the Rock and Roll Hall of Fame induction ceremony to honor Peter Gabriel on Thursday, April 10 in Brooklyn.

"I couldn't be more thrilled to be here," Martin said, as quoted by PEOPLE. "Why do Peter Gabriel fans like myself love his music? Sometimes the songs are reassuring, like 'Don't Give Up'." He then teamed up with Gabriel to perform the honoree's song "Washing of the Water".

Back on Sunday, April 6, Martin and Paltrow attended Robert Downey Jr.'s birthday party in his home in Los Angeles. The couple was joined by their children Apple and Moses at the bash to celebrate the "Iron Man" star's 49th birthday.

"They were lovey-dovey and playful. They were with the kids, they held hands and laughed and joked as a family," a source said of Martin and Paltrow. "It was not the typical picture of a divorce. If the reports had not come out, you would think, 'Wow, they are a perfect couple.' Gwyneth was in such good spirits. She had her hair pulled back in a ponytail and she looked amazing."