Let's take a quick look at four stocks -- Zogenix (NASDAQ:ZGNX), Philips (NYSE:PHG), Siemens (OTC:SIEGY), and TherapeuticsMD (NASDAQ:TXMD) -- which could all loom large in health care headlines this Tuesday morning.

Zogenix drops on an FDA priority review for Purdue's rival painkiller

Shares of Zogenix, the maker of the painkiller Zohydro ER, are down nearly 9% in pre-market trading this morning after Purdue Pharma announced that its competing tamper-resistant pill had received a priority review from the FDA, indicating that it could be approved within six months.

The active ingredient in Zohydro and Purdue's new drug is hydrocodone, a chronic pain medication which is estimated to be five to ten times as potent as Vicodin -- a well-known painkiller which consists of a lower dose of hydrocodone and acetaminophen. Tamper-resistant pills cannot be crushed or dissolved for recreational drug use.

The main issue with Zohydro is its potential for abuse, since it is not tamper-resistant. That risk has caused 29 state governments to demand that the FDA reconsider its approval of the drug, citing the fact that a similar opioid painkiller, oxycodone, was previously implicated in many painkiller-related deaths.

Zogenix recently announced plans to submit an sNDA (supplemental new drug application) for a next-generation of Zohydro which could be more tamper resistant by October 2014. If approved, it could launch the new version by early 2015. Zogenix is also working on a new formulation of Zohydro, through a collaboration with Atlus Formulation, which would maintain the extended-release properties of Zohydro when crushed to further limit its potential for abuse. Zogenix intends to submit an NDA (new drug application) for the new formulation in the first half of 2016.

Looking ahead, Purdue isn't Zogenix's only problem. Teva Pharmaceutical and Pfizer are both testing tamper-resistant versions of hydrocodone in phase 3 trials. Teva intends to submit its drug to the FDA for approval by the end of the year.

Philips health care CEO steps down

Philips' health care division CEO Deborah DiSanzo has stepped down, following the unit's weak second quarter performance. The segment, which accounted for 41% of Philips' top line in 2013, will now report directly to CEO Frans van Houten.

Philips warned that its health care division will post weaker-than-expected second quarter earnings, although the company's overall warnings will remain in line with previous forecasts. During the fourth quarter of 2013, sales at the health care segment fell 3% year-over-year on a nominal basis. Last quarter, sales fell 8%. The business sells a wide array of health care related products to consumers and hospitals, including patient monitoring and medical imaging devices.

Siemens considers selling its hospital IT business

Meanwhile, Siemens is reportedly considering the sales of its hospital IT business, which could be worth more than $1.4 billion. Siemens' main competitors in health care IT -- Cerner, General Electric, and Hewlett-Packard -- could be the potential suitors to keep an eye on, although no formal announcements have been made.

Siemens has divested €2.3 billion ($3.1 billion) worth of assets since late 2012, and divested eight businesses so far in 2014. It has also been exploring the sale of its microbiology unit, which could be worth more than $300 million. Selling both units could indicate that Siemens is ready to exit the health care business for good.

Like Philips, Siemens' health care segment hasn't posted impressive growth. It posted a 1% year-over-year decline in revenue last quarter.

TherapeuticsMD surges on a big upgrade

Last but not least, shares of TherapeuticsMD are up nearly 25% this morning after FBR Capital issued an outperform rating on the stock with a price target of $34, representing a whopping 750% premium from yesterday's closing price.

TherapeuticsMD's four lead product candidates are hormone replacement therapies for women. Two treatments -- TX-001HR and TX-002HR -- have advanced into phase 3 trials. According to the company, TX-001HR and TX-002HR have U.S. sales potential of $2.06 billion and $364 million, respectively. It also believes that TX-004HR, which is in phase 2 trials, has sales potential of $1.09 billion.

TherapeuticsMD has no marketed products, and is expected to only post revenues of $3.1 million next quarter. The stock has climbed 58% over the past 12 months.