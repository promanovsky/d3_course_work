

Left: A magnified human skin-tissue sample from the site of a smallpox lesion. Right: A 1975 electron micrograph of the smallpox virus. (Centers for Disease Control and Prevention/via AP)

A government scientist cleaning out a storage room last week at a lab on the National Institutes of Health’s Bethesda campus found decades-old vials of smallpox, the second incident involving the mishandling of a highly dangerous pathogen by a federal health agency in a month.

The vials, which appear to date from the 1950s, were flown Monday by government plane to the Centers for Disease Control and Prevention headquarters in Atlanta, officials said Tuesday. Initial testing confirmed the presence of ­smallpox-virus DNA. Further testing, which could take up to two weeks, will determine whether the material is live. The samples will be destroyed after the testing is completed.

There is no evidence that any of the vials had been breached or that workers in the lab, which has been used by the Food and Drug Administration for decades, were exposed to infection. FDA employees did not receive an official communication about the discovery until Tuesday evening. One scientist, who works in the building and spoke on the condition of anonymity for fear of retaliation, said he learned about it Tuesday when his supervisor read a media report.

The FBI and the CDC’s division of select agents and toxins are investigating.

“Due to the potential bio-safety and bio-security issues involved, the FBI worked with CDC and NIH to ensure safe packaging and secure transport of the materials,” FBI spokesman Christopher M. Allen said in an e-mail to The Washington Post.





This is the first time the deadly virus has been discovered outside the only two facilities in the world where smallpox samples are allowed, by international agreement, to be stored — a highly secure lab at the CDC headquarters in Atlanta and a virology and biotechnology research center in Novosibirsk, Russia.

Smallpox — which vanished from the United States just after World War II and was eradicated globally — killed hundreds of millions of people in the 20th century alone.

“It was considered one of the worst things that could happen to a community to have a smallpox outbreak,” said Michael Osterholm, a expert on bioterrorism and director of the Center for Infectious Diseases Research and Policy at the University of Minnesota. “It’s a disease that’s had a major impact on human history.”

There is no cure for smallpox, and historically about one-third of people who contract it die from the disease. Though not as readily contagious as some other diseases, such as influenza, smallpox promises plenty of misery once contracted. Symptoms include high fever, fatigue and fluid-filled lesions that often ooze and crust over, leaving survivors irreversibly scarred.

Last month, a safety lapse involving three CDC labs in Atlanta led to the accidental release of live anthrax bacteria. The incident resulted in as many as 84 employees having to get a vaccine or take antibiotics as a precaution and the reassignment of a lab director. Scientists failed to take proper precautions to inactivate bacteria samples before transferring them to other labs not equipped to handle live anthrax.

It is not entirely clear how the smallpox samples ended up in Building 29A on the NIH campus in Maryland. The building is an FDA lab, one of several the agency has operated on the NIH campus since 1972. The vials were discovered while employees were preparing for the lab’s move to the FDA’s main campus in White Oak, Md.

An FDA scientist on July 1 found several cardboard boxes containing dozens of glass vials, each several inches long and sealed with melted glass, according to CDC spokesman Tom Skinner. The boxes were in a storage room kept at about 40 degrees. Several vials were labeled as flu virus, mumps or typhus, he said. Sixteen vials were labeled “variola,” or smallpox, or were suspected of containing smallpox virus. Those vials were in a box with cotton padding. All the vials were immediately secured in a containment laboratory on the NIH campus. The 16 suspect vials were flown to Atlanta. Testing confirmed the presence of smallpox virus DNA in six.

“This was a lab that didn’t realize it had these vials,” Skinner said. Because the vials are made of glass and were sealed with melted glass, officials say the vials appear to date to the 1950s. He said the material could have been sitting around in the storage room “unbeknownst to the people up there for many years.”

In an e-mail late Tuesday, an FDA spokeswoman said the laboratories where the smallpox was discovered are used for biologics research, including research on vaccines. The agency noted that the regulation of vaccines, including the smallpox vaccine, was under the authority of NIH until July 1972, when it transferred to the FDA.

“Although an investigation is ongoing, it is likely that an investigator involved in smallpox research, prior to the time that smallpox was eliminated, may have left the boxes in a cold storage area,” FDA spokeswoman Jennifer Rodriguez said in the e-mail. “These were then only discovered as part of the inventory undertaken in preparation for the move of the FDA laboratories from the NIH campus in Bethesda to the consolidated FDA campus in Silver Spring.”

Rodriguez said the agency has completed an inventory of all common storage areas in its NIH campus buildings and found no other materials of public health concern.

“We are carefully examining our policies and procedures regarding the security of our laboratories and storage of biologic specimens,” she said. “We will ensure the implementation of a corrective action plan to ensure that our biological specimens are inventoried and properly secured.”

About 18,000 people work on the sprawling NIH campus in Bethesda. An NIH spokeswoman said the agency is planning a comprehensive search of all laboratory spaces. Officials did not notify employees about the discovery, she said, because the vials were checked and found to have no breaches. FDA laboratory staffers uninvolved in the discovery were notified by e-mail Tuesday evening, after the vials had been transferred to the CDC and confirmed to contain smallpox, Rodriguez said.

The CDC notified the World Health Organization. A spokeswoman for the WHO said any samples of the smallpox virus found outside the two facilities where it is allowed must be moved to those locations or destroyed.

Osterholm, the expert on bio­terrorism, likened the discovery to finding a long-forgotten trunk in an attic and said that biologists are no different from other people, collecting things and storing them. He said government officials handled the discovery appropriately and acted quickly and cautiously. “I’m not convinced this will be the last of these potential situations,” he said. “I wouldn’t be surprised if somewhere else in the world this same type of thing happens again.”

An accidental release of the virus potentially could sicken a small number of people who come into contact with it, though Osterholm said such an outbreak probably could be contained rapidly given today’s vaccine supplies and antiviral drugs. The more worrisome prospect, he said, would be if someone with bad intentions were able to aerosolize the virus and spread it over a large metropolitan area. “That could be a global crisis,” he said.

When smallpox was officially declared eradicated in late 1979, an agreement was reached under which any remaining stocks of the virus would be destroyed or sent to one of two secure laboratories — the CDC in Atlanta or the State Research Center of Virology and Biotechnology in Russia.

In the decades since, the scientific community has wrestled over whether to destroy the remaining stockpiles of the smallpox virus or hang on to them in case they are needed for research.

Those who argue in favor of destroying the remaining smallpox samples — a group that includes D.A. Henderson, who led a worldwide effort to eradicate the disease decades ago — point out that an effective vaccine exists and that maintaining live samples risks accidental infections or, worse, vials falling into the hands of terrorists. Other scientists, including officials at the CDC and NIH, have insisted that there is more valuable research to be done before scientists can say confidently that adequate protections exist against any future smallpox threats.

This spring at a meeting in Geneva, the World Health Assembly, the WHO’s decision-making body, revisited the question over whether to destroy the remaining stockpiles of smallpox. Amid sharply divided opinions on the issue, the group postponed a final decision.

Sari Horwitz contributed to this report.