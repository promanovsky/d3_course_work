The disappearance of Malaysia Airlines Flight MH370 was not a priority on the night the plane vanished, the company's commercial chief has admitted.

Hugh Dunleavy, a former Ministry of Defence employee who became the airline's director of commercial operations in 2012, said requests to initiate a search for the Boeing 777 were ignored by international air traffic controllers on 8 March, when the plane disappeared with 239 passengers on board.

"We were calling, but they've got other planes in the air; they're saying 'your plane never entered my air space, so technically I don't have to worry about it at the moment'. They're not dropping everything to answer us," Dunleavy told the Evening Standard.

He added that the search for the missing Malaysian plane could take "decades" as he believed "something untoward" had happened to the aircraft.

"I think it made a turn to come back, then a sequence of events overtook it, and it was unable to return to base," Dunleavy said.

"I believe it's somewhere in the south Indian Ocean. But when a plane hits the ocean it's like hitting concrete. The wreckage could be spread over a big area. And there are mountains and canyons in that ocean. I think it could take a really long time to find. We're talking decades."

Dunleavy, who is originally from Ealing, defended his company's initial response to MH370's disappearance.

"People say, 'why didn't you work quicker?' But you're calling pilots, explaining the situation, waiting for them to send out pings, doing the same to the next plane, then the next, and it's four in the morning, you don't have 50 people in the office, only a couple.

"An hour goes by frighteningly quickly — you realise that the missing plane is now another 600 miles somewhere else."

Criticising the Malaysian government for delaying the revelation that MH370 had turned back towards the Strait of Malacca, Dunleavy said Malaysia Airlines only received search updates after they had been reported by the media.

"I only heard about this through the news," he said. "I'm thinking, really? You couldn't have told us that directly?

"Malaysia's air traffic control and military radar are in the same freakin' building. The military saw an aircraft turn and did nothing.

"They didn't know it was MH370, their radar just identifies flying objects, yet a plane had gone down and the information about something in the sky turning around didn't get released by the authorities until after a week. Why? I don't know. I really wish I did."