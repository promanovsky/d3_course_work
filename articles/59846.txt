‘Smoking ban good for child health’

Share this article: Share Tweet Share Share Share Email Share

London - Banning smoking in public places has helped to cut premature births by 10 percent, according to new research from the United States and Europe. A study in The Lancet medical journal found that while the impact of anti-smoking laws varies between countries, the overall effect on child health around the world is positive. “Our research shows that smoking bans are an effective way to protect the health of our children,” said Jasper Been of the University of Edinburgh's Centre for Population Health Sciences, who led the study. He said the findings should help to accelerate the introduction of anti-smoking legislation in cities, countries and districts which have yet to do so. Laws banning smoking in public places such as bars, restaurants, offices and other workplaces have already been proven in previous studies to protect adults from the health threats associated with passive smoking.

According to the World Health Organisation (WHO), tobacco already kills around 6 million people a year worldwide, including more than 600 000 non-smokers who die from exposure to second-hand smoke. By 2030, if current trends continue, it predicts tobacco's death toll could be eight million people a year.

Only 16 percent of the world's population is covered by comprehensive smoke-free laws, and 40 percent children worldwide are regularly exposed to second-hand smoke, the WHO says.

Public health experts hope that as more and more countries in Europe and around the world adopt stricter legislation on smoking in public places, the health benefits will swiftly start to become evident.

Friday's research in The Lancet, which analysed data on more than 2.5 million births and almost 250 000 hospital attendances for asthma attacks, was the first comprehensive study to look at how anti-smoking laws affect children's health.

With results from five North American studies of local bans and six European studies on national bans, it found rates of both pre-term births and hospital attendance for asthma fell by 10 percent within a year of smoke-free laws coming into effect.

“Together with the known health benefits in adults, our study provides clear evidence that smoking bans have considerable public health benefits for perinatal and child health,” said Been.

He said it also provided “strong support for WHO recommendations to create smoke-free public environments on a national level.” - Reuters