Yahoo (YHOO) shares rose in after-hours trading after the company announced a lift in advertising.

Although its first-quarter earnings were flat, the Internet company said Tuesday that revenue from display ads was up 2 percent from the year-ago period. Search revenue jumped 9 percent, as Yahoo topped analyst expectations.

After a prolonged period of decline, investors took the news as a good sign that the company was again attracting advertisers. The stock was up as much as 9 percent after the close of trade.

"I am really pleased by our first-quarter performance, marking our best Q1 revenue... since 2010," CEO Marissa Mayer said in a statement. Calling the first quarter "an early and important sign of growth in our core business," she also mentioned more than 430 million mobile users of the company's services.

The question now is whether Yahoo can build on its latest results in the months to come.

For now, investors can take comfort in the uptick in display ads. Although at $409 million the category brought in less than search, it does represent what should be one of Yahoo's big potential growth areas. Display revenue declined by 11 percent in the first two quarters of 2013, 7 percent in the third, and 6 percent in the fourth. A two percent increase last quarter was small by one measure, but the change from last year is significant.

The news isn't all good, however. Factoring in traffic acquisition costs, Yahoo revenues for the period fell 1 percent. The underlying costs of operating the advertising business may be rising, which could ultimately put pressure on profits.

Whatever greater interest advertisers may be showing, in other words, it is unlikely to make a huge change in the first half of this year. Yahoo estimates that revenue in the current quarter excluding acquisition costs will come in at between $1.06 billion and $1.1 billion.