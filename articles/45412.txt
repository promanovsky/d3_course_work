When Facebook announced it was buying Oculus VR, not everyone was happy — least of all, one of its key developers. Minecraft creator Markus "Notch" Persson said he would no longer be bringing his hit game to the virtual-reality platform.

We were in talks about maybe bringing a version of Minecraft to Oculus. I just cancelled that deal. Facebook creeps me out. — Markus Persson (@notch) March 25, 2014

When Notch speaks, the gaming segment of the Internet listens. Minecraft is one of the most influential independent games of the last five years; its unique voxel style has been embraced by all ages, and it has sold 14 million copies on PC, Xbox, PlayStation and mobile.

He went on to describe visiting Oculus headquarters in his blog, saying he was originally incredibly excited about the design challenges of virtual reality.

"As someone who always felt like they were born five or ten years too late, I felt like we were on the cusp of a new paradigm that I might be able to play around with," Perrson wrote. "I could be part of the early efforts to work out best practices, and while I have no doubt that in ten years we’ll look back at the problems with early VR applications in the same we look back at GUI problems with early PC games, it still felt exciting to me."

But Persson's excitement changed after reading the news on Tuesday.

Facebook is not a company of grass-roots tech enthusiasts. Facebook is not a game tech company. Facebook has a history of caring about building user numbers, and nothing but building user numbers. People have made games for Facebook platforms before, and while it worked great for a while, they were stuck in a very unfortunate position when Facebook eventually changed the platform to better fit the social experience they were trying to build. Don’t get me wrong, VR is not bad for social. In fact, I think social could become one of the biggest applications of VR. Being able to sit in a virtual living room and see your friend’s avatar? Business meetings? Virtual cinemas where you feel like you’re actually watching the movie with your friend who is seven time zones away? But I don’t want to work with social, I want to work with games.

Persson didn't seem to rule out developing for other company's platforms, such as Sony's recently announced Project Morpheus, or whatever potential project Microsoft could be working on.

"I definitely want to be a part of VR, but I will not work with Facebook," he said. "Their motives are too unclear and shifting, and they haven’t historically been a stable platform. There’s nothing about their history that makes me trust them, and that makes them seem creepy to me. And I did not chip in ten grand to seed a first investment round to build value for a Facebook acquisition" — that being the money he donated to Oculus VR's Kickstarter campaign.

That campaign raised nearly $2.5 million in 2012.

"I have the greatest respect for the talented engineers and developers at Oculus," Perrson concluded. "It’s been a long time since I met a more dedicated and talented group of people. I understand this is purely a business deal, and I’d like to congratulate both Facebook and the Oculus owners. But this is where we part ways."

If you're not aware, there's a mod that does it: https://t.co/qD9bCeyaeD — Markus Persson (@notch) March 25, 2014

We've reached out to Persson and Mojang for additional comment.