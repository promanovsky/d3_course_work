Hayden Panettiere Pregnant: Actress Reportedly Expecting First Child With Fiancé Wladimir Klitschko Hayden Panettiere Pregnant: Actress Reportedly Expecting First Child With Fiancé Wladimir Klitschko

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

Hayden Panettiere and her fiancé Wladimir Klitschko are expecting their first child, according to reports this week.

The "Nashville" actress and her Ukranian boxing husband-to-be have been in an on-and-off relationship since 2009. However, the pair are now preparing for parenthood together.

"Hayden is totally pregnant," a source close to the actress told Us Weekly.

The 24-year-old starlet announced her engagement to Klitschko, 38, in May of last year. Even more recently, Panettiere opened up about their future plans together, including where they will put down roots and their wedding ideas.

"He fits in anywhere," Panettiere told People of her 6'6" fiancé earlier this year. "He's a very worldy man!"

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

The actress went on to reveal her adoration for the city of Nashville -- the setting of the hit television series she stars in -- but admitted that wedding planning is not easy with the couple's career demands.

"With both of our schedules, it's crazy trying to schedule anything, much less a wedding," Panettiere noted.

However, the starlet has envisioned her bridal look, complete with a gown, as well as her wedding plans.

"I can picture it," she said before shedding light on her cautious personality. "There are so many ways to get married and we'll figure one out that is comfortable for both of us."

"I'm very shy in some ways," she noted. "Obviously I'm in the spotlight professionally, but to be that vulnerable, standing up in front of people and doing something so personal, that's kind of a new thing for me and not exactly in my comfort zone."

During a different interview, Panettiere admitted her desire to start a family.

"I've lived a very big life, and I don't feel my age, and I feel like I was born to me a mother," she told Us Weekly. "Sometimes people speak about [having kids] like, 'Your life ends- you're never going to be able to do anything again!' And I'm like, 'What are you talking about?' Motherhood is the most beautiful, exciting thing, and there's nothing that I feel like I can't accomplish while having children in my life."

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit