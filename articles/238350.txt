Sales of existing homes rebounded slightly in April, but the pace of buying remained below last year’s level. (J Pat Carter/AP)

Sales of existing U.S. homes rebounded slightly in April, but the pace of buying remained below last year’s level.

The National Association of Realtors said Thursday that sales rose 1.3 percent from March to a seasonally adjusted annual rate of 4.65 million. Purchases of homes in the past 12 months have dropped 6.8 percent.

Much of the gains were concentrated in the volatile condominium market, which experienced growth of 7.3 percent. Sales of single-family homes were up only 0.5 percent last month.

Nearly five years into the recovery from the recession, real estate sales have yet to return to their historic averages. The solid gains made through the middle of 2013 have evaporated, while demand continues to be strong for the most expensive properties and faltering for starter homes and those priced for ­middle-class buyers.

Home buying continues to run significantly below its 2013 pace, when 5.1 million existing homes were bought. That is well below the 5.5 million that is consistent with a healthy housing market.

Snowstorms and cold weather delayed sales in the Midwest and Northeast during the first two months of 2014. Would-be buyers are also wrestling with higher prices and rising interest rates in the past 12 months, causing the real estate market to lose some of its momentum from last year.

But in a positive sign, the April report shows that would-be sellers are shaking off the nasty winter and listing their homes. The market has a 5.9-month supply of homes, up from 5.2 months a year ago.

“With the warmer weather and the ‘promise’ of higher ­prices, more homeowners put their homes on the market,” said Jennifer Lee, a senior economist at BMO Capital Markets.

Prices continue to increase, yet the limited demand appears to have eased the pace of growth. Median prices rose 5.2 percent to $201,700 in April, the slowest growth rate since March 2012, the NAR said.

Buying picked up last month in the West and South, gains that were offset slightly by a decline in the Midwest and flat sales in the Northeast.

The upper tier of the market remains healthier than the low end. Sales continued to fall last month for homes priced below $250,000, while rising for homes sold for more than $750,000. First-time buyers — who tend to purchase lower priced properties — represented 29 percent of all sales, much less than the historical average of 40 percent.

Average U.S. rates on fixed mortgages fell this week for a fourth straight week. Mortgage buyer Freddie Mac said Thursday that the average rate for a 30-year loan declined to 4.14 percent from 4.20 percent last week. The average for the 15-year mortgage eased to 3.25 percent from 3.29 percent.