Online storage firm Box Inc. has filed an Initial Public Offering (IPO) to raise $250 million (£151 million, AU$273 million) as part of an effort to rival Google Drive, OneDrive, and Dropbox.

The company, which launched in 2005, has applied for listing on the New York Stock Exchange with the ticker symbol BOX, according to the Wall Street Journal.

Financial services firms Morgan Stanley, Credit Suisse, and JP Morgan are set to lead its IPO charge. BMO Capital Markets, Canaccord Genuity, Pacific Crest Securities, Raymond James, and Wells Fargo Securities were selected as underwriters.

A box of cash

The company made a loss of $168.6 million (£102.1 million, AU$184.3 million) for the fiscal year ending in January, a substantial increase over the $112.6 million (£68.2 million, AU$123 million) loss it made the year before. Most of this is due to big increases in its sales and marketing costs.

Despite the losses, Box has significant potential, with revenue more than doubling to $124 million (£75 million, AU$135 million) over the last year, and an aggressive expansion policy to turn free users into paying ones. The company has 25 million registered users and was recently valued at $2.2 billion (£1.3 billion, AU$2.4 billion).

"We are moving toward an information economy, where every worker will be an information worker, and every business, regardless of industry, will be in the information business," Aaron Levie, CEO of Box, wrote in a letter to investors. "Our role at Box is to help enable this transition for every organization in the world."