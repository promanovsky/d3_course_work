South Nassau Communities Hospital said Wednesday it has been flooded with phone calls since reports Tuesday that the facility has sent letters to more than 4,200 patients recommending that they be tested for hepatitis B and C and HIV because of the risk of infection from an insulin pen.

"We've been inundated with calls, as you might expect," hospital spokesman Damian Becker said.

Becker said that as of 2:35 p.m. Wednesday the hospital had booked 115 appointments and returned an additional 70 calls -- and had more to return.

Becker apologized if people had been put on hold and encouraged them to keep trying. "We're pounding away at it as best we can," he said. "We take this very seriously."

South Nassau is offering free and confidential testing for those notified. To arrange a test, call 516-208-0029.

In the Feb. 22 letter, the Oceanside hospital said the patients may have received insulin from an insulin pen cartridge -- not the pen's needle, which is used only once -- that could have been used on more than one patient.

Sign up for coronavirus updates Get the latest on the fast-moving developments on the coronavirus and its impact on Long Island. By clicking Sign up, you agree to our privacy policy.

A note to our community: As a public service, this article is available for all. Newsday readers support our strong local journalism by subscribing. Please show you value this important work by becoming a subscriber now. SUBSCRIBE Cancel anytime

Those diabetics potentially exposed were patients between March 2011 and January 2014, Becker said.

Patients who may have been exposed within the past six months may need to be retested if they test negative, Becker said. "This is because it may take up to six months for the test to become positive after an exposure, and we are recommending this out of an abundance of caution," he said.

An insulin pen is a pre-filled syringe that can be used multiple times -- but on only one patient. Because of potential backflow of a patient's blood into the pen cartridge after injection, using a pen on more than one patient could expose them to blood-borne infections.

Becker said no one was observed reusing the insulin pen cartridge on more than one patient, but a nurse was heard saying it was all right to do so. That triggered a report to the state Department of Health.

George Hebert, 69, of Freeport, said he got a letter Wednesday from the hospital. A diabetic, he said he went to South Nassau after he broke his arm Dec. 30 and was released Jan. 4.

Frantic about possibly passing a disease on to his wife, who helps him test his blood twice daily, he said it took four phone calls Wednesday before he got through to the hospital. "And I still don't have an appointment," he said. "I will be worried until I know I'm not going to infect my wife."

Evelyn McKnight of Fremont, Neb., a national advocate for safe needle use, said New York has been a leader in proper needle use, and the problem is with health care providers.

"It behooves the providers to follow the guidelines," said McKnight, who founded HONOReform after she got hepatitis C in 2004 at a cancer clinic.