Yelp Inc. swung to a profit in the second-quarter, switching to the black side of the ledger for the fist time since its market debut in March 2012.

The San Francisco business-review website again raised its revenue projection for the year, saying it now expects $372 million to $375 million, up from its earlier projection of $363 million to $367 million.