2014 may be remembered as the Year of the Recall. GM's "Switchgate" fiasco kicked things off, affecting millions of vehicles in the U.S. and abroad. Other automakers followed suit -- some due to an abundance of caution, others because improved in-house tests revealed flaws faster, and others thanks to the usual pile-up of consumer complaints.

The recalls are affecting everyone -- even companies like Suzuki, which stopped selling vehicles in the U.S. nearly two years ago. According to the Associated Press, Suzuki is now recalling over 19,000 Kizashi sedans from the 2010-2013 model years. The problem? Spiders.

Suzuki has received at least seven complaints about spider webs clogging the Kisashi's fuel vapor vent hose. That's led to reduced air flow, which can deform the Kizashi's gas tank over time. If the deformities are severe enough, they can cause the tank to crack, resulting in fuel leaks -- and where there's a fuel leak, there's almost always an increased risk of fire.

Sound familiar? It should: in 2014, at least two automakers announced some unusual problems having to do with spiders. Ford issued a technical service bulletin, advising mechanics to be on the lookout for spider webs in vent hoses, and the arachnid problem was so bad on the Mazda Mazda6, it was recalled for a second time because spiders were blocking the sedan's fuel line. A couple of years ago, Honda and Hyundai owners experienced similar problems.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Suzuki says that it will notify owners of the Kizashi recall before the end of the month. Its service centers will upgrade the vent line in affected vehicles, replacing it with one equipped with a filter. Technicians will also inspect the car's gas tank and replace it, if necessary.

If you own one of the affected vehicles and have additional questions, you're encouraged to contact Suzuki Customer Service at 800-934-0934.