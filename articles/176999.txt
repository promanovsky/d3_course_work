Samsung apparently has some new tablets in the works, the device will be called the Samsung Galaxy Tab S and it will apparently comes with two different display sizes.

The Samsung Galaxy Tab S is said to come with a choice of an 8.4 inch display or a 10.5 inch display, both devices will feature a WQXGA display with a resolution 2560 x 1600 pixels.

This new Samsung tablet is said to be powered by an octa core Exynos 5420 processor, which will be made up of core 1.9Ghz cores and four 1.3GHz cores.

It will also comes with a 6 core Mali-T628 GPU, plus 3GB of RAM and front and rear facing cameras., on the front there will be a 2.1 megapixel camera for video calls on the back there will be an 8 megapixel camera for photos and videos.

Other rumored specifications on the Samsung Galaxy Tab S will include a built in fingerprint scanner, WiFi, Bluetooth, GPS, and a built in IR blaster.

According to the report, the Samsung Galaxy Tab S will feature a similar design to the Samsung Galaxy Tab 4 tablet, and the device will come with Android 4.4.2 Kit Kat.

As yet there are no details on when Samsung will make the new Galaxy Tab S official, as soon as we get some more information, and some actual pictures of the device, we will let you guys know.

Source Sammobile

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more