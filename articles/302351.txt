Federal Reserve chair Janet Yellen has begun her press conference.

Economic activity is rebounding in the current quarter and will continue to expand at a modest pace after.

Advertisement

"Unemployment remains elevated and underutilization in the labor market remains significant," says Yellen.Inflation is running below the 2% objective, and this could pose risks to economic performance.

The Committee expects inflation to move gradually back towards its objective.

Advertisement

-------

The Federal Open Market Committee (FOMC) just unveiled its latest monetary policy decision.

Advertisement

As expected it moved to cut its monthly asset purchase program by $10 billion to $35 billion. The Fed also kept its interest rate unchanged, though it lowered its GDP forecast. Despite the recent uptick in inflation, its language on inflation was largely unchanged.

Basically, the Fed sees no urgency on the part of the FOMC to tighten monetary policy by raising rates, even though unemployment has come down dramatically, and inflation is picking up.

We'll have live coverage of Fed chair Janet Yellen's press conference.