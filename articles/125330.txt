GOOGLE’S first-quarter earnings growth faltered as the internet's most influential company grappled with a persistent downturn in advertising prices while spending more money to hire employees and invest in daring ideas.

The results announced Wednesday fell below analyst projections. Google's Class A stock shed $17.10, or 3 per cent, to $546.80 in extended trading.

Although it remains among the world's most profitable companies, Google Inc. is struggling to adjust to a shift away from desktop and laptop computers to smartphones and tablets. The upheaval is lowering Google's ad rates because so far marketers haven't been willing to pay as much to pitch consumers who are squinting at the smaller screens on mobile devices.

Google earned $3.45 billion, or $5.04 per share, in the quarter. That was up 3 per cent from $3.3 billion, or $4.97 per share, last year.

If not for costs covering employee stock compensation and other one-time items, Google said it would have earned $6.27 per share. That figure missed the average analyst target of $6.36 per share, according to FactSet.

Revenue rose 19 per cent from last year to $15.4 billion.

After subtracting advertising commissions, Google's revenue stood at $12.2 billion — about $200 million below analyst projections.

Google's average rate for ads appearing alongside its search results fell 9 per cent from last year. It marked the 10th consecutive quarter that the company's “cost-per-click” has declined from the previous year.

The erosion would have hurt Google even more if people hadn't been clicking on ads more frequently. The volume of activity is important because Google bills advertises when people click on a promotional link. Google's paid clicks during the first quarter climbed 26 per cent from last year.

As its ad prices sagged, Google's operating expenses shot up 31 per cent from last year to $5.3 billion. The rise included the addition of about 2,300 Google employees, the biggest three-month rise in the company's workforce since buying Motorola Mobility for $12.4 billion nearly two years ago. Excluding the Motorola deal, it was the largest quarterly increase in Google's total employees since the summer of 2011 when an additional 2,500 people joined the company.

Google is in the process of selling Motorola to Lenovo Group for $2.9 billion. Motorola lost $198 million in the first quarter, extending a streak of uninterrupted losses under Google's ownership.

Some of the first-quarter increase in employees and operating expenses stemmed from Google's $3.2 billion acquisition of hi-tech home appliance maker Nest Labs, said Patrick Pichette, Google's chief financial officer.

The first-quarter results were further muddied by a recently completed stock split that created a new category of Class C shares, which hold no voting power. The split cut Google's per share earnings in half to reflect a doubling of the company's outstanding stock.

“It was a noisy quarter, but nothing to hit the panic button about,” said Edward Jones analyst Josh Olson.

As has been the case for years, Google also is spending heavily on a variety of projects that have little to do with its main business of internet search and advertising. Some of these ventures, such as Google's widely used Android operating system and Chrome browser, have paid off. Others, including Google's internet-connected eyewear, driverless cars and internet-beaming balloons, remain in testing stages.

Google is counting on the prolonged slump in ad prices to eventually reverse. The hope is that advertises will realise that mobile devices are superior marketing machines because they accompany people while they are on the go and near merchants who might have appealing products.

As the company accumulates more information about individual consumers through their search requests, emails and other online activities, Google expects to improve its ability to aim ads at people most likely to be interested in specific products while they are in the vicinity of a store selling the merchandise.

As it is, Google already sells nearly half of the world's mobile advertising, according to the research firm eMarketer. Facebook ranks second with a 17.5 per cent share.