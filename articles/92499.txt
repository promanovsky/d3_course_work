REDMOND, Wash., April 8, 2014 /PRNewswire/ -- Microsoft Corp. will publish fiscal year 2014 third-quarter financial results after the close of the market on Thursday, Apr. 24, 2014 on the Microsoft Investor Relations website at http://www.microsoft.com/investor. A live webcast of the earnings conference call will be made available at 2:30 p.m. Pacific Time on the Microsoft Investor Relations Web site at http://www.microsoft.com/investor.

Founded in 1975, Microsoft (Nasdaq "MSFT") is the worldwide leader in software, services and solutions that help people and businesses realize their full potential.

Logo - http://photos.prnewswire.com/prnh/20000822/MSFTLOGO

SOURCE Microsoft Corp.