Lee Marshall, one of the voices of Kellogg's Tony the Tiger, died of esophageal cancer at the age of 64 in Santa Monica, Calif., on Saturday, April 26, the Associated Press reports.

Marshall first began voicing the iconic cereal mascot—with his catchphrase "They're g-r-r-r-e-a-t!"—in 1999 while filling in for its original actor, Thurl Ravenscroft. He took over the role full-time after Ravenscroft's death in 2005.

Marshall became a disc jockey at the age of 14 in Phoenix, AZ, and was a newscaster for CKLW in Windsor, Ontario. In later years, he traveled the U.S. and conducted ringside interviews with professional wrestlers.

According to the L.A. Times, Marshall was protective of voicing his famous character Tony. "He wouldn't do it with children around," his former KRTH-101 colleague Tom Kelly said. "What a set of pipes," he added. "Sitting next to him, even a whisper became a roar."

Marshall is survived by his wife Judie, his son Jason Marshall VanBorssum, stepdaughter Eve Borders Ottis, and granddaughter Kate.