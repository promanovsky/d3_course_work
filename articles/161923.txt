Supreme Court justices hearing a dispute between broadcasters and an Internet startup company -- a case that has the potential to bring big changes to the television industry -- reportedly appear unsure about a ruling.

The company is Aereo Inc., and the justices are hearing arguments Tuesday over its service that gives subscribers in 11 U.S. cities access to television programs on their laptop computers, smartphones and other portable devices.

Midday Tuesday, justices appeared unsure whether to rule against Aereo, with several raising concerns about how a ruling in favor of broadcasters would impact cloud computing services, according to Reuters.

Justice Stephen Breyer told The Associated Press that the prospect makes him nervous.

Broadcasters say Aereo is essentially stealing their programming by taking free television signals from the airwaves and sending them over the Internet without paying redistribution fees. Those fees, increasingly important to the broadcasters, were estimated at $3.3 billion last year.

Arguments for and against the legality of the service have focused on the 1976 Copyright Act, which protects “private” rebroadcasts and deems “public” rebroadcasts of network feeds as illegal, Digital Trends reports.

Broadcasters claim the service is a public broadcast, while Aereo says it’s only providing legal, private performances.

The case involving Internet innovation is the latest for justices who sometimes seem to struggle to stay abreast of technological changes.

Broadcasters including ABC, CBS, Fox, NBC and PBS sued Aereo for copyright infringement, saying Aereo should pay for redistributing the programming the same way cable and satellite systems do. Some networks have said they will consider abandoning free over-the-air broadcasting if they lose at the Supreme Court.

Aereo founder and CEO Chet Kanojia recently told The Associated Press that broadcasters can't stand in the way of innovation, saying, "the Internet is happening to everybody, whether you like it or not." Aereo, backed by billionaire Barry Diller, plans to more than double the number of cities it serves, although the high court could put a major hurdle in the company's path if it sides with the broadcasters.

Aereo's service starts at $8 a month and is available in New York, Boston, Houston and Atlanta, among others. Subscribers get about two dozen local over-the-air stations, plus the Bloomberg TV financial channel.

In the New York market, Aereo has a data center in Brooklyn with thousands of dime-size antennas. When a subscriber wants to watch a show live or record it, the company temporarily assigns the customer an antenna and transmits the program over the Internet to the subscriber's laptop, tablet, smartphone or other device.

The antenna is only used by one subscriber at a time, and Aereo says that's much like the situation at home, where a viewer uses a personal antenna to watch over-the-air broadcasts for free.

The broadcasters and their backers argue that Aereo's competitive advantage lies not in its product, but in avoiding paying for it, according to The Associated Press.

The federal appeals court in New York ruled that Aereo did not violate the copyrights of broadcasters with its service, but a similar service has been blocked by judges in Los Angeles and Washington, D.C.

The 2nd U.S. Circuit Court of Appeals in New York said its ruling stemmed from a 2008 decision in which it held that Cablevision Systems Corp. could offer a remote digital video recording service without paying additional licensing fees to broadcasters because each playback transmission was made to a single subscriber using a single unique copy produced by that subscriber. The Supreme Court declined to hear the appeal from movie studios, TV networks and cable TV companies.

In the Aereo case, a dissenting judge said his court's decision would eviscerate copyright law. Judge Denny Chin called Aereo's setup a sham and said the individual antennas are a "Rube Goldberg-like contrivance" — an overly complicated device that accomplishes a simple task in a confusing way — that exists for the sole purpose of evading copyright law.

The Associated Press contributed to this report.