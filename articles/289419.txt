Nia Sanchez was crowned Miss USA 2014 over the weekend.

But the 24-year old Las Vegas resident was handed a true honor last night, presenting the Top Ten list on The Late Show with David Letterman.

What are some of the most comment Miss USA Pageant Mistakes, according to that program's writers?

HINT: they involve hoof injuries, gang activities and Vladimir Putin.

Sanchez, meanwhile, wasn't the only Miss USA contestant to earn headlines this week.

Mekayla Diehl (Miss Indiana) received endless praise online for her curvy bikini body, with viewers excited over a beauty pageant hopeful who doesn't resemble a twig.

Heck, even Sanchez is a big fan, saying on The Today Show that the pageant is all about "not being skinny and having the clothes fall off your bones."

What it is it about? "Being fit, healthy and active."

Celebrate both Diehl and Sanchez's win via the following photo gallery: