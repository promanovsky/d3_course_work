Dan Farber/CNET

Jimmy Iovine, one of the founders of Beats Audio and chairman of Universal Music Group's Interscope Records, could soon become a "special adviser" to Apple CEO Tim Cook, a new report claims.

Iovine and Apple are in talks to discuss how the partnership might play out, according to The New York Post, citing people who claim to have knowledge of those discussions. In his role, Iovine, who helped catapult the careers of Lady Gaga and Eminem, would advice Cook on creative topics, said the report.

Apple and Beats are reportedly in talks that could eventually see the iPhone maker acquire the music firm for $3.2 billion. Beats co-founder Dr. Dre appeared to confirm rumors of the buyout in a video posted to Facebook that was later removed. It's not clear whether Iovine's new role at Apple would be part of this deal or sit outside of it.

Iovine has a long history in the music business, working with some of the greatest artists of all time, including Bruce Springsteen. He's also been instrumental in the growth of the Beats brand, building it into a multibillion dollar company once owned by HTC and then acquired back.

According to the Post's sources, Iovine's contract with Universal ends in 2014, leaving open the possibility of him joining Apple early in 2015. It's not clear, however, whether the creative deal would be a part-time or full-time position.

Apple declined CNET's request for comment. CNET has also contacted Beats for comment. We will update this story when we have more information.