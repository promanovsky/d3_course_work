Record-breaking musical Cats is getting a modern makeover complete with a rapping cat when it heads back to the West End later this year.

The show, inspired by the poems in TS Eliot's book Old Possum's Book Of Practical Cats, has been seen by more than 50 million people in more than 30 countries since its launch in 1981.

Composer Andrew Lloyd Webber said he was “never very happy” with some parts of the show including the Rum Tum Tugger song.

He said the character would become “a street cat” in the new version, adding: “I've come to the conclusion that having read Eliot again, maybe Eliot was the inventor of rap”.

He said: “The thing about the Eliot verse is that you can tell he's American, nobody other than Eliot would have written 'The Rum Tum Tugger is a curious cat'.”

The show, which runs at the London Palladium for 12 weeks from December 6, reunites Lloyd Webber with the original team behind Cats including director Trevor Nunn.

His previous show Stephen Ward The Musical, which was inspired by the 1963 Profumo Scandal, ended in March this year after a run of less than six months but Lloyd Webber said there was nothing wrong in putting on a new version of one of his old shows.

Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

He said: “I don't think that is necessarily true because if you said that we wouldn't have a lot of room for Shakespeare.”

Cats made its debut in 1981 and ran in the West End for 21 years and 18 years on Broadway, collecting handfuls of awards along the way.