Czech manufacturing sector continued to expand for the fourteenth consecutive month in June though at a slower rate, the results of a survey by Markit Economics and HSBC Bank revealed Tuesday.

The HSBC Czech Republic Manufacturing PMI came in at 54.7 in June, signaling continued expansion. However, this index was lower than the May reading of 57.3 and marked the steepest month-on-month fall since November 2011.

New order growth continued for the thirteenth successive month but the rate of growth slowed to its weakest pace since October 2013.

Output rose for the fifteenth consecutive month in June, but at a slower rate than in May. This marked the weakest expansion since July 2013. Purchasing activity also slowed in June.

Employment continued to expand for the fourteenth successive month despite the slower rate of growth in new orders and output. However, the rate of job creation eased in June.

The input cost inflation rate remained below the long-run survey average, while output prices rose marginally in June.

For comments and feedback contact: editorial@rttnews.com

Business News