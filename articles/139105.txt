It’s been a tough year for Debbie Gibson, but she finally has a diagnosis and she’s on the road to recovery.

Photo credit: C.M. Wiggins/WENN.com

Debbie Gibson revealed something quite personal on her blog on Wednesday because of comments on her appearance from her recent Instagram pictures. The 43-year-old singer shared that she is suffering from Lyme disease.

She wrote, “At the start of 2013, I spent time both on the West and East Coast followed by travel overseas. I am not exactly sure when or where I picked it up, but, in the early spring, I began to show unusual symptoms. A lot of Lyme symptoms mimic symptoms of anxiety and stress.

“The first symptoms came in the form of food sensitivities. I found I could not touch sugar, starch, caffeine, certain oils, etc., without having a severe reaction that felt like jolts of electricity running through my body,” Gibson shared. “I felt like a live wire. Talk about ‘Electric Youth!'”

Doctors couldn’t pinpoint what was wrong with the musician and she admitted that she “got tested for everything under the sun.” Her health issues became even more serious when “the weight kept falling off” and she sustained “numbness and tingling in my hands and feet, which is very disconcerting for a pianist and dancer to say the least.”

Her gaunt appearance left her in a panic when she was asked to make a cameo in the Mega Shark series on Syfy. She rushed to her dermatologist to help improve her depleted look. The doctor suggested Botox, which Gibson promptly accepted.

However, the “Foolish Beat” singer wrote, “I completely forgot there is a word TOX in BOTOX. It is a POISON.” The treatment “wreaked havoc on my physical and mental state.”

Months went by and it wasn’t until she was referred to specialist Dr. Joseph Sciabbarassi that her illness was correctly diagnosed.

“You could have knocked me over with a feather when I tested positive for Lyme. Western Blot and all the rest that followed… positive, positive, positive,” she said.

The former teen star is on a regimen of antibiotics to wipe out the Lyme disease, but she does have a long road to recovery. Gibson is grateful for the correct diagnosis and putting on a few pounds.

“I am an advocate of being lean and healthy and toned. Not skeletal,” said Gibson.

There is a message in all of this for the singer.

She summed up, “I want to urge you all… NEVER JUDGE… ANYONE! You never know what someone is going through…Keep rockin’ through whatever adversity is presenting it self. We all face challenges. Let it flow on through. It has no power over you!”

Here’s to continued good health on your road to recovery, Debbie!