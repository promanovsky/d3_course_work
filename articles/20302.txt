CAMBRIDGE, England, March 17 (UPI) -- Researchers in Britain said their meta-analysis involving fatty acids and heart disease found evidence did not support guidelines to restrict saturated fats.

An international team led by the University of Cambridge analyzed existing studies and randomized trials on coronary risk and fatty acid intake.

Advertisement

The meta-analysis, published in the journal Annals of Internal Medicine, found current evidence did not support guidelines to restrict the consumption of saturated fats in order to prevent heart disease.

The researchers also found insufficient support for guidelines which advocate the high consumption of polyunsaturated fats -- such as omega 3 and omega 6 -- to reduce the risk of coronary disease.

In addition, the when specific fatty acid subtypes, such as different types of omega 3, were examined, the effects of the fatty acids on cardiovascular risk varied.

Lead author Dr. Rajiv Chowdhury of the University of Cambridge said the researchers analyzed data from 72 unique studies involving 600,000 participants from 18 nations.

The investigators found different sub-types of circulating long-chain omega-3 and omega-6 fatty acids had different associations with coronary risk, with some evidence that circulating levels of eicosapentaenoic and docosahexaenoic acids -- two main types of long-chain omega-3 polyunsaturated fatty acids -- and arachidonic acid, an omega-6 fat, are each associated with lower coronary risk.

RELATED Women have chest pain but are told heart and arteries are fine

Similarly, within saturated fatty acid, the researchers found weak positive associations between circulating fatty acids largely in palm oil and animal fats, respectively, and cardiovascular disease. However, circulating margaric acid -- a dairy fat -- significantly reduced the risk of cardiovascular disease.

"This analysis of existing data suggests there isn't enough evidence to say that a diet rich in polyunsaturated fats but low in saturated fats reduces the risk of cardiovascular disease. But large scale clinical studies are needed, as these researchers recommend, before making a conclusive judgement," said Jeremy Pearson, associate medical director at the British Heart Foundation, which helped fund the study.

"Alongside taking any necessary medication, the best way to stay heart healthy is to stop smoking, stay active, and ensure our whole diet is healthy -- and this means considering not only the fats in our diet but also our intake of salt, sugar and fruit and vegetables."

RELATED Added sugar linked to increased risk of heart disease deaths