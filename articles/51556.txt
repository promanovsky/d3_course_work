Microsoft's new chief executive Satya Nadella used his first public appearance on Thursday to stamp his direction on the company, unveiling a version of its dominant Office software specially tailored for Apple's iPad that analysts say could generate billions of dollars in new sales.

Nadella, who was appointed only in January, moved to put clear water between himself and his predecessor, Steve Ballmer, unveiling a "mobile first, cloud first" strategy and signalling that he would increase sales by putting Microsoft software on every device, whether running Windows or not.

Ballmer, who ran the company for 14 years, had focussed on incremental sales based around the venerable brand but stepped down amid mounting criticism of the company's dependence on Windows, Office and workplace software that generated massive profits for decades but have been increasingly challenged by online alternatives.

The release of Office for iPad had been widely rumoured ahead of the event, pushing Microsoft's share price above $40 (£24) this week for the first time since April 2000, when the dotcom boom had accelerated to the brink of collapse.

Though the touch-optimised software for the top-selling tablet had clearly been ready for some time, Ballmer had apparently blocked its introduction, preferring to try to drive Microsoft's Office sales on products which ran its own Windows software. Nadella swept that away in a 15-minute presentation in San Francisco, where, dressed in a black polo shirt, he enthused about the company's strategy and future.

"What motivates us is the realities of our customers," said Nadella, in a tacit admission of the failure of Ballmer's strategy to keep Office for tablets restricted to Microsoft's own Surface device which has sold poorly.

Apple has sold nearly 200m iPads since its launch in 2010 and says that "virtually all" of the Fortune 500 and Global 500 companies are deploying or testing iPads.

Dwindling sales of standard PCs have contrasted sharply with fast-growing sales of the iPad and Android tablets. Though the Office suite – comprising Word, its word processing software, the Excel spreadsheet program and PowerPoint presentation software – generated $25bn in sales last year, analysts reckon that an iPad version could have generated an extra $2.5bn.

"Office on iPad will generate more revenues for Microsoft than all the Surface sales combined for at least the next two years," said Neil Shah of Counterpoint Research.

Jan Dawson of Jackdaw Research was less sure: "editing documents in Office on the iPad will cost at least $70 a year," he said. "The big question is whether people will want to pay that kind of premium for the ability to use a 'full' version of Office. So many people whose work lives are iPad-centric have moved away from Office entirely and Microsoft will now have to win them back. The overlap between the 3.5m consumer Office365 subscribers and the 150m or so iPad users is likely vanishingly small and Microsoft will have to change that."

Some businesses have held back from adopting tablets because their staff need to be able to edit Office documents or spreadsheets on them. Thursday's release could prove beneficial for both Apple and Microsoft – formerly bitter rivals in the PC space.

Office for iPad will be free to use for reading and presenting; creating and editing documents will require a subscription to Microsoft's cloud-based Office365 service, which synchronises documents between multiple devices.

The move is also a direct challenge to Google, which has been eroding Microsoft's customer base for Office through its cheaper Google Docs collaborative software. While Office365 is still more expensive, the availability on the iPad of Office – which is seen by many businesses as crucial – could bolster its position.

Apple, too, could benefit from a sales boost. Tablet sales have slowed down in the US, with respondents saying that they do not see the use of them but the power of the Office brand could drive sales among some consumers, while also helping business adoption.

Nadella, whose key experience in his career at the Redmond-based company has been working on its "cloud" services, greeted the audience of journalists by commenting of his tenure that "It's day 52 - but who's counting?"

He outlined a long-term vision in which "every interaction will be digitised" and "people will be connected to the internet through phones, tablets, bigger screens, sensors in rooms – it's going to be everywhere." He said that Microsoft would create "a cloud for everyone on every device".

Apple said in a statement that it was "excited" that Office was coming and that "Office for iPad joins an incredible lineup of productivity apps like iWork, Evernote and Paper by FiftyThree, that users can choose from to inspire them to do more with this powerful device."

There will in future be a version of Office to run on Google's Android, Nadella indicated. "Our commitment going forward is to drive Office 365 everywhere, across the web, phones, devices, everywhere," he said.