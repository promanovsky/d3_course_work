THERE has never been a better time to adopt the fist bump as your greeting of choice than now. Who are we kidding? It’s always a great time to fist bump, but now science has actually given us a reason to do it.

A new study in the American Journal of Infection Control has found that fist bumps transfer about 90 per cent less bacteria than a handshake does.

The study was conducted with participants who wore gloves covered in E. coli bacteria. They then shook hands, gave almighty high-fives and bumped fists with other participants who were wearing sterile gloves. The amount of bacteria on the sterile gloves was then tested.

The results showed that the larger area of contact between the hands through a handshake combined with the prolonging of the contact played a key role in relaying more germs. While that may seem quite obvious, this is the first time it has actually been scientifically studied.

High-fives also fared better than the traditional handshake, with only around half the amount of bacteria compared to handshakes transferred.

So next time you meet up with someone, request the highest of fives or the broest of bumps!