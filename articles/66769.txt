Twitter Inc (NYSE:TWTR) has been making many changes of late like adding multiple photo uploads, photo tagging, profile changes on iOS and many more, but that’s not all. In one of the biggest changes, not official yet, some users have claimed that the historic retweet button inside their iOS app is now named the ‘share’ button.

Name changed, not the process

Eli Langer from CNBC was the first to notice the tweets from the concerned users, who are certainly witnessing a Facebook Inc. (NASDAQ:FB) like experience on Twitter now.

As of now, only the name has changed while the process of sharing a tweet has not been touched. Affected users are now asked if they want to ‘share’ or add comment and share.

Schonfeld Profits From Quant And Tactical Strategies In 2020 Schonfeld Strategic Advisors's, Schonfeld Fundamental Equity Fund returned 14.14% net in 2020 and the Schonfeld Strategic Partners Fund added 9.88%, that's according to a copy of the firm's fourth-quarter and full-year letter to investors, which ValueWalk has been able to review. Q4 2020 hedge fund letters, conferences and more Schonfeld celebrated its fifth year of Read More

From the reactions to the share button, it does not appear that users are happy with the change. In past, Twitter has been careful to pay attention to feedback from the user – it even took back some decisions that offended users. From time to time, the social media company does come out with some tests, so it remains to be seen what would be the ultimate fate of the ‘share’ button that has replaced the popular ‘retweet’ option.

Other recent changes from Twitter

Recently, Twitter Inc (NYSE:TWTR) also offered two more updates surrounding photos. Now a user can tag a photo by tapping, “Who’s in this photo?”, which will automatically notify the person tagged. Also, a user can tag up to 10 people in a photo without worrying about the character count.

The second update allows users to include up to four photos in a single Tweet, which would relieve users from not sending individual snaps. With the new update, users could also create a mini collages in a grid style.

Twitter Inc (NYSE:TWTR), recently experimented by removing @ mentions from replies. The reason from the company was its ‘scaffolding’ was discouraging new users by making it difficult for them to understand and use the service. As explained by Gizmodo, one of the biggest problem facing Twitter is that users do not want the micro blogging site to transform into similar to Facebook.