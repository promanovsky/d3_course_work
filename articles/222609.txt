Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Want to see the best video of two Hollywood megastars throwing beer about that will probably ever exist?

Oscar-winning actor Matthew McConaughey can be seen just hanging out on his - apparently massive - balcony with his family when who should appear on the balcony across the street but Hollywood hunk Brad Pitt.

The pair then decided to have a casual chit chat across the street below, waving and laughing and joking, when Brad decided to up the ante by giving his neighbour a gift - the gift of beer. Amazing.

Disappearing into his house for a bit, Angelina Jolie's soon to be husband, returned and hurled the can of beer across the road and over to Matt.

And if you look closely it seems like one of Brangelina's brood appeared at the door to watch Brad show off his throwing arm to the army of fans below.

The video - which is pretty awesome - will just fuel speculation that Brad is ready to sign up to replace Matt in the second season of True Detective.

He gives him beer and gets his TV show in return - seems like a fair swap.

The 12 Years a Slave actor is said to be close to signing a deal to star in the next season of the HBO drama , according to reports.

McConughey and Woody Harrelson starred in the first series of the drama which saw the pair play detectives Rust Cohle and Martin Hart on the trail of a sadistic killer in Louisiana after the satanic murder of prostitute Dora Lange.

Speculation has been rife as to who will replace the pair in the critically-acclaimed drama after they confirmed that they would not be returning for another season.

Pitt is said to be currently in the middle of trashing out the final details of deal with the network to star in the second season of the critically acclaimed show.