Gina Carano is headed to work with Ben Shapiro after her ouster from the galaxy far, far away.

Carano, who was fired from her role as Cara Dune on Disney's Star Wars series The Mandalorian this week over her controversial social media posts, announced Friday she's making a film with the conservative political commentator's website The Daily Wire.

"The Daily Wire is helping make one of my dreams — to develop and produce my own film — come true," Carano told Deadline. "I cried out and my prayer was answered. I am sending out a direct message of hope to everyone living in fear of cancellation by the totalitarian mob. I have only just begun using my voice which is now freer than ever before, and I hope it inspires others to do the same. They can't cancel us if we don't let them."

Carano frequently stirred up controversy on social media in recent months, including with posts mocking the use of face masks during the COVID-19 pandemic and suggesting voter fraud occurred in the 2020 presidential election, prompting the hashtag "#FireGinaCarano" to often trend on Twitter. This week, she shared a post on Instagram comparing the experience of conservatives in the United States to Jews in Nazi Germany, which was reportedly the last straw for Lucasfilm.

The decision to remove Carano from Star Wars garnered celebration by those fans who had been calling for her ouster, as well as criticism from conservatives, including Sen. Ted Cruz (R-Texas), who came to her defense. Shapiro on Friday praised the actress and former MMA star as an "incredible talent dumped by Disney and Lucasfilm for offending the authoritarian Hollywood Left."

In announcing that Carano would no longer be involved in Star Wars, Lucasfilm condemned her social media posts "denigrating people based on their cultural and religious identities" as "abhorrent and unacceptable." Brendan Morrow