Exposure to some common pesticides may increase the risk of autism, according to a new study from researchers at the University of California, Davis. For the study the researchers reviewed records of 486 children with autism.

They compared these records with roughly 200 children suffering with developmental delays and around 300 kids with no disabilities. They found that those kids with autism were far more likely to have lived in an area with potential for pesticide exposure than those with developmental delays or no medical conditions.

"Ours is the third study to specifically link autism spectrum disorders to pesticide exposure, whereas more papers have demonstrated links with developmental delay," the researchers explained.

"We already knew from animal studies as well as from epidemiologic studies of women and children that prenatal exposure (to pesticides) is associated with lower IQ," they added. "This study builds on that, uses the population of a whole state, looks at multiple different pesticides and finds a pattern of wide association between pesticide exposure and developmental disability."

For comments and feedback contact: editorial@rttnews.com

Health News