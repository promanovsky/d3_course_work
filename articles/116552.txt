It’s Tax Day! As of the end of last week, the IRS had received nearly 100 million returns, or about three-quarters of all the returns it expects to get this season. So if you haven’t filed yet, you’re in the minority. Still if you have procrastinated and are owed a refund, it won’t hurt to file late. Tax payers are only hit with penalties if you owe the government money.

It is still a smart idea to file for an extension; about 12 million taxpayers have requested extensions. The average refund is $2,792, up 1% from last year. Here are a few tips, tricks, and useful articles for the longest day of the year at the IRS:

Free Cookies! Free Massages! Here Are All the Best Tax Day Freebies

Are Taxes Fair? If You Answer Yes, You’re Probably Poor

Here’s How Shady Tax Preparers Plan to Steal Your Money

The IRS’ Last-Minute Tax Tips

10 Tax Audit Red Flags

Make the Most of Tax Write-Offs

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.