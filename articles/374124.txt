Call it the Great Kickstarter Food Drive of 2014.

Following news that an Ohio man has raised upward of $52,000 to “basically just make potato salad,” crowdfunding aspirants around the country are cooking up similarly half-baked ideas. Scroll through Kickstarter’s “recently launched” page, and you’ll find projects seeking funds to make macaroni salad, ambrosia salad, homemade spaghetti, smoked wings and even Nutella-covered bacon. And that’s just for starters.

“Help me make coleslaw,” asks one project creator.

“Pasta salad is better than potato salad,” boasts another.

In this bizarre race to the bottom, there is even a Kickstarter hopeful seeking $8 to make a peanut-butter-and-jelly sandwich. “Since I’ve been able to remember, peanut butter and jelly has been my favorite thing,” the project creator writes.

It’s all a response to Zack Danger Brown, who launched a project last week seeking $10 to make potato salad, and found himself with tens of thousands of dollars within a few days. More than 4,000 backers have contributed to his project, most giving small amounts, but some donating $50 or more. Appearing on ABC’s “Good Morning America” on Tuesday, Brown said he had no idea the project would take off in the way that it did. He continues to assert that he is simply an unassuming man who wanted to make potato salad.

Given all the extra money he has raised, Brown has vowed to complete a number of stretch goals, including T-shirts, a party in his hometown of Columbus and a live stream of him making the potato salad. In a Reddit AMA earlier this week, Brown said Kickstarter’s rules prohibited him from saying he’ll donate the money to charity.

Kickstarter is no stranger to weird campaigns, of course, but Brown’s project seems to have struck just the right balance between absurdity and goodwill. From the outside, it could almost look like satire -- a biting commentary on the dangers of crowdfunding, where rules are scarce and culpability scarcer. But Brown’s ostensible sincerity contradicts that notion, and the earnestness and simplicity of the campaign itself has helped propel Brown as the Internet’s champion du jour. Gizmodo on Monday dubbed him a “hero.”

A more cynical question would be to ask whether there is any room for accountability left in crowdfunding. Kickstarter Inc., once one of the most restrictive crowdfunding sites around, recently relaxed its rules amid competition from Indiegogo, GoFundMe and others. Most notably, it removed the screening process by which all new projects had to be looked over by Kickstarter staff, thereby making it easy for funding hopefuls to launch projects on the fly.

Photo: Kickstarter/screenshot

But the company doesn’t appear to have a problem with Brown’s 52K side dish. Asked if the project falls in line with the site’s terms, Justin Kazmark, a Kickstarter spokesman, said simply that the site welcomes projects of all shapes and sizes. “There’s no single recipe for inspiration,” Kazmark joked.

As for the issue of overfunding, Kazmark said there is no limit to how far over a project can go on its initial goal, which means for Brown -- whose project has more than three weeks to go -- the sky is the limit.

So far, none of the copycat efforts appear to have struck gold in the way that Brown did, but that doesn’t mean that the new projects won’t come to fruition. Bill Freitag of Aberdeen, South Dakota, has already managed to take in $5 in his bid to make cookie salad. (“Because potato salad is gross,” he writes.) And with a goal of only $10, he’s already halfway there.

It’s a small victory, perhaps, but Freitag will likely fare better than the person seeking to raise $600 to make rum cake. Then again, anything’s possible.

Got a news tip? Email me. Follow me on Twitter @christopherzara.