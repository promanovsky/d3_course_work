Advertising giants Omnicom Group Inc. and Publicis Groupe SA have called off their $35 billion merger.

The deal billed as a “merger of equals” had been challenged by battles over position and power, including difficulties in getting tax and other regulatory approvals, as well as differences over which company would be listed as the technical acquirer of the other, people familiar with the matter have said.

Bloomberg

“The challenges that still remained to be overcome, in addition to the slow pace of progress, created a level of uncertainty detrimental to the interests of both groups and their employees, clients and shareholders,” the companies said in a joint statement released Thursday evening.

The tie-up between Omnicom OMC, +0.35% and Publicis PUB, +0.02% , announced with much fanfare in July and originally expected to close by the end of 2013, was designed to give the companies more heft in competing with deep-pocketed Silicon Valley giants like Google Inc. GOOG, +0.39%

The megamerger would have created the world’s largest ad holding company by revenue, combining firms such as ad agencies as BBDO, Saatchi & Saatchi, DDB, Leo Burnett and TBWA as well as public-relations firms including FleishmanHillard and Ketchum, and digital ad agencies DigitasLBi and Razorfish.

The companies had made clear it was to be a merger of equals, where the shareholders would each receive about 50% of the equity in the new company — Publicis Omnicom Group — and where the two CEOs would share the top job for 30 months from the closing.

But despite the merger-of-equals billing, technically one company has to acquire the other, for accounting reasons. The two sides hadn’t agreed on which company would be the acquirer of the other, which held up filing of crucial paperwork with the U.S. Securities and Exchange Commission, said people familiar with the situation.

An expanded version of this report appears on WSJ.com.

More MarketWatch on Tesla:

Nigam Arora: Is Tesla Motors worth $50 or $500?

Only 4 Teslas have ever been stolen

Hot stocks: Tesla, SolarCity, Green Mountain, Priceline