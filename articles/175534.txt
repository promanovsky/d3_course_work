Already spinning large webs of money overseas, "The Amazing Spider-Man 2" is a decent superhero franchise product, lent some personality by Andrew Garfield's skyscraper hair and the actor's easy, push-pull rapport with co-star Emma Stone, who plays the eternally disappointed Gwen, freshly graduated from high school, frustratingly in love with Peter Parker.

The love is mootual, as Teri Garr said in "Young Frankenstein." But Spandexed, web-slinging crime-fighting consumes our hero, who is graduating along with Gwen. Spider-Man's primary adversary is Electro, an energy-sucking mutant, an electric eel/human hybrid played by Jamie Foxx.

Speaking of energy suckers: I like Garfield a lot in this role, but he does enjoy his ... hesitations and his ... frequent ... tic-laden ... pauses. "The Amazing Spider-Man 2" runs two hours and 21 minutes, and at least 21 of those minutes can be attributed to loose flaps of dead air preceding simple lines of dialogue meant to be whipped through with a little urgency, contributed by Garfield and by Dane DeHaan, who slithers around looking like a bad-seed version of young Leonardo DiCaprio. He portrays Peter's sometime pal, the super-rich Oscorp heir Harry Osborn, who's dying and desperate for the spider venom at the heart of all the pricey research that went awry and gave Peter his unusual abilities.

Folks, I confess: I'm coping with a mild case of arachno-apatha-phobia, defined as the fear of another so-so "Spider-Man" sequel. It wasn't like this a few short years ago, when director Sam Raimi's franchise (the one with Tobey Maguire and Kirsten Dunst) got around to the second part of that trilogy. Bolstered by a formidable adversary in Alfred Molina's Doc Ock, the 2004 "Spider-Man 2" really did the job; it had size and swagger, and the violence in the action sequences was stylized just enough to honor the material's comic book roots.

This is a problem with many superhero franchises, in or out of the Marvel stable of familiar faces. Producers encourage their creative teams to go for massively destructive and apocalyptically scaled brutality in the name of "dark" "realism," and too often the resulting action sequences go on and on forever. (The climax of the recent "Man of Steel" still hasn't ended, and that movie came out last summer.) Director Marc Webb, whose moderately skillful "The Amazing Spider-Man" came out two years ago, returns here and again delivers a reasonably entertaining melange, shot every whichaway, a little hand-held here, a little bob-and-weave there, capturing the swoony, combative couple at the story's center.

When Garfield and Stone aren't working through their issues, the film's essentially an extended electrocution montage, and electrocution, that bloodlessly nasty way to injure or kill someone and still retain a PG-13 or lower rating, rates among my least favorite means of injury or death. Movies get you thinking along those lines, especially when it's superhero time, which is all the time, i.e., too much of the time. Raimi's second "Spider-Man" ranks high among our best summer-season sequels. This one's just OK, which is probably more than adequate from a business perspective. For the record, the script is by Alex Kurtzman, Roberto Orci and Jeff Pinkner. They provide the film with three action climaxes, which is two too many, but what do I know. For the fan base it's probably two too few.

mjphillips@tribune.com

"The Amazing Spider-Man 2" - 2 stars

MPAA rating: PG-13 (for sequences of sci-fi action/violence)

Running time: 2:21

Opens: Thursday evening