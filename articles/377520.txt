Whiting Petroleum Corp. agreed to buy Kodiak Oil & Gas Corp. for about $3.8 billion in an all-stock deal that will create the dominant crude producer in the Bakken shale of the U.S. Great Plains.

Kodiak stockholders will receive 0.177 of share in Whiting for each share they own, which is the equivalent of $13.90 based on the acquirer’s July 11 price, the Denver-based companies said Sunday in a statement. Including $2.2 billion in debt, the total transaction is valued at about $6 billion.

When the deal is complete, Whiting shareholders will own about 71 percent of the combined company, which will be led by Whiting’s senior managers. Together, the two companies produced the equivalent of more than 107,000 barrels of daily oil output from the Bakken formation in the first quarter. That exceeded the region’s current top producer, billionaire Harold Hamm’s Continental Resources Inc., by almost 10 percent.

The combination will give Whiting drilling rights across 855,000 net acres, surpassing Exxon Mobil Corp. as the second-biggest leaseholder in the area, according to data compiled by Bloomberg. Whiting is buying a company that more than doubled production last year while Whiting’s own output growth slowed as costs to bring new wells online surged.

‘Right Time’

“This is the right deal, at the right time,” James Volker, Whiting’s chairman and chief executive officer, said in a telephone interview Sunday. “It massively enhances the scale of the two companies combined.”

The Bakken region of North Dakota and Montana has been a hotbed of U.S. oil exploration after innovations in sideways drilling and hydraulic fracturing, or fracking, enabled access to previously impenetrable rock layers.

In North Dakota, home to some of the richest and most prolific sections of the Bakken, daily crude output surpassed 1 million barrels in April for the first time in history, making the state a bigger oil supplier than OPEC members Ecuador or Qatar. In the U.S., Texas is the only state that pumps more crude than North Dakota, according to the Energy Department.

The per-share price represents about a 5.1 percent premium to Kodiak’s volume-weighted average over the past 60 days, the companies said. Kodiak fell 2.3 percent to $14.23 on July 11, while Whiting declined 1.8 percent to $78.54.

Enterprise Value

An enterprise value for Kodiak of $6 billion is about 8.8 times earnings before interest, taxes, depreciation and amortization last year. That compares with a median of 11.6 times Ebitda for U.S. oil and exploration deals since 2009 valued at more than $1 billion, according to data compiled by Bloomberg.

Whiting and Kodiak executives have known each other for years, but began to evaluate a deal in recent months, Kodiak Chairman and Chief Executive Officer Lynn Peterson said in an interview today. “We both reside here in Denver, our offices are across the street.”