Elisabeth Hasselbeck has said that she is "shocked" that her fellow former View panellist Sherri Shepherd is exiting the show.

ABC confirmed on Thursday (June 26) that Shepherd and Jenny McCarthy are both leaving The View as part of an overhaul that will also include a new studio.



Hasselbeck spoke about the panel changes on The View during a segment on her current show Fox & Friends today (June 27).

"I was shocked, in some ways," the presenter said.

She was then questioned on if she thought that Shepherd was blind-sided by ABC's decision to move The View in a new direction.



"I'm sure, even though you think that might happen someday, I'm sure it would come across to be a shock," Hasselbeck answered.

"I haven't spoken to her in person yet. [It is] set to happen though."

Hasselbeck left The View last summer to join Fox News, but returned to ABC's daytime chatshow briefly as part of the recent sendoff for the retiring Barbara Walters.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io