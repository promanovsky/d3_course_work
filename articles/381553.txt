Perhaps Thor's nickname should be revised to the Goddess of Thunder.

Marvel Comics announced Tuesday that one of its most popular superheroes will be re-envisioned as a woman.

"The inscription on Thor's hammer reads 'Whosoever holds this hammer, if HE be worthy, shall possess the power of Thor.' Well it's time to update that inscription," Marvel editor Wil Moss said in a statement. "The new Thor continues Marvel's proud tradition of strong female characters like Captain Marvel, Storm, Black Widow and more."

Moss wanted to make it clear to readers that the new incarnation of Thor will still be the same superhero and not a feminized off-shoot, like Batwoman.

"This new Thor isn't a temporary female substitute -- she's now the one and only Thor, and she is worthy," Moss explained.

Jason Aaron, who will be writing the new "Thor" series, echoed those sentiments.

"This is not She-Thor. This is not Lady Thor. This is not Thorita. This is Thor. This is the Thor of the Marvel Universe. But it's unlike any Thor we've ever seen before," Aaron said.

Details about Thor's background and her connection to the planet Asgard and the Marvel Universe as a whole will be revealed with her official debut in the comic book series in October.

Marvel's gender swap announcement reflects an ongoing trend in the comic book industry to bring in readers with shocking twists involving long-beloved characters.

On Monday, Archie Comics announced that its namesake would be shot to death while trying to protect a gay friend.