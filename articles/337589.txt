At the BET Awards last night, Chris Brown made his post-prison stage debut.

Also, Robin Thicke once again made a plea to Paula Patton, attempting to serenade his estranged wife with the track "Forever Love."

But the First Couple of Music managed to steal the show from all other performers on Sunday.

Indeed, Beyonce and Jay Z stunned viewers via a taped rendition of "Partition," as the superstars closed the festivities with a sexy duet.

Queen B and Hova are in the middle of their "On the Run" tour, but you can catch up with the singers right here and now:

Pretty awesome, right?

Click through some red carpet hits and misses from the 2014 BET Awards below: