SAN FRANCISCO (MarketWatch) — Gold futures on Wednesday found some support, as traders stretched the metal’s price advance to a fourth session in a row, reluctant to sell the precious metal ahead of Thursday’s much-anticipated monthly U.S. jobs report and a long holiday weekend.

Gold scored gains even after a stronger-than-expected estimate of U.S. private-sector payrolls fed expectations for a strong employment report.

August gold futures US:GCQ4 tacked on $4.30, or 0.3%, to settle at $1,330.90 an ounce on the Comex division of the New York Mercantile Exchange. Prices, which tallied a gain of nearly $10 an ounce over the past three trading sessions, settled at their highest since March 21, based on the most-active contracts.

September silver US:SIU4 also rose 18.5 cents, or 0.9%, to end at $21.30 an ounce.

AFP/Getty Images

Gold prices had traded a bit lower immediately after the data on private-sector jobs were released Wednesday, but then turned positive again. They found support “mainly on the back of the optimism that Federal Reserve Chairwoman Janet Yellen is “going to stay consistent in her message and will maintain the dovish tone, which is eroding demand for the dollar,” said Naeem Aslam, chief market analyst at AvaTrade.

Private-sector employers added 281,000 jobs in June, according to an estimate Wednesday from Automatic Data Processing. Economists use ADP data to get a feel for the Labor Department’s monthly jobs report, which is forecast to show nonfarm payrolls rose by 215,000 jobs in June versus a May gain of 217,000.

The dollar traded higher after the report, gaining ground on the euro EURUSD, -0.12% and the Japanese yen USDJPY, +0.16% . A stronger dollar can be negative for commodities priced in the currency by making it more expensive to those using other currencies.

“Mid-June’s big jump aside, gold has become so boring not even U.S. payrolls are giving traders much fun right now,” said Adrian Ash, head of research at BullionVault.

Navigating a fractured market

Ahead of Wednesday’s ADP release, prices “tried a little of the usual runaround, but the bid stayed firm,” he said. Unless Thursday’s official nonfarm payrolls data blows past 300,000, “it’s hard to see U.S. futures getting sold before the long weekend.” Comex floor trading will be closed Friday for the Independence Day holiday.

Other metals on Comex ended broadly higher.

High-grade copper for September delivery US:HGU4 closed up 6 cents, or 1.9%, at $3.265 per pound. October platinum US:PLV4 shed $3.50, or 0.2%, to $1,511.50 an ounce after Tuesday’s 2.2% advance. Palladium for September delivery US:PAU4 climbed $2.80, or 0.3%, to $857.40 an ounce.

Shares of gold and silver miners rose in afternoon dealings, with the Philadelphia Gold and Silver Index XAU, +0.31% up 1.9%. Shares of the SPDR Gold Trust GLD, -0.30% climbed 0.2%.

More must-reads from MarketWatch:

July 4th gas prices highest since 2008 thanks to Iraq

How to fly on a private jet for under $200 per person

As Dow flirts with 17,000, earnings warnings face stiffer punishment