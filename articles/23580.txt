The highly anticipated new Star Wars film, Star Wars: Episode VII, is set to begin filming this coming May, THR reports.

“There will be some very familiar faces along with a trio of new young leads,” Walt Disney CEO Bob Iger said at the company’s shareholders conference earlier today. He also revealed that the plot would take place 30 years after the events of 1983′s Return of the Jedi.

The film will be directed by J.J. Abrams and is set for release on December 18, 2015.

Not much is known about casting. Adam Driver struck a deal to play a Darth Vader type role, and many are in contention to star in the male leading roles. Oscar winner Lupita Nyong’o is even in talks to star as a female lead!