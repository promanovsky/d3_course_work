hidden

Facebook has emerged as one of the biggest Silicon Valley companies to have resisted rivals' entreaties to stop poaching each other's employees, according to emails between the No. 1 social network and Google released in court filings.

Sheryl Sandberg had just been installed as Facebook's chief operating officer when one of her former colleagues from Google emailed her, according to the filings unsealed late last week. Facebook's aggressive recruitment of Google employees had heightened tensions between the two companies to "Defcon 2," top Google executive Jonathan Rosenberg told Sandberg in August 2008.

"Fix this problem. Propose that you will substantially lower the rate at which you hire people from us," Rosenberg told Sandberg in an email. "Then make sure that happens."

But Sandberg deflected Rosenberg's entreaties, saying she thought Google only had limited no-solicitation agreements with companies with which it shared board members, not blanket no-hire policies with other companies.

Plaintiffs say companies like Apple, Google and Intel drove down wages by agreeing to avoid soliciting employees from each other. "I declined at that time to limit Facebook's recruitment or hiring of Google employees," Sandberg wrote in a sworn declaration submitted by the plaintiffs in court.

That response had made Facebook a witness, rather than a defendant, in a massive antitrust lawsuit brought by tech workers against the largest players in Silicon Valley.

A Facebook representative declined to comment on Monday, while a Google spokesman could not immediately be reached.

In past statements, Google said it has always "actively and aggressively" recruited top talent.

Harry First, a professor at New York University School of Law who specializes in antitrust, said on Monday the exchange between Sandberg and Rosenberg at a minimum confirms how board relationships between the tech companies initially drove the agreements not to solicit from each other, which then expanded from there. "That does not look good to the jury," First said.

However, the defendants can also argue that the refusal of companies like Facebook to limit hiring shows that any alleged conspiracy could not have impacted the broader job market and wages in general.

In 2010, Google, Apple, Adobe, Intel and others agreed to a settlement of a U.S. Justice Department probe that barred them from agreeing not to poach each other's employees. The companies have since been fighting the civil antitrust lawsuit. They argue the plaintiffs cannot successfully prove an overarching conspiracy to impact wages.

Facebook is not the only Silicon Valley company which rejected a request to curtail its hiring efforts. In 2007, Apple co-founder Steve Jobs threatened to file a patent lawsuit against Palm if that company's chief executive didn't agree to refrain from poaching Apple employees, according to correspondence filed in court.

However, then Palm CEO Edward Colligan told Jobs that the plan was "likely illegal," and that Palm was not "intimidated" by the threat. "If you choose the litigation route, we can respond with our own claims based on patent assets, but I don't think litigation is the answer," Colligan said.

Trial in the hiring class action is scheduled to begin in May. The civil case in U.S. District Court, Northern District of California is In Re: High-Tech Employee Antitrust Litigation, 11-cv-2509.

Reuters