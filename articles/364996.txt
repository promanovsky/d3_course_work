Nikhil Subramaniam

Google launched Android Wear in India, with the LG G Watch and the Samsung Gear Live, while Motorola has also announced its Moto 360 plans to a certain extent. LG and Samsung have now released a new smartwatch each ahead of IFA (Samsung's Gear S also lets you make phone calls), where other brands will also come to the stage with wearables. And as if on cue, we also have reports about Apple's iWatch launch on September 9.

While Android Wear is well and truly here, it does not immediately mean Apple is at a disadvantage. More Apple iWatch details have come in since Google’s big Android Wear launch, and the company has hired Tag Heuer executive for the iWatch team. We are in for a real smartwatch battle later in the year.

Google definitely has a huge advantage in the smartwatch race. Given that Android is pretty much a byword for smartphones in many markets, the wearables-purposed OS should be familiar to many. And Google also got several prominent apps on board for the launch, which means Android Wear is good for use right from word go. By the time Apple’s smartwatch offering is in the market, we could have a lot more joining the initial ones. But here's how Apple could fight back in the market.

Build the ecosystem

However, there is some belief among analysts that the market is really waiting for an Apple product, before it can boom. Apple has the capacity to galvanise the entire wearables ecosystem with its iWatch, as we saw it do with iPods and iPhones. We have already seen new APIs for fitness and home automation that will no doubt boost the third-party accessories market, and we also know of the Made for iPhone programme, which could be extended for the iWatch as well.

The fashion angle

Apple’s recent hires clearly point to a fashion angle, which Google has only shown for its niche Glass. Apple will likely position the iWatch as a premium device, to go along with the premium next-gen iPhones. The design and looks will be big selling points for the iWatch, we have no doubt. We are also likely to see a high margin approach with the iWatch, which will draw in big revenue, where Apple still has the leg over Samsung and other vendors.

Historically, Apple has had great relation with the fashion industry, so we could be seeing designer straps and limited edition designs too. In the run up to the launch, Apple has hired Angela Ahrendts as its head of retail and the former Burberry executive would no doubt be bringing her fashion industry expertise to the job.

Health and smart homes

We also expect Apple to pack in big home and health features in the iWatch, something Google will also aim for in subsequent software updates, but has not outlined as well as Apple has at the moment in HomeKit and HealthKit. The iWatch along with the iPhone will be the pivotal piece in this equation.

Exclusivity

Among the many benefits iPhone users count within the ecosystem is the fact that many apps launch on iOS months and years before Android. The trend has dissipated in recent days, but there are quite a few iOS exclusive apps that have become fan favourites. Apple could incentivise iWatch-only development, which will add another measure of exclusivity to the device. The iWatch is also likely to reinforce the belief that staying within Apple’s ecosystem has great benefits with possible Continuity or Handoff functionality baked in.

Google has aimed for a relatively staid approach, without veering into boring territory while Apple will want the iWatch to be your 24x7 fitness and smart home companion that you would want to wear (and flaunt) all day, and a device which will be just as feature-packed as any smartwatch in the market.

Apple iWatch concept image by Gábor Balogh.