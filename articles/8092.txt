Of those who have signed up to date, the administration said, about 25 percent were ages 18 to 34, the same proportion as in the first four months of the open enrollment period, through January.

The number of people picking plans was smaller in February than in December or January. But administration officials predicted a surge of applications toward the end of this month, just before the open enrollment period ends.

WASHINGTON — The Obama administration said Tuesday that 943,000 people signed up last month for private health insurance under the Affordable Care Act, increasing the number of those who have selected health plans since Oct. 1 to more than 4.2 million.

Advertisement

Insurers and the White House have avidly sought young adults, saying their premium payments were needed to offset the costs of coverage for Americans who were older and presumably sicker.

The latest figures were released as the White House makes a final push to increase enrollment, which got off to a slow start because of the technical problems that crippled the federal insurance marketplace in October. Less than three weeks remain before the March 31 deadline for consumers to sign up.

Of the people who have signed up so far, the administration said, 2.6 million were in the federal insurance marketplace, and 1.6 million were in states running their own exchanges.

“Over 4.2 million Americans have signed up for affordable plans through the marketplace,” said Kathleen Sebelius, the secretary of health and human services. “Now, during this final month of open enrollment, our message to the American people is this: You still have time to get covered, but you’ll want to sign up today.”

The age profile of people signing up is somewhat older than insurers had expected. The number of people age 55 to 64 — 1.3 million — exceeds the number in the 18-to-34 age bracket, 1.1 million.

Advertisement

Insurers say that roughly one in five people who signed up for coverage failed to pay their January premiums. And some of those who paid and received services in January failed to pay premiums for February, putting their coverage at risk.

More than four-fifths of those choosing health plans to date — 83 percent — qualified for financial assistance to help pay their premiums, administration officials said.

People who go without insurance after March 31 may be subject to tax penalties. The Internal Revenue Service can deduct the penalties from any refunds to which taxpayers may be entitled.

New York Times — NEW YORK TIMES

Obama takes a turn ‘Between Two Ferns’

WASHINGTON — President Obama is hamming it up online to promote his health care plan.

Obama joked Tuesday with comedian Zach Galifianakis, including poking fun at the poorly reviewed ‘‘The Hangover Part III’’ during an interview on the website Funny or Die.

Galifianakis typically poses awkward questions to celebrity guests appearing on ‘‘Between Two Ferns,’’ and Obama was no exception. After being asked how it felt to be ‘‘the last black president’’ and about the false notion that he was born in Kenya, the president pitched sign-ups for health care by the March 31 deadline.

Obama’s appearance is part of an effort to reach young Americans crucial to the program’s success.

Asked whether he’d want a third term — which is barred by the Constitution — Obama said, ‘‘If I ran again it would be sort of like doing a third ‘Hangover’ movie. It didn’t really work out very well, did it?’’

Advertisement

Galifianakis is a star of the ‘‘Hangover’’ franchise.

— Associated Press ASSOCIATED PRESS

Senate OK’s bill backing pediatric medical research

WASHINGTON — The Senate Tuesday passed Republican-backed legislation to repeal taxpayer funding for political conventions and to go on record in support of devoting $126 million over the coming decade for additional research into pediatric cancer and other childhood disorders like autism and Down syndrome.

The measure was passed by unanimous voice vote at the request of minority leader Mitch McConnell, Republican of Kentucky. It now goes to President Obama for his signature. The House passed the measure late last year.

The legislation was named after Gabriella Miller, a Virginia girl who died of brain cancer last year at age 10. She had helped raise money for the Make-A-Wish Foundation and also sought to win public support for pediatric cancer research.

‘‘One courageous young girl, Gabriella Miller, inspired bipartisan action to help research, treat, and cure pediatric diseases and disorders,’’ said majority leader Eric Cantor, Republican of Virginia and a key House sponsor of the measure. “When people would remark that Gabriella was wise beyond her years, she would tell them that having a brain tumor means you have to grow up real fast.’’

Majority leader Harry Reid of Nevada said the measure is largely symbolic and doesn’t directly fund additional pediatric research; the actual funding would come in future appropriations bills. Funding for medical research through the National Institutes of Health has been squeezed in recent years under tight spending limits and automatic spending cuts.

Advertisement