Sen. Al Franken appears to be playing a cruel joke on the corporate higher-ups at Comcast.

After failing to block the Comcast-NBC Universal merger, Franken has emerged as one of the biggest detractors of Comcast’s proposed $45 billion buyout of Time Warner Cable.

As he turns up the heat on the deal, the Minnesota Democrat and former “Saturday Night Live” funnyman has enlisted fellow “SNL” alum Amy Poehler to help raise funds for his re-election campaign.

As it happens, Poehler is the star of “Parks and Recreation” on NBC, part of Comcast-owned NBCUniversal. She also co-hosted the Golden Globes that aired on NBC.

The political alliance hasn’t escaped the attention of some in Hollywood — even if Comcast failed to notice.

“These guys try so hard to keep up certain appearances, and this is one that falls through the net,” said one source.

Indeed, “Today” host Kathie Lee Gifford said NBC told her not to plug her new Gifft wine on the show while the Comcast-Time Warner Cable deal is pending.

In a Thursday e-mail to supporters, Franken’s campaign team announced that Poehler would be hosting a fundraiser in Los Angeles on April 24.

The humorous invite reads: “The only thing better than meeting me and Amy Poehler in Los Angeles on April 24 might be … I don’t know, I can’t think of anything.”

A campaign representative for Franken and Poehler’s publicist didn’t respond to requests for comment.

In addition to grilling Comcast during a Senate hearing, Franken called on Netflix to weigh in on the cable tie-up following its controversial “peering” deal with Comcast.