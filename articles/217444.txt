Less than six months after Microsoft (NASDAQ:MSFT) launched the Xbox One for $499 -- $100 more than the PlayStation 4 from Sony (NYSE:SNE), the company has announced that a version of the Xbox One without Kinect will be available in June for $399. The PlayStation 4 has been outselling the Xbox One by a significant margin, with VGChartz putting retail sales (as of May 3) at 7.5 million for the PS4 and just 4.5 million for the Xbox One. While this price cut eliminates Sony's price advantage, the Xbox One still has a serious problem.

Pulling a Sony

This situation is similar to that of Sony's PlayStation 3 during the previous console cycle. Sony launched the PS3 a year after Microsoft launched the Xbox 360, selling it for $599 compared to the Xbox 360's $399 price tag. On top of the massive price difference, the PlayStation 3's Cell processor was unfamiliar to developers and difficult to work with. While the PlayStation 3 had a lot of computational horsepower, it took years before developers were really able to harness it. This gave the Xbox 360 an advantage early on.

But price cuts eventually made the console more affordable, and both consoles have sold roughly the same number of units as of today. While the hole that Sony had dug for itself looked insurmountable at the time, the PlayStation 3 made a comeback. However, Sony's mistakes cost the company its market leadership, as the PlayStation 2 dominated the previous generation.

It looks like Microsoft is copying Sony's playbook, lowering the price in an attempt to close the sales gap. While Sony was confident that consumers would be willing to spend $200 extra on the PlayStation 3, Microsoft was overly confident that consumers would be willing to pay $100 extra for a Kinect. Both companies were wrong.

Price hasn't been the only problem for the Xbox One

Now that the Xbox One will sell for the same price as the PlayStation 4, it would seem that both units are on an even playing field. But this isn't actually true, and I suspect the price difference hasn't been the only reason for the Xbox One's under-performance. The PlayStation 4 is capable of playing cross-platform games at higher resolutions and/or frame rates, and while the average person may not be able to tell the difference, the Xbox One has a perception problem.

Two highly anticipated cross-platform games, Watch Dogs and Call of Duty: Advanced Warfare, illustrate the problem that the Xbox One faces. Watch Dogs will run at a resolution of 792p on the Xbox One while the PS4 will be able to handle a resolution of 900p. The difference for Call of Duty: Advanced Warfare is even greater. While both consoles will run the game at 60 frames per second, the PS4's full 1080p resolution puts the estimated 882p resolution of the Xbox One to shame.

These differences are odd because both consoles are powered by similar Advanced Micro Devices hardware. The Xbox One could simply be more difficult to develop for, or the non-game stuff running in the background (like Kinect) could be eating up valuable processing power. Whatever the reason, an undecided gamer seeing article after article saying that the PS4 will run so-and-so game at a higher resolution than the Xbox One isn't going to stay undecided for long.

The bottom line

Microsoft made a mistake in assuming that gamers would be willing to pay $100 extra for Kinect, and it has corrected that mistake with a new $399 version of the console. While the removal of Sony's price advantage should help boost sales, the Xbox One still faces the perception that it is a less powerful machine. This is a problem for Microsoft, as every article written about how a new game will run better on the PlayStation 4 is free advertising for Sony.

Can the Xbox One make a PS3-like comeback? Sure. But just like Sony during the previous console generation, Microsoft has likely blown any chance it had of becoming the undisputed market leader.