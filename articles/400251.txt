Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

A doctor who treats patients with the Ebola virus is “terrified” after catching the disease himself.

US medic Kent Brantly, 33, fell ill despite wearing full protective gear.

He wrote in an email to a former colleague in Texas: “I’m praying fervently God will help me survive this disease.”

Dr Brantly is being treated in an isolation unit in the Liberian capital of Monrovia. He moved to the country in October to work with a Christian group.

Wife Amber and their two young children had returned to the US for a trip last week before he found out about his condition. They have shown no signs of the highly contagious viral infection.

His proud mother Jan, 72, said: “Kent prepared to be a lifetime medical missionary. His heart is in Africa.”

Read the latest on the Ebola outbreak here.

Earlier this week, Dr Brantly's colleagues at Samaritan's Purse said they were doing everything possible to ensure he received the best possible care.

Dr Brantly joined the Christian international relief organisation after finishing a residency at John Peter Smith Hospital, in Forth Worth near Dallas.

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Robert Earley, the President of the Texas hospital, said Dr Brantly’s former colleagues were extremely upset and shocked after learning of the news.

Mr Early singled out the doctor's work and his dedication to his patients.

“This is the kind of individual that he is," he said.

“They go into the worst situations in the world and try to save lives."

(Image: Daily Mirror)

Ebola has killed 672 people in Liberia, Guinea and Sierra Leone since it broke out in February.

Today there are fears that the outbreak could spread to countries around the world, after American victim Patrick Sawyer was allowed on two flights while he was infected.

Health authorities are trying to trace to dozens of passengers who flew with the 40-year-old.