German upturn driven by domestic demand

Share this article: Share Tweet Share Share Share Email Share

Frankfurt - Growth of the German economy, Europe's biggest, was driven exclusively by booming domestic demand in the first quarter, while foreign trade put a brakes on activity, data showed on Friday. German gross domestic product (GDP) expanded by 0.8 percent in the period from January to March, the federal statistics office Destatis said in a statement, confirming a flash estimate released a week ago. That is the biggest GDP increase in three years. “Positive impulses came exclusively from domestic demand,” the statisticians said. “In particular, investment picked up at the start of the year, with a 3.3-percent rise in investment in equipment an construction investment up 3.6 percent,” the statement said.

In addition, consumer spending increased by 0.7 percent and public spending by 0.4 percent.

By contrast, net foreign trade - the balance between exports and imports - knocked a total 0.9 percentage point off the overall growth rate, Destatis said.

While imports jumped by 2.2 percent, exports edged higher by just 0.2 percent, with goods exports falling by 0.5 percent.

Following its strong start to the year, German growth is nevertheless expected to come in softer in the second quarter, experts predict, as the first-quarter performance received an additional boost from the extremely mild weather.

It is also too early to gauge what effect the Ukraine crisis will have on growth, experts say.

In all, the German economy is expected to expand by 1.7-1.9 percent across the whole of 2014, according to most forecasts. - Sapa-AFP