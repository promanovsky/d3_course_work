Research shows much development of the brain occurs in the first three years of a child’s life — and one of the best ways to build a strong base for language and cognitive development is to read and talk to your babies. However, by age 4, children in poverty hear 30 million fewer words than their wealthier peers. National data has shown that fewer than half of children under the age of 5 are read to daily by a parent.

The Academy, which has 62,000 members, is partnering with Scholastic, which will donate 500,000 books through Reach Out and Read, a nonprofit of medical professionals who donate books to low-income families.

“Reading is the birthright of every child and a door opener to leading successful lives,” said Greg Worrell, president of Scholastic Classroom and Community Group. “We know that the more children are exposed to books, the better off they’re going to be. The unfortunate reality is there just isn’t access to books. Sixty percent of low income families don’t have a single book for children at all.”

AD

AD

The 500,000 books are being printed specifically for this purpose and will go out in two installments in January and June of next year. The groups are working with Too Small to Fail, an organization that focuses on the well-being of children up to age 5. (It is co-chaired by Hillary, Bill and Chelsea Clinton, as well as Next Generation.)

Among children in low-income families, 79 percent entering kindergarten do not know the letters of the alphabet, according to data released by Scholastic and the Bill and Melinda Gates Foundation. It is the hope of the AAP and Scholastic that this program will help kickstart that process earlier.

This is the first time the AAP will include early literacy as an essential component of primary care visits.