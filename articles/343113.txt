Rolf Harris visited Broadmoor Hospital and may have been able to watch female patients undress, an independent investigator who compiled the report into Jimmy Savile’s activities has conceded.

The West London Mental Health NHS Trust confirmed to The Independent that Harris is understood to have visited the hospital in 1973 accompanied by Savile. A spokesperson for the Trust said the disgraced entertainer was escorted by staff at all times and there was no suggestion of inappropriate behaviour or incident during the visit.

In a statement, independent investigator Dr Bill Kirkup CBE said it was usual practice then for female patients to have to undress in front of “staff and others”, and Harris may have seen patients undress.

The report found that Savile would watch female patients as they stripped and undressed while lined up in corridors in front of staff, a practice which was common until at least the late 1980s.

It states: “Until at least the late 1980s, female patients were obliged to strip completely to change into nightwear and to take baths, watched by staff. We conclude that Savile would sometimes attend wards at these times and watch.”

Rolf Harris: A life in pictures Show all 20 1 /20 Rolf Harris: A life in pictures Rolf Harris: A life in pictures Rolf Harris Rolf Harris in 'Stars on Sunday' TV Programme (1969 -1979) Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his daughter painting a wall together, 1967 Rolf Harris: A life in pictures Rolf Harris Rolf Harris on the 'Rolf Harris Show', 1973 Rolf Harris: A life in pictures Rolf Harris Rolf Harris in 1968 Rolf Harris: A life in pictures Rolf Harris Rolf Harris smiles during the 'The Rolf Harris Show' in 1973 Rolf Harris: A life in pictures Rolf Harris Rolf Harris with art book he wrote for children in London, 1978 Rolf Harris: A life in pictures Rolf Harris Australian entertainer Rolf Harris gets ready to blow his didgeridoo to promote a concert at Central Hall, Westminster, staged to raise money for research into cancer in children at the Royal Marsden Hospital. Keystone/Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris sketches a picture of Bambi, 1986 Rolf Harris: A life in pictures Rolf Harris bbc Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his wife at the David Frost's Society Party in London, 2001 Rolf Harris: A life in pictures Rolf Harris Rolf Harris performing at Glastonbury 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris performs in 'The Rolf Harris Show' in 1973 Rolf Harris: A life in pictures Rolf Harris "Rolf Harris" book signing in London, 2010 Rolf Harris: A life in pictures Rolf Harris Rolf Harris with his portrait of the Queen at a London art gallery in 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris at the Daily Mirror's Pride Of Britain Awards 2012 in London Rolf Harris: A life in pictures Rolf Harris Rolf Harris and his wife Alwen attend the Press & VIP preview at The Chelsea Flower Show at Royal Hospital Chelsea in London, 2010 Getty Images Rolf Harris: A life in pictures Rolf Harris Rolf Harris surrounded by media leaves City of Westminster Magistrates Courts in London, 2013 Rolf Harris: A life in pictures Rolf Harris Entertainer Rolf Harris and his wife Alwen Hughes (L) arrive at Southwark Crown Court in central London, 6 May, 2014 Reuters Rolf Harris: A life in pictures Rolf Harris Rolf Harris is surrounded by members of the media as he leaves Westminster Magistrates Court, in central London, 2013 Rolf Harris: A life in pictures Rolf Harris Rolf Harris arrives at Southwark Crown Court in London, 27 June 2014

However, there is no mention of other people outside of other patients, staff and Savile being able to see patients in a state of undress.

Independent investigator Dr Bill Kirkup CBE told The Independent: “The investigation team thoroughly examined the evidence of Rolf Harris’s activities at Broadmoor Hospital when he visited the hospital once in 1973.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Video: Rolf Harris meets Jimmy Savile

“It is important to stress that Harris never had keys to Broadmoor Hospital nor was he given unrestricted access to patient wards as Jimmy Savile was. Harris visited the hospital only on that one occasion."

One ex-patient, Steven George, who attended Broadmoor before undergoing a sex change, claimed Harris turned up one evening “out of the blue” as patients were getting ready for bed – outside of the 10am to 4pm visiting hours.

George, born Alison Pink, said of the visit: “He was being shown around by Savile in an understated way. Normally stars only came if they were there for an official performance but Harris didn’t do one.

“It was also unusual because visitors would come at visiting hours, between 10am and 4pm, but they came in as we were getting ready for bed.”

A spokesperson for the hospital said that "others’ referred to staff, Savile and other patients, and said it could not confirm what time Harris visited the hospital. They said the investigation did not state the nature of his visit. They added: “If anyone has further evidence to give to the investigation, the team at Broadmoor Hospital can be contacted on 01344-754122.”