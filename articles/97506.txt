Samsung launches its Galaxy S5 and Gear range on the same day to take a swipe at Apple



Samsung's Galaxy S5, Gear Fit and Gear 2 watch range is now available

Devices have launched in 125 countries including the U.S. and Europe

Prices for a SIM-free handset start at £570 in the UK and $649 in the U.S.

Gear Fit costs £169 and $199, and the Gear watches start at £199 and $199



Samsung is offering device owners free access to FIFA 14 game

Users in select countries also receive a six-month free Deezer subscription



More than a month after Samsung unveiled its next generation of gadgets, they have hit the high street.



Samsung's Galaxy S5, Gear Fit, Gear 2 and Gear 2 Neo watches have launched in 125 countries across the U.S., Europe, Middle East and Latin America.

By releasing four products on one day, the South Korean firm has made a major play in its consumer war with rival Apple - and experts claim it cements the company's place at the top of the smartphone market.

Samsung's Galaxy S5 and Gear Fit and Gear 2 watch range, pictured, have launched in 125 countries

GALAXY S5 SPECIFICATIONS

Its dimensions are 142.0mm x 72.5mm x 8.1mm. It weighs 145g 5.1-inch FHD Super AMOLED (1920 x 1080) screen

2.5GHz quad-core application processor

Android 4.4.2 (KitKat) operating system

16MP (rear), 2.0MP (front) camera

Ultra HD 30fps video recording

Selective Focus camera Finger scanner and heart rate sensor

Dust and water resistant

Ultra power saving mode

WiFi: 802.11a/b/g/n/ac HT80, MIMO(2x2)

IR Remote

2800mAh battery - Standby time: 390 hrs/Talk time: 21 hrs

Andy Betts, editor of UK-based Android Magazine said: 'The Galaxy S5 is not a massive upgrade over last year's S4, but what has been improved should be more than enough to ensure that Samsung retains its dominance of the market.

'With an improved camera, much longer battery life and water resistance, Samsung has targeted areas that, while not necessarily producing eye catching headlines, will deliver real benefits for users.'

'It's undoubtedly going to be a very, very popular phone. Over to you, Apple,' added T3 magazine's Jeff Parsons.



Prices for a SIM-free Galaxy S5 start at £570 in the UK and $649 in the US, while the Gear Fit costs £169 and $199 and the Gear 2 and Gear 2 Neo start at £199 and $199.

In the UK, Germany and the Netherlands, customers who buy the Galaxy S5 additionally get a free six-month subscription to music service Deezer Premium+.



This service usually costs €9.99 (£8) a month.

All handsets come with FIFA 14, available exclusively on the Galaxy S5 and other Galaxy devices.



The devices were unveiled during February's Mobile World Congress event in Barcelona.



The move would place it in direct competition with the recently-launched Samsung¿s Galaxy S5 (pictured) which also features a plastic shell. The M8 is also expected to have almost identical features to its competitor, including a 5-inch 1080 pixel display plus a 2.5GHz quad-core Snapdragon 801 processor

Like Apple's iPhone 5S, Samsung's fingerprint scanner, 'provides a secure, biometric screen-locking feature'. However, the real innovations come inside with super-fast Wi-Fi and a host of fitness functions, claimed the company

The S5 features a built-in heart monitor, pedometer, fitness apps and fingerprint recognition.



It is at the head of a move by tech giants to turn the ever more sophisticated mobile phone into a substitute doctor and personal trainer.



Samsung’s S5 has its own health functions, but it will also connect to the company’s second generation smartwatch, the Gear 2, as well as a new wearable band device, the Gear Fit, which will play a role in measuring fitness.

The watch and fitness band can pick up the pulse through the skin on the wrist, and display it on the small screen throughout a normal day, or during a workout.



SAMSUNG GALAXY S5: PRICES AND DEALS FROM HIGH-STREET SHOPS

UK PRICES

Carphone Warehouse A 16GB SIM-free Galaxy S5 costs £569.95 It is available for £42 per month with no upfront cost, or from £33 per month for customers trading in any old phone - on top of the trade in value of their old handset. The firm is also offering customers who trade in an old handset when purchasing the new S5 £100 on top of the trade in value. For example, trading in an iPhone 5 16GB could get up to £290 cashback, while an Samsung Galaxy S4 could get up to £261.50 cashback. Vodafone Contracts on 4G start at £42, with a £19 upfront fee and 5GB of data.

The 3G contracts start at £29 a month and an upfront fee of £229 for just 250MB of data. People who pre-ordered the device will additionally receive extra data for free. The amount of data depends on the contract, but if a customer has pre-ordered the S5 on the 4G Red 4G plan, for example, their data allowance is increased from 3GB to 5GB.

Phones 4U Phones4U is also offering the Galaxy S5 for £569.95, or with a pay-as-you-go SIM from Vodafone, Orange and T-Mobile for £559.95. Contract deals start from free, but go up to £42 per month. Three

Three customers can get the handset for an upfront cost of £69. The cheapest contract starts at £38 with 2GB of data, up to £44 a month for unlimited data. Three also offers 4G at no extra cost on all compatible handsets and contracts, plus no roaming charges in select countries. O2 In order to get the handset for free on O2, customers have to sign up for a £48-a-month contract, or £38 a month if they pay £69. EE For £59 upfront, EE is offering the Galaxy S5 for £39.99 a month, this comes with 2GB of 4G data.

For £33 a month, buyers can get double speed 4G of data but have to pay £260 upfront for the handset .

US PRICES AT&T The SIM-free S5 on AT&T costs $649.99. The AT&T Galaxy S5 is available for $199.99 on a two-year contract. With the Next 18 plan, the Galaxy S5 costs $25 per month for 26 months, and with the Next 12 plan, the device is available for $32.50 per month for 20 months. Sprint The handset from Sprint costs $649.99. It will be available for $199.99 on a two-year contract.

Sprint's Easy Pay plan includes a free handset, $27.09 per month for 23 months, with a final payment of $26.92. T-Mobile T-Mobile is selling the handset for $660. On contract it costs $27.50 per month for 24-months, under T-Mobile’s Simple Choice plan. U.S. Cellular The U.S. Cellular Galaxy S5 is $199.99 on a two-year contract, with a $50 Google Play voucher. Verizon For $249.99, customers can get the S5 from Verizon on a two-year contract, which comes down to $199.99 after a $50 trade-in discount. Verizon Edge contracts cost $25.22 per month for 24 months. It’s selling the handset for $599.99. Target Target is offering the Galaxy S5 on Verizon, AT&T and Sprint models for $100, through a trade-in program. Customers can exchange eligible old devices for a $100 credit toward a new Galaxy S5, which originally costs $199.99 on contract. Target is running this offer until April 26.



All of the information gathered by the devices can be used to calculate the number of steps taken, calories burned and general fitness.

The firm’s ‘S Health 3.0’ software creates graphs or tables of the results to track changes in activity and health over time.



Samsung’s apps can then develop a fitness regime, including a range of activities from jogging to climbing the stairs rather than using the lift, to help people improve their health.

'We’ve decided to go back to basics with the Galaxy S5 and focus on the features and things that matter the most to our customers - namely the camera, ability to view and download data and content quickly and their health and wellbeing,' said Simon Stanford, vice president of IT and mobile division, Samsung Electronics UK and Ireland.

'People want a smartphone that enhances and enables their mobile life.



'The Galaxy S5 represents just that and we’re excited to see the response to our latest flagship smartphone device.'

The watch and fitness bands, pictured, can pick up the pulse through the skin on the wrist and display it on the small screen throughout a normal day, or during a workout. All of the information gathered by the devices can be used to calculate the number of steps taken, calories burned and general fitness

The S5 has a 16MP camera - an improvement on the 13MP camera seen on the S4 - and features a number of advanced editing and focus tools similar to those on the 5S.



The new camera has twice as many megapixels as Apple's iPhone 5S, for example.



This camera has phrase-detect, as well as contrast-detect autofocus, which Samsung claims is the fastest autofocus in any smartphone.



Samsung's Galaxy S5 has also got a better battery life than its predecessor.



Even when the battery is on 10 per cent, it has 24 hours of time left before it gives up the ghost, said the firm.



The smartphone is also the first with Wi-Fi MIMO, which Samsung claims nearly doubles Wi-Fi connection speeds.

MAILONLINE'S VERDICT - AN IMPRESSIVE DEVICE



During the launch event, Samsung’s president and CEO JK Shin stressed to the audience that the firm had focused on ‘simple, practical’ enhancements, rather than trying to reinvent the wheel – but that almost plays down the Galaxy S5’s achievements.

It’s an impressive device with a wide range of impressive features.

Samsung has gone almost back to basics by focusing on three areas – battery life, durability and the camera. During our brief hands-on we couldn’t fully test the battery claims, but on durability, the device feels much sturdier than the Samsung S4.

The Galaxy S5, pictured, has handpicked features from it's competitors and taken it's lead from the professional photo industry to launch an outstanding device It’s still a large device and this takes a little getting used to, but compared to some of the larger models launched at today’s Mobile World Congress, the S5 feels comfortable and natural to hold.

The heart rate scanner seems like a gimmick - especially when sold alongside the Gear Fit designed especially for, and more suited, to fitness – and the novelty wore off quite quickly. It’s also slow; it took almost nine seconds to take a reading every time we tried. The position of the scanner felt awkward too.

The overall design, though, is a vast improvement.

It’s still made of plastic, so doesn’t have the premium feel of say an Apple iPhone, but its weight and smooth casing puts it on the right path.

We were also surprised with the quality of the display, colours appear sharp and bright, and noticeably brighter than on the iPhone 5S and Nokia Lumia 1020 when side-by-side.

We were unable to fully test the water resistance, camera features and power modes.



On to the design of the Galaxy S5, its case has a perforated pattern on the back cover creating a 'modern glam look'.

Its new shape comes in Charcoal Black, Shimmery White, Electric Blue and Copper Gold. It is dust and water resistant.



Like Apple's iPhone 5S, Samsung's latest handset has a fingerprint scanner, providing a secure, biometric screen-locking feature.

VP of electronic communications at Samsung, Jean-Daniel Amye, said: 'The finger scanner uses your fingerprint as the ultimate password to your identity.



'All it takes is a simple swipe of your finger to make secure payments.



'Also finger scanner unlocks Private mode where secure files can be stored. I can now keep my passport in a secure way'.

The Galaxy S5, pictured left, has a 16MP camera - an improvement on the 13MP camera seen on the S4 - and features a number of advanced editing and focus tools similar to those on the 5S and 'flattened icons', pictured right, similar to its high-end Apple counterpart



HOW THE GALAXY S5 STACK UP AGAINST APPLE'S IPHONE 5S



Samsung Galaxy S5

Apple iPhone 5S 5.5 x 2.85 x 0.31 inches; 5 oz 4.87 x 2.31 x 0.30 inches; 4 oz. 5.1-inch full-HD IPS; 1,920x1,080 pixels; 431ppi 4-inch Retina Display; 1,136x640 pixels; 326ppi

16 Megapixel, UHD Video

8-megapixel; 1080p full-HD video Fingerprint Scanner Fingerprint Scanner

Rob Skinner, director of PR at PayPal UK, said the world is moving away from passwords that are a hassle and easily forgotten.

'Combining Samsung's fingerprint authentication with PayPal means users will no longer need to remember passwords or login details when they shop. This new method strikes the perfect balance between convenience and security and we believe this will be the future for 2014 and beyond,' he said.



JK Shin, president and CEO of Samsung Electronics said: 'Samsung has been one of the major contributors of the mobile industry's high growth. Especially, the Galaxy S series has been a key driver of smartphone growth.

'A total of 200 million people use Galaxy S devices, he added.

'People are easily excited about technology - disruptive and breakthrough. We're interested in meaningful innovation. It's surprisingly simple.

'ONE TRUE SUPERPHONE' - WHAT THE EXPERTS SAY

On paper, the Galaxy S5 is packed with more features than any other smartphone currently available, thanks to a combination of its fingerprint sensor, heart rate sensor, clever camera tricks and waterproof design. It's taken the most innovative features from other phones - the Xperia Z1's waterproof design, the iPhone 5s' fingerprint sensor - and combined them all into one true superphone.

Esat Dedezade, Stuff

There's no question the S5 is an iteration on the S4, rather than a complete overhaul. But they're all iterations in strong areas. At our briefing for the Galaxy S4 we were told the then-new phone would let you scroll text up and down by tilting the phone. This time we're told we can shoot HD videos with high-dynamic ranges applied in real-time, underwater. We'll let the reader decide which of those two announcements is more exciting.

Nate Laxon Wired

The heart rate monitor might be a bit of a gimmick and even the fingerprint sensor could be considered that - but they're good to have as extras and to add greater potential future software development options. Combined with the Gear 2 or Gear Fit the Samsung Galaxy S5 is a complete life tracker and complete life managment device - we can't wait to get our review sample to really push its apparently near-boundless limits.

Luke Edwards Pocket Lint

Like the new Gear wearables, the Galaxy S5 looks and feels familiar, but offers a number of improvements over last year’s edition. Samsung had a successful formula with the Galaxy S4, and for the most part, it looks like it has retained that with the S5. Things are faster, nicer feeling, and easier to use, but it’s still a Samsung smartphone through and through, and will likely be just as successful if not more so than its predecessor.

Dan Seifert The Verge

All told, it's very much a run-of-the-mill Galaxy S flagship, but there are enough new hardware features and software tweaks to make it feel fresh.

Brad Molen, Engadget



The Galaxy S5's shape is available in Charcoal Black, Shimmery White, Electric Blue and Copper Gold. It is dust and water resistant. The colours can be seen here on show at the launch event in Barcelona in February

GEAR FIT SPECIFICATIONS

Display: 1.84inch curved Super AMOLED

Includes: Pedometer, exercise and heart rate sensors, stopwatch and timer

Notifications: For SMS, calls, emails and apps

Strap: Changeable in three colours, the band is IP67 dust and water resistant

Connectivity: Bluetooth v4.0 LE Dimensions: 23.4mm x 57.4mm x 11.95 mm Weight: 27g Battery: 210mAh with 3-4 days of typical usage or 5 days of low usage

'Our customers don't want eye-popping technology, they want design. Our consumers want a simple yet powerful camera.



'Our consumers want powerful and seamless connectivity and our consumers want a phone that helps them stay for,' he said.



Elsewhere, the Gear Fit gives users notifications about SMS, missed calls, while also serving as a way to monitor their heart rate while exercising.

The Samsung Gear Fit is designed to 'comfortably contour to the wrist while offering a vivid Super AMOLED display,' Samsung said.



The company claims it provides 'much more' than other fitness bands.



For example, it keeps users up to date with incoming calls, emails, SMS, alarm, S-planner and third party apps while they are on the move so they can stay connected.



For style-conscious wearers there are changeable straps in black, orange and grey and it's water resistant.

Samsung's CEO JK Shin unveiled Samsung's Gear Fit in February by simply pulling back the sleeve on his left arm. The wearable device, pictured, gives notifications about SMS, missed calls, and the like, while also serving as a way to monitor a person's heart rate while exercising

WHO IS UPGRADING TO THE SAMSUNG GALAXY S5?

According to a survey by CompareMyMobile, 18.75 per cent of current Samsung owners are upgrading to the new S5 device.

More than two thirds of Apple customers are upgrading to the S5, while 6.27 per cent of HTC owners are considering buing the new Samsung device.

Top handsets traded in for the Galaxy S5: 1. Apple iPhone 4S – 210 percent spike 2. Samsung Galaxy S3 – 196 percent spike 3. Apple iPhone 5S – 184 percent spike

4. Sony Xperia Miro – 182 percent spike 5. Nokia Lumia 1020 – 147 percent spike

Weighing 27g, the Gear Fit offers real-time fitness coaching and personalised workout advice and is compatible with 20 Galaxy devices.



Interestingly, there are built-in heart rate monitors in both the Gear 2 and Gear Fit and the company admitted that users would have an 'overload' of sensors if they had both the Gear 2 and Gear Fit.

Mr Shin said: 'Gear 2 and Gear Fit - quite a team. 'Well, if you buy both and the S5, you'd have an overload of heart rate monitors. Surely one is sufficient.'

While there appears to be a fair amount of overlap with Samsung's new products, it's clear that the firm is making each one marginally different to attract the widest range of users as possible.

It's unlikely one customer will buy all three but Samsung is undoubtedly hoping that by attracting a new user to one or two of its devices it will develop a loyalty that will last into future releases.



The Gear 2 smartwatch, unveiled at the weekend, lasts three times longer than the first watch, which was criticised for being chunky and expensive at £300.

Interestingly, there are built in heart rate monitors in both the Gear 2 , pictured centre, and Gear Fit, pictured right, and the company admitted that users would have an 'overload' of sensors if they tried to use both the Gear 2 and Gear Fit alongside the new handset, pictured left

However, experts said the phone maker could still struggle to persuade users to upgrade.

The updates are so minor that on first glance most consumers would be hard pressed to notice that it has changed from the previous version,' Nick Dillon, senior analyst at Ovum said.



'Still, this should come as no great surprise, given the maturity of the smartphone market and the pressure on the Samsung not to mess with its winning formula.



'Samsung reminded us quite how successful this formula has been, noting that it has sold 200 million Galaxy S devices since launching the franchise in 2010.'