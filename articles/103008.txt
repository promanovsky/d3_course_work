TECHNOLOGY

Report: Amazon smartphone this year

After years of rumors, Amazon.com might finally be getting into the smartphone business.

The Wall Street Journal reported that the Seattle e-commerce giant will release a smartphone in the second half of the year, citing people briefed on the company’s plans. Those people said Amazon plans to announce the phone by the end of June and begin shipping units by the end of September.

A smartphone would be the latest hardware addition for Amazon, which already competes against Apple and Samsung in the tablet space with its Kindle Fire. Its newly announced Kindle Fire TV is an Internet video-streaming set-top box that is going up against the likes of Apple TV, Roku and Google Chromecast. If Amazon follows its tablet and e-reader pricing model, consumers can expect a mid-priced smartphone.

The Journal said Amazon has been demonstrating versions of the phone to developers in recent weeks. The phone reportedly features a screen capable of displaying 3-D images without special glasses.

Amazon didn’t immediately return a call or e-mail for comment. Amazon chief executive Jeffrey P. Bezos owns The Washington Post.

— Los Angeles Times

WALL STREET

Rocky week ends on a downer for stocks

U.S. stocks slid in a volatile session Friday, with the Nasdaq closing below the 4000 mark for the first time since early February.

For the week, the Standard & Poor’s 500-stock index fell 2.6 percent and the Nasdaq lost 3.1 percent, the biggest weekly decline for both indexes since June 2012.

The Dow Jones industrial average fell 143.47 points or 0.9 percent, to end at 16,026.75. The S&P lost 17.39 points or 0.9 percent, to finish at 1815.69. The Nasdaq composite dropped 54.38 points or 1.3 percent, to close at 3999.73.

JPMorgan Chase shares fell 3.7 percent to close at $55.30. The stock was the biggest drag on the S&P after the bank reported a far weaker-than-expected quarterly profit as revenue from securities trading fell.

The Nasdaq biotech index fell 2.8 percent after rising as much as 1 percent earlier. The Global X social media index, which includes Facebook and LinkedIn , slid 2.3 percent. Facebook shares fell 1.1 percent to $58.53. LinkedIn shares lost 2.5 percent to end at $165.78.

In contrast to the day’s sharp downturn, shares of Wells Fargo rose 0.8 percent to $48.08 after the biggest U.S. mortgage lender reported a 14 percent increase in first-quarter net profit.

Shares of Herbalife sold off late in the day after the Financial Times reported that the Justice Department and the FBI had launched a probe into the company. The stock tumbled 14 percent to close at $51.48. Herbalife denied the report.

— Reuters

Also in Business

— From news services

Coming Next Week