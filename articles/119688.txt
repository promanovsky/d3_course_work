Not quite the model Twitter tweet

Share this article: Share Tweet Share Share Share Email Share

Washington - US Airways rushed out an apology Monday after a graphic sexual image was tweeted from its official Twitter account in response to a disgruntled customer. The company has been known to respond rapidly and gracefully to complaints on social media, particularly about delayed flights or lost baggage. This time was different. The company triggered shock, howls of protest and a deluge of crude jokes on the Internet when they responded to Twitter user (at)ellerafter by attaching an extremely not-safe-for-work photograph of a model of a Boeing 777 jet plane in a woman's vagina. “Elle” had complained that her flight sat for an hour on the tarmac, prompting an apology from the airline's Twitter feed, (at)USAirways.

After a second complaint by the customer, the company responded again, saying “We welcome feedback, Elle. If your travel is complete, you can detail it here for review and follow-up,” the company wrote.

But inexplicably the graphic and uncensored image was attached to the tweet.

The airline quickly apologised on Twitter, then offered an explanation after its investigation.

“We captured the tweet to flag it as inappropriate. Unfortunately the image was inadvertently included in a response to a customer,” US Airways said, without clearly explaining the technical steps taken.

“We immediately realised the error and removed our tweet,” communications coordinator Valerie Hooks said in a statement, adding the company would study how to avoid such errors in the future.

The offending image had been sent by another Twitter user earlier in the day, to American Airlines, which is undergoing a merger with US Airways.

Reaction in the Twittersphere was swift and harsh, both at US Airways for posting the picture, and at “Elle,” whose Twitter account apparently has been used frequently to complain about US corporations. - Sapa-AFP