The race to fill vehicles with the latest technology is hurting quality.

Buyers reported more problems in their new cars and trucks this year than last year, according to rankings released Wednesday by the consulting firm J.D. Power. It was the second straight increase since 2012, when the average number of problems in new cars and trucks hit an all-time low.

Car makers are feeling pressure to add new technology and update vehicles quickly so they don't get stale. But car buyers are frustrated with Bluetooth systems that won't connect to their phones, voice recognition systems that don't understand them and navigation systems that aren't getting them where they need to go.

"Anytime you make a significant change to a vehicle you have the opportunity to introduce more problems," said David Sargent, vice president for global automotive at J.D. Power.

Porsche, Jaguar, Lexus and Hyundai and Toyota were the best-performing brands in this year's survey. The worst performers were Fiat, Jeep, Mitsubishi, Scion and Mazda.

Sargent said younger buyers are more likely to complain about problems than older drivers, who are more tolerant of mechanical issues.

"Younger drivers didn't grow up in an era where cars had things that fell off," Sargent said. On the flip side, he said, younger drivers are less frustrated by the technology in their cars.

This year's rankings were compounded by the harsh winter in the Northeast and Midwest. In those areas, there were more complaints than usual about heating systems, engine and transmission problems and exterior trim and paint problems, Sargent said.

Buyers reported an average of 116 problems per 100 vehicles in the first 90 days of ownership, up 3% from last year. J.D. Power surveyed 86,000 owners between February and March of this year. One major brand, Tesla, isn't included because J.D. Power didn't have enough data from owners.

The survey is the first major assessment of quality for 2014 vehicles, and it's closely watched by car shoppers. Consumer Reports magazine's influential quality study comes out in October and includes other years.

Among the findings:

WINNERS AND LOSERS: Porsche, which won for the second year in a row, had 74 problems per 100 vehicles. Fiat was last with 206.

MOST IMPROVED: Ford climbed 11 spots to 16th in the rankings, more than any other automaker. Sargent said the company's MyFordTouch infotainment system is seeing fewer complaints as Ford makes improvements. Ford's luxury Lincoln brand also climbed seven spots into the top 10.

BIGGEST SLIDES: Acura and Infiniti both slid 19 spots in the rankings as they introduced several new models, including the Acura MDX crossover and the Infiniti Q50 sedan.

BEST CORPORATE PERFORMANCE: General Motors had six winners in the segment awards, including best small SUV (Buick Encore), best midsize car (Chevrolet Malibu) and best heavy-duty pickup (Chevrolet Silverado HD). Hyundai earned five, including best small car (Elantra) and midsize premium car (Genesis).