I know that when it comes to design, many people would place Apple right there at the very top, but I am quite sure that the other players in the market have also managed to play catch up over the years. Take South Korean consumer electronics giant Samsung for example. When they first came up with mobile phones, Nokia ruled the roost with their designs, and Samsung was, well, Samsung. Using cheap looking plastic that has been cobbled together, their first truly breakthrough handset was the Samsung A200 in my opinion, sporting a blue glow that was otherworldly. Samsung has since then become a tour de force in the world of phones, and their upcoming Samsung Galaxy S5 looks set to make plenty of waves to boot.

Advertising

In fact, Samsung has decided to open up their design brains, so to speak, to the rest of the world by sharing the consideration and thought processes that has gone into its design, with plenty of focus being placed on its Galaxy range of smartphones. Samsung has teased the world with a new design site that is tipped for launch on March 27th, which is happening next week.

The YouTube video that you watch above does suggest that there will be no shortage of design tales throughout its product range, not to mention opening the window to a future of conceptual floating displays, bendable handsets and the ilk.

Filed in . Read more about Samsung.