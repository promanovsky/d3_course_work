Scientists have known for decades that Greenland’s ice sheet is melting, but they may have underestimated just how much water the second-largest ice sheet on the planet is shedding. New research indicates that a key section of northeast Greenland thought to be stable is actually dumping billions of tons of water into the ocean annually after a barrier of ice debris that had blocked its flow finally gave way.

"We're seeing an acceleration of ice loss," Michael Bevis, professor of earth sciences at Ohio State University and co-author of a new study on Greenland’s melting ice sheet, told USA Today. "Now, there's more ice leaving than snow arriving."

The study, published in the journal Nature Climate Change, included the work of an international team of researchers from Denmark, the Netherlands, the U.S. and China. According to AFP, the team measured the thickness of Greenland’s ice using four satellites and a network of 50 GPS sensors along the island’s coast. The monitors calculate the size of the Greenland ice sheet using Earth’s natural elasticity. When ice melts, it relieves pressure on the land underneath it, causing the ground to rebound just slightly. The monitors can sense these small changes.

The data showed that between 2003 and 2012, the northeast region of Greenland’s ice sheet retreated 12.4 miles following a three-year stretch of particularly high temperatures. The melting ice dumped 10 billion tons of water into the ocean every year during that time. According to researchers, the island is estimated to contribute .5mm to 3.2mm (.012 inches to .13 inches) to the annual rise in global sea levels.

“These new measurements show that the sleeping giant is awakening and suggest -- given likely continued Arctic warming -- that it’s not going back to bed,” Jason Box, a glaciologist at the Geological Survey of Denmark and Greenland, told Climate Central.

The ice melt in the northeast region of Greenland is particularly troubling because the ice stream there, known as Zachariae, stretches more than 370 miles into the island’s center where it joins the heart of Greenland’s ice reservoir. This “river” carries melting ice water from Greenland’s interior to the oceans.

"The Greenland ice sheet has contributed more than any other ice mass to sea-level rise over the last two decades and has the potential, if it were completely melted to raise global sea level by more than seven meters,” Jonathan Bamber, a professor at Britain's University of Bristol, told AFP.

Greenland is the world’s largest island holds roughly 680,000 cubic miles of ice within its ice sheet. The ice is up to 3 miles thick in some areas and covers about three-fourths of the island.

Between 1990 and 2011, climate change caused ocean surface temperatures around Greenland to rise 1.8 to 3.6 degrees Fahrenheit. Researchers say that water from Greenland’s melting ice accounts for nearly one-sixth of annual sea-level rise.