Celebrity

The 'Tammy' star wears a blue dress as her hands and footprints are immortalized at a ceremony which takes place at the TCL Chinese Theatre in Hollywood.

Jul 3, 2014

AceShowbiz - Melissa McCarthy's hands and footprints were immortalized at a ceremony on Wednesday, July 2. Wearing a blue dress, the "Tammy" star was joined by her co-star Susan Sarandon and husband Ben Falcone at the event which took place at the TCL Chinese Theatre in Hollywood.

The actress looked so happy as she posed for photos, being all-smiles and showing off her dirty hands. Sarandon was seen making a speech and joining the mother of two in front of the cameras.

"Tammy", which opened in U.S. theaters on July 2, tells the story of a woman named Tammy, played by McCarthy, who hits the road after a series of unfortunate events in her life. She is joined by her grandmother, played by Sarandon. The comedy was directed and co-written by Falcone, who also plays Tammy's boss.

In a new interview with Reuters, McCarthy and her husband talked about their writing collaboration. "First of all, whenever we have good ideas, it's almost always in the car because you drive a lot in L.A.," Falcone replied when asked about the formula for working together.

"And you just say stupid things to make your spouse laugh, and that's usually when we come up with something that's maybe fun," he added. "So that's our idea place. And then the best way that we write is when we both are sitting there, there's a computer, we start writing, and we trade off typing."

"I'll do a bunch of dialogue, and then he'll say we're not doing that yet," said McCarthy. "And I'll be like, 'Yeah, but I think this is what I say.' And so we'll type that out and keep all that stuff further down the script until we're ready for that scene because I'm a little more scattered."