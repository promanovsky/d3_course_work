Jennifer Goforth Gregory was a regular shopper at Target for years. But last December, after the giant retailer announced that its computer system had been hacked, all of her credit cards were locked down unexpectedly. She found herself stranded at a restaurant with no method of payment, stuck without magazine subscriptions and saddled with other problems that lasted for weeks while her banks reconciled the problem.

Despite the difficulty, she’s still a regular Target (NYSE:TGT) customer today.

“The reality is that the American attention span tends to be fairly short,” said Julie Conroy, research director with the Aite Group, a business consultancy. “To be sure, the timing and magnitude of this breach were such that Target’s black eye has lasted longer than many of the other retailer breaches that we’ve seen, but I don’t expect this to have a long-term brand impact.”

Target CEO Gregg Steinhafel stepped down Monday following the massive holiday-season data breach. Though the case brought unwelcome media attention and Target took a painful financial hit, experts say the costs are only temporary. Dozens of retailers such as Neiman Marcus and J.C. Penny have also been victims of similar hackers in recent years. Though initial costs are always a shock, companies do recover their customers, even if it takes a little while.

“I still do shop at Target, but I’m slightly more wary,” Gregory said, adding that now she uses only one of her credit cards when shopping there, just in case.

It’s likely that the problems of other customers like Gregory contributed to an initial drop in Target’s customer satisfaction rates following the announcement. In a survey from market research firm Cowen & Co., customer satisfaction at Target was down 2 percent in March, with the “most acute” declines among middle- and upper-income customers.

“We continue to see the impact of the data breach on guest sentiment and traffic,” said John Mulligan on the company’s earnings call in February, adding that he expected the trend to diminish by the end of the year as executives make “a vigorous effort” to fix the problem. Mulligan, the CFO, is now acting CEO.

The company reported $61 million in “Data Breach-related expenses,” offset by expected insurance proceeds of $44 million, for net expenses of $17 million on their annual report. Target’s comparable sales decreased 2.5 percent in the fourth quarter of 2013, accompanying a decline in revenue and net income compared to the same quarter of last year. But these costs are just beginning, as they don’t include charges for fraud losses, card reissuance, court costs and other expenses. Executives estimate the total cost to be between $1 billion and $2 billion.

“Target’s perception dropped more in one day than either PlayStation or Citibank did one week after their breaches became public,” wrote Ted Marzilli, managing director of BrandIndex, a market research firm, in a blog post.

The firm uses surveys to determine how popular or unpopular a specific brand is. According to their index, Target’s lowest score ever had been a 10. Before the day of the breach, it was 26. It dropped to -9 the next day, hitting a low of -19 by the end of December.

“The high-profile nature of the Target breach did have an impact on consumer sentiment and earnings, although both are rebounding after their fourth-quarter dip,” said Aite’s Conroy.

Target executives were more forthcoming about the situation than other companies have been, said Dwight Hill, managing director of The Retail Advisory, a Dallas-based firm.

The honesty has two sides. Though it may have made the initial announcement more painful, in the end it should bring more customers back.

Dozens of companies that have had similar data breaches in recent years. Though some have been less forthcoming, they still managed to recover.

In 2012, Barnes & Noble employees discovered that 63 stores in nine states had at least once compromised PIN pad device, which used malicious code to acquire payment card account numbers and PINs, according to a report from Privacy Rights Clearinghouse.

Last year, criminals hacked into the computer systems of luxury retailer Neiman Marcus, a breach that affected 1.1 million credit and debit cards between July and October. In April, the arts and crafts retailer Michaels confirmed that 2.6 million customer cards were affected by a security breach that occurred between May 8 2013 and January 27 2014.

Executives from J.C. Penney Co Inc. reportedly waited more than two years to admit that the company was targeted by known hacker Albert Gonzalez in 2007.

Consumers may be inconvenienced by data breaches, Conroy explained, but they don’t have long-term fears since they aren’t financially liable for credit card fraud.

Chris Dell, an entrepreneur based in New York City, said that he and his roommate both shop at Target regularly, despite the breach.

“Worst-case scenario, I would have to get a new card and have my bank reimburse me,” he said, adding that he planned to continue his twice-a-month shopping trips. “It’s not worth leaving because Target enables me to save money.”