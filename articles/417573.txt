Tiffany & Company ( TIF ) posted impressive second-quarter fiscal 2014 results. The company delivered earnings of 96 cents a share, way ahead of the Zacks Consensus Estimate of 86 cents and higher than the prior-year quarter's 83 cents. Results benefited from higher sales and improved gross margin.

Tiffany & Co - Earnings Surprise | FindTheBest

Shares of this jewelry retailer rose roughly 3% on the index in the pre-market trading session.

Tiffany posted net sales of $992.9 million during the quarter, up 7% from the prior-year quarter, driven by healthy performance primarily across the Americas and Asia-Pacific regions. Moreover, total revenue came ahead of the Zacks Consensus Estimate of $990 million. In constant currencies, net sales jumped 7%, whereas comparable-store sales (comps) climbed 3%.

By geographic segments, sales in the Americas grew 9% to $484 million, while comps rose by 8%; sales in the Asia-Pacific region climbed 14% to $237 million whereas comps increased 7%; sales in Japan declined 13% to $119 million and comps fell 15%; and sales in Europe jumped 8% to $120 million but comps dropped 2%. Other region sales surged 28% to $33 million whereas comps grew 2%.

In constant currencies, sales in the Americas rose 10%, while comps grew by 8% during the quarter; sales in the Asia-Pacific region grew 13%, whereas comps rose 7%; sales in Japan fell 10%, while comps also decreased by 13%; and sales in Europe inched up 1%, whereas comps fell 8%.

Gross profit for the quarter increased 11.8% to $595.2 million, while gross margin expanded 240 basis points to 59.9% due to lower product cost and increase in prices. Operating income jumped 17.9% to $208.5 million, while operating margin increased 190 basis points to 21%. The margin improvement was driven by higher gross margin.

Store Update

Tiffany opened 1 outlet in the Americas in Aventura, FL during the quarter. The company in fiscal 2014 plans to open 10 stores - four in the Americas, two in Asia-Pacific, two in Japan, and one each in Europe and Russia, and shut down 3 stores-one each in the Americas, Asia-Pacific and the U.A.E.

As of Jul 31, 2014, the company operated 293 stores (122 in the Americas, 72 in Asia-Pacific, 55 in Japan, 38 in Europe and 5 in the U.A.E. and 1 in Russia).

Other Financial Details

Tiffany ended the quarter with cash and cash equivalents and short-term investments of $398.4 million, and total short-term and long-term debt of $1,025.5 million, reflecting 35% of shareholders equity. Capital expenditures of $91 million were incurred in the first-half of fiscal 2014. Management projected capital expenditure of $270 million and expects to generate free cash flow of at least $400 million in fiscal 2014.

Tiffany bought back shares worth $9 million in the quarter. In Mar 2014, the company's board of directors had authorized a share repurchase program of $300 million, which is slated to expire on Mar 2017. As of Jul 31, the company had $284 million at its disposal under share repurchase authorization.

Guidance

Following healthy results, Tiffany raised its earnings expectations. The company now projects fiscal 2014 earnings between $4.20 and $4.30 per share against $4.15 and $4.25 expected earlier. The current Zacks Consensus Estimate for fiscal 2014 is $4.28 per share, which could witness an upward revision in the coming days.

Tiffany now project total net sales growth in a high single-digit percentage for fiscal 2014.

Zacks Rank

Tiffany currently sports a Zacks Rank #3 (Hold). Better-ranked retail stocks include Citi Trends, Inc. ( CTRN ) and Signet Jewelers Ltd. ( SIG ) sporting a Zacks Rank #1 (Strong Buy), as well as Abercrombie & Fitch Co. ( ANF ) carrying a Zacks Rank #2 (Buy).

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

ABERCROMBIE (ANF): Free Stock Analysis Report

TIFFANY & CO (TIF): Free Stock Analysis Report

SIGNET JEWELERS (SIG): Free Stock Analysis Report

CITI TRENDS INC (CTRN): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.