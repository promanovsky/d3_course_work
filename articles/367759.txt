LONDON (AP) — Train operators say services through the Channel Tunnel are slowly returning to normal, a day after an electrical fault caused severe delays and reduced services.

Engineers worked through the night to restore power to the affected part of the tunnel, but services were not expected to return to normal until midday Tuesday pending safety checks.

Hundreds of passengers were evacuated from a shuttle train carrying passenger vehicles Monday after the fault brought a train to a halt. The shuttle train traveling from Britain to France was a quarter of the way through the tunnel when it encountered a problem with an overhead power line.

Eurotunnel shuttle services are facing delays of about an hour, while high-speed Eurostar services were facing delays of 30 minutes to an hour on Tuesday.