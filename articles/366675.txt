Ghana is treating its first suspected case of Ebola, the BBC reported Monday. The patient, reportedly an American who had visited Sierra Leone as well, is being treated at the Nyaho Clinic in the capital city of Accra.

In a statement Monday, the Ministry of Health said that although the “suspected fever case” had not yet been confirmed, the government had “put every precautionary measure in place at the clinic, including additional support of public staff from the ministry, directive to quarantine patient and clinical staff and the supply of protective gowns for the staff of the hospital.”

The ministry also appealed to the public to remain calm as it awaited test results from the Noguchi Memorial Research Centre.

If the case is confirmed, Ghana would be the fourth country in West Africa, after Guinea, Liberia and Sierra Leone, to fall prey to the deadly virus.

In April, health authorities tested blood samples of 12-year-old girl who died of viral fever in Kumasi, Ghana’s second-largest city. The tests, however, came back negative for Ebola.

The Ebola virus, which spreads through contact with an infected person’s bodily fluid, has claimed the lives of more than 460 people and infected a total of 760 people in West Africa since it broke out in Guinea in February and spread to Sierra Leone and Liberia.

Since this particular strain of the virus has close to 90 percent mortality, the World Health Organization has called it the “largest in terms of cases and deaths as well as geographic spread” and called for “drastic action” to halt the spread of the disease.

WHO recently concluded an emergency meeting in Accra to draft a comprehensive response to the outbreak. The meeting ended with a resolution to set up a fund to pool resources and support research into the disease.