THE dark hall inside Christianity’s holiest shrine was illuminated with the flames from thousands of candles on Saturday as worshippers participated in the holy fire ceremony, a momentous spiritual event in Orthodox Easter rites.

Christians believe Jesus was crucified, buried and resurrected at the site where the Church of the Holy Sepulcher now stands in the Old City of Jerusalem.

News_Image_File: Sacred flame ... a Christian pilgrim holds candles at the church of the Holy Sepulcher. Picture: Dan Balilty

While the source of the holy fire is a closely guarded secret, believers say the flame appears spontaneously from his tomb on the day before Easter to show Jesus has not forgotten his followers.

The ritual dates back at least 1,200 years.

Thousands of Christians waited outside the church for it to open on Saturday morning.

Custody of the Church of the Holy Sepulcher is shared by a number of denominations that jealously guard their responsibilities under a fragile network of agreements hammered out over the last millennia.

News_Image_File: The flame spreads ... Christian pilgrims hold candles at the church of the Holy Sepulcher, traditionally believed to be the burial site of Jesus Christ, during the ceremony of the Holy Fire. Picture: Dan Balilty

In accordance with tradition, the church’s doors were unlocked by a member of a Muslim family, who for centuries has been the keeper of the ancient key that is passed on within the family from generation to generation.

Once inside, clergymen from the various Orthodox denominations in robes and hoods jostled for space with local worshippers and pilgrims from around the world.

Top Orthodox clergymen descended into the small chamber marking the site of Jesus’ tomb as worshippers eagerly waited in the dim church clutching bundles of unlit candles and torches.

After a while, candles emerged lit with “holy fire” — said to have been lit by a miracle as a message to the faithful from heaven.

Bells rang as worshippers rushed to use the flames to ignite their own candles.

News_Image_File: A Holy Fire ritual participant ... the “holy fire” was passed among worshippers outside the Church and then taken to the Church of the Nativity in Bethlehem. Picture: Ilia Yefimovich

In mere seconds, the bursts of light spread throughout the cavernous church as flames jumped from one candle to another. Clouds of smoke wafted through the crammed hall as flashes from cameras and mobile phones documented what is for many, the spiritual event of a lifetime.

Some held light from the “holy fire” to their faces to bask in the glow while others dripped wax on their bodies.

Israeli police spokeswoman Luba Samri said tens of thousands of worshippers participated in the ceremony.

News_Image_File: The flame comes to Greece ... a Greek Christian Orthodox priest holds holy fire at Prophet Elias church in Chrisopoulis near the northern city of Kavala. Picture: Petros Karadjias

Many couldn’t fit inside the church and the narrow winding streets of the Old City were lined with pilgrims.

The “holy fire” was passed among worshippers outside the Church and then taken to the Church of the Nativity in the West Bank town of Bethlehem, where tradition holds Jesus was born, and from there to other Christian communities in Israel and the West Bank.

Later it is taken aboard special flights to Athens and other cities, linking many of the 200 million Orthodox believers worldwide.

News_Rich_Media: Thousands of Christian worshippers celebrate Easter's Holy Fire ceremony in Jerusalem and the West Bank city of Bethlehem.

POPE’S MESSAGE OF FAITH FOR EASTER

MEANWHILE, in the Vatican, Pope Francis has baptised 10 people and urged them to bring their faith “to the ends of the earth” as he presided over an Easter Vigil in St Peter’s Basilica.

The vigil is among the Vatican’s most solemn services.

The Pope entered the darkened basilica with a lone candle, which he then shared with others to slowly illuminate the church.

The symbolic service commemorates the darkness of the faithful over the crucifixion of Jesus Christ on Good Friday and their joy and light at his resurrection on Easter Sunday.

The Pope urged the priests, bishops, cardinals and ordinary Catholics gathered for the late night service to remember when they first found their faith.

News_Image_File: Message of faith ... Pope Francis at the Easter Vigil at St Peter’s basilica in the Vatican. Picture: Filipo Monteforte

“Do I remember it? Have I forgotten it? Look for it. You’ll find it. The Lord is waiting.” Trying to remember isn’t an act of nostalgia but rather a way to bring the “fire” of faith “to all people, to the very ends of the earth”, he said.

After his homily, The Pope proceeded to baptise each of the 10, starting with Italian brothers Giorgio and Jacopo Capezzuoli, aged 8 and 10.

“Do you want to be baptised?” he asked each one as he smiled. He asked the same of the adult converts, who hailed from Vietnam, Belarus, Senegal, Lebanon, Italy and France.

It was the second late night for the Pope after the long Good Friday Way of the Cross procession at Rome’s Colosseum.

The Pope, 77, will get a few hours of rest before celebrating Easter Sunday Mass in the flower-strewn St Peter’s Square.

He then has a week to prepare for the other major celebration of this year’s Easter season: the April 27 canonisations of Pope John XXIII and John Paul II.

Hundreds of thousands of people are expected to attend.