The 2014 Nickelodeon Kids’ Choice Awards ceremony was a night filled with surprises, inspirational speeches, bright orange blimps and green goo. The winners are in — was your favorite victorious?

Photo credit: Adriana M. Barraza/WENN.com

Who survived the slimefest and who managed to walk away with a bright orange blimp at the Galen Center at USC in Los Angeles, California, on Saturday?

The 2014 Nickelodeon Kids’ Choice Awards ceremony has officially come and gone, but the event was not without its surprises, and host Mark Wahlberg made sure the night was one to remember.

And even though the Transformers: Age of Extinction actor vowed in his opening speech that he would be the only one who was not covered in slime by the end of the night, he was proven wrong! Much to the enjoyment of fans and thanks to Kevin Hart and Wahlberg’s own children, even the host was turned green.

Among the big winners of the night were brunette beauties Selena Gomez, who took home the accolade of Favorite Female Singer and Ariana Grande, who was crowned Favorite TV Actress for Sam & Cat.

Other big names included Oscar-winning star Jennifer Lawrence, British and Irish crooners One Direction, and of course, the best male butt kicker of all time, Robert Downey Jr., whose speech inspired his eager young fans.

“This is the highest honor yet bestowed on me. I’m grateful, humble, the whole deal,” the Iron Man star said. “You know I wasn’t always a butt kicker. In fact, life has kicked my proverbial butt countless times in many ways for many years, until I decided one day to start kicking back. Now look at me!”

See the full list of winners below (winners are in bold).

Favorite Movie

The Hunger Games: Catching Fire

Iron Man 3

Oz the Great and Powerful

The Smurfs 2

Favorite Animated Movie

Frozen

Cloudy With a Chance of Meatballs 2

Despicable Me 2

Monsters University

Favorite Movie Actor

Adam Sandler (Grown Ups 2)

Johnny Depp

Robert Downey Jr.

Neil Patrick Harris

Favorite Movie Actress

Jennifer Lawrence (The Hunger Games: Catching Fire)

Sandra Bullock

Mila Kunis

Jayma Mays

Favorite Male Buttkicker

Robert Downey Jr. (Iron Man 3)

Johnny Depp

Hugh Jackman

Dwayne Johnson

Favorite Female Buttkicker

Jennifer Lawrence (The Hunger Games: Catching Fire)

Sandra Bullock

Evangeline Lilly

Jena Malone

Favorite Song

One Direction — “Story of My Life”

Taylor Swift — “I Knew You Were Trouble”

katy Perry — “Roar”

Miley Cyrus — “Wrecking Ball”

Favorite Music Group

One Direction

Maroon 5

One Republic

Macklemore & Ryan Lewis

Favorite TV Show

Sam & Cat

The Big Bang Theory

Good Luck Charlie

Jessie

Favorite TV Actor

Ross Lynch (Austin & Ally)

Benjamin Flores Jr.

Jack Griffo

Jake Short

Favorite TV Actress

Ariana Grande (Sam & Cat)

Jennette McCurdy

Bridgit Mendler

Debby Ryan

Favorite Male Singer

Justin Timberlake

Bruno Mars

Pitbull

Pharrell Williams

Favorite Female Singer

Selena Gomez

Lady Gaga

Katy Perry

Taylor Swift

Favorite Funny Star

Kevin Hart

Kaley Cuoco

Andy Samberg

Sofia Vergara

Other Categories

Favorite Reality Show — Wipeout (Winner)

Favorite Cartoon — SpongeBob SquarePants (Winner)

Favorite Voice From an Animated Movie — Miranda Cosgrove Despicable Me 2 (Winner)

Favorite Book — Diary of a Wimpy Kid series (Winner)

Favorite Video Game — Just Dance 2014 (Winner)

Favorite App Game — Despicable Me: Minion Rush (Winner)

Favorite Animated Animal Sidekick — Patrick Star (SpongeBob SquarePants) (Winner)

Most Enthusiastic Athlete — Dwight Howard (Winner)