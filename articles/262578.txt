Diet soft drinks are 44 per cent more effective at helping people lose weight than drinking water alone, a new study has claimed. Thinkstock

Diet soft drinks are 44 per cent more effective at helping people lose weight than drinking water alone, a new study has claimed.

The study by researchers at the University of Colorado and colleagues confirms definitively that drinking diet beverages helps people lose weight.

"This study clearly demonstrates that diet beverages can in fact help people lose weight, directly countering myths in recent years that suggest the opposite effect - weight gain," said James O Hill, co-author of the study.

"In fact, those who drank diet beverages lost more weight and reported feeling significantly less hungry than those who drank water alone. This reinforces that if you're trying to shed pounds, you can enjoy diet beverages," said Hill.

The 12-week clinical study of 303 participants is the first prospective, randomised clinical trial to directly compare the effects of water and diet beverages on weight loss within a behavioural weight loss programme.

The study shows that subjects who consumed diet beverages lost an average of 6kg 44 per cent more than the control group, which lost an average of 4kg.

More than half of the participants in the diet beverage group 64 per cent lost at least five per cent of their body weight, compared with only 43 per cent of the control group.

Losing just five per cent of body weight has been shown to significantly improve health, including lowering the risk of heart disease, high blood pressure and type 2 diabetes.

"There's so much misinformation about diet beverages that isn't based on studies designed to test cause and effect, especially on the internet," said John C Peters, co-author of the study.

"This research allows dieters to feel confident that low and no-calorie sweetened beverages can play an important and helpful role as part of an effective and comprehensive weight loss strategy," said Peters.

Study participants were randomly assigned to one of two groups: those who were allowed to drink diet beverages, such as diet sodas, teas and flavoured waters, or those who were in a control group that drank water only.

With the exception of beverage options, both groups followed an identical diet and exercise programme for the duration of the study.

In addition to losing 44 per cent more weight than the control group, the diet beverage group also reported feeling significantly less hungry; showed significantly greater improvements in serum levels of total cholesterol and low-density lipoprotein (LDL) and saw a significant reduction in serum triglycerides.

Both diet soda and water groups saw reductions in waist circumference, and blood pressure, researchers said.

The study was published in the journal Obesity.