Following the market opening Tuesday, the Dow traded up 0.61 percent to 16,375.80 while the NASDAQ surged 0.98 percent to 4,267.66. The S&P also rose, gaining 0.62 percent to 1,869.03.

Leading and Lagging Sectors In trading on Tuesday, basic materials shares were relative leaders, up on the day by about 1.37 percent. Top gainers in the sector included Amyris (NASDAQ: AMRS ), with shares up 5 percent, and Gold Fields (NYSE: GFI ), with shares up 4.2 percent.

Utilities sector gained by just 0.03% in the US market today. Among the sector stocks, Atlas Energy LP (NYSE: ATLS ) was down more than 1.3 percent, while FirstEnergy (NYSE: FE ) tumbled around 1.35 percent.

Top Headline Walgreen Co (NYSE: WAG ) reported a drop in its second-quarter profit and announced its plans to close 76 stores. Walgreen's quarterly net income fell to $754 million, or $0.78 per share, versus a year-ago profit of $756 million, or $0.79 per share. Excluding one-time items, it earned $0.91 per share. Its sales climbed 5.1% to $19.61 billion, while same-store sales climbed 4.3%. However, analysts were expecting earnings of $0.93 per share on sales of $19.6 billion. Its gross profit margin declined 1.3 percentage points to 28.8% of sales.

Equities Trading UP Sonic (NASDAQ: SONC ) shares shot up 10.21 percent to $23.06 after the company reported better-than-expected fiscal second-quarter earnings. Sonic posted its quarterly earnings of $0.07 per share on revenue of $109.7 million. However, analysts were projecting a profit of $0.06 per share on revenue of $110.3 million.

Shares of Endocyte (NASDAQ: ECYT ) got a boost, shooting up 4 percent to $28.31 after Wedbush added the stock to Best Ideas List and raised the price target to $65.

Walgreen Co (NYSE: WAG ) was also up, gaining 4.18 percent to $67.00 after the company reported a drop in its second-quarter profit and announced its plans to close 76 stores.

Equities Trading DOWN Shares of Himax Technologies (NASDAQ: HIMX ) were down 11.81 percent to $11.80 after falling 4.77% on Monday. Bank of America downgraded the stock to underperform.

G-III Apparel Group (NASDAQ: GIII ) shares tumbled 5.47 percent to $70.39 on Q4 results. G-III Apparel reported its Q4 earnings of $0.62 per share on revenue of $472.80 million. The company expected FY15 earnings of $3.95 to $4.10 per share, versus analysts' estimates of $4.30 per share.

Carnival (NYSE: CCL ) was down, falling 2.27 percent to $39.09 after the company reported Q1 results. Carnival reported its Q1 earnings of $0.00 per share on revenue of $3.585 billion.

Commodities In commodity news, oil traded up 0.47 percent to $100.07, while gold traded up 0.09 percent to $1,312.30.

Silver traded up 0.04 percent Tuesday to $20.08, while copper rose 1.66 percent to $2.99.

Eurozone European shares were higher today.

The Spanish Ibex Index rose 0.87 percent, while Italy's FTSE MIB Index climbed 1.32 percent.

Meanwhile, the German DAX jumped 1.70 percent and the French CAC 40 rose 1.48 percent while U.K. shares surged 1.18 percent.

Economics The ICSC-Goldman same-store sales index dropped 1.5% in the week ended Saturday versus the earlier week.

The Johnson Redbook Retail Sales Index declined 0.4% in the first three weeks of March versus February.

US home prices fell 0.1% in January, according to S&P/Case-Shiller's composite index. US home prices climbed 0.8% in January after seasonal adjustments.

Sales of new US homes dropped 3.3% to an annual rate of 440,000 in February. However, economists were expecting sales to reach 445,000 last month.

The Richmond Fed manufacturing index declined to -7.00 in March, versus a prior reading of -6.00. However, economists were expecting a reading of 4.00.

The Conference Board's consumer confidence index rose to 82.30 in March, versus economists' expectations for a reading of 78.50. US home prices climbed 0.5% in January, and rose 7.4% y/y.

The Treasury is set to auction 4-week bills and 2-year notes.

© 2014 Benzinga.com. Benzinga does not provide investment advice. All rights reserved.

Free Trading Education - Check out the free events taking place on Marketfy this week. Spaces are limited. Sign up today.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.