It’s official, at least according to the taxman: bitcoin is not a currency. The Internal Revenue Service ruled Tuesday that the controversial cryptocurrency and its rivals will be treated as property, not cash, for tax purposes.

The ruling had been expected and marks another step in the wider attempt to make bitcoin mainstream. In its notice, the IRS said bitcoin would be treated much like stock or other intangible property.

"The notice provides that virtual currency is treated as property for US federal tax purposes. General tax principles that apply to property transactions apply to transactions using virtual currency," according to an IRS news release.

The ruling means gains in value will be treated as capital gains and as such could be subject to lower tax rates than income. The top long-term capital gains tax rate is 20%, while the top ordinary income tax rate is 39.6%.



The clarification does, however, make using bitcoin as a currency more problematic. Anyone involved in transactions involving digital currencies will be subject to the same record-keeping requirements, and taxes, as those making stock transactions and other deals.

Under the new rules, paying for a beer in bitcoin would be a taxable event with the buyer having to work out if they had made any capital gain on the asset they had just sold.

Bitcoin has been gaining some ground with the US financial establishment. Last year, outgoing Federal Reserve chairman Ben Bernanke told Congress that bitcoin and its ilk “may hold long-term promise, particularly if the innovations promote a faster, more secure and more efficient payment system”.

Benjamin Lawsky, superintendent of New York's department of financial services, said last month that he intends to introduce regulation for bitcoins later this year, making New York the first state to do so.

The IRS clarification will further cement the asset’s legitimacy but it comes as bitcoin has faced some of its biggest problems to date.

In February, Mt Gox, bitcoin’s largest exchange, collapsed and lost $450m worth of bitcoin. The exchange declared bankruptcy amid a series of cyber-attacks and allegations of incompetence among its management.

The currency has also been at the forefront of investigations into online drugs and guns bazaar Silk Road. Last November Ross Ulbricht, a 29-year-old investment adviser and entrepreneur, was arrested by the US authorities and accused of being “Dread Pirate Roberts”, the site's creator. He denies the charges.