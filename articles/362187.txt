The notification by Google that it was removing links to pages on the BBC and some news organisations, including the Guardian and Daily Mail, on some (unspecified) searches has led to huge amounts of confusion about the search company's obligations under the Data Protection directive, following the European Court of Justice ruling of May 2014.



Here, we try to deal with some of the questions around it.

Q Why is Google removing pages from the internet?

It isn't. The original pages are untouched. What is removed is a link in Google's index if someone specifically searches on a particular name. Update: testing by the Guardian has established that pages are being removed on more than just the searches for names.

This is contrary to the statement given by Google's spokesperson in a Radio 4 interview, in which he said: ""Only if you search for the very narrow term of the name of the commentator would it not appear [on a Google search]". The Guardian has established by comparison with US search engine results that links for some searches on a name and a profession have been removed, as well as those on the name.

Q But wasn't the removal of Robert Peston's 2007 blog post a rewriting of Merrill Lynch's history?

No. Peston's blogpost was not "removed" from Google. If you search on Merrill Lynch or Stan O'Neal or Robert Peston or any of those, Peston's 2007 blogpost will still appear. Merrill Lynch's history is unchanged. O'Neal did not request any removal, and would - or should - have been refused anyway because he does not live in Europe.

Q What is happening then?

As stated above: if you search on particular names, then some pages don't appear in searches. In Europe, when you search on someone's name, you'll now see a warning at the bottom of the page saying "Some results may have been removed under data protection law in Europe". This doesn't however mean that any pages necessarily have been removed from search, so you can't infer anything from it.

Q Why is Google doing this?

Because in May a ruling by the European Court of Justice, Europe's highest court, determined that Google is a "data controller" under the meaning of the European data protection directive. That means that it has a responsibility to make sure that the data it holds about people obeys the data protection laws: that is, it must not be irrelevant, out of date, inaccurate or an invasion of privacy.

That's the same law which makes sure that credit rating agencies don't mark you down just because you live next to someone with a criminal record, or because of some 20-year-old data about you, or which says you're in debt when you aren't, or demands to know about your medical conditions when it isn't offering you health insurance. Google has to obey the same rules - but relating to all the information it has about pages around the internet. Obviously it can't check those, so it relies on affected people reporting pages that are wrong/out of date/irrelevant/an invasion of privacy, and then takes searches against those names out of its index.

But to reiterate - any of the constellation of searches that would previously have found that page will still turn it up. The only change is that that particular search on a name won't. (But note the update above: a search on a name and profession is also removed.)

Q Isn't that censorship? Jimmy Wales says it is.

According to Wikipedia, censorship is "the suppression of speech, public communication or other information which may be considered objectionable, harmful, sensitive, politically incorrect or inconvenient as determined by governments, media outlets, authorities or other such entities."

Certainly the production of the link in response to a specific name search has been suppressed, and the decision to do so has been taken following a ruling by a government entity (the ECJ). The question is whether Google's links are "speech". Google has been very careful in Europe not to define itself as a publisher, because of certain obligations that can bring. But that means that its search links aren't actually "speech" under European law.

Q But only paedophiles or criminals or someone with something to hide would want to wipe such a link off the internet.

Heard of revenge porn sites? Jilted lovers - usually male - upload compromising pictures of their ex in order to embarrass them. Sometimes they also upload names, addresses and workplaces. The entire intention of revenge porn is to make searching on someone's name an embarrassment. It's a simple case that such photos constitute an invasion of privacy, but until now it was extremely difficult to get such pages removed from Google's index.

For Europeans, at least, that's now easier.

Similarly, people who committed petty crimes such as robbery or burglary years ago and now think that those crimes should no longer be recorded under the Rehabilitation of Offenders Act could argue that links to their crimes should be removed after some years. Does the RoHA not apply because we have the internet? (It never applies to some crimes, including many sexual ones, so paedophiles would be turned down.)

Q Who at Google is deciding what gets removed? How many removal requests are there?

The company says it has hired "a number" (unspecified) of "paralegals)"to deal with the requests, which initially were coming in at 10,000 per day but have now slowed to around 1,000 per day.

Though Germany led early on in the number of requests, it is now France which has generated the most. By Thursday, there had been over 70,000 removal requests in Europe, with the top five countries being France (14,086), Germany (12,678), the UK (8,497), Spain (6,176) and Italy (5,934).

A paralegal is defined in the UK as "a non-lawyer who does legal work that previously would have been done by a lawyer, or if done by a lawyer, would be charged for." That is, the people who are considering these requests are not qualified lawyers or solicitors. It's unclear what their level of legal training is - GCSE? A Level? Law degree? Chambers? - and Google declined to comment, though it's understood that the unspecified number of people with unspecified qualifications have "the right skills and the right supervision".

The crux though is how Google decides. So far this week it has irked news organisations by removing links to pages when arguably it should have refused the requests for removal. Dougie McDonald, the former Scottish referee, was the focus of news attention in 2010 and 2011; it seems hard to argue that any of the tests - out of date, irrelevant, inaccurate, invasion of privacy - could apply to stories about him. Furthermore, the ECJ said that there could be an exception to refuse link removal for "people in public life". McDonald is retired now, but was in "public life" then; does an exception apply? Google's paralegals apparently thought that requests for pages about him were allowable. But when the Guardian protested, Google reinstated them in its search.

Q So it's all about the paralegals?

Yes. They are the key to all of this: they are the ones who decide what does and what doesn't get accepted. It's impossible to know whether they are approving everything, or rejecting only a small proportion of requests, or rejecting a huge number. It's also unknown how they balance the criteria. But the fact that a number of links have been reinstated following protests by news outlets suggests that Google isn't completely confident in the paralegals' judgement.

Q So Google isn't perfect at this?

Google is willing to admit that it doesn't like the ECJ ruling at all - it fought it through the courts - and that it is still feeling its way in how to decide whether to accept or reject requests; there are understood to be "teething problems".

The odd thing is that Google doesn't have to comply with a single request sent through its systems. It could refer them all to the information commissioner in each relevant country, and let it sort them out. Or it could reject every claim, and tell people to take it to court. Both would effectively be complying with the ECJ ruling, and might even create a body of case law to help determine when and why a search on a name to a link should be removed.

But remember, it's nothing more than that - the removal of one link when a particular search on a name is made. The web page remains; any other search for that page (and even variations on that search) still works.

• Explaining the 'right to be forgotten' - the newest cultural shibboleth