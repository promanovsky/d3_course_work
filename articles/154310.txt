Posted on 22 April 2014

When 22.04.2014

TV presenter Stephen Colbert waves at paparazzi but doesn't stop for photos as he arrives outside the New York studios for his appearance on 'The Late Show With David Letterman'. He also appears to snub a reporter who questions him clutching a microphone as he gets back into his car on leaving the studio.

Stephen Colbert is due to replace David Letterman on 'The Late Show' after the veteran talk show host retires in 2015. He currently hosts 'The Colbert Report' which has won two Emmys and has been nominated for another seven.

Contactmusic

More Videos