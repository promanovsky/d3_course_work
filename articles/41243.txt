How high can prices go when it comes to saving lives?

U.S. lawmakers are scrutinizing the price of Sovaldi, a new hepatitis C drug produced by Gilead Sciences, and have asked the biotech company to explain why it cost $1,000 a pill.

The $84,000 price tag for a three-month treatment has health insurers and state Medicaid programs nervous as it is difficult to draw a line between what the company charges and what it costs to produce it.



"It makes an easy target, certainly for politicians or for folks, but I think the more important constituency here are our payers," Christopher Raymond, an R.W. Baird biotech analyst, told CNBC's "Squawk Box" on Monday.

(Read more: )

Raymond said biotech companies do not encounter such problems in other parts of the world because health-care systems are so different. Europe, for example, has a single-payer system in which governments cover health-care costs.

"Alexion [Pharmaceuticals], which has their own high-price drug, are in the midst of a new product cycle in Europe, ... where they are winning that battle every day," he said.

(Read more: )

But the United States lacks such a system, and "until we get that, I think this is just headlines," Raymond said.

Gilead has said it has been working with a number of stakeholders, including federal and state officials, and looks forward to meeting with members of Congress to address concerns about Sovaldi, according to Reuters.

—By CNBC's Mathilde Hamel. Follow her on Twitter @mathildehamel

