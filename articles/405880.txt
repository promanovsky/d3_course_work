LinkedIn Corp. ( LNKD ) is scheduled to release second-quarter 2014 results on Jul 31, 2014, after the closing bell. Over the past three quarters, LinkedIn's earnings per share missed the Zacks Consensus Estimate while revenues came in ahead of the street.

LinkedIn currently has a Zacks Rank #2 (Buy). While LinkedIn's Zacks Consensus Estimate stands at a breakeven for the second quarter, the Most Accurate estimate is pegged at 2 cents which translates into an Earnings ESP of 0.00%.

Factors to Consider for the Quarter

LinkedIn has taken several initiatives to increase user engagement like the launch of a cheaper subscription service called Premium Spotlight. Apart from this, LinkedIn has redesigned its member profile pages with larger profile photos and header images.

Moreover, the professional networking company has launched a job search app for Apple's iPhone to cater to the growing number of its members who search for jobs via mobile devices. Additionally, LinkedIn resorted to strategic acquisitions, such as Newsle and Bizo, to not only enhance user experience but also garner additional dollars through targeted marketing strategies.

Another positive is LinkedIn's plan to expand in China which is the focal point of its international expansionary initiatives.

Nonetheless, continued investments to improve product and service offerings might affect LinkedIn's near-term profitability. Though impacting the company's operational performance in the short run, these investments drive member growth and user engagement over the long term. We remain encouraged by the impressive top-line growth recorded in the past few quarters.

Other Stocks to Consider

Here are some other companies that you may want to consider as our model shows they have the right combination of elements to post an earnings beat this quarter:

First Solar, Inc. ( FSLR ) with an Earnings ESP of +6.06% and a Zacks Rank #1 (Strong Buy).

Gentiva Health Services Inc. ( GTIV ) with an Earnings ESP of +37.5% and a Zacks Rank #1.

Western Digital Corp ( WDC ), with an Earnings ESP of +4.02% and a Zacks Rank #2.

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

LINKEDIN CORP-A (LNKD): Free Stock Analysis Report

WESTERN DIGITAL (WDC): Free Stock Analysis Report

FIRST SOLAR INC (FSLR): Free Stock Analysis Report

GENTIVA HEALTH (GTIV): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.