NEW YORK (TheStreet) -- Sarepta Therapeutics (SRPT) - Get Report soared Monday after the company announced it had received FDA guidance to apply for regulatory approval of its experimental Duchenne muscular dystrophy drug eteplirsen by the end of the year.

The application would lead to a clinical study on a larger scale.

The company said in a statement it would bolster its FDA application with more safety and efficacy data from an earlier trial of the drug, along with a confirmatory study it plans to begin in the third quarter.

The FDA said in November that Sarepta's plan to file based on data at the time was premature, which caused a 64% drop in the stock in just one day.

Sarepta was up more than 47% to $35.98 at 11:30 a.m., by which point it had easily surpassed its average volume of 1,290,500 with nearly 9 million shares traded for the day. The stock hit a high of $44.60 as of that time.

Must Read:Warren Buffett's 10 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

SRPT data by YCharts

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.