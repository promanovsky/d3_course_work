The young actor Ansel Elgort has worked with Shailene Woodley in two films. And he has a suggestion for anyone meeting her. “My advice to people, when they’re first meeting Shailene, is to hug her back,” Elgort says, laughing. “You are going to be hugged, so be ready.”

“I do love a good hug,” admits Woodley, whose work on TV in The Secret Life of an American Teenager led to break-out film performances in The Descendants and last summer’s The Spectacular Now.

Critics and her peers noticed right away her open-eyed, open-hearted transparency, the way any film she takes on instantly feels more real just because she’s in it.

“‘Authentic’ is the right word for her,” says author Veronica Roth. Roth wrote the Divergent series, the sci-fi novels that Hollywood is predicting could become the next Hunger Games when the first film hits theatres. “Shailene’s not afraid to go places, playing it from the gut. Vulnerable.”

Elgort, who plays Woodley’s character’s brother in Divergent and her love interest in The Fault in Our Stars, agrees. “It’s science fiction. You need somebody as real as her to make it real.”

For her part, Woodley doesn’t consider acting, “not so much a skill as just how I am.” Maybe it’s something she picked up. Maybe she was born with it.

“I’m astrologically a triple water sign [November 15, a Scorpio], so I’m basically an emotional watery mess,” Woodley jokes. “But I grew up with two psychologist parents, which explains a lot. They established feeling and compassion in me in a very young age. I wonder if that helped me connect, emotionally, with someone really easily.”

A demanding role

It was a demanding role, and in casting, filmmakers sought someone who could hold her own in the company of more experienced cast members, including Kate Winslet and Ashley Judd, and embody the brave and at times reckless warrior as well as the ordinary, vulnerable girl.

“She really is very, very self-sufficient and is her own kind of warrior in terms of she wanted to do her stunts herself,” producer Lucy Fisher said. “She has a huge amount of inner strength... She’s very mature beyond her age, as is Tris.”

For Woodley, the draw was the story’s universal appeal, she said, and its parallels with the world we live in.

“It’s not just about young people figuring their way through life,” she said. “It’s about young people being in really adult situations, and they’re treated like adults, which is how adolescents are these days. Everybody’s incredibly smart, and there’s not a lot of movies that do that age range justice.”

Roth admits to being fans of The Hunger Games novels and owns up — a little — to the resemblance between the two series.

Rigid social structure

But Divergent — to be followed by Insurgent and Allegiant as Woodley’s character, Tris, finds herself and sets herself up in opposition to the rigid social structure she’s born into — has the best “Young Adult” bonafides of any book turned to film series in that genre. It’s not just the stars who are young (Woodley is 22, Elgort just turned 20). Roth, the novelist, is only 26 herself.

“The choices real teenagers have to make at 16, 17 or 18, feel like these life and death decisions,” Roth says. “It’s their emotional reality. I felt that at 16, this pressure to get it right the first time. People change majors, change colleges, change jobs. But when you’re 16, 17, you don’t have that perspective yet.”

Woodley felt that herself as she pondered whether to tackle her first “franchise.” She’s spent a couple of years as a “critic’s darling,” making movies that did not become the sort of life-altering blockbusters that The Hunger Games did. That film series changed Winter’s Bone starlet Jennifer Lawrence into the hottest acting property in Hollywood, and a tabloid favourite.

“I had to decide if I wanted something this big in my life,” Woodley says. “I’m grateful that I did, because I have no regrets and I know that thinking about it more made it easier to see it was the right decision.”

To ease her into Divergent, Spectacular Now co-star Miles Teller took a supporting role. And after working with Elgort in Divergent, Woodley was happy to co-star with him in The Fault in Our Stars, a romance about two physically damaged teens who fall in love in a support group for cancer patients.

Friends on set

“It’s very comforting,” Woodley says, “to have friends with you on the set when you’re trying something new or scary. I loved having Miles in Chicago, for Divergent. And having a prior relationship with Ansel made playing these really vulnerable characters in Fault in Our Stars easier.”

And it’s got to be a help, knowing that when you hug somebody on that first day on the set, they know what to do in response. “Oh yeah, they know they’d better hug back.”