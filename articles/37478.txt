The CEO of Malaysia Airlines says his possible resignation is "a personal decision" that he will consider "later".

Ahmad Jauhari Yahya told the BBC he would not be stepping down any time soon, as "there were things to do now".

In an interview with Alastair Leithead, Mr Yahya insisted his company "was not hiding anything", and he was 100% sure missing flight MH370 was lost.