In early trading on Monday, shares of Tesla Motors ( TSLA ) topped the list of the day's best performing components of the Nasdaq 100 index, trading up 3.6%. Year to date, Tesla Motors registers a 42.1% gain.

And the worst performing Nasdaq 100 component thus far on the day is Yahoo! ( YHOO ), trading down 4.9%. Yahoo! is lower by about 13.2% looking at the year to date performance.

Two other components making moves today are Vertex Pharmaceuticals ( VRTX ), trading down 4.2%, and SanDisk ( SNDK ), trading up 3.0% on the day.

VIDEO: Nasdaq 100 Movers: YHOO, TSLA

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.