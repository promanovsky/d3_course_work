By: By Rachael Rettner, Senior Writer

Published: 03/20/2014 09:00 AM EDT on LiveScience

Giving birth in water has no proven benefits and can pose rare but serious risks to the baby, so it is not recommended in most circumstances, according to a new report from two leading groups of doctors.

Although water births have gained popularity in recent years, there's no evidence that delivering a baby in water is safe or beneficial for either the mother or the baby, according to the report, from the American Academy of Pediatrics and the American College of Obstetricians and Gynecologists. And in some cases, water births have resulted in infant drowning, breathing problems due to inhaled water, and infections.

Given the lack of known benefits, and the potential for serious risks, "the practice of immersion in the second stage of labor (underwater delivery) should be considered an experimental procedure," that should only be performed if the mother is part of a clinical trial, the report says. [11 Big Fat Pregnancy Myths]

"There are rare risk associated with [water births], especially if the baby is actually delivered in the water," said Dr. Tonse Raju, chief of the Pregnancy and Perinatology Branch at the National Institute of Child Health and Human Development, who was involved with the recommendations. Mothers should be aware of these risks, he said.

Some supporters of water birth have argued that babies have a reflex that prevents them from inhaling water, but doctors say this reflex can be overridden — for example, it's known that babies can inhale fecal matter while in the womb. In fact, inhaling bits of fecal matter is also a risk of underwater deliveries, Raju said.

On the other hand, the report says some pregnant women may benefit from going through the early stages of labor in a birthing pool. Some studies suggest that women who labor in water experience less pain, need less anesthesia and have shorter labors. But in order to follow the recommendations, women who decide to labor in water will need to get out of the tub before the baby comes.

Facilities that give women the option to labor in water need to have a rigorous protocol in place for water births, including a standard way of monitoring the mother and the progression of labor so that the mother can be moved before she delivers, Raju said.

Medical staff also need to ensure that the practice will not prevent the women from receiving the care she needs during labor, and that pools and tubs are cleaned to prevent infection. (Water births also have a risk of infection because stool can contaminate the water, Raju said.)

There are no official guidelines for which women are good candidates for laboring in water, but in general, these are healthy mothers who doctors expect will deliver without complications, Raju said.

Follow Rachael Rettner @RachaelRettner. Follow Live Science @livescience, Facebook & Google+. Original article on Live Science.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>