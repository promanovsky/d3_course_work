Astronauts at the International Space Station are able to stream video content, including the ongoing World Cup taking place in Brazil. On top of that, NASA has announced it has enabled 3D printing as well on the ISS in innovative testing efforts from the space agency.

The 3D printing has captured the tech world's interest as the International Space Station aims to become more self-sufficient and test a number of new and innovative tools. Those tools include the zero-gravity 3D printer, which could help make other efforts on the station easier to conduct if the printing capacity can be fully manifested.

Designed by Made in Space, a Mountain View, Calif.-based company, the printer is to be the first printer designed to produce in space without gravity. It employs "extrusion additive manufacturing -- building objects one layer at a time out of polymers and other fast-cooling materials" that can persist in space.

"As NASA ventures further into space, whether redirecting an asteroid or sending humans to Mars, we'll need transformative technology to reduce cargo weight and volume," NASA Administrator Charles Bolden said in a statement announcing the launch of NASA's collaboration with the company.

"In the future, perhaps astronauts will be able to print the tools or components they need while in space," he continued.

It could also be a huge step in reducing the costs of maintaining the space station if the astronauts could simply print out the tools and components needed for upkeep, meaning trips loaded with tools could become a thing of the past.

Made in Space only recently passed all NASA tests required to work on the ISS.

"Years of research and development have taught us that there were many problems to solve to make Additive Manufacturing work reliably in microgravity. Now, having found viable solutions, we can welcome a great change -- the ability to manufacture on-demand in space is going to be a paradigm shift for the way development, research, and exploration happen in space," Michael Snyder, lead engineer and director of R&D for Made In Space, said in a statement.

But in a positive, almost fun moment, astronauts were able to also stream World Cup matches, bringing the global soccer tournament off the planet's surface.

Astronauts on the ISS also participated in a "friendly" match on the station, which is approximately the size of a soccer pitch, highlighting just how important the global event taking place in Brazil until July 13 is to so many.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.