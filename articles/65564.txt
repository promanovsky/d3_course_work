Lady Gaga is smokin’ red while performing on stage at Roseland Ballroom on Friday (March 28) in New York City.

During the concert, the 28-year-old entertainer sang some of her hit songs, such as “Just Dance,” an acoustic version of “Poker Face,” and the recent hit “Applause.”

PHOTOS: Check out the latest pics of Lady Gaga

It was the first night that Gaga performed at the venue and is scheduled to perform there until Monday, April 7. Buy your tickets now if you are in the NYC area!

“Went to my old apt last night. Nostalgia is magical. Even better celebrating at the bar I turned 21 in. #samefriends,” Gaga tweeted later in the night with a pic. Check out the pic below!

Earlier in the night, Gaga was a beautiful rose while arriving for the concert.