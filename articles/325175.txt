Kevin Bacon has signed up for the film Black Mass

Kevin Bacon is joining Johnny Depp in crime drama Black Mass.

The Following star will play FBI Special Agent Charles McGuire in the movie about Boston murderer and mobster Whitey Bulger, reported Deadline.

Johnny is playing the lead role and the cast also includes Joel Edgerton, Benedict Cumberbatch, Jesse Plemons, Sienna Miller and Dakota Johnson.

The film is based on Boston Globe reporters Dick Lehr and Gerard O'Neill's book Black Mass: The True Story Of An Unholy Alliance Between The FBI And The Irish Mob.

It will chronicle the life of the Boston crime kingpin-turned-fugitive Bulger, who rose to the top of the FBI's Ten Most Wanted List after he went on the run for a decade. He was later captured in California in 2011 and is serving time in Massachusetts state prison for second-degree murder.

Out Of The Furnace filmmaker Scott Cooper is directing the movie, which has already started shooting in Boston.