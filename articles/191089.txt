Camels can transmit the deadly MERS virus to people, according to new research.

The study, published in the journal Eurosurveillance, showed that MERS levels were especially high in camels' eyes and noses, and the researchers believe that people are most likely to be infected through contact with these sites, especially nasal discharge.

Scientists took nasal and conjunctival swabs from 76 camels in Oman. In five camels, they found the MERS . An analysis of that compared to MERS coronavirus in Qatar and Egypt showed that the viruses differ from region to region, they found.

Currently, there is no available vaccine or recommended treatment for the virus, according to the U.S. Centers for Disease Control and Prevention. MERS is closely related to the SARS virus, which arose in China and killed 800 people worldwide in 2002 and 2003.

For comments and feedback contact: editorial@rttnews.com

Health News