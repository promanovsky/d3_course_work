TORONTO -- Ian Anderson is one of the 2.5 per cent.

After his first diagnosis of melanoma, the Brampton, Ont., man was told that without immediate treatment he would be dead within two months. After his second, he was informed that if 100 people were in his shoes, 97.5 would not make it to the five-year mark, the standard measuring stick for beating a cancer.

Nineteen years after the second occurrence, Anderson, now 56, is still around to tell the tale -- in his ever-present hat and sunscreen, which he slathers on religiously.

"I'm living proof that it can happen," says Anderson, who now counsels people diagnosed with melanoma.

"In doing the volunteer work that I do through peer support with the cancer society I do speak to people who are in a very similar situation to myself. And I've actually had more than one person say to me when we talk for the first time: 'Wow. You're actually giving me some hope.' And that's the whole point."

Anderson's story is one of optimism -- but also of reality. While he's living proof some lucky people can survive Stage 4 melanoma, he is also confirmation that people who don't take sun precautions can develop the more-often deadly disease.

"A lot of people have the mindset 'Well, it won't be me.' ... My job now is to tell people 'You know what? It can happen,"' says Anderson.

"Why not take some form of precaution if it can help you out? Simple as putting on a hat and putting on sunscreen. We're not talking Einstein here."

The Canadian Cancer Society worries that more people will hear the kinds of frightening diagnoses Anderson did in the future.

That's because skin cancers -- particularly the most deadly kind, melanoma -- are the most common kinds of cancers in Canada. And whereas the incidences of some cancers are actually going down, melanoma cases are on the rise, in both women and men. In 2014, it is estimated that 6,500 new melanoma diagnoses will be made, and 1,050 Canadians will die from this form of cancer.

"Because we don't get a lot of sun in this country, skin cancer isn't top of mind for most Canadians," says Prithwish De, an epidemiologist with the Canadian Cancer Society.

"There's over 80,000 skin cancer cases expected in Canada this year. And that's almost the same number of cases of the top four cancers combined -- lung, breast, prostate and colorectal cancer."

The skin cancer figures are among new national projections for the toll cancer will take in Canada this year. The cancer society estimates that 191,300 Canadians will get a new cancer diagnosis in 2014, and 76,600 will die from some form of cancer this year.

The estimate does not include 76,100 cases of non-melanoma skin cancer. Although these types of skin cancer make up about 40 per cent of all cancers in Canada, good estimates are not available because these types of cancers aren't generally reported as part of routine cancer surveillance.

The report says 45 per cent of Canadian men and 41 per cent of Canadian women will have a diagnosis of cancer at some point in their lifetimes.

Apart from skin cancers, the leading cancers among Canadian women are breast, lung and colorectal, in that order; among Canadian men, prostate, colorectal and lung are the main types of cancers. Lung cancer is the leading cancer killer among Canadians and is expected to claim the lives of 36,600 women and 40,000 men this year.

A piece of good news in the report relates to women and lung cancer. De says the tide has finally turned on this deadly form of cancer in Canadian women, with rates hitting a plateau after years of increases.

"This is the first year where we're able to see that the incidence rate has stabilized and that's actually great news because the rates had stabilized in Canadian men about 10, 15 years ago and we were anxiously waiting for that to occur among Canadian women," says De.

He explains that the decrease in smoking among Canadian women that started in the mid-1980s is finally beginning to be reflected in the cancer statistics. Decades ago women took up smoking later than men did. But they also, as a group, started giving up smoking later too. Lung cancer is a disease that takes decades to develop, so it has taken years for rates in women to start to follow the declining pattern of lung cancer in men.

Though the pattern of lung cancer cases shows Canadians got the message about the cancer risk posed by smoking, evidence suggests there is still work to be done on getting Canadians to adopt safe sun behaviours.

The cancer society compared data from two large surveys conducted 10 years apart -- in 1996 and 2006 -- and found that while more Canadians were reporting spending time outdoors, there was not a corresponding increase in the numbers of people reporting they are applying sunscreen, sporting hats, seeking shade or wearing long-sleeved clothing.

"We see more people spending time in the sun. But we don't see more people taking precautions," says Loraine Marrett, senior scientist for prevention and cancer control at Cancer Care Ontario.

One sun safety message that has been sent out consistently -- but hasn't always been heeded -- is the recommendation that people not use tanning beds. The surveys actually showed an increase in the proportion of people who said they used tanning beds between 1996 and 2006.

Caitlin Jones was an avid indoor tanner -- until she discovered at age 23 that a spot on her forehead just below her hair line was basel cell carcinoma, which comprises about four out of five non-melanoma skin cancers.

Jones had surgery to remove a quarter-sized piece of skin from her forehead to excise the cancer -- a procedure that altered her view on tanning. She notes that around the same time some of her friends' parents were also dealing with skin cancers.

"You kind of think: 'Oh, well, when you're 50 and 60, that's something you have to worry about.' But for me it was a rude awakening to realize that I was 23, 24, and that's already begun.

"Now I take a lot better care of my skin to prevent any further damage."