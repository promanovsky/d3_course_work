LONDON--Britons became more confident in early March than at any time since the near-collapse of mortgage lender Northern Rock in the autumn of 2007, an event that marked the beginning of the financial crisis in the U.K., a poll showed Friday.

The results suggest consumer spending will stay strong in coming months and continue to be the driving force for an economic recovery that began last year.

Polling firm GfK said Britons were questioned before Chancellor of the Exchequer George Osborne's annual budget March 19, which relaxed rules on retirees' pension withdrawals and included a modest cut in payroll taxes. Such measures, which appeared to spark a pickup in support for the main party in government, the Conservatives, could buoy confidence further in the near term.

Confidence levels were last higher in August 2007. That was the month before reports of financial difficulty at Northern Rock prompted deposit-holders to queue outside branches to withdraw their money, marking the first run on a British bank in over 100 years. Consumer confidence began to plunge in earnest early the next year, bottoming out at a level of minus 39 in June 2008.

GfK, reporting on behalf of the European Union, said Friday that its monthly gauge of confidence rose to minus 5 in March from minus 7 in February.

The increase leaves GfK's gauge 22 points higher than a year earlier, the biggest annual increase in over four years.

The improvement may in part reflect a sharp fall in the U.K.'s inflation rate and a gradual rise in wage growth that have eased pressure on household finances in recent months. Official data Thursday showed U.K. retail sales surged in February.

"People are now on balance more positive than negative about their own financial prospects over the next year," said Nick Moon, managing director for GfK's social research division. "It is unlikely that anything announced in the recent budget will reverse this."

People were particularly upbeat, relative to a month before, when quizzed about their intentions to make major purchases such as of furniture and electrical goods.

Improving confidence among consumers has accompanied a recovery in the U.K. economy that placed it as one of the fastest-growing developed nations last year. British policymakers say they believe the recovery to date has been too reliant on consumer spending and could prove difficult to sustain unless business investment and exports also pick up.

Write to Alex Brittain at alex.brittain@wsj.com