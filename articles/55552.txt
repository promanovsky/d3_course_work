Google has failed in its attempts to formulate algorithms which accurately predict the prevalence of flu, according to Harvard researchers who accused the search engine giant of technology "hubris".

When Google announced the Flu Trends application to track flu outbreaks in real-time, right down to the street level of those suffering from it, the company promised a new era in health, where new technology would more efficiently and quickly disperse the information required to allow doctors and pharmacies to prepare for an outbreak in advance.

Predicting flu outbreaks is best left to the experts, say Harvard researchers.

Google failed, according to Professor David Lazer and his team at Harvard Kennedy School in the US, who said Flu Trends, which uses search queries to compile its results, tends to overestimate the occurrence of flu when compared with data produced by decades-old organisations that manually collect influenza reports from labs.

The researchers said Google Flu Trends reported too many cases for 100 out of 108 weeks of each flu season between August 2011 and September 2013. It overestimated the prevalence of flu by more than 50 per cent.