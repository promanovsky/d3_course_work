A decreased ability to identify odours may be an early warning sign of Alzheimer's disease, while examinations of the eye could indicate the build-up of toxic beta-amyloid protein in the brain, scientists, including one of Indian-origin, have found.

Matthew E Growdon at Harvard Medical School and Harvard School of Public Health, and colleagues investigated the associations between sense of smell, memory performance, biomarkers of loss of brain cell function, and amyloid deposition in 215 clinically normal elderly individuals.

The researchers administered the 40-item University of Pennsylvania Smell Identification Test (UPSIT) and a comprehensive battery of cognitive tests.

They also measured the size of two brain structures deep in the temporal lobes - the entorhinal cortex and the hippocampus (which are important for memory) - and amyloid deposits in the brain.

At the Alzheimer's Association International Conference 2014 in Copenhagen, Growdon reported that in the study population, a smaller hippocampus and a thinner entorhinal cortex were associated with worse smell identification and worse memory.

They also found that, in a subgroup of study participants with elevated levels of amyloid in their brain, greater brain cell death, as indicated by a thinner entorhinal cortex, was significantly associated with worse olfactory function.

In another study, Davangere Devanand, Professor of Psychiatry (in Neurology and in the Sergievsky Center) at Columbia University Medical Center and colleagues investigated a multi-ethnic sample of 1,037 non-demented elderly people in New York City, with an average age of 80.7.

They were assessed in a variety of ways at three time periods - from 2004-2006, 2006-2008, and 2008-2010.

In 757 subjects who were followed, lower odour identification scores on UPSIT were significantly associated with the transition to dementia and Alzheimer's disease.

For each point lower that a person scored on the UPSIT, the risk of Alzheimer's increased by about 10 per cent.

Another study by Shaun Frost of the CSIRO (Commonwealth Scientific and Industrial Research Organisation, Australia) reported preliminary results of a research in which volunteers took a proprietary supplement containing curcumin which binds to beta-amyloid with high affinity.

It has fluorescent properties that allow amyloid plaques to be detected in the eye using a novel system from NeuroVision Imaging, LLC, and a technique called retinal amyloid imaging (RAI).

Volunteers also underwent brain amyloid PET imaging to correlate the retina and brain amyloid accumulation.

Preliminary results suggest that amyloid levels detected in the retina were significantly correlated with brain amyloid levels as shown by PET imaging.

The retinal amyloid test also differentiated between Alzheimer's and non-Alzheimer's subjects with 100 per cent sensitivity and 80.6 per cent specificity.