Oil: Not good about Iraq, but the U.S. is becoming the global powerhouse.

It's certainly not good that Iraq is in danger of splintering into three separate countries, but before everyone freaks out about the price of oil remember one thing: The U.S. is making up for the difference.

Right now, Iraq produces about three million of the roughly 90 million barrels produced globally on a daily basis.

Given the state of the country, it's a miracle Iraq even produces that much.

Read MoreOil prices head higher as Iraq tensions flare

The U.S., by contrast, produces roughly 8.5 million barrels a day. And--this is the key point--that number has grown by one million barrels a day this year, and will likely grow by an additional one million barrels a day next year.

How is this happening? That's the shale revolution. Names like Marcellus, Bakken, Permian Basin, and Eagle Ford--all production centers for oil and natural gas--have become household names. Add to that a revolution in the technology to get at that oil and natural gas, making drilling much more efficient.

Take a look at the largest oil producers in the world.

Largest oil producers in the world (millions of barrels a day)

Saudi Arabia: 11.0

Russia: 10.0

U.S.: 9.0

China: 4.0

Canada: 3.7

Iran: 3.7

U.A.E.: 3.4

Iraq: 3.1

Kuwait: 3.1

Mexico: 2.9

Venezuela: 2.7

Source: BP

By contrast, the Bakken producers over a million barrels a day. The Eagle Ford is over a million. And Marcellus is growing fast. Get the picture?

Will oil prices go higher? Sure, they could go higher, but bear in mind there is already an awfully high "risk premium" in oil from events in Ukraine, and remember there is also a civil war going on in Libya, continuing downward production trends in Venezuela and Mexico, and ongoing supply problems (i.e. theft) in Nigeria.

How much is that risk premium? It's not sure, but some of my oil trader friends think that if all these issues went away, oil would in the $80's rather than $106.

As for Iraq, every analyst I have spoken with or seen a report from today has noted that almost all Iraq exports are coming from southern Iraq, not the northern part where the fighting is occurring.

That's one reason oil, while up, is not up big.

And what about the drop in airline stocks? Many are down five percent. It's clearly an over-reaction...but remember airlines have become quasi-momentum stocks this year. They have high betas: Delta (DAL) has a beta of 1.6, meaning if the S&P is up 1.0 percent, DAL moves up 1.6 percent on average. The stock has almost doubled in the last nine months!

It's a good excuse to sell off transports.