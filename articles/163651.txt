Amazon.com Inc. (AMZN) on Thursday reported an increase in profit for the fourth quarter, thanks mainly to a 23 percent jump in revenues, with the online retailer splurging money on growth options. Earnings for the quarter came in line with analysts' estimate, while sales trumped expectations.

"We get our energy from inventing on behalf of customers, and 2014 is off to a kinetic start," said Jeff Bezos, founder and CEO of Amazon.com.

Seattle, Washington-based Amazon.com's first-quarter profit rose to $108 million or $0.23 per share from $82 million or $0.18 per share last year. On average, 36 analysts polled by Thomson Reuters expected earnings of $0.23 per share for the quarter. Analysts' estimates typically exclude special items.

Amazon.com's sales grew 23 percent to $19.74 billion from $16.07 billion last year. Forty analysts had a consensus revenue estimate of $19.43 billion for the quarter.

Lately, Amazon has been spending a lot to expand its portfolio of products and services to maintain its sales growth. The company early this month launched its set-top box Fire TV and is also to launch its much-rumored smartphone soon. Amazon.com offers merchandise such as books, movies, electronics, toys, grocery, apparel and tools. The company also makes devices such e-readers and tablets.

Operating costs for the quarter rose to $19.6 billion from $15.9 billion last year.

Looking forward to the second quarter, Amazon expect sales in the range of $18.1 billion to $19.8 billion. Analysts currently expect second-quarter sales of $19.03 billion.

The company expects operating loss of $455 million to $55 million for the second quarter, as its spending takes a toll on the bottom line.

AMZN closed Thursday's trading at $337.15, up $12.57 or 3.87%, on a volume of 6 million shares, on the Nasdaq. The stock further gained $0.35 or 0.25% in after-hours trade.

For comments and feedback contact: editorial@rttnews.com

Business News