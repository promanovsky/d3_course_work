BlackBerry has today reported a smaller than expected first-quarter loss, as the smartphone company's cost cutting and other initiatives started to pay off.

Shares jumped 10% in trading before the bell after BlackBerry said its gross profit margin in the quarter rose to 46.7%, from 33.9% a year earlier.

The Ontario-based company reported net income of $23m, or 4 cents a share, compared with a loss of $84m, or 16 cents, a year earlier.

Excluding a one-time non-cash accounting gain and certain restructuring charges, the loss was $60m, or 11 cents a share. Analysts, on average, had expected a loss of 25 cents a share, according to Thomson Reuters.

Quarterly revenue dropped to $966m from $3.07 billion a year earlier.