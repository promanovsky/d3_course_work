Topping tech headlines Wednesday was the inescapable Flappy Bird mobile game, which will reportedly return, someday.

In a series of tweets, creator Dong Nguyen said that the title will be available for download again, but not anytime soon. While he admitted he was not working on a new version yet, he did say that if the game is re-released, it will come with a warning about addictive gameplay.

Meanwhile, a California judge denied a request to turn a long-running Gmail lawsuit into a class-action suit. The plaintiffs, who accused Google of violating wiretap laws by anonymously scanning Gmail to serve up targeted ads, will now be forced to pursue the case separately. But the move would be costly, and probably not produce a lucrative-enough outcome to make it worthwhile.

In other news, Motorola hosted a Wednesday afternoon Google Hangout to formally introduce its new Moto 360 smartwatch, which runs Google's new Android Wear OS. Motorola design group leader Jim Wicks was on hand to discuss the brief history of smartwatches, the inspiration behind the Moto 360's design, and a potential look into the future of Android Wear.

Be sure to check out PCMag Live from Wednesday in the video, as well as a few other stories making headlines in the links below.

Further Reading