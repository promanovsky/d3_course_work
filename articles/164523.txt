James Martin/CNET

SAN JOSE, Calif. -- An appeals court decision Friday in a patent-infringement suit between Apple and Motorola threw a wrench in the ongoing Apple v. Samsung case currently being argued in a court here, extending the duration of the trial to give the parties time to present more evidence.

The US Court of Appeals for the Federal Circuit on Friday effectively revived a separate lawsuit between Apple and Motorola, which had been thrown out by Judge A. Posner of the Northern District. Apple had accused Motorola of infringing the patent for so-called "quick links," and it also has accused Samsung of infringing the patent. The fact that Apple could again argue its case against Motorola complicated current the trial here, causing a delay.

The '647 "quick links" patent, which is owned by Google after it purchased Motorola Mobility, covers actions such as a user being able to send a telephone number straight to the phone dialer versus having to memorize or copy and paste the number. Apple's patent involves the use of an "analyzer server" to perform the action, and the definition of how that works has differed between the Motorola and Samsung cases.

The 95-page ruling by the appeals court upheld many of Posner's definition:

"Regarding Apple's '647 patent, the parties dispute the meaning of the claim terms 'analyzer server' and 'linking actions to the detected structures.' The district court construed 'analyzer server' as 'a server routine separate from a client that receives data having structures from the client' and 'linking actions to the detected structures' as 'creating a specified connection between each detected structure and at least one computer subroutine that causes the CPU to perform a sequence of operations on that detected structure.' Apple argues that both constructions are erroneous. We disagree with Apple and affirm the district court's claim construction."

Because of the ruling, Apple and Samsung will present more testimony Monday. Both were granted one hour apiece above their originally allotted 25 hours to present expert-witness testimony about the '647 patent. The companies originally would have wrapped up their evidence in the first hour of court Friday, with closing arguments planned for Monday. Closing arguments now will take place Tuesday.

Judge Lucy Koh expressed her frustration with the ruling and what it means for the current case being argued in her court.

"Neither side has been shy about asking for other claim constructions," Koh said. "Now to come in and potentially blow up what we've already done with this jury for a month is frustrating to me."

The patent in question, No. '647 for quick links, is one of the most valuable patents wielded by Apple in its case against Samsung. As patent expert Florian Mueller noted, the ruling could mean that Apple's requested damages total in the current case -- $2.191 billion -- would have to be reduced by a third. Apple had asked for a royalty of $12.49 per unit for allegedly infringing the '647 patent. In total, the Cupertino, Calif., company asked for $40 per unit for Samsung devices that it says infringed all five of its patents.

Koh had allowed the patent to be interpreted in a way that differed from Posner's accepted meaning. Samsung has argued many times that Koh's interpretation was incorrect. Koh on Friday said it will take her some time to get through the ruling, but "just because a district court judge has construed a claim in one way doesn't mean I have to adopt that."

Mueller on his blog, Foss Patents, said that "the only reasonable solution I can see is that the '647 patent be withdrawn from the current case because Apple's infringement argument and its damages claim, both erroneously supported by Koh, were based on a wrong claim construction."

However, that will not happen. Rather, the companies will try to explain to the jury the differences.

"I'm not sure how the jury would understand these particular constructions without having further instructions with witnesses," Samsung attorney Dave Nelson argued early Friday.

However, Nelson reversed his stance when court resumed at 3:30 p.m. PT after a nearly five-hour recess. He then said that it wasn't necessary to call more witnesses.

"I do think this gives Apple a do-over," Nelson said. "They made a strategic choice, so strategic, that they tried to gut my case, literally not allow my expert to testify about analyzer servers."

Koh disagreed and gave each side one extra hour apiece to present information about the '647 patent.

"It's only fair to do a reopening of testimony," she said. Koh added that she will tell the jury that there are new instructions and because of that, the two sides get time to bring back technical experts.

The two parties did agree that it wasn't necessary to present more testimony related to damages, simplifying the issue.

Almost two years after Apple and Samsung faced off in a messy patent dispute, the smartphone and tablet rivals have returned to the same San Jose, Calif., courtroom to argue once again over patents before Federal Judge Lucy Koh. Apple is arguing that Samsung infringed on five of its patents for the iPhone, its biggest moneymaker, and that Apple is due $2 billion for that infringement. Samsung wants slightly more than $6 million from Apple for infringing two of its software patents.

While the companies are asking for damages, the case is about more than money. What's really at stake is the market for mobile devices. Apple now gets two-thirds of its sales from the iPhone and iPad; South Korea-based Samsung is the world's largest maker of smartphones; and both want to keep dominating the market. So far, Apple is ahead when it comes to litigation in the US. Samsung has been ordered to pay the company about $930 million in damages.

Most Samsung features that Apple says infringe are items that are a part of Android, Google's mobile operating system that powers Samsung's devices. All patents except one, called "slide to unlock," are built into Android. Apple has argued the patent infringement trial has nothing to do with Android. However, Samsung argues that Apple's suit is an " attack on Android" and that Google had invented certain features before Apple patented them.

Suing Google wouldn't get Apple far since Google doesn't make its own phones or tablets. Instead, Apple has sued companies that sell physical devices using Android, a rival to Apple's iOS mobile operating system. In particular, Apple believes Samsung has followed a strategy to copy its products and then undercut Apple's pricing. While Apple isn't suing Google, it expects that Google will make changes to its software if Samsung is found to infringe on patents through Samsung's Android devices.

In the current case, Apple and Samsung have accused each other of copying features used in their popular smartphones and tablets, and the jury will have to decide who actually infringed and how much money is due. This trial involves different patents and newer devices than the ones disputed at trial in August 2012 and in a damages retrial in November 2013. For instance, the new trial involves the iPhone 5 , released in September 2012, and Samsung's Galaxy S3 , which also debuted in 2012.

CNET

Samsung on Tuesday revealed that it has reduced the amount of damages it wants for Apple's accused infringement of two patents because it dropped the iPad from the list of infringing devices. Apple should pay Samsung about $6.2 million, testified Brigham Young University economics professor James Kearl, an expert hired by the Korean electronics maker to calculate damages. Earlier in the trial, Samsung asked for about $6.8 million in damages.

The difference comes from the '239 patent that covers video transfer. Samsung now wants $6.07 million in damages for infringement of the patent. Earlier, it asked for $6.78 million. The company dropped infringement claims against the iPad 2 , iPad 3, iPad 4, and iPad Mini over the weekend, Kearl said.

Samsung says Apple's FaceTime video call feature infringes the patent, as does technology that allows users to take videos and send them to other devices via email or text messages. It now has accused only the iPhone 4, iPhone 4S, and iPhone 5.

Apple on Tuesday also launched its own defense against Samsung's infringement suit. Its first witnesses, Apple engineers Tim Millet and Roberto Garcia, testified about the creation of technology used in iPhones and iPads. Millet serves as senior director of platform architecture at Apple, helping create the processors that power iOS devices. Garcia, meanwhile, talked about the creation of the FaceTime product that has been accused of infringing a Samsung patent.

James Storer, a professor of computer science at Brandeis University hired by Apple as an expert witness, then testified that Apple didn't infringe Samsung's patents. Apple rested its defense shortly before 2 p.m. PT.

The latest trial kicked off March 31 with jury selection. The following day featured opening arguments and testimony by Phil Schiller, Apple's head of marketing. Other witnesses who have testified include Greg Christie, an Apple engineer who invented the slide-to-unlock iPhone feature; Thomas Deniau, a France-based Apple engineer who helped develop the company's quick link technology; and Justin Denison, chief strategy officer of Samsung Telecommunications America. Denison's testimony came via a deposition video.

Apple experts who took the stand over the past few weeks included Andrew Cockburn, a professor of computer science and software engineering at the University of Canterbury, New Zealand; Todd Mowry, a professor of computer science at Carnegie Mellon University; and Alex Snoeren, a professor of computer science and engineering at the University of California at San Diego.

The crux of Apple's case came with two expert witnesses, John Hauser, the Kirin professor of marketing at the MIT Sloan School of Management; and Christopher Vellturo, an economist and principal at consultancy Quantitative Economic Solutions. Hauser conducted a conjoint study that determined Apple's patented features made Samsung's devices more appealing, while Vellturo determined the amount of damages Apple should be due for Samsung's infringement -- $2.191 billion.

Samsung, which launched its defense April 11 after Apple rested its case, called several Google engineers to the stand to testify about the early days of Android and technology they created before Apple received its patents. Hiroshi Lockheimer, Google vice president of engineering for Android, said his company never copied iPhone features for Android. Other Google Android engineers, Bjorn Bringert and Dianne Hackborn, also testified about features of the operating system.

High-ranking Samsung executives, including former Samsung Telecommunications America CEO Dale Sohn and STA Chief Marketing Officer Todd Pendleton, also took the stand during the weeks-long trial. The two executives testified about Samsung's marketing push for the Galaxy S2 and other devices, saying a shift in the Korean company's sales and marketing efforts -- not copying Apple -- boosted its position in the smartphone market.

The past several days of testimony have largely been experts hired by Samsung to dispute the validity of Apple's patents and to argue that Samsung didn't infringe. The experts include Martin Rinard, an MIT professor of computer science; Saul Greenberg, a professor of human computer interaction at the University of Calgary in Canada; Kevin Jeffay, professor of computer science at the University of North Carolina, Chapel Hill; and Daniel Wigdor, a computer science professor at the University of Toronto.

David Reibstein, chaired professor of marketing at the University of Pennsylvania's Wharton School of Business, on Friday refuted Apple expert Hauser's testimony from earlier this month. NYU Stern School of Business professor Tulin Erdem, meanwhile, on Friday also testified that she conducted her own studies, using eye tracking, to determine what devices consumers would buy. She concluded that Apple's patented features didn't boost desire for Samsung's products.

Judith Chevalier, a professor of economics and finance at the Yale University School of Management who was hired by Samsung, said her analysis determined that a reasonable royalty for Samsung's assumed infringement would be $1.75 per device, or $38.4 million overall. Apple had argued it deserved $40 per device for infringement as well as lost profits for a total of $2.191 billion.

There are seven patents at issue in the latest case -- five held by Apple and two by Samsung. Apple has accused Samsung of infringing US patents Nos. 5,946,647; 6,847,959; 7,761,414; 8,046,721; and 8,074,172. All relate to software features, such as "quick links" for '647, universal search for '959, background syncing for '414, slide-to-unlock for '721, and automatic word correction for '172. Overall, Apple argues that the patents enable ease of use and make a user interface more engaging.

Samsung, meanwhile, has accused Apple of infringing US patents Nos. 6,226,449 and 5,579,239. The '449 patent, which Samsung purchased from Hitachi, involves camera and folder organization functionality. The '239 patent, which Samsung also acquired, covers video transmission functionality and could have implications for Apple's use of FaceTime.

The Samsung gadgets that Apple says infringe are the Admire, Galaxy Nexus , Galaxy Note , Galaxy Note 2, Galaxy S2, Galaxy S2 Epic 4G Touch, Galaxy S2 Skyrocket, Galaxy S3, Galaxy Tab 2 10.1, and the Stratosphere. Samsung, meanwhile, says the iPhone 4 , iPhone 4S, iPhone 5, iPod Touch (fifth generation) and iPod Touch (fourth generation) all infringe.

The arguments by Apple and Samsung in the latest case should finish by the end of April. Court will be in session three days each week -- Mondays, Tuesdays, and Fridays -- though the jury will deliberate every business day until it has reached a verdict. Closing arguments likely will take place April 29.

Update, 10:22 a.m. and 4:50 p.m. PT: Added that the two sides will be presenting more testimony Monday because of an appeals court ruling.