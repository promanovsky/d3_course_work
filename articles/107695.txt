London: Scientists have revealed that prostate cancer tests, which predict how aggressive a tumour is, underestimate the severity of the disease in half of the cases.

In the study of 847 men with prostate cancer, 209 out of the 415 who were initially told their cancer was slow-growing, were found to have a more aggressive form of the disease, the BBC reported.

And for almost a third of the 415 men, it had spread beyond the prostate.

Scientists are calling for better tests to define the nature of the cancer.

For this study, scientists at the University of Cambridge graded the men's cancer before and after they had surgery, between 2007 and 2011.

Study author Greg Shaw, a urological surgeon at the University of Cambridge, said there were a "surprising" number of men who were not diagnosed appropriately the first time around.