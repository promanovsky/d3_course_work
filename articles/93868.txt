A postmortem examination on Peaches Geldof will be carried out on Wednesday at a Kent hospital by a Home Office pathologist.

The 25-year-old daughter of the rock star and campaigner Bob Geldof was found dead at her home in Wrotham on Monday afternoon. Police are treating the incident an "unexplained" and as a "non-suspicious sudden death".

Dr Peter Graham Jerreat will carry out the autopsy at Darent Valley hospital in Dartford. A decision on whether an inquest will be held will depend on the results of the examination, which could take several weeks, Kent county council said.

Earlier in the day, Fifi Geldof paid tribute to her younger sister in a message on her Instagram page. The 31-year-old wrote: "My beautiful baby sister … Gone but never forgotten. I love you, Peaches x." The note was alongside a picture of the two sisters pulling faces in a garden when they were young girls.Police said Geldof was pronounced dead at her house, after they were called there at 1.35pm on Monday.

Fans of the model, journalist and mother of two infant boys left bouquets of flowers close to the house in Fairseat Lane. Kimmy Milham, 30, from Maidstone, said she did not know Geldof, but had a friend whose children attended the same nursery as her children.

"She was a wonderful person and a wonderful mother," she said. "It's just tragic that her two sons will not grow up with her. Everybody at the nursery loved her and we all admired the way she brought up her kids."

"Officers are working to establish the circumstances and will be compiling a report of their findings for the coroner," a police spokesman said.

Kent police declined to confirm or deny reports that no drugs or suicide note were found at the home, where detectives were continuing their inquiries on Tuesday.

Geldof was married to the musician Thomas Cohen and they had two sons: Astala, who is almost two, and 11-month-old Phaedra.

Her former publicist, Ray Levine, said he was "beyond saddened" by the news of her death. "She was always entertaining. Very, very headstrong which made her quite difficult to look after because she wouldn't follow advice," he told the Sun. "She seemed to be heading off track but managed to pull herself back. Peaches lived with a deep sadness from childhood."

Her mother, Paula Yates, died from a heroin overdose in 2000 when Geldof was 11. Her transformation from wild child to devoted mother seemed to have been sealed when Mother & Baby magazine disclosed in February that she was to be its new columnist.

A magazine spokesman said it would feature her attempts to juggle parenting, work and family. A tribute posted online by the publication read: "We are extremely saddened at the tragic news that Peaches Geldof has died. Our hearts and sympathies are with her family and two little boys."

On Monday night, Geldof's father, Bob, issued a statement from the family describing them as "beyond pain",saying Peaches was "the wildest, funniest, cleverest, wittiest and the most bonkers of us all".

Her husband pledged to bring up their sons "with their mother in their hearts every day". Cohen added: "We shall love her forever."

The Irish president, Michael D Higgins, who began a state visit to Britain on Tuesday and was due to meet her father during the trip, said his and his wife's thoughts were with the family. "I extend my deepest sympathies to Bob Geldof and his family on the sudden and untimely death of his daughter Peaches.

"This is such a difficult cross to bear for any family and all of our thoughts are with Peaches' family and friends at this time. Sabina and I were due to meet Bob Geldof while on the state visit and we are thinking of him at this time of immense loss."