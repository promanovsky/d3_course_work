NEW YORK, May 7 (UPI) -- Chinese e-commerce company Alibaba filed paperwork Tuesday for what could be the largest initial public offering (IPO) in U.S. history.

Alibaba is not well-known in the U.S., but it is a household name comparative to Google in China. Last year, the companies two main online shopping websites, taobao.com and tmall.com had a combined sales of $240 billion. That is three times the size of Ebay and double the size of Amazon.

Advertisement

The tech giant filed with the Securities and Exchange Commission to raise $1 billion, but that number is considered to be just for filing purposes. Stock market analysts are saying that it could bring in more than the $16 billion Facebook raised in 2012.

The filing did not reveal the ticker symbol the company would use, or the exchange on which it plans to list its shares. Both the Nasdaq and the New York Stock Exchange (NYSE) are competing for Alibaba's listing.

RELATED Alibaba and UCWeb partner to launch new mobile search service

Some people are wondering why the company would choose to file in New York instead of Hong Kong, but analysts say that it's because it widens the potential for more investors.

"By listing in the U.S. you open up the doors to a lot more potential investors. There are some question marks about accounting and security rules outside the U.S. and European markets. The U.S. listing will add a layer of creditability," said R.J. Hottovy, senior analyst with Morningstar.

The company was founded in 1999 by Ja Mcack, a former English teacher, four-years after he accessed the internet for the first time during a business trip to Seattle.