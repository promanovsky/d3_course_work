Washington: A new study by NASA has revealed that there might be cracks in Pluto`s giant moon Charon, which could indicate that it once had a subterranean ocean of liquid water.

Alyssa Rhoden of NASA`s Goddard Space Flight Center in Greenbelt, Maryland said that their model predicts different fracture patterns on the surface of Charon depending on the thickness of its surface ice, the structure of the moon`s interior and how easily it deforms and how its orbit evolved.

She further explained that depending on exactly how Charon`s orbit evolved, particularly if it went through a high-eccentricity phase, there may have been enough heat from tidal deformation to maintain liquid water beneath the surface of Charon for some time.

Pluto`s remoteness and small size make it difficult to observe, but in July of 2015, NASA`s New Horizons spacecraft will be the first to visit Pluto and Charon and will provide the most detailed observations to date.

The research is published online in the journal Icarus.