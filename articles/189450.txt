Hewlett-Packard said it plans to invest more than $1 billion over the next two years to develop and offer cloud-computing products and services.

The company said it will make its OpenStack-based public cloud services available in 20 data centers over the next 18 months.

Read More HP's Meg Whitman: One of my 'big failures' at eBay



OpenStack, a cloud computing project that HP co-founded, provides a free and open-source cloud computing platform for public and private cloud services.