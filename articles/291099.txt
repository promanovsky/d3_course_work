These are stories Report on Business is following Monday, June 16, 2014.

Follow Michael Babad and The Globe's Business Briefing on Twitter.

Greenpeace hit by bad deals

Greenpeace has its very own rogue trader.

Story continues below advertisement

The environmental group, known for its high-profile protests, disclosed the loss of €3.8-million, or $5.6-million in Canadian terms, in currency transactions by a misguided staffer.

"As a global organization active in more than 40 countries, Greenpeace International is funded with many different currencies, valuations of which can change rapidly," the group said in a statement yesterday.

"The losses are a result of a serious error of judgment by an employee in our International Finance Unit acting beyond the limits of their authority and without following proper procedures," it added.

"Greenpeace International entered into contracts to buy foreign currency at a fixed exchange rate while the euro was gaining in strength. This resulted in a loss of €3.8-million against a range of other currencies."

The staffer in question, who wasn't identified, thought he was making a smart move for the group, rather than trying to gain personally, the group said. He has been fired from the job.

"We offer a full apology to our supporters for the series of errors that led to the loss," it said.

"We further wish to reassure people that every possible action is being taken to avoid the possibility of such a loss ever occurring again in future."

Story continues below advertisement

There will also be a "complete independent audit" to find out how this happened.

The loss will be reflected in Greenpeace's annual report for last year, which will include a budget deficit of €6.8-million and income of €72.9-million.

"We will make adjustments for these losses in the coming two or three years by amending planned infrastructure investments," Greenpeace said.

Greenpeace is worried that the trading loss will trouble those who support the organization, spokesman Mike Townsley told The Associated Press.

Indeed, the group said in its statement that its donors "will rightly be surprised and disappointed" by the hefty loss.

"Every indication is, this was done with the best of intentions but not the best of judgment," Mr. Townsley told the news organization.

Story continues below advertisement

Economy on track

Canada's economy is performing largely as projected in the Conservative government's February budget, increasing the odds of a significant federal surplus next year, The Globe and Mail's Bill Curry reports.

Finance Minister Joe Oliver met with private sector economists today in Ottawa and later released an updated forecast for growth, inflation, employment and other measurements.

"We're on track and we will be in a surplus position next year," Mr. Oliver told reporters following the meeting.

The Conservative government uses the average forecast as the foundation of its revenue projections for the fall economic update and budget. However the government adds an "adjustment for risk" into the numbers by underestimating revenue by $3-billion a year in an effort to cover unforeseen changes in the economy.

The private sector average released Monday forecasts real GDP growth of 2.2 per cent in 2014, followed by 2.5 per cent in 2015, 2.5 per cent in 2016 and 2.3 per cent in 2017.

Home prices to rise

Canada's realtors expect average home prices to climb 5.7 per cent this year, though driven by just three provinces.

Story continues below advertisement

The average national sales price should hit $404,300, boosted by British Columbia, Alberta and Ontario, the Canadian Real Estate Association said today. Other provinces will see just modest prices.

The group forecast much slower price growth in 2015, when the average will rise just 0.7 per cent to $407,300.

In its new forecast, as The Globe and Mail's Tara Perkins reports, CREA now expects sales to climb 1.2 per cent from 2013, to 463,400 units, with British Columba seeing its fastest growth on record, at 8.3 per cent.

Sales will rise a further 0.9 per cent next year, to 467,800, it said.

The group also reported that sales rose 4.8 per cent in May from a year earlier, and 5.9 per cent from April.

The average price came in 7.1 per cent above last year's level, but that is "skewed" by the expensive Vancouver and Toronto markets.

Story continues below advertisement

The better measure, the MLS home price index, showed a gain of about 5 per cent in May.

Enbridge awaits key decision

The Canadian government is set to announce a key decision on Enbridge Inc.'s politically-charged Northern Gateway project, having spent the past 18 months persuading the public that building an oil sands pipeline through British Columbia is in the national interest, The Globe and Mail's Shawn McCarthy writes today.

Under legislation passed in 2012, Ottawa has a Tuesday deadline for responding to a joint review panel report issued last December that recommended cabinet approve the $7.9-billion project, subject to Enbridge meeting 209 conditions.

The government faces a tough political battle, and is expected to qualify any approval with an admonition to Enbridge that it must win greater support in B.C. before it begins construction. The company can't start building the project until it has satisfied the panel that it has addressed 111 of the 209 conditions, with the rest laying down ongoing responsibilities for project management and reporting. Enbridge executives have said construction would not begin for at least 18 months.

Will Quebecor expand wireless?

There's a growing probability that Quebecor Inc. will decide to go ahead with the expansion of wireless service across Canada amid signs of encouraging talks with the federal government, says a telecom analyst.

"We believe conversations between [Quebecor] and the government have been friendly," Barclays Capital analyst Phillip Huang said in his weekly telecom report today, The Globe and Mail's Bertrand Marotte reports.

Story continues below advertisement

"While the support that [Quebecor] is seeking is certainly not immaterial, we believe the government may be more receptive than expected,"

The federal government is actively engaged in encouraging a viable fourth national wireless carrier to compete with the Big 3, Rogers Communications Inc., BCE Inc. and Telus Corp.

Quebecor's Vidéotron division recently clinched licences for cellular airwaves in Ontario, British Columbia and Alberta, and it has been assessing an expansion outside Quebec but only if the right conditions are in place.

Streetwise (for subscribers)

Real estate

ROB Insight (for subscribers)

Business ticker