It’s been touted as a rare astrological event that could herald the “End of Days.”

But if the experience of generations gone by is anything to go by, we’ll be all right.

Mars, Earth and the sun today aligned in a rare “opposition of the planets” that only happens once every 778 days.

And it comes six days before a full lunar eclipse, when the entirety of the moon is shaded by Earth and appears as an eerie reddish color.

Interestingly, this month’s total eclipse is the first of four consecutive total eclipses dubbed “blood moons” occurring at approximately six-month intervals — a phenomenon called a tetrad.

In addition to the two totalities occurring this year, there will be two more in 2015, on April 4 and Sept. 28.

Some Christians believe the phenomenon represents the End of Days and the second coming of Christ.

The King James Bible predicts: “The sun shall be turned into darkness, and the moon into blood, before the great and the terrible day of the LORD comes” [Joel 2:31].

Pastor and author John Hagee, from San Antonio, Texas, has written a book on the phenomenon. He believes today marks the dawn of a “hugely significant event” for the world.

He says, “This is not something that some religious think tank has put together.

“NASA has confirmed that the Tetrad has only happened three times in more than 500 years — and that it’s going to happen now.”

The first Tetrad since the Middle Ages, in 1493, saw the expulsion of Jews by the Catholic Spanish Inquisition, which rocked Western Europe.

The second coincided with the establishment of the state of Israel — after thousands of years of struggle — in 1949.

The last one occurred in 1967 — far earlier than expected — precisely at the time of the Arab-Israeli Six-Day War.

This article originally appeared on News.com.au.