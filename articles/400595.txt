Running as little as five to 10 minutes per day can significantly cut the risks of getting heart disease. — ©Kzenon/shutterstock.com

Subscribe to our Telegram channel for the latest updates on news you need to know.

NEW YORK, July 30 — Going out for a quick, daily run may be just as effective as a long-distance jaunt when it comes to prolonging your life, said a study out Monday.

Running as little as five to 10 minutes per day can significantly cut the risks of getting heart disease and dying young, said the findings in the Journal of the American College of Cardiology.

People who exercised by running showed a 30 per cent lower risk of death and a 45 per cent lower risk of dying from cardiovascular disease than people who did not run at all.

Runners could be expected to live about three years longer on average than non-runners.

Even more, the benefits of running were the same whether people ran a little or a lot, fast or slow.

There was no statistically significant difference among those who ran 50 minutes per week and those who ran 180 minutes per week, it found.

Nor did it matter if the running was happening at a pace of less than six miles per hour (10 km per hour).

Even these minimal runners and slow joggers fared far better than people who did not run at all.

“Since time is one of the strongest barriers to participate in physical activity, the study may motivate more people to start running,” said lead author Duck-chul Lee, an assistant professor in the Iowa State University Kinesiology Department.

Minimal effort

The study was based on more than 55,000 adults — average age 44 — in Texas who were followed for 15 years. Most were white; about one quarter of those in the study were women.

For the analysis, researchers broke the participants into six groups: non-runners and five groups of runners according to weekly running time, distance, frequency and speed.

“Even the lowest quintiles of weekly running distance (six miles or less), frequency (one to two times)... and speed (less than six mph) had significantly lower risks of all-cause mortality compared with not running,” the study said.

“Similar trends were observed with the risk of cardiovascular disease mortality.”

Studies have shown that globally, between 40 per cent and 80 per cent of the public does not exercise enough.

US health authorities recommend 75 minutes per week of vigorous exercise, or 150 minutes (a half hour a day, five days a week) of moderate-intensity exercise.

“Although such low compliance with physical activity guidelines could be due to unawareness of the strong benefits of exercise, it is also possible that the standard is perceived as being too high, which could deter many people from even trying,” said an accompanying editorial.

“Instead, establishing goals that can be attained with minimal effort becomes important.”

Researchers also found that when comparing running to walking, a five-minute run brought the same health benefits as a 15-minute walk.

A previous study in The Lancet in 2011 showed that brisk walking for 15 minutes a day — half the recommended time amount — could also add three years to a person's life. — AFP-Relaxness