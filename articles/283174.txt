NEW YORK (The Deal) -- Online booking company Priceline Group (PCLN) is adding to its portfolio of services with the $2.6 billion acquisition of OpenTable (OPEN) , announced Friday.

Priceline CEO Darren Huston called OpenTable "a natural extension" of its group of brands in a Friday call with investors.

The transaction values OpenTable at $103 per share, a 46% premium the prior close. Shares of the target jumped $33.28, or 47%, to $103.71 on Friday morning, brushing past Priceline's bid. Priceline stock dropped $15.09, or 1.2%, to $1,210.91.

OpenTable makes 15 million reservations per month internationally. The company generated $81.5 million in adjusted Ebitda last year. Revenue grew 18%, to $190 million, in 2013.

Huston outlined similarities in the "DNA" of OpenTable's restaurant booking service and Priceline's travel accommodations offerings.

"It's the same customers," he said. "Travellers are diners."

Priceline Group owns five main brands: priceline.com; hotel sites Booking.com and agoda.com; travel deal site KAYAK and rentalcars.com.

OpenTable would operate independently from its San Francisco headquarters following the deal.

Huston said Friday that both companies are leaders in their markets, match global demand with local supply and provide services on multiple types of devices.

Priceline could help OpenTable grow internationally, "which has been a challenge to them," Huston said. Priceline also plans to improve the target's wireless offering.

The deal is Priceline's second purchase in a week. The company announced the acquisition of hotel digital marketing group Buuteeq Inc. on Tuesday.

The OpenTable purchase includes $2.5 billion in net cash and $90 million in Priceline equity to cover options.

Priceline CFO Daniel Finnegan noted that the buyer has cash on its balance sheet and can access the capital markets.

The companies said they expect to close the deal in the third quarter. OpenTable could have to pay a $91 million breakup fee if it terminates the deal.

Wells Fargo Securities bankers John Jinishian and Gerry Walters advised Priceline, which received counsel from Sullivan & Cromwell lawyers led by Keith Pagnani and Brian Hamilton.

OpenTable retained George Boutros, Jonathan Turner and Marcie Vu of Qatalyst Partners and Latham & Watkins lawyers Luke Bergstrom and Chad Rolston.