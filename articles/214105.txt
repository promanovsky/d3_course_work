There was once a time where video games were just that, games that we interacted with on our computers or on our consoles. However with smartphones and tablets becoming more popular and becoming ubiquitous, video games are starting to have tie-ins with our mobile devices. We’ve seen how developers like Blizzard and Konami create apps for their games, so it hardly comes as a surprise that EA would do the same too.

Advertising

It has recently been revealed that EA has released an app for its popular FPS game, Titanfall. The app will be available on the iTunes App Store and the Google Play Store where it will be a free download, and where it will also serve as a companion app to the game. The app will be able to deliver notifications to your phone, as well as provide users with information about the Titanfall universe.

This includes information about characters, weapons, maps, modes, and controls. For those who are playing the game on their Xbox One console, the app will also be able to turn your phone into a secondary display. This will allow you to view a full-sized map or an interactive mini map. Gamers will be able to zoom in and out and view scoreboard updates in real time too, which we have to admit sounds pretty cool.

Like we said earlier, the app itself is free for download, so if you’d like to take it for a spin, hit up your respective app store for the download!

Filed in . Read more about Apps and Titanfall.