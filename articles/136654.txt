Nike reportedly made an abrupt exit from the crowded wearable technology market Friday. The sportswear giant fired much of the team responsible for the development of its FuelBand and shelved plans for a new version of the fitness tracker, CNET reports.

Up to 55 people from Nike's 70-member hardware team were let go from the company Thursday. In an email to CNET, a Nike spokesperson confirmed "a small number of layoffs."

Despite the layoffs, Nike told Mashable "the Nike+ FuelBand SE remains an important part of our business."

"We will continue to improve the Nike+ FuelBand App, launch new METALUXE colors, and we will sell and support the Nike+ FuelBand SE for the foreseeable future," said Nike spokesperson KeJuan Wilkins.

It's unclear if it will make new hardware for the category in the future.

News of the impending firings appeared on the anonymous social app Secret last week.

Nike debuted the fitness tracker in early 2012 to okay reviews, and has faced serious competition from wearable makers like Pebble, Samsung and LG. The release of the Gear Fit from Samsung last week and constant rumors about Apple's possible move into the wearables space could have scared Nike off.

Wilkins added that the company will continue to leverage partnerships to expand its ecosystem of digital products and services, specifically related to its FuelBand software. He highlighted Nike's ongoing partnership with Apple products over the years, such as Nike+ Running for iOS and other apps such as Nike+ Training Club, Nike+ FuelBand and Nike+ Move.

"Building on these successful products and services, Nike and Apple continue to partner on emerging technologies to create better solutions for all athletes," Wilkins said.

Perhaps Nike has its sights set on bringing the FuelBand software to the iWatch too.

Additional reporting by Samantha Murphy Kelly