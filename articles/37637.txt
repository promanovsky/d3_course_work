SAN FRANCISCO (MarketWatch) — Plug Power Inc. soared Tuesday after the company’s chief executive disclosed that he is on the verge of announcing a major deal in the next few weeks. Plug Power’s rally ignited strong gains in other fuel cell stocks.

Plug Power PLUG, -1.25% , which was up about 9% in early afternoon, spiked 36% after Chief Executive Andy Marsh told MarketWatch that he will be announcing a deal with a global automaker in two to three weeks.

FuelCell Energy Inc. FCEL, +1.71% also rose in tandem with the stock up 13% and Ballard Power Systems Inc. BLDP, -1.88% shares up 20%.

Gainers

Walt Disney Co. was up after a deal to buy Maker Studios. Disney

McCormick MKC, -0.26% shares gained 5.6%. The spice maker on Tuesday reported fiscal first-quarter earnings of 62 cents a share, ahead of a forecast of 58 cents a share by analysts in a FactSet survey.

Seagate Technology Inc. STX, -0.35% shares rose 4.1%. The stock is one of the tech names recently chosen by MarketWatch’s Jeff Reeves for its staying power, growth and yield.

Bad weather delays search for Flight 370

Decliners

Carnival CCL, -0.58% shares fell 5%. The cruise operator on Tuesday said it swung to a first-quarter loss of 2 cents a share from a profit of 5 cents a share in the year-earlier period. On an adjusted basis, the company said it would have broken even.

Chipotle Mexican Grill Inc. CMG, -1.00% shares slid 3%. Analysts at Sterne Agee last week reiterated their buy rating on the stock and raised its price target to $742 from $603.

Akamai Technologies Inc. AKAM, +0.13% shares dropped 2.9%. Analysts at B. Riley said they are on the sidelines on the stock due to limited upside to revenue estimates for 2014.

Himax Technologies Inc. HIMX, +3.86% slumped 12%. Bank of America Merrill Lynch downgraded the semiconductor maker to underperform, saying the highly competitive market will make margin expansion difficult for Himax according to news reports.

Tickers to Watch

NFLX: Netflix Inc. NFLX, -0.19% extended losses to fall 1.9% in the wake of reports that Apple Inc. AAPL, +0.18% and Comcast Corp. CMCSA, were in talks to develop their own video streaming service.

DIS: Shares in Walt Disney Co. DIS, -1.70% rose 0.3% after the media giant said late Monday it was buying Maker Studios for as much as $950 million. The privately held studio is credited with making the Harlem Shake video go viral.

WAG: Walgreen Co. US:WAG climbed 2.9%. The drugstore chain on Tuesday reported second-quarter earnings of 78 cents a share and said it would close 76 stores.

More from MarketWatch:

Why I’m betting against Warren Buffett

Fed’s Plosser sees rates at 3% at end of 2016

Bill Gates: ‘Techno-Fixing Luddite or world savior?