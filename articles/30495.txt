Toyota Motor Corp’s agreement to pay US$1.2 billion to end a probe of its attempt to hide safety defects was approved by a US federal judge on March 20, 2014. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

NEW YORK, March 21 — Toyota Motor Corp’s agreement to pay US$1.2 billion (RM3.9 billion) to end a US probe of its attempt to hide safety defects was approved by a federal judge, who said the case shows how “corporate fraud can kill.”

The accord is the largest criminal penalty imposed on an automaker in the US, Attorney General Eric Holder said yesterday. As part of the settlement, Toyota, which recalled more than 10 million vehicles in connection with uncontrolled acceleration, admitted wrongdoing and agreed to pay the penalty as well as submit to “rigorous” review by an independent monitor.

“This unfortunately is a case that demonstrates that corporate fraud can kill,” US District Judge William Pauley said today in court in Manhattan in accepting the deal. “I sincerely hope that this is not the end but rather a beginning to seek to hold those individuals responsible for making those decisions accountable.”

Toyota was charged with wire fraud, which the government agreed not to prosecute for three years as long as the company, which pleaded not guilty, continues cooperating with authorities.

The recalls blemished Toyota’s reputation and caused it to relinquish its title as the world’s top-selling automaker for one year to General Motors Co, which is now facing probes into how it handled defective ignition switches blamed for at least 12 deaths.

Unintended acceleration

Toyota recalled more than 10 million vehicles worldwide in 2009 and 2010 following complaints of sudden, unintended acceleration. The Toyota City, Japan-based company made modifications to gas pedals and floor mats that were prone to shifting around and jamming the accelerator. Toyota also installed brake override software on recalled models and began making the systems standard on new vehicles.

Christopher P. Reynolds, chief legal officer for Toyota Motor North America, appeared in court today on behalf of the company, telling the judge that he understood the terms of the accord and financial penalty. In a statement yesterday, Reynolds said the company “took full responsibility” for its actions.

Toyota will not seek tax deductions or credits for the penalty, according to the deferred prosecution agreement.

A statement of facts about the defects that Toyota acknowledged was true as part of the deal “paint a reprehensible picture of corporate misconduct,” Pauley said.

‘Deceptive statements’

Toyota admitted in the statement that it misled US consumers from late 2009 through March 2010 “by concealing and making deceptive statements” about safety issues related to sudden acceleration.

The company also gave inaccurate information to members of Congress, and concealed from regulators the extent of problems some customers experienced with sticking gas pedals and unsecured floor mats, Holder said.

The automaker “undertook acts of concealment” amid scrutiny that followed an August 28, 2009, accident in San Diego. The driver, a California Highway Patrol officer and three family members were killed in the incident after the accelerator of a Lexus ES350 became stuck in the floor mat, causing the vehicle to run an uncontrollably at more than 100 miles an hour, the government said.

The sound of the crash was recorded on a 911 call made from the vehicle just after the caller said “we’re approaching the intersection...hold on...hold on and pray,” according to the government.

‘Root cause’

After the accident, Toyota agreed to recall eight of its models, including the ES350, for floor-mat entrapment, saying it had “addressed the root cause” of the issue, even though it knew from internal testing the defect was more widespread, the government said.

One of the cars not included in the initial recall, the Corolla, had among the worst records for floor-mat entrapment, Toyota’s engineers found — a conclusion the company didn’t share with US safety officials at the time, the government said. Toyota also initially failed to tell US officials that sticky pedals were a second cause of unintended acceleration that had surfaced in Europe, according to the government.

“At the maximum moment of crisis” Toyota was saying “loudly and forcefully, on television and in press releases and on their website, at every juncture, to reassure the public and protect its brand. ‘Don’t worry about it, we’ve got it covered, we have gotten to the root cause of the problem.’ And that was false,” Manhattan US Attorney Preet Bharara said yesterday.

Vehicle values

In addition to the criminal probe by the Manhattan US Attorney’s office and the New York office of the FBI, Toyota’s recalls also led to lawsuits claiming defects harmed the value of Toyota vehicles or caused accidents leading to death and injury. Toyota settled for about US$1.6 billion lawsuits brought by car owners who claimed economic losses.

Toyota last year agreed to try to resolve the personal injury and wrongful death lawsuits.

In a March 17 status report filed in federal court in Santa Ana, California, Toyota and lawyers representing the plaintiffs said they had reached agreements in principle to settle 131 cases. The carmaker is trying to settle more than 300 cases, according to the filing. Terms of the settlements weren’t disclosed.

The economic-loss agreement and yesterday’s penalty bring the automaker’s legal pay-outs to almost US$3 billion. Toyota may earn US$19 billion in the 2014 fiscal year, the average of analyst estimates.

The case is US v. Toyota Motor Corp, 1.14-cr-00186, US District Court, Southern District of New York (Manhattan).— Bloomberg