It is one of the most famous parks in the world and attracts visitors from far and wide. But now Paris's Tuileries garden, next to the Louvre museum, is attracting another kind of visitor in possibly even greater numbers.

"It's horrible, we're scared of being bitten," said Audrey Hacherez, a gardener who was weeding a flowerbed on Monday in the formal gardens, which stretch along the Seine. "They're really big. Sometimes they fight each other."

Tourists' litter is being blamed for an influx of rats that was brought to the attention of Parisians last week after a photographer, Xavier Francolon, took pictures of the rodents scampering in the gardens. He told Le Parisien that he had seen about 30 in the space of the two days, and had been surprised to see so many among picnickers on the grass in broad daylight.

"The tourists throw their scraps of pizza and sandwiches all over the place," said Hacherez.

Standing beside a lavender bed strewn with plastic bottles and discarded food wrappings, another gardener explained that they were using an "ecological" poison against the rats but that it was proving less effective than chemical varieties. The gardeners said they had approached the Louvre's technical experts about the problem and were waiting to hear back.

The Louvre, which along with the culture ministry is responsible for maintaining the Carrousel and Tuileries garden, said on Monday that pest control is carried out twice a month, and more frequently in the summer months. The museum, which coordinates with the city of Paris sanitation services, was aware that last week "there were more rats than usual" in the gardens. It noted that "like any space or urban building near a river, particularly in the centre of Paris, the public domain of the Louvre museum can be the victim of a large and harmful presence of rodents, particularly in the summer".

The rat problem in the Louvre gardens has been acute for the past couple of years, according to local residents.