Nancy Blair

USA TODAY

SAN FRANCISCO — A new batch of Chromebooks will be landing this summer from Intel and a range of computer makers including Lenovo, Acer, Dell, Asus and Toshiba.

The lightweight, low-cost laptops running on Google's Chrome operating systems run on a new generation of Intel microprocessors. They will sell for $300 to $400.

Among the new Chromebooks announced Tuesday, Lenovo is launching its first consumer Chromebooks, including a touchscreen model.

The touchscreen N20p (starting at $329; available in August) is a "convertible" design so you can fold it back in addition to using it in regular laptop mode. The N20 (starting at $279; available in July) has a traditional laptop design and no touchscreen capabilities. They join a couple of Lenovo Chromebooks aimed at the education market.

Additionally, ASUS announced two new Chromebooks, the 11.6-inch C200 and 13.3-inch C300, which will begin shipping in late June starting at $249.99.

Acer announced a new C720 Chromebook version with Intel's 4th generation Core i3 processor. Acer said it will be available "early in the back-to-school season" for $349.99.

Dell later in the year will expand its Chrome-based offerings to include a configuration of the Dell Chromebook 11 with a Corei3 processor to appeal to business users. The Dell Chromebook 11 is widely used in classrooms.

Chromebooks are still a fraction of the overall computer market but they've been gaining steam in school settings and among consumers interested in on-the-go computing at a low price.

In terms of functionality, they fall somewhere between tablets and laptops. Google's Chrome OS is meant to be used online. It can't handle traditional software like full-featured photo or video editing programs.

But since they were first introduced a few years ago, Google has developed a strong app inventory to help you get work done. And there are more ways to use the machines offline.

As part of Tuesday's announcement, Google said that the ability to watch movies and TV shows while offline will be included in an update to the Chrome operating system in the next few weeks.

Contributing: The Associated Press