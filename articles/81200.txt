Stocks moved mostly higher over the course of the trading day on Tuesday, adding to the gains posted in the previous session. With the upward move on the day, the S&P 500 reached a new record closing high.

The major averages all closed firmly positive, although the Nasdaq posted a standout gain. While the Nasdaq surged up 69.05 points or 1.6 percent to 4,268.04, the Dow rose 74.95 points or 0.5 percent to 16,532.61 and the S&P 500 advanced 13.18 points or 0.7 percent to 1,885.52.

The continued strength on Wall Street was partly in reaction to a relatively upbeat reading on U.S. manufacturing activity in the month of March.

The Institute for Supply Management said its index of activity in the manufacturing sector crept up to 53.7 in March from 53.2 in February. A reading above 50 indicates growth in the sector.

The internals of the report were somewhat mixed. While the production and backlog of orders indices showed notable increases, the employment index saw a moderate decrease.

Peter Boockvar, managing director at the Lindsey Group, said, "Bottom line, some normality in the was reflected in the data but the headline figure is still 1 point below its 6-month average."

A separate report from the Commerce Department showed a modest uptick in construction spending in the month of February.

The report said construction spending inched up by 0.1 percent to a seasonally adjusted annual rate of $945.7 billion in February.

The modest increase matched economist estimates, but revised data showed that spending fell 0.2 percent to a rate of $944.6 billion in January.

Overseas, official data released by the Chinese National Statistical Office and the China Federation of Logistics and Purchasing showed that their manufacturing purchasing managers' index edged up to 50.3 in March from 50.2 in February.

Sector News

Continuing to recover from their recent pullback, biotechnology stocks moved sharply higher on the day. The NYSE Arca Biotechnology Index surged up by 2.5 percent, bouncing further off the nearly two-month closing low set on Friday.

Pharmacyclics (PCYC) and Celgene (CELG) turned in two of the biotech sector's best performances, jumping by 7.2 percent and 5 percent, respectively.

Airline stocks also turned in a strong performance, driving the NYSE Arca Airline Index up by 2.3 percent to a nearly twelve-year closing high. United Continental (UAL) led the sector higher after UBS raised its rating on the company's stock to Buy from Neutral.

Significant strength was also visible among networking stocks, as reflected by the 2.1 percent gain posted by the NYSE Arca Networking Index. Cisco (CSCO) and Alcatel-Lucent (ALU) posted standout gains.

Telecom, computer hardware, housing, and semiconductor stocks also saw considerable strength, moving higher along with most of the other major sectors.

Other Markets

In overseas trading, stock across the Asia-Pacific region turned in a mixed performance during trading on Tuesday. Japan's Nikkei 225 Index edged down by 0.2 percent, while Hong Kong's Hang Seng Index surged up by 1.3 percent.

Meanwhile, the major European markets all moved to the upside on the day. While the German DAX Index rose by 0.5 percent, the U.K.'s FTSE 100 Index and the French CAC 40 Index both advanced by 0.8 percent.

In the bond market, treasuries moved moderately lower, extending the pullback seen over the two previous sessions. Subsequently, the yield on the benchmark ten-year note, which moves opposite of its price, rose by 3.6 basis points to 2.759 percent.

Looking Ahead

Economic data may continue to impact trading on Wednesday, with ADP scheduled to release it report on private sector employment in the month of March.

For comments and feedback contact: editorial@rttnews.com