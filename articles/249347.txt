LOS ANGELES, CA - JULY 01: (L-R) Singer Beyonce, rappers Jay-Z and Kanye West and television personality Kim Kardashian attend the 2012 BET Awards at The Shrine Auditorium on July 1, 2012 in Los Angeles, California. (Photo by Christopher Polk/Getty Images For BET)

Beyonce may not have made it to the wedding, but she was eager to share some well wishes for newlyweds Kim Kardashian and Kanye West.

The 32-year-old singer took to Instagram to post a message for Kimye following their extravagant nuptials in Italy, writing, "Wishing you a lifetime of unconditional love. God bless your beautiful family," alongside a photo from their Vogue shoot with baby North.

Beyonce and Jay Z skipped the wedding at Florence's 16th-century Forte di Belvedere on May 24, according to Us Weekly. Despite being West's best friend, Jay Z allegedly decided to head to the Hamptons with Beyonce during the Memorial Day Weekend holiday.

The "Drunk in Love" singer posted a makeup-free Instagram picture of herself amid the wedding chaos, debuting her new hairstyle.

In attendance at Kimye's over-the-top wedding event were the Kardashian-Jenner clan, Chrissy Teigen and John Legend, Rachel Roy, Jonathan Cheban, Q-Tip, LaLa Anthony, Blac Chyna, Tyga, and Jaden Smith, among others.