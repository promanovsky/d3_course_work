As Marvel keeps hunting for its “Ant-Man” director, the company continues to move forward with its next phase of films, tapping Scott Derrickson to helm “Doctor Strange.”

Talks are still early, but sources say Derrickson, who most recently directed “Sinister,” is Marvel’s choice and a deal should be struck shortly.

Marvel had no comment.

The pic will be produced by Kevin Feige, who, over the years, has repeatedly said how badly he wants to get a “Doctor Strange” movie made. Executive producers on the project are Louis D’Esposito, Victoria Alonso, Alan Fine, Stan Lee and Stephen Broussard.

The comicbook revolved around a former neurosurgeon who serves as the “Sorcerer Supreme” — the primary protector of Earth against magical and mystical threats. Strange made his first appearance in the 1963 comicbook series “Strange Tales” and would go on to have his own comicbook series and would appear in several other comicbooks that included “The Fantastic Four,” “Spider-Man” and “Nick Fury.”

Marvel has teased Strange in several pics including “Thor” and “Captain America: The Winter Soldier,” and Derrickson’s commitment only moves things forward in getting the good doctor his own pic.

Derrickson and Marvel will now look to cast its title character, and while there is no front-runner at this time, expect a decision in coming months.

Derrickson has been a rising star that Hollywood has been chasing for their big projects after the success of his horror pic “The Exorcism of Emily Rose,” which earned $144 million at the worldwide box office.

He followed that up with another horror pic, “Sinister,” which overperformed at the box office and helped gained the attention of Hollywood. With execs always looking for directors that can pull the most out of their budgets, Derrickson is a very attractive talent, with both “Sinister” and “The Exorcism of Emily Rose” costing less than $20 million but bringing in more than double that in profits.

Marvel is hoping Derrickson can bring the same dark and eerie storytelling style to the “Dr. Strange” story while not breaking the bank on the budget.

He is repped by WME and Brillstein Entertainment.

His next pic, “Deliver Us From Evil,” bows July 2 and stars Eric Bana and Olivia Munn.