USD/JPY Daily Chart

For the 24 hours to 23:00 GMT, the USD weakened marginally against the JPY and closed at 101.50.



Late Monday, Japan’s Vice Economy Minister, Yasutoshi Nishimura hinted that the Japanese economy is on track as the markets have reacted as expected to the recent sale-tax hike in April. Furthermore, he affirmed that the BoJ stands prepared to take appropriate policy action should it see any downturn in the economy.



In the Asian session, at GMT0300, the pair is trading at 101.55, with the USD trading slightly higher from yesterday’s close.



Early morning, data from Japan showed that the all industry activity index in the nation rose 1.5% (MoM) in March, following a 1.1% drop in February and compared to market expectations for a rise of 1.6%.



The pair is expected to find support at 101.23, and a fall through could take it to the next support level of 100.91. The pair is expected to find its first resistance at 101.74, and a rise through could take it to the next resistance level of 101.93.



During the later course of the day, Japan’s Cabinet Office is scheduled to publish data on the nation’s leading economic and coincident index for March.



The currency pair is trading above its 20 Hr and 50 Hr moving averages.