Oscar Pistorius today embarked on the final stretch of his quest for absolution in the fatal shooting of girlfriend Reeva Steenkamp on Valentine's Day a year ago – and South Africans have returned to constant speculation as to what the judge’s verdict will finally be.

One of South Africa's biggest-ever celebrity murder trials resumed Monday at Pretoria's High Court after a 30-day respite designed to allow a psychiatric evaluation of Mr. Pistorius, one of South Africa's most celebrated athletes, at a nearby hospital.

Pistorius's evalution came after lawyers for the sprinter – known as "Blade Runner" – was suffering from "generalized anxiety disorder" when he shot four times at Ms. Steenkamp through a locked toilet door at his home in Pretoria, telling police when they arrived that he thought he was shooting an intruder.

Yet a panel of doctors from Weskoppies Psychiatric Hospital where Pistorius has spent much of the last month, said the famous Olympic sprinter was sane when he opened fire on the toilet door – a serious blow to defense hopes that his sentence might be reduced.

If convicted of premeditated murder, Pistorius, a double amputee whose exploits running on carbon fiber struts had inspired the nation, could face 25 years to life in prison.

In May the defense began to imply that Pistorius, who openly wept in court through much of the trial, as mentally impaired on the night of the shooting.

Merryll Vorster, a forensic psychiatrist from Wits University in Johannesburg, said the amputation of Pistorius’ legs as an infant, along with his family’s heightened fear of crime, his parents’ divorce, his mother’s death when he was 14 and other genetic factors combined to bring about a disorder in the athlete.

Dr. Vorster said these factors, coupled with Pistorius' physical disability as a double-amputee, partly explained his extreme reaction in shooting at the lavatory door.

Yet doctors in their report found Pistorius, "was capable of appreciating the wrongfulness of his act and acting in accordance with his appreciating of the wrongfulness of his act,” as prosecutor Gerrie Nel read to the judge today.

Had Pistorius been found incapable of thinking clearly when shooting at Steenkamp, the judge could reduce his sentence, should she find him guilty of murder or of culpable homicide, a lesser offense.

The defense today focused on proving the athletes original story, that he shot at the toilet door thinking an intruder was behind it on the fateful Valentine's Day of 2013.

His lawyers worked to discredit testimony and assertions brought by the prosecutors – mainly on neighbors that said they heard a woman screaming before they heard shots coming out of the Pistorius townhouse (testimony that would suggest Pistorius knew who he was shooting) and arguing that the sprinter could not have moved through the bedroom and pounded on the toilet door on his stumps without losing balance (as the prosecutor alleges).

Yesterday, the court heard from Gerald Versfeld, the orthopedic surgeon who amputated the athlete’s legs as a baby about the intense vulnerability he would have felt in confronting what he believed to be an intruder while walking on his stumps.

Dr. Versfeld said Pistorius would struggle to balance, particularly if he had something in his hands in the dark, and would often fall over without his prostheses on.

His evidence backed up the defense claim that Pistorius ran to put on his prostheses when he realized he’d shot Steenkamp before breaking down the door with a cricket bat. The prosecution said he acted while still on his stumps.

But in cross-examination Mr. Nel the prosecutor turned it around, asking how the athlete could manage to fire four shots and then run across the tiled bathroom floor to check his girlfriend’s whereabouts, all before he stopped to put on his legs.

The trial was originally billed to last three weeks but run more than three months; yet there's been little abatement in the fascination among South Africans for its every twist and turn.

Sources close to the defense team have promised a “powerful” last witness although the witness identity remains under wraps for now.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Should Judge Thokozile Masipa decide to convict Pistorius of premeditated murder, he faces 25 years in prison. Should she chose a verdict of culpable homicide rather than murder, he could escape any obligatory prison time.

Should, as his family still believe possible, Judge Masipa acquit him based on his disability and his intense paranoia about crime, he could walk out of court to resume his athletic career.