The International Monetary Fund has again upgraded its projection for Canada's economy, but the latest outlook from the international financial organization shows it is far from sold on the country's underlying fundamentals.

"Downside risks to the outlook still dominate, including from weaker-than-expected exports resulting from competitive challenges, lower commodity prices and a more abrupt unwinding of domestic imbalances," it warns.

"Indeed, despite the recent moderation in the housing market, elevated household leverage (debt) and house prices remain a key vulnerability."

The IMF predicts Canada's economy will expand by 2.3 per cent this year — one-tenth of a point more than it thought three months ago — and continue to pick up speed to 2.4 per cent in 2015.

Housing correction expected

Those numbers are a little softer than private sector forecasts — as well as south of the Bank of Canada's 2.5 projection for both years — but in line with the IMF's recent bearish assessment of Canada, which has included dire warnings about a housing correction.

And it now places Canada in third place behind the United States and the United Kingdom for growth among the Group of Seven industrialized countries in both 2014 and 2015.

The Washington-based institution, which acts an a monitor of global economies and lender of last resort for troubled countries, does acknowledge that Canada is positioned to benefit from the U.S. resurgence, which should boost Canadian exports and business investment.

That could lead to a situation where "external demand could surprise on the upside" and the economy outperforms projections.

But the IMF advises policy-makers against counting their chickens too soon. The rotation from domestic to external demand hasn't happened yet, so the Bank of Canada should keep interest rates low and Ottawa "needs to strike the right balance" between supporting growth and driving too hard to balance the budget, it says.

Spring meetings of IMF, World Bank

The salvation of Canada's economy, the IMF says, will be renewed demand from the United States, which should boost exports and drive business investment.

The new outlook comes days before global finance ministers and central bankers meet in Washington for the spring meetings of the IMF and World Bank, where lighting a fire under the dormant global economy will again be the main topic.

According to the IMF, 2014 will be a year when every region experiences growth, with the euro area advancing by a moderate 1.2 per cent after shrinking by 0.5 per cent in 2013.

But overall, the world remains a risk-filled environment with a tepid growth rate of 3.6 per cent this year and 3.9 per cent next year, one-tenth of a point lower than the IMF's January forecast.

Tepid growth around the world

And, in a break from recent years, the IMF says 2014 will see momentum for growth coming from advanced nations, particularly the U.S., where the expansion will see the economy advance 2.8 per cent in 2014 and 3.0 per cent in 2015. The U.K. also gets high marks with a 2.9 per cent and 2.5 per cent pace in the two years.

Meanwhile, "growth in emerging market economies is project to pick up only moderately," the IMF says. "These economies are adjusting to a more difficult external financial environment in which international investors are more sensitive to policy weakness and vulnerabilities."

And although the euro area has finally turned positive "growth in demand is expected to remain sluggish, given continued financial fragmentation, tight credit and a high corporate debt burden," the IMF said.