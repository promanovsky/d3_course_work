Hanoi - A Chinese boat rammed and sank a Vietnamese fishing vessel not far from where China has stationed a massive oil rig in disputed waters of the South China Sea, the head of Vietnam's coastguard said on Tuesday.

Vietnamese fishing boats operating nearby rescued the 10 fishermen on board following the incident on Monday, said coastguard commander Nguyen Quang Dam.

He said the ramming occurred 17 nautical miles from the rig, which has been deployed between the Paracel islands occupied by China and the Vietnamese coast.

“A Vietnamese boat from the central city of Da Nang was deliberately encircled by 40 fishing vessels from China before it was attacked by a Chinese ship,” Dam told Reuters by telephone.