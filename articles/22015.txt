Google pulled a Mt. Gox today and decided to withdraw itself from the internet... well at least for a few hours. No, Google is not going bankrupt, and yes, bitcoins are losing relevancy as fast as Russia is invading the Ukraine, but not having access to Gmail, Gchat and Google Hangouts is a serious problem yo.

Just how pervasive is this issue? According to Tech Crunch, the problem "appears to be fairly widespread." Messages like "[User] did not receive your chat" and "Things are taking longer than expected" are greeting users instead of the typical lighting fast application opening up.

This problem isn't effecting one-hundred percent of users, whether that be individuals, companies, schools or organizations, but it certainly is annoying. Yet, it's important to point out that the companies award-winning search engine wasn't reported affected.

You really have to applaud Google's response. Like always, the Mountain View, Calif.-based company didn't shy away from talking about their slip-up. A Google spokesperson stated that, "We're investigating reports of an issue with Google+ Hangouts." They went on to say that "we will provide more information shortly."

If your system is still being affected and you want to get real time updates on every Google service status update click here. As of this hour only two apps seem to be acting up. They are Google Talk and Google Sheets (which is part of Google Docs).

While most of the initial site wide disturbances have been addressed this incident will likely get some serious playtime in the media. When the New York Stock Exchange closes later today it's anyones guess as to if these incident's will effect Google's high-flying stock price.

And whatever the case going forward, it's apparent that Google was definitely not feeling lucky today.

Have you experienced any trouble with Google today? Share your frustrating experiences with us in the comments section below. And remember to "have an A-1 day."