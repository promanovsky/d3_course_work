(Reuters) - The S&P 500 closed at a record on Friday, boosted by a better-than-expected reading on the housing market, which gave a lift to homebuilding stocks. Hewlett-Packard's stock, which was the S&P 500's best performer, rallied a day after the personal-computer maker reported results and said it may cut up to 16,000 jobs.

Based on the latest available data, the Dow Jones industrial average .DJI rose 63.19 points or 0.38 percent, to end unofficially at 16,606.27. The S&P 500 .SPX gained 8.04 points or 0.42 percent, to finish unofficially at 1,900.53, a record high.

The Nasdaq Composite .IXIC added 31.47 points or 0.76 percent, to close unofficially at 4,185.81. For the week, the Dow rose 0.7 percent, while the S&P 500 gained 1.2 percent and the Nasdaq climbed 2.3 percent.

(Reporting by Caroline Valetkevitch; Editing by Jan Paschal)