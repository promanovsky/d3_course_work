THE board of Alstom backed a proposed tie-up with General Electric as the French government neared agreement with shareholder Bouygues over the last major outstanding plank of the deal.

An accord is taking shape on the price at which the government will acquire 20pc of Alstom from construction group Bouygues, adding that it would likely be finalised yesterday.

Bouygues, GE and Alstom all declined to comment on the ongoing stake sale talks as the French industrial group's board formally endorsed the plan with unanimous support.

The GE deal "not only addresses the interests of Alstom and of its stakeholders but also provides assurances in connection with concerns expressed by the French state", Alstom said.

President Francois Hollande earlier raised the pressure on Paris-based Bouygues, warning that failure to agree a price for the stake purchase could still scupper the tie-up.

"If this sale did not go ahead at a price acceptable to the government, it would be necessary to reconsider the alliance as it has just been announced," Mr Hollande told reporters in Paris.

On Friday, the government backed the proposed deal with GE. But the official green light remains subject to conditions agreed with GE – as well as the government's purchase of the stake from Bouygues. (Reuters)

Irish Independent