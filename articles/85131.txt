Despite a dull opening, Delsin quickly learns thrilling new superpowers and certainly enjoys using them a lot more than his dour predecessor, Cole McGrath. It's not long before players are causing chaos in a breathtaking downtown Seattle, in a fight against the oppressive Department of Unified Protection.

The combat against the enemy forces is exciting, as you can creatively utilise your powers to both attack and evade. Smoke powers enable Delsin to use vents to dive into the base of a building and hurtle out from the roof, taking shots at opponents as you glide down to a nearby rooftop.

There's terrific freedom to tackle situations differently and many opportunities to use Delsin's frenetic speed and climbing abilities. Unleashing the devastating "Karma Bombs" is also a real buzz.

The story touches on interesting issues of the fear of the unknown and whether security is worth the cost of freedom, yet is underdeveloped.

Moral choices offer simplistic binary decisions between the altruistic hero or powerful villain, but don't impact the action or story considerably. There's not much in the way of interesting diversions, although it's mercifully free of unwanted padding.