Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Robert Pattinson is living in ex-girlfriend Kristen Stewart's LA home after admitting he is now homeless.

The hunk - who split from former Twilight co-star Kristen in 2012 - is said to be back in touch with the brunette beauty after he was booted out of his own digs.

But the pair are not dating again - with Robert using his ex-flame's LA pad while she is away so he can spend time with their dogs, Bear and Bernie.

The pair split following Kristen's rumoured affair with her 'Snow White and the Huntsman' director Rupert Sanders .

A source told HollywoodLife.com: "Rob doesn't have his own place in LA. When he's in town, he sometimes stays at the Chateau Marmont and very often at Kristen's when she's out of town. This gives him the chance to hang out with Bear and Bernie.

"Rob and Kristen aren't romantic or anything, but they are still really close and would do anything for each other."

Rob revealed he doesn't have anywhere to live after his parents moved into his home in Los Angeles while he was away filming in Toronto.

(Image: Summit Entertainment)

He explained: "I've kind of found myself a little bit more at home in LA.

"My parents borrowed my house, which I was borrowing off someone else and then they kicked me out of it when I came back from Toronto.

"So that's the end of that, so I'm now homeless again."

The 28-year-old actor previously claimed he slept behind an archway following his split from Kristen and says he doesn't know where his possessions are currently kept.

When asked where he stores his bits and bobs when he doesn't have a home, Robert replied: "I'm not entirely sure. It's somewhere. I wish I knew."