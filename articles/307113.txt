At their latest Uncarrier event, T-Mobile CEO, John Legere has announced T-Mobile Rhapsody Unradio, which the company describes as a new Internet Radio with an Uncarrier spin.

The new T-Mobile Rhapsody Unradio service will be free to the company’s Simple Choice customers on its 4G LTE plans, it will also be available to other T-Mobile customers for $4 a month.

You can see a list of the T-Mobile Rhapsody Unradio service below.

• Ad-free listening: While traditional Internet radio interrupts you with ads, with unRadio your music streams ad-free.

• Unlimited skips: While traditional Internet radio limits you to six skips per hour, unRadio lets you skip as much as you like.

• Choose the music you want: While traditional Internet radio chooses your music for you, unRadio lets you choose your music. When you hear a song you love on unRadio, you can mark it as a favorite and automatically save it for later listening. You can stream these songs on-demand or download them to enjoy anywhere — even without a connection.

• Create your own stations (or listen to ours): Listen to hundreds of professionally programmed stations, or create your own stations based on the songs or artists you love.

• Live streaming radio from your hometown or around the world: unRadio offers live streaming radio from thousands of terrestrial stations in the U.S., including KCRW in Los Angeles, KEXP in Seattle, and Chicago’s WXRT, among others, and from thousands of stations around the world.

• ID songs anywhere with TrackMatch: Rhapsody unRadio includes a new feature, TrackMatch, you can use to identify songs you hear while out at a bar, ballgame, party or even on TV, and create stations around these songs or save them as favorites for later listening.

This new radio service will launch on T-Mobile next Monday the 23rd of June, you can find out more information over at T-Mobile at the link below. The service will be available on Android, iOS and the Internet at launch.

Source T-Mobile

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more