Despite stating he had no interest in any role in Star Wars Episode VII, Simon Pegg has apparently filmed a not-so-secret-any-more scene in the new J.J Abrams movie.

Simon Pegg filming scenes for the movie 'Absolutely Anything'

According to a source talking to IGN’s Middle East arm, Pegg was present in Abu Dhabi for the desert portion of Star Wars’ filming dates. This directly contravenes what the Cornetto trilogy actor told The Independent.

More: Simon Pegg Invites Fans To Shaun Of The Dead Zombie Walk

"I don't think it would be appropriate for me to be in it, to be honest. I think J.J. should cast new faces with no stunt casting. I wouldn't want to be popped out of the film by a knowing cameo. I think it would be great to do it properly," he said.

Bleeding Cool sat down with Pegg yesterday and, according to Brendon Connelly, “He didn’t show obvious signs of an Abu Dhabi tan.” So basically, it’s all on that source. We’ll probably know either way by Monday, so sit tight.

More: Empire Film Awards: James McAvoy, Emma Thompson, 'Gravity,' And 'The Hobbit' Take Home The Silver

In other galactic news, George Osborne – the British chancellor – announced that the upcoming Star Wars spin off will be filmed in the U.K, and was very proud of that fact, too.

Visiting the set of Episode VII, Osborne said the decision to film another StarWars in Britain is a "testament to the incredible talent" at Pinewood. "This will mean more jobs and more investment. It is great news for people working at Pinewood Studios, from the set designers to the carpenters," he said.