While trading activity was somewhat subdued, stocks moved mostly higher over the course of the trading day on Monday. The gains on the day extended the upward move seen late last Friday, further offsetting the sharp pullback seen in the middle of last week.

The major averages all closed higher, although the tech-heavy Nasdaq outperformed its counterparts. The Nasdaq jumped 35.23 points or 0.9 percent to 4,125.81, while the Dow edged up 20.55 points or 0.1 percent to 16,511.86 and the S&P 500 rose 7.22 points or 0.4 percent to 1,885.08.

The strength that emerged on Wall Street was partly due to a rally by tech stocks, as reflected by the outperformance by the Nasdaq.

Networking stocks saw substantial strength on the day, driving the NYSE Arca Networking Index up by 2.1 percent. With the gain, the index reached its best closing level in almost a month.

Infinera (INFN) helped to lead the networking sector higher, with the provider of optical transport networking equipment, software, and services jumping by 12.5 percent.

Significant strength was also visible among biotechnology stocks, as reflected by the 2 percent gain posted by the NYSE Arca Biotechnology Index. InterMune (ITMN) posted a standout gain following some positive analyst comments.

Internet, computer hardware, and semiconductor stocks also saw notable strength, contributing to the strong upward move by the Nasdaq.

Outside of the tech sector, airline stocks posted particularly strong gains, resulting in a 2.2 percent jump by the NYSE Arca Airline Index. The index regained some ground after closing lower for four consecutive sessions.

Brokerage, trucking, and banking stocks also saw some strength on the day, while weakness was visible among utilities stocks.

Nonetheless, trading activity remained light throughout the session, as trader seemed reluctant to make significant moves amid a lack of major U.S. economic data.

The economic calendar remains light throughout much of the week, although reports on new and existing home sales and weekly jobless claims are likely to attract some attention on Thursday and Friday.

Traders are also likely to keep an eye on the release of the minutes of the latest Federal Reserve meeting on Wednesday, looking for indications regarding the outlook for monetary policy.

Among individual stocks, a notable drop by shares of AT&T (T) limited the upside for the Dow, with the telecom giant falling by 1 percent after announcing an agreement to acquire DirecTV (DTV) for $48.5 billion in cash and stock.

Campbell Soup (CPB) also came under pressure after the soup maker lowered its outlook for full-year revenue growth.

On the other hand, shares of Pfizer (PFE) moved notably higher on the day following news that AstraZeneca (AZN) has rejected the drug giant's latest takeover offer.

In overseas trading, stock across the Asia-Pacific region moved mostly lower during trading on Monday. Japan's Nikkei 225 Index dropped by 0.6 percent, while China's Shanghai Composite Index tumbled by 1.1 percent.

Meanwhile, the major European markets turned in a mixed performance on the day. While the U.K.'s FTSE 100 Index edged down by 0.2 percent, the French CAC 40 Index and the German DAX Index both rose by 0.3 percent.

In the bond market, treasuries turned lower over the course of the session after seeing early strength. As a result, the yield on the benchmark ten-year note, which moves opposite of its price, rose by 1.8 basis points to 2.536 percent.

For comments and feedback contact: editorial@rttnews.com

Business News