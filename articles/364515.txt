MADRID - A passenger jetliner preparing to leave Barcelona's El Prat airport taxied across a runway where another was about to land, forcing the arriving plane to abort its landing and climb sharply to avoid a possible disaster.

Amateur video footage filmed Saturday showed the Aerolineas Argentinas Airbus 340 crossing the runway just as the aircraft from Russian airline Utair was making its final approach.

Spanish airport authority AENA said Monday the Russian plane, which officials described only as a Boeing, circled back and landed safely. None of the passengers on either plane was hurt.

AENA said it has opened an investigation into what happened. It had no details on how close the planes were at the time of the incident.

While unrelated to the Barcelona incident, the U.S. has seen several runway incursions of its own in recent months.

On April 24, a preliminary National Transportation Safety Board report showed that two planes at Newark Liberty Airport missed each other by just hundreds of feet. Passenger jets are supposed to be a minimum distance of two miles apart.

It was just after 3 p.m. when United Airlines Flight 1243 from San Francisco was making its final approach with 161 people onboard.

But just as the Boeing 737 jet prepared to touch down at the airport, an ExpressJet flight bound for Memphis, Tennessee, with 50 people onboard, was cleared to take off on an intersecting runway.

Realizing the jets were getting too close, air traffic control ordered the United Flight to abort landing.

On May 9, two United Airlines aircraft just barely avoided each other preparing for take off at Houston's George Bush Intercontinental Airport.

Air traffic control instructed the pilot of United Flight 601 to take off; at the same time, another flight, United 437, departed for Mexico City on another runway. The Vancouver-bound flight was also told by air traffic control to turn right, putting the aircraft directly in the path of flight 437.

The planes were less than a mile apart, a distance which is only a second or two from impact in the air.

"United 601 stop your turn, stop your- let's just stop your climb and stop your turn, United 601," the air traffic controller said.

Roughly 150 passengers were on board each aircraft. All were unaware of the incident and landed safely.

"You all basically crossed directly over the top of each other," another air traffic controller said.