These days, Apple (NASDAQ:AAPL) launches new iPhone models in September. The supply chain leaks surrounding the purported duo of devices continues to escalate. Investors have already taken to calling the device the "iPhone 6." Here's the thing: Apple shouldn't launch an "iPhone 6."

An iPhone by any other name would be just as fast

To be clear, Apple absolutely should and will launch the 4.7-inch and 5.5-inch models that have been widely leaked. Rather, it should transition away from using numerical branding for its most popular (and financially significant) product. The "iPhone 6" moniker simply doesn't make sense. Apple is a company that values simplicity above all else, and it needs to simplify its product naming conventions.

This year's iPhone will be the eighth-generation model, so "iPhone 6" isn't quite accurate. That didn't stop Apple from using "iPhone 5" two years ago either, though.

Year Generation Name 2007 1st iPhone 2008 2nd iPhone 3G 2009 3rd iPhone 3GS 2010 4th iPhone 4 2011 5th iPhone 4S 2012 6th iPhone 5 2013 7th iPhone 5s/iPhone 5c 2014 8th ???

The current state of iPhone branding is partially due to the tick-tock model that Apple adopted from Intel, and the "S" it appends every other year when Apple focuses on incremental internal improvements. Longtime Apple watchers may remember that when Steve Jobs introduced the iPhone 3GS in 2009, the "S" stood for "speed." Apple has stuck with this pattern for three tick-tock cycles, but it's quickly proving unsustainable in the long run. At this rate, in four years the 12th-generation iPhone would be called the "iPhone 8."

Instead, Apple should simplify its branding under the "iPhone Air" name that's also made the rounds in the rumor mill. The latest speculation is that only the 5.5-inch model will carry the "iPhone Air" name, while the 4.7-inch model will be called the iPhone 6. Moving away from numerical names is much more sustainable in the long run.

Apple has done it before

Apple seems to have acknowledged this with the iPad family, which has a rather inconsistent naming history.

Year Generation Name 2010 1st iPad 2011 2nd iPad 2 2012 3rd The New iPad 2012 4th iPad With Retina Display 2013 5th iPad Air 2014 6th ???

The good news for the iPad is that Apple has mostly gotten away from relying on numerical branding, which is simpler for the consumer and a better long-term marketing strategy. Still, that hasn't stopped people from referring to the upcoming model as the "iPad Air 2."

Every other product in Apple's lineup eschews numerical branding, from the iPod Touch to all Macs; there is no "iMac 12" or "MacBook Pro 5." These product families merely use generations or years to reference their models, similar to the auto industry. This is actually quite an achievement in the realm of consumer electronics, where many companies still use needlessly complex model numbers such as Hewlett-Packard's HP ENVY 15z-j100 (not to be confused with the HP ENVY 15-k020us).

Apple should do what it does best: simplify.