'Star Wars: Episode VII' will begin filming in the UK in May.

The latest installment in the space opera franchise will be shot at the Pinewood Studios near London in two months time, and will be set 30 years after 1983′s 'Return of the Jedi', producers have confirmed.

A press release issued by Disney and Lucasfilm said: ''[It] will star a trio of new young leads along with some very familiar faces.''

Adam Driver is in talks to play the villain in the film, which will be directed by J.J. Abrams, but others battling it out for lead roles include, Ed Speleers, John Boyega, and Jesse Plemons.

It is thought Lupita Nyong'o has also met with famed director Abrams to discuss taking on a female lead role in the upcoming sequel after she took Hollywood by storm last year following her impressive performance in Steve McQueen's '12 Years A Slave'.

Meanwhile, it is believed the stars of the original trilogy, Mark Hamill, Harrison Ford and Carrie Fisher, will also appear in 'Episode VII'.

The highly-anticipated sci-fi adventure was originally thought to centre on Han Solo and Princess Leia's kids, however, Abrams and Lawrence Kasdan's recent rewrite of Michael Arndt's script has allegedly altered the focus.

'Star Wars: Episode VII' is set for release on December 18, 2015.