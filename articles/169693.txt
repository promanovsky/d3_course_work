Beyonce has confirmed rumors that she and Jay Z will be touring together this summer.

The news appeared on her website, where details of the couple's On The Run tour appeared alongside a promotional image of the two standing close together wearing ski masks. The tour kicks off in Miami on June 25 and includes shows in 16 cities (including Cincinnati, Philadelphia, Baltimore, Toronto, Atlanta, Houston, New Orleans, Chicago, and Los Angeles) before warping up in San Francisco on August 5. More tour dates may be revealed later in the summer.

Pre-sale tickets for the tour will go on sale on April 29.

The husband and wife team have routinely appeared in each other's shows in the past. Beyonce finished her Mrs. Carter Show tour a month ago, while Jay Z finished his 52-date Magna Carta… Holy Grail tour in January.

For comments and feedback contact: editorial@rttnews.com

Entertainment News