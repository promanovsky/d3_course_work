Today, Universal Orlando announced that The Wizarding World of Harry Potter – Diagon Alley will officially open on July 8 – allowing guests to experience even more of Harry Potter’s adventures in an all-new, magnificently-themed environment. Located in the Universal Studios Florida theme park, The Wizarding World of Harry Potter - Diagon Alley will feature shops, dining experiences and the next generation thrill ride, Harry Potter and the Escape from Gringotts. The new immersive area will double the size of the sweeping land already found at Universal’s Islands of Adventure, expanding the spectacularly themed environment across both Universal theme parks – and guests can journey between both lands aboard the Hogwarts Express. For additional information, visit www.UniversalOrlando.com/WizardingWorld.

ORLANDO (CBSMiami) – Muggles, witches and wizards alike got their first official chance to visit The Wizarding World of Harry Potter-Diagon Alley at Universal Studios Florida.

Fans of the boy wizard lined up before dawn to be the first in the park when it opened at 8 a.m. While the line of fans didn’t match that of the opening of the Wizarding World of Harry Potter – Hogsmeade in 2010, it did stretch around the lagoon and through the park.

Diagon Alley is filled with the London side shops that Harry, Ron and Hermione visited in the books and movies. It’s marque ride is Harry Potter and the Escape from Gringotts. At 1:15 p.m., Universal posted a seven-and-a-half hour wait for the Gringotts ride, according to themeparkinsider.com, which added that it appeared to be going up and down frequently, so capacity was minimal.

The attraction doubles the size of the Harry Potter footprint on Universal’s property. The Hogwarts Express connects the new area to the Hogsmeade area in Universal’s Islands of Adventure.

If fans want to see both Harry Potter areas, a two-park ticket is required. For adults, a two-park ticket for one day costs $136; for children, it’s $130.

The level of detail in Diagon Alley is amazing, and nowhere are the features more fun and true to the novels and movies than in shop windows. There are lots of things for sale, but others are simply great decorations. Books play a central role in the decor; window-shoppers will see animatronic versions of “The Monster Book of Monsters,” a tome with teeth, along with self-knitting needles and Harry Potter’s snowy owl, Hedwig.

With seven shops in Diagon Alley, there’s plenty to buy. Among the offerings: Weasleys’ Wizard Wheezes where you can pick up everything from chattering teeth to expandable ears, Madam Malkin’s Robes for All Occasions ($250 for a wizard’s robe), Quality Quidditch Supplies (everything for playing the real-life sport based on the game in the series) and Scribbulus (implements for paper, pen and ink lovers). Magical Menagerie sells cute stuffed animals – pygmy puffs are popular.

There’s also the darkside. Borgin and Burkes located off Diagon Alley on Knockturn Alley is a “dark arts” shop that sells skulls, black T-shirts and Death Eater masks. Wiseacre’s Wizarding Equipment sells telescopes and binoculars.

For $35, guests can buy interactive wands at Ollivanders Wand Shop, located in both Diagon Alley and Hogsmeade. When wand-wielding guests see a medallion symbol in either area, they can wave the wand and cast spells. The wands make trolls dance, light lamps and chandeliers, and silence some shrunken heads, among other things. Maps of the medallion areas are available, but Universal says there are also some hidden spell areas for guests to discover.

As gueats enter Diagon Alley and look toward toward Gringotts Bank the one thing that will catch their attention is the fire breathing dragon perched on it’s roof. Once you enter the bank, while in queue for the ride, don’t miss the three enormous glass chandeliers.

(TM and © Copyright 2014 CBS Radio Inc. and its relevant subsidiaries. CBS RADIO and EYE Logo TM and Copyright 2014 CBS Broadcasting Inc. Used under license. All Rights Reserved. This material may not be published, broadcast, rewritten, or redistributed. The Associated Press contributed to this report.)

RELATED CONTENT:

[display-posts category=”entertainment” wrapper=”ul” posts_per_page=”5″]