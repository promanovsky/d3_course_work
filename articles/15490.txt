A new study reveals that autism, a neurodevelopment disorder, may be the direct result of genetics, environmental factor or both, according to Fox News.

The PLOS Computational Biology journal published a report by the University of Chicago researchers who reviewed more than 100 million medical records of U.S. citizens and found that, at the county level, intellectual disability and autism correlate with genital malformation incidences in newborn males.

Based on the study's finding, researchers believe that toxins in the environment are to blame during congenital development in the mother's womb.

Andrey Rzhetsky, a genetic medicine and human genetics professor at the University of Chicago, told Fox News that small molecules such as plasticizers, prescription drugs, and pesticides could find its way to a vulnerable fetus.

"Some of these small molecules essentially alter normal development," Rzhetsky said. "It's not really well know why, but it's an experimental observation, especially in boys and especially in the reproductive system."

Rzhetsky along with his research team examined insurance claims and compared autism rates in 3,100 counties across the U.S. They also researched cases of congenital malformations such as micropenis, hypospadias, undescended testicles and more.

The researchers also found that for every 1 percent increase in frequency of congenital malformations there was a 283 percent increase in autism rate, while intellectual disabilities rose 94 percent for every 1 percent increase, according to the report.

"Malformations predict very strongly the rates of autism, and the rate of malformation per person varies scientifically across the country," he said.

The study showed that, while there is also a correlation between females with autism and malformation rates, the association is more prevalent in males as boys are six times more likely to be born with congenital malformations.

Rzhetsky added, however, that while it's not definitive that environmental factors are the main cause, it is the strongest case thus far as his team performed numerous observations and tests with various control groups, and found environmental toxins to be a substantial indicator.

"Yes, the population is heterogeneous and some counties for whatever reason have subpopulations that are genetically more 'loaded,' but it seems less likely than an explanation that environment differs from county to county ... it would be strange to see those differences if (autism was only caused by) genetics, and it was several fold differences in rate from county to county."