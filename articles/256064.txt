Starbucks will open a restaurant in San Francisco next month, according to the San Francisco Business Times.

The restaurant will be called La Boulange, named for the California-based bakery chain the coffee giant bought a couple years back. The restaurant will stay open until 10, and will feature heavier dishes—main attraction: the croissant burger—as well as beers, cocktails, and wine, during dinner time. Much of its daytime fare will draw from La Boulange’s existing menu, Businessweek reports.

The restaurant figures to serve as something of a test kitchen as Starbucks looks to at least gain some footing in the evening dining game with its existing cafes. From Businessweek:

It's easy to see why Starbucks wants to enter the dinner market. After sundown, the cafe chain largely loses to competitors such as Panera, which racks up 22 percent of its sales at dinner. Starbucks has already announced an "Evenings" menu at its cafes–and will no doubt try to apply any lessons to its new restaurant.

Advertisement

The Business Times reports the restaurant could be the first of many, so thoughts of a croissant burger in Boston might not be too far-fetched.