If Iraq's oil supply goes offline, crude prices could hit $150-$200 a barrel, T. Boone Pickens, founder of BP Capital Management, told CNBC on Friday.

"That's where you have to kill demand with price. That's the only way you can do it, because oil won't be there," Pickens said in an interview with "Street Signs."

Read MoreOil demand to jump as Iraq pushes prices higher



Crude hit a nine-month high Friday as fears intensified over the conflict in Iraq and its potential to disrupt country's the oil supply. U.S. crude—West Texas Intermediate (WTI)—hit a session high of $107.68 a barrel early Friday. Brent rose to an intraday high of $114.69.



President Barack Obama announced Friday he would not be deploying any ground troops in Iraq, and stressed that any other action would take several more days of planning. Insurgents have seized cities in northern Iraq and are only about 40 miles from Baghdad.