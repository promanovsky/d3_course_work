As a young book editor at Little, Brown & Co. in 1992, Michael Pietsch paid $80,000 - $45,000 more than the next-highest bidder - for a postmodern novel by a little-known writer named David Foster Wallace.

He spent years urging Wallace to cut hundreds of pages from the sprawling manuscript and impose at least some structure on the disparate plot strands. The book, "Infinite Jest," was finally published in 1996 and became an instant literary sensation.

Pietsch is now chief executive of Little, Brown's parent company, Hachette Book Group, and is engaged in a very different sort of battle - not with a fragile author, but with one of the most powerful corporations in the United States: Amazon.

As the first chief executive of a major publishing house to negotiate new terms with Amazon since the Justice Department sued five publishers in 2012 for conspiring to raise e-book prices, Pietsch finds himself fighting not just for the future of Hachette, but for that of every publisher who works with Amazon.

"In a sense, Michael Pietsch is like 'Horatius at the Bridge,'" says the literary agent and former Amazon executive Laurence J. Kirshbaum, referring to the soldier of legend who single-handedly saved ancient Rome by fighting off an invading army. "He is carrying the rest of the industry on his back."

Because Hachette and Amazon have signed confidentiality agreements as part of their negotiations, the particulars of their dispute have been kept secret. But inside the publishing world, the consensus is that Amazon wants to offer deep discounts on Hachette's electronic books, and that the negotiations are not going well.

The proudly customer-friendly Amazon is delaying shipments and preventing pre-orders of certain Hachette books, suggesting to potentially frustrated shoppers that they buy them elsewhere. Pietsch, ordinarily easygoing and accessible, is refusing to talk to the news media and has told employees to do the same.

There is little question that Pietsch would not be squaring off against the country's largest bookseller if it were not an absolute necessity for his company's bottom line. Friends say he never wanted the negotiations to become public. But now that they have, everyone in the book industry is watching and waiting.

"We're all Hachette now," one small publisher joked last week at the trade fair BookExpo America in New York.

Given his background, Pietsch, 56, is an unlikely figure to find himself in such a position. He is trained as an editor, not a businessman. He took over Hachette in April 2013, trading a life of poring over manuscripts for one of scrutinizing spreadsheets.

Like a player-coach, though, he has continued to acquire and edit a small handful of books, most notably Donna Tartt's Pulitzer Prize-winning novel, "The Goldfinch." He has built an all-star stable of authors that includes highbrow, literary writers as well as mass-market giants like Michael Connelly and James Patterson.

It is unusual for a lifelong editor to become CEO of his own publishing company, but over the years, Pietsch had developed a reputation as both a man of letters and a shrewd dealmaker. The combination could serve him well in his dispute with Amazon.

"While I don't envy his position in this street fight, I think he's exactly the right guy to be conducting it," said Sloan Harris, an agent at International Creative Management.

The son of an Army officer, Pietsch grew up in Norfolk, Virginia. He attended Harvard, where he concentrated in English and wrote his senior thesis on Chaucer's 14th-century work "The Canterbury Tales." During his senior year, Pietsch interned at a publisher based in Boston, David R. Godine, and liked it so much that he stayed on after graduating.

He eventually moved to New York and spent six years at Scribner - among other things, editing an unpublished Ernest Hemingway manuscript - before joining Little, Brown as an editor in 1991. Over the next 10 years, Pietsch helped transform the company from a sleepy, largely literary house, to a modern, commercial publisher.

Pietsch worked his way up through the ranks there, becoming editor-in-chief in 1998 and publisher in 2001. Even as a publisher, he was known for being hands-on. After Wallace committed suicide in 2008, Pietsch painstakingly assembled the thousands of typed and handwritten manuscript pages Wallace had left behind into a posthumously published novel. He also wrote the introduction to that book, "The Pale King."

A resident of Sleepy Hollow, New York, Pietsch is married to Janet Vultee Pietsch, a children's book editor. As Hachette's chief executive, he now oversees not just Little, Brown but all of the publisher's imprints, which together put out about 1,000 books every year.

Hachette has been largely silent since the dispute broke out into the public last month, though it did issue a public statement - written by Pietsch - that underscored its view that books deserve to be treated differently from hard drives, diapers and the countless other products Amazon sells.

"Amazon indicates that it considers books to be like any other consumer good," the statement said. "They are not."

Last week, Pietsch received some support from one of his own authors, Patterson, who went on a brief tirade against Amazon during a luncheon speech at BookExpo. "Amazon also, as you know, wants to control bookselling, book buying, and even book publishing, and that is a national tragedy," he said.

Pietsch's central role in his industry's dispute with Amazon seems to be nothing more than sheer happenstance. As part of Hachette's antitrust settlement with the government, the company agreed to allow Amazon to continue to discount the price of e-books for two years. That agreement has expired, and for some reason - no one is exactly sure why - Hachette is the first publisher to find itself in the position of negotiating a new one.

(Also see: Regulators Unlikely to Intervene in Amazon-Hachette Dispute: Experts)



Other publishers are holding their breath as it does. It is in their interests for Pietsch to drive a hard bargain, and they are cheering him on, but silently. They have their own relationships with Amazon to protect, and they do not want anything they say to be construed as antagonistic, all the more so now that Amazon has demonstrated its willingness to punish booksellers when negotiations become contentious.

If Pietsch is a hero to some, he is a reluctant one. "He doesn't want to be seen as the warrior against Amazon," said Wallace's agent, Bonnie Nadell, who has known Pietsch for 25 years. "I think that makes him incredibly uncomfortable."

© 2014 New York Times News Service

