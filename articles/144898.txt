NEW YORK (TheStreet) -- Halliburton swung to a profit and reported better-than-expected first-quarter results on Monday, while also forecasting a 25% jump in earnings for the current quarter.

VIDEO TRANSCRIPT:

Shares of Halliburton (HAL) - Get Report were climbing on Monday after the oilfield services company reported better-than-expected first-quarter results and a forecast a 25% jump in earnings in the current quarter.

Halliburton reported net income of $622 million, or 73 cents a share, for the quarter, a swing from a year-earlier loss of $18 million, or 2 cents a share. Analysts expected profit of 71 cents, according to Thomson Reuters.

The company also reported improved revenue, especially from the Eastern Hemisphere, where revenue grew 11%, helping to offset weakness in North America where drilling has been on the decline.

WATCH:More videos from Brittany Umar on TheStreet TV

The results are a big swing from the same period a year ago, when the company was hampered by a charge related to the 2010 Deepwater Horizon oil spill. Halliburton had been a contractor for BP (BP) - Get Report, which owned the well that caused the disaster.

At last check, shares of Halliburton were climbing about 3.4% to $62.98.

In New York, I'm Brittany Umar for TheStreet.

Written by Brittany Umar in New York.