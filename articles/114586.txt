Photo: Reuters Nicki Minaj stunned viewers at the MTV Movie Awards Sunday with her natural look. The 30-year-old rapper, known for her over-the-top hair and makeup, toned it down drastically for the big night.

Wearing a floor-length black dress that showed off her curves, and gold accessories, Minaj stole the show while wearing minimal makeup and showing off her natural hair.

While Minaj often flaunts her body, she rarely shows off her real hair, usually covering it with weaves or wigs. She's one of many black celebrities who have promoted and glorified fake hair to create a certain standard of beauty. Her 2011 single "Did It On 'Em" got many black women were in uproar, as Minaj rapped, "These nappy-headed hoes need a perminator," criticizing black women's natural locks.

But lately, she has been embracing her natural hair and showing it off to fans -- and they seem to be impressed. “Her hair is sooo [expletive] pretty. I love the diversity of the wigs tho [sic]!” one fan said.

Minaj also showed off her natural beauty by going nearly makeup-free. In Elle magazine last year, she spoke about going through a make-under for the shoot. While she used to be afraid of wearing less makeup, she seems to be getting more used to it. “When I saw myself with barely any makeup at all, it was such a … like, I’m so, so attached to my pink lipstick, it’s hard. I feel that it’s become a part of me. To go in front of the camera, without pink lips or big ol’ crazy lashes -- you know, nothing -- I felt naked. It was scary! So this photo shoot was a real accomplishment [for me].”

Minaj is promoting her first film, “The Other Woman,” which will be released April 25.