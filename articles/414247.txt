A set of leaked photos showing what could be Apple Inc.’s (NASDAQ:AAPL) upcoming iPhone 6 alongside an iPhone 5 appeared on Sunday, fueling speculation about the next-generation iPhone ahead of its expected unveiling on Sept. 9. Although the legitimacy of the photos cannot be confirmed, their depictions match up well with iPhone 6-related leaks over the past few months.

In the latest photos, provided by Gizmobic, the iPhone 5 has been compared from every possible angle to what looks like a 4.7-inch iPhone 6 model in space gray color. The photos provide a clear look at how much bigger the smaller iteration of the iPhone 6 could be than the iPhone 5 or iPhone 5s.

The iPhone 6 has recently been rumored to feature an iPod touch-style protruding lens that will extend from the chassis. A new photo of the device hints at the presence of a protruding camera as the iPhone 6 rests slantingly on the table.

Photo: Yaya888 via Gizmobic

The photos also suggest that the power button will be removed from the top and will be repositioned on the right side of the device, which also has a thinner SIM tray. On the left side of the device, there are longer and sleeker volume buttons, along with a much thinner toggle switch.

Photo: Yaya888 via Gizmobic

Although the new iPhone 6 is expected to be bigger, the images show it to be sleeker and thinner than the iPhone 5. And, if the photos prove accurate, it means the bottom of the device will have only one speaker strip and one mike, apart from the audio I/O jack.

Photo: Yaya888 via Gizmobic

Boosting The Resolution

Last week, well-connected Apple blogger John Gruber of Daring Fireball suggested that both versions of the iPhone 6 could get significant improvements in their display resolutions.

According to Gruber, the 4.7-inch version of the iPhone 6 could feature a resolution of 1334x750 with 326 pixels per inch, or ppi, while the 5.5-inch version could have a resolution of 2208x1242 with 461 ppi. In comparison, the current iPhone 5s and iPhone 5c feature a resolution of 1136x640 pixels with 326 ppi.

Rumors are rife that the iPhone 6 is expected to be unveiled on Sept. 9 at a media event. Apple is expected to make the 4.7-inch version available for sale about a week after the announcement, while the 5.5-inch version could go on sale by the end of this year or early next year.

The iPhone 6 is likely to be powered by a faster A8 processor, and feature an improved camera, a more secure Touch ID Fingerprint scanner, iOS 8, Near Field Communication and a stronger battery.