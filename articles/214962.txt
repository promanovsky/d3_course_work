A few tears were shed during Cristina Yang’s last episode on “Grey’s Anatomy’’ Thursday night, but the season finale was surprisingly anticlimactic.

True to “Grey’s’’ finale fashion, there was an explosion at a mall (past seasons have included plane crashes, crazed gunmen, etc.), but no integral characters were hurt (at least physically), and Cristina’s exit was drama-free. Maybe it had to do with the fact that I knew her departure was imminent, but I was expecting more.

Outside of Owen’s pained expression when he realized that Cristina was leaving for good (she waved goodbye at him from an observatory while he was operating), the main characters were particularly emotionless as she walked out of Grey Sloan Memorial Hospital.

Advertisement

After spending what seemed like an eternity pushing Cristina out the door, Meredith teared up as she shoved her in a cab, but this wasn’t even their final goodbye. Cristina turned the cab around and walked back into the hospital to dance it out with her “person’’ and give Mer some orders before her she boarded a plane for Switzerland. But… that’s it?

I’ll give it to the writers. I was on the edge of my seat for the first few minutes after the mall explosion (which was a gas main and not a terrorist attack) when Owen frantically dialed Cristina’s cell (she told him she was thinking of going to the mall that morning), and it went straight to voicemail. But that suspense only lasted for 10 minutes. Cristina showed up to help save the day, aiding Alex in cracking a kid’s chest on the sidewalk outside of the hospital.

The only real exciting moment was when Dr. Webber sat down next to the new director of cardio, Dr. Pierce, who spilled her guts about wanting to be accepted. She gave him her life story at the end of the episode, telling him that she was adopted and had applied for a position at GSMH because her birth mother, who is now deceased, was a doctor there and her name was all over it. Webber paused, took a breath, and asked if she knew who her mother was.

Advertisement

Gasp.

It was Ellis Grey, Webber’s former lover and Meredith’s mother.

We were left looking at a perplexed Webber and loving the fact that Meredith has a new sister to learn all about (we still miss you, Lexie).

Th Dr. Pierce/Dr. Webber/Dr. Grey a-ha moment tied in perfectly to Meredith and McDreamy’s battle over their family’s move to Washington, D.C., which helped wrap up the season. Meredith was ready to sign the papers on a townhouse and make the move to the opposite coast, but a pep talk from Cristina (“Don’t let what he wants eclipse what you need. He’s very dreamy, but he’s not the sun. You are.’’) gave her the ammo to say no.

Meredith used her family ties to Seattle as leverage and mentioned her sister, and although we know she was referencing Lexie, Shonda did a good job of tying in the new Dr. Pierce storyline before we said goodbye to season 10.

The end of the Cristina generation also meant the end of Dr. Shane Ross, who at the last minute asked — er, begged — Cristina to take him with her to Switzerland. Webber objected (“You’re in the middle of your residency!’’), but Cristina said OK and that was that. Dr. Leah Murphy returned to help with the chaos after having been fired by Webber last week, but her final walk out of the hospital after the explosion rush was definitely her last.

Looking back, it was a lot easier to go to bed after watching the episode which was rid of doctor deaths, psychological mind games, and major cliffhangers, but who needs sleep anyway?