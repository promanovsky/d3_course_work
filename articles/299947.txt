NEW YORK - A 1-cent postage stamp from a 19th century British colony in South America has become the world's most valuable stamp - again.

The 1856 British Guiana One-Cent Magenta sold Tuesday at auction in New York for $9.5 million, Sotheby's said. It was the fourth time the stamp has broken the auction record for a single stamp in its long history.

The stamp was expected to bring between $10 million and $20 million. Sotheby's said the buyer wished to remain anonymous. The price included the buyer's premium.

David Redden, Sotheby's vice chairman, called the sale "a truly great moment for the world of stamp collecting."

"That price will be hard to beat, and likely won't be exceeded unless the British Guiana comes up for sale again in the future," Redden said.

Measuring 1 inch-by-1¼ inches, it hasn't been on public view since 1986 and is the only major stamp absent from the British Royal Family's private Royal Philatelic Collection.

"You're not going to find anything rarer than this," according to Allen Kane, director of the Smithsonian National Postal Museum. "It's a stamp the world of collectors has been dying to see for a long time."

An 1855 Swedish stamp, which sold for $2.3 million in 1996, previously held the auction record for a single stamp.

David Beech, longtime curator of stamps at the British Library who retired last year, has compared it to buying the "Mona Lisa" of the world's most prized stamps.

The last owner was John E. du Pont, an heir to the du Pont chemical fortune who was convicted of fatally shooting a 1984 Olympic champion wrestler. The stamp was sold by his estate, which will designate part of the proceeds to the Eurasian Pacific Wildlife Conservation Foundation that du Pont championed.

Printed in black on magenta paper, it bears the image of a three-masted ship and the colony's motto, in Latin: "we give and expect in return." It went into circulation after a shipment of stamps was delayed from London and the postmaster asked printers for the Royal Gazette newspaper in Georgetown in British Guiana to produce three stamps until the shipment arrived: a 1-cent magenta, a 4-cent magenta and a 4-cent blue.

While multiple examples of the 4-cent stamps have survived, only the tiny 1-cent issue is known to exist today.

Its first owner was a 12-year-old Scottish boy living in South America who added it to his collection after finding it among family papers in 1873. He soon sold it for a few shillings to a local collector, Neil McKinnon.

McKinnon kept it for five years before selling it to a Liverpool dealer who recognized the unassuming stamp as highly uncommon. He paid 120 pounds for it and quickly resold it for 150 pounds to Count Philippe la Renotiere von Ferrary, one of the world's greatest stamp collectors.

Upon his death in 1917, the count bequeathed his stamp collection to the Postmuseum in Berlin. The collection was later seized by France as war reparations and sold off in a series of 14 auctions with the One-Cent Magenta bringing $35,000 in 1922 - an auction record for a single stamp.

Arthur Hind, a textile magnate from Utica, New York, was the buyer. King George V was an under-bidder. It is the one major piece absent from the Royal Family's heirloom collection, Beech said.

After Hind's death in 1933, the stamp was to be auctioned with the rest of his collection until his wife brought a lawsuit, claiming it was left to her.

The next owner was Frederick Small, an Australian engineer living in Florida who purchased it privately from Hind's widow for $45,000 in 1940. Thirty years later, he consigned the stamp to a New York auction where it was purchased by an investment consortium for $280,000 - another record.

The stamp set its third record in 1980 when it sold for $935,000 to du Pont.