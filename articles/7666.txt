next Image 1 of 2

prev Image 2 of 2

The Food and Drug Administration said Tuesday it approved a nerve-stimulating headband as the first medical device to prevent migraine headaches.

Agency officials said the device provides a new option for patients who cannot tolerate migraine medications.

The Cefaly device is a battery-powered plastic band worn across the forehead. Using an adhesive electrode, the band emits a low electrical current to stimulate nerves associated with migraine pain. Users may feel a tingling sensation on the skin where the electrode is applied. The device is designed to be used no more than 20 minutes a day by patients 18 years and older.

A 67-person study reviewed by the FDA showed patients using the device experienced fewer migraines per month than patients using a placebo device. The Cefaly headband did not completely eliminate migraine headaches or reduce the intensity of migraines that occurred.

About 53 percent of 2,313 patients in a separate study said they were satisfied with the device and were willing to purchase it for future use.

No serious adverse events were connected with the device.

Cephaly is manufactured by Cephaly Technology of Belgium.