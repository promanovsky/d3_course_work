Alexander Wang has revealed that he has partnered with H&M for an exciting new collaboration!

The reveal was made shortly before the fashion designer and the popular brand’s joint party at the 2014 Coachella Music Festival was set to begin.

A brand new video to tease the collaboration was also released showing the countdown clock of a basketball game’s scoreboard. Perhaps there will be a sports element to the new collection? We will have to wait to find out all the details!

Alexander is fresh off his third collection for Balenciaga and now everyone will be able to afford his work!

“I am honored to be a part of H&M‘s designer collaborations. The work with their team is an exciting, fun process. They are very open to push boundaries and to set a platform for creativity. This will be a great way for a wider audience to experience elements of the Alexander Wang brand and lifestyle,” Alexander said in a statement.

The full collection will be available in 250 H&M stores worldwide, and online starting November 6, 2014.

ARE YOU EXCITED for the new Alexander Wang x H&M collection?



Alexander Wang x H&M Collaboration Has Been Revealed!