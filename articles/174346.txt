AOL Inc. on Monday urged its tens of millions of email account holders to change their passwords and security questions after a cyberattack compromised about 2 percent of its accounts.



The company said it was working with federal authorities to investigate the attack, in which hackers obtained email addresses, postal addresses, encrypted passwords and answers to security questions used to reset passwords.



It said there was no indication that the encryption on that data had been broken.



A company spokesman declined to say how many email accounts are registered on its system.



AOL said that it identified the breach after noticing a "significant" increase in the amount of spam appearing as spoofed emails from AOL addresses. Such emails do not originate from a sender's service provider, but their addresses are edited to make them appear that way.



© 2021 Thomson/Reuters. All rights reserved.