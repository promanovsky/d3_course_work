Altria Group (NYSE:MO), Reynolds American (NYSE:RAI), and Lorillard (NYSE:LO.DL) are about to have a lot more cash to spend. Some three-quarters of a billion dollars will start flowing to the three largest tobacco companies' bottom lines as federal tobacco quota legislation rolls off. Read on to discover why the windfall exists and what it means for your investments in Altria, Reynolds, and Lorillard.



Tobacco quota expiration

The infusion of cash due later this year to Altria, Reynolds, and Lorillard is a result of the expiration of the Tobacco Transition Payment Program, or TTPP. The TTPP was signed into law in 2004 and is set to expire in the second half of 2014.

The TTPP's origins lay in the Great Depression. In one of his many economic experiments, Henry Wallace created a federal program to limit the quantity of tobacco grown in the U.S. and to moderate a minimum price for the crop. The program was designed to aid struggling farmers during the depression by increasing the value of their crop. Producers were given quotas and could only harvest up to a certain amount of tobacco.

However, after decades of declining tobacco consumption, many quota-holding farmers were struggling to stay afloat at the turn of the millennium. As a result, tobacco-state politicians passed a 2004 federal bill that established an industry-funded buyout of tobacco quotas. Manufacturers and importers were forced to pay $9.5 billion to quota-holders over 10 years, ending in the third quarter of 2014.

Altria, Reynolds, and Lorillard pay into the program in amounts proportional to their respective market share. As a result of the program, Altria paid $400 million to quota-holders in each of the last three years, amounting to about 10% of free cash flow. Reynolds averaged a little more than $200 million per year, or 15% of free cash flow. Lorillard recorded quota-related charges of $120 million per year, or 11% of free cash flow. After these obligations are fulfilled by the end of this year, each company will experience a substantial increase in annual free cash flow.

How the companies can put the cash to use

Dividends are the most obvious use of the extra cash. Tobacco stocks attract a wide following of dividend investors because of their ample free cash flow and willingness to pay out a high percentage of earnings. Altria yields 4.9%, Reynolds yields 4.7%, and Lorillard yields 4.3%. If these companies were to use the extra cash to increase their dividends, the market would likely reward the stocks with higher valuations.

However, tobacco companies may have other ideas for using the extra cash flow. Now that the bulk of the industry's major legal liabilities are behind it, a new wave of consolidation may emerge.

Most recently, Reynolds was rumored to consider an acquisition of Lorillard. The deal would merge the No. 2 and No. 3 U.S. tobacco companies. Absent any divestitures demanded by the U.S. Department of Justice, the combined company would have a greater than 40% share of the cigarette market. The combined company could boost profitability by eliminating redundant management and distribution infrastructure while providing product diversification for both companies. Altria, however, would still be the leading U.S. tobacco company with a 50.6% share.

If the companies do not pay out the extra cash flow in dividends or use it to acquire competitors, they will likely invest it in the emerging e-cigarette market. At present, the e-cigarette market is largely unregulated, giving Altria, Reynolds, and Lorillard a huge advantage over established e-cigarette companies. In the unregulated market, the big tobacco companies can put their cash to work in national advertising campaigns that smaller competitors cannot afford to match. If tobacco companies are serious about making a push into the market, now is the time to unleash the advertising -- before regulators restrict, or even ban, e-cigarette promotion.

What to make of it all

Of course, some combination of the three options is also possible. It will take a lot more than $200 million for Reynolds to acquire Lorillard, which has a $21 billion enterprise value. That deal would most likely happen with a combination of cash and stock, if not all stock.

Moreover, Altria may not plow all $400 million into advertising its upstart e-cigarette business; it will probably pay out a substantial portion to shareholders. However, one thing is clear: Regardless of which option these companies choose for spending the cash, it will almost certainly benefit shareholders. So, Altria, Reynolds, and Lorillard shareholders, expect some good news later this year.