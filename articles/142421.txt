Nagaland today celebrated Easter, commemorating the Resurrection of Jesus Christ after Crucifixion, with thanksgiving and religious fervour.

The Kohima Baptist Pastors' Fellowship held Easter Sunrise Service this morning at the historic World War-II Cemetery with thousands of Christians offering thanksgiving prayer for the sacrifice made by Jesus Christ for redemption of mankind.

Greeting the people, Nagaland Governor Dr Ashwani Kumar wished that the day would make the people work for peace, harmony and brotherhood and foster the spirit of love and forgiveness in all.

In his message, General Secretary of Nagaland Baptist Church Council Rev Dr Anjo Keikung called upon the people to believe in Heaven and Hell and prepare oneself for the life after death.

Special Easter Services were also held throughout the state in churches while those in fast and prayer since Good Friday also joined the joyous occasion.