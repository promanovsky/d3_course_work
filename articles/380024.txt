Yahoo! (NASDAQ: YHOO) just reported results for the second quarter of 2014. In after-hours trading, shares rose about 1% on the news.

Second-quarter revenue, excluding traffic acquisition costs, or TAC, decreased 3% year over year to $1.04 billion. Adjusted earnings increased 5% to $0.37 per diluted share.

Analysts were looking for earnings of $0.38 per share on $1.08 billion in sales. Yahoo! fell just a hair short of both targets.

Adjusted display-ad revenue decreased 7% year over year because of higher ad volumes but lower prices per ad. Search sales increased 6% ex-TAC, as both the number and the value of search ad clicks rose.

As part of the earnings release, Yahoo! also announced that the share repurchase agreement with Chinese Internet giant Alibaba has been amended. Yahoo! is now committed to sell 140 million Alibaba shares as part of that company's upcoming IPO, down from the original agreement of 208 million shares.

Moreover, Yahoo! promised to return "at least half of the after-tax IPO proceeds" to Yahoo! investors. Management did not commit to any particular mix of share buybacks and/or dividend payments to achieve this goal.

Yahoo~ CEO Marissa Mayer was not pleased with some of these results.

"Our top priority is revenue growth, and by that measure, we are not satisfied with our Q2 results," she said in a prepared statement. Search ads, social media, and mobile properties wre strong, but "display remains an area of investment and transition."

"I believe we can and will do better moving forward. Overall, I remain confident in Yahoo!'s future, our strategy, and our return to long-term growth," Mayer concluded.