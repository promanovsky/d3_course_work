LG is going to announce its new smart watch, the G Watch R, at the IFA conference in Berlin next week.

The company posted a teaser video on Sunday night that shows a sleek, classic-looking design and a few features, including a compass and step counter. The video was first spotted by Engadget. LG's previous G smart watch was a square design.

The face will be circular.

LG

It will light up, like a computer. LG

The device will have a compass.

LG

And a pedometer. LG

Here's what the home screen could look like, just like normal watch. LG

Here's the teaser video: