Researchers discovered a new "body hack" that could help people beat jet-lag, and even created an app to help them do it.

"Overcoming jet lag is fundamentally a math problem and we've calculated the optimal way of doing it," Danny Forger, a University of Michigan professor who led its development, said in a news release. "We're certainly not the first people to offer advice about this, but our predictions show the best and quickest ways to adjust across time zones."

The free new app, dubbed "Entrain," use math to achieve "entrainment," which means "synchronizing circadian rhythms with the outside hour," the news release reported.

The app relies on the fact that light wavelengths (especially those from the Sun) appear blue to our eyes; this helps regulate our circadian rhythms.

This process is based on the Earth's 24-hour day, it can regulate things such as sleeping patterns but it also works on a cellular level.

People whose circadian rhythms are frequently disturbed by things such as jet leg could have an increased risk of "depression, certain cancers, heart disease and diabetes," the news release reported.

People who are most at risk of these consequences are "pilots, flight attendants and shift workers," the news release reported.

The app provides schedules that let the user know a block of time in which they should seek out bright light and another chunk of time where it's best to be in the dark. During this "dark time" pink sunglasses are recommended if it is necessary to go outside. A light box can be used if it is nighttime during periods where bright light is prescribed.

"Say you're traveling from Detroit to London, five hours ahead. Your flight leaves at 10 p.m. Eastern Time and arrives at 11:05 a.m. London time the next day. It's a work trip and you'll have to spend most of your time in indoor lighting. Under those circumstances, the app says it can adjust you in about three days. That's less than the rule-of-thumb one day per hour outside the starting time zone," the news release reported.

WATCH:



@ 2018 HNGN, All rights reserved. Do not reproduce without permission.