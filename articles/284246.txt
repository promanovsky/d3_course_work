Fred Wilson beat me to it this morning with his post A Big Win For The Patent Reform Movement but he’s got a couple of hour time zone advantage over me. Regardless, I love Fred’s punch line:

So it was with incredible joy that I read these words by Elon Musk, founder and CEO of Tesla Motors and possibly the most innovative entrepreneur in the world right now. [Elon wrote in his post All Our Patent Are Belong To You] “Yesterday, there was a wall of Tesla patents in the lobby of our Palo Alto headquarters. That is no longer the case. They have been removed, in the spirit of the open source movement, for the advancement of electric vehicle technology.”

I’ll pile on with my accolades to Elon. While I don’t know him, I’m long time friends with his brother Kimbal who lives in Boulder so I always feel like I get a little taste of Elon whenever I talk to Kimbal. So – Elon – thank you for being a real leader here and taking action.

I’ve been asserting for a number of years that while software patents are completely fucked up, the general patent system stifles innovation. More and more research is appearing on software patent issues and patent trolls in general, including this recent piece by Catherine Tucker, an MIT Sloan professor of Marketing, titled The Eﬀect of Patent Litigation and Patent Assertion Entities on

Entrepreneurial Activity. As Ars Technica summarizes in New study suggests patent trolls really are killing startups:

Turns out there is a very real, and very negative, correlation between patent troll lawsuits and the venture capital funding that startups rely on. A just-released study by Catherine Tucker, a professor of marketing at MIT’s Sloan School of Business, finds that over the last five years, VC investment “would have likely been $21.772 billion higher… but for litigation brought by frequent litigators.”

As my lawyer friends tell me, “the Supremes” are finally making some calls on this. The induced infringement theory, a particularly obnoxious patent litigation approach, is no longer valid. The main event, Alice Corp. v. CLS Bank, is still waiting to be ruled on. Let’s hope the Supremes take a real stand on when software claims are too abstract to be patented this time around, unlike the punt they made on Bilski.

The post Thanks Elon Musk For Being A Real Leader On Patent Reform appeared first on Feld Thoughts.

Read more posts on Feld Thoughts »