A DirecTV satellite dish stands on the roof of a home in Compton, California, U.S., on Monday, May 5, 2014. DirecTV beat analysts' first-quarter profit estimates on higher bills for U.S. customers and more subscriber sign-ups than expected in Latin America. Photographer: Patrick T. Fallon/Bloomberg via Getty Images

Telecom giant AT&T is in "advanced talks" to buy satellite TV service DirectTV for about $50 billion, according to Bloomberg News, which spoke with people familiar with the matter.

The potential agreement could be as close as two weeks away, according to the Wall Street Journal. With shares of DirecTV trading for around $85 a piece in after-hours trading, the company's market value is currently somewhere in the $45 billion range, or $5 billion less than AT&T's supposed purchasing price.

For DirecTV, partnering with AT&T could be the solution to its biggest problems: declining subscribers and pressure from cord cutters, i.e. Americans who are forgoing cable packages and instead opting for streaming services or set-top boxes. These problems together likely help explain why DirecTV experienced its second-ever quarterly loss of U.S. subscribers last August.

Were the two sides to reach an agreement, the partnership would morph the companies into a massive pay-television conglomerate. According to the WSJ, AT&T and DirecTV had previously attempted to work out a deal, but those talks fell apart because of a number of issues, including pricing.

This post has been revised to clarify that Charlie Ergen has played no role in AT&T's discussions about acquiring DirecTV.