Stand-up comedian John Pinette, known for his role in the finale of “Seinfeld,” was found dead in Pittsburgh on Saturday, according to the Allegheny County medical examiner's office, TMZ reported.

Pinette, 50, was discovered dead in a Sheraton Hotel room at about 2:30 p.m. Foul play is not suspected.

Pinette's manager, Larry Schapiro, told the Hollywood Reporter that the comedian died from a pulmonary embolism. Pinette reportedly had been suffering from liver and heart disease. He had checked into rehab for a prescription pill addiction last August.

The Huffington Post revealed that he was scheduled to be on tour from April to June, with dates set in the U.S. and Canada.

To remember the comedian, some of his funniest jokes have been posted below, courtesy of the Internet Movie Database and Search Quotes:

1. [About having to wear pantyhose during his role as a woman on Broadway] “Guys! We have our underwear 10, 12 years! Ladies just throw pantyhose out! I'd go through a crate of 'em a week! And the pantyhose rip for no reason! You just... *rip*. We have material that stops bullets and the pantyhose are ripping? It's built-in obsolescence! It's bullsh*t!”

2. “What do they give you for a side dish in England for breakfast? Home Fries? Hash Browns? Fresh fruit? Nay, nay. They give you beans. Oh, good. Let's start me off in the morning: empty stomach, cup of black coffee, and some beans. Now let's walk me around London for a little while, getting me all churned up. Put me in a taxi and see what happens. I blew the doors off the taxi.”

3. “And I tried the low-carb bread. Have you tried it? It's horrible. I tasted it. I thought the wrapper was still on. It's not like it went bad, it never went good. They have "I Can't Believe It's Not Butter," they should call it, "This Ain't Bread." 'Cause it looks like bread, but it has no other properties of bread. I said, "You know what? I'll butter it. That'll make it better." Butter won't go on it. It slides right off. The butter's like, "Where are you putting me?" Jam and jelly beads up and fall off it. Did they Scotch-guard this at the factory? You know what I'll do? It's okay, I'll toast it. I'll make a sandwich, I'll toast it. It's better when you toast it. It doesn't toast. You can't toast it. I'm out in the garage with a blow torch. It's absorbing the heat like a space shuttle tile.”

4. [Discussing one of his nutritionist's advice] “She wanted me to have salad as the food! No! Salad isn't food. Salad comes with the food. Salad is a promissory note that something good is going to happen... and I should just wait right here.”

5. [About having to shave off his eyebrows for his role as a woman on Broadway] “Socially this was very difficult on me. When you have no eyebrows... people don't know what's wrong but they're pretty sure something isn't right. And maybe we should take the next elevator, Honey.”

6. “I don't do up. Sit-ups. Push-ups. Pull-ups. I do downs. Sit down. Lay down. Blackjack, I'll double down. Give me a cheeseburger, I'll wolf it down. Put on a little music, I'll boogie down.”

7. “If you go to Bed Bath and Beyond without a coupon, people will wonder if you're OK.”

Follow me on Twitter @mariamzzarella