‘The Bachelorette’ 2014 Spoilers: Name of Andi Dorfman’s Fiancé Revealed

With the season premier date of “The Bachelorette” approaching, Andi Dorfman is returning as the star of the show in search of her prince charming. Dorfman was the brave brunette who left the previous season of “The Bachelor,” when she walked out on Juan Pablo Galavis.

ABC recently revealed the names of the 25 lucky lads who will vie for the heart of the 26-year-old Atlanta, GA, lady.

But spoilers for the show already abound, as websites reveal the name of the man Dorfman is engaged to.

Other spoilers for the season’s premiere episode include Dorfman’s father showing up for the first night of the show, as well as Chris Bukowski who was on “The Bachelorette” two season’s earlier and then on “Bachelor Pad.” Dorfman, however, shoos him away without even meeting him, according to The Hollywood Gossip.

The first episode will air on ABC on Monday, May 19, 2014. In the second episode, viewers can expect to see Craig Muhlbauer, Nick Sutter, and Carl King eliminated. The episode will be shot in Los Angeles.

Episode 5, airing June 16 in Marseilles, France, will show the elimination of Marquel Martin, Andrew Poole, and Pat Jagodzinski. The following episode will be shot in Verona, Italy, where JJ O’Brien will be eliminated during the rose ceremony and Cody Sattler will get booted before the ceremony.

The website reported that Dorfman picks Josh Murray at the end of the show and is now engaged to him. Murray, 29, is from Tampa, FL, he is a former pro baseball player who loves to watch “Gladiator,” “Troy,” and “Wedding Crashers.”

The first runner up was Nick Viall, the 33-year-old software sales executive from Waukesha, WI. All contestant profiles can be viewed here.

Detailed episode by episode spoilers are regularly posted on RealitySteve.com.