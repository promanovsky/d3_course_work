Net neutrality is “the free speech issue of our time,” Sen. Al Franken, D-Minn., says in a YouTube video released Tuesday, and the FCC's proposed rules for Internet service providers threaten a "free and open Internet."

Franken said the rules that FCC Chairman Tom Wheeler proposes would let corporations buy into an Internet “fast lane,” at the expense of consumers and smaller and emerging companies. Wheeler, however, contends that “the proposal would establish that behavior harmful to consumers or competition by limiting the openness of the Internet will not be permitted.”

The comedian-turned-senator joins a small but growing number of tech companies who have spoken out against the FCC’s proposed rules, which it is expected to vote on May 15. Mozilla, Netflix Inc. (NASDAQ:NFLX) and online user-submission community Reddit, which is owned by Advance Publications, have all criticized Wheeler’s proposed rules. Franken says the rules will abolish Net neutrality, allowing big corporations to get their content to consumers faster than their rivals, creating “higher rates for Internet service, and new obstacles to accessing the content that we want.”

The full video of Franken’s statement is below, followed by a transcript of his remarks.

Hi, I’m Senator Al Franken.

It was American taxpayers who paid for the development of the Internet, by DARPA, the Defense Advanced Research Projects Agency. Since then, the Internet has changed everything about the way we communicate with each other. In the astounding innovation that accompanied and accelerated this revolution was because of the basic architecture of the Internet – Net neutrality.

Net neutrality is the principle that all data on the Internet should be treated equally. Under Net neutrality, internet service providers have to let all content flow at the same speed.

Let me give you an example of how Net neutrality has enabled innovation. Before YouTube, there was Google TV. Google TV wasn’t that great, so some guys started YouTube over a pizzeria in San Mateo, California. YouTube was better than Google TV, and because both traveled at the same speed to the viewer, people were able to make a choice between the two.

YouTube won out, and Google ended up buying YouTube for a lot of money, and everyone has benefited. Now, Net neutrality is under threat as it never has been. The chairman of the FCC has proposed new rules to permit a fast lane for content providers that are willing and able to pay for it.

This means that corporations will be able to get their content delivered faster. Mom and pop stores would lose even more ground to corporate giants. Big media companies will be able to get their version of the news to consumers faster, and would end up paying for it with higher rates for internet service, and new obstacles to accessing the content that we want.

This is the free speech issue of our time. We cannot allow the FCC to implement a pay-to-play system that silences our voices and amplifies that of big corporate interests. We have come to a crossroads. Now is the time to rise up and make our voices heard to preserve Net neutrality.