Warren Buffett's Berkshire Hathaway Inc bought a new stake of 11 million shares in Verizon Communications Inc in the first quarter and added to its holding in Wal-Mart Stores Inc, according to a regulatory filing on Thursday.

Verizon declined to comment.

The share price of Verizon rose 1.6 per cent to $48.75 in after-hours trading following the disclosure, which came in a US Securities and Exchange Commission filing made public on Thursday that detailed Berkshire equity investments as of March 31.

Shares of companies often rise after Berkshire reveals new stakes because some investors consider it a vote of confidence by Buffett and try to copy him.

The regulatory filing did not state whether the Verizon investment was made by Buffett himself, or by one or both of his portfolio managers, Todd Combs and Ted Weschler.

Berkshire, an ice cream to insurance conglomerate where Buffett is chairman and chief executive, increased its holding of Wal-Mart by 17.3 per cent to 58.1 million shares.

US regulators require large investors to disclose their stock holdings every quarter, and the disclosures can offer a window into their strategies for buying and selling stocks.

Berkshire added to stakes in other companies, as well, including DaVita HealthCare Partners Inc and IBM Corp, among others.

Berkshire Hathaway reduced its stake in several companies, among them DirecTV and General Motors Co.

Copyright: Thomson Reuters 2014