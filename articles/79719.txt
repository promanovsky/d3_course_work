Leading scientific thinkers of their time, such as Aristotle, Rene Descartes, Guillaume Duchenne, and Charles Darwin, have long promoted the idea that there are a handful of basic emotions that people express. In recent decades, that group has crystalized into six core emotions: happiness, surprise, sadness, anger, fear, and disgust.

But there are clearly many shades of gray between those emotions. For example, there’s the happy-because-I’m-eating-ice cream and the happy-because-I-just-learned-I-got-a-surprise-marriage-proposal looks, each of which is slightly different.

That’s what intrigued Aleix Martinez, associate professor of electrical and computer engineering at Ohio State University. “Six seemed a small number given the rainbow of possibilities of feeling and expressing emotions,” he says.

MORE: Emotions May Not Be So Universal After All

Martinez wanted to know whether compound emotions, such as happy surprise, were expressed using the same muscle movements of both happiness and surprise, or whether the expression involved a unique set of muscles that represented some amalgam of the two.

What he and his colleagues found was that the human face makes 21 different emotional expressions – and each is different from the other. While some represented combinations of emotions, each differed in terms of which muscles were involved.

And surprisingly, these facial expression patterns were remarkably consistent across all 230 volunteers. For example, each showed happy surprise in the same way that was distinct from both happiness and from surprise, and different still from angry surprise.

MORE: To Really Read Emotions, Look at Body Language, Not Facial Expressions

Martinez broke down the facial expressions of 230 volunteers by applying his engineering strategies. He and his colleagues gave each of the students, staff, or faculty members who enrolled in the study different scenarios and asked them to show how they would react in each one. They were told, for example, that they had just learned they had been accepted to a graduate program, that someone had told them a disgusting, but still funny joke, or that they had just smelled something bad. The volunteers were allowed to practice their facial expressions in front of a mirror before Martinez took pictures of their reactions.

He then computer-analyzed each of the 5,000 images, breaking them down by which facial muscles the participants used. These were first defined in 1978 by psychologist Paul Ekman, who codified facial expressions in the Facial Action Coding System (FACS) by action units, or muscles or groups of muscles that went into making facial expressions – such as lip parts (for showing disgust), showing teeth (for expressing happiness), mouth stretch (for fear), or eyelid tightening (for anger).

MORE: How to Lift Your Mood? Try Smiling

Not only are people able to make compound expressions consistently, but we are also able to read these emotions pretty accurately as well. When we see someone who looks angrily surprised, for example, we know that they aren’t happy about whatever unexpected event they just experienced. “We don’t know for sure how much is learned or innate in expressing these emotions,” says Martinez. “But what we do know is that a big component has to be innate because otherwise different people would use different muscles to show the same emotion.”

The results dovetail with recent findings that some cultures may already recognize these more refined, and numerous expressions of emotions. In a study among the remote Himba tribes in Namibia, for example, researchers at Northeastern University reported that when they provided tribe members with images of facial expressions of emotions, rather than creating six neat piles of emotions, the members created many more. For them, happy could be interpreted as anything from happy to laughing to wonder.

That should help to improve how studies of human emotions are conducted in different cultures, says Martinez; his results suggest that the underlying ability to express emotions may be similar around the world, but cultural biases may simply define emotions in different ways – much the same way that babies are born with the capacity to speak and make sounds for any language, but are trained to speak their native tongue by what they hear around them.

Even more exciting for mental health experts is the possibility that this work can teach them about when the processing and expression of human emotions goes awry such as in depression. “It’s important to understand which are categories of emotions that we have,” says Martinez, so that experts can recognize the pathological ones. Expressing emotions is generally a transient exercise, lasting milliseconds or up to a minute. Those that linger longer, for hours or even days, tend to be considered moods, and once these emotions persist for days, weeks or months, they can become the subject of mental illness. So distinguishing how people express themselves, and defining the categories of well-known and well-recognized emotions, could lead to better understanding of which emotions can become detrimental and even harmful.

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.