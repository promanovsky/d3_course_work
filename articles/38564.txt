One thing we hate: the word "recession." The Dow Jones Industrial Average (DJINDICES:^DJI) dipped 26 points Monday as some banks warned that recent economic sanctions imposed on Russia could send the world's ninth-biggest economy into a recession. And no one in the interconnected global economy likes recessions.

1. Netflix falls after rumors of Comcast/Apple streaming video deal

Apple (NASDAQ:AAPL) is in talks with Internet and cable provider Comcast (NASDAQ:CMCSA) to create a special streaming television service available only to Apple TV users. The elite streaming service would bypass the rest of the Internet's congestion altogether (including Netflix Internet usage) and create a very special "Club Apple" within the Internet.



Comcast controls Internet traffic and might give Apple its own special lane on the Internet superhighway. Earlier in the year, Netflix signed a deal with Comcast guaranteeing it would continue getting top "normal" Internet speeds for its customers, but this Apple "elite" online streaming option would make you feel as cool as when the bouncer lets you cut past the rest the line Saturday night at the Meatpacking District.



A cool Apple TV has been rumored about for years in tech blogs, but Cupertino has only managed a mediocre app-based device that works properly about half the time. This new Apple streaming service would be a step above the rest of the Internet, with streaming quality as good as, well, TV. Wall Street and pretentious television bingers viewers applauded the news, shooting Apple up more than 1%.



What about Netflix? If true, the rumor would mean serious streaming competition from the biggest company in the world (that being Apple). That's why Netflix stock dropped 6.7% Monday. Comcast is cashing in on corporate deals like it's its J-O-B recently (it's hooked up with Time Warner Cable and Netflix, and now it's rumored to have a fling with Apple), and the stock rose another 0.6% on the interesting news.

2. Nu Skin surges after a small Chinese fine

It was a glowing day for the sketchy dermatological rando-product seller Nu Skin Enterprises (NYSE:NUS). Shares popped 18% on Monday on word that the Chinese government was fining the company only $540,000 for illegal product sales, much less than analysts were expecting (plus, six very sorry employees are facing more than $240,000 in individual fines).

By the skin of its teeth, Nu Skin is now going to be able to move ahead with its usual business activities of anti-aging and beauty creams. China's State Administration for Industry and Commerce had accused Nu Skin of misleading customers with exaggerated skin results, while some salesmen simply made some illegal sales. The legal action is over, and investors were happy to hear the news. We'll stick with good ol' fashioned pimple popping.

3. Herbalife rises as three more Icahn disciples get board seats

Famous investor Carl Icahn knows his stuff. The dietary supplement company Herbalife (NYSE:HLF) is a well-known stock of Icahn's (he owns 17%). The company announced Monday that it's so enamored by Icahn's vision and market understanding that it will add three more employees of Icahn's hedge fund to join the board of directors.



That brings Icahn board members to five, and Icahn's loving it. The stock rose 6.7% as investors learned that Icahn would have even more influence on the company. The board of directors is in charge of steering the company by representing shareholders, hiring and firing top management, and making sure shareholders get rich. Icahn's strong track record of making shareholders rich (see Apple, Netflix) made investors want to get involved in the party.

Tuesday:

March Consumer Confidence

New Home Sales and the S&P Case-Shiller Home Price Index

MarketSnacks Fact of the Day: Since 1991, Ukraine has received over $30 billlion in government aid (grants and loans) from the U.S. and Europe -- and $200 billion to 300 billion from Russia in the form of Natural gas subsidies.

As originally published on MarketSnacks.com