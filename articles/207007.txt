Scientists have found that zapping the brain with a specific frequency of electricity can induce lucid dreams - a state of mind in which people are aware that they are dreaming, and can control the plot.

Researchers found that when inexperienced dreamers were zapped with a current of 40 Hertz, 77 per cent of the time these participants reported having lucid dreams.

"I never thought this would work. But it looks like it does," said study researcher Dr John Allan Hobson, a psychiatrist and longtime sleep researcher at Harvard University.

In the study, the researchers placed electrodes on the scalps of 27 participants, who were not lucid dreamers, to stimulate the frontal cortex, and recreate the gamma wave activity that has been seen in lucid dreamers.

Over four nights, they applied the 30-second bolts of electrical currents to the participants' scalps, two minutes after the participants had entered the dreaming stage of sleep, as shown by their brains' activity patterns.

The frequency of stimulation varied from 2 Hz to 100 Hz, and sometimes the researchers didn't actually deliver any electrical currents, 'LiveScience' reported.

The participants were then immediately woken up to report their dreams to an interviewer who wasn't aware of which stimulation they had received.

The electroencephalography, or EEG data showed that the brain's gamma activity increased during stimulation with 40 Hz, and to a lesser degree during stimulation with 25 Hz; stimulation with other frequencies didn't lead to any changes in the brain waves, and it didn't increase the likelihood of people having lucid dreams.

The researchers also found that after stimulation, if people did experience a lucid dream, the gamma activity increased even more.

"We were surprised that it's possible to force the brain to take on a frequency from the outside, and for the brain to actually vibrate in that frequency and actually show an effect," said study researcher Ursula Voss, of JW Goethe-University Frankfurt, who designed the experiments.

Beyond advancing the understanding of what happens during lucid dreams, the new findings may add insight to the broader research on the nature of consciousness, and how it comes about, researchers said.