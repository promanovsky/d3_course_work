Valeant Pharmaceuticals Intl Inc (NYSE:VRX) has officially said it’s going hostile on Allergan, Inc. (NYSE:AGN) and has now fired its next shot. In a presentation on Monday, Valeant said it was correcting “recent misrepresentations” and refuting “misleading assertions made by Allergan, Inc. and others.”

Valeant again makes the case for Allergan

In the presentation, Valeant Pharmaceuticals Intl Inc (NYSE:VRX) management said Allergan, Inc. (NYSE:AGN) shareholders will receive a 58% earnings accretion starting from the midpoint of the company’s current 2014 revised guidance, plus $72 per share in cash. They added that if shareholders reinvested the cash into Valeant stock at yesterday’s closing price, the earnings accretion pops up to 176%.

Berkshire Hathaway’s Buffett On Diversification In his 2014 letter preview, Buffett states that, far from being a weak choice, indexing can often achieve investor's goals far more easily than complicated picking strategies. This is a special guest post by Robert R. Johnson, Ph.D., CFA, CAIA. He is a full professor of finance at the Heider College of Business at Creighton Read More

Valeant Pharmaceuticals Intl Inc (NYSE:VRX) will receive earnings accretion of about 25% if the two companies combine. Company management added that Allergan, Inc. (NYSE:AGN) shareholders could realize more than 90% stock price accretion from the unaffected stock price before they announced their bid. Valeant shareholders would see a 45% stock price accretion based on the unaffected Valeant stock price.

4 steps toward a Valeant – Allergan merger

Next, they set out four steps they see as being necessary in combining Valeant Pharmaceuticals Intl Inc (NYSE:VRX) and Allergan, Inc. (NYSE:AGN). First, Valeant said they plan to launch their exchange offer this week. As part of this step, activist investor Bill Ackman’s firm Pershing Square Capital Management has begun a proxy battle to remove six of Allergan’s board members. The firm also filed suit in an attempt to keep the recently adopted poison pill from being triggered.

Pershing must convince 25% of shareholders to hold a special meeting to remove those six board members. That’s step two. Valeant Pharmaceuticals Intl Inc (NYSE:VRX) management said the meeting could be held as early as August if Allergan, Inc. (NYSE:AGN) accepts their suggested date. If not though, they believe it will be by the end of this year.

After the special meeting, if the six board members in question have been removed, the third step is to make applications to elect directors to replace the ones they removed. A collection of shareholders with at least 10% of Allergan, Inc. (NYSE:AGN)’s shares can apply. To do that, just .3% of them needs to apply because Pershing already owns the greatest majority of that required 10%. They want that meeting to be held within two months of the application.

And finally, they believe the new slate of directors will begin negotiating with Valeant Pharmaceuticals Intl Inc (NYSE:VRX) about a takeover and then complete the exchange offer.

Valeant says Allergan is wrong

One of the cases Allergan, Inc. (NYSE:AGN) has made for not wanting to be taken over by Valeant Pharmaceuticals Intl Inc (NYSE:VRX) is because it believes Valeant’s earnings are going to be weak. However, management says that’s not the case. Management said they will continue meeting expectations for the rest of the year.

They went on to address a number of misrepresentations they say Allergan, Inc. (NYSE:AGN) made in past presentations. For example, they said Allergan said Valeant saw 11 of its top 15 worldwide products decline in volume. However, they said the products Allergan listed are not their top 15 products by revenue and not all of them are worldwide.

Valeant Pharmaceuticals Intl Inc (NYSE:VRX) said it will post the full list of misrepresentations and corrections on its website this week.