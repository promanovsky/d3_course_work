After Tesla Motors Inc (NASDAQ:TSLA) reached compromises with auto dealerships in two other states, it appears that New Jersey could be the next one. According to CNN Money, there are a couple of bills making their way through the state legislature right now which would allow the automaker to keep selling its cars in the state.

Tesla ban moved back

Auto dealers in the state have agreed to give Tesla Motors Inc (NASDAQ:TSLA) 15 additional days to sell its vehicles directly to consumers. That ban was originally set to begin April 1, but now the state has moved the start date back to April 15. Dealers also said they would support a longer extension as lawmakers in the state develop a compromise.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

However, a spokesperson for the New Jersey dealer’s trade group told CNN that they wouldn’t support it forever. He said Tesla Motors Inc (NASDAQ:TSLA) must comply with franchise laws in some way.

Details on the legislation

Currently in the New Jersey senate, Sen. Raymond Lesniak, a Democrat, has suggested that sales of zero-emission vehicles, which would include Tesla Motors Inc (NASDAQ:TSLA)’s vehicles, until they are 4% of the total car market. This isn’t projected to happen until 2018. The Electric Drive Transportation Association reported that last year, zero-emission vehicles made up just .6% of car sales in the U.S.

Tesla Motors Inc (NASDAQ:TSLA) itself has acknowledged that one constraint keeping it from selling many more cars is the fact that it doesn’t use dealerships. The automaker is projecting sales of 35,000 cars this year but 500,000 cars by 2020.

The other bill is in the New Jersey Assembly, and it comes from Democrat Timothy Eustace. His bill, however, offers a permanent exemption from franchise laws for makers of electric cars. Lesniak believes Gov. Chris Christie would be open to bills which allow Tesla Motors Inc (NASDAQ:TSLA) to keep selling its cars directly to consumers in the state. The governor initially said that the recently voted-through ban just enforces laws which already exist there. However, he did say previously that the legislature should decide what to do about the issue.