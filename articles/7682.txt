Cefaly, the name of the new device, “provides an alternative to medication for migraine prevention,” Christy Foreman, director of the Office of Device Evaluation at the FDA’s Center for Devices and Radiological Health, said in a statement. “This may help patients who cannot tolerate current migraine medications for preventing migraines or treating attacks.”

The US Food and Drug Administration gave the thumbs-up on Tuesday for the first device to prevent migraines in those who have the headaches on a regular basis. It’s likely safe but how well it works remains questionable since it was only tested in 67 people before it was approved.

Advertisement

It’s a small, portable, battery-powered, device that resembles a plastic headband worn across the forehead and on top of the ears that delivers electrical impulses to stimulate the trigeminal nerve, which is thought to be involved in the onset of pain. The device is indicated for those 18 years of age and older, and should be used no more than once a day for 20 minutes at a time, according to the FDA.

To approve the device, the FDA looked at data from a small clinical study conducted in Belgium involving 67 people who experienced more than two migraine headache attacks a month and who had not taken any drugs to prevent migraines for three months before entering the study. Those who used Cefaly experienced significantly fewer days per month with migraines and used lower doses of painkillers to relieve their headaches compared to those who used a placebo device. But Cefaly didn’t prevent migraines completely.

A patient satisfacton survey involving 2,313 Cefaly French and Belgian users that the Belgium-based manufacturer provided to the FDA found that about 53 percent of users were satisfied with Cefaly treatment and willing to buy the device for continued use. The most commonly reported complaints were dislike of the tingling sensation caused by the device, sleepiness during the treatment session, and headache after the treatment session. No serious side effects were reported during either study, according to the FDA.

Advertisement

The device retails for the equivalent of $225 on the Canadian Costco website.

Deborah Kotz can be reached at dkotz@globe.com. Follow her on Twitter @debkotz2.