SpaceX chief executive Elon Musk answers questions in front of the SpaceX Dragon spacecraft at the SpaceX Rocket Development Facility in McGregor, Tex. (Duane A. Laverty/AP)

SpaceX chief executive Elon Musk, waging an increasingly ferocious battle to gain a larger share of the national security launch business, has accused one of his main competitors of hiring a high-ranking Air Force acquisitions official in exchange for awarding a lucrative contract without competition.

In a series of tweets Thursday night, the billionaire entrepreneur drew attention to the hiring of the former Air Force official, Roger Correll, by Aerojet Rocketdyne, which supplies rocket engines to United Launch Alliance. Even though they are separate companies — ULA has many different suppliers — Musk conflated them in his statements and once again called for an investigation into the contract.

“V likely AF official Correll was told by ULA/Rocketdyne that a rich VP job was his if he gave them a sole source contract,” Musk tweeted. “Reason I believe this is likely is that Correll first tried to work at SpaceX, but we turned him down. Our competitor, it seems, did not.”

Glenn Mahone, a spokesman for Aerojet Rocketdyne, said the allegations “are completely without merit” and that Correll received “the necessary clearances and approvals from the Department of Defense.”

Last month, Musk, the co-founder of PayPal and Tesla Motors, sued the government over a multibillion-dollar contract for 36 rockets to launch defense payloads, such as spy and GPS satellites, into space. The contract was awarded to ULA, a joint venture between Boeing and Lockheed Martin. But Musk has said the contract should have been competitively bid and has called on the Air Force to cancel the contract and to allow SpaceX to compete for it.

1 of 37 Full Screen Autoplay Close Skip Ad × 2014 images of space View Photos Discoveries in our solar system and beyond. Caption Discoveries in our solar system and beyond. An image taken and posted by astronaut Reid Wiseman to social media on Oct. 29, 2014 shows a sunrise, captured from the International Space Station (ISS). Wiseman wrote: 'Not every day is easy. Today was a tough one.' referring to the 28 October loss of the Orbital Sciences Corporation Antares rocket and Cygnus spacecraft, moments after launch at NASA's Wallops Flight Facility in Virginia. Reid Wiseman / NASA/EPA Wait 1 second to continue.

Although SpaceX is still in the process of acquiring the proper certification to allow its Falcon 9 rocket to participate in the program, Musk said his company could perform the work far more cheaply.

In a statement, ULA said: “At the time of the initial acquisition process in 2012, the contract award in June 2013, and even today, ULA is the only government certified launch provider that meets all of the unique . . . requirements that are critical to supporting our troops and keeping our country safe.”

The Air Force, citing the ongoing litigation, declined to comment.

The tension over the contract produced a couple of zingers this week in Colorado Springs, Colo., at the 30th Space Symposium, the premier annual event bringing together the national security and civilian space industry.

On Tuesday, Gen. William Shelton, commander of the Air Force Space Command, was asked in a news conference about the lawsuit by SpaceX seeking to reopen the bidding on the contract for national security launches.

“You know, generally the person you’re going to do business with, you don’t sue them,” Shelton said. When asked to elaborate, he said he wanted to leave it at that.

Shelton said the Air Force is spending $60 million, and detailing 100 people, to certify SpaceX for national security payloads but cautioned that there is much work to do before that certification is complete.

On Wednesday, Gwynne Shotwell, the president and chief operating officer of SpaceX, appeared on a panel with Michael Gass, ULA’s chief executive — separated by a moderator. Gass offered his view that consolidation of companies can lead to more efficient operations and lower launch costs and cited his own company’s history as a consolidation of Lockheed’s and Boeing’s national security launch systems.

That was too much for Shotwell, who said, pointedly, “Consolidating to the point of monopoly has never served the consumer — ever.”

Later, in a phone interview, Shotwell addressed the question of whether the Air Force has an institutional bias in favor of ULA.

“The Air Force is a group of a lot of different folks and a lot of different groups. I think there are lots of leaders in the Air Force that are keenly interested in getting competition. That said, the Air Force is an institution, and they tend to have institutional drag or momentum, and as an institution it’s easier for them to stick with ULA,” Shotwell said.

She said of the noncompetitive contract awarded to ULA, “ULA loves it because they don’t want to compete against SpaceX. They can’t win a fair fight. They either want a dirty fight or they don’t want a fight at all.”

Late Wednesday, Gass sat down for an interview in which he said national security launches are different from other space ventures because the payloads are so precious and the need for reliability is so great.

“People use a pejorative term like ‘monopoly,’ ” Gass said. “I like to think of us as a ‘sole source provider.’ ”

He went on: “Competition is a wonderful word. It’s American. I believe in it. We’re dealing with a national security need.”

He said he expects SpaceX to become certified and compete for launches in the future.

“They’ve got good people who used to work for us who know how to build a rocket. They’ll eventually get there,” Gass said. But when the Air Force contract was awarded, he said, “There was no certified new entrant. There’s still none.” Of SpaceX, he said, “Now they just want to change the rules.”

Sen. John McCain (R-Ariz.) wrote to the Air Force’s inspector general last month, urging him to investigate the program, saying that “without competition [it] has been plagued by exponential cost growth and schedule delays.”

Achenbach reported from Colorado Springs.