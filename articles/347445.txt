Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Stephen Fry delighted fans at London's O2 last night as he made a cameo appearance at the opening performance of Monty Python Live (Mostly).

And judging by his tweet after his walk-on he was just as thrilled to be there.

Other stars in the audience were equally impressed by the comedy kings' comeback...

Meanwhile, The Mirror's Mark Jefferies summed up last night's first Monty Python Live (Mostly) show as the comedy equivalent of watching The Beatles perform a greatest hits gig and awarded it four stars out of five.

poll loading Should the veteran comics have made a comeback? 0+ VOTES SO FAR YES NO

Here's what some of the other newspaper critics had to say about the opening performance at London's O2.

The Telegraph

"The Pythons came, they doddered, but they conquered... Although there’s an over-reliance on TV clips between-scenes, the live material looks far more golden than olden." (Awarded four out of five stars.)

The Guardian

"The sketches are old – of course they are - and you'd have to have a heart of stone not to enjoy hearing them again just a bit, though the campy jokes about men with silly effeminate voices and ladies' underwear have dated." (Awarded three stars).

The Daily Mail

"Once they were the sharpest thing in satire. Last night, quite often, they looked and sounded like a dodgy tribute band. Palin and Idle were the busiest, the most enthusiastic. Palin has the milky gaze of a retired geography teacher but he still looks perfect in a brown shop assistant's coat." (Awarded three stars).

The Express

"Monty Python is side-splitting comedy history in the making. Some of the topspin may have gone from the lines but the comedy timing is still there." (Awarded the full five stars).

The Independent

"This is a desperately lazy production, resting on its laurels, uninterested in showcasing new material, relying on TV footage and the whooping adulation of an audience who know all the words. And when the animations and the hoofing girls and boys are on, you wonder: where have the cast gone? Are they having a little lie-down?" (Awarded two stars).

The Times

The truth is simple. Do this lot have the comic vitality they had 40 years ago? They do not. Yet there are plenty of laughs here - sure, laughs of recognition rather than laughs of surprise, but genuine laughs all the same." (Awarded three stars).

poll loading Were Monty Python right to make a comeback? 0+ VOTES SO FAR YES NO