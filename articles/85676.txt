The Boss will close out three days of performances in Dallas that will keep NCAA Final Four fans rocking.

Andrew Dost of the pop-rock group fun says he's excited to be part of the energy. His band will take the stage Sunday before Bruce Springsteen and the E Street Band on Sunday,

Others performing during the free three-day NCAA March Madness Music Festival at Reunion Park in downtown Dallas include Jason Aldean, Tim McGraw and The Killers.

The park is on the site of the now-demolished Reunion Arena, which hosted the Final Four in 1986. This year's Final Four will be held at the massive stadium in nearby Arlington where the Dallas Cowboys play.