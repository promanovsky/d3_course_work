European bank Credit Suisse AG pleaded guilty Monday to helping wealthy Americans avoid paying taxes through secret offshore accounts and agreed to pay about $2.6 billion.

The Justice Department said it was the largest penalty imposed in any criminal tax case. Credit Suisee is the largest bank to plead guilty in more than 20 years.

The settlement resolves a years-long criminal investigation into allegations that Credit Suisse, Switzerland's second-largest bank, recruited US clients to open Swiss accounts, helped them conceal the accounts from the Internal Revenue Service (IRS), and enabled misconduct by bank employees. The case is part of an Obama administration crackdown on foreign banks believed to be helping US taxpayers hide assets.

Officials said a criminal charge was necessary to account for the bank's pattern of misconduct, which included a lack of cooperation and document destruction. But the deal was structured in such a way as to allow the bank to continue operating.

The penalties will be paid to the Justice Department, the Federal Reserve and the New York State Department of Financial Services.

Attorney General Eric Holder, criticized last year after telling Congress that large banks had become hard to prosecute, appeared to foreshadow the guilty plea in a video message earlier this month in which he said no financial institution was "too big to jail."

"A company's profitability or market share can never and will never be used as a shield from prosecution or penalty," Holder told a news conference. "And this action should put that misguided notion definitively to rest."

As part of its plea agreement, Credit Suisse acknowledged that it helped clients use sham entities to hide undeclared accounts; destroyed bank records and concealed transactions involving undeclared accounts.

The case was filed in federal court in suburban Alexandria, Virginia, where eight former Credit Suisseemployees have been charged. Two have pleaded guilty.

Credit Suisse chief executive Brady Dougan, who has said previously that senior executives at the bank were not aware that some bankers were helping US customers evade taxes, said in a statement Monday that the bank regrets its past "misconduct" and was looking to resolve the matter and move forward.

The bank has said it stopped providing private banking services outside the US to Americans several years ago.

The criminal case follows a Senate subcommittee investigation that found the bank provided accounts in Switzerland for more than 22,000 US clients totaling $10 billion to $12 billion. The report said Credit Suissesent Swiss bankers to recruit American clients at golf tournaments and other events, encouraged US customers to travel to Switzerland and actively helped them hide their assets. In one instance, a Credit Suissebanker handed a customer bank statements hidden in a Sports Illustrated magazine during a breakfast meeting in the United States.

The administration's action against Credit Suisse, a banking fixture on Wall Street, comes amid public outrage that boiled over from the financial crisis that plunged the economy into the deepest recession since the Great Depression of the 1930s. Calls for holding big Wall Street banks accountable, and sending top executives to jail, have come from consumer advocates, lawmakers and others, putting the Justice Department on the defensive.

The Justice Department's highest-profile settlement over sales of risky mortgage securities in the run-up to the financial crisis — the $13 billion deal among the department, state regulators and JPMorgan Chase — was a civil case, and no bank executives were charged. Federal prosecutors in California have been conducting a related criminal investigation.

The Credit Suisse case is part of a broader crackdown on foreign banks believed to be helping US taxpayers hide assets. In 2009, Switzerland's largest bank, UBS, entered a deferred prosecution agreement with the Justice Department in which it agreed to pay $780 million in fines and turn over the names of thousands of customers suspected of evading US taxes. The country's oldest bank, Wegelin & Co., pleaded guilty in January 2013 to US tax charges, admitting that it helped American clients hide more than $1.2 billion from the IRS.

The case against Credit Suisse was intended in part to combat criticism that the US government has not been aggressive enough in its pursuit of banks. A report from the Senate Permanent Subcommittee on Investigations accused the Justice Department of lax enforcement and faulted the government for gleaning only 238 names of US citizens with secret accounts at Credit Suisse, or just 1 percent of the estimated total.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Sen. Carl Levin, who heads the subcommittee that conducted the investigation, said he was disappointed that the bank would not provide names of US clients with secret Swiss bank accounts. But Justice Department officials said they were satisfied with the overall amount of customer information that the bank would be providing as part of the deal.

Copyright 2014 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.