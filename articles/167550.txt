MacBook Air: The lightweight notebooks have just had a processor upgrade for greater speed, efficiency and potentially, battery life. — AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SAN FRANCISCO, April 29 — Apple has very quietly and without any fuss upgraded the processors across its range of ultra-lightweight notebooks and has also lowered their prices.

All of the other specifications remain the same — 4GB of RAM as standard, Intel HD 500 graphics and a choice of 128GB or 256GB of solid state flash memory and of an 11-inch or 13-inch non-Retina display.

However, where last year’s models, launched in June, offered a 1.3GHz Intel processor, the 2014 MacBook Airs get the latest 1.4GHz dual-core Intel Core i5 processor.

This might not be enough to get some consumers excited, but the upgrade means that the computers now have a turbo boost speed (for when serious mobile processing and multi-tasking is called for) of 2.7GHz (up from 2.6GHz) and in all likelihood an even better battery life as the chips are even more efficient than last year’s.

When the 2013 MacBook Air range received its new chip set and the latest version of OSX (Apple’s computer operating system), the result was an 11-inch notebook capable of running for nine hours and a 13-inch notebook capable of lasting 12 hours before needing to be plugged back into the mains to charge the battery.

Apple doesn’t upgrade components for the sake of it, so expect real-world-use battery life to be even better still.

The updated MacBook Air notebooks are available now either via Apple’s websites or at the Apple store starting at a US$100/£100/€100 (RM325/548/451) lower price of US$899 for the 11-inch 128GB version. — AFP-Relaxnews