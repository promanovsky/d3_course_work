Sorry, Kim Kardashian. But Sarah Thislethwaite had a better Mother's Day than you did.

The Akron, Ohio resident gave birth via C-section on Friday to a pair of monoamniotic (or "mono mono") twins, meaning the babies shared the same amniotic sac and placenta during their time in the womb.

When they finally entered the world, Jillian and Jenna were separated for a minute or so - but then held hands upon being placed side by side, a breathtaking moment actually captured on camera and posted to Twitter:

At first, the twins were having trouble breathing on their own and were placed on ventilators.

But they were finally able to be held by Mom yesterday, resulting in "the best Mother's Day present ever," as Sarah told NBC News.

"It's just hard to put into words how amazing it feels to know the girls are OK," Thislethwaite said. "It's great to know that they're doing so well..."

"They're already best friends. That's amazing."

The photo above went viral over the weekend and reminded many of the time twins in Spain held hands inside an incubator soon after leaving the fetus.

Relive that emotional story here: