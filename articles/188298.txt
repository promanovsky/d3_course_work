Alibaba’s U.S. initial public offering is probably the most highly anticipated one this year, and we finally have more official details. Yahoo! Inc. (NASDAQ:YHOO) holds a $26 billion stake or 523.6 million shares of Alibaba, so the better Alibaba has done, the higher Yahoo stock has been driven. However, as part of the IPO, Yahoo will have to sell 208 million of its shares in the IPO. Currently, Yahoo owns 22.6% of the Chinese online retailer.

Alibaba addresses mobile shift

In a report dated May 7, 2014, Bernstein analysts Carlos Kirjner and Peter Paskhaver provided early thoughts on Alibaba’s IPO filing. They note that the Chinese online retailer is already facing headwinds from mobile, although they are already partially “in the numbers.” Also a mix shift to TMall is offsetting that mobile headwind. Alababa did say it monetizes gross merchandise volume for mobile at a lower rate than on the PC. They believe this is because of “the revenue portion driven by advertising.” They also note that TMall has been growing more quickly and that it has a “non-advertising-driven take-rate.”

Based on the numbers Alibaba provided, the Bernstein team said the mix shift toward TMall seems to have driven a year over year increase of 3.8% in the average effective take rate, in spite of the mobile headwind the company faces.

Mobile’s growing fast

They note that in only about two years, mobile gross merchandise volume rose to almost 20% of the total.

Many see Alibaba’s dominant positioning in the Chinese online market is being threatened by competitor Tencent Holdings Ltd. (HKG:700) (ADR) (OTCMKTS:TCEHY), but not so fast, they say. They believe that this rapid shift to mobile indicate that the threat is exaggerated.

In addition they believe Alibaba will continue seeing fast growth in mobile gross merchandise volume, along with the associated monetization headwind. They don’t see mobile couldn’t pass 50% or 60% of the total volume over the next few years. They also note China’s rapid retail revenue growth overall, which rose 65.5% on 59.4% in gross merchandise volume during the last nine months of 2013.

In addition, they believe that the shift to mobile is a net plus for Alibaba’s China retail business. They suspect that the impact of the growth in mobile will offset headwinds in monetization by positively impacting active buyer growth, which rose 44% year over year in the fourth quarter of last year, and gross merchandise volume per buyer, which rose 8% in the same time frame.

Alibaba IPO a negative for Yahoo

The Bernstein analysts say one good thing for Alibaba itself is that its margins will expand by about 150 basis points because it will stop paying royalties to Yahoo! Inc. (NASDAQ:YHOO). Currently the Chinese online retailer is paying 1.5% of consolidated revenues in royalties to Yahoo. With the IPO, that revenue stream comes to a screeching halt. They estimate that the company has paid about $120 million to Yahoo in the last 12 months.