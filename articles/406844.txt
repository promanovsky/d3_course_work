Tesla Motors, Inc. (TSLA) reported a second-quarter net loss of $61.90 million or $0.50 per share, compared to a loss of $30.50 million or $0.26 per share, a year ago. Non-GAAP net income was $16.13 million or $0.11 per share, for the quarter.

On average, 13 analysts polled by Thomson Reuters expected the company to report profit per share of $0.04 for the quarter. Analysts' estimates typically exclude special items.

Total revenues rose to $769.35 million from $405.14 million, prior year. Non-GAAP revenue increased to $857.51 million from $551.95 million. Analysts expected revenue of $810.61 million for the quarter.

Tesla Motors plans to produce about 9,000 cars in the third quarter. The company expects non-GAAP automotive gross margin in the third quarter to be about consistent with the prior quarter. Despite a higher count of leased vehicles, investments in R&D, and geographic expansion, Tesla expects to be marginally profitable in the third quarter on a non-GAAP basis.

Tesla plans to invest between $750 million and $950 million in 2014, an increase of $100 million from the company's prior guidance.

In a separate announcement, Panasonic Corporation (PCRFY.PK, PCRFF.PK) and Tesla Motors, Inc. announced they have signed an agreement that lays out their cooperation on the construction of a large-scale battery manufacturing plant in the United States, known as the Gigafactory. According to the agreement, Tesla will prepare, provide and manage the land, buildings and utilities. Panasonic will manufacture and supply cylindrical lithium-ion cells and invest in the associated equipment, machinery, and other manufacturing tools based on their mutual approval.

The Gigafactory will produce cells, modules and packs for Tesla's electric vehicles and for the stationary storage market. The Gigafactory is planned to produce 35GWh of cells and 50GWh of packs per year by 2020. Tesla projects that the Gigafactory will employ about 6,500 people by 2020.

For comments and feedback contact: editorial@rttnews.com

Business News