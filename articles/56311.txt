Washington - President Barack Obama on Friday said Russia's troop buildup on the Ukraine border was out of the ordinary and called on Moscow to pull its military back and begin talks to defuse tensions.

“You've seen a range of troops massing along that border under the guise of military exercises,” he told CBS This Morning in an interview in Vatican City. “But these are not what Russia would normally be doing.”

Obama said the moves might be no more than an effort to intimidate Ukraine, but also could be a precursor to other actions.

“It may be that they've got additional plans,” he said. - Reuters