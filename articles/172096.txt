LEVITTOWN, N.Y. (CBSNewYork) — A Long Island girl is one step closer to having her art on the Google homepage.

Audrey Zhang, a fifth grader from Island Trees Memorial Middle School in Levittown, has been chosen as the winner for New York in the 7th Annual Doodle 4 Google competition. Her doodle was selected from over 100,000 entries.