File photo of RBI Governor Raghuram Rajan

To improve risk-mitigating environment in the country, the Reserve Bank of India (RBI) has allowed overseas investors to hedge their currency exposure.

In order to enhance hedging facilities for foreign investors in debt instruments, the RBI in its first bi-monthly policy proposed to allow them to hedge the coupon receipts falling due during the next 12 months.

"This is determined by improving the hedging environment for foreign investors in India. It is not necessarily to combat the NDF market but primarily to improve the (hedging) environment in the country," RBI Governor Raghuram Rajan told reporters at the customary post policy meeting with the media.

The apex bank also said it is in final discussion with market regulator Securities and Exchange Board of India (Sebi) to finalise the modalities for allowing foreign institutional investors (FIIs) to hedge their currency risk by using exchange-traded currency futures in the domestic exchanges.

The RBI also proposed to allow all resident individuals, firms and companies with actual foreign exchange exposures to book foreign exchange derivative contracts up to $2,50,000 on declaration, subject to certain conditions.

"The Reserve Bank will continue to work to ease entry while reducing risk to foreign investors from the volatility of flows," it said in the policy.

While answering to media queries, Mr Rajan said the Reserve Bank's effort is to avoid excess volatility in the forex market and not to achieve a level for the rupee.

"I don't think we are trying to establish any level (of rupee) in the market. What we do is try and attempt to keep the exchange rate from becoming overly volatile and it will continue to be our policy," Mr Rajan said.

He said the RBI has taken into account expansion in foreign currency assets while determining the overall liquidity expansion in the market.

"Remember that over the past few years there was very little expansion in the foreign currency assets so most of the expansion came through net domestic assets. Now, I think there will be a more balanced expansion."