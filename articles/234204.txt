Are you a frequent air traveller? Watch out for that armrest!



Disease-causing bacteria can live for up to a week on airplane seats, tray tables, armrests, and other surfaces, scientists have found.

Bacteria such as MRSA and E coli can linger on surfaces commonly found in airplane cabins for days, even up to a week, researchers said.

"Many air travellers are concerned about the risks of catching a disease from other passengers given the long time spent in crowded air cabins," said Kiril Vaglenov, of Auburn University.

"This report describes the results of our first step in investigating this potential problem," Vaglenov said.

In order for disease-causing bacteria to be transmitted from a cabin surface to a person, it must survive the environmental conditions in the airplane.

In the study Vaglenov and his colleagues tested the ability of two pathogens, methicillin-resistant Staphylococcus aureus (MRSA) and E coli to survive on surfaces commonly found in airplanes.

They obtained six different types of material from a major airline carrier (armrest, plastic tray table, metal toilet button, window shade, seat pocket cloth, and leather), inoculated them with the bacteria and exposed them to typical airplane conditions.

MRSA lasted longest (168 hours) on material from the seat-back pocket while E coli survived longest (96 hours) on the material from the armrest.

"Our data show that both of these bacteria can survive for days on the selected types of surfaces independent of the type of simulated body fluid present, and those pose a risk of transmission via skin contact," said Vaglenov.

"Our future plans include the exploration of effective cleaning and disinfection strategies, as well as testing surfaces that have natural antimicrobial properties to determine whether these surfaces help reduce the persistence of disease-causing bacteria in the passenger aircraft cabin," said Vaglenov.

Researchers are currently conducting trials with other human pathogens including the bacteria that cause tuberculosis.

The research was presented at the annual meeting of the American Society for Microbiology.