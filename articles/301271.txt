Food companies and restaurants could soon face government pressure to make their foods less salty — a long-awaited federal effort to try to prevent thousands of deaths each year from heart disease and stroke.

The U.S. Food and Drug Administration is preparing voluntary guidelines asking the food industry to lower sodium levels, FDA Commissioner Margaret Hamburg told The Associated Press. Hamburg said in a recent interview that the sodium is "of huge interest and concern" to the agency.

Most people take in an average of 3,400 milligrams of sodium per day. (Wilfredo Lee, J. Pat Carter/Associated Press)

"We believe we can make a big impact working with the industry to bring sodium levels down, because the current level of consumption really is higher than it should be for health," Hamburg said.

It's still unclear when FDA will release the guidelines, despite its 2013 goal to have them completed this year.

Hamburg said she hoped the agency would be able to publicly discuss the issue "relatively soon." On Tuesday, FDA spokeswoman Erica Jefferson said there is no set timeline for their release.

The food industry has already made some reductions, and has prepared for government action since a 2010 Institute of Medicine report said companies had not made enough progress on making foods less salty. The IOM advised the government to establish maximum sodium levels for different foods, though the FDA said then — and maintains now — that it favours a voluntary route.

Americans eat about 1 ½ teaspoons of salt daily, about a third more than the government recommends for good health and enough to increase the risk of high blood pressure, strokes and other problems. Most of that sodium is hidden inside common processed foods and restaurant meals.

In addition to flavour, companies use sodium to increase shelf life, prevent the growth of bacteria, or improve texture and appearance. That makes it more difficult to remove from some products, Hamburg noted.

Once the guidelines are issued, Americans won't notice an immediate taste difference in higher-sodium foods like pizza, pasta, bread and soups. The idea would be to encourage gradual change so consumers' taste buds can adjust, and to give the companies time to develop lower-sodium foods.

"I think one of the things we are very mindful of is that we need to have a realistic timeline," Hamburg said.

Health groups would prefer mandatory standards, but say voluntary guidelines are a good first step.

Still, Michael Jacobson of the U.S. Center for Science in the Public Interest says he is concerned companies may hesitate, worried that their competitors won't lower sodium in their products.

If that happens, "then FDA should start a process of mandatory limits," Jacobson says.

That's what companies are worried about. Though the limits would be voluntary, the FDA is at heart a regulatory agency, and the guidelines would be interpreted as a stern warning.

Brian Kennedy of the Grocery Manufacturers Association, which represents the country's biggest food companies, says the group is concerned about the FDA setting targets and any guidelines should be based on a "rigorous assessment of all available scientific evidence."

Move to lower sodium options

The food industry has pointed to a separate 2013 IOM report that said there is no good evidence that eating sodium at very low levels — below the 2,300 milligrams a day that the U.S. government recommends — offers benefits.

The government recommends that those older than 50, African-Americans and people with high blood pressure, diabetes or chronic kidney disease eat 1,500 milligrams a day. The American Heart Association recommends that everyone eat no more than 1,500 milligrams a day.

Lanie Friedman of ConAgra Foods, one of the companies that would be subject to the voluntary guidelines, says the newer IOM report is a "paradigm change" and more research is needed. But those pushing for sodium limits say it's pointless to debate how low the recommendations should go — Americans are still eating around 3,400 milligrams a day.

Many food companies and retailers already have pushed to reduce salt. Wal-Mart pledged to reduce sodium in many items by 25 per cent by next year, and food giant ConAgra Foods says it made a 20 per cent reduction. Subway restaurants said it has made a 30 per cent reduction restaurant-wide.

The companies say that in some cases, just removing added salt or switching ingredients does the trick. Potassium chloride can also substitute for common salt (sodium chloride), though too much can cause a metallic taste.

Levels of sodium in food can vary widely. According to the U.S. Centers for Disease Control and Prevention, sodium in a slice of white bread ranges from 80 milligrams to 230 milligrams. Three ounces of turkey deli meat can have 450 milligrams to 1,050 milligrams.

Those ranges give health advocates hope.

"Those differences say to me that the companies that make the highest-sodium products could certainly reduce levels to the same as the companies that make the lower-sodium products," Jacobson says.

Still, the guidelines could be a hard sell. In recent years, congressional Republicans have fought the Obama administration over efforts to require calorie labels on menus and make school lunches healthier. When the administration attempted to create voluntary guidelines for advertising junk food for children, the industry balked and Republicans in Congress fought the idea, prompting the administration to put them aside.

Other members of Congress are pushing the agency to act.

"As the clock ticks, America's blood pressure, along with health costs due to chronic disease, continues to rise," says Sen. Tom Harkin, chairman of the Senate committee that oversees the FDA.

Last year, Canadian specialists suggested people reduce sodium intake to less than 2 grams a day or 2,000 milligrams to lower blood pressure in the population. People with hypertension should continue to follow the advice of their health-care providers.

Most people take in an average of 3,400 milligrams of sodium per day — more than twice the previous recommended level of 1,500 milligrams per day for people nine to 70 years old — according to Health Canada.