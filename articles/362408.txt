Box Office: 'Transformers 4' Tops Long Holiday With $53.8M; 'Tammy' No. 2 With $32.9M

UPDATED: Melissa McCarthy's latest comedy opened lower than her recent movies; new films "Earth to Echo" and "Deliver Us From Evil" also struggle, while Dinesh D'Souza's "America" soared more than 60 percent on July Fourth itself.

Paramount's holdover Transformers: Age of Extinction ruled the lackluster Fourth of July box office, topping the Wednesday-Sunday holiday stretch with $53.8 million for a domestic total of $174.7 million. That included $36.4 million for the three days, a 63 percent decline from opening weekend.

Overseas, Age of Extinction remained a juggernaut, earning $95.8 million from 37 territories and crossing the $400 million mark in just 12 days for a worldwide total of $575.6 million (the tentpole hasn't opened in most of Europe and Latin America because of the World Cup).

Back in North America, the pack of new films opening over the patriotic holiday — led by Melissa McCarthy's R-rated new comedy Tammy — all did less than expected after launching on Wednesday. Revenue for the holiday frame was the lowest in recent memory, including being down a steep 44 percent from last year.

Hurricane Arthur probably didn't help matters. On Saturday, moviegoing was down notably in many East Coast markets as people stayed outside after being cooped up Friday because of rain.

Tammy easily came in No. 2, posting a Wednesday-Sunday debut of $32.9 million, including $21.2 million for the weekend. The movie, earning scathing notices and a C+ CinemaScore from audiences, came in well behind the $39 million debut of The Heat last year, although that film co-starred Sandra Bullock. It also came in behind McCarthy's Identity Thief ($34.6 million).

LIST Hollywood's 100 Favorite Films

Tammy, marking the feature directorial debut of McCarthy's husband, Ben Falcone, cost New Line and Warner Bros. $20 million to make, a relatively modest number for a studio comedy.

"There's nothing wrong here," said Warner Bros. distribution chief Dan Fellman. "It's true that tracking was more favorable for us going into the weekend, but this was a $20 million movie. This is a movie Melissa did by herself with her husband. It doesn't have a big supporting star like Sandy."

Among the other new holiday films this year, horror film Deliver Us From Evil opened to roughly $15 million for the five days, versus the expected $20 million. For the weekend, it took in $9.5 million, putting it in a dead heat with fellow Sony title 22 Jump Street for the No. 3 spot.

STORY Where Are All the $100 Million-Plus Summer Openings?

Marking the summer's first studio horror film, the R-rated movie follows a New York cop (Eric Bana) who teams with a renegade priest (Edgar Ramirez) schooled in exorcisms to eradicate a series of possessions striking New York City. The movie, directed by Scott Derrickson and earning a B+ CinemaScore, is inspired by the book co-written by real-life cop Ralph Sarchie. Deliver Us From Evil cost $30 million to produce.

Earth to Echo, earning an A- CinemaScore, posted a tepid five-day debut of $13.5 million. For the weekend itself, it was beat out by family holdover How to Train Your Dragon 2 ($8.8 million versus $8.2 million). Earth to Echo placed No. 6.

Disney made the found-footage film about a tiny alien robot but put it into turnaround last summer. Relativity points out that it only paid $13 million to acquire the movie and do reshoots.

STORY In the Wrong Theater? Blame Summer's Confusing Movie Titles

Conservative author and filmmaker Dinesh D'Souza's new documentary America, which expanded nationwide Wednesday, grossed $4 million for the five days from 1,105 theaters. That's a modest footprint, compared to the $3,000-plus location count for the other new films.

America, placing No. 11 for the weekend, didn't match the $6.5 million nationwide launch of D'Souza's hit documentary 2016: Obama's America two years ago, but soared more than 60 percent on July Fourth after an intensive marketing push tied to the holiday. On Saturday, America was the only film in the top 20 that was down, however.

STORY Why Dinesh D'Souza's 'America' Features Clips of Matt Damon, Woody Harrelson

Lionsgate is distributing America, which debunks the narrative that the United States has been a force of evil across the world through a combination of historical re-creations and interviews with some of the country's harshest critics, including former Weather Underground radical Bill Ayers.

John Carney's specialty film Begin Again performed nicely for The Weinstein Co. after expanding into 175 theaters Wednesday, grossing $1.7 million for the five days for a domestic total of $1.9 million. The movie, starring Keira Knightley and Mark Ruffalo, posted one of the best location averages of the holiday stretch.

TWC divison Radius also saw nice results with Snowpiercer. The Korean film took in $1.3 million as it expanded into a total of 250 theaters for a total $1.5 million. In an aggressive digital push, Radius will truncate the movie's theatrical run this coming weekend in order to make it available on VOD.

Robert Ebert documentary Life Itself opened in 23 theaters in select cities, grossing $138,000 for a location average of $6,000. Magnolia is also making the film available on VOD.

Here are the top 10 estimates for the weekend of July 4-6 at the domestic box office:

Title, Weeks in Release/Theater Count, Studio, Friday Total, Percentage Drop, Cume

1. Transformers: Age of Extinction, 2/4,233, Paramount, $36.4 million, -64%, $174.6 million.

2. Tammy, 1/3,465, Warner Bros./New Line, $21.2 million, $32.9 million.

3. Deliver Us From Evil, 1/3,049, Sony, $9.5 million, $15 million.

4. 22 Jump Street, 4/3,324, Sony/MGM, $9.4 million, -41%, $158.9 million.

5. How to Train Your Dragon 2, 4/3,297, Fox/DWA, $8.8 million, -34%, $140 million.

6. Earth to Echo, 1/3,230, Relativity, $8.2 million, $13.5 million.

7. Maleficent, 6/2,389, Disney, $6.1 million, -27%, $213.9 million.

8. Jersey Boys, 3/2,630, Warner Bros., $5.2 million, -33%, $36.7 million.

9. Think Like a Man Too, 3/1,729, Sony/Screen Gems, $4.9 million, -53%, $57.2 million.

10. Edge of Tomorrow, 5/1,538, Warner Bros./Village Roadshow, $3.6 million, -33%, $90.9 million.