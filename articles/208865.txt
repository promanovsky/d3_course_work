Since Nokia's (NYSE:NOK) X and X+ smartphones were released recently, there have been more questions than answers on why the company would use Google's (NASDAQ:GOOG) Android operating system instead of the Windows Phone OS. Also, with Microsoft (NASDAQ:MSFT) buying Nokia, can "Microkia" compete on Android's turf?

Why Android?

There are three main types of operating systems in the smartphone market: Apple's iOS, Google's Android, and Windows Phone OS, which trails far behind in market share. According to IDC Worldwide Mobile Phone Tracker, Google's Android tops the global chart with 80%. Apple's iOS is far behind with 15%, followed by Windows Phone at 3%.

Users of Android phones may claim that the many available apps and features endeared Android phones to them. But analysts believe that what clinches things for Android is that it is the most flexible, the least expensive, and the most appealing to developers. For developers, the appeal of an operating system for smartphones is measured by the ease with which they can create and add apps to the system.

Developers are a major driver of devices in the smartphone market. They create apps, software, and other programs that make smartphones more appealing to customers. When you look at all the advantages offered by Android, it becomes less difficult to imagine why Nokia would create a phone that runs on Android Open Source Project, or AOSP.

(NASDAQ:MSFT)

The version of Android in Nokia X is the Android Open Source software without Google apps, but with Microsoft products such as Outlook, Skype, Bing, Microsoft search, mail, online storage, streaming music, and other cloud services. This offers Microsoft a platform for its apps on Android phones. In addition, any apps running on Nokia X would communicate with Microsoft's cloud and not Google's. These are some plausible reasons why Microsoft was unconcerned regarding Nokia X phones.

Nokia X vs. a $100 Google Nexus: Who wins?

Rumor has it that a less than $100 Google Nexus phone is being devised in conjunction with MediaTek. Should that become a reality, it will result in a clash of the titans. At the rate at which Nokia X and Nokia X+ are gaining acceptance, the $100 Nexus may be unable to provide a significant impact for Google when it is eventually unveiled to consumers. Momentum is really on the side of Microsoft. According to IDC, Windows Phone is the fastest-growing OS. From 2011 to 2013, sales of Windows Phones recorded 90% year-over-year growth.

With the completion of the $7.2 billion purchase of Nokia's phone division last Friday, perhaps Microsoft can now target part of its licensing money to hasten the sales of its Windows Phones. It can also try the contra-revenue tactics similar to Intel to establish the Nokia X brands in the marketplace, such as its Lumia brand of smartphones.

If the sub-$100 Google Nexus is all Google has to inhibit Microsoft's move, then the company is in for a big surprise. Just like Lumia, Nokia X is rapidly gaining global acceptance with pre-orders in China reportedly approaching 4 million. In addition, the emerging markets like India, Russia, Africa, and China still adore the Nokia brand. What that means is that shareholders of Microsoft can expect improved financials and, maybe, increased dividend payouts in the coming quarters.

Conclusion

At the moment, Microsoft plans to focus on growing its cloud businesses and Office 365. Now that Nokia's handset and services section has become a part of Microsoft, the software giant plans to target the affordable smartphone market. The company estimates this to be a "$50 billion annual opportunity,'' according to CEO Satya Nadella. Microsoft is poised to be a market leader by continuing with its uptrend in earnings and profit margins. It is a stock for forward-thinking value investors who are ready to invest for the long term.