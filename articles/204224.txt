Europe faces an ugly currency dilemma over unrest in Ukraine and the likelihood of further on-off tension with Russia. Currency flight from East to West is likely to continue — and it will head into the euro, not the dollar, for fear of American sanctions against Russian and Ukrainian owners of greenbacks.

That’s one of the reasons why the reluctant European Central Bank’s action to cut interest rates, signaled last week by Mario Draghi as potentially coming next month, will probably have only a limited effect for the time being in lowering the value of the euro EURUSD, -0.12% .

Europe’s Week Ahead: Signs of European Growth?

The strong showing of pro-Russia separatists in ramshackle unofficial referendums in eastern Ukraine over the weekend, shortly after President Vladimir Putin’s triumphal visit to Crimea, may give Moscow an excuse for more saber-rattling.

Longer term, the euro, like sterling GBPUSD, +0.29% , looks overvalued against the dollar. Investors should get ready for a euro decline.

This would have the dual positive effect of stoking up inflation toward the 2% level favored by the ECB (although it’s difficult to convince German voters that they should support a central bank that backs higher inflation) and promoting the competitiveness of peripheral euro members as they struggle to raise growth rates.

It’s impossible to tell when the currency markets may turn round. But as U.S. tapering proceeds and the time of higher U.S. interest rates draws nearer, the foreign exchanges may become more volatile.

The ECB’s painfully slow progress towards interest-rate easing is proving especially irksome for France’s Socialist government. A cut to zero interest rates in the ECB’s headline lending rate to banks, and negative rates on bank deposits, would be a significant symbolic step, underlining just how much is still wrong with Europe’s slow path to recovery.

One problem is the unsettled state of relations between Germany and France.

President François Hollande has now joined a string of successive French heads of state, extending back to Valéry Giscard d’Estaing in the 1970s, at odds with Germany at different times over exchange-rate policy. Hollande, his new Prime Minister Manuel Valls and Finance Minister Michael Sapin are becoming increasingly outspoken in calls for steps to weaken the euro.

Draghi’s statement at his monthly press conference on Thursday that the strong euro was “a cause for serious concern” given low inflation and low growth will not have done much to appease Paris, considering that the euro is still trading around $1.38.

But on interest rates and other possible easing action, Draghi said that the “governing council is comfortable with acting next time,” adding there was consensus within the ECB council over “dissatisfaction about the projected path of inflation.”

The ECB president made the point last week that the firmer euro, working through to lower food and energy prices, was a strong factor behind the decline in euro area inflation from 2.7% in the first quarter of 2012 to only 0.7% now. Of the two percentage points difference, 80% was due to cheaper energy and food, Draghi said. But the effects on exports have been less benign — of particular concern to France.

Germany continues to run a large trade surplus, while France, which in March 2014 registered its 120th consecutive monthly trade deficit, is registering a large shortfall. German exports remain roughly at their 2012 post-crisis peak, whereas French exports have fallen around 5% since then.

Disheartened by signs that France is still losing ground in the competitiveness struggle, French ministers have insisted in recent weeks that the euro’s political leaders should take responsibility for the euro exchange-rate issue — contrasting with German insistence that this is the task of the central bank.

Valls said Hollande would take up the issue of the over-strong euro with fellow leaders after the European elections at the end of the month. “The ECB is responsible, but it is not the only one,” Sapin said this month, affirming that the EU’s council of ministers should discuss the issue. Arnaud Montebourg, the firebrand economy and industry minister, raised the matter in Parliament on Wednesday, stating that exchange-rate policy was a matter for the political authorities.

Hollande faces a drubbing in the European elections later this month. He will want concessions from Germany to ease his plight. Yet many in Germany are already highly uncomfortable at the specter of negative interest rates at the ECB.

Signs of Germany accommodating France’s wishes for further measures to bring down the high euro are highly unlikely — especially since the single currency is likely to retreat anyway from recent highs, sooner or later, as a result of more fundamental factors.

Also see these must-read stories:

Pfizer, AstraZeneca and the pound — a heady combination

Why Draghi believes the Fed’s way is the best way