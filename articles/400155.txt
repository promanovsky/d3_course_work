For the time being, Apple’s iTunes Radio product is something of an also-ran. It’s a nice value-add for iOS device owners, allowing them to stream Pandora-like Radio stations for free, but it doesn’t really offer any compelling features compared to bigger rival services. According to a new report, however, that may soon change.

Re/code on Monday reported that Apple is close to acquiring Swell for about $30 million. Swell, which you might recall we have covered here on BGR before, is something of a Pandora for online news radio. The app includes many sources from podcasts to news radio stations, and it chooses which content to stream in much the same way that Pandora selects music.

While the deal would be extremely small compared to other recent Apple acquisitions, such as its $3.2 billion Beats Electronics buy, it could be a big boost for iTunes Radio, giving it some interesting and useful functionality that no major competitors currently tout.

According to the report, the Swell app will be shut down at some point this week as part of the deal.