Movie

Fisher stepped out for an an auction event in California on Wednesday, May 14, a day before the J.J. Abrams-directed film kicked off production in the Abu Dhabi desert.

May 16, 2014

AceShowbiz - Carrie Fisher has revealed her impressive physical transformation for her role in "Star Wars Episode 7". The actress, who is set to reprise her role as Princess Leia in the upcoming movie directed by J.J. Abrams, stepped out on Wednesday, May 14 as she attended The Final Auction Preview Night by Profiles in History in North Hollywood, California.

The 57-year-old looked fit, donning a long sleeved dark black and red dress with matching heels. She was joined by her 82-year-old mother Debbie Reynolds, who looked fresh in a blue pantsuit and strappy sandals.

Reynolds told Extra earlier this month about her daughter's diet in preparation for the filming of the new "Star Wars" movie. "Carrie's very excited about it," she said. "She's been on a diet ever since, because they have to be up to par, so she looks terrific. She's very excited, as we all are, because we all love 'Star Wars'."

While Fisher was still in the States, crew for the sci-fi flick has flown to Middle East to begin the principal photography in the Abu Dhabi desert. Bad Robot has just updated its Twitter account with a behind-the-scenes picture of a clapperboard to mark the first day of production.

Besides Fisher, original actors Harrison Ford Mark Hamill, Anthony Daniels, and Kenny Baker are set to return to "Star Wars Episode 7". Added to the cast are John Boyega, Daisy Ridley, Adam Driver, Oscar Isaac, Andy Serkis and Domhnall Gleeson.

Plot details are still kept under wraps. Abrams and Lawrence Kasdan polish the script drafted by Michael Arndt. The movie is set to open in theaters across the U.S. on December 18, 2015.