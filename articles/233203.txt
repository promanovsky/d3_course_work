Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

Deep breaths, everyone. JJ Abrams has unveiled our first look at the Star Wars 7 set in a new video.

The director begins by telling fans that he is “on the set of Stars Wars Episode VII in Abu Dhabi.”

No biggie then, JJ, apart from the fact you have just verified that you are indeed filming in the United Arab Emirates after weeks of speculation.

This also confirms that these pictures recently taken were indeed of the Star Wars 7 set, amid news that a ‘whole world’ is being built on one of the salt lakes.

What’s more, we discover more details about filming and get to meet some of the characters that look set to appear in the latest addition to the sci-fi francise.

(Image: Mona Al Marzooqi/The National)

The desert setting is likely to confirm for many that at least some of the action will take place on Luke Skywalker’s home planet, Tatooine.

The video shows a ramshackle market-style set, complete with street stalls and actors dressed up as traders.

We also see a new (but very old-looking) ostrich-like alien carrying a box of space-turkeys saunter past camera.

However, while filming is currently based in Abu Dhabi, Abrams has confirmed that the majority of the movie will shoot at London’s Pinewood Studios.

(Image: Mona Al Marzooqi/The National)

The video is in aid of Disney and Lucasfilm’s new project with Bad Robot, Star Wars: Force for Change.

To raise money for the campaign, which will raise funds and awareness for UNICEF, fans are being given the chance to appear in the new film.

Yes, you did read that right. By contributing at least $10 (£5.92) at Omaze.com/StarWars you will be entered into a draw to appear on camera in the new movie.

The prize includes the opportunity to meet members of the cast and be transformed by makeup and costume teams into a Star Wars character.

(Image: Twitter/@bad_robot)

"The Star Wars fans are some of the most passionate and committed folks around the globe,” says director J.J. Abrams.

“We’re thrilled to offer a chance to come behind the scenes as our VIP guests and be in Star Wars: Episode VII.

“We’re even more excited that by participating in this campaign, Star Wars fans will be helping children around the world through our collaboration with UNICEF Innovation Labs and projects."

For everything else you need to know about the new movie, head here.

* Star Wars: Episode VII will be released on 18 December 2015