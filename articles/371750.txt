After moving mostly lower over the course of the previous session, stocks may see some further downside in early trading on Tuesday. The major index futures are currently pointing to a modestly lower open for the , with the Dow futures down by 27 points.

The downward momentum for the markets may partly reflect trepidation ahead of the start of earnings season, as Alcoa (AA) is scheduled to release its second quarter results after the close of trading.

While Alcoa was dropped from the list of Dow components last year, the release of quarterly results from the aluminum giant is still seen as the unofficial start of earnings season.

The consensus estimates call for Alcoa to report earnings of $0.12 per share on revenues of $5.66 billion. A year-ago, the company earned $0.07 per share on revenues of $5.85 billion.

Wells Fargo (WFC), Family Dollar (FDO), and WD-40 (WDFC) are also due to release their quarterly results later in the week, although the reporting season won't really start to pick up steam until next week.

In the afternoon, trading could be impacted by speeches by a pair of Federal Reserve officials, with traders likely to analyze the remarks for indications regarding the outlook for monetary policy.

Richmond Fed President Jeffrey Lacker is scheduled to deliver a speech on the economic outlook at 1 pm ET, while Minneapolis Fed President Narayana Kocherlakota is due to deliver a speech on monetary policy and the at 1:45 pm ET.

The Fed is also scheduled to release a report on consumer credit in the month of May at 3 pm ET, although the data typically does not move the markets.

For comments and feedback contact: editorial@rttnews.com

Market Analysis