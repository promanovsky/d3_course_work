Could POLAR BEARS help cure obesity? DNA may hold secret to boosting metabolism and preventing heart disease



The animal's DNA is uniquely evolved to cope with a high fat diet

They have mutated genes involved in fatty acid metabolism

Explains how they eat a high fat diet without suffering clogged arteries

This discovery could help scientists find new ways to treat human obesity



Polar bears may hold the answer to the obesity crisis in their genes, new research has shown.



A study of the animal’s DNA revealed they are uniquely evolved to cope with a high fat diet that would prove disastrous to a human.



The bears prey on blubber-rich seals, and also scavenge the fatty carcasses of whales.



Polar bears could hold the key to the obesity crisis in their genes, new research suggests

Half their body weight consists of fat and their cholesterol levels are sky high, yet they are untroubled by heart disease.



Now scientists believe they know the polar bear’s secret - several mutated genes involved in fatty acid metabolism and cardiovascular function.



The genes appear to be crucial to the bears’ adaptation to extreme conditions in the high Arctic and may explain how they avoid clogged arteries.

Scientists hope understanding how they work will help them find new ways to fight human obesity.



Lead researcher Professor Rasmus Nielsen, from the University of California at Berkeley, U.S., said: ‘The promise of comparative genomics is that we learn how other organisms deal with conditions that we also are exposed to.



‘For example, polar bears have adapted genetically to a high fat diet that many people now impose on themselves.



Studying how bears cope with a hugely high fat diet could help scientists tackle obesity in humans

‘If we learn a bit about the genes that allows them to deal with that, perhaps that will give us tools to modulate human physiology down the line.’



Colleague Dr Eline Lorenzen, also from Berkeley, said: ‘For polar bears, profound obesity is a benign state.

‘We wanted to understand how they are able to cope with that.’



The study, published in the journal Cell, showed that the polar bear is a much younger species than was previously thought.



COULD OTHER BEARS ALSO HELP IN THE FIGHT AGAINST OBESITY?

In December last year it was revealed that scientists are also studying grizzly bears in the hope they could solve the obesity crisis.

A study is being carried out into the creatures because they are able to eat up to 58,000 calories a day without becoming overweight.

Despite eating such huge amounts, they do not suffer heart attacks or clogged arteries - and they don't develop diabetes.

As a result, the U.S. drug maker Amgen is studying 12 of the bears at Washington State University in a bid to work out how they do it.

Test results so far suggest that bears modify their sensitivity to the hormone insulin, which controls how sugars and fats are broken down and used for energy.

As they put on weight before hibernation, bears seem to be more sensitive to insulin, Dr Kevin Corbit said.

But when hibernation begins they seem to shut off their responsiveness to the hormone completely.

Dr Corbit plans to spend the next two years working out how exactly the mechanism works.

His work will be assisted by the sequencing of of the bears' genome which he hopes will be completed soon.

Polar bears diverged from brown bears less than 500,000 years ago, compared with previous estimates of up to five million years.



After setting off on their own evolutionary path they rapidly changed to adapt to Arctic life.



Not only did their fur become white, but genes that affected their metabolism and heart function altered.



One gene, known as APOB, plays a role in moving cholesterol from the bloodstream into cells, thereby reducing the risk of heart disease.



Danish co-author Dr Eske Willerslev, from the University of Copenhagen, said: ‘Such a drastic genetic response to chronically elevated levels of fat and cholesterol in the diet has not previously been reported.



‘It certainly encourages a move beyond the standard model organisms in our search for the underlying genetic causes of human cardiovascular disease.’



The research involved sequencing and analysing the complete genomes, or genetic codes, of 79 polar bears from Greenland and 10 brown bears from around the world.



It comes at a time when the polar bear population - estimated at no more than 20,000 to 25,000 - is declining and the animal’s habitat, Arctic sea ice, rapidly vanishing.



As higher latitudes warm, brown bears are moving further north and occasionally interbreeding with their distant Arctic cousins.



The animals’ ability to interbreed is the result of a genetic relationship a tenth of the distance between humans and chimpanzees, the scientists said.



‘All the unique adaptations polar bears have to the Arctic environment must have evolved in a very short amount of time,’ Professor Nielsen said.



‘These adaptations include not only a change from brown to white fur and development of a sleeker body, but big physiological and metabolic changes as well.

