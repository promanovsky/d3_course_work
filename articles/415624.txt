The events in Ferguson, Missouri, became a focal point at the MTV Video Music Awards on Sunday night at The Forum in Inglewood, California.

Prior to the show MTV President Stephen Friedman said the network would air a 15-second PSA in hopes of sparking a discussion about the Aug. 9 police shooting death of 18-year-old Michael Brown, who was unarmed when he was killed in Ferguson.

"It's a call to action to our audience that we have to confront our own bias head-on before we can truly create change," Friedman said.

About an hour into the show, actor/rapper Common took the stage, saying, "The people in the Ferguson...in St. Louis have used their voices to call for justice and change -- to let everyone know that each and every one of our lives matters."

Common then talked about how hip-hop music has always strove to tell "the truth," noting, "Hip-hop...has always been a powerful instrument of social change."

He then asked for a "moment of silence for Mike Brown and for peace in the country and the world."