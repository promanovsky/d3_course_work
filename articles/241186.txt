The Financial Conduct Authority has fined Barclays £26m for failings related to fixing London gold prices and also banned and fined trader Daniel James Plunkett for inappropriate conduct.

The regulator said in a statement that it slapped Barclays with a fine after it failed to adequately manage conflicts of interest between itself and its customers as well as systems and controls failings, in relation to the gold fixing.

These failures continued from 2004 to 2013, the FCA added.

"A firm's lack of controls and a trader's disregard for a customer's interests have allowed the financial services industry's reputation to be sullied again," said Tracey McDermott, the FCA's director of enforcement and financial crime.

"Plunkett has paid a heavy price for putting his own interests above the integrity of the market and Barclays' customer. Traders who might be tempted to exploit their clients for a quick buck should be in no doubt - such behaviour will cost you your reputation and your livelihood".

"Barclays' failure to identify and manage the risks in its business was extremely disappointing. Plunkett's actions came the day after the publication of our Libor and Euribor action against Barclays. The investigation and outcomes in that case meant that the firm, and Plunkett, were clearly on notice of the potential for conflicts of interests around benchmarks."

"We expect all firms to look hard at their reference rate and benchmark operations to ensure this type of behaviour isn't being replicated. Firms should be in no doubt that the spotlight will remain on wholesale conduct and we will hold them to account if they fail to meet our standards."

What Happened?

The FCA said that on 28 June 2012, former Barclays' trader Plunkett exploited the weaknesses in Barclays' systems and controls to seek to influence that day's 1500 BST Gold Fixing and thereby profited at a customer's expense.

The watchdog said that as a result of Plunkett's actions, Barclays was not obligated to make a $3.9m payment to its customer, although it later compensated the customer in full. Plunkett's actions boosted his own trading book by $1.75m, excluding hedging.

The FCA has fined Plunkett £95,600 and banned him from performing any function in relation to any regulated activity.

What is Gold Fixing? Since joining the Gold Fixing on 7 June 2004, Barclays has contributed to setting the price of gold in the Gold Fixing. The Gold Fixing is an important price-setting mechanism which provides market users with the opportunity to buy and sell gold at a single quoted price.

Who was Plunkett and What Was His Role?

Plunkett was a director on the precious metals desk at Barclays and was responsible for pricing products linked to the price of precious metals and managing Barclays' risk exposure to those products.

Plunkett was responsible for pricing and managing Barclays' risk on a digital exotic options contract (the Digital) that referenced the price of gold during the 1500 BST Gold Fixing on 28 June 2012.

If the price fixed above $1,558.96 (the Barrier) during the 1500 BST Gold Fixing on 28 June 2012, then Barclays would be required to make a payment to its customer. But if the price fixed below the Barrier, Barclays would not have to make that payment.