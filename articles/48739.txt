Mark Zuckerberg is on a shopping spree.

On Tuesday after the market close, Facebook (NASDAQ: FB) announced the acquisition of Oculus for $2 billion in cash and stock. Oculus is the leader in immersive virtual reality technology.

The acquisition comes just five weeks after Facebook acquired WhatsApp, a rapidly growing cross-platform mobile messaging company, for $16 billion in cash and stock.

Similar to the WhatsApp deal, Wall Street doesn't understand the price tag for Oculus, but firms are not questioning Zuckerberg's vision going forward.

Cowen:

Analyst John Blackledge believes management has demonstrated it is “not afraid to make bold investments and their track record (so far) has been solid.” The analyst cites the acquisition of Instagram, which was initially met with skepticism from the Street. Currently, Instagram has “over 200 MM users (vs. 22MM users when acq. in 4/12), exceeding internal user projections (100MM) and is pacing to be the second largest social network by end of ‘14,” Blackledge said.

The analyst reiterated an Outperform rating and $85 price target.

Goldman Sachs:

The Oculus acquisition is “in line with Facebook's focus to enable fast communication and what we see as an emerging strategy to develop applications that can appeal to users outside of the main Facebook app,” said Heather Bellini, analyst at Goldman.

The analyst maintained a Buy rating and price target of $78.

Raymond James:

Analyst Aaron Kessler likes Facebook's “forward thinking strategy as it positions itself for the platforms of the future.”

The analyst has an Outperform rating and $78 price target.

Wells Fargo:

“Stepping back, we view this investment as similar to strategic investments made by Google (NASDAQ: GOOG), where Facebook is willing to make significant investment ahead of market development, perhaps driven in part by an acknowledgement that the company was admittedly slow to anticipate the pace and impact of the mobile computing shift,” said analyst Peter Stabler.

The analyst has an Outperform rating and valuation range of $74 to $76.