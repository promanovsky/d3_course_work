Samsung's 8.4 and 10.5 inch Galaxy Tab S are available for pre-order in the United Kingdom, posts on the company's website confirmed Monday.

According to The Inquirer, the phone manufacturer expects to send out devices July 4. The 8.4-incher costs 329 pounds, about $558. A 4.900mAh battery runs the device. The 10.5 incher has a 7.900mAh battery. The device measures 419.99 pounds. This is 10 pounds more than Apple's iPad Mini. The regular iPad costs 20 pounds more than Apple's least expensive iPad Air.

The Samsung Galaxy Tab S has a 2560x1600 resolution display, an octa-core Exynos 5 processor, and thin 6.6 millimeter makeup. Apple's iPad Air has a 7.5 millimeter build-up. Both devices run on Google's Android 4.4 Kitkat mobile operating system, which offers consumers many programs like Kick, a football app. The tablet also takes calls from a Samsung smartphone.

The tablet also holds 3 gigabytes of RAM, eight megapixel, and two megapixel cameras. The device also links to Bluetooth 4.0, and has a 4 gig connection speed.

The company's CEO and president of its IT and mobile division said consumers are developing a liking for the new technology.

"The tablet is becoming a popular personal viewing device for enjoying content, which makes the quality of the display a critical feature," JK Shin said in a statement. "With the launch of the Galaxy Tab S, Samsung is setting the industry bar higher for the entire mobile industry. It will provide consumers with a visual and entertainment experience that brings colors to life, beautifully packaged in a sleek and ultra-portable mobile device."

The Tab S comes in titanium bronze and dazzling white colorways.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.