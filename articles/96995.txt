Google Glass has been used in a series of medical trials to assess whether it could be effective at helping sufferers of Parkinson's Disease.

Researchers at Newcastle University are carrying out tests to determine whether the headset could grant more independence to those stricken with the neurological illness.



A range of custom apps are being used to serve Parkinson's patents with subtle medication alerts, and remind them of upcoming appointments.

Google Glass will also prompt sufferers to speak or swallow to prevent drooling, and display stimulating visual cues to prevent them from "freezing".

Claire Bale, research communications manager at Parkinson's UK, said: "This new study looking into Google Glass is an exciting example of how new technologies could be used to improve the lives of people living with Parkinson's by tackling a wide variety of problems - from freezing to remembering to take their medication on time.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

"But to really make the most of the potential of new technologies, it's essential that researchers work in partnership with the real experts in the condition - people living with Parkinson's.

"Only people with the condition can tell us if these new approaches will genuinely improve their lives in meaningful and realistic ways."

The medical trials are currently in the early stages, but the researchers are already in the process of developing apps to meet the individual needs of volunteers.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io