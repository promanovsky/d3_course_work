Looks like Twitter is rolling out a brand new 'your week in review' feature to users in the form of a weekly email newsletter. It's not clear if this is being rolled out select users or to everyone, but one just landed in our inbox and it comes with metrics we haven't seen Twitter share with its users before.

The newsletter is divided in four parts, with the first including a summary of the week: how many replies (@mentions) you got, the links you shared got how many links, and perhaps most interestingly, the total number of 'views' your tweets got. We haven't seen Twitter share the 'views' of tweets before, and it would be interesting to here how they are calculating this in terms of considering active/ inactive followers.

The next section includes a list of the user's most popular tweets of the week, complete with the count of number of views, favourites, retweets, and links clicked included for the tweets. Following that is a section saying 'this link got a lot of visits', followed by a section 'you got some attention with this tweet', which seems like the most retweeted tweet of the week, but we can't be sure.

Admittedly, the author hasn't been too active on Twitter recently, so there isn't much to take out this. We'll surely hear more about this shortly.

The newsletter seems to be an interesting way of getting active users more involved with their Twitter presence (as they will surely obsess about the statistics), while getting the not-so-active ones back in the fold.

Have you received this newsletter? What do you make of it?