The exterior of Community Hospital, where a patient with the first confirmed US case of the Middle East Respiratory Syndrome is in isolation, is seen in Munster, Indiana, May 5, 2014. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

INDIANA, May 7 ― The first US patient to test positive for the often deadly Middle East Respiratory Syndrome virus will remain isolated at home after he is released from an Indiana hospital, officials said yesterday.

The patient will remain in isolation until the Indiana State Department of Health and the US Centers for Disease Control and Prevention determines he is no longer an infectious risk to the public, the Community Hospital in Munster said in a statement.

The hospital is working on discharge plans for the patient, who continues to improve and is expected to be released in the coming days, the statement said.

The CDC confirmed on Friday that it had identified the first MERS case in the country, raising new concerns about the global spread of an illness with a high fatality rate and no known treatment.

The patient is a healthcare worker employed in Saudi Arabia, where the virus was first detected in 2012, who had come to Indiana to visit family. Saudi officials said the toll from a recent outbreak was still rising, with 18 new cases in the capital city of Riyadh, where the US patient works.

Up to 50 hospital workers as well as family members and close contacts of the patient who were monitored for signs of the virus have tested negative for MERS.

All of the Indiana hospital employees and the patient's family members will be retested at the end of a 14-day incubation period for final confirmation they are free from MERS. ― Reuters