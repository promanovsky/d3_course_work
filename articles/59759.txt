Ebola ‘a regional threat’

Share this article: Share Tweet Share Share Share Email Share

Conakry - Guinea's capital Conakry was on high alert on Friday after a deadly Ebola epidemic which has killed dozens in the southern forests was confirmed to have spread to the port city of up to two million people. Four new cases, of people believed to have been infected after attending the funeral of a brother in central Guinea, were put in isolation centres to avoid the highly contagious virus getting into the population. Aid organisations have sent dozens of workers to help the poor west African country combat a haemorrhagic fever outbreak which has killed at least 66 people, many of whom have been confirmed to have been infected by Ebola. “Intensive case investigations are underway to identify the source and route of these patients' infection, record their travel histories before arrival in Conakry and determine their period of infectivity for the purposes of contact tracing,” the World Health Organisation (WHO) said in a statement.

Guinea is one of the world's poorest nations despite vast untapped mineral wealth, with a stagnating economy, youth unemployment at 60 percent and a rank of 178th out of 187 countries on the UN's Human Development Index.

Residents of Conakry's suburbs told AFP they feared venturing into the city centre to shop and were keeping their children home from school.

“I wonder what Guinea has done to God to make him send us this untreatable disease... I'm wary of anything that moves that could be a carrier of the disease,” said unemployed graduate Abdoulaye Soumah.

The Economic Community of West African States (ECOWAS), a grouping of 15 countries, said the outbreak was now “a serious threat to regional security” and appealed for help from the international community.

Fifteen new confirmed or suspected cases, including in the Conakry outbreak, were reported on Thursday, the health ministry said, bringing the total in Guinea to 103.

Most victims have been adults and four health care workers' deaths have been attributed to Ebola.

The tropical virus - described in some health publications as a “molecular shark” - causes severe fever and muscle pain, weakness, vomiting, diarrhoea and, in severe cases, organ failure and unstoppable bleeding.

Ebola had never spread among humans in west Africa before the current outbreak, but further suspected cases being investigated in Liberia and Sierra Leone could bring the total death toll to at least 77.

Scientists from Dakar, Lyon and Hamburg have examined around 41 samples from victims, Guinea's health ministry said, with 15 testing positive for the Zaire strain of Ebola, the most virulent.

“WHO has alerted countries bordering Guinea about the outbreak and to heighten surveillance for illness consistent with a viral haemorrhagic fever, especially along land borders,” the statement said.

The UN health agency added, however, that it was not recommending travel restrictions.

The WHO said Liberia had reported eight suspected cases of Ebola fever, including six deaths, while Sierra Leone had reported six suspected cases, five of them fatal.

“We have sent some samples abroad for examination and until now we do not have any confirmation that those people who were suspected to have died from Ebola were indeed affected by the disease,” Liberian Health Minister Walter Gwenigale told journalists.

Transmission of Ebola to humans can come from wild animals, direct contact from another human's blood, faeces or sweat, as well as sexual contact or the unprotected handling of contaminated corpses.

The health charity Doctors Without Borders, known by its French initials “MSF”, said the spread of the disease was being exacerbated by people travelling to funerals in which mourners touch the dead person's body.

Guinea has banned the consumption of bat soup, a popular delicacy in the country, as the fruit bat is believed to be the host species.

No treatment or vaccine is available, and the Zaire strain detected in Guinea - first observed 38 years ago in what is today called the Democratic Republic of Congo - has a 90-percent death rate.

Customers in a suburban cafe in Conakry described the epidemic as “divine retribution” and “a curse that has befallen us and will allow us to reflect on our daily behaviour”.

“There is total panic among the population,” said Fanta Traore.

“We women are even afraid to go to market now and rub up against other people because we are told that even simple friction is a source of contamination.” - Sapa-AFP