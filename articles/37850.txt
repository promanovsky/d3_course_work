U.S. stocks slumped, hit by a selloff in biotechnology and technology shares that put the Nasdaq on course for its biggest daily percentage decline since early February. (AP)

Global equity markets fell on Monday, rattled by the ongoing Ukraine crisis and weak data from China, while gold fell on concerns about higher U.S. interest rates and a stronger US dollar.

U.S. stocks slumped, hit by a selloff in biotechnology and technology shares that put the Nasdaq on course for its biggest daily percentage decline since early February.

The dollar firmed and the euro slipped after signs of slowing growth in Germany, the euro zone's largest economy.

The Dow Jones industrial average fell 47.45 points, or 0.29 percent, at 16,255.32. The Standard & Poor's 500 Index was down 11.77 points, or 0.63 percent, at 1,854.75. The Nasdaq Composite Index was down 64.45 points, or 1.51 percent, at 4,212.34.

Also read: Sunny Leone's Ragini MMS2 - Short review

The Nasdaq biotechnology index, which jumped 66 percent last year, fell 3.5 percent for its fourth straight daily decline, a period over which the group has lost 9 percent. Alexion Pharmaceuticals Inc dropped 6.1 percent to $150.12.

"Biotech stocks have gone parabolic over the past few months, so this is a necessary correction to that," said Mark D'Cruz, senior investment analyst at Key Private Bank in Cleveland, Ohio. "A lot of that interest came from outside traditional biotech investors, who are now being scared off ... Biotechs really have to prove themselves this year, prove that their drugs can deliver."

Tension between Ukraine and Russia continued to simmer after Ukraine announced the evacuation of its troops from Crimea, essentially yielding the region to Russian forces. Diplomats have raised concerns about a NATO-reported buildup of Russian troops at Ukraine's border, and there are fears Russia also has designs on Moldova, another former Soviet republic.

Investors are concerned about the potential economic fallout, and shares of European companies with big exposure to Russia came under renewed pressure. U.S. President Barack Obama on Monday began talks with European allies on a response to the crisis as the potential for more sanctions looms.

"The issue remains contained for the time being, but Obama will try and garner support for more sanctions, which will ultimately shape our view of how things can end up looking," said Art Hogan, chief market strategist at Wunderlich Securities in New York. "This remains at the forefront of what we're paying attention to."

A decline in China's manufacturing growth in the first quarter added to worries about the global economy, although it also raised hopes for new stimulative measures from the world's second-largest economy. Markets in China shrugged off the data, and the Hang Seng Index gained 1.9 percent.

The flash Markit/HSBC China Purchasing Manager index fell to an eight-month low of 48.1 in March. The index has been below 50, the dividing line between expansion and contraction, since January. Weakness in China is a worry for investors because of its demand for raw materials and technology.

Gold hit a one-month low on expectations of higher U.S. interest rates by 2015. Spot gold dipped to $1,310.46 an ounce, following a sharp fall triggered by comments last week from Federal Reserve chief Janet Yellen that suggested interest rates could rise sooner than many in markets had expected.

"With higher yields you get a higher opportunity cost of holding gold, and with the stronger U.S. dollar there is less of a fear of currency debasement," Natixis analyst Bernard Dahdah said.

"We could see gold dropping below $1,300 in the next month if we get the necessary U.S. data, a strengthening dollar and higher yields."

The benchmark 10-year U.S. Treasury note was up 4/32, with the yield at 2.7353 percent.

German Bund futures extended losses after the flash composite purchasing managers' index for France jumped to 51.6 in March from 47.9 last month.

The euro weakened after data showing growth in Germany slowed in March, raising the potential for more monetary easing from the European Central Bank. Data from the euro zone as a whole dipped compared with February.

"Germany is going to determine ECB policy," said Boris Schlossberg, managing director of foreign exchange strategy at BK Asset Management in New York.

The FTSEurofirst 300 index, which rose 1.8 percent last week, ended down 1 percent.

The dollar index, which measures the greenback against a basket of currencies, was at 80.05 after reaching a session peak of 80.29, not far from Thursday's three-week high of 80.354.

The euro last stood at $1.3786, down slightly after hitting a high of $1.3875 earlier. The dollar rose a notch against the yen, to 102.24 yen.