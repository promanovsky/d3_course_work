OAK BROOK, Ill. — McDonald’s CEO Don Thompson sought to address a growing chorus of critics on issues including worker pay and marketing to children at its annual meeting Thursday.

As hundreds of protesters chanted for higher wages outside, Thompson told the audience in the building that the company has a heritage of providing job opportunities that lead to “real careers.”

“We believe we pay fair and competitive wages,” Thompson said.

A day earlier, McDonald’s closed one of its buildings in suburban Chicago, where protesters had planned to demonstrate over the low wages paid to its workers. Organizers then targeted another site on the company’s headquarters, and police say 138 were arrested after they peacefully refused to leave the property.

As in years past, McDonald’s marketing to tactics to children was also brought up by speakers affiliated with Corporate Accountability International. One mother from Lexington, Kentucky, Casey Hinds, said Ronald McDonald was “the Joe Camel of fast food.”

Thompson said McDonald’s wasn’t predatory and that Ronald McDonald was about letting kids have fun. He noted that his children ate the chain’s food and turned out “quite healthy,” with his daughter even becoming a track star.

“We are people. We do have values at McDonald’s. We are parents,” he said.

Although many fast-food chains engage in similar practices, McDonald’s Corp. is a frequent target for critics because of its high profile. The criticism is becoming a more pressing issue for the world’s biggest hamburger chain at a time when it is fighting to boost weak sales amid heightened competition.

Part of the problem is that people are shifting toward foods they feel are fresher or healthier, which has prompted McDonald’s executives in recent months to underscore the quality of the chain’s ingredients.

Thompson struck on those notes again Thursday, saying the company cracks eggs and uses fresh vegetables to make its food.

Still, the issue of worker pay in particular has put McDonald’s in an uncomfortable spotlight since late 2012, when protests for a $15 an hour wage began in New York City. Demonstrators were out again before the meeting, chanting, “I want, I want, I want my $15.”

Shawn Dalton, who traveled from Pittsburgh, said her daughter is a recent high school graduate who can’t afford to go to college right away, so she’ll likely wind up earning Pennsylvania’s $7.25-an-hour minimum wage.

“That won’t get her an apartment, that won’t buy a bus pass, that won’t buy food,” Dalton said. “She’ll either have to depend on welfare or depend on me.”

Despite the ongoing focus on low-wage workers, shareholders overwhelming voted in favor of McDonald’s executive compensation practices. Last year, Thompson was given a pay package worth $9.5 million.

Shareholder meetings offer a rare opportunity for average investors to face top executives at publicly traded companies. Public pension funds and activist groups often show up in hopes of influencing a change in corporate practices.

More trivial matters can also come up during the questions-and-comments period. On Thursday, for instance, one speaker asked Thompson why McDonald’s didn’t serve biscuits and gravy for breakfast in all its markets.

Others complained about the meeting’s early start time and the sugary chocolate chip cookies served for breakfast.