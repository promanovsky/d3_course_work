Despite the failure of a huge supersonic parachute to fully inflate during a test flight at the edge of space, project engineers said Sunday they were elated with the otherwise near-flawless performance of the flying saucer-shaped research craft, designed to test atmospheric braking systems for future Mars landers.

"We designed, built and flew a first of its kind, 15-foot-in-diameter, Mach 4 test vehicle going up to 200,000 feet at the edge of space," said Project Manager Mark Adler. "It worked beautifully, absolutely beautifully. Yesterday was just a great day."

The Low-Density Supersonic Decelerator, or LDSD, research craft was launched Saturday from the U.S. Navy's Pacific Missile Range Facility on Kauai, Hawaii. A huge high-altitude balloon filled with 34 million cubic feet of helium carried the 3.5-ton test vehicle to an altitude of 120,000 feet, or about 23 miles.

At that point, the LDSD was released. Small rocket motors fired to spin it up for stability and then a powerful ATK Star 48 solid-fuel motor ignited to boost the craft's altitude by another 11 miles or so and to increase its velocity to more than four times the speed of sound.

At that altitude and velocity, the LDSD test craft experienced aerodynamic conditions similar to what a Mars lander would find entering the thin atmosphere of Mars on the way to the surface. The goal is to develop atmospheric braking systems that will allow NASA to send heavier, more sophisticated landers to the surface than current entry systems can handle.

A major component in Saturday's LDSD test flight was doughnut-like collar called a Supersonic Inflatable Aerodynamic Decelerator tightly packed around the circumference of a simulated heat shield.

After the Star 48 burned out, the SIAD inflated in a fraction of a second, expanding the diameter of the entry vehicle from about 15.4 feet to 19.7 feet and providing more surface area to react against the atmosphere. After inflation, the SIAD helped reduce the velocity to about 2.5 times the speed of sound.

The collar "performed phenomenally, it inflated extremely quickly, a very stable vehicle after it inflated," said Ian Clark, LDSD principle investigator at the Jet Propulsion Laboratory. "We had over a minute of solid flight time, gathering gigabytes and gigabytes of data on the performance of the SIAD. All the early indications are that it was flawless."

After slowing to around Mach 2.5, a huge new parachute was released, the largest ever built for supersonic deployment.

On-board video showed the parachute streaming away behind the test vehicle, but it never fully inflated. Clark said Sunday engineers do not yet know what caused the problem, whether any of its support lines tangled or even whether the chute suffered a tear of some sort.

But the parachute, the high-altitude balloon and the LDSD vehicle were recovered, and along with it the vehicle's high-resolution video recorders.

"The vehicle was intact," Clark said. "Even though the chute didn't perform 100 percent as expected, a device that large back there was still generating a fair amount of drag, so the vehicle may have hit the water going between, say, 20 and 30 miles an hour. But it was still intact and they were able to recover it. ... It worked great."

Over the next several weeks, engineers will analyze the video and other data to reconstruct the flight in detail and figure out what went wrong with the parachute. Two more test flights are planned next summer in a $200 million research program.

"We've got tons of high-speed and high-resolution video that we're going to bring back, we've got a lot of other data to look at," Adler said. "We're going to spend the next several weeks analyzing this data in detail in understanding what happened on this flight so we can apply lessons learned to the two flights we have planned for next summer."

Dorothy Rasco, deputy associate administrator for the Space Technology Mission Directorate at NASA Headquarters, praised the LDSD team for a solid initial test flight.

"Getting the liftoff, drop, ignition, burn, flight to target speed and altitude and the (SIAD) inflation to all work is fantastic," she said. "Recovering the vehicle and data recorders is amazing and will provide the opportunity to learn.

"Considering the complexity of the flight and our knowledge that parachute inflation is always a huge uncertainty, this is a terrific outcome. ... We took technical risk yesterday in Hawaii to reduce the risk of landing on Mars tomorrow."