Talking Points:

drops by over 40 pips following Kuroda comments

BOJ refrains from further easing at their monetary policy meeting today

The central bank has affirmed their optimistic outlook for the Japanese economy

The BOJ announced today that it would keep its monetary policy on hold as they have garnered confidence that the third largest economy can withstand the recent sales tax hike. Bank of Japan Governor Haruhiko Kuroda reiterated at the ensuing press conference that the board remains confident they will achieve the 2 per cent inflation target that was set a year ago.

While many expect the economy to contract in the 2nd quarter due to the consumer spending hangover after the increase in the sales tax, Japan’s central bank raised its forecast for business investment. This seems to be a signal that the BOJ has become increasingly confident, and they have little intention to introduce further easing in the near term. As it stands the central bank will continue to expand the monetary base at a pace of 60 to 70 trillion yen ($691 billion) per year.

Following the press conference, the yen strengthened against the US Dollar, causing USD/JPY to fall to a 6 month low of 100.84. The pair had fallen by more than 40 pips since the start of Kuroda’s speech. DailyFX Chief Strategist, John Kicklighter, has suggested that fading hopes for further easing from the BOJ may lead the yen to strengthen further (leading to a weakening for yen pairs). Buyers may look to support the pair at the 100.87 mark which forms a Fibonacci Expansion level.

USD/JPY 5 Minute Chart

USD/JPY 5 Minute Chart

Original post