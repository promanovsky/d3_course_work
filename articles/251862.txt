'Winter Sleep', a film by Turkish director Nuri Bilge Ceylan, has been named the winner of the prestigious Palme d'Or award at the 67th annual Cannes Film Festival.

It is only the second film by a Turkish director to win the festival's highest honour, after Yilmaz Guney and Serif Goren's 'The Way' (1982).

The film is a sprawling, character-rich portrait of a self-absorbed Anatolian hotelier and his uneasy relationships with those around him.

The Grand Prix, the festival's second-place honour, was presented to 'The Wonders', Italian director Alice Rohrwacher's semi-autobiographical drama about a family of beekeepers struggling to preserve their way of life in central Italy.

Julianne Moore won the best actress prize for 'Maps to the Stars', while 'Mr Turner' star Timothy Spall was named best actor.

"This is a great surprise for me," Ceylan said when he took the stage, noting that it was perhaps a fitting choice in a year that marked the 100th anniversary of Turkish cinema.

Bennett Miller won the best director honour for his film 'Foxcatcher' starring Channing Tatum, Mark Ruffalo, and Steve Carell.

Scoring big on his first trip to one of the major European film festivals, Miller was a popular choice for directing honours for 'Foxcatcher', his tense, well-acted crime drama about the complex psychological triangle that ensnared Olympic wrestlers Mark and Dave Schultz (played by Tatum and Ruffalo) and the Pennsylvania millionaire John du Pont (Carell).

"This is quite affirming, and I am very grateful," Miller said in his speech.

Moore drew the actress prize for her ferocious turn as a washed-up Hollywood star in David Cronenberg's Tinseltown satire 'Maps to the Stars'. With Moore not present at the ceremony, the film's writer, Bruce Wagner, accepted the award on her behalf.

In another nod to one of the competition's high-profile English language entries, Spall won the best actor prize for his performance as the painter JMW in Mike Leigh's 'Mr Turner'.

He joined his 'Secrets & Lies' co-star Brenda Blethyn and 'Naked' star David Thewlis, both of whom also received acting honours at Cannes for their work with the British director.

The jury prize, essentially third place, was shared by two films from the competition's youngest and oldest helmers, respectively: 'Mommy', from 25-year-old Canadian director Xavier Dolan, and 'Goodbye to Language', from 83-year-old French New Wave icon Jean-Luc Godard.

Although hotly tipped as a front runner for the Palme, the critically adored Russian drama 'Leviathan' had to content itself with a screenplay prize for helmer Andrey Zvyagintsev and his co-writer, Oleg Negin.

Two critically lauded competition entries that were shut out entirely from the awards were Abderrahmane Sissako's 'Timbuktu', which did win an award from the festival's ecumenical jury, and Jean-Pierre and Luc Dardenne's 'Two Days, One Night', which had been widely expected to win best actress for Marion Cotillard.

It marked the first time that the two-time Palme d'Or-winning Belgian brothers have returned home from Cannes without a prize.

The Camera d'Or for best first film was given to 'Party Girl', a three-way directing debut for French helmers Marie Amachoukeli, Claire Burger and Samuel Theis.

The film, which opened the Un Certain Regard sidebar, had already received an ensemble acting prize.