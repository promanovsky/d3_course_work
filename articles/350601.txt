Microsoft is reportedly planning to jump into the wearable space with a fitness tracking band that will work with all three major mobile platforms, Android, iOS and Windows Phone.

While Samsung continues to expand its wearable portfolio and Apple is nearing its iWatch debut, Microsoft is making the headlines with its rumored foray in the wearable space. Several media publications speculated Microsoft's first wearable would be a smartwatch but reports are taking a new turn in the developing story. Winsupersite's Paul Thurrott reported Wednesday that the Redmond-based software giant will first release a fitness band similar to Samsung Gear Fit instead of a smartwatch.

Microsoft is reportedly taking a new approach with its yet unnamed fitness smartband. According to the report, the wearable will work with all three major mobile platforms including Android, iOS and Windows Phone, which other tech giants do not offer. Samsung's Gear line of smartwatches and smartbands work exclusively with Android smartphones, and Apple's iWatch will of course work with iPhones. Microsoft's unique approach will attract a wider audience from all three major platforms.

Defining the device's functionality, Thurrott writes that the fitness tracker will be equipped with multiple sensors, so it can track users' steps, calories burned and heart rate. The data will then be carried forward through Microsoft's Bing Fitness and Health app. Third party apps will also be able to pair with Microsoft's tracker offering flexibility to choose users' favorite apps.

Earlier in May, a patent filing showed sketches and description of an unannounced wearable gadget by Microsoft. A separate report by Forbes suggested that Microsoft's first wearable will feature an always-on heart rate monitor. The information put together by Forbes in late-May echoes Thurrott's predictions of Microsoft's wearable gadget to support Windows Phones, iPhones as well as Android smartphones.

Thurrott suggests the Microsoft fitness device will cost the same as Samsung Gear Fit, which means it will retail for around $200. The device is expected to debut in the fourth quarter this year.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.