Kacy Catanzaro American Ninja Warrior: Age, Video, Facebook Photos

Kacy Catanzaro has made a name for herself by being the first woman to qualify for the finals of American Ninja Warrior.

Catanzaro is a 5-foot, 100-pound gymnast who has been training for the competition for about two years.

“This is my second year competing and before that, I was competing in gymnastics full-time and in college and in places like that,” she explained to Vulture.

“We’re doing a lot of body weights, circuit training, things like that. There are people that lift heavy weights and that’s great for them depending on what your goal is. My goal is to be as light and lean and strong as possible for the course.”

The training goes from four to six days a week while obstacles take up a couple hours every other evening or so.

And what’s she eating all this time?

“I’ve been eating a lot of oatmeal lately, which isn’t the complete grain or gluten free obviously, but it does kind of get your body moving. For the next two meals, it depends on where we are, but we do big salads with eggs or something like that. We love fish – salmon or shrimp, scallops, definitely lots of seafood. Not too much meat…every now and then we’ll splurge and get some chicken nachos or something like that, but meat definitely isn’t on my normal meal regimen or anything.”

Kacy was born on January 14, 1990 in Glen Ridge, New Jersey, making her 24 years old.

She studied at Towson University, majoring in Early Childhood Education.

She was part of the gymnastics squad at the school, earning a number of awards.

Kacy hails from Belleville, New Jersey, where she went to high school.

(Facebook)

(Facebook)

(Facebook)

(Facebook)

(Facebook)

(Facebook)