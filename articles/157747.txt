Eric Hill, a contestant on the upcoming season of The Bachelorette, was severely injured in a paragliding accident on Sunday and has passed away.

His sister Karen Tracy confirmed that Hill died via Facebook today:

"Eric shared his final journey with us this morning as all his immediate family were able to be at his side when he passed away," she wrote after his death.

"Thank you to all of your love and support and prayers and fasting."

"It was amazing to be with so many of his friends and family yesterday in the hospital who came to express their love for Eric. He gave us such a gift of a life."

"It is hard to think of life without his bright spark, but we know he is on to new adventures. We look forward to carrying on his legacy here and greeting him joyfully again someday."

"I love you so much, little brother!"

Hill, one of Andi Dorfman's suitors on the upcoming season of The Bachelorette, was in critical condition after part of his parachute collapsed while he was paragliding.

A rescue team was called to the scene, and he was transported by helicopter to a local hospital, where he remained in a medically induced coma until today.

The accident occurred post-Bachelorette, which is still being filmed, meaning that Eric Hill had already been eliminated from the show at some point.

Sorry for The Bachelorette spoilers. Before the show, Hill had been on a quest to visit every country recognized by the United Nations in the world in 1,200 days.

He had been documenting his journey on his website, GO With Eric: The Global Odyssey, and had visited more than 50 countries in his journey's first year.

R.I.P.