A recent survey by the Centers for Disease Control and Prevention on Thursday states that cigarette smoking among U.S. high school students is at its lowest rate since 1991.

Health officials from the CDC conduct the survey every two years, and in 2013, only 15.7 percent of teens smoked cigarettes, which went down from the 27.5 percent teen smoking rate in 1991. The peak year was in 1997 when it was reported that 36.4 percent of teens in the U.S. smoked cigarettes.

According to USA Today, the government's official goal of reducing the rate of teen smokers to below 16 percent by 2020 has now been met, seven years ahead of schedule.

Based on the national Youth Risk Behavior Survey of more than 13,000 teens at both public and private high schools, CDC Director Tom Frieden said that, besides not smoking, teens also are choosing other healthier lifestyles.

"I think the bottom line is that our teens are choosing health," Frieden said.

According to the survey, teens in the U.S. are drinking less alcohol and sodas, getting into fewer physical altercations, practicing safe sex with birth control and also just having less sex. The survey also found that despite the increase in school shootings, the rate of student-to-student threats with a gun, knife or other weapon on school property has decreased from its peak of 9.2 percent in 2003 to 6.9 percent.

In 2003, the condom use rate among the sexually active, which is roughly one third of teens, was at 63 percent; however, the recent findings show the rate is down to 59 percent, USA Today reported.

Cigar use was rapidly declining until recently where figures have show its use has slowed down and is a popular among high school boys, primarily seniors. According to the data, 23 percent of 12th grade boys smoked cigars a month before the survey.

E-cigarette and hookah use have increased, while the data revealed the declining cigarette use rate varied from place to place, Frieden said.

Vince Willmore, a spokesman for the non-profit Campaign for Tobacco Free Kids, Washington, D.C., said increased taxes on cigarettes, better educational campaigns and various measures have helped the nation to move "in the right direction."

"But the fight against tobacco isn't over, and it can't be over when you still have 2.7 million high school kids who smoke," Willmore said.

Other findings from the survey included a decrease in TV usage as 32 percent of teens admitted to watching three daily hours of TV, an improvement from the 43 percent in 1999. But the rate of teens using a computer for non-school reasons at least three hours a day increased nearly double from 22 percent in 2003 to 41 percent now.