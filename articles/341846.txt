Yahoo says it’s giving the sitcom Community a sixth season online.

Yahoo and Sony Pictures Television announced on Monday that the show’s creator, Dan Harmon, will serve as an executive producer for 13 new episodes.

The companies say cast members including Joel McHale, Gillian Jacobs and Yvette Nicole Brown will come along as the axed NBC show moves online to Yahoo Screen this fall.

In a statement paraphrasing Mark Twain, McHale says reports of the show’s cancellation were “greatly exaggerated.”

Community is about a group of friends who come together at a community college with an iffy pedigree. The comedy has loyal followers but too few for NBC, which dropped it from next season’s schedule.

Yahoo credits passionate Community fans with keeping the show alive.