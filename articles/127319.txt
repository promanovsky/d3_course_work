An unemployed actor undergoes a drastic transformation in a twisted attempt to regain the trust of his estranged family.

An unemployed actor undergoes a drastic transformation in a twisted attempt to regain the trust of his estranged family....

IT’S been more than 20 years since Robin Williams dressed up as a post-menopausal nanny, but he’s about to do it all over again.

After a few false starts, a sequel to the 1993 hit comedy, Mrs Doubtfire, is finally going ahead.

According to The Hollywood Reporter, Robin Williams is on board as is the original director Chris Columbus.

“The thing that fascinates me about a sequel to Mrs. Doubtfire is with most actors who create an iconic character like Mrs. Doubtfire, when you come back and do that character, well, you’re twenty years older so, you’re not going to look the same,” Chris Columbus said to Huff Post Live last year.

SEVEN THINGS YOU NEVER KNEW ABOUT MRS DOUBTFIRE

“The cool thing with Mrs. Doubtfire is there’s a character, there’s a woman, who is actually going to look exactly as she did in 1995.”