A nurse named Nicole doesn't want to hook up with you on Tinder; instead, she wants to hook you up with health information in honor of men's health month.

"Nicole", a beautiful "nurse", takes to @Tinder to talk to men – about health issues: http://t.co/Sr3ys4IqFr pic.twitter.com/wPjX7QDyi4 — ABC News (@ABC) June 3, 2014

"Nurse Nicole" is actually the work of two male interns at Razorfish, Vince Mak and Colby Spear, who are using the popular meet-up app to spread the word about prostate cancer. Using a photo of an attractive woman to lure users in, Mak and Spear have Nicole share a website link for more information on common health problems. Not everyone is amused — one asked, "How are you even on Tinder at work, shouldn't you be saving lives?" — but others don't give up so easily. "That's kinda hot haha," one wrote. "Can I get an exam?"

Mak and Spear plan on continuing the charade throughout the month, and say they will post the best conversations online. "We always thought there were funny things to do with Tinder — both of us use it and joke about it," Spear told ABC News. "It seemed like a perfect way to raise awareness." Catherine Garcia