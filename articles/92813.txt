Three days ahead of the Samsung Galaxy S5 launch, the electronics giant announced the availability of its Galaxy Ace Style smartphone.

Smaller than Samsung's flagship Galaxy, the Ace Style's light, simplified user experience is aimed at "the young, expressive consumer."

Running Android 4.4 KitKat on a 1.2GHz dual core processor, the 4-inch handset promises seamless multi-tasking and fast app activation time.

It also sports a 5-megapixel rear camera, which boasts advanced autofocus and VGA for instantly shareable photos. The One Camera Preview feature provides easy access for full HD video recording and picture taking.

And with the pre-loaded ChatON function, users can send photos to friends while chatting on the go.

Other specs include 512MB of RAM, 4GB of internal memory, and a microSD slot for up to 64GB of expandable storage, as well as a 1500mAh battery and Bluetooth 4.0, Wi-Fi, and NFC capabilities.

The new Samsung Galaxy Ace Style will be available globally in Cream White and Dark Gray this month; an exact launch date and pricing details were not provided.

The South Korean phone maker is also rumored to be developing a Galaxy S4 Zoom followup. According to recent reports, the Galaxy S5 Zoom will feature a 20.2-megapixel camera (5,184-by-3,888 resolution) with optical image stabilization.

This news comes just ahead of Friday's launch of the Galaxy S5 handset, which PCMag's Sascha Segan dubbed "the everything phone," promising new features like camera improvements and health tracking.

For more, see PCMag's review of Samsung's Galaxy S5 and the slideshow above. Also check out The Top 10 Questions About the Samsung Galaxy S5.

Further Reading

Computers & Electronic Reviews