Watch Ria Eaton on ‘The Voice’ Sing ‘Cups (When I’m Gone)’ [Video]

Ria Eaton entered “The Voice” on Monday night, singing “Cups (When I’m Gone).”

She barely got any judges to turn around but Blake Shelton and Shakira turned around at the last second.

“I think you started singing better when it crossed your mind that ‘this isn’t going to happen’,”

It think that’s when you finally let go, it’s like her voice opened up, the pitch was there, you had that rasp, you have to be born with it,” Shelton said.

“When I heard that raspy voice I realized the kind of signer you’re in front of,” Shakira said. “You’re like a pearl in an oyster.”

“If you pick me as your coach I’m going to take that shell, pry it open, and make sure to polish that pearl.”

Check out who she picked below.