Google just debunked the top 10 common myths about Glass, the search giant's Internet-connected glasses.

"Myths can be fun, but they can also be confusing or unsettling," Google writes on its Glass Google+page. "And if spoken enough, they can morph into something that resembles fact. (Side note: did you know that people used to think that traveling too quickly on a train would damage the human body?)"

Advertisement

Some of these myths range from, "Glass Explorers are technology-worshipping geeks" to "Glass is banned... EVERYWHERE."All in all, Google stresses the fact that Glass is still in its prototype stage and is not quite ready for prime time. It also debunked rumors that Glass Explorers are tech nerds, pointing out that they come from all walks of life.

Here's the full post:

Advertisement

Mr. Rogers was a Navy SEAL. A tooth placed in soda will dissolve in 24 hours. Gators roam the sewers of big cities and Walt Disney is cryogenically frozen. These are just some of the most common and -- let's admit it -- awesome urban myths out there.

Advertisement

Myths can be fun, but they can also be confusing or unsettling. And if spoken enough, they can morph into something that resembles fact. (Side note: did you know that people used to think that traveling too quickly on a train would damage the human body?) In its relatively short existence, Glass has seen some myths develop around it. While we're flattered by the attention, we thought it might make sense to tackle them, just to clear the air. And besides, everyone loves a good list:

Myth 1 - Glass is the ultimate distraction from the real world

Instead of looking down at your computer, phone or tablet while life happens around you, Glass allows you to look up and engage with the world. Big moments in life -- concerts, your kid's performances, an amazing view -- shouldn't be experienced through the screen you're trying to capture them on. That's why Glass is off by default and only on when you want it to be. It's designed to get you a bit of what you need just when you need it and then get you back to the people and things in life you care about.

Myth 2: Glass is always on and recording everything

Just like your cell phone, the Glass screen is off by default. Video recording on Glass is set to last 10 seconds. People can record for longer, but Glass isn't designed for or even capable of always-on recording (the battery won't last longer than 45 minutes before it needs to be charged). So next time you're tempted to ask an Explorer if he's recording you, ask yourself if you'd be doing the same with your phone. Chances are your answers will be the same.

Advertisement

Myth 3 - Glass Explorers are technology-worshipping geeksOur Explorers come from all walks of life. They include parents, firefighters, zookeepers, brewmasters, film students, reporters, and doctors. The one thing they have in common is that they see the potential for people to use technology in a way that helps them engage more with the world around them, rather than distract them from it. In fact, many Explorers say because of Glass they use technology less, because they're using it much more efficiently. We know what you're thinking: "I'm not distracted by technology". But the next time you're on the subway, or, sitting on a bench, or in a coffee shop, just look at the people around you. You might be surprised at what you see.

Myth 4 - Glass is ready for prime time

Glass is a prototype, and our Explorers and the broader public are playing a critical role in how it's developed. In the last 11 months, we've had nine software updates and three hardware updates based, in part, on feedback from people like you. Ultimately, we hope even more feedback gets baked into a polished consumer product ahead of being released. And, in the future, today's prototype may look as funny to us as that mobile phone from the mid 80s.

Myth 5: Glass does facial recognition (and other dodgy things) Nope. That's not true. As we've said before, regardless of technological feasibility, we made the decision based on feedback not to release or even distribute facial recognition Glassware unless we could properly address the many issues raised by that kind of feature. And just because a weird application is created, doesn't mean it'll get distributed in our MyGlass store. We manually approve all the apps that appear there and have several measures in place (from developer policies and screenlocks to warning interstitials) to help protect people's security on the device.

Advertisement

Myth 6: Glass covers your eye(s)"I can't imagine having a screen over one eye..." one expert said in a recent article. Before jumping to conclusions about Glass, have you actually tried it? The Glass screen is deliberately above the right eye, not in front or over it. It was designed this way because we understand the importance of making eye contact and looking up and engaging with the world, rather than down at your phone.Myth 7 - Glass is the perfect surveillance deviceIf a company sought to design a secret spy device, they could do a better job than Glass! Let's be honest: if someone wants to secretly record you, there are much, much better cameras out there than one you wear conspicuously on your face and that lights up every time you give a voice command, or press a button.

Myth 8 - Glass is only for those privileged enough to afford it

The current prototype costs $1500 and we realize that is out of the range of many people. But that doesn't mean the people who have it are wealthy and entitled. In some cases, their work has paid for it. Others have raised money on Kickstarter and Indiegogo. And for some, it's been a gift.

Advertisement

Myth 9 - Glass is banned... EVERYWHERESince cell phones came onto the scene, folks have been pretty good at creating etiquette and the requisite (and often necessary) bans around where someone can record (locker rooms, casino floors, etc.). Since Glass functionality mirrors the cell phones ("down to the screen being off by default), the same rules apply. Just bear in mind, would-be banners: Glass can be attached to prescription lenses, so requiring Glass to be turned off is probably a lot safer than insisting people stumble about blindly in a locker room.

Myth 10 - Glass marks the end of privacy

When cameras first hit the consumer market in the late 19th century, people declared an end to privacy. Cameras were banned in parks, at national monuments and on beaches. People feared the same when the first cell phone cameras came out. Today, there are more cameras than ever before. In ten years there will be even more cameras, with or without Glass. 150+ years of cameras and eight years of YouTube are a good indicator of the kinds of photos and videos people capture--from our favorite cat videos to dramatic, perspective-changing looks at environmental destruction, government crackdowns, and everyday human miracles.