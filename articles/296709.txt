Kindred Healthcare Inc. (KND) announced after the bell Monday that it will commence a cash tender offer to acquire Gentiva Health Services (GTIV) on Tuesday, for $14.50 per share in cash. Gentiva is now up 0.55 on 30K shares.

Gentiva Health Services traded in a narrow range throughout Monday's session and closed up by 0.10 at $13.99. The stock finished near the middle of a 1-month range at the highs of the year.

For comments and feedback contact: editorial@rttnews.com

Business News