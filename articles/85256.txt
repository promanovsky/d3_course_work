Amazon’s latest product appears to be a small stick used to make a virtual shopping list.

Late last week, Amazon introduced Dash, a sort–of magic wand that has multiple smaller components inside which collectively aim to simplify shopping. Though the device is restricted to shopping only via Amazon, it is nonetheless a unique and potent innovation that could significantly improve upon one of the oldest chores; Grocery Shopping. Interestingly, Amazon Dash can also help shop other items, but its usefulness seems exceptional in the everyday products.

Amazon Dash is intricately connected to AmazonFresh, the company’s attempt to deliver fresh farm produce to the customers’ doorsteps. Amazon Dash is a thin, wand-like device, that includes both a microphone and a barcode scanner, reports Re/code.

Built with a LED Barcode scanner as well as speech–recognition system, coupled with a microphone simplifies making a shopping list. Users can either scan the barcode of the product or speak the type of product or its name and Amazon Dash collects and collates the information into a shopping list, reports Engadget.

Amazon Dash connects to the home WiFi of the user to interface with AmazonFresh, where users can cull or add to the list. Once the shopping list is finalized, users have to hit the buy–button, pay for the same and the products are home–delivered in Amazon branded grocery bags. Though not immediately clear, the video also shows AmazonFresh delivery guy taking away bags, which could be for recycling or reuse.

Couple the Dash with the same day promise of AmazonFresh and the company has a powerful product that is way easier than a shopping list app found on iOS or Android. The beauty of the as–yet gimmicky platform is its intuitive and non-intrusive nature that allows adding items to the shopping list as and when the need is felt instead of sitting down specifically to make a shopping list.

Apart from the launch of a smart Set–Top–Box (STB), Fire TV, that The Inquisitr covered, there is one another small addition to the list of products that Amazon has launched to help enhance its sale. Called Flow, Amazon launched the feature as an in app in February, reported Journal Sentinel. Flow allows users to point the phone at a product in their home — say, a book or a bottle of shampoo — and Flow is supposed to quickly display the product page on the phone’s screen.

Amazon is trying multiple endeavors that make it easier and convenient to shop on its platform. With Dash and Flow, it has smartly reduced the efforts to make a shopping list.

[Image Credit | Engadget]