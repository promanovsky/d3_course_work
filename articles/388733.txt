The data is in, and Google’s new stealthier, blended-in, less-obviously-an-ad offerings have boosted clickthroughs tremendously: They’re up 20 percent just in the last quarter.

That should be good news for Google when the company reports Q2 earnings tomorrow.

A massive Adobe ad study of over $2 billion in ad spend shows that not only are clickthroughs up, so are costs: CPCs rose slightly by four percent over the same period. In contrast, Yahoo/Bing search ad CPCs declined.

Why?

“Mobile and tablet are having an impact there,” Adobe director of product marketing Tim Waddell told me yesterday. “Not to mention the switchover to Google’s new ad types … going away from the different background colors.”

Google Ads used to be obvious, with a different background color than the rest of the page, which clearly highlighted them as commercial content. At the beginning of this year, however, Google changed its format, giving ads the same white background as the rest of the page and replacing it with a substantially smaller “Ad” notification.

New Google ads versus old Google ads Adobe

That change seems to have an almost instant impact on Google revenue via a massive jump in clickthroughs. Note that Google’s clickthrough had declined compared to Yahoo/Bing starting in late 2013, but them suddenly jumped near the beginning of 2014 … which corresponds with the timeframe that Google changed the format.

Google CTR versus Yahoo-Bing Adobe

A 20 percent jump in clickthroughs may not seem like a big deal, but to digital marketers who scratch and bite and claw for fractions of a percent improvements in their online campaigns, this is absolutely huge.

The interesting part, of course, is that both clickthroughs (CTR) and prices (CPC) are up. As any digital marketer knows, a fundamental Google Adwords principle in individual campaigns is that CTR and CPC are inversely connected. In other words, a more efficient campaign with a higher clickthrough rate should have lower costs per click. That results in more dollars for Google, sure, but also more marketing at a better overall rate for advertisers.

Cost per click trends: slightly up Adobe

And, theoretically, since people are voting with their mice, a better user experience for web searchers to boot.

So it’s interesting to see that both prices and CTR are up. Waddell attributed that to a rising tide: more digital spend. Essentially, more marketers are going digital and driving up costs, even as clickthroughs are significantly up. The question that is yet unanswered, Adobe said in a briefing, is whether this is leading to higher ROI for advertisers.

In other trends, mobile continues to be up. (So what else is new?)

“Before Google’s relatively new enhanced campaigns, mobile was just a checkbox,” Waddell told me. “Now it’s very unique … the trend is that desktop is about 70 percent, but we think mobile is where the growth is happening.”

Desktop could drop as low as 60 percent this calendar year, he said, and while tablets are stable at 14 percent of search volume, smartphones are continuing to grow.

Overall, Adobe expects paid search to grow a healthy 10-12 percent in the last quarter of this year, with Google’s share of the market dropping slightly to 78 percent as Yahoo/Bing capture a couple of extra points.