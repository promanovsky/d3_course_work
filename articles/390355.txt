Flames seen rising from part of the wreckage of a passenger plane carrying 295 people after it was shot down as it flew over Ukraine. (AP)

Several passengers on board the Malaysia Airlines Flight MH 17, shot down over Ukraine, were world-renowned researchers heading to an international AIDS conference in Australia, officials said Friday.

''A number of people'' on board the Boeing 777 were en route to the southern Australian city of Melbourne to attend the 20th International AIDS conference, which starts Sunday, Australian Foreign Affairs Minister Julie Bishop told reporters in Brisbane.

The plane, which was flying from Amsterdam to Kuala Lumpur, Malaysia, crashed Thursday with 298 people on board. American intelligence authorities believe a surface-to-air missile brought the aircraft down but it was not yet clear who fired the missile.

At least 27 Australians were confirmed to be on board the plane, which was scheduled to continue flying to the western Australian city of Perth after stopping in Kuala Lumpur, Bishop said.

Our focus now is to work with the emergency responders and mobilize its full support to provide all possible care to the #MH17 next-of-kin. — Malaysia Airlines (@MAS) July 18, 2014

Among the passengers was former president of the International AIDS Society Joep Lange, a well-known HIV researcher from the Netherlands, opposition leader Bill Shorten said in parliament.

''There are Australians who would have planned to be at the airport tomorrow night to greet friends and family - amongst them, some of the world's leading AIDS experts,'' Shorten said. ''The cost of this will be felt in many parts of the world.''

Former U.S. President Bill Clinton will deliver an address at next week's AIDS conference, which brings together thousands of scientists and activists from around the world to discuss the latest developments in HIV and AIDS research.

House of Representatives Speaker Bronwyn Bishop called for a moment of silence in parliament to honor the victims, adding that she was scheduled to address the AIDS conference on Monday.

''I know there will be many empty spots,'' Bishop said. ''And I think that what we're doing is mourning with all of the world and all that had been lost. And we want to see justice but in a measured way.''

The International AIDS Society issued a statement expressing its grief over the news that several of its colleagues and friends were on board.

''At this incredibly sad and sensitive time the IAS stands with our international family and sends condolences to the loved ones of those who have been lost to this tragedy,'' the group said.