On Sunday night Australian time, the intergovernmental panel on climate change (IPCC) will release its final working group report on reducing carbon pollution. These “mitigation” reports have been historically the bridesmaids to the earlier reports on climate science which get more of the media attention.

Because the report's conclusions will shape the global discussion about what all major emitters should be doing next on climate change policy, it may be more important for Australian and global climate policy than the other two reports combined.

Based on leaked drafts of the work of the world’s leading climate economists, the IPCC are likely to make three important overall conclusions.

The first is that avoiding dangerous global warming of 2oC is possible, but urgent action is required across all major countries and emitting sectors. Turning our coal, oil and gas based energy system to one based on clean energy sources like wind and solar, improving the energy efficiency of our buildings, industries and transport sector, stopping deforestation and deploying technologies that remove carbon pollution from the air are all essential ingredients to effective action. This is possible with political will and sensible, consistent policy.

Secondly, the IPCC is likely to also illustrate that well executed climate policy decisions can provide broader benefits to communities, such as reductions in air pollution and improvements in energy security. Delaying effective action will also substantially increase the cost of achieving long-term climate goals. Those factors are a key driver behind growing global action on renewable energy and energy efficiency.

Thirdly, effective national climate policy should include a suite of policies: carbon pricing, regulations to overcome barriers to action, and long-term investment signals (such as Australia’s renewable energy target scheme) are all essential.

But beyond all this common sense, the real sting in the tail for the government will be what the IPCC says around the scale of emission reductions needed to contribute our fair bit to global action.

In its last report, the IPCC indicated that countries like Australia would need to reduce emissions by 25-40% on 1990 levels by 2020, and 80-95% by 2050, to give a 50/50 chance of avoiding a 2oC increase in global temperature. This target range became a benchmark by which national targets were discussed. Many – including Australia, the USA, the EU, Norway and Japan – indicated their willingness to reduce emissions across this scale in advance of the Copenhagen climate summit.

The new IPCC report will have a similar impact, likely pointing out that countries like Australia need to reduce emissions by 50% on 2010 levels by 2030 to be consistent with the agreed global goal of avoiding 2oC, while major emerging economies like China would need to see emissions peak and begin to fall.

These conclusions are not emerging in a void.

Last December, countries agreed to advance new post 2020 emission targets by April 2015. European Union member countries are likely to be the first to come forward with a 2030 emission target. The US is working with China on sharing information on their targets, and the US government has established an interagency working group to develop their new target by April 2015. These targets will be examined internationally before the new global climate agreement is finalised in Paris in December 2015.

Of course, countries want to see the commitments others are making before they sign on the dotted line in Paris. They want to look under the bonnet at what others are putting forward, so they don’t buy a clunker. A key criterion in this evaluation will be whether each country’s target is consistent with a fair contribution to avoiding warming of 2oC. The IPCC’s conclusions will shape this global conversation.

The government is yet to outline what preparations Australia is making for its post-2020 target, or whether we will join other major emitters in advancing our initial target offer by April 2015 (as we have agreed to do in Warsaw last year).

The ALP has been no less committal, and continues a slip and slide on emission targets that started in 2009. Its policy to reduce emissions through an internationally linked carbon limit and price can achieve emission targets at low cost. However, the ALP keeps deferring a decision on its exact targets, despite initiating a number of independent expert reviews on what they should be. Those experts have recommended at least a 15% reduction in emissions by 2020.

It’s clear that neither the government nor the ALP want us to discuss what contribution Australia is going to make to the Paris agreement and what emission goals we should set post 2020. Other governments are asking politely now, but as the 2015 climate summit gets closer, these questions will get more pointed. This weekend’s report from the IPCC will just be the start.