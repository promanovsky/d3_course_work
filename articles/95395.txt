Over the past two decades teen births have declined to the lowest level ever recorded, but 86,000 teens between the ages of 15 and 17 still gave birth in the U.S. in 2012.

Teen birth comes with consequences such as "medical risks and emotional, social, and financial costs," a Centers for Disease Control and Prevention (CDC) news release reported. Becoming a teen Mom can also affect the individual's chances of graduating college.

Only 38 percent of teens who gave birth at the age of 17 or younger graduated high school by the time they were 22, this number jumped to 60 percent for teens who had a child at the age of 18 or older. Eighty-nine percent of teens who do not have children earn their high school diploma.

The CDC suggests teens should use Long-acting reversible contraception (LARC) such as intrauterine devices (IUDs) and hormonal implants that do not require taking a pill every day or other tasks. Nine out of 10 teens claim to have used some form of contraception the last time they had engaged in intercourse but only one percent used LARC. Condoms and birth control pills were among the most popular methods of contraception.

Sex education is also crucial in preventing teen pregnancy. About eight in 10 teens did not receive sex education before the first time they had sex. Parents can also play a huge role in talking to their children about using contraception or abstaining from sex.

The CDC believes more has to be done to prevent pregnancy in teens. They suggest healthcare providers should provide "confidential, respectful, and culturally appropriate services that meet the needs of teen clients," the news release reported.

Healthcare providers can also work to encourage teens to wait to have sex, offer contraceptive advice, and counsel teens about the importance of condom use.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.