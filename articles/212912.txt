NEW YORK (TheStreet) -- Walmart (WMT) wasted no time Thursday blaming the weather for its poor performance in the first quarter.

"Like other retailers in the United States, the unseasonably cold and disruptive weather negatively impacted U.S. sales and drove operating expenses higher than expected," President and CEO Doug McMillon said in the statement.

Walmart shares fell to $76.75 recently and are down 2.5% for the year to date.

Besides mentioning the bad weather conditions of earlier this year the retailer reported quarterly earnings profit fell 5% over the same period last year, leaving behind a weak outlook for investors. First-quarter sales increased only 0.8%. The retailer earned $3.6 billion, or $1.10 a share, down from $3.8 billion, or $1.14 a share, a year ago.

I view blaming the weather for an earnings miss as an even deeper indicator of more underlying problems.

The press release told the truth about Walmart's performance on fundamental principles. Walmart said severe weather was to blame for its relatively flat sales. Wall Street expected the retail giant to deliver earnings of $1.15 per share on revenue of $116.27 billion, according to the consensus estimate from Thomson Reuters.

Increasing store traffic in the U.S. was one of the challenges for Wal-Mart during the first quarter. Store traffic fell 1.4% during the period, offset by a 1.3% boost in the average ticket of customers. Walmart did report positive same-store sales in the remainder 11 weeks of the quarter as weather conditions improved.

The retailer's e-commerce area added 30 basis points to comp during the period. Comparable-store sales growth for the Neighborhood Market format rose to 5%.

The American consumer is not dead.

U.S. consumers are valuable consumers who love to shop. The bottom line is, we are dependant on Walmart in relation to overall retail sales growth. The retailer drives the U.S. economy.

Its performance results indicates challenges are faced with consumers and spending. Terrible weather may have been at fault for Walmart's reduction in first-quarter profits by 3 cents a share and dented sales. However, I am very optimistic about customer resilience.

Walmart is still super shareholder-friendly. The retailer conducts more than one billion transactions a day. The challenges with slowed growth in consumer shopping are apparent as consumers account for 70% of the economy, suggesting economic recovery isn't as robust as analysts say.

Walmart has also been struggling in areas such as competition, international growth, currency issues, government and the rise of health care costs.

What excuses will investors not accept from Walmart?

At the time of publication the author had no position in any of the stocks mentioned.

This article represents the opinion of a contributor and not necessarily that of TheStreet or its editorial staff.

>>Read more: Sozzi: 3 Amazing Walmart Stats to Not Forget this Spring