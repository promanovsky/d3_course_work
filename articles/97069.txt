Vaio’s 2-in-1 laptop has a unique look and a cornucopia of features, but it’s price keeps this jack-of-all-trades from truly standing out.

The Vaio Z Flip is a 2-in-1 laptop that does literally all the things. It’s an enterprise-grade laptop that plays games (from three years ago), travels well, connects to any projector, takes free-hand notes, flips into a tablet, scans documents, and, in a pinch, acts as a pretty stylish paperweight.

Unfortunately, devices that do everything are rarely inexpensive, and Vaio’s 2-in-1 laptop is no exception.

Spec sheet CPU: 3.3GHz Intel Core i7-6567U (dual-core, 4MB cache, up to 3.6GHz)

Graphics: Intel Iris Graphics 550

RAM: 16GB LPDDR3 (1,866MHz)

Screen: 13.3-inch, QHD (2,560 x 1,440) capacitive touchscreen

Storage: 512GB PCIe SSD

Ports: 1 x SuperSpeed USB 3.0 w/charge, 1 x SuperSpeed USB 3.0, HDMI, Headphone/Microphone combo jack, SD card reader

Connectivity: IEEE 802.11a/b/g/n/ac WiFi and Bluetooth 4.1

Camera: Front 1MP CMOS sensor HD web camera, Rear 8MP CMOS sensor HD web camera

Weight: 2.96 pounds (1.34kg)

Size: 12.76 x 8.48 x 0.59-0.66 inches (32.4 x 21.5 x 1.5-1.68cm; W x D x H)

Pricing and availability

At $2,399 (about £1,890 and AU$3,160), the fully kitted-out configuration of the Vaio Z is a bank breaker. Meanwhile, comparatively spec’d 2-in-1’s like the Lenovo Yoga 910 and the HP Spectre x360 top out at a more reasonable $1,649 (£1,749, AU$ 2,599) and $1,399 (£1,799, AU$ 2,899) , respectively – and both feature sharper 4K screens to boot. With the Z Flip, you’re paying a premium for a stylus, Intel Iris graphics card, and a nifty hinge.

The Z Flip’s high price makes it impossible to ignore some notable flaws: a shallow keyboard, finicky touchpad, and extremely loud fan. Creatives may be willing to ignore these downsides because of the Z Flip’s excellent display and better graphics, but enterprise and casual users will not be as forgiving, especially at this price point.

Design

The Vaio Z Flip’s charcoal panelling is a refreshing change from the MacBook “inspired” designs of its competitors. Said panelling hangs slightly over the device’s corners, giving the Z Flip a distinct winged profile that is just as eye-catching as any ultra-clean Apple device.

Eye-catching is also the best way to describe the Z Flip’s transformation into a tablet. A second, hidden hinge located in the back panel allows the display to fold backwards into a tablet.

The only negative to this final form is that the screen doesn’t lie flush against the base, making the tablet configuration a little awkward to handle. You'll also have to engage a release switch in order to convert the laptop into it's tablet modes.

Awkward tablet handling aside, the Z Flip is indeed a svelte device. Its 2.96 pound, 0.59 inch high frame is Ultrabook -like – especially impressive when considering the goodies packed inside. And despite its featherweight appearance, it’s not fragile. This 2-in-1 took some punches, or rather, some tosses into my messenger bag without suffering any injury.

Imperfect inputs

First, the highlights: the Z Flip’s touchscreen and stylus are responsive and precise – in short, they’re excellent. The Z Flip’s tablet interface is what you’d expect from a 2-in-1 that costs over two grand.

Now for the not so highlights: the keyboard and touchpad. At first blush they appear top-notch, but in actual use they’re far from perfect.

The keyboard has all the typical trappings of an expensive enterprise-grade laptop: large fonts, appropriately spaced keys, backlighting, and quiet and snappy feedback. But all this great engineering is superfluous because the key travel is so shallow. What’s the point of all the shiny stuff if the keyboard can’t comfortably type?

The touchpad has its high-end touches too: it’s oversized and positioned perfectly on the base. Palm detection is excellent. So is the click feedback. But scrolling is sometimes frustratingly imprecise, as is clicking on tabs. Too often tabs “catch” on the pointer and are accidently dragged out of the tab bar.

Psychedelic colors

Like many 2-in-1’s, the Z Flip’s screen is essentially that of a glossy tablet. Both bezel and display sit in one level plane, mutually covered in a sheet of gorilla glass. And like all glass encased things, the display hates both direct sunlight and smudgy fingers. Clean it often and stay away from windows. That said, the display is easily the highlight of the device.

With its 2,560 x 1,440 resolution, the display delivers sharp, clear images. Its color vibrancy, however, is the real MVP here. “Guardians of the Galaxy” sparkled on the Z Flip, its sci-fi color palette was so rich and psychedelic we nearly had an acid flashback.

What more could you ask for in a laptop display?