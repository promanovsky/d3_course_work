Weird Al's Latest Turns 'Blurred Lines' Into a Grammar Lesson (Video)

"Word Crimes," the second single/video from his new album, "Mandatory Fun," is a spirited spoof.

Only “Weird Al” Yankovic could turn Robin Thicke’s salacious video smash “Blurred Lines” into a lesson on grammar, spelling, correct word usage and punctuation suitable for viewing in school classrooms everywhere.

PHOTOS Miley Cyrus, Selena Gomez and Robin Thicke Fire Up Z100's Jingle Ball

The second single/video from his new RCA album, Mandatory Fun, which hit stores and online retailers today, follows yesterday’s release of “Tacky,” his spoof of Pharrell Williams’ “Happy,” released through the site Nerdist.com.

The animated clip, the second of eight he will release over the next week to promote the album, takes on such thorny dilemmas as the correct use of “its” and “it’s,” the Oxford comma and the double negative in “I could care less.” The pseudo-lyric video includes a remarkable T.I. impression that includes the line, "Always say 'to whom,' don't ever say 'to who'/And listen up when I tell you this: I hope you never use quotation marks for emphasis."

STORY 'Weird Al' Yankovic Gets 'Tacky' with Pharrell Williams Spoof (Video)

This morning, ABC’s The View hosted the broadcast debut of “Word Crimes,” followed by additional premiere support from Vevo online. Yankovic’s other production partners for the world premiere videos include CollegeHumor, Yahoo (Principato Young Entertainment), Funny or Die and TruScribe, as well as talented auteurs like Liam Lynch, Jarrett Heather and Tim Thompson. Other videos will premiere on Amazon, PopCrush and The Wall Street Journal’s Speakeasy blog.

In interviews, Yankovic, whose label deal runs out with this album, has talked of releasing his songs individually in the future, using his eight-videos-in-eight-days as a model.