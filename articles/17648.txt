Apple Inc. (NASDAQ:AAPL) has launched a cheaper variant of the iPhone 5C in Europe. The 8GB version of the iPhone 5C first appeared on British telecom company O2’s UK website. Soon, it was available on Apple’s official web store in the UK and France. The UK Apple store lists its price at £429, while the company’s French website has priced the device at 559 euros. The 8GB version is still not available on Apple’s US web store.

Apple tries to revive flagging sales of the iPhone 5C

It’s Apple Inc. (NASDAQ:AAPL)’s attempt to boost the sales of the mid-tier iPhone 5C. On O2’s UK webpage, the 8GB iPhone 5C price ranges from free to £409.99. The unsubsidized price is about £100 ($166) lower than the price of 16GB version. All the hardware and software specifications will remain the same, except for lower storage capacity. According to a March 6 report by PhoneArena, poor sales of the iPhone 5C resulted into an inventory pile up of more than 3 million units.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Apple Inc. (NASDAQ:AAPL) CEO Tim Cook acknowledged in January that the iPhone 5C sales have been weaker than expected. The tech giant sold more than 51 million iPhones during the fourth quarter of 2013. The iPhone 5C sales have lagged because it had relatively higher price tag than the markets were expecting for a plastic, so called “low-cost” smartphone. Dozens of Android-based phones offer much better specifications at much lower price. Anyway, the iPhone 5S, the premium version, continues to sell like hot cakes.

Apple hasn’t yet discontinued iPhone 4S

The debut of 8GB iPhone 5C comes only a day after a leaked email from O2 Germany revealed that an 8GB version of the device will be unveiled this week with a lower price tag. As of writing this article, Apple Inc. (NASDAQ:AAPL)’s entry-level smartphone, the iPhone 4S, continues to appear on O2’s website as well as Apple’s official UK web store. Many experts were saying that the iPhone 4S may be discontinued in favor of the 8GB iPhone 5C.

Apple Inc. (NASDAQ:AAPL) shares were down 0.14% to $526 in pre-market trading Tuesday.