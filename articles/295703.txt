When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up fornow for the biggest moments from morning TV

The District Attorney handed the matter over last week (12Jun14) after a police investigation failed to unearth information linking Bieber to an alleged felony theft.

And now the pop star won't be prosecuted at all, according to TMZ.com.

The mother of a fan claimed Bieber had attempted to take her cellphone from a handbag after catching her trying to take photos of him at an arcade in Sherman Oaks, California.

The Baby hitmaker is hoping for a double legal boost this week - Bieber is expected to learn if he'll be facing any charges for egging a neighbour's house during a dispute earlier this year (14) in the next couple of days.