This is what the iPhone 6 could look like, based on rumors, and leaked drawings that allegedly lay out the schematics for the next iPhone.

We don't know if this is what it will actually look like, but this is as good a guess as any. It comes from French site, Nowwhereelse.fr.

Advertisement

The iPhone 6 will supposedly come in a 4.7-inch screen size and a 5.5-inch screen size.(As an aside, if you were wonder if Apple has lost its mystique, or its cool, the answer is no! People are still making drawings of non-existent products.)