Why are we here? What is the purpose of life on earth? And why are alien robots so hellbent on impersonating our automobiles?

It’s the money shot of the whole Transformers series, of course: a sleek yellow Camero reassembling itself into a gleaming 36ft robot, closely modelled on a Swiss Army knife, which then slices down the inside lane of the highway like an Olympic skater.

But why are they posed as a yellow Camero in the first place? Are they on a secret mission to offer drivers advice at busy intersections? They don’t seem very secret, not after the thrashing they gave Chicago in the last movie. That’s all they seem to do: take out their anger on local architecture, like bored teens.

Chicago gets it in the neck in the new one, too, as does Hong Kong, although curiously, by the end I had counted only two human deaths. I’ve seen deadlier paper bags.

Transformers: Age of Extinction takes place five years after the last film. Shia LaBeouf is gone, recovering from imminent credibility collapse, and in his place we have Mark Wahlberg, who plays an amateur inventor named Cade Yeager, a man fighting a losing battle with the length of his daughter Tessa’s skirts, up which director Michael Bay trains his camera at every opportunity. Someone should really tell her not to wear so much white on a farm.

“I know things have been sucky around here lately,” says Dad, “but one day I’m going to build something that matters” – which may be a little lacking as paternal advice but a great summary of the franchise’s history, aims and artistic intentions.

This one gets kicked off when Cade discovers the rusty chassis of an old 18-wheeler truck in an abandoned cinema – what does he think it was doing in there? Catching the matinee? – only to find that it is none other than Optimus Prime, the robot hero of the first three films. This brings a flock of CIA helicopters to his backyard, each bearing a crew of black-op knuckleheads, a jam from which Cade and his daughter are extricated only by the last-minute arrival of a sandy-haired, blue-eyed Irish hunk who arrives out of nowhere in a logo-festooned rally car.

“His name is Shane and he drives,” says Tessa by way of explanation – by far the best line in the movie, a small miracle of Aristotelian economy, although Shane’s follow up – “I just got picked up by Red Bull” – comes a close second. Really, all characters in a Michael Bay movie should be introduced like this: “His name is Chuck and he bench-presses.” Or: “Her name is Mikaela and she shampoos.” And “He just got picked up by Pepsi-Max.”

Facebook Twitter Pinterest Michael Bay calls his film 'a real epic'. Photograph: Philippe Lopez/AFP/Getty Images

Nothing in the movie even approaches that level of economy. Extinction really does seem to take an age in this film, its running time distended to a lumbering 144 minutes by Bay’s love of check-out-my-shot slow-motion, so we catch the exact angle with which the Transformers pirouette through the air, and the exact number of inches by which they fail to miss an overhead bridge, and the precise scatter-pattern of cratering masonry that results.

This is the Debbie Does Dallas of destruction porn. The real progenitor of these films is not Steven Spielberg, or even Irwin Allen, but Smokey and the Bandit, Honkytonk Freeway and all those other Kentucky-fried demolition derbies that littered up the back end of the seventies with their multiple shunts, pile-ups and smasheroos.

“That was insane!” says one young scientist after the Autobots have torn up much of Chicago’s Michigan Avenue. “It was awesome but it was insane!”



It is also curiously boring. One of the stranger aspects of the Transformer oeuvre is that you can watch all four movies back to back, find your eyes comprehensively boggled, your ears played like timpani, and yet discover that your pulse has not deviated once above a steady 60bpm. Bay has all the attributes of a great action director except the ability to instill fear in an audience. He wants us thrust back in our seats, not on the edge of them, overwhelmed with awesomeness not fretting over what is going to happen next.

The summer blockbuster may originally have pitched battle against outsized antagonists – gigantic Death Stars, giant sharks – but their protagonists were pint-sized, Davids plucking up the courage to face Goliath.

“Aren’t you a little short to be a stormtrooper?” asked a skeptical Princess Leia.

“I don't want to ever feel you could kill that shark,” Spielberg told Roy Scheider while shooting Jaws, filling out his cast with uber-nerds, beta-males and lily livers.

Bay’s snickering giganticism, together with his withering disdain for anything that smacks of weakness, make him very much the man of America’s imperial hour. The Transformer movies delivers a Hobbesian vision of man and machine, in which Goliaths are thumped by even bigger Goliaths, only to be creamed by even more vast uber-Goliaths, in infinite regress.

Does the inside of Dick Cheney’s head look like this? We get jokes about waterboarding and much sucking up to the CIA, where an operative named Harold Attinger (Kelsey Grammer) has put a bounty on the heads of the Transformers and is selling the spare parts to a tech entrepreneur played by Stanley Tucci, who wants to build his own army of the things using a new element called Transformium – a coinage that lets you know that even Bay doesn’t think too much of his own plot. If anyone can spot the flaw in this plan for a Transformer-free world, please speak up.

Tucci is pretty much the only reason to see this graceless leviathan of a movie. Tan, besuited, loosing hell on his underlings (“I wanted boundless! I wanted transcendent!”) he is a fine mixture of camp and fury, more Hollywood than Silicon Valley – as if the actor were channelling his notoriously quick-tempered director.

Like John Turturro before him, Tucci seems to have realise that you don’t fight the ludicrousness of a movie like this – you hold a mirror up to it.

“How do you say ‘Get the fuck out of the way!’ in Chinese?” he barks, after the action has transplanted to China for no other reason than that the last Transformers movie was a big hit in Beijing.

Rather touchingly, a hotel chain has complained about its portrayal in the movie, not understanding that to be turned to rubble in a Michael Bay film is the surest sign that you have arrived, geo-politically, cinematically, existentially.

So is the baton passed. Let the Chinese now fret the stage. Tis’ their turn to withstand the sound and fury of Michael Bay.



Transformers: Age of Extinction is on release now in the US and previews in the UK from 5 July