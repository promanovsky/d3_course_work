NEW YORK (TheStreet) -- Wal-Mart (WMT) - Get Report announced Thursday that it will begin carrying Wild Oats organic food products, selling at prices less than what organic food usually retails for.

The world's largest retailer said it would price Wild Oats products comparably with other items it sells in its grocery section and at least 25% below the cost of other branded organic lines.

Wal-Mart plans to roll out 100 Wild Oats product lines to around half of its domestic stores over the next few months.

Must Read: Warren Buffett's 10 Favorite Growth Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreet Ratings team rates WAL-MART STORES INC as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate WAL-MART STORES INC (WMT) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its revenue growth, good cash flow from operations, reasonable valuation levels, increase in stock price during the past year and notable return on equity. We feel these strengths outweigh the fact that the company shows low profit margins."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

WMT's revenue growth has slightly outpaced the industry average of 6.7%. Since the same quarter one year prior, revenues slightly increased by 1.5%. This growth in revenue does not appear to have trickled down to the company's bottom line, displayed by a decline in earnings per share.

Net operating cash flow has slightly increased to $9,937.00 million or 2.61% when compared to the same quarter last year. The firm also exceeded the industry average cash flow growth rate of -7.47%.

WAL-MART STORES INC's earnings per share declined by 19.8% in the most recent quarter compared to the same quarter a year ago. The company has suffered a declining pattern of earnings per share over the past year. However, we anticipate this trend reversing over the coming year. During the past fiscal year, WAL-MART STORES INC reported lower earnings of $4.86 versus $5.01 in the prior year. This year, the market expects an improvement in earnings ($5.30 versus $4.86).

In its most recent trading session, WMT has closed at a price level that was not very different from its closing price of one year earlier. This is probably due to its weak earnings growth as well as other mixed factors. Turning our attention to the future direction of the stock, it goes without saying that even the best stocks can fall in an overall down market. However, in any other environment, this stock still has good upside potential despite the fact that it has already risen in the past year.

You can view the full analysis from the report here: WMT Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.