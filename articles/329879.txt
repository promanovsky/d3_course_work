Investors have been excitedly waiting to hear that MannKind Corporation (NASDAQ:MNKD)’s diabetes drug has the approval of the Food and Drug Administration. Now that the news is finally here though, Wall Street doesn’t seem too happy—probably because of the warning the drug will carry when it goes to market.

FDA announces Afrezza approval

In a press release today, the FDA said it has approved the inhaled powder Afrezza for diabetes treatment in patients who require mealtime insulin. The inhaled insulin acts quickly and is taken at the beginning of a meal or within 20 minutes of beginning a meal.

DG Value Adds 23.7% In 2020, Plans New SPAC Fund Dov Gertzulin's DG Value Funds returned approximately 19.2% in the quarter ending December 31, 2020, according to a copy of the hedge fund's full-year 2020 letter to investors, a copy of which ValueWalk has been able to review. Following the fourth-quarter performance, DG's flagship value strategy ended 2020 with a positive return of 23.7%. That Read More

The agency said Afrezza’s safety was tested in more than 3,000 patients. Researchers noted significant improvements in the blood sugar measurements of type 2 diabetes patients when Afrezza was taken with oral anti-diabetic drugs. In type 1 diabetes patients though, it met “the pre-specified non-inferiority margin of .4 percent.”

The FDA said MannKind’s drug isn’t a substitute for long-acting insulin and must be used with long-acting insulin in patients with type 1 diabetes. The agency now requires that MannKind conduct more post-marketing studies for the drug, including one for pediatric patients, one to study the risk of pulmonary malignancy, and “two pharmacokinetic-pharmacodynamic euglycemic glucose-clamp clinical trials, one to characterize dose-response and one to characterize within-subject variability.”

Why MannKind is falling

The real catch in this approval is the warning MannKind’s drug carries with it. It’s a boxed warning about acute bronchospasm in patients who suffer from chronic obstructive pulmonary disease and asthma. In other words, patients who have chronic lung problems will not be able to take Afrezza.

Investors may also be reacting to all the capital the company will probably need going forward. It’s going to take a lot of cash to cover four more studies. On Thursday, Seeking Alpha contributor Debra Fiakas of Crystal Equity Research raised some concerns about the company’s need for cash. With the FDA approval, now MannKind has to get production up to capacity and also start a sales and marketing campaign for Afrezza. Fiakas doesn’t think the drug maker can do all of this without getting more capital from investors, and she warned about share dilution.

However, the stock is rallying back. In after hours trading, shares are currently up 3.70%.