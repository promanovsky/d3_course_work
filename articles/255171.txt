European stocks are turning in a mixed performance on Tuesday after sharp gains in the previous session on optimism about the U.S. and amid more hints of easing next week from the European Central Bank. Investors eye further comments by ECB President Mario Draghi later in the day for more clues on whether the ECB will act in June to overcome low inflation.

The Euro Stoxx 50 index of eurozone bluechip stocks and the Stoxx Europe 50 index, which includes some major U.K. companies, are little changed, while the German DAX is up 0.3 percent at a record high. French shares are slightly subdued, while the U.K.'s FTSE 100 is adding half a percent as trading resumed following a public holiday yesterday.

Financials are mostly higher, with French lender BNP Paribas gaining 0.2 percent, Germany's Deutsche Bank adding 0.9 percent and shares of Royal Bank of Scotland rising 0.6 percent in London.

Lloyds Banking Group Plc is rallying 1.4 percent after the British banking major said it plans to sell about 25 percent of the existing ordinary shares in TSB Banking Group Plc to meet European Commission's ruling related to state aid.

Rio Tinto Plc shares are declining 0.6 percent in London. The Anglo-Australian mining giant announced that the company and its partners Aluminum Corp of China and the International Finance Corp, the private-sector arm of the World Bank, have signed the Investment Framework with the Government of Guinea for blocks 3 and 4 of the Simandou project.

Other resource stocks are edging higher, with Vedanta Resources, BHP Billiton and Glencore Xstrata rising between 0.1 percent and 0.6 percent.

Aveva Group Plc shares are up 11 percent after the developer of computer software and services for engineering and related solutions reported higher profit in its fiscal year 2014 and declared higher dividend.

InterContinental Hotels Group Plc is rallying 5 percent on a report that it rejected a takeover bid. AstraZeneca Plc is losing 2 percent after Pfizer Inc. abandoned its attempt to buy the drugmaker.

Accor shares are up 1.6 percent in Paris. The French hotel group said that its HotelInvest agreed to buy two real-estate portfolios representing 86 and 11 hotels respectively (12,838 rooms) for a total consideration of about 900 million euros.

In economic releases, French households' confidence remained stable at 85 in May, the same score as seen in April and matching economists' expectations, survey results by statistical office Insee showed.

The British Bankers Association said in a monthly report that U.K. mortgage home approvals dropped to an 8-month low of 42,173 from 45,045 in March, falling below consensus estimate of 45,200.

Elsewhere, Asian stocks fell broadly as investors weighed growing diplomatic and economic tensions between China and Vietnam and the political turmoil in Thailand.

Trading in the U.S. index futures indicate a positive open as investors await data on durable goods orders, house prices and consumer confidence along with two manufacturing surveys from the Richmond and Dallas Federal Reserve Banks for further cues on the U.S. economic recovery.

For comments and feedback contact: editorial@rttnews.com