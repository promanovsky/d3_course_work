David Jackson

USA TODAY

Obama administration officials are planning a climate change meeting Wednesday that includes a Democratic donor who has pledged to spend up to $100 million to make climate change a major issue in the November elections.

Billionaire Tom Steyer will be one of several people at the Wednesday meeting devoted to a new report detailing the economic consequences of climate change.

The White House is in the midst of a major climate change push, including the recent release of new rules limiting carbon emissions from power plants.

White House spokesman Josh Earnest said President Obama and aides are soliciting a variety of views on how to confront climate change.

Reuters reports:

"Steyer is a major opponent of the Keystone XL oil pipeline project. The Obama administration is in the middle of a long process to decide whether to approve or reject the pipeline, which would bring oil from Canada to the Gulf Coast.

"Steyer has pledged to spend up to $100 million to make sure climate change is a top issue in the November U.S. elections. His meeting with White House officials illustrates the extent to which the administration wants his support, and the increasing importance Steyer has placed on working with, not against, members of the Democratic Party."