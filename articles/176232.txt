Pharmaceutical giant Pfizer, Inc. (PFE), which is aspiring to buy British drug maker AstraZeneca Plc (AZN.L,AZN), Monday said profit for the first quarter declined from the prior year, with revenues falling 9 percent amid generic competition and adverse currency. Adjusted earnings topped Wall Street expectations and the company backed its full year adjusted earnings outlook.

Net income attributable to the company fell to $2.329 billion from $2.750 billion in the previous year. Earnings per share dropped to $0.36 from $0.38.

Reported earnings were hurt by the non-recurrence of income from discontinued operations attributable to the company's Animal Health and the gain associated with the transfer of certain product rights to Pfizer's joint venture with Zhejiang Hisun Pharmaceuticals Co., Ltd. in China in the year-ago quarter as well as by higher legal charges.

Adjusted earnings per share were $0.57, while it was $0.51 last year. On average, 11 analysts polled by Thomson Reuters expected earnings of $0.55 per share. Analysts estimates typically exclude special items.

Reported revenues fell 9 percent to $11.353 billion from $12.410 billion, with an unfavorable currency impact of 3 percent. Analysts expected revenues of $12.09 billion.

The revenue drop reflected an operational decline of 6 percent, primarily due to the expiration of the co-promotion term of the collaboration agreement for Enbrel in the U.S. and Canada, the ongoing expiration of the Spiriva collaboration in some countries, erosion of branded Lipitor in the U.S. due to generic competition, as well as generic competition for Detrol LA in the U.S.

Total Biopharmaceutical revenues fell 9 percent to $10.479 billion. In the Global Established Pharmaceutical segment or GEP, revenues fell 13 percent to $5.990 billion and revenues for the Global Innovative Pharmaceutical segment or GIP dropped 7 percent to $3.076 billion.

Among main products, cholesterol drug Lipitor's sales dropped 27 percent to $457 million and Lyrica sales rose 8 percent to $1.150 billion.

Arthritis drug Celebrex revenues were 4 percent lower at $624 million and Psoriasis drug Enbrel reported sales of $914 million, up 4 percent from last year. Viagra recorded a sales decline of 19 percent at $374 million.

In late January, Pfizer said it continues to expect full year adjusted earnings per share in the range of $2.20 to $2.30 and adjusted revenues of $49.2 billion to $51.2 billion.

The company Monday confirmed that all components of that adjusted financial guidance remain valid.

Wall Street currently expects fiscal year earnings of $2.25 per share on annual revenues of $49.79 billion.

Ian Read, chairman and CEO, said, ''Despite continuing revenue challenges due to ongoing product losses of exclusivity and co-promotion expirations, I look forward to the remainder of the year given the strength of our mid- and late-stage pipeline, the continued growth opportunities for our recently launched products as well as opportunities for upcoming product launches.''

Pfizer said that due to the applicability of the UK Takeover Code to its proposed combination with AstraZeneca, pending reports from its reporting accountants and financial advisers in accordance with the UK Takeover Code, the company is not currently permitted to confirm or update its 2014 reported earnings per share guidance.

Since Pfizer has recorded several charges during the first quarter of 2014, its previously-issued reported earnings per share guidance is no longer valid.

Last week, AstraZeneca rejected Pfizer's sweetened offer of 50 pounds per share or $84.47 per share, up from its prior offer of 46.61 pounds or $76.62 per share. The revised offer reportedly valued AstraZeneca at $106 billion.

PFE closed down 1.3 percent on Friday at $30.75. The stock is down 1.1 percent in pre-market activity.

For comments and feedback contact: editorial@rttnews.com

Business News