Postal workers have been found to have lower rates of colorectal cancer when compared to other occupations, an apparent result of their lack of sedentary time. jr/jr/Jim Ruymen UPI | License Photo

WASHINGTON, June 17 (UPI) -- New research suggests being sedentary for long periods of time increases the risk of developing certain cancers.

To tease out the relationship between sitting and certain types of cancers, researchers analyzed results from 43 existing health studies, including stats and observations on 4 million study participants and 68,936 cancer cases.

Advertisement

In their analysis, researchers found just for every additional two hours of sitting each day the risk of colon cancer went up 8 percent. The same amount of additional sitting lead to a 10 percent increase in the risk of endometrial cancer and a 6 percent increase in the risk of lung cancer.

The results of the multi-study survey were detailed in the latest edition of the Journal of the National Cancer Institute.

RELATED Foreign workers evacuated from energy plant in Iraq

The important takeaway, the authors of the study said, is that sitting itself is a problem, not necessarily the lack of exercise. In fact, the researcher's analysis found that the addition of exercise did not counteract the negative impacts of excessive sedentary time.

"Cutting down on TV viewing and sedentary time is just as important as becoming more active," said Daniela Schmid, study co-author and researcher at the University of Regensburg in Germany. "For those whose jobs require them to sit at a desk most of the day, we recommend breaking up the time spent sitting by incorporating short bouts of light activity into the daily routine."

Previous studies have arrived at similar conclusions. Researchers are still trying to understand what it is about sitting -- whether at the office desk or on the couch in front of the TV -- that is so harmful.

RELATED Orton not present at Cowboys minicamp

For now, all researchers can say is: be more active, sit less.

"This recommendation is not limited to people at risk but also applies to the general population," Schmid said, "who will benefit from being more active and sitting less."