The Ralph Lauren Polo Tech shirt makes its debut at the 2014 US Open and the fashion designer behind the sensor-fitted "second skin" expresses hope that the shirts will move into everyday life after making inroads in the world of sports.

With the Open serving as the stage, ball boys have been dressed out in the Polo Tech shirts for several matches taking place at the Billie Jean National Tennis Center, according to Ralph Lauren. Marcos Giron, leading singles player in the Intercollegiate Tennis Association, has been wearing the second skin during his practices.

The technology inside of the Polo Tech was developed by Omsignal, a Canadian firm staffed with experts from the fields of neuroscience and sports medicine, stated Ralph Lauren in a press release.The sensor-knitted compression shirts conform to the contours of the wearers, reporting biometric information from "black boxes" within the apparel to smartphone apps. The biometrics are relayed by the companion apps into the cloud, where OMsignal's algorithms crunch the numbers to deliver data on elements such as the wearer's stress level and respiration.

Stéphane Marceau, CEO of OMsignal, said Ralph Lauren and OMsignal were a natural match for the fabrication of the Polo Tech shirt. It was apparent from the first meeting between the two sides, Marceau stated.

"It was clear from our very first meeting that Ralph Lauren had clarity of mind about the future of fashion tech," said Marceau. "Its legendary Polo brand and unparalleled design and merchandising capabilities make Ralph Lauren a natural partner to bring smart clothing technology into everyday lives."

The value of Polo Tech shirts extends beyond preparation for sporting events, according to David Lauren, senior vice president of advertising, marketing and public Relations for Ralph Lauren. With new research emerging everyday to advocate more active lifestyles as Americans settle into work in the Information Age, Lauren said his company sees the compression shirts transcending the sports to help individuals of any age in all aspects of life.

"Ralph Lauren continues to be at the cutting edge of fashion and culture," said Lauren. "Our goal is to create and reflect the ultimate lifestyle, and we believe that a healthy and active life is an essential part of that. Ralph Lauren is excited to help lead the industry in wearable technology in this ever-evolving, modern world."

Lauren said his company intends to release the OMSignal tech in a variety of shirts over the next year, taking measured steps to learn how to better implement the technology over time.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.