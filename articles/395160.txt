UPDATE: 7:37AM EST: Apple has just unveiled the Macbook Pro more details from AppleInsider

Apple Inc. (NASDAQ:AAPL) would soon announce new updated 13-inch and 15-inch versions of the professional notebooks with more power. It is expected that the newer versions would be announced on Tuesday, and will remain quite similar to the predecessors in outlook. However, the software of the notebooks will supposedly have a major upgrade, primarily in the 15-inch notebook, which will pack a 16GB RAM as standard even for the entry model.

Intel Core i7 powered MacBook Pro

If sources are to be believed then this time, Intel Core-i7 would power the 15-inch version of the Macbook and provide powerful hard drive options as well. Price points are expected to remain same even though the software will be upgraded, according to Apple Insider.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

The 15-inch notebook might be a ‘maxed out’ version, which will be the only product to have a 2.8Ghz Intel Core i7 processor, 16 GB of RAM, a 1TB SSD and Intel Iris Pro and Nvidia GeForce GT 750M graphics. The information comes out from the Apple store in China. A French site MacG claims that the 13-inch version will also be announced on Tuesday, but the specifications are not yet revealed.

MacBook Pro – New notebook expected from Apple

In year 2014, Apple updated MacBook Air and iMacs, and also lowered the price. Towards the end of this year, a new model of the notebook is expected to arrive with the fine tuning of the best features of the MacBook Air range and Pro notebooks. The device could have a 12-inch Retina display and a fan–less design, which means that it will have almost zero sound. Additionally, the notebook will also have button-less touchpad.

A new computer is not expected at least before the launch of the version of OSX, Yosemite, which will be launched most probably in October.

Also, it is expected that there will be a standalone monitor with a 4K resolution screen replacing the Apple’s current Thuderbolt Display. The issue with the type of screen is that other companies are, also, able to offer thunderbolt screens at a lower cost, in which case it has become even more challenging for Apple to justify its price of $1,000. The current version does not support USB 3.0 ports, which the company included it in MacBooks since 2012.