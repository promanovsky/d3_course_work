The top 10 new species of 2014 includes a see-through snail, a teeny tiny wasp with fairy wings, and an anenome that lives on the underside of a glacier -- all of which are brand-new to science.

The list was put together by the SUNY College of Enviornmental Science and Forestry’s International Institute for Species Exploration. Since 2008 the institute has worked with a consortium of taxonomists and experts around the world to select the most intriguing species discoveries for each previous calendar year.

This year’s selections run the gamut from a 40-foot-tall tree that was hiding in plain sight in Thai forests to microscopic microbes discovered in so-called clean rooms where spacecraft are built. There’s even a new mammal, the olinguito, from the cloud forests in Colombia and Ecuador.

“One of the most inspiring facts about the top 10 species of 2014 is that not all of the ‘big’ species are already known or documented,” said Antonio Valdecassas, chairman of the selection committee and a biologist with the Museo Nacional de Ciencias Naturales in Madrid, in a statement. “One species of mammal and one tree confirm that the species waiting to be discovered are not only on the microscopic scale.”

Advertisement

And in case you think we are closing in on finding all the species on Earth -- we are not. According to a release from the publishers of the list, there are still 10 million species awaiting discovery. That’s 5 times the number that are already known to science.

And now -- the top 10!

1. Bassaricyon neblina, a.k.a. olinguito: This 4.5-pound cutie was found living in the trees in the cloud forests of Andes mountains. It was the first carnivorous mammal discovered in the Western hemisphere in 35 years. (More about the olinguito).

2. Dracaena kaweesakii, a.k.a. Kaweesak’s dragon tree: A 40-foot-tall tree with soft-sword shaped leaves and cream-colored flowers that grows in the limestone mountains of the Loei and Lopburi provinces of Thailand.

Advertisement

3. Edwardsiella andrillae, a.k.a. ANDRILL anemone: The first species of anemone reported to live on ice! It was found living under a glacier on the Ross Ice Shelf in Antarctica.

4. Liropus minusculus, a.k.a. skeleton shrimp: A tiny translucent shrimp just 1/8th of an inch in length. It was discovered in a cave on Santa Catalina Island off the coast of Southern California.

5. Penicillium vanoranjei, a.k.a. Orange penicillium: A bright orange fungus found in soil from Tunisia.

6. Saltuarius eximius, a.k.a. leaf-tailed gecko: A mottled gecko with a wide leaf-shaped tail that helps it camouflage, discovered in the mountains of northeastern Australia. (More about the bizarre leaf-tailed gecko).

Advertisement

7. Spiculosiphon oceana: A one-celled organism that grows up to 2 inches long in the Mediterranean Sea. It constructs a shell out of dead sponge fragments found on the sea floor.

8. Tersicoccus phoenicis: A microbial species collected in two separate clean rooms where spacecraft are built, thousands miles apart, that is resistant to frequent sterilization.

9. Tinkerbella nana, a.k.a. Tinkerbell fairyfly: A teeny tiny wasp named for Peter Pan’s fairy friend is just 0.00984 inches long -- one of the smallest insects ever discovered. It was found in Costa Rica.

10. Zospeum tholussum, a.k.a. domed snail: A translucent snail discovered in a Croatian cave system that goes nearly half a mile underground. (More about the translucent snail).