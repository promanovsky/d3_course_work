Washington: You’ve heard of Sonic the Hedgehog, the video game character. But how about the half-pint hedgehog, the tiniest one that ever lived?

Scientists on Tuesday described fossils from Canada of a hedgehog the size of a shrew — about 5cm long — that lived 52 million years ago in a rainforest in northern British Columbia during an especially warm time on Earth.

The creature, Silvacola acares, lived roughly 13 million years after an asteroid wiped out the dinosaurs and left the mammals as the dominant land animals. About the length of a person’s thumb, it ate insects, plants and maybe seeds, the researchers said.

“We were surprised by its tiny size, and frankly it threw me for a while and made it difficult to identify,” said University of Colorado paleontologist Jaelyn Eberle, one of the researchers in the study published in the Journal of Vertebrate Paleontology.

Its scientific name means “tiny forest dweller”.

“Today’s hedgehogs, and especially the ones that are kept as pets, are considerably larger. The smallest living hedgehogs are about 4 to 6 inches long, not including the tail,” Eberle said, “but the moonrats (close cousins) can actually be upwards of 18 inches long and weigh a few pounds.”

Hedgehogs are known for the quills they use to protect themselves. The fossils were not complete enough to show whether or not Silvacola possessed quills. Eberle said primitive hedgehogs that lived in Europe around the same time had bristly hair, so Silvacola may have had it as well. Today’s hedgehogs also can curl up into a ball for protection.

Instead of being removed from the surrounding rock, the delicate upper jaw was examined with a high-resolution CT scanner at Penn State University to avoid damaging the tiny cheek teeth.

The earliest known member of the family that includes hedgehogs and moonrats lived about 58 million years ago. Today’s hedgehogs and their relatives live in Europe, Asia and Africa.

The researchers described another interesting mammal called Heptodon found at the same site, Driftwood Canyon Provincial Park. It was a tapir-like creature about as big as a medium-sized dog. It was a herbivore about half the size of today’s tapirs but lacking the short trunk usually seen in these mammals.

The half-pint hedgehog and trunk-less tapir lived in a rainforest along the edges of a lake, but it was not tropical, the researchers said. Rather it was a cooler, upland rainforest, more akin to today’s climate in Portland, Oregon in the US Pacific North-west, about 1,100km south of this location.

“Driftwood Canyon is a window into a lost world, an evolutionary experiment where palms grew beneath spruce trees and the insects included a mixture of Canadian and Australian species, said David Greenwood of Canada’s Brandon University, another of the researchers. “Discovering mammals allows us to paint a more complete picture of this lost world.”