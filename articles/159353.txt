In Satya Nadella's first quarterly earnings release as chief executive, Microsoft Corp. on Thursday posted earnings and revenue that beat Wall Street expectations, offering new justification for the CEO's focus on cloud computing.

Factoring out special items from a year ago, revenue in the January-March quarter grew 8 percent.

Nadella, who replaced longtime CEO Steve Ballmer in February, said in a statement that the quarter's results "demonstrate the strength of our business, as well as the opportunities we see in a mobile-first, cloud-first world."

Microsoft's stock rose $1.09, or 2.7 percent, to $40.95 in after-hours trading after the results were released. That followed a 17-cent gain to $39.86 in the regular session.

"It's a good first quarter of earnings for Satya out of the box," said analyst Shannon Cross of Cross Research. "This was enough to offset concerns that investors might have had that growth was slowing or the impact Amazon or other competitors might be having on the cloud strategy."

The company also benefited from the end of support for its 13-year-old operating system, Windows XP, on April 8. Many companies upgraded computers en masse to make sure they'd continue to receive support from Microsoft, including anti-virus updates.

That transition — which helped boost Windows revenue by 4 percent — helped Microsoft buck a 2-4 percent decline in shipments in the quarter as estimated by market research firms Gartner and IDC.

Chris Suh, Microsoft's general manager of investor relations, said it was hard to tell how much Windows revenue benefited from the end of XP. But he said about 10 percent of all Windows computers are still running XP, meaning the company would likely continue to benefit going forward. "It's probably not done. There's a bit of a long tail that will drag out over a long period of time," he said in an interview.

Overall, Microsoft posted flat revenue and a decline in net income for the January-March quarter.

Revenue gains from its Windows operating system and cloud computing services like Azure were offset by the lack of special upgrade offer revenues from a year ago.

Net income in the quarter through March 31 came to $5.66 billion, or 68 cents per share, down from $6.06 billion, or 72 cents per share, in the same period a year ago.

Revenue was nearly unchanged at $20.40 billion, compared with $20.49 billion a year ago.

Analysts polled by FactSet expected Microsoft to post earnings of 63 cents per share on revenue of $20.39 billion.

Revenue from devices and consumer products rose 12 percent to $8.30 billion as the company sold more Xbox One game consoles and consumers upgraded systems from Windows XP, for which Microsoft ended support earlier this month.

Commercial revenue grew 7 percent to $12.23 billion, as revenue grew from its Office 365 productivity software. Azure revenue more than doubled.

Total revenue gains of $1.7 billion from various segments were offset by a $1.8 billion decline in upgrade offers revenue and other items.