Global energy markets are jostling between the return of Libyan crude oil and lingering tensions over Ukraine. It’s domestic supply and demand issues, however, that are weighing on U.S. gasoline prices, AAA said Monday.

Prices waxed and then waned amid dueling overseas developments. Last week, Russian President Vladimir Putin warned European energy security was at risk because of Kiev’s debt obligations. The state-run oil firm in Libya, however, said the port of Zawiya and associated oil infrastructure were open and operating normally after protesters there ended their blockade.

West Texas Intermediate traded Monday morning at $103.74, up 0.34 cents from the previous session, while Brent crude, the global benchmark, traded down 0.13 cents to $107.33.

Crude oil prices account for about 70 percent of the price U.S. consumers pay for a gallon of gasoline. AAA reported a national average price for a gallon of regular unleaded gasoline of $3.64, up nearly 6 cents from last week.

Michael Green, a spokesman for AAA, told Oilprice the national average price for Monday was the highest reported since July 27 because of domestic supply and demand issues. The U.S. Energy Information Administration reported gasoline stocks declined to 210 million barrels, the lowest since November. The onset of warmer temperatures after a long winter season, meanwhile, meant more people were taking to the roads.

“As a result, drivers now are paying more than a year ago with the national average 11 cents per gallon more expensive than last year,” Green said.

Last week’s increase was the largest since February and, while overseas issues were influencing global energy prices, the end of a period of seasonal refinery maintenance at home didn’t mean much for U.S. consumers looking to cure their cabin fever.

Green said most producers have got rid of their winter blend of gasoline stocks in order to make room for the summer blend, which is more expensive to produce. This has resulted in a depletion of gasoline supplies in some parts of the country. California drivers paid more than $4 per gallon Monday, while Illinois drivers saw their state average go above the $3.90 mark.

“Unfortunately for drivers, supplies have dropped more precipitously than expected, which is sending prices higher,” said Green.

EIA later this week is expected to release its petroleum statistics, which could push gasoline prices even higher if supplies are shown to be limited. If the supply situation stabilizes, prices at the pump could start to ease, though Green warned international factors may still come into play.

“The U.S. petroleum market is never fully isolated from events overseas,” he said. “Crude oil is a globally traded commodity, which means that U.S. consumers often pay more at the pumps because of international political instability.”