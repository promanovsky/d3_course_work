Energy utility Wisconsin Energy Corp. (WEC) agreed Monday to acquire energy holding company Integrys Energy Group, Inc. (TEG) in a cash and stock deal valued at $9.1 billion, including assumed debt. The deal, which is expected to close in the summer of 2015, will create a leading Midwest electric and gas utility as well as the 8th largest natural gas distribution company in America.

"We believe this combination provides a unique opportunity to create the premier regulated utility system in the Midwest, with superior service and competitive pricing for years to come," Wisconsin Energy Chairman and CEO Gale Klappa said in a statement.

The deal will see Integrys shareholders receive 1.128 Wisconsin Energy shares plus $18.58 in cash per Integrys share, implying a total consideration of $71.47 per Integrys share, based on Wisconsin's closing share price of $46.89 on Friday.

The offer price represents a 17.3 percent premium over Integrys' closing share price of $60.95 on Friday. The consideration mix is fixed at 74 percent stock and 26 percent cash, with Integrys shareholders owning about 28 percent of the combined company upon closure. The deal has the unanimous approval by the boards of directors of both companies.

The deal positions Wisconsin Energy to deliver enhanced earnings growth, with the deal being accretive to its earnings per share in first full calendar year after closing.

Following the closure of the deal, the combined entity, named WEC Energy Group, Inc., will be headquartered in metropolitan Milwaukee with operating headquarters in Chicago, Green Bay and Milwaukee. The combined company will also hold a 60 percent stake in American Transmission Co. LLC.

Klappa will become chairman and CEO of the combined company, with other senior leadership roles being filled by current senior officers of Wisconsin Energy. Meanwhile, Integrys Chairman and CEO Charlie Schrock will remain in his current roles until the closure, when he will retire.

The two companies have also reiterated commitment to Integrys' 5-year plan to invest up to $3.5 billion in infrastructure and operational initiatives to maintain high levels of reliability and improve customer service.

The companies noted that the combined entity is projected to have a regulated rate base of $16.8 billion in 2015, serving more than 4.3 million total gas and electric customers across Wisconsin, Illinois, Michigan and Minnesota, and operating nearly 71,000 miles of electric distribution lines and more than 44,000 miles of gas transmission and distribution lines.

The deal is subject to approvals from the shareholders of both companies, the Federal Energy Regulatory Commission, Federal Communications Commission, Public Service Commission of Wisconsin, Illinois Commerce Commission, Michigan Public Service Commission and the Minnesota Public Utilities Commission.

"Our shareholders will receive an attractive premium for their investment and will also benefit from the opportunity to participate in the upside of the combination, including future value creation and a growing dividend program," Schrock stated.

Schrock added that Integrys is in the process of divesting its Energy Services' retail electric and natural gas , and is expecting to complete it soon.

The companies stated that "the proposed dividend policy of the combined company will be designed to initially keep Integrys' shareholders neutral after taking into consideration both the stock and cash they received."

Wisconsin Energy said it plans to continue its current dividend policy of a 7 to 8 percent annual increase in the dividend in the period prior to the closing of the deal. The projected payout target for the combined company will be 65-70 percent of earnings in future years.

WEC closed Friday's regular trading session at $46.89, down $0.11 on a volume of 1.89 million shares, while TEG closed at $60.95, up $0.19 on a volume of 0.95 million shares.

For comments and feedback contact: editorial@rttnews.com

Business News