HTC has officially revealed its new flagship handset today (March 25), after witnessing more leaks than a colander.

The HTC One (M8) packs a serious punch in terms of performance and technology - and it needs to if it wants to satisfy the smartphone geek masses.

So, just how well does it stack up on paper? We decided to weight up the M8 against its competition - the Samsung Galaxy S5 and Sony's Xperia Z2.

HTC One (M8) review: "A design that puts its rivals to shame"

Design

The HTC One (M8) is similar to its One predecessor from a design perspective down to the front-facing speaker, except that it weighs 160g instead of 143g. It shares the same 9.35mm depth, which makes it fatter than the 8.1mm-deep Samsung Galaxy S5 and the 8.2mm Sony Xperia Z2.

All three devices come in at under 150mm in height, with the Xperia Z2 tallest and widest and the Galaxy S5 the thinnest - if such things matter to you.

Either way, the aluminium HTC One (M8) should feel as good to hold as the old HTC One and the Xperia Z2, unless you prefer plastic. The Galaxy S5 is tougher than it looks, offering a dust- and water-resistant IP67 rating. The Xperia Z2 is rated at IP58 and the M8 at IP55.



Each device comes in a number of colours. We like the gunmetal M8 best, but the silver and gold options are fetching too. Either way, the new phone is a stylish device but perhaps not too far removed from the One - which may or may not be a bad thing, in your opinion.

HTC One (M8) dimensions: 146.36 x 70.6 x 9.35mm

Sony Xperia Z2 dimensions: 146.8 x 73.3 x 8.2mm

Samsung Galaxy S5 dimensions: 142 x 72.5 x 8.1mm

Performance

HTC has given the M8 a powerful Qualcomm Snapdragon 801 quad core processor clocked at 2.3GHz and 2GB of RAM, which means it should be speedy. The Xperia Z2 and Galaxy S5 have the same Snapdragon 801 processor, but the former has 3GB of RAM while the latter runs at 2.5GHz.

Ultimately, only those with benchmarking software will really care about the difference. These are fast devices that will load up apps, games and other tasks with aplomb, which is what matters.



The M8 benefits from microSD storage cards just like the Xperia Z2 and the Galaxy S5, so you can make up for any storage pitfalls easily. Unlike its rivals, it can support up to 128GB - instead of 64GB in the case of the Xperia Z2.

Display

The HTC One (M8) has a 5-inch Full HD 1080p display, which gives it a pixel-per-inch density of 441. Sony's Xperia Z2 is 5.2 inches in size, which explains why its Full HD display has 423ppi. Samsung's S5 also has a Full HD display at 5.1 inches in size, putting it between its competitors.

Any of the three smartphones will deliver a crystal-clear display. Arguably the Sony offers truer colours, the Samsung richer colours (depending on the display setting) and the One (M8) is a happy medium.

Operating system

All devices have Android 4.4 KitKat. On top of the M8 is HTC's Sense 6, which includes a revised user interface design and gesture shortcuts. Samsung applies TouchWiz 5 to the S5, which is fairly obtrusive but feature-rich. The Xperia Z2 also benefits from a custom skin, which includes Sony Walkman.

HTC



Android enthusiasts will no doubt ditch the standard user interface in favour of a custom launcher to get the most customisable and fast experience. Everyone else will find each of these handsets easy enough to use once acquainted with them. Personal taste is the biggest factor here, as the core functionality is relatively similar.

Camera

The HTC One (M8) has a 4.1-megapixel (yes, you read that right) Ultrapixel 'Duo' camera, which has a secondary lens to capture depth of field. The low megapixel count will be an issue if you plan to heavily crop images, but the quality should be excellent if its predecessor is anything to go by.

At the front is a 5-megapixel snapper for video calls and selfies. Video recording is 1080p.



Sony has opted for sheer megapixels to complement its camera. In fact, it has 20.7 of them, putting it in a league of its own. If it helps, it can record 4K video too. Meanwhile, the Galaxy S5 has a 16-megapixel primary camera, a 2.1-megapixel front-facing camera, and can also shoot 4K video.

In real world use, the Galaxy S5 and Xperia Z2 are excellent - particularly the latter. The One (M8) has its work cut out against the competition here.

Battery life

Sony kicks the Galaxy S5 and M8 hard here. It has a 3200mAh battery, which is a lot more than the M8's 2600mAh and the Galaxy S5's 2800mAh. Interestingly, the M8 has a non-removable battery, which means a spare is out of the question.

Technically the Sony should last the longest, then, but it all depends on usage. Still, having a bigger battery provides peace of mind.

First impressions?

If the HTC One (M8) is as well-built and capable as its predecessor in the performance and photography department, we will be pleased as punch. The Galaxy S5 and Xperia Z2 are fantastic devices, but it seems the new HTC has the firepower to battle both of them convincingly.

Where can I buy the HTC One (M8)?

You can buy it now from most phone shops and networks, actually, which goes against the silly trend of announcing a phone and then making you wait until you are old and grey to buy it. In this respect, the HTC One (M8) has a real chance to strike a blow at the announced-but-still-unavailable Samsung Galaxy S5.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io