Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Don't look now Ashton Kutcher but your fiancee Mila Kunis has been snogging hunky Hollywood actor Channing Tatum.

The 30-year-old actress stars alongside Channing in the Wachowskis' epic sci-fi flick Jupiter Ascending.

The film is set in the future where gods rule over humans and Jupiter Jones (Mila) is an unlucky Russian immigrant who cleans toilets for a living.

She encounters Caine (Channing), an interplanetary warrior who the Queen of the Universe sent to kill Jupiter.

But Caine tells Jupiter that the stars were pointing to an extraordinary event on the night she was born, and that her DNA could mark her as the universe's next leader.

And a great love blossoms. Basically.

(Image: Warner Bros)

But Ashton doesn't have to worry too much - the actress only has eyes for her beau and recently opened up about the pair's low-key date nights, revealing they enjoy “white T-shirt nights”.

She explained: "We have a thing called white T-shirt night and it's when you wear jeans and a white T-shirt and you go on a date.

"Nothing fancy, it's just something that you love to do – whether that's bowling or dinner or going to a cheese shop and have some wine or walking around. I think that's just the best thing to do, and sometimes it's just nice to stay home."

The beaming pair were also spotted smooching at a basketball game over the weekend.

Mila and Ashton sparked engagement rumours earlier this year after she was spotted sporting a huge sparkler.