There have been no measles deaths reported from the outbreak in the United States, and none since 2003. But Schuchat acknowledged that "it's probably just a numbers game, probably just a matter of time until we have more." One or two of every 1,000 cases of measles are fatal, according to the CDC.

AD

AD

California, with 58 cases, has been hit hardest by one of the 13 separate outbreaks of measles in the United States. New York has seen 24 infections and Washington state has had 13.

Measles is a highly contagious respiratory disease that generally affects young children, causing fever, a runny nose, a cough and a distinctive rash all over the body. About one in 10 children also gets an ear infection and one in 20 comes down with pneumonia. A person with measles is contagious as long as four days before the symptoms are apparent, Schuchat said. Parents and even physicians who haven't seen measles in years may be unaware of the early warning signs, she said.

In the past 20 years, a concerted public health campaign, especially among lower-income families, has made measles outbreaks rare. The disease has been considered eradicated since 2000. But today, the number of unvaccinated children has begun to become a problem, Schuchat said. Some people are choosing not to have their children immunized for personal reasons and others are unaware of, or unable to get, vaccinations, before they arrive in the U.S. She said the CDC is also seeing growth in the disease pertussis, also known as whooping cough.

AD

AD

Before vaccinations were available, about 500,000 people were infected with measles annually in the U.S., a number that fell to about 60 after the disease was all but eliminated in 2000. Since 2010, it has increased to an average of 155 cases per year.

Still, she said, fewer than one percent of the toddlers in the United States have received no vaccines at all. "Vaccinating your children is still a social norm in this country," she said.

In a telephone news conference Thursday, CDC Director Thomas Frieden lauded the Vaccines For Children program started in 1994, after a measles outbreak from 1989-1991 resulted in 55,000 cases, primarily because poor and uninsured children had not been immunized. Among children born in the following two decades, the CDC estimated, vaccinations prevented an estimated 322 million illnesses and 732,000 deaths, saving about $295 billion. The program provides free vaccinations for measles and 13 other diseases.

AD

AD

But "we can’t let our successes result in complacency," Frieden said. "In fact measles, is very common in some parts of the world...An it travels fast." He said 20 million people across the globe get the disease annually, and 122,000 of them die.

In a separate commentary Thursday in the Annals of Internal Medicine, epidemiologist Julia Shaklee Sammons warned that "as more parents decline to vaccinate their children, measles incidence is increasing—a fact that alarms me both as a hospital epidemiologist and as a parent of a vulnerable infant too young to receive the measles vaccine." When infected patients seek medical care, she noted, "hospitals and clinics may inadvertently fuel transmission if those with measles are not rapidly triaged and isolated."

The proportion of vaccinated children varies by state, depending on the toughness of their immunization laws, Sammons said. Nationally the measles, mumps, rubella vaccination rate is over 90 percent, but in 15 states it is below that standard, she wrote. New York magazine reported last month on schools in California and New York with low immunization rates among students, in part because parents are choosing not to vaccinate them.

AD

AD

The CDC said that 54 of this year's 58 California cases were in some way associated with importation of the virus from abroad. Twenty-five of the people infected were not immunized--19 of them because of philosophical objections--and 18 more had no documentation of vaccinations. Three were too young for routine vaccination and three others were not vaccinated for unknown reasons.