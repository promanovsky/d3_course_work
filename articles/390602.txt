Sleep disturbances such as apnea may increase the risk of Alzheimer’s disease, while moderate exercise in middle age and mentally stimulating games, such as crossword puzzles, may prevent the onset of the dementia-causing disease, according to new research to be presented Monday.

The findings — which are to be introduced during the six-day Alzheimer’s Association International Conference in Copenhagen — bolster previous studies that suggest sleep plays a critical role in the aging brain’s health, perhaps by allowing the body to cleanse itself of Alzheimer's-related compounds during down time. The studies also add to a growing body of literature that suggests keeping the brain busy keeps it healthy.

The battle against Alzheimer’s disease has become more urgent for the United States and other developing nations as their populations turn increasingly gray. The disease is the leading cause of dementia in older people and afflicts more than 5 million Americans. At its current pace, the number is expected to soar to 16 million people by 2050.

In 2012, the United States adopted a national plan to combat the disease and the G-8 nations last year adopted a goal of providing better treatment and prevention by 2025.

Erin Heintz, a spokeswoman for the Alzheimer’s Association, said U.S. government funding to combat the disease now stands at about $500 million a year. To reach its 2025 goal, the United States should be spending $2 billion a year, she said.

View Graphic One in five U.S. adults shows signs of chronic sleep deprivation

The sleep study, conducted by University of California at San Francisco researchers on a large sample of veterans, found that those with diagnosed sleep disorders such as apnea or insomnia were 30 percent more likely to suffer dementia than veterans without such problems. Veterans who suffered from sleep problems and post-traumatic stress disorder (PTSD) had an 80 percent greater risk.

“I would say that this another important study showing this link between sleep and subsequent diagnosis of dementia,” Kristine Yaffe, a psychiatry professor at UCSF who heads its Dementia Epidemiology Research Group, said in a telephone interview. She said her study’s findings benefited from having such a large sample of participants: researchers used eight years of records on 200,000 veterans, most of whom were male and 55 or older.

It is well known that people afflicted with Alzheimer’s suffer from sleep disorders, Yaffe said, but further research is necessary to determine whether sleep disturbance heightens the risk of getting dementia or is a symptom.

In a separate study, researchers at the Wisconsin Alzheimer’s Institute and the Wisconsin Alzheimer’s Disease Research Center wanted to find out whether middle-aged people who engage in mentally stimulating activities might reduce their risk of cognitive impairment and dementia. Forty percent of the subject group carried the gene linked to Alzheimer's and 74 percent had a parent with the illness, two factors known to increase the risk of getting the disease.

The researchers studied 329 participants — 69 percent of whom were women, whose mean age was about 60 years old — to find out how often the participants read books, visited museums, played games such as checkers or worked on puzzles.

The subjects also underwent a battery of tests, including MRI brain scans to measure the volume of those regions commonly afflicted by Alzheimer’s.

For purposes of the study, researchers focused on the group’s game-playing habits to see if the frequency of playing games was related to better brain and cognitive health.

Stephanie Schultz, lead author of the study, said that although more research is necessary to know for sure, the findings suggest that stimulating the brain with ordinary diversions such as crossword puzzles may help some people preserve brain tissue and cognitive functions that are vulnerable to dementia. Those who reported a higher frequency of playing games also had greater brain volume in regions affected by Alzheimer’s, such as the hippocampus.

“The more they play these types of games, the better it is for . . . brain health,” she said.

One reason could be that game-playing involves more complicated processes across multiple regions of the brain, compared with more passive forms of mental engagement, the researchers said.

“It’s very clear it’s a different quality of mental engagement when you’re playing games of skill than when you’re reading a book,” said Ozioma Okonkwo, an assistant professor of medicine at the University of Wisconsin School of Medicine and senior author of the study. “To win a card game, you have to judge, you have to plan, you have to do something, you have to remember what the last player played.”

Okonkwo said the results were exciting particularly because they held true for people with a family history of Alzheimer’s and a genetic disposition to the disease.

“These individuals already have two strikes against them,” he said.

Similarly, a three-year study of people with mild cognitive impairment by researchers at the Mayo Clinic Study of Aging suggests that moderate physical exercise in middle age could decrease the risk that their cognitive deficits progress to dementia. The study looked at the timing of regular exercise — undertaken either in midlife between the ages of 50 and 65, or later in life, from age 70 and up — and its relationship to the onset of dementia in a group of 280 elderly people. Their median age was 81.

Oddly, however, the association did not hold for people who engaged in light or vigorous exercise in middle age or for any level of physical activity later in life.

On a similarly counterintuitive note, another study suggested that high blood pressure among people at least 90 years old — “the oldest old” — may protect against cognitive impairment. Researchers at the University of California at Irvine said that although hypertension is believed to increase the risk of Alzheimer’s and dementia for middle-aged people, the risk may shift with time.

Their study, which examined 625 people who are 90 or older, found that people who were diagnosed with high blood pressure between the ages of 80 and 89 had a significantly lower risk of dementia. People with hypertension after the age of 90 had an even lower risk, the researchers said.