I saw a t-shirt one time. “I’m a bomb disposal technician,” it read. “If you see me running, try to keep up.”

The same sort of idea can be applied to net security: when all the net security people you know are freaking out, it’s probably an okay time to worry.

This afternoon, many of the net security people I know are freaking out. A very serious bug in OpenSSL — a cryptographic library that is used to secure a very, very large percentage of the Internet’s traffic — has just been discovered and publicly disclosed.

Even if you’ve never heard of OpenSSL, it’s probably a part of your life in one way or another — or, more likely, in many ways. The apps you use, the sites you visit; if they encrypt the data they send back and forth, there’s a good chance they use OpenSSL to do it. The Apache web server that powers something like 50% of the Internet’s web sites, for example, utilizes OpenSSL.

Through a bug that security researchers have dubbed “Heartbleed“, it seems that it’s possible to trick almost any system running any version of OpenSSL from the past 2 years into revealing chunks of data sitting in its system memory.

Why that’s bad: very, very sensitive data often sits in a server’s system memory, including the keys it uses to encrypt and decrypt communication (read: usernames, passwords, credit cards, etc.) This means an attacker could quite feasibly get a server to spit out its secret keys, allowing them to read to any communication that they intercept like it wasn’t encrypted it all. Armed with those keys, an attacker could also impersonate an otherwise secure site/server in a way that would fool many of your browser’s built-in security checks.

And if an attacker was just gobbling up mountains of encrypted data from a server in hopes of cracking it at some point? They may very well now have the keys to decrypt it, depending on how the server they’re attacking was configured (like whether or not it’s set up to utilize Perfect Forward Secrecy.)

The exploit relies on a bug in the implementation of OpenSSL’s “heartbeat” feature, hence the “Heartbleed” name. Security firm Codenomicon has written an in-depth breakdown of the Heartbleed bug here.

To quote their findings:

We have tested some of our own services from attacker’s perspective. We attacked ourselves from outside, without leaving a trace. Without using any privileged information or credentials we were able steal from ourselves the secret keys used for our X.509 certificates, user names and passwords, instant messages, emails and business critical documents and communication.

It seems the bug has been in OpenSSL for 2+ years (since December 2011, OpenSSL versions 1.0.1 through 1.0.1f) before its publicly announced discovery today. Even worse, it appears that exploiting this bug leaves no trace in the server’s logs. So there’s no easy way for a system administrator to know if their servers have been compromised; they just have to assume that they have been.

The bug was discovered and reported to the OpenSSL team by Neel Mehta of Google’s security team. OpenSSL released an emergency patch for the bug along with a Security Advisory this afternoon.