Electronic cigarettes could be banned from sale to young people in Northern Ireland, according to the health minister

Leading scientists have appealed to world health bosses not to clamp down on e-cigarettes.

A total of 53 experts from 15 countries including the UK wrote to the World Health Organisation (WHO) to urge it not to "control and suppress" the devices, saying they have the potential to save millions of lives.

The WHO is preparing to publish recommendations about e-cigarettes to governments later this year.

The letter urged the global health adviser not to impose regulations on the products in the same way it does with conventional cigarettes.

It said: " The potential for tobacco harm reduction products to reduce the burden of smoking related disease is very large, and these products could be among the most significant health innovations of the 21st Century - perhaps saving hundreds of millions of lives.

"The urge to control and suppress them as tobacco products should be resisted and instead regulation that is fit for purpose and designed to realise the potential should be championed by WHO."

The authors of the letter - including experts who have advised the National Institute for Health and Care Excellence (Nice) on its guidelines about reducing the harm from tobacco - published the letter after claiming to have seen a leaked document from the WHO which labelled the e-cigarettes as a "threat".

Professor Robert West, from University College London, who was one of the signatories, said: " For the WHO to suggest that e-cigarettes are as risky as other tobacco products would send an erroneous and bleak message to the millions of current e-cigarette users who have used them to quit smoking.

"It would discourage smokers from trying them and we would miss out on a major opportunity to reduce smoke related deaths globally."

Professor Gerry Stimson, emeritus professor at Imperial College London, said: "If the WHO gets its way and extinguishes e-cigarettes, it will not only have passed up what is clearly one of the biggest public health innovations of the last three decades that could potentially save millions of lives, but it will have abrogated its own responsibility under its own charter to empower consumers to take control of their own health, something which they are already doing themselves in their millions."

Research published last week by Mr West found e-cigarettes can help improve the success rate of people trying to quit smoking by 60% compared to nicotine patches or gum.

But critics say that not enough is known about the long-term effects of the devices, which deliver nicotine in a vapour.

The British Medical Association has backed calls for regulation of the devices.

Its director of professional activities Vivienne Nathanson told BBC Breakfast there was evidence that children who had never smoked were starting to use e-cigarettes.

She said: "Rather like cigarettes in the 50s and 60s, we really need to look at that and, I believe, ban it (advertising), to stop them advertising in a way that attracts children."

Dr Penny Woods, chief executive of the British Lung Foundation, said the overall impact of e-cigarette use on public health is "currently unclear".

"While they could prove to be an important tool to help people stop smoking, the unregulated status of e-cigarettes is problematic," she said.

"The concern is that the safety and effectiveness of e-cigarettes is still unknown. Until we have more substantial research we would encourage all smokers who wish to quit smoking to use 'stop smoking' services and approved nicotine replacement products."

A WHO spokesman told the BBC: "WHO is currently working on recommendations for governments on the regulation and marketing of e-cigarettes and similar devices.

"This is part of a paper that will be submitted to the parties of the WHO Framework Convention on Tobacco Control later this year.

"We are also working with national regulatory bodies to look at regulatory options, as well as toxicology experts, to understand more about the possible impact of e-cigarettes and similar devices on health."