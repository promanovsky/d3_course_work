Activist investor Bill Ackman told CNBC on Wednesday his partnership with Valeant Pharmaceuticals to try to buy Allergan for $46 billion does not violate securities law.

This arrangement, in which Ackman bought a 9.7 percent stake in the Botox maker at the behest of Valeant, was not front-running and not insider trading, Ackman said in a "Squawk Box" interview.

He said his lawyer—Robert Khuzami, former director of enforcement at the SEC—vetted the Valeant partnership and deemed it legal.

"The way the rules work is you're actually permitted to trade on inside information ... as long as you didn't receive the information from someone who breached … fiduciary duty or duty of confidentiality, et cetera," Ackman said.

"Valeant basically came to us and said, 'Look, if you can help us buy Allergan we can work with you.' We said, 'Great,' and we formed a partnership," he said. "The partnership has various terms. It gives us the right and permission from the company to go buy a stake in Allergan."

The founder of the Pershing Square Capital Management hedge fund—with $13 billion in assets under management—said he was not in the market buying Allergan stock before that.

