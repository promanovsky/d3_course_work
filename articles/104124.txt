Folk musician Jesse Winchester, who became a star after moving to the Eastern Townships as a conscientious objector, has died at 69.

Winchester died in Charlottesville, Virginia on Friday, his wife Cindy reportedly told media outlets.

Winchester moved to Quebec in 1967, was granted amnesty in the U.S. a decade later and returned to live in the U.S.A. in 2002.

He had what Rolling Stone Magazine once declared to be “the greatest voice of the decade.”

He was diagnosed with cancer of the esophagus in 2011.

Winchester lived for many years in a log house in Magog and released 14 albums including “Nothing but a Breeze” (1977) which contained a title song reflecting his ambivalence towards being a Southerner living in Quebec. “Me I want to live with my feet in Dixie and my head in the cool blue north.”

He boasted that he was the fifth cousin of Robert E. Lee and that his father served in the second World War but he chose to instead to renounce his family's militaristic tradition and spend his time gardening.

His songs have been covered by such stars as Patti Page, Elvis Costello, Jimmy Buffett, Joan Baez, Anne Murray, Reba McEntire, The Everly Brothers and Emmylou Harris.