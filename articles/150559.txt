Leave it to Apple to celebrate Earth Day by kicking off new device recycling programs while also taking a shot at one of its biggest rivals.

The Cupertino tech company on Tuesday ran an Earth Day ad in numerous newspapers around the world, including The Times, that challenges its competitors to adopt the same environmentally friendly policies that Apple has.

“There are some ideas we want every company to copy,” the ad reads in large letters.

The ad appears to be a shot at Samsung, with which Apple is currently in the middle of a lawsuit. The two companies allege they each infringed on each other’s patents; in opening statements this month, Apple called Samsung a copycat.

Advertisement

VIDEO: Unboxing the Tonino Lamborghini Antares smartphone

Besides using Earth Day to mock Samsung, Apple also launched two new recycling programs.

The tech giant said customers can now come to its stores and trade in their old iPad devices for in-store credit that can be used toward the purchase of either a new iPhone or a new iPad.

Additionally, Apple said customers who come to its stores and trade in an old iPhone can now use the credit they receive for either a new iPhone or a new iPad as well. Previously, customers who came to Apple stores could only trade in old iPhones for credit, and the credit had to be used toward a new iPhone.

Advertisement

Apple also announced customers can now recycle any old device built by the company by bringing it to its stores. Apple will accept any products that it has ever sold and will responsibly recycle them for free.

ALSO:

LG’s next smartwatch will include a screen that never turns off

Verizon annual cybersecurity report: ‘The bad guys are winning’

Advertisement

AOL email hacked: Several users complain about compromised accounts