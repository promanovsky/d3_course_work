Shutterstock photo

Investing.com -

Investing.com - The euro was trading close to more than one-week lows against the dollar on Monday as trading conditions remained subdued following Friday's U.S. market holiday.

EUR/USD was down 0.12% to 1.3578 from 1.3594 late Friday.

The pair was likely to find support at 1.3540 and resistance at 1.3610, Friday's high.

The dollar remained broadly stronger after official data last week showed that the U.S. economy added a larger-than-forecast 288,000 jobs last month, while the unemployment rate ticked down to 6.1%, the lowest in almost six years.

The strong data sparked speculation that the Federal Reserve could bring forward its timetable for raising interest rates.

The US Dollar Index, which tracks the performance of the greenback versus a basket of six other major currencies, was steady at 80.30.

The euro remained under pressure after European Central Bank Vice President Benoit Coeure said Sunday that rates will remain on hold for an extended period to ensure monetary stability in the euro zone.

The ECB left all rates on hold at its meeting last week, after cutting rates to record lows in June in a bid to stave off the threat of persistently low inflation in the region.

Data on Monday showing that German industrial output unexpectedly dropped in May, also weighed on the single currency.

Official data showed that German industrial output fell 1.8% in May, the third consecutive monthly decline. The data fuelled concerns over the outlook for the broader euro zone economy.

A separate report showed that the Sentix index of euro zone investor confidence improved this month, due in part to the ECB's steps to shore up growth and combat low inflation.

The euro edged lower against the yen, with EUR/JPY dipping 0.12% to 138.58, off Thursday's one-month highs of 139.26.

Elsewhere, the dollar drifted lower against the Japanese currency, with USD/JPY slipping 0.13% to 101.93, easing back from last Thursday's two-week high of 102.25.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.