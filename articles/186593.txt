tech2 News Staff

At an event in San Francisco, Intel and Google have announced a slew of new Chromebook models from manufacturers like Acer, Asus, Dell, Toshiba and LG.

LG has also announced an all-in-one desktop running Chrome OS dubbed Chromebase. Sporting a 21.5-inch IPS screen with 1920 x 1080 resolution and LED backlighting, the display is backed by a dual-core Intel Celeron 2955U Haswell processor clocked at 1.4 GHz. The Chromebase is expected to be available in the US by May 26 for $349.99. It will be available through select outlets starting May 12.

By late summer, Dell and Acer are expected to launch Chromebooks that will be powered with Intel's Core i3 processor. While Acer’s C720 is announced to be available during the back-to-school season for a price of $349.99, Dell is expected to launch a new variant of its Chromebook 11 later this year. However, Dell hasn’t yet announced the pricing for its Core i3-powered Chromebook.

Apart from Core i3, Intel has also announced Chromebooks running its Bay Trail-M chipset. Though these systems aren’t expected to be as fast as the ones powered by i3, the company promises 11 hours of battery life. Powered by these processors, Asus have announced new Chromebooks. Asus brings desktop-like Chromebooks or rather Chromebox in the form of the 11.6-inch C200 and a 13.3-inch C300. Toshiba also plans to announce a 13-inch model, while HP is looking to debut a Chromebook soon.

Yesterday, Lenovo joined the Chromebook brigade with N20p and N20. Lenovo has priced the N20 at $279 (approx Rs 16,500) and N20p at $329 (about Rs 19,000) and will be available from July and August respectively. he configuration of both laptops is exactly the same except for different displays. The N20p has an 11-inch touchscreen with 1366×768 pixel resolution. The hinge of the N20p allows users to fold the laptop back up to 300 degrees to use as a tablet in stand mode.