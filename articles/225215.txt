A Michigan firm is recalling 1.8 million pounds of ground beef over fears the meat could be contaminated with E. coli.

Fox affiliate WJBK-TV reported that Wolverine Packing Co., a Detroit-based company, is recalling beef products sold to restaurant distributors throughout Michigan as well as Ohio, Massachusetts and Missouri.

The ground beef products were produced between March 31 and April 18, according to the station.

The products subject to recall bear the establishment number "EST. 2574B" and will have a production date code in the format "Packing Nos: MM DD 14" between "03 31 14" and "04 18 14," WJBK reported.

The U.S. Department of Agriculture’s Food Safety and Inspection Service says 11 illnesses have so far been identified.

E. coli is a potentially deadly bacterium that can cause dehydration, bloody diarrhea and abdominal cramps two to eight days after exposure to the organism.

While most people recover within a week, serious conditions can develop, such as a type of kidney failure called hemolytic uremic syndrome.

Click for more on the recall from MyFoxDetroit.com