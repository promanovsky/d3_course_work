A super-enriched banana genetically engineered to improve the lives of millions of people in Africa will soon have its first human trial, which will test its effect on vitamin A levels, Australian researchers said Monday.

The project plans to have the special banana varieties -- enriched with alpha and beta carotene which the body converts to vitamin A -- growing in Uganda by 2020.

The bananas are now being sent to the United States, and it is expected that the six-week trial measuring how well they lift vitamin A levels in humans will begin soon.

"Good science can make a massive difference here by enriching staple crops such as Ugandan bananas with pro-vitamin A and providing poor and subsistence-farming populations with nutritionally rewarding food," said project leader Professor James Dale.

The Queensland University of Technology (QUT) project, backed by the Bill and Melinda Gates Foundation, hopes to see conclusive results by year end.

"We know our science will work," Professor Dale said.

"We made all the constructs, the genes that went into bananas, and put them into bananas here at QUT."

Dale said the Highland or East African cooking banana was a staple food in East Africa, but had low levels of micro-nutrients, particularly pro-vitamin A and iron.

"The consequences of vitamin A deficiency are dire with 650,000-700,000 children world-wide dying ... each year and at least another 300,000 going blind," he said.

Researchers decided that enriching the staple food was the best way to help ease the problem.

While the modified banana looks the same on the outside, inside the flesh is more orange than a cream color, but Dale said he did not expect this to be a problem.

He said once the genetically modified bananas were approved for commercial cultivation in Uganda, the same technology could potentially be expanded to crops in other countries -- including Rwanda, parts of the Democratic Republic of Congo, Kenya and Tanzania.

"In West Africa farmers grow plantain bananas and the same technology could easily be transferred to that variety as well," he said.

Copyright (2014) AFP. All rights reserved.