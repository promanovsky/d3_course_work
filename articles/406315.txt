Target has hired Pepsi executive Brian Cornell as its new chairman and CEO as it continues to pick up the pieces from last year's huge data breach.

Cornell replaces interim CEO John Mulligan, who is chief financial officer for the company. Mulligan stepped into the interim CEO post in May after Gregg Steinhafel resigned in the wake of a massive data breach during Christmas shopping season that potentially targeted up to 110 million victims.

See also: How to Check If Hackers Stole Your Data in Massive Target Breach

Between Nov. 27 and Dec. 15, hackers used malware to compromise the software controlling point-of-sale systems in Target stores, stealing debit and credit card information from customers. Those credit cards ended up flooding the black market. It was one of the largest data breaches of any consumer business.

Target, however, was criticized for how it handled the breach. The company waited almost a month before notifying its customers after the issue became public. And consumers clearly weren't happy. Sales for the fourth quarter then declined by 5.3%.

Since then, Target has made several moves to tighten up its security, including hiring former Homeland Security tech adviser to serve as chief information officer.

Cornell, 55, most recently served as CEO of PepsiCo Americas Foods. Before that, he was CEO and president of Sam's Club of Wal-Mart International and CEO of Michaels Stores Inc.

PepsiCo Inc. said in a statement Thursday that it will announce Cornell's successor soon.

Cornell will take the helm at Target on Aug. 12.

Amanda Wills, Jason Abbruzzese and Stan Schroeder contributed to this report. Some information was provided by the Associated Press.