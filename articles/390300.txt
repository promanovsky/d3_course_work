Lucasfilm might be heading back to the Forest of Dean in Gloucestershire, England to film "Star Wars Episode 7" scenes.

Forest of Dean was the locations for many "Star Wars" scenes, including Yoda's home and the Forest Moon of Endor. The Daily Mail reports visitors found cables and recording equipment that allegedly belong to the "Star Wars Episode 7" production.

Check out the images of the Forest of Dean here. Unfortunately, Lucasfilm and Disney haven't confirmed the production is filming in Gloucestershire.

However, the "Star Wars Episode 7" has added two new cast members. Crystal Clarke and Pip Andersen were amongst thousands of young talent whom auditioned during the open calls held throughout the U.S. and U.K. in 2013, according to the "Star Wars" official website.

"The Star Wars universe has always been about discovering and nurturing young talent and in casting Episode VII we wanted to remain absolutely faithful to this tradition. We are delighted that so many travelled to see us at the open casting calls and that we have been able to make Crystal and Pip a part of the film," Lucasfilm President Kathleen Kennedy said in a statement.

The film will also star newcomers Lupita Nyong'o, Gwendoline Christie, John Boyega, Daisy Ridley, Adam Driver, Oscar Isaac, Andy Serkis, Domhnall Gleeson and Max von Sydow. Original "Star Wars" actors Harrison Ford, Carrie Fisher, Mark Hamill, Anthony Daniels, Peter Mayhew and Kenny Baker will also be reprising their roles.

Lucasfilm also recently announced a two-week hiatus to adjust the production schedule in order to allow Harrison Ford time to recover from a leg injury. Filming will reportedly wrap later this fall.

"Star Wars Episode 7" will be released to theaters on Dec. 18, 2015.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.