Bullard Sends Contradictory Signal

The stock market took off following Janet Yellen’s equity-friendly message last week. Thursday, James Bullard took to the streets to rein in bullish expectations a bit. From Bloomberg:

U.S. stocks slipped for the third time in four days after James Bullard, president of the Federal Reserve Bank of St. Louis, suggested today that higher interest rates may happen sooner than people thought…Bullard, speaking in an interview on Fox Business Network, predicted the central bank’s first interest-rate rise will happen in the first quarter of next year. Most investors are forecasting higher rates in the latter half of the year, according to Ryan Larson of RBC Global Asset Management (U.S.) Inc. “Bullard’s comments are somewhat contradictory to what Chair Yellen indicated last week,” Larson said in a phone interview. “Contradictory language coming from Fed officials is putting pressure on the market.”

Common For Fed To Deliver Conflicting Messages

If you follow the markets closely, you have probably asked yourself at some point why does the Fed keep sending mixed messages? If we think in extremes, we can understand the Fed’s rationale. Let’s assume the Fed released either of the following statements:

Statement A: We have no plans in the foreseeable future to raise interest rates. Statement B: We will begin an aggressive campaign to raise interest rates effective immediately.

Statement A could fuel inflation expectations and encourage bubble-like behavior in the stock market. Statement B could spark a sharp and pronounced plunge in stock and bond prices. The Fed does not want to see either outcome. Therefore, they try to forge a balance between statement A and statement B, which is exactly what we have seen with Bullard contradicting Yellen.

Did Bullard Give Hope To The Stock Bears?

In an environment marked by extremely easy Fed policy, it is logical to consider the likelihood of a vigilante-induced spike in interest rates. Higher interest rates align with falling bond prices. The price of intermediate-term Treasuries (ARCA: ) went up, not down, despite Bullard’s remarks Thursday. Therefore, we did not see a vigilante-like reaction in the bond market.

How about the stock market? Since the stock market has not corrected in a meaningful way in some time, the chart below serves as a reminder of what “risk-off” looks like. When the stock market dropped 9% during a 2012 correction, rising demand for defensive bonds was clearly evident in the rising ratio shown below (ARCA: ) vs. (ARCA: ).

iShares Core Total US Bond Market vs. S&P 500

The chart below shows the same bond/stock ratio after James Bullard’s comments on interest rates Thursday. The primary trend remains down for bonds relative to stocks. Stock market bears want the ratio below to turn back up in a 2012-correction-like manner.

Bond-Stock Ratio Post James Bullard’s Comments

What About A 1994 Scenario?

Given Bullard’s comments regarding higher interest rates are not particularly helpful for bonds, a 1994-like scenario where stocks and bonds fall in unison is quite possible. However, as you can see by comparing the in 1994 (top below) to the present day (bottom), the 2014 stock market still looks much better than during the 1994 interest rate scare.

1994: Stocks And Bonds Fall Together

Update: The Battle Of Indecisiveness

On June 8, we noted the stock bulls were winning the battle of investor indecisiveness. The chart below shows stocks still have the upper hand relative to their fixed-income brethren.

iShares Core Total US Bond Market vs. S&P 500

Investment Implications – Volatility to Ignore

The evidence we have in had still classifies recent weakness in stocks as volatility to ignore. How long will the previous statement remain true? We have no idea. We do not need to know when things will change if we pay attention meticulously and make allocation shifts as needed. For now, we continue to hold a mix of stocks (ARCA: ), leading sectors (NYSE: ) and a complimentary stake in bonds (ARCA: ).

Ciovacco's Advice

The tweet above applies to the last few days of trading. Watching the markets tick-by-tick can prevent you from seeing the still-bullish forest through the daily-volatility trees.