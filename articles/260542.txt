The following article is entirely the opinion of David Cornell and does not reflect the views of the Inquisitr.

Assassin’s Creed: Unity and Assassin’s Creed: Comet could both end up in the long list of games I’m not planning to try. While both of those games are undoubtedly being given the utmost care in development, there is an undeniable fact that Ubisoft might not like.

Out of all of the Assassin’s Creed games on the market, I only enjoyed two of them enough to want to play them again. This includes the DLC releases which eventually became their own individual purchases. AC 5 needs to impress me on both counts.

The original Assassin’s Creed was a boring mess. The combat controls were broken enough to make the game almost impossible to play to the end, with parkour controls that didn’t stop you from jumping into the water or off a ledge to your death. In the first game, any water above your knees was enough to instantly kill you, turning you into another victim of the usual platforming trope. Thankfully, Assassin’s Creed II fixed that and made it possible for you to swim, and its DLC actually added to the core game.

Assassin’s Creed: Brotherhood and Revelations both fell victim to far-too-difficult mini-games so unforgiving that I couldn’t get past the middle of Brotherhood. Revelations was worse, and I gave up even trying to finish that chariot fight at the very beginning.

Those mini-games are a running trend in the games, and I expect nothing different in Assassin’s Creed: Unity or Comet.

Assassin’s Creed III was the first one I never played and probably never will. It supposedly incorporates so many cutscenes that it’s rumored to be a good half hour before you even get the chance to do anything. It also took out the buildings to make the American Revolutionary War more historically accurate, but that also worked against it. The generally boring character of Connor was just another reason to not even care. At least Assassin’s Creed IV: Black Flag gave Captain Edward Kenway enough personality that I was able to look past the general lack of a storyline and actually have fun.

After trying Freedom Cry and becoming generally frustrated with the controls again, I vowed to never buy another DLC spinoff. Assassin’s Creed: Comet is allegedly a continuation of Black Flag, also giving me flashbacks of earlier games that frustrated me.

Assassin’s Creed: Unity is going to be set during the French Revolution, which may be interesting to developers from the generally French Canadian province of Montreal, but I have no personal interest in the history of France. Before I spend my hard earned money on this game, my opinion of what was once called Assassin’s Creed 5 needs to be proven wrong.

Ubisoft might just surprise me like they did with AC 4, but I’m likely to pass on Assassin’s Creed: Unity and Comet.

[image via adelfrost.deviantart.com]