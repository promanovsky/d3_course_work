Young girls who grow up being told they're "too fat" have a greater chance of becoming obese.



That's according to a new long-term study by psychology researchers at UCLA. Researchers weighed girls at age 10 and again at age 19, and found that the odds of being obese as a young woman increased if someone had labeled the participant "too fat" by the time she was 10. They also found a participant's likelihood of being obese increased as more people told her she was "too fat."

"There's no reason to even say the 'f' word if you're trying to improve health," lead researcher A. Janet Tomiyama told The Huffington Post. "This really adds to a body of research that shows that negatively evaluating someone, especially in the weight domain, can sometimes backfire."

Researchers measured the weights and heights of 2,379 10-year-old girls from Northern California; Cincinnati, Ohio; and Washington, D.C., and asked them if anyone had ever told them they were too fat (58 percent said yes). The researchers followed up with the participants at age 19 -- nine years later -- to again record their heights and weights. They found that the women had 1.66 times greater odds of being obese if people (including parents, teachers, siblings and friends) had told them that they were "too fat" while growing up.

Tomiyama statistically removed the effects of childhood weight, income, race and puberty age in order to isolate the impact of "fat" comments. The study was published online Monday for the June 2014 print issue of JAMA Pediatrics.

Tomiyama said the findings echo other research she's done on how people respond to stress by eating more.

"Anyone can do a simple thought experiment -- if someone makes you feel bad, what do you do? Does that motivate you?" said Tomiyama. "Just put yourself in the shoes of a 10-year-old girl being told" she's too fat.

What should a parent say instead? Perhaps the most important thing that caregivers can do for any child, overweight or not, is to be a healthy role model. That doesn't require many words at all, according to Dr. Larry Cheskin, of the Johns Hopkins Global Center on Childhood Obesity.

"There's no sense in trying to talk your kids into behaving like a saint if you're doing the opposite yourself," said Cheskin, who founded the Johns Hopkins Weight Management Center. "Role modeling is really important." Cheskin wasn't involved in Tomiyama's study, but had thoughts on what does work to get children healthier.

"Emphasize what's nutritious, what's healthy, what's good for you -- rather than getting into, 'You don't look good,' 'You're fat,' or anything critical," Cheskin advised. "Try to be as positive as possible in redirecting a child to eat better."

As for the exercise part of weight loss, he advised talking about "how good it feels to be physically active and physically fit," and saying things like, "You're so skilled. You're so strong. You're getting stronger. You're getting fitter."

It's not just caregivers who need to watch what they say around children. Tomiyama noted that "high-powered health policy scholars," like the ones behind Georgia's controversial childhood obesity advertisements, isolated and shamed overweight kids as part of a public health campaign.

"At its core, [the Georgia campaign] stigmatized overweight kids," said Tomiyama. She reasoned that the campaign is probably based on the notion that stigmatizing campaigns worked against cigarette smokers.