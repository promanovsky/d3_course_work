By Bahar Gholipour, Staff Writer

Published: 04/28/2014 06:00 PM EDT on LiveScience

Medical marijuana can help treat some symptoms of multiple sclerosis (MS), but it doesn't appear to be beneficial in treating people with Parkinson's disease and several other neurological disorders, according to a new study.

Researchers at the American Academy of Neurology reviewed more than 30 studies that compared the effects of medical marijuana to those of placebos in treating people with several conditions.

They found that medical marijuana in the form of pills or oral sprays appeared effective in reducing stiffness and muscle spasms in MS. The medications also eased certain symptoms, such as pain related to spasms, and painful burning and numbness, as well as overactive bladder, according to the study, which will be published tomorrow (April 29) in the journal Neurology. [5 Facts about Pot]

But the researchers cautioned about the drug's side effects.

"It's important to note that medical marijuana can worsen thinking and memory problems, and this is a concern, since many people with MS suffer from these problems already due to the disease itself," said study researcher Dr. Barbara Koppel, a professor of clinical neurology at New York Medical College.

The findings for other neurological disorders in the study were not as promising as those for MS. Researchers didn't find enough evidence to show that medical marijuana is helpful in treating movement problems in people with Huntington's disease, tics in those with Tourette syndrome or seizures in people with epilepsy.

The drug also didn't appear to provide relief to patients with cervical dystonia, which is a rare disorder characterized by involuntary muscle contractions in the neck.

For people with Parkinson's disease, the researchers concluded that medical marijuana is probably ineffective in treating abnormal movements that can develop in the late stages of the disease. These abnormal movements appear as a side effect of the drug levodopa, which is the main drug used to treat Parkinson's disease.

Marijuana contains about 60 active compounds called cannabinoids. The most well-known marijuana compound is tetrahydrocannabinol (THC), which is associated with the "high" effect of smoking marijuana and can cause psychosis and anxiety. Another compound is cannabidiol (CBD), which is not psychoactive and appears to have opposite effects. However, more research is needed to know all the effects of marijuana compounds, experts say.

"Cannabinoids should be studied as other drugs are, to determine their efficacy and, when evidence is available, should be prescribed as other drugs are," the researchers wrote in their study.

The ratio of THC to CBD compounds in marijuana samples varies, and medical marijuana tends to have a lower relative amount of THC.

Most of the MS studies that researchers reviewed involved pill or oral-spray forms of medical marijuana. Only two studies used marijuana that was smoked, and these studies had inconclusive results.

To find the frequency of side effects, the researchers looked at more than 1,600 patients in the studies who were treated with medical marijuana for less than six months. About 7 percent of the study participants dropped out due to the side effects, or because the medication wasn't working for them.

Most of the side effects that were reported were not serious, and included nausea, increased weakness, mood changes, suicidal thoughts and hallucinations. One study participant died from a seizure, which was cited as "possibly related" to treatment with marijuana, researchers said.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed. ]]>