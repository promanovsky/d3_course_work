Get a daily dose of showbiz gossip straight to your inbox with our free email newsletter Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The Kids Choice Awards' red carpet was apparently coloured by the blood of its many fashion victims last night.

While some celebrities like Lea Michele and Selena Gomez managed to get their KCA's outfit oh-so-right, there were some who just, well, missed.

It was nice that Big Bang Theory star Kaley Cuoco has the guts to wear that kind of monochrome, short pantsuit, high-collared, lacy geometric creation - the only saving grace was that it was covered in gunge and slime shortly thereafter.

The future wasn't so much bright as orange for Buffy the Vampire Slayer Ariana Grande, who took a break from flying around hellmouths and apologising to 'textbook demons' to attend this year's KCAs as an EasyJet hostess.

There is a rule that one should only show either leg or cleavage, apparently Ugly Betty star America Ferrera misread and only showed a hint of ankle in her midnight blue and black suit - racy! Where's that Ugly Betty poncho when you really need it?

Well, it turns out Kanye West's new musical mentee Pia Mia had turned it into a headscarf and used the worst bits of approximately 442 pairs of denim jeans to create something that even Katy Perry would turn down. Double demin, rookie mistake.

Though at least Dip It Low singer Christina Milian wisely removed the curtain rings from her frightening frock.

Check out all the horrific looks in the gallery above and then when you've done that head over to the Best Dressed List here for better examples of what to wear to any red carpet event ever.