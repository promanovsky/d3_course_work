Johnson Controls (NYSE: JCI) reported better-than-expected fiscal third-quarter earnings.

The Milwaukee, Wisconsin-based company's quarterly profit dropped 68% to $176 million, or $0.26 per share, from a year-ago profit of $550 million, or $0.80 per share. The latest quarter results included restructuring charges of $162 million related to the automotive interiors business and pre-tax losses of $140 million from divested businesses and other transaction-related costs. Its net income from continuing operations came in at $238 million, or $0.35 per share. Excluding certain items, its earnings from continuing operations rose 17% to $0.84 per share from $0.72 per share.

Its sales climbed 3% to $10.81 billion. However, analysts were expecting earnings of $0.83 per share on revenue of $10.8 billion. Johnson Controls had projected earnings from continuing operations of $0.81 to $0.84 per share.

The company's automotive sales rose 7% to $5.73 billion, while building efficiency sales declined 4% to $3.58 billion. Its power solutions sales gained 6% to $1.51 billion in the quarter.

For the current quarter, Johnson Controls projects earnings from continuing operations of $1 to $1.02 per share, versus analysts' estimates of $1.01 per share.

Alex Molinaroli, Johnson Controls chairman and chief executive officer said, "Our performance was consistent with the expectations we disclosed in our second quarter earnings call, with strong overall performance by our automotive and power businesses and margin improvement in Building Efficiency."

Johnson Controls shares gained 1.44% to $50.60 in pre-market trading.