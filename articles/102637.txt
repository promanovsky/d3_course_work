“Early adopter” seems an inadequate description for the pioneering spirit with which the first users of Google’s internet-linked Glass eyewear are embracing this planet-changing technology.

Google prefers to call them “Explorers”. That is certainly hard to dispute, and in their way, they will be following the example of many other great historic “explorers” – such as Thomas Watson, who took the world’s first telephone call in 1876 from his colleague, Alexander Graham Bell. Or Bertha Benz, who took the opportunity to drive her husband’s latest invention, a three-wheeled motor car powered by an internal combustion engine, the 66 miles from Mannheim to Pforzheim in 1888. Or Ray Tomlinson, who sent the world’s first email, albeit to himself in 1971, with the text “QWERTYIOP”.

Google’s new kit will initially be available for purchase in America for one day only – this coming Tuesday – with a limited number of headsets on offer, costing $1,500 each. A complete waste of money? Or a small price to pay to be part of technological history?

“Smart eyewear” – to give Google Glass its proper name – is easy to mock. But then the cynics will have made similarly disparaging remarks about Bertha, Thomas and Ray in their day. It is true as well that the motor car, the phone and the internet have gone through an almost incredible evolution over the decades, and are still spawning new wonders even today.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the The View from Westminster newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here