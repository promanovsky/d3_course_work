The chikungunya (chik-en-GUN-ye) virus, transmitted to humans through mosquitoes, is reported to be spreading to the United States, carried by travelers from outbreak sites in the Caribbean.

The Pan American Health Organization (PAHO) is reporting that most of the new cases stem from Latin regions of the Caribbean, with Guadeloupe, Martinique and part of Saint Martin also included. The number of chikungunya cases has risen to 135,427 suspected or confirmed cases. Almost two-thirds of new cases seem to be sourced from the Dominican Republic and Haiti. The number of associated deaths is currently at 14.

PAHO claims other countries reporting imported cases now include Cuba, Panama, Chile, Aruba, Barbados and the United States (27 cases to date). The number of U.S. cases reported so far this year has equaled what is usually a year's worth of cases. The U.S. Centers for Disease Control and Prevention (CDC) recently said the number of U.S. cases is likely to rise as they are imported from still-growing infection zones in the Caribbean.

Two new cases were reported in the past day in Nebraska and Tennessee. As the warmest months of summer approach, infection rates will likley also increase, especially in the Southeastern U.S., where the Aedes mosquito prefers to habitate.

The Aedes species of mosquito, which calls most of the world home, is the culprit behind the spread of the virus. It can also spread dengue fever, and does most of its feeding during the daytime. The virus very rarely spreads through mother to newborn or via blood transfusion.

The most common symptoms of chikungunya infection are fever and joint pain. Additional symptoms include headache, muscle pain, joint swelling and rash. Symptoms usually begin three to seven days after infection occurs.

Although it can be severe and disabling, the virus rarely leads to death -- most patients feel better within a week, although some may experience joint pain for months thereafter. There is no specific treatment for infection, other than the usual recommendations for viral fevers such as rest, extra fluid intake and over-the-counter pain and flu medications.

The virus's incursion into the Caribbean region is a new development, having been found there for the first time in late 2013.

There is no vaccine, but travelers can protect themselves by the use of insect repellent, wearing long sleeves and pants and by staying indoors in rooms with air conditioning and effective door and window screens.

The CDC encourages health care providers to report suspected chikungunya cases to their state or local health departments to facilitate screening efforts.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.