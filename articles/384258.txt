They say the only sure things in life are death and taxes. So have you considered the fate of all your online accounts, from Facebook to Google, and your online content, from photos to videos, when you finally shuffle off this mortal coil?

The Uniform Law Commission is currently meeting to discuss the prospect of allowing loved ones access to your online profiles and content after your death. The Uniform Fiduciary Access to Digital Assets Act would suggest that it is beneficial to both the emotional and the financial well-being of families that they get access to loved ones' online assets.

The obvious example is a family wanting to download all of the posts, photos and videos on somebody's online page so that they may preserve it for the future, lest the company that hosts them closes down or changes its policy about unused profiles five, 10 or even 20 years later.

Another possibility is the content of a famous person. Often, the letters of politicians or the notes of writers are treasured for academic study. Shouldn't online content of today's noted individuals have the same regard? Indeed, a family's estate may benefit greatly of the sale of said content to a museum or other institution.

"This is something most people don't think of until they are faced with it. They have no idea what is about to be lost," said Karen Williams of Oregon to the Associate Press. Williams sued Facebook so she could access the online account of her 22-year-old son Loren after he died during a motorcycle accident in 2005.

"Our email accounts are our filing cabinets these days," said Suzanne Brown Walsh, the attorney who chaired the drafting committee on the Digital Assets Act. But "if you need access to an email account, in most states you wouldn't get it."

Currently, different companies and websites have their own disparate policies regarding the profiles of the deceased. Facebook "memorializes" accounts by letting confirmed friends continue to view photos and old posts. Google, which would include content in Gmail, YouTube and Google Plus, has a policy of deleting idle accounts without activity, unless people with proper documentation such as death certificates contact the company. And people who sign up for Yahoo agree that their accounts expire along with them.

Many share their passwords with trusted family members or spouses. But sharing that information and allowing others onto your account violated most companies' terms of service. So other means to legally access there profiles and content must be found.

The meeting in Seattle of the Uniform Law Commission ends tomorrow. It is likely they will have a draft of the Uniform Fiduciary Access to Digital Assets Act prepared by closing. The draft of the law would then be submitted to each state legislature where debate and modification may occur before voting and possible adoption may happen.

The Commission is comprised of 350 lawyers, judges and law professors that were appointed by state governments. It includes members from all 50 states, as well as Washington D.C., Virgin Islands and Puerto Rico. According to the ULC site, "Commissioners are appointed by their states to draft and promote enactment of uniform laws that are designed to solve problems common to all the states."

Photo: Tony Fischer

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.