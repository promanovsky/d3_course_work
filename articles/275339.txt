Baptism of ire! Brad Paisley winds up church activists by taking cheeky selfie at rally protesting his show in Kansas



One of his most popular hits is his humorous paean to booze Alcohol.

And country superstar Brad Paisley showed his famous sense of fun once again when he stopped at a rally protesting his show in Kansas to take a selfie with the attendees.

He posted the photograph he took next to the Westboro Baptist Church members on the internet on Sunday to further goad the activists.

Selfie mocking sense of humour: Brad Paisley took this comical picture of himself with church activists protesting his show in Kansas on Sunday

In the picture he stands in front of firebrands holding signs bearing slogans such as 'God Hates Drunks,' 'Sin Breeds Violence,' and 'God Controls All.'

The 41-year-old captioned the image: 'Westboro Baptist Selfie!! Or west-Burro(a**) selfie. Hopefully they can hear the show out here. We'll play loud."

His reaction was in stark contrast to that of his fellow country favourite Vince Gill when he was protested by the same group in Kansas City, Missouri last year.

His expletive-filled rant during the confrontation was caught on camera and ended up going viral.



Their protest comes on the back of Brad travelling all the way to Afghanistan to perform a morale-boosting concert for U.S. troops stationed there.

Cranking it up: He promised he would play so loud the protesters would have to listen to his show from outside

He hitched a ride on Air Force One with President Barack Obama for the surprise visit to men and women serving at Bagram Airfield.

Once there he performed an hour-long acoustic set to about 3,000 U.S. troops.

Before the show the three-time Grammy winner tweeted: 'About to play a surprise concert for the troops in Afghanistan. God bless our military. Here we go.