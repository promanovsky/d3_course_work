Apple Inc.’s (NASDAQ:AAPL) line of desktop computers just got a little more affordable.

The Cupertino, California, tech titan quietly introduced a cheaper iMac on Wednesday, lowering the cost of entry to the Mac OSX ecosystem. Now starting at $1,099, computer shoppers can finally get their hands on a 21.5-inch iMac desktop at $200 less than the previous baseline model.

Here’s five things you need to know about the new lower-priced iMac.

The new iMac has the same processor as the MacBook Air

Of course there are some tradeoffs to that lower price point. In place of the faster 2.7GHz quad-core Intel i5 found in the previous base model, the $1,099 model sports the slower 1.4GHz dual-core Intel i5, which is currently found in the MacBook Air. While you’ll easily be able to do basic, everyday tasks with the $1,099 iMac, such as checking email, surfing the Web and light photo editing, don’t expect to use it to edit feature films at lightning speed.

Playing bleeding-edge 3D games is out of the question

Replacing the Intel Iris Pro graphics processor is the cheaper and slower Intel HD Graphics 5000 chip. Games that don’t rely too much on 3D graphics will probably be playable on this machine, but games that use bleeding-edge graphics on the iMac are likely out of the question.

Less space

Along with the price cut, the $1,099 iMac’s hard drive space also got cut, from 1 TB to 500 GB. But if you’re willing to shell out a little more, it can be upgraded for more space or a faster (256 GB) solid-state drive.

Only 8 GB of RAM

The $1,099 iMac sports the same amount of base random access memory (RAM) as the $1,299 iMac. However, unlike its pricier sibling, there’s no option to configure the lower-priced iMac with more RAM to run more apps at the same time.

Photo: Courtesy/Apple

Other features of the 21.5-inch iMac remain the same

The $1,099 iMac still sports a 1920x1080-pixel LED-backlit display along with the suite of apps that comes with every Mac computer, which includes iPhoto, iMovie, GarageBand, Pages, Numbers and Keynote.

Take a closer look at the new cheaper iMac at Apple's online store.