Vitamin D pills may help lower hypertension, according to research conducted at the University of South Australia. The study, published in the journal the Lancet, surveyed data on nearly 150,000 people to find a link between high blood pressure and vitamin D deficiency.

Researchers discovered that for each 10 percent increase in vitamin D concentration, the chance of hypertension decreased by 8 percent.

"In view of the costs and side effects associated with antihypertensive drugs, the potential to prevent or reduce blood pressure and therefore the risk of hypertension with vitamin D is very attractive," said study author Elina Hyppönen.

In an accompanying editorial Dr. Shoaib Afzal and Dr. Borge Nordestgaard, from Copenhagen University Hospital and the University of Copenhagen noted that further studies were needed to prove a definite link.

Proven methods of lowering hypertension include cutting down on salt, keeping physically active, maintaining a healthy body weight, keeping alcohol within recommended limits and avoiding stressors.

For comments and feedback contact: editorial@rttnews.com

Health News