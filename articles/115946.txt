Women who use SSRI antidepressants during pregnancy are more likely to have children afflicted with autism, according to research conducted at Johns Hopkins University. The study, published in the journal Pediatrics, found that boys with autism were three times as likely to have been exposed to SSRIs in the womb.

"We found prenatal SSRI exposure was almost three times as likely in boys with autism spectrum disorders relative to typical development, with the greatest risk when exposure is during the first trimester," said study co-author Li-Ching Lee.

However, added Lee, the study did not establish a direct cause-and-effect link between SSRIs and autism:

"It's a complex decision whether to treat or not treat depression with medications during pregnancy. There are so many factors to consider. We didn't intend for our study to be used as a basis for clinical treatment decisions. Women should talk with their doctors about SSRI treatments."

Popular SSRIs include Celexa, Lexapro, Paxil, Prozac and Zoloft.

For comments and feedback contact: editorial@rttnews.com

Health News