Zee Media Bureau

New Delhi: The BSE benchmark Sensex on Tuesday breached 24,000 level for the first time on the optimism backed by exit polls forecast that shows a stable govt led by BJP coming to power at the Centre.

All the sectoral indices led by oil and gas, PSUs, power, banking, realty and capital goods sectors are trading in the positive zone.

The 30-share Sensex rose by over 514.82 points to trade at an all-time high of 24,066 while the 50-share NSE Nifty zoomed 128.70 to trade at 7,143.

Sensex was trading at 23,921.91 in opening trade on heavy fund inflows. Nifty also breached the psychological 7,100 mark for the first time in the opening trade today.