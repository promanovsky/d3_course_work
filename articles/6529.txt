Darren Aronofsky's forthcoming Biblical epic Noah has been banned in several Middle Eastern countries for depicting religious figures.

Censorship boards in Qatar, Bahrain and the United Arab Emirates have confirmed to Paramount Pictures that they will not release the film, which stars Russell Crowe and begins its global release later this month.

Jordan, Kuwait and Egypt are expected to follow suit, stating that the movie offends Islamic teaching and the “feelings of the faithful” with its dark representation of a holy figure.

Al-Azhar, Egypt’s leading Sunni Muslim institute, criticised Noah in a statement on Thursday, arguing that the £75 million movie should be banned in the country.

“Al-Azhar renews its rejection to the screening of any production that characterizes Allah’s prophets and messengers and the companions of the Prophet (Muhammad),” the message read.

“Therefore, Al-Azhar announces the prohibition of the upcoming film about the Allah’s messenger Noah – peace be upon him.”

Noah: Picture Preview Show all 8 1 /8 Noah: Picture Preview Noah: Picture Preview Noah Jennifer Connelly, who plays Noah's wife, and Russell Crowe in Noah Paramount Pictures Noah: Picture Preview Noah Russell Crowe films a dramatic storm scene in Darren Aronofsky's Noah Paramount Pictures Noah: Picture Preview Noah Jennifer Connelly and Russell Crowe in Noah Paramount Pictures Noah: Picture Preview Noah Emma Watson stars as Noah's adopted daughter Ila Paramount Pictures Noah: Picture Preview Noah Logan Lerman, who plays Noah's second son Ham, and Russell Crowe Paramount Pictures Noah: Picture Preview Noah Darren Aronofsky and Russell Crowe film Noah Paramount Pictures Noah: Picture Preview Noah Sir Anthony Hopkins and Gavin Casalegno in Noah Paramount Pictures Noah: Picture Preview Noah A stormy scene from Noah Allstar/Paramount Pictures

Paramount is unsurprised that Noah is facing difficulties in Muslim countries, according to The Hollywood Reporter. The studio has agreed to edit the film’s promotional materials to include a disclaimer making it clear that this is an imaginative adaptation of the Bible story, not a literal one.

Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Independent Culture Newsletter The best in film, music TV & radio straight to your inbox every week Thanks for signing up to the Independent Culture newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

“While artistic license has been taken, we believe that this film is true to the essence, values and integrity of a story that is a cornerstone of faith for millions of people worldwide,” Paramount’s statement reads.

Jennifer Connelly, Emma Watson, Anthony Hopkins and Logan Lerman also feature in Noah, based on the Book of Genesis story in which Noah builds an ark to save his family and pairs of animals from the great flood. In the Koran, a whole chapter is devoted to him as a holy messenger.

Director Aronofsky told Variety: "There isn't really a controversy. The controversy is all about the unknown and about the fear of people trying to exploit a Bible story. It will all disappear as soon as people start seeing the film."

Other films and books to have sparked controversy in the religious world include 2004's The Passion of the Christ, which told the story of Jesus' crucifixion, Stephen King's widely-banned horror novel Carrie and Salman Rushdie's The Satanic Verses. The latter even led to an attempt by extremists to assassinate Rushdie.

US pop star Katy Perry recently provoked outrage with her "Dark Horse" music video, which saw a man wearing an "Allah" pendant burned by lightning shot from her fingers.