Angelina Jolie sits down for an interview with George Stephanopoulos on Good Morning America, which aired this morning (May 21).

The 38-year-old Maleficent star discussed the movie, her wedding to Brad Pitt, her health, and more.

“I didn’t know what to do with the voice … so when I give my kids a bath at night I would tell them stories in different voices,” she said, adding that when she did “this particular voice they couldn’t stop. The bigger I got, the more they laughed,” Angelina said about creating her character’s voice.

When asked about her decision to go public about her mastectomy, Angelina said, “I wasn’t worried about it, but I didn’t expect there to be so much support. And I was very moved by it. … It’s connected me so much to other families, other women.”

She also added about her wedding to Brad Pitt, “We don’t have a date, and we’re not hiding anything, but we really don’t know. We talk to the kids about it once in a while. … And one of them suggested paintball. And we thought, ‘Well, different,’”

Watch the full interview below…