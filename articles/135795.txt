April 17 (Reuters) - The U.S. Food and Drug Administration has approved Merck & Co's pollen allergy drug Ragwitek.

The tablet, which is administered by placing it under the tongue, is to treat the short ragweed pollen induced allergic rhinitis. (http://r.reuters.com/nyg68v)

The approval for Ragwitek comes days after U.S. regulatory approval for Merck's grass pollen allergy drug Grastek.