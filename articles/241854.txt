Set your alarm clock. There could be a massive "meteor storm" early Saturday morning, which is when Earth will pass through a century-old dust trail left by comet 209P/LINEAR. Those debris particles will light up in Earth's atmosphere as a new meteor shower.

According to the the French astronomical organization IMCCE, the comet debris trails left in 1803 and 1924 both fall in Earth's path on Friday and Saturday, giving us even better chances of a spectacular display.

See also: The Best Meteor Shower of the Year Might Happen on May 24

Regardless of the number of meteors observed, the consensus is that at least some kind of meteor shower will take place between 3 a.m. and 4 a.m. ET on Saturday. If you're lucky enough to live somewhere free of light pollution, you're probably in for a treat. But if you're stuck in a major metropolitan area, your chances of catching a shooting star are slim.

Enter your plan B: radio.

No, not Spotify, the actual radio.

Stymied skywatchers can use a technique called radio forward scattering to hear meteors.

What is forward scattering?

Image: Mashable

When meteors pass through the the atmosphere, they leave ionized trails in their wake. These ionized trails reflect the FM signals from distant stations, creating pops, whistles and occasionally blips of discernible broadcasts to a listener, provided the station in the receiving area is unoccupied. TV signals, radar and AM and FM radio broadcasts will all bounce off the ionized trails. One other benefit of using the radio during a meteor shower is that you can hear meteors that would be much too dim to see with the naked eye.

A car radio should suffice, as will an old FM stereo. If you're really dedicated, a Ham radio and/or an FM antennae will really do the trick. More detailed tutorials for building a setup with an external antennae are available from the North American Meteor Network and the Sky Scan Science Awareness Project.

How to tune in

In order to hear a meteor shower, you'll need unoccupied spectrum in your area. The most recent FM Atlas, from 2010, lists all the radio stations broadcasting in the U.S., Mexico and Canada. Stations not listed in an area were presumed to be unoccupied; this blank spectrum needs to be matched with an occupied spectrum below the horizon line of the receiving location.

Since the origin of this meteor shower, called the radiant, is near the constellations Camelopardalis and Polaris (the North Star), the best options for radio interference come perpendicular to the radiant — due east and due west in this case.

To determine which stations in major metropolitan areas were most likely to be good bets for listening to the meteor shower, we scoured FM maps to find the unoccupied stations in each area from 88.1 FM to 91.9 FM (the lower end of the spectrum is less likely to be occupied). Then, using Google Maps, we identified a target patch, covering the area 800 to 1,000 miles west or east of the city, depending on its location. The target zones were roughly 200 miles high, creating an area of approximately 40,000 square miles.

Once this target area was identified, we used station lists for each frequency, and mapped the number of stations broadcasting in the target zone on the corresponding unoccupied frequency of the receiving zone. The stations with the greatest number of matches were considered the most likely bets.

Below are the best station options for several major U.S. cities. Try your luck on Saturday and let us know how it works out.

Washington, D.C.

Image: Mashable

Washington D.C. seems to have the best chance of any city we looked at. Its target zone, due west, covered parts of Oklahoma, Arkansas, Kansas and Missouri. There were several large cities in the target zone and each candidate D.C. station had a number of corresponding broadcasting stations. In fact, nearly every unoccupied frequency in D.C. had at least as many matched broadcasting stations as the best frequencies in the other cities we looked at.

Signal Strength: Strong

New York

Image: Mashable

The forecast for Saturday morning in New York calls for clouds and rain, so if the light pollution wasn't already going to spoil your skywatching plans, the weather probably will. But if you're going to try your luck with a radio, the best FM stations are 88.5, 90.5 and 91.3. Other empty spectrum options are 90.1, 90.9 and 91.7, but these are less likely to work because of fewer broadcasting stations in the target zone. New York's target zone covered parts of Illinois and Iowa and Missouri, but didn't include and major cities or metro areas.

Signal Strength: Moderate

San Francisco

Image: Mashable

The Bay Area's target zone was entirely within Colorado, which, while not the most populous area, did have a number of spectrum matches.

Signal Strength: Moderate

Seattle

Image: Mashable

Seattle probably has the worst chances of the cities we looked at. Its target zone covered sparsely populated areas of Montana and North Dakota. Most open Seattle stations had zero matches, but since there is so much open spectrum in Seattle, it may be worth a shot. The frequencies 88.1 - 89.7 FM are unoccupied, as are 90.1 and 90.5 - 91.9 FM.

But as a consolation prize, denizens of the Pacific Northwest have one of the best locations for viewing the actual meteor shower.

Signal Strength: Weak

Bonus: Chicago and London

The two other major cities on our list were Chicago and London. The analysis for Chicago was complicated by its Midwest position, creating target zones both east and west of the city. But Chicago has unoccupied spectrum on the FM stations 89.1, 89.5, 89.7, 90.3, 90.7 - 91.3, 91.7 and 91.9.

London had more open spectrum than any other city we reviewed. Every station from 87.5 - 91.9FM was open, except for 87.7, 89.1 and 91.3.