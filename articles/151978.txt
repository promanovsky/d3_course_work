NEW YORK (TheStreet) -- Sarepta Therapeutics (SRPT) - Get Report continued to climb Tuesday after the company announced Monday it had received FDA guidance to apply for regulatory approval of its experimental Duchenne muscular dystrophy drug eteplirsen by the end of the year. The application would lead to a clinical study on a larger scale.

The stock surged 14.16% to $38.79, a $4.81 increase from its previous close of $33.98, at the close of trading on Tuesday. More than 4 million shares changed hands, nearly triple the average volume of 1,435,740. Sarepta had a range of $34.51 to $39.20 on Tuesday and holds a 52-week range of $12.12 to $55.61.

The company said in a statement it would bolster its FDA application with more safety and efficacy data from an earlier trial of the drug, along with a confirmatory study it plans to begin in the third quarter.

The FDA said in November that Sarepta's plan to file based on data at the time was premature, which caused a 64% drop in the stock in just one day.

Robert W. Baird also increased its price target on Sarepta to $55 from $35 in the wake of Monday's news. The firm holds an "outperform" rating on the stock.

Must Read:Warren Buffett's 10 Favorite Growth Stocks

SELL NOW: If you own any of the 900 stocks that TheStreet Quant Ratings has identified as a 'Sell'...you could potentially lose EVERYTHING in the next 6-12 months.Learn more.

SRPT data by YCharts

STOCKS TO BUY: TheStreet's Stocks Under $10 has identified a handful of stocks that can potentially TRIPLE in the next 12-months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.