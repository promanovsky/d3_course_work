No Doubt singer Gwen Stefani is reportedly about to become latest addition to ‘The Voice's’ judging pannel, replacing the pregnant Christina Aguilera. The NBC show, which has already added Pharrell Williams to its season 7 lineup, is said to have signed Stefani for one season to fill in for the expectant Aguliera.

Is Gwen Stefani about to join 'The Voice'?

TMZ broke the news, reporting that Gwen has signed on for season 7 of the talent show, which begins taping in June. The deal is said to be only for one season as Aguilera has decided to bow out as she is pregnant with her second child, due later this year with fiancee. It's also reported that NBC wanted Aguilera to stay but the singer was having a difficult time already.

The 33 year old singer is already mother to six year old Max with her former husband Jordan Bratman. After her divorce she began dating film producer Matt in 2010 after meeting on the set of the movie Burlesque. The couple announced their engagement in February and six days later news of Aguilera’s pregnancy broke. The singer has already revealed that her little bundle of joy will be a girl and is due later this year.

Christina Aguilera and fiancee Matt Rutler are expecting their first child together

As for Gwen, she became a new mother for the third time, just two months ago when she gave birth to a son, Apollo Bowie Flynn Rossdale in February. Stefani now has three children, all boys, with husband, Bush frontman Gavin Rossdale. Last weekend she made a surprise appearance at the Coachella festival, where she took to the stage alongside Pharrell Williams to perform her 2005 hit 'Hollaback Girl'.

Speaking of Pharrell, the 'Happy' singer will be seeing a lot more of Stefani soon if she has signed on for 'The Voice'. Pharrell is already confirmed as this season's new judge replacing the exiting Cee-lo Green. Pharrell and maybe Gwen will join returning judges Blake Shelton and Adam Levine when the 7th season starts filming in June.

More: Girls Night Out As Gwyneth Paltrow Meets Gwen Stefani And Nicole Richie For Dinner



More: Christina Aguilera Shares Baby Bump Snap

Watch Gwen and husband Gavin at the premiere of Monster's University: