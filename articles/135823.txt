The number of hits recorded on Wikipedia articles could track the spread of flu and other illnesses faster than existing systems, research says.

Researchers in the United States have developed a highly accurate computer model for estimating levels of flu-like illness in the American population by analysing internet traffic on flu-related Wikipedia articles.

Researchers saw a correlation between traffic to flu-related pages on Wikipedia and subsequent reports of illness by the Centres for Disease Control. Credit:David J. McIver, John S. Brownstein

The research, published in PLOS Computational Biology on Friday, found the Wikipedia-based model estimated flu levels up to two weeks sooner than data from the Centres for Disease Control and Prevention became available. The Wikipedia-based model was also more accurate at estimating the timing of peak flu activity than Google Flu Trends, a service developed by the internet search giant that draws on Google search queries.

Google Flu Trends has been found to be susceptible to error at times when there is heavy media coverage of the flu, such as during the 2009 H1N1 pandemic and during the unusually severe 2012-13 northern-hemisphere flu season.