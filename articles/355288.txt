Richard and Elizabeth Jones, the parents of the camera assistant killed on the set of “Midnight Rider,” have responded to the criminal charges filed in Georgia against the director and producers of the Gregg Allman biopic four months after Sarah Jones‘ death.

“Elizabeth and I are comfortable that the authorities were both careful and meticulous in investigating and bringing charges related to the incident that took our daughter’s life,” Richard said in a statement to the media. “We must allow the criminal justice process to proceed unhindered. Our mission remains the same: to ensure safety on all film sets. Safety for Sarah.”

Also read: ‘Midnight Rider’ Director, Producers Charged With Involuntary Manslaughter

Georgia prosecutors announced on Thursday that “Midnight Rider” director Randall Miller and producers Jody Savin and Jay Sedrish were charged with involuntary manslaughter and criminal trespass as a result of the accident that occurred in February.

Jones was struck by a train after the production placed a hospital bed on a railroad trestle bridge while filming in in Wayne County, Ga. The crew expected only two trains to pass by, but a third freighter train came across the bridge at a speed estimated by witnesses of 60 miles an hour. Debris from the hospital bed was responsible for injuring others as they attempted to escape the oncoming train.

Also read: Gregg Allman, ‘Midnight Rider’ Producers Slapped With New Lawsuit

Jones’ parents have already filed a wide-reaching wrongful death lawsuit against the producers — which includes executive producer Allman — the director, and the owner of the train tracks and surrounding property where Jones died.

The suit seeking a jury trial and unspecified damages alleges that none of the defendants received the proper permission to film on the CSX-owned train tracks, lied to the crew about it and failed to take proper safety precautions even knowing the danger.