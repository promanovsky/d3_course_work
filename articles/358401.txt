Music

Fans who pre-order the new album will get exclusive access to a concert stream where Ariana performs her new music for the first time.

Jun 29, 2014

AceShowbiz - Ariana Grande announces the official title, artwork, and release date for her sophomore set. The Nickelodeon songstress will follow up her debut LP "Yours Truly" with a new album called "My Everything" which is slated for August 25 release in North America.

"I feel like each song is so strongly themed that I just wanted to have a very simple overall cover," she said when making the announcement in an interview with Ryan Seacrest for iHeartRadio.

The album is led by Iggy Azalea-assisted "Problems" which peaked at No. 3 on Billboard Hot 100. It will be followed by a song titled "Break Free" featuring Zedd. A music video is going to be released soon. "It's inspired by Jane Fonda in 'Barbarella' and 'Star Wars' and space," she said. "It's more throwback, campy."

Ariana will also reteam with Big Sean, who was featured in her first album. They collaborate in a new track called "Best Mistake", which she calls one of her favorites.

"My Everything" is available for pre-order on the singer's official website. Fans will get exclusive access to a concert stream where she performs new music from her second album for the first time ever.