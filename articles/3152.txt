Lena Dunham has compared the recent success of Girls star Adam Driver with that of his female co-stars and cited the difference as symptomatic of Hollywood’s attitude towards women.



Dunham, giving Monday’s keynote lecture at the SXSW film festival, said she was happy for Driver, but that the variety of film roles he had been offered since appearing in Girls contrasted sharply with the parts offered to its female cast members.



“People are ready to see Adam play a million different guys in one year – from lotharios to villains to nerds. Meanwhile [co-stars] Allison Williams, Jemima Kirke and Zosia Mamet are still waiting for parts they can get interested in”.



Dunham said there were few opportunities for women of the Girls cast past being typecast as “high school ditzes”. Of the female cast members of HBO’s cult show only Mamet has taken film work. Her movie career predates the show’s 2012 launch and has included small roles in Lisa Cholodenko’s The Kids are All Right and Noah Baumbach’s Greenberg. Meanwhile, Driver has been offered large parts in films directed by the Coen brothers and Stephen Spielberg. He is rumoured to be playing the villain in JJ Abrams’s reboot of the Star Wars franchise.

Dunham’s own film career has been limited to starring roles in her own movies, a role in Joe Swanberg’s upcoming Happy Christmas and bit-part in Girls producer Judd Apatow’s This Is 40. “There’s no place for me in the studio system,” she said.

Tiny Furniture, Dunham’s second feature, won best narrative feature at the 2011 SXSW festival. During her speech at this year’s festival she thanked Janet Pierson, SXSW’s head of film, for launching her career and introducing her to “the only part of my life I’ve really enjoyed”.

Girls will return for a fourth season in 2015.

