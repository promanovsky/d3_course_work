Fleetwood Mac Launching Reunion Tour With Christine McVie

The announcement was made on Thursday's "Today" show following an interview with the band at Stevie Nicks' house.

Christine McVie is officially rejoining Fleetwood Mac for a reunion tour titled "On With the Show" beginning this fall, it was announced on Thursday's edition of NBC's Today show.

McVie hasn't toured with the band since 1998 but joined them in concert in London last year to perform their hit "Don't Stop Thinking About Tomorrow," and Billboard reported earlier this year that she was rejoining the group.

PHOTOS: Meet the Legendary Tour Managers of Rolling Stones, Fleetwood Mac, Alice Cooper and More

The band's 33-city, 34-show North American tour begins in Minneapolis on Sept. 30 and will include stops at Madison Square Garden and two shows at The Forum in L.A.

Tickets go on sale on Monday, April 7.

McVie confirmed she's back on board in the Today interview. She also said the reason she stopped touring with the group was because of her fear of flying, noting that she hasn't flown in 15 years.

STORY: Christine McVie Reunites With Fleetwood Mac

In the sit-down, John McVie, who was recently diagnosed with cancer, said he's "getting better."

The band will perform on Today on October 9.

For Fleetwood Mac's last tour in 2012 there were 69 dates of which 45 reported to Billboard Boxscore. The total gross for these shows was $61,899,473 with 554,548 tickets sold. The average attendance for the shows was 12,323 and the average gross was $1,375,544, according to Boxscore.

A full list of tour dates is below followed by the group's Today interview.

Sept. 30 -- Minneapolis, Minn. (Target Center)

Oct. 2 -- Chicago, Ill. (United Center)

Oct. 6 -- New York City (Madison Square Garden)

Oct. 10 -- Boston, Mass. (TD Garden)

Oct. 11 -- Newark, N.J. (Prudential Center)

Oct. 14 -- Pittsburgh, Penn. (Consol Energy Center)

Oct. 15 -- Philadelphia, Penn. (Wells Fargo Center)

Oct. 18 -- Toronto, ON (Air Canada Centre)

Oct. 19 -- Columbus, Ohio (Nationwide Arena)

Oct. 21 -- Indianapolis, Ind. (Bankers Life Fieldhouse)

Oct. 22 -- Detroit, Mich. (The Palace of Auburn Hills)

Oct. 26 -- Ottawa, ON (Canadian Tire Centre)

Oct. 31 -- Washington, DC (Verizon Center)

Nov. 1 -- Hartford, Conn. (XL Center)

Nov. 10 -- Winnipeg, MB (MTS Centre)

Nov. 12 -- Saskatoon, SK (Credit Union Centre)

Nov. 14 -- Calgary, AB (Scotiabank Saddledome)

Nov. 15 -- Edmonton, AB (Rexall Place)

Nov. 18 -- Vancouver, BC (Rogers Arena)

Nov. 20 -- Tacoma, Wash. (Tacoma Dome)

Nov. 22 -- Portland, Ore. (Moda Center)

Nov. 24 -- Sacramento, Calif. (Sleep Train Arena)

Nov. 25 -- San Jose, Calif. (SAP Center at San Jose)

Nov. 28 -- Los Angeles, Calif. (The Forum)

Nov. 29 -- Los Angeles, Calif. (The Forum)

Dec. 2 -- San Diego, Calif. (Viejas Arena at San Diego State University)

Dec. 3 -- Oakland, Calif. (Oracle Arena)

Dec. 10 -- Phoenix, Ariz. (US Airways Center)

Dec. 12 -- Denver, Colo. (Pepsi Center)

Dec. 14 -- Dallas, Tex. (American Airlines Center)

Dec. 15 -- Houston, Tex (Toyota Center)

Dec. 17 -- Atlanta, Ga. Philips Arena)

Dec. 19 -- Ft. Lauderdale, Fla. (BB&T Center)

Visit NBCNews.com for breaking news, world news, and news about the economy