Twenty-First Century Fox (NASDAQ:FOX) has made an offer to buy Time Warner (NYSE:TWX) in a $80 billion deal that could dramatically change the U.S. media industry. Will a deal go through? What are the implications for investors?

The following slideshow contains some ideas regarding the main factors to watch and important aspects to consider in the medium term.