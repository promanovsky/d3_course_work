Moscow - Mikhail Khodorkovsky, once Russia's richest man and head of the now defunct Yukos company, said on Monday he was satisfied with a court ruling that awarded shareholders of the company over $50 billion in compensation in a legal battle against Russia.

“It is fantastic that the company shareholders are being given a chance to recover their damages,” he said in a statement.

He said that he was not a party to the legal proceedings and does not seek to benefit financially from the outcome of the arbitration. - Reuters