Follow these five tips to avoid spreading the deadly Ebola virus.

Medics say avoiding Ebola should be quite easy if you follow these five simple steps: Wash your hands regularly with soap and clean water; if you suspect someone of having Ebola, do not touch them; if you think someone has died from Ebola, do not touch their body; avoid hunting, touching and eating bushmeat; and don't panic or spread rumours.

Here is the story in 15 seconds.