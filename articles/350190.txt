You might recall that it was a few days ago where it was revealed that Facebook had conducted an experiment on its users. This experiment took place a couple of years ago and it basically manipulated the user’s News Feed to display either positive or negative content to see how it would affect the emotions of its users.

Advertising

While it sounds creepy and a little disturbing, Facebook was well within their rights as laid out by their terms and services agreement, although the company has since received a lot of backlash over it. The original researcher had come forward to offer his explanation and now according to a report from The Wall Street Journal, Facebook’s COO, Sheryl Sandberg, has come forward to offer her apology as well.

According to Sandberg, “This was part of ongoing research companies do to test different products, and that was what it was; it was poorly communicated […] And for that communication we apologize. We never meant to upset you.” Like we said, the experiment/research was within Facebook’s rights, although users are more upset about being manipulated over a study that they weren’t made aware of to begin with.

It also seems that over in the UK, Facebook could face potential legal repercussions as the UK’s Information Commissioner’s Office will be investigating the social network to see if the company had infringed upon any data protection laws during their experiment. What do you guys think? Are we making a bigger deal than we should, or do we have the right to be outraged?

Filed in . Read more about Facebook.