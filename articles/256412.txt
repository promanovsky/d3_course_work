Don't question Aisha Tyler's gamer cred. The best you can hope for is that she'll send you to her Facebook post that tells you she's played "since you were a twinge in the left side of your daddy's underoos."

While you're still feeling the sting of her smackdown, stop for a second and realize that Tyler is now fulfilling a childhood dream by playing herself in Watch Dogs, Ubisoft's much-anticipated new video game. "There's a surreality to it," Tyler told PCMag. "As a gamer I think it's probably much more exciting for me than it would just be for a regular voice actor because you know I'm super stoked."

Having already voiced characters in Halo: Reach and Gears of War 3 and of course as Lana Kane on Archer, Tyler used her skills in bringing an animated character to life to navigate all the possibilities of what could happen to her on the streets of a detailed and dystopic Chicago in Watch Dogs.

The open-world game stretches through all parts of Chicago where you can run into Tyler at any moment. She spent a full day in a voice booth going through all of the possible scenarios for her character's actions and interactions.

"Did you ever have one of those like thousand-piece Lego sets? You know we made a million of the single tiny, one-square Legos to build that whole big world," she explained.

Some of what can happen to Tyler's avatar in the big bad world of Watch Dogs could be violent. "But, you know, it's just 1s and 0s," Tyler shrugs. It's the sort of brush-it-off attitude she has IRL, too. Gaming isn't the most girl-friendly world. Having a tight relationship with Watch Dogs maker Ubisoft, Tyler's hosted two of its E3 pressers, and she faced a bit of a backlash each time.

"You know it's really interesting because I feel like I've faced some version of that attitude through my whole career because obviously I'm a stand-up comedian and for many many years I would be the only woman in a lineup and the only woman in the room," she said.

Sexism doesn't shock her, particularly when it's doled out anonymously online. "The world is unfortunately a bottomless supply of d**ks, so we can't do anything about that," she quipped.

But Tyler suggested that the attitude women in gaming often face is something that can be overcome by exposure. Tyler bases part of that on the reaction to her Ubisoft presser, which she said went from 50 percent negative/50 percent positive her first year to 25 percent negative/75 percent positive after her second appearance.

"I do think that the more that women talk about gaming, talk about their passion for gaming, and how much they love it, the more we actively game, the harder it will be for people to exclude women," she said.

Not being in the majority is something Tyler has practice in since she grew up as a nerd. A typical Saturday for Tyler when she was a girl involved either a day spent alone at the movies playing Tempest or Defender between showings or going to the library to do her homework and look through pathology books.

"I'd look at pictures of like goiters and stuff, which when you're 8 is really interesting, like goiters and tumors and homunculi, so I definitely was a nerd."

She recalled something actor Seth Green, one of her Girl on Guy podcast guests once said to her: If you did not play alone for a significant portion of your childhood, you cannot call yourself a nerd or a geek.

Having the ability to completely immerse yourself in something also makes for a good gamer. "You have to have spent a good amount of time entertaining yourself," she said.

Watch Dogs is a believable world not just in how it looks but that it's based around the idea that a city and all its citizens are so connected that bringing them down can be the work of a simple hack. The easy accessibility of personal information hits close to home but privacy is not one of the things Tyler champions.

Surveillance cameras are on every street corner in Watch Dogs. And it's something that Tyler wants in real life, too. "If I get attacked on the street I really hope there's a f**king surveillance camera watching me," she said. "I mean, that would be really f**king convenient. Not so convenient for Solange Knowles, but you know it would be good for me if I was jumped on the street."

Even online privacy is not something that Tyler frets over. "I'm very ambivalent about privacy and I know most people come down really hard on one side or the other," she said. Her view is more nuanced. "I do think empirically we're entitled to privacy conceptually but I do think that we want the convenience that giving up privacy delivers."

Facebook is Tyler's social network of choice. "Facebook is the Borg, it's in me, I can't pull it out now."

Her husband, a lawyer, is more cautious. "My husband he's like, 'You should use Duck Duck Go to look for porn,' and I'm like, the Internet already knows I look at porn. There's no walking back from that now. Sometimes not on purpose but once I'm there, eh, it's on might as well look at it. I'm not going to be able to undo what's already been done."

Tyler and her husband also have a difference of opinion on games. "I'm kind of almost universally a shooter player," she said. "But I don't like just pure shooters, I really like shooters with complexity. I love open-world gaming and I love games where you have to, like, solve for a variety of issues so that's why I love that [Watch Dogs] is a game where you can shoot and you can drive and you can hack."

She is not a fan of the simpler pleasures of, say, Minecraft, as her husband is. "Every night he wants to tell me about what he builds and he wants to show me his cats and his giant mushroom, which sounds like sex but is definitely not."

Given her level of love for games, Tyler wouldn't rule out creating her own, though her relationship with Ubisoft makes her fully aware of how much goes into it. "I know what kinds of games I like to play but I think developing a new piece of intellectual property is a pretty massive undertaking," she said. "If it wasn't people would be doing a lot more of it and a lot better." She doesn't rule the idea out, though. "I mean the idea of it is really cool," Tyler said. "I'd have to go on like a vision quest for that, though."

Watch Dogs arrives on Tuesday, May 27 for PS4, PS3, Xbox One, and Xbox 360. The Standard Edition will set you back $59.99, while a Limited Editionwith collectible SteelBook game case, Aiden Pearce's vigilante mask, a 9-inch Aiden Pearce statue, the Watch Dogs soundtrack, and an 80-page hardcover concept art bookis $129.99.

Further Reading

Game Reviews