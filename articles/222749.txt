Kim Kardashian and Kanye West head out in Paris on May 20, 2014 (SplashNews)

Kim and Kanye have just five more days until their lavish, May 24, wedding but their 100 guests will reportedly have to foot the bill! HollywoodLifers, should Kimye pay for their guests?

Kim Kardashian and Kanye West may be inviting some of Hollywood’s elite to their destination wedding but don’t you think they should cover the costs? VH1’s The Gossip Table claims that Kimye invited their guests to their exclusive wedding but they didn’t pay for their hotel rooms or flights.

Kim Kardashian & Kanye West Not Footing Their Guest’s Wedding Bills

Weddings are expensive, especially if you’re doing them overseas with only five star treatment. But The Gossip Table has just revealed that Kimye sent the invites but didn’t offer to shell out any cash for their guests to get halfway across the globe.

This is shocking because combined, they have a reported net worth of $140 million. It sounds like they could have splurged for tickets, don’t you think?

Kanye & Kim Marrying In Italy?

There have been multiple reports claiming the couple will marry in Paris, France, as well as Florence, Italy. No one officially knows where this top secret ceremony will be held but Kanye did tells a local Florence newspaper that the city holds a special place in his heart.

“I think that our daughter North was conceived here among the Renaissance masterpieces,” he told La Nazione newspaper. “I adore Florence.”

We’ve learned at HollywoodLife.com that several of Kimye’s guests will fly out Wednesday, May 21, just in time for the rehearsal dinner.

HollywoodLifers, do you think it’s wrong Kimye aren’t paying for their guests?

— Chloe Melas

More Kanye West & Kim Kardashian News: