Talking Points:

Yen weakens after the Bank of Japan releases their April Meeting Minutes

breaks above 102.00 handle following the release

BOJ minutes show minority dissent amongst board members on growth and inflation

Following the release of the Bank of Japan’s April Meeting Minutes, despite no new surprises, the Japanese yen weakened against the US dollar. The traded as high as 102.04, up almost 9 pips, after the Minutes were released. The Minutes revealed that the BOJ’s Policy Board decided, by a unanimous vote, to continue conducting money market operations that will increase the monetary base at an annual pace of about 60-70 trillion yen.

The Minutes also showed the central bank remains confident it will reachits 2 per cent inflation target. They also highlighted an emerging trendwhere Japanese firms are shifting away from a low retail price strategy and becoming more willing to raising prices. Of additional note was an apparent divergence between policy makers’ views on growth and inflation. Three board members emphasized downside risks to the central bank’s assessment for growth in the general price level. The BOJ forecastsinflationof 1.3 per cent for the current fiscal year ending March 2015, 1.9% for the following year, and 2.1 per cent in the year ending March 2017.

USD/JPY 5 Minute Chart

Original post