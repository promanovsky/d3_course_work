Elaine Stritch certainly left an impression on Hollywood.

After word came Thursday that the Broadway veteran had died at 89, many stars took to social media to pay tribute to the sharp-tongued actress.

Among them was Stritch's "30 Rock" co-star and on-screen son Alec Baldwin, who described her as a "true one in a million."

"I'm sure that God is a bit nervous right now. I love you, Elaine," Baldwin added in a separate post, before sharing a picture of himself and Stritch on the "30 Rock" set:

Thanks to all who took time to remember the one and only Elaine Stritch... pic.twitter.com/tbtVTPLwY6 — ABFoundation (@ABFalecbaldwin) July 17, 2014

Other stars, including Anna Kendrick and Josh Gad, also marked the passing of the acclaimed performer:

Anna Kendrick:

Elaine Stritch man. Hell of a performer, spirit and woman. This one really hurts. Today's work is for you ma'am. Crazy love. — Anna Kendrick (@AnnaKendrick47) July 17, 2014





Josh Gad:

#ElaineStritch RIP. The lights on Broadway will never be able to shine as brightly without one of its greatest stars gracing its stages. — Josh Gad (@joshgad) July 17, 2014

Lena Dunham:

Here's to the lady who lunched: Elaine Stritch, we love you. May your heaven be a booze-soaked, no-pants solo show at the Carlyle. Thank you — Lena Dunham (@lenadunham) July 17, 2014

Jane Lynch:

God Bless and God Speed ELAINE STRITCH — Jane Lynch (@janemarielynch) July 17, 2014

James Van Der Beek:





Zachary Quinto:

Illeana Douglas:

In the bar of the Sutton Place I watched #Elainestritch remove a Cuban cigar from Matt Dillon's mouth,stub it out in an ashtray and smile. — Illeana Douglas (@Illeanarama) July 17, 2014





Jesse Tyler Ferguson:

What a life! 89 full years. A true legend & one of my all time favorites. R.I.P. #ElaineStritch — Jesse Tyler Ferguson (@jessetyler) July 17, 2014





Christina Applegate:

Ahhh the Ladies who Lunch! Elaine Stritch shall be missed — christina applegate (@1capplegate) July 17, 2014



Sandra Bernhard:





Frank DeCaro:









Kathie Lee Gifford:



