Internet search giant Google (GOOG) and media conglomerate Viacom, Inc. (VIA,VIAB) have resolved their long-running litigation regarding alleged copyright infringement by Google's video-sharing site YouTube, the two companies said Tuesday. The terms of the settlement were not disclosed.

In a joint statement, the two companies said, "Google and Viacom today jointly announced the resolution of the Viacom vs. YouTube copyright litigation. This settlement reflects the growing collaborative dialogue between our two companies on important opportunities, and we look forward to working more closely together."

New York-based Viacom, which owns MTV, Nickelodeon, Paramount Pictures and Comedy Central, had filed a $1 billion lawsuit against Google and YouTube in 2007, alleging that YouTube allowed its users to upload television-show clips from that were owned by Viacom, without permission.

However, Viacom lost the lawsuit in 2010 after U.S. District Judge Louis Stanton ruled in Google's favor on a motion for summary judgment on the grounds that the Digital Millennium Copyright Act's "safe harbor" rules shielded Google from Viacom's copyright infringement claims. The safe harbor rules can protect owners of websites from copyright infringement committed by their users.

But Viacom won an appeal in 2012 after the U.S. Court of Appeals for the Second Circuit in New York reversed Judge Stanton's grant of summary judgment. The case was sent back to the district court.

In April 2013, Judge Stanton issued another order granting summary judgment in favor of YouTube. That decision was again appealed by Viacom.

When the lawsuit was initially filed in 2007, Google was accused by studios and copyright owners of seeking to profit from illegal content.

However, Google's "ContentID" system now enables copyright owners to easily identify and manage their content on YouTube. When Content ID identifies a match between a copyright owner's video and a file in YouTube's database, it applies the policy chosen by the content owner.

In Tuesday's regular session, GOOG is trading at $1201.04, up $8.94 or 0.75 percent on a volume of 409,862 shares.

VIA is currently trading at $88.78, up $0.36 or 0.41 percent on a volume of 6,030 shares.

For comments and feedback contact: editorial@rttnews.com

Business News