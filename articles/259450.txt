Chechen fighters are among the ranks of rebels active in eastern Ukraine, according to the self-appointed mayor of a militant stronghold in the region.

Vyachoslav Ponomariov, who spoke to Fox News on Wednesday, said the Chechen fighters are in the city of Slovyansk and the Donetsk region.

Chechnya's regional leader said Wednesday that he hasn't sent any troops to fight alongside pro-Russia insurgents, but he says some Chechens may have gone there on their own.

In a statement posted on his Instagram, Ramzan Kadyrov said two-thirds of three million Chechens live outside the province in Russia's North Caucasus mountains, so he "can't and mustn't know where each of them goes."

Fighters who looked like residents of the Caucasus have been seen among pro-Russia rebels, where they have seized government buildings and fought with Ukrainian forces, according to The Associated Press. Ukraine and the West have accused Moscow of fomenting the unrest, but it has denied the claim.

In the most ferocious battle yet, rebels in Donetsk tried to take control of its airport Monday, but were repelled by Ukrainian forces using combat jets and helicopter gunships and lost dozens of men. Some insurgents said up to 100 may have been killed.

The city remained tense Wednesday, with Ukrainian fighter jets flying overhead. Some gunshots were heard.

On Tuesday, a deadline imposed by Kiev for pro-Russian rebels to leave buildings in Donetsk passed.

Kadyrov, a former rebel who fought Russian forces in the first of two devastating separatist wars, switched sides during the second campaign when his father became the region's pro-Russia leader. Following his father's death in a rebel bombing, Kadyrov stabilized the region, relying on generous Kremlin funding and his ruthless paramilitary forces, which have been blamed for extrajudicial killings, torture and other abuses.

Kadyrov's forces, known for their warrior spirit and deadly efficiency, helped Russia win a quick victory in a 2008 war with Georgia. The 37-year old leader has vowed an unswerving fealty to Russian President Vladimir Putin and hailed his policy in Ukraine.

Last week, Kadyrov negotiated the release of two Russian journalists arrested by Ukrainian forces and accused of assisting the rebels in the east, earning Putin's praise. The Chechen leader gave no details how he managed to have the journalists freed, but he has directed threats at the Ukrainian authorities.

"If the Ukrainian authorities want so much to see 'Chechen units' in Donetsk, why go to Donetsk if there is a good highway to Kiev?" he said in Wednesday's statement.

However, he added that he fully supports Putin's policy to help restore peace in Ukraine.

Putin has denied Ukraine's allegations that Russia has sent its special forces to foment the mutiny in the east. On Tuesday, Russia's Federal Security Service rejected the Ukrainian claim that a convoy of vehicles loaded with weapons attempted to break through the border earlier in the day.

Russia, which annexed Crimea in March, has ignored the insurgents' plea to join Russia following controversial independence referendums in the east. The Kremlin also welcomed Ukraine's presidential election Sunday and said it was ready to work with the winner, billionaire candy magnate Petro Poroshenko, in an apparent bid to de-escalate the worst crisis in relations with the West since the Cold War and avoid a new round of Western sanctions.

"It's necessary to use the situation after the election to immediately end using the military and launch a broad all-Ukrainian dialogue involving all regions and political forces in order to start a constitutional reform," Russian Foreign Minister Sergey Lavrov said Wednesday.

Putin's foreign affairs advisor, Yuri Ushakov said that a Ukrainian military action in the east was "pushing the situation into a deadlock, making it increasingly difficult to organize a dialogue."

Ushakov said that Putin would visit Paris on June 5, where he would meet with French President Francois Hollande and then travel to Normandy the next day for the anniversary of marking the 70th anniversary of the allied landing in Normandy.

It will be Putin's first meeting with President Barack Obama and other Western leaders since the start of Ukraine's crisis. Ushakov said that there are no plans for any formal meetings, but Putin would likely have informal contacts with other leaders.

Meanwhile, Poland's Foreign Ministry said a Polish Catholic priest who was abducted in Donetsk has been released.

The priest, Pawel Witek, was reported missing on Tuesday after he failed to show up for prayers.

Marcin Wojciechowski, spokesman for Foreign Minister Radek Sikorski, said Wednesday that the "abducted priest" was "free, safe and sound" and under Polish diplomats' care. Poland's consul general in Donetsk, Jakub Wolasiewicz, had worked to obtain his release.

Poland's news agency PAP quoted a Polish bishop in Ukraine, Jan Sobilo, as saying Witek was held at the security service building which was under the control of the separatists.

Fox News’ Greg Palkot and the Associated Press contributed to this report.