A second traveller to the U.S. has been diagnosed with MERS infection, health officials say.

The Florida Department of Health and the U.S. Centers for Disease Control and Prevention discussed the most recent case of MERS, or Middle East Respiratory Syndrome, during a news conference on Tuesday.

MERS is a respiratory illness that begins with flu-like fever and cough, but can lead to shortness of breath and pneumonia.

The latest case involved a person who flew to the United States on Saudi Airlines Flight 113 from Jeddah to London, and transferred at Heathrow for onward travel, Public Health England (PHE) said in statement.

No sex, age or citizenship details were immediately available for the latest U.S. case. The person is in hospital in Orlando, Fla., in isolation and doing well, the CDC said.

On May 1, the person flew from Jeddah to London and felt unwell with fever, chills and a slight cough. The traveller continued on to Boston, Atlanta and Orlando. On May 8, the patient went to the emergency department of a Florida hospital.

"Risk of transmission is considered extremely low, but as a precautionary measure, [Public Health England] is working with the airline to be able to contact U.K. passengers who were sitting in the vicinity of the affected passenger to provide health information," the British health authority said in a statement.

The CDC said it believes the latest health-care worker hospitalized with MERS was in a facility that cares for patients with MERS.

The CDC said as far as it is aware, the patient did not wear a mask while in transit.

Infection control in hospitals key

Since the incubation period for MERS is thought to be about five days with an outer limit of 14 days, the travelling public can be reassured that they likely would have developed symptoms by this point, the CDC said.

As a precaution, the latest patient's family members are staying at home and monitoring themselves for any symptoms of respiratory illness, such as fever and cough, said Dr. John Armstrong, Florida’s state surgeon general and secretary of health.

Meticulous infection control within hospitals is considered key to fighting cases of MERS, the CDC said. The agency noted that health-care workers or family members directly caring for someone sickened with MERS are considered at greatest risk of contracting the illness.

Most cases have been in Saudi Arabia or elsewhere in the Middle East. But earlier this month, a U.S. medical facility diagnosed MERS in a man who travelled from Saudi Arabia to Indiana.

The man, an American, improved and was released from the hospital late last week. Tests of people who were around the man have all proved negative, health officials have said.

"A second case is unwelcome, but not unexpected news," Dr. Thomas Frieden, director of the CDC, told reporters. "The risk to the general public is extremely low."

WHO calls emergency meeting

MERS belongs to the coronavirus family that includes the common cold and SARS, or severe acute respiratory syndrome, which caused some 800 deaths globally in 2003.



The MERS virus has been found in camels, but officials don't know how it is spreading to humans. It can spread from person to person, but officials believe that happens only after close contact. Not all those exposed to the virus become ill.

It appears to be unusually lethal — by some estimates, it has killed nearly a third of the people it sickened. That's a far higher percentage than seasonal flu or other routine infections. But it is not as contagious as flu, measles or other diseases. There is no vaccine or cure and there's no specific treatment except to relieve symptoms.

Meanwhile, the World Health Organization has scheduled an emergency meeting on MERS for Tuesday in Geneva.

"The recent surge in number of cases that we have seen in Saudi Arabia and the United Arab Emirates, as well as some cases that have been exported to other countries, raised public concern and raised questions whether the virus has changed in any way and this is what members of the emergency committee will be looking into tomorrow," said WHO spokesman Tarik Jasarevic.

As of today, there have been 538 laboratory confirmed cases worldwide including 145 deaths. So far, all had ties to the Middle East or to people who had travelled there.