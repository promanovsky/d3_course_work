Relatives of Ebola patients in Sierra Leone have been removing their loved ones from community health centres despite protests from medical staff, amid warnings from the World Health Organisation that the deadly and highly infectious disease is still spreading across west Africa nearly two months after an outbreak was first reported.

Guinea's capital, Conakry, has recorded its first new Ebola cases in more than a month, and five people have died in Sierra Leone's first confirmed outbreak of Ebola virus, according to the WHO.

"The situation is serious, you can't say it is under control as cases are continuing and it is spreading geographically," Dr Pierre Formenty, a WHO expert who recently returned from Guinea, told a news briefing in Geneva on Wednesday.

"There was no decline. In fact it is because we are not able to capture all the outbreak that we were under the impression there was a decline," he said.

In Sierra Leone, the family of one woman said they had removed her from a clinic in Koindu town, in the country's east, because they did not trust the medical system and feared she would die if a planned transfer to the general hospital in the town of Kenema went ahead, Amara Jambai, the health ministry's director of disease prevention and control, told Reuters.

On Wednesday, Jambai told the BBC that a total of six patients had been "aggressively" removed from care in Koindu, in defiance of medical staff.

When asked about the risk posed by the woman's removal from hospital in Kenema, Jambai said: "She can infect others, her family members and also those in the community. There is no news of her condition because she has been taken away so we need to search and find her and make sure that it [Ebola] doesn't spread."

Jambai said MPs and community leaders would try to talk to family members and persuade them to return her to hospital. She said there had been no news of the woman, whose identity has been kept secret, since she was taken from Koindu to her village nearby.

In Guinea, two districts north of Conakry previously untouched by the disease confirmed outbreaks through laboratory testing, the WHO said. Twelve cases, including four deaths, were reported in Telimele and Boffa between 23 and 26 May, while suspected Ebola infections were documented in the adjacent districts of Boke and Dubreka.

Aboubacar Sidiki Diakité, who heads the Guinean government's efforts to halt the virus's spread, said the origins of all the new outbreaks had been traced back to cases in Conakry. "The problem is that there are families that refuse to give information to health workers. They hid their sick to try to treat them through traditional methods," he told Reuters.

The WHO has documented a total of 281 clinical cases of Ebola, including 185 deaths in Guinea since the virus was first identified as Ebola in March. The disease is thought to have killed 11 people in Liberia, thought there have been no new cases there since 9 April.

Ebola is endemic to Democratic Republic of Congo, Gabon, Uganda and South Sudan. Researchers believe the west African outbreak was caused by a new strain of the virus.