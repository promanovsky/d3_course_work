Heartbreaker star Vanessa Paradis often feels nostalgic about the past.

The 41-year-old actress, who split from Johnny Depp in 2012 after a 14-year relationship, further stated that "it's a good nostalgia."

Speaking to France's Marie Claire magazine about whether she regrets the past, Paradis said: ''These things are too personal. I don't want to spill my soul to a magazine, and moreover, I'm very lucky.

''I don't know if I'm really melancholic because I like to live in the present and make the most of the positive things that exist. Of course, I'm sometimes nostalgic about the past, but it's a good nostalgia.''

Paradis, who has two children, Lily-Rose and Jack, with Depp, further explained that becoming a mother transformed her, saying: ''My biggest change has been becoming a mother. There isn't anything stronger or more intense than that transformation, in your mind, your body and your life.''