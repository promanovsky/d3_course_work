World Health Organization Director-General Margaret Chan warns of a polio threat returning, April 11, 2014. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

GENEVA, May 5 — The World Health Organization warned today that polio has reemerged as a public health emergency, after new cases of the crippling disease began surfacing and spreading across borders from countries like Syria and Pakistan.

“The conditions for a public health emergency of international concern have been met,” WHO assistant director general Bruce Aylward told reporters in Geneva following crisis talks on the virus long thought to be on the road to extinction.

“If unchecked, this situation could result in failure to eradicate globally one of the world’s most serious vaccine-preventable diseases,” he added.

The UN health agency convened the two-day closed-door emergency talks last week amid concern that the virus, which currently affects 10 countries worldwide, was spreading.

Between January and April this year—usually considered the low season for polio transmission—three new importations of the virus were detected, from Pakistan to Afghanistan, Syria to Iraq and Cameroon to Equatorial Guinea, WHO said.

“A coordinated international response is deemed essential to stop the international spread,” Aylward said.

Polio, a crippling and potentially fatal viral disease that mainly affects children under the age of five, has come close to being beaten as the result of a 25-year effort.

In 1988, the disease was endemic in 125 countries, and 350,000 cases were recorded worldwide, according to WHO data.

Today, the virus is considered endemic in only three countries: Afghanistan, Nigeria and Pakistan.

And last year, 417 cases were detected globally, and so far this year there have been 74 cases, 59 of them in Pakistan, Aylward said.

Although the infection rates remain tiny compared to previous decades, Aylward stressed that until the virus is completely exterminated, “it is going to spread internationally, and it is going to find and paralyse susceptible kids”.

Whole world at risk

“There is always a risk that if the virus is reintroduced to a polio-free area, it could become endemic again,” he said, warning that without eradication, “it could become endemic again in the entire world”.

WHO was especially alarmed that the recent cross-border spread of the disease came during the traditional low season, warning that the situation could deteriorate as the high season begins in May.

Considering that this happened “in the low season, this poses now a very real threat to the global eradication efforts,” said Helen Rees, who serves on the WHO polio research committee.

The move to categorise polio as a public health emergency of international concern shows how seriously the UN health body treats an increase in numbers.

The only other time this categorisation has been used was at the beginning of the H1N1 swine flu pandemic in 2009, WHO spokesman Gregory Hartl said.

Today’s announcement carries recommendations for countries infected with the disease to implement vaccine requirements for all international travel.

The WHO called on Pakistan, Cameroon and Syria, seen as posing the greatest risk of exporting wild poliovirus, to ensure all residents and long-term visitors receive a polio vaccine between four weeks and a year before travelling abroad.

For urgent travel, at least one vaccine dose should be given before departure, according to the emergency committee, which also called for all travellers to be issued certificates proving they have been immunised.

The same measures should be encouraged in Afghanistan, Equatorial Guinea, Ethiopia, Iraq, Somalia and Nigeria, which have reported cases but are not currently exporting polio, the WHO said, calling for them to remain in place until a year has passed without new exports of the disease.

“It is in the interest of these countries to get this in place extremely quickly because the risk of the exportation will go up with the high season, which is starting really as we speak,” Aylward said.

Aylward pointed out though that in places like war-ravaged Syria, where there is no chance the government can put in place a mass-vaccination system for those fleeing the country, NGOs and UN agencies were picking up the slack and ensuring vaccination of refugee populations.

The fact that the virus is spreading raises particular concern for the large number of polio-free but conflict-torn and fragile countries around the world, WHO said.

WHO chief Margaret Chan asked the committee to reassess the situation in three months, or before in the case of drastic changes.

The virus has also been found in the sewage system in Israel, but no cases have been discovered among that country’s highly-vaccinated population.

The virus was able to circulate, without paralysing anyone, because Israel had previously used an inactivated poliovirus vaccine (IPV), which does not provide immunity in the gut or intestines. — AFP