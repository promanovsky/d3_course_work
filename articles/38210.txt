European home improvement retailer Kingfisher Plc (KGF.L) Tuesday reported a higher profit for fiscal 2013, reflecting growth in sales and an exceptional credit from final resolution of a French tax case. The board has proposed a higher final dividend. The shares rose more than 5 percent in the early morning trade.

Looking ahead, the firm anticipates an extremely challenging first quarter, but sees the remainder of year more encouraging, except for persistent weak economic backdrop in France.

Chief Executive Sir Ian Cheshire said, "We finish a challenging year in good shape, with our self-help programme meaning we have grown profit and economic return...The economic backdrop was generally soft across Europe for much of the year, particularly in France, our most significant market."

In France, sales increased 5.5 percent to 4.42 billion pounds, while retail profit edged down 0.4 percent to 396 million pounds. In UK & Ireland, sales improved 1 percent to 4.36 billion pounds, and retail profit increased 3.3 percent to 238 million pounds.

Announcing its preliminary results for the fiscal year ended February 1, 2014, the company said its pre-tax profit increased to 759 million pounds from 691 million pounds in the prior year.

For the year, the Group booked exceptional income of 131 million pounds, while the firm had a charge of 25 million pounds last year. The exceptional gain was mainly related to successful resolution of Kesa demerger French tax case.

Excluding exceptional items, profit before tax was 742 million pounds, compared to 717 million pounds a year earlier.

On a per share basis, earnings increased to 29.7 pence from 23.8 pence per share in the preceding year. Adjusted earnings for the current fiscal were 23.2 pence, while the company posted 22 pence last year. Kingfisher has restated its prior-year results.

Annual sales grew 5.2 percent to 11.13 billion pounds from 10.57 billion pounds in the prior fiscal. Revenues rose 3.5 percent at constant currency.

Retail profit improved 3.5 percent to 805 million pounds.

The board has also proposed a final dividend of 6.78 pence, up 6.4 percent from last year, to shareholders on record May 16, 2014, payable June 16. This will result in a full-year dividend of 9.9 pence, an increase of 4.7 percent from last year.



KGF.L is currently trading at 427.30 pence, up 20.80 pence or 5.12 percent, on a volume of 3.76 million shares on the LSE.

For comments and feedback contact: editorial@rttnews.com

Business News