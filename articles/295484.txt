Scientists and art experts have found a hidden painting beneath one of Pablo Picasso's first masterpieces, The Blue Room, using advances in infrared imagery to reveal a bow-tied man with his face resting on his hand. Now the question that conservators at the Phillips Collection in Washington hope to answer is simply: who is he?

Picasso's The Blue Room (detail), painted in 1901. Photograph: AP

It's a mystery that is fuelling new research about the 1901 painting created early in Picasso's career while he was working in Paris at the start of his distinctive blue period of melancholy subjects.

Curators and conservators revealed their findings for the first time last week. Over the past five years, experts from the Phillips Collection, National Gallery of Art, Cornell University and Delaware's Winterthur Museum have developed a clearer image of the mystery picture under the surface. It is a portrait of an unknown man painted in a vertical composition by one of the 20th century's great artists.

"It's really one of those moments that really makes what you do special," said Patricia Favero, the conservator at the Phillips Collection who pieced together the best infrared image yet of the man's face. "The second reaction was, well, who is it? We're still working on answering that question."

Conservators long suspected there might be something under the surface of The Blue Room. Photograph: AP

In 2008, improved infrared imagery revealed for the first time a man's bearded face resting on his hand with three rings on his fingers. He is dressed in a jacket and bow tie. A technical analysis confirmed the hidden portrait was a work Picasso had probably painted just before The Blue Room, curators said. After the portrait was discovered, conservators used other technology to scan the painting for further insights.

The mystery man. Photograph: AP

Conservators long suspected there might be something under the surface of The Blue Room, which has been part of the Phillips Collection since 1927. Brushstrokes on the piece clearly do not match the composition that depicts a woman bathing in Picasso's studio. A conservator noted the odd brushstrokes in a 1954 letter, but it was not until the 1990s that an x-ray of the painting first revealed a fuzzy image of something under the picture. It was not clear, though, that it was a portrait.

"When he had an idea, you know, he just had to get it down and realise it," the curator Susan Behrends Frank said, revealing that Picasso had hurriedly painted over another complete picture. "He could not afford to acquire new canvases every time he had an idea that he wanted to pursue. He worked sometimes on cardboard because canvas was so much more expensive."

Patricia Favero, associate conservator at The Phillips Collection, reveals the find. Photograph: Evan Vucci/AP

Experts are researching who this man might be and why Picasso painted him. They have ruled out the possibility that it was a self-portrait. One possible figure is the Paris art dealer Ambroise Vollard who hosted Picasso's first show in 1901. But there is no documentation and no clues left on the canvas, so the research continues.

Favero has been collaborating with other experts to scan the painting with multi-spectral imaging technology and x-ray fluorescence intensity mapping to try to identify and map the colours of the hidden painting. They would like to recreate a digital image approximating the colours Picasso used.

Curators are planning the first exhibit focused on The Blue Room as a seminal work in Picasso's career for 2017. It will examine the revelation of the man's portrait beneath the painting, as well as other Picasso works and his engagement with other artists.

For now, The Blue Room is on a tour to South Korea until early 2015 as the research continues.

Hidden pictures have been found under other important Picasso paintings. A technical analysis of La Vie at the Cleveland Museum of Art revealed Picasso significantly reworked the painting's composition. And conservators found a portrait of a moustached man beneath Picasso's painting Woman Ironing at the Guggenheim Museum in Manhattan.

Dorothy Kosinski, the director of the Phillips Collection, said new knowledge about Picasso and his process could be discovered through the high-tech collaboration among museums.

"Our audiences are hungry for this. It's kind of detective work. It's giving them a doorway of access that I think enriches, maybe adds mystery, while allowing them to be part of a piecing together of a puzzle," she said. "The more we can understand, the greater our appreciation is of its significance in Picasso's life."