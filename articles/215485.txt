Dark Horse Comics will wrap its Star Wars line in August.

The publisher will conclude its ongoings and miniseries in the summer ahead of its license termination.



Dark Horse's August solicitations have revealed that Star Wars #20, Star Wars: Legacy #18 and Star Wars: Darth Maul - Son of Dathomir #4 will end their respective titles.

Those comics will all be collected in trade paperbacks which were also solicited for release in October.

A Dark Times Gallery Edition will also be released later in the year.



Following the purchase of Lucasfilm by Disney, the license for Star Wars comics has been transferred to Disney subsidiary Marvel Comics.

The Star Wars extended universe has been removed from canon as part of the plans to extend the film franchise into Episode VII and beyond.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io