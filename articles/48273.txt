HTC One M8 Review Video Live From Unveiling in NYC (Hands-On Preview Video) HTC One M8 Review Video Live From Unveiling in NYC (Hands-On Preview Video)

Email Print Img No-img Menu Whatsapp Google Reddit Digg Stumbleupon Linkedin Comment

The new HTC One M8 was unveiled at a special media event held by the company in New York City yesterday.

The Christian Post was able to attend and had the opportunity to speak with HTC Senior Product Manager Jason Vendramin about the brand new handset. He gave us a brief demonstration of the new device in the video below.

The HTC One M8 release date, specs and new features were all revealed this week.

The company's CEO Peter Chou started off the event by discussing the success of the original HTC One M7 and commented on the strategy used to create the latest version.

"People are still saying the HTC One is the best, most beautiful smartphone," said Chou.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit

The device features a refined version of the all metal design found on the original One.

For the new HTC One M8 Chou stated that its "design is so pure and honest" and that "HTC has pushed the use of metal further than anyone in the industry." He also took a shot at some of his company's competitors who use "too much plastic."

Some of the new features on the HTC One M8 include a dual camera, the updated Sense 6, and a new version of BlinkFeed that can be customized to bring in feeds related to just about any particular topic.

The battery life has been greatly improved on the new model as it is said to last 40 percent longer than the original and can work for up to 2 weeks without being charged on standby mode. The speakers on the new model are 25 percent louder and new sensors that run in low power allow for single gesture unlocking. Users will be able to check the time without having to hit the power button and unlock, and to answer a call all one must do is hold the device up to their ear.

The smartphone was actually made available through many carriers at 1 p.m. today including AT&T via online and at Verizon retail outlets. It will go on sale in 100 countries before the April 10.

Free CP Newsletters Join over 250,000 others to get the top stories curated daily, plus special offers! Submit