Among Western stars of the late 1930s, white moviegoers saw their demographic reflected by Gene Autry, Roy Rogers and the young Duke, John Wayne. Black moviegoers had Herbert Jeffrey, also known as Herb Jeffries. A six-foot-seven cool drink of water with large, handsome features and a suave baritone, Jeffrey rode the range as the star of four B-minus Western musicals from 1937 to 1939: Harlem on the Prairie, Two Gun Man from Harlem, Harlem Rides the Range and the movie that gave the actor his nickname: The Bronze Buckaroo.

He also sang with Duke Ellington’s orchestra, reaching the pop-charts’ top 20 with the 1941 standard “Flamingo.” And when the Victor label misspelled his name as Herb Jeffries, he kept it for the rest of his long career as a crooner and actor. Jeffries died Sunday at West Hills Hospital in the San Fernando Valley. He was 100 — maybe. And was he black? Maybe a little. No one can validate the name, age or ethnicity of this pioneering, multicultural man of mystery.

(READ: Corliss on the stars of early black-cast films)

Mainstream culture of the 1930s and ’40s had little room for African-American actors; they got work, if at all, playing maids, Pullman porters and buffoons. Moviegoers of color found few role models, few actors of agreeable star quality, until Jeffrey persuaded B-minus producer Jed Buell to finance a series of below-budget Western musicals. Buell — who had already made history of a sort with The Terror of Tiny Town, the first and only all-midget Western musical — agreed, and put Jeffrey in the saddle.

Perfecting his horsemanship and performing many of his own stunts, Jeffrey gave African-American kids a hero to root for — a minority movie icon. “I liked Gene Autry and Hopalong Cassidy,” blues guitarist B.B. King, recalling the B Westerns of his youth, told John Berlau of American Profile in 2006. “But [Herb] happened to be a black one, and that made it very good.”

(READ: An American Profile of Herb Jeffries)

As an easy-going cowpoke usually named Bob Blake, he dispensed justice and warbled such tunes as “A New Range in Heaven,” “Almost Time for Roundup,” “Prairie Flower” and his theme song “I’m a Happy Cowboy” (with the refrain “A happy cowboy’s work is fun”). True to the peculiar politique of black-cast movies, the Jeffrey Westerns were, if not exactly racist, then shadist. As film historian Donald Bogle observed in his invaluable Toms, Coons, Mulattoes, Mammies, & Blacks, Jeffries and his light-skinned leading ladies were the “whites” in these films; actors of darker visage like Mantan Moreland and Spencer Williams Jr. played the supporting villains and comic relief.

The oddest aspect of these charming, disposable films: their star — born Umberto Alejandro Ballentino in 1913 or 1914 — was mostly, perhaps totally Caucasian. “My mother was Irish, my father was Sicilian, and one of my great-grandparents was Ethiopian,” Jeffries told The Oklahoman in 2004. That would make him, in the parlance of slave-era America, an “octoroon” — of one-eighth African heritage — or exactly as “black” as actor Peter Ustinov, whose great-grandmother was an Ethiopian princess. “So I’m an Italian-looking mongrel with a percentage of Ethiopian blood,” Jeffries said, “which enabled me to get work with black orchestras.” And in black Western musicals.

(READ: Oscars for Peter Ustinov and other ‘black’ actors)

In short, Jeffries passed for black — just as the Greek kid Johnny Otis, the R&B singer-producer would do in the ’50s, saying, “I decided that if our society dictated that one had to be black or white, I would be black.” In his Westerns, Jeffries used makeup to give his face a duskier hue, and surrounded himself with actors of color. What white guy in the Hollywood ’30s would do that?

(READ: Corliss’s tribute to R&B legend Johnny Otis)

Maybe a talented fellow who needed the work. Most of Jeffries’ stints with Ellington, Cab Calloway and Earl “Fatha” Hines were short-lived, despite his graceful vocals on their recordings. “He has these gorgeous tones, and he really knows how to phrase a ballad,” the eminent jazz critic Gary Giddins told the New York Times. “The mystery is why that didn’t lead to a bigger career.”

Actually, that’s just one of the Jeffries conundrums. Why, after starring in the Buell Westerns, did this smooth dude get so few later movie roles? (He played the title role in the 1957 B film Calypso Joe, costarring the young Angie Dickinson.) What about his five marriages, including one to stripper Tempest Storm? How did he manage to earn a star on the Hollywood Walk of Fame but precious little notice as a pioneering movie star? And, most tantalizingly, just how bronze was the Bronze Buckaroo?

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.