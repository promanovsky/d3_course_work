Could the MacBook Pro with Retina display be headed for a refresh? Well according to a recent photo that surfaced in China, it seems that the answer is yes. As you can see in the photo above, it seems that someone at the Apple Store in China has posted up signage at the Paradise Walk outlet which shows off updated specs.

Advertising

The signage, which is displayed on an iPad, shows that Apple has at the very least updated the 15-inch MacBook Pro model with Retina display. There are three configurations that customers can choose from, and all of which will come with faster Intel Core i7 CPUs, and the good news is that even the base model will sport 16GB of RAM by default!

This is good news for those who don’t want to spend too much money, but still wants a relatively speedy computer anyway. Apple also seems to have maxed out their highest-end configuration and will sport an Intel Core i7 CPU clocked at 2.5GHz, 16GB of RAM, a 1TB SSD, an Intel Iris Pro and NVIDIA GeForce GT 750M GPU with 2GB of VRAM.

Apple has yet to officially update their online store with the updated specs, so safe to say that this is not an official announcement. However earlier this year, analyst Ming-Chi Kuo did mention that an update to the MacBook Pro could be happening in Q3, so perhaps we will learn more in August or September.

Filed in . Read more about Macbook Pro.