"Transformers: Age of Extinction" is in theaters Friday, but how does the sequel pull off all of its action intensive scenes?

Well, it helps to have a great stuntman like Dan Mast.

The 26-year-old, based in California, has been doing stunt work for the past five years on films including "Battleship," "After Earth," and "Divergent."

His latest film will be the fourth installment of the popular "Transformers" series, "Transformers: Age of Extinction," in which Mast will double for the film's star, Mark Wahlberg.

Business Insider recently spoke with Mast about his experiences on the film.

Mast taking a death-defying leap for a stunt in the film. Tyrone Siu / Reuters

Mast says one of his first issues on the movie wasn't leaping off a tall building or doing an intricate stunt, but rather finding a place to work out.

"It was hard finding a gym close enough because we were living out of hotels," said Mast. "And hotel gyms are awful."

Once Mast found a location to bulk up, his regime included following strict workouts of intense weight lifting (his goal was an hour a day, 5-6 days a week) and a steady diet of protein shakes.

Capturing Wahlberg's look, on the other hand, didn't require the same effort.

"All we had to do was match the hair color and after that, that was it," said Mast. "There was no wigs, no prosthetics, and no makeup other than the occasional dirty blood."

For Mast, working on "Transformers" was very different from anything he had done before because of the fast pace of the film's director, Michael Bay.

"It was very fast paced," Mast explained about the film's production. "From the minute Bay walks on set to the minute he walks off we're going, going, going 100 miles per hour. Personally I like it like that."

The film's lack of down time did keep Mast on his toes, but it also didn't give him a lot of time to get to know the man he doubled for on set.

"We didn't have a lot of hanging out ... but he was very cool," Mast said of Wahlberg.

Dan Mast (left) with Mark Wahlberg on set of "Transformers: Age of Extinction." Courtesy of Dan Mast

When there were breaks, Wahlberg took the time to say hello and was very humble, which is something that isn't always a standard in the industry.

"I hear stories of other actors that just show up to set and leave, sometimes their doubles don't even get to interact with them," Mast added. "It was nice to work with someone who wasn't like that."

While many will head out to see "Transformers" this weekend for the special effects and CGI Autobots, Mast hopes audiences don't lose sight of how much realistic stunt work went into the making of the film.

"The great thing about working with Michael Bay is that if he wants to blow something up, he's going to blow it up," said Mast. "We're not pretending to run away from things. We're not faking reactions... a lot of this is very real, the only thing they're adding in later are the robots."

For example, many of the Michael Bay signature pyrotechnics that Mast had to flee from on set weren't put together in the editing room.

Paramount / Transformers: Age of Extinction trailer

"I hope they [the audience] don't think that a lot of these explosions were just computer graphics because they weren't," Mast added. "I hope people realize the reality of it."