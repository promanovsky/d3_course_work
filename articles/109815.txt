Tax season is nearing its end. For you last minute filers here are some tax deduction options, and for early filers (congratulations!) consider these ideas for the next season.

Filing for deductions can be tricky business, since it can be hard to decipher where the lines in the IRS tax code begin and end. Bankrate put together some tax deduction guidelines. ALL deductions require a paper trail, so if you don’t already hang on to receipts, start now so you’ll have them next April.

Starting simple, how about deducting the clothes you but/dry clean/and wear on the job? One TV anchor women tried to deduct thousands in clothing and upkeep for wearing on air, but the U.S. Tax Court ruling disagreed. However, you can deduct cleaning for business uniforms or uniforms for nonprofit organizations. For business clothes “not suitable for everyday use”, i.e. worn out, upkeep for the clothes can be claimed as unreimbursed business expense on schedule A.

For the phone bill, the IRS does not allow your first landline to be claimed. Still, you can deduct business long distance calls and a second landline for business is fully deductible. If you’re an employee this claim is put in schedule A. For self employed taxes tack this on to Schedule C or C-EZ.

For commuting, only trips between workplaces can be considered deductible. This means the to-and-from is entirely up to you to cover. But there’s good news in that visits to clients and out-of-office meetings can be claimed for deduction. As always, be sure to keep records of your purchases if you intend to use them for deduction.

And that nose job? Plastic surgery is specifically outlined by the IRS as not deductible. Unless, that is, the surgery it “is necessary to improve a deformity arising from, or directly related to, a congenital abnormality, a personal injury resulting from an accident or trauma or a disfiguring disease.”

For volunteer time, you can’t compensate yourself hourly or estimate a cost for service project. Sorry. But you can deduct travel expenses and cost of materials.

For taxes in general, a good rule to follow is just to stick to common sense. The IRS watches deductions, and certain things can raise a red flag on your taxes. If you have questions about taxes or deductions, it’s a good idea to ask an expert, someone who is more familiar with the IRS code than your common Joe.