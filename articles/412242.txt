Google and Barnes & Noble announced a new service that threatens to undercut the famous undercutter of online book sales, Amazon, by offering free same-day service for subscribers.

Beginning Thursday, shoppers in West Los Angeles, San Francisco's Bay Area and Manhattan will receive same-day delivery on books ordered through Google Shopping Express, according to the New York Times.

"It's our attempt to link the digital and physical," said Michael P. Huseby, Barnes & Noble's CEO.

The book store has closed 63 locations in the past five years, and it has little luck with the Nook tablet sales.

The partnership with Google Shopping is seen as a way of increasing traffic, since the one-year-old service is already working with Costco, Walgreens, Staples and Target for same-day deliveries.

"Many of our shoppers have told us that when they read a review of a book or get a recommendation from a friend, they want a really easy way to buy that book and start reading it tonight," Tom Fallows, director of product for Google Shopping Express, told NYT. "We think it's a natural fit to create a great experience connecting shoppers with their town's Barnes & Noble."

In addition, the authors and book-buyers who are critical of Amazon's tactics, as well as its issues with some publishers, are likely to be encouraged to support the new endeavor.

"We're thrilled to be early in the process," Jaime Carey, Barnes & Noble's chief merchandising officer, told CNET. "We really see this as an opportunity to connect more customers to their local Barnes & Noble."

Carey added that the service is likely to expand to more locations.

The Google service is $4.99 per order, compared to Amazon's $5.99 for same-day delivery for Prime members, and $8.99 for non-members, according to CNET. A 6-month trial membership is available for free.