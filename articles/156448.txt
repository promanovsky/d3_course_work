Tech titan Apple reported a 4.6% increase in quarterly revenue on Wednesday, handily beating Wall Street expectations after selling significantly more iPhones than analysts had predicted.

The company also said it plans to increase its stock buyback program and dividend, pleasing shareholders who have called for the company to distribute more of its cash.

The solid results, which amount to the strongest non-holiday quarter in Apple history, encouraged investors who have grown restless about a perceived lack of innovation at the company. Apple shares soared as much as 8% in after hours trading, adding $33 billion in value and sending the company’s market capitalization back over the $500 billion level.

IPhone sales were particularly strong, with the company selling 43.7 million units, substantially more than 37.7 million that analysts had been expecting.

“These are surprisingly good numbers, especially on the iPhone,” Piper Jaffray technology analyst Gene Munster said in an appearance on CNBC shortly after the results were released. “Investors should breathe a sigh of relief about these numbers.”

Apple reported revenue of $45.6 billion and net profit of $10.2 billion, or $11.62 per share, compared to revenue of $43.6 billion and profit of $9.5 billion, or $10.09 per share one year ago. For the current quarter, Apple forecast revenue between $36 billion and $38 billion, slightly below that $38.1 billion that analysts had been expecting.

“We’re very proud of our quarterly results, especially our strong iPhone sales and record revenue from services,” Apple CEO Tim Cook said in a statement. “We’re eagerly looking forward to introducing more new products and services that only Apple could bring to market.”

On a conference call with Wall Street analysts, Cook said that Apple established a new iPhones sales record in the so-called BRIC countries of Brazil, Russia, India, China. Earlier this year, Apple announced a deal to make the iPhone available for sale to China Mobile’s 760 million customers. Cook said the company plans to triple its number of retail locations in China over the next two years.

The iPhone has been available on smaller carriers in China for years, but sales have faced pressure in the face of lower-cost competition from devices made by Samsung and other manufacturers that use Google’s Android mobile operating system. Analysts estimate that Apple could sell between 20 million and 30 million iPhones on China Mobile’s network next year.

As usual, Cook declined to go into detail about the new products that the company is developing, but said to expect new gadgets this year. “We’ve got some great things there that we’re working on that I’m very proud of and very excited about,” Cook said. Speculation has centered on a new TV product or possibly a wearable computing device like an Internet-connected wristwatch.

Apple has not introduced a new product category since the death of Apple co-founder Steve Jobs in October 2011, prompting fears about the state of innovation at the company. Still, Apple has made impressive improvements to its existing product lines, and the iPhone and iPad are considered the gold-standard in their respective categories.

Apple also announced a rarely seen seven for one stock split — which means that each existing shareholder will receive six additional shares — in an effort to lower the share price and make the company more accessible to small investors. The company, which is now sitting on $156 billion in cash, also boosted its stock buyback program to $90 billion, and increased its dividend by 8% to $3.29 per share.

“We generated $13.5 billion in cash flow from operations and returned almost $21 billion in cash to shareholders through dividends and share repurchases during the March quarter,” Apple CFO Peter Oppenheimer said in a statement. “That brings cumulative payments under our capital return program to $66 billion.”

Cook added: “We’re confident in Apple’s future and see tremendous value in Apple’s stock, so we’re continuing to allocate the majority of our program to share repurchases.”

In a message posted on Twitter, billionaire investor Carl Icahn, who has been agitating for Apple to return more of its cash to shareholders, said he was “very pleased” with the company’s plan to increase its stock buyback program. He added that he continues to believe that Apple is “meaningfully undervalued.”

In one sour note, iPad sales fell short of analyst forecasts, with the company selling 16.35 million units compared to expectations of 19.7 million. Cook explained the difference by saying that last year Apple increased iPad inventory, but reduced it this year. On the bright side, Cook said that two-thirds of users registering iPads were new customers, and he pointed out that the iPad is the fastest growing product in Apple’s history.

On the conference call, Cook paid tribute to Oppenheimer, who is leaving the company this year, by observing that Apple is now 20-times larger than it was when Oppenheimer joined the company. “His expertise, leadership, and incredibly hard work have been instrumental to the company’s success,” Cook said.

Get The Brief. Sign up to receive the top stories you need to know right now. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.