Researchers from Albany Medical Center found that people who suffer from sleep apnea are at a higher risk of developing hearing impairments.

The study was conducted on nearly 14,000 individuals who were part of the Hispanic Community Health Study/Study of Latinos.

"We found that sleep apnea was independently associated with hearing impairment at both high and low frequencies after adjustment for other possible causes of hearing loss," said lead author Amit Chopra in a press statement.

At the start of the study, all participants underwent a sleep apnea assessment test where researchers used the apnea-hypopnea index (AHI), which indicates sleep apnea severity based on the number of apnea (complete cessation of airflow) and hypopneas (partial cessation of airflow) per hour of sleep.

Researchers also set a baseline for hearing impairment. High frequency hearing impairment was defined as having a mean hearing threshold of greater than 25 decibels in either ear at 2000, 3000, 4000, 6000 and 8000 Hz, and low frequency hearing impairment was defined as having a mean hearing threshold of greater than 25 decibels in either ear at 500 Hz and 1000 Hz.

The study authors noted that 9 percent of the participants had sleep apnea, 19 percent had high frequency hearing impairment, 1.5 percent had low frequency hearing impairment, and 8.4 percent had both high and low frequency hearing impairment.

At the end of the study, researchers found that the sleeping disorder increased the risk of high frequency hearing impairment by 31 percent and low frequency hearing impairment by 90 percent. After adjusting age, sex, background, history of hearing impairment, external noise exposure, conductive hearing loss and other factors, researchers found that sleep apnea patients were at a 38 percent higher risk of both high and low frequency hearing impairment.

Researchers also pointed out that the limitation of their study was that they didn't determine whether sleep apnea treatment would diminish these risks or not.

"Patients with sleep apnea are at increased risk for a number of comorbidities, including heart disease and diabetes, and our findings indicate that sleep apnea is also associated with an increased risk of hearing impairment" said Dr. Chopra. "The mechanisms underlying this relationship merit further exploration. Potential pathways linking sleep apnea and hearing impairment may include adverse effects of sleep apnea on vascular supply to the cochlea via inflammation and vascular remodeling or noise trauma from snoring."

Sleep apnea is a common sleeping disorder that disrupts sleep quality. It is characterized by one or more pauses in breathing or shallow breaths while sleeping. These pauses can last for a few seconds to a few minutes and can occur as frequently as 30 times an hour.

Earlier this year, University of Sydney researchers found that this sleep disorder ups the risk of cancer, stroke and death by four times. Another study by researchers at the Taipei Veterans General Hospital found that people suffering from sleep apnea have greater chances of getting pneumonia.

According to CDC estimates, 50-70 million U.S. adults suffer from sleep disorders, sleep apnea being the most common one. Risk factors include being male, overweight, and over the age of 40, but sleep apnea can strike anyone at any age, even children. If untreated, this sleep disorder can have serious consequences including high blood pressure, heart disease, stroke, automobile accidents caused by falling asleep at the wheel, diabetes, depression and other ailments.

Snoring while sleeping is one of the biggest symptoms of sleep apnea and a recent study found that women who snore during pregnancy are at a greater risk of having C-sections and giving birth to smaller babies.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.