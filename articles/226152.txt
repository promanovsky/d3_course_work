Salesforce.com (NYSE:CRM) just reported results for the first quarter of fiscal year 2015. Shares of the cloud-based customer management software vendor rose as much as 3% on the news, but then fell back to a net 1% drop later in the after-hours session.

In the first quarter, Salesforce's sales rose 37% year-over-year to land at $1.2 billion. Adjusted earnings stopped at $0.11 per share, up from $0.10 per share in the year-ago period. Both figures were slightly ahead of analyst expectations.

The non-GAAP earnings total excludes a $0.20 charge per share related to stock-based compensation, as well as a $0.07 non-cash amortization expense for purchased intangibles. Without such adjustments, Salesforce's GAAP loss increased from $0.12 to $0.16 per share, year over year.

Free cash flows nearly doubled year-over-year to $413 million.

Guidance figures for the second quarter and full year were in line with analyst targets.

Revenue growth was particularly healthy in the European segment and slower in Asia-Pacific. Nevertheless, the core American market still represents 71% of Salesforce's total sales.

"Salesforce.com continues to be the #1 CRM platform, and is the fastest growing top ten software company in the world," said Salesforce CEO Marc Benioff in a prepared statement.