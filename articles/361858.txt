Gold price slipped to trade below a three-month high on Thursday as investors awaited a string of key data releases later in the day to gauge the strength of the global economy and the fate of stimulus measures by central banks.

U.S. data on jobs, unemployment and trade in expected on Thursday, while the outcome of European Central Bank (ECB) policy meeting will also be announced.

Weaker-than-expected data and continued stimulus measures would be supportive of safe-haven gold, typically seen as a hedge against inflation.

"There isn't much liquidity today because everyone is waiting for nonfarm payrolls and the ECB," said one precious metals trader in Hong Kong.

"We might see a sharp reaction if the jobs report is good because gold has seen quite a good run up lately."

Spot gold fell 0.3 percent to $1,323.55 an ounce by 0303 GMT. It hit a 3-month high of $1,332.10 hit earlier this week as tensions in Iraq and Ukraine stoked safe-haven demand.

U.S. employers likely maintained a fairly healthy pace of hiring in June, consistent with data that have suggested a sharp economic contraction in the first-quarter was an aberration, according to a Reuters survey.

Other data on Wednesday showed that U.S. private-sector hiring hit a 1-1/2-year high in June, hurting gold.

The technical picture also looked weak with Reuters technical analyst saying gold may retrace to $1,316 as it has failed to break a resistance at $1,334 twice.

Meanwhile, physical demand for gold has been lacklustre due to the recent rally in prices. In top buyer China, domestic prices were at a discount of $1-$2 an ounce to global prices, underscoring sluggish demand.

Among other precious metals, platinum was holding close to a 10-month high of $1,517.50 hit on Wednesday as supply worries persisted even after producers in South Africa resumed work after a five-month strike.