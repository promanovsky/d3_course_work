Andrew Garfield and Emma Stone gave a message to the world via the paparazzi earlier today (June 17).

The couple spotted a group of photographers while eating at a restaurant in Manhattan, New York City and decided to use the opportunity to promote some charities and organisations by using handwritten notes.

Kristin Callahan



Both Garfield and Stone have used the tactic once before when spotted by the paparazzi, and this time decided to add some more charities to their list.

Startraks Photo





Startraks Photo



The charities listed on the card were Youth Mentoring Connection, Autism Speaks, Worldwide Orphans and Gilda's Club New York City.

Earlier this year, Garfield surprised children at a London charity by turning up as Spider-Man and playing in a playground with them.

Watch the Amazing Spider-Man cast talk to Digital Spy below:

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io