Refrain from smoking is one of several ways to reduce the risk of heart disease. UPI/Stephen Shaver | License Photo

CHICAGO, June 30 (UPI) -- All those late nights, lazy days and emptied bags of potato chips may have put your heart health in a real tight spot, but according to new research, it's likely not too late to reverse at least some of the damage.

In a new study, researchers at the Northwestern University Feinberg School of Medicine found that the risk of heart disease in adults could be reversed by the adoption of healthy habits.

Advertisement

Published this week in the journal Circulation, scientists found adults in their 30s and 40s wrest control of their heart health and reverse coronary heart disease by maintaining a healthy weight, not smoking, eating healthy, exercising and limiting alcohol consumption.

"It's not too late," said study author Bonnie Spring, a professor of preventative medicine at Northwestern's medical school. "You're not doomed if you've hit young adulthood and acquired some bad habits. You can still make a change and it will have a benefit for your heart."

Of course, the reverse is also true: if you give up health habits as you get older, your risk of heart disease will invariably go up.

"If you don't keep up a healthy lifestyle, you'll see the evidence in terms of your risk of heart disease," Spring added.

Of the 5,000 patients who participated in the Coronary Artery Risk Development in Young Adults -- a survey conducted by Northwestern researchers -- some 40 percent adopted bad habits or retired good ones as they got older.

RELATED Scientists grow functional human cartilage in lab

On the other hand, 25 percent voluntarily adopted healthier habits to improve their chances of avoiding heart disease. But as Spring pointed out: "A healthy lifestyle requires upkeep to be maintained."