British researchers are launching the largest study in the world to investigate whether using mobile phones and other wireless gadgets might affect children's brain development.

The Study of Cognition, Adolescents and Mobile Phones, or SCAMP, project will focus on cognitive functions such as memory and attention, which continue to develop into adolescence - just the age when teenagers start to own and use personal phones.

While there is no convincing evidence that radio waves from mobile phones effect health, to date most scientific research has focused on adults and the potential risk of brain cancers.

Because of that, scientists are uncertain as to whether children's developing brains may be more vulnerable than adults' brains - partly because their nervous systems are still developing, and partly because they are likely to have a higher cumulative exposure over their lifetimes.

"Scientific evidence available to date is reassuring and shows no association between exposure to radiofrequency waves from mobile phone use and brain cancer in adults in the short term - i.e. less than 10 years of use," said Paul Elliott, director of the Centre for Environment and Health at Imperial College London, who will co-lead the research.

"But the evidence available regarding long term heavy use and children's use is limited and less clear."

Mobile phone use is ubiquitous, with the World Health Organisation estimating 4.6 billion subscriptions globally. In Britain, some 70 percent of 11 to 12 year-olds now own a mobile phone, and that figure rises to 90 percent by age 14.

Cognitive abilities

Elliott and the study's principal investigator, Mireille Toledano, aim to recruit around 2,500 11 to 12 year-old school children and follow their cognitive development over two years whilst collecting data on how often, for what, and for how long they use mobile or smart phones and other wireless devices.

Parents and pupils who agree to take part in the study will answer questions about the children's use of mobile devices and wireless technologies, well-being and lifestyle. Pupils will also undertake classroom-based computerised tests of the cognitive abilities behind functions like memory and attention.

"Cognition is essentially how we think, how we make decisions, and how we process and recall information," said Toledano, who is also at Imperial College's centre for Environment and Health.

"It is linked to intelligence and educational achievement and forms the building blocks of the innovative and creative potential of every individual and therefore society as a whole."

The World Health Organisation says a large number of studies have been performed over the past two decades to assess whether mobile phones pose a potential health risk, and to date, no adverse health effects have been established.

Still, the electromagnetic fields produced by mobile phones are classified by the International Agency for Research on Cancer as "possibly carcinogenic to humans", and the global health agency has said more research into the issue is vital.

Current British health policy guidelines say children under 16 should be encouraged to use mobile phones for essential purposes only, and where possible use a hands-free kit or text.

But Toledano said this advice was "given in the absence of available evidence - and not because we have evidence of any harmful effects".

"As mobile phones are a new and widespread technology central to our lives, the SCAMP study is important to provide the evidence base through which parents and their children can make informed life choices," she said.

© Thomson Reuters 2014