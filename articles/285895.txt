By Mike Dougherty, Jan Carabeo, Andrew Kramer

PHILADELPHIA (CBS) – The SEPTA regional rail strike is over.

An executive order signed by President Obama that came down just after 7 p.m. Saturday forces train engineers back to work and both sides are required to continue negotiations with the help of an emergency mediation board.

President Obama listened to Governor Corbett’s plea for help, and it’s good news for tens of thousands of regional rail riders.

SEPTA spokeswoman Jerri Williams says Regional Rail service will be restored to full Sunday operations in the morning, starting with the first scheduled service trains runs on all 13 commuter rail lines.

“We will certainly comply with the mandates of the law and we will be given the opportunity to present our best case to the presidential emergency board,” says Arthur Davidson with IBEW Local 98.

READ: SEPTA Regional Rail Workers Go On Strike

Both sides are separated by about three percent in proposed wage increases as well as a disagreements over retroactive pay and pension contributions.

“SEPTA has consistently claimed that the pension enhancement that they have given to their other 6,000 employees doesn’t have any value, and we think that’s a lot of bologna,” BLET spokesman Steve Bruno says.

The union had requested binding arbitration to settle the disupte. SEPTA would not agree to that.

Spokeswoman Jerri Williams says the transit authority works with 17 different unions and wants to keep the same negotiation polcies for all of them.

READ: Complete Coverage ‘SEPTA Regional Rail Strike’

SEPTA Regional Rail customers should are encouraged to check online at septa.org for last minute updates regarding the status of specific trains, or they can call SEPTA customer service at 215-580-7800.

You may also be interested in these stories:



[display-posts category=”news” wrapper=”ul” posts_per_page=”5“]