Adam Sandler, Drew Barrymore Reunite at 'Blended' L.A. Premiere

Barrymore discussed the film's theme of "blended" families, with director Frank Coraci telling THR: "It felt like the right time."

Adam Sandler, Drew Barrymore, Terry Crews, Bella Thorne, Joel McHale, Wendi McLendon-Covey and the rest of the Blended cast walked the carpet at the TCL Chinese Theater on Wednesday night for the film's Los Angeles premiere. Even Andy Dick was among the throng of fans trying to get a glimpse of the cast, as he stood on top of the fences yelling for Sandler to let him in the premiere.

In the romantic comedy, after single parents Lauren (Barrymore) and Jim (Sandler) have an unsuccessful blind date at Hooter's restaurant, they both end up on the same vacation with their kids. Filmed on location in South Africa, Lauren and Jim's attraction for each other grows, and they begin to realize their families might be a better fit than they thought.

VIDEO: Adam Sandler, Drew Barrymore Sing Duet Saluting Their Three Onscreen Romances

Blended marks the third collaboration between Sandler and Barrymore, who previously starred together in The Wedding Singer and 50 First Dates. Director Frank Coraci, who directed The Wedding Singer as well as Sandler's Click and The Waterboy, said that Sandler and Barrymore's relationship matures with every film.

"It felt like the right time," Coraci told The Hollywood Reporter. "The Wedding Singer was about young love. This [Blended] was about life after a divorce coming together with parenthood. They're both parents now, so they got to bring a really great perspective with even more laughs cause they're both better than ever at acting and comedy."

After the screening, the cast went over to the Tropicana Bar and Pool at the Hollywood Roosevelt Hotel for the after-party, where guests were greeted by African singers (similar to Crew's singing group in the film) and were treated to free massages while Hooter's girls served appetizers.

STORY: Box Office: Early Tracking Soft for Adam Sandler, Drew Barrymore's 'Blended'

"I think there are so many people who have felt like they're in a family, and if it doesn't look like this very sort of formulaic type of thing that maybe they don't feel like they're given the same kind of credit and respect," Barrymore told THR. "I appreciate, now that we're in a different time, that family is people coming together, loving each other, supporting each other, having each other’s back, caring and worrying about each other, and I love that."

Barrymore, who was raised by a single mother, enjoyed the opportunity to play a strong mother alongside Sandler. The amount of respect they have for each other, she said, fuels their onscreen chemistry.

"I love that this woman that I played is happiest when her kids are happy, or that it's a film where Adam and I's romantic love story is based in being attracted to each other's parenting styles rather than, 'Oh my God, I just love him. He's so awesome,' " said Barrymore. "It's like, no, he's good with his kids and he's great with his girls and he's giving my boys something and things are better when he's around. I think that's very mature and cool and I love that film for that.

Blended hits theaters May 23.