The E3 after a console hardware launch is always an awkward one for investors.

The excitement that surrounds new game systems often has a sweeping effect on share prices of companies in the video game space—but it's hard to get as worked up about game announcements the following year.

Truth be told, the years that follow a console introduction are actually much more important for the industry, though, since it's games that ultimately move the machines. And analysts liked what they saw at this year's annual video game trade show.

"We came away from E3 with a view that the industry's pipeline of games is strong and should build momentum for the next-gen platform beginning in September and continuing through 2015 and beyond," said Eric Handler of MKM Partners.

The optimism comes from a few places. First, the reliable franchises, like Activision's "Call of Duty" and Ubisoft's "Assassin's Creed," look stronger than ever. Additionally, several smaller independent games caught the eye of showgoers, and could boost system sales of Microsoft's Xbox One and Sony's PlayStation 4. (More importantly, those independent studios could be the next big developers—and acquisition targets for publicly traded publishers.)



While this fall/holiday season will see a number of key releases, the industry is still likely another 12 months away from reaching its sweet spot from an installed-base perspective. That's true of the games, too.

Read MoreXBox One now selling for $399

Holiday 2014 has a solid lineup of titles— including "Destiny," "Forza Horizon 2" and "Super Smash Bros."—but 2015 is shaping up to be even more active. Among the games due out next year are "Halo 5: Guardians," "Uncharted 4: A Thief's End," "Rise of the Tomb Raider," "Tom Clancy's The Division" and "Rainbow Six: Siege." That will make for a very competitive holiday season, but it could also be a financial windfall for publishers.



One of the things investors will be looking at in the days that follow E3—and as the hype machines cool down—is who "won" certain aspects of the show. It's certainly a debatable point—and one that may not be settled for as long as a couple of years. But here's our take on which companies seemed to have good shows—and which have a little more work to do.