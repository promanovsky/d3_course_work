We’ve heard countless number of times that Amazon is working on a smartphone that will launch in the coming months. Some rumors hinted the launch could take place in June, but there’s no word from Amazon about it.

Many have thought it’s just a rumor, but today, we got our first sneak peak at the first Amazon smartphone, thanks to the folks at BGR.

The design of the handset looks quite different when compared to smartphones we’ve seen to date, and it seems the company plans to bring forth Amazon’s users experience to its smartphone as well. The folks at BGR also confirmed from multiple trusted sources, and confirmed what we’ve heard about the device.

As far as the specifications are concerned, the device is rumored to come with a 4.7-inch display with 720p resolution, a quad-core Qualcomm Snapdragon processor, 2GB RAM and a heavily customized version of Android, something similar to what we’ve seen on the Kindle tablets.

There’s no exact launch date of the handset, but BGR suggests it will hit the retail shelves in the coming months. Previous rumors hints at June launch, but we’ve yet to come across an official word from Amazon itself. We’ll update you as soon as we hit something.

Source: BGR

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more