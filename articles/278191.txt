Astronauts Reid Wiseman, Steve Swanson, and Alexander Gerst are stuck aboard the International Space Station, hundreds of miles away from the World Cup action set to kick off on Thursday in Brazil. Still, they're pretty excited for the games, and on Wednesday released a video that shows us just how much better soccer would be if it was played in space.

As you'd expect from a zero-gravity game of soccer, there are plenty of bicycle kicks and other trickery that we simply have a harder time doing on gravity-controlled Earth.

Of course, maybe they're just positioning themselves for a bid for the World Cup 2026 to be held aboard the International Space Station. Now that would be something that would increase viewership. But it'd probably be tough to get the millions of fans there to cheer their teams on.

The 2014 World Cup begins Thursday with an opening match between Croatia and the host country, Brazil. Check out our guide for how to stream the action from your computer or home entertainment system.