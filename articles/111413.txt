PATNA: Christians across the city are all set to celebrate the Holy Week; that is, the Lenten week of events leading to Jesus Christ 's crucifixion, with the first day being Palm Sunday commemorating Jesus Christ's triumphant entry into Jerusalem over 2,000 years ago.

Palm Sunday or Passion Sunday is celebrated on the Sunday prior to Easter which falls on April 20 this year, and blessed palm leaves are distributed among believers. "According to the Holy Bible, when Christ entered Jerusalem on this day 2014 years ago, palm branches were placed on His path by followers," parish priest of the Queen of the Apostles church, Fr Johnson Kelakath, told TOI. "We take out a small procession before the prayer services to mark the day," he said.

The 'Way of the Cross' prayers will be said on Wednesday. On April 17; that is Holy Thursday, Catholics celebrate the establishment of Holy Eucharist and priesthood. According to Kelakath, Christ insisted on servant leadership and on this day washed the feet of his disciples. Similarly, at the prayer services, the priests emulate Christ and wash the feet of 12 devotees.

There is also a tradition of celebrating the Mass of Chrism on Maundy Thursday. "However, for convenience, we offered the Mass on April 10 in the presence of Patna's archbishop William D'Souza SJ and 65 priests from across the state," Kelakath said. On the occasion, the archbishop blessed three oils - the oil of catechumens, the oil of the infirm and the oil of Holy Chrism - which will be used in the administration of different sacraments such as Baptism, Confirmation, Ordination and Anointment throughout the diocese for the year.

Over the years, Christmas has gained more popularity. However, Easter is a more important religious festival of the Christians which starts 40 days before the Holy Week and continues for seven weeks after Easter, he said.

The salvific sacrificial death of Christ on the cross is observed on Good Friday. "It is called Good Friday because Christ died for the good of the world," he said adding that all Christians (from 18 years to 60 years) are expected to fast and pray on the day. Over 15 people led by Kurji parishioner Victor Francis will enact the 'Way of the Cross' in the form of a tableau on Friday morning. Reading about the Passion of Christ and veneration of the cross will be held in the afternoon.

Holy Saturday is the only day in the Christian calendar when no Eucharistic celebrations are made. However, on Easter Sunday Christians commemorate the glorious resurrection of Christ. "There are seven reading from the Old Testament of the Holy Bible and blessing of the candle, holy water and fire is also done," Kelakath said adding, "Confessions are also organised for devotees during the entire week."

