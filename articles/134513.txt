“We have snipers all around the stadium, just in case something were to happen,” he says. “Like I said, do whatever it is you normally do. Nobody will bother you. But approach the president, and we go for the kill shot. Are we clear?”

He pauses for a moment to let the words sink in, and it feels like he isn’t only looking into my eyes, but also into my very soul with his blank, unblinking stare. Then he says the same thing again, only a little bit slower this time, making sure I know his warning is not in any way to be misconstrued as some sort of gag…..

“Approach the president, and we go for the kill shot,” he repeats. “ARE–WE–CLEAR?”