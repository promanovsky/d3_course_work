The National September 11 Memorial Museum opens to the public today. Timed to this historic moment, co-editors of Covering Catastrophe: Broadcast Journalists Report September 11 have legally transferred all publishing rights to the Memorial & Museum to support its work in perpetuity.



The Museum, before it even officially opened its doors, came under attack for having a gift shop, the same store where Covering Catastrophe will be sold. Former FDNY Deputy Chief Jimmy Riches accused the Museum of "making money off of my son's dead body."

I understand those feelings. I came very close to dying that day. I was a producer at WNBC-TV and was pounded into the ground by a wall of crushed glass, cement, and debris. Soot and ash flooded my mouth and I was certain I'd suffocate. I was taken to the hospital by ambulance. Doctors in the ER cut off my clothes and shoved tubes down my throat to help me breathe.



But I was lucky. And my son, who was safe at home and 18 months old at the time, is in 8th grade today. I can't imagine Deputy Chief Riches' loss and he's entitled to every one of his emotions.



My wish is that by supporting the Museum I'm doing what little I can to preserve the memory of Deputy Chief Riches' son and the nearly 3,000 other victims of the attacks. Because I lived, it's my duty to use whatever resources I have to make sure their lives are never forgotten, including giving away the publishing rights to my book.

My involvement with the Museum extends beyond the store and into the exhibitions. The triage tag put around my neck is on display and I've recorded my oral history. You can also hear my reflections on Track 8 of Witnessing History, the Museum's primary audio tour.

Giving back in this way has been an odd and humbling experience. Odd because journalists are taught never to get involved in the stories they cover; humbling because I realize my 9/11 story is one of tens of thousands - no more interesting or important than anyone else's.

Thirteen years later, though, I hope by supporting the museum and working with the museum store, I'll ensure all our loved ones will be honored and remembered, including Deputy Chief Riches' son.