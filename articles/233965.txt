PetSmart is the latest major pet food retailer to say it will stop selling dog and cat treats made in China because of ongoing fears that they are making pets sick.

The company plans to have the treats off all of its store shelves by March 2015.

"This is something we've been working toward for some time, and feel it's the right thing to do for pets and our customers," said PetSmart spokeswoman Erin Gray.

Investigators at the U.S. Food and Drug Administration haven't been able to prove that treats made in China are making pets ill. But since 2007, it has received more than 4,800 complaints of pet illnesses and more than 1,000 reports of dog deaths after eating Chinese-made chicken, duck or sweet potato jerky treats.

In an update of its investigation last week, the FDA said it found that antiviral drug amantadine in some samples of imported chicken jerky treats sold a year or more ago, but doesn't think it caused the illnesses. The FDA said it will continue to investigate.

Rival Petco announced on Tuesday that it would remove all Chinese-made treats from its 1,300 stores by the end of this year after shoppers pushed them to do so. The San Diego company said it has been cutting the number of those treats it sells over the past three years.

In January 2013, NBC News reported that "two of the largest retailers of pet chicken jerky treats issued voluntary recalls of several popular brands after New York state agriculture officials detected unapproved antibiotics in the products.

Nestle Purina PetCare Co. recalled its popular Waggin’ Train and Canyon Creek Ranch brand dog treats, and Del Monte Corp. officials recalled their Milo’s Kitchen Chicken Jerky and Chicken Grillers home-style dog treats from shelves nationwide.

In addition, two more firms have recalled their treats as well, including Publix stores, which recalled its private brand Chicken Tenders Dog Chew Treats and IMS Pet Industries Inc., which withdrew its Cadet Brand Chicken Jerky Treats sold in the U.S.

The voluntary recalls effectively remove the pet treats from store shelves nationwide.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

PetSmart Inc., which is based in Phoenix, owns more than 1,300 stores in North America.

Copyright 2014 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.