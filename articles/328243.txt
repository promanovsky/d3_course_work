SHANGHAI (Reuters) - A joint venture of General Motors Co in China will recall 194,107 locally made Buick vehicles due to Chinese consumer complaints about the cars' headlights, the country's quality watchdog said on Friday.

Shanghai General Motors Co Ltd, the joint venture between GM and Chinese automaker SAIC Motor Corp <600104.SS>, will recall its Buick Excelle GT cars due to a glitch which in some cases does not allow the high beam to be turned off, the General Administration of Quality Supervision, Inspection and Quarantine said in statement on its website.

The watchdog added it will closely monitor the recall, which applies to cars made between October 2009 to July 2012, and will urge fresh recalls if similar defects are found in other cars.

A China-based GM spokeswoman confirmed the recall and said the incident is not part of the auto maker's recent global recalls.

The No. 1 U.S. automaker is grappling with a series of safety problems that have prompted the recall of 20 million cars, with Chief Executive Mary Barra saying more recalls are possible.





(Reporting by Samuel Shen and Kazunori Takada; editing by Keiron Henderson)