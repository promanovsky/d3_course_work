Actress Ruby Dee, who first came to prominence as the star of n A Raisin in the Sun and Do the Right Thing, has died at the age of 91. Dee’s daughter, Nora Davis Day has confirmed the sad news.

"She died of natural causes," said Arminda Thomas, who works for Dee's family, for Reuters. "She was blessed with old age."



Dee wasn't just known for her considerable acting talent.

Before achieving prominence as an actress and activist, she studied the craft at the New York Negro Theater and then had several roles on Broadway throughout the 1940s.This was where she met her husband of 56 years and frequent collaborator, Ossie Davis. (He passed away in 2005.) She then became the first black women to play leads at the American Shakespeare Festival in Stratford, Conn. in 1965..

More: Ruby Dee Dies Aged 91: Remembering The Actress, Poet And Activist

More: Broadway's Lights To Be Dimmed In Honour Of Ruby Dee

Aside from her acting, Dee also thrived as a civil rights activist. , Dee served alongside Martin Luther King Jr. and Malcom X as masters of ceremonies for the historic March on Washington. She later spoke at the funerals of both Dr. Kiny and Malcom X. Throughout her film and television career, Dee collaborated five times with Sidney Poitier, including one of her best known films, A Raisin in the Sun. She also starred in Spike Lee's Do the Right Thing alongside her husband and won an Emmy for supporting actress for 1990's Decoration Day. Nora Davis Day is one of the three children Dee has left behind.



She remained active as a civil rights advocate throughout her life.