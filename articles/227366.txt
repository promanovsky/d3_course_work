Just in case you thought Facebook’s “Relationship Status” wasn’t awkward enough, the site has now streamlined the process of asking people if they’re single and ready to mingle.

Over the past few months, Facebook has been rolling out an “Ask" button feature, which is displayed on parts of a person's profile that have not been filled out completely. These include a Facebook friend's phone number, alma mater and -- by far the creepiest one that most commentators are paying attention to -- relationship status.

Facebook's "Ask" button displayed next to a blank "Relationship Status" field.

People on Facebook have the option to respond to these queries by simply displaying their relationship status only to the person who asked, or by sending them a message.

It's not just your love life that Facebook wants to let your friends pry into. The “Ask” button appears on other sections of the profile's “About” page, such as “Work,” "Current City” and “Hometown” -- wherever you've chosen to withhold from the Facebook community your personal information.

When you click the “Ask” button, a small popup window appears asking you to include a note explaining why you're curious about this particular information. According to The Daily Dot, users have to be friends with someone before they can use the “Ask” button, and -- because it’s all done via Facebook accounts -- no request can be anonymous.





This is the notification you will receive if one of your friends sends you an "Ask" about your relationship status.

A Facebook spokesperson told ArsTechnica that the social network has been rolling out this “Ask” feature in waves to some users since January. We reached out to Facebook to and will update if we get a response.