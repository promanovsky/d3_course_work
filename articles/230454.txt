NEW YORK (MarketWatch) — Stock futures pointed to a modest bounce at the open on Wednesday, with investors set to scrutinize comments by Federal Reserve members and the minutes from the central bank’s most recent meeting.

Retailers are in the premarket spotlight with shares of Lowe’s Cos., Target Corp. and Tiffany & Co. rising after quarterly results.

Futures for the Dow Jones Industrial Average US:DJM4 rose 67 points, or 0.4%, to 16,404, while those for the S&P 500 index US:SPM4 gained 7.2 points, or 0.4%, to 1,875.30. Futures for the Nasdaq-100 US:NDM4 added 12 points, or 0.3%, to 3,608.75.

Federal Reserve Chairwoman Janet Yellen on May 15. Getty Images

Ahead of a clutch of Fed speakers, retail results are in the spotlight. On Tuesday, that sector helped drag the Dow industrials DJIA, +0.09% to a triple-digit loss, after disappointing earnings from TJX Cos. TJX, -1.48% and Staples Inc. US:SPLS, among others. Target, Lowe’s, Williams-Sonoma are stocks to watch

Ahead of Wall Street’s open, home-improvement retailer Lowe’s LOW, +0.81% reported first-quarter earnings of 61 cents on sales of $13.09 billion. Earnings came in ahead of analyst expectations by one penny a share, but sales missed an average estimate of $13.89 billion. Lowe’s also lifted its per-share earnings view for 2015. Shares were last down nearly 1% in premarket.

Shares of Tiffany US:TIF rose 6.9% premarket, after the luxury-jewelry maker reported sales and earnings that beat estimates and lifted its outlook for fiscal 2015. Japan sales surged 20%.

Target TGT, +0.54% gained 0.4% premarket after the discount chain posted adjusted quarterly earnings of 70 cents a share, versus forecasts for 71 cents, on sales that roughly matched expectations. The report is getting particular scrutiny, given the retailer’s weak performance over the past year. On Tuesday, the company replaced the president of its struggling Canadian business.

On the downside, PetSmart Inc. US:PETM was last down 8% premarket, after it reduced its outlook as it delivered its quarterly results.

Yellen, Dudley speeches, minutes ahead

Federal Reserve Chairwoman Janet Yellen will deliver the commencement speech at Yankee Stadium for New York University students at 11 a.m. Eastern Time. The Fed chief is part of a busy Fed lineup for Wednesday.

New York Fed President William Dudley, who is a voting member of the Fed policy committee, will hold a quarterly press briefing on regional labor markets and economic conditions at 11 a.m. Eastern Time.

Read: Dudley’s speech may have given a preview of Fed minutes

Kansas City Fed President Esther George, who is not a voting member this year, will deliver a speech in Washington on the economy and banking at 12:50 p.m. Eastern Time. Voting-member Minneapolis Fed President Kocherlakota will speak on monetary policy and the economy at the Economic Club of Minnesota at 1:30 p.m. Eastern Time.

Philadelphia Fed President Charles Plosser added to Wall Street’s Tuesday woes after he said the Fed may need to act sooner rather than later if the economy accelerates.

Jerry Seib: McConnell's expensive campaign victory

Investors will get a look at the minutes of the latest Fed meeting at 2 p.m. Eastern Time, which could reveal details of when, and by how much, interest rates will be raised. The Fed continued to taper at its April 30 meeting, saying activity had “picked up recently” after a winter slowdown.

In overseas markets, European stocks SXXP, +0.64% made small gains as investors kept an eye on key European Parliament elections and purchasing-manager index data due later in the week. U.K.stocks UKX, +0.94% fell for a third day and the British pound GBPUSD, +0.29% jumped against the dollar after the release of minutes from the Bank of England’s latest meeting that were viewed as hawkish.

Asian stocks posted moderate losses across the board, with the Nikkei 225 index NIK, -0.14% off 0.6% and the Bank of Japan leaving its monetary policy on hold at its latest meeting.

Gold for June delivery GCM24, +1.04% dipped, while oil for July delivery CLN24, -2.05% pared gains ahead of a report on U.S. supply data. Citigroup also raised its Brent oil forecasts for 2014 and 2015 on Wednesday.

More must-reads from MarketWatch:

Major U.S. benchmarks approach bull-bear tipping point

Portfolio killers: 5 common investing myths

Microsoft bills new Surface tablet as laptop-killer, aiming at Apple