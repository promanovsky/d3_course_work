Threes’ Creators Publish An Epic Work Log To Show How Frustrating It Is To Be Cloned

Imitation is the sincerest form of flattery, right?

Not really. That’s just something that people like to say without really thinking about, because it sounds nice and makes them feel better. Kind of like saying “there’s no such thing as bad press.” when of-freaking-course there is.

In reality, being imitated can be crushing — especially when your thing thats being imitated is something you’ve worked on for over a year.

The developers behind Threes! (Asher Vollmer and Greg Wohlwend) spent 14 months designing their game. Within 3 weeks of release, their first big “clone” (a modified but remarkably similar title called 1024) appeared. By the end of the month, another (2048) showed up — and within weeks of that, Threes’ developers found their game being called the rip-off.

There are now dozens of Threes/1024/2048 clones in the app store. We first wrote about the massive influx of copycats over here.

To demonstrate just how much work they put into their original game, they’ve published an epic-length breakdown of nearly all of the communication (some 45,000 words) and planning that went into the game’s 14-month design.

Their goal? I won’t put words in their mouth, but this quote is the crux of it:

We want to celebrate iteration on our ideas and ideas in general. It’s great. 2048 is a simpler, easier form of Threes that is worth investigation, but piling on top of us right when the majority of Threes players haven’t had time to understand all we’ve done with our game’s system and why we took 14 months to make it, well… that makes us sad. It’s complicated and hard to express these conflicting feelings but hopefully this is a start. We are so happy with Threes and how it has done and all the response. Seriously. And even writing this feels like we’re whining about some sour grapes that we have no business feeling sour about. Like it’s not ok to feel the way we do some of the time. But we do.

It’s worth, at the very least, poking your head into. You might not get through all of it — but that’s kind of the point.