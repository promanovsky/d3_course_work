Sesame Street's Elmo and Rosita take a close look at broccoli, one of the best vegetables for maintaining stable blood sugar levels. (File/UPI/Pat Benic) | License Photo

COLUMBUS, Ohio, April 14 (UPI) -- Low blood sugar can lead to feelings of anxiety and irritability -- sweaty palms, a racing heartbeat, dizziness, extreme hunger, and other symptoms.

A drop in blood sugar can also make humans more likely to lash out at their spouses or significant others. That according to researchers at Ohio State.

Advertisement

"People can relate to this idea that when they get hungry, they get cranky," said Brad Bushman, lead author of the study. "We found that being hangry can affect our behavior in a bad way, even in our most intimate relationships."

To find out just how much of an effect hunger and low levels of blood glucose had on the likelihood of marital arguments, Bushman and his fellow researchers recruited 107 married to couples to participate in a study.

Over the course of 21 days, participants were asked to take out their anger on a doll representing their spouse. At the end of each day, participants were allowed to use anywhere from 0 to 51 pins to express the amount of anger they were feeling toward their life partners.

All along, participants' fluctuating blood sugar levels were measured. The researchers found lower blood sugar levels closely corresponded with higher numbers of pins stuck in the dolls.

At the end of the 21 day period, spouses came into the lab for a new experiment. Asked to face off against each other in a simple button-pressing game that tested reaction time, winners were allowed to blast their spouse with a loud noise via earphones. In reality, the spouses were not playing each other, but a computer program that allowed them to win half of the time. Each spouse, upon winning, got to choose how loud of a noise to subject their partner to.

Yet again, lower blood sugar levels predicted louder noises, a more intense punishment for the hypothetical loser.

But why? The short answer, our brain is needy. To function properly, humans must feed it with plenty of energy.

"Even though the brain is only 2 percent of our body weight, it consumes about 20 percent of our calories," Bushman explained. "It is a very demanding organ when it comes to energy,” he said"

The old wisdom for marital bless, "never go to bed angry," may need to be updated to include the rule: "never have an argument while hungry."

[Ohio State University]