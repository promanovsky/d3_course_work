Twitter has hired Anthony Noto, a former Goldman Sachs banker who was part of the team that took the social network public, as its new chief financial officer.

Noto replaces Mike Gupta, moved sideways to become "senior vice president of strategic investments", in the latest reshuffle at the social network as Dick Costolo, the company's CEO, tries to assuage fears of slow user growth.

Joining with an annual salary of $250,000 – as well as $80m in Twitter stock to vest over a four year period – Nato's previous role at Goldman Sachs was as a managing director in the technology, media and telecoms sector of the group. While in the role, he oversaw Twitter's initial public offering.

Welcome @anthonynoto and looking forward to @mgupta's new role leading strategic investments! — dick costolo (@dickc) July 1, 2014

Thanks @dickc Thrilled to start my new role as Head of Strategic Investments. Looking forward to having @anthonynoto on the team — Mike Gupta (@mgupta) July 1, 2014

In a previous reshuffle this summer, Twitter said goodbye to Ali Rowghani, the company's former chief operating officer. Rowghani, a former Pixar finance chief, announced his exit on Twitter, telling followers it had been an "amazing ride".

Goodbye Twitter. It's been an amazing ride, and I will cherish the memories. — Ali Rowghani (@ROWGHANI) June 12, 2014

@ROWGHANI Thank you for being an incredible executive & partner. Twitter could not have succeeded without you. — dick costolo (@dickc) June 12, 2014

Twitter did not give a reason for Rowghani's exit, and he was known to be well-liked inside the company, as well as one of Costolo's closest lieutenants.

The company's share price spent the first half of the year slumping from a high of $73 in December 2013 down to just $31 in May, as growth in the service tailed off and Costolo admitted it had a real problem in bringing new users on board. But in June, the shares recovered, rising 30% month on month.

• Twitter COO and 'Mr Fix-it' Ali Rowghani resigns suddenly