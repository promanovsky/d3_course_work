WASHINGTON -- Jon Sacker was near death, too sick for doctors to attempt the double lung transplant he so desperately needed. His only chance: An experimental machine that essentially works like dialysis for the lungs.

But the device has not been approved by the Food and Drug Administration and there were none in the country. It would take an overnight race into Canada to retrieve a Hemolung.

Sacker rapidly improved as the device cleansed his blood of carbon dioxide -- so much so that in mid-March, 20 days later, he got a transplant after all.

"That machine is a lifesaver," Sacker said from the University of Pittsburgh Medical Center.

Sacker's struggle highlights a critical void: There is no fully functioning artificial lung to buy time for someone awaiting a transplant, like patients who need a new heart can stay alive with an implanted heart pump or those with failing kidneys can turn to dialysis.

"It seems like it should be possible for the lung as well," said Dr. Andrea Harabin of the National Institutes of Health.

NIH-funded researchers are working to develop wearable "respiratory assist devices" that could do the lungs' two jobs -- supplying oxygen and getting rid of carbon dioxide -- without tethering patients to a bulky bedside machine.

It has proven challenging.

"The lung is an amazing organ for gas exchange. It's not so easy to develop a mechanical device that can essentially replace the function of a lung," said bioengineer William Federspiel of Pitt's McGowan Institute for Regenerative Medicine, who helped invent the bedside Hemolung and is working on these next-step devices.

So when Sacker needed an emergency fix, Dr. Christian Bermudez, UPMC's chief of cardiothoracic transplants, gambled on the unapproved Hemolung. "We had no other options," he said.

_____

Cystic fibrosis destroyed Sacker's own lungs. The Moore, Oklahoma, man received his first double lung transplant in 2012. He thrived until a severe infection last fall damaged his new lungs, spurring rejection. By February, he needed another transplant.

The odds were long. Donated lungs are in such short supply that only 1,923 transplants were performed last year, just 80 of them repeats, according to the United Network for Organ Sharing.

Still, the Pittsburgh hospital, known for tackling tough cases, agreed to try -- only to have Sacker, 33, arrive too debilitated for an operation. A ventilator was providing adequate oxygen. But carbon dioxide had built to toxic levels in his body.

When a ventilator isn't enough, today's recourse is a decades-old technology so difficult that only certain hospitals, including Pittsburgh, offer it. Called ECMO, it rests the lungs by draining blood from the body, oxygenating it and removing carbon dioxide, and then returning it. Sacker was too sick to try.

"I didn't see any other alternative other than withdrawing support from this young man," Bermudez said.

Then he remembered the Hemolung, invented by Pittsburgh engineering colleagues as an alternative to ECMO. It was designed to treat patients with a different lung disease, called COPD, during crises when their stiffened lungs retain too much carbon dioxide, Federspiel said.

The Hemolung recently was approved in Europe and Canada; its maker is planning the stricter U.S. testing required by FDA. For Sacker to become the first U.S. Hemolung patient, hospital safety officials would have to agree and notify FDA.

"We had actually just almost decided to turn the ventilator off, because we were putting him through suffering," Sacker's wife, Sallie, recalled. Then the phone rang: The experiment was on.

But Pittsburgh-based ALung Technologies Inc. couldn't get a device shipped for a few days. Doctors feared Sacker wouldn't live that long. Late at night, ALung CEO Peter DeComo tracked down a device in Toronto, and started driving. It took some explaining to get the unapproved medical device past U.S. border officials. But the next day, Sacker was hooked up, and quickly improved.

_____

Federspiel, also an ALung co-founder, said researchers' ultimate goal is a fully functioning, portable artificial lung.

Varieties under development consist of small bundles of hollow, permeable fibers. As blood pumps over the fibers, oxygen flows outside to the blood and carbon dioxide returns, explained Dr. Bartley Griffith of the University of Maryland. He has reported success in sheep, and hopes to begin the first human tests within three years.

The idea: Small tubes would connect the fiber device, worn around the waist, to blood vessels, so that patients could move around, keeping up their muscle strength instead of being restricted to bed.

There's "at the least the inkling that we can dream of sending somebody home with an artificial lung," Griffith said.

A bridge to transplant isn't the only need, said Harabin of NIH's National Heart, Lung and Blood Institute, which is funding work by Griffith, Federspiel and others.

Thousands each year suffer acute lung failure from trauma or disease that hits too suddenly to even consider transplant. Researchers like Griffith want to test if these experimental technologies could offer them a better chance to heal than ventilators, which can further damage lungs.

____

Back in Pittsburgh, Sacker is slowly gaining strength with his second set of transplanted lungs. He doesn't remember the fight for his life; he was sedated through it. But his wife has told him how touch and go it was.

"You get a call at the last second about a device that has never been used here in the United States -- that's a miracle," he said.