China's homegrown fast food chains have long played second fiddle to the likes of McDonald's (NYSE:MCD - News) and Yum Brands (NYSE:YUM - News) in the mainland but with the latest food scare, things may be in for a change.

Major U.S. brands found themselves in hot water last week after allegations surfaced that one of their main meat processors - Shanghai Husi Food, a unit of Illinois-based OSI Group - has been supplying out-of-date meat using unhygienic practices. An undercover investigation by Shanghai-based Dragon TV filmed workers picking up meat from a factory floor and mixing in meat beyond its expiration date.

Industry watchers say the latest quality embarrassment could puncture the domination enjoyed by foreign chains in China's $174 billion fast food market.

Behind China's food scare: a US-owned meat plant

"There is growing awareness that the fried and processed foods served by chains like KFC may not be so healthy [and] this is leading to a renewed interest in Chinese chains that may use preparation techniques like steaming for some dishes," Benjamin Cavender, Shanghai-based principal at China Market Research (CMR) told CNBC.

"[As the scandal continues,] I think brands like Real Kung Fu will do well," he added, referring to the Chinese fast food chain known for its steamed dishes.

"The latest scandal will certainly be negative for U.S. fast food chains, particularly at a time when most of them are trying to expand in China. This will drive consumers away to local competitors," said Xueluan Xia, a professor at Peking University.

Read More China's food safety takes another blow

According to various Chinese media like Guangzhou-based daily newspaper Yangcheng Evening News, domestic fast food stores like Real Kung Fu and New York-listed Country Style Cooking (NYSE:CCSC - News), have enjoyed a surge in customers at the onset of the food scare.

Meanwhile, more than two-thirds of Chinese netizens indicated that they no longer trust Western chains and nearly 70 percent said they won't be dining at the restaurants embroiled in the recent scandal, a poll on Sina.com (NASDAQ:SINA - News) last week showed.

Story continues

Aside from growing distrust of U.S. food brands, factors like the appeal of Chinese food and a refocus on ambience will give homegrown brands a leg-up in the competition ahead.

Read More Why fastfood items are staying extreme

"Consumers are thinking more about how to get value for money when they dine out [so they are] shifting their spending to these Chinese casual dining restaurants where they feel the food quality and ambience are better," said CMR's Cavender.

Still, there are analysts believe that McDonald's and Yum brands still have the means to engineer a rebound.

Brian Nelson, President of Equity Research at Valuentum Securities, told CNBC: "When you look at fast food brands in China, McDonald's and Yum Brands' are among the most recognized foreign brands and we expect consumers to come back."

"Take a look at Yum Brands' latest results for example; revenue for its China division came back in a big way after the influenza scare and previous poultry sourcing issues. While this will bring about a short-term impact, there is resilience in its consumer base," he added, referring to the firm's strong performance in the mainland where it posted a 15 percent surge in same store sales in the second quarter.

Read More McDonald's in crosshairs of Russia-US proxy war

To be sure, there will be diners who will be giving both U.S. and Chinese fast food chains the cold shoulder, at least for the time being.

Ziting Chen, a Singaporean copywriter living in Beijing told CNBC: "No, I'm not going to eat any fast food in China. Be it foreign or domestic [brands], they are equally worrying."





More From CNBC

