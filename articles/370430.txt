BOSTON, July 7 (UPI) -- MicroCHIPS, a Massachusetts-based startup, has developed a hormonal contraceptive chip that can be implanted under the skin and controlled by wireless remote.

The drug-delivery device, which is still in the development stages, would be able to last up to 16 years in a woman's body, delivering 30 micrograms a day of levonorgestrel to the upper arms, buttocks or abdomen.

Advertisement

While other birth control implants, like IUDs, need to be removed by medical professionals, the chip could be turned on and off with minimal effort.

According to the Massachusetts Technology Review, Bill Gates suggested the idea of a controlled-release birth control delivery device while visiting the MIT lab of MicroCHIPS co-founder Robert Langer.

The Bill and Melinda Gates Foundation is helping to support the project under its Family Planning program.

The chip will be ready for preclinical testing in 2015, with the aim of making it available to the public in 2018.