With regard to teens, there’s good news and bad news from the Center for Disease Control and Prevention (CDC).

According to the CDC’s National Youth Risk Behavior Survey (YRBS) released Thursday for the September 2012-December 2013 period, typical concerns like smoking, fighting and sexual activity are down, but technology-related health issues are up.

In its first survey of texting while driving, the CDC found 41 percent of students who have driven a car in the past 30 days have texted or emailed while driving, despite a nationwide campaign to raise awareness about its dangers. Just under 35 percent had consumed alcohol and drove, while 23 percent used marijuana and drove. In 2011, 3,331 people were killed in distracted driving accidents, which occur because of drivers talking on cell phones and texting while driving.

The percentage of students who sit at a computer or play video games for more than three hours for non-school-related activities doubled over the last 10 years, from 22 percent to 41 percent. Obesity rates haven’t risen quite as sharply, with a reported 3 percent increase from 1999 to 2013. Sixty-two percent of females and 33 percent of males said they were trying to lose weight. Overall, around 6 percent more respondents in 2013 were trying to lose weight than were in 1991.

Teens are smoking fewer cigarettes (15.7 percent), continuing a general downward trend since 1997, when more than one in three teens smoked, but other tobacco use is up, including hookahs and e-cigarettes.

Fewer teens are having sex than in 1991, but there has been a slight decrease in the percentage who use condoms. Ten percent of teenage girls reported they were forced to have sexual intercourse against their will and 10 percent of all teens reported being physically assaulted in the last 12 months by someone they were dating.

Just under 30 percent of teens reported long-term sadness or hopelessness that made them alter their activity. Nationwide, 17 percent had seriously considered suicide during the last 12 months before the survey. Twice as many girls as boys said they seriously considered suicide.

The 2013 report summarizes data for the YRBS national survey, 42 state surveys and 21 large urban school district surveys. It contains data for 104 health risks. Approximately 13,000 students participated. Read the full report here.