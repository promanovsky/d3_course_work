There was another sign Tuesday morning that the housing market is cooling off. Just a bit.

One of the nation’s most closely watched home price indexes inched downward for the third straight month in January, though prices climbed slightly after seasonal variations were taken into account.

The Case-Shiller Index saw prices tick down 0.1% across the 20 major cities it tracks, though they gained 0.8% when seasonally adjusted. Prices are up 13.2% compared with this time last year, but the quick pace of gains has clearly slowed.

MAP: How home prices are faring in your Zip Code

Advertisement

“The housing recovery may have taken a breather due to the cold weather,” said David Blitzer, chairman of the Index Committee at S&P Dow Jones, which publishes the index.

In Los Angeles, where weather has been less of an issue than in the snowy Midwest and Northeast, prices slipped 0.3%, though they were up on a seasonally adjusted basis and were 18.9% ahead of last year. In San Diego, prices jumped 0.6%, their best January since 2004.

From December to January, prices fell in 12 of the 20 cities Case-Shiller tracks. Still, Blitzer, like other real estate economists, remains optimistic that 2014 will be a solid if unspectacular year for the housing market.

“Expectations and recent data point to continued home price gains for 2014,” he said. “Although most analysts do not expect the same rapid increases we saw last year, the consensus is for moderating gains.”

Advertisement

tim.logan@latimes.com

Twitter: @bytimlogan