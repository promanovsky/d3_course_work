Think opposites attract? Think again: Experts discover groups of friends are so similar they share almost as much DNA as our family

Geneticists found unrelated friends have 1 per cent of genes which match

This is similar to fourth cousins who share great-great-great grandparents

Researchers suspect this trend is rooted in human evolution

Among genes studied, those affecting sense of smell were the most similar



Findings mean scientists are able to make a calculated guess as to which people are likely to become friends just by testing their DNA

Unrelated friends share 1 per cent of genes - similar to fourth cousins with the same great-great-great grandparents. Stock image pictured

You may have more in common with your friends than you think.

Research reveals that mates have a remarkable number of the same genes.

The study, which gives new weight to the saying ‘birds of a feather flock together’, found that pals share significantly more DNA than strangers.

Geneticists discovered that unrelated friends have a 1 per cent of genes that match – about the same as fourth cousins, who share great-great-great grandparents.

‘One per cent may not sound like much to the layperson, but to geneticists it is a significant number,’ said Professor Nicholas Christakis, of the Human Nature Lab at Yale University.

It means that scientists are even able to make a calculated guess as to which people are likely to become friends just by testing their DNA.

Such a test is more accurate than genetic tools used to predict the risk of obesity or schizophrenia.

Co-author Professor James Fowler, a medical geneticist at the University of California at San Diego, said: ‘Looking across the whole genome, we find that, on average, we are genetically similar to our friends.

‘We have more DNA in common with the people we pick as friends than we do with strangers in the same population.’

It is thought the explanation is rooted in human evolution.



Early humans may have formed groups that were genetically suited to the same environments, or had similar likes and dislikes.

Those who had a similar susceptibility to the cold, for example, may have helped each other build a fire.

The trait, termed ‘functional kinship’, may have also been driven because some genetic attributes only work if you have someone to share it with.

Researchers suspect the tendency to choose genetically similar friends is rooted in evolution. Illustration of DNA pictured. Early humans may have formed groups genetically suited to certain environments. The trait, known as 'functional kinship', may have also been driven because some attributes only work when shared

The first humans to develop speech would have grouped together with others who had the same skill, they said.

The scientists analysed the genetic make-up of 2,000 people for the study, published in the journal Proceedings of the National Academy of Sciences.

They wrote: ‘Humans may – when choosing friends from among individuals who are strictly not related to them – come to choose individuals who resemble them on a genotypic level.’

Genes affecting the sense of smell were among the most similar in friends, possibly because it draws us to similar environments.



People who enjoy the smell of coffee, for instance, may be more likely to frequent cafes.

The opposite was true for genes controlling immunity. Friends were more likely than strangers to have different genetic defences against various diseases.

This may be because it helps to reduce the likelihood of epidemics if friends are resistant to different illnesses.