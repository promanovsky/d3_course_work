The United States job report is in, and across the board it appears to be overwhelmingly positive, pointing to an economy that is growing at a brisk pace. Regardless, just as there are often silver lining to bad news, there can be “dark lining” to positive news. And in this case, a deeper look at the U.S. jobs report does suggest that there are some things to be concerned about.

First, the Good News

The jobless rate is now at a six year low, dropping all the way down to 6.1 percent. This marks the lowest point since before the great recession and a dramatic decline for the 10 plus percent seen during the recession.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

Nonfarm payrolls added 288,000 jobs while data for April and May were revised to show an additional 29,000 jobs added. In totally, the economy has now added 200,000 plus jobs each month for five straight months, the best such record since the tech boom in the late 1990’s.

Analysts are also projecting the economy to grow at a brisk 3.5 percent for the last half of the year. This follows a 2.9 percent decline for the first quarter, a sharp revision from earlier estimates that suggested a slight expansion.

Labor Force Participation at Its Lowest Point Since 1978

Perhaps the most worrisome number pulled from the report is the fact that participation in the labor force is at a historical low not matched since the late 1970’s. Only 62.8 percent of working age Americans are participating in the work force. Before the recession, the participation rate regularly hovered about 66 percent.

Most likely, the large number of people choosing not to participate in the labor force are doing so because the economy remains weak and hiring is not as strong as the jobs report suggests. Other data backs this claim up though certainly, the economy does appear to be growing and the long-awaited recovery appears to be under way.

U-6 Unemployment Rate Remains High

The U-6 unemployment rate remains at 12.1 percent. Before the recession, the rate weighed in at 8 percent and in the midst of the recession spiked to about 17 percent. This suggests that the U-6 is recovering more slowly than the U-3.

Why is this important? When considering the overall health of the economy, the U-6 is probably a more important measure than the U-3. It suggests that there are still a lot of people out there who want jobs but have given up looking. Not only that, but many people are accepting part-time employment while looking for full-time work.

That recent college graduate who ends up working part time as a barista while looking for a career isn’t counted as unemployed in the U-3 rate. When you step back and think of the wider consequences for the economy, however, the loss in earnings and shift towards lower wage/skilled work is important to note.

Wages Remain Stagnant

Another important trend to note is that wages are still stagnant. Wages climbed a whopping 6 cents, which would likely fall behind the rising costs of inflation and a slowly recovering housing market. This could eventually crimp consumer spending as potential customers will start closing their wallets.

Wages over the last 12 months have grown by only 2 percent and in the last month grew only .2 percent. In real terms, wages rose by only 6 cents. So while hiring does appear to be picking up, employers aren’t seeing much need to raise wages to attract and retain talent. This suggests that the supply of people looking for work outweighs the demand for labor.