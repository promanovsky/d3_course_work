Gamers looking forward to the launch of the new Bungie Destiny multiplayer online game later this year are sure to enjoy this new preview of the Destiny Titan class, which has been unveiled at E3 this week.

The Destiny Titan player class specialises in the use of in armour and heavy weaponry and were the very first Titans to built and defended The Wall that protects The City within the game with their heritage rooted in strength, sacrifice, and mercilessness. Check out the trailer after the jump to learn more.

“Titans can be distinguished by their heavy, full-body armorand swept-back helmets. Also, as with the other player classes, in addition to head, chest, arm, and leg armor, Titans have a special fifth armor slot that is unique to their class and further helps to distinguish them.

The Titan’s unique armor is a badge, a piece of cloth worn on the right hip intended to show the Titan’s allegiance, lineage, or achievements.”

The new Destiny game being developed by Bungie the development team responsible for the first Halo games will be officially launching on PlayStation 3, PlayStation 4, Xbox One and the Xbox 360 on September 9th 2014 with an open public beta starting next month.

Source: Polygon

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more