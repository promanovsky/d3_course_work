Tesla Motors insists that its Model S electric sedans are perfectly safedespite three car fires late last year. But just to be sure, the manufacturer is outfitting all new vehicles with a titanium triple underbody shield.

All automobile bodies assembled since March 6 now come with the new titanium safeguard; Tesla will also retrofit existing Model S cars for free.

This addition will presumably keep vehicles safe from fiery harm, like that which came to a Seattle man whose car burst into flames as a section of its 85-watt battery pack was apparently punctured by a metal object he ran over. Or Dr. Juris Shibayama, who, despite striking a trailer hitch in the middle of a Tennessee highway and exiting his car just before smoke began pouring from the front underbody, said he'd "buy another one in a heartbeat."

There have been no more reported fires since the fall's "extremely unusual" crashes. Still, "we felt it was important to bring this risk down to virtually zero to give Model S owners complete peace of mind," Tesla CEO Elon Musk wrote in a blog post.

For visual context, the company provided slow-motion GIFs of the new shields deflecting debris (click right).

The first, a rounded, hollow aluminum bar, is designed to deflect objects or absorb their impact. Then the titanium plate is installed to prevent sensitive front underbody components from being damaged. For the rare piece of debris that makes it past the first two obstacles, a third shield, placed at a shallow angle, causes the Model S to "ramp up and over" an immovable object, Musk said.

Tested more than 150 times, the three new titanium shields consistently prevented damage that may cause a fire or crack the battery pack's existing ballistic-grade aluminum armor plating. It even stood up against "every worst case debris impact we can think of," Musk said, including essentially driving a car at highway speed into a steel spear in the road.

The changes are also meant to prevent damage like that done in a recent incident in Mexico, from which the driver miraculously walked away unscathed, after trying to maneuver a roundabout at 110 mph, and tearing off both front wheels while crashing through a concrete wall and into a tree.

"As the empirical evidence suggests, the underbody shields are not needed for a high level of safety," Musk boasted. "However, there is significant value to minimizing owner inconvenience in the event of an impact and addressing any lingering public misperception about electric vehicle safety."

Tesla still carries a clean record, with zero deaths or serious, permanent injuries sustained since its vehicles went into production six years ago.

"There is no safer car on the road than a Tesla," Musk said. "The addition of the underbody shields simply takes it a step further."

Consumer Reports agreed in its recent Top Picks list, which named the Model S the best overall vehicle.

For more, see 5 Ways Tesla Is Transforming the Auto Industry, as well as 10 Thrilling Minutes in Tesla's Model S and the slideshow above.

Further Reading

Cars & Auto Reviews