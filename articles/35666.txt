The S&P 500 set a fresh intraday record, as U.S. stocks are on track to record a second straight week of solid gains. See full story.

Million-dollar Birmingham property could house N.Y., S.F., Miami and Honolulu counterparts

A million-dollar house in Birmingham, Ala, would typically be big enough to house million-dollar properties from New York City, San Francisco, Miami and Honolulu — at the same time, and with space to spare. See full story.

My next boss will be normal

The drama in PIMCO’s executive suites highlights a Wall Street truth: It’s tough to be on top, tougher still to be near the top. See full story.