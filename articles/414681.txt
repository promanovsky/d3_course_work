A dry wash full of 112-million-year-old dinosaur tracks that include an ankylosaurus, dromaeosaurus and a menacing ancestor of the Tyrannosaurus rex, is set to open to the public this fall near Moab.

There are more than 200 tracks in an area smaller than a football field from 10 different ancient animals that lived during the early Cretaceous period, said Utah Bureau of Land Management paleontologist ReBecca Hunt-Foster.

They were first discovered in 2009 by a resident. Since then, paleontologists led by a team at the University of Colorado at Denver have studied them and prepared them for display.

The tracks include a set of 17 consecutive footprints left by Tyrannosaurus rex ancestor and the imprint of an ancient crocodile pushing off into the water. The site is one of the largest areas of dinosaur tracks from the early Cretaceous period known to exist in North America, she said.

"We don't usually get this," said Hunt-Foster, a paleontologist for 16 years. "It is a beautiful track site, one of the best ones I've ever seen."

There are footprints from duckbilled dinosaurs, prehistoric birds, long-necked plant eaters and a dromaeosaur similar to a velociraptor or Utah raptor that had long, sharp claws.

In one rock formation, a footprint left behind by a large plant eater is right in the middle of prints from a meat-eating theropod, Hunt-Foster said.

The imprint of an ancient crocodile shows the chest, body, tail and one foot. Paleontologists believe it was made while the crocodile was pushing off a muddy bank into water.

Paleontologists believe the tracks were made over several days in what was a shallow lake. They likely became covered by sediment that filled them up quickly enough to preserve them but gently enough not to scour them out, Hunt-Foster said.

Over time, as more sediment built up, they became rock. They are near a fault line, where the land has moved up and down over the years, she said. Rains slowly eroded away layers of the rock, exposing the footprints.

When it opens in October, the site will have a trail leading people to the tracks with signs explaining what they are looking at. Officials are trying to raise funds to provide shade and a 1- to 2-foot high boardwalk so people can look at the tracks without being tempted to touch.

Earlier this year, a Utah man was arrested and slapped with the federal charge after authorities say he pried a piece of sandstone with a three-toed ancient dinosaur track from the Sand Flats Recreation Area near Moab. He pleaded guilty while accepting a deal that calls for him to serve one year of probation with six months under house arrest.

Paleontologist Martin Lockley of the University of Colorado at Denver has taken the lead in studying, cleaning and preparing the tracks. Their uniqueness has lured paleontologists from several countries, including from Poland, Korea and China, Hunt-Foster said.

"It's such an important site that they are coming here to study it," she said.