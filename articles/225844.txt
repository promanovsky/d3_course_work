15 million units, and it’s still only mid-May. That number represents the amount of cars it makes in a poor year for the company. The company, and its new CEO, have been raked over the coals (with good reason) by investors, analysts, comedians, and Congress among others for some time now and the problems just continue. Last year, the company recalled almost 758,000 in the United States.

The charges keep mounting

With the announcement of 2.42 million recalls over four new models today, General Motors Company (NYSE:GM) will see around $400 million in charges in the second quarter alone. Year to date, following the close of the second quarter, the company will have taken somewhere in the neighborhood of $1.7 billion in charges based on recalls.

While the four recalls announced today are all safety related, it doesn’t appear as though any fatalities were caused by these safety flaws. However, given General Motors Company (NYSE:GM) behavior and false reporting it’s not out of the realm of possibility that could change.

An Hour With Ben Graham This interview took place on March 6 1976. At the time, a struggling insurer, Government Employees Insurance Company (GEICO) was making headlines as it teetered on the brink of bankruptcy. Ben Graham understood the opportunity GEICO offered, and that’s where the interview began. Ben Graham and his partners had, at one time, been significant shareholders Read More

General Motors models and reasons for recall

According to the company’s press release, the four recalls announced Tuesday include:

1,339,355 Buick Enclave, Chevrolet Traverse, GMC Acadia full-size crossovers from the 2009-2014 model years and Saturn Outlooks from 2009-2010 because front safety lap belt cables can fatigue and separate over time. In a crash, a separated cable could increase the risk of injury to front seat passengers.

1,075,102 of the previous generation 4-speed automatic transmission Chevrolet Malibu and from the 2004-2008 model years and Pontiac G6 from the 2005-2008 model years because of a shift cable that could wear out over time, resulting in mismatches of the gear position indicated by the shift lever.

1,402 Cadillac Escalades and Escalade ESVs from the 2015 model year because an insufficiently heated plastic weld that attaches the passenger side air bag to the instrument panel assembly could result in a partial deployment of the air bag in the event of a crash.

58 Chevrolet Silverado HD and GMC Sierra HD full-size pickups from the 2015 model year because retention clips attaching the generator fuse block to the vehicle body can become loose and lead to a potential fire.

General Motors Company (NYSE:GM) is presently (1:20PM EDT) trading at $33.32 down $0.95 per share on the day or 2.73%. General Motors Company (NYSE:GM) was trading at $40.95 when trading opened following New Year’s Day this year on January 2nd.