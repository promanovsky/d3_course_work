You don't need to have extremely high blood pressure to have an increased risk of stroke -- any number above a "normal" reading could raise risk, according to a new review of studies.

Specifically, having prehypertension -- meaning a blood pressure higher than the "normal" reading of 120/80 millimeters of mercury (mmHg), but not high enough to indicate hypertension -- is associated with a 66 percent higher risk of stroke, compared with having normal blood pressure.

The findings held true even after taking into account other known stroke risk factors, such as smoking, high cholesterol and diabetes.

Blood pressure readings include systolic and diastolic blood pressure; systolic blood pressure refers to the blood pressure when the heart is beating, and diastolic blood pressure refers to the blood pressure when the heart is resting. A normal blood pressure level will have a systolic blood pressure less than 120 and a diastolic blood pressure less than 80. A prehypertensive blood pressure level will have a systolic blood pressure between 120 and 139 or a diastolic blood pressure level between 80 and 89. High blood pressure is indicated by a systolic blood pressure of 140 or higher or a diastolic blood pressure of 90 or higher, according to the National Heart, Lung, and Blood Institute.

The new meta-analysis, published in the journal Neurology, included 19 prospective cohort studies, which involved more than 760,000 people. Study participants were followed anywhere from four to 36 years.

Researchers also looked at the risk of stroke among those considered prehypertensive. They found that those who were in the "high" group -- with blood pressure levels greater than 130/85 mmHg -- had a 95 percent higher risk of stroke compared with people with normal blood pressure levels. Meanwhile, those who were on the "low" end of prehypertension had 44 percent higher risk of stroke compared with people with normal blood pressure.