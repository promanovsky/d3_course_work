How much are those Twitter followers really worth?

A 14-year-old girl from the Netherlands was arrested on Monday after tweeting a terror threat to American Airlines — and marveling at the Twitter followers that she attracted because of it.

See also: Man Jailed After Threatening President Obama on Twitter

"Hello my name's Ibrahim and I'm from Afghanistan. I'm part of Al Qaida and on June 1st I'm gonna do something really big bye," she tweeted to @AmericanAir on Sunday, using a Twitter account mostly dedicated to the American pop star Demi Lovato. The airline responded that it "takes these threats seriously" and would report her tweet and IP address — not information the airline would actually have access to — to the FBI.

Image: Twitter

The girl then fired off a series of tweets in her defense: "omfg I was kidding." "I'm just stupid okay." and "Omg I gained 3k followers today." However, it appears airline wasn't impressed.

Rotterdam police confirmed on Monday that the girl had been arrested.

A spokesperson for the Dutch police told Business Insider, "We’re not in a state that we can communicate any state of charges at this point, we just thought it was necessary to bring this out mostly because of the fact that it caused a great deal of interest on the Internet."

American Airlines, which deleted the tweet, released the following statement:

At American, the safety of our passengers and crew is our number one priority. We take security matters very seriously and work with authorities on a case-by-case basis.

The teen, whose name is "Sarah," later tweeted that she needed some legal representation. "I need a lawyer. Any lawyers on here?" she asked on Sunday, hours before the arrest.

See all her tweets — which have since been deleted — thanks to a Storify by Rachael Perrotta: