The number of Americans filing new applications for unemployment benefits tumbled last week to the lowest level in nearly seven years, strengthening views of faster job growth.



Thursday's report was the latest sign of momentum in the economy after activity was hobbled by an unusually cold winter.



"The return of warmer temperatures has brought with it better data. There are a number of signs that progress in the jobs market could be accelerating, a positive sign for the broad economy as well," said Jim Baird, chief investment officer at Plante Moran Financial Advisors in Kalamazoo, Michigan.



Initial claims for state unemployment benefits dropped 32,000 to a seasonally adjusted 300,000 for the week ended April 5, the Labor Department said. That was the lowest level since May 2007, before the start of the 2007-09 recession.



The report joined other indicators such as automobile sales and employment data in suggesting the economy ended the first-quarter on a stronger footing, positioning it for faster growth in the April-June quarter.



First-quarter growth is expected to have braked sharply from the fourth quarter's annual 2.6 percent pace, largely because of the harsh weather and businesses placing fewer orders with manufacturers while working off massive stockpiles accumulated in the second half of 2013.



Growth was also seen crimped by the expiration of long-term unemployment benefits, cuts to food stamps and weak exports. First-quarter gross domestic product estimates range as low as 0.6 percent.



U.S. financial markets were little moved by the claims data.



Economists had forecast first-time applications for jobless benefits falling to 320,000 for the week ended April 5.



The number of people still receiving unemployment benefits after an initial week of aid for the week ended March 29 hit its lowest level since January 2008.



"The rate of involuntary job losses slowed in early April, which suggests we could have a further pickup in job growth in April," said John Ryding, chief economist at RDQ Economics in New York. "We think that the unemployment rate could fall faster than the Federal Reserve expects over the next year."



Job growth averaged about 195,000 per month in February and March, with the unemployment rate holding at near a five-year low of 6.7 percent over that period.



There have also been improvements in other labor market measures such as the short-term unemployment rate and job openings, which economists say suggest labor market slack is easing.



NO INFLATION PRESSURES



Still the recent job market upturn is unlikely to see the Fed in a hurry to start raising interest rates when it wraps up its monthly bond buying program later this year.



The U.S. central bank slashed overnight interest rates to a record low of zero to 0.25 percent in December 2008 and pledged to keep them low while nursing the economy back to health.



Minutes of its March 18-19 policy meeting published on Wednesday suggested officials were not eager to start tightening monetary policy.



A second report from the Labor Department showed import prices increased 0.6 percent last month after rising 0.9 percent in February.



The increase exceeded economists' expectations of a 0.2 percent gain and was driven by food prices, which recorded their largest advance in three years. Away from food, there was little sign of a broader pickup in imported inflation.



In the 12 months through March, import prices fell 0.6 percent, pointing to continued weak imported inflation that is helping to keep a lid on domestic price pressures.



"The U.S. can't import any inflation," said Chris Rupkey chief financial economist at Bank of Tokyo-Mitsubishi UFJ in New York. "If this is on the Fed's inflation dashboard, there are no inflationary pressures to speak of coming in from overseas."



Last month, imported food prices jumped 3.7 percent, the biggest rise since March 2011, after falling 0.7 percent in February. Imported fuel prices rose 1.2 percent last month, slowing from a 5.3 percent surge in February.



Stripping out food and fuels, import prices rose 0.2 percent after slipping 0.1 percent in February.



The Labor Department report also showed export prices increased 0.8 percent in March, the largest rise since September 2012. That followed a 0.7 percent advance in February. In the 12 months through March, export prices gained 0.2 percent.



© 2021 Thomson/Reuters. All rights reserved.