When The Hollywood Reporter spoke to experts before the Supreme Court's Aereo argument two months ago, they predicted a tough fight for Aereo, but also that the case was so complex legally that the result could be as lopsided as 8-1 in either direction.

By those metrics, the court's decision yesterday did not disappoint. Among the most surprising words of the 6-3 smackdown that crushed Aereo were the ones that began the opinion: "Justice Breyer delivered the opinion of the Court." Breyer is known as a technologist and has stood in the past against what he saw as overextension of copyright law. No one anticipated he'd be writing the startup's death warrant.

But so he did. The legal point at issue in the case was whether Aereo was engaged in "publicly" "performing" other owners' copyrighted works – that is, whether Aereo was publicly transmitting such works. The court answered the question by dividing it in two, first asking if Aereo was performing or transmitting at all.

PHOTOS Hollywood's 100 Favorite Films

That question is harder than it looks, because the usual inquiry in this area of copyright is not whether there is performing or transmitting going on, but who is to be considered the doer. Aereo said the user was the volitional agent here, because its system – unlike a cable TV system, for instance – does nothing for the user until the user affirmatively selects a channel and clicks the Watch button.

It's not just lawyers fussing over nothing. The distinction between company as volitional and user as volitional is also the distinction between direct and secondary liability – or no liability – on the company's shoulders for potential copyright infringement. It's a distinction that made a big difference in the Supreme Court's 1984 Betamax decision, which upheld the legality of home VCRs.

Because Aereo's system is inert until activated by the user, Aereo said the broadcasters were barking up the wrong tree when the sought to hold it liable for direct infringement. The question of secondary liability wasn't at issue, because the case hadn't proceeded that far: the Supreme Court was ruling on denial of a preliminary injunction based on direct liability.

PHOTOS 35 of 2014's Most Anticipated Movies

The court disagreed with Aereo, but it didn't analyze the volitional issue at all. Instead, it said that something that looked so much like a cable system is deemed to be doing the performing. That's because, said the court, when Congress enacted the 1976 Copyright Act – the one we still labor under almost 40 years later – it intended that cable systems be held within the Copyright Act's scope. If that seems to be backing into the logical analysis, so be it.

The court acknowledged that Aereo also had technical features that made it look like a remote storage DVR, but said that didn't matter, because it was under the hood. "Why would a subscriber who wishes to watch a television show care much [about implementation details]?" asked the court, ignoring the fact that copyright decisions in the last couple of decades have often hinged on just such details – and begging the question of why the user's lack of technological curiosity was relevant at all to the copyright analysis.

The court then did another strange thing: it analyzed whether the performing was public. As the dissent by Justice Scalia points out, this second step was unnecessary: after all, every cable system performs to the public. And yet the court dutifully proceeded with the analysis. The purpose, charged Scalia, was simply to add a gloss of rigor to a results-driven decision.

PHOTOS Summer TV Preview

What about other technologies, such as cloud computing? The court had this to say: "Congress, while intending the Transmit Clause to apply broadly to cable companies and their equivalents, did not intend to discourage or to control the emergence or use of different kinds of technologies."

That's a bit odd, since Aereo is certainly a "different kind of technology" from a cable system.

And the court also said "questions involving cloud computing, remote storage DVRs, and other novel issues not before the Court, as to which 'Congress has not plainly marked the course,' should await a case in which they are squarely presented."

That will give technology companies night sweats – and it leads to the first loser in our scorecard:

* Innovators lose because the Aereo decision makes it harder for them to know where the lines are drawn. The court said Aereo – which allowed users to use RS-DVR technology to transmit programs, from a small antenna to a hard drive and thence via packet on the Internet to mobile devices and PCs – was "substantially similar" to a cable system that uses a single big antenna to transmit programs via cables buried in the streets to television sets. The fact that Aereo also resembled an RS-DVR was discarded. With that much elasticity, how does a technologist know whether her brilliant idea too closely resembles a phonograph or player piano roll and therefore runs afoul of some vastly pre-Internet analysis?

* Multichannel video programming distributors – cable companies, satellite providers and telcos – win because the court decision eliminated the threat posed by the $8-$12 per month. Aereo, which was building a cable competitor from the ground up. Aereo already offered Bloomberg News in addition to broadcast stations, and if it added programming, subscribers could readily have built a meal out of Aereo plus Netflix, Hulu and other services, bypassing ever-increasing MVPD subscription fees. Had Aereo survived and thrived, its presence might ultimately have spurred unbundling.

* Dish may lose because the erosion of the volition requirement in copyright liability may render Dish's Hopper product more vulnerable to attack – and the brushing aside of implementation details may make arguments in the case more uncertain all around. Dish has beaten back a preliminary injunction demand by Foxx, most recently in the 9th Circuit in January, but the case continues in the district court.

* Consumers lose for the same reason that MVPDs win. High priced cable bills are here to stay, and unbundling remains a distant dream for consumer advocates.

* Networks and broadcasters win because they beat Aereo and can continue to press MVPDs for carriage fees. Meanwhile, the TV Everywhere initiative survives, which brings live TV to mobile and other Internet devices, but only for users who have a paid MVPD subscription. ABC already has a mobile app – but to access those free over the air signals on their mobile, consumers have be subscribers to a participating MVPD.

* Small broadcasters lose,because for them capturing additional audience from cordcutters and millennial "cord nevers" may have been more valuable in terms of ad revenue than the loss of carriage fees.

* FilmOn aka Aereokiller loses because it operates a system highly similar to Aereo's. The broadcasters' 9th Circuit case against FilmOn has been pending a decision for months, presumably awaiting the Supreme Court's Aereo ruling.

* Content companies win because a portion of the fatter profits for broadcasters flow back to them.

* Content creators – producers, writers, directors, actors and crew – win as well because more money in companies' pockets means less economic pressure on the production business.

Email: jh@jhandel.com

Twitter: @jhandel