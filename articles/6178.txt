The Titanfall release date struck early for some lucky gamers who received a legitimate copy of the game a few days early. Unlike Microsoft, however, Respawn isn’t “banning” these early gamers for playing it before the game is officially released.

When the Xbox One was shipped early to some lucky Target customers back in November, Microsoft banned them from Xbox Live until the day the console was supposed to be released. As previously reported by The Inquisitr, Microsoft even took down YouTube videos from people who posted footage of the working console ahead of time. It appears that Respawn doesn’t want to repeat that upset and anger its gamers.

If anything, those lucky recipients of the early Titanfall release date may experience occasional server issues as Respawn works out some of the anticipated problems so it works better when the game officially launches. They won’t stop you from trying to play, though. Just be aware that the servers may be down for pre-emptive maintenance and you might not be able to play as a result.

About playing early: We won’t stop or ban legit copies. It is prelaunch, so there may be interruptions in service as we prep servers. — Vince Zampella (@VinceZampella) March 7, 2014

Unfortunately PlayStation owners won’t be part of the festivities, and it’s not because of rumors of Microsoft buying out Respawn’s game like they did with Gears of War. If they had bought it out, we could be sure that those who experienced an early Titanfall release date would definitely be banned until the game launched.

Titanfall PS4 may not be happening, but that doesn’t mean that Sony’s console won’t see Titanfall 2. Hopefully by then, Respawn will have released single player DLC allowing gamers to play without needing to be online. PlayStation 4 and single player Titanfall gameplay aren’t likely any time soon though.

We’re aware fans have secured game copies early. Keep in mind, Titanfall has not officially launched & servers may still be offline. — Titanfall (@Titanfallgame) March 9, 2014

We can expect Titanfall server problems similar to those of Grand Theft Auto 5 as they finish smoothing out the gameplay and get everything working properly. If you got your copy of the game ahead of the official Titanfall release date just a couple of days from now, feel free to play it. You have Respawn’s blessing.

Just don’t expect a smooth Titanfall gameplay experience until the game is officially public.

[second image via memegenerator.net]