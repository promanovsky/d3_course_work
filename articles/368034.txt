The company is scheduled to launch the Xperia Z2a in Taiwan tomorrow and as we fait for the final bit of details we now hear chatter about Sony having something else up its sleeve. Apparently Sony is going to announce a new smartphone tomorrow. The company itself has offered no confirmation so the only way we can be sure is to wait until tomorrow to find out what exactly this smartphone has to offer.

Advertising

This new Xperia handset will be launched globally, so says ePrice, which first posted the rumor about a new device coming out tomorrow. As expected not much is known about it at this point in time so we can’t judge by its specifications or its features if it is going to be a major competitor in the Android smartphone market.

Though as per one rumor it is believed that one of the biggest selling points of this new Xperia smartphone will be the ability to take great selfies. Rumor has it that this device will have a flash upfront which will, a helpful addition for those who like to take selfies at night.

Sony’s official Twitter account has also teased that tomorrow’s event contains an announcement that will allow them to “see you selfies in a whole new light.” This is a pretty big hint about what we can expect from Sony tomorrow and we’ll know for usre in less than a day.

Filed in . Read more about Sony and Xperia.