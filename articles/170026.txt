A chance online meeting between two players of a smartphone-based drawing game has inspired one of them to lose nearly 400 of his more than 600 pounds.

Brian Flemming of Michigan was morbidly obese, weighing around 625 pounds and drinking heavily when he was matched with Jackie Eastham, who lives in England, on the Pictionary-like smartphone game Draw Something.

Informal chats during game sessions led to a friendship and the two starting to share more about their lives on Facebook.

In addition to sharing his problems with obesity, Fleming admitted his alcoholism to Eastham, something he had kept from everyone else.

If he expected some sympathy, that's not what he got from Eastham, he says.

"She said there were people struggling for their lives and then there's you -- you have all these opportunities and you're throwing it all away," said Flemming, 32.

Eastham is no stranger herself to health issues, struggling with a form of muscular dystrophy. The symptoms she's experienced have been mild to this point, but she says she refuses to take her health for granted.

She had that in mind when she began getting Flemming's self-pitying, sometimes drunken messages, she said.

"I just thought bloody hell, you're a guy who's 30 ... and you're wasting your life," she told CNN. "My future is a lot gloomier. I'm trying to make the brightest future I can, and ... you're dragging yourself down."

That was a wakeup call, Flemming says, and on that day in October 2012 he made a vow to stop drinking.

That was just a first step in a dramatic transformation; since then Flemming has dropped 380 pounds, reducing his waist size from 60 to a 38 on his 6-foot 2-inch frame.

The change was rough at first, he says, with withdrawal from alcohol bringing shaky hands, cold sweats and sleeplessness.

Just quitting his drinking -- almost a fifth of vodka a day -- let him drop 100 pounds during the first month, inspiring him to abandon his daily fast-food diet of 7,000 calories for a regimen of cereal and Greek yogurt for breakfast and lean-protein lunches and dinners.

He soon added exercise to his daily efforts.

Flemming, now weighing 234 pounds and Eastham finally met in person in December 2013 in Paris, where the two climbed the Eiffel Tower.

"Jackie is the best thing that's ever happened to me," Flemming wrote in a blog post. "I feel that she saved my life, even though she would never take credit."

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.