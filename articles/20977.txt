Britain's Chancellor George Osborne revealed that the growth forecasts for the U.K.'s economy had been upped for this year and next, as he presented his budget to parliament on Wednesday.

The independent Office for Budget Responsibility (OBR) said the economy looked set to grow by 2.7 percent in 2014 - an increase from its December forecast of 2.4 percent - and by 2.3 percent in 2015, up from 2.2 percent. For 2016, the growth forecast remained at 2.6 percent.

But despite these upgrades, Osborne said he was sticking to an austerity agenda.

(Live blog: UK budget: Latest news, reaction and analysis)



"I have never shied away from telling the British people about the difficult decisions we face - and just because things are getting better, I don't intend to do so today," the chancellor said in his annual speech.

Osborne added that the OBR had revised down the U.K.'s deficit for every year of its forecast. In 2013/2014, the country is now expected to have a budget deficit of 6.6 percent of gross domestic product (GDP), down from 6.8 percent, with that percentage decreasing until it hits 2.4 percent in in 2016/2017 - down from 2.7 percent.

The budget came after data revealed that the number of people in employment in the U.K. hit the highest level since records began in the three months to January, although the jobless rate held steady at 7.2 percent.

The number of people claiming unemployment benefits fell more than expected in February to a total of 1.17 million, according to the Office for National Statistics. This marked a fall of 34,600 from January - more than the 25,000 decrease forecast by economists polled by Reuters.