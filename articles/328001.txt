AP Images

The Supreme Court issued a "sweeping and definitive" ruling against streaming service Aereo Tuesday morning.

But Justice Antonin Scalia, conservative anchor of the court, didn't join the opinion.

Six other Supreme Court justices found that Aereo violates copyrights owned by television networks whose programs the company's device streamed. While Scalia feels Aereo's actions likely do violate networks' rights, he wrote that the court's decision to label them as "public performance" distorted the Copyright Act.

"That claim fails at the very outset because Aereo does not perform at all," Scalia writes.

Not only does Aereo not perform, in Scalia's mind, if it did, it still wouldn't directly violate copyright, which the networks claim.

"This suit, or rather the portion before us here, is fundamentally different," Scalia writes.

Two types of liability exist for copyright infringement: direct and secondary. Most suits against equipment manufacturers, like Aereo, constitute secondary liability. In a legal sense, that means holding the defendants responsible for intentionally inducing or encouraging infringing acts — considered a "volitional act" — without actually doing so themselves.

Video-on-demand services, like Netflix, choose their content, which Scalia considers a volitional act. But at a "copy shop," as Scalia calls Aereo, customers choose what they wish to re-create.



"So which is Aereo: the copy shop or the video-on-demand service? In truth, it is neither," Scalia writes. "Rather, it is akin to a copy shop that provides patrons with a library card."

Aereo doesn't provide a pre-arranged assortment of television shows. The antennae act as "library cards," according to Scalia, allowing users to pick up any broadcast freely available. Some are copyrighted. Some aren't.

"In sum, Aereo does not 'perform' for the sole and simple reason that it does not make the choice of content," Scalia writes. "And because Aereo does not perform, it cannot be held directly liable for infringing the Networks' public-performance right."

But that doesn't mean Scalia lets Aereo off the hook. He doesn't.

"I share the Court's feeling that what Aereo is doing (or enabling to be done) to the Networks' copyrighted programming ought not to be allowed," he writes. "But perhaps we need not distort the Copyright Act to forbid it."