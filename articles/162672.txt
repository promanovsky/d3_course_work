Massachusetts General Hospital welcomed its second-biggest baby ever delivered at the facility Tuesday.

Carisa Ruscak was born 14 pounds, 8 ounces to parents Caroline and Bryan Ruscak of Burlington.

“I heard the weight and I was like, ‘Oh my God.’ It validated me because I was in a lot of pain when I was pregnant, so to hear the size, it made sense,’’ Caroline told WCVB-TV.

WHDH-TV reports that the couple’s first child, Claudia, weighed 10.5 pounds.

“Fortunately things went well though. She delivered by C-section. Pregnancy otherwise uncomplicated. She didn’t have diabetes or any other things that we associate with big babies. Fortunately mom and baby are doing well,’’ Dr. Jennifer Kickham told the station.