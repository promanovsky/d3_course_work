Bobby Womack, left, and Ronnie Wood backstage after Bobby was inducted into the Rock and Roll Hall of Fame in 2009 (AP)

LONDON, ENGLAND - OCTOBER 22: Bobby Womack attends the Q Awards at Grosvenor House, on October 22, 2012 in London, England. (Photo by Ferdaus Shamim/WireImage)

Bobby Womack, the influential soul singer who came to prominence in the sixties, died on June 27 aged 70.

Rolling Stone Ronnie Wood today expressed his sadness that his friend Bobby Womack, the inspirational R&B singer-songwriter, has died.

The 70-year-old musician's publicist Sonya Kolowrat said Bobby had died yesterday but did not give any other details.

He was diagnosed with Alzheimer's disease two years ago and dealt with a number of health issues, including prostate cancer.

Ronnie tweeted: "I 'm so sad to hear about my friend Bobby Womack - the man who could make you cry when he sang has brought tears to my eyes with his passing."

He added: "My heart goes out to his family & friends and everyone who loved his music. Bobby you will be greatly missed xx"

Gospel singer Candi Staton knew Bobby since they were children and she toured with him.

"He had a style that nobody else could ever capture," she said in a statement. "I loved him and I will miss him so, so very much."

Bobby had performed recently at the Bonnaroo Music & Arts Festival in Tennessee and appeared to be in good health and spirits.

The star caught the attention of the Stones in the 1960s and influenced many early rockers before fading from popular music for more than a decade.

Blur singer Damon Albarn and XL Recordings president Richard Russell helped him re-launch his career with critically-acclaimed comeback album The Bravest Man In The Universe, in 2012.

He had been scheduled to perform at various events in Europe in July and August.

PA Media