Shares of mobile semiconductor dynamo Qualcomm (NASDAQ:QCOM) has been largely immune to the turmoil that's gripped rival Broadcom (NASDAQ: BRCM) and growth stocks, in general, during the last several weeks.

Qualcomm has been in the cross-hairs of the likes of Broadcom during the past few months, because the company's eying a move into other areas of the semiconductor space, like cellular connectivity, that Qualcomm traditionally dominates.

Qualcomm will give investors a useful update when it reports its FY '14 Q2 earnings after the bell. Keeping that in mind, let's take a peek at what investors should be on the lookout for in Qualcomm's impending earnings.

Qualcomm by the numbers

The general expectation among analysts is for Qualcomm's moderate growth story to continue. The global smartphone and tablet markets are both creeping slowly toward maturity, and that could prove at least a moderate headwind to Qualcomm's overall growth trajectory. However, between its outstanding high-end chipsets, and its high-margin IP licensing business, Qualcomm remains one of the best ways to play the mobile boom, even as names like Broadcom try to encroach on Qualcomm's turf.

In the video below, tech and telecom specialist Andrew Tonner discusses the key numbers and additional storylines that investors should be on the lookout for when Qualcomm reports later today.