One of the worst moments of Tracy Morgan's life was caught on camera, and it has prompted other celebrities to stand up on his behalf.

The New Jersey Turnpike accident killed one and injured four, including Tracy Morgan.

Louis C.K. was unhappy that the video could be viewed on TMZ, the Star Ledger noted.

The first tweet was retweeted more than 3,000 times. He further urged his followers to not watch the video and to also speak out against TMZ. The comedian explained that the gruesome video shows people who are in the ICU, such as his friends Tracy Morgan and Ardie Fuqua. Judd Apatow chimed in a few hours later and tweeted at Harvey Levin, the founder of the website.

.@HarveyLevinTMZ please take down the accident video. — Judd Apatow (@JuddApatow) June 12, 2014

The footage of the accident was taken by a driver. The video taken early on Saturday shows the limo bus that Morgan was in on its side. A crowd surrounds the vehicle.

Krizya Fuqua went on Twitter to ask people for help.

She said, "I need help to get TMZ to remove the video of the accident that shows my dad being pulled from the truck and laid down to the side. This something no one should ever see, it's hurtful and distasteful."

Her picture had a more thorough explanation as to why the video needed to be taken down. She said there was already a lot of false information, and the video was not helping anyone.

It's been reported that Morgan has been more responsive, but he has a broken leg, nose and femur. He also has several broken ribs. He is supposed to remain hospitalized for weeks.

He was supposed to start shooting his new FX show in August, and the network said it would wait for Morgan to return.