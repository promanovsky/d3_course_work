FedEx Corp. posted impressive fourth-quarter earnings gains and set ambitious targets for the new year despite major challenges at its express business caused by lackluster global trade.

Chief Financial Officer Alan B. Graf Jr. called fiscal 2014 "a good year" despite a third quarter plagued by rough weather. He said FedEx expects 2015 "to be even better."

He and other executives said FedEx's restructuring plan is still on track but told analysts on an earnings conference call that the company had to take different steps than initially planned to achieve the goal of $1.6 billion in profit improvements by the end of 2016. Global trade isn't improving as quickly as expected, he said.

"It used to be that international trade was a multiple of GDP, and those days have passed," Mr. Graf said. "We do expect global trade will pick up, but I don't think it'll be a multiple of GDP."

After the economic downturn began in 2008, consumers began shifting to cheaper, slower shipping methods. FedEx in October 2012launched a profitability-improvement effort, mainly affecting the express-shipping business. By the end of last month, about 3,600 employees had taken voluntary buyouts. The plan was to trim costs primarily through reducing the workforce, streamlining processes, modernizing the air fleet and reorganizing some operations.

The additional steps to keep the profitability plan on track included better cost management, reducing capacity, realigning the network and resetting priorities in the express division, executives told analysts. It has reduced the number of trans-Pacific flights in the past year, for example.

Express, the company's biggest business segment, posted a slight increase in revenue, to $7 billion, as volume growth was partially offset by lower express freight revenue and fuel surcharges. The company said one fewer day of operations weighed on the results.

The ground and freight divisions were true bright spots in the quarter. Revenue in the freight segment jumped 12% to $1.55 billion, thanks in part to higher demand for priority service.

E-commerce helped drive growth in FedEx's ground segment, where revenue increased more than 8% to $3.01 billion on an 8% jump in average daily volume during the most recent quarter. This occurred despite the loss of a major customer, which switched its business to the U.S. Postal Service.

Revenue and operating income from the ground segment are expected to grow next year, FedEx said, due to market-share gains and also the recently announced change to "dimensional weight" pricing in the business. In May, the company said that instead of charging by weight alone, all ground packages will be priced according to size starting in 2015, in effect charging more for light but bulky packages.

Overall, FedEx posted a profit for the fourth quarter ended May 31 of $730 million, or $2.46 a share, more than doubling from $303 million, or 95 cents a share, in the year-earlier period, which included $1.18 a share in charges tied to a business realignment program and an aircraft write-down. The company had projected per-share earnings of $2.25 to $2.50.

Revenue rose 3.5% to $11.8 billion, above the $11.66 billion projected by analysts polled by Thomson Reuters.

For the new fiscal year, the company said it expects earnings of $8.50 to $9 a share, with the outlook reflecting no expected year-over-year fuel impact along with moderate growth in the economy. Analysts polled by Thomson Reuters projected $8.76 a share.

The company also expects to boost its capital spending to $4.2 billion, which includes planned aircraft deliveries and the continued expansion of its ground-delivery network, from $3.5 billion in 2014.

Write to Betsy Morris at betsy.morris@wsj.com and Michael Calia at michael.calia@wsj.com

Access Investor Kit for FedEx Corp.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US31428X1063

Access Investor Kit for United Parcel Service, Inc.

Visit http://www.companyspotlight.com/partner?cp_code=A591&isin=US9113121068

Subscribe to WSJ: http://online.wsj.com?mod=djnwires