By now, everyone knows that Will Gardner was killed off of ‘The Good Wife’, but that doesn’t mean that fans are handling the loss well. Viewers are freaking out, even going as far as calling out for help via ‘Good Wife’ support groups! Actor Josh Charles just wants to let the fans know that it will all be okay.

Fan favorite Josh Charles made quite the exit, deciding to leave The Good Wife, and ultimately getting fatally shot off the show on the shocking March 23 episode. The show’s creators have already comforted fans, and now Josh is reaching out to the series’ followers who are mourning over Will Gardner. Keep reading to see what he said!

Josh Charles Comforts Fans After Shocking Death On ‘The Good Wife’

Josh Charles has been very vocal about his decision to leave the The Good Wife, visiting The Late Show With David Letterman to tell his side of the story, which is really quite simple — his contract was up and he wanted out.

Fans are far from happy with Josh’s decision, but in a brand new interview, the actor tells People that the show will go on. “It’s all going to be okay, Josh said in the magazine. “We’re all going to be okay.”

Easy for him to say! He’s known about Will Gardner’s fate since last year! And kept the bloody secret under wraps the entire time: “I only told my family and some close friends,” Josh admitted. “People who I knew could keep a secret.”

Josh goes on to tell People about his fan encounters, after the out-of-nowhere plot twist. “I woke up the next morning, and the people who live across the street, who I don’t even know that well, had left me a letter,” Josh explained to the magazine. “It said that they felt like they’d lost a member of their family, and that it might seem strange to me, but that they really felt like they were in mourning.”

Josh Lends Support To A Famous Fan On Twitter

The man behind Will Gardner isn’t just being bombarded at home, but also on Twitter. Viewers of The Good Wife have been tweeting about the murder nonstop, but a certain famous fan expressed her concerns in the Twitterverse, and Josh directly responded.

Parks and Recreation‘s Retta, who’s known for being comedically vocal on her Twitter, took to the booze to mend her pain.

And Josh ran to the rescue, phoning Retta to keep her calm, but not without warning all of his 83,000 Twitter followers to look out for her!

I just got off the phone with @unfoRETTAble. She’s calm and stable, but everyone please keep an eye out on her as she’s one of my favorites! — Josh Charles (@MrJoshCharles) March 26, 2014

The Good Wife fans have got to stick together! HollywoodLifers, do you think that Josh should have stayed on the show?

— Elizabeth Wagmeister

Follow @EWagmeister

More TV News News: