Rolf Harris told jurors that he was “sickened” by his behaviour as he admitted a string of sexual encounters with his daughter’s best friend when she was a teenager – but he repeatedly denied abusing her as a child.

The 84-year-old entertainer, who has been married for 56 years, detailed seven occasions from sexual touching to full sex, but said they were consensual encounters with a “flirtatious” and “coquettish” teenager. He claimed she initiated the sexual relationship when she was 18.

Southwark Crown Court was told that the woman, 35 years Mr Harris’s junior, was a willing participant in encounters that spanned a decade until their acrimonious break-up – when she accused him of abusing her from the age of 13.

The woman, who cannot be named for legal reasons, has previously told the court that the entertainer first molested her while she was on holiday with his family in the late 1970s, sparking a descent into alcoholism during her twenties.

Giving evidence for the first time, Mr Harris also admitted to an affair with a woman in her thirties after she moved into a small house at the bottom of his garden. The Australian-born artist and musician said that affair began when the woman ferried him to pantomime performances, and ended when she had to move for specialist treatment for breast cancer. “I didn’t feel good about it,” Mr Harris said of the affair.

Court artist drawing of Rolf Harris in the dock (PA)

His voice dropped to murmur as he spoke about his sex life and how he had cheated on his wife, Alwen, with the teenage friend of their daughter, Bindi.

“I felt that I had betrayed everybody: my wife, my daughter and betrayed [the woman’s] parents,” he said.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

“I betrayed their trust because I had had an affair with their daughter who was much younger than me.”

Mr Harris described himself as a “touch-feely” person but denied his victim’s claims that he molested her when she was 13, once as she emerged from a shower and again on a beach in Hawaii.

He claimed the sexual relationship started when the woman stayed at his home in Bray, Berkshire, while visiting his daughter and he took a cup of tea to her bed. Mr Harris said he touched her leg. “I can remember my heart was thumping away,” he added. Mr Harris denies 12 counts of indecent assault on four alleged victims over 18 years from 1968.

It emerged on Tuesday that jurors spotted Mr Harris sketching in the dock on Friday. The pictures have been taken away and destroyed, Mr Justice Sweeney told the court.