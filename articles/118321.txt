The Ice Cube comment at the MTV Movie Awards 2014 unleashed a wave of hate against the rapper. He may have been used to that kind of attention, being one of the original members of the gangsta rap group NWA (don’t ask what that stands for), but this time it put him right back in the spotlight for the wrong crowd.

Ice Cube has been known for his offensive rap career, slamming “the white man” and police equally and openly, but it seems that was all just part of an image that made him famous. His racist stereotype attitude seemed to vanish when it came to some movie roles, where he eventually took on more and more family friendly Christmas films.

However, he is most known for Next Friday, the 21 Jump Street movie, and some older gangster films like Boyz N the Hood. Only Wesley Snipes ever seemed to have more of an attitude, according to rumors from the set of the Blade movies.

Considering his mostly racist stereotype roles and music career, Ice Cube’s comment at the MTV Music Awards 2014 seemed to sink him right back into familiar territory. The award for best on-screen duo had gone to Vin Diesel and Paul Walker, in honor of the late veteran actor from the Fast and Furious franchise. Ice Cube apparently didn’t like that his role alongside Kevin Hart in the movie Ride Along hadn’t won, claiming that his film got “robbed” when the award went to the Fast and Furious stars.

The rapper and actor, born O’Shea Jackson, Ice Cube retracted his comment later. He had tweeted after the ceremony, “I wasn’t really mad we didn’t win. So I would never diss the actors who won. Not even Paul Walker”

On my way to the MTV movie awards. If me and Kevin Hart don’t win. The fix is in. — Ice Cube (@icecube) April 13, 2014

For the record, Ice Cube was not among the several rappers who starred in the Fast and Furious series, including Ludacris and Ja Rule. This could have been why he took such a stance publicly when he stated:

“We had the best chemistry of everybody nominated, for us not to win was crazy. We were the best onscreen duo, period … They should have gave it to him before he passed away.”

This seems to back up what Jackson had said earlier:

“Leaving early from the [MTV Movie Awards 2014]. They told me our category wasn’t gonna be televised. So I left. Just promoting that RIDE ALONG DVD. The sympathy vote: We should honor people before they die. That’s all. Shame on you ‘make something out of nothing’ a** reporters.”

Ice Cube commented later, almost as an apology for his MTV Movie Awards 2014 rant, “Last tweet and I’mma leave it alone. Remember this: A lie travels further then the truth. Damn the devil is busy … And Paul Walker fans don’t trip. I was a fan too.”

What do you think of Ice Cube’s comment and retraction?

[image via Tristan Scholze / Shutterstock.com]