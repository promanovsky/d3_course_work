A European probe is starting to get some good looks at the comet with which it will rendezvous next month.

Recent photos snapped by the European Space Agency's Rosetta spacecraft suggest that its target comet, known as 67P/Churyumov-Gerasimenko, is a lumpy object sporting three large structures, or perhaps a deep hole, researchers said.

"From what we can discern in these early images, 67P is an irregularly looking body," Holger Sierks from the Max Planck Institute for Solar System Research in Germany, principal investigator for Rosetta's scientific imaging system, said in a statement.

Rosetta took the picture on July 4, when it was about 23,000 miles (37,000 kilometers) from the comet. 67P's 2.5-mile-wide (4 km) nucleus covers about 30 pixels in the image, researchers said.

An irregular shape for 67P would not be much of a surprise; none of the five comets that have been visited by spacecraft so far have been anywhere close to spherical. For example, Comet Hartley 2, which NASA's Deep Impact probe flew by in 2010, looks like a chicken drumstick.

Rosetta launched in March 2004, kicking off a looping, 10-year trek to Comet 67P/Churyumov-Gerasimenko. That journey is nearly over; the probe is scheduled to meet up with the comet early next month, then drop a lander called Philae onto its nucleus in November.

Philae will collect samples and take the first-ever photos from the surface of a comet, European Space Agency officials said. The main Rosetta probe, meanwhile, will stay close to the comet as it approaches the sun, helping scientists better understand how these icy bodies change during their voyages through the inner solar system.

The Rosetta mission's estimated total cost is 1.3 billion euros ($1.77 billion at current exchange rates). The mission is scheduled to end in December 2015.

Rosetta's scientific camera system is known as OSIRIS, short for Optical, Spectroscopic, and Infrared Remote Imaging System.

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

Follow Mike Wall on Twitter @michaeldwall and Google+. Follow us @Spacedotcom, Facebook orGoogle+. Originally published on Space.com.

Copyright 2014 SPACE.com, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.