It has been suggested in the past that by introducing a kill switch in smartphones, it would potentially save customers a lot of money from having to pay higher insurance premiums and paying money to replace their device.

Advertising

Unfortunately that bill is currently awaiting action in the Senate but in the meantime, if you’re planning on getting a Samsung Galaxy S5, you might be interested to learn that Samsung will be introducing a couple of security features for both its Verizon and US Cellular models. No word on whether other carriers will be hopping on board but for now, Verizon and US Cellular are the two that have been mentioned.

These security features as “Find My Mobile” and “Reactivation Lock”. If you’re wondering why it sounds familiar, it’s because it resembles Apple’s “Find My iPhone” and “Activation Lock”, both of which are also security features built into the iPhone and iOS devices. In fact, Samsung’s features are more or less the same as well.

Find My Mobile will basically allow users to remotely track a lost/stolen handset with the ability to remotely lock the device as well. It will also alert the person tracking the phone when an unfamiliar SIM card has been inserted into it. As for Reactivation Lock, it works just like Activation Lock in which it prevents anyone from wiping or resetting the phone without putting in a password first.

According to George Gascón, San Francisco District Attorney, Eric T. Schneiderman, New York Attorney General, “While we are concerned that consumers will need to opt-in to the system, thereby limiting the ubiquity and effectiveness of the solution, the fact that Samsung and these carriers have agreed to work together to make Find My Mobile and Reactivation Lock available sends a strong message that all participants in this industry can indeed work together to make their customers safer.”

Filed in . Read more about Galaxy S5, Samsung, Us Cellular and Verizon.