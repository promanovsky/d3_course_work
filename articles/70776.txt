A lot of noise has come out of the electric car manufacturer Tesla Motors Inc (NASDAQ:TSLA) in the last 24 hours. A lot of this talk has come from an appearance on the CBS news program 60 Minutes by Elon Musk, the CEO of Tesla, and also the manufacturer of advanced rockets and spacecraft technology, Space X.

While there were several interesting aspects to the interview with Musk, and the 60 Minutes profile which was generally glowing, it was a comment made by Musk which has received the most attention.

Half Moon Capital Returns 12.2% In 2020 Despite Short Position Drag Eric DeLamarter's Half Moon Capital produced a return of 8% net of fees in the fourth quarter of 2020, bringing the full-year return to 12.2%, according to a copy of its fourth-quarter letter, which ValueWalk has been able to review. The fund maintained an average net exposure of 45% during the period. Q4 2020 hedge Read More

Tesla CEO’s Fear of failure

The Tesla Motors Inc (NASDAQ:TSLA) CEO stated that he never expected the company to be successful! This is not the sort of comment that we usually hear emanating from the head of any company, let alone a multi-billion dollar one such as Tesla. But Musk stated that he wasn’t sure if Americans in particular were ready for the concept of an electric car, but that he had hoped Tesla would be able to “address the false perception that people have that an electric car had to be ugly and slow and boring like a golf cart.”

Tesla has certainly done more than that, establishing itself as a serious rival to the Detroit Big Three car manufacturers. The last eighteen months have seen a period of almost constant growth for Tesla, until the company hit a bit of a snag over the last few weeks. But the long-term prospects for Tesla may still be good if their plans to sell significant amounts of vehicles in pollution-ridden China come to fruition.

Musk’s enthusiasm for Tesla Motors Inc (NASDAQ:TSLA), which apparently went beyond the usual practical considerations of business, is perhaps understandable given the way that he views himself. When Musk was asked about how he sees and describes himself, he answered that he considers himself to be an engineer, and that this is what he has been engaged in since he was a child. Musk emphasized his interest in “things that change the world or that affect the future”, along with “wondrous new technology”, and evidently the Tesla CEO has big plans for the company going forward.

Tesla’s 2008 crisis

The assertion that Musk didn’t expect Tesla Motors Inc (NASDAQ:TSLA) to be a success was not a baseless assertion. Instead, it appeared to be very much grounded in what was going on in Musk’s life and Tesla at the time. Back in 2008, Tesla was losing significant amounts of money, the US economy was in a parlous state (arguably this hasn’t really changed) and SpaceX had also failed to reach orbit with its first three rocket launches. Meanwhile, Musk filed for divorce, and according to the 60 Minutes interview was on the verge of a nervous breakdown at the time.

Eventually, things turned around for both businesses. Space X had a highly successful fourth flight, and received a $1.5 billion contract from NASA. And Tesla managed to raise funding before 2008 which kept the company afloat before its recent upsurge. Musk’s vision and sense of optimism had been rewarded and even in the highly competitive motor trade Tesla Motors Inc (NASDAQ:TSLA)’s future now looks bright.