Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.

Trade-Ideas LLC identified

Deutsche Bank

(

DB

) as a pre-market mover with heavy volume candidate. In addition to specific proprietary factors, Trade-Ideas identified Deutsche Bank as such a stock due to the following factors:

DB has an average dollar-volume (as measured by average daily share volume multiplied by share price) of $48.0 million.

DB traded 121,676 shares today in the pre-market hours as of 8:01 AM, representing 10.5% of its average daily volume.

EXCLUSIVE OFFER: Get the inside scoop on opportunities in DB with the Ticky from Trade-Ideas. See the FREE profile for DB NOW at Trade-Ideas

More details on DB:

Deutsche Bank Aktiengesellschaft provides investment, financial, and related products and services worldwide. Its Corporate Banking & Securities division is engaged in selling, trading, and structuring a range of fixed income, equity, equity-linked, foreign exchange, and commodities products. The stock currently has a dividend yield of 2.4%. DB has a PE ratio of 47.3. Currently there are 2 analysts that rate Deutsche Bank a buy, no analysts rate it a sell, and 2 rate it a hold.

The average volume for Deutsche Bank has been 1.3 million shares per day over the past 30 days. Deutsche has a market cap of $43.4 billion and is part of the financial sector and banking industry. Shares are down 12.8% year-to-date as of the close of trading on Thursday.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

TheStreetRatings.com

Analysis:

TheStreet Quant Ratings

rates Deutsche Bank as a

sell

. The company's weaknesses can be seen in multiple areas, such as its generally disappointing historical performance in the stock itself, deteriorating net income, disappointing return on equity and weak operating cash flow.

Highlights from the ratings report include:

The share price of DEUTSCHE BANK AG has not done very well: it is down 11.27% and has underperformed the S&P 500, in part reflecting the company's sharply declining earnings per share when compared to the year-earlier quarter. Looking ahead, we do not see anything in this company's numbers that would change the one-year trend. It was down over the last twelve months; and it could be down again in the next twelve. Naturally, a bull or bear market could sway the movement of this stock.

The company, on the basis of change in net income from the same quarter one year ago, has significantly underperformed when compared to that of the S&P 500 and the Capital Markets industry. The net income has significantly decreased by 29.5% when compared to the same quarter one year ago, falling from $2,115.92 million to $1,492.05 million.

The company's current return on equity has slightly decreased from the same quarter one year prior. This implies a minor weakness in the organization. Compared to other companies in the Capital Markets industry and the overall market, DEUTSCHE BANK AG's return on equity significantly trails that of both the industry average and the S&P 500.

Net operating cash flow has significantly decreased to -$5,273.84 million or 153.05% when compared to the same quarter last year. In addition, when comparing to the industry average, the firm's growth rate is much lower.

The gross profit margin for DEUTSCHE BANK AG is currently very high, coming in at 72.33%. Regardless of DB's high profit margin, it has managed to decrease from the same period last year. Despite the mixed results of the gross profit margin, the net profit margin of 9.61% trails the industry average.

You can view the full Deutsche Bank Ratings Report.

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

null