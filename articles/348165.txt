The World Health Organization (WHO) will host an emergency meeting on July 2 in Ghana to discuss how to end the deadly Ebola virus outbreak in Africa.

Health officials from 11 countries are attending the meeting to support the agency's efforts. Private companies in Africa woll be joined by policymakers and doctors to join in the fight against the biggest Ebola outbreak that has ever infected the region. There are over 750 people in Liberia, Guinea and Sierra Leone who have already been infected by the disease.

Since mining is the main source of economic growth for countries affected by the outbreak, multinational corporations are leading the campaign. The number of people killed due to the epidemic Ebola virus has reached 467 as of June 30. Because of the heavy traffic near the porous borders of affected countries and the widespread misgivings against health officers and capital cities being affected, the disease was transmitted quickly.

Iron ore miner African Minerals announced that it will be conducting random testing for its employees, saying they are working very closely with the WHO and Sierra Leone government daily. London Mining also said that some of its employees had left the country. Mining and metal corporations Chinalco Mining Corp. International and Rio Tinto which have over 45,000 employees said their each personnel had been tested for the virus.

"We are working very, very closely, on a daily basis, with the government of Sierra Leone and the World Health Organization," said Frank Timmis, founder and executive chairman of African Minerals Executive Chairman and Founder.



The three affected countries will be joined by officials from Mali, Ivory Coast, Uganda, Guinea Bissau, DRC, Ghana and Gambia. There will also be representatives from Democratic Republic of Congo, Cote d'lvoire, Ghana, Gambia, Guinea Bissau, Guinea, Mali, Liberia, Sierra Leone, Senegal and Uganda. Representatives from different U.N. agencies will also be present as well as the Department for International Development, Centers for Disease Control and Prevention, Médecins Sans Frontières, and European Union among others.

The WHO already deployed over 150 teams of experts to help contain the outbreak. They said one of the main reasons why the disease continuously spread is the denial and fear of the illness. Other reasons include the traditional burial of victims in rural communities, dense population in capital cities of Liberia and Guinea and social and commercial activities at the borders.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.