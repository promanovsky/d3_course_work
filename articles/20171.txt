A new chocolate pill is being tested on patients as it is believed that it could help prevent heart attacks and stroke.

The pill has chocolate as its key ingredient, which has cocoa flavanols, as earlier tests have found that the nutrients can improve blood pressure, cholesterol levels, the body's use of insulin, artery health and other heart-related factors, the Daily Express reported.

However, the flavanol capsules, which will be tried out in a study of 18,000 men and women in a 12 million pounds industry-funded clinical trial by the Harvard-affiliated hospital, sponsored by the National Heart, Lung and Blood Institute and Mars Inc., are coated and will have no taste.