What a difference a year makes.

In 2013, Bill Ackman was accused of losing his golden touch. His investment in J.C. Penney was called a "fiasco." His Herbalife short famously turned against him when fellow activist investors Carl Icahn and Dan Loeb took the other side of the trade. Pershing Square Capital Management's assets fell as performance was barely positive into the fall.

This year is different. Late Monday, news broke that Ackman and Valeant Pharmaceuticals were offering to buy Botox maker Allergan. Shares of Allergan, of which Pershing Square owns 9.7 percent, immediately gained about 23 percent.

That's not the only winner: Every Ackman investment appears to have made money this year, including Beam, the Herbalife short and Platform Specialty Products Corp. Nutritional supplement maker Herbalife, for example, has been hit by reports of several government investigations into the company, which has helped push the stock down about 26 percent so far in 2014.



The Pershing Square International fund is up 11.1 percent net of fees in the first quarter alone, according to a report by the HSBC Alternative Investment Group.