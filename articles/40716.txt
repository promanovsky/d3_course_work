First comes love, then comes marriage, then comes a baby in a baby carriage, right?

The playground rhyme may ring true for Ashton Kutcher, 36, and Mila Kunis, 30, who are reportedly expecting their first child. The news comes just weeks after Kutcher allegedly popped the question.

E! news broke both the pregnancy and engagement stories, and although neither Kunis or Kutcher have confirmed, the network is super sure via an exclusive source that the news items are true. Oh, and there’s a giant rock on her ring finger. That helps.