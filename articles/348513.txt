Crude oil prices have been rising over the past few months and are now hovering above the triple-digit mark thanks to geopolitical tensions in the Middle East. A strained relation between the West and Russia for invading Ukraine as well as uncontrollable violence in Iraq have threatened global oil supply disruption.

This is because both the countries are rich in oil and gas resources and are the major suppliers of the commodity. Russia is the world's largest producer of crude oil while Iraq is the seventh largest producer. The supply crunch in other oil producing countries added to further woes. Fights in Libya, robbery in Nigeria oil fields and international sanctions against Iran over its nuclear program for exporting oil have taken a toll on oil supplies.

Though oil production in the U.S. is booming and has reached its highest level in 28 years on shale formations as well as newly tapped oil and gas fields in North Dakota and Texas, it is unlikely to recoup lost supplies from across the globe. On the other hand, global oil demand is rising on improving economic conditions, resulting in higher oil prices (read: Uprising in Iraq Puts These Oil ETFs in Focus ).

While the strength is beneficial for a number of oil producers and key oil producing countries, it is a major threat to oil consuming nations. After all, higher oil prices would restrict tax revenues or GDP growth opportunities in big oil importing countries. This is especially true as imports become more expensive and exports less valuable. It will lead to deterioration in balance of payments, lower output and increase inflation and unemployment rate in these countries, thereby leading to a drop in overall economic growth.

We have seen this phenomenon take place in a number of countries over the past few years, and with the advent of ETFs, these nations are easier to avoid. In light of this, we have highlighted three country ETFs that could see terrible trading in the months ahead should oil price rises or remain above $100 per barrel.

iShares India 50 ETF ( INDY )

India has become the world's third largest importer, overtaking Japan early this year, and accounts for two-thirds of the crude oil requirements through imports. The Indian economy has shown some signs of strength on Narendra Modi led Bharatiya Janata Party's win in the general election held in mid May, but has started losing steam lately with the rising oil prices.

This is because an increase in oil prices would flare up the already high inflation in the country putting pressure on the individual pockets or government's finances in the form of increased subsidies. This suggests tough times ahead for the country and Indian ETFs in the coming months (read: Bear Market for India ETFs Following Modi's Election? ).

INDY is a large cap centric fund that follows the CNX Nifty Index, which seeks to track the performance of the largest 50 Indian stocks. The fund is highly concentrated on its top 10 holdings as it accounts for about 54.7% of total assets. With respect to sector holdings, banks and software take the top two spots at 21.24% and 14.03%, respectively, while others make up for single-digit allocations.

The ETF has amassed $572.9 million and trades in volume of nearly 205,000 million shares a day. Expense ratio came in at 0.93%. INDY is up nearly 6% over the past month and has a Zacks ETF Rank of 3 or 'Hold' rating with Medium risk outlook.

iShares MSCI Turkey ETF ( TUR )

The importance of Turkey in the world energy market is growing as 90% of the energy requirements are met through imports. If geopolitical tensions escalate, a twin attack on the Turkish economy is most likely as high energy prices would push up inflation and hit the recently recovered export market.

This would weigh on the country's efforts to narrow its wide current account deficit and dampen its resilient economy, which expanded 4.3% in the first quarter beating expectations. In such a backdrop, the Turkey ETF seems a bad pick to stuff into the portfolios for the coming months (read: Can the Turkey ETF Make New 2014 Highs? ).

TUR follows the MSCI Turkey Investable Market Index and provides a pure play exposure to 89 Turkish equities. The fund is highly concentrated on its top 10 holdings making up for nearly 59.5% of assets. Financials dominate the fund's return with just less than half of the portfolio while consumer staples and industrials take double-digit exposure in the basket.

The fund has managed assets worth $549.8 million while charges 62 bps in fees per year. Volume is solid though as it exchanges around 489,000 on shares in hand a day. The ETF lost 3.4% over the past one month and has a Zacks ETF Rank of 5 or 'Strong Sell' rating with High risk outlook.

Market Vectors Egypt Index ETF ( EGPT )

Though a major portion of the world's oil is shipped through the Suez Canal, Egypt is actually the largest consumer of crude oil in Africa trailing South Africa. Rising oil prices could have a major impact on its dwindling economy, indicating that some pain is in store for this country ETF as well.

The fund tracks the Market Vectors Egypt Index, which comprises companies that are domiciled in Egypt or generate at least 50% of their revenues in the country. The fund holds 27 securities in its basket and amassed $69.2 million in its assets base. Expense ratio came in at 0.98% while trading volume is weak (see: all Africa-Middle East ETFs here ).

In terms of holdings, the product is concentrated on the top firm - Orascom Telecom Holding - at 9.38% and the top sector, financial, at 45.5%. The ETF is down 1.4% over the one-month period and has a Zacks ETF Rank of 5 with Medium risk outlook.

Bottom Line

Investors should write-off this trio of nations if they expect a continued rise in oil price to push up the cost for these oil-importing countries. The products are expected to underperform as long as the commodity surges.

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days . Click to get this free report >>

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

ISHARS-SP INDIA (INDY): ETF Research Reports

ISHRS-MSCI TURK (TUR): ETF Research Reports

MKT VEC-EGYPT (EGPT): ETF Research Reports

To read this article on Zacks.com click here.

Zacks Investment Research

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.