Robin Williams checks into Minnesota rehab center to 'focus on continued commitment' to sobriety

Robin Williams has checked into rehab.

On Tuesday the 62-year-old actor's rep confirmed to TMZ the Mrs Doubtfire star is seeking treatment at Hazelden Addiction Treatment Center in Minnesota.

'After working back-to-back projects, Robin is simply taking the opportunity to fine-tune and focus on his continued commitment, of which he remains extremely proud,' his spokesperson said.

Scroll down for video



Maintaining his sobriety: On June 29 Robin Williams was spotted at a Dairy Queen in Minnesota, not far from Hazelden, where he was receiving treatment

On Sunday the Mork & Mindy star was spotted at a Dairy Queen in Lindstrom, which is not far from Hazelden. He looked healthy in a black shirt and slacks as he posed next to a female employee.

Williams is staying at Hazelden's The Lodge, which is known for helping patients maintain long-term sobriety.

The site claimed Robin will be staying at The Lodge for several weeks.

The center: Hazelden is located in Minnesota and their Lodge department specializes in helping patients achieve long-term sobriety

Demanding schedule: Williams, pictured in 2013, had been working nonstop and needed some time for himself, his rep said in June

Treatment has not stopped the actor from watching the World Cup.

On Tuesday - the same day his stint in rehab was confirmed - Robin tweeted an image of himself as Teddy Roosevelt in his film Night At The Museum next to soccer super fan Mike Damico, who goes by the name Teddy Goalsevelt.

The caption read, 'Two Teddys. One Goal. #IBelieveThatWeWillWin chaaaarrrrrrrge #USA!!'

Damico tweeted back, 'Thanks RW. You've always been my favorite fake Teddy.'

Prolific tweeter: The Oscar-winner shared many thoughts and images on social media, but had remained silent during August

In 2006 Williams checked into a rehab center in Oregon and admitted he was an alcoholic.

His rep said at the time, 'After 20 years of sobriety, Robin Williams found himself drinking again and has decided to take proactive measures to deal with this for his own well-being and the well-being of his family.'

Robin has admitted to problems with cocaine in the late seventies and early eighties. The death of his friend, comic John Belushi, is what made him want to give up drugs. 'Was it a wake-up call?' he said years later. 'Oh yeah, on a huge level.'

A very big career: In May his film The Angriest Man In Brooklyn with Mila Kunis was released

The Chicago native has been making movies back to back. This year alone he was in the films Boulevard with Kathy Baker and The Angriest Man In Brooklyn with Mila Kunis.



In December he will star in Merry Friggin' Christmas, about a father who drives all night to collect his son's presents. There is also talk that he will star in a sequel to his monster 1993 Mrs Doubtfire.



He has also spent this year filming Night At The Museum' Secrets Of The Tomb with Ben Stiller and Rebel Wilson. Williams plays Teddy Roosevelt.



The comedic legend has just come off the TV series The Crazy Ones with Sarah Michelle Gellar.

Legendary: The comedic genius with Pam Dawber in Mork & Mindy, which aired on TV from 1978 and 1982

The Good Will Hunting star was married to Marsha Garces for 20 years; they divorced in 2008.

Together they had two children, 24-year-old Zelda Rae and 21-year-old Cody Alan.

In 2011 the Oscar winner tied the knot with Susan Schneider and honeymooned in Paris, visiting the Musée de l'Orangerie and kissing in the streets.