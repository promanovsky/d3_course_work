Zebras enjoy the sunshine and the 74 degree weather at the Saint Louis Zoo in St. Louis on December 3, 2012. Temperatures will remain above average for at least another week. (File/UPI/Bill Greenblatt) | License Photo

Why and how did the zebra get its stripes? It's a question that's confounded evolutionary biologists for decades.

But now a team of researchers at the University of California, Davis, believe they've systematically solved the puzzle.

Advertisement

In a study published today in the online journal Nature Communications, scientists have proffered the horseflies and tsetse flies -- and their painful bites -- the main evolutionary drivers of zebra stripes.

Since the days of Charles Darwin and The Origin of Species, scientists have put forward a range of explanations for the stripes: they offer camouflage, visually confuse large predators, help deflect heat, play a social function, and protect against flies.

To test these ideas, researchers compared the geographical range of zebras with factors relevant to the various theories: climate, presence of dangerous predators, breeding grounds for flies, etc. In doing so, they ruled out all but one hypothesis -- that the stripes ward off flies.

"No one knew why zebras have such striking coloration," said Tim Caro, a professor of wildlife biology at UC Davis. “But solving evolutionary conundrums increases our knowledge of the natural world and may spark greater commitment to conserving it."

But why do flies avoid stripes? Caro says they'll need to do more research to figure that out.

[UC Davis]