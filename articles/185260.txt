Alibaba has finally filed for a U.S. initial public offering which will see it become the largest Chinese firm to list in New York. The question now is how much will the e-commerce giant be valued at when it starts trading.

Recent estimates put Alibaba's value between $150 billion to $200 billion. That suggests the IPO could raise up to $15 billion, making it one of the biggest tech IPOs since Facebook listed its shares in 2012.

"There's going to be no trouble at all with this thing having a market cap of over $200 billion in my opinion after looking at the numbers. And there's a fair chance that in three or four years, it could be the most valuable company in the world," said Larry Haverty, a portfolio manager at Gabelli Funds.



Read MoreFor Alibaba, it's breadth, not just scale, of ambition



"Alibaba is a one-off," he added. "U.S. investors have rarely seen a company with these kinds of growth characteristics. You have massive scale and profit growth and enormous market share."

After weeks of anticipation, Alibaba late on Tuesday filed for a nominal $1 billion initial public offering (IPO). The ultimate offering is expected to be much higher.