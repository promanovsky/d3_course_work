Intuitive Surgical Inc. (ISRG) announced Tuesday morning that it received FDA clearance for the da Vinci Xi Surgical System.

Intuitive Surgical gapped open dramatically higher Tuesday, but traded in a range for the majority of the session. The stock closed up by 55.61 at $493.60, with volume at over an 8-month high. Intuitive Surgical leaped to nearly a 9-month high.

For comments and feedback contact: editorial@rttnews.com

Business News