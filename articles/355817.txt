Lea Michele and her new boyfriend Matthew Paetz look super cute together in these newly released photos taken last week in Los Angeles.

This is the first sighting of the new couple out and about together since their relationship was reported back in early June.

The couple was seen out and about twice that day – once in the morning on a hike together and then later in the day taking a stroll in the park after washing up from their workout. He then spent the rest of the weekend at Lea‘s house and even used her car to run errands!

Lea and Matthew met on the set of the music video for her song “On My Way.” He was one of the backup dancers and love interest for her in the video.

FYI: Lea is wearing a Minkpink dress. Lea is also wearing Under Armour sneakers.