The renewable energy sector created around 70,000 jobs in the country in three years ending 2014, according to a report released by Natural Resources Defense Council (NRDC)- Council on Energy, Environment and Water (CEEW) here today.

The report titled 'Clean Energy Jobs and Assessing Opportunities to Boost Financing for Clean Energy Projects in India' stated that around 24,000 full time-equivalent jobs were created in the solar grid connected projects between 2011 and 2014 and 45,000 people were employed in the wind energy sector during this period.

The report mentioned that most of the jobs were generated during the construction and commissioning of the project. The local communities get benefitted during the operations and maintenance of the consignment.

The report found that solar deployment creates employment opportunities in the country with smaller solar projects (with a capacity up to 5 mega watt) having the highest potential to create jobs.

"Solar and wind energy can transform India in a significant way. With access to electricity in rural areas, there will be huge economic activity leading to higher job creation," said Suresh P Prabhu, head of the advisory group for integrated development of power, coal and renewable energy.

Prabhu said that the country, however, needs to find out long-term financial sources to scale the renewable energy market.

The report recommended the Centre to boost financing by improving access to low cost finance "using diverse financial mechanisms such as infrastructure debt funds, priority sector lending, green bonds and tax incentives."