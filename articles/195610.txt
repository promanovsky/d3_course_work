The often misunderstood condition of dementia is caused by a number of diseases that affect the brain, the most common being Alzheimer's.

The condition often starts with short-term memory loss, but it can also affect the way that people think, speak and do things, although symptoms can differ from person to person.

Imagine the brain as a string of fairy lights, with each light representing a different skill such as memory, speech or movement. The action of dementia on the brain is as if some of the lights are flickering on and off, whilst others go out for good, and some continue to work as normal.

Public Health England and Alzheimer's Society have joined forces for the Dementia Friends campaign to help raise awareness of the condition - find out how you can help a person with the condition here.

The 10 signs:

Dementia can significantly affect a person's lifestyle - here are the key signs and symptoms to look out for...

Struggling to remember recent events but easily recalling the past Finding it hard to follow conversations or programmes on TV Quickly losing the thread of a conversation Forgetting the names of friends or everyday objects Repeating sentences Difficulty with thinking and reasoning Feeling anxious, depressed or angry about forgetfulness Others commenting on their forgetfulness Confused even when in a familiar environment A decline in the ability to talk, read or write

The diagnosis process:

Only 48% of people with dementia are professionally diagnosed, yet a timely diagnosis may help gain access to medication that can help slow the progression.

Catching the condition early can also allow a person with dementia to plan and make arrangements for the future.

1. Consult your GP

Your GP is the first person to speak to, they can help rule out other conditions that may have similar symptoms.

A doctor can also help identify other possible causes of confusion, such as poor sight or hearing, emotional traumas such as bereavement, or medication side-effects.

2. Referral

Your GP may then refer the person being diagnosed to a specialist consultant, for further assessment and possibly a brain scan.