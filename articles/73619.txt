Washington: Google has reportedly come up with an interesting prank for this year`s April fool`s day, where it tempts Google Map users to play a Pokemon augmented reality game, making them wish it were real.

Google has partnered with Nintendo to give Pokemon fans a chance to catch 150 Pokemons placed across the globe and be the best trainer.

According to Tech Hive, in a characteristically stylish video, Google shows off how incredible it would be to wander around the world, hold one`s phone out, and see Pokemon appear.

Pokemon fans, in the end, are left disappointed when they exit the window and realize that no such Pokemon game actually exists.

However, Pokemon fans can boot up Google Maps on either iOS or Android and capture all the hidden Pokemons hidden around the world.