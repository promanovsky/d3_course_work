Brittany Murphy's Final Movie 'Something Wicked' Premiering in Oregon

The thriller will debut at Regal Cinemas in Eugene on April 4.

More than four years after her shocking death, Brittany Murphy's final movie will make its world premiere next month.

Something Wicked will open at the Regal Cinemas in Eugene, Oregon (Valley River Center Stadium 15), on April 4, as the start of a planned platform release. Merchant Films is self-distributing the film with plans to expand to more than 20 additional regional Regal theaters the following week, before moving into California and other parts of the country.

PHOTOS: Remembering Brittany Murphy

On April 11, Something Wicked will expand to five Regal theaters in Portland, with seven theaters to be added in Seattle the next week. It will also open the University of Oregon's Cinema Pacific Festival in Eugene and Portland, Oregon, on April 23.

The film, which was shot in Eugene, is a dark thriller starring Murphy, Shantel VanSanten (The Messengers), John Robinson (Lords of Dogtown) and Julian Morris (Valkyrie). Darin Scott directed from a script by Joe Colleran, who also produced.

The story follows a young couple embarking upon their wedding plans, when gruesome secrets from their past collide with sinister forces of the present.

Murphy, who starred in Girl, Interrupted, 8 Mile and Drop Dead Gorgeous, died on December 20, 2009, at the age of 32 from pneumonia, anemia and cardiac arrest.