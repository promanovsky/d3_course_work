On Saturday night, Nickelodeon aired its 27th annual Kids' Choice Awards.

It may have been the kids' choice, but celebrities of all ages made an appearance and walked down the iconic orange carpet that appears each year at the slime-filled award show.

The event was hosted by Mark Wahlberg, who avoided getting slimed all night long up until the ending of the show. Wahlberg's three kids, along with the help of actor and comedian Kevin Hart, pulled the prank that left Wahlberg stunned on stage while drenched in green slime.

"I can't believe my own kids turned on me," Wahlberg yelled.

The Ted star wasn't the only one who fell victim to the green goo, however. Among the slimed prey were Pharrell, who sported a bright yellow Spongebob onesie, and Big Bang Theory actress Kaley Cucoco-Sweeting. Magician David Blaine chose to slime himself while fans chose pop singers Austin Mahone and Cody Simpson to get slimed together.

The best dressed award of the fun-filled night goes to Ariana Grande and Lea Michele who both sported bright colored mini-dresses just in time for spring.

Grande switched things up a bit and finally let her hair down while wearing a fitted bright orange mini dress that coincided with the KCA carpet. Perhaps the most exciting thing about the vocal powerhouse's outfit was her handbag, which featured classic Nickelodeon cartoon characters from shows like Hey Arnold! and The Rugrats.

Michele kept things simple with a blush-colored dress that flared a bit at the bottom. The brunette wore matching pink lipstick and neutral pumps to match her clutch.

The big winners of the night included One Direction, Jennifer Lawrence, Selena Gomez, and Adam Sandler. Nickelodeon's Sam & Cat producer Dan Schneider won the award show's first-ever lifetime achievement award.

What were your favorite moments of the night? Share them with us below.