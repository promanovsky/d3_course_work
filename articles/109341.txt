Dave Grohl of Foo Fighter sings during the 2012 Democratic National Convention at the Time Warner Cable Arena in Charlotte, North Carolina on September 6, 2012. UPI/Nell Redmond | License Photo

Courtney Love arrives on the red carpet before the screening of the film "This Must Be The Place" during the 64th annual Cannes International Film Festival in Cannes, France on May 20, 2011. UPI/David Silpa | License Photo

Foo Fighter lead singer Dave Grohl (R) rehearses at the Time Warner Cable Arena during the Democratic National Convention in Charlotte, North Carolina on September 6, 2012. UPI/Nell Redmond | License Photo

Courtney Love attends the Metropolitan Opera Season Opening with Donizetti's "L'Elisir D'Amore" at the Metropolitan Opera House at Lincoln Center in New York on September 24, 2012. UPI /Laura Cavanaugh | License Photo

NEW YORK, April 11 (UPI) -- Courtney Love and Dave Grohl seemed to end their years-long feud Thursday night at the Rock and Roll Hall of Fame ceremony.

The two musicians were both in attendance to honor Nirvana's induction into the Hall of Fame. Love is deceased Nirvana frontman Kurt Cobain's widow, and Grohl served as Nirvana's drummer and was Cobain's longtime friend.

Advertisement

Love and Grohl have never enjoyed a particularly close friendship, and their relationship became more acrimonious after Cobain's 1994 suicide. Their present-day interactions have been largely legal in nature, with an occasional public insult or accusation thrown in.

The pair appeared to put their differences aside last night when Love embraced Grohl onstage at the ceremony.

"This is my family I'm looking at right now," Love said. "I just wish Kurt could have been here. Tonight he would have really appreciated it."

She later posted a picture of the hug on her Facebook and Twitter and wrote, "The most magical part of the evening. Thank you Dave, love you. I know this made him smile up there." She also wrote a separate tweet addressed to Nirvana bassist Krist Novoselic that included the hashtags "hatchetburied" and "deepintheground."

The most magical part of the evening. Thank you Dave, love you. I know this made him smile up there pic.twitter.com/1VPdVKAAbc — Courtney Love Cobain (@Courtney) April 11, 2014