Hundreds of McDonald's workers and community activists staged a protest Wednesday near the fast-food chain's headquarters in Oak Brook, seeking a wage increase to at least $15 per hour for employees.

The demonstration was peaceful, with 138 protesters presenting their IDs to police and allowing themselves to be arrested and led to a bus, one-by-one, on a hot afternoon. The Oak Brook Police Department said the arrests were made for “criminal trespass to property” and that those arrested could face a fine.

The push to raise fast-food and retail employees' wages has led to protests nationwide since the movement took shape in 2012, with demonstrations from New York to Los Angeles that are organized by groups financially backed by the Service Employees International Union. Wednesday's gathering marked the latest against McDonald's, coming a day before the company's annual meeting. The meeting will go on as scheduled Thursday morning, a McDonald's spokeswoman said.

While waiting to be arrested, Marie Sanders, a 25-year old McDonald's worker from Kansas City, Mo., said that she makes $7.75 an hour now after starting at $7.25 in 2011. She said that her working conditions are fair but “it's not a living wage. It's like a constant struggle every day.”

Earlier, about 500 people clogged the entrance road to the company's headquarters, with some singing “We shall not be moved” as police warned them they could be arrested if they did not move back.

“We think it was a peaceful demonstration,” McDonald's spokeswoman Heidi Barker Sa Shekhem said.

She said the minimum wage is an “industry issue. And like any company, we have our eye on the debate.” She said the low wage some fast-food workers made is “a starting wage,” because workers have the opportunity to advance in their careers at McDonald's.

McDonald's headquarters remained open Wednesday, but workers in one of the five buildings on the company's campus were asked to work from home in advance, according to a company spokeswoman.

“That building is in a high traffic area, off a major highway and is next to a busy shopping mall and the pedestrian and vehicle traffic and congestion would be unbearable,” spokeswoman Lisa McComb said in an email. “They are working from home and the rest of us are working as usual.”

Shareholders will vote on whether to approve the company's executive compensation at Thursday's meetng. Chief Executive Don Thompson earned total compensation of $9.5 million in 2013.

Fast food workers in Chicago make about $8.25 per hour, the state's minimum wage, protest organizers have said. Many are part-time workers without benefits who don't have a set schedule.

“We need to show McDonald's that we're serious and that we're not backing down,” said Jessica Davis, a 25-year-old McDonald's crew trainer.

Davis, a mother of two, works at a company-owned McDonald's in Chicago, said she earns $8.98 per hour and works part-time despite requests for more hours. Most of the protesting workers are employed by franchisees. That makes them a hard group to organize because the union would have to launch a campaign with every employer and gain the support of the majority of the workers at every location.

Cherri Delesline, 27, from Charleston, S.C., said that she makes $7.35 an hour at a McDonald's and has been working there for 10 years. She said that her flight had been paid for by a union.

In Chicago, some McDonald's workers are members of the Workers Organizing Committee of Chicago. For now, the group is educating workers on labor laws and asking them to voluntarily join the group, which is registered as a union. The workers don't pay dues but are asked to attend meetings and participate in events.

The movement began as a walkout in New York in 2012 , and evolved into one-day protests that have targeted retailers and other fast food operators, including Burger King and Wendy's.

Since then, U.S. President Barack Obama has pushed Congress to raise the federal minimum wage to $10.10 per hour from the current $7.25.

Twenty-one states and Washington, D.C. have minimum wages higher than the federal minimum wage, and 38 states have considered minimum wage bills during the 2014 session, according to the National Conference of State Legislatures.

Robert Bruno, a professor of labor and employment relations at the University of Illinois at Chicago, said civil disobedience is a long practiced and developed form of resistance that groups without money or political clout use to drag out their “oppressor.”

The fact that workers are willing to travel and get arrested shows that the movement is getting stronger, Bruno said. The election season, he said, creates a platform for the McDonald's workers' campaign.

On Tuesday, Speaker Michael Madigan ushered through the House a bill that would allow voters to weigh in on whether Illinois should raise the minimum wage to $10. The proposal, which would not have the force of law, still needs Senate approval.

As he campaigns for re-election, Gov. Pat Quinn has requested a minimum wage hike from the current $8.25 per hour. Democrats, however, are unlikely to have the votes this spring to pass such an increase.

Chicago Mayor Rahm Emanuel on Tuesday announced the creation of a committee that will discuss the pros and cons of a minimum wage hike.

The committee could allow Emanuel to put off doing something at City Hall until the results of the statewide referendum are known, then let state lawmakers change the minimum wage if voters back the idea.

Tribune reporters Ray Long, Alejandra Cancino, Monique Garcia, John Byrne and Ellen Jean Hirst contributed, as did Reuters.