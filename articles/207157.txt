Health officials have confirmed a second US case of a mysterious virus that has sickened hundreds in the Middle East.

A news conference to discuss the case has been scheduled for Monday afternoon by the Florida Department of Health and the Centers for Disease Control and Prevention.

The virus is Mers, or Middle East respiratory syndrome. It is a respiratory illness that begins with flu-like fever and cough but can lead to shortness of breath, pneumonia and death.

Most cases have been in Saudi Arabia or elsewhere in the Middle East. But earlier this month a man who traveled from Saudi Arabia to Indiana became the first US case.

That man was a healthcare worker at a hospital in Saudi Arabia's capital city who flew to the US on 24 April. After landing in Chicago, the man took a bus to Munster, Indiana where he became sick and went to a hospital on 28 April.

The man, an American, was released from the hospital late last week. Tests of people who were around the man have all proved negative, health officials have said.

Details about the newest case were not immediately released.

Mers belongs to the coronavirus family that includes the common cold and Sars, or severe acute respiratory syndrome, which caused some 800 deaths globally in 2003.

The Mers virus has been found in camels, but officials don't know how it is spreading to humans. It can spread from person to person, but officials believe that happens only after close contact. Not all those exposed to the virus become ill.

But it appears to be unusually lethal – by some estimates, it has killed nearly a third of the people it has affected. That is a far higher percentage than seasonal flu or other routine infections. But it is not as contagious as flu, measles or other diseases. There is no vaccine or cure and there is no specific treatment except to relieve symptoms.

Overall, at least 400 people have had the respiratory illness, and more than 100 people have died. So far, all had ties to the Middle East region or to people who traveled there.