Not one to let Valve have all the dev-con fun, Oculus is throwing the inaugural Oculus Connect in Los Angeles later this year.

This is the first developer conference from the Oculus Rift maker and a sign there might be something to this virtual reality thing after all.

"With virtual reality's momentum at an all-time high, this is a unique moment for the developer community to come together to take the virtual reality to the next level," Oculus wrote in an announcement post.

The conference goes down September 19 - September 20. It's open to the public, but hopefuls will have to apply for a spot beginning July 10. While anyone can attend, Oculus pointed out this will be a "developer-centric event." That being said, Connect-ers will be the first to learn about new Oculus technologies.

And though it's being billed as an event for people "to share and collaborate in the interest of creating the best virtual reality experiences possible," an Oculus spokesman wasn't able to tell TechRadar if Connect would focus solely on Oculus and the Rift or if it would be open to other types of VR hardware. He promised more details in the coming days.

VR's E3?

If you score a pass to Oculus Connect, you'll have access to keynotes from Oculus' bigwigs. CEO Brendan Iribe, Founder Palmer Luckey, CTO John Carmack and Chief Scientist Michael Abrash are all scheduled to speak.

Likely featuring the usual "this is why you should create stuff for us" pitches and how-to lessons, there's also a bit of E3 flair to the whole affair, too.

"VR developers, gaming, entertainment and cinematic content makers, innovators, creative thinkers, enthusiasts and more" are encouraged to attend, meaning the halls should be teeming with some die-hard VR fans.

There will also be space for attendees to demo their own VR content, offering what could be some first looks at the apps, games and other content Iribe has said Oculus Rift needs to break into the big leagues.

Alongside its Oculus Connect announcement, Oculus also revealed it acquired RakNet, a gaming industry networking middleware firm.