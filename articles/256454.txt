Facebook’s founder and CEO, Mark Zuckerberg, is not very popular in Iran at the moment. In fact, Politico reports that a judge in Iran has ordered Zuckerberg to appear in court in Iran with regards to user privacy violations of the Facebook apps Instagram and WhatsApp.

Huffington Post reports that 30-year-old billionaire Zuckerberg will most likely decide not to show up in the Iranian court, since the U.S. has “no extradition laws with Iran.” However, TheBlaze.com reveals a comment made by Ruhollah Momen-Nasab, an official with the paramilitary Basij force stating, “According to the court’s ruling, the Zionist (Zuckerberg) director of the company Facebook, or his official attorney, must appear in court to defend himself and pay for possible losses.”

In Iran, they refer to Mark Zuckerberg as the Zionist because of his Jewish upbringing, even though Mark professes to be an atheist, as per Huffington Post. Momen-Nasab also claims the judge has also ordered the two apps, Instagram and WhatsApp, be blocked. Facebook has already been blocked in the country, along with other social media sites such as Youtube and Twitter.

Who would have ever thought Mark Zuckerberg would be blocked?

What is interesting, however, is the fact that according to Politico, there are senior leaders within Iran, such as Foreign Minister Mohammad Javad Zarif, who are active on Twitter, even though those types of networks have been banned.

According to The Blaze, this most recent court incident with Mark Zuckerberg is just one example of “the escalating struggle between self-proclaimed moderate Iranian president Hassan Rouhani’s push for increased Internet freedoms and demands by conservative judges for tighter controls.”

In Mark Zuckerberg’s defense, Ecomonic Times reveals Rouhani’s statement, “Iran should embrace the Internet rather than see it as a threat, and use ‘smart filtering’ to weed out immoral content.”

Since Zuckerberg’s Facebook purchased Instagram for $1 billion in 2012, and followed up with the purchase of WhatsApp (the largest messaging center in the world) in 2014 for $19 billion, Zuckerberg’s applications are not going away, even if Iran decides to block them.

Mark Zuckerberg is most likely not losing any sleep over this summons. Zuckerberg is also not packing his bags to make the trip to court because, according to Economic Times, “Iran is still under international sanctions over its disputed nuclear activities, and it is difficult for U.S. citizens to secure travel visas, even if they want to visit.”

It is highly unlikely that Zuckerberg will put up a fight to get there.

One question we may all want to ask Mark Zuckerberg is how does it feel for you to be blocked?

photo credit: blogpocket via photopin cc