Jamie Lynn Spears has reportedly married her partner Jamie Watson.

The younger sister of Britney Spears announced the news of her engagement last year. She posted a picture of the engagement ring on her Instagram page.



US sources (via E! Online) have reported that the couple tied the knot in New Orleans and held the reception at the Audubon Tea Room.

Spears is said to have worn a v-neck Liancarlo gown. Her daughter Maddie was flower girl, while Britney's two sons, Sean Preston and Jayden James, acted as ring bearers during the service.

The 22-year-old found fame when she was cast as the lead role in Nickelodeon show Zoey 101.

After falling pregnant at 16, Jamie Lynn got engaged to her daughter's father Casey Aldridge. The pair split up in 2010.

Catherine Earp Catherine Earp is Digital Spy’s News Editor covering all things entertainment.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io