Scotty McCreery, of American Idol fame, was held at gunpoint at an apartment in Raleigh, N.C. early this morning. According to Taste of Country, McCreery was visiting some friends when three men broke in to the apartment where the guys were hanging out. It’s unknown if the apartment belonged to McCreery — who is a student at the North Carolina State University in Raleigh — or one of his friends.

No one was injured in the break-in, but McCreery and his pals lost their cell phones and their wallets, having been forced to give up their belongings to the gunmen. Neighbors say they heard people running up the stairs, yelling according to Fox News.

One witness recalled:

“All you heard was yelling like ‘Who are you? What are you doing here?’ for a good five minutes. It was just yelling.”

Scotty McCreery is lucky that he didn’t get kidnapped — or worse. It is unknown if the gunmen knew that McCreery was at the apartment or if the break-in was random. It is also unknown how much money the men got away with. A rep for the country music star has not released a statement of any kind.

It is also unclear whether or not police have any leads in the investigation. McCreery, 20, likely had to cancel all of his credit cards and get a new license — which is a pain, but it’s doable. The most important thing is that no one was physically hurt.

As previously reported by Inquisitr.com, McCreery has been really focused lately. He seems to mind his own business in the music world — and that’s a good thing. He recently commented on Miley Cyrus’ twerking and said that he wouldn’t be doing anything of the sort any time soon — and most of his fans totally appreciate that.

“I don’t want to put anyone down, but you won’t see any twerking from me.”

Scotty McCreery has been touring whilst keeping on top of his schoolwork, which is quite a lot for his schedule. On the night of the break-in, McCreery had just gotten back from a long day of travel. According to his Web site, he will be heading back on the road for his show in Bakersfield, Calif. this Friday. McCreery just finished up his second year of college and will be enrolled as a junior in the fall. He will likely use the summer to tour and focus strictly on his music before having to buckle down again in September.

[Photo courtesy of Isaac Brekken / Getty Images via TheBoot.com]