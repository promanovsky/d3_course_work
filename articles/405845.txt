LinkedIn Corp. (LNKD) reported second quarter adjusted EPS of $0.51 after the close Thursday, up from $0.38 last year. Analysts expected EPS of $0.39. Revenues rose 47% to $534 million from $364 million in the same quarter last year. The consensus estimate was for revenues of $510.98 million.

Third quarter adjusted EPS is expected to be about $0.44, compared to the consensus estimate of $0.40. The company expects to report full year EPS of about $1.80, which is above analysts' expectations of $1.64. The stock is now up 13.85 on 1.4 million shares.

LinkedIn declined until mid-morning Thursday and ended the session with a loss of 6.65 at $180.64 on above average volume.

For comments and feedback contact: editorial@rttnews.com

Business News