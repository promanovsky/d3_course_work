Ryan Gosling, a cast member in the motion picture political drama "The Ides of March", attends the premiere of the film at the Academy of Motion Picture Arts & Sciences in Beverly Hills, California, on September 27, 2011. UPI/Jim Ruymen | License Photo

Ryan Gosling, a cast member in the motion picture crime drama "Gangster Squad", attends the premiere of the film with his mother Donna Gosling, at Grauman's Chinese Theatre in the Hollywood section of Los Angeles on January 7, 2013. UPI/Jim Ruymen | License Photo

(From L to R) Reda Kateb, Matt Smith, Iain De Caestecker, Christina Hendricks and Ryan Gosling arrive at a photo call for the film "Lost River" during the 67th annual Cannes International Film Festival in Cannes, France on May 20, 2014. UPI/David Silpa | License Photo

Ryan Gosling (L) and Christina Hendricks arrive at a photo call for the film "Lost River" during the 67th annual Cannes International Film Festival in Cannes, France on May 20, 2014. UPI/David Silpa | License Photo

Ryan Gosling arrives at a photo call for the film "Lost River" during the 67th annual Cannes International Film Festival in Cannes, France on May 20, 2014. UPI/David Silpa | License Photo

CANNES, France, May 20 (UPI) -- Ryan Gosling's Lost River received polarizing reviews Tuesday at Cannes.

The film marks the 33-year-old actor's directorial debut, and his effort was met with a chorus of loud boos that reportedly drowned out the applause.

Advertisement

Lost River stars Christina Hendricks as a single mother named Billy who takes a job at a nightmarish burlesque theater in order to pay her bills. Meanwhile, her son, Bones (Iain De Caestecker), befriends a girl named Rat (Saoirse Ronan) after he discovers a road to an underwater town.

Gosling touts his film as having a "surreal" and "dark fairy tale quality" on the Cannes website. He intended the film to follow an "arc from dream to nightmare" as the characters live "in a reality that bordered on fantasy."

Positive responses from critics focused on the movie's visuals and imagery. Many comparisons were made to directors David Lynch and Nicolas Winding Refn, who directed Gosling in Drive and Only God Forgives.

NW Refn taught his padawan Ryan Gosling well. If you like that sort of thing you'll like LOST RIVER. Personally, I'm mixed. #cannes2014 — Jordan Hoffman (@jhoffman) May 20, 2014

Who knew? His directorial debut Lost River suggests inside Ryan Gosling a new David Lynch has been waiting to get out. — Gregg Kilday (@gkilday) May 20, 2014

Yeah, LOST RIVER's inchoate and indulgent, but at least Ryan Gosling's interested in visuals and cribbing from edgier filmmakers. #cannes — Alison Willmore (@alisonwillmore) May 20, 2014

Others responses were less complimentary.

If a $200 haircut and $900 shades were given lots of money to defecate on Detroit, the result would be Ryan Gosling's directing debut. — Wesley Morris (@Wesley_Morris) May 20, 2014

Lost River: Ryan Gosling confuses 'making film' with 'assembling Tumblr of David Lynch & Mario Bava gifs'. Dumb-foundingly poor #Cannes2014 — Robbie Collin (@robbiereviews) May 20, 2014

Ryan Gosling's directing debut: let's see God forgive this... — Jonathan Romney (@JonathanRomney) May 20, 2014

A clip from the movie was released shortly after its debut. Gosling will next appear in an as-of-yet untitled Terrence Malick film.