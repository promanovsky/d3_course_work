Maya Rudolph has proven time and again that she can sing and make people laugh out loud. Now the question is whether she can breathe new life into a TV staple once thought part of the cultural flotsam of the 1970s.

She will appear on NBC Monday in “The Maya Rudolph Show,” a 10 p.m. special the Peacock bills as a means of “revisiting the classic variety show format.” No less than “Saturday Night Live” impresario Lorne Michaels is the executive producer. Andy Samberg, Kristen Bell, and Fred Armisen are among the guests. Janelle Monae will sing. There’s even a bandleader – retro-soul artist Raphael Saadiq.

“What we found and what I feel emboldened us is there was no shortfall of people that wanted to put their hand up to what I’d describe as ‘play with Maya,’” said Paul Telegdy, president of alternative and latenight programming at NBC Entertainment. Part of the appeal of the show, he said, is getting to see celebrities who have unique skills and don’t often have a venue to put them on display.

As a star in “Bridesmaids” and a former stalwart of “Saturday Night Live,” Rudolph is usually mentioned in the same breath as other modern comediennes like Tina Fey, Amy Poehler or Sarah Silverman. Come Monday, however, she will join the ranks of a different group of entertainers that includes Sonny and Cher (“The Sonny & Cher Comedy Hour,” 1971 to 1974), Donny and Marie Osmond (“Donny & Marie,” 1976 to 1979); the cast of “The Brady Bunch” (“The Brady Bunch Hour,” 1976 to 1977); and comedian Jeff Altman and the Japanese musical duo Pink Lady (“Pink Lady,” 1980). Every member of this diverse list was at the center of a variety program in TV’s past.







The format, once a couch potato staple, has generally faded from the nation’s primetime schedule, but there may be a move to bring it back. Just this week, actor Neal Patrick Harris told Howard Stern he had pitched the idea of a weekly variety program to no less than Leslie Moonves, the chief executive of CBS Corp. And Rudolph’s effort isn’t the first time NBC has tested such a notion. In 2008, the network aired “Rosie Live,” an effort in a similar vein featuring Rosie O’Donnell along with Conan O’Brien, Gloria Estefan, Liza Minnelli and tap dancers the Lombard Twins (The ratings were not robust)..

In many ways, the concept is a relic of a time when fewer TV networks had less competition and the airwaves weren’t glutted with programs like “America’s Got Talent” and “American Idol” that give viewers plenty of song, dance and even the esoteric act of targeted plunger throwing. Even Michaels’ “Saturday Night Live” has in recent years put more song-and-dance routines into its opening monologue (just last week, guest host Charlize Theron tried a musical number about how she couldn’t carry a tune).

And yet, TV networks are looking to surround their traditional primetime series with limited-run, special programming that surprises and intrigues viewers. After finding success with a live airing of “The Sound of Music” this season, NBC may feel music, comedy and celebrity make for an attractive mix. The network has plans for live telecasts of “Peter Pan” and “The Music Man.” Even Fox is getting in on the action, having announced plans to telecast a version of “Grease.”

Rudolph’s show “is something that you would intend to make many more of,” said Telegdy. “The number of things we approach as one and done is extremely limited.” The network could turn the show into a limited-run series or a string of specials, he said, noting that NBC will watch its Monday-night performance “very much with an eye on the future.” That said, the network made no mention of the program this week during an “upfront” presentation to advertisers detailing its plans for the year ahead.

The variety show of the 1970s courted a broad audience that wanted to tune in to see a certain celebrity sing or juggle. In 2014, viewers know how to get just what they want – say, a collaboration between two singer-songwriters, or a clever dance routine – rather than sit through an hour-long collection of different segments.

The genre is “rooted in the mass appeal broad-strokes radio-to-prime-time family entertainment that marked TV’s early years,” said Jim McKairnes, a former CBS and Discovery executive who teaches about the TV business at Philadelphia’s Temple University. “Major stars hosted, told jokes, sang. Guest stars made appearances, likewise to sing or tell jokes or to play in sandboxes of silliness that their day-jobs seldom allowed. It was all … unusual. In a good way.”

But as the years have progressed, “unusual became the usual,” he added. “And TV itself was turning into one giant variety show of its own. No need for a specific show — just spin the dial.”

Today’s audiences would likely be bemused by an hour of one of the older programs. “Donny & Marie,” for example, typically opened with an ice-skating segment, moved on to some comedy skits, and was highlighted by a musical number in which Marie Osmond sang about how she was :”a little bit country” while her brother crooned about being “a little bit rock and roll.”

Rudolph’s show was conceived with a modern viewer in mind, said Telegdy. “You have to be far more conscious of formats and conscious of attention-getting,” he said. “The execution of something like this in 2014 is radically different.” He declined to detail moments from the show, which taped in the last few weeks. “There are surprises for everyone, and also a lot of fun,” he said.

New technology could revive interest in the format, suggested Deborah Jaramillo, an assistant professor of TV and film studies at Boston University. The show essentially “exists as segments that can go viral,’ she said, which could draw attention from the younger viewers TV networks and their advertisers crave.

“I don’t think any genre should be counted out on TV,” she said. “Genres wax and wane, and maybe Maya Rudolph has the star power and the intelligence to make variety relevant in today’s landscape.“