The Reserve Bank of India (RBI) has recommended the Centre reduce its holdings in public sector This, it says, is essential for robust corporate governance practices, considering these lenders’ deteriorating asset quality and rating downgrades.

The recommendation is part of a detailed blueprint for sweeping reforms in public sector banks, prepared by the central bank. The recommendations are being discussed with the ministry and will be taken up with the next government on a priority basis.

Stressing the importance of granting complete autonomy in the day-to-day operations of public sector so that these could effectively compete with their private sector peers, RBI said such autonomy was contingent upon the government reducing its stake, especially at a time when the Centre was resorting to borrowing for capital infusion in these





REFORM AREAS Governance issue: Splitting chairman & managing directors’ post

Corporate governance: RBI & govt to dissociate from selection of top management

Public sector bank board: RBI & govt should withdraw from boards

Auditors: CA director should not be a part of management committee

Fixed period: Chief executives should be given 5-year terms

Remuneration: Market-based remuneration, accompanied with stringer accountability, including clawback clauses

Incentive structure: Incentives/bonuses should be paid over few years and not immediately

HR issues: Need to reassess number of EDs/general managers in each bank

HR appraisal policy: Performance linked remuneration

Holding company: To begin with, such structure could be experimented with slammer PSBs

The banking regulator has identified two areas which it terms the cause of all problems facing public sector banks — government ownership, which creates a constraint in efficient operations of banks, and successive regulatory and supervisory forbearance to government banks, a factor behind their poor performance.

Under the Banking Regulation Act, the government has to hold at least 51 per cent stake in public sector banks. However, in several cases, the stake is as much as 80 per cent.

RBI has also suggested the government and the regulator don’t involve themselves in the process of appointing the senior management at these banks, and also withdraw from the boards of public sector banks. It feels the posts of chairmen and managing directors should be split, as a chairman and managing director has absolute power and often dominates the board.

It also says the chief executive should have a fixed term of five years. The regulator has suggested a host of reforms for various issues — from accountability in loan sanctioning to human resource management and board accountability. It has said lateral movement of staff at public sector banks should start at an early stage — at the level of deputy general manager and general manager. This would ensure a candidate was made aware of the bank he or she might head in the future, it said, adding lateral movement should also be considered for State Bank of India (SBI).

It said the SBI group was more vulnerable in that the bank’s senior management was selected internally, which inhibited new thinking.

The RBI paper also raised the issue of auditors’ accountability, saying chartered accountant director should not be part of the management committee of the board that took decisions on loan sanctioning, as this led to conflict of interest.

The existing committee method of sanctioning loans should be done away with, as this didn’t hold any single individual accountable, the central bank said.

In case of frauds, banks’ boards and senior managements should be held accountable and a branch-level official should not be made scapegoat, it added.