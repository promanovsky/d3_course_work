Just last month, Sen. Charles Schumer (D-N.Y.) warned that the United States may see a rise in cases of chikungunya and urged health authorities to issue an alert that would facilitate the identification and treatment of the disease.

Chikungunya, a disease transmitted by mosquitoes to humans, seldom occurs in the U.S. as mosquitoes that carry the virus that cause the disease are not commonly found in the continental U.S. Individuals who contract the illness are often those who were bitten by infected mosquitoes while traveling abroad such as in Brazil, Africa and Asia, where chikungunya is prevalent. Cases of the disease were also recently identified in the Caribbean islands early this year.

The mosquito-borne disease, which is marked by fever, muscle pain, rash, joint pain and inflammation, is not fatal in most cases but its symptoms can be long-lasting, depending on the patient's age. Younger patients are more likely to recover faster than older patients. It is an arbovirus, the same type of disease as the West Nile virus spread by mosquitoes.

It appears that it pays to take heed of the senator's warning, as health officials have just confirmed new cases of the chikungunya virus.

A Boston Public Health Commission official confirmed on Saturday that there were four individuals in the Boston area who were diagnosed with chikungunya. The four individuals travelled to the Caribbean islands recently where they likely contracted the disease.

The Boston Public Health Commission has already sent an alert advisory to health service providers so they will be aware of the symptoms, as the disease does not typically occur in Boston. Health officials, however, assured that there is no public health threat because the disease does not spread between humans.

The chikungunya virus is transmitted to people by the same mosquitoes responsible for transmitting dengue fever, the Aedes aegypti and Aedes albopictus mosquitoes. While the virus could theoretically be transmitted through blood transfusion, no such case has yet been reported.

The Centers for Disease Control and Prevention (CDC) said that following recent chikungunya outbreaks in the Caribbean and the Pacific, there will likely be an increase in chikungunya cases in the U.S.

"With the recent outbreaks in the Caribbean and the Pacific, the number of chikungunya cases among travelers visiting or returning to the United States from affected areas will likely increase," the CDC said. "These imported cases could result in local spread of the virus in the continental United States."

And while transmission of the chikungunya virus doesn't yet occur in the United States, the CDC and the Pan American Health Organization say the risk for its introduction into local vector mosquito populations is higher than had previously been thought, especially in tropical and subtropical areas where the Ae. aegypti mosquito is common.

That, combined with the general population's lack of exposure to the virus, could raise the risk factors for the introduction and spread of the virus. The agencies have produced guidelines (PDF) to deal with the spread of the virus in the Americas.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.