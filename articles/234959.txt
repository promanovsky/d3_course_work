China and Russia signed a $400 billion gas supply deal on Wednesday, securing the world's top energy user a major source of cleaner fuel and opening up a new market for Moscow as it risks losing European customers over the Ukraine crisis, according to the Associated Press.

The long-awaited agreement is a political triumph for Russian President Vladimir Putin, who is courting partners in Asia as those in Europe and the United States seek to isolate him over Moscow's annexation of Ukraine's Crimea peninsula, the AP reported.

Commercially, much depends on the price and other terms of the contract, which has been more than a decade in the making, according to the AP.

China had the upper hand as talks entered the home stretch, aware of Putin's face-off with the West, the AP reported.

Both sides could take positives from a deal that will directly link Russia's huge gas fields to Asia's booming market for the first time - via thousands of miles of new pipeline across Siberia that form part of the package, according to the AP.

Putin and Chinese counterpart Xi Jinping applauded as they witnessed the deal being signed before the Russian leader was to leave Shanghai at the end of a two-day visit, the AP reported.

Putin loyalist and senior parliamentarian Alexei Pushkov, who was included on a U.S. list people placed under sanctions imposed after the crisis in Ukraine, said the gas deal showed Russia could not be isolated, according to the AP.

In Washington, State Department spokeswoman Jen Psaki said the gas deal would not undermine efforts by the United States to pressure Moscow not to meddle in Ukraine's affairs, the AP reported.

The United States and European countries have warned the Kremlin that it will face broader sanctions, possibly involving new investment in key areas of the Russian economy, if it meddles in Ukraine's elections later this month, according to the AP.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.