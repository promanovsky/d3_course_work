(PARIS-AFP) - Scientists on Sunday said they had used a harmless electrical current to modify sleep so that an individual has "lucid dreams," a particularly powerful form of dreaming.

The discovery provides insights into the mechanism of dreaming -- an area that has fascinated thinkers for millennia -- and may one day help treat mental illness and post-trauma nightmares, they said.

Lucid dreams are considered by many psychologists to be an intermediate stage between two forms of consciousness.

They lie between so-called rapid eye movement (REM) dreams -- which are concerned with the immediate present and have no access to past memories or anticipated events in the future -- and being awake, which brings into play abstract thought and other cognitive functions.

In lucid dreaming, a state believed to be unique to humans, elements of secondary consciousness combine with REM dreams.

A characteristic is that the dreamer becomes aware that he or she is dreaming and is sometimes able to control the dream's plot.

They may dream, for instance, of putting an aggressor to flight or of averting a catastrophic accident.

Researchers led by Ursula Voss at the J.W. Goethe University Frankfurt, used a technique called transcranial alternating current stimulation (tACS) to explore the causes of lucid dreaming.

The gadget comprises two small boxes with electrodes that are placed next to the skull and send a very weak, low-frequency electrical signal across the brain.

The team recruited 15 women and 12 men aged 18 to 26, who spent up to four nights in a sleep laboratory.

After the volunteers had experienced between two and three minutes of REM sleep, the scientists applied tACS, or a "sham" procedure that produced no current, for around 30 seconds. The current was below the sensory threshold, so that the subjects did not wake up.

They then woke up the volunteers and asked them what they had been dreaming.

In control of the dream

"The dream reports were similar, in that most subjects reported to 'see myself from the outside' and the dream was watched from the outside, as if it was displayed on a screen," Voss told AFP.

"Also, they often reported to know that they were dreaming."

The volunteers were tested at frequencies of two hertz (Hz), six Hz, 12 Hz, 25 Hz, 60 Hz and 100 Hz.

"The effect... was only observed for 25 and 40 Hz, both frequencies in the lower gamma frequency band," Voss said.

"This band has linked with conscious awareness, but a causal relationship had so far not been established. Now it is."

When the volunteers were stimulated with 25 Hz, "we had increased ratings for control of the dream plot, meaning they were able to change the action at will," she added

The study, reported in the journal Nature Neuroscience, gave several anecdotes from the recruits about what they had dreamt.

"I am driving in my car, for a long time," said one. "Then I arrive at this place where I haven't been before. And there are a lot of people there. I think maybe I know some of them but they are all in a bad mood, so I go to a separate room, all by myself."

The battery-operated tACS was applied so that the current flowed between the frontal and temporal regions, located on the forward top and side of the brain respectively.

The study suggests that frontotemporal tACS might help to restore dysfunctional brain networks which are fingered in schizophrenia and obsessive-compulsive disorder.

Applied during REM sleep, it could also one day help victims of post-traumatic stress disorder to overcome recurrent nightmares by placing them in charge of the dream plot, the paper theorises.

The tACS gadget itself is a recognised medical invention designed to be used only for research purposes.

Voss said, though, that it seemed inevitable that a similar device would one day be invented for consumers, enabling sleepers to latch onto lucid dreaming, for better or worse.

"Although this is not something I am personally interested in, I am certain that it won't take long until such devices come out. However, brain stimulation should always be carefully monitored by a physician," she cautioned.