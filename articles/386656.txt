Researchers say they've found a way to transform ordinary pig heart muscle cells into a "biological pacemaker," a feat that might one day lead to the replacement of electronic pacemakers in humans.

"Rather than having to undergo implantation with a metallic device that needs to be replaced regularly and can fail or become infected, patients may someday be able to undergo a single gene injection and be cured of slow heart rhythm forever," said senior study author Dr. Eugenio Cingolani, director of the Cedars-Sinai Heart Institute's Cardiogenetics-Familial Arrhythmia Clinic, in Los Angeles.

Using gene therapy, the researchers altered a peppercorn-sized area in the heart muscle of pigs to create a new "sino-atrial node" -- the bundle of neurons that normally serves as the heart's natural pacemaker.

The technique kept alive a handful of pigs suffering from complete heart block, a condition in which the heart beats very slowly or not at all due to problems in the heart's electrical system.

The biological pacemaker also appeared to function as well as an original sino-atrial node and better than typical electronic pacemakers, said study co-author Dr. Eduardo Marban, director of the Cedars-Sinai Heart Institute, in Los Angeles.

"When we exercise, our hearts go faster. When we rest, our hearts slow down," Marban said. "The pigs with the biological pacemaker faithfully reproduced these responses, which were absent in 'control' pigs that had been treated only with an electronic pacemaker."

About 300,000 electronic pacemakers are placed in humans in the United States each year, at an annual cost of $8 billion, Marban said. They work by sending electrical pulses to the heart if it is beating too slowly or if it misses a beat.

The key to the new procedure is a gene called TBX18, which converts ordinary heart cells into specialized sino-atrial node cells, Marban said.

The heart's sino-atrial node initiates the heart beat like a metronome, using electric impulses to time the contractions that send blood flowing through people's arteries and veins, the scientists explained. People with abnormal heart rhythms suffer from a defective sino-atrial node.

Researchers injected the gene into a very small area of the pumping chambers of pigs' hearts. The gene transformed the heart cells into a new pacemaker.

"In essence, we create a new sino-atrial node in a part of the heart that ordinarily spreads the impulse, but does not originate it," Marban said. "The newly created node then takes over as the functional pacemaker bypassing the need for implanted electronics and hardware."

Pigs were used in the research because their hearts are very similar in size and shape to those of humans, said lead study author Dr. Yu-Feng Hu, a fellow at the Cedars-Sinai Heart Institute.

Within two days of receiving the gene injection, pigs had significantly stronger heartbeats than pigs that did not receive the gene. The effect persisted for the duration of the 14-day study. Toward the end of the two weeks, the treated pigs' heart rates began to falter somewhat, but remained stronger than that of the pigs who did not receive the gene injection.

The research team hopes to advance to human trials within three years, Cingolani said. However, results from animal trials often can't be duplicated in humans.

If the approach does work in humans, one expert said biological pacemakers could have several uses.

They could be used as a "bridge" to help patients whose electronic pacemaker has to be removed or replaced, said Dr. David Friedman, chief of heart failure services at North Shore-LIJ's Franklin Hospital in Valley Stream, N.Y.

"Many people with electronic pacemakers that help maintain normal heart rhythm experience short periods when their pacemaker is compromised due to infection or other problems that render it non-functional," Friedman explained. "This gene therapy may someday fill that gap, expanding the arsenal of currently experimental gene and stem cell therapies that symbolize the 'holy grail' of treatments for a wide array of heart conditions."

Cingolani said the therapy could also be used to treat fetuses with congenital heart block, who cannot receive a traditional pacemaker because they are still in the womb. This condition affects one out of every 20,000 fetuses.