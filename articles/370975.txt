A new study claims that Americans’ sedentary lifestyles are to blame for the recent obesity epidemic in the U.S. – not their overconsumption of calories, Medical News Today reported.

Looking at 20 years’ worth of data from the U.S. National Health and Nutrition Examination Survey (NHANES), researchers at Stanford University found that calorie consumption has remained steady, while there has been a sharp decline in leisure-time physical activity levels, accompanied by an increase in average body mass index (BMI).

In 2010, the last year data was collected, 51.7 percent of adult women reported no leisure-time physical activity, compared to 19.1 percent of adult women who reported the same in 1994. For adult men, 43.5 percent reported no activity in 2010, compared to 11.4 percent in 1994.

The team found that black and Mexican-American women, in particular, showed the greatest decreases in reported exercise.

Across the same time period, the average BMI rose by .37 percent annually, with the largest rise occurring in young women. The average waist size went up .37 percent for women and .27 percent for men.

While the researchers were able to calculate daily nutrition information, they found that intake of calories, fat, carbohydrates and protein did not change significantly over the study period – contradicting the common belief that increased calorie consumption has caused the obesity epidemic.

"Our findings do not support the popular notion that the increase of obesity in the United States can be attributed primarily to sustained increase over time in the average daily caloric intake of Americans,” said lead author Uri Ladabaum, an associate professor of medicine at Stanford University School of Medicine.

The study was published in The American Journal of Medicine.

Click for more from Medical News Today.