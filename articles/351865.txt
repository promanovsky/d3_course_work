Katherine Heigl has admitted to Marie Claire that she couldn't stop agreeing to star in romantic comedies.

Appearing in the magazine's August issue, the actress said she "stopped challenging" herself in her film roles.

She said: "I had an amazing time [doing rom-coms]. I love romantic comedies. But maybe I hit it a little too hard. I couldn't say no.

"I stopped challenging myself. It became a bit by rote and, as a creative person, that can wear you down.

"That was part of why I took that time off, to ask myself, 'What do I want? What am I looking for?' and shut down all the noise."

Marie Claire UK/Alex Cayley

Heigl also spoke out about taking time away from Hollywood and going to a ranch in Utah.

She said: "Oh yeah, I had a moment where, I don't know, I was thinking, 'Maybe open a knitting store, get my money out of retirement accounts and live off that, live off the land'.

"I had my moment where it all seemed so complicated and all I wanted to do was simplify."

To read the feature in full, see the August issue of Marie Claire, out today.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io