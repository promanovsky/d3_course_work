London: Gold hit its highest in nearly three weeks on Monday, as turmoil in Iraq and Ukraine supported its safe-haven appeal relative to higher-risk assets like equities, while platinum gained ahead of the outcome of talks to end a strike in South African mines.

Investors often turn to gold or other precious metals in times of political or financial trouble, as they can be perceived as an insurance against risk.

Spot gold rose 0.3 per cent to $1,280.00 an ounce by 1004 GMT, having hit its highest since May 27 at $1,284.85 earlier in the session.

US gold futures for August delivery gained 0.5 per cent to $1,280.90.

“Escalating violence in Iraq is going to be bullish for gold and technically we could see another leg up pushing gold above $1,300,” Societe Generale analyst Robin Bhar said.

“But safe-haven buying could only last until you have to sell any profitable positions to make up margins for other markets.” Sunni insurgents seized a mainly ethnic Turkmen city in northwestern Iraq on Sunday after heavy fighting, solidifying their grip on the north after a lightning offensive that threatens to dismember Iraq.

Ukraine was also in the spotlight after pro-Russian separatists shot down a Ukrainian army transport plane, killing all 49 military personnel on board.

Gold also gained some support from rising crude oil prices, with Brent trading just below a nine-month high hit on Friday, on fears the of supply disruptions from the second-largest Opec producer Iraq.

Gold is usually seen as an hedge against oil-led inflation.

European shares fell, while the dollar gained 0.1 per cent against a basket of currencies.

Data from the Commodity Futures Trading Commission on Friday showed that investor sentiment towards gold was turning positive. Hedge funds and money managers increased their bullish bets in gold futures and options in the week to June 10, their first increase in five weeks.

But traders also warned that this week’s Federal Reserve policy meeting could bring caution to any rally in gold.

The Fed will conclude a policy meeting on Wednesday, with markets watching out for any signals on when the US central bank might begin raising interest rates.

Platinum gains

Among other precious metals, platinum rose after falling sharply last week on expectations that the strike in South Africa might be close to a settlement.

The leader of South Africa’s AMCU union said on Friday a wage deal with the top three platinum producers was imminent.

Platinum rose 0.8 per cent an ounce to $1,435.50. It lost almost 4 per cent in past two sessions on signs that a five-month strike crippling 40 per cent of platinum production could finally come to an end.

Palladium was up 0.1 per cent per ounce to $816.00, having touched its lowest since May 16 on Friday. The metal had rallied to a 13-and-a-half year high earlier last week, also underpinned by strong demand from the auto sector, which accounts for more 70 per cent of total offtake.

“The prospects of an end to the five-month-long industry strike triggered heavy longs liquidation — especially in palladium,” VTB Capital said in a note.

“We could see a little more downside from here in both metals,” it said. “However, questions remain over the long-term damage done to the industry, the size of depletion of stockpiles, which will need to be replenished, as well as the speed at which returning workers can re-ramp up production capacity.” Silver rose 0.3 per cent to $19.72 an ounce.