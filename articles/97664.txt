Samsung has been sued by Apple several times over claims that the company’s Galaxy tablets have stolen the iPad’s design identity. Back in 2011, one judge even illustrated how similar Samsung’s slates were to the iPad. She held a Galaxy Tab 10.1 and an iPad above her head and asked Samsung’s own attorney which one was which. In a painful bit of irony, Samsung’s counsel could not distinguish them.

One thing is certain, however: No one will mistake a Samsung tablet with a big hole in the corner for an iPad.

In a design patent application filed back in 2012 that was just made public earlier this week, Samsung sought to protect its “ornamental design for a tablet computer, as shown and described.” Here’s what it looks like:

Why is there a hole in this tablet? We’re not quite sure, and neither is SamMobile, which pointed us to the patent.

Two more images from Samsung’s filing follow below.