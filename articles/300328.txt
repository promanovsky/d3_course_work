Kanye West is currently in Cannes, which means he is currently talking about Kim Kardashian in Cannes. During his appearance at Translation's seminar, "Technology, Culture, and Consumer Adoption: Learning to Read the Cultural Landscape," on Tuesday, June 17, the rapper spoke about a few things Kardashian-related, including how turned on he is by her.

"I have to work with the No. 1," he said, per AdWeek, while discussing his affinity for Apple over Samsung. "I can't work with anyone but Jay Z, because he's No. 1. I can't be with any girl but Kim, because that's the girl whose pictures I look at the most and get turned on by. I'm not going to represent any company but Louis Vuitton, because that's No. 1."

His quip about his reality-star bride probably comes as no surprise. He has called her one of the most beautiful woman in all of "human existence" and isn't shy about tweeting his excitement over her risque Instagram photos.

Speaking of Kardashian's Instagram photos, West admitted Tuesday that he spent "like four days" digitally editing their wedding kiss photo -- which shows the newlyweds embracing against a backdrop of flowers atop the Forte di Belvedere in Florence, Italy -- to get the coloring right before Kardashian could post it to her account.

A post shared by Kim Kardashian West (@kimkardashian) on May 27, 2014 at 7:58am PDT

"This is pissing my girl off during the honeymoon," he said. "Annie Leibovitz pulled out right before the wedding -- maybe she was scared about the idea of celebrity … But I still wanted my wedding photos to look like Annie Leibovitz. Now, can you imagine telling someone who just wants to Instagram a photo, the No. 1 person on Instagram, that we need to work on the color of the flowered wall? But the fact that the No. 1 most liked photo has this certain aesthetic on it was a win for what the mission is -- of raising the palette."

The romantic snapshot wound up breaking Instagram's record as the most-liked photo ever with 2.2 million likes and counting.