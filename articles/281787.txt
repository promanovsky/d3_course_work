Hooking up is getting easier for everyone, men and women, thanks to online dating sites, social networks and smartphone apps that help people meet up on the moment or find a new mate. But it's not without some drawbacks.

A new research report claims gay men who use such apps may face a higher risk for sexually transmitted disease than counterparts who meet new friends in traditional settings such as bars or clubs or even online dating sites.

A study, involving both bisexual and gay men, reveals those who used smartphone apps to meet partners were 23 percent more likely to contract gonorrhea, and 35 percent were more likely to be diagnosed with chlamydia compared with gay men who found new friends in in-person settings such as clubs and bars.

One reason, say researchers, is that such apps tap GPS navigation and provide a faster and quicker way to meet new people; this leads to more sexual encounters.

"Technological advances which improve the efficiency of meeting anonymous sexual partners may have the unintended effect of creating networks of individuals where users may be more likely to have sexually transmissible infections," states the study conducted by the L.A. Gay & Lesbian Center in Los Angeles. The research paper was published in the journal Sexually Transmitted Infections.

But there is a silver lining to the statistic, note researchers, as the same type of technology can be used to help people find STD testing facilities and support resources.

"Technology is redefining sex on demand -- prevention programs must learn how to effectively exploit the same technology," the researchers said.

The research effort focused on 7,100 participants who were tested for STDs between 2011 and 2013. The participants completed surveys of social network tech use for meeting new friends.

The study reports that one-third met new partners in real-life venues, while another 30 percent met friends using online and real-life social settings. The final 36% met partners using the smartphone apps and other resources.

The research noted no relation was found between HIV and syphilis with the use of smartphone social meeting apps.

"We want to make people aware of the risks and benefits with any new technology," lead study author Matthew Beymer said. "We just want gay and bisexual men to love safely and love carefully."

TAG Gay, Sex, Software, Smartphone, Apps, STDs

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.