It's 25 years to the day since a computer scientist invented a system of interlinked hypertext documents accessed via the internet — or, the world wide web, or, the way that we now work, play, talk, share, search and find.

To celebrate an invention that has quite literally changed lives, here are 25 firsts from the first 25 years of the web.

1989: British scientist Tim Berners-Lee submits a proposal to his bosses at CERN, the European Organization for Nuclear Research, making the case for a “‘web’ of notes with links (like references) between them” to help researchers access and share data more easily.

1990: The world’s first website, a text-only effort describing the World Wide Web or “W3” browser, goes live on the world’s first server. It was run on Tim Berners-Lee’s NeXT computer at CERN, which is now on display at the organization’s museum.

1991: Scientists at Cambridge University build a prototype webcam. Taking three shots per minute, it is trained on the computer lab coffee pot to allow academics anywhere in the building to see when a fresh batch has been brewed.

1992: Tim Berners-Lee and his colleagues upload what was arguably the first photo to appear on the web, a scanned .gif version of a shot of CERN’s house band, Les Horribles Cernettes. The idea was to prove that the web didn’t have to be about just physics.

1993: CERN puts its World Wide Web software into the public domain, granting permission for anyone to “use, duplicate, modify and redistribute it.” It was the beginning of mass W3 development.

1993: Swiss developer Oscar Nierstrasz builds one of the earliest web search engines, W3 Catalog.

1994: WebCrawler becomes the first search engine to enable full-text searches, allowing users to look for keywords anywhere on any webpage.

1994: One of the first known online purchases takes place. It’s an order for a large pepperoni and mushroom pizza — with extra cheese — from Pizza Hut’s “electronic storefront,” PizzaNet.

1994: The White House gets its first website, an “interactive citizens’ handbook” that allows users to find the latest presidential statements and browse pictures of the First Family. Bill Clinton and Al Gore are the first POTUS and VPOTUS respectively to have official email addresses: president@whitehouse.gov and vice-president@whitehouse.gov.

1994: The first banner ad appears. Featured on HotWired.com and promoting AT&T’s telecoms services, it reads: “Have you ever clicked your mouse right HERE? You will.”

1994: Twenty-year-old American student Justin Hall is credited with creating the world’s first internet-based diary (a blog, if you will, though the term had yet to be invented). He would maintain it for the next 11 years.

1995: Craigslist, the brainchild of “self-described nerd” Craig Newmark, begins listing San Francisco-area events online. It would later become one of the world’s most visited — and often, weirdest — websites.

1995: The first sales take place on AuctionWeb — or, as we know it now, eBay. They include a broken laser pointer (sold for $14.83), autographed Marky Mark underwear ($400) and a Superman lunchbox ($22).

1995: The world gets its first dating website, the “interactive digital personals service” that is Match.com.

1995: Windows releases its first browser, Internet Explorer 1.0, setting the stage for the so-called “Browser Wars.”

1996: The launch of Hotmail — or as it was then, HoTMaiL — one of the world’s first web-based messaging services to bring free email to the masses. Within 18 months it has several million customers.

1996: Release of ‘Dancing Baby,’ a 3D animation of a, er, dancing baby that will explode on forums, websites and email to become one of the world’s first viral videos.

1997: Entrepreneurs Larry Page and Sergey Brin register the domain Google.com, with the aim to “to organize a seemingly infinite amount of information on the web.”

1997: American programmer Jorn Barger coins the term “weblog” to describe a record, or log, of his online reading. The term is transformed into “we blog” and then simply “blog” by online author Peter Merholz in 1999. From there, it’s a short step to the verb “to blog” and the noun “blogger.”

1998: An odd phrase in the English version of a Japanese arcade game gives rise to one of the earliest internet memes. What’s more, “All your base are belong to us” goes on to inspires thousands of image-edited versions in the early 2000s, making it one of the first examples of the Photoshop phenomenon.

1999: One of the first major scandals to be caused by an online leak ensues when the names of British intelligence agents appear on a US-based website. The case sends the UK government into a flurry as it attempts to stop the list circulating, demonstrating how difficult it is for anyone — even authorities — to prevent the spread of information online (hello, Wikileaks!).

2000: Some of the world’s most-used websites, including eBay, Amazon and Yahoo, are hit by the biggest denial of service attack to date. The culprit turns out to be a Canadian teenager who goes by the alias “MafiaBoy.”

2004: Launch of “the Facebook,” designed by Harvard University students. More than 1,000 undergraduates sign up within the first 24 hours. The rest is timeline.

)

2012: Several of the world’s favorite websites join what’s billed as the largest online protest in history against two proposed US bills that the government says will protect copyright, but that critics call censorship. The legislation is shelved indefinitely.

2014: As the web turns a quarter of a century, founding father Tim Berners-Lee calls for an internet bill of rights to protect web users’ privacy and freedom of speech. “Our rights are being infringed more and more on every side, and the danger is that we get used to it,” he warns. “So I want to use the 25th anniversary for us all to do that, to take the web back into our own hands and define the web we want for the next 25 years.”