Net neutrality proponents are getting unlikely backers after some of the largest names have joined together in urging the Federal Communications Commission (FCC) to drop its idea of "fast lanes" that many experts say would spell the end of net neutrality as it currently exists. Amazon.com, Google, Facebook and others banded together in a letter to the FCC that claims the changes being proposed would be the end of the Internet as it exists.

Net neutrality has become a vital and now controversial issue for the tech and Internet world after the FCC would bring forward a new set of regulations governing the Internet and ISPs. It would also potentially allow large companies to pay fees to the FCC in order to allow their content to be uploaded and viewed at a faster speed.

An appeals court has struck down the regulations on ISPs, but the fast lanes continue to be a tipping point in the debate over net neutrality.

A vote on the proposal is scheduled for May 15.

The FCC Chairman Tom Wheeler says he would fight back against Internet abuse and was looking into greater oversight in order to maintain standards of net neutrality.

"It is an experience that made me especially wary of the power of closed networks to innovate on their own agenda to the detriment of small entrepreneurs," Wheeler wrote in a letter to the large tech giants.

But the tech giants are continuing to put pressure on the FCC to get it right and not be the death of net neutrality as we know it today.

The letter says FCC rules should not permit "individualized bargaining and discrimination," the companies said.

"[The FCC must] take the necessary steps to ensure that the internet remains an open platform for speech and commerce," the letter says.

Many industry experts believe the FCC's cozying up with the cable and Internet television networks has led to the agency seeing it as an opportunity to bring in much needed funding to the agency in its efforts to revamp how they currently do business and function.

Those ties to the cable industry have led to the continuation of fears that the FCC is attempting to hijack the Internet for those who can afford to pay to have their content uploaded quicker, which would limit competition and hurt smaller organizations or businesses.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.