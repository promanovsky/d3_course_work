* Death toll from suspected Ebola cases rises to 63

* Only 13 cases have been confirmed by laboratory tests

* Minister says Guinea has halted spread of the disease

* Suspected cases detected in Liberia, Sierra Leone (Adds comment from bus driver, details on fear in Conakry)

By Saliou Samb

CONAKRY, March 26 (Reuters) - Guinea said on Wednesday it had stopped an outbreak of deadly Ebola fever from spreading beyond the country's remote southeast, although the number of deaths from suspected infections rose to at least 63.

U.N. agencies and medical charities such as Doctors Without Borders (MSF) have scrambled to help Guinea - one of the world's poorest countries - to cope with the virus, amid fears it might spill across borders into neighbouring West African nations.

Liberia, which shares a border with southeastern Guinea, reported five deaths this week from suspected infections in people who had come across the frontier to seek treatment.

Sierra Leone has also uncovered two deaths in the border town of Boidu suspected to be linked to Ebola, one of the most lethal infectious diseases known to man.

Laboratories have so far only confirmed 13 cases of the disease out of 45 tested. More samples, some of them from Sierra Leone and Liberia, have been sent for examination.

"The epidemic is not spreading to other regions," Guinean Health Minister Remy Lamah told Reuters by telephone from the affected area in Guinea's remote Forest region.

"Medical equipment has been shipped in," he said. "MSF is helping us to control the outbreak."

An MSF spokesperson said the number of suspected infections had risen by just two from Tuesday to 88, according to government figures. Four more people died, however, bringing the death toll to 63.



FEAR OF ILLNESS SPREADS

MSF flew in 33 tonnes of medical equipment at the weekend, enabling the creation of an isolation ward in Gueckedou, the epicentre of the outbreak. Ten patients with Ebola symptoms were receiving treatment there, MSF said.

Another isolation facility is being set up in the town of Macenta to deal with suspected cases there, said the medical charity, which is reinforcing its emergency team of 30 staff.

Despite official assurances that all possible precautions were being taken to contain the outbreak in the southeast, fear of the illness has spread to the distant capital Conakry.

Drivers at one of the seaside capital's main bus stations said travel to the affected regions had slowed to a trickle.

"We have problems. Since the appearance of Ebola, which we'd never before heard of, there's now a psychosis. People are scared," said Ibrahima Oualare, spokesman for the local bus and taxi drivers' union.

Since its discovery in 1976 in what is now Democratic Republic of Congo, only around 2,200 cases of Ebola have been recorded. Of those, 1,500 were fatal.

The outbreak of mysterious haemorrhagic fever was first detected in Guinea in February. Scientists have since identified it as the most virulent Zaire strain of the Ebola virus.

The virus is believed to reside primarily in bats between rare outbreaks in humans. Some experts believe it may have been carried by bats from central Africa, where it is more common.



BAN ON BATS AND BUSHMEAT

In an effort to contain the disease, Guinea has banned the sale and consumption of bats and other types of bush meat, and banned public funerals for those killed. Volunteers from the Guinean Red Cross were disinfecting the homes of victims and dealing with infected bodies.

The disease incubates for up to three weeks and its symptoms are similar to malaria and cholera, making it difficult to detect in West Africa, where such diseases are endemic.

The virus initially causes raging fever, headaches, muscle pain, conjunctivitis and weakness, before moving into more severe phases of causing vomiting, diarrhoea and haemorrhages. There is no vaccine and no known cure.

A team of Guinean and MSF officials were visiting villages advising people on precautionary measures. Infection can be reduced by simple measures such as washing hands, wearing gloves, face masks and protective goggles.

The teams were travelling by foot seeking to identifying people who have been in direct contact with Ebola victims and to isolate people showing symptoms of the disease, MSF said.

International SOS, a leading medical and travel security services company, said business travellers and expatriates working outside the healthcare sector had a low risk of contracting the rare disease. Several international mining companies are active in Guinea, Sierra Leone and Liberia.