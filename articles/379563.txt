The next time you find yourself with an uncomfortable rash, there is a chance that the rash could be the result of nickel allergy which could have come from using an iPad. This is according to a recently published paper by medical journal, Pediatrics, who discovered that a 11-year old boy had developed a rash which was traced back to his use of the iPad.

Advertising

According to the tests conducted on him, they found nickel on his skin and that the nickel had come from the iPad. Later when they decided to wrap his iPad using the Apple Smart Case, the rashes began to improve, thus suggesting that it was the boy’s recent use of the iPad that could have led to the rash in the first place.

According to an Apple spokesman, Chris Gaither, he said that the Cupertino company has no comment on the matter. It seems highly unlikely that Apple will be swapping out nickel for an alternate material, so we guess if you start developing a rash, perhaps, just perhaps, it could be stemming from your iPad.

While allergies to nickel aren’t life-threatening, they can be very uncomfortable as the rashes can be itchy. According to the Mayo Clinic, they list symptoms such as rash or bumps on the skin; itching; redness or changes in skin color; dry patches of skin that resembles a burn; blisters and draining fluid as possible symptoms of a nickel allergy.

Filed in . Read more about iPad.