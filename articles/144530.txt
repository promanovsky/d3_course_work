Victims of bullies suffer the psychological consequences all the way until middle age, with higher levels of depression, anxiety and suicide, new research shows.

The immediate ill effects of bullying have been well documented, with experts increasingly seeing it as a form of child abuse. Influential studies from Finland have made the case that people who were bullied as kids continued to suffer as young adults – girls who were bullied grew up to attempt and commit suicide more frequently by the age of 25, for instance, and boys were more likely to develop anxiety disorders.

Now a trio of researchers has taken an even longer view. They examined data on roughly 18,000 people who were born in England, Scotland and Wales during a single week in 1958 and then tracked periodically up through the age of 50 as part of the U.K.’s National Child Development Study.

Back in the 1960s, when the study subjects were 7 and 11 years old, researchers interviewed their parents about bullying. Parents reported whether their children were never, sometimes or frequently bullied by other kids.

Advertisement

Fast-forward to the 2000s. About 78% of the study subjects are still being tracked at age 45, when they are assessed for anxiety and depression by nurses. By the time they’re 50, 61% of them remain in the study, and are asked to fill out a questionnaire that measures psychological distress.

The researchers found that people who were bullied either occasionally or frequently continued to suffer higher levels of psychological distress decades after the bullying occurred. They were more likely than study subjects who were never bullied to be depressed, to assess their general health as poor, and to have worse cognitive functioning. In addition, those who were bullied frequently had a greater risk of anxiety disorders and suicide.

The consequences of bullying were economic as well. Study subjects who had been bullied frequently had fewer years of schooling than their peers, the researchers found. Men in this group were more likely to be unemployed; if they had jobs, their earnings were typically lower.

Adults who were bullied as kids were more socially isolated too. At age 50, bullying victims were less likely to be living with a spouse or a partner; less likely to have spent time with friends recently; and less likely to have friends or family to lean on if they got sick. Overall, they felt their quality of life was worse than people who hadn’t been bullied, and those who had been frequent victims were less optimistic that their lives would get better in the future.

Advertisement

Overall, 28% of the people in the study were bullied occasionally as kids, and an additional 15% were bullied frequently. Boys were more likely to be victims than girls.

“The findings are compelling in showing that the independent contribution of bullying victimization survives the test of time,” the researchers concluded. “The impact of bullying victimization is pervasive, affecting many spheres of a victim’s life.”

The study was published online Friday by the American Journal of Psychiatry.

If you’re interested in the latest scientific and medical studies, you like the things I write about. Follow me on Twitter and “like” Los Angeles Times Science & Health on Facebook.