Alibaba disclosed Tuesday in its prospectus with the Securities and Exchange Commission that its profit totaled $2.8 billion for the nine months ended Dec. 31 on revenue of $6.5 billion. By contrast, Amazon hasn’t had much in the way of net income during its 20-year history, with $274 million earned for all of 2013 on sales of $74.45 billion. Put another way, Amazon makes less than a penny for every dollar in revenue, while Alibaba makes about 43 cents.

Alibaba Group Holding is often called the Amazon.com of China. Yet the Asian company’s initial public offering filing shows that the differences between the two e-commerce giants are vast.

“People may be very surprised by comparing the balance sheets of Amazon and Alibaba,” said Yan Zhang, professor of strategic management at Rice University, who has been tracking Alibaba. “Once they see the differences in the business models, they’ll understand why.”

Alibaba’s ability to churn out profits underscores how Amazon Chief Executive Officer Jeff Bezos’ strategy of pouring money back into the business and pressuring already razor-thin margins isn’t the only way to become one of the world’s biggest online-commerce companies. That may exacerbate investors’ recent dissatisfaction with Amazon — they have pushed down the company’s shares 25 percent so far this year, as Bezos ramps up spending on fulfillment centers and delivery operations.

Having Alibaba’s shares trading on a U.S. stock exchange will offer global investors an alternative to Amazon’s stock, said Edward Williams, an analyst at BMO Capital Markets. The option to invest in either Amazon or Alibaba may sharpen as both companies increasingly push into each other’s home turfs, with Amazon working to increase sales in China and Alibaba stepping into the U.S., he said.

Mary Osako, a spokeswoman for Seattle-based Amazon, didn’t respond to a request for comment. Lin-Hua Wu, a spokeswoman for Hangzhou-based Alibaba, declined to comment.