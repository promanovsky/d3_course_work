Mars rover clocking up the mileage

Share this article: Share Tweet Share Share Share Email Share

Cape Town - It’s not exactly been at Olympic gold medal pace, but Nasa’s Mars rover Opportunity that landed on the Red Planet in 2004 has set an extraterrestrial distance record and is within sight of completing its first marathon. Amazingly, the rover was only designed to travel one kilometre, but when it trundled a further 48 metres on July 27, it took its total odometer reading since landing on Mars’s Meridiani Planum on January 25, 2004, to 40.5km – just 1 695 metres short of the classic long-distance event. It also passed the previous record held by the Russian rover Lunokhod 2 that arrived on the Moon on January 15 and set off three days later on what was then its record-setting journey across the lunar landscape, clocking 39km in less than five Earth months. “Opportunity has driven farther than any other wheeled vehicle on another world,” said Mars Exploration Rover Project Manager John Callas, of Nasa’s Jet Propulsion Laboratory in Pasadena, California, this week. “This is so remarkable considering Opportunity was intended to drive about one kilometre and was never designed for distance.”

Opportunity arrived on the Red Plant three weeks after its twin, Spirit, also part of Nasa’s Mars Exploration Rover Mission, had touched down on the other side of the planet. Spirit became immobile in 2009 and stopped communicating in 2010.

According to a [email protected] media release, Opportunity had driven more than 32km to Endeavour Crater in 2011 where it examined outcrops on the crater’s rim containing clay and sulphate-bearing minerals.

This month’s journey took it southward along the western rim of this crater, and if it can continue to the marathon mark, it will be approaching the next major investigation site that mission scientists have, not surprisingly, dubbed “Marathon Valley”.

Observations from spacecraft orbiting Mars suggest several clay minerals are exposed close together at this valley site, surrounded by steep slopes where the relationships among different layers may be evident, says [email protected] production editor Dr Tony Phillips.

Steve Squyres of Cornell University in Ithaca, New York, and principal investigator for Nasa’s twin Mars rovers, said the Lunokhod missions “still stand as two signature accomplishments of what I think of as the first golden age of planetary exploration, the 1960s and ’70s”.

“We’re in a second golden age now, and what we’ve tried to do on Mars with Spirit and Opportunity has been very much inspired by the accomplishments of the Lunokhod team on the Moon so many years ago. It has been a real honour to follow in their historical wheel tracks.”

As Opportunity approached its record distance earlier this year, the rover team chose the name Lunokhod 2 for a six-metre crater on the outer slope of Endeavour’s rim.

l On the web: http://www.nasa.gov/rovers and http://marsrovers.jpl.nasa.gov

[email protected]

Cape Argus