LOS ANGELES, May 7 ― Hollywood's most powerful couple, Brad Pitt and Angelina Jolie, famously met when they co-starred in 2005's “Mr. & Mrs. Smith”. Though both were involved with other people at the time, the chemistry between the pair was so strong that it now spans nine years and six children. But in all that time, they've never returned to the big screen together. That may be changing.

Angelina made her writing and directing debut in 2011 with “In the Land of Bread and Honey”. She has now written a second feature film, which Pitt is set to produce through his company, Plan B Entertainment. And rumour has it that the A-list couple may star in the film as well.

No info has been released about the storyline, or about whether Jolie plans to direct this movie too. She is currently gearing up for the release of her second project as a director, “Unbroken”, about the life of an Olympic athlete taken captive during World War II. The picture stars up-and-coming Hollywood it boy Jai Courtney, and will hit cinemas this December. ― Cover Media

Hollywood power couple, Brad Pitt and Angelina Jolie (left), are set to reunite on the big screen. ― File pic