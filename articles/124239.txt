The rapper turned actor was upset that he and his ‘Ride Along’ co-star, Kevin Hart, did not win the award for Best Onscreen Duo. HollywoodLifers, do you think Ice Cube’s comments were rude or misunderstood?

Paul Walker and Vin Diesel were named the winners of MTV Movie Award for Best Onscreen Duo on April 13, for their work in Fast & Furious but Ice Cube didn’t sound too happy about it.

Ice Cube Blasts MTV For Giving Paul Walker An Award

After Paul and Vin won the award, Ice gave an interview to USA Today that made him appear intensely unhappy about the loss.

“We had the best chemistry of everybody nominated, for us not to win was crazy,” he said. “They should have gave it to him before he passed away.”

Wow, pretty harsh for him to say, don’t you think? Especially since Paul died in a fiery car crash, just five months prior.

Ice Cube Speaks Out — ‘I Didn’t Diss Paul’

Following the intense backlash from Paul’s fan, Ice spoke out on his Twitter:

“I would never diss the actors who won. Not even Paul Walker. Seriously people! [Regarding] The sympathy vote: We should honor people before they die. That’s all. Shame on you […] reporters.”

Re: The MTV Awards. I wasn’t really mad we didn’t win. So I would never diss the actors who won. Not even Paul Walker. Seriously people! — Ice Cube (@icecube) April 15, 2014

Paul was previously nominated for two MTV Movie Awards in 2002 and this was always a show he attended.

HollywoodLifers, do you think Ice Cube was dissing Paul?

— Chloe Melas

More MTV Movie Awards News: