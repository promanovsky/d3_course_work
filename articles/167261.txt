AOL says email security breach may have exposed a 'significant number' of users' private information



Company said on Monday users' email addresses, postal addresses, address books, as well as encrypted passwords and security answers may have been exposed

Believes spammers used this contact information to send 'spoofed' emails

Spoofing is a tactic used by spammers to make an email look like it is from someone the recipient knows to trick him or her into opening it

AOL says a security breach may have exposed the private information of a 'significant number' of its email users' accounts.

The company said on Monday that the email addresses, postal addresses, address books, encrypted passwords and encrypted answers to security questions of users may have been exposed, along with some employee information.

AOL believes spammers used this contact information to send 'spoofed' emails that were made to look like they came from about 2 percent of its accounts.



Scroll down for video

Security breach: AOL said on Monday that its email users' personal information, including postal addresses and address books, may have been exposed

Spoofing is a tactic used by spammers to make an email appear that it is from someone the recipient knows to trick him or her into opening it. These emails do not originate from the sender's email — the addresses are just edited to make them appear that way.

The company says the investigation began following an uptick in spoofed emails, and is still underway.

AOL said that there is no indication that encryptions on passwords and security questions were broken, or that users' financial information has been released. However, the company is encouraging users to change passwords, along with their security question and answer.

AOL said that 'best-in-class' forensic experts and federal are investigating the breach and that it is working 'quickly and forcefully' to handle the situation.

