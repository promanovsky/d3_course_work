Andrew Garfield attends the 'Charles James: Beyond Fashion' Costume Institute Gala at the Metropolitan Museum of Art on May 5, 2014 in New York City. (Photo by Larry Busacca/Getty Images)

Some of the hottest hunks in Hollywood made their way to New York City for the 2014 Met Ball Gala on May 5, including ‘The Amazing Spider-Man 2’ star Andrew Garfield, and everyone’s favorite soccer star and underwear model, David Beckham. Check out our gallery of the hottest hunks from the biggest fashion event of the year!

The dress code for men at the 2014 Met Ball Gala in New York City on May 5 was White Tie — that is, tails, white bow ties, and optional top hats. Unfortunately, there were not as many top hats as we would have liked — that would have been amazing — but there was no shortage of hot hunks on the carpet! Keep reading to find out which of the hottest men — and their hotter fashion — hit the Met Ball Gala carpet, and click through our gallery, because you deserve it.

2014 Met Ball Gala Men’s Fashion: Andrew Garfield, David Beckham & More

Your friendly neighborhood Spider-Man, Andrew Garfield, 30, looked dashing in a silver metallic Band Of Outsiders blazer, and David Beckham, 39, proved that he looks as good in a Ralph Lauren suit as he does splashed all over enormous billboards in H&M underwear.

Andrew & David weren’t the only ones who disobeyed Vogue Editor-in-Chief Anna Wintour‘s request for white bow ties! Jay-Z, 44, looked dashing alongside wife — and queen — Beyonce, 32, in a white blazer, suit pants, and a black bow tie.

Meanwhile, Kanye West, 36, nearly followed Anna’s rules with a super formal Lanvin tux with black wool silk tails, a white piqué vest, and black tuxedo pants. No bow tie of any kind, but he looked so dapper!

John Legend, 35, hit the perfect note in a Ralph Lauren purple label black tuxedo — and white bow tie! Finally, a rule-follower in the ranks.

2014 Met Ball Gala Men’s Fashion: Ryan Reynolds, Paul Rudd & More

Looking a little heftier than we’re used to seeing him, Bradley Cooper, 39, proved that he can still look amazing while putting on weight for his upcoming role in American Sniper in a Tom Ford suit!

Finally, another one of our favorites, Ryan Reynolds, 37, looked super handsome alongside wife Blake Lively, 26, in a Gucci tux. In fact, the pair were both wearing the brand!

Other hot hunks on the carpet included Tom Brady in Tom Ford, Paul Rudd, Benedict Cumberbatch, and Jake Gyllenhaal. But enough of these silly words — check out our gallery and vote for your hottest hunk!

What do you think HollywoodLifers? Which hunk revved your engine at the 2014 Met Ball Gala? Vote above and comment below!

— Amanda Michelle Steiner

Follow @AmandaMichl

More 2014 Met Ball Gala News: