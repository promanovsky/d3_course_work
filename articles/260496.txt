Ubisoft is celebrating today's highly anticipated launch of Watch Dogs with a new companion app for iOS and Android devices.

The free app, dubbed ctOS Mobile, puts "Chicago in the palm of your hand," letting you connect to and play live with any Watch Dogs player who is logged in. You don't actually need to own Watch Dogs to play, but you'll need to connect with others who have the console or PC game to get the most out of the app.

"As a ctOS Operative, you will have control of the Chicago Police and control of all ctOS devices across the city. A Police helicopter carrying a sharpshooter will assist you in the hunt," according to the app's description. "The streets of the city become your ultimate battleground where your goal is to stop players and maintain order. Challenge and shut down other players with every tool available."

You'll also be able to control every ctOS device in the helicopter's radius including traffic lights, road blocks, and steam pipes, as well as dispatch different Chicago police units such as patrols, SUVs, and SWAT teams.

CtOS Mobile is now available for download in the App Store and on Google Play. The app requires a Wi-Fi, 3G, or 4G Internet connection as you can only play against other human gamers. You'll also need a Uplay account as well as a free Xbox Live and/or PlayStation Network account to challenge other Xbox Live or PSN players.

Watch Dogs was the most pre-ordered new IP in Ubisoft's history, and the second-highest pre-ordered Ubisoft game ever. The game is now available for PS4, PS3, Xbox One, and Xbox 360; the Wii U version will be released at a later, unspecified date.

For more, check out our interview with actress Aisha Tyler, who plays herself in Watch Dogs.

Further Reading

Mobile Game Reviews