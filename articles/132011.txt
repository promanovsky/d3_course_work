Getty Images

Arts and crafts retail chain Michaels Stores said Thursday that two separate security breaches at point-of-sale terminals last year at its US stores and Aaron Brothers subsidiary may have exposed nearly 3 million credit and payment cards.

The Michaels breach, which occurred between May 8, 2013, and January 27, 2014, may have affected about 2.6 million cards, or about 7 percent of payment cards used at its stores during the period, the retailer said in a statement. A separate breach on Aaron Brothers' payment systems from June 26, 2013 to February 27, 2014, may have exposed another 400,000 cards.

Michaels, which was the victim of a security breach in 2011, said the systems were attacked by hackers using highly sophisticated malware that neither of the security firms hired to investigate the breaches had previously encountered.

The company said it had received a "limited number of reports" of fraudulent use of cards involved in the breaches but said there was no evidence that other customer personal information, such as names, addresses, or PIN codes, was at risk in connection with this issue.

Michaels revealed that it was investigating a potential payment-card security breach in January, just two weeks after retail giant Target revealed that hackers had also infected point-of-sale terminals at its stores to steal to steal the payment card information from millions of customers. That massive breach occurred between November 27 and December 15, exposing the information of up to 110 million customers.