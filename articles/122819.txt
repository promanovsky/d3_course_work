2015 Chevrolet Trax small-SUV arrives with small-car agility

This week at the New York International Auto Show, Chevy announced that they’ll be bringing the Trax to their collection of small vehicles. This new Small SUV will be a global vehicle, arriving here in New York for the first time today. This is Chevrolet’s seventh small-vehicle launch in less than four years – if you’re counting.

Chevrolet suggests that this vehicle “blends small-car agility with the utility of an SUV.” You’ll be rolling with an Ecotec 1.4L turbocharged engine under the hood with electric power steering and available all-wheel drive. Also inside you’ll find a number of smart enhancements.

Available to the future owner of this vehicle are next-generation OnStar and MyLink technologies. We’ve had quite a few experiences with MyLink in the past – items like MyLink integrating Siri on a number of 2014 models struck a chord recently. The 2015 Trax works with a seven-inch-diagonal color touchscreen with USB ports for your smart device plugging.

This vehicle will be working with standard remote keyless entry and available remote start, as well as ten standard airbags throughout the vehicle. A 60/40-split fold-flat rear seat and fold-down front passenger seatback allow you to transport and store items up to 8-feet long, and you’ll find a number of storage areas like upper and lower glove boxes, with a USB port and auxiliary jack for simple plug-in of all music-playing devices.

This vehicle will be offered in LS, LT and LTZ trims, with 10 exterior colors: Black Granite Metallic, Summit White, Silver Ice Metallic, Ruby Red Metallic, Deep Espresso Metallic, Orange Rock Metallic, Satin Steel Metallic, White Pearl Tricoat, Blaze Red, and Brilliant Blue Metallic.

This vehicle has a review camera that comes standard, standard StabiliTrack electronic stability control, cornering brake control and assist, and ABS and electronic brake force distribution.

We’ll know more about this vehicle’s release and pricing soon. Stay tuned to enjoy our first-drive preview!