Story updated at 9:50 a.m. to reflect market activity.

NEW YORK (TheStreet) -- Nomura raised its price target for Microsoft (MSFT) - Get Report to $50 from $45 Tuesday.

Microsoft gained 0.4% to $42.32 in morning trading.

The firm reiterated its "buy" rating for the Windows maker. New CEO Satya Nadella will likely announce more details on his strategic vision and new earnings initiatives during the July 22 earnings report according to Nomura analysts Rick Sherlund, Kashif Sheikhm, and Frederick Grieb.

"We expect bold moves and organizational change intended to restore a culture of innovation," the analysts wrote. "Our expectation is for a substantial ($1 billion plus) charge to mitigate the Nokia acquisition risk and focus the strategic direction of the company away from the previous devices/services strategy and drive the company into a new generation of productivity apps and services and pursue the subscription and cloud businesses of Office 365 and a more aggressive push into PaaS (platform-as-a-service)."

Must read:Warren Buffett's 25 Favorite Stocks

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

--------------

Separately, TheStreet Ratings team rates MICROSOFT CORP as a Buy with a ratings score of A-. TheStreet Ratings Team has this to say about their recommendation:

"We rate MICROSOFT CORP (MSFT) a BUY. This is based on the convergence of positive investment measures, which should help this stock outperform the majority of stocks that we rate. The company's strengths can be seen in multiple areas, such as its largely solid financial position with reasonable debt levels by most measures, notable return on equity, reasonable valuation levels, solid stock price performance and good cash flow from operations. We feel these strengths outweigh the fact that the company has had sub par growth in net income."

Highlights from the analysis by TheStreet Ratings Team goes as follows:

MSFT's debt-to-equity ratio is very low at 0.26 and is currently below that of the industry average, implying that there has been very successful management of debt levels. Along with this, the company maintains a quick ratio of 3.01, which clearly demonstrates the ability to cover short-term cash needs.

The return on equity has improved slightly when compared to the same quarter one year prior. This can be construed as a modest strength in the organization. When compared to other companies in the Software industry and the overall market, MICROSOFT CORP's return on equity exceeds that of the industry average and significantly exceeds that of the S&P 500.

Compared to where it was a year ago today, the stock is now trading at a higher level, regardless of the company's weak earnings results. Turning our attention to the future direction of the stock, it goes without saying that even the best stocks can fall in an overall down market. However, in any other environment, this stock still has good upside potential despite the fact that it has already risen in the past year.

MICROSOFT CORP's earnings per share declined by 5.5% in the most recent quarter compared to the same quarter a year ago. This company has reported somewhat volatile earnings recently. But, we feel it is poised for EPS growth in the coming year. During the past fiscal year, MICROSOFT CORP increased its bottom line by earning $2.60 versus $2.00 in the prior year. This year, the market expects an improvement in earnings ($2.70 versus $2.60).

You can view the full analysis from the report here: MSFT Ratings Report

STOCKS TO BUY: TheStreet Quant Ratings has identified a handful of stocks that can potentially TRIPLE in the next 12 months. Learn more.

Editor's Note: Any reference to TheStreet Ratings and its underlying recommendation does not reflect the opinion of TheStreet, Inc. or any of its contributors including Jim Cramer or Stephanie Link.