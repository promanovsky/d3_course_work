Activision launched a new E3 trailer for upcoming action RPG “Destiny” on Wednesday. The video was part of the title’s promotional appearance at this year’s Electronic Entertainment Expo in Los Angeles.

On Monday, Sony had revealed a limited edition white PlayStation 4 will launch alongside "Destiny" this fall. The bundle will be available for $449 and the PS4 will contain a 500GB hard drive, a copy of the game and a 30-day PlayStation Plus voucher.

“We believe Destiny could be Activision Publishing's third billion-dollar franchise, and we're working hard to make it the biggest new IP launch in video game history," CEO Eric Hirshberg had said of the title on Feb. 6.

"Together, we are bringing gamers an ambitious and innovative game which combines the intensity of great first-person action, with the character investment and social connections of an RPG in an immense connected universe. We believe the game is on track to deliver record pre-orders for a new franchise, a public beta is planned for summer and we expect to launch the game on September 9.”

The game was originally planned for an early 2014 spring release on Xbox One, PS4, Xbox 360 and PS3.

“Destiny” will take place 700 years in the future in a post-apocalyptic setting following a lucrative and successful period of exploration and peace called the Golden Age. The game is set in a universe where humans have colonized a number of livable planets in the solar system. However, a disastrous event known as “the Collapse” results in the mysterious destruction of these colonies, leaving mankind nearly extinct. When humans attempt to repopulate and recover after the Collapse, aggressive and dangerous alien races are discovered in various towns and cities. Players will have the task of investigating and destroying the aliens before the human race is entirely wiped out.

“Destiny” will launch on Sept. 9 this year for the PS3, PS4, Xbox One and Xbox 360. The title was referenced in 2009’s “Halo 3: ODST,” where a sign in the game read “Destiny Awaits” alongside a picture of Earth with an unknown orb floating nearby.

Game features were accidentally leaked in November 2012, revealing plot ideas and concept art. Bungie followed up with the delivery of further details about “Destiny,” while expressing regret that game details were made public before expected. The title was confirmed to be released on the PS4 and PS3 at the PS4 reveal event in February last year. Bungie also stated that both PlayStation versions would feature exclusive content. The release date of Sept. 9, 2014 was confirmed last December.