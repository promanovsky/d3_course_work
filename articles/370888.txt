The vials were discovered last week in an unused part of a storage room inside a laboratory operated by the Food and Drug Administration, the CDC said in a news release. They were found by employees preparing to move the lab, which has been operated by the FDA since 1972, over to the main FDA campus.

Authorities said there was no indication that anyone had been exposed to smallpox, and they said no risk to workers or the public has been found. The vials originated in the 1950s, according to the CDC. (The last smallpox case in the United States occurred in 1949.)

AD

AD

The vials were discovered on July 1 by an FDA scientist. There were 16 vials, six of which were labeled “variola.” These vials were immediately placed in a secure containment laboratory in Bethesda and the CDC sent a team to pick them up on July 6, spokesman Tom Skinner said. The vials were transported on a government plane to the CDC’s most secure containment lab in Atlanta, where testing confirmed that the vials did contain smallpox.

Additional tests will be needed to see if the smallpox is viable, after which the samples will be destroyed, authorities said.

The Federal Bureau of Investigation, along with the CDC’s Division of Select Agents and Toxins, is investigating how these samples came to be stored in the FDA lab. Smallpox can only be kept in two locations on the planet, both of them inspected by the World Health Organization: The CDC in Atlanta and the State Research Center of Virology and Biotechnology in Novosibirsk, Russia.

AD

AD

The big question is what the smallpox was doing in the FDA lab. “We’re trying to find out,” said Skinner. “This certainly is an unusual event.”

This discovery came just weeks after dozens of scientists were potentially exposed to live anthrax bacteria, an episode that required as many as 84 employees to get a vaccine or take antibiotics and resulted in the reassignment of the bioterror lab’s director.