New Delhi: India's private sector output witnessed significant uptrend in June as both manufacturing and services sector registered solid rate of expansion, driven by robust order flows and stronger business sentiment, an HSBC survey said on Thursday.

The headline HSBC Services Business Activity Index was at a 17-month peak of 54.4 in June rising from a modest 50.2 in May, HSBC said adding that "the Modi wave has struck the service sector".

A reading above 50 shows that the sector is expanding, while a reading below 50 depicts that the output in the sector is contracting.

"After months of subdued activity, the Modi wave has struck the service sector and lifted growth to a 17-month high," HSBC Co-Head of Asian Economic Research, Frederic Neumann, said.

Moreover, optimism regarding the output levels in the services sector for the next 12 months remained positive in June, owing to factors such as end of the elections, planned increases in marketing budgets, forecasts of stronger demand and ongoing improvements in India's economy.

Meanwhile, the HSBC India Composite Output Index, which maps both services and manufacturing, rose to a 16-month high of 53.8 in June, up from 50.7 in the prior month.

"New business flows and stronger business sentiment supported the rise. Some of this is simply pent up demand being unleashed," Neumann added.

He further said faster reforms due to political stability should fuel the growth momentum further. However, tensions in the Middle East and below normal monsoon remain the main concern areas.

"Be sure to expect some bumps along the way, as tensions in the Middle East and the absence of monsoon clouds play spoil sport," Neumann said.

According to Met department forecast, India is expected to receive below normal monsoon rainfall. This is being attributed to El-Nino conditions, whose chances of occurrence are forecast to be high.

Monsoon is crucial for the Indian economy, especially for the agriculture sector which is largely rain-fed.

The HSBC Services Business Activity Index averaged 51.1 in the second quarter of 2014, an improvement from Q1's average of 48.2 and the highest quarterly reading since Q2 2013.

Evidence from survey participants pointed to stronger new business inflows and better economic conditions, HSBC said.