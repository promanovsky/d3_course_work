Lindsay Lohan, who often gets the type of publicity that it’s hard to imagine any celebrity would really desire, got more of that type of unwanted attention last month when a handwritten list — allegedly penned by Lindsay herself — of 36 famous or sort-of famous men that she’s had sex with leaked to the public, via InTouch Magazine.

At the time, Lindsay Lohan’s reps, who admittedly have their hands full most of the time, flatly denied that the partially redacted “sex list” was authentic. But now, just over a month later, Lindsay Lohan herself says that the list is the real deal.

Well, she admitted that she made the list herself anyway. What she did not address in an interview on Bravo TV promoting the finale of her Oprah Winfrey-produced reality series, was whether the list was accurate. While most of the gentlemen named as previous sexual partners by Lindsay Lohan have remained mum on the issue, one has come forward to brand the list “lies.”

“Lindsay herself has told lies about me with her people-she’s-slept-with list!” actor James Franco told Los Angeles Magazine in an interview last month. Long before the list came out, Franco had denied ever having sex with Lindsay Lohan in an interview on Howard Stern’s satellite radio program.

“I don’t know how that got out,” Franco told Stern. “She was having issues even then, so you feel weird. Honestly, she was a friend. I’ve met a lot of people that are troubled and sometimes you don’t want to do that.”

The upcoming episode of her reality show supposedly captures the moment Lohan learned that the celebrity sex list had gone public — when she breaks down crying over the revelation.

“The fact that that happened was not only humiliating, but just mean,” she says on the show.

Though Lindsay Lohan has now admitted that she made such a sex list, her version of how the list came to be differs markedly from the story that appeared in InTouch Magazine. The magazine reported that Lindsay Lohan made the list as part of a bragging contest with friends in a hotel bar, and she thoughtlessly left it behind where someone could find it.

But Lindsay Lohan now says that she made the list when she was in the Betty Ford Center being treated for alcoholism, and never meant for anyone to see it.

“That was actually my fifth step in AA at Betty Ford. Someone, when I was moving during the OWN show, must have taken a photo of it,” Lohan said. “That’s a really personal thing and it’s really unfortunate.”

The stark differences in the stories, however, raise the possibility that perhaps Lindsay Lohan is mistakenly referring to a different list. Which raises the question, is there more than one Lindsay Lohan celebrity sex list?