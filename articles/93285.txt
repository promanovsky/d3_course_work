George Lucas and Disney are set to produce a new Star Wars trilogy, with Episode 7 to be released 2015.

George Lucas and Disney are set to produce a new Star Wars trilogy, with Episode 7 to be released 2015....

LAUGH it up, fuzzball: According to The Hollywood Reporter, actor Peter Mayhew will reprise the role of Chewbacca in Star Wars: Episode VII.

Mayhew played the Wookiee in four Star Wars films: Episode III, Episode IV, Episode V and Episode VI.

Mayhew, who turns 70 in May, has not been added to the official Star Wars: Episode VII cast, which currently numbers just one: R2D2.

It’s expected, however, that original stars Harrison Ford, Mark Hamill and Carrie Fisher will all appear in Episode VII, along with newcomers such as Adam Driver (as the film’s villain) and, perhaps, Oscar winner Lupita Nyong’o.

Disney Studios chairman Alan Horn revealed on April 2 that Star Wars: Episode VII was already filming, despite no official casting announcements.

“We have a lot of them [in place],” Horn said about the Star Wars cast. “We’re just not completely done yet.”

Star Wars: Episode VII, which still doesn’t even have an official title, is due out in theatres on Dec. 18, 2015 (in the US).

For more on Mayhew, head to THR.