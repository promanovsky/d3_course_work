The number of Americans taking attention deficit hyperactivity disorder medicines rose 36 percent in 2012 from 2008, led by a surge among women, according to drug-benefits manager Express Scripts Holding Co.

Use of the medications grew 85 percent in 2012 from 2008 for women ages 26 to 34, and women 19 and over now outnumber men in use of the medicines, according to the report released yesterday. Boys 12 to 18 years old are the most heavily prescribed, with about 9.3 percent on ADHD drugs in 2012.

Almost 4.8 million privately insured people were on ADHD medicines in 2012, the report said. Those with the disorder have problems paying attention, the U.S. Centers for Disease Control and Prevention says.

Drugs to treat it, including Ritalin, Adderal and their generic equivalents, are stimulants.

"The population treated for ADHD has exploded in the U.S., dwarfing its diagnosis and treatment seen in other countries," Express Scripts said. Use of the drugs increased among every single age category from 4 to 64, the report found.

Express Scripts, based in St. Louis, using a database of 15 million people with private insurance, looked at the claims of more than 400,000 people who filled an ADHD prescription.

Females take ADHD prescription drugs at lower rates than boys from ages 4 to 18, then increase from age 19. Some may be taking the medications improperly as a weight-loss aid, or for depression, the report said.

Sign up for coronavirus updates Get the latest on the fast-moving developments on the coronavirus and its impact on Long Island. By clicking Sign up, you agree to our privacy policy.

A note to our community: As a public service, this article is available for all. Newsday readers support our strong local journalism by subscribing. Please show you value this important work by becoming a subscriber now. SUBSCRIBE Cancel anytime

More cases needing treatment are being diagnosed, but "there are a variety of clinical and societal trends that have inflated ADHD diagnoses and drug treatments to questionable levels," said the report. "ADHD has unfortunately become the go-to condition for children with behavioral issues." -- Bloomberg News