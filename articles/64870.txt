Ink for printing is more expensive by volume than French perfume. A teen's science fair project shows how we can save significant money in printing costs simply by switching typefaces. UPI

A teenager has presented compelling figures about how much money the U.S. government could save just by switching the typeface it uses in its documents.

In a study published in the Journal for Emerging Investigators, an academic journal run by Harvard grads that publishes middle- and high-school students, 14-year-old Suvir Mirchandani shows how the government could save an estimated $136 million a year of its $467 million annual ink expenditure simply by changing fonts.

Advertisement

Mirchandani's findings started as a science fair project aimed at helping his school save money and conserve resources on all the handouts the school was producing.

According to CNN, Mirchandani collected random samples of the handouts, concentrated on the most common letters, then used a special software to calculate the amount of ink used by each font. He then confirmed his findings by weighing out pieces of cardstock with identical letters printed in different fonts to compare ink usage.

RELATED Concerns over Russia spill into NASA budget discussion

“From this analysis, Mirchandani figured out that by using Garamond with its thinner strokes, his school district could reduce its ink consumption by 24 percent, and in turn save as much as $21,000 annually.”

“We were so impressed. We really could really see the real-world application in Mirchandani's paper,” said the editors at JEI, who challenged Mirchandani to take his study to the next level by applying the analysis to the federal government’s ink budget.

What Mirchandani found was that the government could save nearly 30 percent, or $136 million a year, by using the font Garamond exclusively. If state governments jumped on board as well, an additional $234 million might be saved annually, he reports.

"Ink is two times more expensive than French perfume by volume," Mirchandani told CNN with a chuckle, who confirmed that “Chanel No. 5 perfume costs $38 per ounce, while the equivalent amount of Hewlett-Packard printer ink can cost up to $75.”

Thinking about switching fonts yet?

[CNN] [Journal for Emerging Investigators]