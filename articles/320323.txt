Below, get a First Look at Demi Lovato new video, "Really Don't Care", which will have its debut on MTV, MTV Hits and LOGO TV at 5 p.m. ET on June 26th.

The exclusive sneak peek, courtesy of Mashable.com, also features the pop star's explanation of its LGBT-infused theme. The video, directed by Ryan Pallotta, was shot in Los Angeles on June 8th during the city's annual Pride Parade at which Lovato was the grand marshal.

"The energy was electric and radiates in this video" Lovato said. "For me, 'Really Don't Care' is an anthem that represents being comfortable in your own skin and not caring what others think. Pride defines that."

"Really Don't Care" is the fourth single from the 21-year-old's fourth studio album Demi. In 2013, she asked her fans to unlock the video for "Heart Attack," the first single from the album, by tweeting words from the single along with #UnlockHeartAttack.