next Image 1 of 2

prev Image 2 of 2

Model Emma Appleton says she regrets sharing a racy message about trading sex for a photo shoot that she believed came from famed photographer Terry Richardson.

She posted a screengrab of the message, which caused a stir on Sunday, that read “If i can f—k you i will book you in ny for a.shoot for Vogue”

Appleton has since deleted the Twitter account that she shared the message from, but she told Buzzfeed she believed it came from Richardson’s Facebook page.

Richardson's rep denied the contents of the message in a statement to Fox411.

"Terry Richardson did not reach out to this woman. It was sent from a Facebook page that is fake. Terry has no knowledge of who sent this," a spokesperson said. "Mr. Richardson has retained online forensic expert Theo Yedinsky to determine the actual origin of the faked posting."

Meanwhile, Vogue U.S. told the site it hasn’t worked with Richardson in several years and has no plans to work with the controversial photographer in the future. Vogue Paris worked with him in February.

Appleton insisted via Instagram on Monday morning that she didn’t share the alleged Richardson message for attention, and she doesn’t appreciate the media hype that’s come her way. She acknowledged in her post that the message could have been sent from a Facebook account impersonating Richardson.

“Let me clear something up — I don’t want this attention, I have no desire to become known in the slightest,” she wrote. “If the account is fake then it needs to be removed, if it is real then he is a hideous human. I regret posting it because of what it’s caused but at the same time (if it’s real) then it shouldn’t be covered up, there are too many young, vulnerable girls that this industry can exploit.”

Richardson has been accused of inappropriate conduct with young models on the set of his photo shoots in the past. He has repeatedly denied those allegations. He recently made headlines for filming Miley Cyrus' controversial "Wrecking Ball" video.

Appleton did not immediately respond to an email requesting additional comment.