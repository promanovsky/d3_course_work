"There wasn’t a dry eye in the whole operating theatre", Ohio mum Sarah Thistlewhistle told reporters after giving birth on Mother's Day in the US to identical twins who were born holding hands.

Thistlewhistle had come through a difficult pregnancy after the tiny twins, named Jillian and Jenna, were born with a one in 10,000 condition that means they share the same amniotic sac and placenta.

The twin girls were monoamniotic, a condition that can be deadly if the umbilical cords become twisted and tangled. Monoamniotic children, who account for only 1 to 5 per cent of twins, can also face birth defects.

According to the Akron Beacon Journal newspaper, Jenna and Jillian, born premature at 33 weeks and 2 days, will spend two to four weeks in the nursery after they were delivered by Caesarean section at 2:41pm at Akron General Medical Centre last Friday.

Jenna was born first at 4 pounds, 2 ounces and 17 inches, with Jillian following 48 seconds later at 3 pounds, 13 ounces and 17.5 inches.

They were moved temporarily to Akron Children's Hospital because they needed breathing assistance. Thistlethwaite expects to be released from Akron General on Tuesday, while the girls will remain in the hospital two to four weeks.

Dr. Melissa Mancuso helped deliver the twins, one of several amniotic pairs she has helped deliver in 11 years.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

She said the twins are at risk during pregnancy of entanglement of umbilical cords, which can cause death. Another woman at Akron General is expected to give birth later this week to monoamniotic twins.

"They’re already best friends," Thistlewhistle told the paper. "I can’t believe they were holding hands. That’s amazing."

The twins were removed from ventilators on Sunday after they were able to breathe comfortably.

"It's just hard to put into words how amazing it feels to know the girls are OK," the relieved mother told the Associated Press.