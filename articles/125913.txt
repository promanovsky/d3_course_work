NEW YORK -- Ford is celebrating the 50th anniversary of the Mustang with a limited-edition model and a display atop the Empire State Building.

At the New York International Auto Show on Wednesday, the company revealed the 50 Year Limited Edition. The company will only build 1,964 special cars, honouring the year when the Mustang first went on sale.

"If you don't like this car, you don't like cars," said Executive Chairman Bill Ford, whose first car was an electric green 1975 Mustang.

Earlier in the day, Bill Ford appeared with a bright yellow 2015 Mustang convertible on the 86th floor observation deck of the Empire State Building. It's the first time a car has been there since 1965, when Ford put a Mustang convertible there.

The car had to be broken into five pieces for the ride up the building's elevators and reassembled late at night, when the deck is closed to visitors. It will be on display until Friday.

The 50 Year Limited Edition models will come in one of the two colours of Ford's logo: white or blue. Buyers can choose a manual or automatic transmission.

There are special chrome highlights around the grille, windows and tail lights. The Limited Edition will also be the only 2015 Mustang GT with a faux gas cap badge on the rear, where the original cap sat.

Limited Edition cars will be among the first built when 2015 Mustang production begins later this year. Bill Ford said the company hasn't yet decided how to allocate them, or what the price will be. In 1964, the Mustang's starting price was $2,300.

Ford chose to mark the anniversary in New York because the Mustang was first shown here at the 1964 World's Fair.