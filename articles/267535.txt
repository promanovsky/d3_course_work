Nationally, retail giant Walmart Inc. kept its spot at the top of the list while Apple edged into the top 5 this year. Oil giants Exxon Mobil and Chevron kept their top-5 spots.



The Forbes 500 list, started in 1955, ranks companies by total revenue as a measure of their size and influence.



pfrost@tribune.com

Follow @peterfrost Follow @chibreakingbiz