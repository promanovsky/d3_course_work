Provided by Kaiser Health News:

With less than a week left until insurers stop accepting new customers through the health care marketplaces, a poll released Wednesday finds that half of the people still without health coverage intend to remain uninsured.

Five million people have signed up for insurance since the marketplaces created by the federal health law opened in October. The last day to sign up without facing a financial penalty is March 31. The Congressional Budget Office estimates by the year’s end, 6 million people will have obtained insurance on the marketplaces.

The Kaiser Family Foundation’s latest poll, conducted in mid-March, found that 50 percent of adults under age 65 who still lack coverage plan to remain without insurance, while 40 percent aim to sign up by the deadline at month’s end. (KHN is an editorially independent program of the foundation.) The other 10 percent said they did not know what they would do or refused to talk about it.

Of the uninsured, two out of three said they have not tried to get coverage yet. The rest said they attempted to get it through an online marketplace such as healthcare.gov, the state-federal Medicaid program, their employer or a private insurance company.

Only four in 10 of the uninsured knew the deadline to sign up for coverage, the poll found. A third of the uninsured didn’t know they are subject to a fine if they don’t obtain coverage, absent a few permitted exclusions such as financial hardship. The poll found that direct outreach efforts have had limited success: only 11 percent of uninsured people said they had been contacted about the health law by phone call, email, text message or a home visit.

According to the poll, the majority of the public still holds unfavorable views of the health care law, although that gap has narrowed from the beginning of the year. Forty-six percent of people said they disliked the law, while 38 percent approved. That gap of eight percentage points is half of what it was in November and January. The views of people without insurance also have improved since earlier this year and now essentially mirror the overall public’s opinion. The pollsters noted that it is tricky to compare the views of the uninsured over time since the composition of that group changes as more people get coverage.

A majority of the public, 53 percent, is tired of hearing fights over the health law. Forty-two percent believe the debate should continue.

The poll was conducted among 1,504 adults between March 11 through March 17. The margin of error for questions of the overall public is +/- 3 percentage points. The margin of error for questions of just the uninsured population is +/- 9 percentage points.