When Minecraft first went on sale, it wasn’t finished yet. In fact, it’s still not finished, but when the first Minecraft alpha release hit the Internet, a light went off. Five years later, early access games have become an indelible part of the PC gaming ecosystem, perhaps most notably with DayZ and Rust, and now console makers are looking for a way to bring unfinished games to the living room as well.

Speaking with Gamasutra, PlayStation VP of Publisher and Developer Relations Adam Boyes says that putting a game out on the PS4 before it’s ready for a full release is “one of the massive conversations we have internally” — at what point is a game ready to see the light of day? Boyes and the PlayStation team don’t want unwitting customers to stumble upon an early access game expecting a final product and walk away disappointed. It’s a balancing act.

“We’re figuring out what’s ok,” Boyes continued. “We obviously have our tech requirement checklist that people have to adhere to. So we’re internally discussing, what does that list look like this? What are the caveats? Stuff like this. So it’s still a project that a lot of minds are considering. No details yet, but it’s something on the top of my mind every day.”

Microsoft is listening to developers who are interested in early access titles on the Xbox One too, as ID@Xbox director Chris Charla explained to Develop.

“Right now on Xbox One and Xbox 360, you can do betas,” said Charla. “When we talk about early access, it typically means a game that you buy and it evolves over time to become 1.0, so you’re buying it before it’s 1.0 – Minecraft on PC is a perfect example.”

Charla says it’s something that developers have been asking for, but there’s nothing to announce at the moment.

In the fluid, open environment that is Steam, the ability to release early access games has resulted in both massive success and dismal failure. Sony and Microsoft don’t have the same flexibility, so if early access does come to consoles, it will likely be in a different format, but it could be a major step forward for independent developers who have a promising idea they want to get into gamers’ hands.