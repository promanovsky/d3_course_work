Pharrell Williams leaves his famous hat at home on the cover of GQ magazine’s April 2014 issue.

Here’s what the 40-year-old entertainer had to share with the mag:

On his Oscars snub: “Well, trust me: when they read the results, my face was…frozen. But then I thought about it, and I just decided just to…let it go.”

On Hillary Clinton being the next president of the US: “Let me tell you why Hillary’s going to win. Everywhere you go in this country, you have red and blue. You got the Democrats; you got the Republicans. You got the Bloods; you got the Crips. You know what else is red and blue? Blood. Blood is blue in your body until air hits it, and then it turns red. That means there’s unity. There’s gonna be unity. So when you think about a night where there’s late-night talk-show hosts and it’s mostly women, that’s a different world. Right? A world where 75 percent of the prime ministers and the presidents were women: that’s a different world. That’s gonna happen, and it’s gonna happen when Hillary wins. Because you know what? No matter how staunch of a supporter you are of no-abortion, whatever you are: you’re a woman, and there’s no way in the world you’re going to vote for somebody that’s going to try to tell you what to do with your body. Hillary’s gonna win. Listen, I’m reaching out to her right now. She’s gonna win.”

On his hat: “Anything different, people are going to look at and go, ‘Ha ha ha ha, what is that??’ Then, after a while, they do a little bit of research; they realize it’s Vivienne Westwood, an ode to her boyfriend at the time; they had a store together called World’s End. The guy who went on to sign the Sex Pistols, Malcolm McLaren.”

For more from Pharrell, visit GQ.com.