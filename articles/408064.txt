During a recent interview pop singer Rita Ora admitted that she couldn't remember her lines for "Fifty Shades of Grey," starring Jamie Dornan as Christian Grey and Dakota Johnson as Anastasia Steele. The 23-year-old was cast as Mia Grey, the adoptive sister of Christian.

According to reports, things were so bad that Ora had to wear an earpiece and someone had to recite the lines to her. The singer said she kept freezing up because she was nervous.

"I had to have someone in my ear on set telling me what to say before I said it because I was honestly like so nervous I forgot everything that I had learnt," she said.

During the interview, Ora said Johnson helped her out tremendously and worked with her so she could overcome her first-time acting fear.

"She's [Dakota] is a great friend of mine and she really eased me, and she was really nice to take care of me," Ora said, according to Contactmusic.com.

There have been some "Fifty Shades" fans who've grumbled that author E.L. James got the casting all-wrong. Hopefully, Ora's performance on the big screen will be a lot better than how she described her experience on set.

The "Radioactive" singer had previously said she believes fans will enjoy the movie.

"The movie is amazing, it's such a great passionate movie," she said. "I had to be involved, I love the books."

"I know everybody's going to watch it, whether it's in secret or confidence, but everybody's going to watch it," Ora added. "It's going to be fun. I look really different and it was a good experience."

Fans got their first glimpse of "Fifty Shades of Grey" last week when Universal Pictures and Focus Features released the trailer during San Diego Comic-Con. The clip featured several key Christian/Ana moments: the interview, coffee shop date, elevator makeout session and Red Room of Pain. The trailer also came with a sexy rendition of Beyonce's "Crazy in Love."

"Fifty Shades of Grey" will be released Valentine's Day 2015.

Are you worried about Rita Ora's confession?

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.