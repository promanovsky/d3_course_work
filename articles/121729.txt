Beverage giant Coca Cola today said its volume growth in India increased by 6 per cent in the first quarter ended March 28, 2013.

The company, which reported two per cent increase in global volumes in the March quarter, said this is Coca Cola India's 31st consecutive quarter of growth, 19 of which are double digits.

Coca Cola gained volume and value share in India in non-alcoholic ready to drink beverages (NARTD) beverages in the quarter, the company said in a statement.

Coca Cola's Asia Pacific business reported seven per cent increase in volume in the quarter led by China and India.

Volume of Coca Cola in China grew by 12 per cent, it said.

Commenting on the result, Coca Cola Chairman and CEO Muhtar Kent said: "Our growth momentum is steadily improving in line with our expectations, as we delivered sequentially stronger volume growth of 2 per cent in the quarter while gaining global volume and value share in nonalcoholic ready-to-drink beverages."

"We are firmly committed to further advancing our growth trajectory through 2014 as we are accelerating marketing investments in our brands and focusing relentlessly on marketplace execution in partnership with our bottling partners around the world."

The company said volume of Bottling Investments Group (BIG) grew by 5 per cent in the quarter on a comparable basis, led by China and India.