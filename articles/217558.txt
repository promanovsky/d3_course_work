So we know that the highly popular and addictive game, Minecraft, will be making its way onto next-gen consoles like the PlayStation 4 and the Xbox One. Given that both consoles are more powerful in processor and graphic capabilities compared to their predecessors, the PlayStation 3 and the Xbox 360, it hardly comes as a surprise that gamers are looking forward to graphical improvements in the game.

Advertising

Well the folks at Destructoid have uploaded some screenshots from the game which shows off the differences that gamers can expect in the next-gen version of the game, and if you can’t really tell the difference, we can’t say we blame you. For the most part, both screenshots look almost identical, but then again Minecraft wasn’t exactly designed to look realistic to begin with.

However for those who are eagle-eyed, you can see that the PS4 version of the game is able to render objects further away. This is thanks to the power of next-gen consoles, meaning that gamers can look forward to more viewable voxel blocks. This is pretty much a non-issue for PC gamers, but for Minecraft gamers who want to enjoy the game on their console, we guess it’s still an improvement.

No word on when the next-gen versions of Minecraft will be making their way onto the PS4 or Xbox One, but we’ll be keeping our eyes peeled for more info. In the meantime what do you guys make of these “graphical improvements” to the game?

Filed in . Read more about Minecraft, PS4 and Xbox One.