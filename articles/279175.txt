Amazon will offer more than a million tracks for ad-free streaming and download to Kindle Fire tablets as well as to computers

Amazon is launching a music streaming service in the US for its Prime members, adding yet another freebie to its popular free-shipping plan.

Amazon will offer more than a million tracks for ad-free streaming and download to Kindle Fire tablets as well as to computers and the Amazon Music app for Apple and Android devices.

The service, called Prime Music, is likely to be integrated with an Amazon smartphone which is expected to be previewed soon.

People who pay 99 US dollars (£59) a year for Prime can listen to tens of thousands of albums from artists including Beyonce for no extra cost.

By adding music, Amazon is hoping to hook new customers and retain existing ones on its Prime free-shipping plan, which also allows subscribers to watch streams of movies and TV shows and gives Kindle owners a library of books they can borrow once a month.

Steve Boom, Amazon's vice president of digital music, said the service will pay for itself and is not part of the reason why the company raised the price of Prime from 79 US dollars (£47) in March - a move Amazon said would cover higher shipping costs.

Instead, the company will benefit because Prime members tend to buy more from Amazon and remain loyal customers.

"If they come to Amazon for their music needs, they become better and longer-term Amazon customers and we think that's a good thing," Mr Boom said.

Seattle-based Amazon reached licensing deals with most of the top independent labels and major recording companies Sony and Warner Music, but failed to reach a deal with top-ranked Universal Music Group.

That means that while the service will feature artists like Justin Timberlake, Bruno Mars, Bruce Springsteen, Pink and Madonna - it will lack music by Universal stars such as Katy Perry, Taylor Swift and Jay-Z.

The service also will not have many new releases - and for major artists that could mean music that has been released within the last six months.

Universal did not reach a deal with Amazon because it disagreed with the value of the lump sum royalty payment on offer for the albums in question, according to two people familiar with the matter.

The deal comes on the heels of Apple's announcement that it is purchasing headphone and music-streaming company Beats for three billion US dollars (£1.78 billion) and is a further acknowledgement of the rise in popularity of streaming and the decline of digital downloads.

US sales of downloaded songs slipped 1% last year to 2.8 billion US dollars (£2.66 billion) while streaming music revenue surged 39% to 1.4 billion US dollars (£832 million), according to the Recording Industry Association of America.

Early results this year showed a further decline in music download sales, Mr Boom said.

"Music consumption habits are changing, which is why we started this," he said. "We saw the change happening."

Amazon will recommend songs to customers who have bought music from it in the past with offers to complete albums if they are available on the service.