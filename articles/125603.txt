Sarah Tew/CNET

Sony's PlayStation 4 console has been steadily racking up sales since its November launch, and it doesn't appear to be slowing down anytime soon. In March, Sony announced that the PS4 had surpassed 6 million units sold globally. Now, as of April 6, that number has climbed to 7 million.

"The PS4 journey has just begun, and although we are still facing difficulties keeping up with the strong demand worldwide, we remain steadfast in our commitment to meet the needs of our customers, and surpassing the wildest expectations of gamers, by delivering new user experiences that inspire and engage," Andrew House, president and CEO of Sony Computer Entertainment, said in a press release.

Sony also announced that PS4 software has reached 20.5 million copies sold worldwide, both through retail and its PlayStation Store, as of April 13.

The PS4 is currently available in 72 countries, with one of the larger additions being Japan, where Sony began selling its console on February 22. By March 4, 370,000 units had been sold in the country -- and likely contributed greatly this month to the next million unit milestone -- even as the PS4 enters its fifth month on the market in the US.

Sony stock remained relatively steady Wednesday at roughly $18.78 per share, up 23 cents, or 1.23 percent, for the day.

Sony's decision to release sales figures comes at an interesting time for the neck and neck state of US next-gen console purchases. The PS4 only barely edged out Microsoft's Xbox One in February in that market, with 258,000 Xbox One units sold that month. That pushed Microsoft ahead of Sony in hardware sales on a dollar basis due to the Xbox One's higher price tag.

And those figures didn't include the critically acclaimed game Titanfall, which was bundled with the Xbox One in the US in March in a startlingly low-cost $499 package that essentially netted buyers a free copy of the title. Microsoft and select regional retailers have both used Titanfall to aggressively push unit sales of the console, especially in markets like the UK.

The Titanfall bundle was originally priced in the UK at 400 pounds, the same price as the console on its own after a surprising region-specific price cut of 30 pounds. Then, just last week, British retailers slashed the price of the Titanfall bundle further, to 350 pounds. The PS4's price has remained firm at $399 in the US and 349 pounds in the UK.

While the PS4 is available in 72 markets, Microsoft is still playing catch up when it comes to availability. The Xbox One is expected to debut in 26 more countries in September of this year, including large swaths of Europe and key markets in the Middle East and Asia including Israel, the UAE, Japan and Korea. A full list can be found here.

Ultimately, how all this cost-cutting affects March sales of the Xbox One -- and whether that means Microsoft will once again outsell Sony in the console market -- will be illuminated by tomorrow's March NPD Group report. Microsoft has yet to disclose lifetime sales of the Xbox One since announcing in January that the console sold 3.9 million units to retailers -- having sold "over 3 million" to customers -- by the end of 2013.