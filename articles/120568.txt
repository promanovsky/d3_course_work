Intel Corp. reported a 4.8% decline in first-quarter profit as the Silicon Valley chip maker continues to share the pain of the shrinking personal computer market.

Revenue edged up 1.5%, and the company's closely watched gross profit margin was 59.7% in the quarter, down from 62% in the fourth period but ahead of Intel's 59% projection.

Shares climbed about 3% after hours on the better-than-feared results.

Brian Krzanich, who was named Intel's chief executive in May, said the company saw solid growth in the data center and signs of improvement in the PC business during the first quarter, adding that the company shipped five million tablet processors, "making strong progress on our goal of 40 million tablets for 2014."

Intel, based in Santa Clara, Calif., supplies most of the chips that serve as calculating engines in PCs and server systems. The company has suffered as customer spending has shifted from laptop computers to tablets and smartphones.

The research firm IDC last week said global PC unit shipments declined 4.4% in the first quarter, while rival Gartner put the decline at 1.7%.

Mr. Krzanich has tried to accelerate Intel's push beyond the PC, vowing to sell 40 million chips for tablets in 2014. He has also emphasized the possibilities presented by wearable devices and other everyday products known collectively as the Internet of Things.

Intel last week announced plans to change its financial reporting structure to break out results for such segments. The latest quarterly figures for the first time include a mobile and communications group--which reflects chip sales for smartphones and tablets--and an Internet of Things Group.

Revenue for the mobile and communications group fell 61% to $156 million, while the Internet of Things Group posted a 32% increase to $482 million.

Still, Intel's most lucrative business lately has been selling chips for server systems, a market that is impacted by the growth of companies selling what the industry calls cloud services. The company said revenue from its data center group rose 11% in the first quarter.

In all, Intel reported net income of $1.95 billion, or 38 cents a share, compared with a profit in the year-earlier period of $2.05 billion, or 40 cents a share. Analysts had expected earnings per share of 37 cents, according to Thomson Reuters.

Revenue climbed to $12.76 billion; Intel in January had projected $12.8 billion, plus or minus $500 million.

For the second quarter, Intel said it expects revenue of $13 billion, plus or minus $500 million, and estimated its gross margin at about 63%. Analysts were looking for revenue of $12.96 billion.

Write to Anna Prior at anna.prior@wsj.com and Don Clark at don.clark@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires