Shares of Gap (NYSE: GPS) have traded in a wide range Thursday afternoon, but are largely unchanged following the company's first-quarter results.

Gap reported quarterly sales increased about 1.2 percent from $3.729 billion in the first quarter of 2013 to $3.774 billion. Analysts consensus estimate suggested the company would report sales around $3.71 billion.

The company saw sales to the Gap Global segment declined slightly from $1.464 billion in the same quarter last year to $1.441 billion. Sales to Old Navy moved higher from $1.459 billion last year to $1.481 billion. Sales to Banana Republic totaled $669 million and sales to the Other unit rose modestly to $183 million.

Revenue through online outlets was up 13 percent year over year to $575 million.

Comps at Gap were down five percent, same-store sales at Banana Republic were down one percent and comps at Old Navy were up one percent. Overall comparable-store sales were down one percent versus a gain of two percent in the same quarter last year.

Gap reported a 22 percent decline in net income from $333 million in the year-ago quarter to $260 million. On a per-share basis, profit totaled $0.58, beating the Street estimate of $0.57.

Gap's Chairman and CEO Glenn Murphy said, "After a disappointing start, I'm pleased with how the business performed toward the end of the quarter, especially at Old Navy. We are confident in our strategies to drive long-term value, as evidenced by the reaffirmation of our full-year guidance."

The company reaffirmed FY14 EPS guidance in the range of $2.90-2.95. Analysts are looking for Gap to report FY EPS of $2.93.

In Thursday's post-market session, shares of Gap last traded at $40.70, down about 0.4 percent from the close.