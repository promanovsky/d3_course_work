Drinking diet beverages may help dieters lose weight, according to a new study, but experts say the findings should be interpreted with caution.

In the study, researchers found that people in a 12-week weight-loss program who drank diet beverages, including diet sodas and diet teas, lost an average of 13 pounds. That was 44 percent more weight than the people in the same program who drank only water; those individuals lost 9 pounds on average.

The researchers also found that 64 percent of the people in the diet-beverage group lost at least 5 percent of their weight, compared with 43 percent of the people in the water group. Losing 5 percent of a person's body weight can help reduce the risk of heart disease, diabetes and other health problems, according to the researchers, who detailed their findings online today (May 27) in the journal Obesity.

However, the beverage industry funded this study, and experts not involved in the research said the findings left unanswered questions.

What did the study participants consume?

"What we don't know from this study is what the overall quality of the diet was," said Judith Wylie-Rosett,a professor at the Department of Epidemiology and Population Health and the Department of Medicine at Albert Einstein College of Medicine of Yeshiva University in New York. [11 Surprising Things That Can Make Us Gain Weight]

Because the study did not evaluate exactly what the participants consumed, and in what quantities, "it answers some questions, but raises a bunch of others, in terms of how it may work in terms of diet beverages versus water," Wylie-Rosett said.

"I think, for people who are trying to lose weight, we need to look at their overall pattern of their whole lifestyle," including their diet and physical activity, she said. And although the researchers addressed the aspect of physical activity in the study, they did not include information about what exactly the people's diets included, she said.

Story continues

Another problem with the study was its short duration, she said.

The study included 303 participants who were drinking at least three diet sodas per week prior to the study. All participants were told to follow the same diet and physical activity plan; the only difference came in what they were assigned to drink, the researchers said.

In addition to losing more weight than the people in the water-only group, the people in the diet-beverage group also reported feeling less hungry during the course of the study.

The American Beverage Association funded the study, and two of the study authors had "received consulting fees from the Coca-Cola Company outside of the submitted work," the study said.

"I was surprised that diet beverages performed better than water," said study author John C. Peters, the chief strategy officer of the Colorado University Anschutz Health and Wellness Center.

"We are just trying to say, 'Don't worry if you're drinking them [diet beverages] while you're trying to lose weight' — it is not going to mess up your appetite, or make you want to eat [a] giant Danish or breakfast roll," Peters told Live Science.

Do diet beverages make you hungry?

Previous research has suggested that drinking diet soda may increase a person's appetite for sweet foods, but the new study did not show this effect, the researchers wrote.

One earlier study, published in the American Journal of Clinical Nutrition, also showed that people who drank diet soda were more likely to lose 5 percent of their body weight than were people who drank water, the new study's authors wrote.

"Certainly, as regards to the effect of diet beverages on weight and food intake, I think that bad rap is not deserved at all, and the science is just not there," Peters said. "Most of that controversy comes from what they call observational studies," which associate being overweight with drinking diet beverages, but do not show cause and effect, he said.

Stephen D. Anton, an assistant professor at the University of Florida's College of Medicine, wrote that more research is "needed to determine if all NNS [non-nutritive sweetened beverages] have similar metabolic effects or if these effects differ between the different types of NNS," in a commentary accompanying the study in the journal.

"Until such data become available, it is advised that NNS are not widely recommended in weight-loss programs, particularly for individuals who do not regularly consume them," he wrote.

Follow Agata Blaszczak-Boxe on Twitter. Follow Live Science @livescience, Facebook & Google+. Originally published on Live Science.

Copyright 2014 LiveScience, a TechMediaNetwork company. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.