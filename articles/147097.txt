Telecommunications titan AT&T on Monday unveiled an initiative to expand super-fast Internet service in the US.

The move came in the face of growing competition from Google, which has been steadily adding US cities to its Google Fiber network offering mega-speedy data transmission.

AT&T said that it is seeking to work with as many as 100 cities or municipalities across the country to put in place Internet service that can move data as fast as a gigabit per second.

AT&T is in the process of putting "GigaPower" service in the Texas cities of Dallas and Austin, and is in advanced talks to do likewise in Raleigh-Durham and Winston-Salem.

"We`re interested in working with communities that appreciate the value of the most advanced technologies and are willing to encourage investment by offering solid investment cases and policies," AT&T home solutions senior vice president Lori Lee said in a release.