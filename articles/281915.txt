The number of diabetic patients in the U.S. has reached 29 million, according to research conducted by the CDC. The study, published in the National Diabetes Statistics Report, 2014, also notes that 86 million people have prediabetes, 15-30 percent of whom will develop type 2 diabetes within five years without the proper lifestyle changes.

Of the 12.3 percent of the adult population with diabetes, the CDC estimates that one in four are unaware of their condition.

"These new numbers are alarming, and underscore the need for an increased focus on reducing the burden of diabetes in our country," said Ann Albright, director of CDC's Division of Diabetes Translation. "Diabetes is costly in both human and economic terms. It's urgent that we take swift action to effectively treat and prevent this serious disease."

The government agency estimates that diabetes and its related complications account for $245 billion in total medical costs and lost work and wages. The figure is up from $174 billion in 2010.

For comments and feedback contact: editorial@rttnews.com

Health News