Meet Earth's cousin, a planet similar to ours

Kepler space telescope finds Earth-size planet in the right kind of zone to support presence of liquid water.