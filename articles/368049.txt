Sony Gina smartphone leak shows front LED flash

It has just been revealed that Sony will be launching the Xperia Z2a “mini” premium smartphone in Taiwan tomorrow. But along with that is word that the company will also be revealing a new smartphone headed for international markets. And this time the mysterious device’s defining feature will be its selfie-friendly front camera.

Selfies are a thing, whether you like it or not, and most will probably fall under the “not” category. People who buy smartphones don’t necessarily choose one for their selfie features, but they are unlikely to shy away from a good device that also caters to the front-facing picture-taking crowd. So yes, a selfie-centric Sony Xperia could still sell, as long as the specs are decent and the price is right.

Unfortunately, not much is known about this surprise smartphone that no one has heard of until now. The only information we do have come as two leaked photos. Fortunately, one of those leaked photos shows a front camera setup that sports what looks like an LED flash. On the other hand, the exposed back doesn’t really spill much information. We do have what is presumably the codename for this device, which is a strange “Gina”.

If it does have such a device at hand, Sony would hardly be the only or the first mobile manufacturer to attempt to appeal to selfie lovers. OPPO’s dual-booting N1 may not have a front-facing camera, but its main 13 megapixel camera, which of course has a flash, can be rotated to face the opposite direction. Chinese manufacturer Meitu, on the other hand, actually does put a 13 megapixel camera on both sides of its Meitu 2 smartphone launched last April. Even compact digital camera makers are including selfie-friendly items in their bullet list of features.

Whether or not Sony does unveil a selfie Xperia smartphone, we will know for sure tomorrow. Perhaps Sony fans across the world are more interested in an Xperia Z2 Compact, which is what the Xperia Z2a practically is. Or perhaps some confirmation that there won’t be one at all so that they can pin their hopes on the rumored Xperia Z3 Compact instead.

VIA: G for Games