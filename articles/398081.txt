A flesh eating bacteria has been the cause of eleven deaths so far in the Tropics, a serious problem caused through contact with warm water by the Florida beach. Unfortunately it’s striking one of the most common pastimes for the Southern U.S..

When people take vacations in the Summer, they usually dream of swimming in the refreshing waters of Florida or California. Countless TV shows and movies have glorified water activities in these states, and it seems only California might be the safe place to do it, but it might be better not to risk it.

Vibrio vulnificus is a cousin to the bacterium behind Cholera, and thrives in warm saltwater environments. It enters the human body usually through open wounds, cuts, or scratches, or even ingestion where it causes even more serious damage. Upon contact with human blood or sensitive areas it begins to break down the tissue.

So far 41 have been infected and eleven have died from the warm water disease as of October 2013. The flesh eating bacteria has been a factor in public health warnings against swimming in the water by the Florida beach.

The Florida Department of Health has issued a statement saying that anyone with an open wound of any kind or a compromised immune system should stay out of the water, and avoid swallowing it. So far the warm water disease hasn’t been considered severe, but there have been enough deaths to warrant precautions against exposure.

Alabama, Louisiana, Texas, and Mississippi have also encountered the bacteria, but so far most cases begin with exposure to water by the Florida beach.

Warm water sparks flesh-eating bacteria warning in Florida: http://t.co/7Nn6mxASvN pic.twitter.com/oK7LHXEcH0 — ABC News (@ABC) July 29, 2014

If you suspect you might have come in contact with Vibrio vulnificus, it is suggested that you ask your doctor for antibiotics to treat the infection. Severe skin infections may require surgery or amputation to stop the spread of the flesh eating bacteria. It is also suggested that any shellfish be cooked and handled with care so the juices don’t cause an infection by themselves.

This warm water disease might not be a major problem, but you don’t want to be the next one who dies or loses a limb to Vibrio vulnificus.

[image via hempbeach]