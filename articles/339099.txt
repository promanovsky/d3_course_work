The size of French bank BNP Paribas's fine for busting US sanctions – $8.9bn (£5.2bn) – has left many people, and not just President Hollande, feeling queasy. Inflation in these penalties is running wild, they cry. Are these US regulators competing with one another to generate headlines? Where's the methodology to calculate a fair fine?

It's certainly true that many banks have been luckier than BNP Paribas. HSBC, it now appears, got a bargain when it agreed a $1.9bn settlement in 2012 for a "blatant failure" of money-laundering controls that was exploited by Mexican drug cartels. HSBC even escaped with a deferred prosecution agreement, despite the fact that its Mexican offences were hardly a momentary lapse: they ran from 2006-10.

Maybe BNP Paribas had HSBC in mind when it originally set its provision at just $1.1bn. The allegations against the French bank, after all, did not involve murderous drug gangs. Rather they related to the concealing of transactions for clients in Sudan, Iran and Cuba, countries where the US had economic sanctions in place.

Yet, just because HSBC got lucky, it does not follow that BNP Paribas is being harshly treated. Viewed through the lens of the sums of money involved, the French bank is in a league of its own. US investigators cite $30bn of illicit transactions, a volume that massively exceeds other cases. Paying a fine of 30 cents in the dollar for every dodgy transaction doesn't seem over the top.

BNP Paribas can also afford it. The bank is being fined slightly less than last year's profits. Settling the bill may mean capital ratios have to be strengthened, but so what? Fines are not meant to be painless and shareholders' dividends are not meant to be sacrosanct.

Another argument, promoted by French politicians, was that a big fine for BNP Paribas would cause lending to the French, and even the wider European, economy to be harmed. That always seem far-fetched.

The only penalty that would have permanently damaged BNP Paribas would have been a ban on clearing dollar transactions. On that score, a pragmatic fudge seems to have been agreed. Culpable units within BNP will be barred, but the suspension will be temporary and BNP will have been given time to put alternative arrangements in place.

A final commonly heard objection is that the US is engaged in extra-territorial reach in jumping on transactions that were merely routed through New York and did not involve US entities and would be legal in, say, France.

But there's a simple answer to this point. The US has made no secret of its desire to enforce financial sanctions. If you're using the US banking system, even just as a transit point for a dollar transaction, you had better be sure you are on safe ground. BNP Paribas can have no complaints.