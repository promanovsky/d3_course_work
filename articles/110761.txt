Music

Beyonce dances onstage as her sister Solange sings 'Losing You', while the No Doubt member and the Canadian star appear during Pharrell and Chance the Rapper's sets, respectively.

Apr 14, 2014

AceShowbiz - Beyonce Knowles' appearance at Coachella on Saturday, April 12 surprised the crowd. The singer, who wasn't listed as a performer at the annual festival, unexpectedly took the stage at the Empire Solo Club in Indio during her sister Solange Knowles' set.

The "Drunk in Love" hitmaker's visit was brief. She joined Solange onstage for a choreographed dance as the latter also sang her "Losing You". Once the song ended, Bey hugged her sister and exited the stage.

Beyonce wasn't the only surprise guest at the second day of Coachella. Her husband Jay-Z joined rapper Nas onstage to help him perform "Dead Presidents II". Hova also played "Where I'm From".

Pharrell Williams, meanwhile, brought out several big names to help him sing some tracks during his set at the Outdoor Theatre. The Grammy Award-winning producer was joined by Nelly for "Hot in Herre", Busta Rhymes for "Pass the Courvoisier Part II" and Snoop Dogg for "Drop It Like It's Hot" and "Beautiful".

Tyler, the Creator and Diplo also joined the star-studded set. However, Gwen Stefani's appearance was probably the biggest surprise. The No Doubt member, who recently gave birth to her third son, showed off her post-baby body in a black sleeveless shirt as she performed her 2005 hit "Hollaback Girl" with Pharrell.

Justin Bieber is another star who made a surprise appearance at the fest. The Canadian hearttrob joined Chance the Rapper during the MC's set on the third day of Coachella on Sunday. The duo performed their collaboration "Confident" which appears in Bieber's "Journal" album.

Beyonce Joins Solange Onstage:

Nas Brings Out Jay-Z:

Pharrell's Star-Studded Set:

Justin Bieber Joins Chance the Rapper for "Confident":