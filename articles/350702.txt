Forget climbing Mount Everest – for most humans, just eking out a living on the harsh Tibetan plateau is challenge enough. But Tibetan people have thrived there for thousands of years and a new study says it is thanks to a genetic adaptation they inherited from an ancient human relative.

The study, published on Wednesday in the journal Nature, identifies a long segment of DNA shared by the extinct people known as Denisovans and modern-day Tibetans. The segment contains the gene scientists think gives Tibetans an edge over lowlanders at high altitudes.

A Tibetan woman and her child posing with their yak in front of the Karuola mountain, south-west of Lhasa. A gene inherited from an extinct human species has helped Tibetans adapt to life in high altitudes. Credit:AFP

No one knew the Denisovans ever roamed the Earth until four years ago, when scientists sequenced the DNA of a finger bone unearthed in a cave in the Altai Mountains of southern Siberia. The genome exhibited similarities to that of modern humans and our extinct Neanderthal relatives, but it was different enough to be considered a distinct species.

Like Neanderthals, Denisovans mated with their human contemporaries, scientists soon discovered. People of Melanesian descent who today inhabit Papua New Guinea share 5 per cent of their genetic make-up with the Denisovans.