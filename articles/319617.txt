NEW YORK - The four-page, hand-written manuscript for Bob Dylan's "Like a Rolling Stone," was sold for $2,045,000 at auction, Sotheby's announced Tuesday.

The draft is written in pencil on four sheets of hotel letterhead stationery with revisions, additions, notes and doodles: a hat, a bird, an animal with antlers. The stationery comes from the Roger Smith Hotel in Washington, D.C.

Dylan was 24 when he recorded the song in 1965 about a debutante who becomes a loner when she's cast from upper-class social circles.

"How does it feel To be on your own" it says in his handwriting. "No direction home Like a complete unknown Like a rolling stone."

Scrawls seem to reflect the artist's experimentation with rhymes.

The name "Al Capone" is scrawled in the margin, with a line leading to the lyrics "Like a complete unknown."

Another note says: "...dry vermouth, you'll tell the truth..."

Sotheby's described the seller as a longtime fan from California "who met his hero in a non-rock context and bought directly from Dylan." He was not identified.

The auction house says it is "the only known surviving draft of the final lyrics for this transformative rock anthem."

The manuscript was offered as part of Sotheby's rock and pop music sale.

In 2010, John Lennon's handwritten lyrics for "A Day in the Life," the final track on the Beatles' classic 1967 album "Sgt. Pepper's Lonely Hearts Club Band," sold for $1.2 million, the record for such a sale.

"When you look through the four manuscripts, you see numerous edits and changes," Richard Austin, head of Sotheby's Books and Manuscripts Department, told "CBS This Morning." "Bob Dylan manuscripts are incredibly sought after. Not just because of his role as a rock icon, but also he's now seen as a serious poet."

Dennis McDougal, author of "Dylan: The Biography," says Dylan changed the language of music. He even compares these lyrics to Shakespeare.

"The lyrics are phenomenal," McDougal told CBS News. "What he did in terms of the shear poetry of that song has never been duplicated. How much would you pay for an original working draft of King Lear?

In 2004, Dylan sat down with the late Ed Bradley on "60 minutes" and reflected on his songwriting during the 60's.

"I don't know how I got to write those songs," Dylan said. "Those early songs were like almost magically written."