A clip from the upcoming adaptation of the best-selling YA novel, centering on two teens in a cancer support group, premiered before the MTV Movie Awards.

A clip from The Fault in Our Stars shows the first spark between Hazel and Augustus -- even though they never light a fire.

PHOTOS: Photos of 'Divergent,' 'Fault in Our Stars' Actor Ansel Elgort

The clip aired just before the MTV Movie Awards on Sunday, with Shailene Woodley and Ansel Elgort (who play siblings in the Divergent series) introduced the scene while on the red carpet at the Nokia Theater in Los Angeles.

In the scene, Augustus (Elgort) asks Hazel (Woodley) out to a movie after a terminal-illness support group meeting for teens. When she agrees and suggests a time later in the week, he insists, "I mean now."

"You could be an ax murderer!" she tells him, to which he responds, "There's always that possibility! Come on, Hazel Grace, take a risk!" Augustus then takes out a cigarette and lets it linger between his lips -- a visual that offends Hazel, who has thyroid cancer that has spread to her lungs.

"Really? That is disgusting. Do you think that that's cool or something? You just ruined this whole thing!" she shouts at him. "Even though you have freakin' cancer, you're willing to give money to a corporation for the chance to acquire even more cancer? Let me just tell you that not being able to breathe sucks."

VIDEO: Next Big Thing: 'Divergent,' 'Fault in Our Stars' Actor Ansel Elgort

But Augustus tells her that his fidgeting with the cigarette is just a metaphor. "They don't actually hurt you unless you light them," he explains. "You put the thing that does the killing right between your teeth, but you never give it the power to kill you.

The Fault in Our Stars hits theaters June 6. Watch the clip below:

Email: Ashley.Lee@THR.com

Twitter: @cashleelee