Poor Harrison Ford. His broken ankle that he sustained on the set of Star Wars: Episode VII last week is much worse than Disney reported.

Photo credit: FayesVision/WENN.com

Harrison Ford is laid up for eight weeks from his Star Wars: Episode VII injuries last week. That news is much worse than originally reported by Disney after the actor was hit by the door of the Millennium Falcon, the spacecraft piloted by his character, Han Solo.

This will affect the movie’s shooting timeline, but according to The Mirror, they are working it out.

A source told the U.K. publication, “The initial day or two after the accident it was hard to move things around but now we have had more time; we can juggle things, bring some scenes forward and push others back. There are lots of other actors in the movie so it is not a disaster. We are still confident the film can stay on schedule.”

It’s possible that the Indiana Jones star would be off the set for up to “six to eight weeks.” It is important for Disney to keep the movie on track and on schedule because any delays could cost the studio hundreds of thousands of dollars a day.

Ford’s son, Ben Ford, also revealed to Access Hollywood that surgery might be required to help repair his ankle. The star might need a plate and screws.

Ford’s wife, Calista Flockhart, is reportedly on her way to London to be by her husband’s side while he recovers.

A studio spokesperson said on Monday that they were “hoping to announce some good news soon.”

Here’s to a speedy recovery for Han Solo.