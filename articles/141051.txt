Changing the way babies are held directly after birth could encourage the use of delayed cord clamping; this could significantly reduce the number of babies suffering from iron deficiencies.

Waiting a minimum of two minutes after birth to cut the umbilical cord allows more blood to pass through from Mom's placenta to baby, lowering the risk of iron deficiency, HealthDay reported.

Today's guidelines suggest the baby be held at the level of their mother's placenta directly after birth, but this position can be uncomfortable for the person holding the newborn which could encourage them to clamp the cord sooner.

To make their findings the researchers looked at 197 babies who were held in the recommended position and 194 who were placed directly on their mother's chest after birth. Both groups of babies had similar placenta blood transfer, meaning the position the baby was held in after birth had little to no effect.

"Iron deficiency in newborn babies and children is a serious public health problem in low-income countries, and also prevalent in countries from North America and western Europe," lead author Nestor Vain, of the Foundation for Maternal and Child Health in Buenos Aires, said in a journal news release, HealthDay reported.

The study makes a strong case for delayed umbilical clamping

"Because of the potential of enhanced bonding between mother and baby, increased success of breast-feeding and the compliance with the procedure, holding the infant by the mother immediately after birth should be strongly recommended," he said.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.