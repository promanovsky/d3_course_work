Google has found a partner to develop technology that literally sits on your eyeball.

Alcon, the eye-care division of healthcare company Novartis, has licensed the Glass maker's latest invasive innovation, a smart lens.

The plan is to develop and commercialize the tech that looks like a Tron-inspired contact lens for ocular medical uses.

Questions of privacy are bound to crop up whenever Google is involved, but so far the rhetoric is focused solely on advancing the treatment of eye conditions and other diseases.

Inner eye

Novartis outlined in a press release that "Google's key advances in the miniaturization of electronics" meshed with its "deep pharmaceuticals and medical device expertise" create a perfect tech cocktail that can help map diseases in the human body and, one day, prevent them.

Specifically, Novartis plans to use the smart lens developed in collaboration with Google to help diabetics manage their disease. The lens will provide continuous and minimally invasive glucose level monitoring by measuring tear fluid and wirelessly sending information back to a mobile device, Novartis described.

The company also wants to use the lenses to help people with presbyopia, a condition that causes the eye lens to lose its ability to focus and makes it difficult to see objects up close, such as text on a page.

"The 'smart lens' has the potentially to provide accommodative vision correction to help restore the eye's natural autofocus on near objects in the form of an accommodative contact lens or intraocular lens as part of refractive cataract treatment," Novartis wrote in the release.

If the thought of computer chips situated on your eyeball makes you squeamish, the lenses are built with non-invasive sensors, microchips and other tiny electronic components that, in theory, shouldn't cause undue complications or discomfort.

There's no word on when the smart lens will be available or for how much. When asked to provide these details, an Alcon spokeswoman responded: "We are currently in the early stages of developing the 'smart lens' technology for us in eye care and cannot speculate on timing for commercialization."