Technology and small-cap stocks ensured a positive finish for benchmarks on Friday. However, gains were kept in check following a drop in DuPont's stock. Shares of the company declined after it cut its operating profit forecast for the second quarter and the full year 2014. Markets also received positive economic data on consumer sentiment. Benchmarks had a mixed finish for the week. The Nasdaq gained but the Dow and the S&P 500 ended in the red.

For a look at the issues currently facing the markets, make sure to read today's Ahead of Wall Street article

The Dow Jones Industrial Average (DJI) gained a meager 0.03% to close Friday's trading session at 16,851.84. The Standard & Poor 500 (S&P 500) advanced 0.2% to finish at 1,960.96. The tech-laden Nasdaq Composite Index closed at 4,397.93; gaining 0.4%. The fear-gauge CBOE Volatility Index (VIX) dropped 3.2% to settle at 11.26. Total volume for the day was roughly 8.9 billion shares, lower than this month's average of 5.6 billion. Advancers outpaced declining stocks on the NYSE. For 65% stocks that advanced, 32% declined.

A post noon rally by technology stocks helped benchmarks end in the green. Tech-bellwethers such as Apple Inc. (NASDAQ: AAPL ), Microsoft Corporation (NASDAQ: MSFT ), Verizon Communications Inc. (NYSE: VZ ), AT&T, Inc. (NYSE: T ), International Business Machines Corporation (NYSE: IBM ), Google Inc. (NASDAQ: GOOGL ) and Oracle Corporation (NYSE: ORCL ) gained 1.2%, 1.3%, 0.2%, 0.4%, 0.7%, 0.2% and 0.9%, respectively. Overall the Technology Select Sector SPDR (XLK) advanced 0.6%, the highest among the S&P 500 sectors.

Further, small-cap stocks outperformed the broader markets. The Russell 2000 index of small-cap stocks added 0.7% on Friday. The trading volume was 161% higher than the 30-day average. The spike in trading volume on Friday was due to the indexes final reconstruction that affected more than $5 trillion in assets. The index adjusts its components once every year.

Meanwhile, E. I. du Pont de Nemours and Company (NYSE: DD ) was the biggest drag on both the Dow and the S&P 500. The company registered its biggest loss since October 2012. Shares of the company dropped 3.3% after it reduced its forecasted operating profit for both the second quarter and the full year. The company expects operating earnings in the second quarter to be moderately below the $1.28 per share recorded in the same period last year. As a result, the company also expects its full-year operating earnings to be $4.00-$4.10 per share, less than the earlier forecast of $4.20-$4.45 per share. DuPont cited lower than expected quarterly performance of its agriculture segment to be the primary reason behind this downward revision of operating earnings per share for the second quarter and full year 2014.

On the other hand, Keurig Green Mountain, Inc. (NASDAQ: GMCR ) was the biggest gainer among the S&P 500 components. Shares of the K-cup coffee pod maker surged 4.1% after Argus Research upgraded its rating from "hold" to "buy".

Economic data on consumer sentiment was encouraging. The University of Michigan and Thomson Reuters' final reading of consumer sentiment increased in June. The gauge was at 82.5 in June, up from the earlier estimate of 81.2 this month. The rise in consumer sentiment was also more than the consensus estimate of an increase to 81.9. This rise in consumer sentiment in June came in after consumer spending rose in May.

For the week, benchmarks finished mixed. The blue-chip index and the S&P 500 dropped 0.6% and 0.1%, respectively. However, the tech-heavy Nasdaq Composite Index gained 0.7%; recording its second successive weekly gain.

Benchmarks ended mostly in negative territory for the week following St. Louis Fed president James Bullard's comments on the timing of a rate-hike. Bullard said that he expects the central bank may hike interest rates by early 2015. The Federal Reserve official mentioned that a rise in interest rates may happen sooner than anticipated as unemployment rate falls and inflation increases at a faster pace. He believes U.S jobless rate will fall below 6% and inflation is likely to hit its 2% target later this year.

Additionally, escalating tension in Iraq had a negative impact on benchmarks. U.S. Secretary of State John Kerry had met officials from Iraq's semi-autonomous Kurdish region capital of Arbil on Tuesday. Kerry said: "This is a very critical time for Iraq and the government formation challenge is the central challenge that we face." He added Kurdish forces were "really critical in helping to draw a line with respect to ISIL."

Separately, oil refinery stocks declined after The Wall Street Journal reported that the Commerce Department loosened the nearly 40 year ban on crude oil exports. The industrial sector was also adversely affected by initial opposition from the French government over General Electric Company's (NYSE: GE ) efforts to acquire Alstom SA's energy assets.

Among the positives, broadcaster stocks rose on Wednesday after Supreme Court ruled in their favor over start-up online-video service provider, Aereo Inc. Further, new deals between Wisconsin Energy Corp. (NYSE: WEC ) and Integrys Energy Group, Inc. (NYSE: TEG ), and Oracle Corporation (NYSE: ORCL ) and MICROS Systems, Inc. (NASDAQ: MCRS ) were welcomed by investors.

On the economic front, personal consumption expenditure was weaker-than-expected. However, personal income and claims for unemployment benefits were in line with expectations. Positive economic data on existing home sales, new single-family home sales and an improvement in consumer confidence failed to drive benchmarks higher. At the same time, investors ignored the biggest contraction of the U.S. economy in the first quarter since early 2009 and also overlooked an unexpected fall in durable goods orders.

Coming back to Friday, The SPDR S&P Homebuilders (XHB) gained 0.5%, the second highest among the S&P 500 sectors. Key housing stocks from the sector such as Lennar Corp. (NYSE: LEN ), Toll Brothers Inc. (NYSE: TOL ), Beazer Homes USA Inc. (NYSE: BZH ) and PulteGroup, Inc. (NYSE: PHM ) increased 0.7%, 0.1%, 2.5% and 1.2%, respectively.

Want the latest recommendations from Zacks Investment Research? Today, you can download 7 Best Stocks for the Next 30 Days. Click to get this free report

APPLE INC (AAPL): Free Stock Analysis Report

MICROSOFT CORP (MSFT): Free Stock Analysis Report

VERIZON COMM (VZ): Free Stock Analysis Report

AT&T INC (T): Free Stock Analysis Report

INTL BUS MACH (IBM): Free Stock Analysis Report

GOOGLE INC-CL A (GOOGL): Free Stock Analysis Report

ORACLE CORP (ORCL): Free Stock Analysis Report

DU PONT (EI) DE (DD): Free Stock Analysis Report

KEURIG GREEN MT (GMCR): Free Stock Analysis Report

GENL ELECTRIC (GE): Free Stock Analysis Report

WISC ENERGY CP (WEC): Free Stock Analysis Report

INTEGRYS ENERGY (TEG): Free Stock Analysis Report

MICROS SYS (MCRS): Free Stock Analysis Report

LENNAR CORP -A (LEN): Free Stock Analysis Report

To read this article on Zacks.com click here.

Zacks Investment Research

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.