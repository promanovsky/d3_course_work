Movie

Goddard is also involved in the scriptwriting for the Spider-Man spin-off which is expected to be teased in 'The Amazing Spider-Man 2'.

Apr 8, 2014

AceShowbiz - "The Sinister Six" has apparently got a helmer. The Hollywood Reporter learns that the Spider-Man spin-off that will bring together the biggest villains in Spidey's universe will be directed by Drew Goddard who also wrote the screenplay.

When Goddard was first tapped as a writer for "Sinister Six", there was speculation that he might serve behind the lens as well. Now it seems those plans fall into place, as "The Cabin in the Woods" helmer is nearing a deal to sit at the helm.

Another Spidey spin-off, "Venom (2017)", is also in the works. Alex Kurtzman is attached to direct and work with Roberto Orci and Ed Solomon to pen the script. Sony's chief, Amy Pascal, once said they expanded the franchise so that they could have a Spidey movie every year.

In the meantime, "The Amazing Spider-Man 2" is due May 2. It will be followed by "The Amazing Spider-Man 3" on June 10, 2016 and the fourth one on May 4, 2018. The second movie, which has Electro (Jamie Foxx), Goblin (Dane DeHaan), and The Rhino (Paul Giamatti), is set up as an introduction to The Sinister Six.