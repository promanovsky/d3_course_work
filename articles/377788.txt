Air travel could become even more uncomfortable should the latest seat designs from Airbus ever see the light of day.

The aircraft manufacturer has sought a patent for the seat (pictured above), which resembles a bicycle saddle, in an apparent effort to cram more passengers on board.

The seats, which fold away when they’re not being used, would offer “reduced bulk, for example in an aircraft”, according to the patent application, which was published last month .

The device is designed for short flights, it states, rather obviously considering tray tables are non-existent, there is no-sign of in-flight entertainment, and seats don’t appear to recline. When several rows can be seen (pictured below), the cabin begins to resemble the inside of a galley.

"Reduced comfort remains tolerable for the passengers in as much as the flight lasts only one or a few hours," adds Airbus. “It is now no longer possible to further reduce the seating width, particularly in economy class,” concedes Airbus. “It is [also] difficult to continue to further reduce [the] distance between the seats because of the increase in the average size of the passengers.”

Therefore, to increase the number of cabin seats, “the design of the seats has to be optimized so that they present the smallest possible bulk”, it says.

Would you sit on one of these seats, if it meant a cheaper fare?

However, a spokesman for Airbus told Telegraph Travel that it was "unlikely" the seats would ever be used on passenger planes. "We file hundreds of patents each year and it’s all about protecting ideas," he added. "We are actually pushing airlines for an 18-inch seat width to come as standard on long haul flights – some aircraft have only 16.7 inches."

Airlines are constantly looking at ways to save space, and a similar design – albeit with a more substantial backrest – was unveiled in 2010. The Skyrider (pictured below), by Italian design firm Aviointeriors Group, offered fliers 23 inches of legroom - around seven inches less than the 30 inches typically offered by budget airlines.

The Skyrider

Ryanair has previously suggested that standing-only sections could help it squeeze more passengers inside its planes, and a recent study by the International Journal of Engineering and Technology suggesting the removal of seats could allow airlines to cut fares by up to 44 per cent.

As Telegraph Travel has previously reported, space inside plane cabins is already shrinking fast. The normal seating arrangement on a Boeing 777, for example, was once nine seats per row. In 2010, 15 per cent of the 74 777s delivered to airlines could seat 10 abreast. Last year, that leapt to 69 per cent.

This trend will only continue. At the Aircraft Interiors Expo in Hamburg last April, Airbus revealed plans to squeeze an extra 35-40 seats into its A380, while a Florida-based manufacturer of aircraft interiors recently unveiled designs for a smaller plane loo for the Boeing 737 that would allow airlines to squeeze in four more seats.

Win one of 40 holidays worth £800,000

Telegraph Travel Awards 2014: vote for your favourite destinations and travel companies for the chance to win one of 40 luxury breaks worth a total of £800,000.

Travel Guides app

Download the free Telegraph Travel app, featuring expert guides to destinations including Paris, Rome, New York, Venice and Amsterdam

Follow Telegraph Travel on Twitter

Follow Telegraph Travel on Facebook

Follow Telegraph Travel on Pinterest

Follow Telegraph Travel on FourSquare