Hormel Foods Corp (HRL) on Monday agreed to acquire CytoSport Holdings Inc, maker of Muscle Milk products for about $450 million, in a move to expands its offerings of portable, immediate, protein-rich foods.

Austin, Minnesota-based Hormel expects the deal to provide about 5 cents per share accretion in fiscal 2015, with a neutral impact to fiscal 2014 earnings.

The deal is expected to close within 30 days.

As a provider of premium protein products in the sports nutrition category, CytoSport's brands align with the company's focus on protein while further diversifying the Hormel Foods portfoliom, the company said.

Based in Benicia, California, CytoSport was founded in 1998 by the Pickett family, and produces Muscle Milk products, a leading brand in the ready-to-drink protein beverage category. Total 2014 annual sales are expected to be about $370 million.

For comments and feedback contact: editorial@rttnews.com

Business News