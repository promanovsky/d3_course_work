Conan O'Brien featured Ubisoft's upcoming title "Watch Dogs" during his latest segment of "Clueless Gamer."

O'Brien trolled the virtual city of Chicago, shooting at trees and accidentally shooting a woman on a boat he was interested in stealing. The gameplay footage teased different elements of the hacker-inspired game, including the title character Aiden Pearce's hideout where he can access all of Chicago. Check out O'Brien's hilarious gameplay walkthrough in the video below.

Though O'Brien's walkthrough didn't really show this, it's important to note your actions as Aiden will affect your media and public perception. There is a new morality meter for the title and it's unlike anything you've experienced before.

"The media will still report on you, but maybe not in such a negative tone," Lead Writer Kevin Shortt told Ubisoft's blog. "It will be more questioning. Is he a good guy? Is he a hero? Is he a terrorist? They are going to start raising these kinds of questions."

"The reputation system isn't really a good-versus-bad kind of system," Shortt explained. "We really wanted it to just be the citizens reflecting back on you and what you're doing so that you think about it more. The game doesn't suddenly tilt one way if you get a bad reputation. It doesn't make it exponentially harder. It should just make you consider your actions and what you're doing."

Everything is connected in "Watch Dogs." Creative Director Jonathan Morin recently explained to Ubisoft's official blog how connected a gamer is inside the "Watch Dogs" world.

"Every fuse box you can use in combat will help you turn the tide. All those elements come together so that when the player puts the controller down he or she can say, I really like this," Morin said. "And when they pick up the controller to play another game, they try to hack the traffic lights, but that option's not there anymore."

"Watch Dogs" will be released for the PS4, PS3, Xbox One, Xbox 360 and PC on May 27.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.