GoPro cameras give viewers and sports enthusiasts a unique point-of-view when it comes to viewing extreme sports. (Credit:GoPro)

NEW YORK, June 26 (UPI) -- Camera maker GoPro is having a dream debut on NASDAQ Thursday after opening more than 30 percent higher than its $24 IPO price.

GoPro, which makes portable video cameras favored by extreme athletes, was trading at $31.03 at 1:15 p.m. E.T., up $7.03 or 29.33 percent. The company announced last night that the stock would debut at the high end of its IPO range, $24, resulting in the company raising $427 million.

Advertisement

The San Mateo-based company sells super-portable cameras that can shoot unique point-of-view video. GoPro cameras cost anywhere between $199.99 to $399.99 and the company sold 3.8 million cameras last year.

The company's initial public offering raised $2.96 billion and comes nearly a decade after founder Nicholas Woodman, a surfer, started the company. GoPro has been a profitable company, even though its profit margin reduced to to 37 percent last year from 43 percent in 2012.

GoPro distributes the media created by its customers on poplar social media sites, including Facebook, Twitter and Instagram. On Youtube itself, GoPro videos have accumulated 500 million views collectively. While the company hasn't charged for any of its media so far, they plan to generate some revenue from advertising through partnerships with Microsoft's Xbox Live, Youtube and Virgin America.

Including GoPro's IPO there have been 141 initial public offerings so far this year, nearly 56 percent more than the same period last year, according to Renaissance Capital.