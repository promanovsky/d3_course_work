Australian authorities who were tasked with searching the southern Indian Ocean for the missing flight MH370 have sent four aircraft to investigate at least two objects detected by satellite far off the Australian coast.

What do we know about the objects?

The Australian prime minister, Tony Abbott, said specialist analysis of satellite imagery had identified "two possible objects related to the search". Australia's Defence Imagery and Geospatial Organisation, which examined the images, found that the largest object was 24 metres long. Another was about five metres long, according to the analysis.

Where have they been detected?

The Australian maritime safety authority (Amsa) said the area was about 1,500 miles (2,500km) south-west of Perth, Western Australia. A map provided by Amsa shows this location is east of a possible MH370 route as assessed by the US national transportation safety board.

Australian government map of the area being searched for wreckage from flight MH370. The yellow arrows show the course believed to have been followed by the plane, according to the National Transport Safety Bureau in the US. Photograph: Australian government

What don't we know?

We still do not know what the objects are and whether they are related to the MH370 disappearance. The two grainy satellite images that have been released do not clearly show what the objects could be.

How sure are authorities that the objects might be related to MH370?

Air Commodore John McGarry, a senior Australian military official, said the satellite material was credible enough to divert Australian-led search efforts to this area. Amsa's emergency response general manager, John Young, said it was possible the objects could be debris unrelated to MH370, such as containers fallen overboard from ships. "On this particular occasion the size and the fact that there are multiple [objects] located in the same area really makes it worth looking at," Young said. Abbott said the search was based on "new and credible information" but cautioned that the objects may not be connected with the Malaysia Airlines disappearance.

What is happening with the search now?

An Australian air force Orion was sent to the area on Thursday, to be followed by one from New Zealand, a US navy P8 Poseidon aircraft and another Australian Orion. . An AQustralian C-130 Hercules aircraft has been sent to the area to drop datum marker buoys, which will provide information about water movement so the drifting of the objects can be modelled. A merchant ship was expected to arrive in the area on Thursday evening, Australian time. An Australian navy ship, HMAS Success, is also on its way, but the journey will take a few days. HMAS Success is equipped to recover any objects located and proven to be from MH370.

Have they found the objects yet?

Authorities have yet to announce any sighting of the objects. Amsa said on that Thursday weather conditions were moderate, with visibility reported to be poor.