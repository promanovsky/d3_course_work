You should expect a little more color from your favorite professional baseball team this Sunday afternoon.

For the ninth consecutive year, a number of major leaguers will swing pink bats on Sunday in honor of Mother's Day, part of a league-wide initiative to raise money for breast-cancer research.

See also: 100 Photos of Remarkable Moms in New York

Major League Baseball partners with Louisville Slugger each year to make the bright pink bats, which are donated to players and then auctioned off to fans as a way to raise money for cancer research. Since its inception, Louisville Slugger has raised more than $1 million for breast-cancer research.

This year, thanks to a rule change, players who partner with other bat manufacturers, such as MaxBat or Mizuno, also have the green light to swing pink bats this weekend.

The new rule is the result of a scenario that took place last year when players who partnered with MaxBat were told they couldn't not use black bats with pink logos on Mother's Day. The reasoning was that MLB had an exclusive partnership with Louisville Slugger on the pink bats, which the company donates to players.

This monopoly on pink bats didn't go over with players who wanted to participate in the cause. After the league threatened to fine MaxBat if its players used the pink-logo bats, Minnesota Twins third baseman Trevor Plouffe, whose mother is a breast-cancer survivor, planned to go to MLB's Player's Union to fight the issue, according to FOX Sports.

The rule change occurred before that happened, and now anyone can use a pink bat regardless of which company creates it.

In a statement given to Mashable, Louisville Slugger, which makes 400 to 500 bats per year (seen, above), supported the new rule:

"While we initiated the program and have contributed more to the Mother's Day breast cancer program than all other bat companies combined, Louisville Slugger fully and wholeheartedly supports MLB's decision. We want to make sure the focus stays on the fight against breast cancer. If even one life can be saved through the MLB Mother's Day efforts then that trumps everything else."

One of the many players expected to use the pink hardware on Sunday is Tampa Bay Rays third baseman Evan Longoria. The three-time All Star and 2008 Rookie of the Year winner uses the bats every year, and has no problem replacing his customary beige Louisville Slugger with a pink one on Mother's Day.

"Being a Major League Baseball player, it's kind of our responsibility, given the platform that we have, to raise awareness for all types of things, whether it be prostate cancer in men or breast cancer in women," he told Mashable this week. "It's a great thing."

Brandon Crawford of the San Francisco Giants preps his pink bats last Mother's Day before taking on the Arizona Diamondbacks. Image: Paul Connors/Associated Press

MLB also selects "Honorary Bat Girls" for each team, a tradition that began in 2009 as part of a partnership with Susan G. Komen for the Cure, a foundation dedicated to defeating breast cancer. The winners of the contest — who are all breast-cancer fighters or survivors — are hand-selected by a group of judges who look at categories, including "originality" and "commitment to the cause," to determine the winners.

Longoria was one of this year's judges, and said the selection process was harder than he expected.

"All of these women are pretty spectacular in their own right," he said. "It was pretty hard to go through this list, and read the stories and what these women have gone through, and what they're trying to do now to not only try and better themselves and their families, but the lives of other people."

The rule change aims to direct attention away from the business of making pink bats to the reason they're made in the first place: breast-cancer awareness. Longoria, who said he saves one of the bats to give to his mother each year, is keeping things in perspective.

"I'm the oldest of four, so [my mom] had her hands full, and I know now — having a 15-month-old myself — how tough it can be to raise one, let alone four," he said. "Everyday I thank God for giving me her and the life she gave me."