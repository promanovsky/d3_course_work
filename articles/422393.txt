Scientists have successfully manipulated the brains of mice to remember unhappy experiences in a positive way, in a study which reveals the extent to which our emotional recollections of life may be “reversible”.

In a novel experiment, researchers were able to identify which parts of the brain were involved in forming emotional responses to remembered places or events.

By manipulating the brain cells of the mice, they were able to eradicate a fearful memory associated with a particular part of their enclosure, and were also able to associate fear with an area the mouse had previously linked with pleasant experiences.

The emotions that we attach to events in our past are one the key pillars of our sense of self, but can also be the root of serious psychological problems such as post-traumatic stress disorder (PTSD).

Scientists from Massachusetts Institute of Technology (MIT) and Japan’s RIKEN Institute, who carried out the study, said that the insights gained into the memory-forming in the brain could one day help develop drugs to treat people suffering from painful memories.

Our memories are believed to be formed by two separate parts of the brain. Information about a memory’s context, such as where and how something happened, are stored in cells of the hippocampus. Emotions linked to that memory are stored in the amygdala.

In 'Eternal Sunshine of the Spotless Mind', Kate Winslett and Jim Carrey medically erase each other from their memories (AP)

Usually, the learning process allows links between these two areas of the brain to form. In the study, one group of mice were exposed to a mild electric shock in a new part of their enclosure, while another group was given a pleasant experience – in this case: “socialising with a female mouse”.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

Scientists were able to identify the neurons that were active during the forming of each memory. These same neurons were then manipulated using light – a technique called optogenetics which uses light-sensitive organisms to control cell activity.

Mice which had experienced the electric shock would initially avoid an area of their cage where the lighting was set to “activate” the negative memory, while mice which had been conditioned with the pleasant memory spent more time in the area where that memory was activated.

Two days later, the scientists set out to reverse the mice’s emotional response, by exposing them to negative and positive experiences again, but this time while activating areas of the brain associated with the opposite emotional response.

This time, mice which had avoided the negative area of their cage spent more time there and those which had sought out the positive area of theirs now avoided it. In essence, the mice’s memory of what was pleasant and what was unpleasant had been reversed.

The study, which is published in the journal Nature today, is the closest scientists have yet come to the kind of memory manipulation explored in films like Eternal Sunshine of the Spotless Mind and Inception.

“In the future, one may be able to develop methods that help people to remember positive memories more strongly than negative ones,” said Susumu Tonegawa, director of the RIKEN-MIT Center for Neural Circuit Genetics and the senior author of the paper.

However, Dr Anders Sandberg, neuroscientist and ethicist at the University of Oxford’s Future of Humanity Institute told The Independent that, in the future, any similar techniques would have to be used with caution.