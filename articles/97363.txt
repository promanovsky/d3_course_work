The media is abuzz with talk about a widespread security vulnerability dubbed "Heartbleed" that by some estimates has impacted up to 66% of Internet sites. We here at The Motley Fool wanted to let you know what we have already done to secure our site, the additional measures we're taking, and how you can protect yourself out there on the wider Internet.

The Fool's sites are no longer vulnerable to Heartbleed. Major Internet players like Amazon Web Services and Incapsula, both services that we use to deliver Fool websites, were vulnerable. Both of these services, as well as the other services we use, have updated their software and are no longer vulnerable. This was done within hours of the public announcement of the Heartbleed vulnerability. We have taken the additional precaution of re-issuing our private keys as a proactive measure in order to better ensure the safety of Fool data.

While we have no reason to believe that any of our data has been compromised, part of the nature of Heartbleed is that it is largely undetectable. To better protect yourself, we strongly encourage all users of Fool.com to reset their password now. Our members who access more secure parts of our site will be required to change their password before logging in again. We also highly recommend that you change your password on any site with which you share your personal information. Before you do, you'll want to make sure that those sites have been updated and are now secure.

More information on Heartbleed

Heartbleed is a security vulnerability in encryption software that powers a large portion of the Internet. It can be used to retrieve a website's private key, a major component in encrypting secure Internet traffic.

In the wrong hands, this could be exploited to get usernames or passwords. Thankfully, utilizing this vulnerability to get secure information would have been difficult due to the random nature of the exploit. While vulnerable to Heartbleed, someone would need to continually request an SSL heartbeat over a period of time to collect random segments of secured data. The attacker would then hope that the collected random segments would contain usernames and passwords.

You can read more about Heartbleed here.



