Dr. Oz has been taken to task over his choice of words regarding various diet tips and tricks. The Senate put the television doc in the hot seat Tuesday, grilling him about “miracle” promises.

Dr. Oz’s diet advice was the hot topic at a hearing Tuesday, during which senators questioned the television star about his claims that garcinia cambogia, green coffee extract and/or raspberry ketone are the magic weight loss element so many people have been hoping for.

Senator Claire McCaskill led the critique of the cardiothoracic surgeon by sharing, “I get that you do a lot of good on your show, but I don’t get why you need to say this stuff, because you know it’s not true.”

Using the star of The Dr. Oz Show‘s own words as examples of misleading promises, McCaskill continued, “Now, here’s three statements you made on your show: ‘You may think magic is make-believe, but this little bean has scientists saying they’ve found the magic weight loss cure for every body type — it’s green coffee extract.’ Quote: ‘I’ve got the No. 1 miracle in a bottle to burn your fat. It’s raspberry ketone.’ Quote: ‘Garcinia cambogia — it may be the simple solution you’ve been looking for to bust your body fat for good.'”

For his part, Dr. Oz insisted he stands by his recommendations but made it clear he’s not actually endorsing any products for profit.

“If you see my name, face or show in any type of ad, email, or other circumstance,” he testified, “it’s illegal.”

“I actually do personally believe in the items that I talk about on the show,” Dr. Oz added. “I passionately study them. I recognize that, oftentimes, they don’t have the scientific muster to pass as fact… My job, I feel, on the show is to be a cheerleader for the audience. And when they don’t think they have hope, when they don’t think they can make it happen, I want to look everywhere, including at alternative healing traditions, for any evidence that might be supportive to them.”

Do you think Dr. Oz went too far with the way he talked about weight loss?