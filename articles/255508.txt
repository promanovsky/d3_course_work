[Update: Intuit has announced its acquisition of the mobile payment and bill management services company, Check, in a $360 million deal.]

We had reported earlier Tuesday that the deal would be around $350 million, according to people with knowledge of the transaction.

A report from The Wall Street Journal published yesterday placed the deal at $360 million. And the WSJ had reported last month that the two companies were in discussions.

A provider of mobile account management and bill payment software, Check joins a roster of financial services software offerings at Intuit that provide the technological backbone for billing, payroll, accounting, and financial management at small businesses across the U.S.

Intuit’s bid for Check is just the latest in a string of acquisitions for Intuit, which has been a longtime acquirer of early stage technology companies.

Check is Intuit’s ninth acquisition this fiscal year, according to a spokeswoman. The company joins recently acquired technology vendors like invitco, Lettuce, CustomerLink, Docstoc, Good April, Full Slate, Level Up Analytics and Prestwick Services.

Earlier deals, like the $170 million acquisition of Mint.com in 2009, bolstered a clutch of financial services tools that include Quicken, QuickBooks, and TurboTax.

With the acquisition of Check, formerly known as Pageonce, Intuit adds another mobile payment solution to its arsenal in a computing world that’s increasingly defined by mobile computing.

The Mountain View, Calif.-based company already sells a mobile payment processing technology for small businesses. Check adds a consumer-facing payment solution for bills that aligns with Mint.com’s financial management toolkit.

As of January 2013 the company was already experiencing torrid growth, and now boasts 10 million active users transacting roughly $500 million in payment volumes per year.

Check previously raised $47 million from investors including Pitango Venture Capital and Morgenthaler Ventures.

Photo via Flickr user Morgan.