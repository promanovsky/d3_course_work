Sony Corp.’s PlayStation 4 beat Microsoft Corp.’s Xbox One to first place in U.S. console sales, extending its lead for a fourth month.

“PlayStation 4 was the No. 1 platform for hardware sales in April,” followed by Xbox One, Liam Callahan, an analyst at NPD Group Inc., said in an e-mailed statement Thursday.

Customers continue to snap up the newest generation of consoles, according to Port Washington, New York-based NPD. The gain in hardware revenue in April boosted overall U.S. video-game sales even as software sales dropped.

Microsoft will start selling a cheaper version of its Xbox One in June, shaving $100 off the $499 price to match Sony’s PlayStation 4, as it tries to claw back the Tokyo-based company’s lead. For the Sony, the success of the PS4 is a bright spot for the company that forecast its sixth annual loss in seven years this week as it contends with slumping sales of TVs and PCs.

U.S. hardware sales in April rose 76 percent to $192.8 million from a year earlier, while software excluding titles for PCs fell 10 percent to $227.9 million, as consumers held out for games for the new machines.

Microsoft sold 115,000 Xbox One machines in April, the Redmond, Washington-based company said in a separate e-mail. Sony didn’t release its monthly sales figure, saying in an e- mail only that it has sold “over 7 million” PlayStation 4s worldwide, reiterating a statement last month.

Microsoft has shipped more than 5 million of its Xbox One units, it said last month. Both consoles went on sale in November.