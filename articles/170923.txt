NEW YORK (TheStreet) -- Shares of Alliant Techsystems (ATK) are higher on Tuesday, up roughly 8% after the company announced its M&A plans. The company will spin off its outdoor sports business while merging its defense and aerospace business with Orbital Sciences (ORB) to become OrbitalATK.

On CNBC's "Cramer's Mad Dash" segment, TheStreet's Jim Cramer, co-manager of the Action Alerts PLUS portfolio, said, "this is a breathtaking deal."

Cramer also said that ATK is the leading provider for bullets in the defense industry, and should do well when paired with the undervalued asset known as Orbital Sciences.

Cramer said this is just another demonstration of how today's CEOs demonstrate "incredible creativity" when attempting to create shareholder value.

-- Written by Bret Kenwell in Petoskey, Mich.

Follow @BretKenwell

At the time of publication, Cramer's Action Alerts PLUS had no position in companies mentioned.