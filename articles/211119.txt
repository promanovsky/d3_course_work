H R Giger, the Swiss artist and designer of Ridley Scott's Alien, has died, aged 74.

Hans Rudolf 'Ruedi' Giger sustained injuries caused by a fall, Swiss newspaper Neue Zuercher Zeitung has reported.

He is best known for his Xenomorph alien, which appears in Scott’s sci-fi horror film. The design won him an Oscar for visual effects in 1980.

Born in 1940, Giger described his style as “biomechanical”.

He studied architecture and industrial design in Zurich and started his career in interior design, before later moving into art.

One of his pieces - Necronom IV - inspired the alien killer in Scott’s famous film.

H R Giger dead: Alien in pictures Show all 7 1 /7 H R Giger dead: Alien in pictures H R Giger dead: Alien in pictures Alien The alien monster from 1979's classic sci-fi movie Alien Rex H R Giger dead: Alien in pictures Alien The terrifying monster bursts out of Kane's body in Alien Rex H R Giger dead: Alien in pictures Alien The monster from 1979's Alien after bursting out of Kane's body Rex H R Giger dead: Alien in pictures Alien HR Giger's Alien designs were inspired by night terrors Rex H R Giger dead: Alien in pictures Alien Tom Skerritt finds HR Giger's alien monster in 1979's Alien Rex H R Giger dead: Alien in pictures Alien The monster emerges from behind a curtain in 1992's Alien 3 Rex H R Giger dead: Alien in pictures Alien H R Giger's freaky creation also appears in 1997's Alien: Resurrection Rex

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

He also worked on Alien 3, Dune, Prometheus, Poltergeist II: The Other Side and directed several films, including Swiss Made, Tagtarum, and Necronomicon.

In 1998, he opened his own museum in Gruyeres, Switzerland, which is dedicated to his work, as well as pieces from his own art collection, including works by Salvador Dali and Dada.