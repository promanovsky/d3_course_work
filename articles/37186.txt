An outbreak of Ebola has killed at least 59 people in Guinea, according to UNICEF, CNN reported Sunday. The often-fatal hemorrhagic fever has rapidly spread through southern communities in the West African nation.

Experts in Guinea were initially unable to identify the illness that was causing diarrhea, vomiting and fever in the country about a month ago, but early Saturday, Health Minister Remy Lamah said that a viral hemorrhagic fever was detected. According to the U.S. Centers for Disease Control and Prevention, a viral hemorrhagic fever affects multiple organ systems in the body.

“Viral hemorrhagic fevers (VHFs) refer to a group of illnesses that are caused by several distinct families of viruses,” the CDC wrote on its website. “In general, the term ‘viral hemorrhagic fever’ is used to describe a severe multisystem syndrome (multisystem in that multiple organ systems in the body are affected).”

UNICEF revealed in a written statement that 59 of the 80 people who contracted Ebola died, and that three were children. "In Guinea, a country with a weak medical infrastructure, an outbreak like this can be devastating," the UNICEF representative in Guinea, Dr. Mohamed Ag Ayoya, said in the statement.

The Guinean Health Ministry said the disease is spread through infected people, from objects that belong to those who are ill or dead and also by ingesting meat. The Health Ministry is urging people to wash hands regularly and stay home as much as possible. It has also asked that Ebola cases be reported to authorities.

Isolation units are being set up to contain the disease. The international medical charity Medecins Sans Frontieres said it would fly 33 tons of medicines and equipment to help with the epidemic, and said it would help create a quarantine.

"Isolation units are essential to prevent the spread of the disease, which is highly contagious," Dr. Esther Sterk, MSF tropical medicine adviser, said in a written statement. "Specialized staff are providing care to patients showing signs of infection."

While many are wondering whether the Ebola outbreak will spread to other countries, including the United States, the Inquisitr wrote that it's unlikely that the disease will spread to North America.

Follow me on Twitter @mariamzzarella