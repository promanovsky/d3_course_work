Neither of the two U.S. cases of MERS has spread the often fatal infection to family members or to U.S. healthcare workers who treated them in Indiana and Florida, the Centers for Disease Control and Prevention said on Tuesday.

Both of the U.S. cases involved healthcare workers who traveled to the United States in May from Saudi Arabia, which remains at the center of the Middle East Respiratory Syndrome (MERS) outbreak.

The CDC said test results of specimens from each of the household members and the healthcare workers in hospitals where the infected men were treated tested negative for both active infection and any signs of previous infection with the virus.

Efforts to contact individuals who may have come into contact with the infected men during their travels to the United States are nearly completed, and so far none of these travel contacts has shown signs of MERS infection, the agency said.

MERS, which causes coughing, fever and sometimes fatal pneumonia, has been reported in more than 800 patients, mainly in Saudi Arabia. It has spread to neighboring countries and, in a few cases, to Europe, Asia and the United States. At least 315 people worldwide have died from the disease.