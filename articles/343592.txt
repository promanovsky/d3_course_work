Doctors in some states are more liberal in prescribing narcotic medication than others.

Alabama has the highest rate of narcotic painkiller prescriptions according to researchers from the Centers for Disease Control and Prevention, HealthDay reported. Narcotic Prescription in this state are given out three times more frequently than Hawaii, which is the lowest prescribing state.

"The bottom line is we're not seeing consistent, effective, appropriate prescribing of painkillers across the nation, and this is a problem because of the deaths that result," Doctor Tom Frieden, director of the CDC, said at a news conference, HealthDay reported.

Healthcare providers wrote 259 million painkiller prescriptions in 2012.

"That's enough for every American adult to have their own bottle of pills," Frieden explained.

The researchers found 10 of the top narcotic prescribers were in the south and included "Alabama, Tennessee and West Virginia leading the nation," HealthDay reported. The Northeast including Maine and New Hampshire had the most prescriptions per person for long-acting/extended-release painkillers and for high-dose painkillers. Prescriptions for oxymorphone varied between states, but about 22 times as many were prescribed in Tennessee as were in Minnesota.

The CDC found states that crack down on narcotic painkillers have the ability to dramatically cut down on the number of deaths caused by the drugs. About 46 people die from an overdose every day in the U.S.

"Florida shows that policy and enforcement matter. When you take serious action, you get encouraging results," Frieden said.

In Florida the number of prescription drug overdoses decreased 23 percent between 2010 and 2012.

To help reduce prescription drug use across the country the CDC suggest the federal government should support states that want to create policies and programs to combat the issue as well as increasing mental health and substance abuse treatment through the Affordable Care Act

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.