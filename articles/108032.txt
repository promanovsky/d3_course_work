Researchers from the University of Washington found that when flies detect predators, they move like fighter jets, turning and changing their positions very fast to avoid attacks.

For their experiments, the researchers used Drosophila hydei or fruit flies. The size of these flies is about the size of a single sesame seed.

They used a 3HD camera that can capture 7,500 frames per second, or 40 frames per wing beat, to monitor the flies' movements. The cameras were made to focus on a small area inside of a cylindrical arena containing 40 to 50 flies. The illusion of a predator was created by the intersection of two laser beams pointed at the center of the arena.

The images show that flies behave like fighter jets to avoid attacks and in the middle of a turn, they could roll on their sides at a 90 degree angle. There are also some images that showed that they can fly upside down.

"Although they have been described as swimming through the air, tiny flies actually roll their bodies just like aircraft in a banked turn to maneuver away from impending threats," University of Washington professor of biology and co-author of a paper,Michael Dickinson, explained in a press release. "We discovered that fruit flies alter course in less than one one-hundredth of a second, 50 times faster than we blink our eyes, and which is faster than we ever imagined."

He further explained that the moves of the fly during an attack were a result of a calculation made by its brain during a very short amount of time. Usually, that time is also the fly's leeway whether it will get caught or will be able to fly away.

Dickinson also stated that their next study will be to determine how the brains of the fly are able to calculate and execute these moves at a very high speed.

Further details of the study were published in the April 10 issue of Science.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.