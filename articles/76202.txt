One in every three American children has elevated cholesterol, a worrying portent according to doctors of the country.

High cholesterol is linked to an increase in cardiovascular disease risks. A new study conducted by a pediatric cardiologist at Texas Children's Hospital found that roughly one in every three American children aged between 9 and 11 have high cholesterol. This puts them at a higher risk of developing cardiovascular diseases in the future, according to a press release.

For the study, researchers examined the medical records of 12,712 children who were screened for cholesterol levels at Texas Children's Pediatrics Associates clinics. Results revealed that 30 percent of these children had high cholesterol as defined by the National Cholesterol Education Program.

"The sheer number of kids with abnormal lipid profiles provides further evidence that this is a population that needs attention and could potentially benefit from treatment," said Thomas Seery, lead investigator of the study, in the statement. "But we can only intervene if we diagnose the problem."

Though cardiovascular diseases in children are rare, certain contributing factors during childhood put children at a higher risk of developing such diseases in the future. A previous study found that atherosclerosis, the hardening and narrowing of arteries, can begin in childhood.

"We know that higher levels of, and cumulative exposure to, high cholesterol is associated with the development and severity of atherosclerosis," Seery said. "If we can identify and work to lower cholesterol in children, we can potentially make a positive impact by stalling vascular changes and reducing the chances of future disease." He said that this is especially important amid the growing obesity epidemic, which is resulting in a larger population of children with dyslipidemia, an abnormal amount of cholesterol or fats in the blood.

Researchers also found that boys were more likely to have "bad" cholesterol and triglycerides than girls. Additionally, obese children were more likely to have elevated cholesterol than non-obese children.

Findings of the new study support guidelines introduced in 2011 by the National Heart Lung and Blood Institute which urges health systems to institutionalize cholesterol screenings for children ages 9 to 11 and then for young adults ages 17 to 21.

"Kids need to have their cholesterol panel checked at some point during this timeframe [9 to 11 years old]," Seery said. "In doing so, it presents the perfect opportunity for clinicians and parents to discuss the importance of healthy lifestyle choices on cardiovascular health. Our findings give a compelling reason to screen all kids' blood cholesterol."

The new study will be presented at the American College of Cardiology's 63rd Annual Scientific Session.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.