Could a "Full House" return be in the works? The family sitcom, which aired on ABC from 1987 to 1995, is still a ratings juggernaut via repeats on Nick at Nite. Now Warner Bros. TV is mulling a new take on "Full House," with some of the original cast intact, TVGuide reports.

Leading the charge is John Stamos (Uncle Jesse) — who has an ownership stake in the show, which gives him good reason to champion the new series. Original executive producer Bob Boyett and creator Jeff Franklin (who's reportedly writing the new version) are said to be actively involved. Candace Cameron Bure (D.J.), Jodie Sweetin (Stephanie) and Andrea Barber (Kimmy) are reportedly on board, while Bob Saget (Danny) and Dave Coulier (Joey) are also involved in some way.

Candace Cameron Bure reveals her hot body secrets

The cast has remained tight-knit and some of the actors have let it slip in the past that they'd like to see "Full House" return. Stamos, Coulier and Saget appeared in a yogurt commercial earlier this year, and also showed up on "Late Night With Jimmy Fallon" dressed as their "Full House" characters. Appearing on Bravo's "Watch What Happens Live" this past winter, Stamos hinted that a "Full House" revisit was being pondered: "We're sort of working on a twist on a sequel," he told host Andy Cohen. "But we don't know if it's going to happen yet or not." In 2009, Stamos also told the press that a "Full House" movie had been developed.

But another comedy from ABC's family-friendly "TGIF" lineup, ," was recently revived on Disney Channel as "Girl Meets World." Such a revival would make perfect sense given the timeless popularity of "Full House."

According to Nick at Nite, repeats of the show in primetime average 1.5 million viewers, up 7 percent from last year. Episodes are also average a 0.4 rating in the key 18-49 demo, up 8 percent from a year ago. The show even has a healthy teen fan base, a demo that wasn't even born when the show ended.

Costars reunited!

"Full House" was routinely a Top 25 show during its original run, but ABC canceled it in 1995 as the sitcom became too expensive to continue and tastes changed. Staffers offered to cut their pay in order to keep the show going, but to no avail.

Insiders say the studio pitched a "Full House" revival last year that would have been set up like FX's "Anger Management," in which the network that airs it must pick up a 90-episode slate (produced at an accelerated pace) if the initial 10 episodes hit a certain rating. Ultimately that idea didn't pan out, but as new generations continue to discover "Full House," there's no doubt a reboot would generate loads of interest.

Subscribe to TV Guide Magazine now!