Colon cancer rates in the United States have fallen sharply in the last ten years, primarily due to a marked increase in the use of colonoscopy screenings.

For people over 50, the rate of colon cancer incidence has dropped by 30% in the last ten years, according to a new report in the journal CA: A Cancer Journal for Clinicians. The success drop is being heralded as a public health success story, after use of colonoscopy screenings among people over 50 almost tripled from 2000 to 2010, from 19% to 55%.

Despite the sharp improvement, more remains to be done. Colon cancer is the third most common cancer in both men and women and the third most common cause of cancer death. Only 59% of people over 50 “reported having received colorectal cancer testing consistent with current guidelines in 2010,” according to Cancer.

Black men and women are afflicted with colon cancer at a rate about 25% higher than whites and mortality rates are 50% higher. Other racial groups have lower incidence and mortality rates than whites.

[Medical News Today]

Get our Health Newsletter. Sign up to receive the latest health and science news, plus answers to wellness questions and expert tips. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.