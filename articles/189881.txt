NEW YORK, May 7 (UPI) -- Angelina Jolie opened up about her life with Brad Pitt and why she thought she'd never be a mother in an interview for the June issue of Elle magazine.

"I never thought I’d have children," Jolie told the fashion glossy. "I never thought I’d be in love, I never thought I’d meet the right person. Having come from a broken home -- you kind of accept that certain things feel like a fairy tale, and you just don’t look for them."

Advertisement

In the end, the 38-year-old actress did find love and have children, six of them actually. The Oscar-winning actress and Pitt are parents to Maddox, Pax, Zahara, Shiloh, Knox, and Vivienne.

“You get together and you’re two individuals and you feel inspired by each other, you challenge each other, you complement each other, drive each other beautifully crazy," Jolie said of her relationship with Pitt.

"After all these years, we have history -- and when you have history with somebody, you’re friends in such a very real, deep way that there’s such a comfort, and an ease, and a deep love that comes from having been through quite a lot together.”

Jolie also discussed feeling "misinterpreted" during her twenties and having a need "to be true to myself, whatever that may be."

You can reed more excepts from her interview here and read the full story when the June issue of Elle hits stands on May 20.