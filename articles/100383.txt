Researchers in a medical breakthrough have successfully implanted laboratory-grown vaginas in four teenage girls.

The girls in the study were born with Mayer-Rokitansky-Kuster-Hauser (MRKH) syndrome, a rare genetic condition in which the vagina and uterus are underdeveloped or absent.

The girls were between 13 and 18 years old at the time of the surgeries led by Anthony Atala, M.D., director of Wake Forest Baptist Medical Center's Institute for Regenerative Medicine, which were performed between June 2005 and October 2008. Data from annual follow-up visits show that even up to eight years after the surgeries, the organs had normal function.

In addition, the patients' responses to a Female Sexual Function Index questionnaire showed they had normal sexual function after the treatment, including desire and pain-free intercourse.

The organ structures were engineered using muscle and epithelial cells (the cells that line the body's cavities) from a small biopsy of each patient's external genitals. In a Good Manufacturing Practices facility, the cells were extracted from the tissues, expanded and then placed on a biodegradable material that was hand-sewn into a vagina-like shape.

About five to six weeks after the biopsy, surgeons created a canal in the patient's pelvis and sutured the scaffold to reproductive structures.

Followup testing on the lab-engineered vaginas showed the margin between native tissue and the engineered segments was indistinguishable and that the scaffold had developed into tri-layer vaginal tissue.

The study has been published in the journal Lancet.