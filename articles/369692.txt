Selena Gomez is apparently using Instagram to share her feelings and it seems like the 21-year-old singer has a lot on her mind. Gomez spent a couple hours this morning (July 8) posting sad videos and cryptic messages to her millions of followers. Immediately, it was speculated that her on-again, off-again boyfriend Justin Bieber had something to do with her melancholy mood.

Gomez posted a short video to her account of her playing a haunting tune on the piano with the caption, "I don't know. I'm on my way you know." Fans quickly commented on the video offering Gomez a few encouraging words and showing their support.

Cleary Gomez appreciated the outpouring of love and posted an upside down shot of the messages, along with the caption, "You're always listened to." The next photo Gomez posted was a moody-looking selfie with the line, "And usually always right."

A short while later, the "Come & Get It" singer uploaded a screen shot of a text message that read: "I just wanna give you a hug and remind you how special you are." Alongside the picture, she wrote "That's who you wanna wake up to. #promise."

It's not clear who the mysterious text is from but fans guessed that Bieber was probably the sender. There were rumors that the couple had split over the weekend after getting into a nasty argument. Reportedly the two spent 4th of July separate and Gomez was not spotted at Bieber's manager's July 6 wedding to Yael Cohen in British Columbia, Canada.

A source told Hollywood Life that the "Baby" singer didn't invite Gomez to Scooter Braun's nuptials because he knew it would turn into a "paparazzi nightmare." According to reports, Gomez didn't understand that and was upset with her beau for not inviting her.

"Scooter made it clear that he just wanted Justin as that would allow the focus to be on their special day and not what Justin and Selena were doing," the source said. "Justin obeyed Scooter's wishes which in turn got Selena upset and then got the ball rolling for them to fight and break up and not hang out during the 4th. It all spiraled from there."

A different source told Hollywood Life that Gomez's family is very worried and wants her back in rehab. Her romance with Bieber is rumored to have ended friendships with Taylor Swift and Demi Lovato.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.