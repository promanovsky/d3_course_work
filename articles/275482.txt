A study shows that bilingual senior citizens might have slower cognitive decline than monolingual speakers. — AFP pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LONDON, June 4 — A study from the University of Edinburgh suggests that being bilingual may help slow our rate of cognitive decline.

The research aimed to answer the question as to whether the most intelligent people are likely to learn a new language, or whether learning a new language increases cognitive ability.

The results proved the latter is true.

The study began in 1947 when 835 anglophones from Edinburgh, about 11 years of age, were given intelligence tests. They were retested in 2008 and 2010, after reaching their 70s.

Of the participants, 262 were bilingual by the time of the second round of tests.

Among the bilinguals, 195 had learned their second language before age 18 and 65 had learned it during adulthood.

The bilinguals performed better than monolinguals with respect to their baseline results, and researchers were surprised to see that the age at which participants had learned their second language made no difference.

“These findings are of considerable practical relevance,” said Dr. Thomas Bak from the Centre for Cognitive Ageing and Cognitive Epidemiology at the University of Edinburgh. “Millions of people around the world acquire their second language later in life. Our study shows that bilingualism, even when acquired in adulthood, may benefit the aging brain.”

While it’s commonly known that learning a language is easier at a young age, this study suggests that it’s never too late to start in order to promote increased intelligence and healthy brain aging.

“The epidemiological study provides an important first step in understanding the impact of learning a second language and the ageing brain,” says Dr. Alvaro Pascual-Leone, professor of medicine at Harvard Medical School in Boston. “This research paves the way for future causal studies of bilingualism and cognitive decline prevention.”

In 2013, Bak led another study concluding that bilingualism could delay the onset of dementia.

It could be, however, that just learning a second language isn’t enough. A 2013 study published in the journal Society for Neuroscience suggests that bilinguals who retain their capacities into the golden years reap cognitive benefits.

A 2011 study, meanwhile, suggests that bilinguilism delays the onset of Alzheimer’s disease. — AFP-Relaxnews