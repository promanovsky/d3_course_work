Reviewers have criticized Microsoft's pricing scheme for the new Office for iPad as being too high

But it's really a brilliant way to give iPad users access to Office, while giving Microsoft a solid chance to avoid forking over 30% of the price to Apple.

Advertisement

The deal is, you can download the app for free and this lets you view Office apps. If you want to edit them, you have to sign in with an Office 365 account.To get an Office 365 account, you have to have one of the subscription options: $99/year for a family account (works on up to five devices); $80 for a student account that last four years and covers two devices.

But the only subscription that can be bought from within the iPad app is the $99/year option.

Advertisement

By giving the app away for free, Microsoft gets a shot at completely side-stepping revenue sharing with Apple, reports Computerworld's Gregg Kiezer

If users download the freebie iPad app first and then they upgrade to the full app from the iPad app, Microsoft will have to pay Apple the 30%.

Advertisement

But if Microsoft sells them the Office 365 account first and then they get the iPad app, Microsoft doesn't have to pay Apple. Given that Office 365 is software that you install on directly on your PC or Mac and that the $99 version covers five devices, many users will buy it first for their PC or Mac, and then get the iPad app. Microsoft says it has already signed on 3.5 million consumers for Office 365, before the iPad app became available.

Microsoft will soon a release a cheaper $70/year personal account that covers two devices. This lower-cost version will likely be the most popular with iPad users who buy Office through the iPad app. And the lower price also means less revenue shared with Apple.

But by far, the biggest money maker for Office for iPad will be businesses, and Apple won't see a cut of those contracts. Businesses buy Office 365 directly from Microsoft as part of big software license deals. After they buy Office 365, they'll get the iPad app.