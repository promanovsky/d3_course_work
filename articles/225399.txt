Fears over harm to young brains comes with the increase in phone usage

When you subscribe we will use the information you provide to send you these newsletters. Sometimes they'll include recommendations for other related newsletters or services we offer. Our Privacy Notice explains more about how we use your data, and your rights. You can unsubscribe at any time.

Sign up forfor latest news plus tips to save money and the environment

British experts today launch the world’s largest study into their effect on developing brains and whether they harm intelligence and attention.

At least 70 per cent of 11 to 12-year-olds in the UK own a mobile phone, rising to 90 per cent by the age of 14.

While there is no convincing evidence that they affect adult health, experts believe children may be more vulnerable because of their developing nervous systems and thinner skulls which absorb higher levels of radio energy.

Researchers say children could be more at risk because their “potential cumulative lifetime exposure is far greater than any adult today”.

The World Health Organisation has ranked studies into mobiles’ impact on youth as a “highest priority research need”.