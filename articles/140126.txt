As X-Men director Bryan Singer is gearing up for his defense case, which he will have to make in court and in the media, the sexual abuse allegations against him have sent the Days of Future Past marketing campaign into a tailspin. 20th Century Fox and ABC are scrambling to save the promotional campaigns for several of the director/producer’s projects. According to The Hollywood Reporter, Singer has already cancelled an appearance at a comic book convention this weekend, where he was supposed to promote Days of Future Past and an MPAA-sponsored conference in Washington, D.C., on May 2 at which Vice President Joe Biden will be the featured speaker.



Singer was accused in a detailed lawsuit filed earlier this week.

This is likely to be just the beginning of a slew of complications for Fox, which is currently in the midst of a heavy promo campaign for Singer’s $205 million movie. Fox is scheduled to release X-Men: Days of Future Past, the latest installment in its X-Men series, in the U.S. on May 23. Singer, who directed the first two X-Men movies, returns to the franchise as director and producer of the new film, which will have its world premiere in New York City on May 10.

Watch the trailer for X-Men: Days of Future Past



In the meantime, Singer is working on his defense against the accusations made by Michael Egan. In his lawsuit, Egan claims to have been drugged and sexually assaulted by singer during several parties in Hawaii. Singer’s defense, however, claims that he was not in Hawaii during the period, specified by his accuser. Sources close to the director have reportedly confirmed that he was in Toronto at the time, filming the first X-Men movie. According to TMZ, Singer will file credit card bills and various other documents proving he was in Toronto during the times in question.



Several others of Singer's projects are also in jeopardy.

The movie was filmed between September 22 and March 3 in Toronto, which doesn’t cover the time period specified by Egan. According to TMZ’s sources, however, Singer was also present during pre-production.

Read More: Bryan Singer's Accuser Says He Was "Piece Of Meat"

Read More: Bryan Singer Dismisses Storm Cut Story