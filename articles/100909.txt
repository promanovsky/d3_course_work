The government has spent an additional £49m within the last year on renewing its stockpile of Tamiflu, the drug which independent scientists say may do nothing to prevent a flu pandemic.

The decision to spend yet more money on a stockpile of controversial drugs that has already cost over £500m "verges on the reckless, given that the evidence base was already very shaky", according to Richard Bacon, Conservative MP and senior member of the public accounts committee.

The revelation of the extra spending came in the government's response to the committee's report in January, which recommended that no more Tamiflu should be bought until the Department of Health had reviewed the need for it.

At the time, the committee was told the department had spent £424m on Tamiflu and £136m on a similar, but inhaled, flu drug called Relenza. The extra £49m takes the total spend to £609m.

On Thursday, independent scientists from the Cochrane Collaboration published an analysis of all the data from the clinical trials conducted by manufacturers Roche and GlaxoSmithKline prior to licensing of the drugs, which took them over four years of negotiations to obtain.

They concluded that the drugs shorten a flu bout by approximately half a day, but there is no proof that they prevent hospitalisations or complications such as pneumonia and, when used to prevent flu, they cause some people serious complications including psychiatric and kidney problems.

The companies maintain the drugs are safe and useful, but Bacon says the doubts about the drugs were evident at the time of his committee's inquiry last year, before the extra £49m had been spent. A key member of the Cochrane team gave evidence. "You don't spend £400m to reduce flu symptoms by a day or half a day. You buy Lemsip for that. You spend millions of pounds on it to prevent pandemic flu," he told the Guardian. "They should have held off and waited until the further analysis was available."

In its response to the PAC report, the government said that the 2013-14 programme to replace drug stocks going out of date with new batches was already in place. The decision to go ahead "supports the department's commitment to be ready for a more severe influenza pandemic and to pay the prices agreed in the current contract."

The government also dismissed calls by the committee for all clinical trials – past and future – to be registered and the data from them made available for wider independent scrutiny, saying it was "not feasible" to do it retrospectively.

"What is wrong with opening it up to wider independent scrutiny?" asked Bacon. "Are we to believe the work undertaken by regulators has some kind of magic charm about it and that there is a priestly caste called government-appointed scientists who have available to them insights that are not available to others? Are they asking us to go back to the 13th century or the 16th century or back to alchemy? Is this what they want? That's what it sounds like to me and I think it is ridiculous."

A growing number of senior scientists believe the problem lies with the piecemeal way in which clinical trials are done. They are not set up to answer specific questions that are of agreed international important, such as, in the case of Tamiflu, precisely what complications it can prevent and what use it would be in a pandemic.

Jeremy Farrar, director of the Wellcome Trust and a professor of tropical medicine at Oxford University, said regulators, scientists, doctors, policymakers and others involved in drug development must come together to work out what the crucial questions are that need to be answered, so that when there are many trials, often of broadly similar drugs, the results can be aggregated for a clear picture of what works and what does not. On Tamiflu, he said, "the honest truth is we simply do not know", because the trials were not designed to seek the right answers.

We could learn from the approaches that have been taken to diseases in poor countries, such as malaria and HIV, he said, where using money and time cost-effectively to come to the right answers is critical.