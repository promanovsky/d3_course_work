Twitter forced to shut down Tweetdeck app amid major security alert



Vulnerability allows attackers to place computer code in a tweet

Service was shut for for several hours while fix was issued

Experts warn data could have been compromised by attack

Twitter was forced shut down its popular TweetDeck application for several hours today following a major security alert.

Users of the Chrome browser version of the app reported getting random pop-up windows containing messages such as 'Yo!' or 'Please close now TweetDeck [sic], it is not safe.'

The vulnerability allowed attackers to place computer code in a tweet.



Users of the Tweetdeck Chrome plugin were greeted with this message after hackers found a flaw in its code

HOW IT WORKS The vulnerability allows attackers to place computer code in a tweet.

Once the tweet appears inside Tweetdeck, the code can run actions and be re-tweeted to other accounts, further propagating the problem.

The firm took to Twitter to acknowledge the bug, first saying 'We've temporarily taken TweetDeck services down to assess today's earlier security issue,' it said.

'We'll update when services are back up.'

Earlier in the day Twitter pushed out a code fix that was supposed to close the security hole but did not.



At that point the company tweeted 'A security issue that affected TweetDeck this morning has been fixed.



'Please log out of TweetDeck and log back in to fully apply the fix.'

However, it is believed the problem continued, until the firm later tweeted: 'We've verified our security fix and have turned TweetDeck services back on for all users.



'Sorry for any inconvenience.'

Experts say the flaw could have been used to steal data.

'Cross site scripting or XSS is a type of exploit that usually works in a website or a web application. It allows the attacker to run a script on the users device, which makes XSS vulnerability so dangerous,' said George Anderson, of security firm Webroot.



The firm took to Twitter to acknowledge the bug following complaints from users

'The script is able to send any sensitive information accessible from within the browser back to the hacker, so a potential attacker can gains access to the user’s private information – such as passwords, usernames and card numbers.'

He advised users to log out of the app and remove their saved passwords as a precaution.



'As Tweetdeck is a web app, signing out might help to contain the infection, as long as users devices are not already infected.



'Because XSS steals the cookie sign-on information, users should get rid of all saved passwords, as well as sign-in again on a secure browser session and change their logins.



'It’s also best not to use Tweetdeck as long as it remains infected.'

TweetDeck is a free download for desktop computers, iPhones, Google's Android devices and the Google Chrome browser.



The software allows users to organize their Twitter streams and offers a more user friendly view of Twitter feeds.



Twitter bought TweetDeck in 2011 for about $40 million.

