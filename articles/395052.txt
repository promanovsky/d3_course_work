OkCupid, a top US dating / matchmaking website, intentionally mismatched users to test its technology, the IAC/InterActive Corp service said on Monday, weeks after Facebook Inc admitted to misleading users in a psychological study.

"When we tell people they are a good match, they act as if they are," co-founder Christian Rudder wrote in a blog post. "Even when they should be wrong for each other."

Conversely, couples told they were bad matches, even when OkCupid's algorithm showed the opposite, were less likely to exchange four messages. Exchanging four messages is an OkCupid measure for gauging romantic interest.

In the post, titled "We Experiment on Human Beings!" Rudder explained the tests helped the company refine its product. He did not respond to an email asking how many users were tested.

"Most ideas are bad," he wrote. "Even good ideas could be better. Experiments are how you sort all this out."

An IAC spokeswoman said OkCupid planned to continue with the experiments, which are known in the business as A/B testing.

But experimenting on users without their consent could cost the company credibility, said Irina Raicu, director of the Internet ethics program at Santa Clara University.

"They are messing with emotions and with communications," she said. "That's different than other things we are A/B tested about."

The experiment drew heavy criticism online. In a tweet, University of Pennsylvania computer scientist Matt Blaze suggested a few new clauses for online user licensing agreements:"We reserve the right to induce despair" and "You agree that there will be no love, except the love of Big Brother."

In June, Facebook users were outraged when a study showed that the world's largest social networking site had manipulated news feeds to see how viewing more positive or negative posts affected users' posting habits. The researcher who led the study apologized for the anxiety the experiments caused but stopped short of saying the company would halt the practice.

OkCupid is one of the top U.S. dating services, behind Match.com, eHarmony, and Plenty of Fish, according to the Pew Research Center.

"We use math to get you dates," the site's "About Us" section reads. "It's extremely accurate, as long as (a) you're honest, and (b) you know what you want."

Other experiments OkCupid has tried include a "Love Is Blind Day" in January 2013, when the company removed all photographs from the service for seven hours. Fewer people used the service, but a greater percentage of user messages drew responses.