They may be firm friends now, but Johnny Depp’s first meeting with Iggy Pop wasn’t very friendly at all. Depp’s former band, Kids, supported The Stoodges in the 80s.

The actor was just 17-years-old, and desperate to capture his heroes’ attention, regardless of how obnoxious he might have to act to do so.

“I was standing at the bar, all of 17-years-old, guzzling as much spirits as I could to get up the nerve to talk to you,” Depp said to Iggy in Interview magazine.

“And I remember making the horrific decision in my teenage drunken state to go, “Well, I'll just get his attention.”

I started going, ‘I-I-Iggy Pop, Piggy Slop,’ you know? You walked towards me and put your face about a quarter of an inch from mine and just went, ‘You little turd.’”

Oddly, Depp was delighted with the result.

“And not only was it exactly the reaction that I deserved, but it was the one I wanted,” he said.

Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here Thanks for signing up to the News newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here

“I had a moment with you. Even though it was three words that dressed me down to absolutely nothing, I was so satisfied and happy that I had that experience with you.”

Depp went on to discuss his friendship with celebrated Beat poet Allen Ginsberg, who he says was an incorrigible flirt. They first met while working on a documentary called The United States of Poetry in 1995.

“He was a relentless flirt,” he said. “Every time I saw him, he'd want to hold hands. It was sweet. I think he just wanted affection, on whatever level.”

In other Johnny Depp news, yesterday he confirmed speculation that he is soon to marry actress Amber Heard, by wearing a feminine engagement ring at a press event held in China.