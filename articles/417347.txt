Our free email newsletter sends you the biggest headlines from news, sport and showbiz Sign up Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

The last British man to survive the Ebola virus nearly 40 years ago said the public must be warned of its dangers.

Geoffrey Platt contracted the virus when he accidentally pricked his finger while taking a sample from an infected guinea pig at the Microbiological Research Establishment at Porton Down in Wiltshire.

The former laboratory technician suffered from vomiting, diarrhoea and a rash when he fell ill in November 1976.

He said: “I was in a tent in quarantine for 40 days by myself and I really don’t want to bring up what happened to me.”

(Image: Getty)

Mr Platt, 80, of Salisbury, Wilts, added that steps must be taken to stop the virus reaching the UK.

He said: “There are people with much more expertise than me who will know how to warn of the dangers. It happened a long time ago to me and you would need to speak to someone else about warning of the dangers from this outbreak.”

The Porton Down military installation was founded in 1916 and is the oldest chemical warfare facility in the world. It wasn’t until the 1960s that the government admitted the existence of the 7,000 acre site near Salisbury.

Mr Platt's appeal comes as a British volunteer nurse who contracted deadly Ebola in Sierra Leone is being treated at a specialist hospital after being evacuated to the UK.

(Image: Sky news)

He was named as William Pooley, 29, by Dr Robert Garry, an American scientist who worked at the same hospital as him in the west Africa country. It is the first confirmed case of a Briton contracting the virus during the recent outbreak.

There is no cure for Ebola and outbreaks have a fatality rate of up to 90%.

Mr Pooley tested positive for Ebola after treating patients suffering from the virus at Kenema Government Hospital in the south-east of Sierra Leone.

He was airlifted to the UK on a specially equipped C17 Royal Air Force jet, landing at RAF Northolt in west London at 9pm on Saturday night. He was then transported to the UK's only high level isolation unit at the Royal Free Hospital in Hampstead, north London.

The Department of Health said he was not 'seriously unwell' and health chiefs have insisted the risk to the British public from Ebola is 'very low'.

(Image: Reuters)

Dr Garry, of Tulane University in New Orleans, US, has worked at KGH for around a decade on a virus research project.

He said he was told by a university colleague that the test results for Mr Pooley were received in the early hours of Saturday morning. "They worked as hard as they could, as fast as humanly possible to make these arrangements," he said.

"Of course they were wanting to make sure that he got the best care possible. It was kind of a remarkable turnaround, barely over 24 hours (later) he was heading towards that plane."

Mr Pooley was working at a hospice in the capital Freetown but moved to Kenema when he heard that other healthcare workers at KGH had died from Ebola.

In an interview with a blogger for freetownfashpack.com published earlier this month, he is reported to have said: "It's the easiest situation in the world to make a difference. I'm not particularly experienced or skilled, but I can do the job and I am actually helping."

(Image: Reuters)

Dr Garry paid tribute to Mr Pooley's decision to treat Ebola sufferers. "It's a very honourable thing. He saw the need. He read about our nurses who were unfortunately dying there and took it on himself to come over and volunteer and learned how to be as safe as he could.

"But when you work hard like that, when you put in so many hours, you're going to make a mistake and unfortunately that seems to have happened in this case.

"I just hope the best for him, that he can get the best treatment he can get. He's a young man, he's got a good chance. It was caught early."