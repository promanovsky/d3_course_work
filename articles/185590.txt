Apple granted its new retail chief 113,334 restricted stock units, valued at more than $68 million based on Monday’s closing share price.

Angela Ahrendts, a senior vice president and the highest-ranking woman executive at the company, will receive the shares spread over several different vesting periods between June 1, 2014 and June 14, 2018, Apple said in a regulatory filing late on Monday.

Ahrendts is the first woman to join Apple’s executive team in nearly a decade and will take on an expanded role, overseeing its vast network of stores that employs about 42,400 people and online teams. Apple announced the appointment in October, but Ahrendts only started her job at Apple on May 1.

Prior to joining Apple, Ahrendts served as the CEO of Burberry Group Plc, where she oversaw a doubling in sales and a 250-percent jump in the shares of the British fashion brand.