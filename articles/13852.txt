Paris: France will introduce driving restrictions in Paris on Monday to tackle dangerous pollution levels, the first such ban for twenty years as politicians try to get rid of health-threatening smog days before municipal elections.

Paris is more prone to smog than other European capitals because of France’s diesel subsidies and its high number of private car drivers. A week-long spell of unseasonably warm, sunny weather has recently exacerbated the problem.

Under the scheme, drivers may only use their cars on alternate days, according to the odd or even numbers on their licence plates. Free public transport, including cycle and electric car-sharing schemes, was introduced last week as a visible haze hung over Paris streets.

“Our core objective is to ensure public safety because we want to end this pollution,” Environment Minister Philippe Martin told a news conference on Sunday, warning that the air quality was likely to worsen on Monday.

Last week European Environment Agency (EEA) figures for Thursday showed there was 147 microgrammes of particulate matter (PM) per cubic metre of air in Paris — compared with 114 in Brussels, 104 in Amsterdam, 81 in Berlin and 79.7 in London.

Political opponents and car associations criticised the decision, saying it would be tough to police, and accused the Socialist government of conceding to pressure from its coalition Green partners ahead of local elections in late March.

“This is impossible to enforce, stupid and an attempt to win votes,” Pierre Chasseray, president of drivers’ lobby 40 Millions d’Autombolistes, told French televion and newspapers.

Opposition UMP chief Jean-Francois Cope and mayor of Meaux in the suburbs of Paris, said there was a lot of confusion about the scheme.

“The ecologists have applied a lot of pressure on the government and the decision was rushed...It lacks coherence, explanation and — on the ground, as a mayor from one of Paris’s suburbs — it’s panic,” he told Europe 1 radio.

The last restricted driving scheme was introduced in October 1997 in response to pollution from heavy diesel fumes. It lasted one day.