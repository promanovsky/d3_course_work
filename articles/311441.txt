Joan Solsman/CNET

Oculus VR has a resource arsenal at its disposal after being acquired by Facebook back in March for $2 billion. That doesn't mean, however, that the Irvine, Calif.-based virtual reality headset maker isn't seeking out new ways to spread its technology far and wide.

In fact, the startup is modeling its expansion after Google's Android mobile operating system, Oculus CEO Brendan Iribe tells Bloomberg. Instead of restricting its mobile OS to its own handsets, as Apple does with iOS devices, Google let a bevy of companies -- competitors included -- use Android. Not only have electronics companies like Samsung and HTC developed top-tier flagship phones running Android, but companies can even fork it, as Amazon as done for the Kindle line of tablets, to develop their own takes on the mobile OS.

The goal: boost adoption. That's precisely what's happened with Android, which now runs on 81 percent of mobile devices worldwide, according to the latest report from market researchers IDC. Iribe is eyeing a similar tactic and is opening its arms wide to get Oculus' tech onto as many devices as possible.

"If we do want to get a billion people on virtual reality, which is our goal, we're not going to sell 1 billion pairs of glasses ourselves," Iribe said. "We are openly talking to any kind of partner that wants to jump into VR, and there's a lot of interest right now."

Oculus currently makes only a prototype virtual reality kit, called the Rift, and has been selling the second generation of that device to developers since unveiling the updated model at the Game Developers Conference (GDC) in March. The company has yet to announce a consumer model, and is seemingly plotting its course to a retail version carefully, hoping to avoid the pitfalls of plunging VR into the market too soon.

Oculus has even sat down with Japanese electronics giant Sony, which is currently far along in its own virtual reality project, codenamed Morpheus. Developed for the PlayStation 4 game console, Morpheus made its debut at GDC back in March, and it also made a quiet showing with all-new demos at E3 earlier this month. Morpheus is, in some ways, the antithesis to the Rift in that it's being developed as a proprietary technology for Sony's platform, whereas Oculus is meant to run on any PC.

Still, Iribe claims the two companies may hammer out a working relationship down the line. "We showed their key executives our prototypes and we said if you want to work with us, we are happy to engage deeply and be friends in this industry," he said. "It hasn't gone anywhere past that, but they did show up and opened up to us and in turn we opened up to them."

Despite its efforts toward hardware partnerships, Oculus is still putting money and energy toward its in-house hardware manufacturing and design. On Tuesday, Oculus announced that it has acquired the Carbon Design Group, the product engineering team responsible for gaming mainstays like the Xbox 360 Kinect and controller and numerous other industrial design projects. Oculus would not disclose how much it paid for the acquisition, a company spokesperson told CNET.

CNET's Ian Sherr contributed to this report.

Update at 12:50 p.m. PT: Added news of Oculus' acquisition of Carbon Design Group.

