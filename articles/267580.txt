China's manufacturing sector has improved in May showing signs of stabilisation in the economy, but weakness remains forcing the government to loosen its policy further.

The HSBC China Purchasing Managers' Index (PMI) showed a reading of 49.4 in May, compared to the earlier flash reading of 49.7 and 48.1 in April.

A reading below 50 indicates contraction in the sector.

The reading signalled only a marginal deterioration in business conditions, but the health of the sector has now deteriorated in each month of 2014 so far, according to HSBC.

In May, new orders stabilised, while new export orders recorded an impressive expansion of 53.2. But growth momentum looked weaker as the stocks of finished goods index was revised up to 49.8 from 48.8 in the flash reading.

"The final PMI reading for May confirmed that the economy is stabilizing, but it is too early to say that it has bottomed out, particularly in light of a weaker property sector," Hongbin Qu, China chief economist at HSBC said in a statement.

"The lack of a sustainable growth momentum warrants stronger policy support. We expect both monetary and fiscal policy to be loosened gradually over the coming months."

Earlier, the official PMI released by the National Bureau of Statistics showed a reading of 50.8 in May, up from 50.4 in April. The figure was a five-month high and a third straight monthly improvement.

China announced a mini stimulus to help industries, after first-quarter growth declined to 7.4% - the weakest expansion in 18 months – from 7.7% in the final quarter of 2013.

Policymakers are trying to reduce the economy's dependence on investment and exports, and propel a consumption-backed growth.