Gold is heading for its first monthly decline this year on growing optimism about the US economy.

Gold was trading near a six-week low on Monday, and was headed for its first monthly decline this year on growing optimism about the U.S. economy and weak physical demand in Asia.

Gold has lost nearly $100 an ounce in the last 10 trading sessions, moving further away from a six month high hit mid-March.

*Check Live Updates: BSE Sensex

The metal's safe-haven demand had been bolstered earlier in the year when fears over economic growth in the United States and emerging markets hurt global equities, and tensions between Russia and the West were high.

"Prices will go down further since those factors supporting gold in the first quarter are weakening now, such as geopolitical issues, and the emerging markets crisis," said Chen Min, a precious metals analyst at Jinrui Futures in Shenzhen.

Physical demand in Asia could pick up if prices fall to $1,180-$1,200, Chen said.

Spot gold fell 0.1 percent to $1,291.80 an ounce by 0706 GMT, not far from a six-week low of $1,285.34 hit on Friday. It is down nearly 3 percent for the month.

Recent U.S. data has been strong, indicating that the economy is recovering well after severe weather conditions.

Federal Reserve Chair Janet Yellen indicated earlier this month that interest rates could rise in the first half of 2015. Low interest rates, which cut the opportunity cost of holding non-yielding bullion above other assets, had been an important factor driving bullion higher in recent years.

Investors are now eyeing U.S. nonfarm payrolls data on Friday for further clues about the economy.

Physical demand in top bullion buyer Asia has been quiet due to the recent volatility in gold prices, which have edged lower for two straight weeks.

Only Japan has seen some pick-up in demand as consumers brought forward their purchases ahead of a sales tax hike from April 1.

Among other precious metals, platinum rose nearly 1 percent on Monday as labour strikes continued in top producer South Africa.

Job cuts are a certainty in South Africa's platinum belt because of losses stemming from a 10-week strike in the industry, the chief executive of world No. 1 producer American Platinum said, raising the risk of further unrest.