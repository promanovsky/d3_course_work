Market Drivers July 01, 2014

Cable hits best levels since 2008 as PMI Manufacturing shows strong demand

RBA keeps rates on hold, makes little change in the statement

Nikkei 1.08% Europe 0.40%

Oil $105/bbl

Gold $1326/oz.



Europe and Asia:

AUD RBA at 2.5%

EUR GE Unemployment 9K vs. -9K

GBP UK PMI Manufacturing 57.5 vs. 56.5

EUR Unemployment Rate 11.6% vs. 11.7%



North America:

USD Manufacturing PMI 9:45 AM

USD ISM Manufacturing PMI 10:00 AM



Cable hit its highest levels since 2008 rising to 1.7140 in morning London dealing after UK PMI Manufacturing printed better than expected indicating that the sector is enjoying its best growth in more than two decades.



UK PMI Manufacturing came in at 57.5 versus 56.7 eyed as all the key components showed improvement . The employment index rose 55.8 from 54.4 the month prior while new orders rose to 61.1 from 59.5. As analysts as Markit noted, "UK manufacturing continued to flourish in June, rounding off one of the best quarters for the sector over the past two decades. With levels of production surging higher, and order books swollen by a further upswing in demand from both domestic and overseas clients, job creation accelerated to its highest for over three years."



With UK data continuing to exceed expectations the pressure builds on BoE to hike rates this year rather than in 2015. The UK monetary authorities have consistently downplayed rate hike expectations on concerns that a strengthening pound would hurt the country's nascent export rebound. But given today's strong manufacturing results the impact of higher exchange rates appears to be minimal and the BoE is now in danger of losing its credibility if it does not act soon , especially if the data continues to surprise to the upside.



Meanwhile in Europe the news was not nearly as sanguine, with German unemployment rising to 9K from -9K eyed. This was the second month in row that German joblessness increased, although the unemployment rate remained the same at 6.7%. Taken together with yesterday's weak German Retail sales, the news indicates that EZ largest and most important economy is starting to slow down, although the slack is picked up somewhat by the periphery especially Spain, where the rebound continues to gather pace.



The EUR/USD saw little reaction to the news dipping to 1.3680 in the aftermath of the release but remained well within striking distance of the 1.3700 barrier as investors continued to shun the greenback.



US yields have however firmed somewhat to 2.55% and that rise along with a 1% gain in the Nikkei helped nudge USD/JPY through the 101.50 level. The yen crosses have generally been well bid in overnight trade with AUD/JPY peeking over the 96.00 figure after the RBA left rates unchanged at 2.5% and offered no tangible threats of any easing in the foreseeable future.



With US ISM Manufacturing data on the docket during North American trade the focus on USD/JPY recovery could continue especially of the ISM numbers beat expectations. Traders will look with particular care at the employment component which if it shows further growth could fuel a move in USD/JPY back towards the 102.00 level as the day proceeds.