OTTAWA--Canadian police said Wednesday they had charged a 19-year college student from Ontario with stealing confidential taxpayer data by exploiting the Internet security flaw known as the Heartbleed bug, the first such arrest since the bug was discovered this month.

The Royal Canadian Mounted Police said they had arrested the man, identified as Stephen Arthuro Solis-Reyes, on Tuesday at his London, Ontario residence. They said they seized computer equipment at his home.

Mr. Solis-Reyes, who faces two charges under the Canadian Criminal Code, is scheduled to appear in court in Ottawa on July 17, the RCMP said in a statement.

Mr. Solis-Reyes is a computer science major at Western University, according to a university spokesman. In 2012, he won a computer science prize for his high school, the Mother Teresa Catholic Secondary School in London, Ontario. It remains unclear whether Mr. Solis-Reyes was working alone, or what he did with the information he allegedly stole.

The police action came two days after the country's tax authority, the Canada Revenue Agency, said someone had, in a "malicious" act, removed about 900 social insurance numbers--akin to U.S. Social Security numbers--from its systems. The agency knew about the breach on Friday, but the RCMP had asked it to delay disclosing the matter until Monday.

"The RCMP treated this breach of security as a high priority case and mobilized the necessary resources to resolve the matter as quickly as possible," RCMP Assistant Commissioner Gilles Michaud said in the statement.

The police said the investigation is continuing. It wasn't clear whether there might be more arrests. RCMP spokeswoman Lucy Shorey declined to provide further information.

"We're still analyzing the data, and we are still very much involved in this," Cpl. Shorey said.

London police spokesman Constable Ken Steeves directed questions about the arrest to the RCMP. He said he couldn't disclose any details that might jeopardize the RCMP probe.

"It's not our investigation. We were only involved with what was asked of us. We're not apprised of all the information," he said.

David George-Cosh and Will Connors contributed to this article

Write to Nirmala Menon at nirmala.menon@wsj.com

Subscribe to WSJ: http://online.wsj.com?mod=djnwires