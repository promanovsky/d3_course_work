A sweetener developed from agave - the plant used for making tequila, can help reduce blood sugar and weight, suggests a recent study.

Agavins, a natural form of sugar found in the agave plant, would not raise blood glucose as they are non-digestible and can act as a dietary fiber unlike artificial sweeteners, which are absorbed by the body and can cause side effects, like headaches, say the researchers.

In addition, agavins can help a diabetic feel fuller, which could help them eat less, added the researchers. However, the only slight downside is that agavins are not as sweet as their artificial counterparts.

As part of the study, the researchers led by Mercedes López, gave a group of mice a standard diet and water mixed with agavins. After weighing the mice daily and conducting weekly glucose checks, the researchers found that most mice that drank agavins, ate less, lost weight and had a drop in blood glucose levels compared to other sweeteners such as glucose, fructose, sucrose, agave syrup and aspartame.

According to the researchers, Agavins work by reducing glucose levels and increases the level of GLP-1, a gut hormone that stimulates insulin secretion.

The study findings were presented at a meeting of the American Chemical Society.

For comments and feedback contact: editorial@rttnews.com

Business News