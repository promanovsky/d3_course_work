Investing.com - The dollar traded higher against the yen on Wednesday after data revealed demand for U.S. durable goods came in much stronger in February than anticipated, which kept expectations firm for the Federal Reserve to tighten monetary policy in about a year.

In U.S. trading, was up 0.09% and trading at 102.36, up from a session low of 102.25 and off a high of 102.47.

The pair was expected to test support at 102.10, Tuesday's low, and resistance at 102.64, Monday's high.

The dollar rose after the Commerce Department reported that U.S. durable goods orders rose 2.2% in February, wiping out two months of declines and surpassing expectations for a 1.0% increase.

Core durable goods orders, which exclude transportation items, inched up 0.2%, slightly below forecasts for a 0.3% gain.

The overall data indicated that economy is gaining momentum and brushing off a weather-related slowdown, which cemented expectations for the Fed to wind down its monthly asset-purchasing program this year and hike interest rates the next.

The Fed's asset-purchasing program, currently set at $55 billion in Treasury and mortgage debt a month, weakens the dollar by suppressing long-term interest rates.

The yen, meanwhile, was up against the euro and down against the pound, with down 0.21% at 141.10, and trading up 0.29% at 169.52.

The euro continued to come under pressure stemming from dovish comments from ECB officials on Tuesday, indicating that the monetary authority is mulling policy options to stave off deflationary risks.

Also on Wednesday, a widely-watched German consumer climate gauge remained unchanged last month.

In a report, research group Gfk said that its forward-looking index of Germany’s consumer climate remained unchanged at 8.5 for April from March, in line with market expectations.

On Thursday, the U.S. is to publish final data on fourth-quarter economic growth as well as weekly data on initial jobless claims and private-sector data on pending home sales.