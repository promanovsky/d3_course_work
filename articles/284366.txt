Tesla Motors Inc (NASDAQ:TSLA) has targeted China as a key market big time, and it is wasting no time. On June 18, any Shanghai resident who users Uber’s services will get to ride in a Model S as part of a one-day Experience Tesla event. An extra bonus in the deal is a free ride for those who book a Tesla on that day. To book a Tesla, Uber users just select the Model S when choosing from among the other options, which include a luxury sedan or a vehicle that seats seen people.

Tesla and Uber test a partnership

Uber made the announcement on its Chinese blog. And there’s more. Those who book with Uber on that day and have a valid driver’s license will be allowed to test drive the Model S. There is one limit to the deal, however. Uber requests that customers who book one of Tesla Motors Inc (NASDAQ:TSLA)’s Model S sedans limit their trips to about 15 minutes. This will provide more Uber users with the opportunity to ride in and possibly drive a Tesla.

According to CNET, a spokesperson for Tesla Motors Inc (NASDAQ:TSLA) said their partnership with Uber is a test. They will be trying to gauge interest in the Model S in China by offering test drives. Uber is also testing the partnership to see if offering a ride in a Model S boosts bookings.

How many Teslas?

One thing that Tesla Motors Inc (NASDAQ:TSLA) and Uber hasn’t made clear, however, is the number of Model S sedans that will be available on the test day. Also on Uber’s blog, it says there are going to be “a few super-secret Teslas on the system” and advises users to keep their “fingers crossed and keep trying.” In other words, some Uber users may be surprised if a Tesla shows up to pick them up instead of the car they picked—as long as it can carry the number of passengers they have indicated in their booking.

CNET’s sources said the reason the partnership is just being held a single day in Shanghai is leading up to something bigger. Uber is reportedly planning to add many more Teslas to its fleet of vehicles in other Chinese cities. Beijing is expected to be next, following Shanghai.

Shares of Tesla Motors Inc (NASDAQ:TSLA) edged upward by 1% in premarket trading this morning.