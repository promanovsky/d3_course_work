For obese patients with Type 2 diabetes, bariatric weight loss surgeries have not only proved effective in helping them attain a healthy Body Mass Index, but have also cured them of the acquired diabetic condition.

150 Type 2 diabetics participated in the study, which was spearheaded by the Cleveland Clinic. Of the 150 participants, 50 controlled their condition with a combination of medication and adapted diet and exercise habits; another 50 did the same in conjunction with gastric bypass surgery; while the remaining 50 opted instead for sleeve gastrectomies. In all cases, Type 2 diabetes had been the result of surplus weight gain and was proving difficult to manage with medication alone.

The study checked in on the patients regularly over the next three years, with the 100 participants who had undergone surgeries faring better than the patients on medication alone. Indeed, the patients who opted for surgical measures had blood sugar levels approaching the 'normal' threshold, with surgeons and researchers noting that the improvements began more or less instantly from the recovery time. The longer the patients went after surgery, the better their conditions became, effectively reversing the damage that caused Type 2 diabetes.

"At three years, the therapeutic gap - the difference between blood sugar in the surgical group and the medical group - got even larger in favor of surgery," said the study's co-author Dr. Philip Schauer, director of the Cleveland Clinic's Bariatric and Metabolic Institute.

Most of the patients were able to stop taking insulin-based treatments: just five to 10 percent of the surgery patients were still taking them three years after the surgery. Compared with 55 percent of patients still taking the medication in the non-surgical group, quality of life for the groups opting for surgery was undoubtedly on a sharp incline.

"People who had surgery were able to feel less body pain, they were more functional, they had a better quality of life, and their general health was better," said co-author Dr. Sangeeta Kashyap, an endocrinologist at the Cleveland Clinic, to NBC News. "People were happier and healthier as a result of having the bariatric surgery."

The study was published in the New England Journal of Medicine.

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.