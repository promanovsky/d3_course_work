A team of doctors ruled Oscar Pistorius was not mentally incapacitated when he allegedly shot and killed his girlfriend, Reeva Steenkamp.

The ruling was delivered in court Monday, after the trial took a month-long hiatus so Pistorius could visit with doctors. An independent panel of psychiatric experts from the Weskoppies Psychiatric Hospital performed a series of tests on the former Olympic sprinter and concluded that “at the time of the alleged offenses, the accused did not suffer from a mental disorder or mental defect that affected his ability to distinguish between the rightful or wrongful nature of his deeds,” according to Gerrie Nel, the chief prosecutor.

"Mr. Pistorius was capable of appreciating the wrongfulness of his act," the doctors' report read, according to CNN.

Establishing Pistorius' proper mental health was a fight started by the defense and may ultimately backfire, as the prosecution pushed it to it's logical conclusion. Per The New York Times:

Before the trial adjourned last month, Dr. Merryll Vorster, a forensic psychiatrist called by the defense, said Mr. Pistorius’s anxiety dated to the amputation of his legs, exacerbated by a childhood when his father was frequently absent and his mother was so worried about intruders that she slept with a firearm under her pillow. The athlete’s condition, which worsened with time, made him “hypervigilant” about potential threats and led him to respond to perceived danger with reflexes of “fight” rather than “flight,” Dr. Vorster said. Mr. Nel, the prosecutor, used the testimony to demand a psychiatric examination of Mr. Pistorius.

Pistorius is accused of shooting his girlfriend, 29-year-old model Steenkamp, four times. The prosecution alleges Pistorius was in a fit of jealous rage at the time and the trial could have ended abruptly if doctors found Pistorius mentally unfit. Pistorius would have potentially gone free or been committed to a mental health facility, if doctors' ruled differently. The defense contends Pistorius thought Steenkamp was an intruder hiding in the bathroom. Establishing his mental health as something other than what doctors ruled today was key to their argument.

Story continues

Both sides will wrap up with closing arguments soon, and then the judge will begin a lengthy deliberation period, ultimately deciding Pistorius' fate with the help of two assessors.

This article was originally published at http://www.thewire.com/global/2014/06/psychiatrists-rule-oscar-pistorius-wasnt-crazy/373696/

Read more from The Wire

• What Is an Islamic Caliphate and Why Did ISIS Make One?