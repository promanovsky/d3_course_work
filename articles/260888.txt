hidden

A conservative Iranian court opened a case against instant messaging services WhatsApp and Instagram while also summoning Facebook CEO Mark Zuckerberg over complaints of privacy violation, state news agency ISNA reported on Tuesday.

The case underscores the growing struggle between moderate Iranian president Hassan Rouhani's drive to increase Internet freedoms and demands by the conservative judiciary for tighter controls.

The Iranian court in the southern province of Fars opened the cases against the social networks after citizens complained of breaches of privacy.

"According to the court's ruling, the Zionist director of the company of Facebook, or his official attorney must appear in court to defend himself and pay for possible losses," said Ruhollah Momen-Nasab, an Iranian internet official, according to state news agency ISNA, referring to Zuckerberg's Jewish background.

Zuckerberg, whose company owns WhatsApp and Instagram, is unlikely to heed the summons.

Iran is still under international sanctions over its disputed nuclear activities and it is difficult for U.S. citizens to secure travel visas, even if they want to visit.

Internet use is high in Iran, partly because many young Iranians turn to it to bypass an official ban on Western cultural products, and Tehran occasionally filters popular websites such as Twitter and Facebook.

Rouhani, in remarks that challenge hardliners who have stepped up measures to censor the Web, said earlier this month that Iran should embrace the Internet rather than see it as a threat.

A Rouhani administration official said Iran would loosen Internet censorship by introducing "smart filtering", which only keeps out sites the Islamic government considers immoral.

Reuters