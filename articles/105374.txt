Initial jobless claims fell 9.6% to 300,000 for the week ending April 5, according to a Labor Department report released today. That's the lowest they've been in seven years.

After jumping up a revised 7.1% the previous week, this newest report blew away analysts' expectations of a smaller dip to 318,000 claims.

From a more long-term perspective, a 1.5% decrease in the four-week moving average to 316,250 initial claims puts the labor market on a steadier improvement trend. Both the latest week's claims and the four-week average fall significantly below 400,000, a cutoff point that economists consider a sign of an improving labor market.

On a state-by-state basis, only Pennsylvania and Texas recorded a decrease of more than 1,000 initial claims for the week ending March 29 (most recent available data). Pennsylvania's 2,010 decrease came primarily from fewer food-service layoffs, while Texas cited fewer manufacturing layoffs as the main reason for its 1,820-initial-claims dip.

For the same period, four states registered increases of more than 1,000 initial claims. California accounted for the vast majority of the hike, with services layoffs contributing to a 17,630-initial-claims increase for the Golden State.

link