Federal regulators in the U.S. want to hear from companies using engineered micro-particles in their products, part of an effort to stay abreast of the growing field of nanotechnology.

The U.S. Food and Drug Administration issued final recommendations Tuesday for companies using nanotechnology in products regulated by the government, which can include medical therapies, food and cosmetics.

FDA regulators want companies to consult with the government before launching nanotechnology products, though the decision will essentially rest with manufacturers.

The guidelines don't make a judgment on the overall safety of nanotechnology or even define the term.

Industry groups generally consider nanoparticles as those less than 100 nanometers wide. A nanometer is one billionth of a meter. A human hair, for example, is 80,000 nanometers thick, while a sheet of paper is 100,000 nanometers.