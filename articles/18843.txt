Russell Crowe stars as Noah in the film inspired by the epic story of courage, sacrifice and hope. Directed by filmmaker Darren Aronofsky.

Russell Crowe stars as Noah in the film inspired by the epic story of courage, sacrifice and hope. Directed by filmmaker Darren Aronofsky....

POPE Francis reportedly pulled the plug on a potential powwow with “Noah’’ star Russell Crowe at the Vatican this week, apparently because he got cold feet about the media circus it would create.

The simplicity-loving pontiff had tentatively agreed to sit down with the burly Australian actor on Wednesday around 8.30am, Variety reported.

Crowe, 49, had personally reached out to the pope last month via Twitter, offering to let him view the flick before the multitudes, in the hope he’d give the biblical epic his blessing. “Dear Holy Father ... The message of the film is powerful, fascinating, resonant,’’ Crowe wrote.

Vatican officials then tentatively agreed to a meeting, but Francis then reconsidered, worried about the media frenzy it would create, as Crowe and his entourage touched down in Rome. Noah premiered in Madrid on Monday night.

A source told the New York Post: “[The Vatican] was concerned that because Russell is such a big celebrity, a meeting could become a huge spectacle.”

Paramount recently added to the film’s promotional campaign: “While artistic license has been taken, we believe that this film is true to the essence, values and integrity of a story that is a cornerstone of faith for millions of people worldwide,” after some controversy stirred around the big-budget movie’s less-than-traditional interpretation of the biblical tale, sources have said.

In Moscow this week to promote the film, Crowe said: “[People] consider Noah to be a benevolent figure because he looked after the animals: ‘Awww, Noah.’ It’s like, are you kidding me? This is the dude that stood by and watched the entire population of the planet perish. He’s not benevolent. He’s not even nice. At one point in the story, his son says, ‘I thought you were chosen because you were good?’ And he goes, ‘I was chosen because I can get the job done, mate.’’’

Read more at the New York Post.