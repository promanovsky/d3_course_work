HOME > The Bachelorette > The Bachelorette 10 'The Bachelorette' star Andi Dorfman eliminates Cody Sattler and JJ O'Brien

By Elizabeth Kwiatkowski, 06/24/2014



star



ADVERTISEMENT Cody Sattler, a 28-year-old personal trainer from Chicago, IL, was eliminated in the middle of his one-on-one date because Andi just didn't feel romantic chemistry with him. In addition, JJ O'Brien, a 30-year-old entrepreneur from San Francisco, CA, was ousted traditionally during the sixth episode's Rose Ceremony.



"I did not expect it at all. Ugh. From Day 1, I started falling for her and started getting hopeful, thinking about what our life could be like together. I thought of her becoming my wife and making a life together. It made me happy. It just hurts for your heart to be wrong. I have never fallen in love before and my greatest fear coming here was getting hurt. It happened," JJ said following his ouster.



's sixth tenth-season episode featured three dates: two one-on-one dates and one group date in Venice, Italy.



With eight men still in the running for Andi's heart,



Andi had a lot of questions for Nick following the last group date, so she felt she needed to get to the bottom of things. Although she knew Nick would be the unpopular choice for the date, she hoped the guys would ultimately trust her decisions. Andi noted something wasn't sitting right with Nick, whom she had previously called "salty." She didn't like that Nick failed to get along with the guys in the house, saying it's unattractive when a man makes enemies so easily.



For their date, the couple walked the streets, had pizza and Nick planned to show Andi that her feedback mattered to him. Andi wondered if Nick was cocky and arrogant or simply misunderstood. She realized she'd rather be heartbroken by letting him go now than wait it out for the long run. The pair then took a romantic gondola ride during which Nick brought up the issue right away.



Meanwhile, the following guys learned they must share the week's group date with Andi: JJ;



As a result, Cody was thrilled to discover he'd finally get a one-on-one date.



Back on Nick's date, he told the cameras their time together was "surreal and unbelievable." Andi was everything he was looking for in someone to spend the rest of his life with. They kissed and Nick said it was easy to get lost in the moment.



"We're back!" Nick said of their seemingly mended relationship.



Andi felt their issues were resolved, however, the night portion of the date would be "sink or swim" for Nick, who had told the cameras he was "falling in love" with the Bachelorette. He felt they belonged together, like it just worked between them. The couple then had dinner in a masquerade hall and Andi asked Nick why the guys hated him.



Nick explained he was "rattled" during his prior argument with Cody over using the term "frontrunner" and allegedly mocking him for being thankful. Nick said he wasn't sure when he became the bad guy and people accusing him of being overconfident was hurting his feelings. Andi asked Nick if he truly thought he was a "frontrunner," and Nick replied that although he didn't like the word, he couldn't imagine another guy sharing the same connection they shared.

RELATED LINK: 'THE BACHELORETTE' COUPLES NOW

'The Bachelorette' Couples Now: See whom each Bachelorette and their final bachelor ultimately ended up married or engaged to! (PHOTO GALLERY)

Nick told Andi he tried to be friends with the guys and maintain those friendships, but Andi was his No. 1 priority. Nick liked how open Andi was in regards to her concerns, and he told her that she'd challenge him and make him a better person. Nick felt he could rely on her and he appreciated that.



"I can confidently say, I mean, I'm definitely falling in love. I know we have a long way to go, but I definitely am," Nick told Andi at dinner.



Andi was surprised about Nick's honesty and respected how he took responsibility for his words and actions. Andi then offered him the date's rose and felt reassured in their relationship. Her skepticism had disappeared. The pair then danced in the courtyard to live classical instruments and kissed. Andi noted in a confessional she could eventually look back on how Venice was where she had fallen in love with Nick.



The next day, Andi embarked on her group date with the guys in Monselice. Andi also received a third romantic letter from her secret admirer. During the group date, the guys were hooked up to a lie detector test. Andi claimed she trusted all of them and therefore wanted them to have "a little fun with it." She was prepared, however, for surprises. When the guys learned about the test, their mouths dropped and they looked very nervous.



"I never lie. That's a lie," JJ told the cameras with a laugh.



Chris admitted in a confessional he was hiding something from Andi but didn't want the news to come out via the test, which Andi also participated in with hard-hitting questions of her own. While the guys just tried to make the best of the situation, Josh was especially displeased and turned off. He didn't like the fact you couldn't explain yourself other than a "yes or no" answer. He also wanted to earn Andi's trust on his own without the use of a lie detector.



Questions the bachelors were asked included -- Are you good in bed? Do you prefer blondes to brunettes? Do you wash your hands after you go to the bathroom? Have you ever cheated? Are you ready for marriage? Do you want kids?



After taking his test, Dylan pulled Andi aside and said he felt awful. He told her that his stomach and head hurt, so he headed back to the hotel.



ADVERTISEMENT



In the end, the man who orchestrated the lie detector test revealed three men had told zero lies, one bachelor told two lies, and two suitors boldly lied three times. Andi told two lies. Andi received the men's results and the men received hers. Brian immediately opened the envelope containing Andi's answers and learned she had lied about Italy being her favorite country in the world and the fact she believes all the guys are on the show for the right reasons.



Andi loved having the answers about what's important to her right in her hands, but she also wanted the guys to be convinced that she trusted them. Therefore, she decided to rip up the envelope containing the results from her bachelors' tests. The gesture was apparently very significant to Josh.



That night, Brian stole Andi first for some alone time. He gave her a human lie detector test, which they both jokingly passed. He got a kiss out of it and assumed she'd appreciate him taking initiative.



Marcus then revealed to Andi he had considered leaving the show before his one-on-one date but decided against it because he believed in them as a couple and didn't want to live with regrets. The news was a wake-up call to Andi because she realized she might've been taking Marcus' vulnerability and openness for granted.



"I am in love with you," Marcus told the Bachelorette.



Josh and Andi then talked about the day's date. He explained how glad he was that she ripped up the guys' results, but that made Andi wonder whether Josh was hiding something. She then became paranoid and a bit argumentative.



"A cloud of doubt" came upon her, and Andi began questioning her feelings. She started to wish she had read the results since Brian had read hers aloud anyway. Andi was feeling vulnerable and wanted to make sure she wasn't opening up for nothing. She didn't know how genuine people were acting and whether she had summed people up incorrectly.



Chris then admitted to Andi he was her secret admirer, which apparently didn't come as much of a surprise to Andi. He explained that he meant every word he wrote. Chris said in a confessional it felt awesome to "put himself out there." They kissed and he noted the moment was "incredible, one of the best experiences" of his entire life. Andi told the cameras Chris always served as her "dash of hope" when times got tough.



Andi then gave the group date's rose to Chris -- a guy with whom she had "trust and confidence." Chris felt reassured about how their relationship was developing.



Meanwhile, Josh wasn't happy about the mood-change and how he had argued with Andi, leaving their alone time on a sour note. JJ was also openly expressing frustration about other men advancing, getting roses, and getting dates. He said they shouldn't be congratulating one another because another man's success in the competition simply meant he was more fated and at risk to get sent home. Chris didn't like JJ's attitude at all, yelling at him to keep his negativity to himself. Chris thought JJ's "true colors" would eventually shine.



The next day, Cody embarked on his date with Andi in Verona. She thought he had the best eyes but was also well aware their relationship was a slow-burn and much further behind the rest of the pack. Cody told the cameras he just needed one chance to make Andi fall in love with him. He was ready to be Andi's "Romeo," and he didn't care that he got the last one-on-one date of the season.



Andi and Cody responded to some romantic "Letters to Juliet." Cody related to a guy who got nervous and tongue tied around "the girl of his dreams," hinting to Andi that's how he felt about her. Cody opened up to Andi throughout the date, explaining that he was an emotional guy and she gave him butterflies like he was a 16-year-old boy again. Andi was so impressed with the advice Cody gave to a guy in his letter.



ADVERTISEMENT



"You'll fall in love with me the more you stay around me. I'm telling you right now, hands down, the longer you keep me around, you're in trouble because you're going to like me more and more," Cody explained to the Bachelorette.



Andi stopped him in tears and said there was just a friendship at that point. She didn't know if she saw potential in a romance and it was scary for her not to know. Andi felt she didn't deserve such attention and praise because she was breaking so many hearts. Andi tried to let Cody down gently, explaining that she had so much respect for him and he had made her feel so amazing and special. Andi didn't want to drag things out just to hurt him worse down the road. She felt letting him go right then and there was "the right thing to do."



It was a "tough pill" for Cody to swallow, and he realized that you can't always get what you want. Andi felt terrible about the elimination and causing a great, genuine guy heartache. She said it was the worst moment for her thus far in the journey.



"You're killing me," Cody repeated to Andi as she walked him out.



"I can't believe it, you know?" Cody said following his ouster. "Man. It's hard. It sucks. It's just rejection. That's the bottom line."



During the night of the cocktail party, Nick managed to piss the guys off once again by being the first person to steal Andi away for some alone time even though he already had a rose. Dylan then interrupted their conversation to get some time of his own. Chris told Nick it was an "arrogant, disappointing move."



Josh then talked to Andi and asked her to forget their whole previous conversation. She admitted such an exchange made her fear that her journey may end in heartbreak. Josh insisted he wouldn't hurt Andi -- that he wanted to be the last one standing and the last one there for her.



Andi later admitted to



The Rose Ceremony then commenced. Besides Nick and Chris, Andi handed out roses to Dylan, Brian, Marcus, and Josh in that exact order.



When walking JJ out, Andi explained she just didn't envision a future with him and wanted to let him go before things got more emotional. JJ admitted Andi's decision wasn't easy for him but he was glad she sent him home sooner rather than later.





About The Author: Elizabeth Kwiatkowski

Elizabeth Kwiatkowski is Associate Editor of Reality TV World and has been covering the reality TV genre for more than a decade.

FOLLOW REALITY TV WORLD ON GOOGLE NEWS star Andi Dorfman sent two more bachelors packing during Monday night's broadcast of the ABC reality dating series' tenth season.Cody Sattler, a 28-year-old personal trainer from Chicago, IL, was eliminated in the middle of his one-on-one date because Andi just didn't feel romantic chemistry with him. In addition, JJ O'Brien, a 30-year-old entrepreneur from San Francisco, CA, was ousted traditionally during the sixth episode's Rose Ceremony."I did not expect it at all. Ugh. From Day 1, I started falling for her and started getting hopeful, thinking about what our life could be like together. I thought of her becoming my wife and making a life together. It made me happy. It just hurts for your heart to be wrong. I have never fallen in love before and my greatest fear coming here was getting hurt. It happened," JJ said following his ouster.'s sixth tenth-season episode featured three dates: two one-on-one dates and one group date in Venice, Italy.With eight men still in the running for Andi's heart, Nick Viall , a 33-year-old software sales executive from Chicago, IL, got the first one-on-one date of the week -- which was actually his second one-on-one date with Andi of the season. All the guys expected Cody to get the date because he was the only guy left in the house who hadn't enjoyed a one-on-one with Andi yet. Cody was definitely frustrated and said he felt like "the pet dog" of the group in that he was just being dragged around from destination to destination.Andi had a lot of questions for Nick following the last group date, so she felt she needed to get to the bottom of things. Although she knew Nick would be the unpopular choice for the date, she hoped the guys would ultimately trust her decisions. Andi noted something wasn't sitting right with Nick, whom she had previously called "salty." She didn't like that Nick failed to get along with the guys in the house, saying it's unattractive when a man makes enemies so easily.For their date, the couple walked the streets, had pizza and Nick planned to show Andi that her feedback mattered to him. Andi wondered if Nick was cocky and arrogant or simply misunderstood. She realized she'd rather be heartbroken by letting him go now than wait it out for the long run. The pair then took a romantic gondola ride during which Nick brought up the issue right away.Meanwhile, the following guys learned they must share the week's group date with Andi: JJ; Marcus Grodd , a 25-year-old sports medicine manager from Dallas, TX; Dylan Petitt , a 26-year-old accountant from Boston, MA; Chris Soules , a 32-year-old farmer from Arlington, IA; Josh Murray , a 29-year-old former professional baseball player from Atlanta, GA; and Brian Osborne , a 27-year-old basketball coach from Camp Hill, PA.As a result, Cody was thrilled to discover he'd finally get a one-on-one date.Back on Nick's date, he told the cameras their time together was "surreal and unbelievable." Andi was everything he was looking for in someone to spend the rest of his life with. They kissed and Nick said it was easy to get lost in the moment."We're back!" Nick said of their seemingly mended relationship.Andi felt their issues were resolved, however, the night portion of the date would be "sink or swim" for Nick, who had told the cameras he was "falling in love" with the Bachelorette. He felt they belonged together, like it just worked between them. The couple then had dinner in a masquerade hall and Andi asked Nick why the guys hated him.Nick explained he was "rattled" during his prior argument with Cody over using the term "frontrunner" and allegedly mocking him for being thankful. Nick said he wasn't sure when he became the bad guy and people accusing him of being overconfident was hurting his feelings. Andi asked Nick if he truly thought he was a "frontrunner," and Nick replied that although he didn't like the word, he couldn't imagine another guy sharing the same connection they shared.Nick told Andi he tried to be friends with the guys and maintain those friendships, but Andi was his No. 1 priority. Nick liked how open Andi was in regards to her concerns, and he told her that she'd challenge him and make him a better person. Nick felt he could rely on her and he appreciated that."I can confidently say, I mean, I'm definitely falling in love. I know we have a long way to go, but I definitely am," Nick told Andi at dinner.Andi was surprised about Nick's honesty and respected how he took responsibility for his words and actions. Andi then offered him the date's rose and felt reassured in their relationship. Her skepticism had disappeared. The pair then danced in the courtyard to live classical instruments and kissed. Andi noted in a confessional she could eventually look back on how Venice was where she had fallen in love with Nick.The next day, Andi embarked on her group date with the guys in Monselice. Andi also received a third romantic letter from her secret admirer. During the group date, the guys were hooked up to a lie detector test. Andi claimed she trusted all of them and therefore wanted them to have "a little fun with it." She was prepared, however, for surprises. When the guys learned about the test, their mouths dropped and they looked very nervous."I never lie. That's a lie," JJ told the cameras with a laugh.Chris admitted in a confessional he was hiding something from Andi but didn't want the news to come out via the test, which Andi also participated in with hard-hitting questions of her own. While the guys just tried to make the best of the situation, Josh was especially displeased and turned off. He didn't like the fact you couldn't explain yourself other than a "yes or no" answer. He also wanted to earn Andi's trust on his own without the use of a lie detector.Questions the bachelors were asked included -- Are you good in bed? Do you prefer blondes to brunettes? Do you wash your hands after you go to the bathroom? Have you ever cheated? Are you ready for marriage? Do you want kids?After taking his test, Dylan pulled Andi aside and said he felt awful. He told her that his stomach and head hurt, so he headed back to the hotel.Chris' big secret ended up being the fact he's Andi's secret admirer. He wanted to tell her in a personal, romantic setting -- not within a group dynamic.In the end, the man who orchestrated the lie detector test revealed three men had told zero lies, one bachelor told two lies, and two suitors boldly lied three times. Andi told two lies. Andi received the men's results and the men received hers. Brian immediately opened the envelope containing Andi's answers and learned she had lied about Italy being her favorite country in the world and the fact she believes all the guys are on the show for the right reasons.Andi loved having the answers about what's important to her right in her hands, but she also wanted the guys to be convinced that she trusted them. Therefore, she decided to rip up the envelope containing the results from her bachelors' tests. The gesture was apparently very significant to Josh.That night, Brian stole Andi first for some alone time. He gave her a human lie detector test, which they both jokingly passed. He got a kiss out of it and assumed she'd appreciate him taking initiative.Marcus then revealed to Andi he had considered leaving the show before his one-on-one date but decided against it because he believed in them as a couple and didn't want to live with regrets. The news was a wake-up call to Andi because she realized she might've been taking Marcus' vulnerability and openness for granted."I am in love with you," Marcus told the Bachelorette.Josh and Andi then talked about the day's date. He explained how glad he was that she ripped up the guys' results, but that made Andi wonder whether Josh was hiding something. She then became paranoid and a bit argumentative."A cloud of doubt" came upon her, and Andi began questioning her feelings. She started to wish she had read the results since Brian had read hers aloud anyway. Andi was feeling vulnerable and wanted to make sure she wasn't opening up for nothing. She didn't know how genuine people were acting and whether she had summed people up incorrectly.Chris then admitted to Andi he was her secret admirer, which apparently didn't come as much of a surprise to Andi. He explained that he meant every word he wrote. Chris said in a confessional it felt awesome to "put himself out there." They kissed and he noted the moment was "incredible, one of the best experiences" of his entire life. Andi told the cameras Chris always served as her "dash of hope" when times got tough.Andi then gave the group date's rose to Chris -- a guy with whom she had "trust and confidence." Chris felt reassured about how their relationship was developing.Meanwhile, Josh wasn't happy about the mood-change and how he had argued with Andi, leaving their alone time on a sour note. JJ was also openly expressing frustration about other men advancing, getting roses, and getting dates. He said they shouldn't be congratulating one another because another man's success in the competition simply meant he was more fated and at risk to get sent home. Chris didn't like JJ's attitude at all, yelling at him to keep his negativity to himself. Chris thought JJ's "true colors" would eventually shine.The next day, Cody embarked on his date with Andi in Verona. She thought he had the best eyes but was also well aware their relationship was a slow-burn and much further behind the rest of the pack. Cody told the cameras he just needed one chance to make Andi fall in love with him. He was ready to be Andi's "Romeo," and he didn't care that he got the last one-on-one date of the season.Andi and Cody responded to some romantic "Letters to Juliet." Cody related to a guy who got nervous and tongue tied around "the girl of his dreams," hinting to Andi that's how he felt about her. Cody opened up to Andi throughout the date, explaining that he was an emotional guy and she gave him butterflies like he was a 16-year-old boy again. Andi was so impressed with the advice Cody gave to a guy in his letter.At dinner that night, Cody wrote Andi a letter of her own. He wrote about his own love story with her, which was getting to know a beautiful and down-to-earth woman whom he could envision as his wife. Cody wanted to really get to know Andi, saying he'd love to take her home to Chicago to meet his family and take their connection to a whole new level. Cody insisted he just wanted to grab and kiss Andi and simply be with her. Cody continued to pour his heart out as Andi broke down into tears."You'll fall in love with me the more you stay around me. I'm telling you right now, hands down, the longer you keep me around, you're in trouble because you're going to like me more and more," Cody explained to the Bachelorette.Andi stopped him in tears and said there was just a friendship at that point. She didn't know if she saw potential in a romance and it was scary for her not to know. Andi felt she didn't deserve such attention and praise because she was breaking so many hearts. Andi tried to let Cody down gently, explaining that she had so much respect for him and he had made her feel so amazing and special. Andi didn't want to drag things out just to hurt him worse down the road. She felt letting him go right then and there was "the right thing to do."It was a "tough pill" for Cody to swallow, and he realized that you can't always get what you want. Andi felt terrible about the elimination and causing a great, genuine guy heartache. She said it was the worst moment for her thus far in the journey."You're killing me," Cody repeated to Andi as she walked him out."I can't believe it, you know?" Cody said following his ouster. "Man. It's hard. It sucks. It's just rejection. That's the bottom line."During the night of the cocktail party, Nick managed to piss the guys off once again by being the first person to steal Andi away for some alone time even though he already had a rose. Dylan then interrupted their conversation to get some time of his own. Chris told Nick it was an "arrogant, disappointing move."Josh then talked to Andi and asked her to forget their whole previous conversation. She admitted such an exchange made her fear that her journey may end in heartbreak. Josh insisted he wouldn't hurt Andi -- that he wanted to be the last one standing and the last one there for her.Andi later admitted to Chris Harrison she kind of regretted not reading the lie detector results, especially because it caused a combative argument between herself and Josh.The Rose Ceremony then commenced. Besides Nick and Chris, Andi handed out roses to Dylan, Brian, Marcus, and Josh in that exact order.When walking JJ out, Andi explained she just didn't envision a future with him and wanted to let him go before things got more emotional. JJ admitted Andi's decision wasn't easy for him but he was glad she sent him home sooner rather than later. THE BACHELORETTE 10 THE BACHELORETTE SPOILERS MORE THE BACHELORETTE 10 NEWS << PRIOR STORY

'American Idol' judging panel to feature Jennifer Lopez, Keith Urban and Harry Connick Jr. again NEXT STORY >>

Rob "Boston Rob" Mariano and wife Amber Mariano have another baby, now parents to four little girls

Get more Reality TV World! Follow us on Twitter, like us on Facebook or add our RSS feed.











ADVERTISEMENT

































ADVERTISEMENT





- - - - - - - - - -













































Page generated Thu Feb 11, 2021 13:50 pm in 1.2121331691742 seconds



