The CEO of General Motors apologized for an ignition switch defect that was linked to more than a dozen deaths -- and which triggered a massive vehicle recall -- in prepared testimony for the House Energy and Commerce Committee.

CEO Mary Barra is set to testify on Tuesday before the committee about the defect, amid mounting questions over what GM and others could have done to prevent it.

In prepared remarks, Barra vowed that the company will be "fully transparent" and "will not shirk from our responsibilities now and in the future."

"Today's GM will do the right thing," she said. "That begins with my sincere apologies to everyone who has been affected by this recall -- especially to the families and friends of those who lost their lives or were injured. I am deeply sorry."

She noted that the company has named a new vice president for Global Vehicle Safety, and said customers affected by the recall "are getting our full and undivided attention."

More On This...

New details about the defect have been emerging from the committee in advance of the hearing. One letter from committee members said General Motors officials approved a sub-standard design for ignition switches back in 2002.

Further, congressional investigators said federal regulators had declined on two separate occasions to open formal probes into complaints about the ignition switch defect in certain General Motors cars.

The findings from the House Energy and Commerce Committee emerged as part of a continuing investigation into events surrounding GM's eventual recall of 2.6 million small cars due to the defect, which has been linked to 13 deaths in traffic accidents. The investigators also determined that GM rejected a proposed fix for the problem in 2005 due to both the length of time needed for repair and the costs involved.

In response to the panel, the National Highway Traffic Safety Administration released a statement Sunday saying it had "reviewed data from a number of sources in 2007, but the data we had available at the time did not warrant a formal investigation."

The cars were recalled by GM due to a flaw which causes ignition switches to move from the "run" to the "accessory" or "off" position, which causes the car to stall and disables the air bags and power steering. The recall includes the Chevrolet Cobalt, Chevrolet HHR, Pontiac G5, Pontiac Solstice, Saturn Ion and Saturn Sky from the 2003-2011 model years.

GM also announced a new recall on Monday of more than 1.3 million vehicles in the U.S. to fix a problem that could cause the sudden loss of power steering.

Investigations by the House panel, a Senate committee, regulators and federal prosecutors are continuing. GM CEO Mary Barra and NHTSA Administrator David Friedman are scheduled to appear Tuesday before a House Energy and Commerce subcommittee. A separate Senate hearing is scheduled for Wednesday.

According to the memo, in September 2007, the then-head of NHTSA's defect assessments division emailed other officials in the Office of Defects Investigation recommending a probe into why front air bags weren't deploying in crashes involving 2003-2006 Chevrolet Cobalts and Saturn Ions.

"Notwithstanding GM's indications that they see no specific problem pattern, DAD perceives a pattern of nondeployments in these vehicles that does not exist in their peers," the email, which is cited by House investigators, said.

But on Nov. 15, 2007, NHTSA officials concluded there was no discernible trend, and it decided not to pursue the matter, the House memo states.

In 2010, NHTSA officials again considered data on whether the Cobalt had a problem with malfunctioning air bags but again decided there was no trend, the memo states.

Congress is also investigating why GM didn't recall the cars sooner, because it first found problems with the ignition switches in 2001. The House memo provides new details about GM's consideration - but ultimate rejection - of potential solutions.

According to the memo, GM engineers met in February 2005 to consider making changes to the ignition switch after reports it was moving out of position and causing cars to stall. But an engineer said the switch was "very fragile" and advised against changes. In March 2005, the engineering manager of the Cobalt closed the case, saying an ignition switch fix would take too long and cost too much, and that "none of the solutions represents an acceptable business case."

In May 2005, the company's brand quality division requested a new investigation into ignitions turning off while driving, and a new review suggests changing the design of the key so it wouldn't drag down the ignition. That proposal was initially approved but later cancelled.

In a statement released Sunday, GM said it deeply regrets the events that led to the recall.

"We are fully cooperating with NHTSA and the Congress and we welcome the opportunity to help both have a full understanding of the facts," the company said.

The Associated Press contributed to this report.