It identified Pakistan, Syria, and Cameroon as having allowed the virus to spread beyond their borders, and the group recommended that those three governments require citizens to obtain a certificate proving they have been vaccinated for polio before traveling abroad.

The agency described polio outbreaks across at least 10 countries in Asia, Africa, and the Middle East as an ‘‘extraordinary event’’ that required a coordinated international response.

LONDON — For the first time, the World Health Organization on Monday declared the spread of polio an international public health emergency that could grow in the next few months and unravel the nearly three-decade effort to eradicate the crippling disease.

Advertisement

‘‘Until it is eradicated, polio will continue to spread internationally, find and paralyze susceptible kids,’’ Dr. Bruce Aylward, who leads WHO’s polio efforts, said during a press briefing.

Critics, however, questioned whether Monday’s announcement would make much of a difference, given the limits faced by governments confronting not only polio but armed insurrection and widespread poverty.

‘‘What happens when you continue whipping a horse to go ever faster, no matter how rapidly he is already running?’’ said Dr. Donald A. Henderson, who led the WHO’s initiative to get rid of smallpox, the only human disease ever to have been eradicated.

The WHO has never issued an international alert on polio, a disease that usually strikes children under 5 and is most often spread through infected water. There is no specific cure, but several vaccines exist.

Experts are particularly concerned that polio is reemerging in countries previously free of the disease, such as Syria, Somalia, and Iraq, where civil war or unrest now complicates efforts to contain the virus. It is happening during the traditionally low season for the spread of polio, leaving experts worried that cases could spike as the weather becomes warmer and wetter in the coming months across the northern hemisphere.

Advertisement

The vast majority of new cases are in Pakistan, a country that an independent monitoring board set up by the WHO has called ‘‘a powder keg that could ignite widespread polio transmission.’’

Dozens of polio workers have been killed over the last two years in Pakistan, where militants accuse them of spying for the US government. Those suspicions stem at least partly from the disclosure that the CIA used a Pakistani doctor to uncover Osama bin Laden’s hideout by trying to get blood samples from his family under the guise of a hepatitis vaccination program.

US commandos killed the Al Qaeda leader in May 2011 in the Pakistani garrison town of Abbottabad.

At the end of last month, there were 68 confirmed polio cases worldwide, compared with 24 at the same time last year. In 2013, polio reappeared in Syria, sparking fears the civil war there could ignite a wider outbreak as refugees flee to other countries across the region. The virus has also been identified in the sewage system in Israel, the West Bank, and Gaza, although no cases have been spotted. In February, the WHO found that polio had also returned to Iraq, where it spread from neighboring Syria.