U.S. Agriculture Secretary Tom Vilsack said on Wednesday he was confident a vaccine approved this week by the U.S. administration would help fight a deadly virus that has killed millions of pigs in the United States.

The U.S. Department of Agriculture said on Wednesday preliminary studies of a vaccine developed by Harrisvaccines "have been promising'' in controlling Porcine Epidemic Diarrhea virus (PEDv). The virus has killed up to 8 million pigs and pushed pork prices to record highs since it was first identified in the United States last year.

Read More Farmers boost biosecurity measures to fight a deadly pig virus



"I don't want to say the virus will be eradicated, but I think you will see we're on the other side of this,'' Vilsack said after a speech to representatives of the French farm sector at the U.S. embassy in Paris.

He added that warmer temperatures over the summer would weaken the virus, while systematic notification by farmers of new PEDv cases and higher biosecurity measures at farms will help in the fight against PEDv.

Earlier this month, Vilsack ordered farmers to start reporting cases of the deadly pig virus and pledged more than $26 million in funding to combat the virus, pushing back against criticism of his handling of the outbreak.

Read More Iowa start-up moves to solve a deadly pig virus

