The self-styled pro-Russian mayor of the east Ukrainian town of Slovyansk has said that he will hold talks over the release of detained security OSCE observers with the West only if the EU rejects sanctions against separatist leaders.

A team of six observers from the Organisation for Security and Cooperation in Europe (OSCE), a democracy watchdog, was detained last week after separatists said they had found a Ukrainian spy among them.

The rebels paraded the captives before the media, saying they were ready for a prisoner swap.

But the fresh imposition of visa bans and asset freezes by the European Union against Denis Pushilin, leader of the self-styled People's Republic of Donetsk and Andrei Purgin, another separatist leader in east Ukraine, "was not conducive to dialogue", according to Ponomaryov

Ponomaryov said:

We will resume dialogue on the status of the prisoners of war only when the European Union rejects these sanctions. If they fail to remove the sanctions, then we will block access for EU representatives, and they won't be able to get to us. I will remind my guests from the OSCE about this.

UN Secretary-General Ban Ki-moon urged separatists to release the OSCE military observers along with 40 other people, who were locked in makeshift jails in Slovyansk.

"The Secretary-General strongly condemns the recent capture and detention of OSCE military monitors as well as a number of accompanying Ukrainian staff," a UN statement read. "He urges those responsible for their abduction to release them immediately, unconditionally and unharmed."

The captives come from Germany, the Czech Republic, Denmark, Poland and Sweden.