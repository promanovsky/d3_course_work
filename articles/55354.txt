A report out today said a lot more American children have autism than previously thought.

The Centers for Disease Control says one in 68 have it. In 2012, the estimate was one in 88. That's a 30 percent increase in two years.

The CDC says the findings don't mean more children are developing autism. It means more cases are being discovered due to heightened awareness.

Michael Rosanoff - with the advocacy group Autism Speaks - says this study looked at cases of 8-year-olds from 11 sites across the country.

Michael Rosanoff of Autism Speaks CBS News

"The approach that the CDC uses is based on medical and service records," Rosanoff said. "So that's why we believe that part of the increase in prevalence is due to better detection of autism. Better records and more records lead to higher prevalence."

Boys are still more likely to be diagnosed with an autism spectrum disorder than girls - 1 in 42, versus 1 in 189. Whites more likely than African Americans or Hispanics. Diagnoses varied widely across the country, ranging from 1 in 175 children in Alabama to 1 in 45 in New Jersey.

"The reason for the variation is not because there is additional risk of autism in certain states, it's because there are more and better quality records in some states than others," Rosanoff said.

Michael and Kevin Troy CBS News

The average age of diagnosis in the United States is 4 1/2. That's too late, say Lori and Kevin Troy in Franklin Square New York. Their twin 11-year-olds, Michael and Kevin, were diagnosed at 20 months.

"We were told 10 years ago that they would probably never speak, would never laugh or have friends or experience most of life," Lori Troy said.

And as for the boys now?

Twins11-year-old Michael and Kevin Troy were diagnosed at 20 months CBS News

Kevin is in a regular public school, while his brother Michael attends a program geared for those with autism, run by Brookville Center for Children's Services.

What was the key?

"Before 2 years they had 30 hours a week each of therapy in the house," Lori Troy said. "It just opens a whole new world of possibilities for them."



Asked whether the 30 percent could be a case of over-diagnosis, the CDC said that its clinicians used standard definitions of autism to evaluate each diagnosis in the study and that in it views, they are all reliable.