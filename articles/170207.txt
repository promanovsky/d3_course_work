Manama: Saudi Arabia is launching a strong drive to persuade the elderly, pregnant women and people with chronic diseases to postpone Haj this year in a bid to prevent the spread of the Middle East Respiratory Syndrome (Mers) coronavirus during the pilgrimage season.

Acting Health Minister Adel Faqih on Tuesday said that the foreign ministry is to ask its embassies and consulates abroad to convince the particularly vulnerable categories of people not to come to the kingdom that has emerged as the epicentre of the new respiratory virus.

More than 100 people have so far died in Saudi Arabia since the outbreak of the potentially deadly virus in 2012.

Around three million Muslims from all over the world congregate in the city of Makkah in western Saudi Arabia annually for at least four days to perform the Haj, the fifth and last pillar of Islam.

Last year, Saudi Arabia said that it would not issue Haj visas for the elderly and people with chronic diseases.

The kingdom has been deploying greater efforts to deal with the virus.

The minister said that three centres have been designated to treat and isolate patients with the syndrome as a record surge in April took the numbers of infected cases to 323.

The facilities are located at Prince Mohammad Bin Abdul Aziz Hospital in the capital Riyadh, King Abdullah Medical Complex in the Red Sea city of Jeddah, and Dammam Medical Complex in the Eastern Province.

Addressing a press conference, in the presence of experts from the World Health Organisation (WHO) and from Australia, Germany and Britain, the minister said that health officials have visited several hospitals in the kingdom in their follow up to Mers outbreak.

At the conference, his first since he was given the delicate health portfolio this month, the minister warned against “dealing directly with camels, particularly the ones not in stable condition.”

He said that those who are obliged to deal with camels should make sure to wear face masks, local news site Sabq reported.

When one of WHO experts said that camels were one of the sources of the Mers virus, the minister said that studies were being still conducted.

Business leaders in Saudi Arabia have challenged studies that referred to a link between Mers and camels, arguing that none of the people infected had close contacts with the animals.