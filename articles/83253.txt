HYDERABAD: The city may be one of the best in the country in terms of healthcare infrastructure, but when it comes to catering to the needs of numerous children suffering from the lifelong developmental disability called autism , it is in need of a massive booster dose.

The situation is grim right from the availability of doctors who can diagnose the disorder well in time to facilities providing counselling and therapy.

If the concept of out-of-home “respite centres” has already taken shape in developed nations, parents here are worried about the future of their children who cannot be left on their own.

“Compared to other metros, Hyderabad is still lacking in terms of therapists and infrastructure. Some good facilities exist but do not provide all the services under one roof and charge exorbitantly. We have to shuttle from one place to another,” K Lakshmi, founder member, Parents Association for Autistic Children (PAAC), said on World Autism Awareness Day on Wednesday.

Autism is a lifelong developmental disability that affects how a person communicates with and relates to other people. It also affects how they make sense of the world around them.

Lakshmi, who has a 17-year-old son suffering from autism, is anxious about his future. “Our main concern is non-availability of respite facilities here. We require proper respite facilities where the future of autistic children can be secure. Otherwise, who will look after him later in life,” she adds.

New studies by the Centers for Disease Control and Prevention (CDC) show that one in 68 children (1 in 42 boys and 1 in 189 girls) have autism spectrum disorder (ASD) in the United States. While no such study has been done in India, estimates suggest that around four million people with autism are living in the country.

“The main concern of parents of autistic children is what will happen after we are gone. We need a safe and secure environment for our children, especially girl children. The government should take the initiative,” said J Sharda Ram, founder director, Aarambh Association for Autism, and mother of a 14-year-old girl suffering from autism. “There is a dearth of training faculty in Hyderabad,” she said.

The prevailing situation clearly shows that the government is doing little. For instance, at Niloufer Hospital, which is the tertiary care facility for children in the state diagnosing around 10 cases of autism every month, the post of special educator is vacant. The post of psychiatrist social worker is non-existent now. “Previously, the post of psychiatrist social worker was there but now it is not. It is needed,” said Dr Rama Subba Reddy, child psychiatrist at Niloufer.

Dr Sita Jayalakshmi, neurologist at Krishna Institute of Medical Sciences pointed out that paediatric neurologists are very few in the state. “In AP, genetic problems are more common due to consanguineous marriages. If there is a family history, children should be screened periodically right from the age of six months,” suggested Dr Sita.

According to Om Sai Ramesh, faculty, National Institute of Mental Health, the cases are increasing alarmingly. He said genetic and environmental factors are possibly triggering the disease. “We are eating polluted food, drinking polluted water and the air we breathe is toxic,” said Ramesh.

He further said that while both urban and rural areas are having almost an equal load of children with autism, children in urban areas are getting little professional help but those in the rural areas do not get any help at all.