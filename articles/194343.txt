Now he's smiling! 'Grumpy' Kanye West grins while zip-lining with Kim Kardashian as more vintage vacation snaps emerge



His dejected posture before zip-lining with fiancee Kim Kardashian in a vintage vacation snap sparked countless Internet memes.

But once in the air, it seems Kanye West finally embraced the adventurous excursion.

The 36-year-old is seen grinning widely as he hangs from a wire during another picture taken on the same holiday with 33-year-old Kim.

Scroll down for video



Now he's smiling! Kanye West grins while zip-lining with Kim Kardashian during an old vacation, a far cry from a previous snap showing him looking dejected



Kanye is seen indulging in the popular vacation sport, secured in a harness that is attached to a thick wire.



He looks at the camera and smiles, in what appears to be a rare unguarded moment for the rapper, who generally looks surly when photographed.



Kim, meanwhile, gives a small smile, looking slightly more fearful as she hangs from the great height.

