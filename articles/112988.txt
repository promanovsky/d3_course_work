Hand scanning has become an alternative payment method for people in a city in southern Sweden, researchers at Lund University said today.

Vein scanning terminals have been installed in 15 shops and restaurants in Lund thanks to an engineering student who came up with the idea two years ago while waiting in line to pay.

Some 1,600 people have signed up already for the system, which its creator says is not only faster but also safer than traditional payment methods.

"Every individual's vein pattern is completely unique, so there really is no way of committing fraud with this system," researcher Fredrik Leifland said in a statement.

"You always need your hand scanned for a payment to go through."



While vein scanning technology existed previously, it has not been used as a form of payment before.

"We had to connect all the players ourselves, which was quite complex: the vein scanning terminals, the banks, the stores and the customers," Leifland added.

The creators have plans to further expand the business and other companies around the world are already starting to implement the new payment method.

To sign up users have to visit a shop or restaurant with a terminal, where they scan their palm three times and enter their social security and telephone numbers.

A text message is then sent to their mobile phone with an activation link to a website, with payments taken directly from customers bank accounts twice a month.