Batman: Arkham Knight, an open-world action-adventure game coming from Warner Bros. Interactive Entertainment in 2015, has to be totally awesome because it’s the final chapter of Rocksteady Studios’ Arkham trilogy. So naturally, it has a badass Batmobile, which can transform into a heavily armored mobile tank complete with missile launchers and a 60-millimeter cannon. This kind of cool gadgetry will appeal to longtime Batman fans, who are going to have to wait a little longer to get their hands on this game.

Rocksteady is pulling out the stops (and taking longer than expected) to try to make this the best Batman game ever, starting with a brand new villain, a highly skilled combat specialist and military leader named the Arkham Knight. He’s after Batman, and, based on a preview of the game that I saw in advance of the Electronic Entertainment Expo (E3) trade show in Los Angeles next week, the Arkham Knight is going to be a fierce and cutthroat bad guy.

The movie-like game will feature the Batmobile, a new Batsuit, new combat moves, armored warfare, and Batman’s traditional enemies, like Scarecrow and the new villain, who reports to Scarecrow but has his own secret agenda. And the world of Gotham City is five times bigger than the previous game’s Arkham City. After all, you have to have some room to drive that Batmobile around.

Batman tracks down a mysterious villain, who runs a sophisticated military force with tanks, drones, infantry, and other weapons of war. The Arkham Knight hates Batman, and he can bring an army against him. He speaks through a filter, sounding like a cocky Darth Vader.

“This guy has everything he needs to bring war to Gotham,” said Dax Ginn, the marketing producer at Rocksteady, who screams like a boxing match announcer.

This is where Rocksteady is aiming to produce a next-generation experience on the Xbox One, PlayStation 4, and PC. The game takes advantage of better graphics hardware to throw a lot more enemies at Batman onscreen at the same time. The potential for bigger brawls is there. That’s not easy to do, and it requires the horsepower of next-gen machines to be able to render it all at a high speed.

Image Credit: WBIE

But Batman finally has his Batmobile.

“She drives fast, she hits hard,” said Ginn, speaking at a press event. “Let’s kick some ass!”

In the first game, Batman: Arkham Asylum, published in 2009, Rocksteady brought back the brutal hand-to-hand combat and the fun of being the world’s greatest detective. In Batman: Arkham City, published in 2011, you could take to the air and fly around an open world. Now, “we have to give you the legendary vehicle to make you feel part of the game,” said Guy Perkins, the marketing manager for the title, in an interview with GamesBeat.

The Batmobile can drive fast like a race car or shift into Battle mode. In that mode, the tank morphs like a Transformer into a battle tank, with a heavy cannon, vulcan cannon, non-lethal riot suppressor, and missiles. As a tank, the Batmobile can take on the Arkham Knight’s own armored vehicles or just wreak havoc among his foot soldiers.

“We’ve got everything that Batman needs to defend the city,” Ginn said.

In chapter two, Batman meets with Commissioner Gordon at the Ace Chemicals factory. There, he finds that the Arkham Knight is mass-producing a toxin that can kill everybody on the Eastern seaboard. The Knight has taken the workers hostage, and Batman has to free them and stop the toxin production at the same time.

While the Arkham Knight has an army on his side, Batman has his gadgets. He uses his Bat Scanner to find the workers. His job is to sneak up on the guards and take them out in the most violent way before they can alert their friends. The combat is cinematic. When you jump through a glass ceiling, you move directly into combat and hit a bad guy in the guts for a finishing move. You have to take out three guards at once in a single move.

Batman gets help hacking into the communications of the soldiers to gather information about them and the Arkham Knight. When I played, it was easy to locate the hostages but hard to rescue them alive. The first one I rescued had already been killed by the guards. I had to call in the Batmobile via remote control and drive it into the compound.

The Arkham Knight predicted that Batman would try to save the hostages, and he set up an ambush to trap Batman. When Batman was surrounded by the Arkham Knight’s soldiers, it seemed like Batman was in a fix. But I used the remote to call up the Batmobile and have it shoot its riot gun at the soldiers from behind. They went down just like that.

That meant that I was able to rescue one worker, who could be stored safely amid all of the chemical gases inside the Batmobile. Then I had to deal with a bunch of armored tanks that came into the compound. Once transformed from Pursuit mode to Battle mode, the tank is a lot less mobile than the Batmobile. But it can use its multiple weapons to fire at multiple targets at the same time. The enemy tanks were no match for the Batmobile’s tank weapons. But there were a lot of them.

If you’re getting the sense that the Batmobile is the star of the game and not Batman himself, that might not be far from the truth.

Image Credit: WBIE