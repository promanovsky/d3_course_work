Warning: This article contains spoilers

At this point, anyone planning large-scale nuptials in the Seven Kingdoms could reasonably be accused of harboring a death wish.

In Sunday night’s game-changing “Game of Thrones” episode, we bid a not-so-fond farewell to His Royal Highness Joffrey Baratheon (Jack Gleeson), one of the most loathsome villains in TV memory.

There are so few moments of really tasty justice in this show — its motto this season may be “All men must die,” but it often feels like the good guys get the lion’s share (or the wolf’s, perhaps) of the truly awful deaths. Not this week, though. After three seasons of running rampant as a sneering, sadistic, cowardly tyrant, Joffrey got a taste of his own vile medicine, choked on it and died in the middle of his wedding.

It’s hard to imagine who, besides the king’s weeping mother, Cersei (Lena Headey), would feel anything but celebratory at this development, which makes pinning down a single murderer tricky. Was it Tyrion (Peter Dinklage), who handed Joffrey the fatal cup? Sansa (Sophie Turner), who handed it to Tyrion? Joffrey’s appointed fool, Ser Dontos (Tony Way), who appeared at Sansa’s side to urge her to escape with him?

Or could it be the reliably arch Olenna Tyrell (Diana Rigg), who remarked shortly beforehand, “Killing a man at a wedding — what sort of monster would do such a thing?” Or her granddaughter, newly anointed Queen Margaery (Natalie Dormer), whose facial expressions during the preceding wedding reception flitted hilariously between abject revulsion and spousal adoration? Time will tell, but this was an episode to savor, up there with Daenerys’ (Emilia Clarke) epic slave-owner takedown.

It wasn’t all about the marriage — we checked in with psycho Ramsay Snow (Iwan Rheon), who brings Reek/Theon (Alfie Allen) on a human hunting expedition, and watched Tyrion and the humbled Jamie (Nikolaj Coster-Waldau) forge a new bond as Tyrion sets his brother up to retrain as a swordfighter with Bronn (Jerome Flynn).

Tyrion also finally manages to be heartless enough to convince his girlfriend Shae (Sibel Kekilli) to leave Westeros, though this is one of the episode’s least believable plotlines.

But it really was a lovely wedding — if by lovely you mean “a parade of indignities rained down on guests by the groom.” Its shocking conclusion couldn’t have happened to a more deserving subject. For once.