The Dow Jones Industrial Average (DJINDICES:^DJI) was trading 0.63% higher as of 2 p.m. EDT. You're watching the Dow set brand new all-time highs today.

But the rising tide isn't lifting all boats. AT&T (NYSE:T) was barely breaking even today, and Verizon Communications (NYSE:VZ) was trading 0.4% lower. This fine Monday, the telecoms are among the Dow's worst performers.

So what's going on? Here's a clue: Shares of Netflix (NASDAQ:NFLX) were going in the opposite direction, trading more than 4% higher. And if you need yet another data point, Comcast (NASDAQ:CMCSA) followed Verizon by dropping 0.4%.

What's good for the telecom goose is often bad for the Netflix gander, and vice versa. As of yesterday, FCC Chairman Tom Wheeler is saying that the controversial network neutrality rules he's been mulling over might not be such a great idea after all.

If he follows through on that thought, it's good news for Netflix's business costs and operating principles. It's also bad for the short-term profit margins and long-term viability of some newfangled service provider business models. Comcast, AT&T, and Verizon have all been casting bedroom eyes at Wheeler's neutrality proposal, which would have allowed them to double-dip into broadband revenue streams.

So now that the Wheeler plan teeters on the brink of extinction, the telecoms and cable guys may miss out on a huge margin booster for their Internet service operations. Keep in mind that broadband subscriptions tend to grow revenue faster than all other segments, and you'll see why this is terrible news for AT&T and Verizon investors.

At the other end of the equation, Netflix may not save a whole lot of money -- networking costs are not a major expense next to towering content license payments -- but the Wheeler plan would give service providers all kinds of leverage in future discussions. Moreover, Verizon and Comcast have their own video projects and hope to compete head-to-head with Netflix. That arm-twisting could get out of hand very quickly.

Wheeler's change of heart didn't come out of the blue, of course. His first network neutrality draft raised a firestorm of criticism. Over 100 Internet businesses, large and small, signed an open letter in support of a free and open Internet.

"This Commission should take the necessary steps to ensure that the Internet remains an open platform for speech and commerce so that America continues to lead the world in technology markets," the letter said. Netflix was among the lead signers, in a list that reads like a who's who of online businesses. A handful of Tier 1 Internet backbone operators added their John Hancocks, but I can't find any consumer-facing cable companies or telecoms among the signers.

Separately, 50 venture capital firms put together their own signature list on a similar letter. This one included heavyweight firms including Andreessen Horowitz, Greylock Partners, and Y Combinator.

Wheeler's reaction shows the power of public pressure. When established corporations come together with the angel investors behind tomorrow's upstarts, all singing the same consumer-friendly tune, it becomes very difficult to stay the course. Even if you're the chairman of the FCC.

Long story short, that's why the telecoms aren't joining their Dow peers in a market rally today. The regulatory climate just changed, and not in AT&T's or Verizon's favor.