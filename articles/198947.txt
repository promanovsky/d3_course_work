Lee Marshall, the voice of Tony the Tiger, has died.

Tony the Tiger is not a celebrity per se, but most American kids grew up not only eating Frosted Flakes — the cereal that bears the cartoon tiger’s iconic likeness — they also were very familiar with the character’s distinctive voice and catchphrase of “they’re greeeeeeat!”

CBS in Detroit reports that Lee Marshall, the voice of Tony the Tiger, had big shoes to fill when he took on the role:

“Michigan-based Kellogg is looking for a new voice of Tony the Tiger, following the death of former ‘Big 8’ news anchor Lee Marshall. Marshall had been the voice of the Frosted Flakes mascot for nearly 10 years, following in the footsteps of the legendary Thurl Ravenscroft, who was the voice of Tony the Tiger for decades until his death in 2005.”

Ad rep John Sheehy, who works on Kellogg’s, said that Tony the Tiger is a hard role to cast for voice talent. Sheehy explained:

“There’s a lot of voiceover talent, but there’s that certain timber that comes with the ‘They’re GR-R-REAT’ and that’s hard to capture…It’s really looking for that tone of voice and keeping that consistency with the voice. Certainly, there will be minor differences.”

One commenter on the site had a novel suggestion — voicing Tony the Tiger using Ted Williams, the homeless man discovered on a the street a few years back, who became famous due to a “golden voice.” The comment poster said:

“TED WILLIAMS YOU TUBE “GOLDEN VOICE”!, LOOK HIM UP! CA you say GRRRRREAT! I CAN HEAR HIM NOW!!”

A spokesperson for Kellogg’s commented on Marshall’s death, telling the Huffington Post:

“We are saddened by the loss of Lee Marshall. His talent and warmth helped bring Tony the Tiger to life and will always be fondly remembered.”

Marshall, who was also known as a wrestling announcer, left behind a body of distinctive work as Tony’s voice:

Marshall was 64 when he passed away on April 26 in a Santa Monica hospital, due to esophageal cancer.

Over on Twitter, Tony the Tiger’s official account has been silent on the passing of Marshall:

Snap, Crackle and Pop are trying to get me to join their #CrossFit gym. #ShouldCatsCrossFit — Tony the Tiger (@realtonytiger) April 28, 2014

According to news sources, Tony the Tiger voice actor Lee Marshall began voicing the character back in 1999, before taking over completely in 2005 after Ravenscroft’s passing. Following the actor’s death, Kellogg’s plans to search for a successor to take over Tony the Tiger’s duties.