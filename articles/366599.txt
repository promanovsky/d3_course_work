Get the latest news from across Ireland straight to your inbox every single day Sign up! Thank you for subscribing We have more newsletters Show me See our privacy notice Invalid Email

Video Loading Video Unavailable Click to play Tap to play The video will auto-play soon 8 Cancel Play now

This terrifying video shows the moment two riders became the first people to take on the world's tallest water slide.

The Verruckt in Kansas City is taller than the world-famous Niagara Falls with users shooting down at speeds of 65mph.

Jeff Henry, who developed the ride, and John Schooley, the ride engineer, decided to be the first people to take on the ride.

They did it knowing that when engineers first tested the slide the raft fell off.

The giant slide is 17 stories high and is based at the Schlitterbahn waterpark in Kansas. Its name means 'insane' in German, ITV reported.

The ride is 2.38metres higher than Kilimanjaro in Brazil - which is the previous tallest slide - and is also taller than the Statue of Liberty.

Video from the Schlitterbarn waterpark's YouTube channel.