Amazon Fire Phone hands-on

There are two sides to Amazon’s new Fire Phone. On the one hand is the technology, Amazon experimenting with face tracking and optical recognition. On the other is the price and AT&T exclusivity; many believed Amazon would try to shake up the phone industry with its pricing, not match it. I’ve spent some time with the Fire Phone today: read on for some first impressions.

With its sober black casing and minimal logos – no splashy AT&T badge, despite its sole-carrier status – the family resemblance to the Kindle Fire HDX tablets is clear. The Amazon logo on the back, under the sheet of Gorilla Glass (a second protects the touchscreen), is a little bigger than I’d have liked, but overall it’s fairly discrete.

It’s also solid, though doesn’t feel especially premium in the hand. Think along the lines of a Nexus 4 rather than, say, an HTC One M8 or iPhone 5s. The rubberized outer edges are at least grippy and the whole thing is creak-free, aided by the fact that the battery is non-user-removable.

The focus, instead of hardware, is on software and store. Most obvious is Dynamic Perspective, which uses the four front-facing low-power cameras to spot what you’re looking at and shift the interface around to suit.

It sounds like a gimmick, but it actually works very well: assuming a 3D-style UI is what you actually want. Amazon’s mapping app is a good example, the Nokia HERE-powered 3D view of cities allowing you to look around the edges simply by moving your head. Tilting the phone calls up extra metadata, like Yelp reviews. There’s none of the sense of the screen “jumping out” at you, like HTC tried on the EVO 3D, but then again that wasn’t something I was ever particularly impressed by. It’s worth noting that you can turn it off, too, if you decide you don’t like it.

Movement also makes an appearance in UI navigation, with tilting the Fire Phone panning through carousel lists, menus, and other parts of the interface. Again, it all works as Amazon says it will, but there’s no conclusive use-case for why it’s essential, or indeed better than the current system of swipes and taps. The saving grace might be for single-handed use, but it’ll take some familiarizing to make it as predictable as a touchscreen.

As for Firefly, I’d argue that’s the most on-message part of the Fire Phone. Effectively the Amazon Dash barcode scanner, only turbocharged, it’s clear that the retailer is hoping it will encourage Fire Phone users to buy products from it rather than elsewhere.

If you’re considering the Fire Phone in the first place, though, that assumption may well already be a given. More important, then, is how Firefly actually performs.

The theory is that Amazon’s cloud-processing system can recognize one of 100,000,000 objects or items using only the Fire Phone’s rear camera. Amazon is so confident about it, there’s even a dedicated Firefly button on the side of the handset; hit that, and Firefly swiftly springs into action, ready to snap whatever you put in front of it.

I was limited in what I could test by Amazon’s demo – exactly how flexible Firefly is will have to wait until review devices are ready – but initial impressions are positive. Spotting books, DVDs, food packaging, and other everyday objects (i.e. the sort of things that fill Amazon’s virtual shelves) works consistently well: you get product information, along with relevant links to purchasing options.

So, on a novel for instance, you might get options to buy the Kindle ebook, the paperback, or the Audible audiobook. It’s also possible to share the result, and Firefly keeps a list of recent searches.

I was more intrigued by how Firefly treats less obvious scans: phone numbers and email addresses on business cards or adverts can be opened in the dialer or email app, while the Fire Phone can figure out what you’re watching across 240,000+ movies and TV episodes, along with 160 live TV channels, and pull in IMDb information through X-Ray.

If third-party apps add support using the Firefly SDK, they can even edge in on Amazon’s turf. For instance, using Firefly on a song will pull up the option to buy it as an MP3 from Amazon; however, you can also use it to trigger a playlist from iHeartRadio. It remains to be seen whether Amazon will be so open to services that more closely overlap with its core business, however.

For all the time Amazon has spent developing the headline technology, some of the company’s hardware decisions are head-scratchers. The choice of a Snapdragon 800 rather than the newer 801 or 805 Qualcomm chips is probably down to Amazon’s existing experience with it on the Kindle Fire HDX tablets, though I’d still prefer to see something newer.

I can get behind the 4.7-inch display – not every phone has to be phablet-scale – but the 720p resolution is disappointing on paper at least when most flagships arrive with 1080p. In practice, however, the shortfall in pixel density is not really noticeable, and it’s a bright panel, with Amazon claiming 590 cd/m2. That should in theory pay dividends outdoors; Amazon wouldn’t let me take it outside to actually put that to the test in overcast Seattle.

More frustrating is the fact that the Fire Phone won’t support Bluetooth LE at launch, which means the low-power modes of most current wearables and fitness trackers wont’ be supported. Amazon tells me that it’s software not hardware limiting the handset to Bluetooth 3.0, and is planning a firmware update to switch that to Bluetooth 4.0 in due course, but wouldn’t commit to a timeline for that.

Meanwhile, Amazon has opted for a mere 2,400 mAh battery, which it claims is good for up to 22 hours of talk time, up to 11 hours of video playback, or 65 hours of audio playback. It seems on the small side to me, and it remains to be seen how the Dynamic Perspective camera quartet – even if low-power – will put a dampener on runtimes.

The question of pricing is also going to take longer to figure out. Amazon is always loath to announce actual numbers when it comes to sales, but there’s going to be above average interest in just how many people are actually convinced by the Fire Phone.

My hope had been that Amazon would try something different with price. That could’ve been as straightforward as following the existing Kindle strategy, and relying on content purchases made over the lifetime of the device to offset a lower upfront cost.

Alternatively, there were more outlandish – and potentially more ground-breaking – options Amazon could’ve picked, like giving away the Fire Phone “free” with each Prime subscription. In actual fact, the reverse is true; you sign up for the phone and, for a limited time at least, get a year’s worth of Prime bundled in. Some predicted a revolutionary data bundle, to accommodate all that Prime streaming.

At $199.99 on-contract for the entry level Fire Phone, though, Amazon’s approach seems disappointingly pedestrian. As a challenger, it would’ve made more sense to undercut existing rivals like Apple and Samsung, or at least make a clear effort to differentiate from them. It’s not clear to me whether that’s Amazon’s fault or AT&T’s, but I’d have thought the carrier would’ve learned from the appalling failure of the HTC First “Facebook phone”.

My concern for Amazon is that the features that make the Fire Phone intriguing – the Dynamic Perspective UI, and Firefly – rely on being physically demonstrated in order to be most engaging. The best way of doing that would’ve been a spread across as many carriers as possible. Not limiting the handset to just one. Amazon’s VP of software told me he thinks great products will naturally sell, but I’m not entirely convinced that’s always something you can rely on.

Jeff Bezos described Amazon’s aim as having been designing a phone for Amazon customers, not necessarily for everybody. On that front, the tight store and Appstore integration, not to mention the links to Prime multimedia and Cloud Drive storage, should certainly ring familiar. Problem is, Amazon customers are also used to getting a bargain, and I’d question whether that’s something the Fire Phone succeeds at.

Amazon Fire Firefly demo

Amazon Fire Phone Mayday service demo