The U.S. Food and Drug Administration has approved a first-of-a-kind robotic exoskeleton that can help some paralyzed people walk again, known as the ReWalk system. And paralyzed patients who've tested it out say they are thrilled by the latest development.

"I'm just so excited; I wish I had it on. I could just jump up and down," Robert Woo told CBS News today. "It's a great leap forward for a lot of us who are confined to wheelchairs."

Woo, who lost his ability to walk when seven tons of steel fell about thirty stories onto his construction trailer, was part of a clinical trial last year to test out the ReWalk and another exoskeleton device known as the Ekso (Ekso Bionics).

The ReWalk was developed for people paralyzed from the waist down due to certain spinal cord injuries. It allows them to stand upright and walk with assistance from a caretaker.

The device consists of leg braces with motion sensors and motorized joints that respond to subtle changes in upper-body movement and shifts in balance. A harness around the patient's waist and shoulders keeps the suit in place, and a backpack holds the computer and rechargeable battery. Crutches are used for stability.

Robert Woo (L) walks using the ReWalk with senior biomedical engineer Pierre Asselin (RT) trailing at Mount Sinai Hospital. CBS

Though Woo only had the opportunity to use the device for a few short months, he said the ReWalk vastly improved his quality of life and allowed him to be less sedentary, a problem people with paralysis face.

"I can't want to take my family out for a walk in the park and do things that I couldn't do being in a wheelchair," he said.

Errol Samuels, a former athlete, also took part in the same clinical trial, conducted at Mount Sinai Medical Center in New York and the James J. Peters VA Medical Center in the Bronx. At the time, the Rewalk was only approved by the FDA for use in rehabilitation facilities, and not yet for personal use at home.

He trained on both the ReWalk and the Ekso.

Errol Samuels walks with the Ekso with therapist Shantel Firpi supporting him. CBS

Samuels said the news of the FDA's decision has taken him by surprise. "It's like a breath of fresh air. I wasn't expecting it to happen this soon," he told CBS News today. "I was expecting it later on, in December."

While trying out the device in January at Mount Sinai, Samuels told CBS News that he hoped one day to run again.

He said today he believes the ReWalk model that's just been approved is simply the first generation of the technology, which will only improve over time. "I'm sure they will have something more advanced for quadriplegics and triplegics," he said.

But even in the flurry of excitement both men know it will be challenging to have the opportunity to bring one home, since the ReWalk currently sells for close to $70,000 and may be much more expensive now that its manufacturer, the Israel-based Argo Medical Technologies, has the green light to market it to patients and doctors.

Woo is already making elaborate plans to finance the device, which includes selling some stock and investments, asking family members for help and contacting his health insurance provider and the medical team that ran the clinical trial. He also plans to talk with executives at Argo.

"I'd like to propose to the company that maybe if they give me a better price for it they can track the progress of me using it at home," he said.

The founder of Argo Medical Technologies developed the ReWalk after he was paralyzed in a 1997 car crash.