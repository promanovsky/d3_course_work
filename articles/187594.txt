Cyrus ‘keeps it clean’ on tour

Share this article: Share Tweet Share Share Share Email Share

London - Miley Cyrus doesn’t drink or smoke on tour. The Wrecking Ball singer resumed her Bangerz world tour in London on May 6 after having several weeks off due to a serious allergic reaction to an antibiotic and claims she is back on top form for her fans, even shunning alcohol, cigarettes and drugs when she’s performing. According to multiple sources, she said during a press conference: “I'm probably the only one on this tour who doesn't drink or smoke before a show, as I take this really seriously. It's almost like being an athlete being up here, because if someone was f**ked up, they definitely couldn't do my show.” Miley feels “better than ever” following her hospital stay and threatened to explode like a shooting star when she took to the stage. She enthused: “I'm feeling good, I'm alive – so that's f**king awesome. I'm the poster child for good health. I've been laying down for three weeks. I'm gonna go off – I'm like a star waiting to explode.”

The 21-year-old singer, who urged members of the audience of the same sex to kiss each other during her controversial, twerk-heavy set at London’s O2 Arena, revealed she was on a natural high of vitamins these days.

She said: “I'm on this crazy vitamin rush and crazy honey and lavender. I learned a lot about taking care of yourself [while I was in hospital].”

Miley also slammed hurtful rumours she had postponed a number of her US shows and the first two dates of the European leg of her tour because of a drug overdose.

She lamented: “There is nothing I would rather not do than lay in a bed for two weeks. It was the most miserable two weeks of my life.” - Bang Showbiz