Earlier this month, New York's attorney general announced that every cop who patrols the state's transit system will carry a life-saving drug to give people who are overdosing on heroin. Now, New York City's police force is following suit

The state attorney general's office will provide about $1.1 million in funding to equip 19,500 NYPD officers with the drug and train them on how to use it.

That drug, naloxone, is sprayed into the nose or injected into the body to counteract the effects of an opioid overdose and allow a person to breathe normally.

When people take heroin, it converts into morphine in the brain, where it binds to opioid receptors. When too many opioids attach to receptors on the brain stem, which controls critical automatic processes like respiration and blood pressure, "people just slowly stop breathing," as the physician Robert Hoffman has written in The New York Times.

To counteract an overdose, naloxone pushes opioids away from the brain's opioid receptors long enough to restore normal breathing for the victim.

There are few, if any, drawbacks to naloxone. If it turns out opioids are not in a person's system, naloxone has no effect on the body. It is non-addictive, and civilians with little training can administer it if emergency workers or police aren't available.

Once distributed to drug users, their families, healthcare workers, and others, the drug is used very effectively. The Centers for Disease Control and Prevention surveyed 48 U.S. programs that made naloxone available to 53,000 people who could administer it between 1996 and 2010, and it found that there were 10,171 overdose reversals.

"Providing opioid overdose education and naloxone to persons who use drugs and to persons who might be present at an opioid overdose can help reduce opioid overdose mortality, a rapidly growing public health concern," the CDC said in that 2012 report.

In 2010, the police department in Quincy, Massachusetts, became the first in the country that requires officers to carry naloxone. As of this February, the police department has administered naloxone 221 times and reversed 211 overdoses, a 95% success rate, according to New York's attorney general.

The use of naloxone is rising as more people realize its benefits.

Last month, Massachusetts Governor Deval Patrick declared opioid use in his state a public health emergency and ordered police to use Narcan, a brand of the naloxone medication. A police officer and EMT there said he's seen overdose victims who have turned blue from not breathing "get up and walk out to the ambulance," moments after naloxone is administered, substance.com reported.

The federal government has also begun championing the overdose-reversal medication. In March, U.S. Attorney General Eric Holder called for more law enforcement agencies to supply their members with naloxone.