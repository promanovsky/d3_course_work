INTEREST rates, already at historic lows, could fall further next month.

Economists are predicting there could be a new cut in European Central Bank rates in early June following broad hints from the head of the European Central Bank.

That would be good for the 375,000 people on a tracker mortgage but probably hurt those on variable mortgages and anybody with savings. Irish borrowers with variable rate mortgages have seen the cost of borrowing rise to subsidise loss-making tracker mortgages.

Hints of a new cut came as the ECB left its key interest rate steady at the historic low of 0.25pc despite very low inflation which threatens Europe's economy. That has pushed up the value of the euro, making Irish exports more expensive in major markets such as Britain and the US.

In Brussels yesterday, ECB president Mario Draghi admitted yesterday that the euro's strength was "a serious concern" and said the exchange rate would have to be addressed.

Irish Independent