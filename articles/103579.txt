Andrew Garfield was stuck with a transportation trauma April 10, attempting to get to Leicester Square for the world bow of “The Amazing Spider-Man 2.”

As helmer Marc Webb explained, “(London’s) buildings aren’t quite tall enough and not densely packed either,” how’s a swinging superhero to get around?

PHOTOS: Andrew Garfield, Emma Stone in London for ‘The Amazing Spider-Man 2″ World Premiere

Garfield did as any self-respecting Londoner would do — he caught the bus. A red double decker, delivering him directly onto the crimson carpet, with friends from a Brixton charity called Kids’ City.

Producer Matt Tolmach divulged a desire to shoot Spidey’s sequel “Sinister Six” in London. “It’s a dream we think might happen,” he said. “Put it this way, I was looking around today for neighborhoods!”

With the “Sinister Six” the hot topic, pic’s villain Jamie Foxx was swayed by an alternative team title. “The Revengers!” he exclaimed, “I like it!”

Garfield, Emma Stone, Dane DeHaan and producer Avi Arad stepped inside the Odeon for the screening before jumping on the Spidey bus express, destination Paris, for the next spindly promo leg.