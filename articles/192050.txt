Tesla beats Q1 EV goals: Ramps for 2015’s Model X

Tesla delivered 6,457 Model S EVs in its first 2014 quarter, and produced 7,535, as the company ramps up to deliver right-hand drive cars from June and continues working on the delayed Model X SUV. Elon Musk’s car firm saw a non-GAAP income of $17m (GAAP loss of $50m) and expects to deliver more than 35,000 vehicles in 2014.

Spending on R&D increased 17-percent compared to the previous quarter, which Tesla says is down to Model X development as well as modifications required for broader international sales of the Model S. Selling powertrains to Toyota for cars like the RAV4 EV brought in $15m.

Telsa warns that Model S production is still hamstrung by battery supplies, with the company now making almost 700 cars per week. That’s expected to increase to 1,000 per week by the end of the year.

Production in the coming Q2 2014 is planned to reach as many as 9,000 cars in the three month period. Leasing of the Model S will only amount to around 200 cars at the same time, because of the lingering delays between ordering and final production.

Tesla says it will be “marginally profitable” on a non-GAPP basis in Q2. It will also spend up to $850m in financial year 2014 across areas like battery production, the Supercharger network, servicing, and boosting production.

As for the Gigafactory – Tesla’s expensive new battery production plant – Tesla says it’s still on track to begin producing cells and packs in 2017. “We have not yet finalized the ultimate location for the Gigafactory,” the company admits, “and we are going to start work on at least two locations in parallel in order to minimize risk of delays arising after groundbreaking.”

SOURCE Tesla [pdf]