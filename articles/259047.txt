Residents of Toronto enjoyed one of the warmest days of the year so far, as temperatures hovered near the 30 C mark on Monday.

The warm weather peaked during the afternoon, as the mercury hit 29 C.

Typical temperatures for this time of year are between 9 C and 21 C. The record temperature for this day is 31.7 C, recorded in 2010.

Westerly winds of 30 kilometres per hour will provide some relief from the heat, but the UV index is 8, which is considered very high.

Environment Canada is advising people limit their exposure to the sun, especially between the hours of 11 a.m. and 4 p.m.

During those hours, the weather agency recommends people cover up and use sunscreen. Sunscreen should provide UVA and UVB protection, and have an SPF of at least 15.

Things will cool down by Monday evening, with an overnight low of 18 C, and 30 per cent chance of showers overnight.

But the temperature is predicted to heat up again on Tuesday, with a forecasted high of 28 C and a 40 per cent chance of showers later in the day.

On Wednesday, Toronto will return to average seasonal temperatures with a predicted high of 20 C.