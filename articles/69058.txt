1. Noah * $44,000,000 Total: $44,000,000 LW: N

THTRS: 3,567 2. Divergent $26,500,000 Total: $95,260,000 LW: 1

THTRS: 3,936 3. Muppets Most Wanted $11,373,000 Total: $33,210,000 LW: 2

THTRS: 3,194 4. Mr. Peabody & Sherman $9,500,000 Total: $94,909,000 LW: 3

THTRS: 3,299 5. God's Not Dead $9,075,000 Total: $22,028,000 LW: 4

THTRS: 1,178 6. The Grand Budapest Hotel $8,825,000 Total: $24,457,000 LW: 7

THTRS: 977 7. Sabotage (2014) * $5,330,000 Total: $5,330,000 LW: N

THTRS: 2,486 8. Need for Speed $4,335,000 Total: $37,753,000 LW: 6

THTRS: 2,705 9. 300: Rise of An Empire $4,300,000 Total: $101,145,000 LW: 5

THTRS: 2,601 10. Non-Stop $4,087,000 Total: $85,167,000 LW: 8

THTRS: 2,515

Audiences didn't exactly flood into theaters to see Darren Aronofsky's latest offering,, but the much discussed Biblical adaptation made a solid opening, snagging a clear number one with $44 million. That's by far the largest opening for an Aronofsky film, butalso represents his biggest budget to date.At $125 million plus marketing expenses (and just $55 million in foreign ticket sales so far) the movie isn't set to be wildly profitable, but if it holds strong the next couple of weeks it should give Aronofsky a little more credit with studios who till now have had to consider his edgy and unique storytelling style almost a liability.Arnold Schwarzenegger's latest vehiclestumbled in at number seven with just $5 million. That's the third movie in a row for the aging action star to open under $10 million, something that rarely occurred in his pre-governor career. Are people done going to the movies to see Arnold in action roles? Maybe it's time for him to start looking at scripts that need a lovable but intimidating grandfather with a thick accent.The biopicopened in 650+ venues this weekend, and while it made more per screen thanits $3 million total was only enough to warrant twelfth place.For the full weekend top ten, check out the chart below: