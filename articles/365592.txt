Sir Paul McCartney helped a man propose at his concert.

The 72-year-old rocker returned to the stage on Saturday night (05.07.14) after cancelling part of his 'Out There' tour in May due to a viral infection but he was more than happy to share the spotlight with 64 year old John Dann, who popped the question to his girlfriend at the gig in Albany, New York.

According to local newspaper The Democrat & Chronicle, Paul invited John onstage and allowed him to sing part of The Beatles' 1967 song 'When I'm Sixty-Four' in front of the 13,000-strong crowd. The fan then proposed to his girlfriend Claudia Rodgers, who happily accepted.

It seems the couple caught Paul's attention with their humorous signs as they were both spotted holding up pieces of card reading, ''He won't marry me 'til he meets you'' and ''I have the ring and I'm 64''.

Following the proposal, Paul quipped the couple had ''booked us for the wedding'' too.

The rocker's performance on Saturday marked his first in nearly two months after he was hospitalised for six nights in Tokyo, Japan, in May.