Pregnancy is not without health risks, and now researchers from Canada have identified a new one: serious car crashes.

During the second trimester of pregnancy, a woman’s odds of being behind the wheel in a multi-vehicle accident that was bad enough to send her to a hospital emergency room were 42% greater than they were in the three years before she became pregnant, according to a study published Monday in CMAJ, the Canadian Medical Assn. Journal. However, by the third trimester, the risk was significantly lower than it was before pregnancy, and it fell even further in the first year after the women gave birth.

“It amounts to about a 1 in 50 statistical risk of the average women having a motor vehicle crash at some point during her pregnancy,” said Dr. Donald Redelmeier of the University of Toronto, who led the study.

The research team, from the University of Toronto and affiliated institutions, wondered whether the fatigue, distraction, nausea and other annoyances that accompany pregnancy might make women more vulnerable behind the wheel. They were particularly curious about the second trimester, a time when pregnant women often feel like their normal selves. As a result, they don’t change their behavior to account for the significant physiological changes happening in their bodies.

Advertisement

The researchers identified more than 500,000 women who gave birth in Ontario during a five-year period from 2006 to 2011. They combed through data from Ontario hospitals to see how often they got into serious car crashes during the three years before they became pregnant; during each trimester of their pregnancy; and for the first year after their babies were born.

The women in the study were not paragons of perfect driving during the baseline period. The researchers counted up a total of 6,922 crashes, which worked out to about 4.55 crashes per 1,000 women per year. That was more than double the population-wide average of roughly 2 crashes per 1,000 people per year. However, the researchers noted that the women in the study were relatively young, which helps explain their higher crash rate.

During the first month of the first trimester of pregnancy, the crash rate fell slightly to 4.33 crashes per 1,000 women, the researchers found. However, that difference was too small for the drop to be considered statistically significant.

But something had definitely changed by the first month of the second trimester. During that month, the women’s crash rate soared to 7.66 collisions per 1,000 women per year, according to the study. That was the most dangerous month for pregnant women behind the wheel. For the entire second trimester, the crash rate was 6.47 collisions per 1,000 women per year, which was 42% higher than during the baseline period.

Advertisement

Women from all walks of life became more vulnerable during this period, the researchers found. The increased crash risk was seen in all women regardless of age, socioeconomic status, whether their babies were born early, the gender of their babies and most other factors. Time of day, week and year also had no effect. The only characteristic that seemed to influence the crash rate in the second trimester was whether the women lived in urban or rural areas (it was higher for city drivers).

The safest month for all women turned out to be the last month of pregnancy. In those final weeks, the women tracked in the study had only 2.74 crashes per 1,000 women per year. And in the year after giving birth, the accident rate dropped even more, to 2.35 crashes per 1,000 women per year, the researchers reported.

The women saw no increase in car accident injuries when they were passengers in other people’s cars, or as pedestrians, according to the study.

Health experts have previously identified auto accidents as the leading cause of fetal death related to a trauma. However, the study did not include data on crashes that caused injuries serious enough to kill a fetus because women in those cases didn’t go on to give birth. As a result, the statistics probably underestimate the true risk of driving while pregnant, the study authors wrote.

Advertisement

“The message here is not to stop driving,” Redelmeier said in a video released by the Sunnybrook Research Institute, which is affiliated with the University of Toronto and the Sunnybrook Health Sciences Centre (where Redelmeier is a staff physician). “The message is to start driving more carefully.”

He added that the pregnant women he cares for ask about the risks of flying planes, soaking in hot tubs and skating on Rollerblades. However, he said, “not once has a woman asked me about road safety despite it being the much larger, greater risk.”

Redelmeier’s previous research on automobile accidents has found that American drivers are more likely to crash on days when presidential elections are held.