Urgent:

Do You Approve Or Disapprove of President Obama's Job Performance? Vote Now in Urgent Poll

When will people start caring?? "@cnnbrk: For the first time, U.S. surgeon general issues skin cancer warning. http://t.co/DG7udQhpTj” — Kelly Decker (@KellyDecker23) July 29, 2014

.@Surgeon_General today released Call to Action to Prevent #SkinCancer. Never a better time for lawmakers to act. http://t.co/3ZKiuFCN8q — Shane Ferraro ACS (@sferraroACS) July 29, 2014

For 1st time, U.S. surgeon general issues skin cancer warning. http://t.co/9M2XNK2zMf << how is this news? Is this tweet from 1914 or 2014? — Andy Frankenberger (@AMFrankenberger) July 29, 2014

Urgent:

Assess Your Heart Attack Risk in Minutes. Click Here.

Acting Surgeon General Dr. Boris Lushniak said Tuesday that skin cancer is a “major public health problem that requires immediate action.”Incidences of skin cancer are on the rise, with about 5 million people treated for it each year."We have to change the social norms about tanning," Lushniak, a dermatologist, said, according to CNN . "Tanned skin is damaged skin, and we need to shatter the myth that tanned skin is a sign of health."Cases of melanoma, the most deadly form of skin cancer, have almost tripled during the past 30 years, with about 63,000 new cases annually and about 9,000 annual deaths.Lushniak encourages people to wear hats, sunglasses, sunscreen and protective clothing while outside and to seek shade. He calls on communities to put up canopies and other shady areas and supports stronger regulation of indoor tanning.The U.S. Department of Health and Human Services has goals to reduce skin cancer by such measures as providing shade in public places and reducing indoor tanning. The Centers for Disease Control and Prevention advocates for the use of sunscreen with an SPF of at least 15 along with wearing protective clothing.Two months ago, the Food and Drug Administration said it will require tanning beds and lamps to include labels warning against use by anyone younger than 18.News of the surgeon general’s report spread on Twitter.