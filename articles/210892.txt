IT’S not a bird or a plane, but this vehicle will star in the coming ‘Superman vs Batman’ movie due for release in 2016. Tweeted overnight by film director Zach Snyder, the Batmobile is teased as a caped road crusader, with a cover only partially revealing what we’ll see on screen.

What we can already see is a hint of the armoury styling that echoes the recent Batmobiles -- a long way from the sleek designs under Tim Burton when the franchise was revived for the big screen in 1989, and even further from the 1960s TV series finned double-cockpit car designed by George Barris on a 955 Lincoln Futura show concept.

The latest vehicle was designed by Patrick Tatopoulos, who’s also handled 300: Rise of an Empire, Underworld, and Live Free or Die Hard. Snyder tweeted “Could be time to pull the tarp...Tomorrow?” which suggests that we’ll see more of the next Batmobile in the next 24 hours.

Shooting for the film is scheduled to start this month, with the movie expected to hit the screens in May 2016. The working title Batman vs Superman is likely to be replaced by the real title during filming and as promotion starts to build up. It will feature Ben Affleck as Batman and Henry Cavill as Superman.

This article was originally posted on Cars Guide.