FCC

Many of the world's top tech heavyweights have made a plea to the Federal Communications Commission to lay down the law and safeguard Net neutrality.

Google, Microsoft, Facebook, Amazon, Twitter, Yahoo, eBay, and dozens of others wrote a letter (PDF) to the FCC on Wednesday asking for a "free and open Internet" and rules that protect users and Internet companies. In all, nearly 150 Internet companies signed the letter.

The move comes as the FCC is considering a new Net neutrality proposal from FCC Chairman Tom Wheeler. Critics say the proposal could create so-called Internet that would allow Internet service providers to block access or discriminate against traffic traveling over their connections.

"According to recent news reports, the Commission intends to propose rules that would enable phone and cable Internet service providers to discriminate both technically and financially against Internet companies and to impose new tolls on them," reads the letter the tech companies sent to the FCC Wednesday. "If these reports are correct, this represents a grave threat to the Internet."

"Instead of permitting individualized bargaining and discrimination, the Commission's rules should protect users and Internet companies on both fixed and mobile platforms against blocking, discrimination, and paid prioritization, and should make the market for Internet services more transparent," the letter continues. "Such rules are essential for the future of the Internet."

Over the past few weeks, Wheeler has worked to calm critics of the proposal saying that he is all for an open Internet. A couple of weeks ago, he wrote a blog post in which he pledged to use to prevent ISPs from degrading service for the benefit of a few.

Still, it appears some people haven't been persuaded -- including two of Wheeler's fellow commissioners. Democratic Commissioners Jessica Rosenworcel and Mignon Clyburn both issued statements on Wednesday questioning Wheeler's proposal.

Clyburn wrote a blog post that called for a free and open Internet, as well as prohibiting pay-for-priority arrangements, and Rosenworcel issued a statement (PDF) asking the commissioners to "delay our consideration of his rules by a least a month. I believe that rushing headlong into a rulemaking next week fails to respect the public response to his proposal."

The FCC is scheduled to vote on Wheeler's proposal on May 15; but, if Rosenworcel gets her way, that vote could be delayed.