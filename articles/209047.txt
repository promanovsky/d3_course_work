The Moto G is a low-end device by Motorola which has proven to be something of a success, thanks in part to its affordable price tag and its somewhat decent specs, making the phone a value for money purchase. We had heard the rumors that Motorola could be planning an LTE version of the device and sure enough it seems that the LTE model has surfaced.

Advertising

The device was recently spotted on Amazon where customers interested in the device could pre-order it. According to the listing, the LTE version of the Moto G is expected to begin shipping on the 30th of June where it will retail for $219.99, which we have to say is pretty affordable. However we should note that LTE phones typically consume more battery than their 3G counterparts, so battery life on the Moto G LTE might not be as good as its 3G version.

Motorola has yet to make an official announcement regarding the device, but given that they are expected to announce new phones on the 13th of May, which is tomorrow, it seems that someone at Amazon pulled the trigger a little too early. Apart from the LTE version of the Moto G, the event is also expected to unveil other Motorola devices, like the rumored Moto G Ferrari and maybe even the Moto E, which some are speculating could be the successor of the Moto G, albeit with slightly lower specs. Either way do check back with us tomorrow for the details.

Filed in . Read more about Amazon, Moto G and Motorola.