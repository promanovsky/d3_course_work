Photo via USA TODAY

Those of you tanning or burning your skin this summer should stop — seek the shade, wear a hat and some sunscreen and, whatever you do, stay out of indoor tanning salons. It's a familiar skin-cancer prevention message, but it's coming from a new source: the office of the U.S. Surgeon General.

USA TODAY reports the call to action from acting Surgeon General Boris Lushniak, released Tuesday, says that skin cancer is a "major public health problem" and that too much exposure to indoor and outdoor ultraviolet light is a major cause. It comes just two months after the Food and Drug Administration announced it will soon require labels on tanning beds and lamps warning against use by anyone younger than 18.

But more action is needed, because skin cancers in the United States, unlike many other cancers, continue to rise, the new report says. Nearly 5 million people in the United States are treated for skin cancer each year, at a cost of $8.1 billion, the report says. About 63,000 cases are the most serious kind, melanoma, and about 6,000 of those cases are directly linked to indoor tanning, the report says.