Workers from Unicef and partners speak with families about how they can best protect themselves from the Ebola virus disease, at the Marche Niger, a market in Conakry, Guinea, courtesy of this UNICEF handout photograph taken March 31, 2014. — Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

LONDON, July 31 — For scientists tracking the deadly Ebola virus in West Africa, it is not about complex virology and genotyping, but about how contagious microbes — like humans — use planes, bikes and taxis to spread.

So far, authorities have taken no action to limit international travel in the region. The airlines association IATA said yesterday that the World Health Organisation is not recommending any such restrictions or frontier closures.

The risk of the virus moving to other continents is low, disease specialists say. But tracing every person who may have had contact with an infected case is vital to getting on top of the outbreak within West Africa, and doing so often means teasing out seemingly routine information about victims' lives.

In Nigeria, which had an imported case of the virus in a Liberian-American who flew to Lagos this week, authorities will have to trace all passengers and anyone else he may have crossed paths with to avoid the kind of spread other countries in the region have suffered.

The West Africa outbreak, which began in Guinea in February, has already spread to Liberia and Sierra Leone. With more than 1,300 cases and 729 deaths, it is the largest since the Ebola virus was discovered almost 40 years ago.

Sierra Leone has declared a state of public emergency to tackle the outbreak, while Liberia is closing schools and considering quarantining some communities.

“The most important thing is good surveillance of everyone who has been in contact or could have been exposed,” said David Heymann, a professor of infectious disease epidemiology and head of global health security at Britain's Royal Institute of International Affairs.

The original case

The spread of this outbreak from Guinea to Liberia in March shows how tracing even the most routine aspects of peoples' lives, relationships and reactions will be vital to containing Ebola's spread.

Epidemiologists and virus experts believe the original case in that instance to have been a woman who went to a market in Guinea and then returned, unwell, to her home village in neighbouring northern Liberia.

The woman's sister cared for her, and in doing so contracted the Ebola virus herself before her sibling died of the haemorrhagic fever it causes.

Feeling unwell and fearing a similar fate, the sister wanted to see her husband — an internal migrant worker then employed on the other side of Liberia at the Firestone rubber plantation.

She took a communal taxi via Liberia's capital Monrovia, exposing five other people to the virus who later contracted and died of the Ebola. In Monrovia, she switched to a motorcycle, riding pillion with a young man who agreed to take her to the plantation and whom health authorities were subsequently desperate to trace.

“It's an analogous situation to the man in the airplane” who flew into Lagos and died there, said Derek Gatherer of Britain's Lancaster University, an expert in viruses who has been tracking the West Africa outbreak closely.

Liberia's Ebola case count is now 329 including 156 deaths, according to latest data from the World Health Organisation — although not all are linked to the Guinea market case.

Gatherer noted that while Ebola does not spread through the air and is not considered “super infectious,” cross-border human travel can easily help it on its way. “It's one of the reasons why we get this churn of infections,” he said.

The risk of the Ebola virus making its way out of Africa into Europe, Asia or the Americas is extremely low, according to infectious disease specialists, partly due to the severity of the disease and its deadly nature.

Patients are at the most dangerous when Ebola haemorrhagic fever is in its terminal stages, inducing both internal and external bleeding, and profuse vomiting and diarrhoea — all of which contain high concentrations of infectious virus.

Anyone at this stage of the illness is close to death, and probably also too ill to travel, said Bruce Hirsch, an infectious diseases expert at North Shore University Hospital in the United States.

“It is possible, of course, for a person to think he might just be coming down with the flu, and to get onto transport and then develop more critical illness. That's one of the things we are concerned about,” he said in a telephone interview.

He added, however: “The risk (of Ebola spreading to Europe or the United States) is not zero, but it is very small.”

Heymann noted that the only case in which an Ebola case was known to have left Africa and made it to Europe via air travel was in 1994 when a Swiss zoologist became infected with the virus after dissecting a chimpanzee in Ivory Coast.

The woman was isolated in a Swiss hospital and discharged after two weeks without infecting anyone else.

“Outbreaks can be stopped with good infection control and with understanding by people who have been in contact with infected cases that they have to be responsible,” Heymann said. — Reuters