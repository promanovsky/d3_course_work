'Midnight Rider' Producer Jay Sedrish Surrenders to Georgia Police

Fellow producer Jody Savin and director Randall Miller turned themselves in to authorities on Sunday after their indictment on charges of involuntary manslaughter and criminal trespass.

Midnight Rider executive producer and production manager Jay Sedrish turned himself in to the Wayne County, Ga., police on Thursday following his July 3 grand jury indictment on charges of involuntary manslaughter and criminal trespassing related to the death of camera assistant Sarah Jones.

Sedrish was charged along with director Randall Miller and fellow producer Jody Savin in the death of Jones, who was struck and killed by a train on the set of the Gregg Allman biopic.

PHOTOS 'Midnight Rider' and Other On-Set Accidents

Sedrish was released after posting $27,700 bond, a spokesperson for the jail tells The Hollywood Reporter.

Miller and Savin surrendered to Georgia authorities on Sunday and were released as of Tuesday.

The Midnight Rider team was filming a dream sequence on railroad tracks along a trestle over the Altamaha River when Jones was killed. The crew tried to clear the tracks when a train approached, but Jones was unable to escape. Several other crewmembers were injured. It's unclear whether the crew had permission to be on the tracks.

LIST The Hollywood Reporter Reveals Hollywood's Favorite Films

Involuntary manslaughter carries a potential sentence of 10 years in prison under Georgia law. Criminal trespass is a misdemeanor and carries a potential sentence of 12 months.

Jones’ death has served as a wake-up call for the entertainment industry about on-set safety issues. Production on the film has since been suspended, with star William Hurt, who was on set when Jones was killed, pulling out of the project.

Several civil suits have recently been filed against Miller, Savin, Sedrish and various other individuals and companies associated with the film.