Click Here for More Articles on BWW TV NEWS

GLEE standout Dot-Marie Jones has taken to Twitter to share a touching and emotional remembrance of her deceased former co-star from the hit FOX musical dramedy series, Cory Monteith, who passed a way a year ago this week.

Jones shared, "AWW TODAY IS A TOUGH DAY! BUT I HOLD IN MY HEART SO MANY BEAUTIFUL & FUN MEMORIES! MISS & LOVE THIS MAN/BOY! LOVE U".

Shortly thereafter, Jones followed up with, "A YEAR..I WILL ALWAYS HOLD DEAR THE AMAZING TIME I HAD WITH CORY! AS WE MOVE ON IN LIFE I KNOW HE IS MOVING ON IN A BEAUTIFUL PLACE!"

Additionally, Jones posted a picture of the two together on set of GLEE.

Follow Dot-Marie Jones on Twitter here.

Of note, Cory Monteith passed away on July 13, 2013, after ingesting a lethal combination of alcohol and heroin and was subsequently paid tribute with a special episode of Glee titled "The Quarterback" last season.

GLEE returns for its final season in 2015 on FOX.