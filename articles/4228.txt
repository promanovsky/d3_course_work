For once, it’s parents and not youngsters who are being told to get their noses out of their smartphones. The reason? Thumbing through one’s phone incessantly could damage the parent-child bond.

A new study published in the journal Pediatrics found that adults absorbed in their mobile devices were more likely to harshly scold their children’s behavior.

Researchers from Boston Medical Center observed parents interacting with their children during meals in fast-food restaurants. They noted that one-third of the adults used their phones continuously during the meals, and 73 percent of them checked their devices at least once.

When a parent who was glued to the phone was interrupted by a child, the parent was apt to react negatively, according to the study. One mother even kicked her child under the table after the child attempted to get her attention while she tapped away at her smartphone. Another mother ignored her child as he tried to lift her head from looking down at a tablet. Researchers believed the kids may have been acting out as a way to test limits or gain the parent’s attention.

"What stood out was that in a subset of caregivers using the device almost through the entire meal, how negative their interactions could become with the kids," Dr. Jenny Radesky, a fellow in developmental and behavioral pediatrics at the center and lead author of the study, told Time. "[There were] a lot of instances where there was very little interaction, harsh interaction or negative interaction between the adults and children."

She added: "Caregivers who were highly absorbed in their devices seemed to have more negative or less engaged interactions with children.”

Before we go pointing the finger at all parents who use their smartphones during meals, let’s keep in mind that most of us have trouble putting the mobile devices away. One British survey found that 66 percent of people are afraid to be separated from their cell phones. Another study by Time noted that 84 percent of respondents said they couldn’t go a single day without their cell phones, and 20 percent said they check their phone every 10 minutes. There’s even evidence that more and more people are using smartphones during sex.

Still, researchers say their study of the effects of smartphone addiction on the parent-child dynamic is particularly important because it could have consequences for a child’s development.

"We know from decades of research that face-to-face interactions are important for cognitive, language and emotional development,” Radesky told Health Day. “Before mobile devices existed, mealtime would've been a time where we would've seen those interactions.”

Researchers say additional studies and further discussion about the use of technology around children are needed.