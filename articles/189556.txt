Researchers created a computer model that simulated how the universe formed around clumps of elusive dark matter.

This is the most extensive model of the universe's evolution that has ever been created, the BBC reported.

"Now we can get to grips with how stars and galaxies form and relate it to Dark Matter," Professor Richard Ellis of the California Institute of Technology (Caltech) in Pasadena told the BBC.

The researchers were pleasantly surprised at how well the model turned out considering it started off with dark matter.

In past models the outcome of the simulation tended to be drastically different from the way the universe actually looks. This new model came up with a simulation that is much closer to the real thing.

The model shows dark matter strewn around empty space in a formation resembling tree branches; over millions of years these clumps condense to form the "seeds" of future galaxies. Soon after this occurs, non-dark matter appears.

The model depicts a series of cataclysmic explosions where the non-dark matter is sucked in and spat out repeatedly, resulting in a regulation of stars and galaxies. Eventually the models turns into a similar form as the galaxy we see today.

"Many of the simulated galaxies agree very well with the galaxies in the real Universe. It tells us that the basic understanding of how the Universe works must be correct and complete," Doctor Mark Vogelsberger of Massachusetts Institute of Technology (MIT) told the BBC.

"If you don't include dark matter (in the simulation) it will not look like the real Universe," he said.

The European Space Agency (ESA) plans to launch a spacecraft called Euclid in 2020; its purpose will be to measure the acceleration of the universe.

"In order to use the data from Euclid we will need to simulate what we expect to see for dark energy and compare it with what we see," Doctor Joanna Dunkley, of Oxford University told the BBC.

WATCH:



@ 2018 HNGN, All rights reserved. Do not reproduce without permission.