Oscar Pistorius was not suffering from a mental disorder when he shot dead girlfriend Reeva Steenkamp, a psychiatric evaluation has found.

Four mental health experts assessed the Paralympian over six weeks at a hospital on the outskirts of Pretoria.

Reading the report in court, prosecutor Gerrie Nel said: "Mr Pistorius did not suffer from a mental illness or defect that would have rendered him criminally not responsible for the offence charged."

The panel's report stated that its authors believed Pistorius was "capable of appreciating the wrongfulness of his act".

The finding means Pistorius, 27, can now be held criminally responsible for killing Steenkamp, 29, in a hail of bullets on Valentine's Day, last year.

Judge Thokozile Masipa ordered the review in to his mental health after a defence witness claimed the fallen star had Generalized Anxiety Disorder (GAD). Nel requested the assessment.

He claims he shot dead Steenkamp because he thought she was an intruder in the bathroom of his luxury home.

The prosecution claims Steenkamp was murdered by Pistorius following an argument between them, earlier in the evening.

Today (Monday) is the first of resumed proceedings, following a six week adjournment for tests to be carried out on Pistorius.