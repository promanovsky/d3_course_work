An employee of a smartphone store shows Samsung Electronics’ Galaxy S4 (left) and Apple’s iPhone 5 in Seoul in this file photo. —Reuters pic

Subscribe to our Telegram channel for the latest updates on news you need to know.

SAN JOSE, May 4 — Apple Inc’s US$120 million (RM391 million) verdict against Samsung Electronics Co in a trial over smartphone technology, after seeking US$2 billion, may not give the mobile- device giants any more incentive to end their four-year global legal battle.

The jury in San Jose, California, after hearing almost four weeks of evidence in the companies’ second US trial, found May 2 that Samsung infringed two of four Apple patents it considered and Apple infringed one of two Samsung patents.

The trial revolved around whether Samsung, the maker of Galaxy phones, used features in Google Inc’s Android operating system that copied the iPhone maker’s technology. The verdict sets the stage for each company to seek an order banning US sales of some of its competitor’s older devices.

“They’re each looking for that knockout punch and they don’t seem to be able to find it,” said Susan Kohn Ross, a lawyer with Mitchell Silberberg & Knupp in Los Angeles who has been monitoring the cases. “They sort of feel they are on an even keel and need to keep punching.”

The world’s top two smartphone makers have spent hundreds of millions of dollars in legal fees on battles across four continents to dominate a market that was valued at US$338.2 billion last year, according to data compiled by Bloomberg.

Samsung, based in Suwan, South Korea, had 31 per cent of industry revenue, compared with 15 per cent for Cupertino, California-based Apple, whose share of the market has shrunk as the touch-screen interface has become more commonplace and Samsung, LG Electronics Inc and Lenovo Group Ltd have introduced lower-cost alternatives.

Mixed record

While Apple’s iPhone altered how consumers looked at smartphones, the company has been less successful in fending off what it considers copycats since filing its first smartphone-related patent suit, against HTC Corp, more than four years ago.

In addition to the earlier jury verdict, Apple won an import ban at the US International Trade Commission against some of Samsung’s older phone models. Samsung won an import ban on some versions of the iPhone 4 and iPad 2, only to have it vetoed by President Barack Obama’s administration.

In December 2012, Apple was able to reach an agreement in which HTC agreed not to make “cloned” copies of the iPhone. It’s had less luck with its suits against Samsung or Motorola Mobility, which Google bought in 2012.

While last week’s verdict is the eighth-largest jury award in the US this year, it was a narrow victory for Apple. Even if Apple persuades the judge to issue an injunction halting sales, most of the 10 devices it sought to ban during the trial are no longer sold by Samsung in the US.

Absurd request

Apple wanted Samsung to pay as much as US$40 for each phone sold that uses infringing technology, for a total of US$2.19 billion. Samsung argued that the jury should award no more than US$38.4 million for any infringement damages.

From the start, Samsung cast the iPhone maker’s case as a bid to displace Google as the leading supplier of smartphone operating systems and limit consumer choice.

The verdict “doesn’t slow down Samsung at all” because the implied royalty rate is so low that Samsung doesn’t need to worry about paying it, said Kevin Rivette, a 3LP Advisors LLC partner. “It was absurd for Apple to ask for US$2 billion. That was a case of ‘Am I going to laugh now or laugh later?’”

Apple might have considered the trial a win if the verdict amounted to more than US$10 per phone, Rivette said.

Older devices

The iPhone maker was awarded US$930 million in the first US trial in San Jose against Samsung two years ago while not getting what it considered more important than the money—an order barring sales of infringing Samsung phones.

“It is hard to view this outcome as much of a victory for Apple,” Brian Love, an assistant professor at the Santa Clara University School of Law, said in an e-mail. The amount of damages “probably doesn’t surpass by too much the amount Apple spent litigating this case,” he said.

The jury rejected Apple’s claim on the patent it counted as the most valuable of the group, one that enables updating of applications while other features of the phone are in use. Apple also lost its claim over a patent that allows a user to perform a “universal” search for information with a single click.

“The most important outcome of the case is the message that small, user interface component patents are not justifying gigantic damage awards,” Michael Risch, a professor at Villanova University’s law school, said in an e-mail.

Samsung’s newest smartphones, the Galaxy S4 and S5, weren’t on trial in the case. Samsung is counting on the Galaxy S5, which went on sale March 27 in South Korea, as its marquee device to maintain its global lead competing with Apple for high-end shoppers and with Chinese producers including Xiaomi Corp that target budget buyers.

It may take one of the companies gaining a technological advantage, rather than a legal one, to convince their executives and their lawyers to settle, Kohn Ross said.

“I don’t think this is going to stop,” said patent lawyer Robert Stoll of Drinker Biddle in Washington. “It’s almost like there’s a mindset against settling, and that’s embedded in their culture at this point.”

The case is Apple Inc v. Samsung Electronics Co, 12- cv-00630, US District Court, Northern District of California (San Jose). — Bloomberg