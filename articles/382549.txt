PEOPLE tend to choose friends that are genetically similar to themselves, so much so that a person’s social circle could be made up of their fourth cousins, scientists have found.

The research is based on the Framingham Heart Study in the north-eastern US state of Massachusetts, which contains both extensive genetic detail — 1.5 million markers — and information about friends and connections.

Scientists focused on 1,932 people and compared pairs of unrelated friends against pairs of unrelated strangers.

Those in the same social circles shared about one per cent of their genes, much higher than the amount they shared with nearby strangers.

Friends tended to be about as related as people who share great-great-great grandparents.

“The increase in similarity relative to strangers is at the level of fourth cousins,” said the study in the Proceedings of the National Academy of Sciences.

Even though sharing one per cent of genes may not sound like a lot, co-author Nicholas Christakis, professor of sociology, evolutionary biology and medicine at Yale University, said that “to geneticists, it is a significant number.

“We are somehow, among a myriad of possibilities, managing to select as friends the people who resemble our kin,” he said.

Most of the people in the Framingham study were white and of European descent. Researchers said they could tell from their analysis that the genetic ties among friends went above and beyond what would be expected among people of shared ancestry, or by people who tend to befriend those of the same ethnic group.

They could also tell that friends were most similar in genes affecting the sense of smell, and most different when it came to genes that affect immunity against various diseases.

Researchers do not fully understand how this happens.

But they said perhaps friends might be most easily made among those who enjoy the same smells, such as the aroma of a coffee shop.

On the other hand, the disparity in immune systems could foster survival in groups of friends by reducing the spread of dangerous pathogens.