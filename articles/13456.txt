Residents in parts of Ontario and the U.S. are being asked to help observe a rare astronomical event on Thursday, when an asteroid will pass and block one of the brightest stars in the night sky.

The International Occultation Timing Association is asking volunteers to watch and time as the asteroid 163 Erigone passes in front of the star Regulus.

According to the association, Regulus -- which is the brightest star in the constellation Leo -- may completely disappear for as long as 14 seconds as the asteroid sails in front of it.

The event, called an occultation, is rare, the IOTA says. And to take advantage of its occurrence, the association wants residents from parts of the U.S. Northeast and Ontario to help them document it.

"Astronomers have been predicting occultations of stars by asteroids for almost 40 years, and Regulus is the brightest star to ever be predicted to be occulted by a sizable asteroid anywhere in the USA," the association says on its website. "So it really is likely to be a once-in-a-lifetime experience for most who live within the path."

By observing and timing the occultation, willing participants will be helping to build knowledge about the asteroid, the IOTA says.

The asteroid, which is estimated to be about 70 kilometres wide, is expected to pass Regulus early Thursday morning, around 2:06 a.m. ET.

Residents in parts of Bermuda, New York State, New Jersey, Connecticut and Ontario should be able to see the event with their naked eye, as the asteroid's shadow reaches Earth, the association says.

The asteroid's shadow will first touchdown in Bermuda, before racing northwest across the Atlantic Ocean. In Ontario, the shadow is expected to travel over the Kingston region, before heading up to North Bay.

Residents interested in viewing the occultation are being asked to observe and time the event, and report some basic observations to the IOTA through its public reporting page.

Tools to help observe the occultation

To help residents best observe and time the occultation, the IOTA has released a map showing the expected flight path of the asteroid's shadow.

It has also released some tips to help residents locate the Regulus star (and to make sure they're looking at the right area of the sky).

There are also guidelines on how viewers can best time the event by either using a counting method, a stopwatch or by recording the occultation with a video camera.

And a special occultation timing app for iPhones and iPads has been created ahead of Thursday's event.

Viewers are encouraged to submit any observations, with the IOTA stressing that even submitting an observation of "I did not see the star disappear from my location" would be adding to its data.

Anyone who submits their observations can receive the results by email once they've been compiled and analyzed by the IOTA.