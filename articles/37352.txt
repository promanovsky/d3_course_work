Why dark chocolate really IS good for you: Stomach microbes turn cocoa into a natural drug that reduces blood pressure

Previous studies found dark chocolate reduces blood pressure



Now scientists have discovered this is due to how our guts ferment cocoa

By breaking down chocolate compounds, microbes produce molecules that act like a natural anti-inflammatory

This 'drug' enters the bloodstream and helps protect arteries from damage

Dark chocolate contains a higher cocoa content than milk chocolate

Dark chocolate, pictured, can reduce blood pressure because gut microbes ferment fibres in cocoa and produce a natural anti-inflammatory

Love dark chocolate?

Now you can eat it with much less guilt because scientists have discovered why it is so good for us.



Previous studies have found daily consumption of dark chocolate reduces blood pressure and is good for the heart.



Now scientists have discovered why this happens - and its down to how our guts ferment the fibre in cocoa beans.



Researcher Maria Moore, from Louisiana State University said: 'We found that there are two kinds of microbes in the gut: the 'good' ones and the 'bad' ones.

'The good microbes, such as Bifidobacterium and lactic acid bacteria, feast on chocolate.

'When you eat dark chocolate, they grow and ferment it, producing compounds that are anti-inflammatory.'

This naturally forming anti-inflammatory enters the bloodstream and helps protest the heart and arteries from damage.



Bad gut bacteria, such as Clostridia and some strains of Escherichia coli (E.coli) trigger inflammation, leading to bloating, diarrhoea and constipation.

The team tested three types of cocoa powder, the raw ingredient used to make chocolate, in an artificial digestive tract consisting of a series of modified test tubes.

Cocoa contains so-called antioxidant polyphenol compounds, such as catechin and epicatechin, and a small amount of dietary fibre.

Both components are poorly digested and absorbed, but are readily processed by the friendly bacteria in the colon.

Cocoa, stock image pictured, contains so-called antioxidant polyphenol compounds, such as catechin and epicatechin, and dietary fibre. These components are poorly digested, but are readily processed by friendly bacteria in the colon. This turns large polymers into smaller molecules with an anti-inflammatory effect

HOW COCOA IS DIGESTED

Cocoa contains so-called antioxidant polyphenol compounds, such as catechin and epicatechin, and a small amount of dietary fibre. Both components are poorly digested and absorbed into the body, but are readily processed by the friendly bacteria in the colon. 'In our study we found that the fibre is fermented and the large polyphenolic polymers are metabolised to smaller molecules, which are more easily absorbed,' said Dr John Finley, who led the Louisiana research team. 'These smaller polymers exhibit anti-inflammatory activity. When these compounds are absorbed by the body, they lessen the inflammation of cardiovascular tissue, reducing the long-term risk of stroke.'

Dark chocolate contains a higher cocoa content, increasing this process.



'In our study we found that the fibre is fermented and the large polyphenolic polymers are metabolised to smaller molecules, which are more easily absorbed,' said Dr John Finley, who led the Louisiana team.

'These smaller polymers exhibit anti-inflammatory activity. When these compounds are absorbed by the body, they lessen the inflammation of cardiovascular tissue, reducing the long-term risk of stroke.'

The findings were presented at the American Chemical Society's annual meeting in Texas.

Combining cocoa with prebiotics - indigestible food ingredients that stimulate bacterial growth - is likely to enhance the process with beneficial results, said Dr Finley.

'When you ingest prebiotics, the beneficial gut microbial population increases and out-competes any undesirable microbes in the gut, like those that cause stomach problems,' he added.

Prebiotics are found in foods such as raw garlic, raw wheat bran, and cooked whole wheat flour, and are especially abundant in raw chicory root. They can also be obtained from widely available supplements.