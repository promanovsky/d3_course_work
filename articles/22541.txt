KATE Winslet has revealed why she gave her son the unusual name of Bear Blaze.

The actress says she and her husband Ned Rocknroll settled on Bear quite early in the pregnancy.

“A friend of mine when I was younger was nicknamed Bear,” the 38-year-old told Ellen DeGeneres on her talk show, “and I just had always really loved it. He was very much a bear. He was everyone’s shoulder to cry on, he was a big bear hug, he was just this great figure in my life, and I just always remembered him.”

Winslet also said her son’s middle name Blaze was a reference to the house fire she escaped on Richard Branson’s Necker Island in August 2011. It was here that she met her husband, who is also Branson’s nephew.

“The house burned down and we survived,” she says. “But we wanted something of the fire, and so Blaze was the name that we came up with.”

When asked why her son was Winslet’s surname instead of his father’s, Winslet replied: “Why do you think, Ellen?”

Winslet has two other children, Mia Honey, 13 with ex-husband Jim Threapleton and Joe Alfie, 10 with ex-husband Sam Mendes.