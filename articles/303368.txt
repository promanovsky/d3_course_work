From TikTok to Instagram, Facebook to YouTube, and more, learn how data is key to ensuring ad creative will actually perform on every platform.

Don’t hold your breath for any crazy smartphone pricing from Amazon.

Instead, Amazon is following most other smartphone makers with a $200 AT&T contract price for its Fire Phone. You can also snag the phone with AT&T’s Next installment plan for $27 a month, but that’s also in-line with most other phones.

Amazon is also chipping in a year of free Prime membership with Fire Phone buyers, which typically costs $100.

READ MORE: Amazon’s ‘Fire Phone’ makes 3D on smartphones seem worthwhile

The entry-level Fire Phone also comes with 32GB of storage, twice the amount of Apple’s $200 (on-contract) iPhone 5S. Amazon is also offering a Fire Phone model with 64GB of storage for $300 (again, $100 less than the competing iPhone 5S model).

The Fire Phone is available July 25, according to a listing on Amazon’s website.

While it would have been nice to see Amazon break new ground in smartphone pricing — imagine if this were $100 with a year of free Prime — the amount of high-end hardware inside it likely made that prohibitive. Instead of being a budget device, the Fire Phone is a bold statement that Amazon can go toe-to-toe with established smartphone makers like Apple and Samsung.