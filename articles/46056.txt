Movie

The 'Captain America: The Winter Soldier' actor credits his Marvel superhero role for helping him land his dream job as a movie director.

Mar 26, 2014

AceShowbiz - After revealing his desire to do more directing gig, Chris Evans gives a hint when he will take the hiatus. When asked when he planned to hang up his Captain America shield, he replied, "That's a good question. We can do this out loud, 'Avengers: Age of Ultron' will shoot till August. I wouldn't be surprised if for all of 2015, we didn't do a movie. I bet that by 2017, I'll be done."

"I've known for a while I wanted to direct. But (time) never really opens up. There's another movie to do, there's another acting job. It just got to a point where I was like, you know what - I have to do this," he said in an interview with Variety.

"If I'm acting at all, it's going to be under Marvel contract, or I'm going to be directing. I can't see myself pursuing acting strictly outside of what I'm contractually obligated to do," he added. In fact, it was thanks to Marvel that he landed his dream job as a director. "Without these movies, I wouldn't be directing," he explained.

"They gave me enough overseas recognition to greenlight a movie," he elaborated. "And if I'm speaking extremely candidly, it's going to continue to do that for as long as the Marvel contract runs."

The actor makes his directorial debut in upcoming movie, "1:30 Train", which revolves around a woman who is robbed on her way to catch a train. "I put everything in '1:30 Train'," he said. But whether the movie is a success of not, he will continue his passion. "That's not a luxury that most people are afforded," he stated.

Evans was initially reluctant to join Marvel and turned the offer multiple times before. "The problem was initially, it was a nine-movie contract," he recalled the first time he was approached for the role. "And they said, if these movies take off and do very well, and my life changes and I don't respond well, I don't have the opportunity to say, listen, I need a f***ing break. That just scared me."

"They called back and they tweaked the deal," he continued. "It went from nine (films) to six. I said no again." His reps, his close people, and Robert Downey Jr. all tried to convince him to take the offer. He remembered, "My family was even going, 'Are you sure you're making the right decisions?' It started to feel like maybe this is what I'm supposed to do."

Evans who previously played Johnny Storm or Human Torch in "Fantastic Four" will reprise his Captain America role in the upcoming "Captain America: The Winter Soldier", "Avengers: Age of Ultron", and then "Captain America 3". In respective order, they are due April 4 this year, May 1, 2015, and May 6, 2016.