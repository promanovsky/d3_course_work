Clearly, the saying -- words can either heal or hurt -- holds true to this latest research that suggests how calling or labeling a teenage girl "too fat" isn't going to motivate her to make herself lose weight but to likely even push her the other way around.

The research, Weight Labeling and Obesity, conducted by psychologists at the University of California - Los Angeles discloses that girls labeled too fat by the age of 10 have higher chances of becoming more obese at the age of 19, following data from 1,213 girls of African-American descent as well as 1,166 white girls residing in Cincinnati, Washington D.C. and Northern California. Of the respondents, whose weight and height were measured during the start of the research and also nine years after, 58 percent said they were called too fat at 10 years old.

"That means it's not just that heavier girls are called too fat and are still heavy years later; being labeled as too fat is creating an additional likelihood of being obese," said senior study author A. Janet Tomiyama in a statement, adding that the effect remained the same even after removing some factors in the research including the girls' race, income and actual weight. She is also an assistant professor of psychology at the College of Letters and Science at UCLA.

Jeffrey Hunger, co-author of the study, explains that name-calling or labeling may result to certain behaviors such as people worrying about the discrimination and stigma experienced by overweight people. He adds that the recent study also proves that the anticipation or actual experience of the stigma intensifies the stress and brings about overeating.

"Making people feel bad about their weight could increase their levels of the hormone cortisol, which generally leads to weight gain," Tokiyama also says. She adds that when people feel bad about themselves, there's a tendency to indulge more in food or decide not to go on exercise or diet.

Providing the data of the study was the National Heart, Lung and Blood Institute, which is part of National Institutes of Health. The JAMA Pediatrics Journal published the said study online on April 28 and will publish in its print edition in the June 2014 issue.

It appeared, though, in two separate studies that being thin doesn't always mean being healthy and being able to live longer lives.

Tomiyama, along with Traci Mann, formerly UCLA psychology professor and currently at the University of Minnesota-Minneapolis, and Britt Ahlstrom, a UCLA graduate student, analyzed several long-term studies to figure out if losing weight would also mean health improvements associated with diabetes, blood glucose, hypertension and cholesterol. The study, Long-term Effects of Dieting: Is Weight Loss Related to Health?, published by the Social and Personality Psychology Compass Journal in December 2013, found no clear relationship among the two.

"Everyone assumes that the more weight you lose, the healthier you are, but the lowest rates of mortality are actually in people who are overweight," Tomiyama says.

She adds that they found no increased mortality risk in people with body mass index of 30, but rather found highest mortality rate in underweight people.

These findings likewise proved another study published in 2007, wherein Tomiyama and her colleagues examined several long-term studies and discovered people can cut weight initially by five to 10 percent with any diet regiments. Unfortunately, majority of them gain their weight back, even tagging along more pounds, while only the remaining minority can maintain weight loss.

Blame it on the power of genes, which can make it harder for people to maintain a normal weight or to lose excess pounds.

"People who say it's your fault if you're fat underappreciate the role of genes," Tomiyama says.

In fact, she says there's a research proving how twins, separated by birth and irrespective of the varied environment they grew up in, still have weights that are very similar.

Currently directing the Dieting, Stress and Health Laboratory at the UCLA, Tomiyama suggests that people should focus on healthy eating and fitness instead of obsessing about their weight. She is strongly against the stigma thrown on overweight people.

TAG childhood obesity

ⓒ 2018 TECHTIMES.com All rights reserved. Do not reproduce without permission.