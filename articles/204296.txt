Shutterstock photo

Investing.com -

Investing.com - Crude oil prices were up slightly in early Asian trade on Monday as tensions heightened in the Ukraine on a referendum in the east of the country to join Russia.

Pro-Russian separatists in eastern Ukraine appeared to be on track to declare victory in a referendum Sunday that Kiev and the West say is illegal and riddled with irregularities.

The vote ratchets up tensions between the Kremlin, which may recognize the vote, and the nascent government in Kiev, which is struggling to regain control of the two provinces that it says Moscow is destabilizing through support of rebels.

Russia produced 10.4 million barrels of oil per day in 2012 and exported 7.4 million, making it the world's second largest oil exporter after Saudi Arabia.

On the New York Mercantile Exchange, West Texas Intermediate crude oilfor delivery traded at $100.05 a barrel, up 0.06%, after settling at $99.99 a barrel by close of trade last week.

Elsewhere, on the ICE Futures Exchange in London, Brent oil for June delivery dipped 0.14%, or 15 cents, on Friday to settle at $107.89 a barrel by close of trade.

Last week, crude oil futures briefly rose to a two-week high before turning lower as a broadly stronger U.S. dollar prompted investors to lock in recent gains.

The U.S. dollar rallied to a one-month peak against the euro on Friday, extending steep gains from the previous session after the European Central Bank indicated that it could ease monetary policy as soon as next month.

Oil prices typically weaken when the U.S. currency strengthens as the dollar-priced commodity becomes more expensive for holders of other currencies.

Futures were higher earlier in the day amid easing concerns over a slowdown in demand from China. Data released Friday showed that consumer price inflation in China rose 1.8% in April from a year earlier, less than market expectations for a 2.0% gain.

On Thursday, official trade data showed that China's surplus widened to $18.45 billion in April from a surplus of $7.7 billion in March, compared to estimates for a surplus of $13.9 billion.

Exports climbed 0.9% from a year earlier, beating expectations for a 1.7% decline and following a 6.6% drop in March. Imports rose 0.8%, compared to forecasts for a 2.3% decline and after plunging 11.3% in the previous month.

China is the world's second largest oil consumer after the U.S. and has been the engine of strengthening demand.

In the week ahead, investors will be looking to U.S. data on retail sales, consumer prices and consumer sentiment for further indications on the strength of the economy and the need for stimulus.

Data from the Commodities Futures Trading Commission released Friday showed that hedge funds and money managers decreased their bullish bets in New York-traded oil futures in the week ending May 6.

Net longs totaled 299,543 contracts, down 9.4% from net longs of 330,710 in the preceding week.

Investing.com offers an extensive set of professional tools for the financial markets.

Read more News on Investing.com and download the new Investing.com Stocks & Forex App for Android!

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.

The views and opinions expressed herein are the views and opinions of the author and do not necessarily reflect those of Nasdaq, Inc.