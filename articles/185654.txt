A new report showed that housing prices rose in March, but at a slower pace than a month earlier. Reuters

U.S. stocks are falling in early trading on Tuesday as corporate earnings wind down and investors wait for testimony from Federal Reserve Chair Janet Yellen on the economy. A new report showed that housing prices rose in March, but at a slower pace than a month earlier.

KEEPING SCORE: The Dow Jones industrial average fell 56 points, or 0.3 percent, to 16,474 as of 10:35 a.m. Eastern time. The Standard & Poor's 500 index dropped three points, or 0.2 percent, to 1,881. The Nasdaq composite fell six points, or 0.2 percent, to 4,131.

OFFICE DEPOT UP: The office supply chain soared 81 cents, or 19 percent, to $4.97 after reporting adjusted profits for the first quarter twice as high as analysts had expected. The company also said it would close at least 400 U.S. stores, as its merger with OfficeMax resulted in overlap of retail locations.

Also Check: HDFC Bank Q4, Indian rupee, BSE Sensex, NSE Nifty, Gold price on May 06, 2014

INSURER WOES: American International Group fell $1.24, or 2 percent, to $51.50. The company reported revenue that was below what investors were expecting.

HOME PRICES: U.S. home prices rose at a slightly slower pace in the 12 months that ended in March, according to data provider CoreLogic. It's another sign that weak sales, caused in part by rising mortgage rates, have begun to restrain the housing market's sharp price gains.

ANOTHER BIG PHARMA DEAL: Merck fell $1.14, or 2 percent, to $57.48 after the drug company agreed to sell its non-prescription medicine and consumer-care business to Germany's Bayer for $14.2 billion. Products in that business include Claritin allergy pills, Coppertone sunscreen and Dr. Scholl's footcare products.

MORE EXPORTS: The U.S. trade deficit narrowed in March as exports rose to the second highest level on record, led by gains in sales of aircraft, autos and farm goods. The deficit declined to $40.4 billion, down 3.6 percent from a revised February imbalance of $41.9 billion, the Commerce Department reported. The February deficit had been the biggest trade gap in five months.

EUROPE: In Europe, Germany's DAX fell 0.8 percent while the CAC-40 in France fell 0.8 percent. Britain's FTSE-100 lost 0.5 percent.

BONDS AND OIL: U.S. government bond prices rose slightly. The yield on the 10-year Treasury note fell to 2.60 percent from 2.61 percent Monday. The yield is close to its lowest of the year and has fallen from 3 percent at the start of January. The price of oil rose 25 cents, or 0.3 percent, to $99.73 a barrel.