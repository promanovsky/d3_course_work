Federal regulators are filing a complaint alleging that T-Mobile customers have been scammed by their mobile phone provider.

The Federal Trade Commission brought the lawsuit on Tuesday against T-Mobile USA on the grounds that "until at least December 2013" the wireless company made millions of dollars by charging for third-party services that were never authorized by customers and were often offered by scammers, according to the complaint.

This is a process known as "cramming," whereby a phone company charges customers for services offered by another provider, yet the phone company receives a large percentage of those charges. In T-Mobile's case, according to the FTC, it was receiving somewhere between 35 and 40 percent of the total amount that consumers paid for dubious services such as ringtones, wallpaper, flirting tips, horoscope information, and celebrity gossip that often cost customers $9.99 a month. These practices "have caused consumers millions of dollars," the complaint states.

The complaint further notes that T-Mobile's billing practices made it difficult for people to know they were in fact paying for these third-party services. In a customer's online bill, these charges were labeled under a heading called "Usage Charges." But this heading still did not show what exactly the individual charges were for. For example, under "Usage Charges" customers would be billed for things such as text messaging and "Premium Services." But the bill did not clarify that "Premium Services" also included third-party services, according to the complaint. Even the company's full 50-page phone bill did not adequately clarify that "Premium Services" included these external charges, the FTC says.

The FTC is seeking a court order to end T-Mobile's practice of "mobile cramming" and to ensure refunds for T-Mobile customers. The FTC also says there is evidence that T-Mobile continued the practice of "cramming" even after receiving complaints at least as early as 2012.

“It’s wrong for a company like T-Mobile to profit from scams against its customers when there were clear warning signs the charges it was imposing were fraudulent,” FTC Chairwoman Edith Ramirez said in a statement. “The FTC’s goal is to ensure that T-Mobile repays all its customers for these crammed charges."

The complaint says that T-Mobile, the fourth largest US mobile phone provider in terms of customers, has refused to refund some of its customers. It has offered only partial refunds to others and in some cases has urged customers to seek refunds from the scammers themselves. And in some instances, it has insisted that these charges were authorized even when customers had no record of the authorization, according to the complaint.

For its part, T-Mobile announced in November of last year that it would no longer let third-party providers charge customers for premium services. And last month the company launched a new program to "proactively reach out" to customers who were billed for such third-party services and give them the opportunity to request a refund for these charges.

"If you’ve been charged for a third-party service you didn’t sign up for, it should be easy to get a refund," Mike Sievert, Chief Marketing Officer for T-Mobile, said in a company blog post last month. "If customers were charged for services they didn’t want, we’ll make it right. That’s being the Un-carrier."

But in response to these recent allegations T-Mobile chief executive officer John Legere has recently stated that the real blame lies with the third-party providers.

"We are disappointed that the FTC has chosen to file this action against the most pro-consumer company in the industry – rather than the real bad actors," Mr. Legere said in a company blog post. "As the Un-carrier, we believe that customers should only pay for what they want and what they sign up for."

Get the Monitor Stories you care about delivered to your inbox. By signing up, you agree to our Privacy Policy

In the past year, the FTC has sought to end mobile cramming, filing several lawsuits and holding a public workshop on the matter.

The complaint was filed in the US District Court for the Western District of Washington.