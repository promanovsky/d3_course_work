Eurozone private sector growth slowed for a second month running in June and was the weakest since December as downturn deepened in France.

The headline index covering output of both manufacturing and services fell to 52.8 from 53.5 in May, flash survey data from Markit Economics showed Monday. The score was above the neutral 50-mark, but below economists' forecast of 53.4.

Although the survey suggested a slowdown, the average Purchasing Managers' Index reading for the second quarter as a whole was the highest since the second quarter of 2011.

The currency bloc is currently finding it hard to build growth momentum despite much reduced fiscal headwinds, sustained negligible sovereign debt tensions and very accommodative monetary policy providing a more supportive environment, IHS Global Insight's Chief European Economist Howard Archer said.

The services PMI dropped to 52.8 in June from 53.2 in May, when it was expected to rise to 53.3. Meanwhile, the manufacturing PMI fell to 51.9 from 52.2 in the prior month. The expected reading was 52.1.

"The June PMI rounded off the strongest quarter for three years, but a concern is that a second consecutive monthly fall in the index signals that Eurozone recovery is losing momentum," Chris Williamson, chief economist at Markit, said.

The euro area grew only 0.2 percent in the first quarter. The European Central Bank predicts 1 percent growth this year and 1.7 percent in 2015.

Output rose for the twelfth consecutive month in June, but the rates of growth in manufacturing and services slowed to nine- and three-month lows, respectively.

In a sign that activity may re-accelerate, the survey's measure of new orders rose to its highest since May 2011, driven by the service sector.

Firms took on more staff in order to boost capacity in line with the ongoing growth in activity. Nonetheless, the rates of job creation in both manufacturing and services remained similar to the very modest rates seen in April and May.

The survey indicated that price pressures picked up in June. Average input costs showed the largest monthly increase since November. Prices charged continued to fall, but the decline was the smallest in the current 27-month sequence of reductions.

Survey results from Germany and France highlighted the continuing divergence between two largest euro area economies.

Germany's private sector expanded at the slowest pace in eight months in June. The flash composite PMI declined to 54.2 in June from 55.6 in May. Despite softening to the weakest in eight months, the pace of growth remained above the long-run series average.

The services index dipped to 54.8 from 56 in May. The score was expected to drop moderately to 55.8. The manufacturing PMI, meanwhile, rose marginally to 52.4 from 52.3 in the preceding month. The expected reading was 52.5.

The French private sector contracted further in June, reflecting weakness in new orders and employment. The flash composite PMI came in at 48, a four-month low, and down from 49.3 in May.

The decline in output was broad-based, with both manufacturers and service providers registering reductions since May. The services PMI slid to 48.2 from 49.1 in May. Likewise, the manufacturing PMI dropped to 47.8 from 49.6 in the previous month.

For comments and feedback contact: editorial@rttnews.com

Business News