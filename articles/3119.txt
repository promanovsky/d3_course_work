True Detective, the HBO cop series that brought a pair of Hollywood heavyweights to the small screen for an eight-episode mini-series blending brooding character study, vaguely existentialist philosophy, obscure literary references and a convoluted serial killer plot — oh yeah, and plenty of sex — turned out to be a winning formula for the pay cable network.

But HBO proved not quite ready for prime time when it came to delivering True Detective on the internet.

Despite being ahead of the game when it created its online HBO GO service four years ago, well before its competition started offering programming via the internet, HBO’s online app could not handle the overwhelming fan demand to watch Sunday night’s finale of True Detective on their iPads and Roku devices.

[Mild Spoilers Ahead]

With hundreds of thousands — even millions — of fans attempting to log in and see Woody Harrelson and freshly-minted Oscar winner Matthew McConaughey catch an overweight psychopath who in the end, seemed inspired as much by The Texas Chainsaw Massacre‘s Leatherface as by Robert W. Chambers’ “Yellow King,” the HBO GO service simply could not handle the workload. Thousands of would-be True Detective viewers saw the above message, telling them “some videos may not be available at this time.”

On its official Twitter account, HBO posted a somewhat friendlier, even a bit gloating message.

Due to overwhelmingly popular demand for #TrueDetective, we’ve been made aware of an issue affecting some users. Please try again soon. — HBO GO (@HBOGO) March 10, 2014

But not all True Detective fans were in an understanding mood.

In fact, Twitter itself felt like was about to be overwhelmed with tweets from dissatisfied customers, that ranged in tone from the outraged to the snarky.

Worried this is what I’ll look like by the time HBOGO loads the True Detective finale pic.twitter.com/pl9kTycI2j — Jake Laperruque (@JakeLaperruque) March 10, 2014

Actually not all of the frustrated True Detective viewers were, in fact, HBO customers.

HBO GO is supposed to be available only to people who have a cable subscription through a provider such as Comcast Xfinity or Time Warner Cable, and pay for a monthly HBO GO subscription on top of the basic cable package.

But the 41-year-old cable network has made no secret of the fact that it will tolerate, even encourage, “piggy back” viewers — that is, people who borrow log-in credentials from their parents, friends or any other HBO subscriber willing to share.

While some online services, such as Netflix, regulate this kind of log-in sharing rather strictly, HBO sees it as just another marketing gimmick.

“What we’re in the business of is building addicts, building video addicts,” said HBO boss Richard Plepler, “and the way we do that is exposing our product and our shows and our brand to more and more people.”

In other words, even though HBO might have been somewhat red-faced over its inability to stream the True Detective finale to everyone who wanted to see it, the network had to be delighted that the show had spawned a whole new level of addiction.

[Images By Lacey Terrell/HBO]