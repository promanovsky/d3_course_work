Solar energy is soaring, and 2013 was no exception. A new report from the Solar Electric Power Association reveals three of the latest solar trends, and they've got little to do with residential rooftops. Here's what you need to know.

1. 2013 was a stellar year for solar

It's been a long time coming, but solar is finally getting some serious scale. Before 2013, solar capacity clocked in at 6,298 MW. While that might seem like an impressive number, it accounts for around 1.3% of our nation's total 467,900 MW of generation capacity.

But with 4,211 MW of new capacity added in 2013 alone, that percentage jumped to around 2.2%. And while that's hardly on par with wind energy or stalwarts like nuclear, coal, and natural gas, it's making solar increasingly hard to sneeze at.

2. Utilities are running the solar show

Investors are pouring money into solar growth stocks that promise to blanket every residential rooftop with a solar panel. And while this may someday be the case, utilities have finally gotten the message -- and they're not leaving solar generation to regular citizens. In 2013, utility-scale solar installations maxed out at 2,627 MW -- nearly four times residential's relatively lackluster 679 MW. The U.S. Energy Information Administration's (EIA) numbers put utility capacity additions even higher (2,959 MW), but the additions are enormous, either way.

3. Not everyone's stoked about solar

Despite federal tax credits, improved technology, and increasingly expensive traditional fuels, solar isn't for everyone. California-based PG&E (NYSE:PCG) topped the list of utilities for 2013 solar capacity additions, increasing its stake by a whopping 1,471 MW. With Sempra Energy's (NYSE:SRE) San Diego Gas & Electric Company in second place with 643 MW, it's easy enough to see where the sun's shining brightest.

California's solar capacity clocks in at 5,537 MW -- just over half that of the country's entire capacity. Compare that to 10th-place Florida, which boasts just 188 MW, and the story of concentrated solar growth becomes increasingly clear.

California's own sunny weather creates a good starting point for solar, but it's the state's regulators and support structure that have truly pushed its capacity forward. The California Solar Initiative has set a goal of 3,000 new MW of solar power by 2017, and the California Public Utilities Commission has allocated over $2.1 billion in incentives to boost capacity.

Where to invest?

If you're looking for a solar investment with steadier growth and smaller risk than rooftop start-ups, utilities are an increasingly attractive option. Their regulated earnings, long-term power purchase agreements, and pre-scaled infrastructure give investors the option of a stable solar stake. Utilities have shown solar investors they mean business -- and in 2014 and beyond, it could prove to be a profitable business indeed.