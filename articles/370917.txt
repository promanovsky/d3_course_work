Employees at the National Institutes of Health (NIH) found vials containing variola, also known as smallpox, in an FDA laboratory storage room in Bethesda, Maryland. They reported the information to the Centers for Disease Control and Prevention's (CDC) Division of Select Agents and Toxins (DSAT).

The workers found the vials as they were preparing to move the lab over to the main FDA campus on Monday night. They were immediately secured in a CDC-registered select agent containment laboratory and sent to the CDC's facility in Atlanta for testing. The results revealed that the vials did contain smallpox, but no workers were exposed to the virus.

Smallpox is a serious, contagious, and sometimes fatal infectious disease caused by the variola virus. The World Health Organization declared smallpox to be eradicated in 1979; with the last global case documented in Somalia in 1977 (the last case in the US was documented in 1949). Routine vaccinations for smallpox are not required because they're no longer necessary, but samples of the disease are kept in storage in case the disease was to resurface (as some predict through a bioterrorism event). Medical experts would use a strain of the variola virus to develop more vaccines.

The only two repositories for smallpox are the CDC in Atlanta and the State Research Centre of Virology and Biotechnology (VECTOR) in Novosibirsk, Russia, which are both official World Health Organization-designated facilities. The CDC contacted the WHO about the situation and invited officials to participate in the investigation. However, there is no evidence that the vials have been breached. According to the CDC, the DSAT and FBI are currently working to unveil the history of how the vials were originally prepared and stored in the FDA laboratory.

In similar fashion, the Pasteur Institute in Paris reported over 2,000 lost SARS vials back in April. The SARS outbreak in the early 2000s killed nearly 800 people and infected thousands more. These situations remind medical experts and other such employees to ensure every safety procedure is followed. All it takes is a single misstep and a deadly disease could be reintroduced to the population.

In May, the WHO was actually considering destroying the remaining smallpox vials in the US and Russia, but they postponed the decision. Perhaps this situation will force the WHO to rethink their stance on the issue.

You can read more about the smallpox vials found in the FDA lab in this CDC news release.

@ 2018 HNGN, All rights reserved. Do not reproduce without permission.