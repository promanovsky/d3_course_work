The attorney general of the Iranian province of Fars on Wednesday denied a report that a local court had summoned Facebook CEO Mark Zuckerberg over privacy violations, despite complaints having been filed by some individuals.

Iranian state news agency ISNA reported on Tuesday that a conservative court had opened a case against instant messaging services WhatsApp and Instagram while also summoning Zuckerberg over the complaints.

"Reports regarding the summoning of Zuckerberg, the CEO of Facebook, by a court in Fars are completely false," the attorney general, Ali Alghasi-Mehr, said according to official state IRNA.

"But there are complaints by specific people over Facebook regarding the printing of certain videos and films," he added.

ISNA on Tuesday quoted Ruhollah Momen-Nasab, an Iranian internet official, as saying that the court in southern Fars province had opened the cases against the social networks after citizens complained of breaches of privacy.

"According to the court's ruling, the Zionist director of the company of Facebook, or his official attorney must appear in court to defend himself and pay for possible losses," ISNA quoted Momen-Nasab as saying, referring to Zuckerberg's Jewish background.

(Also see: Iranian Judge Summons Mark Zuckerberg for Breach of Privacy)

Most recently a Facebook campaign called "My Stealthy Freedom" where women in Iran can upload pictures of themselves without the mandatory hijab has garnered significant attention, with over 380,000 likes. A website has been created in response to the first entitled "Iranian Women's Real Freedom."

An escalating struggle is underway between moderate President Hassan Rouhani's drive to increase Internet freedoms and demands by the conservative judiciary for tighter controls.

Iran is still under international sanctions over its disputed nuclear activities and it is difficult for U.S. citizens to secure travel visas, even if they want to visit.

Internet use is high in Iran, partly because many young Iranians turn to it to bypass an official ban on Western cultural products, and Tehran occasionally filters popular websites such as Twitter and Facebook.

Rouhani, in remarks that challenge hardliners who have stepped up measures to censor the Web, said earlier this month that Iran should embrace the Internet rather than see it as a threat.

A Rouhani administration official said Iran would loosen Internet censorship by introducing "smart filtering", which only keeps out sites the Islamic government considers immoral.

© Thomson Reuters 2014

