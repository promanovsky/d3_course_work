The Samsung Galaxy S5 fingerprint scanner has been successfully hacked just days after the smartphone release as was the case with iPhone 5s.

The biometric sensor technology is still immature as more and more devices seem susceptible to hacks due to their design focus being limited to convenience rather than enhanced security.

A European group of hackers had earlier exposed the vulnerability of iPhone 5s fingerprint scanner by training the TouchID sensor to accept fingerprints through dummy objects like nipples and paw prints.

According to German-language security blog H Security, SRLabs has posted a video footage showing how Galaxy S5 can be spoofed with a dummy fingerprint to gain unauthorised access to the phone, besides compromising the PayPal user account information on the device.

Unlike Apple's Touch ID system which requires the user to input password every time the device is rebooted, the Galaxy S5 does not require any password input while using sensitive apps such as PayPal or even after a reboot.

Check out the video depicting the fingerprint scanner hack in action:

Nevertheless, PayPal has reassured its commitment to protect biometric-based security loopholes with the use of auto-generated cryptographic keys for enhanced security while its service denies access via fingerprint scanning.

As PayPal notes, the existing security key can be reset when the device is hacked. PayPal has not clarified if a new key can be generated while using a fingerprint scan from the same device.

Nevertheless, PayPal offers protection to its consumers via purchase protection policy in case of fraudulent incidents while using its service.

In a statement to BGR, a PayPal spokesperson has issued the following public statement via email: