Salesforce.com (CRM), Tuesday reported first-quarter net loss of $96.9 million, or $0.16 per share, wider than $67.7 million, or $0.12 per share last year.

Adjusted earnings for the latest first quarter rose to $69.5 million or $0.11 per share, from $61.0 million or $0.10 per share in the comparable quarter last year. On average, 38 analysts polled by Thomson Reuters expected the company to report earnings of $0.10 per share for the quarter. Analysts' estimates typically exclude special items.

Total revenues for the quarter grew 37 percent to $1.23 billion from $892.63 million last year, while thirty five analysts estimated revenues of $1.21 billion for the quarter.

For the second quarter, the company expects loss of $0.13 to $0.12 per share, adjusted earnings of $0.11 to $0.12 per share, and revenues of $1.285 billion to $1.290 billion. Analysts currently estimate earnings of $0.12 per share on revenues of $1.27 billion.

For the full year 2015, the company now expects loss of $0.49 to $0.47 per share, adjusted earnings of $0.49 to $0.51 and revenues of $5.30 billion to $5.34 billion. Analysts currently expect earnings of $0.50 per share on revenues of $5.29 billion for the full year 2015.

Earlier, the company expected full year 2015 loss of $0.53 to $0.51 per share, adjusted earnings of $0.48 to $0.50 and revenues of $5.25 billion to $5.30 billion.

For comments and feedback contact: editorial@rttnews.com

Business News