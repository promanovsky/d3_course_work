An 11-year-old Michigan girl whose face was half-eaten by a pet raccoon has undergone the next stage of revolutionary surgery to recreate an ear torn off a decade ago.

Charlotte Ponce was just three months old when her biological parents left her alone in the company of their raccoon only for the animal to pounce on the baby and maul her face.

In the wake of the horrific attack Charlotte and her brother Marshall were adopted by great-aunt and uncle, Sharon and Tim Ponce.

Mrs Ponce told ABC News: "They had a couple of raccoons as pets. Their mother went next door and I don't know if the dad was supposed to be watching the kids.

"A raccoon got in the house and I figured he was hungry, because they weren't feeding them. The reason he probably ate the right side of her face was because of the drool."

On Tuesday, a decade later, Charlotte, underwent a revolutionary eight hour procedure to recreate the ear that was gnawed off in the attack.

Surgeons at Beaumont Children's Hospital have taken cartilidge from Charlotte's ribcage to use as a replacement ear in a procedure that has been performed only twice before.

Hospital spokesman Mark Geary said: "Dr Chaiyasate will pull a piece of the rib and mold it into the shape of an ear and put inside her forearm to grow. Then, eight weeks later, they will remove it and attach it to her ear."