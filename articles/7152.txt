Anne Sweeney: Industry Reacts to ABC/Disney Chief's Shocking News

Hollywood's most powerful woman quits at the top after 18 years to become a TV director (yes, really!) as Disney CEO Bob Iger reveals his succession plan ("My goal is to do it fast") and both detail the full backstory in interviews with THR.

"Wait: Disney's Anne Sweeney wants to become a TV director? Is this another @jimmykimmel prank?," writes Vulture editor Joe Adalian.

The surprise reveal that Hollywood's most powerful woman is stepping down prompted a flood of reactions.

In an exclusive cover interview with The Hollywood Reporter, Anne Sweeney and Disney CEO Bob Iger speak at length about the decision as well as the next steps for Sweeney and Disney. Among their admissions: Sweeney intends to pursue a career as a TV director, and Iger would like to hire her replacement while pilot season is still in "full swing."

STORY: Disney's Anne Sweeney Exit: 5 Possible In-House Replacements

Many in the industry, including television reporters covering Disney and its machinations, took to Twitter Tuesday afternoon to react to the news, with thoughts on succession plans (former ABC reality chief Andrea Wong? CFO Peter Seymour?), Iger's stated support for ABC chief Paul Lee and the surprise decision for the long-time executive to take a creative turn.

Here are some of the most telling -- and in some cases, the most entertaining -- reactions.

I love it when people follow their dreams. Never easy but always rewarding! http://t.co/IU3qIxZm8H — shonda rhimes (@shondarhimes) March 11, 2014

Wait: Disney's Anne Sweeney wants to become a TV director? Is this another @jimmykimmel prank? — Joe Adalian (@TVMoJoe) March 11, 2014

BREAKING Anne Sweeney exiting Disney. Leaning out. — Claire Atkinson (@claireatki) March 11, 2014

FTR, Anne Sweeney says she wants to "LEARN to be a director." [My caps.] And I want to learn to be a concert pianist. Alas... — vernejgay (@vernejgay) March 11, 2014

guys, I'm leaving @TVGuideMagazine to take over #Disney. Because HUGE career leaps make sense. — Damian Holbrook (@TVGMDamian) March 11, 2014

Do TV directors have heads of PR? — Joe Flint (@JBFlint) March 11, 2014

RE: Anne Sweeney leaving Disney..she was clearly marginalized after Iger transition pit Staggs v Rasulo for successor..not surprising.. — Peter Lauria (@peterlauria3) March 11, 2014

Happy to offer Anne Sweeney tips on TV directing. Little busy at the moment, but maybe once @MomCBS wraps. — Jeff Greenstein (@blue439) March 11, 2014

Wouldn't it be a hoot if there was a typo in the Disney release and Sweeney is actually going to run DirecTV? Wouldn't t? — Masked Scheduler (@maskedscheduler) March 11, 2014

Good luck to Disney/ABC head Anne Sweeney on her next chapter. One of the smartest + nicest execs I've interviewed. http://t.co/A7jJ3KDluy — Shirley Brady (@shirleybrady) March 11, 2014

Best part of @janicebmin's Anne Sweeney story is when AS pretends she doesn't know when Paul Lee's contract is up. http://t.co/n8uVtT20AL — Kate Aurthur (@KateAurthur) March 11, 2014

Anne Sweeney leaving Disney/ABC to "direct?" gobsmacked doesn't do it justice. — Bill Carter (@wjcarter) March 11, 2014

“@shondarhimes: I love it when people follow their dreams. Never easy but always rewarding! http://t.co/V44RSjCXkV” Right! Congrats Anne!!! — Tony Goldwyn (@tonygoldwyn) March 11, 2014