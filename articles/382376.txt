Three countries in western Africa — Guinea, Liberia and Sierra Leone — are in the midst of the planet’s worst-ever Ebola outbreak, marking the first time the disease has ever been detected in the region.

As of July 15, 2014, 603 people have died as a result of the epidemic out of a total of 964 confirmed, probable and suspected cases. But those numbers are steadily rising as health workers on the ground have described the outbreak as “out of control.”

1. What is Ebola?

The Ebola virus disease — which is abbreviated as "EVD" and was once known as Ebola hemorrhagic fever — is a severe illness with a fatality rate that reaches 90%, depending on the strain. The World Health Organization (WHO) claims it is one of the world’s most virulent diseases.

Staff of the 'Doctors without Borders' ('Medecin sans frontieres') medical aid organization carry the body of a person killed by viral haemorrhagic fever, at a center for victims of the Ebola virus in Guekedou, on April 1, 2014. Image: SEYLLOU/AFP/Getty Images

Ebola was first detected in 1976, when a pair of simultaneous outbreaks occurred in Nzara, Sudan and in a village on the banks of the Ebola River in Democratic Republic of Congo — that’s where the virus gets its name.

2. What are the symptoms?

Because the Ebola virus is a hemorrhagic fever, the most extreme symptoms of a pre-fatal case of EVD is bleeding from the ears, nose and mouth.

But Ebola is often characterized by a sudden fever, weakness, muscle pain, headache and sore throat. Those symptoms are then followed by vomiting, diarrhea, rash, impaired kidney and liver function, and in some cases, both internal and external bleeding, according to WHO.

It can take anywhere from two to 21 days for an infected person to start showing symptoms.

3. How is it transmitted?

This particular strain of the Ebola virus is a new one and did not spread to West Africa from previous outbreaks in Uganda and Congo, researchers say. Rather, it's linked to the human consumption of bats carrying the virus, according to the AP.

A Ugandan man displays a bat he captured for food December 1, 2000 in a cave in Guru Guru, Uganda. Bats are being studied as one possible carrier of the Ebola virus. Image: Tyler Hicks/Getty Images

People generally become infected with Ebola through contact with infected animals — such as those bats or undercooked bushmeat — and then spread the virus to others through direct contact with bodily fluids.

Men who have recovered from the illness can still spread the virus to their partner through their semen for up to seven weeks after recovery, WHO says, stating men need to avoid sexual intercourse or wear condoms during that period of time.

Many of those who have fallen ill in the current outbreak are family members of victims and the health workers who treated them.

Health workers wearing protective suits walk in an isolation center for people infected with Ebola at Donka Hospital in Conakry on April 14, 2014. Image: CELLOU BINANI/AFP/Getty Images

Mourners, on the other hand, face a major risk of contracting the virus, too, as local custom dictates the victims’ bodies must be washed before they are buried. The fluid, for instance, could get onto their hands and they could in turn put their hands into their mouths or touch their eyes.

"The cultural practices are so deeply embedded that local people have told health care workers that if they did not adhere to the ablutions (ritual washing of the corpse), they would be shunning by everyone in their family and village," Dr. William Schaffner, an infectious disease specialist at Vanderbilt University Medical Center, says, illustrating the difficulties officials face in containing the outbreak.

4. Is there a cure?

There is no Ebola vaccine and there is no known cure. Furthermore, Ebola is a particularly challenging virus because of the difficulties preventing and containing an outbreak.

Patients who contract the disease are often kept comfortable and hydrated while they await their death. Several EVD vaccines are in a testing phase, according to WHO, but none of them are available for clinical use. Drug therapies for treating the disease are currently under evaluation as well.

5. Why is containment so difficult?

The key to halting Ebola is isolating the sick, but fear and panic have sent some patients into hiding, complicating efforts to stop its spread.

Activists and nonprofit groups are desperately trying to spread awareness in the African countryside where literacy is low, even if they have to sing their warnings or scream them through megaphones.

"It has no cure, but it can be prevented; let us fight it together. Let's protect ourselves, our families and our nation," goes the chorus of one song enlisted by the activists.

"Do not touch people with the signs of Ebola," sings musician and activist Juli Endee. "Don't eat bushmeat. Don't play with monkey and baboons. Plums that bats have bitten or half-eaten, don't eat them."

UNICEF and partners take to the streets of Conakry to combat the Ebola outbreak with information on how to keep families safe and to prevent the spread. Image: Flickr, unicefguinea

"Let this warning go out: Anyone found or reported to be holding suspected Ebola cases in homes or prayer houses can be prosecuted under the law of Liberia," President Ellen Johnson Sirleaf recently said, following a similar warning from Sierra Leone that stated some patients had discharged themselves from the hospital and had gone into hiding.

Last week, police fired tear gas at relatives trying to retrieve bodies from a hospital in Kenema.

6. Is it going to spread beyond the three countries to the U.S. or Europe?

Not if the global health community has anything to do about it.

Where is the #ebolaoutbreak? Our map explains which countries are most affected by the virus: pic.twitter.com/2BYbvX5wiU — British Red Cross (@BritishRedCross) July 8, 2014

A doctor with one of the groups that warned the world weeks ago of the worsening outbreak says Ebola is still contained to Africa, but it could easily make the terrifying jump.

"There are no cases from outside Africa to date,” said Dr. Unni Krishnan, head of disaster preparedness and response for the aid group Plan International. But “the threat of it spreading though is very much there.”

The mortality rate at this #EbolaOutbreak clinic has fallen from 90 % to 37% - docs say #Ebola can be beaten pic.twitter.com/O3GCK5QtEe — Dan Rivers (@danieljerivers) July 7, 2014

"The chance of Ebola spreading out of West Africa is very, very low," says infectious disease specialist Kamran Khan. "But if it did spread, Paris is probably the first city on the list."

The reason for Paris as the most likely city, Khan tells NPR, is that 10% of all flights out of a major international airport in Guinea head to the French capital. The rest are primarily local.

To combat that threat, officials force departing passengers to undergo temperature screening; those with a fever are pulled aside for further evaluation.

Employees of the sanitary control of Conakry airport check passengers before they leave the country on April 10, 2014. Image: CELLOU BINANI/AFP/Getty Images

The Associated Press contributed to this report.