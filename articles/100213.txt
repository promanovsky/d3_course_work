Welcome to Wonkbook, Wonkblog’s morning policy news primer by Puneet Kollipara. To subscribe by e-mail, click here. Send comments, criticism or ideas to Wonkbook at Washpost dot com. To read more by the Wonkblog team, click here.

Wonkbook’s Number of the Day: $121 million. That's the sum of what the top 10 billers charged for Medicare in 2012.

Wonkbook’s Chart of the Day: This chart shows a close relationship between wage inequality among women and the minimum wage.

Wonkbook's Top 5 Stories: (1) The limitations on that Medicare data; (2) what the Fed minutes tell us; (3) the pay-gap debate isn't going away for now; (4) Congress' corporate accountability clout; and (5) how Democrats are pushing immigration action.

1. Top story: Why Doctors are unhappy about the Medicare payment data release

Doctors react to release of Medicare billing records. "Doctors reacted swiftly and indignantly to Wednesday’s release of government records revealing unprecedented details about Medicare payments to physicians. Many resented being included on a list that showed some doctors billing Medicare for millions of dollars. The top 10 doctors alone received a combined $121.4 million for Medicare Part B payments in 2012. Federal investigators have scrutinized payments to three of the top 10 earners. In interviews, many of the doctors said they were just passing through the payment to drug companies. Some said they were unfairly singled out even though they were billing for an entire practice. And still others disputed the accuracy of Medicare data." Jason Millman and David S. Fallis in The Washington Post.

Explainers:

The top 10 Medicare billers explain why they charged $121M in one year. Jason Millman in The Washington Post.

Everything you need to know about today’s unprecedented Medicare pricing data dump. Jason Millman in The Washington Post.

Some doctors have more nuanced views of it. "Many say they favor sharing information but worry that the data presented by Medicare omits important details and may mislead the public and paint an unfairly negative picture of individual doctors....Dr. Gary Heit, a recently retired neurosurgeon in Redwood City, Calif., said he thought some of the details revealed could be useful to patients who want to make sure that the physicians they choose are experienced -- say in carotid-artery surgery, where more operations performed correlate with better patient outcomes. 'Being able to track the number of procedures is incredibly valuable,' Dr. Heit said. 'I think the ability to see the number of cases a physician does is really important.' However, the data only include treatments given to certain Medicare patients. Many other doctors worried that the data released was incomplete and often misleading. In some cases, enormous payments that seem to be going to one doctor are actually distributed to multiple others. But the data tables do not reveal that the money was shared." Denise Grady and Sheri Fink in The New York Times.

Zooming in: Eye doctors say their profits are smaller than data makes them appear. "More than any other specialists, ophthalmologists -- not cardiologists, cancer doctors or orthopedic surgeons -- were the biggest recipients of Medicare money in 2012. The 17,000 providers, most of whom are concentrated in Florida, Texas, California and New York, accounted for 7 percent -- $5.6 billion -- of the reimbursements to doctors and other providers. Included in the amount is $929 million for cataract surgery, about $1 billion for an expensive eye drug and $707 million for eye exams. Put another way: Of the 100 physicians who receive the largest payments from Medicare, nearly half are eye specialists. The reasons say volumes about an aging population and a specialty that does many procedures, some involving a very expensive drug, all of which are well-reimbursed by Medicare. They also speak to the difficulty of interpreting the data and what the numbers do and don’t reveal....Ophthalmologists say that their high representation among the list of big recipients is misleading. Much of what Medicare pays them, they say, goes to the cost of the drugs they administer to patients in their offices and the bulk of that money ultimately goes to the drug companies." Andrew Pollack and Reed Abelson in The New York Times.

Lots of regional variation in the cost of drugs that Medicare doctors use. "Most of the 4,000 doctors who received at least $1 million from Medicare in 2012 billed mainly for giving patients injections, infusions and other drug treatments, those records show. The data, an unprecedented trove of millions of billing records from Medicare -- as well as interviews with doctors -- highlight the role of pharmaceuticals in the nation’s staggering health-care costs. Of $64 billion Medicare paid to doctors in 2012, $8.6 billion was used to cover drugs, an amount that has been rising for years. Yet Medicare bureaucrats seeking to rein in drug costs have been stymied by rules that forbid the government to negotiate lower prices. In 2010, they even lost the ability to mandate, when two equivalent drugs are available, that physicians be paid only for the cheapest. At the same time, pharmaceutical companies have offered physicians incentives, such as discounts, to use large volumes. Now, with the data released by Medicare, the public has a clearer view of individual doctors’ drug practices. Many of the physicians who have submitted multimillion-dollar bills to Medicare blame high drug prices and say the pharmaceutical industry is taking most of the money. Typically, Medicare reimburses a physician for the price of the drug plus 6 percent." Peter Whoriskey, Dan Keating and Lena H. Sun in The Washington Post.

Is more data on the way? "Doctors denounced the accuracy and value of data listing $77 billion in Medicare payments to 880,000 medical providers, while consumer and industry groups said it could make the health-care system more cost-effective. The divergent views of Medicare’s first-ever release of U.S. payments to physicians suggested the impact may take years to play out. U.S. officials, meanwhile, said they may follow yesterday’s report on 2012 data by providing the same information from earlier years, a move that would help regulators and consumers trace changes in health care over time." Alex Wayne and Caroline Chen in Bloomberg.

Other health care reads:

Conservative experts wonder how to coexist with Obamacare. Sahil Kapur in Talking Points Memo.

House defeats bipartisan Obamacare tweak. Andrew Taylor in the Associated Press.

More evidence that Obamacare is getting to people who need it? Dylan Scott in Talking Points Memo.

More kids are getting health insurance...and it's not because of Obamacare. Clara Ritger in National Journal.

AIDS patients flock to Obamacare. Julie Appleby in The Daily Beast.

About those who want bigger Obamacare. Jonathan Bernstein in Bloomberg View.

LOS ANGELES TIMES: Medicare's real doctor payment problem. "The news that a small percentage of the country's physicians collected billions of dollars from Medicare in a single year may or may not be a testament to individual greed; some of the top recipients are under investigation for allegedly bilking the system, while others work long hours delivering costly care. But it is a powerful reminder that the program needs to stop rewarding doctors for the quantity of care they deliver rather than the quality. Happily, there's a bipartisan plan to do just that; unhappily, lawmakers haven't been able to agree on how to cover its cost. If Congress needed any further incentive to settle its differences, the fact that 1,000 doctors raked in $3 billion from Medicare should provide it." Editorial Board.

CHICAGO TRIBUNE: Important sunshine from Medicare data release. "Medicare, the federal health care system for the aged that spends more than $500 billion a year, faces an uncertain future. It desperately needs to be more efficient. The data released Wednesday suggest this system has a long, long way to go to control its costs. Now, high Medicare payments to particular doctors or institutions are not by definition signs of waste or fraud. Some doctors justifiably earn more because they're trained in certain specialties. Some treat patients who require more constant care. There are major regional variations in how doctors practice. Americans want their doctors to be good. High compensation draws talented people to medicine and to Medicare. They also want Medicare to survive ... but that's going to require a far more efficient system. The release of this data is an important step toward that goal." Editorial Board.

COHN: Republicans: We'll have that Obamacare alternative soon! Really! "You know that Obamacare alternative, the one that House Republicans promised to unveil in April? Their leaders announced on Tuesday that it’s not ready just yet. This should surprise exactly nobody....One reason for the lack of real action is that embracing a plan would force Republicans to commit to the real-life tradeoffs that any reform plan would require....But there are other reasons Republicans may find it hard to craft an alternative. The general idea of the Affordable Care Act -- creating a competitive private market, through which people choose plans and weigh choices of cost and coverage individually -- is pretty much the only way to achieve universal coverage through private insurance....Republicans are finally, if grudgingly, recognizing that the Affordable Care Act is here to stay." Jonathan Cohn in The New Republic.

MULLIGAN: The power of Sebelius. "In setting the 2015 calendar parameters for health plans and employers, Kathleen Sebelius, the secretary of health and human services, quietly did some creative but questionable arithmetic that forced taxpayers to give still more help to businesses and people who buy health insurance....Perhaps taxpayers of the future will remember March 11, 2014, as the day when one cabinet secretary added billions of dollars to the deficit." Casey B. Mulligan in The New York Times.

BLOOMBERG VIEW: Medicare's wasted advantage. "Anyone worried about the cost of health care in the U.S. should view this week's capitulation on Medicare Advantage with concern. Faced with a lobbying campaign that health insurers have described as their 'largest ever,' the federal government backed down from next year's proposed cuts to the program, which pays private insurers to deliver Medicare benefits. If the government has this much trouble trimming a useless subsidy for the insurance industry, it will have a hard time being frugal when it comes to health spending that actually matters." The Editors.

Top opinion

KRISTOF: Where the GOP gets it right. "Republicans may seem like ultimate Scrooges. Many want to slash food stamps, unemployment benefits and just about any program that helps the needy. So they know nothing about poverty, right? Wrong. Actually, conservatives have been proved right about three big ideas of social policy. Liberals may grimace, but hear me out on these points....These aren’t just abstract policies. These are ethical issues, touching on our obligations to fellow humans. If we offer the needy nothing but slogans and reprimands -- 'Strengthen your family! Get a job! Get an education!' -- then our antipoverty programs are a cruel joke as bankrupt as Marie Antoinette’s “Let them eat cake." Nicholas Kristof in The New York Times.

THE NEW YORK TIMES: The exact value of the gender-pay gap isn't what matters. "Threaded through the political fight over pay fairness is a continuing debate about the size of the pay gap. Mr. Obama and others often cite 77 cents as what women make on average for every $1 earned by men -- a figure that critics say is an exaggeration. In fact, it is a rough, but important, measure of overall workplace inequality. It is not a comparison of what men and women are paid for performing the same or comparable jobs. But, in representing the full-time wages of a working woman against that of a full-time working man, it reflects overt discrimination as well as more nuanced gender-based factors, like the fact that women are disproportionately concentrated in the lowest-paying fields and not well-represented in higher-paying fields. Of course, 77 cents is not the only measure. But there is no doubt that the pay gap is real." Editorial Board.

ROVE: Debate on Obamacare far from over. "The administration seems to inhabit a parallel universe in which their health law is a glorious success that will carry Democrats to victory in the midterm elections. In November, the real world will deliver a harsh message about ObamaCare. The debate over this unpopular legislation is far from over." Karl Rove in The Wall Street Journal.

JOHNSON: Heroes of banking reform. "These new rules represent a vindication for Ms. Bair, Mr. Hoenig and others who have played a leadership role on this topic, including Jeremiah Norton, a member of the board at the F.D.I.C. (see his statement this week). I’m a member of the independent and nonpartisan Systemic Risk Council, founded and led by Ms. Bair, and we have also written comment letters urging higher capital requirements. While this aspect of the broader debate on financial stability is not over, people who argue in favor of even stronger capital requirements are increasingly gaining the upper hand....The consensus among officials is shifting. Perhaps not enough and definitely not fast enough, but it is definitely moving in the right direction." Simon Johnson in The New York Times.

HILTZIK: Comcast and Time Warner Cable tell Congress, 'We'll be good -- promise!' "Comcast and Time Warner Cable today brought their road show to the U.S. Senate, trying to prove that their proposed $40-billion merger will bring fabulous service to consumers, create new competition and, one would think, end the stalemate over the Crimea. No, they didn't actually make that last claim in joint testimony prepared for the Senate Judiciary Committee....Less innovation, less competition, less service. They're intent on painting this as some sort of nirvana, but we, and the Senate and the regulators at the Department of Justice and Federal Communications Commission who will be reviewing this deal, should know better." Michael Hiltzik in the Los Angeles Times.

LOWRY: The equal pay canard. "Drawn from Census Bureau data, the 77-cent figure is a comparison of the earnings of women working full time compared to men working full time. Its fatal flaw is that it accounts for none of the important factors that play into the disparity, such as hours worked. Mark Perry and Andrew Biggs of the American Enterprise Institute note that men are twice as likely to work more than 40 hours per week as women. Then, there are differences in choice of occupation, in education and in uninterrupted years of work. Once such factors are taken into account, there is about a 5 percent differential in the earnings of women and men, about which various theories are plausible, including the effect of residual discrimination. What is clear is that the wage gap is largely an artifact of the fact that women devote more time to caring for children than men do." Rich Lowry in Politico Magazine.

BOUIE: Could America become Mississippi? "There’s no guarantee that the United States becomes a majority-minority country. As I’ve written before, one possible path for some Latino and Asian immigrants -- especially those with upward mobility -- is that they 'become white' like their European immigrant fore bearers. But even if there’s no minority-majority it’s still true that the United States is becoming browner, with whites making up a declining share of the population. And if this Northwestern study is any indication, that could lead to a stronger, deeper conservatism among white Americans. The racial polarization of the 2012 election -- where the large majority of whites voted for Republicans, while the overwhelming majority of minorities voted for Democrats -- could continue for decades. That would be great for Democratic partisans excited at the prospect of winning national elections in perpetuity, but terrible for our democracy, which is still adjusting to our new multiracial reality, where minority groups are equal partners in political life. To accomplish anything -- to the meet the challenges of our present and future -- we’ll need a measure of civic solidarity, a common belief that we’re all Americans, with legitimate claims on the bounty of the country." Jamelle Bouie in Slate.

Action kid interlude: Movie compilation.

2. What we learned from the Fed minutes

Fed plays down own forecasts on rate rise. "The Federal Reserve played down forecasts by some of its own policy makers that interest rates might rise faster than they previously predicted. 'Several participants noted that the increase in the median projection overstated the shift in the projections,' according to minutes of the March 18-19 meeting of the Federal Open Market Committee released today. Some expressed concern the rate forecasts 'could be misconstrued as indicating a move by the committee to a less accommodative reaction function'...The minutes reinforce Janet Yellen’s message at her debut press conference as chair last month that the interest-rate forecasts of policy makers -- which are displayed as a series of dots on a chart -- are less important than the Fed’s post-meeting statement." Jeff Kearns and Craig Torres in Bloomberg.

Explainers:

5 things we learned from the minutes of the Fed’s March meeting. Ylan Q. Mui in The Washington Post.

6 highlights from the minutes. Jon Hilsenrath in The Wall Street Journal.

A major concern for Fed officials is low inflation. "Federal Reserve officials are growing concerned the U.S. inflation rate won't budge from low levels, the latest sign of angst among central bankers about weakness in the global economy. The Fed began 2014 hopeful that a strengthening U.S. economy would push very low inflation from 1% toward the 2% level that officials associate with healthy business activity. Three months into a year marked by unusually harsh winter weather, which appears to have damped economic growth, there is little evidence of such movement. Fed officials expressed worry about the persistence of low inflation at a policy meeting last month....They discussed at the March 18-19 meeting whether to make a more explicit commitment to keeping short-term interest rates pinned near zero until they saw inflation move up, but chose instead to take a wait-and-see approach. Low inflation is high on the agenda of global central bankers and finance ministers gathering in Washington this week for semiannual meetings of the International Monetary Fund....On its face, flat consumer prices sound like a blessing that holds down household costs. But when tepid inflation is associated with small wage gains, excess business capacity and soft global demand, as now, economists see it as a sign of broader economic malaise that restrains investment and hiring. Exceptionally slow wage and profit gains also make it harder for household and business borrowers to pay off debt." Jon Hilsenrath in The Wall Street Journal.

Stocks rally on news of the Fed's minutes. "Once again, it was the Federal Reserve to the rescue for the stock market. Major U.S. indexes rose broadly Wednesday, helped by a report out of the nation's central bank that showed Fed policymakers want to be absolutely certain the U.S. economy had recovered before starting to raise interest rates. Confident that the Fed won't be raising rates until sometime next year, investors once again embraced some of the market's more risky names. Biotechnology and technology stocks, beaten down over the past week, were among the biggest gainers. Wednesday's trading had one broad theme: risk on. Investors sold utility and telecommunications stocks -- which are usually less volatile, rich-dividend companies -- and piled into areas that typically benefit from a growing economy: materials makers, industrial companies and technology stocks." The Associated Press.

But Wall St. may not be so thrilled about this: Fed's hard line on funding. "The U.S. Federal Reserve's drive to wean Wall Street off risky funding sources is expected to bring more financial pain to the biggest U.S. banks in the coming months, analysts warned on Wednesday. They said bank regulators' release this week of tough new limits on debt funding is just a preview of other rules that may have even more bite. The eight largest U.S. banks must boost their capital levels by an estimated total of $68 billion to meet new limits on debt that regulators approved on Tuesday, a move designed to reduce banks' reliance on the type of risky financing that fueled the 2007-2009 financial crisis." Emily Stephenson and Lauren Tara LaCapra in Reuters.

Poll: Fed to hold rates until at least July 2015, slim majority of economists say. Deepti Govind in Reuters.

Other banking reads:

Energy firms to Fed: Hands off banks' commodity trading. Anna Louie Sussman in Reuters.

IMF warns on rising debt levels. Chris Giles and Robin Harding in The Financial Times.

White House reviews community bankers for Fed. Kristina Peterson and Damian Paletta in The Wall Street Journal.

Life's ponderables interlude: Why do cats knead? Science has an answer.

3. The gender-pay debate isn't going away anytime soon

Senate Republicans unanimously reject 'paycheck fairness' bill. "On Tuesday, during a news conference, House Minority Leader Nancy Pelosi wondered out loud whether Republican senators who had tweeted support for the idea of equal pay for equal work could be counted on to vote for the Paycheck Fairness Act of 2014. On Wednesday, Pelosi got her answer. Despite weeks of heavy messaging, Democrats failed to get a single GOP vote as the third attempt in recent years to pass the wage equality legislation fell six votes short. The bill, sponsored by Sen. Barbara A. Mikulski (D-Md.), had 52 sponsors, but Democrats were unable to persuade Republicans to vote for the legislation, which needed to clear a 60-vote threshold to open debate on the bill. Had it passed, the bill would have made it illegal for employers to retaliate against workers who inquire about or disclose their wages or the wages of other employees in a complaint or investigation. It also would make employers subject to civil actions by employees who feel aggrieved. As part of the bill, the Equal Employment Opportunity Commission would be required to collect pay information from employers." Wesley Lowery in The Washington Post.

Explainers:

Skeptics are wrong. The gender pay gap is very very real. Matthew Yglesias in Vox.

Fact-check: President Obama’s persistent '77-cent' claim on the wage gap gets a new Pinocchio rating. Glenn Kessler in The Washington Post.

Democrats vow to press on. "'This isn’t over. Equal pay for equal work is going to remain center stage in this year’s agenda, and we are not going to let the Republicans who blocked this bill off the hook. That could absolutely mean another vote later in the year,' said Sen. Patty Murray (D-Wash.), the chamber’s highest-ranking woman....Senate Majority Leader Harry Reid (D-Nev.) voted against the bill, which allows him to bring it up for another vote later in the year....The vote is one in a series of poll-tested measures that Senate Democrats plan to bring to the floor this year. It will be followed after the Easter recess by a bill to raise the federal minimum wage. After engaging in a debate over unemployment insurance that dragged on for months, Senate Republicans decided to reject the Paycheck Fairness Act from the start, preferring to endure a day of tough headlines rather than a drawn-out debate over gender-based pay inequality." Burgess Everett in Politico.

Democrats quickly fundraise off pay vote. "Democrats wasted little time trying to capitalize on the GOP rejection of the Paycheck Fairness Act, sending out a fundraising solicitation less than three hours after the vote." Burgess Everett in Politico.

Charts: Why Democrats really need women to turn out in November. Jaime Fuller in The Washington Post.

Could the GOP's vote hurt them in November? "Experts are still uncertain whether the vote on the bill will hurt Republicans chances for a Senate win in November. Kelly Dittmar, an assistant research professor at the Center for American Women and Politics at Rutgers University, said Republicans have been finding it a challenge to speak to issues that are most important for women and that standing in the way of the Paycheck Fairness Act was not a good impression for the party to give. Republicans, however, have an ideological defense when it comes to the specifics of the pay equity legislation. Dittmar said that Republicans can argue to voters about opposing the expanding role of government. Whether these arguments will work until November is far from certain." Laura Matthews in International Business Times.

Other economic policy reads:

GOP mulls advancing jobless-benefits bill with jobs, tax provisions. Alan K. Ota in Roll Call.

As wage debate rages, owners shuffle costs. Julie Jargon and Eric Morath in The Wall Street Journal.

Unemployment-insurance extension's problems go past House Republicans. Sarah Mimms in National Journal.

Musical performance interlude: 30 songs on the electric guitar in one minute.

4. Congress has more power in corporate accountability than it might seem

How Congress is shaking up the Comcast-Time Warner merger debate. "Comcast Corp and Time Warner Cable Inc executives sought to reassure lawmakers on Wednesday that their planned merger would not send cable TV prices skyrocketing but found a fair amount of congressional skepticism....Lawmakers can be a powerful voice on merger deliberations although they will have no formal role in deciding whether the Comcast deal gets the green light from the Justice Department, which ensures the merger complies with antitrust law, and the Federal Communications Commission, which has a broader public-interest standard. In a bid to make the merger more palatable, Comcast has pledged to divest 3 million subscribers, keeping the combined company's customer base just under 30 percent of the U.S. pay television market. The merged company would also serve between 20 percent and 40 percent of the high-speed Internet market, Comcast said in a filing on Tuesday with the FCC." Alina Selyukh and Diane Bartz in Reuters.

Background reading: Comcast, Time Warner to face lawmakers on merger plan. Diane Bartz and Alina Selyukh in Reuters.

The backlash to the Comcast-Time Warner merger is now bipartisan. "Ever since Comcast unveiled its plan to take over the nation's second biggest cable company, liberals have been pretty upset about the idea. Among the most vocal is Sen. Al Franken (D-Minn.), who argued recently in blunt messages to federal regulators that 'the Internet belongs to the people, not huge corporations.' On Tuesday, dozens of left-leaning organizations, such as Moveon.org and SumofUs, sent a letter to the Justice Department and the Federal Communications Commission expressing their displeasure.Conservatives, by contrast, have mostly kept mum or praised the looming merger. But that may be starting to change as Republicans detect a political opportunity in the proposal -- not to mention some burgeoning problems with the merger itself. The result is bipartisan objection to a buyout that critics say would be harmful to competition." Brian Fung in The Washington Post.

Under the microscope: Cable rates. "For all the complexity of the Comcast-Time Warner Cable deal, the companies’ first appearance before Congress to defend the transaction often boiled down to one factor: people’s cable bills. The companies and critics of the $45 billion deal sparred over the merger’s effect on consumers at a Senate Judiciary Committee hearing. Comcast argues the acquisition -- which would combine the nation’s top two cable companies -- would bring fiercer competition to both the video and broadband markets." Brooks Boliek in Politico.

Could Comcast crack down on conservative media? That's what some Republicans are worrying. "Sen. Mike Lee is worried that Comcast, which owns NBC-Universal, could discriminate against conservative media outlets....David Cohen, a Comcast executive vice president, insisted that even after the merger, his company will lack the market power to discriminate against any TV channels -- regardless of their political leanings." Brendan Sasso in National Journal.

That hearing was the sideshow. But it still matters. "The Department of Justice is assessing whether to object to the deal on antitrust measures. When Comcast and Time Warner Cable insist that they aren’t competing against one another and that their deal won’t reduce the number of choices for, say, broadband-seeking Clevelanders, the true audience is the federal agency deciding whether to sue. Justice Department officials will gather evidence with the power to subpoena everyone involved....The most likely course might be a middle ground, where the Justice Department wrangles with the companies over what conditions it needs not to challenge the deal outright. Comcast could give up some customers or agree not to take certain actions....But competition isn’t the only concern. The deal could be bad for America without running afoul of antitrust laws....Given that this happens outside Congress’s jurisdiction, why even bother with Wednesday’s hearings? Because senatorial skepticism registers throughout Washington, even if the agencies involved are not directly beholden to lawmakers." Joshua Brustein in Bloomberg Businessweek.

Congress' involvement in the GM recall affair matters, too. "U.S. lawmakers investigating General Motors' slow recall of 2.6 million cars are zeroing in on engineers and others who may have been aware of problems with ignition switches linked to at least 13 deaths. One month after congressional committees launched formal probes into why it took GM more than a decade to respond to ignition switch safety defects with the recall, lawmakers still do not know exactly how company engineers initially reacted to the problem or whether senior executives were made aware of it. House of Representatives Energy and Commerce Committee investigators last month spoke with GM lawyers about company documents. That panel and the Senate Commerce Science and Transportation Committee now want to hear from people with direct knowledge of the switch defect, which can unexpectedly shut off engines, disabling airbags and making steering and braking more difficult....As Congress proceeds with its investigation, it also is laying the groundwork for possible legislation later this year that would prevent future safety defects from going unaddressed. While it is not clear what kind of a measure would win Republican and Democratic support, key lawmakers already are weighing their options." Richard Cowan in Reuters.

Toyota, GM scrutiny seen ushering in new era of recalls. Craig Trudell, Yuki Hagiwara and Ma Jie in Bloomberg.

Maybe that new recall era is already here: Toyota pulls over 6 million vehicles worldwide. Sonari Glinton in NPR.

Other highlights:

Comcast wants to talk merger deal. Senators want to talk bad rep with consumers. Cecilia Kang in The Washington Post.

Golf channel: Merger would hurt us little guys. Cecilia Kang in The Washington Post.

Animals interlude: See how this cat reacts to guitar shredding.

5. How Democrats are trying to force action on immigration reform

White House may slow immigration deportations unless Congress acts. "After meeting with Homeland Security Secretary Jeh Johnson, leaders of the Congressional Hispanic Caucus voiced confidence Wednesday that if the Republican-led House fails to undertake immigration reform this year, the administration will act by executive action. Last month, President Obama promised Latino leaders that his administration would review its deportation policy and enforce laws "more humanely." Under Obama, deportations hit the 2-million mark, often separating families. Johnson arrived for the morning meeting at the Capitol with a file full of potential immigration law changes. Latino leaders also presented their own six-page proposal of reforms, including a halt to deportations of many of the estimated 11 million immigrants who would have qualified for legal status by paying fines and learning English under a bipartisan Senate bill that has stalled in the House. An hour later, the lawmakers left saying that their concerns were understood." Brian Bennett and Lisa Mascaro in the Los Angeles Times.

Pelosi's immigration road show. "Nancy Pelosi is harnessing the same level of energy to push immigration reform that she brought to her biggest legislative achievement when she ran the House: the health care overhaul. The California Democrat does not wield the speaker’s gavel anymore, but she’s hitting the road to bring attention to immigration, which she calls her top legislative priority. In recent weeks, she’s pitched an overhaul to audiences from Miami to Los Angeles to Laredo, Texas. The Pelosi immigration road show is unfolding as the issue remains on the back burner in the Republican-controlled House. Democrats on and off Capitol Hill are frustrated that legislation has stalled and divided over the best way forward. But in an interview this week from her Capitol office, Pelosi said Democrats need to stay focused on a policy goal that she sees as more important than regaining control of the House." Seung Min Kim in Politico.

Why reform advocates aren't giving up on Congress yet. Elise Foley in The Huffington Post.

Other legal reads:

Obama to mark U.S. civil rights law that paved way for presidency. Steve Holland and Thomas Ferraro in Reuters.

Obama pushes voting rights in Texas. Edward-Isaac Dovere in Politico.

After wars abroad, Obama confronts new one at home. Scott Wilson in The Washington Post.

Adorable interlude: A 2-year-old's first experience.

Wonkblog roundup

The big environmental problem highlighted by the search for Flight 370. Jia Lynn Yang.

What we know about how poor neighborhoods become wealthier. Emily Badger.

The top 10 Medicare billers explain why they charged $121M in one year. Jason Millman.

5 things we learned from the minutes of the Fed’s March meeting. Ylan Q. Mui.

Start saving now: Day care costs more than college in 31 states. Christopher Ingraham.

Everything you need to know about today’s unprecedented Medicare pricing data dump. Jason Millman.

Want to see how problematic Medicare pricing is? Look to ophthalmology. Max Ehrenfreund.

Why big banks have to raise $68 billion by 2018. Danielle Douglas.

Everyday low prices? Wal-Mart’s competitors can play that game too. Lydia DePillis.

Et Cetera

Why Republicans love Obama's post office plan. Devin Leonard in Bloomberg Businessweek.

Fannie, Freddie shareholders lobby against U.S. housing reform bill. Margaret Chadbourn in Reuters.

More college students battle hunger as education and living costs rise. Tara Bahrampour in The Washington Post.

Amid Ukraine discord, U.S. resumes nuclear-security work in Russia. Douglas P. Guarino in Global Security Newswire.

Long read: Dueling dilemmas for national security reform. Darren Samuelsohn and Jake Sherman in Politico.

GMO labeling bill would trump states. Jenny Hopkinson in Politico.

Got tips, additions, or comments? E-mail us.

Wonkbook is produced with help from Michelle Williams.