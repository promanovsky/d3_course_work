To most Canadians, Victoria Day marked the unofficial start of summer. Sure, the calendar might not agree and the leaves on the trees might be budding a bit late this year, but for most of us, summer is here -- and it couldn’t have come soon enough.

But while you’re dusting off the coolers and the sunscreen, it might be a good idea to make sure you’re not also dragging out some of these warmer-weather myths because we’re happy to report that science has now firmly debunked all of them:

1. Always wait a half-hour after eating before swimming

This one has been around for generations and it's not even clear anymore how it got started.

Yes, it's true that you can get a stomach cramp if you try to exercise vigorously after eating, but it won't be a cramp that would impair your ability to swim or to keep your head above water. Chances are if you've eaten a big meal, you're probably not interested in going for a swim anyway.

It's also not clear where the 30-minutes wait window came from either, since it typically takes a lot longer -- three to four hours -- for the stomach to empty.

2. A base tan will help prevent sunburns

Let’s remember why our skin tans when exposed to the sun: the solar radiation causes DNA damage to the skin and the body naturally tries to repair the damage and protect the skin by creating the melanin that marks a tan.

But contrary to popular belief, a tan does not do a really great job of protecting your skin from skin damage. In fact, the extra melanin the sun produces in your skin offers a sun protection factor (SPF) of only about 2 to 4 -- nowhere near the recommended SPF of 15 that helps protect the skin from sunburn and cancer. And in the meantime, all that tanning has just hiked your risk for skin cancer.

So good idea? Perhaps not.

3. If it’s not raining, you don’t need to worry about lightning

Oddly enough, most injuries and deaths from lightning occur either before the rain starts, or after the rain has stopped, Environment Canada says. Lightning can strike more than five kilometres from the centre of the thunderstorm, far outside the thunderstorm cloud. As well, some thunderstorms produce no rain at all because the droplets evaporate before they hit the ground, particularly in the dry West.

So if you can hear thunder, you are within striking distance of lightning. Lightning often strikes several kilometres from the centre of the thunderstorm, far outside the rain cloud.

4. Dark-skinned people don't have to worry about skin cancer

Although dark skin does not burn as easily as fair skin, and it’s true that people with fair skin get skin cancer more often, everyone is at risk for skin cancer, even people who don't normally burn.

In fact, the myth that non-Caucasian people don't have to worry about skin cancer is likely one of the reasons people with dark skin are diagnosed with skin cancer at later stages: they don't realize what the cancerous area is until the cancer is well advanced.

People with dark skin need to follow the same rules as those more likely to burn. Avoid being in the sun between 10 a.m. and 2 p.m., wear a hat and long sleeves, and use a sunscreen with an SPF of at least 15

5. Tilt your head back to stop a nosebleed

While this is still what you see most people do to stop the flow of blood, it’s actually precisely what you don’t want to do. Tilting the head back can cause the blood to flow into the back of the throat and then into the stomach, which is not where you want blood to go.

Instead, we should do the opposite: sit forward, pinch the nose and hold it for at least 10 minutes. If it doesn’t stop after 10 minutes, hold it another 10 minutes. If it doesn’t stop after 20 minutes, or if you suspect the nose is broken, get to a doctor.

6. When barbecuing burgers, colour is the best way to tell if they're done

If you’re used to grilling burgers until you no longer see pink, you may not be cooking them enough. Meat can turn brown before all the E. coli and other bacteria are killed.

The best way to check for doneness is to use a digital thermometer to take the temperature in the thickest part of the meat and ensure it’s reached 160ºF (71 C). Even burgers that are still pink inside can be safe to eat if -- and only if -- they’ve reached this temperature. So toss out the colour test and switch it for a good meat thermometer.

7. When driving, open windows are more fuel-efficient than the A/C

This one has no doubt been the topic of heated debate -- yes, heated -- in many a family car trip. So about a decade ago, the Society of Automotive Engineers, which sets standards for various engineering industries, put this question to the ultimate test.

They tested a sedan and an SUV, at speeds of 50 km/h, 80 km/h and 110 km/h to see whether the wind drag of opening the windows burned up more gas than the air conditioning system.

What they found was that the ambient heat didn’t matter much, nor did the size of the engine or the vehicle. What it really came down to was speed.

When driving at a low speed, say in the city, it's more efficient to leave windows open, they found. When the speed picks up past 65 km/h though, the wind drag effect really kicks in and you end up using less gas by switching on the air conditioning.