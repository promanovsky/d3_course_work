U.S. mortgage rates for 30-year loans fell for a third week, reducing borrowing costs as housing demand regains strength.

The average rate for a 30-year fixed mortgage was 4.12 percent this week, down from 4.14 percent, Freddie Mac said in a statement Thursday. The average 15-year rate held at 3.22 percent, according to the McLean, Virginia-based mortgage-finance company.

Home purchases are accelerating after a slow start to the year. Contracts to buy previously owned U.S. houses climbed 6.1 percent in May, the biggest monthly gain since April 2010, the National Association of Realtors said this week. Strict credit standards are holding back some buyers who may want to take advantage of the drop in loan costs from the beginning of 2014.

“Housing is on a slow recovery path with credit conditions being crucial to the outlook,” Neil Dutta, head of U.S. economics at Renaissance Macro Research LLC in Manhattan, said in a July 1 note to clients. “Real estate lending is picking up but far from booming.”

Mortgage applications fell for a fifth time in six weeks, the Mortgage Bankers Association said Wednesday. The group’s purchase measure decreased 0.7 percent, while the refinancing gauge advanced 0.1 percent.

The average rate for a 30-year loan was 4.53 percent in early January, according to Freddie Mac.

Job growth is helping to bolster housing demand. U.S. employers added 288,000 workers in June and the jobless rate fell to 6.1 percent, almost a 6-year low, Labor Department figures showed Thursday.