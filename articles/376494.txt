MicroCHIPS

If you could have safe, effective, long-term birth control that you didn't have to think about, would you jump at the chance? That's what's being proposed by a company called MicroCHIPS of Lexington Massachusetts -- in the form of a chip to be implanted under the skin.

The chip, just 20 x 20 x 7 millimetres, is designed to last up to 16 years -- about half of a woman's reproductive lifespan -- delivering a daily dose of 30mcg of levonorgestrel, used in several hormonal contraceptives and emergency contraceptives. In the event a couple wants to conceive, the woman can use a remote control to turn the chip off, and then back on again when she needs to.

The implications of the technology go beyond contraceptives. Inside the chip is a reservoir array which contains and protects the hormone. In these reservoirs, however, any drug could be placed, to be released on demand, or according to a pre-programmed schedule.

"These arrays are designed for compatibility with pre-programmed microprocessors, wireless telemetry, or sensor feedback loops to provide active control," the MicroCHIPS web page reads. "Individual device reservoirs can be opened on demand or on a predetermined schedule to precisely control drug release or sensor activation."

The chip releases the contents of the reservoir when a minute electric current from a small internal battery is passed through the hermetic titanium and platinum seal, melting it and allowing the dose to release into the body.

So far, the chips have been tested in a human clinical trial, delivering osteoporosis medication to post-menopausal women over a one-month period, demonstrating that the technology works, producing no adverse immune reaction, and demonstrating the durability of the chip. The device was implanted using a local anaesthetic, and the procedure took no more than 30 minutes.

There are still some logistics to work out -- such as encrypting the chips to keep wireless data secure -- but the concept has been proven to work as desired.

Now, as part of the Bill & Miranda Gates Foundation Family Planning program, the team, led by MIT's Robert Langer, is adapting it for contraceptives, and hopes to have FDA approval for pre-clinical trials next year, with a view to have it available on the market by 2018.