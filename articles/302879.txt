Apple is cutting the price of its low-end iMac by $200, but buyers will have to deal with a slower processor and smaller hard drive to save the money.

Wednesday, Apple announced it would introduce a 21.5-inch iMac desktop computer that will sell for $1,099. The company said the model will be available on Apple's website, at Apple stores and at select authorized retailers.

The cheapest iMac available before this model was $1,299. That model is still available.

For $200 less than the $1,299 model, buyers will receive an entry-level iMac with a 21.5-inch screen. The hard drive will be reduced from 1 terrabyte to 500 gigabytes, which is exactly half the amount of storage. The processor speed will also be reduced by about half, from 2.7 GHz to 1.4 GHz on the cheaper model. However, the processor can be boosted to up to 2.7 GHz.

The only other difference between the $1,299 iMac and the $1,099 iMac released today is the graphics chip. The $1,299 model has Intel Iris Pro Graphics and the $1,099 model has Intel HD Graphics 5000.

No new late-breaking Apple products have been announced since the iPad was introduced in 2010. Apple CEO Tim Cook has hinted at new and exciting products and it was expected that something would be introduced at the recent Worldwide Developers Conference earlier this month. Instead, Apple just announced the new iOS 8 software and the new Mac operating system OS X Yosemite and no new hardware.

Apple has also added refreshments to its laptops. The MacBook Air has dropped its price by $100 while adding a small boost to its speed.

The next step for Apple could be to release a Retina Display iMac. No release date has been set for those iMacs, but it makes sense for Apple.

Apple computers have always been high ticket items. The cheapest laptop for Apple is the MacBook Air's 11-inch model which starts at $899. The cheapest desktop Apple offers is the 21.5-inch iMac, which now sells for $1099 for the entry-level model.

What do you think about the new entry-level iMac? Would you sacrifice the smaller hard drive and slower processor for the $200 savings? Let us know in the comments section below.