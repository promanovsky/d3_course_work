The Food and Drug Administration said Thursday that it plans to begin regulating certain diagnostic tests developed by laboratories, which have proliferated and grown more sophisticated in recent years, spurring increased concerns about their safety and reliability.

Just as new drugs must be proved safe and effective before reaching consumers, the devices used to diagnose complex medical conditions also must work properly to avoid subjecting patients to harmful or unnecessary treatments, FDA Commissioner Margaret A. Hamburg said.

“We must make sure that the information is accurate, consistent and reliable,” she said.

For decades, the FDA had chosen not to actively regulate laboratory-developed tests, or LDTs, because they tended to be simple devices used to diagnose rare or “low-risk” diseases. In recent years, however, the tests have become more widely used and more specialized, able to diagnose specific forms of cancer and other diseases with complex genetic data.

Government regulators say the evolution makes it more critical than ever to ensure that the tests are safe and trustworthy and that patients who rely on them aren’t misdiagnosed. Several incidents in recent years have underscored those concerns, including instances in which the reliability of LDTs for cervical cancer, Lyme disease and whooping cough have been questioned.

Until now, the FDA has relied on a two-tiered approach for regulating diagnostic tests. The agency has long reviewed devices made by traditional manufacturers before they hit the market to ensure their safety and accuracy. But in the past, tests developed and sold by a single laboratory generally have not been subject to “pre-market” review. As diagnostic testing has become a staple of medical decision-making, many laboratories that once produced tests only for use by local doctors and patients morphed into marketing their diagnostics to much broader populations.

Under the new proposal, the FDA said, the safety and efficacy of only the most “high-risk” LDTs would have to be proved before going to market. Eventually, they would have to adhere to the same rules as tests produced by traditional manufacturers, including abiding by good manufacturing practices and reporting any adverse events associated with their tests.

Any new requirements also would be phased in over several years, and lower-risk diagnostics, including those for rare diseases or for conditions for which no other test currently exists, would be exempt from the expanded oversight, FDA officials said.

Thursday’s action comes years after the FDA first intended to regulate LDTs. The agency convened a meeting to hear from public health and industry officials in 2010 and later drafted proposals, which were quickly mired in politics and bureaucracy.

“For years this draft guidance has languished at OMB causing continued unpredictability and uncertainty for industry, clinicians, patients and the general public,” five Democratic senators wrote in early July to the top official at the Office of Management and Budget, which must approve regulations proposed by federal agencies.

At the same time, the decision about whether — and how — to regulate LDTs remains far from unanimous. The American ­Clinical Laboratory Association (ACLA), an industry group, has repeatedly argued that LDTs already face stringent oversight under a federal law that requires monitoring of the reliability of the tests.

“FDA intervention would add another layer of unnecessary and duplicative regulation to the LDT approval process, impose a stranglehold on diagnostic innovation and reduce patient access to the latest groundbreaking diagnostic advancements,” ACLA President Alan Mertz said in a recent statement.

Nearly two dozen university lab directors from around the country made much the same case in a recent letter to the OMB, arguing that LDTs are not technically medical devices and that allowing the FDA to regulate them would slow the development of critical testing and ultimately do a disservice to patients.

“The ability of laboratories to develop custom diagnostic tests has been critical to the growth of personalized medicine and keeping pace with the changing face of disease to best serve patients and clinicians,” they wrote. “FDA regulation of laboratory developed tests would stifle the medical innovation occurring in academic medical centers today, and interfere with our ability to care for patients.”

FDA officials said Thursday that despite the move to regulate LDTs, they support the development of new diagnostic tests, which play a critical role in an age of increasingly personalized, targeted therapies.

“We understand the value of, and the need for, these types of tests,” said Jeffrey Shuren, director of the agency’s Center for Devices and Radiological Health. “We believe the proposed framework is flexible and designed to support future innovation. At the same time, we have a responsibility to ensure that the products on the market are reasonably safe and effective.”

On Thursday, the FDA also finalized guidance for the development of “companion diagnostics,” which are tests created in tandem with specific new medicines or other therapies, particularly for certain kinds of gene-based cancers. To date, the agency has approved 18 such devices, and others remain in the pipeline.