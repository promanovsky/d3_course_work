Khloe Kardashian wants only the best things for her sister Kim’s wedding.

The Kardashian-Jenner family landed in Paris for the upcoming “fairytale wedding” of Kim Kardashian and Kanye West, and they wasted no time going to work doing what they do best — shopping. The family was spotted Wednesday in the ritzy shopping center of the city, the Champs-Elysees.

The gang stopped first at the Montaigne Market, where Khloe Kardashian picked up an $1,800 Valentino clutch. She later picked up a pair of pants for younger sister Kylie after their mother Kris declined to buy them for the family’s youngest girl.

Just two days before, Kim and Kanye stopped at the famous Montaigne Market, where Kanye reportedly helped his future wife with some fashion choices.

“Kanye and Kim were very nice and cool, they were affectionate together and Kanye was picking out clothes for Kim do try on like he was her personal stylist,” an observer told Us Weekly, adding, “She was trying on items that he picked out for her — they agreed on most items, but I think there were a few Kim didn’t like.”

The wedding is a highly anticipated affair, although the public won’t see much of it. The couple has reportedly tried to keep the nuptials a small, intimate affair.

“There are only 100 guests invited. It is only their closest friends and family,” an insider told E! News. “The couple had to be very selective and cut it off at no work friends no matter how close or cherished.”

There will still be some high-profile people expected to be in attendance, including Demi Lovato, Apple co-founder Steve Wozniak, and the power couple of Beyonce and Jay Z (who is serving as Kanye’s best man).

Designer Rachel Roy, who reportedly played a role in the fight between Solange Knowles and Jay Z, was one of the handful of people present when Kanye popped the question.

“It was a magical night filled with people that love Kim and Kanye dearly,” she told E! News. “Every detail was planned out by Kanye with the intent of making Kim know how loved, adored and cherished she is, from the music to the design of the Lorraine Schwartz ring, love was in every detail.”

Khloe Kardashian will get a chance to debut her pricey purse on Friday, when Kim and Kanye’s wedding is kicked off in Paris and Florence, Italy.