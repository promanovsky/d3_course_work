Parrot the makers of the awesome range of Parrot AR Drone quadcopters has this week unveiled their new Bebop Parrot AR Drone and Parrot Skycontroller that can be connected directly to FPV (First Person View) glasses such as the Oculus Rift virtual reality headset.

Parrot will offer the Skycontroller as an option for Bebop AR Drone which is equipped with an amplified Wi-Fi radio and 4 antennas extending the Wi-Fi range up to 2 kms.

Once users have connected the Parrot Skycontroller to a pair of virtual reality glasses, users can then leaned their head to position the camera of the Bebop Parrot AR Drone. Which is a 14 megapixel camera with a fisheye lens enabling you to capture silky-smooth video and can even pan and tilt while the Parrot AR Drone hovers on the spot.

The news of the new Parrot Skycontroller and Bebop AR Drone have been unveiled at this weeks AUVSI 2014 show. Unfortunately no information on pricing or worldwide availability has been released as yet by Parrot, but more information is expected to be released about the new Bebop Parrot AR Drone tomorrow, and as always we will keep you updated with details and specifications as they are released by Parrot.

Source: Parrot

Latest Geeky Gadgets Deals

Some of our articles include affiliate links. If you buy something through one of these links, Geeky Gadgets may earn an affiliate commission. Learn more