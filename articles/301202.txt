Reynolds American Inc. plans to begin national distribution of its Vuse brand electronic cigarette next week.

The owner of the nation's second-biggest tobacco company said Tuesday that retail outlets in all 50 states will be carrying Vuse beginning June 23. More stores will be added throughout the rest of the year.

The Winston-Salem, North Carolina-based company launched Vuse in Colorado last summer as part of an industry-wide push to diversify beyond the traditional cigarette business. It expanded into Utah earlier this year. Reynolds says Vuse quickly became the top-selling brand in both states with high levels of repeat purchases.

The company says its rechargeable e-cigarette assembled in the U.S. has technology that monitors and adjusts heat and power to deliver the "perfect puff."