Box Office: 'Transformers 4' Leads Top 10 Global Chart

The Michael Bay film rakes in $301.3 million, while "The Fault in Our Stars" and "22 Jump Street" approach the $200 million mark.

Paramount's Transformers: Age of Extinction did massive business at the global box office, scoring the top opening of the year with $100 million domestically and $201.3 million overseas from 37 markets for a total $301.3 million.

The fourth installment in the action franchise was especially huge in Asia, including a best-ever, $90 million debut in China (it won't open in most of Europe or Latin America until after the World Cup). Still, that didn't stop new Chinese romantic comedy The Break Up Guru from opening to a stellar $20.5 million in China (it also had a limited launch in the U.S.).

BOX OFFICE 'Transformers 4' Hits $100 Million for $301.3 Million Worldwide Debut

Elsewhere, Fox 2000's $12 million YA film adaptation The Fault in Our Stars and Channing Tatum-Jonah Hill comedy 22 Jump Street approached the $200 million mark worldwide, while Clint Eastwood's Jersey Boys continued to struggle in its second weekend, both in North America and overseas.

Below are the top 10 films at the global box office, according to Rentrak.

1. Transformers: Age of Extinction (Paramount, 38 territories, week 1)

Global weekend: $301.3 million

International: $201.3 million

Domestic: $100 million

2. How to Train Your Dragon 2 (Fox/DreamWorks Animation, 53 territories, week 3)

Global weekend: $31 million (total: $229.2 million)

International: $17.9 million ($107.4 million)

Domestic: $13.1 million ($121.8 million)

3. 22 Jump Street (Sony, 34 territories, week 3)

Global weekend: $25 million (total: $194 million)

International: $9.6 million ($54.2 million)

Domestic: $15.4 million ($139.8 million)

4. Maleficent (Disney, 55 territories, week 5)

Global weekend: $24.2 million (total: $585.6 million)

International: $16 million ($383.7 million)

Domestic: $8.2 million ($201.9 million)

5. The Break Up Guru (Beijing Enlight/China Lion, 2 territories, week 1)

Global weekend: $20.6 million

International: $20.5 million

Domestic: $67,000

6. The Fault in Our Stars (Fox, 53 territories, week 4)

Global weekend: $17.8 million (total: $195.3 million)

International: $13 million ($85.8 million)

Domestic: $4.8 million ($109.5 million)

7. Edge of Tomorrow (Warner Bros., 65 territories, week 4)

Global weekend: $12.1 million (total: $318.7 million)

International: $6.9 million ($234.5 million)

Domestic: $5.2 million ($84.2 million)

8. Think Like a Man Too (Sony, 1 territory, week 2)

Global weekend: $10.4 million (total: $48.2 million)

Domestic: $10.4 million ($48.2 million)

9. X-Men: Days of Future Past (Fox, 40 territories, week 6)

Global weekend: $9.5 million (total: $714.7 million)

International: $6.2 million ($491.3 million)

Domestic: $3.3 million ($223.4 million)

10. Jersey Boys (Warner Bros., 12 territories, week 12

Global weekend: $8.7 million (total: $31 million)

International: $1.1 million ($3.7 million)

Domestic: $7.6 million ($27.3 million)