Are you using your baby gate correctly? There was a sharp increase in the number of baby gate-related injuries from 1990 to 2010, but you can help reduce your child’s risk of injury.

Photo credit: Allen Donikowski/Moment/Getty Images

If you think your baby gate is keeping your little one safe, you may have an unhappy surprise for both of you in your future. A recent study reveals an alarming statistic — baby gate-related injuries tripled from 1990 to 2010. What is to account for the massive increase, and how can you be sure your baby gate isn’t a danger to your child?

Increase in baby gate-related injuries

Researchers gathered data from the National Electronic Injury Surveillance System, which tracks juvenile injuries from a selection of hospitals. They found that the majority of kids with baby gate-related injuries were older than 2, and 61 percent were male. Bumps and bruises were the most common injuries reported, and 16 percent suffered head injuries, such as concussions. Most of these children were not admitted to the hospital.

How do these injuries occur?

They found that most of these injuries happened when the children pushed through the gates, and some of the gates had sharp edges that caused injuries. Also, some mishaps were the result of a pressure-mounted baby gate that was installed at the top of a flight of stairs, which is extremely dangerous.

Baby gate installation 101

Always make sure your baby gate is installed correctly by following the enclosed instructions. If you have a hand-me-down or secondhand gate, check to see if you can find installation instructions on the internet or phone the manufacturer (and don’t forget to see if it’s been the subject of a recall). It’s very important to never install a pressure-mounted baby gate at the top of a flight of stairs. And if a gate is not closed properly or is mounted on a set of uneven walls (in a hallway, for example) it can be pushed through by a strong-willed toddler. Also, even though you have peace of mind when a gate is installed, you should always keep your child in sight, because some children are clever enough to climb them — and topple over once they’ve reached the top.

More on child safety

Protect your family from baby monitor hackers

5 Apps that help keep your kids safe

Is swaddling safe?