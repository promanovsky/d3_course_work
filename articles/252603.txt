Sony’s next generation console, the PlayStation 4, has performed spectacularly well in the market up till now beating its rival, Microsoft’s Xbox One, by raking in the most unit sales. The console’s owners are required to subscribe to PlayStation Plus if they want to play online and the company’s CEO has revealed that “approximately half” of all PS4 owners subscribe to the service. CEO Kazuo Hirai revealed this information at a recent meeting with investors.

Advertising

Sony’s last update about PS4 unit sales was that it has sold more than seven million consoles. That’s sales to end users, not shipments to retailers. This means that approximately 3.5 million PS4 owners have subscribed to the paid PS Plus service.

The cheapest subscription plan is the 12-month option which costs $49.99 for the entire year and comes down to just under $5/month. There are also 3-month and 1-month membership options that cost $17.99 and $9.99 respectively. Even though Sony hasn’t disclosed exact figures that it has made off of PS Plus subscriptions, it would be safe to assume that its up in the millions.

Sony has already said that the PS4 console is now making a profit based on hardware sales alone. The company expects that in the long run it will be able to exceed the profits made by the very popular PlayStation 2 console, no doubt aided by the strong interest in digital services as well.

Filed in . Read more about PlayStation 4, Playstation Plus and Sony.