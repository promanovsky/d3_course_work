On The Bachelorette's After the Final Rose special, emotional train wreck Nick Viall shocked fans with a comment about Andi Dorfman that he now regrets.

Revealing they "made love," in the Fantasy Suite, Nick asked Andi why they had sex if she wasn't in love with him, like she clearly is with Josh Murray.

A shocked Andi called his comments "below the belt" and bristled, telling Nick that everything she told him and felt for him was entirely genuine.

Looking back on their emotional confrontation, Nick admitted to reporters Tuesday that he wishes he would've handled the conversation differently.

"I definitely wish I had that opportunity to have that conversation with her in private. I did not go into last night expecting to ask her that," Viall said.

"I was just so caught off guard by her demeanor."

"It wasn't so much about the sex but the level of intimacy that night and I made it very clear where I was in my feelings for her and what something like that meant to me."

"I was just so caught off guard, it sort of just came out. If I had any regrets, I didn't want to humiliate her or make her feel that way. I feel very sorry if she did feel that way."

The After the Final Rose special was the first time Nick has seen Andi since she had sent him home broken-hearted on The Bachelorette finale.

Andi revealed that she did read the letter Nick Viall wrote to Dorfman, which the producers decided to post in its entirety on the show's Facebook page.

"I was in no way ashamed of what was in the letter," Nick said of that decision. "I knew by giving it to her it was a possibility and I accepted that."

Even though he is still not over Andi - as evidenced by the letter and their awkward After the Final Rose reunion - Viall says he is ready to move on.

"I think that [ATFR] it was a lot of closure for me, just seeing her demeanor with me, I don't know where it came from. I think for me it was a lot of closure."

"It doesn't eliminate any heartbreak, but it was good for me," Nick said. "I would say I have accepted her decision and am willing to move forward."

Andi Dorfman "will always have a place in my heart and she is special person and what I had was special to me. I'm excited for things to come."