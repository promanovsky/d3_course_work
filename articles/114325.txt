Leonardo DiCaprio has reportedly showed off some... shall we say... unique dance moves at the Coachella music festival.

The Hollywood actor is thought to have been caught on camera throwing some shapes to MGMT at the event in California at the weekend.

This content is imported from YouTube. You may be able to find the same content in another format, or you may be able to find more information, at their web site.

An amazed festival-goer filmed what is assumed to be the actor jumping in the air and kicking his legs with a friend.

While it has not been officially confirmed if the man in the video is DiCaprio, a Twitter photo shows the actor wearing the same outfit earlier in the day.

DiCaprio attended the three-day music festival with 21-year-old model girlfriend Toni Garrn.

This content is created and maintained by a third party, and imported onto this page to help users provide their email addresses. You may be able to find more information about this and similar content at piano.io