Once self-driving cars are everywhere, Uber thinks they’ll make up its entire fleet.

Speaking at Re/code’s Code conference, Uber CEO Travis Kalanick said he loves the idea of autonomous vehicles, like the one Google unveiled on Tuesday, and would happily replace his human drivers with a self-driving fleet.

“The reason Uber could be expensive is you’re paying for the other dude in the car,” Kalanick said, according to Business Insider. “When there is no other dude in the car, the cost of taking an Uber anywhere is cheaper. Even on a road trip.”

Kalanick’s comments come as Uber is engaged in a PR push over driver pay. Faced with mounting protests over workers’ wages, Uber recently said its drivers can make up to $90,000 annually, although that number doesn’t factor in costs associated with being a professional driver.

Kalanick also said that, over time, the cost of a ride would be so low that even the idea of car ownership itself might just “go away.”

That seems a bit extreme, for at least as long as we’ve got cars that let you take the wheel, and people who enjoy the act of driving. And either way, owning a car would probably still make economic sense if you drove it enough. But a company with a huge fleet of reasonably-priced, self-driving cars could do pretty well in this hypothetical future, so it’s no surprise that Kalanick and Uber are on board.

The Leadership Brief. Conversations with the most influential leaders in business and tech. Please enter a valid email address. * The request timed out and you did not successfully sign up. Please attempt to sign up again. Sign Up Now An unexpected error has occurred with your sign up. Please try again later. Check the box if you do not wish to receive promotional offers via email from TIME. You can unsubscribe at any time. By signing up you are agreeing to our Terms of Service and Privacy Policy . This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply. Thank you! For your security, we've sent a confirmation email to the address you entered. Click the link to confirm your subscription and begin receiving our newsletters. If you don't get the confirmation within 10 minutes, please check your spam folder.

Contact us at letters@time.com.