Clarissa Serna vs Jeremy Briggs: Who Won ‘The Voice’ Head-to-Head? [Video]

Jeremy Briggs and Clarissa Serna faced off in a battle round on NBC’s “The Voice” on Tuesday night.

Once the head of the team decides who will stay, the other mentors-judges will have the chance to steal a singer who was not chosen to give them another chance to compete.

The pair, both on Team Shakira, sang “Cold as Ice.”

Here were the judge’s reactions:

Adam: It was a strong ending. He would choose Clarissa.

Blake: Jeremy hit the same note as Clarissa and it was a tall order. He would choose Clarissa.

Usher: They both did well, but he would choose Jeremy.

Shakira: They complimented each other and sang so well together.

But who won? Check out the video below.