In a first for the advertising industry, marketers are spending more to promote their brands online than on broadcast television, driven mostly by the significant increase of ads appearing on digital platforms.

Annual ad buys on mobile devices soared to $7.1 billion last year, up from $3.4 billion in 2012, according to a new study by the Interactive Advertising Bureau and PricewaterhouseCoopers. The companies lump search, display and video ads together in the mobile category, which accounted for 17% of the Internet ad market.

Broadcast and cable advertising expenditures combined still far exceed online ad buys, however. Combined, they were worth $74.5 billion.

Overall, online advertising revenue climbed 17% to a record $42.8 billion in the U.S. in 2013; by comparison, revenue generated from advertising on broadcast TV came in at $40.1 billion last year.

According to IAB, Google, and Twitter are driving much of the online gains as those companies, along with Yahoo and YouTube, increasingly compete for a bigger share of ad campaigns launched by major marketers that target younger consumers.

Broken out, digital video generated $3 billion in ad revenue, while search, the largest online category, brought in $18.4 billion, to account for 43%.

More than 600 media and tech companies that sell online advertising in the U.S. are members of the IAB.

The results bode well for content distributors and platforms looking to court Madison Avenue during the NewFronts, the digital version of the more traditional Upfront ad sales market in New York City during which broadcasters attempt to woo marketers with their programming slates.

The biggest buyers of online ads in 2013 were retailers, with 21%, followed by financial services and the automotive industry.