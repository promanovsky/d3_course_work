Nokia Corp. (NOK) said it has completed the sale of substantially all of its Devices & Services to Microsoft.

The transaction, which also includes an agreement to license patents to Microsoft, was originally announced on September 3, 2013.

Nokia currently expects the total transaction price to be slightly higher than the earlier-announced transaction price of 5.44 billion euros, after the final adjustments are made based on the verified closing balance sheet.

Nokia and Microsoft made some adjustments to the scope of the assets originally planned to transfer. These adjustments included Nokia's manufacturing facilities in Chennai in India and Masan in the Republic of Korea not being transferred to Microsoft.

These adjustments have no impact on the material deal terms of the transaction and Nokia will be materially compensated for any retained liabilities.

For comments and feedback contact: editorial@rttnews.com

Business News