Oscar Pistorius murder trial set to resume after month-long break for health experts to evaluate Olympic sprinter's mental state when he shot Reeva Steenkamp



The South African was ordered to undergo evaluation for mental illness

Mental health experts observed the runner for 30 days to see if he has an anxiety disorder

The murder trial of Paralympian Oscar Pistorius resumes on Monday after mental health experts spent a month evaluating the athlete to determine if he has an anxiety disorder that could have influenced his actions on the night he killed girlfriend Reeva Steenkamp.



Judge Thokozile Masipa is expected to receive the conclusions of a panel of one psychologist and three psychiatrists who were instructed to assess whether the double-amputee runner was capable of understanding the wrongfulness of his act when he shot Steenkamp through a closed toilet door in his home on Valentine's Day last year.

Arrived: Oscar Pistorius checking in to a government psychiatric hospital on May 26, 2014

Calling: The South African was spotted on his mobile phone as he was driven through the hospital gates

Waiting game: There was a line of media outlets camped outside for a glimpse of the Paralympian VIDEO Psychologist's view on Pistorius trial

The assessments of a panel at a state psychiatric hospital could determine whether he should be held criminally responsible and affect the judge's deliberations on a verdict or, in the event of a conviction, the severity of the sentence, according to legal analysts.



The evaluation came after a psychiatrist, Dr. Merryll Vorster, testified for the defence that Pistorius, who has said he feels vulnerable because of his disability and long-held worry about crime, had an anxiety disorder that could have contributed to the killing in the early hours of February 14, 2013.



He testified that he opened fire after mistakenly thinking there was a dangerous intruder in the toilet.



On trial: Pistorius looks on at Pretoria High Court while listening to evidence from the prosecution

Prosecutor Gerrie Nel has alleged Pistorius, 27, killed the 29-year-old model after an argument, and has portrayed the Olympic athlete as a hothead with a love of guns and an inflated sense of entitlement.



But he requested an independent inquiry into Pistorius' state of mind, based on concern the defence would argue Pistorius was not guilty because of mental illness.



Pistorius' defence team could get a boost if the hospital evaluation roughly aligns with the conclusions of Vorster, the defence witness, said a legal expert observing the trial.



Kelly Phelps, a senior lecturer in the public law department at the University of Cape Town, said a diagnosis that Pistorius has an anxiety disorder could add weight to his account and compel the judge to consider the question: ‘Is it more likely that he is telling the truth about what occurred on that night?’

Distracted: The Paralympian has been caught looking at his phone during court proceedings

Even if the judge rules that Pistorius is guilty despite any disorder that he is suffering, Phelps said, the diagnosis could be a mitigating factor when he is sentenced.



‘That is the area of law that is often referred to as diminished responsibility,’ she said.



Other possible conclusions in the psychiatric evaluation are that Pistorius is not suffering from any anxiety disorder, which could undermine his defence.



Alternatively, it might be found that he was incapable of distinguishing between right and wrong or acting in accordance with that understanding, which could lead to a verdict of not guilty because of mental illness and referral to state psychiatric care.



Trial: Pistorius admits to shooting Reeva Steenkamp on Valentine's Day 2013 but denies premeditated murder

Pistorius faces 25 years to life in prison if found guilty of premeditated murder, and could also face years in prison if convicted of murder without premeditation or negligent killing. He is free on bail.



Once Judge Masipa receives the conclusions from the mental health experts, the defence will be in a position to call its few remaining witnesses, prior to closing arguments and Masipa's deliberation on a verdict.



If, however, the experts who observed Pistorius are not unanimous in their conclusions, the judge can call them to the stand to clarify their findings.



Pistorius was evaluated as an outpatient at Weskoppies Psychiatric Hospital in Pretoria, the South African capital.



He has been staying at his uncle’s home.



