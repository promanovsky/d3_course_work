Steve Perry hasn’t performed publicly since leaving Journey 19 years ago — until now.

Don’t stop believin’ in the power of a comeback: Former Journey front man Steve Perry has finally taken up the mic again after 19 years of radio silence.

Related story Whitney Houston's Hologram Concert Is Coming to North America, & We're Not Sure How to Feel

The ultimate rock voice left Journey and stopped performing altogether after a series of serious health problems, but in a surprise move over the weekend hopped onstage with indie band the Eels during an encore at the band’s show in St. Paul, Minnesota.

Watch Steve Perry perform with the Eels

www.youtube.com/embed/Ip4F9fGHZ00?rel=0

The crowd went absolutely nuts!

Perry’s split with Journey was deemed to be contentious, but bandmate Neal Schon said the rumors aren’t exactly accurate and he misses his old friend, who has pretty much become a recluse.

“I never kicked him out of the band,” Schon recently told Classic Rock magazine. “He chose not to perform. He didn’t want to sing with us. He didn’t want to do a record. He didn’t want to do anything. That was pretty much where he was at. And he wanted us to sit still and not do anything too. But we all wrote the music together, and I think we’re all entitled to play it if we wish.

“Steve and I had some really amazing times together,” he said. “He was a very funny guy, and he liked to party with the best of us. In the beginning, when we wrote ‘Lights’ and ‘Patiently,’ the first two songs we wrote together, we realized immediately that we had chemistry. And the guy was really funny to be around, so we hung out all the time. We were very tight and close for years and years.”

What remains to be seen: If this weekend’s performance is the first step in a comeback for the gifted vocalist. Like we said, don’t stop believin’.