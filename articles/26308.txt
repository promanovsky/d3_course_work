Google has celebrated the spring equinox, with a colourful, animated Doodle on its search page.

Users who land on the search engine’s homepage will see a black and white line drawing of a man holding a watering can.

The character then comes to life, and walks along the top of the search bar, pouring water as he goes. Sprouts shoot up from the white background, and grow into a row of colourful flowers which spell out ‘Google’. After sprinkling water on his head, the character takes his place as the letter ‘g’.

Also known as the vernal equinox, the 20 March is the first day of spring.

During an equinox, the Earth’s north and south poles are not tilted toward or away from the sun. This phenomena occurs twice a year: on 20 March and on 22 September.

English Heritage have confirmed that, weather permitting, Stonehenge will be open from the start of the equinox at 5:45am until 8:30am, to allow Druids and Pagans to gather and see the sun rise above the ancient stones, according the IB Times.

INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Please enter your email address Please enter a valid email address Please enter a valid email address SIGN UP Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here INDY/ LIFE Newsletter Be inspired with the latest lifestyle trends every week Thanks for signing up to the INDY/LIFE newsletter {{#verifyErrors}} {{message}} {{/verifyErrors}} {{^verifyErrors}} {{message}} {{/verifyErrors}} The Independent would like to keep you informed about offers, events and updates by email, please tick the box if you would like to be contacted

Read our full mailing list consent terms here