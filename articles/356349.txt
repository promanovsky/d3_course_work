Accra: The United Nations health agency said on Thursday it expected the worst Ebola outbreak in history to continue its deadly rampage through west Africa for at least “several months”.

The highly-contagious tropical bug has infected hundreds of people in Guinea, Liberia and Sierra Leone, with the latest World Health Organisation (WHO) figures showing that confirmed or suspected cases had left 467 people dead and experts fearing it could spread throughout the region.

Keiji Fukuda, the UN agency’s assistant director-general of health security, said at the close of a regional summit of health ministers on the crisis it was “impossible to give a clear answer” on how far the epidemic could spread or when it might begin to retreat.

“I certainly expect that we are going to be dealing with this outbreak minimum for a few months to several months,” he said.

“I really hope for us to see a turnaround where we begin to see a decrease in cases in the next several weeks.”

Marie-Christine Ferir, of medical aid agency Doctors Without Borders (MSF), echoed the assessment, saying the outbreak could “continue for about a few weeks, or perhaps months in certain parts”.

The warning came as health ministers from 12 nations wrapped up two days of talks in Accra with global experts in communicable diseases, with debate raging over the measures required to stop Ebola in its tracks.

They were expected to make a raft of recommendations to regional governments and to WHO on containing the disease, including the launch of a $10 million (Dh36.7 million) war chest to boost medical aid in the worst hit regions.

There are five species of Ebola, three of which — Zaire, Sudan and Bundibugyo — can kill humans.

Zaire Ebola, the deadliest and the species behind the current outbreak, can fell its victims within days, causing severe fever and muscle pain, weakness, vomiting and diarrhoea — in some cases shutting down organs and causing unstoppable bleeding.

There have been 21 Ebola outbreaks — not including isolated cases involving only one patient — since the virus first spread to humans in the Democratic Republic of Congo, then known as Zaire, in 1976.

Before the current crisis, Ebola had killed 1,587 people, two-thirds of those infected, according to an AFP tally based on WHO data.

The death rate for Ebola has been widely but incorrectly reported as “90 per cent” by global media and some scientists, probably because around that percentage died in the original outbreak and a subsequent epidemic in neighbouring Congo-Brazzaville in 2003.

The mortality rate in the west African epidemic is slightly lower than the average, at 61.5 per cent, but it dwarves every other outbreak in terms of the geographical spread and the number of cases and deaths.

MSF said last week the outbreak was “out of control”, with more than 60 hotspots.

However, experts say those who receive correct care — paracetamol to contain fevers, rehydration for diarrhoea and antibiotics for secondary infections — have a greatly improved chance of survival.

Ministers from Guinea, where 413 confirmed, suspected and probable cases have surfaced so far, including 303 deaths, and Liberia, which has seen 107 cases and 65 deaths, attended the Accra conference.

Sierra Leone, which has recorded 239 cases and 99 deaths, was also represented.

In addition, officials from Ivory Coast, Mali, Senegal, Gambia and Guinea-Bissau, along with Ghana and countries as far afield as Uganda and the Democratic Republic of Congo attended.

They have been joined by a host of UN agencies and other aid organisations, including MSF and the Red Cross, as well as personnel from disease control centres in western Africa, the United States, Britain and the European Union.

One of the biggest obstacles to combatting the epidemic, say health experts, has been traditional practices — such as touching the bodies of victims at their funerals — which are causing the virus to spread.

Ministers and experts set out a strategy placing traditional village elders — who are often more influential than foreign medical experts among the traditional forest-dwelling populations at the epicentre of the outbreak — at the forefront of an education drive, a delegate said.

“We have agreed to mobilise community leaders to be active participants in the efforts to curb the spread of the disease by building their confidence to trust the health workers who are in their communities,” said Abdulsalami Nasidi, of the Nigeria Centre for Disease Control.