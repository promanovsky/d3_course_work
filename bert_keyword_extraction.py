# https://towardsdatascience.com/keyword-extraction-with-bert-724efca412ea
# https://towardsdatascience.com/conditional-text-generation-by-fine-tuning-gpt-2-11c1a9fc639d
# https://medium.com/swlh/fine-tuning-gpt-2-for-magic-the-gathering-flavour-text-generation-3bafd0f9bb93

from sklearn.feature_extraction.text import CountVectorizer
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import itertools
import os


# метод вычитывает содержимое директории posts_data - txt файлы в кодировке utf-8
def load_files_from_dir(dir_name='posts_data', ignore=None):
    result = {}
    for dir_name, subdirs, files in os.walk(dir_name):
        if ignore and os.path.basename(dir_name) in ignore:
            continue

        for file_name in files:
            if ignore and file_name in ignore:
                continue

            with open(os.path.join(dir_name, file_name), 'r+', encoding='utf-8') as f:
                result[file_name] = f.read().replace('\n', '')
    return result


# all_posts = load_files_from_dir()
# item = all_posts.popitem()
# doc = item[1]
# print('title : ', item[0])

doc = """
What web frameworks should be learned in 2020?
The world will never be the same due to web development. It is one of the leading areas of human activity in terms of relevance, influence and degree of involvement.
The React framework is gaining constant popularity, supported by the use of hooks in the framework – something between the simplicity of components and the complexity of Redux. Hooks are a promising technology that really simplifies code.
“I personally prefer Angular. It has been very popular for almost 10 years. I believe it is the most holistic and mature framework that allows solving the most complicated tasks,” says Dmitry Parshin, the Head of Artezio Development Center.

At the same time, the number of vacancies for Angular is only slightly less than the number of vacancies for React. Angular is supported on various platforms (web, mobile devices, native desktop), it is powerful, modern, has a great ecosystem, and is just cool. Angular offers not only tools but also design templates for creating a serviced project. If you create an Angular app correctly, you won’t have a mess with classes and methods that are hard to edit and even harder to test. The code is conveniently structured, and it is easy to make sense of it.
As for Vue.JS, its popularity and potential are still in doubt. There is a lot of hype around it—it is simpler than React and Angular, but serious large companies prefer solutions behind Facebook (React) or Google (Angular) to the development of Chinese specialists – there just aren’t many open positions in the market yet.
Modern backend frameworks provide design patterns, architecture templates, for example MVC, RESTful api. Python, one of the most popular programming language now and Django, the free framework for web applications in this language, use the MVC design pattern. You can get the basic mechanisms for working with databases, authorization, a function library, components, an integrated security system, and much more out of the box.
Another popular MVC framework written in the Ruby language is Ruby on Rails that has many packages and is actively supported. It was one of the first to implement code generation based on the designed database. PHP, the most widespread language, should also be mentioned as well as its frameworks that are worth studying for PHP programmers – Laravel, Yii, Zend, Symfony.
Let’s consider in more detail client frameworks based on JavaScript. Unlike server-side, client-side frameworks are not connected with the server logic of the app and work in the browser. JavaScript developers often face the choice of which framework to use for developing a web application.
According to Dmitry Parshin, today there are three popular frameworks that are actively used in the market and are being developed: Angular managed by Google, React released by Facebook, and Vue, developed by Evan You, a former Google engineer who worked with AngularJS and eventually decided to create his own lightweight framework.
If you look at the ratings of frameworks and libraries, then the most popular in the world is React. It has one big advantage when it comes to transition from the lowest version to the latest – you don’t need to rewrite half of the code, unlike, for example, Angular, when you need to switch from AngularJS to a new version, you will have to rewrite almost everything from scratch. Thus, in the entire history of React, it has not changed globally, but has new features, such as context.
Alternatively, Angular has changed a lot. This can also be considered a benefit, especially for new projects for which this framework provides everything out of the box. There is no need to invent anything, to write additional functionality to access the backend, unlike React. Besides, Angular implements a full-fledged SPA application.
Vue.js is similar to Angular. It was supplemented with convenient functionality, for example, “syntactic sugar”, which allows you to use code more conveniently. Different services were removed, and some features of other frameworks were taken into account. In fact, Vue is the quickest framework today.
In terms of distinguishing View and BL code, Angular is one of the best. View can be developed by a designer, and BL – by a developer. In React, everything can be mixed up via JSX, a syntax extension to JavaScript, and many may not like it despite the fact that it is a great interface-oriented tool, especially for those who are used to writing in Angular.
Angular contains everything you need, which is an advantage, because you can write a project of almost any complexity. A good frontend developer should try all three engines to decide for himself which solution is more convenient and prepare for an easier transition from one framework to another. Thus, all three engines will find their application in the market and will develop simultaneously.
At the same time, the framework that is most supported by the community and is constantly evolving with open source code, will be in most demand.
"""

n_gram_range = (3, 3)
stop_words = "english"

# Extract candidate words/phrases
count = CountVectorizer(ngram_range=n_gram_range, stop_words=stop_words).fit([doc])
candidates = count.get_feature_names()

model = SentenceTransformer('distilbert-base-nli-mean-tokens')
doc_embedding = model.encode([doc])
candidate_embeddings = model.encode(candidates)

top_n = 20
distances = cosine_similarity(doc_embedding, candidate_embeddings)
keywords = [candidates[index] for index in distances.argsort()[0][-top_n:]]

print(keywords)


# Max Sum Similarity
def max_sum_sim(doc_embedding, word_embeddings, words, top_n, nr_candidates):
    # Calculate distances and extract keywords
    distances = cosine_similarity(doc_embedding, candidate_embeddings)
    distances_candidates = cosine_similarity(candidate_embeddings,
                                             candidate_embeddings)

    # Get top_n words as candidates based on cosine similarity
    words_idx = list(distances.argsort()[0][-nr_candidates:])
    words_vals = [candidates[index] for index in words_idx]
    distances_candidates = distances_candidates[np.ix_(words_idx, words_idx)]

    # Calculate the combination of words that are the least similar to each other
    min_sim = np.inf
    candidate = None
    for combination in itertools.combinations(range(len(words_idx)), top_n):
        sim = sum([distances_candidates[i][j] for i in combination for j in combination if i != j])
        if sim < min_sim:
            candidate = combination
            min_sim = sim

    return [words_vals[idx] for idx in candidate]


print(max_sum_sim(doc_embedding, candidate_embeddings, candidates, top_n=5, nr_candidates=20))


def mmr(doc_embedding, word_embeddings, words, top_n, diversity):
    # Extract similarity within words, and between words and the document
    word_doc_similarity = cosine_similarity(word_embeddings, doc_embedding)
    word_similarity = cosine_similarity(word_embeddings)

    # Initialize candidates and already choose best keyword/keyphras
    keywords_idx = [np.argmax(word_doc_similarity)]
    candidates_idx = [i for i in range(len(words)) if i != keywords_idx[0]]

    for _ in range(top_n - 1):
        # Extract similarities within candidates and
        # between candidates and selected keywords/phrases
        candidate_similarities = word_doc_similarity[candidates_idx, :]
        target_similarities = np.max(word_similarity[candidates_idx][:, keywords_idx], axis=1)

        # Calculate MMR
        mmr = (1 - diversity) * candidate_similarities - diversity * target_similarities.reshape(-1, 1)
        mmr_idx = candidates_idx[np.argmax(mmr)]

        # Update keywords & candidates
        keywords_idx.append(mmr_idx)
        candidates_idx.remove(mmr_idx)

    return [words[idx] for idx in keywords_idx]


print(mmr(doc_embedding, candidate_embeddings, candidates, top_n=5, diversity=0.7))
