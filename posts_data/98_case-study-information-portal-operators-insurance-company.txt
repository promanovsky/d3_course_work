Artezio was challenged with developing an information portal for an insurance company to train new employees, place the information materials, search it and view it. Also, the Customer had high demands for assigning user rights access and integrating with certain systems.
Artezio engineers have successfully created the system that now can support simultaneous work of more than 200 users and process more than one million documents of different formats (Word, Excel, RTF, HTML, JPEG, PNG, GIF, etc.) with high performance.
Learn more about the solution here
