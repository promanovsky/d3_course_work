Many companies today aim at creating software solutions that can run on all mobile platforms and PCs at the same time. And this is particularly important when developers build an app that motivates people to use it in any situation and any place.
What We Created:
– An Uber-app for mutual help. It is a unique solution that we started building after a detailed market analysis. It turned out that there were no similar apps allowing users to ask for help quickly. We only found very simple apps that should be bought or instead of getting help, users had to watch ads. Therefore, we developed a new concept of a completely free app designed for both iOS and Android platforms.
While considering tools for development, we faced the choice between NativeScript and React Native. We wanted to evaluate these technologies and get a program solution applicable to all platforms.
At first, we chose NativeScript, but then we quickly realized that we had made a mistake due to problems with the creation of native components and setting up routing.
Thus, the development team switched to React Native, since it was easy and convenient to work with. We received a simple process for building native control components and, as a result, the development process became clear and easily structured.

There are downsides in any development environment, and React Native is no exception:
– An inconvenient process of app update to the latest versions of the target operation systems. During each update, it is required to create files all over again that are responsible for the native code applied in iOS or Android.
– Growing dependency on plugins used for work with native components (to interact with cameras, maps, etc.). Besides, components often contain errors that cannot be fixed immediately.
– React Native requires constant development and rework.
– React Native provides an extremely convenient templating with JSX.
– We have a single control code for both platforms (iOS and Android) written in JavaScript (based on the ES6 standard).
– A fast development process. As long as technical approaches to project implementation are thoroughly examined, the project development advances rapidly.
– There are no limitations on the choice of IDE. For convenient work with ES6 and React Native components, we used free Visual Studio Code.
JavaScript – a convenient scripting programming language. Artezio has considerable expertise in its application.
XCode – a development environment for configuring a native part of iOS apps.
Fabric – a platform for distributing iOS apps.
Crashlytics – a service used for monitoring app errors on devices.
As a result, we gained excellent experience applying React Native and JavaScript in the development process. However, it is hardly effective to build apps extensively using native components (for example, games), but service apps can be quickly designed provided there is relevant competence. Mobile apps are developing rapidly today, and demand for mobile development is steadily growing. Thus, we are certain that Artezio expertise in the field of fast and effective creation of technological apps will definitely be in high demand.
