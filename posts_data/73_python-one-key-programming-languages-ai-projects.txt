The research on perspective of programming tools usage had been held at the company during 5 years. The results cover the data about the efficiency of AI solutions provided by the developers engaged in related projects.
The goal of the research was to find out the ways and tools to increase efficiency of AI projects development.
Python as one of the leading programming languages in the AI field has shown its reasonability on many projects at Artezio.
“Today more and more developers choose Python as a multi-purpose language for modeling and creation of API and for learning processes managing,” says the Head of Artezio R&D Department and Professor of National Research University High School of Economics Vladimir Krylov.

 
