from datetime import datetime
import numpy as np
import random
from sklearn.metrics.pairwise import cosine_similarity
from transformers import AutoTokenizer, AutoConfig, AutoModelForPreTraining
import torch
from torch.utils.data import Dataset


MODEL = 'gpt2'

SPECIAL_TOKENS = {"bos_token": "<|BOS|>",
                  "eos_token": "<|EOS|>",
                  "unk_token": "<|UNK|>",
                  "pad_token": "<|PAD|>",
                  "sep_token": "<|SEP|>"}
MAXLEN = 768  # {768, 1024, 1280, 1600}
DEBUG = False
n_gram_range = (3, 3)
stop_words = "english"


# токенайзер для спец токенов
def get_tokenizer(special_tokens=None):
    tokenizer = AutoTokenizer.from_pretrained(MODEL)  # GPT2Tokenizer

    if special_tokens:
        tokenizer.add_special_tokens(special_tokens)
        print("Special tokens added")
    return tokenizer


# функция создает или загружает модель, подключает к окружению CUDA, добавляет токены
def get_model(tokenizer, special_tokens=None, load_model_path=None):
    # GPT2LMHeadModel
    if special_tokens:
        config = AutoConfig.from_pretrained(MODEL,
                                            bos_token_id=tokenizer.bos_token_id,
                                            eos_token_id=tokenizer.eos_token_id,
                                            sep_token_id=tokenizer.sep_token_id,
                                            pad_token_id=tokenizer.pad_token_id,
                                            output_hidden_states=False)
    else:
        config = AutoConfig.from_pretrained(MODEL,
                                            pad_token_id=tokenizer.eos_token_id,
                                            output_hidden_states=False)

        # ----------------------------------------------------------------#
    model = AutoModelForPreTraining.from_pretrained(MODEL, config=config)

    if special_tokens:
        # Special tokens added, model needs to be resized accordingly
        model.resize_token_embeddings(len(tokenizer))

    if load_model_path:
        if torch.cuda.is_available():
            model.load_state_dict(torch.load(load_model_path))
            model.cuda()
        else:
            model.load_state_dict(torch.load(load_model_path, map_location=torch.device('cpu')))

    return model


# класс для удобства работы с данными
class myDataset(Dataset):

    def __init__(self, data, tokenizer, randomize=True):

        title, text, keywords = [], [], []
        for k, v in data.items():
            title.append(v[0])
            text.append(v[1])
            keywords.append(v[2])

        self.randomize = randomize
        self.tokenizer = tokenizer
        self.title = title
        self.text = text
        self.keywords = keywords

        # ---------------------------------------------#

    @staticmethod
    def join_keywords(keywords, randomize=True):
        N = len(keywords)

        # random sampling and shuffle
        if randomize:
            M = random.choice(range(N + 1))
            keywords = keywords[:M]
            random.shuffle(keywords)

        return ','.join(keywords)

    # ---------------------------------------------#

    def __len__(self):
        return len(self.text)

    # ---------------------------------------------#

    def __getitem__(self, i):
        keywords = self.keywords[i].copy()
        kw = self.join_keywords(keywords, self.randomize)

        if DEBUG:
            print(' getItem >>>', kw)

        input = SPECIAL_TOKENS['bos_token'] + self.title[i] + \
                SPECIAL_TOKENS['sep_token'] + kw + SPECIAL_TOKENS['sep_token'] + \
                self.text[i] + SPECIAL_TOKENS['eos_token']

        encodings_dict = self.tokenizer(input,
                                        truncation=True,
                                        max_length=MAXLEN,
                                        padding="max_length")

        input_ids = encodings_dict['input_ids']
        attention_mask = encodings_dict['attention_mask']

        return {'label': torch.tensor(input_ids),
                'input_ids': torch.tensor(input_ids),
                'attention_mask': torch.tensor(attention_mask)}


# реализация Maximal Marginal Relevance метода для разнообращия ключевых слов в случае длинных н-грамм
def mmr(doc_embedding, word_embeddings, words, top_n, diversity):
    word_doc_similarity = cosine_similarity(word_embeddings, doc_embedding)
    word_similarity = cosine_similarity(word_embeddings)

    keywords_idx = [np.argmax(word_doc_similarity)]
    candidates_idx = [i for i in range(len(words)) if i != keywords_idx[0]]

    for _ in range(top_n - 1):
        candidate_similarities = word_doc_similarity[candidates_idx, :]
        target_similarities = np.max(word_similarity[candidates_idx][:, keywords_idx], axis=1)

        mmr = (1 - diversity) * candidate_similarities - diversity * target_similarities.reshape(-1, 1)
        if mmr.size:
            mmr_idx = candidates_idx[np.argmax(mmr)]
            keywords_idx.append(mmr_idx)
            candidates_idx.remove(mmr_idx)

    return [words[idx] for idx in keywords_idx]


def decoding_results(sample_outputs, tokenizer, keywords, title):
    print('start decoding >>> {}'.format(datetime.now()))
    results = []
    for i, sample_output in enumerate(sample_outputs):
        text = tokenizer.decode(sample_output, skip_special_tokens=True)
        a = len(title) + len(','.join(keywords))
        results.append(text[a:])
    print('decoding end >>> {}'.format(datetime.now()))
    return results
