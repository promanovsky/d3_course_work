import {Pipe, PipeTransform} from '@angular/core';
import {isIterable} from 'rxjs/internal-compatibility';

@Pipe({
  name: 'documentContent'
})
export class DocumentContentPipe implements PipeTransform {

  private regExp = /\[IMG_[0-9]*\]/g;

  transform(value: string, ...args: unknown[]): string {
    const imgArr = value.match(this.regExp);
    if (imgArr && isIterable(imgArr)) {
      for (let v of imgArr) {
        if (v) {
          value = value.replace(v, '<img src=\'assets/img/' +
            v.replace('[', '').replace(']', '') +
            '.png\' alt=\'\'>');
        }
      }
    }
    return value.replace(/\n/g, '<br/>');
  }

}
