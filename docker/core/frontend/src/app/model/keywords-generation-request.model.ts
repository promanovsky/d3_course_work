export class KeywordsGenerationRequest {
  articleText: string;

  constructor(articleText: string) {
    this.articleText = articleText;
  }
}
