export class TextGenerationRequest {
  keywords: string[];
  textSearchAlgorithm: string;
}
