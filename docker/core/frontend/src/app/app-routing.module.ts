import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ContentComponent} from './components/content/content.component';

export const routes: Routes = [
  {
    path: '',
    component: ContentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
