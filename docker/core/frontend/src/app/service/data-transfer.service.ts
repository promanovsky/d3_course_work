import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {KeywordsGenerationRequest} from '../model/keywords-generation-request.model';
import {KeywordsGenerationResponse} from '../model/keywords-generation-response.model';
import {TextGenerationRequest} from '../model/text-generation-request.model';
import {TextGenerationResponse} from '../model/text-generation-response.model';


@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  private askUrl = '/api';

  constructor(private httpClient: HttpClient) {
  }

  generateKeywords = (request: KeywordsGenerationRequest): Observable<KeywordsGenerationResponse> => {
    return this.httpClient.post<KeywordsGenerationResponse>(this.askUrl + '/keywords', request, {
      observe: 'body',
      responseType: 'json'
    });
  }

  generateText = (request: TextGenerationRequest): Observable<TextGenerationResponse> => {
    return this.httpClient.post<TextGenerationResponse>(this.askUrl + '/text', request, {
      observe: 'body',
      responseType: 'json'
    });
  }

}
