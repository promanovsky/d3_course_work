import {Component, OnInit} from '@angular/core';
import {DataTransferService} from '../../service/data-transfer.service';
import {Observable, of} from 'rxjs';
import {TextGenerationResponse} from '../../model/text-generation-response.model';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  generationKeywords = false;
  generationText = false;

  articleText = '';
  generatedKeywords = '';

  textResult: Observable<TextGenerationResponse>;

  textSearchAlgorithm: 'top-p';

  constructor(private service: DataTransferService) {
  }

  ngOnInit(): void {
  }

  generateKeywords() {
    this.generationKeywords = true;
    this.service.generateKeywords({articleText: this.articleText}).subscribe(resp => {
      console.log(resp);
      this.generatedKeywords = resp.keywords.join(',');
      this.generationKeywords = false;
    }, error => {
      this.generationKeywords = false;
      alert('Keywords generation error = ' + error.message);
    });
  }

  generateText() {
    const keywords = this.generatedKeywords.split(',');
    this.generationText = true;
    this.service.generateText({keywords, textSearchAlgorithm: this.textSearchAlgorithm}).subscribe(resp => {
      this.textResult = of(resp);
      this.generationText = false;
    }, error => {
      this.generationText = false;
      alert('Text generation error = ' + error.message);
    });
  }

}
