from http.server import BaseHTTPRequestHandler
import socketserver
import json
import cgi
from datetime import datetime
from sklearn.feature_extraction.text import CountVectorizer
from sentence_transformers import SentenceTransformer
import torch

from common.declarations import get_tokenizer, SPECIAL_TOKENS, get_model, n_gram_range, stop_words, mmr, \
    myDataset, MAXLEN, decoding_results


print(f"PyTorch version: {torch.__version__}")

port = 7575
host = 'd3_core'
# host = 'localhost'

# непосредственное создание токенайзера и загрузка модели
tokenizer = get_tokenizer(special_tokens=SPECIAL_TOKENS)
model = get_model(tokenizer,
                  special_tokens=SPECIAL_TOKENS,
                  load_model_path='model/pytorch_model_V2.bin')

sentenceModel = SentenceTransformer('distilbert-base-nli-mean-tokens')

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class RequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        self.send_header('Content-type', 'application/json')

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.get_content_type())

        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return

        self._set_headers()
        if self.path == '/api/keywords':
            req = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
            article_text = req['articleText']

            count = CountVectorizer(ngram_range=n_gram_range, stop_words=stop_words).fit([article_text])
            candidates = count.get_feature_names()
            doc_embedding = sentenceModel.encode([article_text])
            candidate_embeddings = sentenceModel.encode(candidates)
            keywords = mmr(doc_embedding, candidate_embeddings, candidates, top_n=5, diversity=0.7)
            self.wfile.write(bytes(json.dumps({"keywords": keywords}), encoding='utf-8'))
        else:
            if self.path == '/api/text':
                req = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
                keywords = req['keywords']
                search_algorithm = req['textSearchAlgorithm']

                title = "article"
                kw = myDataset.join_keywords(keywords, randomize=False)

                prompt = SPECIAL_TOKENS['bos_token'] + title + \
                         SPECIAL_TOKENS['sep_token'] + kw + SPECIAL_TOKENS['sep_token']

                generated = torch.tensor(tokenizer.encode(prompt)).unsqueeze(0)
                generated = generated.to(device)
                print('working on type device = {}'.format(device))
                model.eval()

                results = []

                if search_algorithm == 'top-p':
                    print('start generation >>> algorithm = {0}, time = {1}'.format(search_algorithm, datetime.now()))
                    sample_outputs = model.generate(generated,
                                                    do_sample=True,
                                                    min_length=MAXLEN,
                                                    max_length=MAXLEN,
                                                    top_k=30,
                                                    top_p=0.7,
                                                    temperature=0.9,
                                                    repetition_penalty=2.0,
                                                    num_return_sequences=3)

                    print('end generation >>> algorithm = {0}, time = {1}'.format(search_algorithm, datetime.now()))
                    results = decoding_results(sample_outputs, tokenizer, keywords, title)

                if search_algorithm == 'beam-search':
                    print('start generation >>> algorithm = {0}, time = {1}'.format(search_algorithm, datetime.now()))
                    sample_outputs = model.generate(generated,
                                                    do_sample=True,
                                                    max_length=MAXLEN,
                                                    num_beams=5,
                                                    repetition_penalty=5.0,
                                                    early_stopping=True,
                                                    num_return_sequences=3)
                    print('end generation >>> algorithm = {0}, time = {1}'.format(search_algorithm, datetime.now()))
                    results = decoding_results(sample_outputs, tokenizer, keywords, title)

                if len(results) > 0:
                    self.wfile.write(bytes(json.dumps({"texts": results}), encoding='utf-8'))
                else:
                    self.wfile.write(bytes(json.dumps({"error": "empty answer"}), encoding='utf-8'))
            else:
                self.wfile.write(bytes(json.dumps({"error": "wrong api path"}), encoding='utf-8'))


handler = RequestHandler
handler.timeout = None
with socketserver.TCPServer((host, port), handler) as httpd:
    print('SERVER READY FOR CONNECTIONS')
    httpd.serve_forever()
