# https://huggingface.co/transformers/model_doc/pegasus.html
# https://medium.com/towards-artificial-intelligence/summarization-using-pegasus-model-with-the-transformers-library-553cd0dc5c2

import torch
from transformers import PegasusForConditionalGeneration, PegasusTokenizer

src_text = [ """
The pace of business automation in the United States in 2020 will grow in all areas. Artezio analysts expressed this idea based on information obtained while communicating with the representatives of US companies.
In the coming year, businesses are expected to grow their interest in automation technologies.
Today we see that the demand for high tech solutions is growing in all areas. It is obvious that automation and application of solutions based, for example, on AI, become a competitive advantage in the market and allow building effective schemes for production and services,   – says Dmitry Rodionov, Director of Artezio office in the USA.
Therefore, for our part, we expect the development of the market and business due to new technologies, and, as a result, an increase in the number of requests to replace obsolete business schemes with new automation solutions, he adds.
AI will be increasingly applied for copyright protection.
The possibilities of using artificial intelligence to solve various problems are very wide. The effectiveness of AI is high when we talk about automation of some routine processes or analysis of large data arrays. Registration and monitoring of intellectual property is exactly the area in which the use of AI is almost the only way to achieve high results. Artificial intelligence has already been successfully applied to protect intellectual property. For example, Google uses AI technologies for copyright protection of music and visual content, admits Artezio CEO.
According to Pavel Adylin, special intelligent software solutions have been developed to identify cases of illegal use of intellectual property and to prevent such cases.
Indeed, now some of the existing software solutions can prevent actions of those who use objects protected by copyright. However, many experts see the greatest benefit from the use of AI in the field of intellectual property protection not so much in protecting copyright objects as in helping with registering patents and tracking changes in them. Scientific developments can now change over time or be supplemented. Sometimes a copyright object may completely change, which leads to the need to re-register or obtain a separate patent. In this case, the use of AI can help people effectively track changes in protected objects and quickly rebuild legislative protection based on how the author’s product has changed, says Artezio top manager.
"""
]

torch_device = 'cuda' if torch.cuda.is_available() else 'cpu'

model_name = 'google/pegasus-reddit_tifu'
tokenizer = PegasusTokenizer.from_pretrained(model_name)
model = PegasusForConditionalGeneration.from_pretrained(model_name).to(torch_device)
batch = tokenizer.prepare_seq2seq_batch(src_text, truncation=True, padding='longest', return_tensors="pt").to(torch_device)
translated = model.generate(**batch)
tgt_text = tokenizer.batch_decode(translated, skip_special_tokens=True)

print(tgt_text[0])
