"""
Соберем данные постов блога и сохраним их в директории posts_data в отдельных файлах
"""
import requests
from bs4 import BeautifulSoup as soup
from selenium import webdriver
import os

par_dir = os.path.abspath(os.pardir)
posts_urls = set()

dir_name = 'posts_data'
base_url_part = 'https://www.artezio.com/pressroom/blog/'

for pageNum in range(1, 19):
    url = (base_url_part + 'page/{}/').format(pageNum)
    with webdriver.PhantomJS() as browser:
        browser.get(url)
        html = browser.page_source
        doc = soup(html, 'html.parser')
        posts = doc.select('.block-title > h3 > a')
        for post in posts:
            posts_urls.add(post['href'])

print('combined posts urls count = ', len(posts_urls))

if not os.path.exists('posts_data'):
    os.makedirs('posts_data')


def parse_post_and_save_text(url, num):
    result = requests.get(url)
    doc = soup(result.text, 'html.parser')
    post_data = ''
    for tag_element in doc.select('div.content div.row div.col-md-9 p'):
        post_data += tag_element.text + '\n'

    file_name = str(num) + '_' + url.replace(base_url_part, '').replace('/', '.txt')
    with open(os.path.join(dir_name, file_name), 'w+', encoding='utf-8') as f:
        f.write(post_data)


for ind, url in enumerate(posts_urls):
    parse_post_and_save_text(url, ind)
